require( "WeaponUpgrades.lua" )
require( "ShopItem.lua" )

ItemCategory = {
    weapons = 1,
    perks = 2,
    items = 3,
    cash = 4,

    catNum = 4
}

ItemShopShareReward = {
    soft = 50,
    hard = 0,
    fuel = 0
}

ItemUnlockLevelRange = -1

--[[
 Item fields:
  f - asset with item gfx
  a - asset with item gfx for avatar
  d - item name
  desc - item description
  cs - price soft + upgrade costs
  ch - price hard + upgrade costs
  cu - unlock price if lvl to low
  bonus - bonus % for cash items
  ord - order of display for cash items
  lvl - min player level
  id - shop id
  n - item's name in registry
  cat - which shop category should item go to
  max - how many items can be owned
  maxPerGame - how many items can be bought during single game in quick shop
  dmg - damage stars
  exclusive - no secondar weapon can be slected on premission screen
  rate - fire rate stars
  clip - clip size stars
]]--

ItemSmg = {
    f = "gfx/weapons/weap_smg.png@linear",
    a = { file = "gfx/weapons/weap_smg_dum.png@linear", px = 15, py = 46 },
    d = "TEXT_ITEM_SMG",
    cs = {0, 80, 120,  0,  0,   0},
    ch = {0, 0, 0, 40, 64, 80},
    lvl = 0,
    id = ShopItem.SMG,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {0, 0, 1, 1, 1, 1},
    rate = {0, 1, 1, 1, 2, 3},
    clip = {0, 0, 0, 1, 1, 1},
    upgrades = UpgradeUzi,
    atlas = AtlasSet.Player16
}

ItemShotgun = {
    f = "gfx/weapons/weap_shotgun.png@linear",
    a = { file = "gfx/weapons/weap_shotgun_dum.png@linear", px = 14, py = 67 },
    d = "TEXT_ITEM_SHOTGUN",
    cs = {640, 80, 120,  0,  0,   0},
    ch = {  0, 0, 0, 40, 72, 96},
    cu = 10,
    lvl = 1,
    id = ShopItem.Shotgun,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {0, 1, 1, 2, 2, 2},
    rate = {0, 0, 0, 0, 1, 1},
    clip = {0, 0, 1, 1, 1, 2},
    upgrades = UpgradeShotgun,
    atlas = AtlasSet.Player14
}

ItemPistol = {
    f = "gfx/weapons/weap_spitter.png@linear",
    a = { file = "gfx/weapons/weap_spitter_dum.png@linear", px = 14, py = 83 },
    d = "TEXT_ITEM_PISTOL",
    cs = {800, 80, 120,  0,   0,   0},
    ch = {   0, 0, 0, 64, 96, 120},
    cu = 40,
    lvl = 2,
    id = ShopItem.Pistol,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {0, 1, 1, 1, 2, 2},
    rate = {0, 0, 1, 1, 1, 2},
    clip = {1, 1, 1, 2, 2, 2},
    upgrades = UpgradePistol,
    atlas = AtlasSet.Player07
}

ItemNailer = {
    f = "gfx/weapons/weap_nailer.png@linear",
    a = { file = "gfx/weapons/weap_nailer_dum.png@linear", px = 19, py = 81 },
    d = "TEXT_ITEM_NAILER",
    cs = {1400, 80, 120,  0,   0,   0},
    ch = {   0, 0, 0, 64, 88, 104},
    cu = 80,
    lvl = 4,
    id = ShopItem.Nailer,
    cat = ItemCategory.weapons,
    max = 1,
    funcs = WeaponMission,
    dmg  = {1, 1, 2, 2, 3, 3},
    rate = {0, 0, 0, 1, 1, 1},
    clip = {0, 1, 1, 1, 1, 2},
    upgrades = UpgradeNailer,
    atlas = AtlasSet.Player15
}

ItemRipper = {
    f = "gfx/weapons/weap_cutter.png@linear",
    a = { file = "gfx/weapons/weap_cutter_dum.png@linear", px = 15, py = 65 },
    d = "TEXT_ITEM_RIPPER",
    cs = {2400, 80, 120,  0,   0,   0},
    ch = {   0, 0, 0, 64, 96, 120},
    cu = 160,
    lvl = 6,
    id = ShopItem.Ripper,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {1, 2, 2, 2, 3, 3},
    rate = {0, 0, 1, 2, 2, 2},
    clip = {1, 1, 1, 1, 1, 2},
    upgrades = UpgradeRipper,
    atlas = AtlasSet.Player09
}

ItemSpiral = {
    f = "gfx/weapons/weap_spiral_gun.png@linear",
    a = { file = "gfx/weapons/weap_spiral_gun_dum.png@linear", px = 21, py = 101 },
    d = "TEXT_ITEM_SPIRAL",
    cs = {4000,  0,  0,   0,   0,   0},
    ch = {   0, 32, 48, 80, 136, 160},
    cu = 280,
    lvl = 8,
    id = ShopItem.Spiral,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {0, 0, 1, 2, 2, 2},
    rate = {1, 2, 2, 2, 3, 3},
    clip = {1, 1, 1, 1, 1, 2},
    upgrades = UpgradeSpiral,
    atlas = AtlasSet.Player17
}

ItemRocketLauncher = {
    f = "gfx/weapons/weap_rpg.png@linear",
    a = { file = "gfx/weapons/weap_rpg_dum.png@linear", px = 33, py = 53 },
    d = "TEXT_ITEM_LAUNCHER",
	cs = {7600,  0,  0,   0,   0,   0},
    ch = {   0, 32, 56, 96, 152, 184},
    cu = 400,
    lvl = 10,
    id = ShopItem.RocketLauncher,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {2, 2, 2, 3, 3, 3},
    rate = {0, 1, 2, 2, 2, 2},
    clip = {1, 1, 1, 1, 2, 3},
    upgrades = UpgradeRocketLauncher,
    atlas = AtlasSet.Player11
}

ItemElectricity = {
    f = "gfx/weapons/weap_electro.png@linear",
    a = { file = "gfx/weapons/weap_electro_dum.png@linear", px = 23, py = 90 },
    d = "TEXT_ITEM_ELECTRICITY",
    cs = {11200,  0,  0,   0,   0,   0},
    ch = {    0, 40, 56, 104, 160, 176},
    cu = 600,
    lvl = 12,
    id = ShopItem.Electricity,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {1, 2, 2, 2, 2, 3},
    rate = {1, 1, 2, 3, 3, 3},
    clip = {1, 1, 1, 1, 2, 2},
    upgrades = UpgradeElectricity,
    atlas = AtlasSet.Player13
}

ItemFlamer = {
    f = "gfx/weapons/weap_flamethrower.png@linear",
    a = { file = "gfx/weapons/weap_flamethrower_dum.png@linear", px = 11, py = 70 },
    d = "TEXT_ITEM_FLAMER",
    cs = {16800,  0,  0,   0,   0,   0},
    ch = {    0, 40, 72, 104, 176, 200},
    cu = 800,
    lvl = 14,
    id = ShopItem.Flamer,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {2, 2, 3, 3, 3, 4},
    rate = {1, 1, 1, 2, 2, 2},
    clip = {1, 2, 2, 2, 3, 3},
    upgrades = UpgradeFlamer,
    atlas = AtlasSet.Player10
}

ItemMagnum = {
    f = "gfx/weapons/weap_dual_magnum.png@linear",
    a = { file = "gfx/weapons/weap_dual_magnum_dum.png@linear", px = 21, py = 59 },
    d = "TEXT_ITEM_MAGNUM",
    cs = {25600,  0,  0,   0,   0,   0},
    ch = {    0, 56, 72, 120, 184, 208},
    cu = 1040,
    lvl = 16,
    id = ShopItem.Magnum,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {1, 2, 2, 3, 3, 3},
    rate = {1, 1, 2, 2, 3, 3},
    clip = {2, 2, 2, 2, 2, 3},
    upgrades = UpgradeMagnum,
    atlas = AtlasSet.Player03
}

ItemLineGun = {
    f = "gfx/weapons/weap_line_gun.png@linear",
    a = { file = "gfx/weapons/weap_line_gun_dum.png@linear", px = 35, py = 60 },
    d = "TEXT_ITEM_LINEGUN",
    cs = {37600,  0,   0,   0,   0,   0},
    ch = {    0, 56, 96, 136, 192, 232},
    cu = 1360,
    lvl = 18,
    id = ShopItem.LineGun,
    cat = ItemCategory.weapons,
    max = 1,
    funcs = WeaponMission,
    dmg  = {2, 2, 3, 3, 3, 4},
    rate = {2, 2, 2, 2, 3, 3},
    clip = {1, 2, 2, 3, 3, 3},
    upgrades = UpgradeLineGun,
    atlas = AtlasSet.Player02
}

ItemRailgun = {
    f = "gfx/weapons/weap_railgun.png@linear",
    a = { file = "gfx/weapons/weap_railgun_dum.png@linear", px = 25, py = 79 },
    d = "TEXT_ITEM_RAILGUN",
    cs = {56800,  0,   0,   0,   0,   0},
    ch = {    0, 80, 104, 144, 208, 264},
    cu = 2000,
    lvl = 20,
    id = ShopItem.Railgun,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {3, 3, 4, 4, 4, 4},
    rate = {1, 2, 2, 3, 3, 4},
    clip = {2, 2, 2, 2, 3, 3},
    upgrades = UpgradeRailgun,
    atlas = AtlasSet.Player08
}

ItemCombatShotgun = {
    f = "gfx/weapons/weap_guerilla_shotgun.png@linear",
    a = { file = "gfx/weapons/weap_guerilla_shotgun_dum.png@linear", px = 19, py = 61 },
    d = "TEXT_ITEM_COMBAT_SHOTGUN",
    cs = {65600,   0,   0,    0,  0,   0},
    ch = {    0, 80, 104, 144, 208, 264},
    cu = 2400,
    lvl = 22,
    id = ShopItem.CombatShotgun,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {3, 3, 3, 4, 4, 5},
    rate = {2, 2, 3, 3, 3, 3},
    clip = {2, 3, 3, 3, 4, 4},
    upgrades = UpgradeCombatShotgun,
    atlas = AtlasSet.Player05
}

ItemChaingun = {
    f = "gfx/weapons/weap_chaingun.png@linear",
    a = { file = "gfx/weapons/weap_chaingun_dum.png@linear", px = 19, py = 70 },
    d = "TEXT_ITEM_CHAINGUN",
    cs = {76000,   0,   0,   0,   0,   0},
    ch = {    0, 96, 112, 160, 224, 280},
    cu = 2800,
    lvl = 24,
    id = ShopItem.Chaingun,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {3, 3, 3, 4, 4, 4},
    rate = {3, 3, 4, 4, 4, 5},
    clip = {2, 3, 3, 3, 4, 4},
    upgrades = UpgradeChaingun,
    atlas = AtlasSet.Player01
}

ItemVortex = {
    f = "gfx/weapons/weap_vortex_gun.png@linear",
    a = { file = "gfx/weapons/weap_vortex_gun_dum.png@linear", px = 29, py = 68 },
    d = "TEXT_ITEM_VORTEX",
    cs = {88000,   0,   0,   0,   0,   0},
    ch = {     0, 96, 112, 160, 240, 296},
    cu = 3200,
    lvl = 26,
    id = ShopItem.Vortex,
    cat = ItemCategory.weapons,
    max = 1,
    funcs = WeaponMission,
    dmg  = {3, 3, 4, 4, 5, 5},
    rate = {3, 4, 3, 4, 4, 4},
    clip = {4, 4, 4, 4, 4, 5},
    upgrades = UpgradeVortex,
    atlas = AtlasSet.Player18
}

ItemChainsaw = {
    f = "gfx/weapons/weap_chainsaw.png@linear",
    a = { file = "gfx/weapons/weap_chainsaw_dum.png@linear", px = 25, py = 76 },
    d = "TEXT_ITEM_CHAINSAW",
	cs = {100000,   0,   0,   0,   0,   0},
    ch = {     0, 104, 120, 176, 256, 320},
    cu = 4000,
    lvl = 28,
    id = ShopItem.Chainsaw,
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {3, 3, 4, 4, 5, 5},
    rate = {4, 4, 4, 5, 5, 5},
    clip = {3, 4, 4, 4, 4, 5},
    upgrades = UpgradeChainsaw,
    atlas = AtlasSet.Player12
}

ItemMech = {
    f = "gfx/weapons/weap_mech.png@linear",
    a = { file = "gfx/weapons/weap_mech_dum.png@linear", px = 75, py = 90, hideDumDum = true },
    d = "TEXT_ITEM_MECH",
    cs = {120000,   0,   0,   0,   0,   0},
    ch = {     0, 120, 160, 240, 320, 400},
    cu = 8000,
    lvl = 30,
    exclusive = true,
    id = ShopItem.Mech,
    id2 = {ShopItem.MechRocket},
    cat = ItemCategory.weapons,
    max = 1,
    dmg  = {4, 4, 4, 5, 5, 5},
    rate = {3, 3, 4, 4, 5, 5},
    clip = {3, 4, 4, 4, 4, 5},
    upgrades = UpgradeMechRocket,
    atlas = AtlasSet.PlayerMech
}

ItemNuke = {
    f = "menu2/item_nuke.png@linear",
    d = "TEXT_ITEM_NUKE",
    ch = 16,
    id = ShopItem.Nuke,
    cat = ItemCategory.items,
    max = 1,
    maxPerGame = 1,
    ord = 5
}

ItemAirstrike = {
    f = "menu2/item_airkstrike.png@linear",
    d = "TEXT_ITEM_AIRSTRIKE",
    ch = 8,
    id = ShopItem.Airstrike,
    cat = ItemCategory.items,
    max = 3,
    maxPerGame = 3,
    ord = 4
}

ItemGrenade = {
    f = "menu2/item_grenades.png@linear",
    d = "TEXT_ITEM_GRENADE",
    cs = 80,
    id = ShopItem.Grenade,
    cat = ItemCategory.items,
    max = 10,
    maxPerGame = 5,
    ord = 1
}

ItemMine = {
    f = "menu2/item_mines.png@linear",
    d = "TEXT_ITEM_MINE",
    cs = 80,
    id = ShopItem.Mine,
    cat = ItemCategory.items,
    max = 10,
    maxPerGame = 5,
    ord = 0
}

ItemShield = {
    f = "menu2/item_shield.png@linear",
    d = "TEXT_ITEM_SHIELD",
    cs = 400,
    id = ShopItem.Shield,
    n = "shield",
    u = { 300, 1000, 1500 },
    cat = ItemCategory.items,
    max = 3,
    maxPerGame = 3,
    ord = 3
}

ItemHealthKit = {
    f = "menu2/item_stim_pack.png@linear",
    d = "TEXT_ITEM_HEALTH_KIT",
    cs = 120,
    id = ShopItem.HealthKit,
    cat = ItemCategory.items,
    max = 7,
    ord = 2
}

ItemSpeedBonus = {
    f = "menu2/item_bonus_speed.png@linear",
    d = "TEXT_ITEM_BONUS_SPEED",
    desc = "TEXT_PERK_SPEED",
    cs = 8000,
    id = ShopItem.SpeedBonus,
    cat = ItemCategory.perks,
    max = 1
}

ItemDamageBonus = {
    f = "menu2/item_bonus_damage.png@linear",
    d = "TEXT_ITEM_BONUS_DMG",
    desc = "TEXT_PERK_DMG",
    cs = 8000,
    id = ShopItem.DamageBonus,
    cat = ItemCategory.perks,
    max = 1
}

ItemHealthBonus = {
    f = "menu2/item_bonus_health.png@linear",
    d = "TEXT_ITEM_BONUS_HEALTH",
    desc = "TEXT_PERK_HEALTH",
    cs = 8000,
    id = ShopItem.HealthBonus,
    cat = ItemCategory.perks,
    max = 1
}

ItemCashBonus = {
    f = "menu2/item_extra_cash.png@linear",
    d = "TEXT_ITEM_BONUS_CASH",
    desc = "TEXT_PERK_CASH",
    ch = 100,
    id = ShopItem.CashBonus,
    cat = ItemCategory.perks,
    max = 1
}

ItemReloadBonus = {
    f = "menu2/item_fast_reload.png@linear",
    d = "TEXT_ITEM_BONUS_RELOAD",
    desc = "TEXT_PERK_RELOAD",
    ch = 100,
    id = ShopItem.ReloadBonus,
    cat = ItemCategory.perks,
    max = 1
}

ItemAmmoBonus = {
    f = "menu2/item_more_ammo.png@linear",
    d = "TEXT_ITEM_BONUS_AMMO",
    desc = "TEXT_PERK_AMMO",
    ch = 50,
    id = ShopItem.AmmoBonus,
    cat = ItemCategory.perks,
    max = 1
}

ItemMedKitBonus = {
    f = "menu2/item_super_healing.png@linear",
    d = "TEXT_ITEM_BONUS_MEDKIT",
    desc = "TEXT_PERK_MEDKIT",
    cs = 15000,
    id = ShopItem.MedKitBonus,
    cat = ItemCategory.perks,
    max = 1
}

--[[
-- TODO: When enabled must be added to 'Spinach' (MainMissions/stage-4/buy-perk) mission objective
ItemPowerupBonus = {
    f = "menu2/item_more_pickups.png@linear",
    d = "TEXT_ITEM_BONUS_POWERUP",
    desc = "TEXT_PERK_POWERUP",
    cs = 10000,
    id = ShopItem.PowerupBonus,
    cat = ItemCategory.perks,
    max = 1
}
]]

ItemCashFree = {
    f = "menu2/iaps_gold_free.png@linear",
    id = ShopItem.IapCashFree,
    cat = ItemCategory.cash,
    max = 1,
    ord = 1,
    tj_conv_rate = 0.6666667
}

ItemSubscription = {
    f = "menu2/iaps_subscription.png@linear",
    d = "TEXT_ITEM_SUBSCRIPTION",
    desc = "TEXT_DESC_SUBSCRIPTION",
    usd_price = 1999,
    id = ShopItem.IapSubscription,
    cat = ItemCategory.cash,
    max = 1,
    ord = 1,
    subscription = true
}

ItemCashMonsterPack = {
    f = "menu2/iaps_mix_mega_pack.png@linear",
    d = "TEXT_ITEM_CASH_MONSTER_PACK",
    cs = 30000,
    ch = 1200,
    usd_price = 1999,
    id = ShopItem.IapCashMonsterPack,
    cat = ItemCategory.cash,
    max = 1,
    ord = 2
}

ItemCashStarterPack = {
    f = "menu2/iaps_mix_small_pack.png@linear",
    d = "TEXT_ITEM_CASH_STARTER_PACK",
    cs = 12500,
    ch = 350,
    usd_price = 699,
    id = ShopItem.IapCashStarterPack,
    cat = ItemCategory.cash,
    max = 1,
    ord = 3
}

ItemCashPennyGold= {
    f = "menu2/iaps_gold_bar_1.png@linear",
    d = "TEXT_ITEM_CASH_PENNY_GOLD",
    ch = 50,
    usd_price = 699,
    id = ShopItem.IapCashPennyGold,
    cat = ItemCategory.cash,
    max = 1,
    ord = 4
}

ItemCashLittlegold = {
    f = "menu2/iaps_gold_bar_5.png@linear",
    d = "TEXT_ITEM_CASH_LITTLE_GOLD",
    ch = 105,
    usd_price = 699,
    id = ShopItem.IapCashLittlegold,
    cat = ItemCategory.cash,
    max = 1,
    ord = 5
}

	
ItemCashMiddlegold = {
    f = "menu2/iaps_gold_bar_9.png@linear",
    d = "TEXT_ITEM_CASH_MIDDLE_GOLD",
    ch = 220,
    usd_price = 699,
    id = ShopItem.IapCashMiddlegold,
    cat = ItemCategory.cash,
    max = 1,
    ord = 6
}
ItemCashGoodgold = {
    f = "menu2/iaps_gold_bar_11.png@linear",
    d = "TEXT_ITEM_CASH_GOOD_GOLD",
    ch = 300,
    usd_price = 699,
    id = ShopItem.IapCashGoodgold,
    cat = ItemCategory.cash,
    max = 1,
    ord = 7
}
ItemCashPennycash = {
    f = "menu2/iaps_cash_1.png@linear",
    d = "TEXT_ITEM_CASH_PENNY_CASH",
    cs = 2500,
    usd_price = 699,
    id = ShopItem.IapCashPennycash,
    cat = ItemCategory.cash,
    max = 1,
    ord = 8
}
ItemCashLittlecash = {
    f = "menu2/iaps_cash_3 .png@linear",
    d = "TEXT_ITEM_CASH_LITTLE_CASH",
    cs = 5500,
    usd_price = 699,
    id = ShopItem.IapCashLittlecash,
    cat = ItemCategory.cash,
    max = 1,
    ord = 9
}
ItemCashMiddlecash = {
    f = "menu2/iaps_cash_6.png@linear",
    d = "TEXT_ITEM_CASH_MIDDLE_CASH",
    cs = 11500,
    usd_price = 699,
    id = ShopItem.IapCashMiddlecash,
    cat = ItemCategory.cash,
    max = 1,
    ord = 10
}
ItemCashGoodcash = {
    f = "menu2/iaps_cash_9 .png@linear",
    d = "TEXT_ITEM_CASH_GOOD_CASH",
    cs = 17000,
    usd_price = 699,
    id = ShopItem.IapCashGoodcash,
    cat = ItemCategory.cash,
    max = 1,
    ord = 11
}


ItemCashStackOfGold = {
    f = "menu2/iaps_gold_mega_pack.png@linear",
    d = "TEXT_ITEM_CASH_STACK_OF_GOLD",
    ch = 15000,
    usd_price = 699,
    id = ShopItem.IapCashStackOfGold,
    cat = ItemCategory.cash,
    max = 1,
    ord = 12
}

ItemCashLargeCargo = {
    f = "menu2/iaps_gold_big_pack.png@linear",
    d = "TEXT_ITEM_CASH_LARGE_CARGO",
    ch = 6000,
    usd_price = 4999,
    id = ShopItem.IapCashLargeCargo,
    cat = ItemCategory.cash,
    max = 1,
    ord = 13
}

ItemCashCaseOfGold = {
    f = "menu2/iaps_gold_medium_pack.png@linear",
    d = "TEXT_ITEM_CASH_CASE_OF_GOLD",
    ch = 2750,
    usd_price = 2499,
    id = ShopItem.IapCashCaseOfGold,
    cat = ItemCategory.cash,
    max = 1,
    ord = 14
}

ItemCashGoldenBar = {
    f = "menu2/iaps_gold_small_pack.png@linear",
    d = "TEXT_ITEM_CASH_GOLDEN_BAR",
    ch = 300,
    usd_price = 299,
    id = ShopItem.IapCashGoldenBar,
    cat = ItemCategory.cash,
    max = 1,
    ord = 15
}

ItemCashPackedSafe = {
    f = "menu2/iaps_cash_mega_pack.png@linear",
    d = "TEXT_ITEM_CASH_PACKED_SAFE",
    cs = 400000,
    usd_price = 4999,
    id = ShopItem.IapCashPackedSafe,
    cat = ItemCategory.cash,
    max = 1,
    ord = 16
}

ItemCashPrivateATM = {
    f = "menu2/iaps_cash_big_pack.png@linear",
    d = "TEXT_ITEM_CASH_PRIVATE_ATM",
    cs = 175000,
    usd_price = 2499,
    id = ShopItem.IapCashPrivateAtm,
    cat = ItemCategory.cash,
    max = 1,
    ord = 17
}

ItemCashJarOfCash = {
    f = "menu2/iaps_cash_medium_pack.png@linear",
    d = "TEXT_ITEM_CASH_JAR_OF_CASH",
    cs = 60000,
    usd_price = 999,
    id = ShopItem.IapCashJarOfCash,
    cat = ItemCategory.cash,
    max = 1,
    ord = 18
}

ItemCashFatWallet = {
    f = "menu2/iaps_cash_small_pack.png@linear",
    d = "TEXT_ITEM_CASH_FAT_WALLET",
    cs = 10000,
    usd_price = 199,
    id = ShopItem.IapCashFatWallet,
    cat = ItemCategory.cash,
    max = 1,
    ord = 19
}

-- Override hud icons lookup for any weapon
local HudIcons = {}
HudIcons[ShopItem.Mech] = { weapon = "gfx/weapons/weap_flamethrower.png@linear" }
HudIcons[ShopItem.MechRocket] = { weapon = "gfx/weapons/weap_rpg.png@linear" }

function ItemDbRegisterItems()
    AllItems = {}
    table.insert( AllItems, ItemSmg )
    table.insert( AllItems, ItemShotgun )
    table.insert( AllItems, ItemRipper )
    table.insert( AllItems, ItemPistol )
    table.insert( AllItems, ItemRailgun )
    table.insert( AllItems, ItemElectricity )
    table.insert( AllItems, ItemFlamer )
    table.insert( AllItems, ItemRocketLauncher )
    table.insert( AllItems, ItemCombatShotgun )
    table.insert( AllItems, ItemChaingun )
    table.insert( AllItems, ItemSpiral )
    table.insert( AllItems, ItemVortex )
    table.insert( AllItems, ItemChainsaw )
    table.insert( AllItems, ItemLineGun )
    table.insert( AllItems, ItemMagnum )
    table.insert( AllItems, ItemNailer )
    table.insert( AllItems, ItemMech )
    table.insert( AllItems, ItemNuke )
    table.insert( AllItems, ItemGrenade )
    table.insert( AllItems, ItemMine )
    table.insert( AllItems, ItemShield )
    table.insert( AllItems, ItemAirstrike )
    table.insert( AllItems, ItemSpeedBonus )
    table.insert( AllItems, ItemDamageBonus )
    table.insert( AllItems, ItemHealthBonus )
    table.insert( AllItems, ItemCashBonus )
    table.insert( AllItems, ItemReloadBonus )
    table.insert( AllItems, ItemAmmoBonus )
    table.insert( AllItems, ItemMedKitBonus )
    table.insert( AllItems, ItemPowerupBonus )
    table.insert( AllItems, ItemHealthKit )
end

function ItemDbRegisterCash()
    --table.insert( AllItems, ItemCashFree )
    if IAPS_ENABLED then
        if SUBSCRIPTION_ENABLED then
            table.insert( AllItems, ItemSubscription )
        end
        table.insert( AllItems, ItemCashMonsterPack )
        table.insert( AllItems, ItemCashStarterPack )
		table.insert( AllItems, ItemCashPennyGold )
		table.insert( AllItems, ItemCashLittlegold )
		table.insert( AllItems, ItemCashMiddlegold )
		table.insert( AllItems, ItemCashGoodgold )
		table.insert( AllItems, ItemCashPennycash )
		table.insert( AllItems, ItemCashLittlecash )
		table.insert( AllItems, ItemCashMiddlecash )
		table.insert( AllItems, ItemCashGoodcash )
		table.insert( AllItems, ItemCashStackOfGold )
        table.insert( AllItems, ItemCashLargeCargo )
        table.insert( AllItems, ItemCashCaseOfGold )
        table.insert( AllItems, ItemCashGoldenBar )
        table.insert( AllItems, ItemCashPackedSafe )
        table.insert( AllItems, ItemCashPrivateATM )
        table.insert( AllItems, ItemCashJarOfCash )
		table.insert( AllItems, ItemCashFatWallet )
		
		
    end
end

function ItemDbRegisterQuickShopItems()
    QuickShopItems = {}
    table.insert( QuickShopItems, ItemHealthKit )
    table.insert( QuickShopItems, ItemGrenade )
    table.insert( QuickShopItems, ItemMine )
    table.insert( QuickShopItems, ItemNuke )
    table.insert( QuickShopItems, ItemShield )
    table.insert( QuickShopItems, ItemAirstrike )
end

function ItemDbResolvePrice( price, upgrade )
    if not price then return 0 end
    if type(price) == "table" then
        local idx = upgrade or 1 
        return #price > (idx-1) and price[idx] or 0
    end
    return price
end

function ItemDbSortItems()
    function CheapestFirst( item0, item1 )
        local cs0 = ItemDbResolvePrice(item0.cs)
        local cs1 = ItemDbResolvePrice(item1.cs)
        if cs0 == cs1 then
            local ch0 = ItemDbResolvePrice(item0.ch)
            local ch1 = ItemDbResolvePrice(item1.ch)
            return ch0 < ch1
        end
        return cs0 < cs1
    end

    table.sort( AllItems, CheapestFirst )

    ItemsCategory = {}
    for i=1,ItemCategory.catNum do
        ItemsCategory[i] = {}
    end

    for i,v in ipairs( AllItems ) do
        table.insert( ItemsCategory[v.cat], v )
    end
    
    function ByOrd( item1, item2 )
        return item1.ord < item2.ord
    end
    table.sort( ItemsCategory[ItemCategory.cash], ByOrd )
    table.sort( ItemsCategory[ItemCategory.items], ByOrd )
    table.sort( QuickShopItems, ByOrd )

    ItemsById = {}
    for _,item in ipairs( AllItems ) do
        ItemsById[tostring(item.id)] = item
        if item.id2 ~= nil then
            for _, id2 in ipairs(item.id2) do
                ItemsById[tostring(id2)] = item
            end
        end
    end

    Items = ItemsCategory[1]
end

function ItemDbGetItemPrice( itemId )
    local cs = ItemDbResolvePrice(ItemsById[itemId].cs)
    local ch = ItemDbResolvePrice(ItemsById[itemId].ch)
    return cs, ch
end

function ItemDbGetItemUnlockPrice( itemId )
    local cu = ItemsById[itemId].cu or 0
    return cu
end

function ItemDbGetCashItemBonus( itemId )
    local bonus = ItemsById[itemId].bonus or 0
    return bonus
end

function ItemDbGetItemUpgradePrice( itemId )
    if ItemDbCanUpgrade( itemId ) then
        local item = ItemsById[itemId]
        local nextUpgradeIdx = Shop:GetUpgrades( itemId ) + 2
        local cs = ItemDbResolvePrice(item.cs, nextUpgradeIdx)
        local ch = ItemDbResolvePrice(item.ch, nextUpgradeIdx)
        return cs, ch
    end
    return nil, nil
end

function ItemDbCanUpgrade( itemId )
    local item = ItemsById[itemId]
    if item == nil then return false end
    
    local csTable = type(item.cs) == "table"
    local chTable = type(item.ch) == "table"
    
    if csTable or chTable then
        local currUpgrade = Shop:GetUpgrades( itemId ) + 1
        local maxUpgrade = math.max( csTable and #item.cs or 1, chTable and #item.ch or 1 )
        return currUpgrade < maxUpgrade
    end
    return false
end

function ItemDbGetItemIcon( itemId )
    local iconPath = HudIcons[itemId]
    if iconPath ~= nil and iconPath.weapon ~= nil then
        return iconPath.weapon
    else
        local item = ItemsById[itemId]
        if item ~= nil and item.f ~= nil then
            return item.f
        else 
            return ""
        end
    end
end

function ItemDbGetItemLevel( itemId )
    local item = ItemsById[itemId]
    
    if item ~= nil and item.lvl ~= nil then
        return item.lvl
    else 
        return 0
    end
end

function ItemDbGetItemById( itemId )
    return ItemsById[itemId]
end

function ItemDbGetBestOwnedWeaponId( includeMech )
    includeMech = includeMech or false

    local items = ItemsCategory[1]
    local bestWeaponLevel = 0
    local bestWeaponId = "smg"
    local bestWeaponIdUpgradeLevel = "0"

    for i=1,# items do
        local item = items[i]
        if item.id ~= ShopItem.Mech or includeMech then
            if Shop:IsBought( item.id ) > 0 then
                local weaponLevel = ItemDbGetItemLevel ( item.id )
            
                if weaponLevel > bestWeaponLevel then
                    bestWeaponLevel = weaponLevel
                    bestWeaponId = item.id 
                    bestWeaponIdUpgradeLevel = Shop:GetUpgrades( item.id  )
                end
            end
        end
    end

    return bestWeaponId , bestWeaponIdUpgradeLevel
end

function ItemDbGetItemCategory( itemId )
    local item = ItemDbGetItemById( itemId )
    if not item then return nil end
    return item.cat
end

function ItemDbGetUsdPrice( itemId )
    local item = ItemsById[itemId]
    if not item then return nil end
    return item.usd_price or 0
end

function ItemDbGetTapjoyConversionRate()
    return ItemCashFree.tj_conv_rate
end

function ItemDbCheckSubscriptions()
    if SUBSCRIPTION_ENABLED then
        Shop:CheckSubscription( ItemSubscription.id )
    end
end

function ItemDbCanUpgradeAnything()
    local canUpgrade = false
    for _, item in ipairs(AllItems) do
        if item.upgrades then
            if ItemDbCanUpgrade( item.id ) then
                canUpgrade = true
                break
            end
        end
    end
    return canUpgrade
end

ItemDbRegisterItems()
ItemDbRegisterCash()
ItemDbRegisterQuickShopItems()
ItemDbSortItems()

local CurrentWave = 1
local Waves = {}
local Time = 0
local KilledMonsters = 0
MonsterCount = 0
PotatoCount = 0

local SpawnType = { Enemy = 1, Pickup = 2 }

local LevelMonsterSet = {}
local idx = 1
while true do
    local v = registry:Get( "/internal/monsters/" .. idx )
    if v == -1 then
        break
    else
        table.insert( LevelMonsterSet, v )
    end
    idx = idx + 1
end

idx = idx - 1

LevelSurvivalSet = {}
if registry:Get( "/internal/boss" ) then
    LevelSurvivalModifier = 1
else
    local rsum = 0
    local weight = {}
    for i=1,idx do
        table.insert( LevelSurvivalSet, { id = LevelMonsterSet[i] } )
        rsum = rsum + 1/EntityLeveling[LevelMonsterSet[i]].d
        table.insert( weight, rsum )
    end
    for i=1,idx do
        LevelSurvivalSet[i].weight = weight[i] / rsum
        ClawMsg( "Survival monster id: " .. LevelSurvivalSet[i].id .. ", diff: " .. EntityLeveling[LevelSurvivalSet[i].id].d .. ", weight: " .. i .. ": " .. LevelSurvivalSet[i].weight )
    end
    LevelSurvivalModifier = 1/rsum
    ClawMsg( "Survival modifier: " .. LevelSurvivalModifier )
end

local ObjectsToDestroy = {}

function AddWave( num, types, spawn, time, minnum, items )
    table.insert( Waves, { st = SpawnType.Enemy, n = num, t = types, s = spawn, tt = time * 60, min = minnum + 1, i = items } )
    MonsterCount = MonsterCount + num
end

function AddWave2( target, tier, spawn, time, minnum, items )
    local lvl = registry:Get( "/monstaz/player/level" )
    local data = EntityLeveling[LevelMonsterSet[tier]]
    local maxlvl = 50
    target = math.ceil( target / data.d * ( 1 + math.tanh( data.m * lvl / maxlvl - data.m / 2 ) ) / 2 )
    table.insert( Waves, { st = SpawnType.Enemy, n = target, t = { LevelMonsterSet[tier] }, s = spawn, tt = time * 60, min = minnum + 1, i = items } )
    MonsterCount = MonsterCount + target
end

function AddPickup( num, types, spawn, life, liferandom, time, minnum )
    table.insert( Waves, { st = SpawnType.Pickup, n = num, t = types, s = spawn, tt = time * 60, min = minnum + 1, l = life, lr = liferandom } )
end

function AddObjectToDestroy( name )
    table.insert( ObjectsToDestroy, name )
    Map:ObjectToDestroy( name )
end

function TickWaves()
    local count = EntityManager:Count()

    if CurrentWave > #Waves then
        return
    end

    Time = Time + 1
    local w = Waves[CurrentWave]
    if w.tt < Time and w.min >= count then
        if w.st == SpawnType.Enemy then
            SpawnDelayed( w.s, w.n, w.t, w.i )
        else
            SpawnPickup( w.s, w.n, w.l, w.lr, w.t )
        end
        CurrentWave = CurrentWave + 1
        Time = 0
    end
end

function CheckEndWaves()
    local count = EntityManager:Count()
    return CurrentWave > #Waves and count == 0
end

function CheckEndDestroy()
    return #ObjectsToDestroy == 0
end

function CheckEndPotato( count )
    return PotatoCount >= count
end

function GetMonsterCount()
    return MonsterCount
end

function MonsterKilled()
    KilledMonsters = KilledMonsters + 1
end

function PotatoPickup()
    PotatoCount = PotatoCount + 1
end

function FriendDied()
    if player then
        GameOver()
    end
end

function AIFriendDied()
    AIShot( false )
    AIFriend = nil
end

function SpawnEntityUser( x, y )
    SpawnEntity( EntityType.FloaterElectric, x, y )
end

function ObjectDestroyedInternal( name )
    if ObjectDestroyed then ObjectDestroyed( name ) end
    for i,v in ipairs( ObjectsToDestroy ) do
        if v == name then
            table.remove( ObjectsToDestroy, i )
            return
        end
    end
end

function StoryText( text )
    GameManager:StartStoryTutorial( text )
end

local icons = {
    "menu2/perk_skull.png@linear",
    "menu2/perk_muscular_arm.png@linear",
    "menu2/perk_money.png@linear",
    "menu2/perk_gun.png@linear",
}

local PerkRunner = {
    icon = icons[2],
    header = "TEXT_PERK_1",
    text = "TEXT_PERK_2",
    perk = Perk.Runner
}

local PerkAmmoManiac = {
    icon = icons[4],
    header = "TEXT_PERK_3",
    text = "TEXT_PERK_4",
    perk = Perk.AmmoManiac
}

local PerkFastHands = {
    icon = icons[4],
    header = "TEXT_PERK_5",
    text = "TEXT_PERK_6",
    perk = Perk.FastHands
}

local PerkComeGetSome = {
    icon = icons[4],
    header = "TEXT_PERK_7",
    text = "TEXT_PERK_8",
    perk = Perk.ComeGetSome
}

local PerkSlaughter = {
    icon = icons[2],
    header = "TEXT_PERK_9",
    text = "TEXT_PERK_10",
    perk = Perk.Slaughter,
}

local PerkRegeneration = {
    icon = icons[2],
    header = "TEXT_PERK_11",
    text = "TEXT_PERK_12",
    perk = Perk.Regeneration
}

local PerkUnstoppable = {
    icon = icons[2],
    header = "TEXT_PERK_13",
    text = "TEXT_PERK_14",
    perk = Perk.Unstoppable
}

local PerkRage = {
    icon = icons[1],
    header = "TEXT_PERK_15",
    text = "TEXT_PERK_16",
    perk = Perk.Rage
}

local PerkEndoskeleton = {
    icon = icons[2],
    header = "TEXT_PERK_17",
    text = "TEXT_PERK_18",
    perk = Perk.Endoskeleton
}

local PerkVengeance = {
    icon = icons[1],
    header = "TEXT_PERK_19",
    text = "TEXT_PERK_20",
    perk = Perk.ColdVengeance
}

local PerkFirstAid = {
    icon = icons[2],
    header = "TEXT_PERK_21",
    text = "TEXT_PERK_22",
    perk = Perk.FirstAid
}

local PerkToughSkin = {
    icon = icons[2],
    header = "TEXT_PERK_23",
    text = "TEXT_PERK_24",
    perk = Perk.ToughSkin
}

local PerkOrbExtender = {
    icon = icons[3],
    header = "TEXT_PERK_25",
    text = "TEXT_PERK_26",
    perk = Perk.OrbExtender
}

local PerkGreed = {
    icon = icons[3],
    header = "TEXT_PERK_27",
    text = "TEXT_PERK_28",
    perk = Perk.Greed
}

local PerkWrath = {
    icon = icons[2],
    header = "TEXT_PERK_29",
    text = "TEXT_PERK_30",
    perk = Perk.Wrath
}

local PerkTeamPlayer = {
    icon = icons[2],
    header = "TEXT_PERK_31",
    text = "TEXT_PERK_32",
    perk = Perk.TeamPlayer
}

local PerkMrClean = {
    icon = icons[2],
    header = "TEXT_PERK_33",
    text = "TEXT_PERK_34",
    perk = Perk.MrClean
}

local PerkBigBang = {
    icon = icons[4],
    header = "TEXT_PERK_35",
    text = "TEXT_PERK_36",
    perk = Perk.BigBang
}

local PerkPurfectCat = {
    icon = icons[2],
    header = "TEXT_PERK_37",
    text = "TEXT_PERK_38",
    perk = Perk.PurfectCat
}

local PerkRagenerator = {
    icon = icons[2],
    header = "TEXT_PERK_39",
    text = "TEXT_PERK_40",
    perk = Perk.Ragenerator
}

local PerkRussianRoulette = {
    icon = icons[1],
    header = "TEXT_PERK_41",
    text = "TEXT_PERK_42",
    perk = Perk.RussianRoulette
}

local PerkExtraExplosives = {
    icon = icons[1],
    header = "TEXT_PERK_43",
    text = "TEXT_PERK_44",
    perk = Perk.ExtraExplosives
}

local PerkWithdraw = {
    icon = icons[3],
    header = "TEXT_PERK_45",
    text = "TEXT_PERK_46",
    perk = Perk.Withdraw
}

local PerkMuscleFever = {
    icon = icons[1],
    header = "TEXT_PERK_47",
    text = "TEXT_PERK_48",
    perk = Perk.MuscleFever
}


PerkPool = {}
table.insert( PerkPool, PerkRunner )
table.insert( PerkPool, PerkComeGetSome )
table.insert( PerkPool, PerkSlaughter )
table.insert( PerkPool, PerkRegeneration )
table.insert( PerkPool, PerkUnstoppable )
table.insert( PerkPool, PerkRage )
table.insert( PerkPool, PerkEndoskeleton )
table.insert( PerkPool, PerkVengeance )
table.insert( PerkPool, PerkFirstAid )
table.insert( PerkPool, PerkToughSkin )
table.insert( PerkPool, PerkOrbExtender )
table.insert( PerkPool, PerkWrath )
table.insert( PerkPool, PerkMrClean )
table.insert( PerkPool, PerkBigBang )
table.insert( PerkPool, PerkRagenerator )
table.insert( PerkPool, PerkExtraExplosives )
table.insert( PerkPool, PerkMuscleFever )

local t = {}
for i=1,2 do
    local v = registry:Get( "/monstaz/weaponselection/" .. i )
    if v == "" then
        break
    else
        table.insert( t, v )
    end
end
local infammo = true
for _,v in ipairs( t ) do
    if ItemDbCanUpgrade( v ) then
        infammo = false
        break
    end
end
if not infammo then
    table.insert( PerkPool, PerkAmmoManiac )
    table.insert( PerkPool, PerkFastHands )
end

if not TutorialActive then
    table.insert( PerkPool, PerkGreed )
    table.insert( PerkPool, PerkRussianRoulette )
    table.insert( PerkPool, PerkWithdraw )
end

if registry:Get( "/internal/friend" ) == true then
    table.insert( PerkPool, PerkTeamPlayer )
end

function SetupPerksCatLevel()
    table.insert( PerkPool, PerkPurfectCat )
end

local PerksByType = {}
for _, p in ipairs( PerkPool ) do
    PerksByType[p.perk] = p
end

function PerkGetHeader(perkType)
    local perk = PerksByType[perkType]
    if not perk then return nil end
    return perk.header
end

function ShopItemGenerateIapId( iapName )
    return "com.gamelion.ms2." .. iapName
end

ShopItem = {
    -- Weapons
    SMG = "smg",
    Ripper = "ripper",
    Pistol = "plasma",
    Railgun = "railgun",
    Electricity = "electricity",
    Flamer = "flamer",
    Shotgun = "shotgun",
    RocketLauncher = "rl",
    CombatShotgun = "combat",
    Chaingun = "chaingun",
    Spiral = "spiral",
    Vortex = "vortex",
    Chainsaw = "chainsaw",
    LineGun = "linegun",
    Magnum = "magnum",
    Nailer = "nailer",
    Mech = "mechflamer",
    MechRocket = "mechrocket", -- Fake id, not used directly inside the shop

    -- Items
    Nuke = "nuke",
    Grenade = "grenade",
    Mine = "mine",
    Shield = "shield",
    HealthKit = "healthkit",
    Airstrike = "airstrike",

    -- Perks
    SpeedBonus = "speedbonus",
    DamageBonus = "damagebonus",
    HealthBonus = "healthbonus",
    CashBonus = "cashbonus",
    ReloadBonus = "reloadbonus",
    AmmoBonus = "ammobonus",
    MedKitBonus = "medkitbonus",
    PowerupBonus = "powerupbonus",

    -- IAPS
    IapCashFree = "free",
    IapCashFatWallet = ShopItemGenerateIapId("fatwallet"),
    IapCashJarOfCash = ShopItemGenerateIapId("jarofcash"),
    IapCashPrivateAtm = ShopItemGenerateIapId("privateatm"),
    IapCashPackedSafe = ShopItemGenerateIapId("packedsafe"),
    IapCashGoldenBar = ShopItemGenerateIapId("goldenbar"),
    IapCashCaseOfGold = ShopItemGenerateIapId("caseofgold"),
    IapCashLargeCargo = ShopItemGenerateIapId("largecargo"),
    IapCashStackOfGold = ShopItemGenerateIapId("stackofgold"),
	IapCashGoodcash = ShopItemGenerateIapId("goodcash"),
	IapCashMiddlecash = ShopItemGenerateIapId("middlecash"),
	IapCashLittlecash = ShopItemGenerateIapId("littlecash"),
	IapCashPennycash = ShopItemGenerateIapId("pennycash"),
	IapCashGoodgold = ShopItemGenerateIapId("goodgold"),
	IapCashMiddlegold = ShopItemGenerateIapId("middlegold"),
	IapCashLittlegold = ShopItemGenerateIapId("littlegold"),
	IapCashPennyGold = ShopItemGenerateIapId("pennygold"),
    IapCashStarterPack = ShopItemGenerateIapId("starterpack"),
    IapCashMonsterPack = ShopItemGenerateIapId("monsterpack"),
    IapSubscription = ShopItemGenerateIapId("sub.release"),
	
	
	
	
	
	
	
}

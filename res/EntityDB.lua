require( "ShopItem.lua" )
require( "LevelScaling.lua" )

local EntitySectoid = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.825,
    Orbs = 1,
    XP = 10,
    Damage = 0.1,
    HP = 1.2,
    MoneyChance = 0.1,
    MoneyAmount = 3,
    Score = 1,
    ShieldScale = 0.3046
}

local EntitySectoidShooting = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.825,
    Orbs = 1,
    XP = 15,
    Damage = 0.1,
    HP = 1.5,
    MoneyChance = 0.15,
    MoneyAmount = 4,
    Score = 1,
    ShieldScale = 0.3046
}

local EntityNautilSimple = {
    RotationSpeed = 0.0,
    MoveSpeed = 2.9,
    Orbs = 1,
    XP = 20,
    Damage = 0.05,
    HP = 2,
    MoneyChance = 0.2,
    MoneyAmount = 5,
    Score = 1,
    ShieldScale = 0.4375
}

local EntityCrab = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.825,
    Orbs = 1,
    XP = 17,
    Damage = 0.1,
    HP = 1.7,
    MoneyChance = 0.15,
    MoneyAmount = 5,
    Score = 1,
    ShieldScale = 0.3046
}

local EntityNautilTurning = {
    RotationSpeed = 0.05,
    MoveSpeed = 2,
    Orbs = 2,
    XP = 22,
    Damage = 0.05,
    HP = 2,
    MoneyChance = 0.2,
    MoneyAmount = 6,
    Score = 1,
    ShieldScale = 0.4375
}

local EntityAvoider = {
    RotationSpeed = 0.1,
    MoveSpeed = 1.15,
    Orbs = 2,
    XP = 12,
    Damage = 0.075,
    HP = 2.3,
    MoneyChance = 0.1,
    MoneyAmount = 6,
    Score = 2,
    ShieldScale = 0.4375
}

local EntitySqueezerSimple = {
    RotationSpeed = 0.0,
    MoveSpeed = 2.9,
    Orbs = 2,
    XP = 23,
    Damage = 0.1,
    HP = 3,
    MoneyChance = 0.2,
    MoneyAmount = 7,
    Score = 2,
    ShieldScale = 0.4375
}

local EntityKillerWhaleSimple = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.825,
    Orbs = 2,
    XP = 24,
    Damage = 0.06,
    HP = 6,
    MoneyChance = 0.2,
    MoneyAmount = 7,
    Score = 2,
    ShieldScale = 0.4375
}

local EntityChaser = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.825,
    Orbs = 2,
    XP = 16,
    Damage = 0.15,
    HP = 4,
    MoneyChance = 0.1,
    MoneyAmount = 8,
    Score = 2,
    ShieldScale = 0.5898
}

local EntityNerval = {
    RotationSpeed = 0.01,
    MoveSpeed = 1.0,
    Orbs = 3,
    XP = 16,
    Damage = 0.15,
    HP = 5,
    MoneyChance = 0.1,
    MoneyAmount = 8,
    Score = 3,
    ShieldScale = 0.4375
}

local EntityKillerWhale = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.825,
    Orbs = 3,
    XP = 25,
    Damage = 0.06,
    HP = 6,
    MoneyChance = 0.2,
    MoneyAmount = 9,
    Score = 3,
    ShieldScale = 0.4375
}

local EntitySqueezerTurning = {
    RotationSpeed = 0.05,
    MoveSpeed = 1.75,
    Orbs = 3,
    XP = 18,
    Damage = 0.1,
    HP = 3,
    MoneyChance = 0.15,
    MoneyAmount = 9,
    Score = 3,
    ShieldScale = 0.4375
}

local EntityFishSimple = {
    RotationSpeed = 0.1,
    MoveSpeed = 0.725,
    Orbs = 3,
    XP = 26,
    Damage = 0.15,
    HP = 7,
    MoneyChance = 0.2,
    MoneyAmount = 10,
    Score = 3,
    ShieldScale = 0.4375
}

local EntityHoundSimple = {
    RotationSpeed = 0.1,
    MoveSpeed = 1.6,
    Orbs = 4,
    XP = 17,
    Damage = 0.2,
    HP = 4,
    MoneyChance = 0.1,
    MoneyAmount = 11,
    Score = 3,
    ShieldScale = 0.4375
}

local EntityShotAvoider = {
    RotationSpeed = 0.15,
    MoveSpeed = 1.7,
    Orbs = 4,
    XP = 21,
    Damage = 0.08,
    HP = 4,
    MoneyChance = 0.15,
    MoneyAmount = 11,
    Score = 4,
    ShieldScale = 0.4375
}

local EntityFishThrowing = {
    RotationSpeed = 0.1,
    MoveSpeed = 1,
    Orbs = 4,
    XP = 25,
    Damage = 0.15,
    HP = 6,
    MoneyChance = 0.2,
    MoneyAmount = 12,
    Score = 4,
    ShieldScale = 0.4375
}

local EntityFloaterSimple = {
    RotationSpeed = 0.02,
    MoveSpeed = 2.8,
    Orbs = 4,
    XP = 17,
    Damage = 0.05,
    HP = 4,
    MoneyChance = 0.1,
    MoneyAmount = 13,
    Score = 4,
    ShieldScale = 0.4375
}

local EntityFloaterElectric = {
    RotationSpeed = 0.01,
    MoveSpeed = 2.5,
    Orbs = 5,
    XP = 25,
    Damage = 0.01,
    HP = 3.5,
    MoneyChance = 0.3,
    MoneyAmount = 13,
    Score = 5,
    ShieldScale = 0.5898
}

local EntityHoundShooting = {
    RotationSpeed = 0.04,
    MoveSpeed = 2,
    Orbs = 5,
    XP = 30,
    Damage = 0.1,
    HP = 5,
    MoneyChance = 0.3,
    MoneyAmount = 15,
    Score = 5,
    ShieldScale = 0.75
}

local EntityLobster = {
    RotationSpeed = 0.04,
    MoveSpeed = 1.3,
    Orbs = 5,
    XP = 30,
    Damage = 0.1,
    HP = 5,
    MoneyChance = 0.3,
    MoneyAmount = 15,
    Score = 5,
    ShieldScale = 0.5898
}

EntityFriend = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.425,
    Orbs = 0,
    XP = 10,
    Damage = 0.25,
    HP = 100,
    MoneyChance = 0,
    MoneyAmount = 15,
    Score = 2,
    ShieldScale = 1
}

local EntityFloaterSower = {
    RotationSpeed = 0.02,
    MoveSpeed = 2.8,
    Orbs = 2,
    XP = 0,
    Damage = 0.05,
    HP = 1.5,
    MoneyChance = 0,
    MoneyAmount = 1,
    Score = 0,
    ShieldScale = 0.4375
}

local EntityMechaBoss = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.6,
    Orbs = 15,
    XP = 60,
    Damage = 0.3,
    HP = 100,
    OrbsDrop = 10,
    MoneyChance = 0.5,
    MoneyAmount = 50,
    Score = 15,
    ShieldScale = 1
}

local EntitySowerBoss = {
    RotationSpeed = 0.05,
    MoveSpeed = 0.6,
    Orbs = 15,
    XP = 60,
    Damage = 0.3,
    HP = 100,
    OrbsDrop = 30,
    MoneyChance = 0.075,
    MoneyAmount = 15,
    Score = 15,
    ShieldScale = 1
}

local EntityOctobrainBoss = {
    RotationSpeed = 0.05,
    MoveSpeed = 1,
    Orbs = 15,
    XP = 60,
    Damage = 0.15,
    HP = 100,
    OrbsDrop = 30,
    MoneyChance = 0.075,
    MoneyAmount = 15,
    Score = 15,
    ShieldScale = 1
}

local EntityOctobrainBossClone = {
    RotationSpeed = 0.05,
    MoveSpeed = 2,
    Orbs = 0,
    XP = 5,
    Damage = 0.1,
    HP = 2,
    MoneyChance = 0,
    MoneyAmount = 1,
    Score = 0,
    ShieldScale = 1
}

EntityPlayer = {
    RotationSpeed = 0.15,
    MoveSpeed = 1.875,
    Orbs = 0,
    XP = 0,
    Damage = 0,
    HP = 100,
    MoneyChance = 0,
    MoneyAmount = 15,
    Score = 0,
    ShieldScale = 0.4375
}

EntityAIFriend = {
    RotationSpeed = 0.15,
    MoveSpeed = 1.875,
    Orbs = 0,
    XP = 0,
    Damage = 0,
    HP = 100,
    MoneyChance = 0,
    MoneyAmount = 15,
    Score = 0,
    ShieldScale = 0.4375
}

EntityParams =  {}
EntityParams[EntityType.Player] = EntityPlayer
EntityParams[EntityType.OctopusSimple] = EntityAvoider
EntityParams[EntityType.OctopusShotAware] = EntityShotAvoider
EntityParams[EntityType.OctopusChaser] = EntityChaser
EntityParams[EntityType.SqueezerSimple] = EntitySqueezerSimple
EntityParams[EntityType.SqueezerTurning] = EntitySqueezerTurning
EntityParams[EntityType.FishSimple] = EntityFishSimple
EntityParams[EntityType.FishSimpleNonExploding] = EntityFishSimple
EntityParams[EntityType.FishThrowing] = EntityFishThrowing
EntityParams[EntityType.FloaterSimple] = EntityFloaterSimple
EntityParams[EntityType.FloaterElectric] = EntityFloaterElectric
EntityParams[EntityType.FloaterSower] = EntityFloaterSower
EntityParams[EntityType.HoundSimple] = EntityHoundSimple
EntityParams[EntityType.HoundShooting] = EntityHoundShooting
EntityParams[EntityType.SectoidSimple] = EntitySectoid
EntityParams[EntityType.SectoidShooting] = EntitySectoidShooting
EntityParams[EntityType.KillerWhale] = EntityKillerWhale
EntityParams[EntityType.KillerWhaleSimple] = EntityKillerWhaleSimple
EntityParams[EntityType.Crab] = EntityCrab
EntityParams[EntityType.MechaBoss] = EntityMechaBoss
EntityParams[EntityType.SowerBoss] = EntitySowerBoss
EntityParams[EntityType.OctobrainBoss] = EntityOctobrainBoss
EntityParams[EntityType.OctobrainBossClone] = EntityOctobrainBossClone
EntityParams[EntityType.OctopusFriend] = EntityFriend
EntityParams[EntityType.AIFriend] = EntityAIFriend
EntityParams[EntityType.Lobster] = EntityLobster
EntityParams[EntityType.NautilSimple] = EntityNautilSimple
EntityParams[EntityType.NautilTurning] = EntityNautilTurning
EntityParams[EntityType.Nerval] = EntityNerval

function EntityUpdateParams()
    if Shop:IsBought( ShopItem.HealthBonus ) ~= 0 then
        EntityPlayer.HP = EntityPlayer.HP * 1.1
    end
    if Shop:IsBought( ShopItem.SpeedBonus ) ~= 0 then
        EntityPlayer.MoveSpeed = EntityPlayer.MoveSpeed * 1.1
    end

    local autoaim = registry:Get( "/monstaz/settings/autoaim" )
    local level = registry:Get( "/monstaz/player/level" )

    for i,v in ipairs( EntityParams ) do
        if v ~= EntityPlayer and autoaim then
            local orig = v.HP
            v.HP = v.HP * 1.5
            ClawMsg( "Increasing HP from " .. orig .. " to " .. v.HP )
        end

        local e = 0
        local j = 1
        while true do
            local m = registry:Get( "/internal/monsters/" .. j )
            if m == -1 then break end
            if m == i then
                e = registry:Get( "/internal/elements/" .. j )
                break
            end
            j = j + 1
        end

        if e == nil then
            e = 0
        elseif e == 3 then
            e = 4   -- bit field
        end

        v.e = e

        ScaleLevels( v, i, level )
        EntityManager:SetEntityData( i, v.RotationSpeed, v.MoveSpeed, v.Orbs, v.XP, v.Damage, v.HP, v.MoneyChance, v.MoneyAmount, v.Score, v.e, v.ShieldScale, v.OrbsDrop )
    end
    EntityManager:SetupEntityBehaviors()

    GameManager:SetPlayerHP( EntityPlayer.HP )
end

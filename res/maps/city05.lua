-- to musi by�. Mars albo Moon
Map:SetType( MapType.City )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )


-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    -- SurvivalPickupSpawnTick()
    TickWaves()
    if CheckEndWaves() and EntityManager:Count() == 0 then
        GameWon()
    end
end

function TriggerSpawn()
    return false
end

AddWave2( 30, 1, "spawn3", 1, 0 )
AddWave2( 30, 2, "spawn4", 0, 100 )
AddWave2( 30, 1, "spawn5", 1, 100 )
AddWave2( 30, 1, "spawn1", 0, 100 )
AddWave2( 30, 1, "spawn2", 0, 0 )
AddWave2( 30, 1, "spawn10", 0, 100 )
AddWave2( 30, 1, "spawn11", 0, 100 )
AddWave2( 30, 1, "spawn6", 0, 100 )
AddWave2( 30, 3, "spawn7", 5, 100 )
AddWave2( 30, 2, "spawn9", 5, 100 )
AddWave2( 30, 1, "spawn1", 7, 100 )
AddWave2( 30, 1, "spawn2", 3, 100 )
AddWave2( 30, 1, "spawn4", 3, 100 )
AddWave2( 30, 2, "spawn5", 3, 100 )
AddWave2( 30, 2, "spawn8", 3, 100 )
AddWave2( 30, 1, "spawn9", 3, 0 )
AddWave2( 30, 1, "spawn3", 0.4, 100 )
AddWave2( 30, 2, "spawn9", 0.4, 100 )
AddWave2( 30, 1, "spawn3", 0.4, 100 )
AddWave2( 30, 1, "spawn4", 0.4, 100 )
AddWave2( 30, 1, "spawn5", 0.4, 100 )
AddWave2( 30, 1, "spawn1", 3, 0 )
AddWave2( 30, 1, "spawn2", 0, 100 )
AddWave2( 30, 1, "spawn10", 0, 100 )
AddWave2( 30, 2, "spawn10", 0, 100 )
AddWave2( 30, 1, "spawn1", 0, 100 )
AddWave2( 30, 1, "spawn11", 0, 100 )
AddWave2( 30, 1, "spawn9", 3, 0 )
AddWave2( 30, 1, "spawn10", 0.4, 100 )
AddWave2( 30, 1, "spawn11", 0.4, 100 )
AddWave2( 30, 2, "spawn7", 0.4, 100 )
AddWave2( 30, 2, "spawn6", 0.4, 100 )
AddWave2( 30, 1, "spawn4", 0.4, 100 )
AddWave2( 30, 1, "spawn5", 0, 0 )
AddWave2( 30, 1, "spawn10", 0.4, 100 )
AddWave2( 30, 2, "spawn11", 0.4, 100 )
AddWave2( 30, 3, "spawn6", 0.4, 100 )

SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.WeaponBoost } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )
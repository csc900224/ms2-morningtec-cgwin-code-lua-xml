-- to musi by�. Mars albo Moon
Map:SetType( MapType.Grass )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "spawn1", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )



-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    --SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    TickWaves()
    if CheckEndDestroy() and EntityManager:Count() == 0 then
        GameWon()
    end
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end

TargetMonsters = 2

AddObjectToDestroy( "beacon1" )
AddObjectToDestroy( "beacon2" )
AddObjectToDestroy( "beacon3" )

local t1f1 = false
local t1f2 = false
local t2f1 = false
local t2f2 = false
local t3f1 = false
local t3f2 = false

function ObjectDestroyed( name )
    ObjectDamaged( name, 0 )
end

function ObjectDamaged( name, life )
    if not t1f1 and life < 0.75 and name == "beacon1" then
        t1f1 = true
        AddWave2( 2, 1, "spawn1-1", 0, 100 )
        AddWave2( 2, 1, "spawn1-2", 0, 100 )
        AddWave2( 1, 1, "spawn1-1", 5, 100 )
        AddWave2( 1, 1, "spawn1-2", 0, 100 )
    end
    if not t1f2 and life < 0.1 and name == "beacon1" then
        t1f2 = true
        AddWave2( 1, 1, "spawn1-1", 0, 100 )
        AddWave2( 1, 2, "spawn1-2", 0, 100 )
    end
    if not t2f1 and life < 0.75 and name == "beacon2" then
        t2f1 = true
        AddWave2( 2, 1, "spawn2-1", 0, 100 )
        AddWave2( 2, 1, "spawn2-2", 0, 100 )
        AddWave2( 1, 1, "spawn2-1", 5, 100 )
        AddWave2( 1, 1, "spawn2-2", 0, 100 )
    end
    if not t2f2 and life < 0.1 and name == "beacon2" then
        t2f2 = true
        AddWave2( 1, 1, "spawn2-1", 0, 100 )
        AddWave2( 1, 2, "spawn2-2", 0, 100 )
    end
    if not t3f1 and life < 0.75 and name == "beacon3" then
        t3f1 = true
        AddWave2( 2, 1, "spawn3-1", 0, 100 )
        AddWave2( 2, 1, "spawn3-2", 0, 100 )
        AddWave2( 1, 1, "spawn3-1", 5, 100 )
        AddWave2( 1, 1, "spawn3-2", 0, 100 )
    end
    if not t3f2 and life < 0.1 and name == "beacon3" then
        t3f2 = true
        AddWave2( 1, 1, "spawn3-1", 0, 100 )
        AddWave2( 1, 2, "spawn3-2", 0, 100 )
    end
end

-- to musi by�. Mars albo Moon
Map:SetType( MapType.City )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )


-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    -- SurvivalPickupSpawnTick()
    TickWaves()
    -- CheckEndWaves()
end

local e = SpawnFriend( "kot", { EntityType.OctopusFriend } )
e:AddWaypoint( "wp00", 5 )
e:AddWaypoint( "wp01", 10 )
e:AddWaypoint( "wp02" )
e:AddWaypoint( "wp03" )
e:AddWaypoint( "wp04", 10 )
e:AddWaypoint( "wp05" )
e:AddWaypoint( "wp06" )
e:AddWaypoint( "wp07", 10 )
e:AddWaypoint( "wp08" )

function ender ( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        GameWon()
        return false
    end
    return true
end

function trigger01( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn1-1", 0, 100 )
        AddWave2( 30, 1, "spawn1-2", 0, 100 )
		AddWave2( 30, 2, "spawn1-1", 2, 100 )
        AddWave2( 30, 3, "spawn1-2", 0, 100 )
        return false
    end
    return true
end

function trigger02( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn2-1", 0, 100 )
        AddWave2( 30, 1, "spawn2-2", 0, 100 )
        AddWave2( 30, 3, "spawn2-1", 2, 100 )
        AddWave2( 30, 1, "spawn2-2", 0, 100 )
		AddWave2( 30, 2, "spawn1-1", 0, 100 )
        AddWave2( 30, 1, "spawn1-2", 3, 100 )
        AddWave2( 30, 2, "spawn1-2", 0, 100 )
		AddWave2( 30, 1, "spawn2-3", 5, 100 )
		AddWave2( 30, 1, "spawn2-3", 0, 100 )
        return false
    end
    return true
end

function trigger03( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 3, "spawn3-1", 0, 100 )
        AddWave2( 30, 3, "spawn3-2", 3, 100 )
        AddWave2( 30, 1, "spawn3-3", 0, 100 )
        AddWave2( 30, 1, "spawn3-1", 0, 100 )
        AddWave2( 30, 3, "spawn3-2", 2, 100 )
        AddWave2( 30, 2, "spawn3-1", 0, 100 )
        return false
    end
    return true
end

function trigger04( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 2, "spawn4-1", 0, 100 )
        AddWave2( 30, 2, "spawn4-1", 3, 100 )
        AddWave2( 30, 3, "spawn4-2", 1, 100 )
        AddWave2( 30, 1, "spawn4-3", 0, 100 )
        AddWave2( 30, 3, "spawn4-1", 3, 100 )
        AddWave2( 30, 3, "spawn4-2", 0, 100 )
        return false
    end
    return true
end

function trigger05( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn5-1", 0, 100 )
        AddWave2( 30, 2, "spawn5-2", 3, 100 )
        AddWave2( 30, 1, "spawn5-3", 0, 100 )
		AddWave2( 30, 1, "spawn5-1", 2, 100 )
        AddWave2( 30, 2, "spawn5-3", 0, 100 )
        return false
    end
    return true
end

function trigger06( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn6-1", 0, 100 )
        AddWave2( 30, 1, "spawn6-3", 2, 100 )
        AddWave2( 30, 1, "spawn6-4", 0, 100 )
        AddWave2( 30, 1, "spawn6-1", 2, 100 )
        AddWave2( 30, 3, "spawn6-3", 4, 100 )
        AddWave2( 30, 2, "spawn6-4", 0, 100 )
        return false
    end
    return true
end

function trigger07( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn7-1", 0, 100 )
        AddWave2( 30, 3, "spawn7-2", 0, 100 )
		AddWave2( 30, 1, "spawn7-1", 3, 100 )
        AddWave2( 30, 1, "spawn7-2", 0, 100 )
        AddWave2( 30, 1, "spawn7-1", 0, 100 )
        AddWave2( 30, 1, "spawn7-2", 0, 100 )
        AddWave2( 30, 3, "spawn7-1", 3, 100 )
        AddWave2( 30, 2, "spawn7-2", 0, 100 )
		AddWave2( 30, 3, "spawn7-1", 3, 100 )
        AddWave2( 30, 3, "spawn7-2", 0, 100 )
        AddWave2( 30, 2, "spawn7-1", 0, 100 )
        AddWave2( 30, 2, "spawn7-2", 0, 100 )
        return false
    end
    return true
end

SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup3", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup4", 1, 19999999, 0, { PickupType.WeaponBoost } )
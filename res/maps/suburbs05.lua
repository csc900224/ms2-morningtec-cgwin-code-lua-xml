-- to musi by�. Mars albo Moon
Map:SetType( MapType.Grass )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )


-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    -- SurvivalPickupSpawnTick()
    if CheckEndDestroy() and CheckEndWaves() and EntityManager:Count() == 0 then
        GameWon()
    end
    TickWaves()
end

function TriggerSpawn()
    return false
end

AddObjectToDestroy( "broadcaster1" )
AddObjectToDestroy( "broadcaster2" )
AddObjectToDestroy( "broadcaster3" )

local t1f1 = false
local t1f2 = false
local t2f1 = false
local t2f2 = false
local t3f1 = false
local t3f2 = false

function ObjectDamaged( name, life )
    if not t1f1 and life < 0.75 and name == "broadcaster1" then
        t1f1 = true
        AddWave2( 30, 1, "spawn1", 0, 100 )
        AddWave2( 30, 1, "spawn2", 0, 100 )
        AddWave2( 30, 1, "spawn3", 0, 100 )
        AddWave2( 30, 1, "spawn1", 5, 100 )
        AddWave2( 30, 1, "spawn2", 0, 100 )
        AddWave2( 30, 1, "spawn3", 0, 100 )
    end
    if not t1f2 and life < 0.1 and name == "broadcaster1" then
        t1f2 = true
        AddWave2( 30, 1, "spawn1", 0, 100 )
        AddWave2( 30, 2, "spawn2", 0, 100 )
        AddWave2( 30, 1, "spawn3", 0, 100 )
		AddWave2( 30, 1, "spawn1", 0, 100 )
        AddWave2( 30, 3, "spawn2", 0, 100 )
        AddWave2( 30, 3, "spawn3", 0, 100 )
    end
    if not t2f1 and life < 0.75 and name == "broadcaster2" then
        t2f1 = true
        AddWave2( 30, 1, "spawn4", 0, 100 )
        AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 2, "spawn6", 0, 100 )
        AddWave2( 30, 3, "spawn4", 5, 100 )
        AddWave2( 30, 2, "spawn5", 0, 100 )
        AddWave2( 30, 1, "spawn6", 0, 100 )
    end
    if not t2f2 and life < 0.1 and name == "broadcaster2" then
        t2f2 = true
        AddWave2( 30, 1, "spawn4", 0, 100 )
        AddWave2( 30, 2, "spawn5", 0, 100 )
        AddWave2( 30, 1, "spawn6", 0, 100 )
		AddWave2( 30, 3, "spawn4", 0, 100 )
        AddWave2( 30, 3, "spawn5", 0, 100 )
        AddWave2( 30, 1, "spawn6", 0, 100 )
    end
    if not t3f1 and life < 0.75 and name == "broadcaster3" then
        t3f1 = true
        AddWave2( 30, 2, "spawn7", 0, 100 )
        AddWave2( 30, 1, "spawn8", 0, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
        AddWave2( 30, 3, "spawn7", 5, 100 )
        AddWave2( 30, 3, "spawn8", 0, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
    end
    if not t3f2 and life < 0.1 and name == "broadcaster3" then
        t3f2 = true
        AddWave2( 30, 1, "spawn7", 0, 100 )
        AddWave2( 30, 2, "spawn8", 0, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
		AddWave2( 30, 3, "spawn7", 0, 100 )
        AddWave2( 30, 3, "spawn8", 0, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
    end
end



function trigger01( entity )
    if entity:GetType() == EntityType.Player then
        AddWave2( 30, 2, "spawn10", 0, 100 )
        AddWave2( 30, 3, "spawn11", 0, 100 )
		AddWave2( 30, 1, "spawn10", 1, 100 )
        AddWave2( 30, 2, "spawn11", 0, 100 )
		AddWave2( 30, 3, "spawn10", 0, 100 )
        AddWave2( 30, 1, "spawn11", 0, 100 )
		AddWave2( 30, 2, "spawn10", 1, 100 )
        AddWave2( 30, 2, "spawn11", 0, 100 )
        return false
    end
    return true
end


AddWave2( 30, 3, "spawn1", 2, 100 )
AddWave2( 30, 2, "spawn2", 0, 100 )
AddWave2( 30, 1, "spawn3", 0, 100 )
AddWave2( 30, 2, "spawn4", 0, 100 )
AddWave2( 30, 3, "spawn5", 1, 100 )
AddWave2( 30, 1, "spawn6", 0, 100 )
AddWave2( 30, 2, "spawn7", 0, 100 )
AddWave2( 30, 3, "spawn8", 0, 100 )
AddWave2( 30, 1, "spawn9", 1, 100 )
AddWave2( 30, 1, "spawn10", 0, 100 )
AddWave2( 30, 1, "spawn1", 1, 100 )
AddWave2( 30, 2, "spawn2", 0, 100 )
AddWave2( 30, 3, "spawn3", 0, 100 )
AddWave2( 30, 1, "spawn4", 0, 100 )
AddWave2( 30, 2, "spawn5", 1, 100 )
AddWave2( 30, 2, "spawn6", 0, 100 )
AddWave2( 30, 3, "spawn7", 0, 100 )
AddWave2( 30, 1, "spawn8", 0, 100 )
AddWave2( 30, 2, "spawn9", 1, 100 )
AddWave2( 30, 2, "spawn10", 0, 100 )
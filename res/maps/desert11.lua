-- to musi by�. Mars albo Moon
Map:SetType( MapType.Desert )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )

local started1 = false
local timer1 = 0
local destroyed1 = false
local started2 = false
local timer2 = 0
local finalwave = false
local destroyed2 = false

-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    TickWaves()
    
    if started1 and not destroyed1 then
        timer1 = timer1 - 1
        if timer1 <= 0 then
            AddWave2( 1, 1, "spawn1", 0, 15 )
            AddWave2( 1, 2, "spawn1", 2, 15 )
            timer1 = 150 * 3
        end
    end
    if started2 and not destroyed2 then
        timer2 = timer2 - 1
        if timer2 <= 0 then
            AddWave2( 1, 1, "spawn3", 0, 20 )
            AddWave2( 1, 2, "spawn3", 2, 20 )
            timer2 = 150 * 3
        end
    end
    if not finalwave and CheckEndDestroy() and EntityManager:Count() == 0 then
        Map:AddMarker( "dupa", 1820, 500 )
        finalwave = true
    end
end

AddObjectToDestroy( "ufo1" )
AddObjectToDestroy( "ufo2" )

function triggerend( entity )
    if entity:GetType() == EntityType.Player and CheckEndDestroy() then
        GameWon()
        return false
    end
    return true
end

function triggerstart1( entity )
    if entity:GetType() == EntityType.Player then
        started1 = true
        return false
    end
    return true
end

function triggerstart2( entity )
    if entity:GetType() == EntityType.Player then
        started2 = true
        return false
    end
    return true
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end

function trigger01( entity )
    if entity:GetType() == EntityType.Player then
        AddWave2( 30, 1, "spawn1", 0, 100 )
        AddWave2( 30, 2, "spawn2", 0, 100 )
        AddWave2( 30, 3, "spawn11", 2, 100 )
		AddWave2( 30, 3, "spawn1", 0, 100 )
        AddWave2( 30, 2, "spawn2", 2, 100 )
        AddWave2( 30, 1, "spawn11", 0, 100 )
        return false
    end
    return true
end

function trigger02( entity )
    if entity:GetType() == EntityType.Player then
        AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
		AddWave2( 30, 2, "spawn7", 2, 100 )
		AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
		AddWave2( 30, 2, "spawn7", 0, 100 )
        return false
    end
    return true
end

function trigger03( entity )
    if entity:GetType() == EntityType.Player then
        AddWave2( 30, 3, "spawn8", 0, 100 )
		AddWave2( 30, 2, "spawn9", 0, 100 )
		AddWave2( 30, 1, "spawn10", 0, 100 )
		AddWave2( 30, 3, "spawn8", 2, 100 )
		AddWave2( 30, 1, "spawn9", 0, 100 )
		AddWave2( 30, 3, "spawn10", 0, 100 )
		AddWave2( 30, 3, "spawn8", 2, 100 )
		AddWave2( 30, 1, "spawn9", 0, 100 )
		AddWave2( 30, 3, "spawn10", 0, 100 )
        return false
    end
    return true
end


function ObjectDestroyed( name )
    if name == "ufo1" then
        destroyed1 = true
    elseif name == "ufo2" then
        destroyed2 = true
    end
end

SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.WeaponBoost } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup3", 1, 19999999, 0, { PickupType.WeaponBoost } )


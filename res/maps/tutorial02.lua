-- to musi by�. Mars albo Moon
Map:SetType( MapType.Grass )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )

for i,v in ipairs( EntityParams ) do
    if v ~= EntityPlayer then
        EntityManager:SetEntityData( i, v.RotationSpeed, v.MoveSpeed, v.Orbs, v.XP, 0, 1, 0, 0, v.Score, 0, 1 )
    end
end

-- Add electricity shield to simple sectoid
local entity = EntityParams[EntityType.SectoidSimple]
EntityManager:SetEntityData( EntityType.SectoidSimple, entity.RotationSpeed, entity.MoveSpeed, entity.Orbs, entity.XP, 0, entity.HP, 0, 0, entity.Score, 1, entity.ShieldScale )

local wave = 0
-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    player:SetHitPoints( player:GetMaxHitPoints() )

    if wave > 0 and CheckEndWaves() and EntityManager:Count() == 0 then
        if wave == 3 then
            GameWon()
        else
            TutorialManager:OnEnemyWaveKilled( wave )
        end
    end
    TickWaves()
end

function Tutorial01()
    AddWave2( 30, 3, "spawn1", 1, 0 )
    AddWave2( 30, 2, "spawn2", 0.3, 20 )
    AddWave2( 30, 2, "spawn3", 0.3, 20 )
    AddWave2( 30, 2, "spawn4", 0.3, 20 )
    AddWave2( 30, 2, "spawn5", 0.3, 20 )
	AddWave2( 30, 3, "spawn6", 1.5, 50 )
    AddWave2( 30, 2, "spawn7", 0.3, 50 )
    AddWave2( 30, 2, "spawn8", 0.3, 50 )
    wave = 1
end

function Tutorial02()
	AddWave2( 30, 3, "spawn6", 1, 50 )
    AddWave2( 30, 2, "spawn7", 0, 50 )
    AddWave2( 30, 2, "spawn8", 0, 50 )
    AddWave2( 30, 2, "spawn9", 0, 50 )
    AddWave2( 30, 2, "spawn10", 0, 50 )
    wave = 2
end

function Tutorial03()
    AddWave2( 30, 1, "spawn1", 1, 0 )
    AddWave2( 30, 1, "spawn5", 0.3, 50 )
    AddWave2( 30, 1, "spawn2", 0.3, 50 )
    AddWave2( 30, 1, "spawn6", 0.3, 50 )
    AddWave2( 30, 1, "spawn3", 0.3, 50 )
    AddWave2( 30, 1, "spawn7", 0.3, 50 )
    AddWave2( 30, 1, "spawn4", 0.3, 50 )
    AddWave2( 30, 1, "spawn8", 0.3, 50 )
    wave = 3
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end

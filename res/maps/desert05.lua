-- to musi by�. Mars albo Moon
Map:SetType( MapType.Desert )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )

local started1 = false
local timer1 = 0
local destroyed1 = false
local started2 = false
local timer2 = 0
local finalwave = false
local destroyed2 = false

-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    TickWaves()
    
    if started1 and not destroyed1 then
        timer1 = timer1 - 1
        if timer1 <= 0 then
            AddWave2( 30, 1, "spawn1", 0.2, 20 )
            AddWave2( 30, 2, "spawn1", 0.3, 20 )
            AddWave2( 30, 3, "spawn1", 0.3, 20 )
            timer1 = 60 * 2
        end
    end
    if started2 and not destroyed2 then
        timer2 = timer2 - 1
        if timer2 <= 0 then
            AddWave2( 30, 1, "spawn3", 0, 15 )
            AddWave2( 30, 2, "spawn3", 0, 15 )
            AddWave2( 30, 1, "spawn3", 1, 15 )
            timer2 = 60 * 2
        end
    end
    if not finalwave and CheckEndDestroy() and EntityManager:Count() == 0 then
        Map:AddMarker( "dupa", 1310, 480 )
        finalwave = true
    end
end

AddObjectToDestroy( "ufo1" )
AddObjectToDestroy( "ufo2" )

function triggerend( entity )
    if entity:GetType() == EntityType.Player and CheckEndDestroy() then
        GameWon()
        return false
    end
    return true
end

function trigger1( entity )
    if entity:GetType() == EntityType.Player then
        started1 = true
        return false
    end
    return true
end

function trigger3( entity )
    if entity:GetType() == EntityType.Player then
        started2 = true
        return false
    end
    return true
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end

function trigger2( entity )
    if entity:GetType() == EntityType.Player then
        AddWave2( 30, 1, "spawn2-1", 0, 100 )
        AddWave2( 30, 2, "spawn2-2", 0, 100 )
        AddWave2( 30, 1, "spawn2-1", 0, 100 )
        AddWave2( 30, 2, "spawn2-2", 0, 100 )
        AddWave2( 30, 1, "spawn2-1", 0, 100 )
        AddWave2( 30, 2, "spawn2-2", 0, 100 )
        return false
    end
    return true
end

function ObjectDestroyed( name )
    if name == "ufo1" then
        destroyed1 = true
    elseif name == "ufo2" then
        destroyed2 = true
    end
end

SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.WeaponBoost } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )

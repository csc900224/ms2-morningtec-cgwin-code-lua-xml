-- to musi by�. Mars albo Moon
Map:SetType( MapType.Grass )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )


-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    if CheckEndWaves() and EntityManager:Count() == 0 then
        GameWon()
    end
    TickWaves()
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end


AddWave2( 30, 1, "spawn1", 0, 30 )
AddWave2( 30, 1, "spawn2", 0, 30 )
AddWave2( 30, 2, "spawn3", 0, 30 )
AddWave2( 30, 3, "spawn4", 0, 30 )
AddWave2( 30, 1, "spawn5", 2, 30 )
AddWave2( 30, 1, "spawn6", 0.5, 30 )
AddWave2( 30, 1, "spawn7", 0.5, 30 )
AddWave2( 30, 2, "spawn8", 0.5, 30 )
AddWave2( 30, 3, "spawn9", 0.5, 30 )
AddWave2( 30, 1, "spawn10", 1, 30 )
AddWave2( 30, 1, "spawn11", 1, 30 )
AddWave2( 30, 1, "spawn12", 1, 30 )
AddWave2( 30, 2, "spawn13", 1, 30 )
AddWave2( 30, 3, "spawn14", 0.5, 30 )
AddWave2( 30, 1, "spawn15", 0.5, 30 )
AddWave2( 30, 1, "spawn16", 0.5, 30 )
AddWave2( 30, 1, "spawn1", 0.5, 30 )
AddWave2( 30, 1, "spawn2", 0.5, 30 )
AddWave2( 30, 2, "spawn3", 0.5, 30 )
AddWave2( 30, 3, "spawn4", 0.5, 30 )
AddWave2( 30, 1, "spawn5", 1, 30 )
AddWave2( 30, 1, "spawn6", 1, 30 )
AddWave2( 30, 1, "spawn7", 1, 30 )
AddWave2( 30, 2, "spawn8", 1, 30 )
AddWave2( 30, 3, "spawn9", 1, 30 )
AddWave2( 30, 1, "spawn10", 1, 30 )
AddWave2( 30, 1, "spawn11", 0.5, 30 )
AddWave2( 30, 1, "spawn12", 0.5, 30 )
AddWave2( 30, 2, "spawn13", 0.5, 30 )
AddWave2( 30, 3, "spawn14", 0.5, 30 )
AddWave2( 30, 1, "spawn15", 0.5, 30 )
AddWave2( 30, 1, "spawn16", 0.5, 30 )
AddWave2( 30, 1, "spawn1", 5, 30 )
AddWave2( 30, 1, "spawn2", 0.5, 30 )
AddWave2( 30, 2, "spawn3", 0.5, 30 )
AddWave2( 30, 3, "spawn4", 0.5, 30 )
AddWave2( 30, 1, "spawn5", 0.5, 30 )
AddWave2( 30, 1, "spawn6", 0.5, 30 )
AddWave2( 30, 1, "spawn7", 0.5, 30 )
AddWave2( 30, 2, "spawn8", 0.5, 30 )
AddWave2( 30, 3, "spawn9", 0.5, 30 )
AddWave2( 30, 1, "spawn10", 0.5, 30 )
AddWave2( 30, 1, "spawn11", 0.5, 30 )
AddWave2( 30, 1, "spawn12", 0.5, 30 )
AddWave2( 30, 2, "spawn13", 0.5, 30 )
AddWave2( 30, 3, "spawn14", 0.5, 30 )
AddWave2( 30, 1, "spawn15", 0.5, 30 )
AddWave2( 30, 1, "spawn16", 0.5, 30 )

SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.WeaponBoost  } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup3", 1, 19999999, 0, { PickupType.WeaponBoost  } )
SpawnPickup( "powerup4", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup5", 1, 19999999, 0, { PickupType.WeaponBoost  } )
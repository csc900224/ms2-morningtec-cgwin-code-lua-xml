-- to musi by�. Mars albo Moon
Map:SetType( MapType.Grass )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )

local e = SpawnFriend( "kot", { EntityType.OctopusFriend } )
e:AddWaypoint( "wp00", 2 )
e:AddWaypoint( "wp000", 14 )
e:AddWaypoint( "wp01" )
e:AddWaypoint( "wp02", 3 )
e:AddWaypoint( "wp03", 3 )
e:AddWaypoint( "wp04" )
e:AddWaypoint( "wp05" )
e:AddWaypoint( "wp06" )
e:AddWaypoint( "wp07" )
e:AddWaypoint( "wp08" )
e:AddWaypoint( "wp09" )
e:AddWaypoint( "wp10" )
e:AddWaypoint( "wp11" )
e:AddWaypoint( "wp12" )
e:AddWaypoint( "wp13" )
e:AddWaypoint( "wp14", 10000 )

-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    
    TickWaves()
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end


function trigger00( entity )
    if entity:GetType() == EntityType.OctopusFriend and EntityManager:Count() == 0 then
        GameWon()
        return false
    end
    return true
end

function trigger01( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn1-1", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-2", 0.4, 100 )
		AddWave2( 30, 1, "spawn1-3", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-4", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-5", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-6", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-7", 0.4, 100 )
		AddWave2( 30, 2, "spawn1-8", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-9", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-10", 0.4, 100 )
		AddWave2( 30, 3, "spawn1-11", 0.4, 100 )
		AddWave2( 30, 2, "spawn1-12", 0.4, 100 )
        return false
    end
    return true
end

function trigger02( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn1-5", 0, 100 )
        AddWave2( 30, 2, "spawn1-6", 0, 100 )
        AddWave2( 30, 3, "spawn3", 0, 100 )
		AddWave2( 30, 2, "spawn11", 0, 100 )
		AddWave2( 30, 1, "spawn1-5", 1.5, 100 )
        AddWave2( 30, 2, "spawn1-6", 0, 100 )
        AddWave2( 30, 3, "spawn3", 0, 100 )
		AddWave2( 30, 2, "spawn11", 0, 100 )
        return false
    end
    return true
end

function trigger03( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn4", 0, 100 )
        AddWave2( 30, 2, "spawn5", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
		AddWave2( 30, 2, "spawn5", 0, 100 )
        AddWave2( 30, 1, "spawn6", 0, 100 )
        AddWave2( 30, 3, "spawn4", 2, 100 )
        AddWave2( 30, 2, "spawn5", 0, 100 )
        AddWave2( 30, 1, "spawn6", 0, 100 )
		AddWave2( 30, 2, "spawn5", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
        return false
    end
    return true
end

function trigger04( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 1, "spawn7", 0, 100 )
        AddWave2( 30, 1, "spawn5", 1, 100 )
        AddWave2( 30, 1, "spawn7", 0, 100 )
        AddWave2( 30, 2, "spawn5", 1, 100 )
        AddWave2( 30, 1, "spawn7", 0, 100 )
        return false
    end
    return true
end

function trigger05( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn8", 0, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
        AddWave2( 30, 1, "spawn8", 2, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
		AddWave2( 30, 1, "spawn8", 2, 100 )
        AddWave2( 30, 1, "spawn9", 0, 100 )
        return false
    end
    return true
end

function trigger06( entity )
    if entity:GetType() == EntityType.OctopusFriend then
		AddWave2( 30, 1, "spawn10", 0, 100 )
		AddWave2( 30, 3, "spawn3", 0, 100 )
		AddWave2( 30, 3, "spawn1-4", 0, 100 )
		AddWave2( 30, 1, "spawn10", 0, 100 )
		AddWave2( 30, 2, "spawn3", 2, 100 )
		AddWave2( 30, 1, "spawn10", 0, 100 )
		AddWave2( 30, 3, "spawn3", 0, 100 )
		AddWave2( 30, 3, "spawn1-4", 0, 100 )
		AddWave2( 30, 1, "spawn10", 0, 100 )
		AddWave2( 30, 2, "spawn3", 0, 100 )
        return false
    end
    return true
end

function trigger07( entity )
    if entity:GetType() == EntityType.OctopusFriend then
		AddWave2( 30, 1, "spawn11", 0, 100 )
		AddWave2( 30, 3, "spawn13", 0, 100 )
		AddWave2( 30, 3, "spawn11", 0, 100 )
		AddWave2( 30, 1, "spawn12", 4, 100 )
		AddWave2( 30, 2, "spawn11", 0, 100 )
		AddWave2( 30, 2, "spawn12", 0, 100 )
        return false
    end
    return true
end

SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.WeaponBoost } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "powerup3", 1, 19999999, 0, { PickupType.WeaponBoost } )
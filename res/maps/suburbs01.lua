-- to musi by�. Mars albo Moon
Map:SetType( MapType.Grass )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "spawn1", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )

local e = SpawnFriend( "spawn2", { EntityType.OctopusFriend } )
e:AddWaypoint( "wp00", 1.2 )
e:AddWaypoint( "wp01" )
e:AddWaypoint( "wp02", 9 )
e:AddWaypoint( "wp03", 3 )
e:AddWaypoint( "wp04", 9 )
e:AddWaypoint( "wp05" )
e:AddWaypoint( "wp06" )
e:AddWaypoint( "wp07", 9 )
e:AddWaypoint( "wp08" )
e:AddWaypoint( "wp09", 9 )
e:AddWaypoint( "wp10", 10000 )

-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    
    TickWaves()
end

-- trigger
function TriggerSpawn()
    return false
end


-- trigger
function TriggerStartSpawning()
    return false
end


function trigger01( entity )
    if entity:GetType() == EntityType.OctopusFriend and EntityManager:Count() == 0 then
        GameWon()
        return false
    end
    return true
end

function trigger02( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn3", 0, 100 )
        AddWave2( 30, 1, "spawn4", 0, 100 )
        AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 2, "spawn3", 2, 100 )
        return false
    end
    return true
end

function trigger03( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
        AddWave2( 30, 1, "spawn7", 0, 100 )
        AddWave2( 30, 2, "spawn6", 1, 100 )
        return false
    end
    return true
end

function trigger04( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn7", 0, 100 )
        AddWave2( 30, 3, "spawn8", 0, 100 )
        AddWave2( 30, 2, "spawn9", 0, 100 )
        AddWave2( 30, 3, "spawn7", 2, 100 )
        AddWave2( 30, 1, "spawn8", 0, 100 )
        return false
    end
    return true
end

function trigger06( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 1, "spawn5", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
        AddWave2( 30, 1, "spawn7", 0, 100 )
        AddWave2( 30, 3, "spawn6", 0, 100 )
        AddWave2( 30, 2, "spawn7", 0, 100 )
        return false
    end
    return true
end

function trigger07( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 2, "spawn14", 0, 100 )
        AddWave2( 30, 1, "spawn13", 0, 100 )
		AddWave2( 30, 3, "spawn4", 1, 100 )
        return false
    end
    return true
end

function trigger08( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 3, "spawn5", 0, 100 )
        AddWave2( 30, 2, "spawn14", 0, 100 )
		AddWave2( 30, 1, "spawn15", 0, 100 )
        return false
    end
    return true
end

function trigger05( entity )
    if entity:GetType() == EntityType.OctopusFriend then
        AddWave2( 30, 2, "spawn10", 0, 100 )
        AddWave2( 30, 1, "spawn11", 0, 100 )
		AddWave2( 30, 3, "spawn16", 0, 100 )
		AddWave2( 30, 2, "spawn10", 5, 100 )
		AddWave2( 30, 1, "spawn11", 0, 100 )
        return false
    end
    return true
end


SpawnPickup( "spawnWP1", 1, 19999999, 0, { PickupType.WeaponBoost } )
SpawnPickup( "spawnWP2", 1, 19999999, 0, { PickupType.Health } )
SpawnPickup( "spawnWP3", 1, 19999999, 0, { PickupType.WeaponBoost } )
-- to musi by�. Mars albo Moon
Map:SetType( MapType.Desert )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )


-- to musi by�
function LevelTick()
	-- spawn jak w survivalu
    -- SurvivalSpawnTick()
	-- spawn pickupow jak w survivalu
	--SurvivalPickupSpawnTick()
	if CheckEndWaves() and EntityManager:Count() == 0 and CheckEndPotato(3) then
		GameWon()
	end
	TickWaves()
end

-- trigger
function TriggerSpawn()
	return false
end


-- trigger
function TriggerStartSpawning()
	return false
end

SpawnPickup( "quest1", 1, 19999999, 0, { PickupType.Potato } )
SpawnPickup( "quest2", 1, 19999999, 0, { PickupType.Potato } )
SpawnPickup( "quest3", 1, 19999999, 0, { PickupType.Potato } )

function trigger1( entity )
	if entity:GetType() == EntityType.Player then
		AddWave2( 30, 1, "spawn1", 0, 100 )
		AddWave2( 30, 1, "spawn2", 0, 100 )
		AddWave2( 30, 1, "spawn3", 0, 100 )
		AddWave2( 30, 2, "spawn1", 2, 100 )
		AddWave2( 30, 2, "spawn2", 0, 100 )
		AddWave2( 30, 2, "spawn3", 0, 100 )
		return false
	end
	return true
end

function trigger2( entity )
	if entity:GetType() == EntityType.Player then
		AddWave2( 30, 1, "spawn1", 0, 100 )
		AddWave2( 30, 1, "spawn2", 0, 100 )
		AddWave2( 30, 3, "spawn1", 2, 100 )
		AddWave2( 30, 2, "spawn3", 0, 100 )
		AddWave2( 30, 1, "spawn1", 0, 100 )
		AddWave2( 30, 1, "spawn2", 0, 100 )
		AddWave2( 30, 3, "spawn1", 0, 100 )
		AddWave2( 30, 2, "spawn3", 0, 100 )
		return false
	end
	return true
end

function trigger3( entity )
	if entity:GetType() == EntityType.Player then
		AddWave2( 30, 1, "spawn1", 0, 100 )
		AddWave2( 30, 1, "spawn2", 0, 100 )
		AddWave2( 30, 1, "spawn3", 0, 100 )
		AddWave2( 30, 2, "spawn1", 5, 100 )
		AddWave2( 30, 2, "spawn2", 0, 100 )
		AddWave2( 30, 2, "spawn3", 0, 100 )
		return false
	end
	return true
end

function trigger4( entity )
	if entity:GetType() == EntityType.Player then
		AddWave2( 30, 1, "spawn1-1", 0, 100 )
		AddWave2( 30, 1, "spawn1-2", 0, 100 )
		AddWave2( 30, 1, "spawn1-1", 0, 100 )
		AddWave2( 30, 1, "spawn1-2", 0, 100 )
		AddWave2( 30, 1, "spawn1-1", 0, 100 )
		AddWave2( 30, 1, "spawn1-2", 0, 100 )
		return false
	end
	return true
end

function trigger5( entity )
	if entity:GetType() == EntityType.Player then
		AddWave2( 30, 1, "spawn2-1", 0, 100 )
		AddWave2( 30, 2, "spawn2-2", 0, 100 )
		AddWave2( 30, 1, "spawn2-1", 0, 100 )
		AddWave2( 30, 2, "spawn2-2", 0, 100 )
		AddWave2( 30, 1, "spawn2-1", 0, 100 )
		AddWave2( 30, 2, "spawn2-2", 0, 100 )
	
		return false
	end
	return true
end

function trigger6( entity )
	if entity:GetType() == EntityType.Player then
		AddWave2( 30, 3, "spawn3-1", 0, 100 )
		AddWave2( 30, 3, "spawn3-2", 0, 100 )
		AddWave2( 30, 2, "spawn3-3", 0, 100 )
		AddWave2( 30, 3, "spawn3-1", 0, 100 )
		AddWave2( 30, 1, "spawn3-2", 0, 100 )
		AddWave2( 30, 3, "spawn3-3", 0, 100 )
		return false
	end
	return true
end


SpawnPickup( "powerup1", 1, 19999999, 0, { PickupType.WeaponBoost } )
SpawnPickup( "powerup2", 1, 19999999, 0, { PickupType.Health } )
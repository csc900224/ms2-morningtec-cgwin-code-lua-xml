-- to musi by�. Mars albo Moon
Map:SetType( MapType.City )

-- to musi by�
--player = EntityManager:Add( 450, 450, EntityType.Player )
player = Spawn( "start", 1, { EntityType.Player } )[1]

-- ustawienie liczby punkt�w wymaganych dla 1, 2, 3 gwiazdek (opcjonalne)
SetStars( 10, 50, 100 )

GameManager:NextPerkLevel( 100 )
GameManager:NextPerkLevel( 200 )
GameManager:NextPerkLevel( 350 )
GameManager:NextPerkLevel( 550 )
GameManager:NextPerkLevel( 900 )
GameManager:NextPerkLevel( 1400 )
GameManager:NextPerkLevel( 2000 )
GameManager:NextPerkLevel( 3000 )
GameManager:NextPerkLevel( 4500 )
GameManager:NextPerkLevel( 6000 )
GameManager:NextPerkLevel( 8000 )
GameManager:NextPerkLevel( 12000 )
GameManager:NextPerkLevel( 16500 )
GameManager:NextPerkLevel( 22000 )
GameManager:NextPerkLevel( 32000 )
GameManager:NextPerkLevel( 50000 )
GameManager:NextPerkLevel( 75000 )

for i,v in ipairs( EntityParams ) do
    if v ~= EntityPlayer then
        EntityManager:SetEntityData( i, v.RotationSpeed, v.MoveSpeed, v.Orbs, v.XP, 0, 1, 0, 0, v.Score, 0, 1 )
    end
end

TutorialHitPointsRatio = 1

local check = false
-- to musi by�
function LevelTick()
    -- spawn jak w survivalu
    -- SurvivalSpawnTick()
    -- spawn pickupow jak w survivalu
    --SurvivalPickupSpawnTick()
    player:SetHitPoints( player:GetMaxHitPoints() * TutorialHitPointsRatio )

    if check and CheckEndWaves() and EntityManager:Count() == 0 then
        GameWon()
    end
    TickWaves()
end

function Tutorial01()
    AddWave2( 60, 1, "spawn1", 1, 0 )
    AddWave2( 60, 1, "spawn2", 0, 100 )
    AddWave2( 60, 1, "spawn3", 1, 100 )
    AddWave2( 60, 1, "spawn6", 0, 100 )
    AddWave2( 60, 1, "spawn4", 1, 100 )
    AddWave2( 60, 1, "spawn5", 0, 100 )
    AddWave2( 60, 1, "spawn1", 1, 100 )
    AddWave2( 60, 1, "spawn2", 0, 100 )
    AddWave2( 60, 1, "spawn6", 1, 100 )
    AddWave2( 60, 1, "spawn3", 0, 100 )
    AddWave2( 60, 1, "spawn4", 1, 100 )
    AddWave2( 60, 1, "spawn5", 0, 100 )
    check = true
end

-- trigger
function TriggerSpawn()
    return false
end

-- trigger
function TriggerStartSpawning()
    return false
end

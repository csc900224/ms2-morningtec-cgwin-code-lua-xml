require( "WeaponUpgrades.lua" )
require( "ShopItem.lua" )
require( "deepcopy.lua" )

local function angle( val )
    return val / 180 * math.pi
end

function WeaponCalcTime( val )
    return val
end

WeaponPistol = {
    id = ShopItem.Pistol,
    InitialDelay = 0,
    Delay = 0,
    Speed = 7,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 2 ),
    Damage = 2,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 50,
    Reload = 50,
    MovementChange = 0.35,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Plasma,
    SfxShot = Sfx.SFX_PLASMA,
    SfxSpindown = Sfx.SFX_PLASMA_END,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    UpgradeData = UpgradePistol,
    Atlas = 6
}

WeaponShotgun = {
    id = ShopItem.Shotgun,
    InitialDelay = 0,
    Delay = 0,
    Speed = 10,
    SpeedVariation = 3,
    Bullets = 0,
    Spread = 0.5,
    Damage = 0,
    DamageVariation = 1,
    Clip = 0,
    Reload = 70,
    MovementChange = 0.3,
    LifeTime = 30,
    Steps = 1,
    Shake = 1,
    Type = ShotType.Shell,
    SfxShot = Sfx.SFX_SHOTGUN2,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0 },
    Elements = 0,
    UpgradeData = UpgradeShotgun,
    Atlas = 13
}

WeaponCombatShotgun = {
    id = ShopItem.CombatShotgun,
    InitialDelay = 0,
    Delay = 0,
    Speed = 10,
    SpeedVariation = 3,
    Bullets = 0,
    Spread = 0.3,
    Damage = 0,
    DamageVariation = 1,
    Clip = 0,
    Reload = 90,
    MovementChange = 0.3,
    LifeTime = 30,
    Steps = 1,
    Shake = 2,
    Type = ShotType.CombatShell,
    SfxShot = Sfx.SFX_SHOTGUN1,
    SfxLoop = false,
    Vibra = false,
    Warning = { 1, 3 },
    Elements = 0,
    UpgradeData = UpgradeCombatShotgun,
    Atlas = 4
}

WeaponUzi = {
    id = ShopItem.SMG,
    InitialDelay = 0,
    Delay = 0,
    Speed = 12,
    SpeedVariation = 3,
    Bullets = 1,
    Spread = 0.1,
    Damage = 0,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 50,
    MovementChange = 0.3,
    LifeTime = 25,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Bullet,
    SfxShot = Sfx.SFX_PISTOL,
    SfxSpindown = Sfx.SFX_PISTOL_END,
    SfxLoop = true,
    Vibra = false,
    Warning = { 10, 20 },
    Elements = 0,
    UpgradeData = UpgradeUzi,
    Atlas = 15
}

WeaponChaingun = {
    id = ShopItem.Chaingun,
    InitialDelay = 40,
    Delay = 0,
    Speed = 18,
    SpeedVariation = 3,
    Bullets = 2,
    Spread = 0.2,
    Damage = 0,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 80,
    MovementChange = 0.3,
    LifeTime = -1,
    Steps = 1,
    Shake = 1,
    Type = ShotType.Minigun,
    SfxShot = Sfx.SFX_MINIGUN,
    SfxSpinup = Sfx.SFX_SPINUP,
    SfxSpindown = Sfx.SFX_SPINDOWN,
    SfxLoop = true,
    Vibra = true,
    Warning = { 15, 30 },
    Elements = 0,
    UpgradeData = UpgradeChaingun,
    Atlas = 0
}

WeaponRocketLauncher = {
    id = ShopItem.RocketLauncher,
    InitialDelay = 0,
    Delay = 0,
    Speed = 8,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 10 ),
    Damage = 0,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 50,
    MovementChange = 0.1,
    LifeTime = -1,
    Steps = 1,
    Shake = 2,
    Type = ShotType.Rocket,
    SfxShot = Sfx.SFX_ROCKET,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0, 1 },
    Elements = 0,
    UpgradeData = UpgradeRocketLauncher,
    Atlas = 10
}

WeaponFlamer = {
    id = ShopItem.Flamer,
    InitialDelay = 5,
    Delay = 0,
    Speed = 4,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 30 ),
    Damage = 0,
    DamageVariation = 0.2,
    Clip = 0,
    Reload = 70,
    MovementChange = 0.2,
    LifeTime = 40,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Flamer,
    SfxShot = Sfx.SFX_FLAMER,
    SfxSpindown = Sfx.SFX_SHOCKERBURN,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    UpgradeData = UpgradeFlamer,
    Atlas = 9
}

WeaponElectricity = {
    id = ShopItem.Electricity,
    InitialDelay = 1,
    Delay = 0,
    Speed = 4,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 30 ),
    Damage = 0,
    DamageVariation = 0.05,
    Clip = 0,
    Reload = 70,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Electricity,
    SfxShot = Sfx.SFX_SHOCKERLOOP,
    SfxSpinup = Sfx.SFX_SHOCKERSTART,
    SfxSpindown = Sfx.SFX_SHOCKEREND,
    SfxLoop = true,
    Vibra = false,
    Warning = { 10, 20 },
    Elements = 0,
    UpgradeData = UpgradeElectricity,
    Atlas = 12
}

WeaponLaser = {
    id = "laser",
    InitialDelay = 0,
    Delay = 10,
    Speed = 4,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = 0,
    Damage = 0.975,
    DamageVariation = 0.05,
    Clip = 100,
    Reload = 70,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Laser,
    SfxShot = Sfx.SFX_CRAB_LASER,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponRailgun = {
    id = ShopItem.Railgun,
    InitialDelay = 0,
    Delay = 50,
    Speed = 30,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 0 ),
    Damage = 30,
    DamageVariation = 0,
    Clip = 5,
    Reload = 50,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 4,
    Shake = 5,
    Type = ShotType.Railgun,
    SfxShot = Sfx.SFX_MAGNUM,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0, 1 },
    Elements = 0,
    UpgradeData = UpgradeRailgun,
    Atlas = 7
}

WeaponRipper = {
    id = ShopItem.Ripper,
    InitialDelay = 0,
    Delay = 0,
    Speed = 5,
    SpeedVariation = 3,
    Bullets = 1,
    Spread = 0,
    Damage = 0,	-- multiplied by 4.0 in case of player hit
    DamageVariation = 1,
    Clip = 0,
    Reload = 100,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 4,
    Shake = 1,
    Type = ShotType.Saw,
    SfxShot = Sfx.SFX_RIPPER,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0, 1, 2 },
    Elements = 0,
    UpgradeData = UpgradeRipper,
    Atlas = 8
}

WeaponSpiral = {
    id = ShopItem.Spiral,
    InitialDelay = 0,
    Delay = 0,
    Speed = 5,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 2 ),
    Damage = 0,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 70,
    MovementChange = 0.35,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Spiral,
    SfxShot = Sfx.SFX_TRIPALIZER_LOOP,
    SfxSpinup = Sfx.SFX_TRIPALIZER_START,
    SfxSpindown = Sfx.SFX_TRIPALIZER_END,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    UpgradeData = UpgradeSpiral,
    Atlas = 16
}

WeaponFishGlop = {
    id = "fishglop",
    InitialDelay = 0,
    Delay = 10,
    Speed = 3,
    SpeedVariation = 1,
    Bullets = 1,
    Spread = 0.15,
    Damage = 1, -- multiplied by 4.0 in case of player hit
    DamageVariation = 0.5,
    Clip = 1,
    Reload = 100,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 10,
    Type = ShotType.FishGlop,
    SfxShot = Sfx.SFX_SPIT,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponGrenade = {
    id = ShopItem.Grenade,
    InitialDelay = 0,
    Delay = 10,
    Speed = 1,
    SpeedVariation = 1,
    Bullets = 3,
    Spread = 0.6,
    Damage = 20,
    DamageVariation = 0.5,
    Clip = 1,
    Reload = 100,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Grenade,
    SfxShot = Sfx.SFX_RIPPER,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponMine = {
    id = ShopItem.Mine,
    InitialDelay = 0,
    Delay = 10,
    Speed = 1,
    SpeedVariation = 1,
    Bullets = 3,
    Spread = 0.6,
    Damage = 40,
    DamageVariation = 0.5,
    Clip = 1,
    Reload = 100,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Mine,
    SfxShot = Sfx.SFX_MINE_ARM,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponHoundShot = {
    id = "houndshot",
    InitialDelay = 0,
    Delay = 10,
    Speed = 0,
    SpeedVariation = 0,
    Bullets = 1,
    Spread = 0,
    Damage = 2.5,
    DamageVariation = 0.5,
    Clip = 1,
    Reload = 10,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 0,
    Type = ShotType.HoundShot,
    SfxShot = Sfx.SFX_RIPPER,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponLobsterShot = {
    id = "lobstershot",
    InitialDelay = 0,
    Delay = 10,
    Speed = 0,
    SpeedVariation = 0,
    Bullets = 1,
    Spread = 0,
    Damage = 2.5,
    DamageVariation = 0.5,
    Clip = 1,
    Reload = 10,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 0,
    Type = ShotType.HoundShot,
    SfxShot = Sfx.SFX_RIPPER,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponBossFlamer = {
    id = "bossflamer",
    InitialDelay = 5,
    Delay = 0,
    Speed = 4,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 30 ),
    Damage = 0.2,
    DamageVariation = 0.2,
    Clip = 150,
    Reload = 70,
    MovementChange = 0.2,
    LifeTime = 40,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Flamer,
    SfxShot = Sfx.SFX_FLAMER,
    SfxSpindown = Sfx.SFX_SHOCKERBURN,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    Atlas = 0
}

WeaponBossRocketLauncher = {
    id = "bossrocketlauncher",
    InitialDelay = 0,
    Delay = 20,
    Speed = 6,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 10 ),
    Damage = 3,
    DamageVariation = 0.1,
    Clip = 6,
    Reload = 50,
    MovementChange = 0.1,
    LifeTime = -1,
    Steps = 1,
    Shake = 2,
    Type = ShotType.Rocket,
    SfxShot = Sfx.SFX_ROCKET,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0, 1 },
    Elements = 0,
    Atlas = 0
}

WeaponVortex = {
    id = ShopItem.Vortex,
    InitialDelay = 0,
    Delay = 20,
    Speed = 4,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 10 ),
    Damage = 0.1,
    DamageVariation = 0,
    Clip = 6,
    Reload = 50,
    MovementChange = 0.2,
    LifeTime = 120,
    Steps = 1,
    Shake = 2,
    Type = ShotType.Vortex,
    SfxShot = Sfx.SFX_VORTEX,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0, 1 },
    Elements = 0,
    UpgradeData = UpgradeVortex,
    Atlas = 17
}

WeaponSectoid = {
    id = "sectoid",
    InitialDelay = 0,
    Delay = 0,
    Speed = 7,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 2 ),
    Damage = 2,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 50,
    Reload = 50,
    MovementChange = 0.35,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Plasma,
    SfxShot = Sfx.SFX_RAILGUN,
    SfxLoop = false,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    Atlas = 0
}

WeaponSowerSpit = {
    id = "sowerspit",
    InitialDelay = 0,
    Delay = 0,
    Speed = 5,
    SpeedVariation = 1,
    Bullets = 1,
    Spread = 0.15,
    Damage = 6,
    DamageVariation = 2,
    Clip = 1,
    Reload = 100,
    MovementChange = 0.5,
    LifeTime = 0,
    Steps = 1,
    Shake = 10,
    Type = ShotType.SowerSpit,
    SfxShot = Sfx.SFX_SOWER_SPIT,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponSowerEgg = {
    id = "soweregg",
    InitialDelay = 0,
    Delay = 10,
    Speed = 3,
    SpeedVariation = 1,
    Bullets = 1,
    Spread = 0.6,
    Damage = 0,
    DamageVariation = 0,
    Clip = 1,
    Reload = 100,
    MovementChange = 0.2,
    LifeTime = 0,
    Steps = 1,
    Shake = 15,
    Type = ShotType.SowerEgg,
    SfxShot = Sfx.SFX_SOWER_TOSS_EGGS,
    SfxLoop = false,
    Vibra = false,
    Warning = {},
    Elements = 0,
    Atlas = 0
}

WeaponOctobrain = {
    id = "octobrain",
    InitialDelay = 0,
    Delay = 0,
    Speed = 3,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 0 ),
    Damage = 1.3,
    DamageVariation = 0.5,
    Clip = 0,
    Reload = 70,
    MovementChange = 0.35,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Spiral,
    SfxShot = Sfx.SFX_TRIPALIZER_LOOP,
    SfxSpinup = Sfx.SFX_TRIPALIZER_START,
    SfxSpindown = Sfx.SFX_TRIPALIZER_END,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    Atlas = 0
}

WeaponOctobrainClone = {
    id = "octobrain",
    InitialDelay = 0,
    Delay = 0,
    Speed = 2,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 0 ),
    Damage = 1,
    DamageVariation = 0.1,
    Clip = 0,
    Reload = 70,
    MovementChange = 0.35,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Spiral,
    SfxShot = Sfx.SFX_TRIPALIZER_LOOP,
    SfxSpinup = Sfx.SFX_TRIPALIZER_START,
    SfxSpindown = Sfx.SFX_TRIPALIZER_END,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    Atlas = 0
}

WeaponChainsaw = {
    id = ShopItem.Chainsaw,
    InitialDelay = 40,
    Delay = 0,
    Speed = 18,
    SpeedVariation = 0,
    Bullets = 3,
    Spread = angle( 45 ),
    Damage = 1,
    DamageVariation = 0.1,
    Clip = 200,
    Reload = 25,
    MovementChange = 0.35,
    LifeTime = 2,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Chainsaw,
    SfxShot = Sfx.SFX_CHAINSAW_LOOP,
    SfxSpinup = Sfx.SFX_CHAINSAW_START,
    SfxSpindown = Sfx.SFX_CHAINSAW_STOP,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5 },
    Elements = 0,
    UpgradeData = UpgradeChainsaw,
    Atlas = 11
}

WeaponDeadSpace = {
    id = ShopItem.LineGun,
    InitialDelay = 0,
    Delay = 30,
    Speed = 8,
    SpeedVariation = 0,
    Bullets = 1,
    Spread = 0,
    Damage = 0.5,
    DamageVariation = 0.1,
    Clip = 10,
    Reload = 25,
    MovementChange = 0.35,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.DeadSpace,
    SfxShot = Sfx.SFX_LINEGUN,
    SfxLoop = false,
    Vibra = false,
    Warning = { 5 },
    Elements = 0,
    UpgradeData = UpgradeLineGun,
    Atlas = 1
}

WeaponDual = {
    id = ShopItem.Magnum,
    InitialDelay = 0,
    Delay = 5,          -- must be odd
    Speed = 12,
    SpeedVariation = 3,
    Bullets = 1,
    Spread = 0.1,
    Damage = 0.6,
    DamageVariation = 0.1,
    Clip = 80,
    Reload = 50,
    MovementChange = 0.3,
    LifeTime = -1,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Magnum,
    SfxShot = Sfx.SFX_SHOTGUN1,
    SfxLoop = false,
    Vibra = false,
    Warning = { 10, 20 },
    Elements = 0,
    UpgradeData = UpgradeMagnum,
    Atlas = 2
}

WeaponLurker = {
    id = ShopItem.Nailer,
    InitialDelay = 0,
    Delay = 30,
    Speed = 5,
    SpeedVariation = 0,
    Bullets = 1,
    Spread = 0.1,
    Damage = 0,
    DamageVariation = 0,
    Clip = 5,
    Reload = 70,
    MovementChange = 0.3,
    LifeTime = 0,
    Steps = 1,
    Shake = 1,
    Type = ShotType.Lurker,
    SfxShot = Sfx.SFX_SPIKES_SHOT,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0 },
    Elements = 0,
    UpgradeData = UpgradeNailer,
    Atlas = 14
}

WeaponMechFlamer = {
    id = ShopItem.Mech,
    InitialDelay = 2,
    Delay = 0,
    Speed = 4.5,
    SpeedVariation = 0.5,
    Bullets = 2,
    Spread = angle( 45 ),
    Damage = 0.5,
    DamageVariation = 0.2,
    Clip = 300,
    Reload = 50,
    MovementChange = 0.3,
    LifeTime = 40,
    Steps = 1,
    Shake = 0,
    Type = ShotType.Flamer,
    SfxShot = Sfx.SFX_FLAMER,
    SfxSpindown = Sfx.SFX_SHOCKERBURN,
    SfxLoop = true,
    Vibra = false,
    Warning = { 5, 10 },
    Elements = 0,
    UpgradeData = UpgradeMechFlamer,
    Atlas = 18
}

WeaponMechRocketLauncher = {
    id = ShopItem.MechRocket,
    InitialDelay = 0,
    Delay = 15,
    Speed = 8,
    SpeedVariation = 0.5,
    Bullets = 1,
    Spread = angle( 5 ),
    Damage = 10,
    DamageVariation = 0.1,
    Clip = 20,
    Reload = 50,
    MovementChange = 0.3,
    LifeTime = -1,
    Steps = 1,
    Shake = 2,
    Type = ShotType.Rocket,
    SfxShot = Sfx.SFX_ROCKET,
    SfxLoop = false,
    Vibra = false,
    Warning = { 0, 1 },
    Elements = 0,
    UpgradeData = UpgradeMechRocket,
    Atlas = 19
}

Weapons = {}
table.insert( Weapons, WeaponPistol )			-- 1
table.insert( Weapons, WeaponShotgun )			-- 2
table.insert( Weapons, WeaponCombatShotgun )		-- 3
table.insert( Weapons, WeaponUzi )			-- 4
table.insert( Weapons, WeaponChaingun )			-- 5
table.insert( Weapons, WeaponRocketLauncher )		-- 6
table.insert( Weapons, WeaponFlamer )			-- 7
table.insert( Weapons, WeaponElectricity )		-- 8
table.insert( Weapons, WeaponRailgun )			-- 9
table.insert( Weapons, WeaponRipper )			-- 10
table.insert( Weapons, WeaponSpiral )			-- 11
table.insert( Weapons, WeaponFishGlop )			-- 12
table.insert( Weapons, WeaponGrenade )			-- 13
table.insert( Weapons, WeaponMine )			-- 14
table.insert( Weapons, WeaponHoundShot )		-- 15
table.insert( Weapons, WeaponLobsterShot )		-- 16
table.insert( Weapons, WeaponLaser )			-- 17
table.insert( Weapons, WeaponBossFlamer )		-- 18
table.insert( Weapons, WeaponBossRocketLauncher )	-- 19
table.insert( Weapons, WeaponVortex )			-- 20
table.insert( Weapons, WeaponSectoid )			-- 21
table.insert( Weapons, WeaponSowerSpit )		-- 22
table.insert( Weapons, WeaponSowerEgg )			-- 23
table.insert( Weapons, WeaponOctobrain )			-- 24
table.insert( Weapons, WeaponOctobrainClone )			-- 25
table.insert( Weapons, WeaponChainsaw )			-- 26
table.insert( Weapons, WeaponDeadSpace )		-- 27
table.insert( Weapons, WeaponDual )     		-- 28
table.insert( Weapons, WeaponLurker )     		-- 29
table.insert( Weapons, WeaponMechFlamer )     	-- 30
table.insert( Weapons, WeaponMechRocketLauncher ) -- 31

WeaponsAI = deepcopy( Weapons )

WeaponsIdxById = {}
for idx, v in ipairs( Weapons ) do
    if v.id ~= nil then
        WeaponsIdxById[v.id] = idx
    end
end

WeaponsAIIdxById  = {}
for idx, v in ipairs( WeaponsAI ) do
    if v.id ~= nil then
        WeaponsAIIdxById[v.id] = idx
        
        -- Use same upgrade data as player weapons
        v.UpgradeData = Weapons[idx].UpgradeData
    end
end

WeaponSelected = WeaponsIdxById[registry:Get( "/monstaz/weaponselection/1" )]
if not WeaponSelected then WeaponSelected = 4 end
Reload = false
AutoReload = true
Switch = false
weaponboost = false

WeaponSwitched = 4
WeaponSwitchTime = 50

local autoaim = registry:Get( "/monstaz/settings/autoaim" )

function WeaponApplyUpgrade( weapon, lvl )
    if weapon.UpgradeData ~= nil then
        local idx = lvl + 1
        if weapon.UpgradeData.Delay then
            local delayIdx = math.max( 1, math.min( idx, #weapon.UpgradeData.Delay ) )
            weapon.Delay = weapon.UpgradeData.Delay[ delayIdx ]
        end
        if weapon.UpgradeData.Damage then
            local dmgIdx = math.max( 1, math.min( idx, #weapon.UpgradeData.Damage ) )
            weapon.Damage = weapon.UpgradeData.Damage[ dmgIdx ]
        end
        if weapon.UpgradeData.Clip then
            local clipIdx = math.max( 1, math.min( idx, #weapon.UpgradeData.Clip ) )
            weapon.Clip = weapon.UpgradeData.Clip[ clipIdx ]
        end
        if weapon.UpgradeData.Bullets then
            local bulletsIdx = math.max( 1, math.min( idx, #weapon.UpgradeData.Bullets ) )
            weapon.Bullets = weapon.UpgradeData.Bullets[ bulletsIdx ]
        end
        if weapon.UpgradeData.LifeTime then
            local lifeTimeIdx = math.max( 1, math.min( idx, #weapon.UpgradeData.LifeTime ) )
            weapon.LifeTime = weapon.UpgradeData.LifeTime[ lifeTimeIdx ]
        end
        weapon.Elements = 0
        if weapon.UpgradeData.Elements then
            local elIdx = math.max( 1, math.min( idx, #weapon.UpgradeData.Elements ) )
            for i=1,elIdx do
                local element = weapon.UpgradeData.Elements[i]
                if element > 0 then
                    weapon.Elements = weapon.Elements + element
                end
            end
        end
    end
end

Clip = {}
BulletReserve = {}

function WeaponApplyUpgrades()
    for i, v in ipairs( Weapons ) do
        if v.id ~= nil then
            local upgrade = Shop:GetUpgrades( v.id )
            WeaponApplyUpgrade( v, upgrade )
        end

        if v.Clip > 0 and Shop:IsBought( ShopItem.AmmoBonus ) > 0 then
            local prev = v.Clip
            v.Clip = math.max( v.Clip + 1, math.floor( v.Clip * 1.25 ) )
            ClawMsg( "Clip upgrade [" .. ( v.id and v.id or "<nil>" ) .. "] " .. prev .. " -> " .. v.Clip )
        end

        table.insert( Clip, v.Clip )
        table.insert( BulletReserve, 0 )
        
        if autoaim then
            if v == WeaponUzi then
                v.Damage = v.Damage * 0.7
            else
                v.Damage = v.Damage * 0.85
            end
        end

        if Shop:IsBought( ShopItem.DamageBonus ) ~= 0 then
            v.Damage = v.Damage * 1.1
            v.DamageVariation = v.DamageVariation * 1.1
        end
    end
end

function AIWeaponSelect( idx, upgrade )
    local weapon = WeaponsAI[idx]
    WeaponApplyUpgrade( weapon, upgrade )

    WeaponSelectedAI = idx
    WeaponUpgradeAI = upgrade
    ReloadAI = false
    ClipAI = weapon.Clip
    initdelayAI = 0
    cooldownAI = 0
    cooldownmaxAI = 1
    BulletReserveAI = 0
end

AIWeaponSelect( WeaponsAIIdxById[ShopItem.SMG], 0 )
-- Will be called form native code when server-sync will be finished
--WeaponApplyUpgrades()

BoughtWeapons = {}
for i,weapons in ipairs( BoughtPickups ) do
    for _,v in ipairs( weapons ) do
        local id = WeaponPickups[v]
        table.insert( BoughtWeapons, id )
        GameManager:AddVPadWeapon( Weapons[id].id )
    end
end
SelectedBoughtWeapon = 1

local cooldown = 0
local cooldownmax = 1
local initdelay = 0
local grenadecooldown = 0
local minecooldown = 0

SfxLoop = {}

function EntityStopFiring( weaponType, entity )
    local weapon = Weapons[weaponType]
    local px, py = entity:GetPos()

    if weapon.SfxLoop and SfxLoop[entity] and SfxLoop[entity][weapon.SfxShot] then
        AudioManager:StopLooped( SfxLoop[entity][weapon.SfxShot] )
        SfxLoop[entity][weapon.SfxShot] = nil
    end
    if weapon.SfxSpindown then
        AudioManager:Play( weapon.SfxSpindown )
    end
end

function EntityFireWeapon( weaponType, entity, lx, ly )
    local weapon = Weapons[weaponType]

    local px, py = entity:GetPos()
    local sx, sy = px + lx * 5, py + ly * 5

    local br = math.floor( weapon.Bullets )

    if weapon.SfxLoop then
        if not SfxLoop[entity] then SfxLoop[entity] = {} end
        if not SfxLoop[entity][weapon.SfxShot] then
            if weapon.SfxSpinup then
                AudioManager:Play( weapon.SfxSpinup, px, py )
            end
            SfxLoop[entity][weapon.SfxShot] = AudioManager:PlayLooped( weapon.SfxShot, px, py )
        else
            AudioManager:UpdatePos( SfxLoop[entity][weapon.SfxShot], px, py )
        end
    else
        AudioManager:Play( weapon.SfxShot, px, py )
    end

    -- Sniper perk decreases shot recoil by 30%
    local spread = weapon.Spread

    for i=1,br do
        local dx, dy = Rotate( lx, ly, math.random() * spread - spread / 2 )

        ShotManager:Add( entity, sx, sy, dx, dy,
            weapon.Speed + math.random() * weapon.SpeedVariation,
            ( weapon.Damage + math.random() * weapon.DamageVariation ),
            weapon.Type, weapon.LifeTime, weapon.Steps, false, weapon.Elements )
    end
end

local NoAmmoTimer = 0
local NoAmmoPlay = 2

local function PlayerFireWeapon()
    if cooldown > 0 then
        return
    end

    local weapon = Weapons[WeaponSelected]
    local unlimited = weapon.Clip == -1

    if not weaponboost then
        if not unlimited and Clip[WeaponSelected] < 1 then
            if AutoReload then DoReload() end
            return
        else
            if not unlimited then
                Clip[WeaponSelected] = Clip[WeaponSelected] - 1
            end
            cooldown = weapon.Delay

            local clip = Clip[WeaponSelected]
            for i,v in ipairs( weapon.Warning ) do
                if v == clip then
                    AudioManager:Play( Sfx.SFX_NO_AMMO )
                    break
                end
            end
        end
        ShakePower = math.max( weapon.Shake, ShakePower )
    else
        cooldown = weapon.Delay * 0.5
        ShakePower = math.max( weapon.Shake * 1.5, ShakePower )
    end

    local px, py = player:GetPos()
    local lx, ly = player:GetLook()
    local sx, sy = px + lx * 5, py + ly * 5
    local dmul = 1
    if playerRage and player:GetHitPoints() < player:GetMaxHitPoints() * 0.15 then
        dmul = 2
    end
    if weaponboost then
        dmul = dmul * 2
    end

    BulletReserve[WeaponSelected] = BulletReserve[WeaponSelected] + weapon.Bullets
    local br = math.floor( BulletReserve[WeaponSelected] )

    if weapon.SfxLoop then
        if not SfxLoop[player] then SfxLoop[player] = {} end
        if not SfxLoop[player][weapon.SfxShot] then
            SfxLoop[player][weapon.SfxShot] = AudioManager:PlayLooped( weapon.SfxShot )
        end
    else
        AudioManager:Play( weapon.SfxShot )
    end
    -- Process vibrations (StartLoop function checks internally if any loop is currently processed)
    if weapon.Vibra then
        local clips = Clip[WeaponSelected]
        if weaponboost then
            clips = 10000
        end
        local shakeTime = 0.5
        local breakTime = 0.5
        local tickTime = 1.0 / 60.0
        local shotingTime = tickTime * clips
        VibraController:StartLoop( shakeTime, breakTime, shotingTime / ( shakeTime + breakTime ) + 1 )
    end

    -- Sniper perk decreases shot recoil by 30%
    local weaponSpread = weapon.Spread
    if playerSniper then
        weaponSpread = weaponSpread * 0.7
    end

    for i=1,br do
        local spread = weaponSpread
        local dx, dy = Rotate( lx, ly, math.random() * spread - spread / 2 )

        ShotManager:Add( player, sx, sy, dx, dy,
            weapon.Speed + math.random() * weapon.SpeedVariation,
            ( weapon.Damage + math.random() * weapon.DamageVariation ) * dmul,
            weapon.Type, weapon.LifeTime, weapon.Steps, dmul == 2, weapon.Elements )
    end
    BulletReserve[WeaponSelected] = BulletReserve[WeaponSelected] - br

    if not weaponboost and Clip[WeaponSelected] < 1 then
        if AutoReload then DoReload() end
    end
end

local function AIFriendFireWeapon( entity )
    if cooldownAI > 0 then
        return
    end

    local weapon = WeaponsAI[WeaponSelectedAI]
    local unlimited = weapon.Clip == -1

    if not unlimited and ClipAI < 1 then
        DoReloadAI( entity )
        return
    else
        if not unlimited then
            ClipAI = ClipAI - 1
        end
        cooldownAI = weapon.Delay
    end

    local px, py = entity:GetPos()
    local lx, ly = entity:GetLook()
    local sx, sy = px + lx * 5, py + ly * 5

    BulletReserveAI = BulletReserveAI + weapon.Bullets
    local br = math.floor( BulletReserveAI )

    if weapon.SfxLoop then
        if not SfxLoop[entity] then SfxLoop[entity] = {} end
        if not SfxLoop[entity][weapon.SfxShot] then
            SfxLoop[entity][weapon.SfxShot] = AudioManager:PlayLooped( weapon.SfxShot, px, py )
        else
            AudioManager:UpdatePos( SfxLoop[entity][weapon.SfxShot], px, py )
        end
    else
        AudioManager:Play( weapon.SfxShot, px, py )
    end

    local weaponSpread = weapon.Spread

    for i=1,br do
        local spread = weaponSpread
        local dx, dy = Rotate( lx, ly, math.random() * spread - spread / 2 )

        ShotManager:Add( entity, sx, sy, dx, dy,
            weapon.Speed + math.random() * weapon.SpeedVariation,
            ( weapon.Damage + math.random() * weapon.DamageVariation ),
            weapon.Type, weapon.LifeTime, weapon.Steps, false, weapon.Elements )
    end
    BulletReserveAI = BulletReserveAI - br

    if ClipAI < 1 then
        DoReloadAI( entity )
    end
end

function DoReload()
    if Reload or Switch or Clip[WeaponSelected] == Weapons[WeaponSelected].Clip or Weapons[WeaponSelected].Clip == -1 then return end

    local weapon = Weapons[WeaponSelected]

    --Clip[WeaponSelected] = weapon.Clip
    cooldown = weapon.Reload

    if Shop:IsBought( ShopItem.ReloadBonus ) > 0 then
        cooldown = math.floor( cooldown * 0.75 )
    end

    cooldownmax = cooldown
    initdelay = 0
    Reload = true
    AudioManager:Play( Sfx.SFX_RELOAD1 + math.random( 4 ) - 1 )
    StopFiring()
end

function DoReloadAI( entity )
    if ReloadAI or ClipAI == WeaponsAI[WeaponSelectedAI].Clip or WeaponsAI[WeaponSelectedAI].Clip == -1 then return end

    local weapon = WeaponsAI[WeaponSelectedAI]

    local ex, ey = entity:GetPos()

    cooldownAI = weapon.Reload
    cooldownmaxAI = cooldownAI
    initdelayAI = 0
    ReloadAI = true
    AudioManager:Play( Sfx.SFX_RELOAD1 + math.random( 4 ) - 1, ex, ey )
    StopFiringAI( entity )
end

function WeaponTick( shot )
    if autoaim and shot then
        local e = GameManager:GetClosestEnemy()
        local tx, ty = GameManager:GetClosestTarget()

        if e or tx then
            local px, py = player:GetPos()
            local sx, sy = ShotManager:GetShotPos()
            local nx, ny

            if e and tx then
                local ex, ey = e:GetPos()
                local dex, dey = ex - px, ey - py
                local dtx, dty = tx - px, ty - py
                if DotProduct( dex, dey, dex, dey ) < DotProduct( dtx, dty, dtx, dty ) then
                    tx = nil
                else
                    e = nil
                end
            end

            if e then
                local ex, ey = e:GetPos()
                nx, ny = Normalize( ex - ( px + sx ), ey - ( py + sy ) )
            else
                nx, ny = Normalize( tx - ( px + sx ), ty - ( py + sy ) )
            end

            player:SetTarget( nx, ny )
            EntityManager:PlayerLookDirectionChanged()
        end
    end

    if cooldown > 0 then
        cooldown = cooldown - 1
        if Reload or Switch then
            if shot then
                if NoAmmoTimer == 0 and NoAmmoPlay > 0 then
                    AudioManager:Play( Sfx.SFX_NO_AMMO )
                    NoAmmoTimer = 20
                    NoAmmoPlay = NoAmmoPlay - 1
                else
                    NoAmmoTimer = NoAmmoTimer - 1
                end
            else
                NoAmmoTimer = 0
                NoAmmoPlay = 2
            end
        end
        if cooldown == 0 then
            NoAmmoTimer = 0
            NoAmmoPlay = 2
            if Switch then
                Switch = false
                WeaponSelected = WeaponSwitched
            elseif Reload then
                Reload = false
                Clip[WeaponSelected] = Weapons[WeaponSelected].Clip
            end
        end
    elseif initdelay > 0 then
        initdelay = initdelay - 1
        if initdelay == 0 then
            initdelay = -1
        end
    elseif shot then
        if initdelay == 0 then
            initdelay = Weapons[WeaponSelected].InitialDelay
            if initdelay == 0 then
                initdelay = -1
            elseif Weapons[WeaponSelected].SfxSpinup then
                AudioManager:Play( Weapons[WeaponSelected].SfxSpinup )
            end
        end
        if initdelay == -1 then
            PlayerFireWeapon()
        end
    else
        initdelay = 0
    end

    if grenadecooldown > 0 then
        grenadecooldown = grenadecooldown - 1
    end
    if minecooldown > 0 then
        minecooldown = minecooldown - 1
    end
end

function WeaponTickAI( shot, entity )
    if cooldownAI > 0 then
        cooldownAI = cooldownAI - 1
        if cooldownAI == 0 then
            if ReloadAI then
                ReloadAI = false
                ClipAI = WeaponsAI[WeaponSelectedAI].Clip
            end
        end
    elseif initdelayAI > 0 then
        initdelayAI = initdelayAI - 1
        if initdelayAI == 0 then
            initdelayAI = -1
        end
    elseif shot then
        if initdelayAI == 0 then
            initdelayAI = WeaponsAI[WeaponSelectedAI].InitialDelay
            if initdelayAI == 0 then
                initdelayAI = -1
            elseif WeaponsAI[WeaponSelectedAI].SfxSpinup then
                local ex, ey = entity:GetPos()
                AudioManager:Play( WeaponsAI[WeaponSelectedAI].SfxSpinup, ex, ey )
            end
        end
        if initdelayAI == -1 then
            AIFriendFireWeapon( entity )
        end
    else
        initdelayAI = 0
    end
end

function CheckReload()
    if Reload then
        return ( cooldownmax - cooldown ) / cooldownmax
    else
        return 1
    end
end

function CheckAmmo()
    local w = Weapons[WeaponSelected]
    if w.Clip == -1 then
        return -1
    else
        return Clip[WeaponSelected]
    end
end

function SetClip( ammo )
    local w = Weapons[WeaponSelected]
    if w.Clip ~= -1 then
        Clip[WeaponSelected] = ammo
    end
end

function CheckWeapon()
    return Weapons[WeaponSelected].id, Weapons[WeaponSelected].Elements
end

function CheckWeaponSlowDown()
    return Weapons[WeaponSelected].MovementChange
end

function CheckNumWeapons()
    return #Weapons
end

function CheckNumBoughtWeapons()
    return #BoughtWeapons
end

function NextWeapon()
    local ws = WeaponSelected + 1
    if ws > #Weapons then
        ws = 1
    end
    SelectWeapon( ws )
end

function NextBoughtWeapon()
    local ws = SelectedBoughtWeapon + 1
    if ws > #BoughtWeapons then
        ws = 1
    end
    SelectWeapon( BoughtWeapons[ws] )
    SelectedBoughtWeapon = ws
    GameManager:VPadWeaponSwitch()
end

function PrevBoughtWeapon()
    local ws = SelectedBoughtWeapon - 1
    if ws < 1 then
        ws = #BoughtWeapons
    end
    SelectWeapon( BoughtWeapons[ws] )
    SelectedBoughtWeapon = ws
end

function SelectWeapon( weapon )
    ClawMsg( "Selected weapon: " .. weapon )
    StopFiring()
    WeaponSelected = weapon
    registry:Set( "/internal/currentweapon", Weapons[WeaponSelected].id )
--	Clip[WeaponSelected] = Weapons[WeaponSelected].Clip
    StopReload()
    StopSwitch()
    GameManager:PlayerChangedWeapon( Weapons[weapon].Atlas )
end

function StopFiring()
    if not playerShot then return end

    local weapon = Weapons[WeaponSelected]

    if weapon.SfxLoop and SfxLoop[player][weapon.SfxShot] then
        AudioManager:StopLooped( SfxLoop[player][weapon.SfxShot] )
        SfxLoop[player][weapon.SfxShot] = nil
    end
    if weapon.SfxSpindown then
        AudioManager:Play( weapon.SfxSpindown )
    end
    if weapon.Vibra then
        VibraController:Stop()
    end
end

function StopFiringAI( entity )
    if not AIFriendShot then return end

    local ex, ey = entity:GetPos()

    local weapon = WeaponsAI[WeaponSelectedAI]

    if weapon.SfxLoop and SfxLoop[entity][weapon.SfxShot] then
        AudioManager:StopLooped( SfxLoop[entity][weapon.SfxShot] )
        SfxLoop[entity][weapon.SfxShot] = nil
    end
    if weapon.SfxSpindown then
        AudioManager:Play( weapon.SfxSpindown, ex, ey )
    end
end

function StopSwitch()
    Switch = false
    cooldown = 0
end

function StopReload()
    Reload = false
    cooldown = 0
end

function FireGrenade()
    if grenadecooldown > 0 then return end

    Shop:Use( ShopItem.Grenade )

    local weapon = WeaponGrenade

    local px, py = player:GetPos()
    local lx, ly = player:GetLook()
    local sx, sy = px + lx * 5, py + ly * 5

    AudioManager:Play( weapon.SfxShot )

    local weaponSpread = weapon.Spread
    local br = math.floor( weapon.Bullets )

    for i=1,br do
        local dx, dy = Rotate( lx, ly, math.random() * weaponSpread - weaponSpread / 2 )

        ShotManager:Add( player, sx, sy, dx, dy,
            weapon.Speed + math.random() * weapon.SpeedVariation,
            ( weapon.Damage + math.random() * weapon.DamageVariation ),
            weapon.Type, weapon.LifeTime, weapon.Steps, false, weapon.Elements )
    end

    ShakePower = math.max( weapon.Shake, ShakePower )

    grenadecooldown = weapon.Reload
end

function PlaceMine()
    if minecooldown > 0 then return end

    Shop:Use( ShopItem.Mine )

    local weapon = WeaponMine

    local px, py = player:GetPos()
    local lx, ly = player:GetLook()
    local sx, sy = px + lx * 5, py + ly * 5

    AudioManager:Play( weapon.SfxShot )

    local weaponSpread = weapon.Spread
    local br = math.floor( weapon.Bullets )

    for i=1,br do
        local dx, dy = Rotate( lx, ly, math.random() * weaponSpread - weaponSpread / 2 )

        ShotManager:Add( player, sx, sy, dx, dy,
            weapon.Speed + math.random() * weapon.SpeedVariation,
            ( weapon.Damage + math.random() * weapon.DamageVariation ),
            weapon.Type, weapon.LifeTime, weapon.Steps, false, weapon.Elements )
    end

    ShakePower = math.max( weapon.Shake, ShakePower )

    minecooldown = weapon.Reload
end

function HoundShot( x, y )
    if not player then return end

    local weapon = WeaponHoundShot

    AudioManager:Play( weapon.SfxShot )

    local lx, ly = player:GetLook()

    ShotManager:Add( player, x, y, 0, -1, 0, weapon.Damage, weapon.Type, weapon.LifeTime, weapon.Steps, false, weapon.Elements )
end

function LobsterShot( x, y )
    if not player then return end

    local weapon = WeaponLobsterShot

    AudioManager:Play( weapon.SfxShot )

    local lx, ly = player:GetLook()

    ShotManager:Add( player, x, y, 0, -1, 0, weapon.Damage, weapon.Type, weapon.LifeTime, weapon.Steps, false, weapon.Elements )
end

function SowerEggShot( entity, x, y )
    local weaponType = 23
    EntityFireWeapon( weaponType, entity, x, y )
    local weapon = Weapons[weaponType]
    ShakePower = math.max( weapon.Shake, ShakePower )
end

function IsRailgun()
    return Weapons[WeaponSelected] == WeaponRailgun
end

GameManager:SetShieldTime( 5 )

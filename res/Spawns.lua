TargetIncrease = 1
TargetMonsters = 10 * LevelSurvivalModifier
local ticks = 0

ClawMsg( "Survival target: " .. TargetMonsters .. ", increase: " .. TargetIncrease )

function SurvivalSpawnTick()
    local count = EntityManager:Count()
    local px, py = player:GetPos()

    ticks = ticks + 1
    if ticks >= 15 * 60 then
        TargetMonsters = TargetMonsters + TargetIncrease
        TargetIncrease = TargetIncrease + 0.2 * LevelSurvivalModifier
        ticks = 0

        ClawMsg( "Survival target: " .. TargetMonsters .. ", increase: " .. TargetIncrease )
    end

    while count < TargetMonsters do
        local vx = 0
        local vy = 512
        vx, vy = Rotate( vx, vy, math.random() * math.pi * 2 )
        local x = px + vx
        local y = py + vy

        if not GameManager:CheckObstacleCollision( x, y ) then
            local et
            local rnd = math.random()
            for i=1,#LevelSurvivalSet do
                if rnd < LevelSurvivalSet[i].weight then
                    EntityManager:Add( px + vx, py + vy, LevelSurvivalSet[i].id )
                    count = count + 1
                    break
                end
            end
        end
    end
end

PickupTimerTime = 60 * 10
local PickupTimer = 0
local WeaponPickupTimer = 0

WeaponPickups = {}
WeaponPickups[PickupType.ElectricityGun] = 8
WeaponPickups[PickupType.Flamer] = 7
WeaponPickups[PickupType.Minigun] = 5
WeaponPickups[PickupType.PlasmaGun] = 1
WeaponPickups[PickupType.Railgun] = 9
WeaponPickups[PickupType.RocketLauncher] = 6
WeaponPickups[PickupType.Shotgun] = 2
WeaponPickups[PickupType.CombatShotgun] = 3
WeaponPickups[PickupType.SMG] = 4
WeaponPickups[PickupType.Ripper] = 10
WeaponPickups[PickupType.Spiral] = 11
WeaponPickups[PickupType.Vortex] = 20
WeaponPickups[PickupType.Chainsaw] = 26
WeaponPickups[PickupType.LineGun] = 27
WeaponPickups[PickupType.Magnum] = 28
WeaponPickups[PickupType.Nailer] = 29
WeaponPickups[PickupType.MechFlamer] = 30
WeaponPickups[PickupType.MechRocket] = 31

BoughtPickups = {}
local ShopItems = {}
ShopItems[ShopItem.SMG]             = {PickupType.SMG}
ShopItems[ShopItem.Electricity]     = {PickupType.ElectricityGun}
ShopItems[ShopItem.Flamer]          = {PickupType.Flamer}
ShopItems[ShopItem.Chaingun]        = {PickupType.Minigun}
ShopItems[ShopItem.Pistol]          = {PickupType.PlasmaGun}
ShopItems[ShopItem.Railgun]         = {PickupType.Railgun}
ShopItems[ShopItem.RocketLauncher]  = {PickupType.RocketLauncher}
ShopItems[ShopItem.Ripper]          = {PickupType.Ripper}
ShopItems[ShopItem.CombatShotgun]   = {PickupType.CombatShotgun}
ShopItems[ShopItem.Shotgun]         = {PickupType.Shotgun}
ShopItems[ShopItem.Spiral]          = {PickupType.Spiral}
ShopItems[ShopItem.Vortex]          = {PickupType.Vortex}
ShopItems[ShopItem.Chainsaw]        = {PickupType.Chainsaw}
ShopItems[ShopItem.LineGun]         = {PickupType.LineGun}
ShopItems[ShopItem.Magnum]          = {PickupType.Magnum}
ShopItems[ShopItem.Nailer]          = {PickupType.Nailer}
ShopItems[ShopItem.Mech]            = {PickupType.MechFlamer, PickupType.MechRocket}

local idx = 1
while true do
    local id = registry:Get( "/monstaz/weaponselection/" .. idx )
    if id == "" then
        break
    elseif id ~= nil then
        table.insert( BoughtPickups, ShopItems[id] )
    end
    idx = idx + 1 
end

local PickupSpawnList = {
    PickupType.Health,
    PickupType.WeaponBoost
}

function SurvivalPickupSpawnTick()
    if not player then return end

    local px, py = player:GetPos()
    local dist = 100
    local dist2 = dist*dist

    PickupTimer = PickupTimer - 1
    if PickupTimer <= 0 then
        PickupTimer = PickupTimerTime + PickupTimerTime * 0.1 * math.random()
        local done = false
        while not done do
            local x = 10 + math.random( MapWidth - 20 )
            local y = 10 + math.random( MapHeight - 20 )
            local dx = px - x
            local dy = py - y
            if not GameManager:CheckObstacleCollision( x, y ) and DotProduct( dx, dy, dx, dy ) > dist2 then
                done = true
                PickupManager:Add( x, y, PickupSpawnList[math.random( #PickupSpawnList )], 15 + 1.5 * math.random() )
            end
        end
    end

    --[[
    WeaponPickupTimer = WeaponPickupTimer - 1
    if WeaponPickupTimer <= 0 then
        WeaponPickupTimer = PickupTimerTime + PickupTimerTime * 0.1 * math.random()
        local done = false
        while not done do
            local x = 10 + math.random( MapWidth - 20 )
            local y = 10 + math.random( MapHeight - 20 )
            local dx = px - x
            local dy = py - y
            if not GameManager:CheckObstacleCollision( x, y ) and DotProduct( dx, dy, dx, dy ) > dist2 then
                done = true
                PickupManager:Add( x, y, BoughtPickups[math.random( #BoughtPickups )], 15 + 1.5 * math.random() )
            end
        end
    end
    ]]
end

function Spawn( id, num, entities )
    local x, y, r = Map:GetSpawnData( id )
    local ents = {}

    for i=1,num do
        local l = math.random() * r
        local a = math.random() * 2 * math.pi
        local s = math.sin( a )
        local c = math.cos( a )

        local e = EntityManager:Add( x + s * l, y + c * l, entities[math.random( #entities )] )
        table.insert( ents, e )
    end

    return ents
end

function SpawnFriend( id, entities )
    local x, y, r = Map:GetSpawnData( id )

    local l = math.random() * r
    local a = math.random() * 2 * math.pi
    local s = math.sin( a )
    local c = math.cos( a )

    local e = EntityManager:Add( x + s * l, y + c * l, entities[math.random( #entities )] )
    e:SetClass( EntityClass.Friendly )

    return e
end

function SpawnDelayed( id, num, entities, items )
    local x, y, r = Map:GetSpawnData( id )

    if items and #items > num then
        ClawMsg( "More items requested than there are monsters available." )
        return
    end

    local itable = {}
    if items then
        local ifree = {}
        for i=1,num do
            table.insert( itable, nil )
            table.insert( ifree, i )
        end
        for _,v in ipairs( items ) do
            local idx = math.random( #ifree )
            itable[ifree[idx]] = v
            table.remove( ifree, idx )
        end
    end

    for i=1,num do
        local l = math.random() * r
        local a = math.random() * 2 * math.pi
        local s = math.sin( a )
        local c = math.cos( a )

        EntityManager:AddDelayed( x + s * l, y + c * l, entities[math.random( #entities )], itable[i] )
    end
end


function SpawnPickup( id, num, life, liferandom, pickups )
    local x, y, r = Map:GetSpawnData( id )

    for i=1,num do
        local l = math.random() * r
        local a = math.random() * 2 * math.pi
        local s = math.sin( a )
        local c = math.cos( a )

        PickupManager:Add( x + s * l, y + c * l, pickups[math.random( #pickups )], life + math.random() * liferandom )
    end
end

function SpawnEntity( id, x, y )
    EntityManager:Add( x, y, id )
end

function SpawnEntityDelayed( id, x, y )
    EntityManager:AddDelayed( x, y, id, nil )
end

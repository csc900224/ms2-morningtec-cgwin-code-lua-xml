local iconFile = "menu2/cat_02.png@linear"

function FillTimeoutIndicator( parent , width , height )

    local verticalSpace = R(5)

    local rootTimeout = parent:GetControl( "TimeoutRoot")

    local icon = rootTimeout:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconFile )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true ) 

    rootTimeout:SetX( width/2 )
    rootTimeout:SetY( height/2 - h/2 )


    local refreshInfo = rootTimeout:GetControl("RefreshInfo")
    refreshInfo:SetY( h/2 + verticalSpace )
    refreshInfo:SetX( - refreshInfo:GetWidth()/2 )
    local refresh = rootTimeout:GetControl("Refresh")
    local refW, refH = refresh:GetControl("Button"):GetGraphic( "TS" ):GetSize()
    refresh:SetY( h/2 + 3*verticalSpace + refreshInfo:GetHeight()  )
    refresh:SetX( -refW/2)

end
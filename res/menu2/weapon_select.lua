require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/shop/shop_item_list.lua" )
require( "menu2/popup_settings.lua" )
require( "ItemDB.lua" )

WeaponSelectActive = false
local popup = nil
local buttons = nil
local top = nil
local root = nil
local buttonsW = 0
local buttonsH = 0
local buttonsY = 0
local topH = 0
local topW = 0

local x = 0
local y = 0
local height = 0
local width = 0

local itemList = nil

local savePath = nil
local slot = 1

local Constants = {
    widthRatio = 0.9,
    heightRatio = 0.95,
    yOffset = R( 50 ),
    buttonsYOffset = -R(4),
    itemListHorizontalMargin = R(15),
    itemListVerticalMargin = R(20),
    itemListYOffset = R(5),
    settingsBtnMargin = R(5)
}

function WeaponSelectInit( parent )
    -- Init
    parent:InsertFromXml( "menu2/weapon_select.xml" )
    popup = parent:GetControl( "WeaponSelect" )

    -- Buttons pos
    buttons = popup:GetControl( "Buttons" )
    buttonsW, buttonsH = callback:GetControlSize( popup:GetPath() .. "/Buttons" )
    buttons:SetX( (SCREEN_WIDTH - buttonsW) /2 )
    buttons:SetY( SCREEN_HEIGHT )
    buttonsY = SCREEN_HEIGHT - buttonsH + Constants.buttonsYOffset

    -- Popup size and pos
    width = SCREEN_WIDTH * Constants.widthRatio
    height = (buttonsY - Constants.yOffset) * Constants.heightRatio
    x = ( SCREEN_WIDTH - width )/2
    y = Constants.yOffset + (buttonsY - Constants.yOffset - height) / 2
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    -- Create Frame
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height, PopupFrameBlue, true )

    -- Title
    local title = popup:GetControl( "Title" )
    titleW, titleH = callback:GetControlSize( popup:GetPath() .. "/Title" )
    title:SetX( (width - titleW) /2 )

    -- Item list size
    local itemsW = width - 2*Constants.itemListHorizontalMargin
    local itemsH = height - 2*Constants.itemListVerticalMargin
    local itemsRoot = popup:GetControl( "ItemsRoot" )

    -- Create item list
    itemList = ShopItemList:new()
    itemList:Init( itemsRoot, ShopItemListMode.weaponSelect, itemsW, itemsH, height )
    itemList:SetSelectCallback( WeaponSelectSelected )

    -- Item list pos
    local itemW, itemH = itemList:GetItemSize()
    itemsRoot:SetX( (width - itemsW)/2 )
    itemsRoot:SetY( (height - itemsH)/2 + (itemsH - itemH) /2 + Constants.itemListYOffset)

    -- Gradients for item list
    local gradientL = popup:GetControl( "GradientL" )
    local gradeintW, gradientH = gradientL:GetGraphic( "TS" ):GetSize()
    local gradientScale = itemH/gradientH
    gradientL:GetGraphic( "TS" ):SetScale( 1, gradientScale )
    gradientL:SetX( itemsRoot:GetX() )
    gradientL:SetY( itemsRoot:GetY() )
    local gradientR = popup:GetControl( "GradientR" )
    gradientR:GetGraphic( "TS" ):SetScale( 1, gradientScale )
    gradientR:SetX( itemsRoot:GetX() +  itemsW - gradeintW )
    gradientR:SetY( itemsRoot:GetY() )

    -- Top section
    top = popup:GetControl( "Top" )
    topW, topH = callback:GetControlSize( popup:GetPath() .. "/Top" )
    top:SetY( -topH )
    
    -- Settings buttons
    local settingsButton = top:GetControl( "SettingsButton" )
    local settingsW, settingsH = settingsButton:GetGraphic( "TS" ):GetSize()
    settingsButton:SetX( SCREEN_WIDTH - settingsW - Constants.settingsBtnMargin )

    popup:SetVisibility( false )
end

function WeaponSelectUpdate( dt )
    if not WeaponSelectActive then return end
    ProcessTransactionNotifications( dt )
    itemList:Update( dt )
    CountersUpdate( dt )
end

function WeaponSelectTouchUp( x, y, id )
    if not WeaponSelectActive then return end
    local c = screen:GetTouchableControl( x, y )
    itemList:TouchUp( x, y, id, c )
    if not c then return end
    
    local path = c:GetPath()
    WeaponSelectAction( c, path )
end

function WeaponSelectTouchDown( x, y, id )
    if not WeaponSelectActive then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    itemList:TouchDown( x, y, id, c )
    
    local path = c:GetPath()
    WeaponSelectPress( c, path )
end

function WeaponSelectTouchMove( x, y, id )
    if not WeaponSelectActive then return end
    itemList:TouchMove( x, y, id )
end

function WeaponSelectKeyDown( code )
    if not WeaponSelectActive then
        return
    end

    if code == keys.KEY_ESCAPE then
        WeaponSelectBack()
        return true
    end

    return false
end

function WeaponSelectPress( control, path )
    if not WeaponSelectActive then
        return
    end

    if string.find( path, "/Buttons/" ) or string.find( path, "/Top/SettingsButton") then
        ButtonPress( control )
    else
        SettingsPopupPress( control, path )
    end
end

function WeaponSelectAction( control, name )
    if not WeaponSelectActive then
        return
    end

    if string.find(name, "WeaponSelect/Buttons/Back") then
        TutorialAction( name )
        WeaponSelectBack()
    elseif string.find(name, "/Top/SettingsButton") then
        SettingsPopupShow()
    else
        SettingsPopupAction( control, name )
    end
end

function WeaponSelectBack()
    WeaponSelectHide()
    anim:Add( Delay:New( 0.15 ),
        function()
            PremissionShow( true )
        end
    )
end

function WeaponSelectShow( excludeList, saveRegPath, slotIdx, reopen )
    popup:SetVisibility(true)
    
    if reopen ~= true then
        savePath = saveRegPath
        slot = slotIdx
        --PopupLock()
        
        itemList:RefreshList()
        itemList:ResetScroll()

        -- Set final item list
        local finalList = {}
        if excludeList == nil or #excludeList == 0 then
            finalList = ItemsCategory[ItemCategory.weapons]
        else
            for _, item in ipairs( ItemsCategory[ItemCategory.weapons] ) do
                local excluded = false
                for __, exclude in ipairs( excludeList ) do
                    if item.id == exclude then
                        excluded = true
                        break
                    end
                end
                
                if not excluded then
                    table.insert( finalList, item )
                end
            end
        end
        itemList:SetContent( finalList )
    else
        itemList:RefreshList()
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
            top:SetY( -topH + SmoothStep( t ) * topH )
        end
        ),
        function()
            WeaponSelectActive = true
            anim:Add( Slider:New( 0.15,
                function( t )
                    buttons:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - buttonsY ) )
                end
                ),
                function()
                    TutorialOnWeaponSelection()
                end
            )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function WeaponSelectHide( callback )
    if not WeaponSelectActive then return end
    WeaponSelectActive = false

    anim:Add( Slider:New( 0.15,
        function( t )
            buttons:SetY( buttonsY + SmoothStep( t ) * ( SCREEN_HEIGHT - buttonsY ) )
        end
        ),
        function()
            --PopupUnlock()
            anim:Add( Slider:New( 0.3,
                function( t )
                    popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
                    top:SetY( SmoothStep( t ) * -topH )
                end
                ),
                function()
                    popup:SetVisibility( false )
                    if callback then
                        callback()
                    end
                end
            )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function WeaponSelectSelected( itemId )
    if not WeaponSelectActive then
        return
    end

    WeaponSelectBack()
    if savePath then
        local weapon = ItemsById[itemId]
        if weapon.exclusive == true then
            registry:Set( savePath .. 1, itemId )
            registry:Set( savePath .. 2, "" )
        else 
            if slot > 1 then
                local slot1ItemIdx = registry:Get( savePath .. 1 )
                if ItemsById[slot1ItemIdx].exclusive == true then
                    slot = 1
                end
            end
            registry:Set( savePath .. slot, itemId )
        end
    end
end
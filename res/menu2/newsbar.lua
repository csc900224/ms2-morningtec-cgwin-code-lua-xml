require( "menu2/animation.lua" )
require( "menu2/common.lua" )

local newsbar = nil

local counter = 0
local content = {}

local events = {
    { msg = "TEXT_NEWSBAR_EVENT_1", active = function() return true end }
}

function NewsbarInit( parent )
    parent:InsertFromXml( "menu2/newsbar.xml" )
    newsbar = parent:GetControl( "Newsbar" )

    NewsbarCheckEvents()
    NewsbarFill()

    do
        local lt = 0
        anim:Add(
            Perpetual:New(
                function( t )
                    local dt = t - lt
                    lt = t

                    NewsbarUpdate( dt )
                end
            )
        )
    end
end

function NewsbarUpdate( dt )
    local remove = 0
    for i,c in pairs( content ) do
        local x = c:GetX() - dt * R(50)
        if x + c:GetTextWidth() < 0 then
            remove = remove + 1
            c:Remove()
        else
            c:SetX( x )
        end
    end

    for i = 1,remove do
        table.remove( content, 1 )
    end

    NewsbarFill()
end

function NewsbarFill()
    local offset = 0
    local n = #content
    if n > 0 then
        local c = content[n]
        offset = c:GetAbsX() + c:GetTextWidth()
    end

    while offset < SCREEN_WIDTH do
        local text = nil
        if counter % 2 == 1 then
            text = "TEXT_NEWSBAR_TIP_" .. math.random( 15 )
        else
            local pool = {}
            for i, event in ipairs( events ) do
                if event.active() then
                    table.insert( pool, event.msg )
                end
            end

            text = pool[math.random( #pool )]
        end

        local c = NewsbarAddMessage( screen:GetText( text ), offset )
        offset = offset + c:GetWidth()

        counter = counter + 1
    end
end

function NewsbarCheckEvents()
    local playerLevel = registry:Get( "/monstaz/player/level" )

    local bestOwnedWeaponId = ItemDbGetBestOwnedWeaponId()
    local bestOwnedWeaponLevel = ItemDbGetItemLevel( bestOwnedWeaponId )

    if playerLevel > bestOwnedWeaponLevel then
        local bestWeapon = nil
        local bestWeaponLevel = 0

        local weapons = ItemsCategory[ItemCategory.weapons]
        for i, weapon in ipairs( weapons ) do
            local weaponLevel = ItemDbGetItemLevel( weapon.id )
            if weaponLevel <= playerLevel and weaponLevel > bestOwnedWeaponLevel and weaponLevel > bestWeaponLevel then
                bestWeapon = weapon
                bestWeaponLevel = weaponLevel
            end
        end

        if bestWeapon then
            local message = screen:GetText( "TEXT_NEWSBAR_EVENT_4" )
            message = string.gsub( message, "%$%(level%)", playerLevel )
            message = string.gsub( message, "%$%(weapon%)", screen:GetText( bestWeapon.d ) )

            NewsbarAddEvent( { msg = message, active = function() return Shop:IsBought( bestWeapon.id ) == 0 end } )
        end
    end

end

function NewsbarAddEvent( event )
    if event.active == nil then
        event.active = function() return true end
    end

    table.insert( events, event )
end

function NewsbarAddMessage( message, offset )
    local c = newsbar:GetControl( "Content" ):InsertControl( "Text" .. counter, 0, "TextLine" )
    c:SetRelative( true )
    c:SetX( offset )

    c:AddRepresentation( "default", "menu2/text.xml" )
    c:SetTextWidth( 99999 )
    c:SetText( message .. " \194\172 " )

    table.insert( content, c )

    return c
end

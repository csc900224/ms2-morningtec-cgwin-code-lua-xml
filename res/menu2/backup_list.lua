require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/backup_item.lua" )
require( "menu2/backup_scroll_indicator.lua" )

local root = nil
local clip = nil
local width = nil
local height = nil
local items = nil
local currentCat = nil
local indicator = nil

BackupConsts = {
    itemHeight = R( 206 ),
    itemWidth = R( 130 ),
    itemSpacing = R(10),
    itemXOffset = R(5) 
}

local BackupScrollData = {
    touchId = -1,
    minScrollDist = R(5),
    maxScrollOffset = 0,
    scrollOffset = 0,
    sampleTouchPos = 0,
    currentTouchPos = 0,
    startScrollOffset = 0,
    touchStart = 0,
    scrolling = false,    
    targetScrollOffset = 0,
    scrollAcceleration = 1,
    samplingInterval = 0.025,
    sampleTimer = -1,
    velocityEpsilon = 0.1,
    velocity = 0,
    lastVelocity = 0,
    draggTime = 0
}

function BackupListInit( parent, listWidth, listHeight )
    parent:InsertFromXml( "menu2/backup_list.xml" )
    clip = parent:GetControl( "BackupList" )
    root = parent:GetControl( "BackupList/Backups" )
    indicator = parent:GetControl( "BackupScrollIndicatorRoot" )
    width = listWidth
    height = listHeight

    BackupConsts.itemHeight = listHeight

    BackupScrollIndicatorInit( indicator, 0, BackupConsts.itemWidth )
    BackupListCaluclateLayout()

    clip:GetGraphic( "r" ):SetWidth( width )
    clip:GetGraphic( "r" ):SetHeight( BackupConsts.itemHeight )

end

function BackupListCaluclateLayout()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    --BackupConsts.itemHeight = math.min( height - iH, BackupConsts.itemHeight)
    indicator:SetY( BackupConsts.itemHeight )
    indicator:SetX( (width - iW)/2 )
end

function BackupListUpdateIndicatorPos()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    indicator:SetX( (width - iW)/2 )
end

function BackupListSetContent( itemList, resetScroll )
    if resetScroll == nil or resetScroll == true then
        BackupListResetScroll()
    end
    BackupListDropAllItems()
    
    items = itemList
    local x = BackupConsts.itemXOffset
    local idx = 1
    for _, item in ipairs( itemList ) do
        local itemRoot = root:InsertControl( tostring( idx ) )
        idx=idx+1
        itemRoot:SetRelative( true )
        itemRoot:SetX( x )
        BackupItemInit( itemRoot, BackupConsts.itemWidth, BackupConsts.itemHeight, item )
        x = x + BackupConsts.itemWidth + BackupConsts.itemSpacing
    end
    BackupScrollData.maxScrollOffset = math.max( x - BackupConsts.itemSpacing + BackupConsts.itemXOffset - width, 0 )
    
    BackupScrollIndicatorUpdateMaxScrollOffset( BackupScrollData.maxScrollOffset )
    BackupListUpdateIndicatorPos()
    
    BackupListCreateClipBorder()
end

function BackupListResetScroll()
    BackupScrollData.scrollOffset = 0
    root:SetX( 0 )
    BackupScrollData.scrolling = false
    BackupScrollData.touchId = -1
    BackupScrollData.velocity = 0
    BackupScrollData.lastVelocity = 0
    BackupScrollData.sampleTimer = -1
    BackupScrollData.draggTime = 0
    BackupScrollData.targetScrollOffset = 0
    BackupScrollData.scrollOffset = 0
end

function BackupListUpdate( dt )
    if BackupScrollData.touchId >= 0 then
        BackupScrollData.draggTime = BackupScrollData.draggTime + dt
    end

    BackupListSampleVelocity( dt )
    BackupListUpdateScrolling( dt )
    
    BackupScrollIndicatorUpdateScrollOffset( BackupScrollData.scrollOffset )
    
    if items == nil then return end
    
    local itemIdx = 1
    for _, item in ipairs( root:GetChildren() ) do
    
        local tempCanHelp = tonumber( items[itemIdx].CanHelp )
        
        if tempCanHelp > 0 then
        
            local currentTimeLeft = tempCanHelp - TimeSinceUpdateDB
            
            if currentTimeLeft <= 0 then
                items[itemIdx].CanHelp = 0
            end
            
            BackupItemUpdate( item , items[itemIdx].UniqueId , currentTimeLeft )
        end
        itemIdx = itemIdx + 1
    end

end

function BackupListSampleVelocity( dt )
    if BackupScrollData.touchId >= 0 and BackupScrollData.sampleTimer >= 0 then
        BackupScrollData.sampleTimer = BackupScrollData.sampleTimer - dt
        if BackupScrollData.sampleTimer < 0 then
            BackupScrollData.sampleTimer = BackupScrollData.samplingInterval
            BackupScrollData.lastVelocity = (BackupScrollData.currentTouchPos - BackupScrollData.sampleTouchPos) / BackupScrollData.samplingInterval
            BackupScrollData.sampleTouchPos = BackupScrollData.currentTouchPos
        end
    end
end

function BackupListTouchDown( x, y, id, c )
    if items == nil then return end

    local path = c:GetPath()
    if string.find( path, "/BackupList" ) then

        if BackupScrollData.touchId == id then
            BackupListTouchUp( BackupScrollData.currentTouchPos, y, id, nil )
        end

        if BackupScrollData.touchId < 0 then
            BackupScrollData.touchId = id
            BackupScrollData.touchStart = x
            BackupScrollData.scrolling = false
            BackupScrollData.velocity = 0
            BackupScrollData.lastVelocity = 0
            BackupScrollData.sampleTimer = BackupScrollData.samplingInterval
            BackupScrollData.currentTouchPos = x
            BackupScrollData.sampleTouchPos = x
            BackupScrollData.draggTime = 0
        end
    end
    
end

function BackupListTouchUp( x, y, id, c )
    if items == nil then return end

    if BackupScrollData.scrolling and id == BackupScrollData.touchId then
        BackupScrollData.scrolling = false
        BackupScrollData.touchId = -1
        BackupScrollData.sampleTimer = -1
        BackupScrollData.targetScrollOffset = BackupItemClampOffset( BackupScrollData.startScrollOffset + BackupScrollData.touchStart - x )

        -- Check if dragg time is shorter than sampling rate. If so recalculate velocity manualny
        if BackupScrollData.draggTime < BackupScrollData.samplingInterval and BackupScrollData.draggTime > 0 then
            BackupScrollData.lastVelocity = (x - BackupScrollData.sampleTouchPos) / BackupScrollData.draggTime
        end
        BackupScrollData.velocity = BackupScrollData.lastVelocity
    else
        if not c then return end
        local path = c:GetPath()
        if string.find( path, "/Buttons" ) then
            ButtonPress( c )
            BackupItemAction( c,path )
        end
    end
end

function BackupListTouchMove( x, y, id, c )
    if items == nil then return end

    if id == BackupScrollData.touchId then
        if not BackupScrollData.scrolling then
            if math.abs( BackupScrollData.touchStart - x ) > BackupScrollData.minScrollDist then
                -- Start real scrolling
                BackupScrollData.touchStart = x
                BackupScrollData.startScrollOffset = BackupScrollData.scrollOffset
                BackupScrollData.scrolling = true
            end
        else
            BackupScrollData.targetScrollOffset = BackupItemClampOffset( BackupScrollData.startScrollOffset + BackupScrollData.touchStart - x )
            BackupScrollData.currentTouchPos = x
        end    
    end
end

function BackupItemClampOffset( offset )
    return math.min( math.max(offset, 0), BackupScrollData.maxScrollOffset )
end

function BackupListUpdateScrolling( dt )
    if math.abs(BackupScrollData.velocity) > BackupScrollData.velocityEpsilon then
        BackupScrollData.velocity = BackupScrollData.velocity - BackupScrollData.velocity * BackupScrollData.scrollAcceleration * dt
        BackupScrollData.targetScrollOffset = BackupItemClampOffset( BackupScrollData.targetScrollOffset - BackupScrollData.velocity * dt )
    end
    
    if math.abs(BackupScrollData.scrollOffset - BackupScrollData.targetScrollOffset) > BackupScrollData.velocityEpsilon then
        BackupScrollData.scrollOffset = BackupScrollData.targetScrollOffset
        root:SetX( -BackupScrollData.scrollOffset )
    end  
    
    BackupListClipItems()
end

function BackupListClipItems()
    for _, item in ipairs( root:GetChildren() ) do
        local visible = -BackupScrollData.scrollOffset + item:GetX() <= width and -BackupScrollData.scrollOffset + item:GetX() + BackupConsts.itemWidth >= 0
        item:SetVisibility( visible )
    end
end

function BackupListDropAllItems()
    for _, item in ipairs( root:GetChildren() ) do
        item:Remove()
    end
end

function BackupListCreateClipBorder()
    local clipMargin = R(20)
    local ls = clip:InsertControl("ls")
    ls:AddRepresentation( "default", "menu2/timage.xml" )
    ls:SetRelative( true )
    ls:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    ls:SetY( - clipMargin )
    ls:SetX( - R( 2 ) )
    
    local lsw, lsh = ls:GetGraphic( "TS" ):GetSize()

    ls:GetGraphic( "TS" ):SetScale( 1 , ( height + 2 * clipMargin ) / lsh  )


    local rs = clip:InsertControl("rs")
    rs:AddRepresentation( "default", "menu2/timage.xml" )
    rs:SetRelative( true )
    rs:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    rs:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    rs:GetGraphic( "TS" ):SetScale( 1 , ( height + 2 * clipMargin ) / lsh  )
    rs:SetX( width - lsw + R(2) )
    rs:SetY( - clipMargin )
end

function BackupListHidePosIndicator()
    indicator:SetVisibility( false )
end

function BackupListShowPosIndicator()
    indicator:SetVisibility( true )
end

function OnBackupItemCantAfford( itemId )
    ShopNoCashPopupShow( nil, nil , function() BackupTeamShow() end )
end

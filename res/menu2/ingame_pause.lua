require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/counters.lua" )
require( "menu2/shop/shop_item_list.lua" )
require( "menu2/popup_settings.lua" )
require( "ItemDB.lua" )

local active = false
local popup = nil
local buttons = nil
local top = nil
local root = nil
local buttonsW = 0
local buttonsH = 0
local buttonsY = 0
local topH = 0
local topW = 0
local itemsRoot = nil

local x = 0
local y = 0
local height = 0
local width = 0

local itemList = nil
local forceUpdateCounters = false

local Constants = {
    widthRatio = 0.9,
    heightRatio = 0.95,
    yOffset = R( 50 ),
    buttonsYOffset = -R(4),
    itemListHorizontalMargin = R(15),
    itemListVerticalMargin = R(15),
    settingsBtnMargin = R(5)
}

local KeyHandlers = {
    SettingsPopupKeyDown
}

function PauseMenuInit( parent )
    -- Init
    parent:InsertFromXml( "menu2/ingame_pause.xml" )
    popup = parent:GetControl( "PausePopup" )

    -- Buttons pos
    buttons = popup:GetControl( "Buttons" )
    buttonsW, buttonsH = callback:GetControlSize( popup:GetPath() .. "/Buttons" )
    buttons:SetX( (SCREEN_WIDTH - buttonsW) /2 )
    buttons:SetY( SCREEN_HEIGHT )
    buttonsY = SCREEN_HEIGHT - buttonsH + Constants.buttonsYOffset

    -- Create item list
    itemList = ShopItemList:new()
    local itemW, itemH = itemList:GetItemSize()
    local itemSpacing = itemList:GetItemSpacing()
    
    -- Popup size and pos
    local maxWidth = #QuickShopItems * (itemW + itemSpacing) - itemSpacing + 2*Constants.itemListHorizontalMargin
    width = math.min( SCREEN_WIDTH * Constants.widthRatio, maxWidth )
    height = (buttonsY - Constants.yOffset) * Constants.heightRatio
    x = ( SCREEN_WIDTH - width )/2
    y = Constants.yOffset + (buttonsY - Constants.yOffset - height) / 2
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    -- Create Frame
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height, PopupFrameBlue, true )

    -- Item list size
    local itemsW = width - 2*Constants.itemListHorizontalMargin
    local itemsH = height - 2*Constants.itemListVerticalMargin
    itemsRoot = popup:GetControl( "ItemsRoot" )

    -- Init item list
    itemList:Init( itemsRoot, ShopItemListMode.quickShop, itemsW, itemsH, height )
    itemList:SetContent( QuickShopItems )

    -- Item list pos
    itemW, itemH = itemList:GetItemSize()
    itemsRoot:SetX( (width - itemsW)/2 )
    itemsRoot:SetY( (height - itemsH)/2 + (itemsH - itemH) /2 )

    -- Gradients for item list
    local gradientL = popup:GetControl( "GradientL" )
    local gradeintW, gradientH = gradientL:GetGraphic( "TS" ):GetSize()
    local gradientScale = itemH/gradientH
    gradientL:GetGraphic( "TS" ):SetScale( 1, gradientScale )
    gradientL:SetX( itemsRoot:GetX() )
    gradientL:SetY( itemsRoot:GetY() )
    local gradientR = popup:GetControl( "GradientR" )
    gradientR:GetGraphic( "TS" ):SetScale( 1, gradientScale )
    gradientR:SetX( itemsRoot:GetX() +  itemsW - gradeintW )
    gradientR:SetY( itemsRoot:GetY() )

    -- Top section
    top = popup:GetControl( "Top" )
    CountersInit( top:GetControl( "Counters") )
    CountersButtonsEnable( false )
    topW, topH = callback:GetControlSize( popup:GetPath() .. "/Top" )
    top:SetY( -topH )

    -- Settings buttons
    local settingsButton = top:GetControl( "SettingsButton" )
    local settingsW, settingsH = settingsButton:GetGraphic( "TS" ):GetSize()
    settingsButton:SetX( SCREEN_WIDTH - settingsW - Constants.settingsBtnMargin )

    if TutorialActive then
        buttons:GetControl( "Quit" ):SetVisibility( false )
        top:GetControl( "MissionsButton" ):SetVisibility( false )
    end

    popup:SetVisibility( false )
end

function PauseUpdate( dt )
    if active or forceUpdateCounters then
        CountersUpdate( dt )
    end
    if not active then return end
    ProcessTransactionNotifications( dt )
    itemList:Update( dt )
end

function PauseKeyDown( code )
    if not active then
        return false
    end

    if TutorialActive then
        return false
    end

    for i, handler in pairs( KeyHandlers ) do
        if handler( code ) then
            return true
        end
    end

    if code == keys.KEY_ESCAPE then
        PauseMenuHide( function() callback:StopPerkMenu() end )
        return true
    end

    return false
end

function PauseTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    itemList:TouchUp( x, y, id, c )
    if not c then return end
    
    local path = c:GetPath()
    PauseMenuAction( c, path )
end

function PauseTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    itemList:TouchDown( x, y, id, c )
    
    local path = c:GetPath()
    PauseMenuPress( c, path )
end

function PauseTouchMove( x, y, id )
    if not active then return end
    itemList:TouchMove( x, y, id )
end

function PauseMenuPress( control, path )
    if not active then
        return
    end

    if string.find( path, "/PausePopup/Buttons/" ) or 
       string.find( path, "/PausePopup/Top/SettingsButton") or 
       string.find( path, "/PausePopup/Top/MissionsButton") then
        ButtonPress( control )
    else
        SettingsPopupPress( control, path )
    end
end

function PauseMenuAction( control, name )
    if not active then
        return
    end

    if string.find(name, "/PausePopup/Buttons/Resume") then
        TutorialAction( name )
        PauseMenuHide( function() callback:StopPerkMenu() end )
    elseif string.find(name, "/PausePopup/Buttons/Quit") then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_LEVEL_FINISHED, GameEventParam.GEP_LEVEL_FINISHED_ABORT )
        PauseMenuHide( function() callback:StopSummary() end )
    elseif string.find(name, "/PausePopup/Top/SettingsButton") then
        SettingsPopupShow()
    elseif string.find(name, "/PausePopup/Top/MissionsButton") then
        MissionsShow()
    else
        SettingsPopupAction( control, name )
    end
end

function PauseMenuShow( lvl )
    Lock()

    active = true
    PopupLock( nil, "/PauseOverlay" )

    popup:SetVisibility(true)
    itemList:RefreshList()
    itemList:ResetScroll( TutorialActive and R(0) or R(26) )
    
    MissionsUpdateToClaimCounter( top:GetPath() .. "/MissionsButton/ToClaim" )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
            top:SetY( -topH + SmoothStep( t ) * topH )
        end
        ),
        function()
            anim:Add( Slider:New( 0.15,
                function( t )
                    buttons:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - buttonsY ) )
                end
                ),
                function()
                    TutorialOnPauseMenu()
                    Unlock()
                end
            )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function PauseMenuHide( callback )
    Lock()

    anim:Add( Slider:New( 0.15,
        function( t )
            buttons:SetY( buttonsY + SmoothStep( t ) * ( SCREEN_HEIGHT - buttonsY ) )
        end
        ),
        function()
            PopupUnlock( nil, "/PauseOverlay" )
            anim:Add( Slider:New( 0.3,
                function( t )
                    popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
                    top:SetY( SmoothStep( t ) * -topH )
                end
                ),
                function()
                    active = false
                    popup:SetVisibility( false )
                    if callback then
                        callback()
                    end

                    Unlock()
                end
            )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function PauseMenuMissionsShow( lvl )
    active = true
    forceUpdateCounters = false
    itemsRoot:SetVisibility(true)
    
    MissionsUpdateToClaimCounter( top:GetPath() .. "/MissionsButton/ToClaim" )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
            top:GetControl("MissionsButton"):SetAlpha( 255 * SmoothStep( t ) )
        end
        ),
        function()
            anim:Add( Slider:New( 0.15,
                function( t )
                    buttons:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - buttonsY ) )
                end
                )
            )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function PauseMenuMissionsHide( callback )
    anim:Add( Slider:New( 0.15,
        function( t )
            buttons:SetY( buttonsY + SmoothStep( t ) * ( SCREEN_HEIGHT - buttonsY ) )
        end
        ),
        function()
            anim:Add( Slider:New( 0.3,
                function( t )
                    popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
                    top:GetControl("MissionsButton"):SetAlpha( 255 - SmoothStep( t ) * 255 )
                end
                ),
                function()
                    active = false
                    forceUpdateCounters = true;
                    itemsRoot:SetVisibility( false )
                    if callback then
                        callback()
                    end
                end
            )
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function OnMissionCompleted( missionPath )
    MissionsUpdateToClaimCounter( top:GetPath() .. "/MissionsButton/ToClaim" )
end

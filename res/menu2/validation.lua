local passMinLength = 5
local emailMinLength = 5
local userIdLength = 9
local nameMinLength = 5


function IsEmailValid( email )
    if email == nil then return false end
    
    local length = callback:GetInputLenght( email )--string.len( email ) 
    
    if length < emailMinLength then return false end

    if string.find(email, " " ) then return false end
    
    if not (email:match("[A-Za-z0-9%.%%%+%-]+@[A-Za-z0-9%.%%%+%-]+%.%w%w%w?%w?")) then return false end
    
    return true
end

function IsPassValid( pass )

    if pass == nil then return false end
    
    local length = callback:GetInputLenght( pass )-- string.len( pass ) 
    
    if length < passMinLength then return false end

    if string.find(pass, " " ) then return false end
    
    return true
end

function IsNameValid( name )
    
    if name == nil then return false end

    local length = callback:GetInputLenght( name )--string.len( name ) 
    
    if length < nameMinLength then return false end

    if string.find(name, " " ) then return false end
    
    return true
end

function IsNameTaken( name )
    if string.lower( name ) == string.lower( BotName ) or string.lower( name ) == string.lower( GamelionUserName ) then return true end
    return false
end

function IsUserIdValid( userId )
    if userId == nil then return false end


    local length = callback:GetInputLenght( userId )--string.len( userId ) 
    local paternUserId = "%d%d%d%d%d%d%d%d%d"

   -- if not length == userIdLength then return false end

    if not ( length == userIdLength ) then return false end


    if tonumber( userId ) == nil then return false end

    if not string.find( userId, paternUserId ) then  return false end 

    if string.find( userId, " " ) then return false end

    return true
end

local updateInputText = nil
local emptyInputText = nil

local currInput = nil
local currInputType = nil

function SetInputTextToUpdate( control , emptyText , inputFieldType )
    
    --if updateInputText ~= nil then
        --StopCursorAnimation()
    --end
    
    updateInputText = control
    emptyInputText = emptyText
    currInputType = inputFieldType
end

function UpdateInput( text )

    currInput = text
    updateInputText:SetVisibility( false )
    
    if text == nil or string.len( text ) == 0 then
        --updateInputText:SetText( emptyInputText )

        if currInputType == InputFieldsType.username then 
            InputFieldsText.username = emptyInputText
        elseif currInputType == InputFieldsType.password then
            InputFieldsText.password = emptyInputText
        elseif currInputType == InputFieldsType.email then
                InputFieldsText.email = emptyInputText		
        elseif currInputType == InputFieldsType.inviteId then
            InputFieldsText.inviteId = emptyInputText		
        elseif currInputType == InputFieldsType.passwordVerify then
            InputFieldsText.passwordVerify = emptyInputText
        end
        return
    end

    if currInputType == InputFieldsType.username then 
        InputFields.username = text
        InputFieldsText.username = text
        --updateInputText:SetText( text ) 
    elseif currInputType == InputFieldsType.password then
        InputFields.password = text
        local charCount = callback:GetInputLenght( text ) --string.len( text )
        local mask = string.rep("*",charCount)
        --updateInputText:SetText( mask ) 
        InputFieldsText.password = mask
    elseif currInputType == InputFieldsType.email then
        InputFields.email = text
        --updateInputText:SetText( text )
        InputFieldsText.email = text
    elseif currInputType == InputFieldsType.inviteId then
        InputFields.inviteId = text
        --updateInputText:SetText( text )
        InputFieldsText.inviteId = text
    elseif currInputType == InputFieldsType.passwordVerify then
        InputFields.passwordVerify = text
        local charCount = callback:GetInputLenght( text ) --string.len( text )
        local mask = string.rep("*",charCount)
        --updateInputText:SetText( mask ) 
        InputFieldsText.passwordVerify = mask
    end
    
    --updateInputText:SetTextWidth( updateInputText:GetTextWidth() )

end

local cursorAlpha = 0
local blinkSign ="_"

function StartCursorAnimation()

        if updateInputText ~= nil then
            local tempText = nil
            anim:Add( Perpetual:New(
                    function( t )
                        local sin = math.sin(t * 8)
                        local cursorAlpha = 80 +  50* (1 + sin)


                        if ( currInputType == InputFieldsType.passwordVerify or currInputType == InputFieldsType.password ) then
                            local charCount = callback:GetInputLenght( currInput ) --string.len( currInput )
                            tempText = string.rep("*",charCount) 
                        else
                            tempText = currInput
                        end
                    
            if cursorAlpha > 128 then 
                --updateInputText:SetText( tempText .. "_" )

                if currInputType == InputFieldsType.username then 
                    InputFieldsText.username = tempText .. blinkSign
                elseif currInputType == InputFieldsType.password then
                    InputFieldsText.password = tempText .. blinkSign
                elseif currInputType == InputFieldsType.email then
                    InputFieldsText.email = tempText .. blinkSign
                elseif currInputType == InputFieldsType.inviteId then
                    InputFieldsText.inviteId = tempText .. blinkSign
                elseif currInputType == InputFieldsType.passwordVerify then
                    InputFieldsText.passwordVerify = tempText .. blinkSign
                end
            else
                --updateInputText:SetText( tempText )

                if currInputType == InputFieldsType.username then 
                    InputFieldsText.username = tempText 
                elseif currInputType == InputFieldsType.password then
                    InputFieldsText.password = tempText
                elseif currInputType == InputFieldsType.email then
                    InputFieldsText.email = tempText	
                elseif currInputType == InputFieldsType.inviteId then
                    InputFieldsText.inviteId = tempText	
                elseif currInputType == InputFieldsType.passwordVerify then
                    InputFieldsText.passwordVerify = tempText
                end
            end
                    end
                ),
                nil,
                updateInputText
            )
            
        end
    
end

function StopCursorAnimation()

    anim:RemoveAll( updateInputText )
    
    local tempText = nil
    
    if ( currInputType == InputFieldsType.passwordVerify or currInputType == InputFieldsType.password ) then
        local charCount = callback:GetInputLenght(currInput)--string.len( currInput )
        tempText = string.rep("*",charCount) 
    else
        tempText = currInput
    end

    if tempText == nil or string.len( tempText ) == 0 then
        --updateInputText:SetText( emptyInputText )
        tempText = emptyInputText
    else
        --updateInputText:SetText( tempText )
    end
    

    if currInputType == InputFieldsType.username then 
        InputFieldsText.username = tempText 
    elseif currInputType == InputFieldsType.password then
        InputFieldsText.password = tempText
    elseif currInputType == InputFieldsType.email then
        InputFieldsText.email = tempText	
    elseif currInputType == InputFieldsType.inviteId then
        InputFieldsText.inviteId = tempText	
    elseif currInputType == InputFieldsType.passwordVerify then
        InputFieldsText.passwordVerify = tempText
    end
    
    
end

function OnCloseVkb()
    if updateInputText ~= nil then
        StopCursorAnimation()
    end
end

function TimeFormat( timeInSeconds )

    local hours = math.floor( timeInSeconds / 3600) 
    local minutes =  math.floor((timeInSeconds - (hours*3600)) / 60)
    local seconds = math.floor(timeInSeconds - hours*3600 - minutes*60)
    
    if (string.len( tostring( hours )) < 2 ) then
        hours = "0"..hours
    end
    
    if (string.len( tostring( minutes )) < 2 ) then
        minutes = "0"..minutes
    end
    
    if (string.len( tostring( seconds )) < 2 ) then
        seconds = "0"..seconds
    end
    
    local timeFormatted = hours ..":"..minutes..":"..seconds
    
    return timeFormatted
end


InputFields =  { username = "", password ="", email ="" , inviteId ="" , passwordVerify = ""  }
InputFieldsText =  { username = "", password ="", email ="" , inviteId ="" , passwordVerify = ""  }
InputFieldsType =  { username = "username", password ="password", email ="email" , inviteId ="inviteId" , passwordVerify = "passwordVerify"  }
InputFieldsMaxLenght =  { username = 12, password = 12, email = 64 , inviteId = 9 , passwordVerify = 12  }



function CheckLoginInput()
    return InputFieldsText.username
end

function CheckPassInput()
    return InputFieldsText.password
end

function CheckEmailInput()
    return InputFieldsText.email
end

function CheckInviteInput()
    local text = string.sub(InputFieldsText.inviteId,1,3) .. " " .. string.sub(InputFieldsText.inviteId,4,6).. " " .. string.sub(InputFieldsText.inviteId, 7 )--InputFieldsText.passwordVerify
    return text--InputFieldsText.inviteId
end

function CheckPassVerifyInput()
    return InputFieldsText.passwordVerify
end

function IsNameBot( username )
    return username == BotName
end


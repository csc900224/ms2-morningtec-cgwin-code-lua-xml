require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )


local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 180 )
local height = R( 230 )

local userEmailRecovery = ""

function RecoverPasswordPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_recover_pass.xml" )
    popup = screen:GetControl( "/RecoverPasswordPopup" )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( - SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
end

function RecoverPasswordPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        RecoverPasswordPopupHide( LoginPopupShow  )
        return true
    end

    return false
end

function RecoverPasswordPopupAction( name )
    if not active then
        return
    end

    if name == "/RecoverPasswordPopup/ResetPassword" then
    
        userEmailRecovery = InputFields.email
        
        --userEmailRecovery = "jerzy.bondarowicz@game-lion.com"
        
        if not IsEmailValid( userEmailRecovery ) then
            RecoverPasswordPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_5" , function() RecoverPasswordPopupShow() end ) end )
            return
        end

        RecoverPasswordPopupHide()
        callback:ResistanceRecoverPassword( userEmailRecovery )
        
    elseif name == "/RecoverPasswordPopup/RecoveryBack" then
        RecoverPasswordPopupHide( LoginPopupShow  )
    elseif name == "/RecoverPasswordPopup/LoginButton" then
        RecoverPasswordPopupHide( LoginPopupShow )
    elseif name == "/RecoverPasswordPopup/RecoveryEmail/RecoveryEmailInputButton" then	
        SetInputTextToUpdate( popup:GetControl("RecoveryEmail/RecoveryEmailText") , "TEXT_RESISTANCE_EMAIL", InputFieldsType.email )
        UpdateInput( userEmailRecovery )
        callback:OpenVkb( userEmailRecovery , InputFieldsMaxLenght.email )
    end
end

function RecoverPasswordPopupPress( control, path )
    if not active then
        return
    end

    if path == "/RecoverPasswordPopup/ResetPassword" then
        ButtonPress( control )
    elseif path == "/RecoverPasswordPopup/RecoveryBack" then
        ButtonPress( control )
    end
end

function RecoverPasswordPopupShow()
    active = true

    PopupRecoverPassResetInputFileds()
    callback:ResistanceRefreshInputs()
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function RecoverPasswordPopupHide( callback )
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function RecoveryPasswordMailSendSuccess()
    SyncIndicatorHide()
    OperationSuccessPopupShow( "TEXT_SUCCESS_EMAIL_SENT" , LoginPopupShow )
    --RecoverPasswordPopupHide(function() OperationSuccessPopupShow( "TEXT_SUCCESS_EMAIL_SENT" , LoginPopupShow ) end  )
end

function RecoveryPasswordMailSendFailed()
    SyncIndicatorHide()
    ErrorPopupShow( "TEXT_FAIL_EMAIL_SENT" , LoginPopupShow )
    --RecoverPasswordPopupHide( function() ErrorPopupShow( "TEXT_FAIL_EMAIL_SENT" , LoginPopupShow ) end )
end

function RecoveryPasswordMailSendFailedNoNetwork()
    SyncIndicatorHide()
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" )
    --RecoverPasswordPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" ) end )
end

function PopupRecoverPassResetInputFileds()

        userEmailRecovery = ""
        
        InputFields.email = ""
        
        SetInputTextToUpdate( popup:GetControl("RecoveryEmail/RecoveryEmailText") , "TEXT_RESISTANCE_EMAIL", InputFieldsType.email )
        UpdateInput( userEmailRecovery )
end

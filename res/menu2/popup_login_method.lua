require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )


local active = false
local popup = nil

local useFb = true
local useGp = true

local x = 0
local y = 0
local width = R( 220 )
local height = R( 160 )


function LoginMethodPopupInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_login_method.xml" )
    popup = screen:GetControl( "/LoginMethodPopup" )
    
    HelpButtonInit( popup , width )
    -- chinese build uses only fb
    useGp = false
    useFb = true

    -- if IS_ANDROID then 
        -- if IS_AMAZON then
            -- useGp = false
            -- useFb = true
        -- else
            -- useGp = true
            -- useFb = false
        -- end
    -- elseif IS_IPHONE then
        -- useGp = false
        -- useFb = true
    -- else
        -- useGp = true
        -- useFb = true
    -- end

    if useGp and useFb then
        height = R( 160 )
    else
        height = R( 160 )
    end

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( - SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
    local topMargin = R(50) 

    local loginLabel = popup:GetControl( "LoginMethodLabel" )
    local labelW = loginLabel:GetWidth()
    loginLabel:SetX( ( width - labelW ) /2 )
    loginLabel:SetY( topMargin /2)
    local loginBtn = popup:GetControl( "Login" )
    -- local createBtn = popup:GetControl( "CreateAccount" )
    local loginFbBtn = popup:GetControl( "LoginWithFB" )
    --local loginGoogleBtn = popup:GetControl( "LoginWithGoogle" )
    --local skipBtn = popup:GetControl( "SkipLoginButton" )

    local w, h = loginBtn:GetGraphic( "TS" ):GetSize()
    local bottomMargin = R(10)

    local btnVertMargin = R(10)


    loginBtn:SetX( ( width - w ) /2 )
    --createBtn:SetX( ( width - w ) /2 )
    loginFbBtn:SetX( ( width - w ) /2 )
    --loginGoogleBtn:SetX( ( width - w ) /2 )
    --skipBtn:SetX( ( width - skipBtn:GetWidth() ) /2 )

	--add by csc
    loginFbBtn:SetY( topMargin )
	
    loginBtn:SetY( topMargin + h + btnVertMargin)
    -- createBtn:SetY( topMargin + h + btnVertMargin)

   -- loginFbBtn:SetY( createBtn:GetY() + h + btnVertMargin )
    
    -- if useFb then
        -- loginGoogleBtn:SetY( loginFbBtn:GetY() + h + btnVertMargin)
        -- loginFbBtn:SetVisibility( true )
    -- else
        -- loginGoogleBtn:SetY( createBtn:GetY() + h + btnVertMargin )
        -- loginFbBtn:SetVisibility( false )
    -- end

    -- local iconGpLabel = loginGoogleBtn:GetControl("Rep/LoginWithGPLabel")
    -- local iconGpImage = loginGoogleBtn:GetControl("Rep/GPicon")

    -- local iw, ih = iconGpImage:GetGraphic( "TS" ):GetSize()

    -- iconGpLabel:SetText( "TEXT_LOGIN_VIA_GOOGLE_PLUS" )
    -- iconGpLabel:SetTextWidth( iconGpLabel:GetTextWidth() )
    -- local labelWidth = iconGpLabel:GetWidth()
    -- local wholeWidth = labelWidth + iw + R(5)
    -- iconGpLabel:SetX( iw + R(5) )
    -- iconGpImage:SetX( 0 )
    -- iconGpImage:SetY( 0 )
    -- iconGpLabel:SetY( ih /2 - R(9) )

    -- loginGoogleBtn:GetControl("Rep"):SetX(( w - wholeWidth )/2)
    -- loginGoogleBtn:GetControl("Rep"):SetY(( h -ih )/2)

    -- if useGp then 
        -- loginGoogleBtn:SetVisibility( true )
    -- else
        -- loginGoogleBtn:SetVisibility( false )
    -- end

    --skipBtn:SetY( height - skipBtn:GetHeight() - bottomMargin )

end

function LoginMethodPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        LoginMethodPopupHide( function() PopupUnlock() end )
        return true
    end

    return false
end

function LoginMethodPopupAction( name )
    if not active then
        return
    end

    if name == "/LoginMethodPopup/Login" then
        -- LoginMethodPopupHide( function() LoginPopupShow() end )
        -- GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SOCIAL_LOGIN_TRY, GameEventParam.GEP_LOGIN_LOGIN )
		GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SOCIAL_LOGIN_SKIP )
        LoginMethodPopupHide( function() PopupUnlock() end)	
    -- elseif name == "/LoginMethodPopup/CreateAccount" then
        -- LoginMethodPopupHide( function() RegisterPopupShow() end )
        -- GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SOCIAL_LOGIN_TRY, GameEventParam.GEP_LOGIN_CREATE )
    elseif name == "/LoginMethodPopup/LoginWithFB" then
        LoginMethodPopupHide( function()  callback:ResistanceFB() end )
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SOCIAL_LOGIN_TRY, GameEventParam.GEP_LOGIN_FACEBOOK )
    -- elseif name == "/LoginMethodPopup/LoginWithGoogle" then
        -- LoginMethodPopupHide( function() callback:ResistanceGooglePlus() end )
        -- GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SOCIAL_LOGIN_TRY, GameEventParam.GEP_LOGIN_GOOGLE_PLUS )
    --elseif name == "/LoginMethodPopup/SkipLoginButton" then
        ---GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SOCIAL_LOGIN_SKIP )
        --LoginMethodPopupHide( function() PopupUnlock() end)	
    end

end

function LoginMethodPopupPress( control, path )
    if not active then
        return
    end

    if path == "/LoginMethodPopup/Login" then
       
   -- elseif path == "/LoginMethodPopup/CreateAccount" then
        ButtonPress( control )
    elseif path == "/LoginMethodPopup/LoginWithFB" then
        ButtonPress( control )
   -- elseif path == "/LoginMethodPopup/LoginWithGoogle" then
        ButtonPress( control )
    --elseif path == "/LoginMethodPopup/SkipLoginButton" then
        
    end
end

function LoginMethodPopupShow()
    Lock()

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            Unlock()
            active = true
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function LoginMethodPopupHide( callback )
    active = false
    Lock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
            Unlock()
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/survival_item.lua" )
require( "menu2/survival_scroll_indicator.lua" )

local root = nil
local clip = nil
local width = nil
local height = nil
local items = nil
local currentCat = nil
local indicator = nil

local SurvivalConsts = {
    itemHeight = R( 148 ),
    itemWidth = R(300),
    itemSpacing = R(-3),
    itemXOffset = R(5), 
    itemYOffset = R(5) 
}

local SurvivalScrollData = {
    touchId = -1,
    minScrollDist = R(5),
    maxScrollOffset = 0,
    scrollOffset = 0,
    sampleTouchPos = 0,
    currentTouchPos = 0,
    startScrollOffset = 0,
    touchStart = 0,
    scrolling = false,    
    targetScrollOffset = 0,
    scrollAcceleration = 1,
    samplingInterval = 0.025,
    sampleTimer = -1,
    velocityEpsilon = 0.1,
    velocity = 0,
    lastVelocity = 0,
    draggTime = 0
}

function SurvivalListInit( parent, listWidth, listHeight, maxItemH, itemWidth, itemSpacing )
    parent:InsertFromXml( "menu2/survival_list.xml" )
    clip = parent:GetControl( "SurvivalList" )
    root = parent:GetControl( "SurvivalList/Survivals" )
    indicator = parent:GetControl( "SurvivalScrollIndicatorRoot" )
    width = listWidth
    height = listHeight

    if itemWidth ~= nil then
        SurvivalConsts.itemWidth = itemWidth
    else
        SurvivalConsts.itemWidth = listWidth
    end
    if maxItemH ~= nil then
        SurvivalConsts.itemHeight = maxItemH
    end
    if itemSpacing ~= nil then
        SurvivalConsts.itemSpacing = itemSpacing
    end 

    SurvivalScrollIndicatorInit( indicator, 0, SurvivalConsts.itemHeight )
    SurvivalListCaluclateLayout()

    clip:GetGraphic( "r" ):SetWidth( width )
    clip:GetGraphic( "r" ):SetHeight( height )

end

function SurvivalListCaluclateLayout()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
   -- SurvivalConsts.itemHeight = math.min( height - iH, SurvivalConsts.itemHeight)
    indicator:SetX( width )
    indicator:SetY( (height - iH)/2 )
end

function SurvivalListUpdateIndicatorPos()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    indicator:SetY( (height - iH)/2 )
end

function SurvivalListSetContent( itemList, resetScroll )
    if resetScroll == nil or resetScroll == true then
        SurvivalListResetScroll()
    end
    SurvivalListDropAllItems()
    
    items = itemList
    local y = SurvivalConsts.itemYOffset
    local idx = 1
    for _, item in ipairs( itemList ) do
        local itemRoot = root:InsertControl( tostring( idx ) )
        idx=idx+1
        itemRoot:SetRelative( true )
        itemRoot:SetY( y )
        SurvivalItemInit( itemRoot, SurvivalConsts.itemWidth, SurvivalConsts.itemHeight, item )
        y = y + SurvivalConsts.itemHeight + SurvivalConsts.itemSpacing
    end
    SurvivalScrollData.maxScrollOffset = math.max( y - SurvivalConsts.itemSpacing - height, 0 )
    
    SurvivalScrollIndicatorUpdateMaxScrollOffset( SurvivalScrollData.maxScrollOffset )
    SurvivalListUpdateIndicatorPos()
    
    SurvivalListCreateClipBorder()
    
    for i=1,#SurvivalLevelsInfo do
        SurvivalItemSelectTab( LeaderboardTabs.Friends ,i,root, true)
    end

end

function SurvivalListResetScroll()
    SurvivalScrollData.scrollOffset = 0
    root:SetY( 0 )
    SurvivalScrollData.scrolling = false
    SurvivalScrollData.touchId = -1
    SurvivalScrollData.velocity = 0
    SurvivalScrollData.lastVelocity = 0
    SurvivalScrollData.sampleTimer = -1
    SurvivalScrollData.draggTime = 0
    SurvivalScrollData.targetScrollOffset = 0
    SurvivalScrollData.scrollOffset = 0
end

function SurvivalListUpdate( dt )
    if SurvivalScrollData.touchId >= 0 then
        SurvivalScrollData.draggTime = SurvivalScrollData.draggTime + dt
    end

    SurvivalListSampleVelocity( dt )
    SurvivalListUpdateScrolling( dt )
    
    SurvivalScrollIndicatorUpdateScrollOffset( SurvivalScrollData.scrollOffset )

    if items then
        for i=1,#items do
            local levelIdx = i
            SurvivalItemUpdate( root:GetControl( i .. "/SurvivalItem" ) , levelIdx )
        end
    end
end

function SurvivalListSampleVelocity( dt )
    if SurvivalScrollData.touchId >= 0 and SurvivalScrollData.sampleTimer >= 0 then
        SurvivalScrollData.sampleTimer = SurvivalScrollData.sampleTimer - dt
        if SurvivalScrollData.sampleTimer < 0 then
            SurvivalScrollData.sampleTimer = SurvivalScrollData.samplingInterval
            SurvivalScrollData.lastVelocity = (SurvivalScrollData.currentTouchPos - SurvivalScrollData.sampleTouchPos) / SurvivalScrollData.samplingInterval
            SurvivalScrollData.sampleTouchPos = SurvivalScrollData.currentTouchPos
        end
    end
end

function SurvivalListTouchDown( x, y, id, c )
    if items == nil then return end

    local path = c:GetPath()
    if string.find( path, "/SurvivalList" ) then

        if SurvivalScrollData.touchId == id then
            SurvivalListTouchUp( SurvivalScrollData.currentTouchPos, y, id, nil )
        end

        if SurvivalScrollData.touchId < 0 then
            SurvivalScrollData.touchId = id
            SurvivalScrollData.touchStart = y
            SurvivalScrollData.scrolling = false
            SurvivalScrollData.velocity = 0
            SurvivalScrollData.lastVelocity = 0
            SurvivalScrollData.sampleTimer = SurvivalScrollData.samplingInterval
            SurvivalScrollData.currentTouchPos = y
            SurvivalScrollData.sampleTouchPos = y
            SurvivalScrollData.draggTime = 0
        end

         SurvivalItemPress( c, path )
    end
    
end

function SurvivalListTouchUp( x, y, id, c )
    if items == nil then return end

    if SurvivalScrollData.scrolling and id == SurvivalScrollData.touchId then
        SurvivalScrollData.scrolling = false
        SurvivalScrollData.touchId = -1
        SurvivalScrollData.sampleTimer = -1
        SurvivalScrollData.targetScrollOffset = SurvivalItemClampOffset( SurvivalScrollData.startScrollOffset + SurvivalScrollData.touchStart - y )

        if SurvivalScrollData.draggTime < SurvivalScrollData.samplingInterval and SurvivalScrollData.draggTime > 0 then
            SurvivalScrollData.lastVelocity = ( y - SurvivalScrollData.sampleTouchPos ) / SurvivalScrollData.draggTime
        end
        SurvivalScrollData.velocity = SurvivalScrollData.lastVelocity
    else
        if not c then return end
        local path = c:GetPath()
        SurvivalItemAction( c, path )
        if string.find( path, "LeaderboardRoot/Tabs/TabGlobal/Button" ) then
            local levelId = 1
            levelId = tonumber( c:GetChildren()[1]:GetName() )
            SurvivalItemSelectTab( LeaderboardTabs.Global , levelId , root )
        elseif string.find( path, "LeaderboardRoot/Tabs/TabFriends/Button" ) then
            local levelId = 1
            levelId = tonumber( c:GetChildren()[1]:GetName() )
            SurvivalItemSelectTab( LeaderboardTabs.Friends , levelId , root )
        elseif string.find( path, "SurvivalItem/TimeoutRoot/Refresh/Button" ) then
            SetLastSyncTimeLeaderboardsFriends(0)
            SetLastSyncTimeLeaderboardsGlobal (0)
            callback:ResistanceUpdateAll()
            for i=1,#SurvivalLevelsInfo do
                SurvivalItemSelectTab( LeaderboardTabs.Friends ,i,root, true)
            end
        end
    end
end

function SurvivalListTouchMove( x, y, id, c )
    if items == nil then return end

    if id == SurvivalScrollData.touchId then
        if not SurvivalScrollData.scrolling then
            if math.abs( SurvivalScrollData.touchStart - y ) > SurvivalScrollData.minScrollDist then
                -- Start real scrolling
                SurvivalScrollData.touchStart = y
                SurvivalScrollData.startScrollOffset = SurvivalScrollData.scrollOffset
                SurvivalScrollData.scrolling = true
            end
        else
            SurvivalScrollData.targetScrollOffset = SurvivalItemClampOffset( SurvivalScrollData.startScrollOffset + SurvivalScrollData.touchStart - y )
            SurvivalScrollData.currentTouchPos = y
        end    
    end
end

function SurvivalItemClampOffset( offset )
    return math.min( math.max(offset, 0), SurvivalScrollData.maxScrollOffset )
end

function SurvivalListUpdateScrolling( dt )
    if math.abs(SurvivalScrollData.velocity) > SurvivalScrollData.velocityEpsilon then
        SurvivalScrollData.velocity = SurvivalScrollData.velocity - SurvivalScrollData.velocity * SurvivalScrollData.scrollAcceleration * dt
        SurvivalScrollData.targetScrollOffset = SurvivalItemClampOffset( SurvivalScrollData.targetScrollOffset - SurvivalScrollData.velocity * dt )
    end
    
    if math.abs(SurvivalScrollData.scrollOffset - SurvivalScrollData.targetScrollOffset) > SurvivalScrollData.velocityEpsilon then
        SurvivalScrollData.scrollOffset = SurvivalScrollData.targetScrollOffset
        root:SetY( -SurvivalScrollData.scrollOffset )
    end  
    
    SurvivalListClipItems()
end

function SurvivalListClipItems()
    for _, item in ipairs( root:GetChildren() ) do
        local visible = -SurvivalScrollData.scrollOffset + item:GetY() <= height and -SurvivalScrollData.scrollOffset + item:GetY() + SurvivalConsts.itemHeight >= 0
        item:SetVisibility( visible )
    end
end

function SurvivalListDropAllItems()
    for _, item in ipairs( root:GetChildren() ) do
        item:Remove()
    end
end

function SurvivalListCreateClipBorder()

    --local ls = clip:InsertControl("ls")
    --ls:AddRepresentation( "default", "menu/timage.xml" )
    --ls:SetRelative( true )
    --ls:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    --ls:SetY( - R( 8 ) )
    
    --local lsw, lsh = ls:GetGraphic( "TS" ):GetSize()

    --local rs = clip:InsertControl("rs")
    --rs:AddRepresentation( "default", "menu/timage.xml" )
    --rs:SetRelative( true )
    --rs:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    --rs:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    --rs:SetX( width - lsw )
    --rs:SetY( - R( 8 ) )
    
end


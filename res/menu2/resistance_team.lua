require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/social_item.lua" )
require( "menu2/friend_list.lua" )
require( "ResistanceDB.lua" )
local active = false
local syncing = false

local x = R( 10 )
local y = R( 45 )

local width = R( 320 )
local height = R( 280 )

local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local popup = nil
local root = nil

function ResistanceTeamAction( name )
    if not active then
        return
    end
end

function ResistanceTeamPress( control, path )
    if not active then
        return
    end
end

function ResistanceTeamShow()
    active = true
    callback:Playhaven( "social" )

    if UpdateInfo.Friends > 0 then
        TimeoutIndicatorHide( nil, true )
        FriendsListSetContent( Friends )
        popup:GetControl("SyncIndicator"):SetVisibility( false ) 
        FriendsListShowPosIndicator()
        syncing = false
    elseif UpdateInfo.Friends < 0 then
        popup:GetControl("SyncIndicator"):SetVisibility( false ) 
        FriendsListDropAllItems()
        FriendsListHidePosIndicator()
        TimeoutIndicatorShow()
    else
        TimeoutIndicatorHide( nil, true )
        ResistanceTeamSyncingStart()
    end

    root:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( SmoothStep( t ) * 255 )
        end
        )
    )
end

function ResistanceTeamHide( callback )

    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end
        ),
        function()
            root:SetVisibility( false )
            if callback then callback() end
        end
     )
end

local dot1 = nil
local dot2 = nil
local dot3 = nil

function ResistanceTeamInit( parent , listWidth )

    parent:InsertFromXml( "menu2/resistance_team.xml" )

    root = parent
    popup =  parent:GetControl( "ResistanceTeam" )

    local friendsListWidth = listWidth
    local friendsListHeight = R(170)
    
    --creating list of all item
    local listRoot = popup:GetControl( "FriendsListRoot")

    FriendsListInit( listRoot, friendsListWidth, friendsListHeight )
    
    popup:GetControl( "SyncIndicator"):SetX( friendsListWidth / 2 )
    popup:GetControl( "SyncIndicator"):SetY( FriendsConsts.itemHeight/2 )

    dot1 = popup:GetControl( "SyncIndicator/SyncIndicatorDot1" )
    dot2 = popup:GetControl( "SyncIndicator/SyncIndicatorDot2" )
    dot3 = popup:GetControl( "SyncIndicator/SyncIndicatorDot3" )

    local w, h = dot1:GetGraphic( "TS" ):GetSize()
    local margin = R ( 3 )
    dot1:SetX( - margin - w - w/2)
    dot2:SetX( -w/2 )
    dot3:SetX( margin + w - w/2)
end

function ResistanceTeamUpdate( dt )
    if not active then return end
    FriendsListUpdate( dt )
end

function ResistanceTeamTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    FriendsListTouchDown( x, y, id, c )

    local path = c:GetPath()
    --if string.find(path, "/ShopRoot/Buttons/") then
    --	ButtonPress( c )
    --end
end

function ResistanceTeamTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    FriendsListTouchUp( x, y, id, c )
end

function ResistanceTeamTouchMove( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    FriendsListTouchMove( x, y, id, c )
end

function IsResistanceTeamSyncing()
   return syncing 
end

function EndResistanceTeamSyncingSucces()

    syncing = false
    ResistanceTeamSyncIndicatorHide()
    TimeoutIndicatorHide( nil,true )
    root:SetVisibility( false )
    
    FriendsListSetContent( Friends )
    root:SetAlpha( 0 )
    root:SetVisibility( true )
    anim:Add( Slider:New(0.3,
        function( t )
            root:SetAlpha( SmoothStep( t ) * 255 )
        end,
        FriendsListShowPosIndicator()
        )
    )
end

function EndResistanceTeamSyncingFail()
    ResistanceTeamSyncIndicatorHide()
    syncing = false
end

function ResistanceTeamSyncingStart()
    FriendsListDropAllItems()
    FriendsListHidePosIndicator()
    syncing = true
    ResistanceTeamSyncIndicatorShow()
end

function ResistanceTeamSyncIndicatorShow()   
    
    local indicator  = popup:GetControl("SyncIndicator")
    
    indicator:SetVisibility( true )
    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "ResistanceTeamPopup" )
    anim:Add( Slider:New( (time or 0.3) * (255-startAlpha)/255,
        function( t )
            indicator:SetAlpha( startAlpha + SmoothStep( t ) * (255-startAlpha) )
        end
        ),
        nil,
        "ResistanceTeamSyncPopup"
    )
    
    anim:RemoveAll( "ResistanceTeamSyncBlink" )

    local lt = 0
    local timer = 0
    local speed = 5
    anim:Add( Perpetual:New(
        function( t )
            local dt = t - lt
            lt = t
            if ( timer > 3) then 
                timer = 0
            else
                timer = timer + speed*dt
            end

            if timer <= 1 then
                dot1:SetRepresentation( "active" )
            else
                dot1:SetRepresentation( "default" )
            end
            
            if timer >= 1 and timer <= 2 then
                dot2:SetRepresentation( "active" )
            else
                dot2:SetRepresentation( "default" )
            end
            
            if timer >= 2 and timer <= 3 then
                dot3:SetRepresentation( "active" )
            else
                dot3:SetRepresentation( "default" )
            end
        end
        ),
        nil,
        "ResistanceTeamSyncBlink"
    )
end

function ResistanceTeamSyncIndicatorHide( callback )

    local indicator  = popup:GetControl("SyncIndicator")

    indicator:SetVisibility( true )

    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "ResistanceTeamSyncPopup" )
    anim:RemoveAll( "ResistanceTeamSyncBlink" )
    anim:Add( Slider:New( (time or 0.3) * startAlpha/255,
        function( t )
            indicator:SetAlpha( startAlpha - SmoothStep( t ) * startAlpha )
        end
        ),
        function()
            indicator:SetVisibility( false )
        end,
        "ResistanceTeamSyncPopup"
    )

    if callback then callback() end
end

function RefreshFriendList()
    ResistanceTeamHide( function() ResistanceTeamShow() end )
end


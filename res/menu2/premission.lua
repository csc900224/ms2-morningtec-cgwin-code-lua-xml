require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/math.lua" )
require( "menu2/popup.lua" )
require( "menu2/slider.lua" )
require( "menu2/weapon_atlas.lua" )
require( "menu2/avatar_social.lua" )
require( "menu2/lock.lua" )

local active = false
local root = nil
local level = nil
local friendLock = nil
local backupFriend = nil

local margin = {
    top = R(60),
    bottom = R(40)
}

local missionPopup = nil
local missionFrame = {
    x = 0,
    y = 0,
    width = R(236),
    height = R(216)
}

local weaponsSelected = {}
local weaponsRoots = {}

local weaponFrame = {
    x = 0,
    y = 0,
    width = R(108),
    height = R(108)
}

local friendLayout = {
    x = 0,
    y = 0,
    offset = 0
}

local friend = false
local survival = false

ElementMap = {}
ElementMap[Element.electric] = "Electric"
ElementMap[Element.fire] =  "Fire"
ElementMap[Element.frost] = "Frost"

ElementCharsMap = {}
ElementCharsMap[Element.electric] = "\194\175"
ElementCharsMap[Element.fire] = "\194\174"
ElementCharsMap[Element.frost] = "\194\176"

EnemyIcons =  {}
EnemyIcons[EntityType.OctopusSimple] = "menu2/enemy_simple_octopus_electric.png@linear"
EnemyIcons[EntityType.OctopusShotAware] = "menu2/enemy_avoider_octopus_frost.png@linear"
EnemyIcons[EntityType.OctopusChaser] = "menu2/enemy_chaser_octopus_fire.png@linear"
EnemyIcons[EntityType.SqueezerSimple] = "menu2/enemy_simple_roller_electric.png@linear"
EnemyIcons[EntityType.SqueezerTurning] = "menu2/enemy_turning_roller_fire.png@linear"
EnemyIcons[EntityType.FishSimple] = "menu2/enemy_simple_fish_frost.png@linear"
EnemyIcons[EntityType.FishThrowing] = "menu2/enemy_shooting_fish_frost.png@linear"
EnemyIcons[EntityType.FloaterSimple] = "menu2/enemy_floater_frost.png@linear"
EnemyIcons[EntityType.FloaterElectric] = "menu2/enemy_rayzor_electric_v2.png@linear"
EnemyIcons[EntityType.FloaterSower] = ""
EnemyIcons[EntityType.HoundSimple] = "menu2/enemy_hound_frost.png@linear"
EnemyIcons[EntityType.HoundShooting] = "menu2/enemy_big_dog_fire.png@linear"
EnemyIcons[EntityType.SectoidSimple] = "menu2/enemy_lesser_sectoid_normal.png@linear"
EnemyIcons[EntityType.SectoidShooting] = "menu2/enemy_shooting_sectoid_normal.png@linear"
EnemyIcons[EntityType.KillerWhale] = "menu2/enemy_turning_killerwhale_fire.png@linear"
EnemyIcons[EntityType.KillerWhaleSimple] = "menu2/enemy_simple_killerwhale_electric.png@linear"
EnemyIcons[EntityType.Crab] = "menu2/enemy_crab_normal.png@linear"
EnemyIcons[EntityType.MechaBoss] = "menu2/enemy_boss_octo_mech.png@linear"
EnemyIcons[EntityType.SowerBoss] = "menu2/enemy_boss_sower.png@linear"
EnemyIcons[EntityType.OctobrainBoss] = "menu2/enemy_boss_psycho_brain.png@linear"
EnemyIcons[EntityType.OctobrainBossClone] = "menu2/enemy_boss_psycho_brain.png@linear"
EnemyIcons[EntityType.OctopusFriend] = "menu2/enemy_chaser_octopus_fire.png@linear"
EnemyIcons[EntityType.Lobster] = "menu2/enemy_lobster_frost.png@linear"
EnemyIcons[EntityType.NautilSimple] = "menu2/enemy_turning_nautil_normal.png@linear"
EnemyIcons[EntityType.NautilTurning] = "menu2/enemy_simple_nautil_normal.png@linear"
EnemyIcons[EntityType.Nerval] = "menu2/enemy_nerval_fire.png@linear"

function PremissionInit( parent )
    parent:InsertFromXml( "menu2/premission.xml" )
    root = parent:GetControl( "Premission" )
    root:SetVisibility( false )

    missionPopup = root:GetControl( "Mission" )

    PremissionCalculateLayout()

    PopupCreateFrame(
        missionPopup:GetControl( "Frame" ),
        missionFrame.width,
        missionFrame.height
    )

    HelpButtonInitBottomLeft( missionPopup , missionFrame.height, { x = R(16), y = R(16) } )

    local weapon = root:GetControl( "Weapon01" )
    PopupCreateFrame(
        weapon:GetControl( "Window" ):GetControl( "Frame" ),
        weaponFrame.width,
        weaponFrame.height
    )
    table.insert( weaponsRoots, weapon )

    if TutorialActive then
        PopupCreateFrame(
            weapon:GetControl( "Window" ):GetControl( "TutorialFrame" ),
            weaponFrame.width,
            weaponFrame.height,
            PopupFrameGreen
        )
    end

    weapon = root:GetControl( "Weapon02" )
    PopupCreateFrame(
        weapon:GetControl( "Window" ):GetControl( "Frame" ),
        weaponFrame.width,
        weaponFrame.height
    )
    table.insert( weaponsRoots, weapon )

    if TutorialActive then
        PopupCreateFrame(
            weapon:GetControl( "Window" ):GetControl( "TutorialFrame" ),
            weaponFrame.width,
            weaponFrame.height,
            PopupFrameGreen
        )
    end

    local bg = missionPopup:GetControl( "Description/Bg" )
    anim:Add(
        Perpetual:New(
            function( t )
                bg:SetAlpha( ( 0.75 + 0.125 * ( 1 + math.sin( t * 4 ) ) ) * 255 )
            end
        )
    )
end

function PremissionCalculateLayout( scale )
    do
        local height = SCREEN_HEIGHT - margin.top - margin.bottom
        local scale = height/missionFrame.height
        missionFrame.height = height

        root:SetY( margin.top )

        local buttons = root:GetControl( "Buttons" )
        buttons:SetY( missionFrame.height )

        local description = missionPopup:GetControl( "Description" )
        description:SetY( description:GetY() * scale )

        local rewards = missionPopup:GetControl( "Rewards" )
        rewards:SetY( rewards:GetY() * scale )

        local cash = rewards:GetControl( "Cash" )
        cash:SetY( cash:GetY() * scale )

        local xp = rewards:GetControl( "Xp" )
        xp:SetY( xp:GetY() * scale )

        local friend = missionPopup:GetControl( "Friend" )
        friend:SetY( friend:GetY() * scale )

        local avatar = friend:GetControl( "Avatar" )
        avatar:SetY( avatar:GetY() * scale )

        friendLayout.x = friend:GetX()
        friendLayout.y = friend:GetY()
        friendLayout.offset = ( missionFrame.width - avatar:GetWidth() )/2 - friend:GetX() - avatar:GetX()

        local line = friend:GetControl( "Line" )
        line:SetY( avatar:GetY() + avatar:GetHeight() - R(8) )

        local add = friend:GetControl( "Add" )
        add:SetY( line:GetY() + avatar:GetY() )

        local remove = friend:GetControl( "Remove" )
        remove:SetY( remove:GetY() * scale )
    end

    do
        local monsters = root:GetControl( "Monsters" )
        local frame = monsters:GetControl( "Frame" )
        local fill = frame:GetControl( "Fill" )
        local bottom = frame:GetControl( "Bottom" )

        local padding = bottom:GetControl( "Left" ):GetHeight()
        local height = missionFrame.height - monsters:GetY()

        fill:GetGraphic( "r" ):SetHeight( height - padding )
        bottom:SetY( fill:GetY() + height - padding )

        local icons = monsters:GetControl( "Icons" )
        local iconsHeight = icons:GetControl( "1/Icon" ):GetHeight()
        icons:SetY( ( height - padding/4 - iconsHeight )/2 )

        local header = monsters:GetControl( "Header" )
        header:SetY( ( icons:GetY() - header:GetHeight() )/2 )

        local requirements = monsters:GetControl( "RequiredElements" )
        requirements:SetY( icons:GetY() + iconsHeight + header:GetY() )

        header:SetVisibility( header:GetY() + header:GetHeight() < icons:GetY() )
        requirements:SetVisibility( icons:GetY() + iconsHeight < requirements:GetY() )
    end
end

function PremissionSetup( lvl, surv, lock )
    level = lvl
    survival = surv
    friendLock = lock

    missionPopup:GetControl( "Description/Text" ):SetText( level.d )
    registry:Set( "/internal/objective", level.d )

    local icon = LevelIcons[level.i]
    missionPopup:GetControl( "Description/Img" ):GetGraphic( "s" ):SetImage( icon )
    registry:Set( "/internal/objectiveicon", icon )

    local rewards = missionPopup:GetControl( "Rewards" )
    rewards:SetVisibility( not survival )

    if survival then
        local friend = missionPopup:GetControl( "Friend" )
        friend:SetX( friendLayout.x + friendLayout.offset )
        friend:SetY( friendLayout.y )

        local cost = ( registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" ) ) and registry:Get( "/app-config/fuel/cost/survival" ) or 0
        if cost > 0 then
            missionPopup:GetControl( "Header/Title" ):SetText( screen:GetText( level.n ) .. " \194\157 -" .. cost )
        else
            missionPopup:GetControl( "Header/Title" ):SetText( screen:GetText( level.n ) )
        end
    else
        local friend = missionPopup:GetControl( "Friend" )
        friend:SetX( friendLayout.x )
        friend:SetY( friendLayout.y )

        missionPopup:GetControl( "Header/Title" ):SetText( screen:GetText( level.n ) )

        if not TutorialActive and registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" ) then
            local cost = registry:Get( "/internal/boss" ) and registry:Get( "/app-config/fuel/cost/boss" ) or registry:Get( "/app-config/fuel/cost/normal" )
            rewards:GetControl( "Fuel" ):SetVisibility( true )
            rewards:GetControl( "Fuel/Value" ):SetText( "\194\157 -" .. cost )
        else
            rewards:GetControl( "Fuel" ):SetVisibility( false )
        end

        local soft = registry:Get( "/internal/reward/soft" ) or 0
        rewards:GetControl( "Cash/Value" ):SetText( "\194\156 +" .. soft )

        local xp = registry:Get( "/internal/reward/xp" ) or 0
        rewards:GetControl( "Xp/Value" ):SetText( "\194\173 +" .. xp )
    end

    -- Request environment atlases
    for i = 1, #level.a do
        callback:RequestAtlas( level.a[i] )
    end

    -- Request enemies atlases
    local i = 1
    while true do
        local m = registry:Get( "/internal/monsters/" .. i )
        if not m or m == -1 then break end

        for j, a in ipairs( EntityAtlas[m] ) do
            callback:RequestAtlas( a )
        end

        i = i + 1
    end
end

function PremissionKeyDown( code )
    if not active or IsSyncIndicatorVisible() then
        return
    end

    if code == keys.KEY_ESCAPE then
        PremissionBack()
        return true
    end

    return false
end

function PremissionPress( control, path )
    if not active then
        return
    end

    if path == "/Premission/Premission/Buttons/Missions/Button" then
        ButtonPress( control )
    elseif path == "/Premission/Premission/Buttons/Back" then
        ButtonPress( control )
    elseif path == "/Premission/Premission/Buttons/Play" then
        ButtonPress( control )
    elseif path == "/Premission/Premission/Buttons/FriendLock" then
        ButtonPress( control )
    elseif path == "/Premission/Premission/Mission/Friend/Add" then
        -- MorningTec Begin
        -- ButtonPress( control )
        -- MorningTec End
    elseif path == "/Premission/Premission/Mission/Friend/Remove" then
        ButtonPress( control )
    elseif path == "/Premission/Premission/Monsters/Help" then
        ButtonPress( control )
    end
end

function PremissionAction( path )
    if not active then
        return
    end

    if path == "/Premission/Premission/Buttons/Back" then
        PremissionBack()
    -- MorningTec Begin
    -- elseif path == "/Premission/Premission/Buttons/FriendLock" then
        -- local avatar = missionPopup:GetControl( "Friend/Add" )
        -- local x = avatar:GetAbsX() + avatar:GetWidth()
        -- local y = avatar:GetAbsY() + avatar:GetHeight() / 2
        -- TutorialTooltipShow( "/", "TutorialFriendLock", "TEXT_FRIENDLOCK_TUTORIAL", x, y, TutorialAnchor.Left, 0, true, 5, R(200), false )
    -- MorningTec End
    elseif path == "/Premission/Premission/Buttons/Play" then
        local function Go()
            TutorialAction( path )

            registry:Set( "/internal/friend", friend )
            registry:Set( "/internal/story", not survival )
            registry:Set( "/internal/survival", survival )
            registry:Set( "/internal/levelcompleted", false )

            if backupFriend and friend then
                registry:Set( "/internal/friendName", backupFriend.Name )
                registry:Set( "/internal/friendWeapon", backupFriend.UsedWeaponId )
                registry:Set( "/internal/friendWeaponLevel", backupFriend.UsedWeaponUpgrade )
                registry:Set( "/internal/friendId", backupFriend.UniqueId )
                SetBackupFlag( backupFriend.UniqueId , TimeSinceUpdateDB + 86400 )
                callback:ResistanceSetFriendAsBackup( backupFriend.UniqueId )

            end

            Lock()
            StartLevelSelected()
        end

        if not TutorialActive and registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" ) then
            local fuel = registry:Get( "/monstaz/cash/fuel" )
            local cost = survival and registry:Get( "/app-config/fuel/cost/survival" ) or registry:Get( "/internal/boss" ) and registry:Get( "/app-config/fuel/cost/boss" ) or registry:Get( "/app-config/fuel/cost/normal" )

            if fuel >= cost then
                registry:Set( "/monstaz/cash/fuel", fuel - cost )
                callback:Save()

                Go()
            else
                OutOfFuelShow()
            end
        else
            Go()
        end
    -- MorningTec Begin
    -- elseif path == "/Premission/Premission/Mission/Friend/Add" then
        -- if not IsLoggedIn() then
            -- BackupRequirePopupShow()
        -- else
            -- PremissionHide( BackupTeamShow() , false )
        -- end
    -- MorningTec End
    elseif path == "/Premission/Premission/Mission/Friend/Remove" then
        RefreshBackupSlot()
    elseif path == "/Premission/Premission/Weapon01/Window" then
        TutorialAction( path )
        ReleaseWeaponAtlas( weaponsSelected[1] )
        WeaponSelectShow( {weaponsSelected[2]}, "/monstaz/weaponselection/", 1 )
        PremissionHide( nil, false )
    elseif path == "/Premission/Premission/Weapon02/Window" then
        TutorialAction( path )
        ReleaseWeaponAtlas( weaponsSelected[2] )
        WeaponSelectShow( {weaponsSelected[1]}, "/monstaz/weaponselection/", 2 )
        PremissionHide( nil, false )
    elseif path == "/Premission/Premission/Buttons/Missions/Button" then
        PremissionHide( nil, false )
        MissionsShow( function () PremissionShow() end )
    end
end

function PremissionBack()
    for i = 1, #level.a do
        callback:ReleaseAtlas( level.a[i] )
    end

    for i = AtlasSet.Boss1, AtlasSet.Enemy8 do
        callback:ReleaseAtlas( i )
    end

    PremissionHide()

    if survival then
        SurvivalShow()
    end
end

function PremissionInitWeapons()
    local maxWeaponsSelected = #weaponsRoots

    weaponsSelected = {}
    for idx=1,maxWeaponsSelected do
        local v = registry:Get( "/monstaz/weaponselection/" .. idx )
        if v == "" then
            break
        else
            table.insert( weaponsSelected, v )
            LoadWeaponAtlas( v )
        end
    end
    -- End list mark
    registry:Set( "/monstaz/weaponselection/" .. maxWeaponsSelected + 1, "" )

    PremissionUpdateWeaponIcons()
end

function PremissionFillOwnedWeapons()
    local lastWepIdx = 1
    local wepNum = #ItemsCategory[ItemCategory.weapons]
    for idx=1,#weaponsRoots do
        if weaponsSelected[idx] == nil then
            for wepIdx=lastWepIdx, wepNum do
                local wepId = ItemsCategory[ItemCategory.weapons][wepIdx].id
                if (idx == 1 or wepId ~= weaponsSelected[idx-1]) and Shop:IsBought( wepId ) > 0 then
                    registry:Set( "/monstaz/weaponselection/" .. idx, wepId )
                    weaponsSelected[idx] = wepId
                    lastWepIdx=wepIdx+1
                    break
                end
            end
        end
    end
end

function PremissionUpdateWeaponIcons()
    local exclusive = nil
    for idx=1,#weaponsRoots do
        local weaponId = weaponsSelected[idx]
        local header = weaponsRoots[idx]:GetControl( "Header" )
        local name = header:GetControl( "Name" )
        local icon = weaponsRoots[idx]:GetControl( "Window/Icon" )
        local msg = weaponsRoots[idx]:GetControl( "Window/TapMessage" )
        local exMsg = weaponsRoots[idx]:GetControl( "Window/ExclusiveMessage" )
        local elements = weaponsRoots[idx]:GetControl( "Window/Elements" )

        if weaponId ~= nil then
            local item = ItemsById[weaponId]
            if item.exclusive == true then exclusive = item end
            header:SetVisibility( true )
            icon:SetVisibility( true )
            msg:SetVisibility( false )
            exMsg:SetVisibility( false )
            name:SetText( item.d )

            icon:GetGraphic( "TS" ):SetImage( item.f )
            local iconW, iconH = icon:GetGraphic( "TS" ):GetSize()
            icon:GetGraphic( "TS" ):SetPivot( iconW/2, iconH/2 )
            icon:SetAlpha( 255 )
            icon:GetGraphic( "TS" ):SetScale( 1.0 )

            PremissionUpdateElemetns( item, elements )
        else
            msg:SetVisibility( exclusive == nil )
            header:SetVisibility( false )
            icon:SetVisibility( exclusive ~= nil )
            exMsg:SetVisibility( exclusive ~= nil )
            elements:SetVisibility( false )
            if exclusive ~= nil then
                icon:GetGraphic( "TS" ):SetImage( exclusive.f )
                icon:GetGraphic( "TS" ):SetScale( 0.9 )
                local iconW, iconH = icon:GetGraphic( "TS" ):GetSize()
                icon:GetGraphic( "TS" ):SetPivot( iconW/2, iconH/2 )
                icon:SetAlpha( 128 )
            end
        end
    end
end

function PremissionInitEnemyIcons()
    local boss = registry:Get( "/internal/boss" )

    local requiredElements = {}
    local requiredElementsCount = 0

    root:GetControl( "Monsters/Icons/1" ):SetVisibility( false )
    root:GetControl( "Monsters/Icons/2" ):SetVisibility( false )
    root:GetControl( "Monsters/Icons/3" ):SetVisibility( false )
    root:GetControl( "Monsters/Icons/Boss" ):SetVisibility( false )

    anim:RemoveAll( "PremissionElements" )

    local i = 1
    while true do
        local monster = registry:Get( "/internal/monsters/" .. i )
        if monster == -1 then break end

        local c = nil
        if not boss then
            c = root:GetControl( "Monsters/Icons/" .. i )
        else
            c = root:GetControl( "Monsters/Icons/Boss" )
        end

        c:SetVisibility( true )
        c:GetControl( "Icon" ):GetGraphic( "TS" ):SetImage( EnemyIcons[monster] )

        local element = registry:Get( "/internal/elements/" .. i )
        if element == 3 then element = 4 end

        if element == 0 then
            c:GetControl( "Element" ):SetVisibility( false )
            c:GetControl( "Bg" ):SetRepresentation( "default" )

            local line = c:GetControl( "Line" )
            line:GetControl( "Left" ):SetRepresentation( "default" )
            line:GetControl( "Fill" ):SetRepresentation( "default" )
            line:GetControl( "Right" ):SetRepresentation( "default" )
        else
            local r = string.lower( ElementMap[element] )

            if not boss then
                c:GetControl( "Bg" ):SetRepresentation( r )
            end

            local e = c:GetControl( "Element" )
            e:SetVisibility( true )
            e:SetRepresentation( r )

            local found = false
            for _, weapon in ipairs( weaponsSelected ) do
                local item = ItemsById[weapon]
                local index = math.min( #item.upgrades.Elements, Shop:GetUpgrades( item.id ) + 1 )
                for j = 1, index do
                    if element == item.upgrades.Elements[j] then
                        found = true
                        break
                    end
                end

                if found then break end
            end

            if not found and not requiredElements[element] then
                requiredElements[element] = true
                requiredElementsCount = requiredElementsCount + 1

                anim:Add(
                    Perpetual:New(
                        function( t )
                            e:GetGraphic( "TS" ):SetAngle( math.sin( t * 8.41 ) * math.pi * 0.025 * 2 )
                        end
                    ),
                    nil,
                    "PremissionElements"
                )
            end

            local line = c:GetControl( "Line" )
            line:GetControl( "Left" ):SetRepresentation( found and "default" or "red" )
            line:GetControl( "Fill" ):SetRepresentation( found and "default" or "red" )
            line:GetControl( "Right" ):SetRepresentation( found and "default" or "red" )
        end

        if not boss then i = i + 1 else break end
    end

    local label = root:GetControl( "Monsters/RequiredElements/Label" )
    if requiredElementsCount > 0 then
        local text = screen:GetText( "TEXT_PREMISSION_REQUIRED_ELEMENTS" )
        local idx = 1
        for e, _ in pairs( requiredElements ) do
            text = text .. ElementCharsMap[e]
            registry:Set( "/internal/missingelements/" .. idx, e )
            idx = idx + 1
        end
        registry:Set( "/internal/missingelements/" .. idx, 0 )

        label:SetText( text )

        anim:Add(
            Perpetual:New(
                function( t )
                    label:SetAlpha( ( math.sin( t*6 ) + 1 )*128 )
                end
            ),
            nil,
            "PremissionElements"
        )

        root:GetControl( "Monsters/Frame/Fill" ):SetRepresentation( "red" )
        root:GetControl( "Monsters/Frame/Bottom/Left" ):SetRepresentation( "red" )
        root:GetControl( "Monsters/Frame/Bottom/Fill" ):SetRepresentation( "red" )
        root:GetControl( "Monsters/Frame/Bottom/Right" ):SetRepresentation( "red" )
    else
        label:SetAlpha( 255 )
        label:SetText( screen:GetText( "TEXT_PREMISSION_GO" ) )
        registry:Set( "/internal/missingelements/1", 0 )

        root:GetControl( "Monsters/Frame/Fill" ):SetRepresentation( "default" )
        root:GetControl( "Monsters/Frame/Bottom/Left" ):SetRepresentation( "default" )
        root:GetControl( "Monsters/Frame/Bottom/Fill" ):SetRepresentation( "default" )
        root:GetControl( "Monsters/Frame/Bottom/Right" ):SetRepresentation( "default" )
    end
end

function PremissionUpdateElemetns(item, elementsRoot)
    local label = elementsRoot:GetControl( "Label" )
    local text = screen:GetText( "TEXT_ELEMENTS2" ) .. " "

    local hasElements = false
    local currentLvlIdx = Shop:GetUpgrades( item.id ) + 1
    local maxIdx = math.min( #item.upgrades.Elements, currentLvlIdx )
    for i=1,maxIdx do
        local element = item.upgrades.Elements[i]
        if element > 0 then
            hasElements = true
            text = text .. ElementCharsMap[element]
        end
    end

    elementsRoot:SetVisibility( hasElements )
    if hasElements then
        label:SetText( text )
    end
end

function PremissionShow( dontResetBackup )
    Lock()

    callback:Playhaven( survival and "pre_mission_survival" or "pre_mission_story" )
    root:SetAlpha( 0 )
    root:SetVisibility( true )
    PremissionInitWeapons()
    PremissionInitEnemyIcons()
    
    MissionsUpdateToClaimCounter( root:GetPath() .. "/Buttons/Missions/Button/ToClaim" )

    if dontResetBackup ~= true then
        RefreshBackupSlot()

        if friendLock and registry:Get( "/app-config/friendlock/enabled" ) then
            FriendLockShow()
        end
    else
        if IsWeaponSelectedByUser( ShopItem.Mech ) then
            RefreshBackupSlot()
        else
            if backupFriend and ( not IsWeaponSelectedByUser( backupFriend.UsedWeaponId ) ) then
                LoadWeaponAtlas( backupFriend.UsedWeaponId )
            end
        end
    end
    SwitchBackupSlot( not IsWeaponSelectedByUser( ShopItem.Mech ) )
   
    FuelShopCallback = function() PremissionHide() end

    OnPremissionShow()
    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( SmoothStep( t )*255 )
        end),
        function()
            active = true
            Unlock()
        end
    )
end

function PremissionHide( callback, goToMap )
    Lock()
    active = false

    FuelShopCallback = nil

    TutorialTooltipHide( "/TutorialFriendLock" )

    if goToMap == nil or goToMap == true then
        OnPremissionHide()
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( 255 - SmoothStep( t )*255 )
        end),
        function()
            active = false
            root:SetVisibility( false )
            if callback then callback() end

            Unlock()
        end
    )
end

function RefreshBackupSlot( backupMember )

    local avatar = missionPopup:GetControl( "Friend/Avatar" )
    local addButton = missionPopup:GetControl( "Friend/Add")
    local removeButton = missionPopup:GetControl( "Friend/Remove")
    local requestLabel = missionPopup:GetControl( "Friend/RequestBackupLabel")
    requestLabel:SetX( avatar:GetX() + ( avatar:GetWidth() - requestLabel:GetWidth() ) / 2 )

    local vipframe = missionPopup:GetControl( "Friend/VIP1" )
    local vipbadge = missionPopup:GetControl( "Friend/VIP2" )
    vipbadge:SetX( avatar:GetX() + ( avatar:GetWidth() - vipbadge:GetControl("Badge"):GetWidth() ) / 2 )


    local nameImg = missionPopup:GetControl( "Friend/PlayerName")

    if backupMember == nil and backupFriend ~= nil then
        if not IsWeaponSelectedByUser( backupFriend.UsedWeaponId ) then
            ReleaseWeaponAtlas( backupFriend.UsedWeaponId )
        end
    end

    backupFriend = backupMember

    local fl = friendLock and backupFriend == nil and registry:Get( "/app-config/friendlock/enabled" )
    root:GetControl( "Buttons/Play" ):SetVisibility( not fl )
    root:GetControl( "Buttons/FriendLock" ):SetVisibility( fl )

    if backupFriend == nil then
        -- MorningTec Begin
        addButton:SetVisibility( false )
        addButton:SetTouchable( false )
        -- addButton:SetVisibility( true )
        -- addButton:SetTouchable( true )
        -- MorningTec End
        removeButton:SetVisibility( false )
        removeButton:SetTouchable( false )
        avatar:GetGraphic( "TS" ):SetImage( avatarDefultPath )
        avatar:GetGraphic( "TS" ):SetScale( 1 )
        avatar:SetAlpha( 128 )
        requestLabel:SetVisibility( true )
        nameImg:SetVisibility( false )
        friend = false
        vipframe:SetVisibility( false )
        vipbadge:SetVisibility( false )
    else
        callback:ResistanceSetUsernameImg( nameImg:GetPath() , backupFriend.Name )
        local w, h = nameImg:GetGraphic( "TS" ):GetSize()
        local maxWidth = removeButton:GetWidth() + R(6) 

        if w > maxWidth then
            nameImg:GetGraphic( "TS" ):SetScale( maxWidth/w , 1 )
        else
            nameImg:GetGraphic( "TS" ):SetScale( 1 )
        end

        nameImg:SetX( avatar:GetX() + ( avatar:GetWidth() - nameImg:GetWidth() ) / 2 )
        nameImg:SetY( 0 )
        nameImg:SetVisibility( true )

        requestLabel:SetVisibility( false )
        addButton:SetVisibility( false )
        addButton:SetTouchable( false )
        removeButton:SetVisibility( true )
        removeButton:SetTouchable( true )

        local avatarPath = GetAvatarImgPath( backupFriend )
        if avatarPath == avatarDefultPath then
            avatar:GetGraphic( "TS" ):SetImage( avatarDefultPathPremission )
            avatar:GetGraphic( "TS" ):SetScale( 1 )
        else
            SetAvatarImg( avatar , avatarPath )
        end
        avatar:SetAlpha( 255 )
        friend = true
        if not IsWeaponSelectedByUser( backupFriend.UsedWeaponId )then
            LoadWeaponAtlas( backupFriend.UsedWeaponId )
        end

        local isUserVip = ( tonumber( backupFriend.IsVip ) == 1 )
        vipframe:SetVisibility( isUserVip )
        vipbadge:SetVisibility( isUserVip )
        vipframe:SetY( avatar:GetY() )
        vipbadge:SetY( avatar:GetY() + avatar:GetHeight() )
        vipbadge:GetControl("Badge"):SetY( - ( vipbadge:GetControl("Badge"):GetHeight()/2 ) )
    end
end

function StartLevelSelected()
    callback:StartGame( level.f )
end

function IsWeaponSelectedByUser( weaponId )
    local isUsed = false

    local maxWeaponsSelected = #weaponsRoots


    for idx=1,maxWeaponsSelected do
        local v = registry:Get( "/monstaz/weaponselection/" .. idx )
        if v == "" then
            break
        else
            if v == weaponId then
                isUsed = true
                break
            end
        end
    end

    return isUsed
end

function SwitchBackupSlot( enable )

    local avatar = missionPopup:GetControl( "Friend/Avatar" )
    local addButton = missionPopup:GetControl( "Friend/Add")
    local removeButton = missionPopup:GetControl( "Friend/Remove")
    local requestLabel = missionPopup:GetControl( "Friend/RequestBackupLabel")

    addButton:SetTouchable( enable )

    if enable == true then
        avatar:SetAlpha(255)
        addButton:SetAlpha(255)
    else
        avatar:SetAlpha(90)
        addButton:SetAlpha(90)
    end
end

function ForceBackupFriend( weaponId )

    Friend = {
        Type = ItemType[1],
        UniqueId = "Wilson",
        Name = "Wilson",
        Level = 17,
        FbId = "",
        Kills = 124,
        CompletedEvents = 12,
        UsedWeaponId = weaponId,
        UsedWeaponUpgrade = 5,
        CanSendGifts = "120",
        CanHelp = "120"
    }

    RefreshBackupSlot( Friend )
end

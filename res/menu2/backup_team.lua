require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/backup_item.lua" )
require( "menu2/backup_list.lua" )
require( "ResistanceDB.lua" )
require( "menu2/timeout.lua" )

local active = false
local syncing = false
local timeoutIndicatorActive = false

local x = 0
local y = 0

local width = SCREEN_WIDTH - R(10)
local height = SCREEN_HEIGHT - R(44)


local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local popup = nil
local rootTimeout = nil

local dot1 = nil
local dot2 = nil
local dot3 = nil


local sideMargin = R(35)
local bottomMargin = R(2)

function BackupTeamKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        BackupTeamHide( function() PremissionShow( true ) PopupUnlock() end )
        return true
    end

    return false
end

function BackupTeamAction( name )

    if not active then
        return
    end

    if string.find( name, "/BackupTeam/Back/Button" ) then
        BackupTeamHide( function() PremissionShow( true ) PopupUnlock() end )
    elseif string.find( name, "/BackupTeam/TimeoutRoot/Refresh/Button" ) then
        BackupTimeoutIndicatorHide( callback:ResistanceUpdateAll() )
    end

end

function BackupTeamPress( control, path )

    if not active then
        return
    end

    if string.find( path,"/BackupTeam/Back/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "/BackupTeam/TimeoutRoot/Refresh/Button" ) then
        ButtonPress( control )
    end

end

function BackupTeamShow( resetScroll )
    active = true

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    if UpdateInfo.Friends > 0 then
        TimeoutIndicatorHide( nil, true )
        UpdateBackupMember()
        BackupListSetContent( BackupItems , resetScroll )
        popup:GetControl("SyncIndicator"):SetVisibility( false ) 
        BackupListShowPosIndicator()
        syncing = false
    elseif UpdateInfo.Friends < 0 then
        popup:GetControl("SyncIndicator"):SetVisibility( false ) 
        BackupListDropAllItems()
        BackupListHidePosIndicator()
        BackupTimeoutIndicatorShow()
    else
        BackupTimeoutIndicatorHide( nil, true )
        BackupTeamSyncingStart()
    end

    OnBackupTeamShow()

    popup:SetVisibility( true )

    anim:Add( Slider:New( 0.3,
            function( t )
                popup:SetAlpha( SmoothStep( t ) * 255 )
            end),
            nil
        )
end

function BackupTeamHide( callback )
    active = false
    OnBackupTeamHide()
    anim:Add( Slider:New( 0.3,
        function( t )
             popup:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end
        ),
        function()
            if callback then callback() end
            popup:SetVisibility( false )
        end
    )
end

function BackupTeamInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/backup_team.xml" )

    popup = screen:GetControl( "/BackupTeam" )

    x = ( SCREEN_WIDTH - width )/2
    y = 0

    popup:SetX( x )
    popup:SetY( y )
    popup:SetVisibility( false )

    -- Frame
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    -- Help button
    HelpButtonInit( popup , width )

    -- Back button
    local btnBack = popup:GetControl( "Back" ) 
    btnBack:SetY( height + bottomMargin  )
    btnBack:SetX( sideMargin )

    --Sync indicator init
    popup:GetControl( "SyncIndicator"):SetX( width / 2 )
    popup:GetControl( "SyncIndicator"):SetY( height / 2 )

    dot1 = popup:GetControl( "SyncIndicator/SyncIndicatorDot1" )
    dot2 = popup:GetControl( "SyncIndicator/SyncIndicatorDot2" )
    dot3 = popup:GetControl( "SyncIndicator/SyncIndicatorDot3" )

    local w, h = dot1:GetGraphic( "TS" ):GetSize()
    local margin = R ( 5 )
    dot1:SetX( - margin - w - w/2)
    dot2:SetX( -w/2 )
    dot3:SetX( margin + w - w/2)

    --List backup
    local backupListWidth = width - R(40)
    local backupListHeight = BackupConsts.itemHeight

    local listRoot = popup:GetControl( "BackupTeamRoot")
    local listYstart = ( (height - BackupConsts.itemHeight ) /2 ) + R(5)
    listRoot:SetY( listYstart )

    -- Header
    local header = popup:GetControl("BackupTeamHeader")
    header:SetX( ( width - header:GetWidth() )/2 )
    header:SetY( listYstart - header:GetHeight() - R(2) )

    BackupListInit( listRoot , backupListWidth, backupListHeight )

    rootTimeout = popup:GetControl( "TimeoutRoot" )
    FillTimeoutIndicator( popup , width , height )
    rootTimeout:SetVisibility( false )
    
end

function BackupTeamUpdate( dt )
    if not active then return end

    if UpdateInfo.Friends > 0 and IsBackupTeamSyncing() then
        EndBackupTeamSyncingSuccess()
    end

    if not IsBackupTeamSyncing() and UpdateInfo.Friends == 0  then
        if timeoutIndicatorActive == true then
            BackupTimeoutIndicatorHide( function() BackupTeamSyncingStart() end )
        else
            BackupTeamSyncingStart()
        end
    end

    if UpdateInfo.Friends < 0 and IsBackupTeamSyncing() then
        EndBackupTeamSyncingFail()
        BackupTimeoutIndicatorShow()
    end

    BackupListUpdate( dt )
end

function BackupTeamTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    BackupListTouchDown( x, y, id, c )

    local path = c:GetPath()

    if string.find(path, "/Buttons/") then
        --ButtonPress( c )
    end
end

function BackupTeamTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    BackupListTouchUp( x, y, id, c )
end

function BackupTeamTouchMove( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    BackupListTouchMove( x, y, id, c )
end

function IsBackupTeamActive()
    return active
end

function IsBackupTeamSyncing()
    return syncing 
end

function EndBackupTeamSyncingSuccess()

    syncing = false
    BackupTeamSyncIndicatorHide()
    BackupTimeoutIndicatorHide( nil,true )

    local root = popup:GetControl( "BackupTeamRoot")
    root:SetVisibility( false )
    BackupListDropAllItems()
    UpdateBackupMember()
    BackupListSetContent( BackupItems )

    root:SetAlpha( 0 )
    root:SetVisibility( true )
    anim:Add( Slider:New(0.3,
        function( t )
            root:SetAlpha( SmoothStep( t ) * 255 )
        end,
        BackupListShowPosIndicator()
        )
    )
end

function EndBackupTeamSyncingFail()
    BackupTeamSyncIndicatorHide()
    syncing = false
end

function BackupTeamSyncIndicatorShow()

    local indicator  = popup:GetControl("SyncIndicator")

    indicator:SetVisibility( true )
    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "BackupTeamPopup" )
    anim:Add( Slider:New( (time or 0.3) * (255-startAlpha)/255,
        function( t )
            indicator:SetAlpha( startAlpha + SmoothStep( t ) * (255-startAlpha) )
        end
        ),
        nil,
        "BackupTeamSyncPopup"
    )

    anim:RemoveAll( "BackupTeamSyncBlink" )

    local lt = 0
    local timer = 0
    local speed = 5
    anim:Add( Perpetual:New(
        function( t )
            local dt = t - lt
            lt = t
            if ( timer > 3) then 
                timer = 0
            else
                timer = timer + speed*dt
            end
            
            if timer <= 1 then
                dot1:SetRepresentation( "active" )
            else
                dot1:SetRepresentation( "default" )
            end
            
            if timer >= 1 and timer <= 2 then
                dot2:SetRepresentation( "active" )
            else
                dot2:SetRepresentation( "default" )
            end
            
            if timer >= 2 and timer <= 3 then
                dot3:SetRepresentation( "active" )
            else
                dot3:SetRepresentation( "default" )
            end
        end
        ),
        nil,
        "BackupTeamSyncBlink"
    )
end

function BackupTeamSyncIndicatorHide( callback )

    local indicator  = popup:GetControl("SyncIndicator")

    indicator:SetVisibility( true )

    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "BackupTeamSyncPopup" )
    anim:RemoveAll( "BackupTeamSyncBlink" )
    anim:Add( Slider:New( (time or 0.3) * startAlpha/255,
        function( t )
            indicator:SetAlpha( startAlpha - SmoothStep( t ) * startAlpha )
        end
        ),
        function()
            indicator:SetVisibility( false )
        end,
        "BackupTeamSyncPopup"
    )

    if callback then callback() end
end

function BackupTeamSyncingStart()
    BackupListDropAllItems()
    BackupListHidePosIndicator()
    syncing = true
    BackupTeamSyncIndicatorShow()
end

function BackupTimeoutIndicatorShow()
    BackupListDropAllItems()
    timeoutIndicatorActive = true
    rootTimeout:SetAlpha( 0 )
    rootTimeout:SetVisibility( true ) 

    anim:Add( Slider:New( 0.3,
            function( t )
            rootTimeout:SetAlpha( SmoothStep( t ) * 255 )
        end),
             function() StartStarAnimation( rootTimeout:GetControl( "Icon/Star" ) ) end
        )
end

function BackupTimeoutIndicatorHide( callback, instant )

    if timeoutIndicatorActive == false then return end

    timeoutIndicatorActive = false

    if instant == true then
        StopStarAnimation( rootTimeout:GetControl( "Icon/Star" )  )  
        rootTimeout:SetVisibility( false ) 
        if callback then callback() end
    else
        anim:Add( Slider:New( 0.3,
                    function( t )
                    rootTimeout:SetAlpha( 255 - SmoothStep( t ) * 255 )
                end ),
                    function() 
                        StopStarAnimation( rootTimeout:GetControl( "Icon/Star" )  )  
                        rootTimeout:SetVisibility( false )  
                        if callback then callback() end
                    end
                )
    end
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )

local segmentsNum = 0
local root = nil
local itemW = 0
local x = 0
local spacing = R(-7)
local lastOffset = -1
local maxOffset = -1
local activeSegment = 0

local maxNumberSegments = 10

function BackupScrollIndicatorInit( parent, maxScrollOffset, itemWidth )
    itemW = itemWidth
    
    root = parent:GetControl( "ScrollIndicator" )
    if root == nil then
        root = parent:InsertControl( "ScrollIndicator" )
        root:SetRelative( true )
    end
    
    root:SetRelative( true )
    
    BackupScrollIndicatorUpdateMaxScrollOffset( maxScrollOffset )
    BackupScrollIndicatorUpdateScrollOffset( 0 )
end

function BackupScrollIndicatorUpdateMaxScrollOffset( maxScrollOffset )
    if maxOffset ~= maxScrollOffset then
        local newSegmentsNum = math.floor( maxScrollOffset / itemW ) + 1
            
        if newSegmentsNum == 1 and maxScrollOffset > 0 then
           newSegmentsNum = 2
        end

        if newSegmentsNum < maxNumberSegments then

            if newSegmentsNum ~= segmentsNum then
                BackupScrollIndicatorSetSegments( newSegmentsNum )
            end
            
        else
            BackupScrollIndicatorSetSegments( maxNumberSegments )
        end

        maxOffset = maxScrollOffset

    end
end

function BackupScrollIndicatorUpdateScrollOffset( scrollOffset )
    if lastOffset ~= scrollOffset then
        lastOffset = scrollOffset
        
        local newActiveSegment = 1

        if maxOffset > 0 then
            newActiveSegment = 1 + math.floor((scrollOffset / maxOffset) * (segmentsNum - 1) + 0.5)

            if newActiveSegment > maxNumberSegments then
                newActiveSegment = 1 + math.floor((scrollOffset / maxOffset) * (maxNumberSegments) + 0.5)
            end
        end

        if newActiveSegment ~= activeSegment then
            if activeSegment > 0 and activeSegment <= segmentsNum then
                root:GetChildren()[activeSegment]:GetChildren()[1]:SetRepresentation( "default" )
            end
            root:GetChildren()[newActiveSegment]:GetChildren()[1]:SetRepresentation( "active" )
            activeSegment = newActiveSegment
        end
    end
end

function BackupScrollIndicatorSetSegments( newSegmentsNum )
    if newSegmentsNum > segmentsNum and newSegmentsNum < maxNumberSegments + 1 then
        for i=1,newSegmentsNum - segmentsNum do
            local segmentRoot = root:InsertControl( tostring(segmentsNum + i) )
            segmentRoot:SetRelative( true )
            segmentRoot:InsertFromXml( "menu2/backup_scroll_indicator.xml" )
            segmentRoot:SetX( x )
            x = x + segmentRoot:GetChildren()[1]:GetGraphic( "TS" ):GetSize() + spacing
        end
    elseif newSegmentsNum < segmentsNum then
        for idx, segment in ipairs( root:GetChildren() ) do
            if idx > newSegmentsNum then
                x = x - (segment:GetChildren()[1]:GetGraphic( "TS" ):GetSize() + spacing)
                segment:Remove()
            end
        end
    end

    segmentsNum = newSegmentsNum

    if segmentsNum == 1 then
        root:SetVisibility( false )
    else
        root:SetVisibility( true )
    end
end
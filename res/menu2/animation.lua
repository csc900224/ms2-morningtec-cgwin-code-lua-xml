AnimList = {}

local isUpdating = false
local delayedRemove = { l = {}, a = {} }

local function Remove( what, where )
    local remove_list = {}
    for i,v in ipairs( where ) do
        if v.n == what then
            table.insert( remove_list, i )
        end
    end
    for i,v in ipairs( remove_list ) do
        table.remove( where, v - i + 1 )
    end
end

local function DelayedRemove( what, where )
    table.sort( what )
    local dec = 0
    local last = -1
    for _,v in ipairs( what ) do
        if v ~= last then
            table.remove( where, v - dec )
            last = v
            dec = dec + 1
        end
    end
end

function AnimList:New()
    local obj = { list = {}, add_list = {} }
    setmetatable( obj, { __index = AnimList } )
    return obj
end

function AnimList:Update( dt )
    isUpdating = true

    if #delayedRemove.l ~= 0 or #delayedRemove.a ~= 0 then
        DelayedRemove( delayedRemove.l, self.list )
        DelayedRemove( delayedRemove.a, self.add_list )
        delayedRemove.l = {}
        delayedRemove.a = {}
    end
    
    for i,v in ipairs( self.add_list ) do
        table.insert( self.list, v )
    end
    self.add_list = {}

    local remove_list = {}
    for i,v in ipairs( self.list ) do
        v.a:Play( dt )
        if v.a:IsFinished() then
            table.insert( remove_list, { i = i, c = v.c, n = v.n } )
        end
    end

    local dec = 0
    for i,v in ipairs( remove_list ) do
        table.remove( self.list, v.i - dec )
        if v.c ~= nil then
            v.c( v.n )
        end
        dec = dec + 1
    end

    isUpdating = false
end

function AnimList:Add( anim, callback, name )
    table.insert( self.add_list, { n = name, a = anim, c = callback } )
end

function AnimList:HasAnim( name )
    for i,v in ipairs( self.list ) do
        if v.n == name then
            return true
        end
    end
    
    for i,v in ipairs( self.add_list ) do
        if v.n == name then
            return true
        end
    end

    return false
end

function AnimList:RemoveAll( name )
    if isUpdating or #delayedRemove.l > 0 or #delayedRemove.a > 0 then
        assert( name )
        for i,v in ipairs( self.list ) do
            if v.n == name then
                table.insert( delayedRemove.l, i )
            end
        end
        for i,v in ipairs( self.add_list ) do
            if v.n == name then
                table.insert( delayedRemove.a, i )
            end
        end
    else
        if not name then
            self.list = {}
            self.add_list = {}
            return
        end

        Remove( name, self.list )
        Remove( name, self.add_list )
    end
end

anim = AnimList:New()

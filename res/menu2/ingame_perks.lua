require( "PerkDB.lua" )

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil
local perkarea = nil

local tip = false

local x = 0
local y = 0
local width = R( 304 )
local height = R( 213 )
local widthIcon = R( 52 )

local perks = {}
local selected = 0

local dancepower = 1

function PerksMenuInit( parent )
    parent:InsertFromXml( "menu2/ingame_perks.xml", 0 )
    popup = parent:GetControl( "Perks" )
    perkarea = screen:GetControl( "/PerkArea" )

    y = ( SCREEN_HEIGHT - height )/2

    popup:SetX( -width )
    popup:SetY( y )
    perkarea:SetY( y )

    local ico = popup:GetControl( "Icon" ):GetGraphic( "TS" )
    anim:Add(
        Perpetual:New(
            function( t )
                local s = math.sin( t * 8 )
                s = s * s
                s = s * s
                ico:SetScale( 1 + ( s + s*s ) * 0.33 * dancepower )
                ico:SetAngle( math.sin( t * 8.41 ) * math.pi * 0.025 * dancepower )
            end
            ) )
end

function PerksMenuUpdate( dt )
    if not TutorialActive and not active and not tip and callback:GetNumPerks() > 1 then
        TutorialTooltipShow( "/Perks", "Tip", "TEXT_PERK_MENU_TIP", popup:GetControl( "Icon" ):GetX() + widthIcon/2, popup:GetControl( "Icon" ):GetY(), TutorialAnchor.Left, 0.5, true )
        tip = true
    end
end

function PerksMenuAction( path )
    if not active then
        return
    end

    local item = select( 3, string.find( path, "Perks/Items/(%d+)/Bg" ) )
    if item then
        PerksMenuItemSelected( tonumber( item ) )
    end
end

function PerksMenuItemSelected( index )
    selected = index

    local item = popup:GetControl( "Items/" .. index )
    local bg = item:GetControl( "BgSel" )
    bg:SetVisibility( true )
    anim:Add(
        Slider:New( 0.1,
            function( t )
                bg:SetAlpha( SmoothStep( t )*255 )
            end
        )
    )

    local perk = perks[index]
    callback:LogPerkName( "Perk " .. perk.header )
    callback:PerkSelected( perk.perk )

    table.remove( PerkPool, perk.i )

    if perk.func then
        perk.func()
    end

    TutorialAction( item:GetPath() )

    PerksMenuHide()
    AudioManager:Play( Sfx.SFX_MENU_SELECT )
end

function PerksMenuSetup()
    selected = 0

    local pool = {}
    for i, v in ipairs( PerkPool ) do
        v.i = i
        table.insert( pool, v )
    end

    for i = 1, 4 do
        local index = math.random( #pool )
        local perk = pool[index]
        table.remove( pool, index )

        local item = popup:GetControl( "Items/" .. i )
        item:GetControl( "Icon" ):GetGraphic( "s" ):SetImage( perk.icon )
        item:GetControl( "Header" ):SetText( perk.header )
        item:GetControl( "Description" ):SetText( perk.text )

        table.insert( perks, i, perk )
    end
end

function PerksMenuShow()
    active = true
    perkarea:SetTouchable( false )

    TutorialOnPerkMenuShow()

    local perks = callback:GetNumPerks()
    local dance = perks == 1 and 1 or 0.8

    local target = width - widthIcon
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            local ss = SmoothStep( t )
            popup:SetX( ss * ( x + target ) - target )
            dancepower = 1.0 - ss * dance
        end
        )
    )

    if tip then
        TutorialTooltipHide( "/Perks/Tip" )
        tip = false
    end

    AudioManager:Play( Sfx.SFX_MENU_POPUP_LONG )
end

function PerksMenuHide()
    active = false

    local perks = callback:GetNumPerks()
    local target = perks == 0 and width or width - widthIcon

    anim:Add(
        Delay:New( 0.2 ),
        function()
            anim:Add(
                Slider:New( 0.3,
                    function( t )
                        popup:SetX( x - ( x + target ) * SmoothStep( t ) )
                    end
                ),
                function()
                    popup:GetControl( "Items/" .. selected .. "/BgSel" ):SetVisibility( false )
                    if perks == 0 then
                        popup:SetVisibility( false )
                    end
                    callback:StopPerkMenu()
                    perkarea:SetTouchable( true )

                    TutorialOnPerkMenuHide()
                end
            )
            if perks ~= 0 then
                anim:Add(
                    Slider:New( 0.3,
                        function( t )
                            dancepower = 0.2 + SmoothStep( t ) * 0.8
                        end
                ) )
            end
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_LONG_CLOSE )
end

function PerksMenuGetPosition()
    return x, y
end

function PerksMenuGetSize()
    return width, height
end

function PerksMenuGetIconWidth()
    return widthIcon
end

function PerksMenuShowNotification()
    dancepower = 1
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.1,
        function( t )
            popup:SetX( SmoothStep( t ) * ( x + widthIcon ) - width )
        end
        )
    )
end

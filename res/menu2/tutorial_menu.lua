require( "menu2/tutorial_finger.lua" )

local ShopButtonPath = "/Menu/ShopButton"
local ShotgunBuyPath = "/Shop/ShopRoot/ItemsRoot/ShopItemList/Items/shotgun/ShopItem/Buttons/Buy"
local ShotgunBuyConfirmPath = "/Shop/ShopRoot/ItemsRoot/ShopItemList/Items/shotgun/ShopItem/Buttons/Confirm"
local ShotgunBuyOkPath = "/ShopSuccessPopup/OK/Button"
local BackPath = "/Shop/ShopRoot/Buttons/Back"
local AreaButtonPath = "/Content/Map/View/Map-01/Areas/Area-01/Indicator/Icon/Touch"
local AreaButtonGlowPath = "/Content/Map/View/Map-01/Areas/Area-01/Indicator/Icon/Glow"
local AreaButtonSkullPath = "/Content/Map/View/Map-01/Areas/Area-01/Indicator/Icon/Skull"
local PremissionFirstWeaponSlotPath = "/Premission/Premission/Weapon01/Window"
local PremissionFirstWeaponFramePath = "/Premission/Premission/Weapon01/Window/TutorialFrame"
local PremissionSecondWeaponSlotPath = "/Premission/Premission/Weapon02/Window"
local PremissionSecondWeaponFramePath = "/Premission/Premission/Weapon02/Window/TutorialFrame"
local PremissionEquipShotgunPath = "/Premission/WeaponSelect/ItemsRoot/ShopItemList/Items/shotgun/ShopItem/Buttons/SelectSmall"
local PremissionEnemyIconPath = "/Premission/Premission/Monsters/Icons/1/Icon"
local PremissionUpgradeSmgPath = "/Premission/WeaponSelect/ItemsRoot/ShopItemList/Items/smg/ShopItem/Buttons/UpgradeSmall"
local PremissionUpgradeSmgConfirmPath = "/Premission/WeaponSelect/ItemsRoot/ShopItemList/Items/smg/ShopItem/Buttons/ConfirmSmall"
local PremissionUpgradeSmgBackPath = "/Premission/WeaponSelect/Buttons/Back"
local PremissionPlayPath = "/Premission/Premission/Buttons/Play"
local OutroPopupClosePath = "/TutorialPopup/Close"

ShopActions = {
    {
        path = ShopButtonPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ShopEnter ) end,
        completed = function() end
    },
    {
        path = ShotgunBuyPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ShopBuyShotgun ) end,
        completed = function() end
    },
    {
        path = ShotgunBuyConfirmPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ShopBuyShotgun ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ShopBuyShotgun ) end
    },
    {
        path = ShotgunBuyOkPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ShopBuyShotgunPopup ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ShopBuyShotgunPopup ) end
    },
    {
        path = BackPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ShopExit ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ShopExit ) end
    },
}

local PremissionActions = {
    {
        path = AreaButtonPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionSelect ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.PremissionSelect ) end
    },
    {
        path = PremissionSecondWeaponSlotPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionSecondWeaponSlot ) end,
        completed = function() TutorialTooltipHide( "/TutorialPremissionSecondWeaponSlot" ) end
    },
    {
        path = PremissionEquipShotgunPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionEquipShotgun ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.PremissionEquipShotgun ) end
    },
    {
        path = PremissionFirstWeaponSlotPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionFirstWeaponSlot ) end,
        completed = function()
            TutorialTooltipHide( "/TutorialPremissionFirstWeaponSlot1" )
            TutorialTooltipHide( "/TutorialPremissionFirstWeaponSlot2" )
        end
    },
    {
        path = PremissionUpgradeSmgPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionUpgradeSmg ) end,
        completed = function() end
    },
    {
        path = PremissionUpgradeSmgConfirmPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionUpgradeSmg ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.PremissionUpgradeSmg ) end
    },
    {
        path = PremissionUpgradeSmgBackPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionUpgradeSmgBack ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.PremissionUpgradeSmgBack ) end
    },
    {
        path = PremissionPlayPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.PremissionPlay ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.PremissionPlay ) end
    },
}

local ChallengesActions = {
    {
        path = AreaButtonPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ChallengesSelectArea ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ChallengesSelectArea ) end
    },
    {
        path = PremissionPlayPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.ChallengesPlay ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ChallengesPlay ) end
    },
}

local MechActions = {
    {
        path = AreaButtonPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.MechSelectArea ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.MechSelectArea ) end
    },
    {
        path = PremissionPlayPath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.MechPlay ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.MechPlay ) end
    },
}

local OutroActions = {
    {
        path = OutroPopupClosePath,
        allowed = function() return TutorialManager:IsTaskActive( TutorialTask.OutroGoodbye ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.OutroGoodbye ) end
    }
}

local Actions = {}
Actions[TutorialChapter.Shop] = ShopActions
Actions[TutorialChapter.Premission] = PremissionActions
Actions[TutorialChapter.Challenges] = ChallengesActions
Actions[TutorialChapter.Mech] = MechActions
Actions[TutorialChapter.Outro] = OutroActions

function TutorialIsActionAllowed( path )
    local chapter = TutorialManager:GetCurrentChapter()
    local actions = Actions[chapter]
    if actions then
        local action = actions[1]
        if action and action.path == path and action.allowed() then
            return true
        end	
    end

    return false
end

function TutorialAction( path )
    local chapter = TutorialManager:GetCurrentChapter()
    local actions = Actions[chapter]
    if actions then
        local action = actions[1]
        if action and action.path == path and action.allowed() then
            action.completed()
            table.remove( actions, 1 )
        end
    end
end

-- Tutorial events

function TutorialOnShop()
    if TutorialManager:GetCurrentChapter() == TutorialChapter.Shop and TutorialManager:IsTaskActive( TutorialTask.ShopEnter ) then
        TutorialManager:SetTaskCompleted( TutorialTask.ShopEnter )
    end
end

function TutorialOnWeaponSelection()
    if TutorialManager:GetCurrentChapter() == TutorialChapter.Premission then
        if TutorialManager:IsTaskActive( TutorialTask.PremissionSecondWeaponSlot ) then
            TutorialManager:SetTaskCompleted( TutorialTask.PremissionSecondWeaponSlot )
        elseif TutorialManager:IsTaskActive( TutorialTask.PremissionFirstWeaponSlot ) then
            TutorialManager:SetTaskCompleted( TutorialTask.PremissionFirstWeaponSlot )
        end
    end
end

-- Tutorial tasks

function TutorialShopEnter()
    local button = screen:GetControl( ShopButtonPath )
    local x = button:GetX()
    local y = button:GetY() + button:GetHeight()/2

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialFingerShow( ShopButtonPath, "TutorialFinger", button:GetWidth()/2, button:GetHeight()/4, 0, true )
    TutorialTooltipShow( "/Menu", "Tutorial", "TEXT_TUTORIAL_25", x, y, TutorialAnchor.Right, 0.25, true )
end

function TutorialShopEnterCompleted()
    local overlay = screen:GetControl( ShopButtonPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialFingerHide( ShopButtonPath .. "/TutorialFinger" )
    TutorialTooltipHide( "/Menu/Tutorial" )
end

function TutorialShopBuyShotgun()
    local button = screen:GetControl( ShotgunBuyPath )
    local x = button:GetAbsX()
    local y = button:GetAbsY() + button:GetHeight()/2

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "BuyShotgun", "TEXT_TUTORIAL_27", x, y, TutorialAnchor.Right, 0.25, true, 0, R(130) )
end

function TutorialShopBuyShotgunCompleted()
    local overlay = screen:GetControl( ShotgunBuyPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/BuyShotgun" )
end

function TutorialShopBuyShotgunPopup()
    local button = screen:GetControl( ShotgunBuyOkPath )
    local x = button:GetX() + button:GetWidth()/2
    local y = button:GetY() + button:GetHeight()

    TutorialTooltipShow( "/ShopSuccessPopup/OK", "BuyShotgunPopup", "TEXT_TUTORIAL_28", x, y, TutorialAnchor.Top, 0.25, true )
end

function TutorialShopBuyShotgunPopupCompleted()
    TutorialTooltipHide( "/ShopSuccessPopup/OK/BuyShotgunPopup" )
end

function TutorialShopExit()
    local button = screen:GetControl( BackPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + button:GetHeight()

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialShopExit", "TEXT_TUTORIAL_31", x, y, TutorialAnchor.Top, 0.5, true )
end

function TutorialShopExitCompleted()
    local overlay = screen:GetControl( BackPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/TutorialShopExit" )
end

function TutorialPremissionSelect()
    local button = screen:GetControl( AreaButtonGlowPath )
    local skull = screen:GetControl( AreaButtonSkullPath )
    local x = button:GetAbsX() + skull:GetWidth()
    local y = button:GetAbsY()

    TutorialFingerShow( AreaButtonSkullPath, "TutorialFinger", skull:GetWidth()/2, -skull:GetHeight()/2 )
    TutorialTooltipShow( "/", "TutorialPremissionSelect", "TEXT_TUTORIAL_32", x, y, TutorialAnchor.Left, 0.25, true )
end

function TutorialPremissionSelectCompleted()
    TutorialFingerHide( AreaButtonSkullPath .. "/TutorialFinger" )
    TutorialTooltipHide( "/TutorialPremissionSelect" )
end

function TutorialPremissionSecondWeaponSlot()
    local slot = screen:GetControl( PremissionSecondWeaponSlotPath )
    local icon = slot:GetControl( "Icon" )
    local x = slot:GetAbsX() + icon:GetX()
    local y = slot:GetAbsY() + 2*icon:GetY()

    local frame = screen:GetControl( PremissionSecondWeaponFramePath )
    frame:SetAlpha( 0 )
    frame:SetVisibility( true )

    anim:Add(
        Perpetual:New(
            function( t )
                local s = math.sin( 4 * t )
                s = s * s
                frame:SetAlpha( 255 * s )
            end
        ),
        nil,
        PremissionSecondWeaponFramePath
    )

    TutorialFingerShow( PremissionSecondWeaponSlotPath, "TutorialFinger", R(50), R(50) )
    TutorialTooltipShow( "/", "TutorialPremissionSecondWeaponSlot", "TEXT_TUTORIAL_33", x, y, TutorialAnchor.Top, 0.3, true )
end

function TutorialPremissionSecondWeaponSlotCompleted()
    local frame = screen:GetControl( PremissionSecondWeaponFramePath )
    frame:SetVisibility( false )

    anim:RemoveAll( PremissionSecondWeaponFramePath )

    TutorialFingerHide( PremissionSecondWeaponSlotPath .. "/TutorialFinger" )
end

function TutorialPremissionEquipShotgun()
    local button = screen:GetControl( PremissionEquipShotgunPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + button:GetHeight()

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialPremissionEquipShotgun", "TEXT_TUTORIAL_34", x, y, TutorialAnchor.Top, 0, true )
end

function TutorialPremissionEquipShotgunCompleted()
    TutorialTooltipHide( "/TutorialPremissionEquipShotgun" )
end

function TutorialPremissionFirstWeaponSlot()
    do
        local enemy = screen:GetControl( PremissionEnemyIconPath )
        local x = enemy:GetAbsX() - R(2)
        local y = enemy:GetAbsY() + enemy:GetWidth()/2

        TutorialTooltipShow( "/", "TutorialPremissionFirstWeaponSlot1", "TEXT_TUTORIAL_42", x, y, TutorialAnchor.Right, 0.5, true, 0, R(235), true )
    end

    do
        local slot = screen:GetControl( PremissionFirstWeaponSlotPath )
        local icon = slot:GetControl( "Icon" )
        local x = slot:GetAbsX() + icon:GetX() - icon:GetWidth()/2
        local y = slot:GetAbsY() + icon:GetY()

        TutorialFingerShow( PremissionFirstWeaponSlotPath, "TutorialFinger", R(50), R(50), 2.5 )
        TutorialTooltipShow( "/", "TutorialPremissionFirstWeaponSlot2", "TEXT_TUTORIAL_43", x, y, TutorialAnchor.Right, 3, true, 0, R(200) )
    end

    do
        local frame = screen:GetControl( PremissionFirstWeaponFramePath )
        frame:SetAlpha( 0 )
        frame:SetVisibility( true )

        anim:Add(
            Delay:New( 3 ),
            function()
                anim:Add(
                    Perpetual:New(
                        function( t )
                            local s = math.sin( 4 * t )
                            s = s * s
                            frame:SetAlpha( 255 * s )
                        end
                    ),
                    nil,
                    PremissionFirstWeaponFramePath
                )
            end,
            PremissionFirstWeaponFramePath
        )
    end
end

function TutorialPremissionFirstWeaponSlotCompleted()
    local frame = screen:GetControl( PremissionFirstWeaponFramePath )
    frame:SetVisibility( false )

    anim:RemoveAll( PremissionFirstWeaponFramePath )

    TutorialFingerHide( PremissionFirstWeaponSlotPath .. "/TutorialFinger" )
end

function TutorialPremissionUpgradeSmg()
    local button = screen:GetControl( PremissionUpgradeSmgPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + button:GetHeight()

    button:GetControl( "TutorialOverlay" ):SetVisibility( false )
    button:GetControl( "Icon/TutorialOverlay" ):SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialPremissionUpgradeSmg", "TEXT_TUTORIAL_44", x, y, TutorialAnchor.Top, 0, true )
end

function TutorialPremissionUpgradeSmgCompleted()
    screen:GetControl( PremissionUpgradeSmgPath .. "/TutorialOverlay" ):SetVisibility( true )
    screen:GetControl( PremissionUpgradeSmgPath .. "/Icon/TutorialOverlay" ):SetVisibility( true )

    TutorialTooltipHide( "/TutorialPremissionUpgradeSmg" )
end

function TutorialPremissionUpgradeSmgBack()
    local button = screen:GetControl( PremissionUpgradeSmgBackPath )
    local x = button:GetAbsX()
    local y = button:GetAbsY() + button:GetHeight()/2

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialPremissionUpgradeSmgBack", "TEXT_TUTORIAL_45", x, y, TutorialAnchor.Right, 0.25, true )
end

function TutorialPremissionUpgradeSmgBackCompleted()
    local overlay = screen:GetControl( PremissionUpgradeSmgBackPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/TutorialPremissionUpgradeSmgBack" )
end

function TutorialPremissionPlay()
    local button = screen:GetControl( PremissionPlayPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY()

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialPremissionPlay", "TEXT_TUTORIAL_35", x, y, TutorialAnchor.Bottom, 0.25, true )
end

function TutorialPremissionPlayCompleted()
    local overlay = screen:GetControl( PremissionPlayPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/TutorialPremissionPlay" )
end

function TutorialChallengesSelectArea()
    local button = screen:GetControl( AreaButtonGlowPath )
    local skull = screen:GetControl( AreaButtonSkullPath )
    local x = button:GetAbsX() + skull:GetWidth()
    local y = button:GetAbsY()

    TutorialFingerShow( AreaButtonSkullPath, "TutorialFinger", skull:GetWidth()/2, -skull:GetHeight()/2 )
    TutorialTooltipShow( "/", "TutorialChallengesSelectArea", "TEXT_TUTORIAL_38", x, y, TutorialAnchor.Left, 0.5, true )
end

function TutorialChallengesSelectAreaCompleted()
    TutorialFingerHide( AreaButtonSkullPath .. "/TutorialFinger" )
    TutorialTooltipHide( "/TutorialChallengesSelectArea" )
end

function TutorialChallengesPlay()
    local button = screen:GetControl( PremissionPlayPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY()

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialChallengesPlay", "TEXT_TUTORIAL_39", x, y, TutorialAnchor.Bottom, 0.5, true )
end

function TutorialChallengesPlayCompleted()
    TutorialTooltipHide( "/TutorialChallengesPlay" )
end

function TutorialMechSelectArea()
    local button = screen:GetControl( AreaButtonGlowPath )
    local skull = screen:GetControl( AreaButtonSkullPath )
    local x = button:GetAbsX() + skull:GetWidth()
    local y = button:GetAbsY()

    TutorialFingerShow( AreaButtonSkullPath, "TutorialFinger", skull:GetWidth()/2, -skull:GetHeight()/2 )
    TutorialTooltipShow( "/", "TutorialMechSelectArea", "TEXT_TUTORIAL_40", x, y, TutorialAnchor.Left, 0.5, true )
end

function TutorialMechSelectAreaCompleted()
    TutorialFingerHide( AreaButtonSkullPath .. "/TutorialFinger" )
    TutorialTooltipHide( "/TutorialMechSelectArea" )
end

function TutorialMechPlay()
    do
        local slot = screen:GetControl( PremissionFirstWeaponSlotPath )
        local icon = slot:GetControl( "Icon" )
        local x = slot:GetAbsX()
        local y = slot:GetAbsY() + icon:GetY()

        TutorialTooltipShow( "/", "TutorialMechPlay1", "TEXT_TUTORIAL_46", x, y, TutorialAnchor.Right, 0.5, true, 0, R(200), true )
    end

    do
        local button = screen:GetControl( PremissionPlayPath )
        local x = button:GetAbsX() + button:GetWidth()/2
        local y = button:GetAbsY()

        local overlay = button:GetControl( "TutorialOverlay" )
        overlay:SetVisibility( false )

        TutorialTooltipShow( "/", "TutorialMechPlay2", "TEXT_TUTORIAL_41", x, y, TutorialAnchor.Bottom, 3, true )
    end
end

function TutorialMechPlayCompleted()
    TutorialTooltipHide( "/TutorialMechPlay" )
end

function TutorialOutroGoodbye()
    TutorialPopupInit( screen:GetControl( "/" ) )
    TutorialPopupSetCloseCallback(
        function()
            anim:Add(
                Delay:New( 0.25 ),
                function()
                    MapNextView( true )
                end
            )
        end
    )

    TutorialPopupShow()
end

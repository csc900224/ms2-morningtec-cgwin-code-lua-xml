require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/math.lua" )
require( "menu2/popup.lua" )
require( "menu2/slider.lua" )
require( "MissionDB.lua" )
require( "menu2/missions/ranks.lua" )
require( "menu2/missions/group.lua" )
require( "menu2/missions/scroll.lua" )

local MissionsLayoutConstants = {
    layoutMargin = {
        top = Y(5),
        bottom = R(5),
        left = R(5),
        right = R(5)
    },
    achievementsMargin = R(10),
    missionsWidth = SCREEN_WIDTH * 1,
    groupsSpacing = R(5)
}

local root = nil
local active = false
local backAction = nil
local scroll = nil
local refreshed = false
local ingame = false

function MissionsInit( parent, fromIngame )
    parent:InsertFromXml( "menu2/missions/missions.xml" )
    root = parent:GetControl( "MissionsRoot" )
    ingame = fromIngame

    MissionRanksInit( root:GetControl( "Ranks" ) )
    MissionsLayout()
    
    scroll = MissionsScroll:new()
    local scrollRoot = root:GetControl("MissionsScroll")
    scroll:Init( scrollRoot, SCREEN_WIDTH, SCREEN_HEIGHT - scrollRoot:GetY(), MissionsLayoutConstants.groupsSpacing )
    scroll:SetContent( MissionsGetCurrentMissions(), true )
        
    root:SetVisibility( false )
end

function MissionsGetCurrentMissions()
    local currentMissions = {}
    
    -- Daily Set
    local dailyMissions = {}
    local dailyGroup = MissionManager:GetGroup( "DailyMissions" )
    for _, subgroup in ipairs( dailyGroup:GetGroups() ) do
        for _, mission in ipairs( subgroup:GetMissions() ) do
            if mission:GetStatus() ~= MissionStatus.S_DISABLED then
                table.insert( dailyMissions, mission )
            end
        end
    end
    table.insert( currentMissions, dailyMissions )
    
    -- Main Missions
    local mainGroup = MissionManager:GetGroup( "MainMissions" )
    MissionsCollectCurrentMissions( mainGroup, currentMissions )

    return currentMissions
end

function MissionsCollectCurrentMissions( group, missionsTable )
    local missions = group:GetMissions()
    if #missions > 0 then
        if Groups[group:GetPath()].type ~= "Sequence" then
            table.insert( missionsTable, missions )
        else
            local found = false
            for _, mission in ipairs( missions ) do
                local status = mission:GetStatus()
                if status == MissionStatus.S_ACTIVE or status == MissionStatus.S_COMPLETED then
                    table.insert( missionsTable, mission )
                    found = true
                end
            end
            if found == false then
                if missions[#missions]:GetStatus() == MissionStatus.S_REWARDED then
                    table.insert( missionsTable, missions[#missions] )
                else
                    table.insert( missionsTable, missions[1] )
                end
            end
        end
    else
        local sequenceMissions = {}
        for _, group in ipairs( group:GetGroups() ) do
            if Groups[group:GetPath()].type ~= "Sequence" then
                MissionsCollectCurrentMissions( group, missionsTable )
            else
                MissionsCollectCurrentMissions( group, sequenceMissions )
            end
        end
        if #sequenceMissions > 0 then
            table.insert( missionsTable, sequenceMissions )
        end
    end
end

function MissionsKeyDown( code )
    if not active then return end
    if code == keys.KEY_ESCAPE then
        MissionsHide()
        return true
    end
    return false
end

function MissionsTouchDown( x, y, id )
    if not active then return end

    local c = screen:GetTouchableControl( x, y )
    scroll:TouchDown( x, y, id, c )
    if not c then return end

    local path = c:GetPath()
    MissionsPress( c, path )
end

function MissionsTouchUp( x, y, id )
    if not active then return end
    
    local c = screen:GetTouchableControl( x, y )    
    scroll:TouchUp( x, y, id, c )    
    if not c then return end
    
    local path = c:GetPath()
    MissionsAction( c, path )
end

function MissionsTouchMove( x, y, id )
    if not active then return end
    scroll:TouchMove( x, y, id )
end

function MissionsPress( control, path )
    if not active then return end
    
    if string.find( path, "/MissionsRoot/Buttons/Back" ) or ( string.find( path, "/MissionsRoot/Buttons/Achievements" ) and not ingame ) then
        ButtonPress( control )
    else
        MissionPress( control, path )
    end
end

function MissionsAction( c, path )
    if not active then return end
    
    if string.find( path, "/MissionsRoot/Buttons/Back" ) then
        TutorialAction( path )
        MissionsHide()
    elseif string.find( path, "/MissionsRoot/Buttons/Achievements" ) then
        if not ingame then
            OpenAchievementsUI()
        end
    else
        MissionAction( c, path )
    end
end

function MissionsShow( backCallback, ignoreBackAction )
    if active then return end
    callback:Playhaven( "missions" )
    
    active = true
    root:SetAlpha( 0 )
    root:SetVisibility( true )
    
    if not ignoreBackAction then
        backAction = backCallback
    end

    -- if IS_ANDROID and not IS_AMAZON then
        -- local component = root:GetControl( "Buttons/Achievements" )
        -- if IsGpUser() then
            -- component:SetVisibility( true )
        -- else
            -- component:SetVisibility( false )
        -- end
    -- end
    
    -- chinese build achievements disabled
    local component = root:GetControl( "Buttons/Achievements" )
    component:SetVisibility( false )

    MissionRanksRefresh( true )
    
    scroll:UpdateContent( MissionsGetCurrentMissions(), true, false )
    
    local delay = OnMissionsShow()

    FuelShopCallback = function() MissionsHide() end
    
    anim:Add( Delay:New( delay ),
        function()
            anim:Add( Slider:New( 0.3,
                function( t )
                    root:SetAlpha( SmoothStep( t )*255 )
                end)
            )
        end
    )
end

function MissionsHide( callback, goToMap, ignoreBackAction )
    active = false

    FuelShopCallback = nil
    
    if goToMap == nil or goToMap == true then
        OnMissionsHide()
    end
    
    if ignoreBackAction ~= true and backAction then
        backAction()
    end
    
    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( 255 - SmoothStep( t )*255 )
        end),
        function()
            root:SetVisibility( false )
            if callback then callback() end
        end
    )
end

function MissionsLayout()
    -- Back button
    local component = root:GetControl( "Buttons/Back" )
    component:SetY( MissionsLayoutConstants.layoutMargin.top )
    w, h = component:GetGraphic( "TS" ):GetSize()
    component:SetX( SCREEN_WIDTH - MissionsLayoutConstants.layoutMargin.right - w )
    local backButtonBottom = component:GetY() + h

    component = root:GetControl( "Buttons/Achievements" )
    -- OpenAchievementsUI button
    -- if IS_ANDROID and not IS_AMAZON then
        -- component:SetY( MissionsLayoutConstants.achievementsMargin )
        -- w, h = component:GetGraphic( "TS" ):GetSize()
        -- component:SetX( MissionsLayoutConstants.achievementsMargin )
        -- if ingame then
            -- component:SetAlpha( 100 )
        -- else
            -- component:SetAlpha( 255 )
        -- end
    -- else
        -- component:SetVisibility( false )
    -- end
    --chinese build achievements disabled
    component:SetVisibility( false )

    -- Ranks
    component = root:GetControl( "Ranks" )
    component:SetY( backButtonBottom )
    local ranksBottom = component:GetY() + MissionRanksGetHeight()

    -- Missions 
    component = root:GetControl( "MissionsScroll" )
    component:SetY( ranksBottom )
    component:SetX( (SCREEN_WIDTH - MissionsLayoutConstants.missionsWidth) / 2 )
end

function MissionsUpdate( dt )
    if not active then return end
    scroll:Update( dt )
    refreshed = false
end

function MissionsRefresh()
    if refreshed == false then
        scroll:UpdateContent( MissionsGetCurrentMissions(), false, true )
        refreshed = true
    end
end

function MissionsUpdateToClaimCounter( counterPath )
    local counter = screen:GetControl( counterPath )
    if counter then
        local toClaim = math.min( MissionManager:GetMissionsNumToClaim(), 99 )
        counter:SetVisibility( toClaim > 0 )
        counter:GetControl( "Counter" ):SetTextRaw( tostring( toClaim ) )
    end
end

function OpenAchievementsUI()
    callback:OpenAchievementsUI()
end

function IsGpUser()
    local isGp = false
    local isLogin = registry:Get( "/internal/loggedIn" )
    if isLogin then
        isGp = string.len(registry:Get( "/monstaz/resistance/fbId" )) > 2 and string.sub(registry:Get( "/monstaz/resistance/fbId" ),0,2) == "gp"
    end

    return isGp
end

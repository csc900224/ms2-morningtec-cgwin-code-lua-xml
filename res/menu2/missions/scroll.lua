require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/missions/group.lua" )
require( "ItemDB.lua" )

MissionsScroll = {
    root = nil,
    clip = nil,
    width = nil,
    height = nil,
    items = nil,
    itemsSpacing = 0,
    unlockingIdx = 0,
    firstItemOffset = R(4),
    
    Groups = {},

    ScrollData = {
        touchId = -1,
        minScrollDist = R(5),
        maxScrollOffset = 0,
        scrollOffset = 0,
        sampleTouchPos = 0,
        currentTouchPos = 0,
        startScrollOffset = 0,
        touchStart = 0,
        scrolling = false,
        targetScrollOffset = 0,
        scrollAcceleration = 1,
        samplingInterval = 0.025,
        sampleTimer = -1,
        velocityEpsilon = 0.1,
        velocity = 0,
        lastVelocity = 0,
        draggTime = 0
    }
}

function MissionsScroll:new (o)
    o = o or {}   -- create object if user does not provide one
    o.ScrollData = {}
    o.Groups = {}

    setmetatable(o, self)
    setmetatable(o.ScrollData, self.ScrollData)
    setmetatable(o.Groups, self.Groups)
    
    self.__index = self
    self.ScrollData.__index = self.ScrollData
    self.Groups.__index = self.Groups

    return o
end

function MissionsScroll:Init( parent, listWidth, listHeight, itemSpacing )
    parent:InsertFromXml( "menu2/missions/scroll.xml" )
    self.clip = parent:GetControl( "MissionsScroll" )
    self.root = parent:GetControl( "MissionsScroll/Items" )
    self.width = listWidth
    self.height = listHeight

    if itemSpacing ~= nil then
        self.itemsSpacing = itemSpacing
    end 

    self.clip:GetGraphic( "r" ):SetWidth( self.width )
    self.clip:GetGraphic( "r" ):SetHeight( self.height )
    
    self:CreateGradient()
end

function MissionsScroll:CreateGradient()
    local ls = self.clip:InsertControl("ls")
    ls:AddRepresentation( "default", "menu2/timage.xml" )
    ls:SetRelative( true )
    ls:SetVisibility( true )
    ls:GetGraphic( "TS" ):SetAngle( 1.57 )
    ls:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    local lsw, lsh = ls:GetGraphic( "TS" ):GetSize()

    ls:SetY( -R(2) )
    ls:SetX( self.width * 1.5 )

    ls:GetGraphic( "TS" ):SetScale( 1 , ( self.width * 2 ) / lsh  )
end

function MissionsScroll:UpdateContent( groups, resetScroll, animated )
    if resetScroll == nil or resetScroll == true then
        self:ResetScroll()
    end
    
    for idx, group in ipairs( self.Groups ) do
        local lastGroupState = group.state
        local nextState = group:ChangeMissions( groups[idx], animated )
        if animated and nextState ~= lastGroupState and nextState == MissionGroupState.Completed and idx < #self.Groups then
            -- Group was completed
            self:StartUnlockAnim( idx + 1, groups )
            break
        end
    end
    if animated == false then
        self:SortGroups()
    end
    self:ClipItems()
end

function MissionsScroll:StartUnlockAnim( idx, groups )
    self.unlockingIdx = idx
    
    -- End finger scrolling process
    if self.ScrollData.touchId >= 0 then
        self:TouchUp( 0, 0, self.ScrollData.touchId, nil )
    end
     
    local startOffset = self.ScrollData.scrollOffset
    local targetOffset = self.Groups[idx].root:GetControl(".."):GetY()
    anim:Add( Slider:New( 1.0, 
        function( t )
            -- Scroll to new group
            self.ScrollData.targetScrollOffset = self:ClampOffset( startOffset - (startOffset - targetOffset + self.firstItemOffset) * SmoothStep(t) )
            self.ScrollData.scrollOffset = self.ScrollData.targetScrollOffset
            self.root:SetY( -self.ScrollData.scrollOffset )
            self:ClipItems()
        end),
        function()
            -- Wait a moment
            anim:Add( Delay:New( 0.15 ),
                function()
                    anim:Add( Slider:New( 0.15, 
                        function( t )
                            -- Fade out
                            self.Groups[idx].root:SetAlpha( 255 - SmoothStep(t) * 255 )
                        end),
                        function()
                            -- Continue group update
                            for i, group in ipairs( self.Groups ) do
                                if i >= idx then
                                    group:ChangeMissions( groups[i], animated )
                                end
                            end
                            anim:Add( Slider:New( 0.3, 
                                function( t )
                                    -- Fade in
                                    self.Groups[idx].root:SetAlpha( SmoothStep(t) * 255 )
                                end),
                                function()
                                    -- Finished
                                    self.unlockingIdx = 0
                                end
                             )
                        end
                    )
                end
            )
        end
    )
end

function MissionsScroll:SetContent( groups, resetScroll )
    if resetScroll == nil or resetScroll == true then
        self:ResetScroll()
    end
    self:DropAllItems()

    if groups then
        for idx, missions in ipairs( groups ) do
            local itemRoot = self.root:InsertControl( tostring(idx) )
            itemRoot:SetRelative( true )
            groupObj = MissionsGroup:new()
            groupObj:Init( itemRoot, self.width, idx - 1, missions )
            
            table.insert( self.Groups, groupObj )
            table.insert( self.items, itemRoot )
        end
    end
    local y = self:SortGroups()
    self.ScrollData.maxScrollOffset = math.max( y - self.itemsSpacing - self.height, 0 )
    self:ClipItems()
end

function MissionsScroll:ResetScroll()
    self.ScrollData.scrollOffset = 0
    self.root:SetY( 0 )
    self.ScrollData.scrolling = false
    self.ScrollData.touchId = -1
    self.ScrollData.velocity = 0
    self.ScrollData.lastVelocity = 0
    self.ScrollData.sampleTimer = -1
    self.ScrollData.draggTime = 0
    self.ScrollData.targetScrollOffset = 0
    self.ScrollData.scrollOffset = 0
    self:ClipItems()
end

function MissionsScroll:Update( dt )
    if self.ScrollData.touchId >= 0 then
        self.ScrollData.draggTime = self.ScrollData.draggTime + dt
    end

    self:SampleVelocity( dt )
    self:UpdateScrolling( dt )
  
    for _, group in ipairs( self.Groups ) do
        group:Update( dt )
    end
end

function MissionsScroll:SampleVelocity( dt )
    if self.ScrollData.touchId >= 0 and self.ScrollData.sampleTimer >= 0 then
        self.ScrollData.sampleTimer = self.ScrollData.sampleTimer - dt
        if self.ScrollData.sampleTimer < 0 then
            self.ScrollData.sampleTimer = self.ScrollData.samplingInterval
            self.ScrollData.lastVelocity = (self.ScrollData.currentTouchPos - self.ScrollData.sampleTouchPos) / self.ScrollData.samplingInterval
            self.ScrollData.sampleTouchPos = self.ScrollData.currentTouchPos
        end
    end
end

function MissionsScroll:TouchDown( x, y, id, c )
    if self.items == nil or c == nil or self.unlockingIdx > 0 then return end
    
    local path = c:GetPath()
    if string.find( path, "/MissionsScroll" ) then
        if self.ScrollData.touchId == id then
            self:TouchUp( x, self.ScrollData.currentTouchPos, id, nil )
        end

        if self.ScrollData.touchId < 0 then
            self.ScrollData.touchId = id
            self.ScrollData.touchStart = y
            self.ScrollData.scrolling = false
            self.ScrollData.velocity = 0
            self.ScrollData.lastVelocity = 0
            self.ScrollData.sampleTimer = self.ScrollData.samplingInterval
            self.ScrollData.currentTouchPos = y
            self.ScrollData.sampleTouchPos = y
            self.ScrollData.draggTime = 0
        end
    end
end

function MissionsScroll:TouchUp( x, y, id, c )
    if self.items == nil then return end

    local wasScrolling = false
    if self.ScrollData.scrolling and id == self.ScrollData.touchId then
        wasScrolling = true
        self.ScrollData.scrolling = false
        self.ScrollData.touchId = -1
        self.ScrollData.sampleTimer = -1
        self.ScrollData.targetScrollOffset = self:ClampOffset( self.ScrollData.startScrollOffset + self.ScrollData.touchStart - y )

        -- Check if dragg time is shorter than sampling rate. If so recalculate velocity manualny
        if self.ScrollData.draggTime < self.ScrollData.samplingInterval and self.ScrollData.draggTime > 0 then
            self.ScrollData.lastVelocity = (y - self.ScrollData.sampleTouchPos) / self.ScrollData.draggTime
        end
        self.ScrollData.velocity = self.ScrollData.lastVelocity
    elseif id == self.ScrollData.touchId then
        self.ScrollData.touchId = -1
        self.ScrollData.sampleTimer = -1
    end
end

function MissionsScroll:TouchMove( x, y, id )
    if self.items == nil then return end
    if id == self.ScrollData.touchId then
        if not self.ScrollData.scrolling then
            if math.abs( self.ScrollData.touchStart - y ) > self.ScrollData.minScrollDist then
                -- Start real scrolling
                self.ScrollData.touchStart = y
                self.ScrollData.startScrollOffset = self.ScrollData.scrollOffset
                self.ScrollData.scrolling = true
            end
        else
            self.ScrollData.targetScrollOffset = self:ClampOffset( self.ScrollData.startScrollOffset + self.ScrollData.touchStart - y )
            self.ScrollData.currentTouchPos = y
        end
    end
end

function MissionsScroll:ClampOffset( offset )
    return math.min( math.max(offset, 0), self.ScrollData.maxScrollOffset )
end

function MissionsScroll:UpdateScrolling( dt )
    if math.abs(self.ScrollData.velocity) > self.ScrollData.velocityEpsilon then
        self.ScrollData.velocity = self.ScrollData.velocity - self.ScrollData.velocity * self.ScrollData.scrollAcceleration * dt
        self.ScrollData.targetScrollOffset = self:ClampOffset( self.ScrollData.targetScrollOffset - self.ScrollData.velocity * dt )
    end

    if math.abs(self.ScrollData.scrollOffset - self.ScrollData.targetScrollOffset) > self.ScrollData.velocityEpsilon then
        self.ScrollData.scrollOffset = self.ScrollData.targetScrollOffset
        self.root:SetY( -self.ScrollData.scrollOffset )
        self:ClipItems()
    end  
end

function MissionsScroll:ClipItems()
    for _, group in ipairs( self.Groups ) do
        local groupParent = group.root:GetControl("..")
        local groupY = groupParent:GetY()
        local visible = -self.ScrollData.scrollOffset + groupY <= self.height and -self.ScrollData.scrollOffset + groupY + group.height >= 0
        groupParent:SetVisibility( visible )
        
        if visible == true then
            groupY = groupY + group.missionsRoot:GetY()
            for _, mission in ipairs( group.missions ) do
                local missionParent = mission.root:GetControl("..")
                local visible = -self.ScrollData.scrollOffset + groupY + missionParent:GetY() <= self.height and -self.ScrollData.scrollOffset + groupY + missionParent:GetY() + mission:GetHeight() >= 0
                missionParent:SetVisibility( visible )
            end
        end
    end
end

function MissionsScroll:DropAllItems()
    for _, item in ipairs( self.root:GetChildren() ) do
        item:Remove()
    end
    self.items = {}
    self.Groups = {}
end

function MissionsScroll:RefreshList()
    for _, group in ipairs( self.Groups ) do
        local groupParent = group.root:GetControl( ".." )
        local visibility = groupParent:GetVisibility()
        groupParent:SetVisibility( true )
        group:Refresh()
        groupParent:SetVisibility( visibility )
    end
end

function MissionsScroll:SortGroups()
    local completedGroups = {}
    
    local y = self.firstItemOffset
    for _, group in ipairs( self.Groups ) do
        if group.state == MissionGroupState.Completed then
            table.insert( completedGroups, group )
        else
            group.root:GetControl(".."):SetY( y )
            y = y + group.height + self.itemsSpacing
        end
    end
    
    for _, group in ipairs( completedGroups ) do
        group.root:GetControl(".."):SetY( y )
        y = y + group.height + self.itemsSpacing
    end
    return y
end

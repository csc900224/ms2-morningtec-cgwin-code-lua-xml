require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/math.lua" )
require( "menu2/slider.lua" )
require( "menu2/missions/popup_rankup.lua" )
require( "MissionDB.lua" )
require( "deepcopy.lua" )

local root = nil
local bulletsRoot = nil
local bulletsNum = 0
local lastBulletX = 0
local activeBullets = 0
local lastRank = nil

local LayoutConsts = {
    widthRatio = 1.0,
    bulletsSpacing = R(-3),
    bulletFrames = 20,
    bulletFrameTime = 0.033,
    bulletDelay = 0.02,
    bulletSfxDealy = 0.75,
    widthOffset = -R(8)
}

function MissionRanksInit( parent )
    parent:InsertFromXml( "menu2/missions/ranks.xml" )
    root = parent:GetControl( "RanksRoot" )
    bulletsRoot = root:GetControl( "Bullets" )
    
    MissionRanksLayoyt()
end

function MissionRanksLayoyt()
    local width = SCREEN_WIDTH * LayoutConsts.widthRatio + LayoutConsts.widthOffset

    -- Background
    local component = root:GetControl( "Background" ):GetGraphic( "TS" )
    local w, h = component:GetSize()
    component:SetScale( width / w, 1 )
    local bgBottom = h
    
    -- Line
    component = root:GetControl( "Background/Line" )
    local lineCenter = component:GetControl( "Center" )
    local lineRight = component:GetControl( "Right" )
    w, h = lineCenter:GetGraphic( "TS" ):GetSize()
    component:SetY( bgBottom - h/2 )
    lineCenter:GetGraphic( "TS" ):SetScale( width / w, 1 )
    lineRight:SetX( width )    
    
    -- Center horizontaly
    root:SetX( (SCREEN_WIDTH - width) / 2 )
end

function MissionRanksSetBulletsNum( newBulletsNum )
    local bulletW = -1
    if newBulletsNum > bulletsNum then
        for i=1,newBulletsNum - bulletsNum do
            local bullet = bulletsRoot:InsertControl( tostring(bulletsNum + i) )
            bullet:SetRelative( true )
            bullet:InsertFromXml( "menu2/missions/bullet.xml" )
            bullet:SetX( lastBulletX )
            
            local bulletSprite = bullet:GetControl("Bullet"):GetGraphic( "TS" )
            bulletSprite:CloneAnimation()
            bulletSprite:Play( false )
            bulletSprite:SetFrame( 0 )
            if bulletW < 0 then
                bulletW = bullet:GetControl("Bullet"):GetGraphic( "TS" ):GetSize()
            end
            lastBulletX = lastBulletX + bulletW + LayoutConsts.bulletsSpacing
        end
    elseif newBulletsNum < bulletsNum then
        bulletW = bulletsRoot:GetChildren()[1]:GetControl("Bullet"):GetGraphic( "TS" ):GetSize()
        for idx, bullet in ipairs( bulletsRoot:GetChildren() ) do
            if idx > newBulletsNum then
                lastBulletX = lastBulletX - (bulletW + LayoutConsts.bulletsSpacing)
                bullet:Remove()
            end
        end
        if newBulletsNum < activeBullets then
            activeBullets = newBulletsNum
        end
    end
    bulletsNum = newBulletsNum
end

function MissionRanksSetActiveBullets( newActiveBullets, animated )
    if newActiveBullets < activeBullets then
        if animated then
            MissionRanksSetActiveBullets( 0, false )
            MissionRanksSetActiveBullets( newActiveBullets, true )
        else
            for idx, bullet in ipairs( bulletsRoot:GetChildren() ) do
                if idx > newActiveBullets then
                    local sprite = bullet:GetControl("Bullet"):GetGraphic( "TS" )
                    sprite:SetFrame( 0 )
                    sprite:Play( false )
                end
            end
            activeBullets = newActiveBullets
        end
    elseif newActiveBullets > activeBullets then
        if animated then
            local bulletIdx = activeBullets
            for i=1,newActiveBullets - activeBullets do
                anim:Add( Delay:New( LayoutConsts.bulletDelay * LayoutConsts.bulletFrames * i ),
                    function()
                        bulletIdx =  bulletIdx + 1
                        bulletsRoot:GetChildren()[ bulletIdx]:GetControl("Bullet"):GetGraphic( "TS" ):Play( true )
                        anim:Add( Delay:New( LayoutConsts.bulletFrames * LayoutConsts.bulletSfxDealy * LayoutConsts.bulletFrameTime ),
                            function()
                                AudioManager:Play( Sfx.SFX_MENU_MISSION_BULLET )
                            end
                        )
                    end
                )
            end
            activeBullets = newActiveBullets
        else
            for idx, bullet in ipairs( bulletsRoot:GetChildren() ) do
                if idx > activeBullets and idx <= newActiveBullets then
                    local sprite = bullet:GetControl("Bullet"):GetGraphic( "TS" )
                    sprite:SetFrame( LayoutConsts.bulletFrames - 1 )
                    sprite:Play( true )
                end
            end
            activeBullets = newActiveBullets
        end
    end
end

function MissionRanksSetName( textId )
    root:GetControl( "Label/Text" ):SetText( textId )
end

function MissionRanksRefresh( initial, onFinishCallback )
    local group = MissionManager:GetGroup( "Ranks" );
    local ranks = group:GetMissions()
    
    for _, rank in ipairs(ranks) do
        if rank:GetStatus() == MissionStatus.S_ACTIVE then
            local animated = initial == false or initial == nil
            
            if lastRank == nil or lastRank == rank then
                lastRank = rank
                MissionRanksSetRankData( rank, animated )
                if onFinishCallback ~= nil then
                    onFinishCallback()
                end
            elseif lastRank ~= rank then
                local bulletsToEnd = lastRank:GetTarget() - activeBullets
                MissionRanksSetActiveBullets( lastRank:GetTarget(), animated )
                anim:Add( Delay:New( bulletsToEnd * LayoutConsts.bulletDelay * LayoutConsts.bulletFrames + LayoutConsts.bulletFrames * LayoutConsts.bulletFrameTime ),
                    function()
                        AudioManager:Play( Sfx.SFX_LEVELUP )
                        local reward = deepcopy( Missions[lastRank:GetPath()].rewards[1] )
                        if registry:Get( "/monstaz/subscription" ) then
                            if reward.type == "SubscriptionGold" then
                                reward.value = math.floor( reward.value * 1.5 )
                            end
                        end
                        MissionRankupPopupShow( 
                            reward,
                            Missions[rank:GetPath()].title, 
                            function() 
                                lastRank = rank
                                MissionRanksSetRankData( rank, animated ) 
                                if onFinishCallback ~= nil then
                                    onFinishCallback()
                                end
                            end
                        )
                    end
                )
            end
        end
    end
end

function MissionRanksSetRankData( rank, animated )
    MissionRanksSetBulletsNum( rank:GetTarget() )
    MissionRanksSetActiveBullets( rank:GetValue(), animated )
    MissionRanksSetName( Missions[rank:GetPath()].title )
end

function MissionRanksGetHeight()
    local component = root:GetControl( "Background" ):GetGraphic( "TS" )
    local w, h = component:GetSize()
    return h
end

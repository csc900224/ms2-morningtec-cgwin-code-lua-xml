require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/star_animation.lua" )
require( "menu2/background_star.lua" )

local active = false
local popup = nil
local star = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 190 )

local weaponAngle = 0
local itemBought = nil

local originalYayX = -1
local closeCallback = nil

function MissionRankupPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/missions/popup_rankup.xml" )
    popup = screen:GetControl( "/MissionRankup" )
    popup:SetVisibility( false )
    
    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
    
    star = BackgroundStar( popup, width/2, height/2 )
end

function MissionRankupPopupShowFilldata( reward, title )
    -- Rank
    local rankName = popup:GetControl("RankName")
    popup:GetControl("RankName"):SetText( title )
    local rankWidth = popup:GetControl("RankName"):GetTextWidth()
    rankName:SetX( (width - rankWidth) / 2 )
    
    local lineCenter = rankName:GetControl( "Underline/Center" )
    local rX, rY, rW, rH  = lineCenter:GetGraphic( "s" ):GetClipRect()
    lineCenter:GetGraphic( "s" ):SetClipRect( rX, rY, rankWidth, rH )
    rankName:GetControl( "Underline/Right" ):SetX( rankWidth )
    
    -- Reward
    local icon = ""
    if reward.type == "Cash" then
        icon = "TEXT_ICON_CASH"
    elseif reward.type == "Gold" or reward.type == "SubscriptionGold" then
        icon = "TEXT_ICON_GOLD"
    elseif reward.type == "Fuel" then
        icon = "TEXT_ICON_FUEL"
    end
    
    local rewardText = popup:GetControl("Reward")
    rewardText:SetVisibility( true )
    rewardText:SetTextWithVariables( "TEXT_MISSIONS_RANKUP_REWARD", { VALUE = tostring(reward.value), ICON=icon } )
end

function MissionRankupPopupShow( reward, title, onClose )
    if active then return end
    active = true
    closeCallback = onClose
    PopupLock()
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    
    MissionRankupPopupShowFilldata( reward, title )
    
    StartStarAnimation( popup:GetControl( "Star" ) )
    BackgroundStarStart( star, 0.3 )
    AudioManager:Play( Sfx.SFX_MENU_RANK_UP )
end

function MissionRankupPopupKeyDown( code )
    if not active then return end

    if code == keys.KEY_ESCAPE then
        MissionRankupPopupHide(false)
        return true
    end
    return false
end

function MissionRankupPopupPress( control, path )
    if not active then return end

    if path == "/MissionRankup/OK/Button" then
        ButtonPress( control )
    end
end

function MissionRankupPopupAction( name )
    if not active then return end

    if name == "/MissionRankup/OK/Button" then
        MissionRankupPopupHide(false)
    end
end

function MissionRankupPopupHide()
    active = false
    
    if not share then
        PopupUnlock()
    end
    
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Star" ) )
            popup:SetVisibility( false )
            if closeCallback ~= nil then
                closeCallback()
            end
        end
    )

    BackgroundStarStop( star, 0.3 )
end

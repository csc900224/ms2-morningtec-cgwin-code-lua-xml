require( "menu2/common.lua" )
require( "menu2/animation.lua" )
require( "menu2/delay.lua" )
require( "menu2/slider.lua" )
require( "menu2/perpetual.lua" )
require( "menu2/math.lua" )
require( "MissionDB.lua" )

local height = 0
local root = 0

local Time = {
    slideIn = 0.5,
    slideOut = 0.7,
    visibile = 3.0
}

local titleMarginBottom = R( 0 )
local iconOffset = R( 5 )

function MissionNotifierInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/missions/notifier.xml" )
    root = screen:GetControl( "/MissionNotifier" )
    root:SetVisibility( false )

    -- Fetch any pending notifications
    MissionNotifier:OnNotificationFinished()
end

function MissionNotifierMissionCompleted( missionPath )
    if string.find( missionPath, "Ranks/" ) then
        -- Don't notify about ranks
        MissionNotifier:OnNotificationFinished()
    else
        root:SetVisibility( true )
        MissionNotifierSetContent( Missions[missionPath] )
        MissionNotifierStartAnimation()
    end
end

function MissionNotifierSetContent( mission )
    local title = root:GetControl( "Title" )
    title:SetText( mission.title )
    local tW, tH = title:GetSize()

    local reward = root:GetControl( "Reward" )
    reward:SetY( title:GetY() + tH + titleMarginBottom )
    local rW, rH = reward:GetSize()

    local icon = root:GetControl( "Icon" )
    icon:GetGraphic( "s" ):SetImage( mission.icon )
    local iW, iH = icon:GetGraphic( "s" ):GetSize()
    
    local frame = root:GetControl( "Frame" )
    local bgW, bgH = frame:GetGraphic( "TS" ):GetSize()
    height = math.max( math.max( bgH, reward:GetY() + rH ), iconOffset * 2 + iH )

    local fillHeight = height - bgH
    if fillHeight > 0 then
        local fX, fY, fW, fH  = frame:GetControl( "Fill" ):GetGraphic( "s" ):GetClipRect()
        frame:GetControl( "Fill" ):GetGraphic( "s" ):SetClipRect( fX, fY, fW, fillHeight )
    end

    icon:SetY( height / 2 + iconOffset )
    
    local tick = root:GetControl( "Tick" )
    local tW, tH = tick:GetGraphic( "TS" ):GetSize()
    tick:SetY( height - tH )
end

function MissionNotifierStartAnimation()
    anim:Add( Slider:New( Time.slideIn, 
        function( t )
            root:SetY( SCREEN_HEIGHT - height * SmoothStep( t ) )
        end),
        function()
            anim:Add( Delay:New( Time.visibile ),
                function()
                    anim:Add( Slider:New( Time.slideOut, 
                        function( t )
                            root:SetY( SCREEN_HEIGHT - height + SmoothStep( t ) * height )
                        end),
                        function()
                            root:SetVisibility( false )
                            MissionNotifier:OnNotificationFinished()
                        end
                     )
                end
            )
        end
    )
end

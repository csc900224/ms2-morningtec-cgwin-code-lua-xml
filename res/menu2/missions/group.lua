require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/math.lua" )
require( "menu2/slider.lua" )
require( "menu2/missions/mission.lua" )

MissionGroupState = {
    Locked = 1,
    Active = 2,
    Completed = 3
}

MissionsGroup = {
    root = nil,
    label = nil,
    missionsRoot = nil,
    missions = {},
    state = nil,
    frame = nil,
    width = 0,
    height = 0,
    missionsHeight = 0,
    idx = 0
}

local LayoutConstants = {
    headerYOffset = R(5),
    headerMarginBottom = R(5),
    misisonsMarginBottom = R(20),
    missionMargin = R(14)
}

function MissionsGroup:new (o)
    o = o or {}   -- create object if user does not provide one
    o.missions = {}
    
    setmetatable(o, self)
    setmetatable(o.missions, self.missions)
    
    self.__index = self
    self.missions.__index = self.missions
    return o
end

function MissionsGroup:Init( parent, width, idx, missions )
    parent:InsertFromXml( "menu2/missions/group.xml" )
    self.root = parent:GetControl( "GroupRoot" )
    self.label = self.root:GetControl( "Label" )
    self.frame = self.root:GetControl( "Frame" )
    self.missionsRoot = self.root:GetControl( "MissionsRoot" )
    self.width = width
    self.idx = idx
        
    self:CreateMissions( self.root:GetControl( "MissionsRoot" ), missions )   
    self:CalculateLayout()
    
    self:UpdateState()
    self:UpdateLabel()    
        
    PopupCreateFrame( self.frame, self.width, self.height, self:GetFrameAssets( self.state ) )
end

function MissionsGroup:CreateMissions( root, missions )
    self.missionsHeight = 0
    self.missions = {}
    
    for idx, dbMission in ipairs( missions ) do
        local mission = Mission:new()
        local missionRoot = root:InsertControl( tostring(idx) )
        missionRoot:SetRelative( true )
        missionRoot:SetY( self.missionsHeight )
        
        mission:Init( missionRoot, self.width - LayoutConstants.missionMargin * 2, dbMission, idx )
        self.missionsHeight = self.missionsHeight + mission:GetHeight()
        table.insert( self.missions, mission )
    end
end

function MissionsGroup:ChangeMissions( missions, animated )
    local delay = 0
    for idx, dbMission in ipairs( missions ) do
        anim:Add( Delay:New( delay ),
            function()
                self.missions[idx]:ChangeDbMission( dbMission, animated )
            end
        )
    
        if animated and dbMission ~= self.missions[idx].dbMission then
            delay = delay + 0.1
        end
    end
    anim:Add( Delay:New( delay ),
        function()
            self:Refresh()
        end
    )
    return self:GetState( missions )
end

function MissionsGroup:GetFrameAssets( state )
    if state == MissionGroupState.Locked then
        return PopupFrameGray
    elseif state == MissionGroupState.Completed then
        return PopupFrameGreen
    else
        return PopupFrameBlue
    end
end

function MissionsGroup:CalculateLayout()
    self.label:SetTextWidth( self.width )
    self.label:SetY( LayoutConstants.headerYOffset )
    local w, h = callback:GetControlSize( self.label:GetPath() )
    local labelBottom = self.label:GetY() + h + LayoutConstants.headerMarginBottom
        
    self.missionsRoot:SetY( labelBottom )
    self.missionsRoot:SetX( LayoutConstants.missionMargin )
    
    self.height = self.missionsRoot:GetY() + self.missionsHeight + LayoutConstants.misisonsMarginBottom
end

function MissionsGroup:UpdateState()
    local lastState = self.state
    if self.idx == 0 then
        self.state = MissionGroupState.Active
        return lastState ~= self.state
    end
    
    self.state = MissionGroupState.Active
    local completed = true
    for _, mission in ipairs( self.missions ) do
        if mission.dbMission:GetStatus() ~= MissionStatus.S_REWARDED then
            completed = false
        end
        
        if mission.dbMission:GetStatus() == MissionStatus.S_DISABLED then
            self.state = MissionGroupState.Locked
            break
        end
    end
    if completed then
        self.state = MissionGroupState.Completed
    end
    
    return lastState ~= self.state
end

function MissionsGroup:GetState( dbMissions )
    if self.idx == 0 then
        return MissionGroupState.Active
    end
    
    local state = MissionGroupState.Active
    local completed = true
    for _, mission in ipairs( dbMissions ) do
        if mission:GetStatus() ~= MissionStatus.S_REWARDED then
            completed = false
        end
        
        if mission:GetStatus() == MissionStatus.S_DISABLED then
            state = MissionGroupState.Locked
            break
        end
    end
    if completed then
        state = MissionGroupState.Completed
    end
    return state
end

function MissionsGroup:Refresh()
    for _, mission in ipairs( self.missions ) do
        mission:Refresh()
    end
    self:RefreshBackground()
end

function MissionsGroup:RefreshBackground()
    if self:UpdateState() then
        PopupChangeFrame( self.frame, self:GetFrameAssets( self.state ) ) 
        self:UpdateLabel()
    end
end

function MissionsGroup:UpdateLabel()
    if self.idx == 0 then
        self.label:SetText( "TEXT_MISSION_MISSION_GROUP_DAILY" )
    elseif self.state == MissionGroupState.Locked then
        self.label:SetTextWithVariables( "TEXT_MISSION_MISSION_GROUP_STAGE_LOCKED", { STAGE = tostring( self.idx ), PREV_STAGE = tostring( self.idx-1 ) } )
    elseif self.state == MissionGroupState.Completed then
        self.label:SetTextWithVariables( "TEXT_MISSION_MISSION_GROUP_STAGE_COMPLETED", { STAGE = tostring( self.idx ) } )
    else
        self.label:SetTextWithVariables( "TEXT_MISSION_MISSION_GROUP_STAGE", { STAGE = tostring( self.idx ) } )
    end
end

function MissionsGroup:Update( dt )
    for _, mission in ipairs( self.missions ) do
        mission:Update( dt )
    end
end

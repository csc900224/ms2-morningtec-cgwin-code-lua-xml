require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/math.lua" )
require( "menu2/slider.lua" )
require( "MissionDB.lua" )

MissionAssetsBlue = {
    BgFill = "menu2/color_fill_blue_daily.png@linear",
    LineEnd = "menu2/line_01_end.png@linear",
    LineFill = "menu2/line_01_fill.png@linear"
}

MissionAssetsGreen = {
    BgFill = "menu2/color_fill_green_daily.png@linear",
    LineEnd = "menu2/line_01_end_green.png@linear",
    LineFill = "menu2/line_01_fill_green.png@linear"
}

MissionAssetsGray = {
    BgFill = "menu2/color_fill_gray_daily.png@linear",
    LineEnd = "menu2/line_01_end_gray.png@linear",
    LineFill = "menu2/line_01_fill_gray.png@linear"
}

local MissionButtonToMission = {}

local MissionLayout = {
    textX = 0,
    claimMarginR = R(8),
    progressMarginR = R(15)
}

Mission = {
    path = "",
    root = nil,
    bgRoot = nil,
    fgRoot = nil,
    width = 0,
    dbMission = nil,
    listIdx = 0,
    lastAssets = nil,
    timer = nil,
    nextTimerRefresh = 0,
    updateTimer = true,
    refreshMissions = false
}

function Mission:new (o)
    o = o or {}   -- create object if user does not provide one
    setmetatable(o, self)
    self.__index = self
    return o
end

function Mission:Init( parent, width, mission, listIdx )
    parent:InsertFromXml( "menu2/missions/mission.xml" )
    self.root = parent:GetControl( "Mission" )
    self.bgRoot = self.root:GetControl( "Background" )
    self.fgRoot = self.root:GetControl( "Foreground" )
    self.width = width
    self.dbMission = mission
    self.listIdx = listIdx
    
    self:CreateBackground( self:GetBgAssets() )
    self:UpdateLock()
    self:FillData()
    
    MissionButtonToMission[self.fgRoot:GetControl( "Reward/ClaimButton" ):GetPath()] = self
    
    self:CalculateLayout()
    self:UpdateTimer( 1 )
end

function Mission:GetBgAssets()
    if self.dbMission:GetStatus() == MissionStatus.S_DISABLED then
        return MissionAssetsGray
    elseif self.dbMission:GetStatus() == MissionStatus.S_COMPLETED or self.dbMission:GetStatus() == MissionStatus.S_REWARDED then
        return MissionAssetsGreen
    else
        return MissionAssetsBlue
    end
end

function Mission:CreateBackground( assets )
    -- Bg Fill
    local fill = self.bgRoot:InsertControl("Fill")
    fill:AddRepresentation( "default", "menu2/tile.xml" )
    fill:SetRelative( true )
    fill:GetGraphic( "s" ):SetImage( assets.BgFill )
    local fW, fH = fill:GetSize()
    fill:GetGraphic( "s" ):SetClipRect( 0, 0, self.width, fH )
    
    if self.listIdx == 1 then
        -- First item on the list - add top border
        local topLine = self.bgRoot:InsertControl( "LineTop" )
        topLine:SetRelative( true )
        
        local left = topLine:InsertControl( "Left" )
        left:AddRepresentation( "default", "menu2/timage.xml" )
        left:SetRelative( true )
        left:GetGraphic( "TS" ):SetImage( assets.LineEnd )
        left:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
        left:SetX( R(-8) )
        
        local lineFill = topLine:InsertControl( "Fill" )
        lineFill:AddRepresentation( "default", "menu2/tile.xml" )
        lineFill:SetRelative( true )
        lineFill:GetGraphic( "s" ):SetImage( assets.LineFill )
        local lW, lH = lineFill:GetSize()
        lineFill:GetGraphic( "s" ):SetClipRect( 0, 0, self.width, lH )
        
        local righ = topLine:InsertControl( "Right" )
        righ:AddRepresentation( "default", "menu2/timage.xml" )
        righ:SetRelative( true )
        righ:GetGraphic( "TS" ):SetImage( assets.LineEnd )
        righ:SetX( self.width )
        
        topLine:SetY( -lH/2 )
    end
    
    -- Bottom border
    local bottomLine = self.bgRoot:InsertControl( "LineBottom" )
    bottomLine:SetRelative( true )
    
    local left = bottomLine:InsertControl( "Left" )
    left:AddRepresentation( "default", "menu2/timage.xml" )
    left:SetRelative( true )
    left:GetGraphic( "TS" ):SetImage( assets.LineEnd )
    left:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    left:SetX( R(-8) )
    
    local lineFill = bottomLine:InsertControl( "Fill" )
    lineFill:AddRepresentation( "default", "menu2/tile.xml" )
    lineFill:SetRelative( true )
    lineFill:GetGraphic( "s" ):SetImage( assets.LineFill )
    local lW, lH = lineFill:GetSize()
    lineFill:GetGraphic( "s" ):SetClipRect( 0, 0, self.width, lH )
    
    local righ = bottomLine:InsertControl( "Right" )
    righ:AddRepresentation( "default", "menu2/timage.xml" )
    righ:SetRelative( true )
    righ:GetGraphic( "TS" ):SetImage( assets.LineEnd )
    righ:SetX( self.width )
    
    bottomLine:SetY( fH - lH/2 )
    
    self.lastAssets = assets
end

function Mission:UpdateBackground( assets )
    if self.lastAssets == assets then return false end
    self.lastAssets = assets

    local rX, rY, rW, rH  = self.bgRoot:GetControl( "Fill" ):GetGraphic( "s" ):GetClipRect()
    self.bgRoot:GetControl( "Fill" ):GetGraphic( "s" ):SetImage( assets.BgFill )
    self.bgRoot:GetControl( "Fill" ):GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH )

    local topLine = self.bgRoot:GetControl( "LineTop" )
    if topLine ~= nil then
        rX, rY, rW, rH  = topLine:GetControl( "Fill" ):GetGraphic( "s" ):GetClipRect()
        topLine:GetControl( "Fill" ):GetGraphic( "s" ):SetImage( assets.LineFill )
        topLine:GetControl( "Fill" ):GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH )

        topLine:GetControl( "Left" ):GetGraphic( "TS" ):SetImage( assets.LineEnd )
        topLine:GetControl( "Right" ):GetGraphic( "TS" ):SetImage( assets.LineEnd )
    end

    local bottomLine = self.bgRoot:GetControl( "LineBottom" )
    rX, rY, rW, rH  = bottomLine:GetControl( "Fill" ):GetGraphic( "s" ):GetClipRect()
    bottomLine:GetControl( "Fill" ):GetGraphic( "s" ):SetImage( assets.LineFill )
    bottomLine:GetControl( "Fill" ):GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH )

    bottomLine:GetControl( "Left" ):GetGraphic( "TS" ):SetImage( assets.LineEnd )
    bottomLine:GetControl( "Right" ):GetGraphic( "TS" ):SetImage( assets.LineEnd )
    return true
end

function Mission:CalculateLayout()
    local _, _, width, height = self.bgRoot:GetControl( "Fill" ):GetGraphic( "s" ):GetClipRect()
    self.textX = height * 2
    
    -- Lock
    local component = self.root:GetControl( "Lock" )
    local w, h = self.root:GetControl( "Lock" ):GetSize()
    component:SetX( (width + 3*w) /2 )
    component:SetY( (height - h) /2 )
       
    -- Rewards
    local reward = self.fgRoot:GetControl( "Reward" )
    w, h = callback:GetControlSize( reward:GetPath() )
    reward:SetX( width - w - MissionLayout.claimMarginR )
    reward:SetY( (height - h*0.9) / 2 )
    
    -- Progress
    component = self.fgRoot:GetControl( "Progress" )
    w, h = callback:GetControlSize( component:GetPath() )
    component:SetY( (height - h) / 2 )
    component:SetX( reward:GetX() - w - MissionLayout.progressMarginR )
    local progressLeft = reward:GetX() - component:GetTextWidth() - MissionLayout.progressMarginR
    
    -- Texts
    component = self.fgRoot:GetControl( "Texts" )
    component:SetX( self.textX  )
    local textWidth = progressLeft - component:GetX()
    component:GetControl( "Title" ):SetTextWidth( textWidth )
    component:GetControl( "Description" ):SetTextWidth( textWidth )
    
    -- Icons
    local bullets = self.fgRoot:GetControl( "Bullets" )
    local icon = self.fgRoot:GetControl( "Icon" )
    local iconW, iconH = icon:GetSize()    
    icon:SetY( (height - iconH * 0.9) / 2 )
    
    if bullets:GetVisibility() then
        local bulletsW, bullersH = callback:GetControlSize( bullets:GetPath() )
        bullets:SetY( (height - bullersH) / 2 )
        icon:SetX( self.textX * 0.7 - iconW * 0.5  )
        bullets:SetX( self.textX * 0.25 - bulletsW * 0.5 )
    else
        icon:SetX( (self.textX - iconW) / 2 )
    end

    -- Timer
    if self.timer:GetVisibility() then
        local subW, subH = self.timer:GetSize()
        w, h = icon:GetGraphic( "TS" ):GetSize()
        icon:SetY( (height - h - subH * 0.5) / 2 )
        self.timer:SetY( h * 0.9 )
        self.timer:SetX( -w * 0.16 )
    end
end

function Mission:FillData()
    local missionConsts = Missions[self.dbMission:GetPath()]
    self.fgRoot:GetControl( "Texts/Title" ):SetText( missionConsts.title )
    self.fgRoot:GetControl( "Texts/Description" ):SetTextWithVariables( missionConsts.desc, { TARGET = tostring( self.dbMission:GetTarget() ) } )
    
    local progress = self.fgRoot:GetControl( "Progress" )
    progress:SetVisibility( missionConsts.progress ~= nil )
    
    local icon = self.fgRoot:GetControl( "Icon" )
    icon:SetVisibility( missionConsts.icon ~= nil )
    if missionConsts.icon then
        icon:GetGraphic( "TS" ):SetImage( missionConsts.icon )
    end
    self.timer = icon:GetControl( "Subscript" )
    self.timer:SetVisibility( missionConsts.timer == true )
    
    self:FillRewards( missionConsts )
    self:UpdateData()
end

function Mission:FillRewards( consts )
    local bullets = self.fgRoot:GetControl( "Bullets" )
    local claimReward = self.fgRoot:GetControl( "Reward/ClaimButton/Reward" )
    local rewardValue = self.fgRoot:GetControl( "Reward/Reward" )

    bullets:SetVisibility( false )
    claimReward:SetVisibility( false )
    rewardValue:SetVisibility( false )
    
    for _, reward in ipairs( consts.rewards ) do
        if reward.type == "Bullets" then
            bullets:SetVisibility( true )
            for idx, bullet in ipairs( bullets:GetChildren() ) do
                bullet:SetRepresentation( reward.value >= idx and "checked" or "default" )
            end
        else
            self:FillRewardText( claimReward, "TEXT_MISSIONS_CLAIM_REWARD", reward )
            self:FillRewardText( rewardValue, "TEXT_MISSIONS_REWARD", reward )
        end
    end
end

function Mission:FillRewardText( control, text, reward )
    local icon = ""
    if reward.type == "Cash" then
        icon = "TEXT_ICON_CASH"
    elseif reward.type == "Gold" then
        icon = "TEXT_ICON_GOLD"
    elseif reward.type == "Fuel" then
        icon = "TEXT_ICON_FUEL"
    end
    
    control:SetVisibility( true )
    control:SetTextWithVariables( text, { VALUE = tostring(reward.value), ICON=icon } )
end

function Mission:UpdateData()
    local missionConsts = Missions[self.dbMission:GetPath()]
    if missionConsts.progress then
        local target = self.dbMission:GetTarget()
        local value = math.min( self.dbMission:GetValue(), target )
        self.fgRoot:GetControl( "Progress" ):SetTextWithVariables( missionConsts.progress, { TARGET = tostring(target), VALUE = tostring(value) } )
    end
    
    local missionStatus = self.dbMission:GetStatus()
    self.fgRoot:GetControl( "Reward/Reward" ):SetVisibility( missionStatus == MissionStatus.S_DISABLED or missionStatus == MissionStatus.S_ACTIVE )
    self.fgRoot:GetControl( "Reward/ClaimButton" ):SetVisibility( missionStatus == MissionStatus.S_COMPLETED )
    self.fgRoot:GetControl( "Reward/Tick" ):SetVisibility( missionStatus == MissionStatus.S_REWARDED )
end

function Mission:UpdateLock()
    local locked = self.dbMission:GetStatus() == MissionStatus.S_DISABLED
    self.root:GetControl( "Lock" ):SetVisibility( locked )
    self.fgRoot:SetAlpha( locked and 100 or 255 )
end

function Mission:Refresh()
    if self:UpdateBackground( self:GetBgAssets() ) then
        self:UpdateLock()
    end
    self:UpdateData()
end

function Mission:ChangeDbMission( mission, animated )
    if self.dbMission ~= mission then
        if animated == true then
            anim:Add( Slider:New( 0.3, 
                function( t )
                    self.root:SetAlpha( 255 - SmoothStep( t ) * 255 )
                end
                ),
                function()
                    self:SetDbMissionData( mission )
                    anim:Add( Slider:New( 0.3, 
                        function( t )
                            self.root:SetAlpha( SmoothStep( t ) * 255 )
                        end
                        )
                    )     
                end
            )
        else
            self:SetDbMissionData( mission )
        end
    else
        self.updateTimer = true
    end
end

function Mission:SetDbMissionData( mission )
    local visibility = self.root:GetControl(".."):GetVisibility()
    self.root:GetControl(".."):SetVisibility( true )
    
    self.updateTimer = true
    self.dbMission = mission
    self:UpdateBackground( self:GetBgAssets() )
    self:UpdateLock()
    self:FillData()
    self:CalculateLayout()
    
    self.root:GetControl(".."):SetVisibility( visibility )
end

function Mission:Update( dt )
    if self.timer:GetVisibility() then
        self:UpdateTimer( dt )
    end
end

function Mission:UpdateTimer( dt )
    self.nextTimerRefresh = self.nextTimerRefresh - dt
    if self.nextTimerRefresh < 0 then
        local time = MissionManager:GetGroupTimeLeft( MissionManager:GetGroup("DailyMissions") )
        if self.updateTimer == true then
            if time == -1 then
                self.timer:SetTextRaw( "" )
            else
                local h = math.floor(time / 3600)
                local m = math.floor(time / 60) % 60
                local s = math.floor(time) % 60
                local timeString = string.format("%02d:%02d:%02d", h, m, s)
                self.timer:SetTextRaw( timeString )
            end
            self.nextTimerRefresh = 0.15
            
            if math.floor(time) == 0 then 
                self.updateTimer = false
                self.refreshMissions = true
            end
        elseif self.refreshMissions == true and math.floor(time)> 0 then
            MissionsRefresh()
            self.refreshMissions = false
        end
    end
end

function Mission:Action( c, path )
    if string.find( path, "/Reward/ClaimButton" ) then
        self.dbMission:GiveRewards()
        self:Refresh()
        MissionRanksRefresh( false, function() MissionsRefresh() end )
        AudioManager:Play( Sfx.SFX_MONEYDROP )
    end
end

function Mission:GetHeight()
    local w, h = self.bgRoot:GetControl( "Fill" ):GetSize()
    return h
end

function MissionPress( control, path )   
    if string.find( path, "/Reward/ClaimButton" ) then
        ButtonPress( control )
    end
end

function MissionAction( c, path )
    local owner = MissionButtonToMission[path]
    if owner then
        owner:Action( c, path )
    end
end

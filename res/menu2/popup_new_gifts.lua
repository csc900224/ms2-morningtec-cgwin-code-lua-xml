require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/social_item.lua" )
require( "menu2/new_gifts_list.lua" )
require( "menu2/gift_animation.lua" )
require( "ResistanceDB.lua" )

local active = false

local x = R( 0 )
local y = R( 0 )

local width = R( 400 )
local height = R( 250 )

local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local popup = nil
local giftButtonMainMenu = nil

function NewGiftsKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        NewGiftsHide()
        --ConfirmGiftsActionSucces()
        SyncIndicatorShow()
        callback:ResistanceConfirmGifts()
        return true
    end

    return false
end

function NewGiftsInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_new_gifts.xml" )
    giftButtonMainMenu = screen:GetControl( "/Resistance/GiftsButton" )
    popup = screen:GetControl( "/NewGifts" )

    x = ( SCREEN_WIDTH - width )/2 
    y = math.max( (SCREEN_HEIGHT - height )/2 - R(20) ,0 )
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    popup:SetVisibility( false )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    local listWidth = width - R(40)
    local listHeight = NewGiftsConsts.itemHeight--R(140)

    local listRoot = popup:GetControl( "NewGiftsRoot")
    NewGiftsListInit( listRoot, listWidth, listHeight )
    listRoot:SetY( ( height - listHeight ) / 2 )
    
    local margin = R( 2 )
    local confirmBtn = popup:GetControl( "Confirm" )
    confirmBtn:SetX( width - confirmBtn:GetControl( "Button" ):GetWidth() )
    confirmBtn:SetY( height + margin)

    -- header position
    local header = popup:GetControl("NewGiftsText")

    header:SetX( ( width - header:GetWidth() )/2 )
    header:SetY( R(12) )

end

function NewGiftsAction( name )

    if not active then
        return
    end

    if name == "/NewGifts/Confirm/Button" then
        NewGiftsHide()
        --ConfirmGiftsActionSucces()
        SyncIndicatorShow()
        callback:ResistanceConfirmGifts()
    end

end

function NewGiftsPress( control, path )

    if not active then
        return
    end

    if path == "/NewGifts/Confirm/Button" then
        ButtonPress( control )
    end

end

function NewGiftsShow()
    active = true

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    NewGiftsListSetContent( GiftsGroup )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end,
        function() active = true end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function NewGiftsHide( callback )

    local timeDef = 0.3
    local startY = popup:GetY()
    local timer = ( SCREEN_HEIGHT - startY) /  ( SCREEN_HEIGHT - y ) * timeDef

    anim:Add( Slider:New( timer,
        function( t )
            popup:SetY( startY + SmoothStep( t ) * ( SCREEN_HEIGHT - startY ) )
        end
        ),
        function()
            if callback then callback() end
            active = false
            popup:SetVisibility( false )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function NewGiftsUpdate( dt )
    if not active then return end
    NewGiftsListUpdate( dt )
end

function NewGiftsTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    NewGiftsListTouchDown( x, y, id, c )

    local path = c:GetPath()

    if string.find(path, "/Buttons/") then
        --ButtonPress( c )
    end
end

function NewGiftsTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    NewGiftsListTouchUp( x, y, id, c )
end

function NewGiftsTouchMove( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    NewGiftsListTouchMove( x, y, id, c )
end

function IsNewGiftsActive()
    return active
end

function UpdateGiftsButton()
    local giftCount = GetGiftsNumber()
    if giftCount > 0 then
        StartGiftAnimation( giftButtonMainMenu )
        giftButtonMainMenu:SetVisibility( true )
    else
        StopGiftAnimation( giftButtonMainMenu )
        giftButtonMainMenu:SetVisibility( false )
    end
end

function ConfirmGiftsActionSucces()
    SyncIndicatorHide()
    local currentFuelPacks = registry:Get( "/monstaz/resistance/fuelpackcount")
    local s, h = Shop:GetCash()

    local newFuelPacks = GetFuelPacksFromGiftsNumber()
    local newCash = GetCashFromGiftsNumber()
    local newGold = GetGoldFromGiftsNumber()

    --ClawMsg("fp " .. newFuelPacks .." cash " .. newCash .. " gold " .. newGold )
    registry:Set( "/monstaz/resistance/fuelpackcount", currentFuelPacks + newFuelPacks )
    Shop:SetCash( s + newCash, h + newGold )
    callback:Save()
    FlushGifts()
    UpdateGiftsButton()
    PopupUnlock()
end

function ConfirmGiftsActionFailed()
    SyncIndicatorHide()
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT")
end

function IsPopupNewGiftsActive()
    return active
end

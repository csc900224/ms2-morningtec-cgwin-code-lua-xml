require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/requests_list.lua" )
require( "menu2/resistance_requests.lua" )
require( "menu2/resistance_account.lua" )
require( "menu2/timeout.lua" )
require( "ResistanceDB.lua" )

local active = false
local timeoutIndicatorActive = false

local x = R( 10 )
local y = R( 45 )

local width = SCREEN_WIDTH - R(10)
local height = SCREEN_HEIGHT - R(60)

local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local popup = nil
local rootTimeout = nil
local inboxBtn = nil
local inboxCounterValue = 0

function ResistanceHqKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ResistanceHqHide( function() OnResistanceHqHide( true ) end )
        return true
    end

    return false
end

function ResistanceHqInit( screen )

    screen:InsertFromXml( "menu2/resistance_hq.xml" )

    popup = screen:GetControl( "ResistanceHq" )

    x = ( SCREEN_WIDTH - width )/2
    y = R(10)

    popup:SetX( x )
    popup:SetY( y )
    popup:SetVisibility( false )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    -- Help button init
    HelpButtonInitBottomLeft( popup , height )

    -- button positioning
    local backBtn = popup:GetControl("Back")
    local profileBtn = popup:GetControl("Profile")

    inboxBtn = popup:GetControl("Inbox")
    local inviteBtn = popup:GetControl("Invite")

    local bw, bh = backBtn:GetControl("Button"):GetGraphic( "TS" ):GetSize()

    local topMargin = R(18)
    local sideMargin = R(35)
    local bottomMargin = R(6)

    inboxBtn:SetY( topMargin )
    inboxBtn:SetX( sideMargin )

    inviteBtn:SetY( topMargin )
    inviteBtn:SetX( width - sideMargin - bw )

    backBtn:SetY( height + bottomMargin  )
    backBtn:SetX( sideMargin )

    profileBtn:SetY( height + bottomMargin )
    profileBtn:SetX( width - sideMargin - bw )

    -- header position
    local header = popup:GetControl("SocialHeader")

    header:SetX( ( width - header:GetWidth() )/2 )
    header:SetY( bh/2 )
    

    -- invite button label
    local label = inviteBtn:GetControl("Label")
    local lw = label:GetTextWidth()

    local icon = inviteBtn:GetControl("SocialIcon")
    local iw, ih = icon:GetGraphic( "TS" ):GetSize()

    local margin = R(8)
    local wholeWidth = lw + iw + margin

    icon:SetX( (bw - wholeWidth)/2 )
    label:SetX( (bw - wholeWidth)/2 + iw + margin)
    icon:SetY ( ( bh - ih )/2 )

    --inbox info
    FillInboxButtonInfo( inboxBtn , 0 )

    --list item init
    ResistanceTeamInit(  popup:GetControl( "FriendsScreenRoot" ) , width - R(40))

    popup:GetControl( "FriendsScreenRoot" ):SetY(( height - ( FriendsConsts.itemHeight ))/ 2 + R(20))

    --TimeoutRoot item init
    rootTimeout = popup:GetControl( "TimeoutRoot" )
    FillTimeoutIndicator( popup , width , height )
    rootTimeout:SetVisibility( false )

    --local inviteIdCopyBtn = popup:GetControl("Copy")
    local inviteIdLabel = popup:GetControl("InvitationCode")

    inviteIdLabel:SetY( height )
    inviteIdLabel:SetX( (width - inviteIdLabel:GetWidth()) / 2 )
    --inviteIdCopyBtn:SetY( height + inviteIdLabel:GetHeight() )
    --inviteIdCopyBtn:SetX( (width - inviteIdCopyBtn:GetWidth()) / 2 )
end

function ResistanceHqAction( name )
    if not active then
        return
    end

    if string.find( name, "/ResistanceHq/Invite" ) then
        InvitePopupShow()
    elseif string.find( name, "/ResistanceHq/Profile" ) then
        UserLoggedOut()
	   ResistanceHqHide(OnResistanceHqHide( true ))
    elseif string.find( name, "/ResistanceHq/Back" ) then
        ResistanceHqHide( OnResistanceHqHide( true ) )
    elseif string.find( name, "/ResistanceHq/Inbox" ) then
        ResistanceHqHide( ResistanceRequestsShow() )
    elseif string.find( name, "/ResistanceHq/TimeoutRoot/Refresh/Button" ) then
        TimeoutIndicatorHide()
        callback:ResistanceUpdateAll()
    --elseif string.find( name, "/ResistanceHq/Copy" ) then
    -- callback:SetClipboard( tostring ( User.UniqueId ) )
    end

    ResistanceTeamAction( name )
end

function ResistanceHqPress( control, path )

    if not active then
        return
    end

    if string.find( path, "/ResistanceHq/Back/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceHq/Inbox/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceHq/Invite/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceHq/Profile/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceHq/TimeoutRoot/Refresh/Button" ) then
        ButtonPress( control )
    --elseif string.find( path, "/ResistanceHq/Copy" ) then
        --ButtonPress( control )
    end

    ResistanceTeamPress( control, path )
end

function ResistanceHqShow()
    if active then 
        return 
    end

    OnResistanceHqShow()
    ResistanceTeamShow()
    active = true
    Lock()

    PopupLock( nil, "/Social/Overlay" )

    --inbox info
    FillInboxButtonInfo( inboxBtn , GetInvitesNumber() ,true )
    FillInviteId()

    popup:SetVisibility( true )

    anim:Add( Slider:New( 0.3,
            function( t )
                popup:SetAlpha( SmoothStep( t ) * 255 )
            end),
            function()
                Unlock()
            end
        )
end

function ResistanceHqHide( callback )
    active = false
    Lock()

    StopStarAnimation( rootTimeout:GetControl( "Icon/Star" ) )

    anim:Add( Slider:New( 0.3,
        function( t )
             popup:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end
        ),
        function()
            if callback then callback() end
            popup:SetVisibility( false )
            Unlock()
        end
    )
end

function ResistanceHqUpdate( dt )
    if not active then return end

    ResistanceTeamUpdate( dt )

    if UpdateInfo.Friends > 0 and IsResistanceTeamSyncing() then
        EndResistanceTeamSyncingSucces()
        FillInboxButtonInfo( inboxBtn , GetInvitesNumber() ,true )
    end

    if not IsResistanceTeamSyncing() and UpdateInfo.Friends == 0  then
        if timeoutIndicatorActive == true then
            TimeoutIndicatorHide( function() ResistanceTeamSyncingStart() end )
        else
            ResistanceTeamSyncingStart()
        end
    end

    if UpdateInfo.Friends < 0 and IsResistanceTeamSyncing() then
        EndResistanceTeamSyncingFail()
        TimeoutIndicatorShow()
    end

end

function ResistanceHqTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    ResistanceTeamTouchDown( x, y, id, c )

    -- local path = c:GetPath()

    -- if string.find(path, "/Buttons/") then
        -- --ButtonPress( c )
    -- end
end

function ResistanceHqTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    ResistanceTeamTouchUp( x, y, id, c )

end

function ResistanceHqTouchMove( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    ResistanceTeamTouchMove( x, y, id, c )

end

function IsResistanceHqActive()
    return active
end

function ResistanceHqButtonAction()
    isLogin = registry:Get( "/internal/loggedIn" )
        
    if not IsLoggedIn() then
        LoginMethodPopupShow()
    else
        -- if NewFriendsCount > 0 then
            -- callback:ResistanceOnRewardNewFriendsAccepted()
        -- else
            ResistanceHqShow()
        -- end
    end
end

function NewFriendsReward( newFriendsCount )

    local fuel = registry:Get( "/monstaz/cash/fuel" )

    if fuel then
        registry:Set( "/monstaz/cash/fuel",fuel + InviteRewardValue * newFriendsCount )
        ResetNewFriendsCount()
        callback:Save()
    end
end

function RewardAccepted()
    SyncIndicatorHide()
    NewFriendsPopupShow( NewFriendsCount )
    NewFriendsReward( NewFriendsCount )
end

function RewardAcceptFail()
    SyncIndicatorHide()
    ResistanceHqShow()
end

function FillInboxButtonInfo( button , count , fade )

    local inboxInfoBg = button:GetControl( "RedBg" )
    local valueText = inboxInfoBg:GetControl("InboxCounter")

    if count == 0 then
        inboxInfoBg:SetVisibility( false )
        valueText:SetVisibility( false )
    else
        local bw, bh = button:GetControl("Button"):GetGraphic( "TS" ):GetSize()

        inboxInfoBg:SetY( R(-3) )
        inboxInfoBg:SetX( bw - R(15) )

        local bgw, bgh = inboxInfoBg:GetGraphic( "TS" ):GetSize()

        valueText:SetText( tostring(count) )

        local scale = ( (valueText:GetTextWidth() + R(14)) / bgw ) 

        if scale > 1 then
            inboxInfoBg:GetGraphic( "TS" ):SetScale( scale, 1 )
            valueText:SetX( R(7) )
        else
            valueText:SetX( (bgw - valueText:GetTextWidth() )/2 )
        end

        if fade == true then
            inboxInfoBg:SetAlpha( 0 )
            valueText:SetAlpha( 0 )
            inboxInfoBg:SetVisibility( true )
            valueText:SetVisibility( true )

            anim:Add( Slider:New( 0.3,
                    function( t )
                    local setpDtAlpa = SmoothStep( t ) * 255
                    inboxInfoBg:SetAlpha( setpDtAlpa )
                    valueText:SetAlpha( setpDtAlpa )
                end),
                nil
            )
        else
            inboxInfoBg:SetAlpha( 255 )
            valueText:SetAlpha( 255 )
            inboxInfoBg:SetVisibility( true )
            valueText:SetVisibility( true )
        end
    end
end

function TimeoutIndicatorShow()

    timeoutIndicatorActive = true
    rootTimeout:SetAlpha( 0 )
    rootTimeout:SetVisibility( true ) 

    anim:Add( Slider:New( 0.3,
            function( t )
            rootTimeout:SetAlpha( SmoothStep( t ) * 255 )
        end),
             function() StartStarAnimation( rootTimeout:GetControl( "Icon/Star" ) ) end
        )
end

function TimeoutIndicatorHide( callback, instant )
    
    if timeoutIndicatorActive == false then return end

    timeoutIndicatorActive = false

    if instant == true then
        StopStarAnimation( rootTimeout:GetControl( "Icon/Star" )  )  
        rootTimeout:SetVisibility( false ) 
        if callback then callback() end
    else
        anim:Add( Slider:New( 0.3,
                    function( t )
                    rootTimeout:SetAlpha( 255 - SmoothStep( t ) * 255 )
                end ),
                    function() 
                        StopStarAnimation( rootTimeout:GetControl( "Icon/Star" )  )  
                        rootTimeout:SetVisibility( false )  
                        if callback then callback() end
                    end
                )
    end
end

function IsSocialHqLocked()
    return IsResistanceHqActive() or IsResistanceRequestsActive() or IsBackupTeamActive() or IsSurvivalListActive() or IsPopupNewGiftsActive()
end

function FillInviteId()
    local bottomMargin = R(6)
    --local inviteIdCopyBtn = popup:GetControl("Copy")
    local inviteIdLabel = popup:GetControl("InvitationCode")

    local inviteCodeText = tostring ( User.UniqueId )

    local onScreenIniteText = string.sub(inviteCodeText,1,3) .. " " .. string.sub(inviteCodeText,4,6).. " " .. string.sub(inviteCodeText, 7 )

    inviteIdLabel:SetTextWithVariables( "TEXT_ACCOUNT_INVITATION_CODE", { VALUE = onScreenIniteText } )
    inviteIdLabel:SetTextWidth( inviteIdLabel:GetTextWidth() )
    inviteIdLabel:SetY( height )
    inviteIdLabel:SetX( (width - inviteIdLabel:GetWidth())/2 )
    --inviteIdCopyBtn:SetY( height + inviteIdLabel:GetHeight() )
    --inviteIdCopyBtn:SetX( (width - inviteIdCopyBtn:GetWidth())/2 )
end
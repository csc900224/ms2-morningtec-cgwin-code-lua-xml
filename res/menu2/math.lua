function SmoothStep( val )
    val = math.min( math.max( val, 0 ), 1 )
    return val*val*(3-2*val)
end

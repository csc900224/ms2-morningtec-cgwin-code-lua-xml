require( "Endless.lua" )
require( "EntityLevels.lua" )
require( "LevelDB.lua" )
require( "StoryDB.lua" )
require( "menu2/map_scroll.lua" )
require( "menu2/popup_endless_intro.lua" )

local map = 1
local locked = false

local root = nil
local scroll = nil

local notifications = {}

local NotificationType = {
    Infection = 1,
    Lost = 2,
    Secured = 3
}

function MapInit( parent )
    parent:InsertFromXml( "menu2/map.xml" )
    root = parent:GetControl( "Map" )

    map = registry:Get( "/maps/current" ) or map

    local bg = root:GetControl( "Background" )
    local grid = root:GetControl( "Grid" )
    bg:GetGraphic( "TS" ):SetScale( grid:GetWidth() / bg:GetWidth(), grid:GetHeight() / bg:GetHeight() )

    MapLoadView( map )
    local willChangeView = MapSetupView( map )

    local lto = false
    if not willChangeView and not TutorialActive and registry:Get( "/internal/story" ) then
        local count = registry:Get( "/internal/lto-display-count" ) or 0
        local limit = registry:Get( "/app-config/lto/display-limit" ) or 0
        if count < limit then
            local timer = registry:Get( "/internal/lto-display-timer" ) or 0
            local interval = registry:Get( "/app-config/lto/display-interval" )
            if timer == interval then
                registry:Set( "/internal/lto-display-timer", 0 )
                registry:Set( "/internal/lto-display-count", count + 1 )

                lto = true
                LimitedTimeOffersShow()
            else
                registry:Set( "/internal/lto-display-timer", timer + 1 )
            end
        end
    end

    if not willChangeView and not lto and not TutorialActive then
        local weapon = registry:Get( "/internal/unlocked-weapon" )
        if weapon and weapon ~= "" then
            registry:Set( "/internal/unlocked-weapon", "" )
            if Shop:IsBought( weapon ) == 0 then
                WeaponUnlockedPopupSetup( weapon )
                WeaponUnlockedPopupShow()
            end
        end
    end

    MapCheckInfection()
end

function MapCheckInfection()
    if not StoryIsEndless( map ) then
        return
    end

    local totalMissions = 0;
    local totalMissionsCompleted = 0;

    local view = MapGetView( map )
    local areas = MapGetAreas( view )

    local areasFree = {}
    local areasInfected = {}
    local areasLost = {}

    local areaSecured = 0
    if registry:Get( "/internal/story" ) and registry:Get( "/internal/endless" ) and registry:Get( "/internal/map" ) == map and registry:Get( "/internal/levelcompleted" ) and not registry:Get( "/internal/boss" ) then
        areaSecured = registry:Get( "/internal/area" ) or 0
    end

    for i = 1,#areas do
        local missions = EndlessGetMissionsCount( i ) or 0
        totalMissions = totalMissions + missions

        local missionsCompleted = registry:Get( "/maps/" .. map .. "/" .. i .. "/completed" )

        local infectionTime = registry:Get( "/maps/" .. map .. "/" .. i .. "/infection-time" )
        if infectionTime and infectionTime > 0 then
            if infectionTime < callback:GetTime() then
                table.insert( areasLost, i )
                missionsCompleted = 0
                registry:Set( "/maps/" .. map .. "/" .. i .. "/completed", 0 )
                registry:Set( "/maps/" .. map .. "/" .. i .. "/level", -1 )
            else
                table.insert( areasInfected, i )
            end
        elseif missionsCompleted > 1 and i ~= areaSecured then
            table.insert( areasFree, i )
        end

        totalMissionsCompleted = totalMissionsCompleted + missionsCompleted
    end

    if totalMissionsCompleted > totalMissions/2 then
        while #areasInfected < 2 and #areasFree > 0 do
            local index = math.random(#areasFree)
            local area = areasFree[index]

            local missionsCompleted = registry:Get( "/maps/" .. map .. "/" .. area .. "/completed" )
            registry:Set( "/maps/" .. map .. "/" .. area .. "/completed", missionsCompleted - 1 )

            registry:Set( "/maps/" .. map .. "/" .. area .. "/level", -1 )
            registry:Set( "/maps/" .. map .. "/" .. area .. "/infection-time", callback:GetTime() + 24*60*60 )

            table.remove( areasFree, index )
            table.insert( areasInfected, area )
        end
    end

    for i, area in ipairs(areasInfected) do
        local infectionTime = registry:Get( "/maps/" .. map .. "/" .. area .. "/infection-time" )

        local time = infectionTime - callback:GetTime()
        local hours = math.floor( time / 3600 )
        local minutes = math.floor( time / 60 ) - hours * 60
        local seconds = time % 60

        local message = string.format( TextDict:Get( "TEXT_ENDLESS_AREA_INFECTION" ) .. "   ", hours, minutes, seconds )
        MapShowAreaTooltip( NotificationType.Infection, map, area, message )
    end

    for i, area in ipairs(areasLost) do
        MapShowAreaTooltip( NotificationType.Lost, map, area, TextDict:Get( "TEXT_ENDLESS_AREA_LOST" ) )
    end

    if areaSecured > 0 then
        MapShowAreaTooltip( NotificationType.Secured, map, areaSecured, TextDict:Get( "TEXT_ENDLESS_AREA_SECURED" ) )
    end

    MapStorePosition()
    MapSetupView( map )
end

function MapShowAreaTooltip( type, map, area, message )
    local viewPath = string.format( "/Content/Map/View/Map-%02d/Areas", map )
    local glowPath = string.format( "/Content/Map/View/Map-%02d/Areas/Area-%02d/Indicator/Icon/Glow", map, area )
    local view = screen:GetControl( viewPath )
    local glow = screen:GetControl( glowPath )
    local x = glow:GetAbsX() - view:GetAbsX()
    local y = glow:GetAbsY() - view:GetAbsY()

    local representation = "default"
    if type == NotificationType.Infection then
        representation = "orange"
    elseif type == NotificationType.Lost then
        representation = "red"
    end

    local anchor = EndlessGetTooltipAnchor( area )
    if type == NotificationType.Secured then
        local missionsCompleted = registry:Get( "/maps/" .. map .. "/" .. area .. "/completed" )
        if missionsCompleted == EndlessGetMissionsCount( area ) then
            anchor = TutorialAnchor.Bottom
            y = y + R(22)
        end
    end
    if not anchor then return end

    local offset = R(12)
    if anchor == TutorialAnchor.Left then
        x = x + offset
    elseif anchor == TutorialAnchor.Right then
        x = x - offset
    elseif anchor == TutorialAnchor.Top then
        y = y + offset
    elseif anchor == TutorialAnchor.Bottom then
        y = y - offset
    end

    local tooltip = TutorialTooltipShow( viewPath, string.format( "Area-%02d-Notification-%s", area, representation ), message, x, y, anchor, 0.25, true, 0, nil, false, representation, true )

    notifications[area] = {
        type = type,
        tooltip = tooltip
    }
end

function MapUpdate( dt )
    if scroll then scroll:Update( dt ) end

    local currentTime = callback:GetTime()
    local updateView = false

    local updateMapView = false
    for area, notification in pairs( notifications ) do
        if notification and notification.type == NotificationType.Infection then
            local infectionTime = registry:Get( "/maps/" .. map .. "/" .. area .. "/infection-time" )
            if infectionTime and infectionTime > callback:GetTime() then
                local time = infectionTime - callback:GetTime()
                local hours = math.floor( time/3600 )
                local minutes = math.floor( time/60 ) - hours*60
                local seconds = time % 60

                local tooltip = notification.tooltip
                local message = string.format( TextDict:Get( "TEXT_ENDLESS_AREA_INFECTION" ), hours, minutes, seconds )
                tooltip:GetControl( "Text" ):SetText( message )
            else
                local tooltip = notification.tooltip
                TutorialTooltipHide( tooltip:GetPath() )

                MapShowAreaTooltip( NotificationType.Lost, map, area, TextDict:Get( "TEXT_ENDLESS_AREA_LOST" ) )

                registry:Set( "/maps/" .. map .. "/" .. area .. "/completed", 0 )
                registry:Set( "/maps/" .. map .. "/" .. area .. "/level", -1 )

                updateMapView = true
            end
        end
    end

    if updateMapView then
        MapStorePosition()
        MapSetupView( map )
    end
end

function MapStorePosition()
    if scroll then
        registry:Set( "/maps/" .. map .. "/scroll/x", scroll.ScrollData.scrollOffset.x / R(1) )
        registry:Set( "/maps/" .. map .. "/scroll/y", scroll.ScrollData.scrollOffset.y / R(1) )
    end
end

function MapTouchDown( x, y, id )
    if scroll then
        local c = screen:GetTouchableControl( x, y )
        if not c then return end

        scroll:TouchDown( x, y, id, c )
    end
end

function MapTouchMove( x, y, id )
    if scroll then scroll:TouchMove( x, y, id ) end
end

function MapTouchUp( x, y, id )
    if scroll then scroll:TouchUp( x, y, id ) end
end

function MapLoadView( index )
    local atlas = AtlasSet.Map01 + index - 1
    callback:RequestAtlas( atlas )
    while not callback:IsAtlasLoaded( atlas ) do end

    root:GetControl( "View" ):InsertFromXml( string.format( "menu2/map_%02d.xml", index ) )
end

function MapUnloadView( index )
    anim:RemoveAll( string.format( "Map%02dLevelGlow", index ) )
    anim:RemoveAll( string.format( "Map%02dBossGlow", index ) )

    local view = MapGetView( index )
    view:Remove()

    local atlas = AtlasSet.Map01 + index - 1
    callback:ReleaseAtlas( atlas )
end

function MapGetView( index )
    local view = root:GetControl( string.format( "View/Map-%02d", index ) )
    return view
end

function MapSetupView( index )
    local missionsCount = 0
    local completedMissionsCount = 0

    local view = MapGetView( index )
    local areas = MapGetAreas( view )
    for i = 1,#areas do
        local area = areas[i]

        local locked = StoryIsLocked( index, i )
        local missions = StoryGetMissionsCount( index, i ) or EndlessGetMissionsCount( i ) or 0
        local completed = registry:Get( "/maps/" .. index .. "/" .. i .. "/completed" )

        missionsCount = missionsCount + missions
        completedMissionsCount = completedMissionsCount + completed

        MapSetupArea( index, area, missions, completed, locked )

        if not locked and completed < missions then
            local mission = StoryGetMission( index, i )
            if mission and mission.news then
                NewsbarAddEvent( { msg = mission.news } )
            end
        end
    end

    local grid = root:GetControl( "Grid" )
    scroll = MapScroll:new()
    scroll:Init(
        view,
        grid:GetWidth(),
        grid:GetHeight(),
        R( registry:Get( "/maps/" .. index .. "/scroll/x" ) or 0 ),
        R( registry:Get( "/maps/" .. index .. "/scroll/y" ) or 0 )
    )

    MapSetupProgressBar( index )

    if StoryIsCompleted( index ) then
        if StoryHasBoss( index ) and not StoryIsBossCompleted( index ) then
            MapShowBoss( index, view:GetControl( "Areas/Boss" ) )

            local mission = StoryGetMission( index, "boss" )
            if mission and mission.news then
                NewsbarAddEvent( { msg = mission.news } )
            end
        elseif not TutorialActive then
            anim:Add(
                Delay:New( 0.75 ),
                function()
                    if StoryIsEndless( index + 1 ) then
                        MapShowView( index + 1 )
                    else
                        MapNextView()
                    end
                end
            )

            return true
        end
    elseif StoryIsEndless( index ) and EndlessIsBossActive( index ) then
        MapShowBoss( index, view:GetControl( "Areas/Boss" ) )
    end

    return false
end

function MapSetupProgressBar( map )
    local bar = root:GetControl( "Bar" )
    local barWidth = bar:GetControl( "Bg" ):GetGraphic( "r" ):GetWidth()

    local fill = bar:GetControl( "Fill" )
    local backFill = bar:GetControl( "BackFill" )

    local endless = StoryIsEndless( map )
    if StoryHasBoss( map ) or endless then
        if StoryIsCompleted( map ) or StoryIsBossCompleted( map ) or endless and EndlessIsBossActive( map ) then
            MapHideProgressBar( 0.5, 0.5 )
        elseif bar:GetVisibility() then
            local progress = StoryGetBossProgress( map ) or endless and EndlessGetBossProgress( map ) or 0
            local width = math.floor( barWidth*progress )
            local diff = width - fill:GetWidth()

            anim:Add(
                Delay:New( 0.5 ),
                function()
                    anim:Add(
                        Slider:New(
                            2*math.abs( diff/barWidth ),
                            function( t )
                                fill:GetGraphic( "r" ):SetWidth( width - diff*( 1 - SmoothStep( t ) ) )
                            end
                        )
                    )
                end
            )
        else
            fill:GetGraphic( "r" ):SetWidth( 0 )

            backFill:SetVisibility( false )
            backFill:GetGraphic( "r" ):SetWidth( 0 )

            anim:Add(
                Delay:New( 0.3 ),
                function()
                    MapShowProgressBar( 0, 0.5 )

                    local offset = 0
                    if registry:Get( "/internal/story" ) and registry:Get( "/internal/map" ) == map and registry:Get( "/internal/levelcompleted" ) and not registry:Get( "/internal/boss" ) then
                        offset = -1
                    end

                    local progress = StoryGetBossProgress( map, offset ) or endless and EndlessGetBossProgress( map, offset )
                    fill:GetGraphic( "r" ):SetWidth( barWidth*progress )

                    local diff = ( StoryGetBossProgress( map ) or endless and EndlessGetBossProgress( map ) ) - progress
                    if diff > 0 then
                        backFill:SetVisibility( true )
                        backFill:SetX( fill:GetX() + fill:GetWidth() )
                        backFill:GetGraphic( "r" ):SetWidth( diff*barWidth )

                        anim:Add(
                            Delay:New( 0.35 ),
                            function()
                                anim:Add(
                                    Slider:New(
                                        0.5,
                                        function( t )
                                            fill:GetGraphic( "r" ):SetWidth( barWidth*( progress + diff*SmoothStep( t ) ) )
                                        end
                                    ),
                                    function()
                                        backFill:SetVisibility( false )
                                    end
                                )
                            end
                        )
                    end
                end
            )
        end
    else
        MapHideProgressBar( 0.5, 0.5 )
    end
end

function MapShowProgressBar( delay, duration )
    local bar = root:GetControl( "Bar" )
    if bar:GetVisibility() then
        return
    end

    bar:SetAlpha( 0 )
    bar:SetVisibility( true )

    anim:Add(
        Delay:New( delay ),
        function()
            anim:Add(
                Slider:New(
                    duration,
                    function( t )
                        bar:SetAlpha( 255*SmoothStep( t ) )
                    end
                )
            )
        end
    )
end

function MapHideProgressBar( delay, duration )
    local bar = root:GetControl( "Bar" )
    if not bar:GetVisibility() then
        return
    end

    anim:Add(
        Delay:New( delay ),
        function()
            anim:Add(
                Slider:New(
                    duration,
                    function( t )
                        bar:SetAlpha( 255*( 1 - SmoothStep( t ) ) )
                    end
                ),
                function()
                    bar:SetVisibility( false )
                end
            )
        end
    )
end

function MapShowBoss( map, c )
    c:SetVisibility( true )

    anim:Add(
        Slider:New( 0.3,
            function( t )
                local scale = 1.75 - 0.75*SmoothStep( t )
                c:GetControl( "Glow" ):GetGraphic( "TS" ):SetScale( scale )
                c:GetControl( "Background" ):GetGraphic( "TS" ):SetScale( scale )
                c:GetControl( "Icon" ):GetGraphic( "TS" ):SetScale( scale )
                c:SetAlpha( 255*SmoothStep( t ) )
            end
        ),
        function()
            AudioManager:Play( Sfx.SFX_SHOTGUN1 )

            local glow = c:GetControl( "Glow" )
            anim:Add( Perpetual:New(
                function( t )
                    local scale = 0.9 + math.sin( t*4 )*0.1
                    glow:GetGraphic( "TS" ):SetScale( scale )
                end
                ),
                nil,
                string.format( "Map%02dBossGlow", map )
            )
        end
    )
end

function MapSetupArea( map, area, missions, completed, locked )
    local indicator = area:GetControl( "Indicator" )
    if locked then
        area:SetRepresentation( "red" )

        indicator:GetControl( "Icon" ):SetVisibility( false )
        indicator:GetControl( "Lock" ):SetVisibility( true )

        for i = 1,5 do
            indicator:GetControl( i ):SetVisibility( false )
        end
    elseif completed == missions then
        area:SetRepresentation( "default" )

        indicator:GetControl( "Icon" ):SetVisibility( false )
        indicator:GetControl( "Lock" ):SetVisibility( false )

        for i = 1,5 do
            indicator:GetControl( i ):SetVisibility( false )
        end
    else
        if completed == 0 then
            area:SetRepresentation( "red" )
        else
            area:SetRepresentation( "orange" )
        end

        indicator:GetControl( "Icon" ):SetVisibility( true )
        indicator:GetControl( "Lock" ):SetVisibility( false )

        for i = 1, missions do
            local square = indicator:GetControl( i )
            square:SetVisibility( true )

            if i <= completed then
                square:SetRepresentation( "default" )
            else
                square:SetRepresentation( "red" )
            end
        end

        local icon = indicator:GetControl( "Icon" )
        icon:SetVisibility( true )

        local base = indicator:GetControl( missions )
        icon:SetX( base:GetX() )
        icon:SetY( base:GetY() )

        local glow = icon:GetControl( "Glow" )
        anim:Add( Perpetual:New(
            function( t )
                local scale = 0.9 + math.sin( t*4 )*0.1
                glow:GetGraphic( "TS" ):SetScale( scale )
            end
            ),
            nil,
            string.format( "Map%02dLevelGlow", map )
        )
    end
end

function MapGetAreas( view )
    local i = 1
    local areas = {}
    local parent = view:GetControl( "Areas" )
    while true do
        local area = parent:GetControl( string.format( "Area-%02d", i ) )
        if not area then
            break
        else
            areas[i] = area
            i = i + 1
        end
    end

    return areas
end

function MapAction( path )
    local area = select( 3, string.find( path, "Area[-](%d+)" ) )
    if area then
        area = tonumber( area )
    elseif string.find( path, "Boss" ) then
        area = 'boss'
    else
        return
    end

    TutorialAction( path )

    registry:Set( "/internal/map", map )
    registry:Set( "/internal/area", area )
    registry:Set( "/internal/areaidx", StroyGetAreaIdx( map, area ) )
    registry:Set( "/internal/boss", area == "boss" )

    local areas = StroyGetAreasCount( map )
    if areas then
        registry:Set( "/internal/areascount", areas )
    end

    registry:Set( "/internal/endless", StoryIsEndless( map ) )

    local mission = nil
    local missionIdx = StoryGetMissionIdx( map, area )
    if missionIdx ~= nil then
       mission = StoryGetMission( map, area )
    else
        missionIdx = 0;
        mission = EndlessGetMission( map, area )
    end
    registry:Set( "/internal/endless", missionIdx == 0 )
    registry:Set( "/internal/levelidx", missionIdx )
    registry:Set( "/internal/levelname", mission.l.n )
    registry:Set( "/internal/reward/soft", mission.r.s or 0 )
    registry:Set( "/internal/reward/hard", mission.r.h or 0 )
    registry:Set( "/internal/reward/xp", mission.r.xp or 0 )

    for i = 1,#mission.m do
        registry:Set( "/internal/monsters/" .. i, mission.m[i] )
        registry:Set( "/internal/elements/" .. i, mission.e[i] or 0 )
    end
    registry:Set( "/internal/monsters/" .. #mission.m + 1, -1 )

    if mission.cutscene then
        registry:Set( "/internal/loadingmovie", mission.cutscene )
    end

    local notification = notifications[area]
    if notification and notification.type ~= NotificationType.Infection then
        local tooltip = notification.tooltip
        TutorialTooltipHide( tooltip:GetPath() )

        notifications[area] = nil
        registry:Set( "/maps/" .. map .. "/" .. area .. "/infection-time", 0 )
    end

    PremissionSetup( mission.l, false, mission.fl )
    PremissionShow()

    AudioManager:Play( Sfx.SFX_MENU_BACK )
end

function MapNextView( showLogin )
    if map >= 6 then
        return
    end

    locked = true

    registry:Set( "/maps/current", map + 1 )
    callback:Save()

    MapLoadView( map + 1 )
    MapSetupView( map + 1 )

    local currentView = MapGetView( map )
    local currentViewAreas = MapGetAreas( currentView )

    local nextView = MapGetView( map + 1 )
    local nextViewAreas = MapGetAreas( nextView )

    local delay = 0

    for i = 1,#currentViewAreas do
        local area = currentViewAreas[i]

        local indicator = area:GetControl( "Indicator" )
        indicator:GetControl( "Icon" ):SetVisibility( false )

        local missions = StoryGetMissionsCount( map, i ) or EndlessGetMissionsCount( i ) or 0
        for j = 1, missions do
            local square = indicator:GetControl( j )
            if square:GetVisibility() then
                anim:Add(
                    Delay:New( j*0.05 ),
                    function()
                        anim:Add(
                            Slider:New(
                                0.1,
                                function( t )
                                    square:SetAlpha( 255*( 1 - SmoothStep( t ) ) )
                                end
                            )
                        )
                    end
                )
            end
        end
    end

    delay = delay + 5*0.05

    local ox = 999999
    local oy = 999999
    local ow = 0
    local oh = 0

    local positions = {}
    for i = 1,#currentViewAreas do
        local area = currentViewAreas[i]
        local x = area:GetX()
        local y = area:GetY()
        positions[i] = { x = x, y = y }
        ox = math.min( ox, x )
        oy = math.min( oy, y )
        ow = math.max( ow, x + area:GetWidth() )
        oh = math.max( oh, y + area:GetHeight() )
    end

    ow = ow - ox
    oh = oh - oy

    local tx = nextViewAreas[1]:GetX()
    local ty = nextViewAreas[1]:GetY()
    nextViewAreas[1]:SetAlpha( 0 )

    local downscale = nextViewAreas[1]:GetWidth()/ow

    for i = 2,#nextViewAreas do
        local area = nextViewAreas[i]
        area:SetAlpha( 0 )

        local x = area:GetX()
        local y = area:GetY()
        local width = area:GetWidth()
        local height = area:GetHeight()

        anim:Add(
            Delay:New( delay + 0.3 + (i-1)*0.25 ),
            function()
                anim:Add(
                    Slider:New( 0.3,
                        function( t )
                            local scale = 1.75 - 0.75*SmoothStep( t )
                            area:SetX( x - ( width*scale - width )/2 )
                            area:SetY( y - ( height*scale - height )/2 )
                            area:GetGraphic( "TS" ):SetScale( scale )
                            area:SetAlpha( 255*SmoothStep( t ) )
                        end
                    ),
                    function()
                        AudioManager:Play( Sfx.SFX_SHOTGUN1 )
                    end
                )
            end
        )
    end

    -- Current map background fade-out
    do
        local bg = currentView:GetControl( "Background" )
        anim:Add(
            Delay:New( delay + 0.2 ),
            function()
                anim:Add(
                    Slider:New(
                        0.2,
                        function( t )
                            bg:SetAlpha( 255 - 255*SmoothStep( t ) )
                        end
                    )
                )
            end
        )
    end

    -- Next map background fade-in
    do
        local bg = nextView:GetControl( "Background" )
        bg:SetAlpha( 0 )
        anim:Add(
            Delay:New( delay + 0.3 + 0.2 ),
            function()
                anim:Add(
                    Slider:New(
                        0.2,
                        function( t )
                            bg:SetAlpha( 255*SmoothStep( t ) )
                        end
                    )
                )
            end
        )
    end

    anim:Add(
        Delay:New( delay ),
        function()
            AudioManager:Play( Sfx.SFX_MENU_ROTATE )
            anim:Add( Slider:New( 0.5,
                function( t )
                    local scale = 1 - SmoothStep( t )*( 1 - downscale )
                    for i = 1,#currentViewAreas do
                        local area = currentViewAreas[i]
                        local position = positions[i]
                        area:GetGraphic( "TS" ):SetScale( scale )
                        area:SetAlpha( 255 - SmoothStep( t ) * 255 )
                        area:SetX( ox + ( position.x - ox )*scale + ( tx - ox )*SmoothStep( t ) )
                        area:SetY( oy + ( position.y - oy )*scale + ( ty - oy )*SmoothStep( t ) )
                    end

                    nextViewAreas[1]:GetGraphic( "TS" ):SetScale( 1/downscale*scale )
                    nextViewAreas[1]:SetAlpha( SmoothStep( t ) * 255 )
                    nextViewAreas[1]:SetX( ox + ( tx - ox )*SmoothStep( t ) )
                    nextViewAreas[1]:SetY( oy + ( ty - oy )*SmoothStep( t ) )
                end
                ),
                function()
                    locked = false
                    MapUnloadView( map )

                    map = map + 1
                end
            )
        end
    )

    delay = delay + 1.6

    for i = 1,#nextViewAreas do
        local area = nextViewAreas[i]
        local indicator = area:GetControl( "Indicator" )

        local duration = 0.075

        local missions = StoryGetMissionsCount( map, i ) or EndlessGetMissionsCount( i ) or 0
        for j = 1, missions do
            local square = indicator:GetControl( j )
            square:SetAlpha( 0 )

            anim:Add(
                Delay:New( delay + j*duration ),
                function()
                    anim:Add(
                        Slider:New(
                            duration*2,
                            function( t )
                                square:SetAlpha( 255*SmoothStep( t ) )
                            end
                        )
                    )
                end
            )
        end

        local icon = indicator:GetControl( "Icon" )
        icon:SetAlpha( 0 )

        local lock = indicator:GetControl( "Lock" )
        lock:SetAlpha( 0 )

        anim:Add(
            Delay:New( delay + ( missions + 1 )*duration ),
            function()
                anim:Add(
                    Slider:New(
                        duration*3,
                        function( t )
                            icon:SetAlpha( 255*SmoothStep( t ) )
                            lock:SetAlpha( 255*SmoothStep( t ) )
                        end
                    )
                )
            end
        )
    end

    if showLogin then
        Lock()
        anim:Add(
            Delay:New( delay + 0.9 ),
            function()
                LoginMethodPopupOnStart()
                Unlock()
            end
        )
    end
end

function MapShowView( index )
    if locked or index < 1 or index > 6 then
        return
    end

    locked = true

    local currentView = MapGetView( map )
    anim:Add(
        Slider:New(
            0.5,
            function( t )
                currentView:SetAlpha( 255*( 1 - SmoothStep( t ) ) )
            end
        ),
        function()
            MapUnloadView( map )
            MapLoadView( index )
            MapSetupView( index )

            local nextView = MapGetView( index )
            nextView:SetVisibility( true )
            nextView:SetAlpha( 0 )

            anim:Add(
                Slider:New(
                    0.5,
                    function( t )
                        nextView:SetAlpha( 255*SmoothStep( t ) )
                    end
                ),
                function()
                    map = index
                    locked = false

                    registry:Set( "/maps/current", index )

                    if StoryIsEndless( map ) then
                        EndlessIntroPopupInit( screen:GetControl( "/" ) )
                        EndlessIntroPopupShow()
                    end
                end
            )
        end
    )
end

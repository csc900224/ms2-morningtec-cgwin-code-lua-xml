require( "ItemDB.lua" )
require( "menu2/lock.lua" )
require( "MissionDB.lua" )
require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/map.lua" )
require( "menu2/math.lua" )
require( "menu2/newsbar.lua" )
require( "menu2/perpetual.lua" )
require( "menu2/popup.lua" )
require( "menu2/popup_settings.lua" )
require( "menu2/popup_settings_warning.lua" )
require( "menu2/slider.lua" )
require( "menu2/counters.lua" )
require( "menu2/shop/shop.lua" )
require( "menu2/popup_recover_pass.lua")
require( "menu2/popup_login_method.lua" )
require( "menu2/popup_register_social.lua" )
require( "menu2/resistance_team.lua" )
require( "menu2/premission.lua" )
require( "menu2/shop/popup_success.lua" )
require( "menu2/shop/popup_no_cash.lua" )
require( "menu2/shop/popup_no_cash_weapon.lua" )
require( "menu2/shop/popup_no_cash_else.lua" )
require( "menu2/shop/popup_wait.lua" )
require( "menu2/shop/popup_fail.lua" )
require( "menu2/shop/popup_info.lua" )
require( "menu2/popup_free_stuff.lua" )
require( "menu2/popup_outoffuel.lua" )
require( "menu2/popup_outoffuel_invite.lua" )
require( "menu2/popup_quit.lua" )
require( "menu2/weapon_select.lua" )
require( "menu2/tutorial.lua" )
require( "menu2/tutorial_menu.lua" )
require( "menu2/tutorial_popup.lua" )
require( "menu2/missions/missions.lua" )
require( "menu2/missions/popup_rankup.lua" )
require( "menu2/missions/notifier.lua" )
require( "menu2/popup_invite.lua" )
require( "menu2/popup_error.lua" )
require( "menu2/friend_info.lua" )
require( "menu2/popup_send_gift_success.lua" )
require( "menu2/resistance_hq.lua" )
require( "menu2/validation.lua" )
require( "menu2/popup_new_friends.lua" )
require( "menu2/popup_operation_success.lua" )
require( "menu2/popup_backup_require.lua" )
require( "menu2/backup_team.lua" )
require( "menu2/help_button.lua" )
require( "menu2/survival.lua" )
require( "menu2/sync_indicator.lua" )
require( "menu2/resistance_account.lua" )
require( "menu2/popup_confirm_remove.lua" )
require( "menu2/resistance_requests.lua" )
require( "menu2/popup_lto.lua" )
require( "menu2/popup_new_gifts.lua" )
require( "menu2/notifier_new_friends.lua" )
require( "menu2/popup_weapon_unlocked.lua" )
require( "menu2/premission_friendlock.lua" )
require( "menu2/popup_no_network.lua" )
require( "menu2/popup_help.lua" )
require( "menu2/popup_about.lua" )


local dailyTimer = nil
local LevelMonsterSet = nil
ShopActive = false
PremissionActive = false
ResistanceActive = false
BackupTeamActive = false
SurvivalActive = false
HelpActive = false
MissionsActive = false
FuelShopCallback = nil

local KeyHandlers = {
    FriendLockKeyDown,
    OutOfFuelInvitePopupKeyDown,
    OutOfFuelKeyDown,
    SettingsPopupKeyDown,
    HelpPopupKeyDown,
    AboutPopupKeyDown,
    BackupRequirePopupKeyDown,
    OperationSuccessPopupKeyDown,
    LoginMethodPopupKeyDown,
    LoginPopupKeyDown,
    RegisterSocialPopupKeyDown,
    RegisterPopupKeyDown,
    RecoverPasswordPopupKeyDown,
    ErrorPopupKeyDown,
    BackupTeamKeyDown,
    WeaponSelectKeyDown,
    PremissionKeyDown,
    SurvivalKeyDown,
    FreeStuffPopupKeyDown,
    ShopWaitPopupKeyDown,
    ShopSuccessKeyDown,
    ShopNoCashKeyDown,
    ShopNoCashWeaponKeyDown,
    ShopNoCashElseKeyDown,
    ShopInfoKeyDown,
    ShopFailKeyDown,
    ShopSharePopupKeyDown,
    ShopShareSuccessPopupKeyDown,
    ShopKeyDown,
    DailyChallengesPopupKeyDown,
    InvitePopupKeyDown,
    SendGiftSuccessPopupKeyDown,
    FriendInfoKeyDown,
    NewFriendsPopupKeyDown,
    ResistanceHqKeyDown,
    QuitPopupKeyDown,
    ResistanceRequestsKeyDown,
    ResistanceAccountKeyDown,
    LimitedTimeOffersKeyDown,
    WeaponUnlockedPopupKeyDown,
    NewGiftsKeyDown,
    MissionsKeyDown,
    MissionRankupPopupKeyDown,
    SyncIndicatorKeyDown
}

function IsMapActive()
    return not ShopActive and not PremissionActive and not BackupTeamActive and not MissionsActive
end

function Screen:Update( dt )
    anim:Update( dt )
    ShopUpdate( dt )
    CountersUpdate( dt )
    WeaponSelectUpdate( dt )
    ResistanceHqUpdate( dt )
    ResistanceRequestsUpdate( dt )
    UpdateAccountSettings( dt )
    FriendInfoUpdate( dt )
    BackupTeamUpdate( dt )
    SurvivalUpdate( dt )
    UpdateDB( dt )
    MapUpdate( dt )
    OutOfFuelUpdate( dt )
    NewGiftsUpdate( dt )
    MissionsUpdate( dt )
end

function Screen:OnKeyDown( code )
    if CheckLock() or TutorialActive then
        return
    end

    if HelpActive then
        EndHelp()
        return
    end

    for i, handler in pairs( KeyHandlers ) do
        if handler( code ) then
            return
        end
    end

    if code == keys.KEY_ESCAPE then
        --QuitPopupShow()
        callback:CallExitDialog() 

    end
end

function Screen:OnTouchDown( x, y, id )
    if CheckLock() or HelpActive then
        return
    end

    local c = self:GetTouchableControl( x, y )
    if TutorialActive then
        if not c or not TutorialIsActionAllowed( c:GetPath() ) then
            return
        end
    end
    if not IsMapActive() then
        if BackupTeamActive  then 
            BackupTeamTouchDown( x, y, id )            
        else
            ShopTouchDown( x, y, id )
        end
    elseif ResistanceActive then
        ResistanceHqTouchDown( x, y, id )
        ResistanceRequestsTouchDown( x, y, id )
    elseif SurvivalActive then
        SurvivalTouchDown( x, y, id )
    end
    NewGiftsTouchDown( x, y, id )
    MissionsTouchDown( x, y, id ) 
    WeaponSelectTouchDown( x, y, id )
    MapTouchDown( x, y, id )

    if not c then
        return
    end

    local path = c:GetPath()
    if path == "/Menu/SettingsButton" then
        ButtonPress( c )
    elseif path == "/Menu/SurvivalButton" then
        ButtonPress( c )
    elseif path == "/Menu/ShopButton" then
        ButtonPress( c )
    elseif path == "/Menu/FreeButton" then
        ButtonPress( c )
    elseif path == "/Menu/MissionsButton" then
        ButtonPress( c )
    elseif path == "/Resistance/ResistanceHqButton" or path == "/Resistance/GiftsButton" then
        ButtonPress( c )
    else
        CountersPress( c, path )
        SettingsPopupPress( c, path )
        HelpPopupPress( c, path )
        AboutPopupPress( c, path )
        SettingsQualityWarningPress( c, path )
        LoginMethodPopupPress( c, path )
        LoginPopupPress( c, path )
        RecoverPasswordPopupPress( c, path )
        RegisterPopupPress( c, path )
        RegisterSocialPopupPress( c, path )
        PremissionPress( c, path )
        InvitePopupPress ( c, path )
        ErrorPopupPress( c, path )
        FriendInfoPress( c, path )
        ShopSuccessPopupPress( c, path )
        ShopNoCashPopupPress( c, path )
        ShopNoCashWeaponPopupPress( c, path )
        ShopNoCashElsePopupPress( c, path )
        ShopInfoPopupPress( c, path )
        ShopFailPopupPress( c, path )
        SendGiftSuccessPopupPress( c, path )
        ResistanceHqPress( c, path )
        ResistanceAccountPress( c, path )
        NewFriendsPopupPress( c, path )
        OperationSuccessPopupPress( c, path )
        BackupRequirePopupPress( c, path )
        BackupTeamPress( c, path )
        HelpButtonPress( c, path )
        SurvivalPress( c, path )
        FreeStuffPopupPress( c, path )
        OutOfFuelPress( c, path )
        OutOfFuelInvitePress( c, path )
        QuitPopupPress( c, path )
        ConfirmRemovePopupPress( c, path )
        ResistanceRequestsPress( c, path )
        LimitedTimeOffersPress( c, path )
        WeaponUnlockedPopupPress( c, path )
        NewGiftsPress( c, path )
        MissionRankupPopupPress( c, path )
        FriendLockPress( c, path )
        EndlessIntroPopupPress( c, path )
        NoNetworkPopupPress( c, path )
        if TutorialActive then TutorialPopupPress( c, path ) end
    end
end

function Screen:OnTouchUp( x, y, id )
    if CheckLock() or HelpActive then
        EndHelp()
        return
    end

    local c = self:GetTouchableControl( x, y )
    if TutorialActive then
        if not c then
            return
        end

        local path = c:GetPath()
        if not TutorialIsActionAllowed( path ) then
            return
        end
    end
    if not IsMapActive() then
        if BackupTeamActive then 
            BackupTeamTouchUp( x, y, id )
        else
            ShopTouchUp( x, y, id )
        end
    elseif ResistanceActive then
        ResistanceHqTouchUp( x, y, id )
        ResistanceRequestsTouchUp( x, y, id )
    elseif SurvivalActive then
        SurvivalTouchUp( x, y, id )
    end
    NewGiftsTouchUp( x, y, id )
    MissionsTouchUp( x, y, id ) 
    WeaponSelectTouchUp( x, y, id )
    MapTouchUp( x, y, id )

    if not c then
        return
    end

    local path = c:GetPath()
    if TutorialActive then
        if not TutorialIsActionAllowed( path ) then return end
    end

    if path == "/Menu/SettingsButton" then
        SettingsPopupShow()
    elseif path == "/Menu/ShopButton" then
        TutorialAction( path )
        ShopShow()
    elseif path == "/Menu/SurvivalButton" then
        SurvivalShow()
    elseif path == "/Menu/MoregamesButton" then
        callback:FindMoreGames()	
    elseif path == "/Menu/QuitgameButton" then
        callback:CallExitDialog() 
        --QuitPopupShow()
    elseif path == "/Menu/MissionsButton" then
        TutorialAction( path )
        MissionsShow()
    elseif path == "/Resistance/ResistanceHqButton" then
        ResistanceHqButtonAction()
    elseif 	path == "/Resistance/GiftsButton" then
        NewGiftsShow()
    elseif path == "/Resistance/HelpButton" then
        HelpPopupShow()	
    elseif path == "/Resistance/AboutButton" then
        AboutPopupShow()


    else
        CountersAction( path )
        SettingsPopupAction( c, path )
        LoginPopupAction( path )
        RegisterPopupAction( path )
        RecoverPasswordPopupAction( path )
        LoginMethodPopupAction( path )
        RegisterSocialPopupAction( path )
        ShopSuccessPopupAction( path )
        ShopNoCashPopupAction( path )
        ShopNoCashWeaponPopupAction( path )
        ShopNoCashElsePopupAction( path )
        ShopInfoPopupAction( path )
        ShopFailPopupAction( path )
        PremissionAction( path )
        InvitePopupAction( path )
        ErrorPopupAction( path )
        FriendInfoAction( path )
        SendGiftSuccessPopupAction( path )
        ResistanceHqAction( path )
        ResistanceAccountAction( path )
        NewFriendsPopupAction( path )
        OperationSuccessPopupAction( path )
        BackupRequirePopupAction( path )
        BackupTeamAction( path )
        HelpButtonAction( c, path )
        SurvivalAction( path )
        FreeStuffPopupAction( path )
        OutOfFuelAction( path )
        OutOfFuelInviteAction( path )
        QuitPopupAction( c, path )
        ConfirmRemovePopupAction( path )
        ResistanceRequestsAction( path )
        LimitedTimeOffersAction( path )
        WeaponUnlockedPopupAction( path )
        NewGiftsAction( path )
        MissionRankupPopupAction( path )
        FriendLockAction( path )
        EndlessIntroPopupAction( c, path )
        NoNetworkPopupAction( path )
        if TutorialActive then TutorialPopupAction( c, path ) end
    end
end

function Screen:OnTouchMove( x, y, id )
    if CheckLock() then
        return
    end

    if not IsMapActive() then	  
        if BackupTeamActive then 
            BackupTeamTouchMove( x, y, id ) 
        else
            ShopTouchMove( x, y, id )
        end
    elseif ResistanceActive then	  
        ResistanceHqTouchMove( x, y, id )
        ResistanceRequestsTouchMove( x, y, id )
    elseif SurvivalActive then	 
        SurvivalTouchMove( x, y, id )
    end
    NewGiftsTouchMove( x, y, id )
    MissionsTouchMove( x, y, id )
    WeaponSelectTouchMove( x, y, id )
    MapTouchMove( x, y, id )
end

function MapHide( callback, instant )
    local menu = screen:GetControl( "/Menu" )
    local content = screen:GetControl( "/Content" )
    local resistance = screen:GetControl( "/Resistance" )
    local newsBar = screen:GetControl( "/NewsBar")

    if instant == true or content:GetVisibility() == false then
        menu:SetVisibility( false )
        content:SetVisibility( false )
        resistance:SetVisibility( false )
        newsBar:SetVisibility( false )

        if callback then callback() end
    else
        anim:Add( Slider:New( 0.3,
            function( t )
                local alpha = 255 - SmoothStep( t ) * 255
                menu:SetAlpha( alpha )
                content:SetAlpha( alpha )
                resistance:SetAlpha( alpha )
                newsBar:SetAlpha( alpha )
            end),
            function()
                menu:SetVisibility( false )
                content:SetVisibility( false )
                resistance:SetVisibility( false )
                newsBar:SetVisibility( false )
                
                if callback then callback() end
            end
        )
    end
end

function MapShow( clb )
    MissionsUpdateToClaimCounter( "/Menu/MissionsButton/ToClaim" )
    callback:Playhaven( "main_menu" )

    local menu = screen:GetControl( "/Menu" )
    local content = screen:GetControl( "/Content" )
    local resistance = screen:GetControl( "/Resistance" )
    local newsBar = screen:GetControl( "/NewsBar")

    menu:SetVisibility( true )
    content:SetVisibility( true )
    resistance:SetVisibility( true )
    newsBar:SetVisibility( true )

    anim:Add( Slider:New( 0.3,
        function( t )
            local alpha = SmoothStep( t ) * 255
            menu:SetAlpha( alpha )
            content:SetAlpha( alpha )
            resistance:SetAlpha( alpha )
            newsBar:SetAlpha( alpha )
        end),
        function()
            if clb then clb() end
        end
    )
end

function OnShopShow( instant )
    MapHide( nil, instant )
    ShopActive = true
end

function OnShopHide()
    if not PremissionActive and not MissionsActive then
        MapShow( function() ShopActive = false end )
    else
        ShopActive = false
    end
end

function OnPremissionShow()
    MapHide()
    PremissionActive = true
end

function OnPremissionHide()
    MapShow( function() PremissionActive = false end )
end

function OnMissionsShow()
    MapHide()
    MissionsActive = true
    return 0
end

function OnMissionsHide()
    if not PremissionActive then
        MapShow( function() MissionsActive = false end )
    else
        MissionsActive = false
    end
end

function LoginMethodPopupOnStart()

if TutorialActive == false then

    local playerWasLogged = registry:Get( "/monstaz/resistance/successLogin" )  

    if registry:Get( "/internal/firstLogin" ) == nil then
        registry:Set( "/internal/firstLogin",true)
    end

    local playerWasLogged = registry:Get( "/monstaz/resistance/successLogin" )
    local firtsLogin = registry:Get( "/internal/firstLogin" )

    if firtsLogin == true then
        if playerWasLogged then
            local name = registry:Get( "/monstaz/resistance/login" )
            local userUniqueId = registry:Get( "/monstaz/resistance/uniqueId" )
            local password =  registry:Get( "/monstaz/resistance/pass" )
            local mail = registry:Get( "/monstaz/resistance/mail" )
            local fbId = registry:Get( "/monstaz/resistance/fbId" )

            registry:Set( "/internal/login", name )
            registry:Set( "/internal/pass", password )
            registry:Set( "/internal/uniqueId", userUniqueId )
            registry:Set( "/internal/mail", mail )
            registry:Set( "/internal/fbId", fbId )
            registry:Set( "/internal/firstLogin",false)
            registry:Set( "/internal/loggedIn", true )
            callback:ResistanceLoginLastUser( userUniqueId )
        else
            LoginMethodPopupShow()
            registry:Set( "/internal/firstLogin",false)
        end
    end
end

end

function OnResistanceHqShow()
    MapHide( nil, false )
    ResistanceActive = true
end

function OnResistanceHqHide( toMap )
    if toMap == true then
        ResistanceActive = false
        PopupUnlock( nil, "/Social/Overlay" )
        MapShow()
    end
end

function OnBackupTeamShow()
    BackupTeamActive = true
end

function OnBackupTeamHide()
    BackupTeamActive = false
end

function OnHelpShow()
    HelpActive = true
end

function OnHelpHide()
    HelpActive = false
end

function OnSurvivalShow()
    SurvivalActive = true
end

function OnSurvivalHide()
    SurvivalActive = false
end

function SocialButtonInit()
    local button = screen:GetControl("/Resistance/ResistanceHqButton")
    local bw, bh = button:GetGraphic( "TS" ):GetSize()

    local label = screen:GetControl("/Resistance/ResistanceHqButton/ResistanceHqButtonLabel")
    local lw = label:GetTextWidth()

    local icon = screen:GetControl("/Resistance/ResistanceHqButton/SocialIcon")
    local iw, ih = icon:GetGraphic( "TS" ):GetSize()

    local margin = R(2)
    local wholeWidth = lw + iw + margin

    icon:SetX( (bw - wholeWidth)/2 )
    label:SetX( (bw - wholeWidth)/2 + iw + margin)
    label:SetY( margin + (ih - R(16))/2 )

    local giftsButton = screen:GetControl("/Resistance/GiftsButton")
    local iconGift = giftsButton:GetControl("GiftIcon")
    local iconGiftW, iconGiftH = iconGift:GetGraphic( "TS" ):GetSize()
    local bg = giftsButton:GetControl("BgBlow")
    local bgW, bgH = bg:GetGraphic( "TS" ):GetSize()
    
    bg:GetGraphic( "TS" ):SetPivot( bgW/2 , bgH/2 )
    iconGift:GetGraphic( "TS" ):SetPivot( iconGiftW/2 , iconGiftH/2 )

    giftsButton:SetVisibility( false )
end

function OnMissionCompleted( missionPath )
    MissionsUpdateToClaimCounter( "/Menu/MissionsButton/ToClaim" )
end

function MainMenuInit()
    local root = screen:GetControl( "/" )
    root:InsertFromXml( "menu2/mainmenu.xml" )

    LimitedTimeOffersInit( screen )
    WeaponUnlockedPopupInit( root )

    local content = root:GetControl( "Content" )
    MapInit( content )

    local newsbar = root:GetControl( "NewsBar")
    NewsbarInit( newsbar )

    local premission = root:GetControl( "Premission" )
    PremissionInit( premission )
    WeaponSelectInit( premission )
    FriendLockInit( screen )

    local missions = root:GetControl( "Missions" )
    MissionsInit( missions, false )
    MissionsUpdateToClaimCounter( "/Menu/MissionsButton/ToClaim" )

    CountersInit( root:GetControl( "Counters" ) )
    ShopInit( root:GetControl( "Shop" ) )


    local socialRoot = root:GetControl( "Social" )
    ResistanceHqInit( socialRoot )
    ResistanceAccountInit( socialRoot )
    ResistanceRequestsInit( socialRoot )

    SettingsPopupInit( screen )
    HelpPopupInit( screen )
    AboutPopupInit( screen )
    SettingsQualityWarningInit( screen )
    QuitPopupInit( root )
    LoginPopupInit( screen )
    RegisterPopupInit( screen )
    RecoverPasswordPopupInit( screen )
    LoginMethodPopupInit( screen )
    RegisterSocialPopupInit( screen )
    InvitePopupInit( screen )
    ErrorPopupInit( screen )
    FriendInfoInit( screen )
    SendGiftSuccessPopupInit( screen )
    NewFriendsPopupInit( screen ) 
    OperationSuccessPopupInit( screen )
    BackupRequirePopupInit( screen )
    BackupTeamInit( screen )
    SurvivalInit( screen )
    FreeStuffPopupInit( screen )
    ConfirmRemovePopupInit( screen )
    OutOfFuelInit( screen )
    OutOfFuelInviteInit( screen )
    MissionRankupPopupInit( screen )

    ShopWaitPopupInit( screen )
    ShopSuccessPopupInit( screen )
    ShopNoCashPopupInit( screen )
    ShopNoCashWeaponPopupInit( screen )
    ShopNoCashElsePopupInit( screen )
    ShopInfoPopupInit( screen )
    ShopFailPopupInit( screen )
    NewGiftsInit( screen )

    MissionNotifierInit( screen )
    NewFriendsNotifierInit( screen )
    NoNetworkPopupInit( screen )

    SyncIndicatorInit( screen )

    callback:Playhaven( "main_menu" )

    LoginMethodPopupOnStart()
    UserInfoRefresh()

    SocialButtonInit()

    local glow = screen:GetControl( "/Menu/SurvivalButtonGlow" )
    anim:Add( Perpetual:New(
        function( t )
            local s = math.sin( t * 4 )
            glow:SetAlpha( ( s * s ) * 255 )
        end
        ) )
end

require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false

local popup = nil
local popupCloseCallback = nil
local popupFrame = {
    x = 0,
    y = 0,
    width = R( 206 ),
    height = R( 122 )
}

function QuitPopupInit( parent )
    parent:InsertFromXml( "menu2/popup_quit.xml" )
    popup = parent:GetControl( "QuitPopup" )

    x = ( SCREEN_WIDTH - popupFrame.width )/2
    y = ( SCREEN_HEIGHT - popupFrame.height )/2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    PopupCreateFrame( popup:GetControl( "Frame" ), popupFrame.width, popupFrame.height )
end

function QuitPopupSetCloseCallback( callback )
    popupCloseCallback = callback
end

function QuitPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        QuitPopupHide()
        return true
    end

    return false
end

function QuitPopupPress( control, path )
    if not active then
        return
    end

    if path == "/QuitPopup/Close" then
        ButtonPress( control )
    end
end

function QuitPopupAction( control, path )
    if not active then
        return
    end

    if path == "/QuitPopup/Quit" then
        callback:ExitApplication()
    elseif path == "/QuitPopup/Close" then
        QuitPopupHide()
    end
end

function QuitPopupShow()
    active = true
    Lock()

    PopupLock( 0.3, "/QuitPopup/Overlay" )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            Unlock()
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function QuitPopupHide()
    active = false
    Lock()

    PopupUnlock( 0.3, "/QuitPopup/Overlay" )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            if popupCloseCallback then popupCloseCallback() end
            popup:SetVisibility( false )
            Unlock()
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

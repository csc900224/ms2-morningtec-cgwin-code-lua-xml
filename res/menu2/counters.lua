require( "menu2/animation.lua" )
require( "menu2/common.lua" )

local root = nil
local fuelenabled = nil
local plusenabled = true
local fuelitem = nil

FuelLevels = {}

function CountersInit( parent )
    parent:InsertFromXml( "menu2/counters.xml" )
    root = parent
    fuelitem = root:GetControl( "Fuel" )

    fuelenabled = registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" )
    fuelitem:SetVisibility( fuelenabled )

    for i=1,4 do
        table.insert( FuelLevels, registry:Get( "/app-config/fuel/" .. i ) )
    end
end

function CountersUpdate( dt )
    fuelenabled = registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" )
    fuelitem:SetVisibility( fuelenabled )

    if fuelenabled then
        local fuel = registry:Get( "/monstaz/cash/fuel" )
        local fuelmax = registry:Get( "/monstaz/cash/fuelmax" )
        local control = root:GetControl( "Fuel" )
        if fuel then
            control:GetControl( "Value" ):SetText( tostring( fuel ) .. "/" .. FuelLevels[fuelmax] )
        end

        local active = fuel ~= FuelLevels[fuelmax]
        control:GetControl( "Bg" ):SetTouchable( plusenabled and active )
        control:GetControl( "Plus" ):SetVisibility( plusenabled and active )
    end

    local soft, hard = Shop:GetCash()
    root:GetControl( "Hard/Value" ):SetText( tostring( hard ) )
    root:GetControl( "Soft/Value" ):SetText( tostring( soft ) )
end

function CountersPress( c, path )
    local rootName = root:GetName()
    if string.find( path, rootName .. "/Hard" ) then
        ButtonPress( root:GetControl( "Hard/Plus" ) )
    elseif string.find( path, rootName .. "/Soft" ) then
        ButtonPress( root:GetControl( "Soft/Plus" ) )
    elseif string.find( path, rootName .. "/Fuel" ) then
        ButtonPress( root:GetControl( "Fuel/Plus" ) )
    end
end

function CountersAction( path )
    if path == "/Counters/Soft/Bg" then
        CountersBuySoft()
    elseif path == "/Counters/Hard/Bg" then
        CountersBuyHard()
    elseif path == "/Counters/Fuel/Bg" then
        local fuel = registry:Get( "/monstaz/cash/fuel" )
        local max = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]
        if fuel < max then
            OutOfFuelShow( true )
        end
    end
end

function CountersBuySoft()
    local best = nil
    local items = ItemsCategory[ItemCategory.cash]
    for i, item in ipairs( items ) do
        if item.cs and item.cs > 0 and ( not best or item.cs > best.cs ) and ( item.ch == nil or item.ch == 0 ) then
            best = item
        end
    end

    CountersShopShow( best )
end

function CountersBuyHard()
    local best = nil
    local items = ItemsCategory[ItemCategory.cash]
    for i, item in ipairs( items ) do
        if item.ch and item.ch > 0 and ( not best or item.ch > best.ch ) and ( item.cs == nil or item.cs == 0 ) then
            best = item
        end
    end

    CountersShopShow( best )
end

function CountersShopShow( item )
    if ShopShow then
        local backAction = nil
        if MissionsActive then
            MissionsHide( nil, false, true )
            backAction = function () MissionsShow( nil, true ) end
        elseif PremissionActive then 
            if WeaponSelectActive == true then
                backAction = function () WeaponSelectShow( nil, nil, nil, true ) end
            else  
                backAction = function () PremissionShow() end
            end
            
            PremissionHide( nil, false )
            WeaponSelectHide()
        end

        ShopShow(ShopTab.Cash, false, backAction, item)
    end
end

function CountersButtonsEnable( enable )
    root:GetControl( "Soft/Plus" ):SetVisibility( enable )
    root:GetControl( "Soft/Bg" ):SetTouchable( enable )
    root:GetControl( "Hard/Plus" ):SetVisibility( enable )
    root:GetControl( "Hard/Bg" ):SetTouchable( enable )
    root:GetControl( "Fuel/Plus" ):SetVisibility( enable )
    root:GetControl( "Fuel/Bg" ):SetTouchable( enable )
    plusenabled = enable
end

function CountersCashEarned( cashEarned )
    local w, h = root:GetControl( "Hard/Bg" ):GetGraphic( "TS" ):GetSize()
    local msg = TextDict:Get( "TEXT_FREE_GOLD_CONFIRM" ) .. " " .. cashEarned
    TutorialTooltipShow( root:GetControl( "Hard" ):GetPath(), "GoldInfo", msg, w/2, h*0.75, TutorialAnchor.Top, 0, true, 4, nil, nil )
end

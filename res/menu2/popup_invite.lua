require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )


local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 180 )
local height = R( 180 )


local friendUniqueId = ""

function InvitePopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_invite.xml" )
    popup = screen:GetControl( "/InvitePopup" )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( - SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    HelpButtonInit( popup , width )
end

function InvitePopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        InvitePopupHide( PopupUnlock() )
        return true
    end

    return false
end

function InvitePopupAction( name )
    if not active then
        return
    end

    if name == "/InvitePopup/Back" then
        InvitePopupHide( PopupUnlock() )
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FRIEND_INVITATION_CANCELED )
    elseif name == "/InvitePopup/Invite" then

        friendUniqueId = InputFields.inviteId
        --friendUniqueId = "1810012342"

        if not IsUserIdValid( friendUniqueId ) then
            InvitePopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_INVITE_ERROR" , function() InvitePopupShow() end ) end )
            return
        end

        if friendUniqueId ==  registry:Get( "/internal/uniqueId") then
            InvitePopupHide( function() ErrorPopupShow( "TEXT_FAIL_INVITE_YOURSELF" , function() InvitePopupShow() end ) end )
            return
        end

        InvitePopupHide( function() callback:ResistanceOnSendInvite( friendUniqueId ) end )


    elseif name == "/InvitePopup/FriendIDInput/FriendIDInputButton" then
        SetInputTextToUpdate( popup:GetControl("FriendIDInput/FriendIDInputText") , "_", InputFieldsType.inviteId  )
        UpdateInput( friendUniqueId )
        callback:OpenVkb( friendUniqueId , InputFieldsMaxLenght.inviteId )
    end
end

function InvitePopupPress( control, path )
    if not active then
        return
    end

    if path == "/InvitePopup/Invite" then
        ButtonPress( control )
    elseif path == "/InvitePopup/Back" then
        ButtonPress( control )
    end
end

function InvitePopupShow()
    active = true

    PopupLock()
    PopupInviteResetInputFileds()
    callback:ResistanceRefreshInputs()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function InvitePopupHide( callback )
    if not active then return end
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function InviteSuccess()
    OperationSuccessPopupShow( "TEXT_RESISTANCE_INVITE_SUCCESS" , ResistanceHqShow )
end

function InviteFail()
    ErrorPopupShow( "TEXT_RESISTANCE_INVITE_ERROR" , InvitePopupShow )
end

function InviteFailNoNetwork()
   ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" ) 
end

function InviteFailAlreadyFriend()
    ErrorPopupShow( "TEXT_FAIL_INVITE_ALREADY_FRIEND" , InvitePopupShow )
end

function InviteFailAlreadySend()
    ErrorPopupShow( "TEXT_FAIL_INVITE_ALREADY_SEND" , InvitePopupShow )
end

function PopupInviteResetInputFileds()

        friendUniqueId = ""
        
        InputFields.inviteId = ""
        
        SetInputTextToUpdate( popup:GetControl("FriendIDInput/FriendIDInputText") , "_", InputFieldsType.inviteId  )
        UpdateInput( friendUniqueId )
end


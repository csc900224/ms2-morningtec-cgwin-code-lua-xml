require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

LineBlue = {
    lineEnd = "menu2/line_01_end.png@linear",
    lineFill = "menu2/line_01_fill.png@linear"
}

avatarDefultPath = "menu2/avatar_man.png@linear"
avatarDefultPathPremission = "menu2/avatar_dum_dum.png@linear"
avatarGamelionPath = "menu2/avatar_gamelion.png@linear"

function GetAvatarImgPath( friend )

    if IsGamelionUser( friend ) then
        playerAvatar = avatarGamelionPath
    else
        if friend.FbId =="" or friend.FbId == nil then
            playerAvatar = avatarDefultPath 
        else
            local avatarPath = callback:ResistanceGetUsernamePath( friend.FbId  )
            local isAvaible = callback:ResistanceIsFbAvatarAvaible( avatarPath )

            if ( isAvaible ) then
                playerAvatar = avatarPath.."@linear"
            else
                playerAvatar = avatarDefultPath
            end
        end
    end

    return playerAvatar
end

function SetAvatarImg( avatarRoot , avatarPath )

    local scale = 1

    if not string.find( avatarPath , avatarDefultPath ) then
        avatarRoot:GetGraphic( "TS" ):SetImage( avatarDefultPath )
        local defaultW, defaultH = avatarRoot:GetGraphic( "TS" ):GetSize()

        avatarRoot:GetGraphic( "TS" ):SetImage( avatarPath )
        local aw, ah = avatarRoot:GetGraphic( "TS" ):GetSize()
        scale = defaultW / aw
    else
        avatarRoot:GetGraphic( "TS" ):SetImage( avatarPath )
    end 

    -- avatar scale
    avatarRoot:GetGraphic( "TS" ):SetScale( scale )
    
    return scale
end

function IsAvatarDefault( avatarPath )
    return string.find( avatarPath, avatarDefultPath )
end

function CreateBottomLine(root,width,height)

    local lb = root:InsertControl("lb")
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( LineBlue.lineEnd )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )

    lbw, lbh = lb:GetGraphic( "TS" ):GetSize()
    rbw, rbh = lb:GetGraphic( "TS" ):GetSize()

    lb:SetX( -lbw )
    lb:SetY( height )

    local bottom = root:InsertControl("bottom")
    bottom:AddRepresentation( "default", "menu2/tile.xml" )
    bottom:SetRelative( true )

    bottom:SetX( 0 )
    bottom:SetY( height )

    bottom:GetGraphic( "s" ):SetImage( LineBlue.lineFill )
    bottom:GetGraphic( "s" ):SetClipRect( 0, 0, width , lbh )

    local rb = root:InsertControl("rb")
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( LineBlue.lineEnd )
    rb:SetY( height )
    rb:SetX( width )
end

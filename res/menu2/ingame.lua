require( "menu2/avatar.lua" )
require( "menu2/ingame_pause.lua" )
require( "menu2/ingame_perks.lua" )
require( "menu2/ingame_revive.lua" )
require( "menu2/popup_levelup.lua" )
require( "menu2/popup_summary.lua" )
require( "menu2/tutorial.lua" )
require( "menu2/tutorial_ingame.lua" )
require( "menu2/shop/popup_success.lua" )
require( "menu2/shop/popup_no_cash.lua" )
require( "menu2/shop/popup_no_cash_else.lua" )--MorningTEC
require( "menu2/shop/popup_wait.lua" )
require( "menu2/shop/popup_fail.lua" )
require( "menu2/missions/missions.lua" )
require( "menu2/missions/notifier.lua" )
require( "menu2/missions/popup_rankup.lua" )
require( "menu2/lock.lua" )
require( "menu2/popup_no_network.lua" )
require( "menu2/sync_indicator.lua" )

local KeyHandlers = {
    SummaryKeyDown,
    ShopSuccessKeyDown,
    ShopNoCashKeyDown,
    ShopNoCashElseKeyDown,--MorningTEC
    ShopFailKeyDown,
    LevelupKeyDown,
    DailyChallengesPopupKeyDown,
    PauseKeyDown,
    MissionsKeyDown,
    MissionRankupPopupKeyDown
}

function Screen:Update( dt )
    anim:Update( dt )
    PauseUpdate( dt )
    MissionsUpdate( dt )
    PerksMenuUpdate( dt )
end

function Screen:OnKeyDown( code )
    if TutorialActive or CheckLock() then
        return
    end

    for i, handler in pairs( KeyHandlers ) do
        if handler( code ) then
            break
        end
    end
end

function Screen:OnTouchDown( x, y, id )
    if CheckLock() then
        return
    end

    local c = self:GetTouchableControl( x, y )
    if TutorialActive then
        if not c or not TutorialIsActionAllowed( c:GetPath() ) then return end
    end

    PauseTouchDown( x, y, id )
    MissionsTouchDown( x, y, id )
    
    if not c then
        return
    end

    local path = c:GetPath()
    SummaryPopupPress( c, path )
    ShopSuccessPopupPress( c, path )
    ShopNoCashPopupPress( c, path )
    ShopNoCashElsePopupPress( c, path )--MorningTEC
    ShopFailPopupPress( c, path)
    LevelupPopupPress( c, path )
    MissionRankupPopupPress( c, path )
    NoNetworkPopupPress( c, path )
end

function Screen:OnTouchUp( x, y, id )
    if CheckLock() then
        return
    end

    local c = self:GetTouchableControl( x, y )
    if TutorialActive then
        if not c or not TutorialIsActionAllowed( c:GetPath() ) then return end
    end

    PauseTouchUp( x, y, id )
    MissionsTouchUp( x, y, id )

    if not c then
        return
    end

    local path = c:GetPath()

    if path == "/PerkArea" then
        callback:PerkMenu()
        TutorialAction( path )
    else
        PerksMenuAction( path )
        ReviveMenuAction( path )
        SummaryPopupAction( path )
        ShopSuccessPopupAction( path )
        ShopNoCashPopupAction( path )
        ShopNoCashElsePopupAction( path )--MorningTEC
        ShopFailPopupAction( path )
        LevelupPopupAction( c, path )
        MissionRankupPopupAction( path )
        NoNetworkPopupAction( path )
    end
end

function Screen:OnTouchMove( x, y, id )
    if CheckLock() then
        return
    end

    PauseTouchMove( x, y, id )
    MissionsTouchMove( x, y, id )
end

function StartSummary( level, time, kills, multi, score, fail )
    local weponId = registry:Get( "/internal/currentweapon" )
    local weapon = weponId and ItemsById[weponId] or ItemsById[ShopItem.SMG]
    local avatarData = weapon ~= nil and weapon.a or nil
    if avatarData then
        AvatarSetWeapon( avatarData )
    end

    screen:GetControl( "/PerkArea" ):SetTouchable( false )
    local item = screen:GetControl( "/Perks" )
    anim:Add( Slider:New( 0.3,
        function( t )
            item:SetAlpha( (1-t) * 255 )
        end
        ) )

    anim:Add(
        Delay:New( 1.5 ),
        function()
            AvatarShow()

            SummaryPopupSetup( level, time, kills, multi, score, fail )
            SummaryPopupShow()
        end
    )
end

function StartPauseMenu()
    PauseMenuShow()
end

function StartPerkMenu()
    PerksMenuSetup()
    PerksMenuShow()
end

function OnMissionsShow()
    PauseMenuMissionsHide()
    return 0.3
end

function OnMissionsHide()
    PauseMenuMissionsShow()
end

local root = screen:GetControl( "/" )
root:InsertFromXml( "menu2/ingame.xml" )

AvatarInit( root )
PauseMenuInit( root:GetControl( "PauseRoot" ) )
MissionsInit( root:GetControl( "Missions" ), true )
PerksMenuInit( root )
SummaryPopupInit( root )
LevelupPopupInit( root )
ReviveMenuInit( root )

ShopWaitPopupInit( screen )
ShopSuccessPopupInit( screen )
ShopNoCashPopupInit( screen )
ShopNoCashElsePopupInit( screen )--MorningTEC
ShopFailPopupInit( screen )
SettingsPopupInit( screen, true )

MissionNotifierInit( screen )
MissionRankupPopupInit( screen )

NoNetworkPopupInit( screen )

SyncIndicatorInit( screen )

-- Perk Menu
PM = nil
do
    local PerkMenuControl = screen:GetControl( "/PerkArea" )
    PM = function( perk, nuke )
        PerkMenuControl:SetVisibility( perk )
    end
end

local objective = not TutorialActive and registry:Get( "/internal/objective" )
if objective then
    local obj = screen:GetControl( "/Objective" )
    obj:GetControl( "Icon" ):GetGraphic( "s" ):SetImage( registry:Get( "/internal/objectiveicon" ) )
    obj:GetControl( "Text" ):SetText( objective )
    obj:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            obj:SetY( SCREEN_HEIGHT - SmoothStep( t ) * R(58) )
        end
        ),
        function()
            anim:Add( Delay:New( 3 ),
            function()
                anim:Add( Slider:New( 0.3,
                    function( t )
                        obj:SetY( SCREEN_HEIGHT - SmoothStep( 1-t ) * R(58) )
                    end
                    ),
                    function()
                        obj:SetVisibility( false )
                    end
                    )
            end
            )
        end
        )
end

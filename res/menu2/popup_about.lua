require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil
local scroll = nil

local x = 0
local y = 0
local width = SCREEN_WIDTH
local height = SCREEN_HEIGHT - R(50)



function AboutPopupInit( screen, isingame )
    ingame = isingame

    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_about.xml" )
    popup = screen:GetControl( "/AboutPopup" )

    x = 0
    y = 0


    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

   
end


function AboutPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        AboutPopupHide()
        return true
    end

    return false
end

function AboutPopupPress( control, path )
    if not active then
        return
    end

    if path == "/AboutPopup/Back" then
        AboutPopupHide()
    end
end

function AboutPopupAction( control, path )
    if not active then
        return
    end

    if path == "/AboutPopup/Back" then
        AboutPopupHide()
    end
end

function AboutPopupShow()
    active = true
    callback:Playhaven( "options" )
    PopupLock()
    SettingsUpdateVersion()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )

    SettingsTab( 1 )
end

function AboutPopupHide()
    active = false
    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function SettingsUpdateVersion()
    if versionText then
        versionText:SetText( callback:GetVersionString() )
    end
end

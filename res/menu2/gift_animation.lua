require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/perpetual.lua" )

function StartGiftAnimation( control )
    local bg = control:GetControl("BgBlow"):GetGraphic( "TS" )
    local ico = control:GetControl("GiftIcon"):GetGraphic( "TS" )
    anim:Add( Perpetual:New(
        function( t )
            local delta = 7*t
            bg:SetScale( 0.92 + math.cos( delta ) * 0.08 )
            ico:SetScale( 0.92 + math.sin( delta ) * 0.08 )
        end
        ), nil, control:GetPath() .. "/" .. "Pulse" )
end

function StopGiftAnimation( control )
    anim:RemoveAll( control:GetPath() .. "/" .. "Pulse" )
end
require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/social_item.lua" )
require( "ResistanceDB.lua" )


local active = false

local x = R( 10 )
local y = R( 45 )

local width = R( 400 )
local height = R( 300 )

local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local weaponSlotWidth = R( 125 )
local weaponSlotHeight = R( 90 )

local root = nil
local currentFriend = nil
local dumDum = nil

local timeLeft = 0
local sendGiftButton = nil

local ElementMap = {}
ElementMap["Electric"] = Element.electric
ElementMap["Fire"] = Element.fire
ElementMap["Frost"] = Element.frost

function FriendInfoInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/friend_info.xml" )

    root = screen:GetControl( "/FriendInfo" )
    
    HelpButtonInit( root , width )
    
    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    root:SetX( x )
    root:SetY( SCREEN_HEIGHT )
    root:SetVisibility( false )
    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
    
    root:GetControl( "FriendAvatarRoot" ):InsertFromXml( "menu2/social_item.xml" )
    dumDum = root:GetControl( "DumDumAvatarRoot/DumDum" )
end

function FriendInfoKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        FriendInfoHide( function() PopupUnlock() end )
        return true
    end

    return false
end

function FriendInfoAction( name )
    if not active then
        return
    end

    if name == "/FriendInfo/SendGift" then
        if sendGiftButton:GetRepresentation() == "default" then
            SendGift()
            GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FRIEND_PROFILE_ACTION, GameEventParam.GEP_PROFILE_SEND_GIFT )
        end
    elseif name == "/FriendInfo/Back" then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FRIEND_PROFILE_ACTION, (timeLeft > 0) and GameEventParam.GEP_PROFILE_CLOSE or GameEventParam.GEP_PROFILE_CLOSE_WITHOUT_GIFT )
        FriendInfoHide( function() PopupUnlock() end )
    end
end

function FriendInfoPress( control, path )
    if not active then
        return
    end

    if path == "/FriendInfo/AcceptGift" then
        ButtonPress( control )
    elseif path == "/FriendInfo/SendGift" then
        if sendGiftButton:GetRepresentation() == "default" then
            ButtonPress( control )
        end
    elseif path == "/FriendInfo/Back" then
        ButtonPress( control )
    end
end

function FriendInfoShow( friend )
    
    active = true
    SyncIndicatorHide()
    
    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    SocialAvatarFill( root:GetControl( "FriendAvatarRoot" ),  R(60), R( 120 ), friend )

    root:GetControl( "FriendProfileText" ):SetText( "TEXT_FRIEND_PROFILE" )
    root:GetControl( "LevelInfo/LevelIcon/LevelValue" ):SetText( friend.Level )
    
    root:GetControl( "InfoRoot/KillsInfo/KillsValue"):SetText( friend.Kills )
    root:GetControl( "InfoRoot/EventsCompletedInfo/EventsCompletedValue"):SetText( friend.CompletedEvents )
    
    currentFriend = friend
    
    local weapons = ItemsCategory[1]
    local weapon = nil
    
    local currentLvlIdx = tonumber( currentFriend.UsedWeaponUpgrade ) + 1
    local idxWeapon = 1

    for i=1,# weapons do
        if weapons[i].id == friend.UsedWeaponId then
            idxWeapon = i
            weapon =  weapons[i]
            break
        end
    end

    local weaponAvatar = weapons[idxWeapon].a 

    local weaponDumDum = dumDum:GetControl("Weapon")
    local charcterDumDum = dumDum:GetControl("Character")
    weaponDumDum:GetGraphic("TS"):SetImage( weaponAvatar.file )
    local w, h = weaponDumDum:GetGraphic("TS"):GetSize()
    weaponDumDum:GetGraphic("TS"):SetPivot( R(weaponAvatar.px), R(weaponAvatar.py) )

    local elementsOwned = {}

    if weapon then
        if weapon.upgrades and weapon.upgrades.Elements then
            local maxIdx = math.min( #weapon.upgrades.Elements, currentLvlIdx )
            for i=1,maxIdx do
                if weapon.upgrades.Elements[i] > 0 then
                    table.insert( elementsOwned, weapon.upgrades.Elements[i] )
                end
            end
        end
        
        FriendWeaponMarkElements( weapon, root:GetControl( "InfoRoot/WeaponInfoRoot/WeaponSlot/Elements" ), elementsOwned ) 
    end

    
    root:GetControl( "InfoRoot/WeaponInfoRoot/WeaponTextInfo/WeaponName" ):SetText( weapons[idxWeapon].d )

    -- Icon position
    local weaponAngle = 0
        
    local icon = root:GetControl( "InfoRoot/WeaponInfoRoot/WeaponSlot/WeaponIcon" )
    icon:GetGraphic( "TS" ):SetImage( weapons[idxWeapon].f )

    --special case mech

    if weapons[idxWeapon].id == ShopItem.Mech then
        charcterDumDum:SetVisibility( false )
        icon:GetGraphic("TS"):SetScale( 0.8 )
    else
        charcterDumDum:SetVisibility( true )
        icon:GetGraphic("TS"):SetScale( 1 )
    end

        
    local w, h = icon:GetGraphic( "TS" ):GetSize()

    icon:GetGraphic( "TS" ):SetAngle( weaponAngle )
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    
    icon:SetX( weaponSlotWidth/2 )
    icon:SetY( weaponSlotHeight/2 )

    root:SetVisibility( true )

    sendGiftButton = root:GetControl( "SendGift" )
    
    timeLeft = tonumber( friend.CanSendGifts )
    
    if timeLeft > 0 then
        sendGiftButton:SetRepresentation( "pressed" )
        FriendInfoUpdate( 0 )
    else
        sendGiftButton:SetRepresentation( "default" )
        sendGiftButton:GetControl("SendGiftLabel"):SetText("TEXT_SEND_GIFT")
    end
    
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FRIEND_PROFILE_SHOWN )
    
    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function FriendInfoHide( callback )
    active = false
    
    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            root:SetVisibility( false )
            if callback then callback() end
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end


function SendGift()
    FriendInfoHide()
    --callback:ResistanceOnSendGift( GiftType.giftSoftCurrency , currentFriend.UniqueId , tostring( GiftSoftCurrencyQuantity ) )
    callback:ResistanceOnSendGift( GiftType.giftFuelPack , currentFriend.UniqueId , tostring( GiftFuelPackQuantity ) )
end

function SendGiftSucces( friendUniqueId )

    SetGiftFlag( friendUniqueId , 86400 + TimeSinceUpdateDB )
    SendGiftSuccessPopupShow()
end

function SendGiftFailed( alreadyGiven )
    --FriendInfoHide( function() ErrorPopupShow( "TEXT_05" ) end )
    if alreadyGiven == true then
        ErrorPopupShow( "TEXT_SEND_GIFT_ALREADY_GIVEN" )
    else
        ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" )
    end
end

function FriendWeaponMarkElements( item, elementsRoot, elementsActive )

    local activeSet = {}
    for _, l in ipairs(elementsActive) do activeSet[l] = true end

    local rootPath = elementsRoot:GetPath()

    for idx, element in ipairs( elementsRoot:GetChildren() ) do
        local elelmentName = element:GetName()
        local elelmentId = ElementMap[elelmentName]

        local hasElement = ItemHasElementUpgrade( item, elelmentId )

        if hasElement then
            element:SetVisibility( true )

            -- Center pivots
            local graphic = element:GetGraphic( "TS" )
            local pivX, pivY = graphic:GetPivot()
            if pivX == 0 and pivY == 0 then
                local w, h = graphic:GetSize()
                
                element:SetRepresentation( "active" )
                element:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
                element:SetRepresentation( "default" )
                element:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
                
                element:SetX( element:GetX() + w/2 )
                element:SetY( element:GetY() + h/2 )
            end
            
            if activeSet[elelmentId] ~= nil then
                element:SetRepresentation( "active" )
                element:SetAlpha( 255 )
            else
                element:SetRepresentation( "default" )
                element:SetAlpha( 255 )
            end
        else
            element:SetVisibility( false )
        end
    end
end

function FriendInfoUpdate( dt )

    if not active then
        return
    end

    timeLeft = tonumber( currentFriend.CanSendGifts )

    if ( timeLeft > 0 ) then
        timeLeft = timeLeft - TimeSinceUpdateDB
        sendGiftButton:GetControl( "SendGiftLabel"):SetText( TimeFormat( timeLeft ) ) 
    else
        sendGiftButton:SetRepresentation( "default" )
        sendGiftButton:GetControl( "SendGiftLabel"):SetText("TEXT_SEND_GIFT")
    end

end
    


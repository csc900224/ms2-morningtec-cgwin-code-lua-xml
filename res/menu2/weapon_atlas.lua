function LoadWeaponAtlas( id )
    for _,v in ipairs( AllItems ) do
        if v.id == id then
            callback:RequestAtlas( v.atlas )
            return
        end
    end
end

function ReleaseWeaponAtlas( id )
    for _,v in ipairs( AllItems ) do
        if v.id == id then
            callback:ReleaseAtlas( v.atlas )
            return
        end
    end
end

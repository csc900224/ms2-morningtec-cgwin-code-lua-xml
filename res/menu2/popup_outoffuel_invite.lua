local active = false

local OutOfFuelInviteRewards = {
    sms = 1,
    facebook = 1,
    email = 1
}

local OutOfFuelInvitationLimits = {
    sms = 5,
    facebook = -1,
    email = 5
}

local popup = nil
local overlay = nil

local x = 0
local y = 0

function OutOfFuelInviteInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_outoffuel_invite.xml" )
    popup = screen:GetControl( "/OOFInvitePopup" )
    overlay = popup:GetControl( "Overlay" )    
    overlay:SetVisibility( false )

    local frame = popup:GetControl( "Frame" )

    x = math.floor( ( SCREEN_WIDTH - OutOfFuelInviteW )/2 )
    y = math.floor( math.max( ( SCREEN_HEIGHT - OutOfFuelInviteH )/2, R(56) ) )
    PopupCreateFrame( frame, OutOfFuelInviteW, OutOfFuelInviteH )
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    popup:SetVisibility( false )
end

function OutOfFuelInviteShow()
    active = true

    popup:SetVisibility( true )
    
    OutOfFuelInviteUpdateParams()
    OutOfFuelInviteUpdateRewars()
    OutOfFuelInviteUpdateButtonsAvailability()
    OutOfFuelInviteUpdateButtonsState()
    
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function OutOfFuelInviteHide()
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( 1-t ) * ( SCREEN_HEIGHT - y ) )
        end,
        function()
            popup:SetVisibility( false )
        end
        )
    )

    local fuel = registry:Get( "/monstaz/cash/fuel" )
    local max = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]

    if fuel < max then
        anim:Add( Delay:New( 0.3 ),
            function()
                OutOfFuelShow( nil, true )
            end
            )
    else
        PopupUnlock( 0.3, "/Counters/Overlay" )

        anim:Add( Delay:New( 0.3 ),
            function()
                CountersButtonsEnable( true )
            end
            )
    end
end

function OutOfFuelInvitePopupKeyDown( code )
    if not active then return end

    if code == keys.KEY_ESCAPE then
        OutOfFuelInviteHide()
        return true
    end

    return false
end

function OutOfFuelInvitePress( control, path )
    if not active then return end

    if path == "/OOFInvitePopup/Facebook" then
        ButtonPress( control )
    elseif path == "/OOFInvitePopup/SMS" then
        ButtonPress( control )
    elseif path == "/OOFInvitePopup/EMail" then
        ButtonPress( control )
    elseif path == "/OOFInvitePopup/Back" then
        ButtonPress( control )
    end
end

function OutOfFuelInviteAction( path )
    if not active then return end

    if path == "/OOFInvitePopup/Facebook" then
        callback:SendFacebookInvitation()
    elseif path == "/OOFInvitePopup/SMS" then
        overlay:SetVisibility( true )        
        callback:SendSmsInvitation()
    elseif path == "/OOFInvitePopup/EMail" then
        overlay:SetVisibility( true )
        callback:SendEmailInvitation()
    elseif path == "/OOFInvitePopup/Back" then
        OutOfFuelInviteHide()
    end
end

function OutOfFuelInviteUpdateParams()
    OutOfFuelInviteRewards.facebook = registry:Get( "/app-config/fuel/invite/reward/facebook" )
    OutOfFuelInviteRewards.sms = registry:Get( "/app-config/fuel/invite/reward/sms" )
    OutOfFuelInviteRewards.email = registry:Get( "/app-config/fuel/invite/reward/email" )
    
    OutOfFuelInvitationLimits.facebook = registry:Get( "/app-config/fuel/invite/limit/facebook" )
    OutOfFuelInvitationLimits.sms = registry:Get( "/app-config/fuel/invite/limit/sms" )
    OutOfFuelInvitationLimits.email = registry:Get( "/app-config/fuel/invite/limit/email" )
end

function OutOfFuelInviteUpdateRewars()
    popup:GetControl( "Facebook/Text" ):SetTextWithVariables( "TEXT_OOF_FACEBOOK", { VALUE = tostring( OutOfFuelInviteRewards.facebook ) } )
    popup:GetControl( "SMS/Text" ):SetTextWithVariables( "TEXT_OOF_SMS", { VALUE = tostring( OutOfFuelInviteRewards.sms ) } )
    popup:GetControl( "EMail/Text" ):SetTextWithVariables( "TEXT_OOF_EMAIL", { VALUE = tostring( OutOfFuelInviteRewards.email ) } )
end

function OutOfFuelInviteUpdateButtonsAvailability()
    local smsButton = popup:GetControl( "SMS" )
    local emailButton = popup:GetControl( "EMail" )
    local fbButton = popup:GetControl( "Facebook" )
    
    smsButton:SetVisibility( callback:IsSMSSupported() )
    emailButton:SetVisibility( callback:IsEmailSupported() )
    
    if not smsButton:GetVisibility() and not emailButton:GetVisibility() then
        fbButton:SetX( OutOfFuelInviteW * 0.5 )
    elseif not smsButton:GetVisibility() then 
        fbButton:SetX( OutOfFuelInviteW * 0.3 )
        emailButton:SetX( OutOfFuelInviteW * 0.7 )
    elseif not emailButton:GetVisibility() then 
        fbButton:SetX( OutOfFuelInviteW * 0.3 )
        smsButton:SetX( OutOfFuelInviteW * 0.7 )
    else
        fbButton:SetX( OutOfFuelInviteW * 0.2 )
        smsButton:SetX( OutOfFuelInviteW * 0.5 )
        emailButton:SetX( OutOfFuelInviteW * 0.8 )
    end
end

function OutOfFuelInviteUpdateButtonsState()
    OutOfFuelInviteUpdateButtonState( "Facebook", OutOfFuelInvitationLimits.facebook )
    OutOfFuelInviteUpdateButtonState( "SMS", OutOfFuelInvitationLimits.sms )
    OutOfFuelInviteUpdateButtonState( "EMail", OutOfFuelInvitationLimits.email )
end

function OutOfFuelInviteUpdateButtonState( buttonName, limit )
    local allowed = OutOfFuelInviteSendAllowd( buttonName, limit )
    popup:GetControl( buttonName ):SetAlpha( allowed and 255 or 100 )
    popup:GetControl( buttonName ):SetTouchable( allowed )
end

function OutOfFuelInviteSendAllowd( buttonName, limit ) 
    local fuel = registry:Get( "/monstaz/cash/fuel" )
    local max = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]
    if fuel >= max then return false end
    
    local lastTimestamp = registry:Get( "/monstaz/invitation/" .. buttonName .. "/timestamp" )
    local currentTimestamp = callback:GetCurrentDateString()
    
    if lastTimestamp == currentTimestamp and limit >= 0 then
        local savedCount = registry:Get( "/monstaz/invitation/" .. buttonName .. "/count" ) or 0
        return savedCount < limit
    end
    return true
end

function OutOfFuelInviteIncrementSendCount( buttonName )
    local timeStampKey = "/monstaz/invitation/" .. buttonName .. "/timestamp"
    local counterKey = "/monstaz/invitation/" .. buttonName .. "/count"
    
    local lastTimestamp = registry:Get( timeStampKey )
    local currentTimestamp = callback:GetCurrentDateString()
    
    if lastTimestamp ~= currentTimestamp then
        -- Time for reset
        registry:Set( timeStampKey, currentTimestamp )
        registry:Set( counterKey, 0 )
    end
    
    local invCount = registry:Get( counterKey ) or 0
    registry:Set( counterKey, invCount + 1 )
end

function OutOfFuelInviteOnSmsSent( success )
    if success then
        OutOfFuelInviteIncrementSendCount( "SMS" )
        OutOfFuelInviteAddFuel( OutOfFuelInviteRewards.sms )
        OutOfFuelInviteUpdateButtonsState()
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_INVITE, "sms" )
    end
    overlay:SetVisibility( false )
end

function OutOfFuelInviteOnEmailSent( success )
    if success then
        OutOfFuelInviteIncrementSendCount( "EMail" )
        OutOfFuelInviteAddFuel( OutOfFuelInviteRewards.email )
        OutOfFuelInviteUpdateButtonsState()
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_INVITE, "email" )
    end
    overlay:SetVisibility( false )
end

function OutOfFuelInviteOnFacebookSent( success )
    if success then
        OutOfFuelInviteIncrementSendCount( "Facebook" )
        OutOfFuelInviteAddFuel( OutOfFuelInviteRewards.facebook )
        OutOfFuelInviteUpdateButtonsState()
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_INVITE, "facebook" )
    end
    overlay:SetVisibility( false )
end

function OutOfFuelInviteAddFuel( reward )
    local fuel = registry:Get( "/monstaz/cash/fuel" )
    local max = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]
    registry:Set( "/monstaz/cash/fuel", math.min( max, fuel + reward ) )
    callback:Save()
end

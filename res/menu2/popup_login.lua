require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/popup_register.lua" )
require( "menu2/popup_recover_pass.lua")
require( "ResistanceDB.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 260 )
local height = R( 250 )

local userLogin = ""
local userPass = ""

function LoginPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_login.xml" )
    popup = screen:GetControl( "/LoginPopup" )

    HelpButtonInit( popup , width )
    
    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )


    local loginInputButton = popup:GetControl("Login/LoginUsernameInputButton")
    local lW = loginInputButton:GetWidth()

    local loginInput = popup:GetControl("Login")
    local passInput = popup:GetControl("Pass")

    local loginInputImg = popup:GetControl("LoginImg")
    local passInputImg = popup:GetControl("LoginPassImg")

    loginInputImg:SetX( ( width - lW ) /2 )
    passInputImg:SetX( ( width - lW ) /2 )

    loginInput:SetX( ( width - lW ) /2 )
    passInput:SetX( ( width - lW ) /2 )

    local loginBtn = popup:GetControl("LoginButton")
    local forgotButton = popup:GetControl("RecoverPass")
    local backButton = popup:GetControl("Back")
    local createButton = popup:GetControl("CreateAccountButton")


    local w, h = backButton:GetGraphic( "TS" ):GetSize()

    loginBtn:SetX( ( width - w ) /2 )
    backButton:SetX( ( width - w ) /2 )
    forgotButton:SetX( ( width - w ) /2 )
    createButton:SetX( ( width - createButton:GetWidth() ) /2 )

end

function LoginPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        LoginPopupHide( function() LoginMethodPopupShow() end )
        return true
    end

    return false
end

function LoginPopupAction( name )
    if not active then
        return
    end

    if name == "/LoginPopup/CreateAccountButton" then
        LoginPopupHide( function() RegisterPopupShow() end)
    elseif name == "/LoginPopup/LoginButton" then
        
       --InputFields.username = "login"
       --InputFields.password = "haslo1"
        
        userLogin = InputFields.username
        userPass = InputFields.password

        --quick validation
        if not IsNameValid( userLogin ) or not IsPassValid( userPass ) then
            LoginPopupHide( function() ErrorPopupShow( "TEXT_FAIL_LOGIN" , LoginPopupShow ) end )
            return
        end
    
        LoginPopupHide()
        callback:ResistanceLogin( userLogin, userPass )

    elseif name == "/LoginPopup/RecoverPass" then
        LoginPopupHide( function() RecoverPasswordPopupShow() end )
    elseif name == "/LoginPopup/Back" then
        LoginPopupHide( function() LoginMethodPopupShow() end)
    elseif name == "/LoginPopup/Login/LoginUsernameInputButton" then
        SetInputTextToUpdate( popup:GetControl("Login/LoginUsernameText"),"TEXT_RESISTANCE_USER_NAME", InputFieldsType.username )
        UpdateInput( userLogin )
        callback:OpenVkb( userLogin , InputFieldsMaxLenght.username )
    elseif name == "/LoginPopup/Pass/LoginPasswordInputButton" then
        SetInputTextToUpdate( popup:GetControl("Pass/LoginPasswordText") , "TEXT_RESISTANCE_PASSWORD", InputFieldsType.password  )
        UpdateInput( userPass )
        callback:OpenVkb( userPass ,InputFieldsMaxLenght.password )
    end
end

function LoginPopupPress( control, path )
    if not active then
        return
    end

    if path == "/LoginPopup/LoginButton" then
        ButtonPress( control )
    elseif path == "/LoginPopup/RecoverPass" then
        ButtonPress( control )
    elseif path == "/LoginPopup/Back" then
        ButtonPress( control )
    end
end

function LoginPopupShow()
    active = true
    
    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    PopupLoginResetInputFields()
    callback:ResistanceRefreshInputs()
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function LoginPopupHide( callback )
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function PopupLoginResetInputFields()
    userLogin = ""
    userPass = ""
    
    InputFields.username = ""
    InputFields.password = ""
    
    --userLogin = User.Name
    --userPass = User.Pass
    
    SetInputTextToUpdate( popup:GetControl("Login/LoginUsernameText"),"TEXT_RESISTANCE_USER_NAME", InputFieldsType.username )
    UpdateInput( userLogin )
    
    SetInputTextToUpdate( popup:GetControl("Pass/LoginPasswordText") , "TEXT_RESISTANCE_PASSWORD", InputFieldsType.password  )
    UpdateInput( userPass )

end

function SetInput()
    popup:GetControl("LoginImg"):GetGraphic("s"):RenderSurfaceToSprite()
end

function UserLoginFailed()
    --LoginPopupHide( function() ErrorPopupShow( "TEXT_FAIL_LOGIN", LoginPopupShow ) end )
    ErrorPopupShow( "TEXT_FAIL_LOGIN", LoginPopupShow )
end

function UserLoginFailedNoNetwork()
    --LoginPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT") end )
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT")
end

function UserLoggedOut()
    registry:Set( "/internal/loggedIn",false)
    registry:Set( "/monstaz/resistance/successLogin",false )
    
    SetUserInfo( "" , "" , "" , "" ,"" )
    FlushFriends()
    FlushInvitations()
    FlushGifts()
    GiftAndInviteItems = {}
    FlushBackup()
    UpdateGiftsButton()
    updated = false
    InputFields =  { username = "", password ="", email ="" , inviteId ="" , passwordVerify = ""  }
    ResetStats()
    ResetSyncTimeUpdateInfo()
    callback:ResistanceLoggedOut()
    callback:Save()
end


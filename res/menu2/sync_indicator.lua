require( "menu2/animation.lua" )
require( "menu2/common.lua" )


local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 0 )
local height = R( 0 )


local dot1 = nil
local dot2 = nil
local dot3 = nil

function SyncIndicatorInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/sync_indicator.xml" )
    popup = screen:GetControl( "/SyncIndicator" )
    popup:SetVisibility( false )
    
    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( y )
    
    dot1 = screen:GetControl( "/SyncIndicator/SyncIndicatorDot1" )
    dot2 = screen:GetControl( "/SyncIndicator/SyncIndicatorDot2" )
    dot3 = screen:GetControl( "/SyncIndicator/SyncIndicatorDot3" )
    
    
    local w, h = dot1:GetGraphic( "TS" ):GetSize()
    local margin = R ( 5 )
    dot1:SetX( - margin - w )
    dot2:SetX( 0 )
    dot3:SetX( margin + w )
    
end

function SyncIndicatorShow( dontLock )
    if active then return end

    if not dontLock then
        PopupLock()
    end

    active = true
    popup:SetVisibility( true )
    local startAlpha = popup:GetAlpha()
    anim:RemoveAll( "Popup" )
    anim:Add( Slider:New( (time or 0.3) * (255-startAlpha)/255,
        function( t )
            popup:SetAlpha( startAlpha + SmoothStep( t ) * (255-startAlpha) )
        end
        ),
        nil,
        "Popup"
    )
    
    anim:RemoveAll( "SyncBlink" )

    local lt = 0
    local timer = 0
    local speed = 5
    anim:Add( Perpetual:New(
        function( t )
            local dt = t - lt
            lt = t
            if ( timer > 3) then 
                timer = 0
            else
                timer = timer + speed*dt
            end
            
            if timer <= 1 then
                dot1:SetRepresentation( "active" )
            else
                dot1:SetRepresentation( "default" )
            end
            
            if timer >= 1 and timer <= 2 then
                dot2:SetRepresentation( "active" )
            else
                dot2:SetRepresentation( "default" )
            end
            
            if timer >= 2 and timer <= 3 then
                dot3:SetRepresentation( "active" )
            else
                dot3:SetRepresentation( "default" )
            end
        end
        ),
        nil,
        "SyncBlink"
    )
end

function SyncIndicatorHide( callback )
    if not active then return end

    active = false

    popup:SetVisibility( true )
    local startAlpha = popup:GetAlpha()
    anim:RemoveAll( "Popup" )
    anim:RemoveAll( "SyncBlink" )
    anim:Add( Slider:New( (time or 0.3) * startAlpha/255,
        function( t )
            popup:SetAlpha( startAlpha - SmoothStep( t ) * startAlpha )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end,
        "Popup"
    )
end


function SyncIndicatorKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        return true
    end

    return false
end

function IsSyncIndicatorVisible()
    return popup:GetVisibility() 
end

require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false

local popup = nil
local popupCloseCallback = nil
local popupFrame = {
    x = 0,
    y = 0,
    width = R( 212 ),
    height = R( 132 )
}

function TutorialPopupInit( parent )
    parent:InsertFromXml( "menu2/tutorial_popup.xml" )
    popup = parent:GetControl( "TutorialPopup" )

    x = ( SCREEN_WIDTH - popupFrame.width )/2
    y = ( SCREEN_HEIGHT - popupFrame.height )/2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    PopupCreateFrame( popup:GetControl( "Frame" ), popupFrame.width, popupFrame.height )
end

function TutorialPopupSetCloseCallback( callback )
    popupCloseCallback = callback
end

function TutorialPopupPress( control, path )
    if not active then
        return
    end

    if path == "/TutorialPopup/Close" then
        ButtonPress( control )
    end
end

function TutorialPopupAction( control, path )
    if not active then
        return
    end

    if path == "/TutorialPopup/Close" then
        TutorialPopupHide()
        TutorialAction( path )
    end
end

function TutorialPopupShow()
    active = true

    PopupLock( 0.3, "/TutorialPopup/Overlay" )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function TutorialPopupHide()
    active = false

    PopupUnlock( 0.3, "/TutorialPopup/Overlay" )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            if popupCloseCallback then popupCloseCallback() end
            popup:SetVisibility( false )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

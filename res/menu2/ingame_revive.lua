require( "menu2/shop/shop.lua" )

local active = false
local popup = nil
local perkarea = nil
local counterwrap = nil
local counter = nil
local countergfx = nil
local revivetext = nil
local counterValue = 0
local iapInProgress = false

local cost = registry:Get( "/monstaz/player/revive" )
local coststep = registry:Get( "/app-config/revive/step" )
local costmax = registry:Get( "/app-config/revive/end" )

function ReviveMenuInit( parent )
    parent:InsertFromXml( "menu2/ingame_revive.xml" )
    popup = parent:GetControl( "RevivePopup" )
    perkarea = screen:GetControl( "/PerkArea" )
    counterwrap = popup:GetControl( "Counter" )
    counter = counterwrap:GetControl( "Item" )
    countergfx = counter:GetGraphic( "TS" )
    revivetext = popup:GetControl( "ReviveButtonLive/Text" )
end

function ReviveMenuShow()
    if not IAPS_ENABLED then
        local _,gold = Shop:GetCash()
        if cost > gold then
            AudioManager:Play( Sfx.SFX_VO_YOU_FAILED )
            callback:Revive( false )
            return
        end
    end

    active = true
    perkarea:SetTouchable( false )
    counter:SetVisibility( false )
    counterwrap:SetAlpha( 255 )

    if cost == 0 then
        revivetext:SetText( "TEXT_REVIVE_LIVE" )
    else
        revivetext:SetText( TextDict:Get( "TEXT_REVIVE_LIVE" ) .. " -" .. cost .. " \194\158" )
    end

    AudioManager:Play( Sfx.SFX_VO_DONT_GIVE_UP )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ReviveH )
        end
        ),
        function()
            ReviveCounter( 5 )
        end
        )
end

function ReviveMenuHide( result )
    if not active then return end

    active = false

    if result then
        AudioManager:Play( Sfx.SFX_VO_TAKE_YOUR_REVENGE )
    elseif not registry:Get( "/internal/survival" ) then
        AudioManager:Play( Sfx.SFX_VO_YOU_FAILED )
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - ( 1 - SmoothStep( t ) ) * ReviveH )
            counterwrap:SetAlpha( 255 * (1-t) )
        end
        ),
        function()
            popup:SetVisibility( false )
            perkarea:SetTouchable( true )
            callback:Revive( result )
        end
        )
end

function ReviveMenuAction( path )
    if not active then return end


    if not iapInProgress and string.find( path, "ReviveButtonLive" ) then
        local soft, gold = Shop:GetCash()
        if gold >= cost then
            Shop:SetCash( soft, gold - cost )
            cost = math.min( cost + coststep, costmax )
            registry:Set( "/monstaz/player/revive", cost )
            ReviveMenuHide( true )
        else
            iapInProgress = true
            RviveRegisterIapCallbacks()
            anim:RemoveAll( "revive-counter" )
            Shop:BuyVirtualCash( ShopFindBestIap( 0, cost ) )
        end
    elseif not iapInProgress and string.find( path, "ReviveButtonDie" ) then
        ReviveMenuHide( false )
    elseif path == "/ShopSuccessPopup/OK/Button" then
        ReviveIapSuccessfull()
    elseif path == "/ShopFailPopup/Back/Button" then
        RevifeIapFailed()
    end
end

function RviveUnregisterIapCallbacks()
    RegisterOnTransactionCancellCallback( nil )
    if IS_AMAZON then
        RegisterOnTransactionCompletedCallback( nil )
        RegisterOnTransactionFailedCallback( nil )
    end
end

function RviveRegisterIapCallbacks()
    RegisterOnTransactionCancellCallback( RevifeIapFailed )
    if IS_AMAZON then
        RegisterOnTransactionCompletedCallback( ReviveIapSuccessfull )
        RegisterOnTransactionFailedCallback( RevifeIapFailed )
    end
end

function ReviveIapSuccessfull()
    iapInProgress = false
    RviveUnregisterIapCallbacks()
    ReviveMenuAction( "ReviveButtonLive" )
end

function RevifeIapFailed()
    iapInProgress = false
    RviveUnregisterIapCallbacks()
    ReviveCounter( counterValue )
end


function ReviveCounter( num )
    counterValue = num
    countergfx:SetImage( "menu2/number_" .. num .. ".png@linear" )

    counter:SetVisibility( true )
    anim:Add( Slider:New( 1,
        function( t )
            countergfx:SetScale( 1 + t * 10 )
            counter:SetAlpha( 255 * ( 1 - t*t ) )
        end
        ),
        function()
            if active then
                if num == 0 then
                    ReviveMenuHide( false )
                else
                    ReviveCounter( num - 1 )
                end
            end
        end,
        "revive-counter"
        )
end

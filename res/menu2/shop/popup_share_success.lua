require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/shop/share_common.lua" )
require( "menu2/star_animation.lua" )
require( "ItemDB.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 180 )
local height = R( 180 )

function ShopShareSuccessPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_share_success.xml" )
    popup = screen:GetControl( "/ShopShareSuccessPopup" )
    popup:SetVisibility( false )
    
    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
end

function ShopShareSuccessPopupShow()
    if active then
        return
    end
    active = true
    FillShareRewardText( popup:GetControl( "Reward" ) )
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    StartStarAnimation( popup:GetControl( "Star" ) )
end

function ShopShareSuccessPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopShareSuccessPopupHide()
        return true
    end

    return false
end

function ShopShareSuccessPopupPress( control, path )
    if not active then
        return
    end

    if path == "/ShopShareSuccessPopup/Back/Button" then
        ButtonPress( control )
    end
end

function ShopShareSuccessPopupAction( name )
    if not active then
        return
    end

    if name == "/ShopShareSuccessPopup/Back/Button" then
        ShopShareSuccessPopupHide()
    end
end

function ShopShareSuccessPopupHide()
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            StopStarAnimation( popup:GetControl( "Star" ) )
            ShopSharePopupShow( nil, false )
        end
    )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 190 )

function ShopInfoPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_info.xml" )
    popup = screen:GetControl( "/ShopInfoPopup" )
    popup:SetVisibility( false )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
end

function ShopInfoPopupShow( item )
    if active then
        return
    end
    active = true
    PopupLock()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function ShopInfoKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopInfoPopupHide()
        return true
    end

    return false
end

function ShopInfoPopupPress( control, path )
    if not active then
        return
    end

    if path == "/ShopInfoPopup/OK/Button" then
        ButtonPress( control )
    end
end

function ShopInfoPopupAction( name )
    if not active then
        return
    end

    if name == "/ShopInfoPopup/OK/Button" then
        ShopInfoPopupHide()
    end
end

function ShopInfoPopupHide()
    active = false

    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
    )
end

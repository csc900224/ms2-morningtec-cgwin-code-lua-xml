local ShopLayoutConstants = {
    layoutMargin = {
        top = Y(5),
        bottom = R(5),
        left = R(5),
        right = R(5)
    },
    buttonSpacing = R(3),
    clipRectOffsetX = R(-4)
}

function CalculateShopButtonsLayout( root )
    -- Back button
    local component = root:GetControl( "Buttons/Back" )
    component:SetY( ShopLayoutConstants.layoutMargin.top )
    w, h = component:GetGraphic( "TS" ):GetSize()
    component:SetX( SCREEN_WIDTH - ShopLayoutConstants.layoutMargin.right - w )
    local backButtonBottom = component:GetY() + h

    -- Free Stuf button
    component = root:GetControl( "Buttons/FreeStuff" )
    w, h = component:GetGraphic( "TS" ):GetSize()
    component:SetY( ShopLayoutConstants.layoutMargin.top )
    component:SetX( ShopLayoutConstants.layoutMargin.left )
    local freeStuffH = h

    -- OST button
    component = root:GetControl( "Buttons/OST" )
    component:SetY( ShopLayoutConstants.layoutMargin.top )
    component:SetX( ShopLayoutConstants.layoutMargin.left )
    
    -- More Games
    component = root:GetControl( "Buttons/MoreGames" )
    w, h = component:GetGraphic( "TS" ):GetSize()
    component:SetY( ShopLayoutConstants.layoutMargin.top + freeStuffH + ShopLayoutConstants.buttonSpacing )
    component:SetX( ShopLayoutConstants.layoutMargin.left )
    local moreGamesBottom = component:GetY() + h
end

function CalculateShopLayout( root )
    -- More Games
    local component = root:GetControl( "Buttons/MoreGames" )
    w, h = component:GetGraphic( "TS" ):GetSize()
    local moreGamesBottom = component:GetY() + h

    -- Avatar
    component = root:GetControl( "AvatarRoot/Avatar" )
    component:SetX( math.floor( ShopLayoutConstants.layoutMargin.left ) )
    w, h = callback:GetControlSize( root:GetPath() .. "/AvatarRoot/Avatar" )
    component:SetY( math.max( moreGamesBottom * 0.75 + ( SCREEN_HEIGHT - moreGamesBottom - h ) / 2, moreGamesBottom ))
    local avatarRight = component:GetX() + w
    local avatarTop = component:GetY()
    local avatarH = h
    local avatarW = w

    -- Tabs
    component = root:GetControl( "Tabs" )
    w, h = callback:GetControlSize( root:GetPath() .. "/Tabs" )
    component:SetY( (moreGamesBottom + ShopLayoutConstants.buttonSpacing + avatarTop) / 2 - h )
    component:SetX( SCREEN_WIDTH - ShopLayoutConstants.layoutMargin.right - w )
    local tabsBottom = component:GetY() + h 

    -- Items
    component = root:GetControl( "ItemsRoot" )
    component:SetX( avatarRight + ShopLayoutConstants.clipRectOffsetX )
    component:SetY( avatarTop )
    local itemsTop = component:GetY()

    -- Avatar Gradient
    component = root:GetControl( "AvatarRoot/Gradient" )
    w, h = component:GetGraphic( "TS" ):GetSize()
    local itemsW, itemsH = GetShopItemsListSize( root )
    local hegightOffset = R(14)
    local targetHeight = avatarH
    component:SetX( avatarRight - w * 0.4 )
    component:SetY( math.max( itemsTop - hegightOffset/2, avatarTop ) )
    component:GetGraphic( "TS" ):SetScale( 1, targetHeight/h )
end

function GetShopAvatarSize( root )
    local component = root:GetControl( "Buttons/MoreGames" )
    w, h = component:GetGraphic( "TS" ):GetSize()
    return math.max(SCREEN_HEIGHT - (component:GetY() + h) - R(20), R( 190 ) )
end

function GetShopItemsListSize( root )
    local itemsRoot = root:GetControl( "ItemsRoot" )
    local w = SCREEN_WIDTH - itemsRoot:GetX()
    local h = SCREEN_HEIGHT - itemsRoot:GetY()
    local maxItemH = h
    
    local avatar = root:GetControl( "AvatarRoot/Avatar" )
    local avatarW, avatarH = callback:GetControlSize( root:GetPath() .. "/AvatarRoot/Avatar" )
    local avatarBottom = avatar:GetY() + avatarH
    
    -- make sure bottom of list will hide behind avatar box
    if avatarBottom < itemsRoot:GetY() + maxItemH then
        maxItemH = avatarBottom - itemsRoot:GetY()
    end
        
    return w, h, maxItemH
end
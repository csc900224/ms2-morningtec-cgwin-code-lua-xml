require( "menu2/common.lua" )
require( "ItemDB.lua" )

function FillShareRewardText( textControl )
    if ItemShopShareReward.soft > 0 then
        textControl:SetTextWithVariables( "TEXT_SHARE_REWARD", { VALUE = tostring(ItemShopShareReward.soft), ICON="TEXT_ICON_CASH" } )
    elseif ItemShopShareReward.hard > 0 then
        textControl:SetTextWithVariables( "TEXT_SHARE_REWARD", { VALUE = tostring(ItemShopShareReward.hard), ICON="TEXT_ICON_GOLD" } )
    elseif ItemShopShareReward.fuel > 0 then
        textControl:SetTextWithVariables( "TEXT_SHARE_REWARD", { VALUE = tostring(ItemShopShareReward.fuel), ICON="TEXT_ICON_FUEL" } )
    end
end

function GiveShopShareAward()
    local s, h = Shop:GetCash()
    if ItemShopShareReward.soft > 0 then
        s = s + ItemShopShareReward.soft
    elseif ItemShopShareReward.hard > 0 then
        h = h + ItemShopShareReward.hard
    elseif ItemShopShareReward.fuel > 0 then
        UpdateRegKeyValue( "/monstaz/cash/fuel", ItemShopShareReward.fuel )
    end
    Shop:SetCash( s, h )
end

function UpdateRegKeyValue( regKey, delta )
    local val = registry:Get( regKey )
    registry:Set( regKey, val + delta )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "ItemDB.lua" )

local active = false
local popup = nil
local itemId = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 140 )

local parent = nil
local callbackReturn = nil

function ShopNoCashPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_no_cash.xml" )
    popup = screen:GetControl( "/ShopNoCashPopup" )

    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    if not IAPS_ENABLED then
        popup:GetControl( "Message" ):SetVisibility( false )
        local get = popup:GetControl( "Get" )
        local back = popup:GetControl( "Back" )
        get:SetVisibility( false )
        back:SetX( (width - back:GetControl( "Button" ):GetSize())/2 )
    end
    popup:SetVisibility( false )
end


function ShopNoCashPopupShow( expensiveItemId, parentList , callback )
    if active then
        return
    end
    active = true
    parent = parentList
    PopupLock()
    itemId = expensiveItemId
    callbackReturn = callback

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function ShopNoCashKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopNoCashPopupHide(false)
        return true
    end

    return false
end

function ShopNoCashPopupPress( control, path )
    if not active then
        return
    end

    if path == "/ShopNoCashPopup/Back/Button"
        or path == "/ShopNoCashPopup/Get/Button" then
        ButtonPress( control )
    end
end

function ShopNoCashPopupAction( name )
    if not active then
        return
    end

    if name == "/ShopNoCashPopup/Get/Button" then
        ShopNoCashPopupHide(true)
    elseif name == "/ShopNoCashPopup/Back/Button" then
        ShopNoCashPopupHide(false)
    end
end

function ShopNoCashPopupHide( getCash )
    active = false

    if callbackReturn ~= nil then 
        if getCash == true then
            PopupUnlock()
        end
    else
        PopupUnlock()
    end
    
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_NO_CASH_ACTION, getCash and GameEventParam.GEP_NO_CASH_GET or GameEventParam.GEP_NO_CASH_BACK )

    if getCash then
        if ShopActive and ShopShow then
            ShopShow( ShopTab.Cash )
        elseif PremissionActive and CountersAction then
            CountersShopShow()
        elseif itemId then
            ShopNoCashPopupBuyIapForItem()
        end
    else
        if callbackReturn ~= nil then
            callbackReturn()
            callbackReturn = nil
        end
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end,
        function()
            popup:SetVisibility( false )
        end
        )
    )
end

function ShopNoCashPopupBuyIapForItem()
    local priceSoft, priceHard = 0
    if ItemDbCanUpgrade(itemId) and Shop:IsBought(itemId) > 0 then
        priceSoft, priceHard = ItemDbGetItemUpgradePrice( itemId )
    else
        priceSoft, priceHard = ItemDbGetItemPrice( itemId )
    end
    
    local bestIap = ShopFindBestIap( priceSoft, priceHard )
    
    if parent and parent.OnBuyVirtualCashTry then
        parent:OnBuyVirtualCashTry( bestIap )
    end
    Shop:BuyVirtualCash( bestIap )
end

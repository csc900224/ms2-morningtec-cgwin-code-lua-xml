require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 140 )

ShopFailReason = {
    Error = "TEXT_TRANSACTION_FAILED",
    NotSupported = "TEXT_IAP_NOT_SUPPORTED"
}

function ShopFailPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_fail.xml" )
    popup = screen:GetControl( "/ShopFailPopup" )
    popup:SetVisibility( false )
    
    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
end


function ShopFailPopupShow( reason )
    if active then
        return
    end
    active = true
    PopupLock()
    
    popup:GetControl( "Message" ):SetText( reason )
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function ShopFailKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopFailPopupHide()
        return true
    end

    return false
end

function ShopFailPopupPress( control, path )
    if not active then
        return
    end

    if path == "/ShopFailPopup/Back/Button" then
        ButtonPress( control )
    end
end

function ShopFailPopupAction( name )
    if not active then
        return
    end

    if name == "/ShopFailPopup/Back/Button" then
        ShopFailPopupHide()
    end
end

function ShopFailPopupHide()
    active = false
    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end,
        function()
            popup:SetVisibility( false )
        end
        )
    )
end

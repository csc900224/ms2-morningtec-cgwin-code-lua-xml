require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/shop/shop_item.lua" )
require( "menu2/shop/scroll_indicator.lua" )
require( "ItemDB.lua" )

ShopItemList = {
    root = nil,
    clip = nil,
    width = nil,
    height = nil,
    items = nil,
    currentCat = nil,
    indicator = nil,
    mode = 0,
    selectCallback = nil,

    scrollIndicator = nil,

    Consts = {
        itemHeight = 0,
        itemWidth = GAME_SCALE == 1 and R(120) or R(110),
        itemSpacing = R(-3)
    },

    ScrollData = {
        touchId = -1,
        minScrollDist = R(5),
        maxScrollOffset = 0,
        scrollOffset = 0,
        sampleTouchPos = 0,
        currentTouchPos = 0,
        startScrollOffset = 0,
        touchStart = 0,
        scrolling = false,
        targetScrollOffset = 0,
        scrollAcceleration = 1,
        samplingInterval = 0.025,
        sampleTimer = -1,
        velocityEpsilon = 0.1,
        velocity = 0,
        lastVelocity = 0,
        draggTime = 0
    }
}

local notificationsQueue = {} -- Global table for transaction restore notifications
local transactionListeners = {}

ShopItemListMode = {
    normal = 1,
    quickShop = 2,
    weaponSelect = 3
}

function ShopItemList:new (o)
    o = o or {}   -- create object if user does not provide one
    o.Consts = {}
    o.ScrollData = {}

    setmetatable(o, self)
    setmetatable(o.Consts, self.Consts)
    setmetatable(o.ScrollData, self.ScrollData)
    
    self.__index = self
    self.Consts.__index = self.Consts
    self.ScrollData.__index = self.ScrollData

    return o
end

function ShopItemList:Init( parent, shopMode, listWidth, listHeight, maxItemH, itemWidth, itemSpacing )
    self.mode = shopMode
    parent:InsertFromXml( "menu2/shop/shop_item_list.xml" )
    self.clip = parent:GetControl( "ShopItemList" )
    self.root = parent:GetControl( "ShopItemList/Items" )
    self.indicator = parent:GetControl( "ScrollIndicatorRoot" )
    self.width = listWidth
    self.height = listHeight

    if itemWidth ~= nil then
        self.Consts.itemWidth = itemWidth
    end
    if maxItemH == nil then
        self.Consts.itemHeight = listHeight
    else
        self.Consts.itemHeight = maxItemH
    end
    if itemSpacing ~= nil then
        self.Consts.itemSpacing = itemSpacing
    end 

    self.scrollIndicator = ScrollIndicator:new()
    self.scrollIndicator:Init( self.indicator, 0, self.Consts.itemWidth )
    self:CaluclateLayout()

    self.clip:GetGraphic( "r" ):SetWidth( self.width )
    self.clip:GetGraphic( "r" ):SetHeight( self.Consts.itemHeight )
end

function ShopItemList:SetSelectCallback( callback )
    self.selectCallback = callback
end

function ShopItemList:CaluclateLayout()
    local iW, iH = callback:GetControlSize( self.indicator:GetPath() )
    self.Consts.itemHeight = math.min( self.height - iH, self.Consts.itemHeight)
    self.indicator:SetY( self.Consts.itemHeight )
    self.indicator:SetX( (self.width - iW)/2 )
end

function ShopItemList:GetItemSize()
    return self.Consts.itemWidth, self.Consts.itemHeight
end

function ShopItemList:GetItemSpacing()
    return self.Consts.itemSpacing
end

function ShopItemList:UpdateIndicatorPos()
    local iW, iH = callback:GetControlSize( self.indicator:GetPath() )
    self.indicator:SetX( (self.width - iW)/2 )
end

function ShopItemList:SetContent( itemList, resetScroll )
    if self.resetScroll == nil or self.resetScroll == true then
        self:ResetScroll()
    end
    self:DropAllItems()
    
    self.items = itemList
    local x = 0
    if itemList then
        for _, item in ipairs( itemList ) do
            local itemRoot = self.root:InsertControl( tostring(item.id) )
            itemRoot:SetRelative( true )
            itemRoot:SetX( x )
            ShopItemInit( itemRoot, self.Consts.itemWidth, self.Consts.itemHeight, item, self:GetShopItemMode() )
            x = x + self.Consts.itemWidth + self.Consts.itemSpacing
        end
    end
    self.ScrollData.maxScrollOffset = math.max( x - self.Consts.itemSpacing - self.width, 0 )

    self.scrollIndicator:UpdateMaxScrollOffset( self.ScrollData.maxScrollOffset )
    self:UpdateIndicatorPos()
    self:ClipItems()
end

function ShopItemList:GetShopItemMode()
    return self.mode == ShopItemListMode.weaponSelect and ShopItemMode.select or ShopItemMode.normal
end

function ShopItemList:ResetScroll( offset )
    if not offset then
        offset = 0
    end

    self.ScrollData.scrollOffset = offset
    self.ScrollData.targetScrollOffset = offset
    self.root:SetX( -offset )

    self.ScrollData.scrolling = false
    self.ScrollData.touchId = -1
    self.ScrollData.velocity = 0
    self.ScrollData.lastVelocity = 0
    self.ScrollData.sampleTimer = -1
    self.ScrollData.draggTime = 0

    self:ClipItems()
end

function ShopItemList:FocusOn( item )
    local idx = nil
    local id = item.id or item
    for i,v in ipairs( self.root:GetChildren() ) do
        if id == v:GetName() then
            idx = i - 1
            break
        end
    end

    if not idx then return end

    self:ResetScroll( math.min( math.max( 0, -( self.width - self.Consts.itemWidth ) / 2 + self.Consts.itemWidth * idx ), self.ScrollData.maxScrollOffset ) )
end

function ShopItemList:Update( dt )
    if self.ScrollData.touchId >= 0 then
        self.ScrollData.draggTime = self.ScrollData.draggTime + dt
    end

    self:SampleVelocity( dt )
    self:UpdateScrolling( dt )
    
    self.scrollIndicator:UpdateScrollOffset( self.ScrollData.scrollOffset )
end

function ShopItemList:SampleVelocity( dt )
    if self.ScrollData.touchId >= 0 and self.ScrollData.sampleTimer >= 0 then
        self.ScrollData.sampleTimer = self.ScrollData.sampleTimer - dt
        if self.ScrollData.sampleTimer < 0 then
            self.ScrollData.sampleTimer = self.ScrollData.samplingInterval
            self.ScrollData.lastVelocity = (self.ScrollData.currentTouchPos - self.ScrollData.sampleTouchPos) / self.ScrollData.samplingInterval
            self.ScrollData.sampleTouchPos = self.ScrollData.currentTouchPos
        end
    end
end

function ShopItemList:TouchDown( x, y, id, c )
    if self.items == nil then return end

    local path = c:GetPath()
    if not TutorialActive and string.find( path, "/ShopItemList" ) then
        if self.ScrollData.touchId == id then
            self:TouchUp( self.ScrollData.currentTouchPos, y, id, nil )
        end

        if self.ScrollData.touchId < 0 then
            self.ScrollData.touchId = id
            self.ScrollData.touchStart = x
            self.ScrollData.scrolling = false
            self.ScrollData.velocity = 0
            self.ScrollData.lastVelocity = 0
            self.ScrollData.sampleTimer = self.ScrollData.samplingInterval
            self.ScrollData.currentTouchPos = x
            self.ScrollData.sampleTouchPos = x
            self.ScrollData.draggTime = 0
        end
    end
    
    if string.find( path, "/ShopItem" ) then
        ShopItemPress( c )
    end
end

function ShopItemList:TouchUp( x, y, id, c )
    if self.items == nil then return end

    local wasScrolling = false
    if self.ScrollData.scrolling and id == self.ScrollData.touchId then
        wasScrolling = true
        self.ScrollData.scrolling = false
        self.ScrollData.touchId = -1
        self.ScrollData.sampleTimer = -1
        self.ScrollData.targetScrollOffset = self:ClampOffset( self.ScrollData.startScrollOffset + self.ScrollData.touchStart - x )

        -- Check if dragg time is shorter than sampling rate. If so recalculate velocity manualny
        if self.ScrollData.draggTime < self.ScrollData.samplingInterval and self.ScrollData.draggTime > 0 then
            self.ScrollData.lastVelocity = (x - self.ScrollData.sampleTouchPos) / self.ScrollData.draggTime
        end
        self.ScrollData.velocity = self.ScrollData.lastVelocity
    elseif id == self.ScrollData.touchId then
        self.ScrollData.touchId = -1
        self.ScrollData.sampleTimer = -1
    end

    if not c then return end
    local path = c:GetPath()
    if string.find( path, "/ShopItem" ) then
        ShopItemAction( c, self )

        if not wasScrolling and AvatarSetWeapon then
            local item = ShopItemGetDbItem( ShopItemGetRoot( c ) )
            if item and item.a then
                AvatarSetWeapon( item.a )
            end
        end
    end
end

function ShopItemList:TouchMove( x, y, id )
    if self.items == nil then return end
    if id == self.ScrollData.touchId then
        if not self.ScrollData.scrolling then
            if math.abs( self.ScrollData.touchStart - x ) > self.ScrollData.minScrollDist then
                -- Start real scrolling
                self.ScrollData.touchStart = x
                self.ScrollData.startScrollOffset = self.ScrollData.scrollOffset
                self.ScrollData.scrolling = true
            end
        else
            self.ScrollData.targetScrollOffset = self:ClampOffset( self.ScrollData.startScrollOffset + self.ScrollData.touchStart - x )
            self.ScrollData.currentTouchPos = x
        end
    end
end

function ShopItemList:ClampOffset( offset )
    return math.min( math.max(offset, 0), self.ScrollData.maxScrollOffset )
end

function ShopItemList:UpdateScrolling( dt )
    if math.abs(self.ScrollData.velocity) > self.ScrollData.velocityEpsilon then
        self.ScrollData.velocity = self.ScrollData.velocity - self.ScrollData.velocity * self.ScrollData.scrollAcceleration * dt
        self.ScrollData.targetScrollOffset = self:ClampOffset( self.ScrollData.targetScrollOffset - self.ScrollData.velocity * dt )
    end

    if math.abs(self.ScrollData.scrollOffset - self.ScrollData.targetScrollOffset) > self.ScrollData.velocityEpsilon then
        self.ScrollData.scrollOffset = self.ScrollData.targetScrollOffset
        self.root:SetX( -self.ScrollData.scrollOffset )
        self:ClipItems()
    end  
end

function ShopItemList:ClipItems()
    for _, item in ipairs( self.root:GetChildren() ) do
        local visible = -self.ScrollData.scrollOffset + item:GetX() <= self.width and -self.ScrollData.scrollOffset + item:GetX() + self.Consts.itemWidth >= 0
        item:SetVisibility( visible )
    end
end

function ShopItemList:DropAllItems()
    for _, item in ipairs( self.root:GetChildren() ) do
        ShopItemRelease( item:GetControl( "ShopItem" ) )
        item:Remove()
    end
end

function ShopItemList:RefreshList()
    for _, item in ipairs( self.root:GetChildren() ) do
        local itemId = item:GetName()
        local visibility = item:GetVisibility()
        item:SetVisibility( true )
        ShopItemRefresh(item:GetChildren()[1], ItemsById[itemId], self.mode ~= ShopItemListMode.normal, self:GetShopItemMode())
        item:SetVisibility( visibility )
    end
end

function ShopItemList:OnShopItemBought( itemId )
    self:RefreshList()

    local item = ItemsById[itemId]
    if item.cat ~= ItemCategory.items then
        ShopSuccessPopupShow( item )
    end
end

function ShopItemList:OnItemSelected( itemId )
    if self.selectCallback then
        self.selectCallback( itemId )
    end
end

function ShopItemList:OnBuyVirtualCashTry( itemId )
    transactionListeners[itemId] = self
end

-- MorningTec Change Begin
function ShopItemList:OnBuyWeaponTry( itemId )
    transactionListeners[itemId] = self
end
-- MorningTec Change End

function ShopItemList:OnShopItemUpgrade( itemId )
    self:RefreshList()
end

function ProcessTransactionNotifications( dt )
    if #notificationsQueue > 0 and not PopupIsLocked() then
        ShopSuccessPopupShow( ItemsById[notificationsQueue[1]] )
        table.remove( notificationsQueue, 1 )
    end
end

function ShopItemList:OnShopItemCantAfford( itemId, category )
    -- if category == ItemCategory.weapons and ShopNoCashWeaponPopupInit then
    --     ShopNoCashWeaponPopupShow( itemId, self )
    -- else
    --     ShopNoCashElsePopupShow( itemId, self )
    -- end

    -- MorningTec Change Begin
    if itemId == ShopItem.Flamer or
       itemId == ShopItem.Magnum or
       itemId == ShopItem.LineGun or
       itemId == ShopItem.Railgun or
       itemId == ShopItem.CombatShotgun or
       itemId == ShopItem.Chaingun or
       itemId == ShopItem.Vortex or
       itemId == ShopItem.Chainsaw or
       itemId == ShopItem.Mech then
       -- MorningTec Begin
        -- ShopNoCashWeaponPopupShow( itemId, self )
        if Shop:IsBought( itemId ) == 0 then
            ShopNoCashElsePopupShow ( itemId, self )
        else
            ShopNoCashPopupShow (itemId, self)
        end
       -- MorningTec End

    elseif itemId == ShopItem.Nuke or
           itemId == ShopItem.Airstrike or
           itemId == ShopItem.Shield or
           itemId == ShopItem.CashBonus or
           itemId == ShopItem.ReloadBonus or
           itemId == ShopItem.AmmoBonus then
        ShopNoCashElsePopupShow( itemId, self )

    else
        ShopNoCashPopupShow( itemId, self )
    end
    -- MorningTec Change End
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_ITEM_CANT_AFFORD, 1, itemId )
end

function OnTransactionNotSupported()
    if IS_AMAZON then
        ShopWaitPopupHide(true)
    else
        ShopWaitPopupHide()
        ShopFailPopupShow( ShopFailReason.NotSupported )
    end
end

function OnTransactionStart()
    ShopWaitPopupShow()
end

local transFailedCallback = nil
function RegisterOnTransactionFailedCallback( callback )
    transFailedCallback = callback 
end

function OnTransactionFailed( itemId )
    if IS_AMAZON then
        ShopWaitPopupHide(true)
    else
        ShopWaitPopupHide()
        ShopFailPopupShow( ShopFailReason.Error )
    end
    
    if transFailedCallback then 
        transFailedCallback()
    end
end

local transCancellCallback = nil
function RegisterOnTransactionCancellCallback( callback )
    transCancellCallback = callback 
end

function OnTransactionCancel( itemId )
    ShopWaitPopupHide(true)
    if transCancellCallback then
        transCancellCallback()
    end
end

local transCompletedCallback = nil
function RegisterOnTransactionCompletedCallback( callback )
    transCompletedCallback = callback 
end

function OnTransactionComplete( itemId )
    if IS_AMAZON then
        ShopWaitPopupHide(true)
    else
        ShopWaitPopupHide()
        ShopSuccessPopupShow( ItemsById[itemId] )
    end

    local listener = transactionListeners[itemId];
    if listener ~= nil then
        listener:RefreshList()
    end
    
    if transCompletedCallback then
        transCompletedCallback()
    end
end

function OnTransactionRestore( itemId )
    table.insert( notificationsQueue, itemId )
end


require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/shop/popup_share.lua" )
require( "menu2/star_animation.lua" )
require( "menu2/background_star.lua" )

local active = false
local popup = nil
local star = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 210 )

local weaponAngle = 0
local itemBought = nil

local originalYayX = -1

function ShopSuccessPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_success.xml" )
    popup = screen:GetControl( "/ShopSuccessPopup" )
    popup:SetVisibility( false )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    ShopSuccessPopupFillTexts()

    ShopSharePopupInit( screen )

    star = BackgroundStar( popup, width/2, height/2 )
end

function ShopSuccessPopupFillTexts()
    local shareText = popup:GetControl( "Share/Text" )

    if ItemShopShareReward.soft > 0 then
        shareText:SetTextWithVariables( "TEXT_SHARE", { VALUE = tostring(ItemShopShareReward.soft), ICON="TEXT_ICON_CASH" } )
    elseif ItemShopShareReward.hard > 0 then
        shareText:SetTextWithVariables( "TEXT_SHARE", { VALUE = tostring(ItemShopShareReward.hard), ICON="TEXT_ICON_GOLD" } )
    elseif ItemShopShareReward.fuel > 0 then
        shareText:SetTextWithVariables( "TEXT_SHARE", { VALUE = tostring(ItemShopShareReward.fuel), ICON="TEXT_ICON_FUEL" } )
    end
end

function ShopSuccessPopupFillItemData( item )
    itemBought = item

    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( item.f )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )

    if item.cat == ItemCategory.weapons then
        icon:GetGraphic( "TS" ):SetAngle( weaponAngle )
    else
        icon:GetGraphic( "TS" ):SetAngle( 0 )
    end

    local message =  popup:GetControl( "Message" )
    message:SetTextWithVariables( "TEXT_CONGRATULATIONS_DESC", { NAME = item.d } )

    -- Hide Share button for IAPs
    local shareBtn = popup:GetControl( "Share" )
    local yayBtn = popup:GetControl( "OK" )
    if originalYayX < 0 then
        originalYayX = yayBtn:GetX()
    end


    -- if item.cat == ItemCategory.cash then
    --     shareBtn:SetVisibility( false )
    --     yayBtn:SetX( (shareBtn:GetX() + originalYayX) / 2 )
    -- else
    --     shareBtn:SetVisibility( true )
    --     yayBtn:SetX( originalYayX )
    -- end
    -- MonrningTec Change
    shareBtn:SetVisibility( false )
     yayBtn:SetX( (shareBtn:GetX() + originalYayX) / 2 )

end

function ShopSuccessPopupShow( item )
    if active then
        return
    end
    active = true
    PopupLock()

    ShopSuccessPopupFillTexts()
    ShopSuccessPopupFillItemData( item )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
    BackgroundStarStart( star, 0.3 )

    AudioManager:Play( Sfx.SFX_MENU_LEVEL_FINISHED )
end

function ShopSuccessKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopSuccessPopupHide(false)
        return true
    end

    return false
end

function ShopSuccessPopupPress( control, path )
    if not active then
        ShopSharePopupPress( control, path )
        return
    end

    if path == "/ShopSuccessPopup/OK/Button"
        or path == "/ShopSuccessPopup/Share/Button" then
        ButtonPress( control )
    end

end

function ShopSuccessPopupAction( name )
    if not active then
        ShopSharePopupAction( name )
        return
    end

    if name == "/ShopSuccessPopup/OK/Button" then
        TutorialAction( name )
        ShopSuccessPopupHide(false)
    elseif name == "/ShopSuccessPopup/Share/Button" then
        ShopSuccessPopupHide(true)
    end
end

function ShopSuccessPopupHide( share )
    active = false

    if not share then
        PopupUnlock()
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Icon/Star" ) )
            if share then
                ShopSharePopupShow( itemBought, false, true )
            end
            popup:SetVisibility( false )
        end
    )

    BackgroundStarStop( star, 0.3 )
end

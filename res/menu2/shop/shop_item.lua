require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "ItemDB.lua" )

local ItemFrameBlue = {
    popupFrame = PopupFrameBlue,
    lineFill = "menu2/line_01_fill.png@linear",
    lineEnd = "menu2/line_01_end.png@linear",
    gradient = "menu2/color_fill_med.png@linear",
    starBg = "menu2/star_small_2.png@linear",
    elFireBg = "menu2/ammo_fire_blue.png@linear",
    elFrostBg = "menu2/ammo_snow_blue.png@linear",
    elElectricBg = "menu2/ammo_slash_blue.png@linear",
    fontColor  = {
        gradient = {r=18, g=151, b=219},
        outline = {r=6, g=19, b=59},
        fill = {r=32, g=193, b=246}
    }
}

local ItemFrameGreen = {
    popupFrame = PopupFrameGreen,
    lineFill = "menu2/line_01_fill_green.png@linear",
    lineEnd = "menu2/line_01_end_green.png@linear",
    gradient = "menu2/color_fill_med_green.png@linear",
    starBg = "menu2/star_small_2_green.png@linear",
    elFireBg = "menu2/ammo_fire_green.png@linear",
    elFrostBg = "menu2/ammo_snow_green.png@linear",
    elElectricBg = "menu2/ammo_slash_green.png@linear",
    fontColor  = {
        gradient = {r=5, g=108, b=19},
        outline = {r=6, g=28, b=24},
        fill = {r=0, g=160, b=63}
    }
}

local ItemFrameRed = {
    popupFrame = PopupFrameRed,
    lineFill = "menu2/line_01_fill_red.png@linear",
    lineEnd = "menu2/line_01_end_red.png@linear",
    gradient = "menu2/color_fill_med_red.png@linear",
    starBg = "menu2/star_small_2_red.png@linear",
    elFireBg = "menu2/ammo_fire_red.png@linear",
    elFrostBg = "menu2/ammo_snow_red.png@linear",
    elElectricBg = "menu2/ammo_slash_red.png@linear",
    fontColor  = {
        gradient = {r=145, g=0, b=0},
        outline = {r=46, g=5, b=9},
        fill = {r=221, g=22, b=0}
    }
}

local Constants = {
    midleHeightRatio = 0.65,
    midleWidthRatio = 0.85,
    lineEndOffsetRatio = 0.75,
    barWidthRatio = 0.95,
    weaponAngle = 0,
    priceMargins = 0.05,
    buttonShadowWidth = R(2),
    noLvlButtonsOffsetRatio = 1.3,
    starsSpacing = 0,
    potentialAlha = 135,
    elementsSpacing = R(11),
    elementsInfoCenter = R(46) + R(11) * 1.5
}

ShopItemMode = {
    normal = 1,
    select = 2
}

local ElementMap = {}
ElementMap["Electric"] = Element.electric
ElementMap["Fire"] = Element.fire
ElementMap["Frost"] = Element.frost


function ShopItemInit( parent, width, height, item, mode )
    if not item then return end

    -- Insert from xml and get item root
    parent:InsertFromXml( "menu2/shop/shop_item.xml" )
    root = parent:GetControl( "ShopItem" )

    -- Select background color and set button groups visibility
    assets = CheckItemAvailability( root, item, mode )

    -- Create main "popup-like" frame
    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height, assets.popupFrame )
    local touch = root:GetGraphic( "r" )
    touch:SetWidth( width )
    touch:SetHeight( height )

    -- Create middle section background
    local middle = root:GetControl( "MiddleRoot/BG" )
    ShopItemCreateMiddle( middle, width, height, assets )

    -- Stats related
    ShopItemCreateInfoPage( root, assets, item )
    ShopItemChangeStatsAssets( root, assets, true )
    ShopItemChangeFontColors( root, assets, item.cat )

    -- Fill data (price, stock, etc.)
    ShopItemFillItemData( root, item )

    -- Calculate alginment of neccessary layout items
    CalculateShopItemLayout( root, width, height, item.cat, mode, item )
end

function ShopItemRefresh( root, item, fullRefresh, mode )
    local assets = CheckItemAvailability( root, item, mode )

    local frame = root:GetControl( "Frame" )
    PopupChangeFrame( frame, assets.popupFrame )

    local middle = root:GetControl( "MiddleRoot/BG" )
    ShopItemChangeMiddle( middle, assets )
    ShopItemChangeStatsAssets( root, assets )
    ShopItemChangeFontColors( root, assets, item.cat )

    if fullRefresh and fullRefresh == true then
        ShopItemUpdateItemData( root, item )
        UpdateShopItemLayout( root, mode )
    elseif item.cat == ItemCategory.weapons or item.cat == ItemCategory.perks then
        UpdateShopItemLayout( root, mode )
    end
end

function ShopItemRelease( root )
    local rootPath = root:GetPath()
    anim:RemoveAll( rootPath .. "/MiddleRoot/PageInfo/Weapon/ClipSize/Stars" )
    anim:RemoveAll( rootPath .. "/MiddleRoot/PageInfo/Weapon/Damage/Stars" )
    anim:RemoveAll( rootPath .. "/MiddleRoot/PageInfo/Weapon/FireRate/Stars" )
    anim:RemoveAll( rootPath .. "/MiddleRoot/PageInfo/Weapon/Elements/Icons" )
end

function CheckItemAvailability( root, item, mode )
    if item.cat == ItemCategory.cash then
        root:GetControl( "Buttons" ):SetVisibility( true )
        root:GetControl( "ButtonsNoLvl" ):SetVisibility( false )
        root:GetControl( "Cash" ):SetVisibility( true )
        root:GetControl( "MiddleRoot/Price" ):SetVisibility( false )
        if item.subscription then
            local bought = registry:Get( "/monstaz/subscription" )
            root:GetControl( "Buttons/Buy" ):SetVisibility( not bought )
            root:GetControl( "Buttons/OutOfStockMsg" ):SetVisibility( bought )
            return bought and ItemFrameGreen or ItemFrameBlue
        else
            return ItemFrameBlue
        end
    else
        local owned, owendInSession = Shop:IsBought( item.id )
        local outOfStock = item.maxPerGame ~= nil and owendInSession >= item.maxPerGame or false
        local maxedOut = owned >= item.max
        local lvlOK = ShopItemIsLevelOK( item )

        if owned > 0 then
            root:GetControl( "Buttons" ):SetVisibility( true )
            root:GetControl( "ButtonsNoLvl" ):SetVisibility( false )

            local buy = root:GetControl( "Buttons/Buy" )
            local upgrade =  root:GetControl( "Buttons/Upgrade" )
            local upgradeSmall = root:GetControl( "Buttons/UpgradeSmall" )

            if item.cat == ItemCategory.items or item.cat == ItemCategory.perks then
                buy:SetVisibility( not (maxedOut or outOfStock) )
                upgrade:SetVisibility( false )
            else
                buy:SetVisibility( false )
                local canUpgrade = CanUpgradeShopItem( item )
                if mode == ShopItemMode.normal then
                    upgrade:SetVisibility( canUpgrade )
                else
                    local select = root:GetControl( "Buttons/Select" )
                    local selectSmall = root:GetControl( "Buttons/SelectSmall" )

                    upgrade:SetVisibility( false )
                    upgradeSmall:SetVisibility( canUpgrade )
                    select:SetVisibility( not canUpgrade )
                    selectSmall:SetVisibility( canUpgrade )
                end
            end
            root:GetControl( "Buttons/FullMsg" ):SetVisibility( maxedOut and mode ~= ShopItemMode.select )
            root:GetControl( "Buttons/OutOfStockMsg" ):SetVisibility( not maxedOut and outOfStock and mode ~= ShopItemMode.select )
            root:GetControl( "MiddleRoot/Price" ):SetVisibility( buy:GetVisibility() or upgrade:GetVisibility() or upgradeSmall:GetVisibility() )
        elseif not outOfStock then
            root:GetControl( "Buttons/Buy" ):SetVisibility( true )
            root:GetControl( "Buttons/Upgrade" ):SetVisibility( false )

            root:GetControl( "Buttons" ):SetVisibility( lvlOK )
            root:GetControl( "ButtonsNoLvl" ):SetVisibility( not lvlOK )

            if not lvlOK and (item.cu == nil or item.cu == 0 or not ShopItemIsUnlockLevelOk( item ) ) then
                root:GetControl( "ButtonsNoLvl/Buy" ):SetVisibility( false )
            end
        else -- not owned and out of stock
            root:GetControl( "Buttons" ):SetVisibility( true )
            root:GetControl( "ButtonsNoLvl" ):SetVisibility( false )

            root:GetControl( "Buttons/Buy" ):SetVisibility( false )
            root:GetControl( "Buttons/Upgrade" ):SetVisibility( false )

            root:GetControl( "Buttons/FullMsg" ):SetVisibility( false )
            root:GetControl( "Buttons/OutOfStockMsg" ):SetVisibility( true )
        end

        if owned > 0 then
            return ItemFrameGreen
        elseif lvlOK and ShopItemCanAfford(item) then
            return ItemFrameBlue
        else 
            return ItemFrameRed
        end
    end
end

function ShopItemCanAfford( item )
    local playerCashSoft, playerCashHard = Shop:GetCash()
    local itemSoft, itemHard = ItemDbGetItemPrice( tostring(item.id) )
    return (playerCashSoft >= itemSoft) and (playerCashHard >= itemHard)
end

function ShopItemCanAffordUnlock( item )
    local playerCashSoft, playerCashHard = Shop:GetCash()
    return item.cu == nil or playerCashHard >= item.cu
end

function ShopItemCanAffordUpgrade( item )
    local playerCashSoft, playerCashHard = Shop:GetCash()
    local itemSoft, itemHard = ItemDbGetItemUpgradePrice( tostring(item.id) )
    return (playerCashSoft >= itemSoft) and (playerCashHard >= itemHard)
end

function ShopItemIsLevelOK( item )
    local playerLevel = registry:Get( "/monstaz/player/level" )
    return item.lvl == nil or playerLevel >= item.lvl   
end

function ShopItemIsUnlockLevelOk( item )
    if ItemUnlockLevelRange > 0 then
        local playerLevel = registry:Get( "/monstaz/player/level" )
        return (item.lvl - playerLevel) <= ItemUnlockLevelRange
    elseif ItemUnlockLevelRange == 0 then
        return false
    else
        return true
    end
end

function CanUpgradeShopItem( item )
    return ItemDbCanUpgrade( tostring( item.id ) )
end

function ShopItemCreateMiddle( root, width, height, assets )
    if Constants.midleHeightRatio < 0 then    
        local btn = root:GetControl( "../../Buttons/Buy" )
        local _, buttonHeight = btn:GetGraphic( "s" ):GetSize()
        Constants.midleHeightRatio = 1 - (buttonHeight * 1.4 / height)
    end

    local middleW = width * Constants.midleWidthRatio
    local middleH = height * Constants.midleHeightRatio
    local middleX = (width - middleW) / 2

    local gradient = root:InsertControl( "Gradient" )
    gradient:AddRepresentation( "default", "menu2/tile.xml" )
    gradient:SetRelative( true )
    gradient:GetGraphic( "s" ):SetImage( assets.gradient )
    gradeintW, gradeintH  = gradient:GetGraphic( "s" ):GetSize()
    gradient:GetGraphic( "s" ):SetClipRect( 0, 0, middleW, gradeintH )

    local lineL = gradient:InsertControl("lineL")
    lineL:AddRepresentation( "default", "menu2/timage.xml" )
    lineL:SetRelative( true )
    lineL:GetGraphic( "TS" ):SetImage( assets.lineEnd )
    lineL:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    lineEndW, lineEndH = lineL:GetGraphic( "TS" ):GetSize()
    lineY = gradeintH - lineEndH / 2
    lineL:SetX( -lineEndW * Constants.lineEndOffsetRatio )
    lineL:SetY( lineY )

    local lineR = gradient:InsertControl("lineR")
    lineR:AddRepresentation( "default", "menu2/timage.xml" )
    lineR:SetRelative( true )
    lineR:GetGraphic( "TS" ):SetImage( assets.lineEnd )
    lineR:SetX( middleW - lineEndW * (1-Constants.lineEndOffsetRatio) )
    lineR:SetY( lineY )

    local lineF = gradient:InsertControl("lineF")
    lineF:AddRepresentation( "default", "menu2/tile.xml" )
    lineF:SetRelative( true )
    lineF:GetGraphic( "s" ):SetImage( assets.lineFill )
    lineF:SetX( lineEndW * (1-Constants.lineEndOffsetRatio) )
    lineF:SetY( lineY )
    lineF:GetGraphic( "s" ):SetClipRect( 0, 0, middleW - lineEndW * (1-Constants.lineEndOffsetRatio) * 2 + 0.5, lineEndH )
end

function ShopItemCreateStars( starsRoot, assets )
    local x = 0
    for i=1,5 do
        local star = starsRoot:InsertControl( tostring(i) )
        star:SetRelative( true )

        star:AddRepresentation( "default", "menu2/timage.xml" )
        star:GetGraphic( "TS" ):SetImage( assets.starBg )
        star:AddRepresentation( "active", "menu2/timage.xml" )
        star:SetRepresentation( "active" )
        star:GetGraphic( "TS" ):SetImage( "menu2/star_small.png@linear" )

        star:SetX( x )
        local sW, sH = star:GetGraphic( "TS" ):GetSize()
        x = x + sW + Constants.starsSpacing

        star:GetGraphic( "TS" ):SetPivot( sW/2, sH/2 )
        star:SetX( star:GetX() + sW/2 )
        star:SetY( star:GetY() + sH/2 )

        star:SetRepresentation( "default" )
        star:GetGraphic( "TS" ):SetPivot( sW/2, sH/2 )
    end
end

function ShopItemCreateInfoPage( root, assets, item )
    if item.cat == ItemCategory.weapons then
        ShopItemCreateStars( root:GetControl( "MiddleRoot/PageInfo/Weapon/Damage/Stars" ), assets )
        ShopItemCreateStars( root:GetControl( "MiddleRoot/PageInfo/Weapon/FireRate/Stars" ), assets )
        ShopItemCreateStars( root:GetControl( "MiddleRoot/PageInfo/Weapon/ClipSize/Stars" ), assets )
    end
end

function ShopItemChangeMiddle( root, assets )
    local gradient = root:GetControl( "Gradient" )

    local rX, rY, rW, rH = gradient:GetGraphic( "s" ):GetClipRect()
    gradient:GetGraphic( "s" ):SetImage( assets.gradient )
    gradient:GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH )

    gradient:GetControl( "lineL" ):GetGraphic( "TS" ):SetImage( assets.lineEnd )
    gradient:GetControl( "lineR" ):GetGraphic( "TS" ):SetImage( assets.lineEnd )

    rX, rY, rW, rH = gradient:GetControl( "lineF" ):GetGraphic( "s" ):GetClipRect()
    gradient:GetControl( "lineF" ):GetGraphic( "s" ):SetImage( assets.lineFill )
    gradient:GetControl( "lineF" ):GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH  )
end

function ShopItemChangeStarsAssets( starsRoot, assets )
    for _,star in ipairs( starsRoot:GetChildren() ) do
        ShopItemUpdateRepresentation( star, "default", assets.starBg )
    end
end

function ShopItemChangeStatsAssets( root, assets, initial )
    local pageMain = root:GetControl( "MiddleRoot/PageMain" )
    local pageInfo = root:GetControl( "MiddleRoot/PageInfo" )

    ShopItemUpdateRepresentation( pageMain:GetControl( "Elements/Electric" ), "default",  assets.elElectricBg )
    ShopItemUpdateRepresentation( pageMain:GetControl( "Elements/Frost" ), "default",  assets.elFrostBg )
    ShopItemUpdateRepresentation( pageMain:GetControl( "Elements/Fire" ), "default",  assets.elFireBg )

    local infoElements = pageInfo:GetControl( "Weapon/Elements/Icons" )
    ShopItemUpdateRepresentation( infoElements:GetControl( "Electric" ), "default",  assets.elElectricBg )
    ShopItemUpdateRepresentation( infoElements:GetControl( "Frost" ), "default",  assets.elFrostBg )
    ShopItemUpdateRepresentation( infoElements:GetControl( "Fire" ), "default",  assets.elFireBg )

    if not initial then
        ShopItemChangeStarsAssets( pageInfo:GetControl( "Weapon/Damage/Stars" ), assets )
        ShopItemChangeStarsAssets( pageInfo:GetControl( "Weapon/FireRate/Stars" ), assets )
        ShopItemChangeStarsAssets( pageInfo:GetControl( "Weapon/ClipSize/Stars" ), assets )
    end
end

function ShopItemUpdateRepresentation( control, representation, imagePath )
    local oldRep = control:GetRepresentation()
    control:SetRepresentation( representation )
    control:GetGraphic( "TS" ):SetImage( imagePath )
    control:SetRepresentation( oldRep )
end

function ShopItemUpdateFontColor( control, assets )
    control:SetShaderColors( assets.fontColor.gradient.r, assets.fontColor.gradient.g, assets.fontColor.gradient.b,
                             assets.fontColor.outline.r, assets.fontColor.outline.g, assets.fontColor.outline.b,
                             assets.fontColor.fill.r, assets.fontColor.fill.g, assets.fontColor.fill.b )
end

function ShopItemChangeFontColors( root, assets, category )
    if category == ItemCategory.weapons then
        local weaponInfo = root:GetControl( "MiddleRoot/PageInfo/Weapon" )
        ShopItemUpdateFontColor( weaponInfo:GetControl( "Damage/Label" ), assets )
        ShopItemUpdateFontColor( weaponInfo:GetControl( "FireRate/Label" ), assets )
        ShopItemUpdateFontColor( weaponInfo:GetControl( "ClipSize/Label" ), assets )
        ShopItemUpdateFontColor( weaponInfo:GetControl( "Elements/Label" ), assets )
    elseif category == ItemCategory.perks or category == ItemCategory.cash then
        local perkInfo = root:GetControl( "MiddleRoot/PageInfo/Perk" )
        ShopItemUpdateFontColor( perkInfo, assets )
    end
end

function ShopItemFillItemData( root, item )
    -- Name
    local label = root:GetControl( "Label" )
    label:SetVisibility( item.d ~= nil )
    if item.d then
        label:SetText( item.d )
    end

    -- Icon
    local icon = root:GetControl( "MiddleRoot/PageMain/Icon" )
    icon:GetGraphic( "TS" ):SetImage( item.f )

    -- Elements
    local elements = root:GetControl( "MiddleRoot/PageMain/Elements" )
    if item.cat == ItemCategory.weapons then
        elements:SetVisibility( true )
    else
        elements:SetVisibility( false )
    end

    local badge = root:GetControl( "MiddleRoot/PageMain/Badge" )
    if item.badge then
        badge:SetVisibility( true )
        if item.badge == 1 then
            badge:GetGraphic( "s" ):SetImage( "menu2/offers_only_now.png@linear" )
        else
            badge:GetGraphic( "s" ):SetImage( "menu2/offers_10_off.png@linear" )
        end
    else
        badge:SetVisibility( false )
    end

    -- Cash bonus
    local bonus = root:GetControl( "MiddleRoot/Bonus" )
    if item.bonus and item.bonus > 0 then
        bonus:SetVisibility( true )
        bonus:GetControl( "Value" ):SetTextWithVariables( "TEXT_SHOP_BONUS", { VALUE = tostring(item.bonus) } )
    else
        bonus:SetVisibility( false )
    end

    -- Info button
    local info = root:GetControl( "InfoButton" )
    info:SetVisibility( item.cat == ItemCategory.weapons or item.cat == ItemCategory.perks or item.desc )

    -- Rank
    if item.lvl ~= nil then
        local rank = root:GetControl( "ButtonsNoLvl/Msg/Rank" )
        rank:SetTextRaw( tostring( item.lvl ) )
    end

    -- Unlock price
    if item.cu ~= nil and ShopItemIsUnlockLevelOk( item ) then
        local unlockPrice = root:GetControl( "ButtonsNoLvl/Buy/Value" )
        --local gold = root:GetControl( "ButtonsNoLvl/Buy/Gold" )
        --unlockPrice:SetX( -gold:GetWidth()/2 )
		unlockPrice:SetTextRaw( screen:GetText( "TEXT_BUY_NOW" ))
        --unlockPrice:SetTextRaw( screen:GetText( "TEXT_BUY_NOW" ) .. tostring( item.cu ) )
        --gold:SetX( unlockPrice:GetX() + unlockPrice:GetWidth()/2 + unlockPrice:GetTextWidth()/2 )
    end

    -- Stock
    local stock = root:GetControl( "Stock" )
    stock:SetVisibility( item.cat == ItemCategory.items )

    -- Full message 
    local fullMsg = root:GetControl( "Buttons/FullMsg" )
    fullMsg:SetText( item.cat == ItemCategory.items and "TEXT_INV_FULL" or "TEXT_MAX_OUT" )

    -- Info
    if ( item.cat == ItemCategory.perks or item.cat == ItemCategory.cash ) and item.desc then
        local msg = root:GetControl( "MiddleRoot/PageInfo/Perk" )
        msg:SetText( item.desc )
        msg:SetVisibility( true )
    elseif item.cat == ItemCategory.weapons then
        local weaponInfo = root:GetControl( "MiddleRoot/PageInfo/Weapon" )
        weaponInfo:SetVisibility( true )
        ShopItemFillWeaponStats( root, owned, canUpgrade, item )
    end

    ShopItemUpdateItemData( root, item )
end

function ShopItemUpdateItemData( root, item )
    -- Buy / Upgrade price
    local soft = 0
    local hard = 0
    local owned = item.cat ~= ItemCategory.cash and Shop:IsBought( item.id ) > 0 or false
    local canUpgrade = item.cat ~= ItemCategory.cash and CanUpgradeShopItem( item ) or false

    if owned == true and canUpgrade == true then
        soft, hard = ItemDbGetItemUpgradePrice( tostring(item.id) )
    else
        soft, hard = ItemDbGetItemPrice( tostring(item.id) )
    end

    -- Hard price
    local hardPrice = item.cat == ItemCategory.cash and root:GetControl( "Cash/Hard" ) or root:GetControl( "MiddleRoot/Price/Hard" )
    if hard > 0 then
        hardPrice:SetVisibility( true )
        local prefix = item.cat == ItemCategory.cash and "+" or ""
        hardPrice:GetControl("Value"):SetTextRaw( prefix ..  tostring( hard ) )
    else
        hardPrice:SetVisibility( false )
    end

    -- Soft price
    local softPrice = item.cat == ItemCategory.cash and root:GetControl( "Cash/Soft" ) or root:GetControl( "MiddleRoot/Price/Soft" )
    if soft > 0 then
        softPrice:SetVisibility( true )
        local prefix = item.cat == ItemCategory.cash and "+" or ""
        softPrice:GetControl("Value"):SetTextRaw( prefix .. tostring( soft ) )

        if hard == 0 then
            softPrice:SetY( hardPrice:GetY() )
        end
    else
        softPrice:SetVisibility( false )
    end
    
    -- Free Cash
    if item.cat == ItemCategory.cash then
        local noReward = hard == 0 and soft == 0
        local isFree = item.usd_price == nil
        root:GetControl( "Cash" ):SetVisibility( not noReward )
        root:GetControl( "MiddleRoot/Price" ):SetVisibility( true )
        root:GetControl( "MiddleRoot/Price/Bar" ):SetVisibility( false )
        root:GetControl( "MiddleRoot/Price/Hard" ):SetVisibility( false )
        root:GetControl( "MiddleRoot/Price/Soft" ):SetVisibility( false )
        root:GetControl( "MiddleRoot/Price/FreeBar" ):SetVisibility( isFree )
        root:GetControl( "Buttons/Buy/Label" ):SetText( isFree and "TEXT_FREE_GOLD" or "TEXT_BUY" )
    end 

    -- Stock
    ShopItemUpdateStock( root, item )

    -- Stats
    if item.cat == ItemCategory.weapons then
        ShopItemFillWeaponStats( root, owned, canUpgrade, item )
    end
end

function ShopItemFillWeaponStats( root, owned, canUpgrade, item )
    local currentLvlIdx = Shop:GetUpgrades( item.id ) + 1
    local dmgFrom, dmgTo = 0, 0
    local fireFrom, fireTo = 0, 0
    local clipFrom, clipTo = 0, 0
    local elementsOwned = {}
    local elementsAvailable = {}

    if item.upgrades and item.upgrades.Elements then
        local maxIdx = math.min( #item.upgrades.Elements, currentLvlIdx )
        for i=1,maxIdx do
            if item.upgrades.Elements[i] > 0 then
                table.insert( elementsOwned, item.upgrades.Elements[i] )
            end
        end
        if owned == true and canUpgrade == true and currentLvlIdx < #item.upgrades.Elements then
            local element = item.upgrades.Elements[currentLvlIdx+1]
            if element > 0 then
                table.insert( elementsAvailable, element )
            end
        end
    end

    if owned == false or canUpgrade == false then
        if item.dmg then
            dmgFrom = item.dmg[currentLvlIdx]
        end
        if item.rate then
            fireFrom = item.rate[currentLvlIdx]
        end
        if item.clip then
            clipFrom = item.clip[currentLvlIdx]
        end
    else
        if item.dmg then
            dmgFrom = item.dmg[currentLvlIdx]
            dmgTo = item.dmg[currentLvlIdx + 1]
        end
        if item.rate then
            fireFrom = item.rate[currentLvlIdx]
            fireTo = item.rate[currentLvlIdx + 1]
        end
        if item.clip then
            clipFrom = item.clip[currentLvlIdx]
            clipTo = item.clip[currentLvlIdx + 1]
        end
    end

    ShopItemMarkStars( root:GetControl( "MiddleRoot/PageInfo/Weapon/Damage/Stars" ), dmgFrom, dmgTo, item.dmg[#item.dmg] )
    ShopItemMarkStars( root:GetControl( "MiddleRoot/PageInfo/Weapon/FireRate/Stars" ), fireFrom, fireTo, item.rate[#item.rate] )
    ShopItemMarkStars( root:GetControl( "MiddleRoot/PageInfo/Weapon/ClipSize/Stars" ), clipFrom, clipTo, item.clip[#item.clip] )

    ShopItemMarkElements( item, root:GetControl( "MiddleRoot/PageMain/Elements" ), elementsOwned, {} )
    ShopItemMarkElements( item, root:GetControl( "MiddleRoot/PageInfo/Weapon/Elements/Icons" ), elementsOwned, elementsAvailable )
end

function ShopItemUpdateStock( root, item )
    stock = root:GetControl( "Stock" )
    if stock:GetVisibility() then
        stock:SetTextRaw( tostring(Shop:IsBought(item.id) .. "/" .. item.max) )
    end
end

function UpdateShopItemLayout( root, mode )
    local price = root:GetControl( "MiddleRoot/Price" )

    if not price:GetVisibility() then
        root:GetControl( "MiddleRoot/PageMain/Elements" ):SetY( price:GetY() - R(2) )

        local gradient = root:GetControl( "MiddleRoot/BG/Gradient" )
        local gradientH = select(4, gradient:GetGraphic( "s" ):GetClipRect())
        local gradientY = gradient:GetY()

        local icon = root:GetControl( "MiddleRoot/PageMain/Icon" )
        local msgW, msgH = callback:GetControlSize( root:GetPath() .. "/MiddleRoot/PageInfo/Perk" )
        local pageInfo = root:GetControl( "MiddleRoot/PageInfo/Perk" )
        pageInfo:SetY( icon:GetY() - msgH/2 )
    else
        local gradient = root:GetControl( "MiddleRoot/BG/Gradient" )
        local gradientW = select(3, gradient:GetGraphic( "s" ):GetClipRect())

        -- Pricing layout
        local price = root:GetControl( "MiddleRoot/Price" )
        local softPrice = price:GetControl( "Soft" )
        local hardPrice = price:GetControl( "Hard" )
        local softW = softPrice:GetControl("Value"):GetX() + softPrice:GetControl("Value"):GetTextWidth()
        local hardW = hardPrice:GetControl("Value"):GetX() + hardPrice:GetControl("Value"):GetTextWidth()

        if softPrice:GetVisibility() and hardPrice:GetVisibility() then
            softPrice:SetX( gradientW * Constants.priceMargins )
            hardPrice:SetX( gradientW * (1-Constants.priceMargins) - hardW )
        elseif softPrice:GetVisibility() then
            softPrice:SetX( (gradientW - softW)/2 )
        elseif hardPrice:GetVisibility() then
            hardPrice:SetX( (gradientW - hardW)/2 )
        end
    end

    local cash = root:GetControl( "Cash" )
    if cash:GetVisibility() then
        local count = 0
        local hard = cash:GetControl( "Hard" )
        if hard:GetVisibility() then
            local bar = hard:GetControl( "Bar" )
            local value = hard:GetControl( "Value" )
            local offset = ( bar:GetWidth() - value:GetX() - value:GetTextWidth() )/2
            value:SetX( value:GetX() + offset )

            local icon = hard:GetControl( "Icon" )
            icon:SetX( icon:GetX() + offset )

            count = count + 1
        end

        local soft = cash:GetControl( "Soft" )
        if soft:GetVisibility() then
            local bar = soft:GetControl( "Bar" )
            local value = soft:GetControl( "Value" )
            local offset = ( bar:GetWidth() - value:GetX() - value:GetTextWidth() )/2
            value:SetX( value:GetX() + offset )

            local icon = soft:GetControl( "Icon" )
            icon:SetX( icon:GetX() + offset )

            count = count + 1
        end

        if count > 0 then
            local icon = root:GetControl( "MiddleRoot/PageMain/Icon" )
            icon:SetY( icon:GetY() + count*R(10) )
        end
    end

    -- Small buttons
    if mode == ShopItemMode.select then
        local buttons = root:GetControl( "Buttons" )
        local upgrade = buttons:GetControl( "UpgradeSmall" )
        if upgrade:GetVisibility() then
            local select = buttons:GetControl( "SelectSmall" )
            local confirm = buttons:GetControl( "ConfirmSmall" )
            slectW, selectH = select:GetGraphic( "TS" ):GetSize()

            local fitOffset = -selectH / 4
            select:SetY( fitOffset )
            upgrade:SetY( selectH + fitOffset - R(1) )
            confirm:SetY( selectH + fitOffset - R(1) )
        end
    end
end

function CalculateShopItemLayout( root, width, height, category, mode, item )
    local middleW = width * Constants.midleWidthRatio
    local middleH = height * Constants.midleHeightRatio
    local middleX = (width - middleW) / 2
    local gradientH = select( 4, root:GetControl( "MiddleRoot/BG/Gradient" ):GetGraphic( "s" ):GetClipRect() )

    -- Midle section position
    local middle = root:GetControl( "MiddleRoot" )
    middle:SetX( middleX )
    middle:SetY( middleH - gradeintH  ) 

    local priceBarBottomLine = gradeintH
    local priceBarOffsetRatio = 1.5
    local iconOffsetRatio = 0.85
    -- Bonus
    local bonus = middle:GetControl( "Bonus" )
    if bonus:GetVisibility() then
        local w, h = callback:GetControlSize( bonus:GetPath() )
        bonus:SetX( (middleW-w)/2 )
        bonus:SetY( gradeintH - h * 0.85 )
        priceBarBottomLine = bonus:GetY()
        priceBarOffsetRatio = 0.6
        iconOffsetRatio = 1
    end

    -- FreeBar
    local freeBar = middle:GetControl( "Price/FreeBar" )
    if freeBar:GetVisibility() then
        freeBar:SetX( (middleW - freeBar:GetGraphic( "TS" ):GetSize()) / 2 )
        iconOffsetRatio = 1.0
    else  
        -- Black bar
        local bar = middle:GetControl( "Price/Bar" )
        local barW, barH = bar:GetGraphic( "TS" ):GetSize()
        local barTargetSize = middleW * Constants.barWidthRatio
        local barScale = barTargetSize / barW

        local cash = root:GetControl( "Cash" )
        if cash:GetVisibility() then
            local hard = cash:GetControl( "Hard" )
            local bar1 = hard:GetControl( "Bar" )
            bar1:GetGraphic( "TS" ):SetScale( barScale, 1 )
            hard:SetX( middleX + (middleW - barTargetSize)/2 )

            local soft = cash:GetControl( "Soft" )
            local bar2 = soft:GetControl( "Bar" )
            bar2:GetGraphic( "TS" ):SetScale( barScale, 1 )
            soft:SetX( middleX + (middleW - barTargetSize)/2 )
        else
            bar:GetGraphic( "TS" ):SetScale( barScale, 1 )
            bar:SetX( (middleW - barTargetSize)/2 )
            bar:GetControl(".."):SetY( priceBarBottomLine - barH * priceBarOffsetRatio )
        end
    end
    
    -- Icon position
    local icon = root:GetControl( "MiddleRoot/PageMain/Icon" )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    if category == ItemCategory.weapons then
        icon:GetGraphic( "TS" ):SetAngle( Constants.weaponAngle )
    end
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetX( middleW/2 )
    icon:SetY( -(middleH-gradientH) * iconOffsetRatio + middleH/2 )

    -- Info button
    local labelRightMargin = width
    local info = root:GetControl( "InfoButton" )
    local infoBottom = 0
    if info:GetVisibility() then
        local w, h = info:GetGraphic( "s" ):GetSize()
        info:SetX( width - w + info:GetX() )
        labelRightMargin = info:GetX()
        infoBottom = info:GetY() + h
    else
        -- Stock
        local stock = root:GetControl( "Stock" )
        if stock:GetVisibility() then
            local w = stock:GetWidth()
            stock:SetX( width - w + stock:GetX() )
            labelRightMargin = stock:GetX() + w - stock:GetTextWidth()
        end
    end

    -- Label alignemt
    local label = root:GetControl( "Label" )
    if category == ItemCategory.cash and item.desc == nil then
        label:SetX( (width - label:GetTextWidth()) / 2 )
    else
        label:SetTextWidth( labelRightMargin - label:GetX() )
    end

    -- Info page 
    if info:GetVisibility() then
        if category == ItemCategory.weapons then
            local pageInfoW, pageInfoH = callback:GetControlSize( root:GetPath() .. "/MiddleRoot/PageInfo/Weapon" )
            local pageInfo = root:GetControl( "MiddleRoot/PageInfo/Weapon" )
            local price = root:GetControl( "MiddleRoot/Price" )
            pageInfo:SetX( (middleW - pageInfoW * (GAME_SCALE == 1 and 0.95 or GAME_SCALE == 4 and 0.87 or 0.9))/2 )
            pageInfo:SetY( icon:GetY() - pageInfoH/2 )
            
            local elIcons = root:GetControl( "MiddleRoot/PageInfo/Weapon/Elements/Icons" )
            local elWidht = ShopItemGetNumElements( root ) * Constants.elementsSpacing
            elIcons:SetX( Constants.elementsInfoCenter - elWidht/2 )
        elseif category == ItemCategory.perks or category == ItemCategory.cash then
            local msgW, msgH = callback:GetControlSize( root:GetPath() .. "/MiddleRoot/PageInfo/Perk" )
            local pageInfo = root:GetControl( "MiddleRoot/PageInfo/Perk" )
            pageInfo:SetX( (middleW - msgW)/2 )
            pageInfo:SetY( icon:GetY() - msgH/2 )
        end
    end

    -- Main Buttons
    local buttons = root:GetControl( "Buttons" )
    w, h = callback:GetControlSize( root:GetPath() .. "/Buttons" )
    buttons:SetX( (width - w) / 2 + Constants.buttonShadowWidth )
    buttons:SetY( middleH + (height - middleH - h)/ 2 )

    -- Buttons No Level
    local buttonsNoLvl = root:GetControl( "ButtonsNoLvl" )
    w, h = buttonsNoLvl:GetControl( "Buy" ):GetGraphic( "s" ):GetSize()
    buttonsNoLvl:SetX( (width - w) / 2 + Constants.buttonShadowWidth )
    if buttonsNoLvl:GetControl( "Buy" ):GetVisibility() then
        h = h + buttonsNoLvl:GetControl( "Buy" ):GetY() * Constants.noLvlButtonsOffsetRatio
    end
    buttonsNoLvl:SetY( middleH + (height - middleH - h)/ 2 )

    UpdateShopItemLayout( root, mode )
end

function ShopItemSwitchPages( root )
    if not ShopItemHasInfo(root) then return end
    
    local pageMain = root:GetControl( "MiddleRoot/PageMain" )
    local pageInfo = root:GetControl( "MiddleRoot/PageInfo" )
    local mainVisible = pageMain:GetVisibility() 

    pageMain:SetVisibility( not mainVisible )
    pageInfo:SetVisibility( mainVisible )
end

function ShopItemMarkStars( starsRoot, activeNum, potentialNum, availableNum )
    local rootPath = starsRoot:GetPath()
    anim:RemoveAll( rootPath )
    for idx, star in ipairs( starsRoot:GetChildren() ) do
        star:SetVisibility( idx <= availableNum )         
        if idx <= (activeNum or 0) then
            star:SetRepresentation( "active" )
            star:SetAlpha( 255 )
        elseif idx <= (potentialNum or 0) then
            star:SetRepresentation( "active" )
            star:SetAlpha( Constants.potentialAlha )

            anim:Add( Perpetual:New(
                function( t )
                    local sin = math.sin(t * 8)
                    star:SetAlpha( 80 +  50* (1 + sin) )
                    star:GetGraphic("TS"):SetScale( 0.9 + 0.05 * (1 + sin) )
                end
                ),
                nil,
                rootPath
            )
        else
            star:SetRepresentation( "default" )
            star:SetAlpha( 255 )
        end
    end
end

function ShopItemMarkElements( item, elementsRoot, elementsActive, elementsPotential )
    local activeSet = {}
    for _, l in ipairs(elementsActive) do activeSet[l] = true end

    local potentialSet = {}
    for _, l in ipairs(elementsPotential) do potentialSet[l] = true end

    local elementX = 0
    
    local rootPath = elementsRoot:GetPath()
    anim:RemoveAll( rootPath )
    for idx, element in ipairs( elementsRoot:GetChildren() ) do
        local elelmentName = element:GetName()
        local elelmentId = ElementMap[elelmentName]
        local hasElement = ShopItemHasElementUpgrade( item, elelmentId )

        if hasElement then
            element:SetVisibility( true )

            -- Center pivots
            local graphic = element:GetGraphic( "TS" )
            local pivX, pivY = graphic:GetPivot()
            if pivX == 0 and pivY == 0 then
                local w, h = graphic:GetSize()
                
                element:SetRepresentation( "active" )
                element:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
                element:SetRepresentation( "default" )
                element:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
                
                element:SetX( elementX + w/2 )
                element:SetY( element:GetY() + h/2 )
                
                elementX = elementX + Constants.elementsSpacing
            end
            
            if activeSet[elelmentId] ~= nil then
                element:SetRepresentation( "active" )
                element:SetAlpha( 255 )
            elseif potentialSet[elelmentId] ~= nil then
                element:SetRepresentation( "active" )
                element:SetAlpha( Constants.potentialAlha )

                anim:Add( Perpetual:New(
                    function( t )
                        local sin = math.sin(t * 8)
                        element:SetAlpha( 80 +  50* (1 + sin) )
                        element:GetGraphic("TS"):SetScale( 0.9 + 0.05 * (1 + sin) )
                    end
                    ),
                    nil,
                    rootPath
                )
            else
                element:SetRepresentation( "default" )
                element:SetAlpha( 255 )
            end
        else
            element:SetVisibility( false )
        end
    end
end

function ShopItemPress( c )
    local path = c:GetPath()
    if string.find( path, "/ShopItem/InfoButton" )
        or string.find( path, "/ShopItem/ButtonsNoLvl/" )
        or string.find( path, "/ShopItem/Buttons/" ) then
        ButtonPress( c )
    end
end

function ShopItemAction( c, sender )
    local path = c:GetPath()

    if string.find( path, "/ShopItem/InfoButton" ) then
        if string.find( path, "sub.release" ) then
            ShopInfoPopupShow()
        else
            ShopItemSwitchPages( ShopItemGetRoot( c ) )
        end
    elseif string.find( path, "/ShopItem/Buttons/Buy" ) then
        TutorialAction( path )
        local root = ShopItemGetRoot( c )
        ShopItemClearConfirmations( root:GetControl( "../.." ) )
        if ShopItemBuyTry( root ) then
            ShopItemShowConfirm( root, true, false, true )
        else
            ShopItemBuy( root, false, sender )
        end
    elseif string.find( path, "/ShopItem/Buttons/Upgrade" ) then
        TutorialAction( path )
        local root = ShopItemGetRoot( c )
        ShopItemClearConfirmations( root:GetControl( "../.." ) )
        if ShopItemUpgradeTry( root ) then
            ShopItemShowConfirm( root, true, false, true )
        else
            ShopItemUpgrade( root, false, sender )
        end
    elseif string.find( path, "/ShopItem/Buttons/Confirm" ) then
        TutorialAction( path )
        if c:GetControl( "../Upgrade" ):GetVisibility() or c:GetControl( "../UpgradeSmall" ):GetVisibility() then
            ShopItemUpgrade( ShopItemGetRoot( c ), true, sender )
        else
            ShopItemBuy( ShopItemGetRoot( c ), true, sender )
        end
    elseif string.find( path, "/ShopItem/ButtonsNoLvl/Buy" ) then
        TutorialAction( path )
        local root = ShopItemGetRoot( c )
        ShopItemClearConfirmations( root:GetControl( "../.." ) )
        if ShopItemUnlockTry( root ) then
            ShopItemShowConfirm( root, true, true, true )
        else
            ShopItemUnlock( root, false, sender )
        end
    elseif string.find( path, "/ShopItem/ButtonsNoLvl/Confirm" ) then
        TutorialAction( path )
        ShopItemUnlock( ShopItemGetRoot( c ), true, sender )
    elseif string.find( path, "/ShopItem/Buttons/Select" ) then
        TutorialAction( path )
        if sender and sender.OnItemSelected then
            local root = ShopItemGetRoot( c )
            local item = ShopItemGetDbItem( root )
            sender:OnItemSelected( item.id )
        end
    end
end

function ShopItemGetDbItem( root )
    local itemId = root:GetControl( ".." ):GetName()
    if itemId then
        return ItemsById[itemId]
    end
    return nil
end

function ShopItemUnlockTry( root )
    local item = ShopItemGetDbItem( root )
    if item then
        return ShopItemCanAffordUnlock( item )
    end
    return false
end

function ShopItemUnlock( root, canAfford, sender )
    local item = ShopItemGetDbItem( root )
    if item then
        if canAfford then
            Shop:UnlockBuy( item.id, true )
            local canUpgrade = CanUpgradeShopItem( item )
            ShopItemShowConfirm( root, false, false, true )

            if canUpgrade then
                ShopItemUpdateItemData( root, item )
            end

            if sender and sender.OnShopItemBought then
                sender:OnShopItemBought( tostring(item.id) )
            end
            if item.atlas then
                Shop:Preload( item.atlas )
            end
        else
            if sender and sender.OnShopItemCantAfford then
                sender:OnShopItemCantAfford( tostring(item.id), item.cat )
            end
        end
    end
end
function ShopItemBuyTry( root )
    local item = ShopItemGetDbItem( root )
    if item and item.cat ~= ItemCategory.cash then
        return ShopItemCanAfford( item )
    end
    return false
end

function ShopItemBuy( root, canAfford, sender )
    local item = ShopItemGetDbItem( root )
    if not item then return end
    
    if item.cat ~= ItemCategory.cash then
        if canAfford then
            ShopItemShopBuy( item )
            local canUpgrade = CanUpgradeShopItem( item )
            ShopItemShowConfirm( root, false, false, true )

            ShopItemUpdateStock( root, item )
            if canUpgrade then
                ShopItemUpdateItemData( root, item )
            end

            if sender and sender.OnShopItemBought then
                sender:OnShopItemBought( tostring(item.id) )
            end
        else
            if sender and sender.OnShopItemCantAfford then
                sender:OnShopItemCantAfford( tostring(item.id), item.cat )
            end  
        end
    else
        if sender and sender.OnBuyVirtualCashTry then
            sender:OnBuyVirtualCashTry( item.id )
        end
        if item.subscription then
            Shop:BuySubscription( item.id )
        else
            Shop:BuyVirtualCash( item.id )
        end
    end
end

function ShopItemShopBuy( item )
    if item.id2 ~= nil then
        for _, id in ipairs( item.id2 ) do
            Shop:Buy( id, false )
        end
    end
    Shop:Buy( item.id, true )
    if item.atlas then
        Shop:Preload( item.atlas )
    end
end


function ShopItemUpgradeTry( root )
    local item = ShopItemGetDbItem( root )
    if item then
        return ShopItemCanAffordUpgrade( item )
    end
    return false
end

function ShopItemUpgrade( root, canAfford, sender )
    local item = ShopItemGetDbItem( root )
    if not item then return end
    
    if item.cat ~= ItemCategory.cash then
        if canAfford then
            ShopItemShopUpgrade( item )
            local canUpgrade = CanUpgradeShopItem( item )
            ShopItemShowConfirm( root, false, false, true )
            ShopItemUpdateItemData( root, item )

            if sender and sender.OnShopItemUpgrade then
                sender:OnShopItemUpgrade( tostring(item.id) )
            end
        else
            if sender and sender.OnShopItemCantAfford then
                sender:OnShopItemCantAfford( tostring(item.id) )
            end
        end
    end
end

function ShopItemShopUpgrade( item )
    if item.id2 ~= nil then
        for _, id in ipairs( item.id2 ) do
            Shop:Upgrade( id, false )
        end
    end
    Shop:Upgrade( item.id, true )

    if not CanUpgradeShopItem( item ) then
        AudioManager:Play( Sfx.SFX_VO_MAXED_OUT )
    end
end

function ShopItemClearConfirmations( containerRoot )
    for _, item in ipairs( containerRoot:GetChildren() ) do
        ShopItemShowConfirm( item:GetChildren()[1], false, false, false )
    end
end

function ShopItemHasInfo( root )
    return root:GetControl( "InfoButton" ):GetVisibility()
end

function ShopItemShowConfirm( root, show, unlock, changePages )
    local modePostifx = root:GetControl( "Buttons/UpgradeSmall" ):GetVisibility() and "Small" or ""
    if show then
        if unlock and unlock == true then
            root:GetControl( "ButtonsNoLvl/Confirm" ):SetVisibility( true )
        else
            root:GetControl( "Buttons/Confirm" .. modePostifx ):SetVisibility( true )
        end
        if changePages and root:GetControl( "MiddleRoot/PageMain" ):GetVisibility() then
            ShopItemSwitchPages( root )
        end
    else
        root:GetControl( "Buttons/Confirm" .. modePostifx ):SetVisibility( false )
        root:GetControl( "ButtonsNoLvl/Confirm" ):SetVisibility( false )
        if changePages and root:GetControl( "MiddleRoot/PageInfo" ):GetVisibility() then
            ShopItemSwitchPages( root )
        end
    end
end

function ShopItemGetRoot( c )
    local root = c
    while root and root:GetName() ~= "ShopItem" do
        root = root:GetControl( ".." )
    end
    return root
end

function ShopItemHasElementUpgrade( item, elementToCheck )
    for _, element in ipairs( item.upgrades.Elements ) do
        if element == elementToCheck then
            return true
        end
    end
    return false   
end

function ShopItemGetNumElements( root )
    local item = ShopItemGetDbItem( root )
    local num = 0
    for _, element in ipairs( item.upgrades.Elements ) do
        if element ~= 0 then
            num = num + 1
        end
    end
    return num   
end



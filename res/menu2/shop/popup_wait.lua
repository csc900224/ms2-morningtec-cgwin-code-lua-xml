require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 140 )

function ShopWaitPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_wait.xml" )
    popup = screen:GetControl( "/ShopWaitPopup" )
    popup:SetVisibility( false )
    
    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
end

function ShopWaitPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        return true
    end

    return false
end

function ShopWaitPopupShow()
    if active then
        return
    end
    active = true
    local showTime = IS_ANDROID and 0.05 or 0.3
    PopupLock( showTime )
    
    popup:SetVisibility( true )
    if IS_ANDROID then
        popup:SetY( y )
    else
        anim:Add( Slider:New( showTime,
            function( t )
                popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
            end
            )
        )
    end
end

function ShopWaitPopupHide( unlock )
    active = false
    if unlock then
        PopupUnlock()
    end

    local startY = popup:GetY()
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( startY + SmoothStep( t ) * ( SCREEN_HEIGHT - startY ) )
        end,
        function()
            popup:SetVisibility( false )
        end
        )
    )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/shop/popup_share_success.lua" )
require( "menu2/shop/share_common.lua" )
require( "menu2/star_animation.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 190 )

local itemBought = nil

function ShopSharePopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_share.xml" )
    popup = screen:GetControl( "/ShopSharePopup" )
    popup:SetVisibility( false )
    
    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    -- Hide Tweeter button and change buttons alignement   
    local canTweet = callback.CanSendTweet ~= nil and callback:CanSendTweet()
    popup:GetControl( "Twitter" ):SetVisibility( canTweet )
    if not canTweet then
        local back = popup:GetControl( "Back" )
        local tweet = popup:GetControl( "Twitter" )
        local fb = popup:GetControl( "Facebook" )
        
        local fX = fb:GetX()
        local tX = tweet:GetX()
        local offsetX = (tX + fX) / 2 - fX
        
        fb:SetX( fX + offsetX )
        back:SetX( back:GetX() + offsetX )
    end
    
    ShopShareSuccessPopupInit( screen )
end

function ShopSharePopupShow( item, lock, reset )
    if active then
        return
    end
    active = true
    FillShareRewardText( popup:GetControl( "Reward" ) )
    
    if item ~= nil then
        itemBought = item
    end
    
    if lock == nil or lock == true then
        PopupLock()
    end
    
    if reset and reset == true then
        ShopSharePopupResetShareButton( "Twitter" )
        ShopSharePopupResetShareButton( "Facebook" )
    end
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    StartStarAnimation( popup:GetControl( "Star" ) )
end

function ShopSharePopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopSharePopupHide()
        return true
    end

    return false
end

function ShopSharePopupPress( control, path )
    if not active then
        ShopShareSuccessPopupPress( control, path )
        return
    end

    if path == "/ShopSharePopup/Twitter/Button" 
        or path == "/ShopSharePopup/Facebook/Button"
        or path == "/ShopSharePopup/Back/Button" then
        ButtonPress( control, path )
    end
end

function ShopSharePopupAction( name )
    if not active then
        ShopShareSuccessPopupAction( name )
        return
    end

    if name == "/ShopSharePopup/Back/Button" then
        ShopSharePopupHide()
    elseif name == "/ShopSharePopup/Twitter/Button" then
        PopupLock( nil, "/ShopSharePopup/Overlay" )
        callback:SendTweet( itemBought.d, itemBought.cat )
    elseif name == "/ShopSharePopup/Facebook/Button" then
        PopupLock( nil, "/ShopSharePopup/Overlay" )
        callback:PublishOnFacebook( itemBought.id, itemBought.d, itemBought.cat )
    end
end

function ShopShareFacebookSuccess()
    ShopSharePopupShareSuccess( "Facebook" )
    GiveShopShareAward()
    anim:Add( Delay:New( 0.01 ),
        function() 
            PopupUnlock( nil, "/ShopSharePopup/Overlay" )
        end
    )
end

function ShopShareTwitterSuccess()
    ShopSharePopupShareSuccess( "Twitter" )
    GiveShopShareAward()
    anim:Add( Delay:New( 0.01 ),
        function() 
            PopupUnlock( nil, "/ShopSharePopup/Overlay" )
        end
    )
end

function ShopShareTwitterFail()
    PopupUnlock( nil, "/ShopSharePopup/Overlay" )
end

function ShopShareFacebookFail()
    PopupUnlock( nil, "/ShopSharePopup/Overlay" )
end

function ShopSharePopupShareSuccess( buttonName )
    local button = popup:GetControl( buttonName )
    local bg = button:GetControl( "Button" )
    local icon = button:GetControl( "Icon" )
    anim:RemoveAll( bg:GetPath() )
    bg:SetRepresentation( "pressed" )
    bg:SetTouchable( false )
    icon:SetAlpha( 125 )
    ShopSharePopupHide( true )
end

function ShopSharePopupResetShareButton( buttonName )
    local button = popup:GetControl( buttonName )
    local bg = button:GetControl( "Button" )
    local icon = button:GetControl( "Icon" )
    
    bg:SetRepresentation( "default" )
    bg:SetTouchable( true )
    icon:SetAlpha( 255 )
end

function ShopSharePopupHide( share )
    active = false
    
    if not share then
        PopupUnlock()
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            StopStarAnimation( popup:GetControl( "Star" ) )
            if share then
                ShopShareSuccessPopupShow()
            end
        end
    )
end

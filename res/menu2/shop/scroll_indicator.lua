require( "menu2/animation.lua" )
require( "menu2/common.lua" )

ScrollIndicator = {
    segmentsNum = 0,
    root = nil,
    itemW = 0,
    x = 0,
    spacing = R(-7),
    lastOffset = -1,
    maxOffset = -1,
    activeSegment = 0
}

function ScrollIndicator:new (o)
    o = o or {}   -- create object if user does not provide one
    setmetatable(o, self)
    self.__index = self
    return o
end

function ScrollIndicator:Init( parent, maxScrollOffset, itemWidth )
    self.itemW = itemWidth

    self.root = parent:GetControl( "ScrollIndicator" )
    if self.root == nil then
        self.root = parent:InsertControl( "ScrollIndicator" )
        self.root:SetRelative( true )
    end
    self:UpdateMaxScrollOffset( maxScrollOffset )
    self:UpdateScrollOffset( 0 )
end

function ScrollIndicator:UpdateMaxScrollOffset( maxScrollOffset )
    if self.maxOffset ~= maxScrollOffset then
        local newSegmentsNum = math.floor( maxScrollOffset / self.itemW ) + 1
        if maxScrollOffset > 0 and newSegmentsNum == 1 then
            newSegmentsNum = 2
        end
        
        if newSegmentsNum ~= self.segmentsNum then
            self:SetSegments( newSegmentsNum )
        end
        self.maxOffset = maxScrollOffset
    end
end

function ScrollIndicator:UpdateScrollOffset( scrollOffset )
    if self.lastOffset ~= scrollOffset then
        self.lastOffset = scrollOffset
        
        local newActiveSegment = 1
        if self.maxOffset > 0 then
            newActiveSegment = 1 + math.floor((scrollOffset / self.maxOffset) * (self.segmentsNum - 1) + 0.5)
        end
        if newActiveSegment ~= self.activeSegment then
            if self.activeSegment > 0 and self.activeSegment <= self.segmentsNum then
                self.root:GetChildren()[self.activeSegment]:GetChildren()[1]:SetRepresentation( "default" )
            end
            self.root:GetChildren()[newActiveSegment]:GetChildren()[1]:SetRepresentation( "active" )
            self.activeSegment = newActiveSegment
        end
    end
end

function ScrollIndicator:SetSegments( newSegmentsNum )
    self.root:SetVisibility( newSegmentsNum ~= 1 )
    if newSegmentsNum > self.segmentsNum then
        for i=1,newSegmentsNum - self.segmentsNum do
            local segmentRoot = self.root:InsertControl( tostring(self.segmentsNum + i) )
            segmentRoot:SetRelative( true )
            segmentRoot:InsertFromXml( "menu2/shop/scroll_indicator.xml" )
            segmentRoot:SetX( self.x )
            self.x = self.x + segmentRoot:GetChildren()[1]:GetGraphic( "TS" ):GetSize() + self.spacing
        end
    elseif newSegmentsNum < self.segmentsNum then
        for idx, segment in ipairs( self.root:GetChildren() ) do
            if idx > newSegmentsNum then
                self.x = self.x - (segment:GetChildren()[1]:GetGraphic( "TS" ):GetSize() + self.spacing)
                segment:Remove()
            end
        end
    end
    self.segmentsNum = newSegmentsNum
end
require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/avatar.lua" )
require( "menu2/shop/shop_layout.lua" )
require( "menu2/shop/shop_item_list.lua" )
require( "ItemDB.lua" )

local active = false
local shown = false
local root = nil
local currentTab = nil

local backAction = false

local freeFuffButton = nil
local ostButton = nil

ShopTab = {
    Weapons = "Weapons",
    Perks = "Perks",
    Items = "Items",
    Cash = "Cash"
}

local ItemsCatInTab = {}
ItemsCatInTab[ShopTab.Weapons] = ItemCategory.weapons
ItemsCatInTab[ShopTab.Perks] = ItemCategory.perks
ItemsCatInTab[ShopTab.Items] = ItemCategory.items
ItemsCatInTab[ShopTab.Cash] = ItemCategory.cash

local itemList = nil
local specialOffer = nil

function ShopInit( screen )
    screen:InsertFromXml( "menu2/shop/shop.xml" )
    root = screen:GetControl( "ShopRoot" )
    
    freeFuffButton = root:GetControl( "Buttons/FreeStuff" )
    ostButton = root:GetControl( "Buttons/OST" )

    CalculateShopButtonsLayout( root )
    AvatarInit( root:GetControl( "AvatarRoot" ), GetShopAvatarSize( root ) )
    CalculateShopLayout( root )

    itemList = ShopItemList:new()
    local listW, listH, maxItemH = GetShopItemsListSize( root )
    itemList:Init( root:GetControl( "ItemsRoot" ), ShopItemListMode.normal, listW, listH, maxItemH )

    root:SetVisibility( false )
end

function ShopSpecialOffer( item, discount, badge )
    specialOffer = { item = item }
    local level = registry:Get( "/monstaz/player/level" )
    specialOffer.isUpdate = Shop:IsBought( item.id ) > 0 and item.cat == ItemCategory.weapons
    if specialOffer.isUpdate then
        specialOffer.ul = Shop:GetUpgrades( item.id ) + 2
        local s, h = ItemDbGetItemUpgradePrice( item.id )
        specialOffer.s = s
        specialOffer.h = h
        item.cs[specialOffer.ul] = math.floor( s * discount )
        item.ch[specialOffer.ul] = math.floor( h * discount )
    else
        specialOffer.tt = type( item.cs ) == "table"
        if specialOffer.tt then
            specialOffer.isHard = item.lvl > level
            if specialOffer.isHard then
                specialOffer.cu = item.cu
                item.cu = math.floor( item.cu * discount )
            else
                specialOffer.cs = item.cs[1]
                item.cs[1] = math.floor( item.cs[1] * discount )
            end
        else
            specialOffer.cs = item.cs
            specialOffer.ch = item.ch
            item.cs = item.cs and math.floor( item.cs * discount ) or item.cs
            item.ch = item.ch and math.floor( item.ch * discount ) or item.ch
        end
    end
    item.badge = badge
    currentTab = nil
end

local function UndoSpecialOffer()
    if not specialOffer then return end

    local item = specialOffer.item
    if specialOffer.isUpdate then
        item.cs[specialOffer.ul] = specialOffer.s
        item.ch[specialOffer.ul] = specialOffer.h
    else
        if specialOffer.tt then
            if specialOffer.isHard then
                item.cu = specialOffer.cu
            else
                item.cs[1] = specialOffer.cs
            end
        else
            item.cs = specialOffer.cs
            item.ch = specialOffer.ch
        end
    end

    item.badge = nil

    specialOffer = nil
    currentTab = nil
end

function ShopShow( tab, instant, backCallback, focus )
    if not tab then
        tab = ShopTab.Weapons
    end

    if active then 
        ShopSelectTab( tab, true )
        return 
    end

    Lock()

    backAction = backCallback
    callback:Playhaven( "shop" )

    active = true
    Shop:OnEnter()
    AvatarRefresh()
    ShopRefreshFreeStuffButton()

    if OnShopShow then
        OnShopShow( instant )
    end

    root:SetAlpha( 0 )
    root:SetVisibility( true )

    ShopSelectTab( tab )
    itemList:ResetScroll()

    if focus then
        itemList:FocusOn( focus )
    end
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SHOP_STATE_CHANGED, GameEventParam.GEP_SHOP_OPEN )

    if TutorialActive then
        TutorialOnShop()
    end
    
    if AvatarSetWeapon then
        local weapon = registry:Get( "/monstaz/weaponselection/1" )
        if weapon == nil then
            weapon = ShopItem.SMG
        end
        local avatarData = ItemsById[weapon].a
        if avatarData then
            AvatarSetWeapon( avatarData )
        end
    end

    if instant == true then
        root:SetAlpha( 255 )
        shown = true
        if OnShopShown then
            OnShopShown()
        end
        Unlock()
    else
        anim:Add( Slider:New( 0.3,
            function( t )
                root:SetAlpha( SmoothStep( t ) * 255 )
            end),
            function()
                shown = true
                if OnShopShown then
                    OnShopShown()
                end
                Unlock()
            end
        )
    end
end

function ShopHide()
    if not active then return end

    Lock()

    UndoSpecialOffer()

    if OnShopHide then
        OnShopHide()
    end

    shown = false
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SHOP_STATE_CHANGED, GameEventParam.GEP_SHOP_CLOSE )
    
    if backAction then
        backAction()
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end),
        function()
            root:SetAlpha( 0 )
            root:SetVisibility( false )

            active = false
            Shop:OnExit()

            if OnShopHidden then
                OnShopHidden()
            end

            Unlock()
        end
    )
end

function ShopUpdate( dt )
    -- Process pending notifiactions even if shop isn't active
    ProcessTransactionNotifications( dt )

    if not active then return end
    itemList:Update( dt )
end

function ShopKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopHide()
        return true
    end

    return false
end

function ShopTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    itemList:TouchDown( x, y, id, c )
    
    local path = c:GetPath()
    if string.find(path, "/ShopRoot/Buttons/") then
        ButtonPress( c )
    end
end

function ShopTouchUp( x, y, id )
    if not active or not shown then return end
    local c = screen:GetTouchableControl( x, y )
    itemList:TouchUp( x, y, id, c )
    if not c then return end

    local path = c:GetPath()
    if string.find(path, "/ShopRoot/Buttons/Back") then
        TutorialAction( path )
        ShopHide()
    elseif string.find(path, "/ShopRoot/Buttons/MoreGames") then
        callback:MoreGames()
    elseif string.find(path, "/ShopRoot/Buttons/FreeStuff") then
        FreeStuffPopupShow( function() ShopRefreshFreeStuffButton() end )
    elseif string.find(path, "/ShopRoot/Buttons/OST") then
        callback:BuyOST()
    else 
        local tab = select( 3, string.find( path, "Tabs/(%a+)" ) )
        if tab and tab ~= currentTab then
            ButtonPressPlaySound()
            TutorialAction( path )
            ShopSelectTab( tab, true )
        end
    end
end

function ShopTouchMove( x, y, id )
    if not active then return end
    itemList:TouchMove( x, y, id )
end

function ShopRefreshFreeStuffButton()
    if not IS_IPHONE then return end
    local freeStuffActive = FreeStuffIsActive()
    freeFuffButton:SetVisibility( freeStuffActive ) 
    ostButton:SetVisibility( not freeStuffActive )
end

function ShopSelectTab( path, animated )
    if currentTab ~= path then
        Lock()
        local tabs = root:GetControl( "Tabs" ):GetChildren()
        for _,tab in ipairs( tabs ) do
            tab:SetRepresentation( TutorialActive and "locked" or "default" )
        end
        tab = root:GetControl( "Tabs/" .. path )
        tab:SetRepresentation("selected")

        callback:Playhaven( "shop_tab_" .. string.lower(path) )

        local itemCat = ItemsCatInTab[path]
        local itemsRoot = root:GetControl( "ItemsRoot" )
        local alphaStart = itemsRoot:GetAlpha()

        if animated then
            anim:RemoveAll( "ItemsRootFade" )
            anim:Add( Slider:New( 0.1,
            function( t )
                itemsRoot:SetAlpha( alphaStart - SmoothStep( t ) * alphaStart )
            end),
            function()
                itemsRoot:SetAlpha( 0 )
                anim:Add( Delay:New( 0.01 ),
                    function() 
                        itemList:SetContent( ItemsCategory[itemCat] )
                        
                        anim:Add( Slider:New( 0.2,
                        function( t )
                            itemsRoot:SetAlpha( SmoothStep( t ) * 255 )
                        end), 
                        function()
                            Unlock()
                        end,
                        "ItemsRootFade")
                    end
                )
            end,
            "ItemsRootFade"
            )
        else
            itemList:SetContent( ItemsCategory[itemCat] )
            Unlock()
        end
        currentTab = path
    end
end

function ShopFindBestIap( priceSoft, priceHard )
    if not IAPS_ENABLED then return nil end

    local playerCashSoft, playerCashHard = Shop:GetCash()
    
    local minSoftDif = nil
    local minHardDif = nil
    local bestIap = nil
    
    local mostExpensiveIap = nil
    local maxSoftCash = nil
    local maxHardCash = nil
    
    local iaps = ItemsCategory[ItemCategory.cash]
    function ByUSD( item1, item2 )
        return (item1.usd_price or 0) < (item2.usd_price or 0)
    end
    table.sort( iaps, ByUSD )

    for _, iap in ipairs( iaps ) do
        if not iap.subscription then
            local iapCashSoft, iapCashHard = ItemDbGetItemPrice( iap.id )
            
            -- Designer request: do not sarch for bundle packs
            if iapCashSoft == 0 or iapCashHard == 0 then
                local softDiff = playerCashSoft + iapCashSoft - priceSoft 
                local hardDiff = playerCashHard + iapCashHard - priceHard
                
                if hardDiff >= 0 and softDiff >= 0 and ((minHardDif == nil or hardDiff < minHardDif) and (minSoftDif == nil or softDiff < minSoftDif)) then
                    minHardDif = hardDiff
                    minSoftDiff = softDiff
                    bestIap = iap.id
                end
            end  
            if mostExpensiveIap == nil or maxSoftCash < iapCashSoft and maxHardCash < iapCashHard then
                mostExpensiveIap = iap.id
                maxSoftCash = iapCashSoft
                maxHardCash = iapCashHard
            end
        end
    end
    
    if bestIap == nil then
        bestIap = mostExpensiveIap
    end
    return bestIap
end

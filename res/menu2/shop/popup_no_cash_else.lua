require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "ItemDB.lua" )

local active = false
local popup = nil
local itemId = nil

local x = 0
local y = 0
local width = R( 300 )
local height = R( 140 )

local parent = nil
local callbackReturn = nil

function ShopNoCashElsePopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/shop/popup_no_cash_else.xml" )
    popup = screen:GetControl( "/ShopNoCashElsePopup" )

    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    if not IAPS_ENABLED then
        popup:GetControl( "Message" ):SetVisibility( false )
        popup:GetControl( "Get" ):SetVisibility( false )
        popup:GetControl( "Buy" ):SetVisibility( false )
        local back = popup:GetControl( "Back" )
        back:SetX( (width - back:GetControl( "Button" ):GetSize())/2 )
    end
    popup:SetVisibility( false )
end


function ShopNoCashElsePopupShow( expensiveItemId, parentList , callback )
    if active then
        return
    end
    active = true
    parent = parentList
    PopupLock()
    itemId = expensiveItemId
    callbackReturn = callback

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function ShopNoCashElsePopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ShopNoCashElsePopupHide(0)
        return true
    end

    return false
end

function ShopNoCashElsePopupPress( control, path )
    if not active then
        return
    end

    if path == "/ShopNoCashElsePopup/Back/Button"
        or path == "/ShopNoCashElsePopup/Get/Button"
        or path == "/ShopNoCashElsePopup/Buy/Button" then
        ButtonPress( control )
    end
end

function ShopNoCashElsePopupAction( name )
    if not active then
        return
    end

    if name == "/ShopNoCashElsePopup/Get/Button" then
        ShopNoCashElsePopupHide(1)
    elseif name == "/ShopNoCashElsePopup/Buy/Button" then
        ShopNoCashElsePopupHide(2)
    elseif name == "/ShopNoCashElsePopup/Back/Button"  then
        ShopNoCashElsePopupHide(0)
    end
end

function ShopNoCashElsePopupHide( getCash )
    active = false

    if callbackReturn ~= nil then 
        if getCash == 1 then
            PopupUnlock()
        end
    else
        PopupUnlock()
    end
    
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_NO_CASH_ACTION, getCash and GameEventParam.GEP_NO_CASH_GET or GameEventParam.GEP_NO_CASH_BACK )

    if getCash == 1 then
        if ShopActive and ShopShow then
            ShopShow( ShopTab.Cash )
        elseif PremissionActive and CountersAction then
            CountersShopShow()
        elseif itemId then
            ShopNoCashElsePopupBuyIapForItem()
        end
    elseif getCash == 2 then
        -------------------------------
        -- Implement try-it-now here --
        -------------------------------
        -- MorningTec Change Begin
        if parent and parent.OnBuyWeaponTry then
            parent:OnBuyWeaponTry( itemId )
        end
        Shop:BuyItDirect( itemId , true )
        -- MorningTec change End

    else
        if callbackReturn ~= nil then
            callbackReturn()
            callbackReturn = nil
        end
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end,
        function()
            popup:SetVisibility( false )
        end
        )
    )
end

function ShopNoCashElsePopupBuyIapForItem()
    local priceSoft, priceHard = 0
    if ItemDbCanUpgrade(itemId) and Shop:IsBought(itemId) > 0 then
        priceSoft, priceHard = ItemDbGetItemUpgradePrice( itemId )
    else
        priceSoft, priceHard = ItemDbGetItemPrice( itemId )
    end
    
    local bestIap = ShopFindBestIap( priceSoft, priceHard )
    
    if parent and parent.OnBuyVirtualCashTry then
        parent:OnBuyVirtualCashTry( bestIap )
    end
    Shop:BuyVirtualCash( bestIap )
end
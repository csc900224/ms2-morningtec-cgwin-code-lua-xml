require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil
local scroll = nil

local x = 0
local y = 0
local width = SCREEN_WIDTH
local height = SCREEN_HEIGHT - R(50)

local musicBar = {}
local soundBar = {}

local vpad = nil
local autoaim = nil

local currentTab = 1

local tabs = { h = {}, p = {} }

local ingame = false
local versionText = nil

local AboutText
local AboutImg
local AboutStart
local AboutRestart
local AboutDing
local AboutDingEnable
local AboutError
local AboutJanusz
local AboutPingwin
local AboutMadej
local AboutMap = {}
do
    local off = 0
    AboutText = function( txt, f )
        scroll:InsertFromXml( "menu2/about_text.xml" )
        local item = scroll:GetControl( "AboutText" )
        item:SetName( "About" .. off )
        item:SetY( R( off ) )
        off = off + 15
        item:SetText( txt )
        if f then
            item:SetTouchable( true )
            AboutMap[item:GetPath()] = { f = f, i = item }
        end
        return item
    end
    AboutImg = function( img )
        scroll:InsertFromXml( "menu2/about_img.xml" )
        local item = scroll:GetControl( "AboutImg" )
        item:SetName( "About" .. off )
        item:GetGraphic( "s" ):SetImage( img )
        item:SetX( ( SCREEN_WIDTH - item:GetWidth() ) / 2 )
        item:SetY( R( off ) )
        off = off + Rinv( item:GetHeight() ) + 10
    end
    local pos
    local clip
    AboutStart = function()
        local lt = 0
        clip = Rinv( scroll:GetParent():GetHeight() )
        pos = clip
        anim:Add( Perpetual:New(
            function( t )
                local diff = t - lt
                lt = t
                pos = pos - diff * 40
                if pos < -off then
                    pos = clip
                end
                scroll:SetY( R( pos ) )
            end
            ) )
    end
    local cnt = 0
    local enabled = false
    AboutRestart = function()
        pos = clip
        cnt = 0
        enabled = false
    end
    AboutDing = function()
        if enabled then
            cnt = cnt + 1
            if cnt == 5 then
                AudioManager:Play( Sfx.SFX_MENU_DING )
            end
        end
    end
    AboutDingEnable = function()
        enabled = true
    end
    local cnt = 0
    AboutError = function( item )
        if enabled then
            cnt = cnt + 1
            if cnt == 5 then
                item:SetText( "\074\069\082\122\121\032\066\076\065\068\065\082\079\119\105\067\122" )
            end
        end
    end
    local cnt = 0
    AboutJanusz = function( item )
        if enabled then
            cnt = cnt + 1
            if cnt == 5 then
                item:SetText( "Programista muzyki Janusz" )
            end
        end
    end
    local cnt = 0
    AboutMadej = function( item )
        if enabled then
            cnt = cnt + 1
            if cnt == 5 then
                item:SetText( "Madej, bez skojarzen" )
            end
        end
    end
    local cnt = 0
    AboutPingwin = function( item )
        if enabled then
            cnt = cnt + 1
            if cnt == 5 then
                item:SetText( "Pingwin" )
            end
        end
    end
end

function SettingsTab( tab )
    if tab == currentTab then return end

    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SETTINGS_TAB_CHANGED, tab )

    local on0 = tabs.h[currentTab]:GetControl( "on" )
    local on1 = tabs.h[tab]:GetControl( "on" )
    local off0 = tabs.h[currentTab]:GetControl( "off" )
    local off1 = tabs.h[tab]:GetControl( "off" )
    local p0 = tabs.p[currentTab]
    local p1 = tabs.p[tab]

    off0:SetVisibility( true )
    off0:SetAlpha( 0 )
    on1:SetVisibility( true )
    on1:SetAlpha( 0 )

    active = false
    anim:Add( Slider:New( 0.2,
        function( t )
            local st = SmoothStep( t ) * 255
            on0:SetAlpha( 255 - st )
            off0:SetAlpha( st )
            on1:SetAlpha( st )
            off1:SetAlpha( 255 - st )
        end
        ),
        function()
            on0:SetVisibility( false )
            off1:SetVisibility( false )
            currentTab = tab
            active = true
        end
        )
    anim:Add( Slider:New( 0.1,
        function( t )
            p0:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end
        ),
        function()
            p0:SetVisibility( false )
            p1:SetVisibility( true )
            p1:SetAlpha( 0 )
            anim:Add( Slider:New( 0.1,
                function( t )
                    p1:SetAlpha( SmoothStep( t ) * 255 )
                end
                ) )
        end
        )

    if tab == 2 then
        AboutRestart()
    end
end

function SettingsSetVolume()
    local musicVolume = registry:Get( "/monstaz/settings/sound" )
    local soundVolume = registry:Get( "/monstaz/settings/sfx" )

    for i=1,10 do
        musicBar[i][1]:SetVisibility( musicVolume >= i )
        musicBar[i][2]:SetVisibility( musicVolume < i )
        soundBar[i][1]:SetVisibility( soundVolume >= i )
        soundBar[i][2]:SetVisibility( soundVolume < i )
    end
end

function SettingsSetAiming()
    local autoaim = registry:Get( "/monstaz/settings/autoaim" )

    popup:GetControl( "PanelOptions/Aim/Auto" ):SetRepresentation( autoaim and "on" or "default" )
    popup:GetControl( "PanelOptions/Aim/Manual" ):SetRepresentation( autoaim and "default" or "on" )

    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_AUTOAIMING_CHANGED, autoaim and 1 or 0 )
end

function SettingsSetVPad()
    local vpad = registry:Get( "/monstaz/settings/fixedvpad" )

    popup:GetControl( "PanelOptions/Navigation/Fixed" ):SetRepresentation( vpad and "on" or "default" )
    popup:GetControl( "PanelOptions/Navigation/Floating" ):SetRepresentation( vpad and "default" or "on" )
end

function SettingsSetBlood()
    local blood = registry:Get( "/monstaz/settings/blood" )

    popup:GetControl( "PanelOptions/Blood/Red" ):SetRepresentation( ( blood == 1 ) and "on" or "default" )
    popup:GetControl( "PanelOptions/Blood/Green" ):SetRepresentation( ( blood == 1 ) and "default" or "on" )
end

function SettingsSetQuality()
    local pp = registry:Get( "/monstaz/settings/postprocess" )
    local low = registry:Get( "/monstaz/settings/lowquality" )

    popup:GetControl( "PanelOptions/Quality/Low" ):SetRepresentation( ( not pp and low ) and "on" or "default" )
    popup:GetControl( "PanelOptions/Quality/Medium" ):SetRepresentation( ( not pp and not low ) and "on" or "default" )
    popup:GetControl( "PanelOptions/Quality/High" ):SetRepresentation( ( pp and not low ) and "on" or "default" )
end

function SettingsPopupInit( screen, isingame )
    ingame = isingame

    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_settings.xml" )
    popup = screen:GetControl( "/SettingsPopup" )

    x = 0
    y = 0

    local options = popup:GetControl( "PanelOptions" )
    scroll = popup:GetControl( "PanelAbout/Clip/Scroll" )

    for i=1,10 do
        local tab = {}
        table.insert( tab, options:GetControl( "Music/" .. i .. "/on" ) )
        table.insert( tab, options:GetControl( "Music/" .. i .. "/off" ) )
        table.insert( musicBar, tab )
        local tab = {}
        table.insert( tab, options:GetControl( "Sound/" .. i .. "/on" ) )
        table.insert( tab, options:GetControl( "Sound/" .. i .. "/off" ) )
        table.insert( soundBar, tab )
    end

    SettingsSetVolume()
    SettingsSetAiming()
    SettingsSetVPad()
    SettingsSetBlood()
    SettingsSetQuality()

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    table.insert( tabs.h, popup:GetControl( "Options" ) )
    table.insert( tabs.h, popup:GetControl( "About" ) )
    table.insert( tabs.h, popup:GetControl( "Help" ) )

    table.insert( tabs.p, popup:GetControl( "PanelOptions" ) )
    table.insert( tabs.p, popup:GetControl( "PanelAbout" ) )
    table.insert( tabs.p, popup:GetControl( "PanelHelp" ) )

    AboutImg( "menu2/logo.png@linear" )

    AboutText( "TEXT_PRODUCTION", AboutDingEnable )
    AboutText( "" )
    AboutText( "TEXT_EXECUTIVEPRODUCER" )
    AboutText( "Wojtek Wronowski" )
    AboutText( "" )
    AboutText( "TEXT_PRODUCER" ) 
    AboutText( "Marcin Chelkowski" )
    AboutText( "" )
    AboutText( "TEXT_ARTDIR" )
    AboutText( "Adam Chwastowski" )
    AboutText( "" )
    AboutText( "TEXT_LEADPROG", AboutJanusz )
    AboutText( "Bartosz Taudul" )
    AboutText( "" )
    AboutText( "TEXT_PROGRAMMERS" )
    AboutText( "Jerzy Bondarowicz", AboutError )
    AboutText( "Jacek Nijaki" )
    AboutText( "Jakub Wegrzyn" )
    AboutText( "" )
    AboutText( "TEXT_LEAD2D" )   
    AboutText( "Tomek Biniek" )    
    AboutText( "" )
    AboutText( "TEXT_2DART" ) 
    AboutText( "Zofia Gdula" )
    AboutText( "Jakub Markiewicz" )
    AboutText( "Magdalena Proszowska" )
    AboutText( "" )
    AboutText( "TEXT_3DART" ) 
    AboutText( "Mariusz Kadewski" )   
    AboutText( "Konrad Krzeminski" )
    AboutText( "" )
    AboutText( "TEXT_DESIGNERS" ) 
    AboutText( "Kamil Popielski", AboutDing )
    AboutText( "Wojciech Wronowski" )
    AboutText( "" )
    AboutText( "TEXT_MUSICSFX" ) 
    AboutText( "Marcin Przybylowicz" )
    AboutText( "Przemyslaw Laszczyk" )
    AboutText( "" )
    AboutText( "TEXT_VOICEACTOR" ) 
    AboutText( "Tori Kamal" )
    AboutText( "" )
    AboutText( "TEXT_QALEAD" ) 
    AboutText( "Pawel Makarewicz" )
    AboutText( "" )
    AboutText( "TEXT_QATESTERS" ) 
    AboutText( "Kamil Bielarczyk" )
    AboutText( "Kamil Tulikowski" )
    AboutText( "" )
    AboutText( "TEXT_DATAANALYST", AboutMadej )
    AboutText( "Grzegorz Tarczynski", AboutPingwin )
    AboutText( "" )
    AboutText( "TEXT_THANKS" )
    AboutText( "Sebastian Szczygiel" )
    AboutText( "Wojtek Piejko" )
    AboutText( "Mateusz Bozykowski" )
    AboutText( "Marcin Moys" )
    AboutText( "Michal Grzeda" )
    AboutText( "Wojtek Gasek" )
    AboutText( "" )
    AboutText( "" )
    AboutText( "TEXT_VERSION" )
    versionText = AboutText( "" )


    AboutStart()

    if ingame then
        options:GetControl( "Aim" ):SetAlpha( 128 )
        options:GetControl( "Quality" ):SetAlpha( 128 )
        options:GetControl( "Blood" ):SetAlpha( 128 )
    end
end

function SettingsPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        SettingsPopupHide()
        return true
    end

    return false
end

function SettingsPopupPress( control, path )
    if not active then
        return
    end

    if path == "/SettingsPopup/Back" then
        ButtonPress( control )
    elseif path == "/SettingsPopup/PanelOptions/Music/VolumeUp" then
        ButtonPress( control )
    elseif path == "/SettingsPopup/PanelOptions/Music/VolumeDown" then
        ButtonPress( control )
    elseif path == "/SettingsPopup/PanelOptions/Sound/VolumeUp" then
        ButtonPress( control )
    elseif path == "/SettingsPopup/PanelOptions/Sound/VolumeDown" then
        ButtonPress( control )
    else
        for k,v in pairs( AboutMap ) do
            if k == path then
                v.f( v.i )
            end
        end
    end
end

function SettingsPopupAction( control, path )
    if not active then
        return
    end

    if path == "/SettingsPopup/Back" then
        SettingsPopupHide()
    elseif string.sub( path, 1, 22 ) == "/SettingsPopup/Options" then
        SettingsTab( 1 )
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
    elseif string.sub( path, 1, 20 ) == "/SettingsPopup/About" then
        SettingsTab( 2 )
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
    elseif string.sub( path, 1, 19 ) == "/SettingsPopup/Help" then
        SettingsTab( 3 )
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
    elseif path == "/SettingsPopup/PanelOptions/Music/Plus" then
        local musicVolume = registry:Get( "/monstaz/settings/sound" )
        AudioManager:VolumeChange( Sfx.SFX_SHOTGUN2 ) -- Morningtec
        musicVolume = math.min( 10, musicVolume + 1 )
        registry:Set( "/monstaz/settings/sound", musicVolume )
        SettingsSetVolume()
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
    elseif path == "/SettingsPopup/PanelOptions/Music/Minus" then
        local musicVolume = registry:Get( "/monstaz/settings/sound" )
        AudioManager:VolumeChange( Sfx.SFX_SHOTGUN2 ) -- Morningtec
        musicVolume = math.max( 0, musicVolume - 1 )
        registry:Set( "/monstaz/settings/sound", musicVolume )
        SettingsSetVolume()
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
    elseif path == "/SettingsPopup/PanelOptions/Sound/Plus" then
        local soundVolume = registry:Get( "/monstaz/settings/sfx" )
        AudioManager:VolumeChange( Sfx.SFX_SHOTGUN2 ) -- Morningtec
        soundVolume = math.min( 10, soundVolume + 1 )
        registry:Set( "/monstaz/settings/sfx", soundVolume )
        SettingsSetVolume()
        AudioManager:Play( Sfx.SFX_SHOTGUN2 )
    elseif path == "/SettingsPopup/PanelOptions/Sound/Minus" then
        local soundVolume = registry:Get( "/monstaz/settings/sfx" )
        AudioManager:VolumeChange( Sfx.SFX_SHOTGUN2 ) -- Morningtec
        soundVolume = math.max( 0, soundVolume - 1 )
        registry:Set( "/monstaz/settings/sfx", soundVolume )
        SettingsSetVolume()
        AudioManager:Play( Sfx.SFX_SHOTGUN2 )
    elseif not ingame and string.sub( path, 1, 31 ) == "/SettingsPopup/PanelOptions/Aim" then
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
        local mode = string.find( path, "Auto" ) ~= nil
        registry:Set( "/monstaz/settings/autoaim", mode )
        SettingsSetAiming()
    elseif string.sub( path, 1, 38 ) == "/SettingsPopup/PanelOptions/Navigation" then
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
        local mode = string.find( path, "Fixed" ) ~= nil
        registry:Set( "/monstaz/settings/fixedvpad", mode )
        SettingsSetVPad()
    elseif not ingame and string.sub( path, 1, 33 ) == "/SettingsPopup/PanelOptions/Blood" then
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
        local red = string.find( path, "Red" ) ~= nil
        registry:Set( "/monstaz/settings/blood", red and 1 or 2 )
        SettingsSetBlood()
    elseif not ingame and string.sub( path, 1, 35 ) == "/SettingsPopup/PanelOptions/Quality" then
        AudioManager:Play( Sfx.SFX_MENU_SELECT )
        local hi = string.find( path, "High" ) ~= nil
        local lo = string.find( path, "Low" ) ~= nil
        registry:Set( "/monstaz/settings/postprocess", hi )
        registry:Set( "/monstaz/settings/lowquality", lo )
        SettingsSetQuality()
        if hi then
            SettingsQualityWarningShow()
        end
    end
end

function SettingsPopupShow()
    active = true
    callback:Playhaven( "options" )
    PopupLock()
    SettingsUpdateVersion()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )

    SettingsTab( 1 )
end

function SettingsPopupHide()
    active = false
    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function SettingsUpdateVersion()
    if versionText then
        versionText:SetText( callback:GetVersionString() )
    end
end

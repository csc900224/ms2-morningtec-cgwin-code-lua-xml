require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 220 )
local height = R( 195 )

local errorMassage = "error"
local iconFile = "menu2/cat_02.png@linear"

local callbackReturn = nil

function ErrorPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_error.xml" )
    popup = screen:GetControl( "/ErrorPopup" )

    x = math.floor(( SCREEN_WIDTH - width )/2)
    y = math.floor(( SCREEN_HEIGHT - height )/2)

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )


    local backBtn = popup:GetControl( "Back" )

    local w, h = backBtn:GetGraphic( "TS" ):GetSize()
    backBtn:SetX( ( width - w )/2 )
    backBtn:SetY( height - h - R(15))
end

function ErrorPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ErrorPopupHide()
        return true
    end

    return false
end

function ErrorPopupAction( name )
    if not active then
        return
    end

    if name == "/ErrorPopup/Back" then
        ErrorPopupHide()
    end
end

function ErrorPopupPress( control, path )
    if not active then
        return
    end

    if path == "/ErrorPopup/Back" then
        ButtonPress( control )
    end
end

function ErrorPopupShow( errorMsg , callback )
    
    active = true
    
	SyncIndicatorHide()
	
    ErrorPopupFillData( iconFile, errorMsg )
    
    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    
    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
    callbackReturn = callback
end

function ErrorPopupHide()
    active = false
    StopStarAnimation(  popup:GetControl( "Icon/Star" ) )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callbackReturn then
                callbackReturn()
            else
                PopupUnlock()
            end
            
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function ErrorPopupFillData( iconPath , errorMsg )

    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconPath )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )

    --local message =  popup:GetControl( "Message" )
    --message:SetText( errorMsg )
    
    
    
        if (errorMsg == "TEXT_RESISTANCE_TIMED_OUT") then
            popup:GetControl( "ErrorTextLong" ):SetText( errorMsg )
            popup:GetControl( "ErrorTextLong" ):SetVisibility( true )
            popup:GetControl( "ErrorText" ):SetVisibility( false )
        else
            popup:GetControl( "ErrorText" ):SetText( errorMsg )
            popup:GetControl( "ErrorText" ):SetVisibility( true )
            popup:GetControl( "ErrorTextLong" ):SetVisibility( false )
        end
        
    errorMassage = errorMsg
    
    
end

require( "menu2/common.lua" )
require( "menu2/animation.lua" )
require( "menu2/delay.lua" )
require( "menu2/slider.lua" )
require( "menu2/perpetual.lua" )
require( "menu2/math.lua" )


local height = 0
local root = 0

local Time = {
    slideIn = 0.5,
    slideOut = 0.7,
    visibile = 3.0
}

local iconOffset = R( 6 )
local socialIconPath = "menu2/icon_social.png@linear"

function NewFriendsNotifierInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/notifier_new_friends.xml" )
    root = screen:GetControl( "/NewFriendsNotifier" )
    root:SetVisibility( false )
end

function NewFriendsNotifierShow( count )
    root:SetVisibility( true )
    NewFriendsNotifierSetContent( socialIconPath , count )
    NewFriendsNotifierStartAnimation()
end

function NewFriendsNotifierSetContent( iconPath , newFriendsNumber )
    local msg = root:GetControl( "Msg" )

    if newFriendsNumber > 1 then
        msg:SetTextWithVariables( "TEXT_NEW_FRIENDS", { VALUE = tostring( newFriendsNumber ) } )
    else
        msg:SetText( "TEXT_NEW_FRIEND" )
    end

    local tW, tH = msg:GetSize()

    local icon = root:GetControl( "Icon" )
    icon:GetGraphic( "s" ):SetImage( iconPath )
    local iW, iH = icon:GetGraphic( "s" ):GetSize()
    
    local frame = root:GetControl( "Frame" )
    local bgW, bgH = frame:GetGraphic( "TS" ):GetSize()
    height = math.max(  bgH, iconOffset * 2 + iH )

    local fillHeight = height - bgH
    if fillHeight > 0 then
        local fX, fY, fW, fH  = frame:GetControl( "Fill" ):GetGraphic( "s" ):GetClipRect()
        frame:GetControl( "Fill" ):GetGraphic( "s" ):SetClipRect( fX, fY, fW, fillHeight )
    end

    icon:SetY( height / 2 + iconOffset )
    msg:SetY( icon:GetY() - msg:GetHeight() / 2 )
end

function NewFriendsNotifierStartAnimation()
    anim:Add( Slider:New( Time.slideIn, 
        function( t )
            root:SetY( SCREEN_HEIGHT - height * SmoothStep( t ) )
        end),
        function()
            anim:Add( Delay:New( Time.visibile ),
                function()
                    anim:Add( Slider:New( Time.slideOut, 
                        function( t )
                            root:SetY( SCREEN_HEIGHT - height + SmoothStep( t ) * height )
                        end),
                        function()
                            root:SetVisibility( false )
                        end
                     )
                end
            )
        end
    )
end

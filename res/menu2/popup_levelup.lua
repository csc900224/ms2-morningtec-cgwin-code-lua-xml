require( "PlayerLevels.lua" )
require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/background_star.lua" )

local active = false

local popup = nil
local star = nil
local popupCloseCallback = nil
local popupFrame = {
    x = 0,
    y = 0,
    width = R( 206 ),
    height = R( 204 )
}

local itemFrame = {
    x = 0,
    y = 0,
    width = R(108),
    height = R(108)
}

function LevelupPopupInit( parent )
    parent:InsertFromXml( "menu2/popup_levelup.xml" )
    popup = parent:GetControl( "LevelupPopup" )

    x = math.floor( ( SCREEN_WIDTH - popupFrame.width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - popupFrame.height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    PopupCreateFrame( popup:GetControl( "Frame" ), popupFrame.width, popupFrame.height )
    star = BackgroundStar( popup, popupFrame.width/2, popupFrame.height/2, 1 )
end

function LevelupPopupSetup()
    local level = registry:Get( "/monstaz/player/level" )
    local soft = PlayerLevels[level].s
    local hard = PlayerLevels[level].h

    if registry:Get( "/monstaz/subscription" ) then
        hard = math.floor( hard * 1.5 )
    end

    popup:GetControl( "Header/Level" ):SetText( tostring( level ) )
    popup:GetControl( "Rewards/Cash/Text" ):SetText( "\194\156 +" .. soft )
    popup:GetControl( "Rewards/Gold/Text" ):SetText( "\194\158 +" .. hard )

    local s, h = Shop:GetCash()
    Shop:SetCash( s + soft, h + hard )

    local weapons = ItemsCategory[ItemCategory.weapons]
    for i, weapon in ipairs( weapons ) do
        if Shop:IsBought( weapon.id ) == 0 and ItemDbGetItemLevel( weapon.id ) == level then
            registry:Set( "/internal/unlocked-weapon", weapon.id )
            break
        end
    end
end

function LevelupPopupSetCloseCallback( callback )
    popupCloseCallback = callback
end

function LevelupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        LevelupPopupHide()
        return true
    end

    return false
end

function LevelupPopupPress( control, path )
    if not active then
        return
    end

    if path == "/LevelupPopup/Close" then
        ButtonPress( control )
    end
end

function LevelupPopupAction( control, path )
    if not active then
        return
    end

    if path == "/LevelupPopup/Close" then
        LevelupPopupHide()
        TutorialAction( path )
    end
end

function LevelupPopupShow()
    active = true
    callback:Playhaven( "level_up" )
    PopupLock( 0.3, "/LevelupPopup/Overlay" )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_LEVEL_UP )
    anim:Add( Delay:New( 2 ),
        function( t )
            AudioManager:Play( Sfx.SFX_VO_LEVELUP )
        end
        )
    BackgroundStarStart( star, 0.3 )
end

function LevelupPopupHide()
    active = false

    PopupUnlock( 0.3, "/LevelupPopup/Overlay" )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            if popupCloseCallback then popupCloseCallback() end
            popup:SetVisibility( false )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
    BackgroundStarStop( star, 0.3 )
end

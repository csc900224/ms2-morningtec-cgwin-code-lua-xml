require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/avatar_social.lua" )

local margin = R( 5 )
local startY = R( 9 )

local item = nil
local itemSelected = nil

local ElementMap = {}
ElementMap["Electric"] = Element.electric
ElementMap["Fire"] = Element.fire
ElementMap["Frost"] = Element.frost

local weaponSlotWidth = R( 125 )
local weaponSlotHeight = R( 100 )

function BackupItemInit( parent, width, height, friend )

    if not friend then return end
    -- Insert from xml and get item root
    parent:InsertFromXml( "menu2/backup_item.xml" )
    root = parent:GetControl( "BackupItem" )

    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    -- avatar img 
    local playerAvatarPath = GetAvatarImgPath( friend )

    local avatar = parent:GetControl( "BackupItem/InfoRoot/BackupInfo/Avatar" )
    local scale  = SetAvatarImg( avatar , playerAvatarPath )

    if IsBot( friend ) then 
        avatar:GetGraphic( "TS" ):SetImage( avatarDefultPathPremission )
    end

    local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
    avatar:SetX( ( width - aw*scale ) /2)
    avatar:SetY( startY + margin )

    -- vip frame
    local vip_frame = parent:GetControl( "BackupItem/InfoRoot/BackupInfo/VIP1" )
    vip_frame:SetX( ( width - aw*scale ) /2)
    vip_frame:SetY( startY + margin )

    local vip_badge = parent:GetControl( "BackupItem/InfoRoot/VIP2" )
    vip_badge:SetX( ( width - parent:GetControl( "BackupItem/InfoRoot/VIP2/Badge"):GetWidth() ) /2)
    vip_badge:SetY( startY + ah * scale )

    local isFriendVip = tonumber(friend.IsVip) == 1
    vip_frame:SetVisibility( isFriendVip )
    vip_badge:SetVisibility( isFriendVip )

    -- level info 
    local lvlInfo  = parent:GetControl( "BackupItem/InfoRoot/BackupInfo/LevelInfo")
    lvlInfo:GetControl( "TextLevel"):SetText("TEXT_LVL")
    lvlInfo:GetControl( "LevelValue"):SetText( friend.Level )
    lvlInfo:SetY( startY - R( 2 ) )
    lvlInfo:SetX( avatar:GetX() - R( 12 ) )


    -- bg - avatar img bottom line
    local bg = parent:GetControl( "BackupItem/InfoRoot/BG" )
    bg:SetX( ( width - aw * scale )/2 )
    CreateBottomLine( bg, aw * scale, startY + ah * scale )

    -- player Name
    -- text vesrion disabled - used for debug 
    local name = parent:GetControl( "BackupItem/InfoRoot/BackupInfo/Name" )
    name:SetY( startY + ah * scale + 2.5 * margin )
    name:SetTextWidth( width );
    name:SetText( friend.Name )
    name:SetVisibility( false )

    -- img version
    local nameImg = parent:GetControl( "BackupItem/InfoRoot/BackupInfo/NameImage" )

    callback:ResistanceSetUsernameImg( nameImg:GetPath() , friend.Name )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 

    nameImg:SetX( ( width - w )/2)
    nameImg:SetY( startY + ah * scale + 2.5 * margin )
    nameImg:SetVisibility( true )

    -- Weapon info root
    local weaponRoot = parent:GetControl( "BackupItem/InfoRoot/WeaponInfoRoot")
    weaponRoot:SetX( ( width - weaponSlotWidth ) / 2 )
    weaponRoot:SetY( nameImg:GetY() + nameImg:GetHeight()/2 )

    local weapons = ItemsCategory[1]
    local weapon = nil

    local currentLvlIdx = tonumber( friend.UsedWeaponUpgrade ) + 1
    local idxWeapon = 1 -- smg default

    for i=1,# weapons do
        if weapons[i].id == friend.UsedWeaponId then
            idxWeapon = i
            weapon =  weapons[i]
            break
        end
    end

    --Elements
    local elementsOwned = {}

    if weapon then
        if weapon.upgrades and weapon.upgrades.Elements then
            local maxIdx = math.min( #weapon.upgrades.Elements, currentLvlIdx )
            for i=1,maxIdx do
                if weapon.upgrades.Elements[i] > 0 then
                    table.insert( elementsOwned, weapon.upgrades.Elements[i] )
                end
            end
        end

        WeaponMarkElements( weapon , weaponRoot:GetControl( "WeaponSlot/Elements" ), elementsOwned ) 
    end

    -- wepon Icon position
    local weaponAngle = 0
    local icon =  weaponRoot:GetControl( "WeaponSlot/WeaponIcon" )
    icon:GetGraphic( "TS" ):SetImage( weapons[idxWeapon].f )

    -- special case mech
    if weapons[idxWeapon].id == ShopItem.Mech then
        icon:GetGraphic("TS"):SetScale( 0.8 )
    else
        icon:GetGraphic("TS"):SetScale( 1 )
    end

    local w, h = icon:GetGraphic( "TS" ):GetSize()

    icon:GetGraphic( "TS" ):SetAngle( weaponAngle )
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )

    icon:SetX( weaponSlotWidth/2 )
    icon:SetY( weaponSlotHeight/2 )

    -- buttons
    local selectFriendButton = parent:GetControl( "BackupItem/InfoRoot/Buttons/SelectFriend" )
    local speedUpButton = parent:GetControl( "BackupItem/InfoRoot/Buttons/SpeedUp" )
    local confirmButton = parent:GetControl( "BackupItem/InfoRoot/Buttons/Confirm" )

    local buttonW , buttonH = selectFriendButton:GetGraphic( "TS" ):GetSize()

    local marginName =  R(8)
    local posX = ( width - buttonW ) / 2
    local posY = height - buttonH - 3*marginName/2 -- startY + ah * scale + margin + buttonH + marginName

    selectFriendButton:SetX( posX )
    selectFriendButton:SetY( posY )

    speedUpButton:SetX( posX )
    speedUpButton:SetY( posY )

    confirmButton:SetX( posX )
    confirmButton:SetY( posY )

    selectFriendButton:InsertControl( parent:GetName() )
    speedUpButton:InsertControl( parent:GetName() )
    confirmButton:InsertControl( parent:GetName() )

    local recoveryTimeTextValue = parent:GetControl( "BackupItem/InfoRoot/BackupInfo/RecoveryTimer" )

    recoveryTimeTextValue:SetX( ( width - recoveryTimeTextValue:GetWidth() ) /2)
    recoveryTimeTextValue:SetY( avatar:GetY() +  ah * scale - R(18) )
    local timeCanHelp =  tonumber( friend.CanHelp ) - TimeSinceUpdateDB

    if timeCanHelp > 0 then
        selectFriendButton:SetVisibility( false )
        speedUpButton:GetControl( "SpeedUpLabel" ):SetTextWithVariables( "TEXT_BACKUP_SPEED_UP", { VALUE = tostring( GetCostOfSpeedUp() ) } )
        speedUpButton:SetVisibility( true )
        recoveryTimeTextValue:SetText( TimeFormat( timeCanHelp ) )
        recoveryTimeTextValue:SetVisibility( true )
        avatar:SetAlpha( 120 )
    else
        selectFriendButton:SetVisibility( true )
        avatar:SetAlpha( 255 )
        speedUpButton:SetVisibility( false )
        recoveryTimeTextValue:SetVisibility( false )
    end
end

function BackupItemAction( control, path )

        if string.find( path, "/SelectFriend" ) then
            local idxFriend = control:GetChildren()[2]:GetName()
            local friend = Friends[tonumber ( idxFriend )]
            itemSelected = friend
            BackupTeamHide( function() PremissionShow( true )  RefreshBackupSlot( FindFriendByUniqueId( friend.UniqueId ) ) PopupUnlock() end )
        elseif string.find( path, "/SpeedUp" ) then
            local idxFriend = control:GetChildren()[2]:GetName()
            local friend = Friends[tonumber ( idxFriend )]
            itemSelected = friend
            local root = BackupItemGetRoot( control )
            BackupItemClearConfirmations( root:GetControl( "../.." ))

            local timeLeft = tonumber( GetBackupFlag( friend.UniqueId ) )

            local _,hard = Shop:GetCash()
            if hard < GetCostOfSpeedUp() then
                BackupTeamHide( OnBackupItemCantAfford() )
            else
                BackupItemShowConfirm( root , true )
            end

        elseif string.find( path, "/Confirm" ) then
            local idxFriend = control:GetChildren()[2]:GetName()
            local friend = Friends[tonumber ( idxFriend )]
            itemSelected = friend

            local root = BackupItemGetRoot( control )

            BackupItemClearConfirmations( root:GetControl( "../.." ))
            BackupTeamHide( function() SpeedUpBackupRecovery( friend ) end ) 

            root:GetControl("InfoRoot/Buttons/SelectFriend"):SetVisibility( true )
            root:GetControl("InfoRoot/Buttons/SpeedUp"):SetVisibility( false )  
        end
end

function GetInfoForBackup( friend )
    callback:ResistanceOnGetSocialInfo( friend.UniqueId )
end

function BackupItemUpdate( root , uniqueId , timeCanHelp )

        local recoveryTimeTextValue = root:GetControl( "BackupItem/InfoRoot/BackupInfo/RecoveryTimer" )
        local selectFriendButton = root:GetControl( "BackupItem/InfoRoot/Buttons/SelectFriend" )
        local speedUpButton = root:GetControl( "BackupItem/InfoRoot/Buttons/SpeedUp" )

        if timeCanHelp > 0 then
            recoveryTimeTextValue:SetText( TimeFormat( timeCanHelp ) )
            speedUpButton:GetControl( "SpeedUpLabel" ):SetTextWithVariables( "TEXT_BACKUP_SPEED_UP", { VALUE = tostring( GetCostOfSpeedUp() ) } )
        else
            selectFriendButton:SetVisibility( true )
            selectFriendButton:SetTouchable( true )
            SetBackupFlag( uniqueId , 0 )
            recoveryTimeTextValue:SetVisibility( false )
            speedUpButton:SetVisibility( false )
            speedUpButton:SetTouchable( false )
        end
end

function WeaponMarkElements( item , elementsRoot, elementsActive )

    local activeSet = {}
    for _, l in ipairs(elementsActive) do activeSet[l] = true end

    local rootPath = elementsRoot:GetPath()

    for idx, element in ipairs( elementsRoot:GetChildren() ) do

        local elelmentName = element:GetName()
        local elelmentId = ElementMap[elelmentName]
        local hasElement = ItemHasElementUpgrade( item, elelmentId )

        if hasElement then
            element:SetVisibility( true )
            -- Center pivots
            local graphic = element:GetGraphic( "TS" )
            local pivX, pivY = graphic:GetPivot()
            if pivX == 0 and pivY == 0 then
                local w, h = graphic:GetSize()
                
                element:SetRepresentation( "active" )
                element:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
                element:SetRepresentation( "default" )
                element:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
                
                element:SetX( element:GetX() + w/2 )
                element:SetY( element:GetY() + h/2 )
            end
            
            if activeSet[elelmentId] ~= nil then
                element:SetRepresentation( "active" )
                element:SetAlpha( 255 )
            else
                element:SetRepresentation( "default" )
                element:SetAlpha( 255 )
            end
        else
            element:SetVisibility( false )
        end
    end
end

function ItemHasElementUpgrade( item, elementToCheck )
    for _, element in ipairs( item.upgrades.Elements ) do
        if element == elementToCheck then
            return true
        end
    end
    return false   
end

function BackupItemClearConfirmations( containerRoot )
    for _, item in ipairs( containerRoot:GetChildren() ) do
        BackupItemShowConfirm( item:GetChildren()[1] , false )
    end
    
end

function BackupItemShowConfirm( root, show )
    if show then
        root:GetControl( "InfoRoot/Buttons/Confirm" ):SetVisibility( true )
    else
        root:GetControl( "InfoRoot/Buttons/Confirm" ):SetVisibility( false )
    end
end

function BackupItemGetRoot( c )
    local root = c
    while root and root:GetName() ~= "BackupItem" do
        root = root:GetControl( ".." )
    end
    return root
end

CostsReq = { 20 , 15 , 10, 0} --hours left
Costs = { 5,  4, 3, 2  } -- cost - gold 

function GetCostOfSpeedUpByTime( timeleft )
    local cost = 5

    for i=1, # CostsReq do
        if timeleft > CostsReq[i]*3600 then
            cost = Costs[i]
            break
        end	
    end

    return cost
end

MaxSpeedUpPrice = 5

function GetCostOfSpeedUp()
    local cost = registry:Get( "/monstaz/resistance/speedupcount") 

    MaxSpeedUpPrice = registry:Get( "/app-config/resistance/speedmaxcost")

    if cost < registry:Get( "/app-config/resistance/speedupcostmin") then 
        cost = registry:Get( "/app-config/resistance/speedupcostmin")
        registry:Set( "/monstaz/resistance/speedupcount", cost ) 
    end

    return math.min( cost , MaxSpeedUpPrice )
end

function SpeedUpBackupRecovery( friend )

    if IsBot( friend ) then
        SyncIndicatorShow()
        
        SpeedUpResetSuccess( friend.UniqueId )
        registry:Set( "/monstaz/aifriend/canHelp", 0 );
        callback:Save()
    else
        callback:ResistanceSpeedUpFriendBackup( friend.UniqueId )
        SyncIndicatorShow()
    end

end

function SpeedUpResetSuccess( friendId )

    MaxSpeedUpPrice = registry:Get( "/app-config/resistance/speedmaxcost" )
    local timeLeft = tonumber( GetBackupFlag( friendId ) )

    local soft, hard = Shop:GetCash()
    Shop:SetCash( soft, hard - GetCostOfSpeedUp() )
    callback:Save()

    local step = 1
    if registry:Get( "/app-config/resistance/speedupcoststep") then
        step = registry:Get( "/app-config/resistance/speedupcoststep")
    end
    registry:Set( "/monstaz/resistance/speedupcount", math.min( MaxSpeedUpPrice , registry:Get( "/monstaz/resistance/speedupcount") + step ) )

    SetBackupFlag( friendId , 0 )

   -- OperationSuccessPopupShow( "TEXT_RESISTANCE_SPEED_UP_SUCCESS", function()  BackupTeamShow( false ) end )
    OperationSuccessPopupShow( "TEXT_RESISTANCE_SPEED_UP_SUCCESS", function() PremissionShow( true )  RefreshBackupSlot( FindFriendByUniqueId( friendId ) ) PopupUnlock()end )
    SyncIndicatorHide()
end

function SpeedUpResetFailed()
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" , function() BackupTeamShow() end)
    SyncIndicatorHide()
end

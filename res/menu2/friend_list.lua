require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/social_item.lua" )
require( "menu2/friend_scroll_indicator.lua" )

local root = nil
local clip = nil
local width = nil
local height = nil
local items = nil
local currentCat = nil
local indicator = nil

FriendsConsts = {
    itemHeight = R( 155 ),
    itemWidth = R( 128 ),
    itemSpacing = R(5),
    itemXOffset = R(5) 
}

local FriendsScrollData = {
    touchId = -1,
    minScrollDist = R(5),
    maxScrollOffset = 0,
    scrollOffset = 0,
    sampleTouchPos = 0,
    currentTouchPos = 0,
    startScrollOffset = 0,
    touchStart = 0,
    scrolling = false,    
    targetScrollOffset = 0,
    scrollAcceleration = 1,
    samplingInterval = 0.025,
    sampleTimer = -1,
    velocityEpsilon = 0.1,
    velocity = 0,
    lastVelocity = 0,
    draggTime = 0,
    hidePosIndicator = false
}

function FriendsListInit( parent, listWidth, listHeight, itemWidth, itemSpacing )
    parent:InsertFromXml( "menu2/friend_list.xml" )
    clip = parent:GetControl( "FriendList" )
    root = parent:GetControl( "FriendList/Friends" )
    indicator = parent:GetControl( "FriendScrollIndicatorRoot" )
    width = listWidth
    height = listHeight

    if itemWidth ~= nil then
        FriendsConsts.itemWidth = itemWidth
    end

    if itemSpacing ~= nil then
        FriendsConsts.itemSpacing = itemSpacing
    end 

    FriendScrollIndicatorInit( indicator, 0, FriendsConsts.itemWidth )
    FriendsListCaluclateLayout()

    clip:GetGraphic( "r" ):SetWidth( width )
    clip:GetGraphic( "r" ):SetHeight( FriendsConsts.itemHeight )

end

function FriendsListCaluclateLayout()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    --FriendsConsts.itemHeight = math.min( height - iH, FriendsConsts.itemHeight)
    indicator:SetY( FriendsConsts.itemHeight + iH/4)
    indicator:SetX( (width - iW)/2 )

    local bottomFrame = screen:GetControl( "/Social/ResistanceHq/Frame/lb" )
    if indicator:GetAbsY() > bottomFrame:GetAbsY() + bottomFrame:GetHeight()/2 then
        FriendsScrollData.hidePosIndicator = true
        indicator:SetVisibility( false )
    end
end

function FriendsListUpdateIndicatorPos()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    indicator:SetX( (width - iW)/2 )
end

function FriendsListSetContent( itemList, resetScroll )
    if resetScroll == nil or resetScroll == true then
        FriendsListResetScroll()
    end
    FriendsListDropAllItems()

    items = itemList
    local x = FriendsConsts.itemXOffset
    local idx = 1
    for _, item in ipairs( itemList ) do
        local itemRoot = root:InsertControl( tostring( idx ) )
        idx=idx+1
        itemRoot:SetRelative( true )
        itemRoot:SetX( x )
        SocialItemInit( itemRoot, FriendsConsts.itemWidth, FriendsConsts.itemHeight, item )
        x = x + FriendsConsts.itemWidth + FriendsConsts.itemSpacing
    end
    FriendsScrollData.maxScrollOffset = math.max( x - FriendsConsts.itemSpacing + FriendsConsts.itemXOffset - width, 0 )

    FriendScrollIndicatorUpdateMaxScrollOffset( FriendsScrollData.maxScrollOffset )
    FriendsListUpdateIndicatorPos()
    
    FriendsListCreateClipBorder()
end

function FriendsListResetScroll()
    FriendsScrollData.scrollOffset = 0
    root:SetX( 0 )
    FriendsScrollData.scrolling = false
    FriendsScrollData.touchId = -1
    FriendsScrollData.velocity = 0
    FriendsScrollData.lastVelocity = 0
    FriendsScrollData.sampleTimer = -1
    FriendsScrollData.draggTime = 0
    FriendsScrollData.targetScrollOffset = 0
    FriendsScrollData.scrollOffset = 0
end

function FriendsListUpdate( dt )
    if FriendsScrollData.touchId >= 0 then
        FriendsScrollData.draggTime = FriendsScrollData.draggTime + dt
    end

    FriendsListSampleVelocity( dt )
    FriendsListUpdateScrolling( dt )

    FriendScrollIndicatorUpdateScrollOffset( FriendsScrollData.scrollOffset )
end

function FriendsListSampleVelocity( dt )
    if FriendsScrollData.touchId >= 0 and FriendsScrollData.sampleTimer >= 0 then
        FriendsScrollData.sampleTimer = FriendsScrollData.sampleTimer - dt
        if FriendsScrollData.sampleTimer < 0 then
            FriendsScrollData.sampleTimer = FriendsScrollData.samplingInterval
            FriendsScrollData.lastVelocity = (FriendsScrollData.currentTouchPos - FriendsScrollData.sampleTouchPos) / FriendsScrollData.samplingInterval
            FriendsScrollData.sampleTouchPos = FriendsScrollData.currentTouchPos
        end
    end
end

function FriendsListTouchDown( x, y, id, c )
    if items == nil then return end

    local path = c:GetPath()
    if string.find( path, "/FriendList" ) then

        if FriendsScrollData.touchId == id then
            FriendsListTouchUp( FriendsScrollData.currentTouchPos, y, id, nil )
        end

        if FriendsScrollData.touchId < 0 then
            FriendsScrollData.touchId = id
            FriendsScrollData.touchStart = x
            FriendsScrollData.scrolling = false
            FriendsScrollData.velocity = 0
            FriendsScrollData.lastVelocity = 0
            FriendsScrollData.sampleTimer = FriendsScrollData.samplingInterval
            FriendsScrollData.currentTouchPos = x
            FriendsScrollData.sampleTouchPos = x
            FriendsScrollData.draggTime = 0
        end
    end
end

function FriendsListTouchUp( x, y, id, c )
    if items == nil then return end

    if FriendsScrollData.scrolling and id == FriendsScrollData.touchId then
        FriendsScrollData.scrolling = false
        FriendsScrollData.touchId = -1
        FriendsScrollData.sampleTimer = -1
        FriendsScrollData.targetScrollOffset = FriendItemClampOffset( FriendsScrollData.startScrollOffset + FriendsScrollData.touchStart - x )

        if FriendsScrollData.draggTime < FriendsScrollData.samplingInterval and FriendsScrollData.draggTime > 0 then
            FriendsScrollData.lastVelocity = (x - FriendsScrollData.sampleTouchPos) / FriendsScrollData.draggTime
        end
        FriendsScrollData.velocity = FriendsScrollData.lastVelocity
    else
        if not c then return end
        local path = c:GetPath()
        if string.find( path, "/Buttons" ) then
            ButtonPress( c )
            SocialItemAction( c,path )
        end
    end
end

function FriendsListTouchMove( x, y, id, c )
    if items == nil then return end
    local path = c:GetPath()
    if id == FriendsScrollData.touchId then
        if not FriendsScrollData.scrolling then
            if math.abs( FriendsScrollData.touchStart - x ) > FriendsScrollData.minScrollDist then
                -- Start real scrolling
                FriendsScrollData.touchStart = x
                FriendsScrollData.startScrollOffset = FriendsScrollData.scrollOffset
                FriendsScrollData.scrolling = true
            end
        else
            FriendsScrollData.targetScrollOffset = FriendItemClampOffset( FriendsScrollData.startScrollOffset + FriendsScrollData.touchStart - x )
            FriendsScrollData.currentTouchPos = x
        end    
    end
end

function FriendItemClampOffset( offset )
    return math.min( math.max(offset, 0), FriendsScrollData.maxScrollOffset )
end

function FriendsListUpdateScrolling( dt )
    if math.abs(FriendsScrollData.velocity) > FriendsScrollData.velocityEpsilon then
        FriendsScrollData.velocity = FriendsScrollData.velocity - FriendsScrollData.velocity * FriendsScrollData.scrollAcceleration * dt
        FriendsScrollData.targetScrollOffset = FriendItemClampOffset( FriendsScrollData.targetScrollOffset - FriendsScrollData.velocity * dt )
    end

    if math.abs(FriendsScrollData.scrollOffset - FriendsScrollData.targetScrollOffset) > FriendsScrollData.velocityEpsilon then
        FriendsScrollData.scrollOffset = FriendsScrollData.targetScrollOffset
        root:SetX( -FriendsScrollData.scrollOffset )
    end  

    FriendsListClipItems()
end

function FriendsListClipItems()
    for _, item in ipairs( root:GetChildren() ) do
        local visible = -FriendsScrollData.scrollOffset + item:GetX() <= width and -FriendsScrollData.scrollOffset + item:GetX() + FriendsConsts.itemWidth >= 0
        item:SetVisibility( visible )
    end
end

function FriendsListDropAllItems()
    for _, item in ipairs( root:GetChildren() ) do
        item:Remove()
    end
end

function FriendsListCreateClipBorder()

    local ls = clip:InsertControl("ls")
    ls:AddRepresentation( "default", "menu2/timage.xml" )
    ls:SetRelative( true )
    ls:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    ls:SetY( - R(4) )
    ls:SetX( - R(2) )

    local lsw, lsh = ls:GetGraphic( "TS" ):GetSize()

    local rs = clip:InsertControl("rs")
    rs:AddRepresentation( "default", "menu2/timage.xml" )
    rs:SetRelative( true )
    rs:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    rs:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    rs:SetX( width - lsw + R(2) )
    rs:SetY( - R(4) )

end

function FriendsListHidePosIndicator()
    indicator:SetVisibility( false )
end

function FriendsListShowPosIndicator()
    indicator:SetVisibility( not FriendsScrollData.hidePosIndicator )
end


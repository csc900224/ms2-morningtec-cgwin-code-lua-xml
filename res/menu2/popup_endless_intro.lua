require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/background_star.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false

local popup = nil
local star = nil
local popupFrame = {
    x = 0,
    y = 0,
    width = R( 260 ),
    height = R( 132 )
}

function EndlessIntroPopupInit( parent )
    parent:InsertFromXml( "menu2/popup_endless_intro.xml" )
    popup = parent:GetControl( "EndlessIntroPopup" )

    x = ( SCREEN_WIDTH - popupFrame.width )/2
    y = ( SCREEN_HEIGHT - popupFrame.height )/2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    PopupCreateFrame( popup:GetControl( "Frame" ), popupFrame.width, popupFrame.height )
    star = BackgroundStar( popup, popupFrame.width/2, popupFrame.height/2, 1 )
end

function EndlessIntroPopupPress( control, path )
    if not active then
        return
    end

    if path == "/EndlessIntroPopup/Close" then
        ButtonPress( control )
    end
end

function EndlessIntroPopupAction( control, path )
    if not active then
        return
    end

    if path == "/EndlessIntroPopup/Close" then
        EndlessIntroPopupHide()
    end
end

function EndlessIntroPopupShow()
    Lock()

    PopupLock( 0.3, "/EndlessIntroPopup/Overlay" )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            active = true
            Unlock()
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
    BackgroundStarStart( star, 0.3 )
end

function EndlessIntroPopupHide()
    Lock()

    active = false
    PopupUnlock( 0.3, "/EndlessIntroPopup/Overlay" )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            Unlock()
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
    BackgroundStarStop( star, 0.3 )
end

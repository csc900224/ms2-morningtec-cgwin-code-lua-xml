function R(val)
    return val * GAME_SCALE
end

function Rinv(val)
    return val / GAME_SCALE
end

function X( x )
    return R(x * SCREEN_WIDTH / R(480))
end

function Y( y )
    return R(y * SCREEN_HEIGHT / R(320))
end

function RIGHT(val)
    return SCREEN_WIDTH + R(val)
end

function BOTTOM(val)
    return SCREEN_HEIGHT + R(val)
end

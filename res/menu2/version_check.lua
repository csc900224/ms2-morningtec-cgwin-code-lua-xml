require( "menu2/common.lua" )
require( "menu2/animation.lua" )
require( "menu2/delay.lua" )
require( "menu2/slider.lua" )
require( "menu2/perpetual.lua" )
require( "menu2/math.lua" )

require( "menu2/popup.lua" )
require( "menu2/popup_version.lua" )

function Screen:Update( dt )
    anim:Update( dt )
end

function Screen:OnKeyDown( code )
end

function Screen:OnTouchDown( x, y, id )
end

function Screen:OnTouchUp( x, y, id )
    local c = self:GetTouchableControl( x, y )
    if not c then
        return
    end

    local path = c:GetPath()
    VersionPopupAction(path)
end

function Screen:OnTouchMove( x, y, id )
end

VersionPopupInit( screen )

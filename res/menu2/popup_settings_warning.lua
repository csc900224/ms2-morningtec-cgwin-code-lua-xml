local active = false
local popup = nil

local x = 0
local y = 0

function SettingsQualityWarningInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_settings_warning.xml" )
    popup = screen:GetControl( "/SettingsQualityWarning" )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, QualityWarningW, QualityWarningH )

    x = ( SCREEN_WIDTH - QualityWarningW ) / 2
    y = ( SCREEN_HEIGHT - QualityWarningH ) / 2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
end

function SettingsQualityWarningPress()
    if active then
        SettingsQualityWarningHide()
    end
end

function SettingsQualityWarningShow()
    popup:SetVisibility( true )
    PopupLock( 0.3, "/SettingsQualityWarning/Overlay" )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            active = true
        end
        )
end

function SettingsQualityWarningHide()
    active = false
    PopupUnlock( 0.3, "/SettingsQualityWarning/Overlay" )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( 1-t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
        )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/inbox_item.lua" )
require( "menu2/requests_scroll_indicator.lua" )

local root = nil
local clip = nil
local width = nil
local height = nil
local items = nil
local currentCat = nil
local indicator = nil

RequestsConsts = {
    itemHeight = R(34),
    itemWidth = SCREEN_WIDTH - R(40),
    itemSpacing = R(2),
    itemYOffset = R(7) 
}

local RequestsScrollData = {
    touchId = -1,
    minScrollDist = R(5),
    maxScrollOffset = 0,
    scrollOffset = 0,
    sampleTouchPos = 0,
    currentTouchPos = 0,
    startScrollOffset = 0,
    touchStart = 0,
    scrolling = false,    
    targetScrollOffset = 0,
    scrollAcceleration = 1,
    samplingInterval = 0.025,
    sampleTimer = -1,
    velocityEpsilon = 0.1,
    velocity = 0,
    lastVelocity = 0,
    draggTime = 0
}

function RequestsListInit( parent, listWidth, listHeight )
    parent:InsertFromXml( "menu2/requests_list.xml" )
    clip = parent:GetControl( "RequestsList" )
    root = parent:GetControl( "RequestsList/Requests" )
    indicator = parent:GetControl( "RequestsScrollIndicatorRoot" )
    width = listWidth
    height = listHeight

    RequestsScrollIndicatorInit( indicator, 0, RequestsConsts.itemHeight )
    RequestsListCaluclateLayout()

    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    clip:GetGraphic( "r" ):SetWidth( width )
    clip:GetGraphic( "r" ):SetHeight( height )

end

function RequestsListCaluclateLayout()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    --RequestsConsts.itemHeight = math.min( height - iH, RequestsConsts.itemHeight)
    indicator:SetX( width - R( 5 ))
    indicator:SetY( (height - iH)/2 )
end

function RequestsListUpdateIndicatorPos()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    indicator:SetY( (height - iH)/2 )
end

function RequestsListSetContent( itemList, resetScroll )
    if resetScroll == nil or resetScroll == true then
        RequestsListResetScroll()
    end
    RequestsListDropAllItems()

    items = itemList
    local y = RequestsConsts.itemYOffset
    local idx = 1
    for _, item in ipairs( itemList ) do
        local itemRoot = root:InsertControl( tostring( idx ) )
        idx=idx+1

        itemRoot:SetRelative( true )
        itemRoot:SetY( y )
        InboxItemInit( itemRoot, RequestsConsts.itemWidth, RequestsConsts.itemHeight, item )
        y = y + RequestsConsts.itemHeight + RequestsConsts.itemSpacing
    end

    RequestsScrollData.maxScrollOffset = math.max( y - RequestsConsts.itemSpacing + RequestsConsts.itemYOffset - height, 0 )

    RequestsScrollIndicatorUpdateMaxScrollOffset( RequestsScrollData.maxScrollOffset )
    RequestsListUpdateIndicatorPos()

    RequestsListCreateClipBorder()
end

function RequestsListResetScroll()
    RequestsScrollData.scrollOffset = 0
    root:SetY( 0 )
    RequestsScrollData.scrolling = false
    RequestsScrollData.touchId = -1
    RequestsScrollData.velocity = 0
    RequestsScrollData.lastVelocity = 0
    RequestsScrollData.sampleTimer = -1
    RequestsScrollData.draggTime = 0
    RequestsScrollData.targetScrollOffset = 0
    RequestsScrollData.scrollOffset = 0
end

function RequestsListUpdate( dt )
    if RequestsScrollData.touchId >= 0 then
        RequestsScrollData.draggTime = RequestsScrollData.draggTime + dt
    end

    RequestsListSampleVelocity( dt )
    RequestsListUpdateScrolling( dt )

    RequestsScrollIndicatorUpdateScrollOffset( RequestsScrollData.scrollOffset )
end

function RequestsListSampleVelocity( dt )
    if RequestsScrollData.touchId >= 0 and RequestsScrollData.sampleTimer >= 0 then
        RequestsScrollData.sampleTimer = RequestsScrollData.sampleTimer - dt
        if RequestsScrollData.sampleTimer < 0 then
            RequestsScrollData.sampleTimer = RequestsScrollData.samplingInterval
            RequestsScrollData.lastVelocity = (RequestsScrollData.currentTouchPos - RequestsScrollData.sampleTouchPos) / RequestsScrollData.samplingInterval
            RequestsScrollData.sampleTouchPos = RequestsScrollData.currentTouchPos
        end
    end
end

function RequestsListTouchDown( x, y, id, c )
    if items == nil then return end

    local path = c:GetPath()
    if string.find( path, "/RequestsList" ) then

        if RequestsScrollData.touchId == id then
            RequestsListTouchUp( RequestsScrollData.currentTouchPos, y, id, nil )
        end

        if RequestsScrollData.touchId < 0 then
            RequestsScrollData.touchId = id
            RequestsScrollData.touchStart = y
            RequestsScrollData.scrolling = false
            RequestsScrollData.velocity = 0
            RequestsScrollData.lastVelocity = 0
            RequestsScrollData.sampleTimer = RequestsScrollData.samplingInterval
            RequestsScrollData.currentTouchPos = y
            RequestsScrollData.sampleTouchPos = y
            RequestsScrollData.draggTime = 0
        end
    end
end

function RequestsListTouchUp( x, y, id, c )
    if items == nil then return end

    if RequestsScrollData.scrolling and id == RequestsScrollData.touchId then
        RequestsScrollData.scrolling = false
        RequestsScrollData.touchId = -1
        RequestsScrollData.sampleTimer = -1
        RequestsScrollData.targetScrollOffset = RequestsItemClampOffset( RequestsScrollData.startScrollOffset + RequestsScrollData.touchStart - y )

        -- Check if dragg time is shorter than sampling rate. If so recalculate velocity manualny
        if RequestsScrollData.draggTime < RequestsScrollData.samplingInterval and RequestsScrollData.draggTime > 0 then
            RequestsScrollData.lastVelocity = (y - RequestsScrollData.sampleTouchPos) / RequestsScrollData.draggTime
        end
        RequestsScrollData.velocity = RequestsScrollData.lastVelocity
    else
        if not c then return end
        local path = c:GetPath()
        if string.find( path, "/Buttons" ) then
            ButtonPress( c )
            InboxItemAction( c,path )
        end
    end
end

function RequestsListTouchMove( x, y, id, c )
    if items == nil then return end

    if id == RequestsScrollData.touchId then
        if not RequestsScrollData.scrolling then
            if math.abs( RequestsScrollData.touchStart - y ) > RequestsScrollData.minScrollDist then
                -- Start real scrolling
                RequestsScrollData.touchStart = y
                RequestsScrollData.startScrollOffset = RequestsScrollData.scrollOffset
                RequestsScrollData.scrolling = true
            end
        else
            RequestsScrollData.targetScrollOffset = RequestsItemClampOffset( RequestsScrollData.startScrollOffset + RequestsScrollData.touchStart - y )
            RequestsScrollData.currentTouchPos = y
        end    
    end
end

function RequestsItemClampOffset( offset )
    return math.min( math.max(offset, 0), RequestsScrollData.maxScrollOffset )
end

function RequestsListUpdateScrolling( dt )
    if math.abs(RequestsScrollData.velocity) > RequestsScrollData.velocityEpsilon then
        RequestsScrollData.velocity = RequestsScrollData.velocity - RequestsScrollData.velocity * RequestsScrollData.scrollAcceleration * dt
        RequestsScrollData.targetScrollOffset = RequestsItemClampOffset( RequestsScrollData.targetScrollOffset - RequestsScrollData.velocity * dt )
    end

    if math.abs(RequestsScrollData.scrollOffset - RequestsScrollData.targetScrollOffset) > RequestsScrollData.velocityEpsilon then
        RequestsScrollData.scrollOffset = RequestsScrollData.targetScrollOffset
        root:SetY( -RequestsScrollData.scrollOffset )
    end  

    RequestsListClipItems()
end

function RequestsListClipItems()
    for _, item in ipairs( root:GetChildren() ) do
        local visible = -RequestsScrollData.scrollOffset + item:GetY() <= height and -RequestsScrollData.scrollOffset + item:GetY() + RequestsConsts.itemHeight >= 0
        item:SetVisibility( visible )
    end
end

function RequestsListDropAllItems()
    for _, item in ipairs( root:GetChildren() ) do
        item:Remove()
    end
end

function RequestsListHidePosIndicator()
    indicator:SetVisibility( false )
end

function RequestsListShowPosIndicator()
    indicator:SetVisibility( true )
end

function RequestsListCreateClipBorder()
    local clipMargin = R(20)

    local ls = clip:InsertControl("ls")
    ls:AddRepresentation( "default", "menu2/timage.xml" )
    ls:SetRelative( true )
    ls:GetGraphic( "TS" ):SetAngle( 1.57 )
    ls:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    local lsw, lsh = ls:GetGraphic( "TS" ):GetSize()

    ls:SetY( - R(2) )
    ls:SetX( width + clipMargin )

    ls:GetGraphic( "TS" ):SetScale( 1 , ( width + 2*clipMargin ) / lsh  )


    local rs = clip:InsertControl("rs")
    rs:AddRepresentation( "default", "menu2/timage.xml" )

    rs:SetRelative( true )
    rs:GetGraphic( "TS" ):SetAngle( - 1.57 )
    rs:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
    rs:SetX( -clipMargin )
    rs:SetY(  height  )
    rs:GetGraphic( "TS" ):SetScale( 1 , ( width + 2 * clipMargin ) / lsh )

end


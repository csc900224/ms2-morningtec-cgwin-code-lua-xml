require( "menu2/animation.lua" )

function ButtonPressPlaySound()
    AudioManager:Play( Sfx.SFX_MENU_SELECT )
end

function ButtonPress( control, animName, playSound )
    if playSound ~= false then
        ButtonPressPlaySound()
    end

    control:SetRepresentation( "pressed" )
    anim:Add(
        Delay:New( 0.2 ),
        function()
            control:SetRepresentation( "default" )
        end,
        animName
    )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )

local parent = nil
local root = nil

local Line = {
    lineFill = "menu2/line_01_fill.png@linear",
    lineEnd = "menu2/line_01_end.png@linear",
}

local rowFirst = nil
local rowSecond = nil
local rowThird = nil
local rowAny = nil

local currentTab = nil
LeaderboardTabs = { Friends = "Friends", Global = "Global" }

function SurvivalItemInit( parent, width, height, level, listControl )
    -- Insert from xml and get item root
    parent:InsertFromXml( "menu2/survival_item.xml" )

    parent = parent
    root = parent:GetControl( "SurvivalItem" )

    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )


    root:GetControl( "Play/Button"):InsertControl( level.Level )
    root:GetControl( "LeaderboardRoot/Tabs/TabFriends/Button"):InsertControl( level.Level )
    root:GetControl( "LeaderboardRoot/Tabs/TabGlobal/Button"):InsertControl( level.Level )	
    root:GetControl("SyncIndicator"):SetVisibility( false )

    anim:RemoveAll( "SurvivalItemSyncPopup".. level.Level )
    anim:RemoveAll( "SurvivalItemSyncBlink".. level.Level )

    SurvivalItemInitFillData( level , width, height )
    if ( IsLoggedIn() ) then
        CheckStats( level )
    end
end

function SurvivalItemSelectTab( newTab, levelIdx , Listroot , force )

    local itemRoot = Listroot

    if SurvivalLevelsInfo[levelIdx].TabActive ~= newTab or force == true then

        local tabs = itemRoot:GetControl( levelIdx.."/SurvivalItem/LeaderboardRoot/Tabs" ):GetChildren()
        for _,tab in ipairs( tabs ) do
            tab:GetChildren()[1]:SetRepresentation( "default" )
        end

        tab = itemRoot:GetControl(  levelIdx.."/SurvivalItem/LeaderboardRoot/Tabs/Tab" .. newTab .."/Button")

        tab:SetRepresentation("pressed")


        local friendBoard = itemRoot:GetControl( levelIdx.."/SurvivalItem/LeaderboardRoot/FriendLeaderboardList" )
        local globalBoard = itemRoot:GetControl( levelIdx.."/SurvivalItem/LeaderboardRoot/GlobalLeaderboardList" )
        local statsInfo = SurvivalLevelsInfo[levelIdx]
        local item = itemRoot:GetControl(  levelIdx.."/SurvivalItem" )

        if newTab == LeaderboardTabs.Friends then

            friendBoard:SetVisibility( true )
            globalBoard:SetVisibility( false )

            FillStats( statsInfo, item , false )

            if IsLoggedIn() then
                if UpdateInfo.LeaderboardsFriends == 0 then
                    friendBoard:GetControl( "Underline" ):SetVisibility( false )
                    if not ( registry:Get( "/monstaz/player/level" ) < SurvivalLevelsInfo[levelIdx].LevelRequred ) then
                        SurvivalItemSyncIndicatorShow( item , levelIdx )
                    end
                elseif UpdateInfo.LeaderboardsFriends < 0 then
                    if not ( registry:Get( "/monstaz/player/level" ) < SurvivalLevelsInfo[levelIdx].LevelRequred ) then
                        ShowTimeoutRoot( item )
                    end
                end
            end
        else
            
            friendBoard:SetVisibility( false )
            globalBoard:SetVisibility( true )
            FillStats( statsInfo, item , true )
            if IsLoggedIn() then
                if UpdateInfo.LeaderboardsFriends == 0 then
                    globalBoard:GetControl( "Underline" ):SetVisibility( false )
                    if not ( registry:Get( "/monstaz/player/level" ) < SurvivalLevelsInfo[levelIdx].LevelRequred ) then
                        SurvivalItemSyncIndicatorShow( item , levelIdx )
                    end
                elseif UpdateInfo.LeaderboardsFriends < 0 then
                    if not ( registry:Get( "/monstaz/player/level" ) < SurvivalLevelsInfo[levelIdx].LevelRequred ) then
                        ShowTimeoutRoot( item )
                    end
                end
            end
        end

        SurvivalLevelsInfo[levelIdx].TabActive = newTab
    end
end

function FillStats( level , item , isGlobal )

    local prefix = "GlobalLeaderboardList"

    if isGlobal == false then 
        prefix = "FriendLeaderboardList" 
    end


    rowFirst = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row1" )
    rowSecond = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row2" )
    rowThird = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row3" )
    rowPlayer = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row4" )
    local underline = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Underline" )

    local rows = { rowFirst , rowSecond , rowThird , rowPlayer }

    if not IsLoggedIn() then
        for i=1,# rows do 
            rows[i]:SetVisibility( false )
        end
        rows[1]:SetVisibility( true )
        FillRow( rows[1] , StatsLocalPlayer[ level.Level ] , underline )
    else
        for i=1,# rows do 
            local stat = level.StatsGlobal[i]
            if isGlobal == false then stat = level.StatsFriends[i] end
            FillRow( rows[i] , stat , underline )
        end
    end
end

function FillRow( rowRoot , stats , underline )

    if stats == nil then 
        rowRoot:SetVisibility( false ) 
        return 
    end
    rowRoot:SetVisibility( true ) 
    rowRoot:GetControl( "Place" ):SetText( "#" ..  stats.Place )
    rowRoot:GetControl( "Place" ):SetX( - R(10) )
    rowRoot:GetControl( "Score"):SetText( stats.Score )
    rowRoot:GetControl( "Name" ):SetText( stats.Name )

    if stats.Name ~= "TEXT_YOU" then
        rowRoot:GetControl( "Name" ):SetVisibility( false )
    end

    local nameImg = rowRoot:GetControl( "NameImage" );
    callback:ResistanceSetUsernameImg( nameImg:GetPath() , stats.Name  )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 

    if ( stats.Name == "TEXT_YOU" or ( User.Name and stats.Name == User.Name )) then
        underline:SetVisibility( true )
        underline:SetY( rowRoot:GetControl("Place"):GetY() )
    end

    underline:SetX( rowRoot:GetControl("NameImage"):GetX()-R(3))
end

function SurvivalItemInitFillData( level, width, height )

    local currX = R (15)
    local currY = R (15)

    local levelHeaderText =  root:GetControl( "LevelHeader" )
    levelHeaderText:SetText( level.LevelTitle )
    levelHeaderText:SetX( currX )
    levelHeaderText:SetY( currY )

    local icon = root:GetControl( "LevelHeader/LevelIcon/Image" )
    icon:GetGraphic( "TS" ):SetImage( level.LevelIcon )

    icon:GetGraphic( "TS" ):SetPivot(0,0)
    local widthIcon, heightIcon = icon:GetGraphic( "TS" ):GetSize()

    icon:SetY(R(27))
    CreateSurvivalLine( icon , widthIcon , 0 )
    CreateSurvivalLine( icon , widthIcon , heightIcon )

    --check if level is avaible 
    local requiredText =  root:GetControl( "NeedLevelUp/MessageLevelRequred" )
    local buttonPlay = root:GetControl( "Play")
    local buttonLogin = root:GetControl( "Login")
    
    local currentUserLevel = registry:Get( "/monstaz/player/level" )
    local needLevelText = root:GetControl( "NeedLevelUp" )
    local redHighlight = root:GetControl( "NeedLevelUp/RedBlow" )
    local wBlow,hBlow = redHighlight:GetGraphic( "TS" ):GetSize()

    --redHighlight:GetGraphic( "TS" ):SetPivot( wBlow/2,hBlow/2 )

    --position of play button
    local buttonImg = root:GetControl( "Play/Button")
    local w, h = buttonImg:GetGraphic( "TS" ):GetSize()

    local timeoutRoot = root:GetControl( "TimeoutRoot" )
    timeoutRoot:SetX( icon:GetX() + widthIcon + R(50))
    timeoutRoot:SetY(icon:GetY() + heightIcon/4 )
    timeoutRoot:SetVisibility( false )

    root:GetControl( "SyncIndicator" ):SetX( 2 * width / 3 )
    root:GetControl( "SyncIndicator" ):SetY( icon:GetY() + heightIcon/4 )

    local dot1 = root:GetControl( "SyncIndicator/SyncIndicatorDot1" )
    local dot2 = root:GetControl( "SyncIndicator/SyncIndicatorDot2" )
    local dot3 = root:GetControl( "SyncIndicator/SyncIndicatorDot3" )

    local wD, hD = dot1:GetGraphic( "TS" ):GetSize()
    local margin = R ( 5 )
    dot1:SetX( - margin - wD - wD/2)
    dot2:SetX( -wD/2 )
    dot3:SetX( margin + wD - wD/2)

    if currentUserLevel < level.LevelRequred then
        timeoutRoot:SetVisibility( false )
        requiredText:SetTextWithVariables( "TEXT_SURVIVAL_NEED_LEVEL_TO_UNLOCK", { VALUE = tostring( level.LevelRequred ) } )

        needLevelText:SetX( icon:GetX() + widthIcon + R(50))
        needLevelText:SetY( icon:GetY() + heightIcon/2 - R(15) )
        needLevelText:SetVisibility( true )

        redHighlight:SetX( requiredText:GetX() + requiredText:GetWidth()/2 - wBlow/2 )
        redHighlight:SetY( requiredText:GetY() + R(13) - hBlow/2 )

        buttonPlay:SetVisibility( false )
        buttonLogin:SetVisibility( false )

        root:GetControl( "LeaderboardRoot" ):SetVisibility( false )
    else
        buttonPlay:SetVisibility( true )
        needLevelText:SetVisibility( false )

        root:GetControl( "LeaderboardRoot" ):SetVisibility( true )

        if not IsLoggedIn() then
            buttonLogin:SetVisibility( true )
            buttonLogin:SetX( 2 * width / 3 - w)
            buttonLogin:SetY( icon:GetY() + heightIcon - h/3 )
            buttonPlay:SetX( 2 * width / 3 + R(7))
            buttonPlay:SetY( icon:GetY() + heightIcon - h/3 )
        else
            buttonLogin:SetVisibility( false )
            buttonPlay:SetX( 2 * width / 3 - w/2)
            buttonPlay:SetY( icon:GetY() + heightIcon - h/3 )
        end
    end
end

function CreateSurvivalLine( root , width , height )

    local xMargin = R(6)

    local lb = root:InsertControl("lb")
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( Line.lineEnd )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )

    lbw, lbh = lb:GetGraphic( "TS" ):GetSize()
    rbw, rbh = lb:GetGraphic( "TS" ):GetSize()

    local y =  height - lbh/2
    lb:SetY( y )
    lb:SetX( - xMargin )

    local bottom = root:InsertControl("bottom")
    bottom:AddRepresentation( "default", "menu2/tile.xml" )
    bottom:SetRelative( true )

    bottom:SetX( - xMargin + lbw )
    bottom:SetY( y )

    bottom:GetGraphic( "s" ):SetImage( Line.lineFill )
    bottom:GetGraphic( "s" ):SetClipRect( 0, 0, width - xMargin   , lbh )

    local rb = root:InsertControl("rb")
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( Line.lineEnd )
    rb:SetY( y )
    rb:SetX( -xMargin + lbw + ( width -  xMargin ) )
end

function SurvivalItemAction( c, path )
    if string.find( path, "SurvivalItem/Play/Button" ) then
        local levelId = tonumber( c:GetChildren()[1]:GetName() )
        registry:Set( "/internal/survivalLevel", levelId )

        registry:Set( "/internal/map", -1 )
        registry:Set( "/internal/area", -1 )
        registry:Set( "/internal/boss", false )

        registry:Set( "/internal/reward/soft", 0 )
        registry:Set( "/internal/reward/hard", 0 )
        registry:Set( "/internal/reward/xp", 0 )

        local level = SurvivalLevels[levelId]
        for i = 1,#level.m do
            registry:Set( "/internal/monsters/" .. i, level.m[i] )
            registry:Set( "/internal/elements/" .. i, level.e[i] or 0 )
        end
        registry:Set( "/internal/monsters/" .. #level.m + 1, -1 )

        PremissionSetup( level, true )
        PremissionShow()

        SurvivalHide( function() PopupUnlock() end )
    elseif string.find( path, "SurvivalItem/Login/Button" ) then
        SurvivalHide( function() LoginMethodPopupShow() end )
    end
end

function SurvivalItemPress( control, path )
    if string.find( path, "SurvivalItem/Play/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "SurvivalItem/Login/Button" ) then
        ButtonPress( control )
    elseif string.find( path, "SurvivalItem/TimeoutRoot/Refresh/Button" ) then
        ButtonPress( control )
    end
end

function SurvivalItemSyncIndicatorShow( item, idx )

    item:GetControl( "TimeoutRoot" ):SetVisibility( false )
    HideAllLeaderboardRows( item )

    local indicator = item:GetControl("SyncIndicator")

    if indicator:GetVisibility() == true then return end

    indicator:SetVisibility( true )

    local startAlpha = indicator:GetAlpha()

    anim:RemoveAll( "SurvivalItemSyncPopup" .. idx )

    anim:Add( Slider:New( 0.3,
        function( t )
            indicator:SetAlpha( startAlpha + SmoothStep( t ) * (255-startAlpha) )
        end
        ),
        nil,
        "SurvivalItemSyncPopup".. idx
    )

    anim:RemoveAll( "SurvivalItemSyncBlink" .. idx )

    local dot1 = indicator:GetControl( "SyncIndicatorDot1" )
    local dot2 = indicator:GetControl( "SyncIndicatorDot2" )
    local dot3 = indicator:GetControl( "SyncIndicatorDot3" )


    local lt = 0
    local timer = 0
    local speed = 5
    anim:Add( Perpetual:New(
        function( t )
            local dt = t - lt
            lt = t
            if ( timer > 3) then 
                timer = 0
            else
                timer = timer + speed*dt
            end

            if timer <= 1 then
                dot1:SetRepresentation( "active" )
            else
                dot1:SetRepresentation( "default" )

            end

            if timer >= 1 and timer <= 2 then
                dot2:SetRepresentation( "active" )
            else
                dot2:SetRepresentation( "default" )
            end

            if timer >= 2 and timer <= 3 then
                dot3:SetRepresentation( "active" )
            else
                dot3:SetRepresentation( "default" )
            end
         end
         ),
         nil,
         "SurvivalItemSyncBlink".. idx
     )
end

function SurvivalItemSyncIndicatorHide( item, idx , callback )
    local indicator  = item:GetControl("SyncIndicator")
    if indicator:GetVisibility() == false then return end
    indicator:SetVisibility( true )

    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "SurvivalItemSyncPopup".. idx )
    anim:RemoveAll( "SurvivalItemSyncBlink".. idx )
    anim:Add( Slider:New( (0.2) * startAlpha/255,
        function( t )
            indicator:SetAlpha( startAlpha - SmoothStep( t ) * startAlpha )
        end
        ),
        function()
            indicator:SetVisibility( false )
            if callback then callback() end
        end,
        "SurvivalItemSyncPopup".. idx
    )
end

function SurvivalItemUpdate( item , idx )

   local indicator = item:GetControl("SyncIndicator")

   if indicator and indicator:GetVisibility() == true then

        if UpdateInfo.LeaderboardsFriends > 0 then

            if ( IsLoggedIn() ) then
                CheckStats( SurvivalLevelsInfo[idx] )
            end

            SurvivalItemSyncIndicatorHide( item , idx , function() SurvivalItemSelectTab( LeaderboardTabs.Friends , idx , item:GetControl( ".." ):GetControl( ".." ), true ) end )
 
        elseif UpdateInfo.LeaderboardsFriends < 0 then
            if not ( registry:Get( "/monstaz/player/level" ) < SurvivalLevelsInfo[idx].LevelRequred ) then
               SurvivalItemSyncIndicatorHide( item , idx , function() ShowTimeoutRoot( item ) end )
            end
        end
    end
end

function ShowTimeoutRoot( item )
    HideAllLeaderboardRows( item )
    item:GetControl( "TimeoutRoot" ):SetVisibility( true )
end

function HideAllLeaderboardRows( item )

    local prefix = "GlobalLeaderboardList"

    rowFirst = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row1" )
    rowSecond = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row2" )
    rowThird = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row3" )
    rowPlayer = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row4" )
    local underline = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Underline" )
    underline:SetVisibility( false )

    local rows = { rowFirst , rowSecond , rowThird , rowPlayer }

    for i=1,# rows do 
        rows[i]:SetVisibility( false )
    end

    prefix = "FriendLeaderboardList" 

    rowFirst = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row1" )
    rowSecond = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row2" )
    rowThird = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row3" )
    rowPlayer = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Row4" )
    underline = item:GetControl( "LeaderboardRoot/" .. prefix .. "/Underline" )
    underline:SetVisibility( false )

    local rows = { rowFirst , rowSecond , rowThird , rowPlayer }

    for i=1,# rows do 
        rows[i]:SetVisibility( false )
    end
end
require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/perpetual.lua" )

function StartStarAnimation( starControl )
    local star = starControl:GetGraphic( "TS" )
    anim:Add( Perpetual:New(
        function( t )
            star:SetAngle( t * 0.2 )
            star:SetScale( 0.95 + math.sin( 2.5 * t ) * 0.05 )
        end
        ), nil, starControl:GetPath() .. "/" .. "StarAnim" )
end

function StopStarAnimation( starControl )
    anim:RemoveAll( starControl:GetPath() .. "/" .. "StarAnim" )
end
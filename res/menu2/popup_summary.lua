require( "LevelDB.lua" )
require( "SurvivalDB.lua" )
require( "PlayerLevels.lua" )
require( "menu2/animation.lua" )
require( "menu2/avatar.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/delay.lua" )
require( "menu2/math.lua" )
require( "menu2/popup.lua" )
require( "menu2/slider.lua" )

local active = false

local root = nil
local popup = nil
local menu = nil
local levelFailed = false

local suggestedWeapons = {}

local x = 0
local y = 0
local width = R( 330 )
local height = R( 190 )

function SummaryPopupInit( parent )
    parent:InsertFromXml( "menu2/popup_summary.xml" )

    root = parent:GetControl( "Summary" )
    popup = root:GetControl( "Popup" )
    menu = root:GetControl( "Menu" )

    x = math.floor( R(16) + R(116) + ( SCREEN_WIDTH - R(480) )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( SCREEN_WIDTH )
    popup:SetY( y )

    menu:SetX( x )
    menu:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    local item = popup:GetControl( "SuggestedItems/Anim/Item" )
    local t = 0
    local last = 0
    anim:Add( Perpetual:New(
        function( tt )
            t = t + ( tt - last ) * 5
            last = tt
            while t > math.pi do t = t - math.pi end
            item:SetAlpha( math.sin( t ) * 255 )
            item:SetX( 44 * R( math.sin( t * 0.5 ) ) )
        end
        ) )
end

function SummaryPopupGetRoot()
    return root
end

function SummaryPopupSetup( level, time, kills, multi, score, fail )
    registry:Set( "/internal/levelcompleted", not fail )
    levelFailed = fail

    local survival = registry:Get( "/internal/survival" )
    if survival then
        local survivalLevelIdx = registry:Get( "/internal/survivalLevel" )
        popup:GetControl( "Header/Title" ):SetText( SurvivalLevels[survivalLevelIdx].n )

        popup:GetControl( "LevelCompleted" ):SetVisibility( true )

        local highscore = registry:Get( "/monstaz/hiscore/" .. survivalLevelIdx .. "/score" )
        if score > highscore then
            local label = popup:GetControl( "LevelCompleted/Label" )
            label:SetText( "TEXT_SURVIVAL_NEW_HIGHSCORE" )

            anim:Add(
                Perpetual:New(
                    function( t )
                        label:SetAlpha( ( 1 - math.sin( t*6 ) )*128 )
                    end
                )
            )

            registry:Set( "/monstaz/hiscore/" .. survivalLevelIdx .. "/score", score )
            GameEventDispatcher:HandleGameEvent( GameEvent.GEI_GAME_HISCORE_CHANGED, survivalLevelIdx )
            RefreshLocalStats()
        else
            popup:GetControl( "LevelCompleted/Label" ):SetText( "TEXT_SURVIVAL_LEVEL_COMPLETED" )
        end

        popup:GetControl( "Stats/Score" ):SetVisibility( true )
        popup:GetControl( "Stats/Score/Value" ):SetText( score )

        local collected = registry:Get( "/internal/money" )
        SummaryPopupSetupStats( kills, time, collected )
        local restartButton = menu:GetControl( "Restart" )
        restartButton:SetVisibility( false )

        local shopButton = menu:GetControl( "Shop" )
        shopButton:SetX( restartButton:GetX() )
        shopButton:SetY( restartButton:GetY() )

        menu:GetControl( "Back" ):SetVisibility( true )
        SummaryPopupShowMenu()
    else
        popup:GetControl( "Header/Title" ):SetText( registry:Get( "/internal/levelname" ) )
        if fail then
            popup:GetControl( "LevelFailed" ):SetVisibility( true )
            popup:GetControl( "SuggestedItems" ):SetVisibility( true )

            local item1 = popup:GetControl( "SuggestedItems/Item01" )
            local item2 = popup:GetControl( "SuggestedItems/Item02" )

            local items = SummaryGetSuggestedItems( 2 )
            item1:GetControl( "Label" ):SetText( ItemsById[items[1]].d )
            item1:GetControl( "Image" ):GetGraphic( "s" ):SetImage( ItemsById[items[1]].f )
            item2:GetControl( "Label" ):SetText( ItemsById[items[2]].d )
            item2:GetControl( "Image" ):GetGraphic( "s" ):SetImage( ItemsById[items[2]].f )

            suggestedWeapons = items

            if registry:Get( "/internal/friend" ) then
                local restartButton = menu:GetControl( "Restart" )
                restartButton:SetVisibility( false )

                local shopButton = menu:GetControl( "Shop" )
                shopButton:SetX( restartButton:GetX() )
                shopButton:SetY( restartButton:GetY() )
            elseif registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" ) then
                local restartButton = menu:GetControl( "Restart" )
                local fuel = registry:Get( "/monstaz/cash/fuel" )
                local cost = registry:Get( "/internal/boss" ) and registry:Get( "/app-config/fuel/cost/boss" ) or registry:Get( "/app-config/fuel/cost/normal" )
                if fuel < cost then
                    restartButton:SetVisibility( false )

                    local shopButton = menu:GetControl( "Shop" )
                    shopButton:SetX( restartButton:GetX() )
                    shopButton:SetY( restartButton:GetY() )
                else
                    restartButton:GetControl( "Text" ):SetText( TextDict:Get( "TEXT_SUMMARY_RESTART" ) .. " -" .. cost .. "\194\157" )
                end
            end

            local bg1 = item1:GetControl( "Bg" )
            local bg2 = item2:GetControl( "Bg" )
            anim:Add(
                Perpetual:New(
                    function( t )
                        bg1:GetGraphic( "TS" ):SetAngle( t )
                        bg2:GetGraphic( "TS" ):SetAngle( t )

                        bg1:GetGraphic( "TS" ):SetScale( 1 + math.sin( t )*0.1 )
                        bg2:GetGraphic( "TS" ):SetScale( 1 + math.cos( t )*0.1 )
                    end
                )
            )

            menu:GetControl( "Back" ):SetVisibility( true )
            SummaryPopupShowMenu()
        else
            popup:GetControl( "LevelCompleted" ):SetVisibility( true )

            local restartButton = menu:GetControl( "Restart" )
            restartButton:SetVisibility( false )

            local shopButton = menu:GetControl( "Shop" )
            shopButton:SetX( restartButton:GetX() )
            shopButton:SetY( restartButton:GetY() )

            local map = registry:Get( "/internal/map" )
            local area = registry:Get( "/internal/area" )

            local completed = registry:Get( "/maps/" .. map .. "/" .. area .. "/completed" )
            registry:Set( "/maps/" .. map .. "/" .. area .. "/completed", completed + 1 )

            if not registry:Get( "/internal/boss" ) then
                local count = registry:Get( "/maps/" .. map .. "/completed-missions-count" ) or 0
                registry:Set( "/maps/" .. map .. "/completed-missions-count", count + 1 )
            end

            if registry:Get( "/internal/endless" ) then
                if registry:Get( "/internal/boss" ) then
                    registry:Set( "/maps/" .. map .. "/boss/progress", 0 )
                else
                    local progress = registry:Get( "/maps/" .. map .. "/boss/progress" )
                    registry:Set( "/maps/" .. map .. "/boss/progress", progress + 1 )
                end
            end

            if registry:Get( "/maps/" .. map .. "/" .. area .. "/level" ) then
                registry:Set( "/maps/" .. map .. "/" .. area .. "/level", -1 )
            end

            if registry:Get( "/maps/" .. map .. "/" .. area .. "/infection-time" ) then
                registry:Set( "/maps/" .. map .. "/" .. area .. "/infection-time", 0 )
            end

            local soft, h = Shop:GetCash()
            local reward = registry:Get( "/internal/reward/soft" ) or 0
            Shop:SetCash( soft + reward, h )

            local collected = registry:Get( "/internal/money" )
            SummaryPopupSetupStats( kills, time, collected + reward )

            menu:GetControl( "Continue" ):SetVisibility( true )

            SummaryPopupSetupXpBar()
        end
    end
end

function SummaryPopupSetupStats( kills, time, cash )
    local stats = popup:GetControl( "Stats" )
    stats:SetVisibility( true )

    stats:GetControl( "Cash/Value" ):SetText( cash )
    stats:GetControl( "Kills/Value" ):SetText( kills )

    local minutes = math.floor( time / 60 )
    local seconds = time - minutes * 60
    stats:GetControl( "Time/Value" ):SetText( string.format( "%02i:%02i", minutes, seconds ) )
end

function SummaryGetSuggestedItems( count )
    local playerLevel = registry:Get( "/monstaz/player/level" )

    local suggestedItems = {}

    local bestOwnedWeaponId = ItemDbGetBestOwnedWeaponId()
    local bestOwnedWeaponLevel = ItemDbGetItemLevel( bestOwnedWeaponId )

    local weapons = ItemsCategory[ItemCategory.weapons]

    for i, weapon in ipairs( weapons ) do
        if Shop:IsBought( weapon.id ) == 0 then
            local weaponLevel = ItemDbGetItemLevel( weapon.id )
            if weaponLevel > bestOwnedWeaponLevel and weaponLevel <= playerLevel then
                table.insert( suggestedItems, weapon.id )
                if #suggestedItems == count then
                    return suggestedItems
                end
            end
        end
    end

    for i, weapon in ipairs( weapons ) do
        if Shop:IsBought( weapon.id ) == 0 then
            local weaponLevel = ItemDbGetItemLevel( weapon.id )
            if weaponLevel > bestOwnedWeaponLevel and weaponLevel > playerLevel then
                if ItemUnlockLevelRange == -1 or weaponLevel - playerLevel <= ItemUnlockLevelRange then
                    table.insert( suggestedItems, weapon.id )
                    if #suggestedItems == count then
                        return suggestedItems
                    end
                end
            end
        end
    end

    local items = ItemsCategory[ItemCategory.items]

    for i, item in ipairs( items ) do
        if Shop:IsBought( item.id ) < item.max then
            table.insert( suggestedItems, item.id )
            if #suggestedItems == count then
                return suggestedItems
            end
        end
    end

    for i, item in ipairs( items ) do
        table.insert( suggestedItems, item.id )
        if #suggestedItems == count then
            return suggestedItems
        end
    end

    return suggestedItems
end

function SummaryPopupSetupXpBar()
    popup:GetControl( "Stats/XpBar" ):SetVisibility( true )

    local level = registry:Get( "/monstaz/player/level" )
    local xp = registry:Get( "/monstaz/player/xp" )

    local prevLevelXp = PlayerLevels[level].xp
    local nextLevelXp = PlayerLevels[level + 1].xp

    local width = popup:GetControl( "Stats/XpBar/Bg" ):GetGraphic( "r" ):GetWidth()
    local scale = width/( nextLevelXp - prevLevelXp )

    local orangeBar = popup:GetControl( "Stats/XpBar/Orange" ):GetGraphic( "r" )
    orangeBar:SetWidth( ( xp - prevLevelXp )*scale )

    local reward = registry:Get( "/internal/reward/xp" ) or 0
    registry:Set( "/monstaz/player/xp", xp + reward )

    local levelup = xp + reward >= nextLevelXp
    if levelup then
        registry:Set( "/monstaz/player/level", level + 1 )
    else
        SummaryPopupShowMenu()
    end

    anim:Add( Delay:New( 0.25 ),
        function()
            local offset = reward
            if levelup then
                offset = nextLevelXp - xp
            end

            local yellowBar = popup:GetControl( "Stats/XpBar/Yellow" ):GetGraphic( "r" )
            anim:Add( Slider:New( offset*0.02,
                function( t )
                    yellowBar:SetWidth( orangeBar:GetWidth() + SmoothStep( t )*offset*scale )
                end
                ),
                function()
                    if levelup then
                        LevelupPopupSetCloseCallback(
                            function()
                                AvatarRefresh()

                                orangeBar:SetWidth( 0 )
                                yellowBar:SetWidth( 0 )

                                offset = reward - offset
                                if offset > 0 then
                                    anim:Add( Slider:New( offset*0.03,
                                        function( t )
                                            yellowBar:SetWidth( SmoothStep( t )*offset*scale )
                                        end
                                        )
                                    )
                                end

                                SummaryPopupShowMenu()
                            end
                        )

                        LevelupPopupSetup()
                        LevelupPopupShow()

                        TutorialOnLevelupPopup()
                    end
                end
            )
        end
    )
end

function SummaryKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        callback:StopSummary()
        return true
    end

    return false
end

function SummaryPopupPress( control, path )
    if not active then
        return
    end

    if path == "/Summary/Menu/Shop" then
        ButtonPress( control )
    elseif path == "/Summary/Menu/Restart" then
        ButtonPress( control )
    elseif path == "/Summary/Menu/Back" then
        ButtonPress( control )
    elseif path == "/Summary/Menu/Continue" then
        ButtonPress( control )
    end
end

function SummaryPopupAction( path )
    if not active then
        return
    end

    if path == "/Summary/Menu/Restart" then
        if registry:Get( "/app-config/fuel/enabled" ) and not registry:Get( "/monstaz/subscription" ) then
            local fuel = registry:Get( "/monstaz/cash/fuel" )
            local cost = registry:Get( "/internal/boss" ) and registry:Get( "/app-config/fuel/cost/boss" ) or registry:Get( "/app-config/fuel/cost/normal" )
            registry:Set( "/monstaz/cash/fuel", fuel - cost )
        end
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SUMMARY_ACTION, GameEventParam.GEP_SUMARY_RESTART );
        callback:RestartLevel()
    elseif path == "/Summary/Menu/Back" or path == "/Summary/Menu/Continue" then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SUMMARY_ACTION, GameEventParam.GEP_SUMARY_BACK );
        TutorialAction( path )
        callback:StopSummary()
    elseif path == "/Summary/Menu/Shop" then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SUMMARY_ACTION, GameEventParam.GEP_SUMARY_SHOP );
        callback:GoToShop()
    elseif string.find( path, "/Summary/Popup/SuggestedItems/Item01" ) then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SUMMARY_ACTION, GameEventParam.GEP_SUMARY_ITEM_1 );
        registry:Set( "/internal/shop-startup-tab", ShopTab.Weapons )
        registry:Set( "/internal/shop-startup-item", suggestedWeapons[1] )
        callback:GoToShop()
    elseif string.find( path, "/Summary/Popup/SuggestedItems/Item02" ) then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_SUMMARY_ACTION, GameEventParam.GEP_SUMARY_ITEM_2 );
        registry:Set( "/internal/shop-startup-tab", ShopTab.Weapons )
        registry:Set( "/internal/shop-startup-item", suggestedWeapons[2] )
        callback:GoToShop()
    else
        return
    end

    active = false
end

function SummaryPopupShow()
    active = true
    PopupLock()
    callback:Playhaven( levelFailed and "level_failed" or "level_completed" )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetX( SCREEN_WIDTH - SmoothStep( t ) * ( SCREEN_WIDTH - x ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function SummaryPopupShowMenu()
    menu:SetVisibility( true )
    anim:Add(
        Slider:New( 0.5,
            function( t )
                menu:SetAlpha( SmoothStep( t ) * 255 )
                menu:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - height - y ) )
            end
        )
    )
end

function SummaryPopupHide()
    active = false
    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetX( x + SmoothStep( t ) * ( SCREEN_WIDTH - x ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            menu:SetVisibility( false )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

Slider = {}

function Slider:New( time, setter )
    local obj = { t = 0, s = 1/time, f = setter }
    setmetatable( obj, { __index = Slider } )
    return obj
end

function Slider:Play( dt )
    self.t = math.min( self.t + dt * self.s, 1 )
    self.f( self.t )
end

function Slider:IsFinished()
    return self.t >= 1
end

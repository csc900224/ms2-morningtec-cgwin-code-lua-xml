function BackgroundStar( item, width, height, after )
    item:InsertFromXml( "menu2/background_star.xml", after or 0 )

    local star = item:GetControl( "BgStar" )
    star:SetX( width )
    star:SetY( height )

    return star
end

local function StartRotation( star )
    local tab = {}
    for i=1,4 do
        table.insert( tab, star:GetControl( tostring( i ) ):GetGraphic( "TS" ) )
    end

    anim:Add( Perpetual:New(
        function( t )
            local a = t * -0.3
            local s = 1.25 + math.sin( t * 2 ) * 0.25
            for i=1,4 do
                tab[i]:SetAngle( a + math.pi * 0.5 * (i-1) )
                tab[i]:SetScale( s )
            end
        end
        ),
        nil,
        "BackgroundStarRotationAnim" )
end

local function FireStar( star )
    anim:Add( Delay:New( 0.05 ),
        function()
            FireStar( star )
        end,
        "BackgroundStarAnim"
        )

    star:InsertFromXml( "menu2/background_star_star.xml", 1 )
    local item = star:GetControl( "Star" )
    local dx, dy = 0, R(1)
    dx, dy = Rotate( dx, dy, math.random() * math.pi * 2 )
    local scale = math.random() + 0.5
    item:GetGraphic( "TS" ):SetScale( scale )
    local last = 0
    local time = 3
    anim:Add( Slider:New( time,
        function( t )
            local dt = ( t - last ) * time
            last = t
            dt = dt * 200 / scale
            item:ShiftPos( dx * dt, dy * dt )
            item:SetAlpha( math.cos( t * math.pi * 0.5 ) * 255 )
        end
        ),
        function()
            item:Remove()
        end
        )
end

function BackgroundStarStart( star, time )
    anim:Add( Slider:New( time,
        function( t )
            star:SetAlpha( 255 * SmoothStep( t ) )
        end
        ) )

    FireStar( star )
    StartRotation( star )
end

function BackgroundStarStop( star, time )
    anim:Add( Slider:New( time,
        function( t )
            star:SetAlpha( 255 * SmoothStep( 1-t ) )
        end
        ) )

    anim:RemoveAll( "BackgroundStarAnim" )
    anim:RemoveAll( "BackgroundStarRotationAnim" )
end

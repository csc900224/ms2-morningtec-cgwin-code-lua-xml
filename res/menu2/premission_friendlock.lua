local active = false
local popup = nil

local x = 0
local y = 0

function FriendLockInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/premission_friendlock.xml" )
    popup = screen:GetControl( "/FriendLockPopup" )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, FriendLockPopupW, FriendLockPopupH )

    local avatar = popup:GetControl( "Avatar/Img" )
    anim:Add( Perpetual:New(
        function( t )
            avatar:SetAlpha( ( 0.75 + 0.125 * ( 1 + math.sin( t * 4 ) ) ) * 255 )
        end
        ) )

    x = ( SCREEN_WIDTH - FriendLockPopupW ) / 2
    y = ( SCREEN_HEIGHT - FriendLockPopupH ) / 2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
end

function FriendLockPress( c, path )
    if active then
        if path == "/FriendLockPopup/Close" then
            ButtonPress( c )
        end
    end
end

function FriendLockKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        FriendLockHide()
        return true
    end

    return false
end

function FriendLockAction( path )
    if active then
        if path == "/FriendLockPopup/Close" then
            FriendLockHide()
            if not IsLoggedIn() then
                BackupRequirePopupShow()
            else
                PremissionHide( BackupTeamShow() , false )
            end
        end
    end
end

function FriendLockShow()
    Lock()
    popup:SetVisibility( true )
    PopupLock( 0.3, "/FriendLockPopup/Overlay" )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            active = true
            Unlock()
        end
        )
end

function FriendLockHide()
    Lock()
    active = false
    PopupUnlock( 0.3, "/FriendLockPopup/Overlay" )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( 1-t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            Unlock()
        end
        )
end

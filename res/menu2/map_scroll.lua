require( "menu2/animation.lua" )
require( "menu2/common.lua" )

MapScroll = {
    clip = nil,
    areas = nil,
    width = nil,
    height = nil,

    ScrollData = {
        touchId = -1,
        minScrollDist = R(5),
        maxScrollOffset = { x = 0, y = 0 },
        scrollOffset = { x = 0, y = 0 },
        sampleTouchPos = { x = 0, y = 0 },
        currentTouchPos = { x = 0, y = 0 },
        startScrollOffset = { x = 0, y = 0 },
        touchStart = { x = 0, y = 0 },
        scrolling = false,
        targetScrollOffset = { x = 0, y = 0 },
        scrollAcceleration = 5,
        samplingInterval = 0.025,
        sampleTimer = -1,
        velocityEpsilon = 0.1,
        velocity = { x = 0, y = 0 },
        lastVelocity = { x = 0, y = 0 },
        dragTime = 0
    }
}

function MapScroll:new( o )
    o = o or {}
    o.ScrollData = {}

    setmetatable( o, self )
    setmetatable( o.ScrollData, self.ScrollData )

    self.__index = self
    self.ScrollData.__index = self.ScrollData

    return o
end

function MapScroll:Init( root, width, height, x, y )
    self.clip = root
    self.areas = root:GetControl( "Areas" )
    self.width = width
    self.height = height

    self.clip:GetGraphic( "r" ):SetWidth( width )
    self.clip:GetGraphic( "r" ):SetHeight( height )

    self:ResetScroll( x, y )

    local i = 1
    while true do
        local area = self.areas:GetControl( string.format( "Area-%02d", i ) )
        if not area then
            break
        else
            self.ScrollData.maxScrollOffset.x = math.max( area:GetX() + area:GetWidth() - self.width, 0 )
            self.ScrollData.maxScrollOffset.y = math.max( area:GetY() + area:GetHeight() - self.height, 0 )
            i = i + 1
        end
    end
end

function MapScroll:ResetScroll( x, y )
    self.areas:SetX( -x )
    self.areas:SetY( -y )

    self.ScrollData.scrollOffset.x = x
    self.ScrollData.scrollOffset.y = y
    self.ScrollData.scrolling = false
    self.ScrollData.touchId = -1
    self.ScrollData.velocity.x = 0
    self.ScrollData.velocity.y = 0
    self.ScrollData.lastVelocity.x = 0
    self.ScrollData.lastVelocity.y = 0
    self.ScrollData.sampleTimer = -1
    self.ScrollData.dragTime = 0
    self.ScrollData.targetScrollOffset.x = x
    self.ScrollData.targetScrollOffset.y = y
end

function MapScroll:Update( dt )
    if self.ScrollData.touchId >= 0 then
        self.ScrollData.dragTime = self.ScrollData.dragTime + dt
    end

    self:SampleVelocity( dt )
    self:UpdateScrolling( dt )
end

function MapScroll:SampleVelocity( dt )
    if self.ScrollData.touchId >= 0 and self.ScrollData.sampleTimer >= 0 then
        self.ScrollData.sampleTimer = self.ScrollData.sampleTimer - dt
        if self.ScrollData.sampleTimer < 0 then
            self.ScrollData.sampleTimer = self.ScrollData.samplingInterval
            self.ScrollData.lastVelocity.x = (self.ScrollData.currentTouchPos.x - self.ScrollData.sampleTouchPos.x) / self.ScrollData.samplingInterval
            self.ScrollData.lastVelocity.y = (self.ScrollData.currentTouchPos.y - self.ScrollData.sampleTouchPos.y) / self.ScrollData.samplingInterval
            self.ScrollData.sampleTouchPos.x = self.ScrollData.currentTouchPos.x
            self.ScrollData.sampleTouchPos.y = self.ScrollData.currentTouchPos.y
        end
    end
end

function MapScroll:TouchDown( x, y, id, c )
    local path = c:GetPath()
    if string.find( path, "/Map%-%d%d" ) then
        if self.ScrollData.touchId < 0 then
            self.ScrollData.touchId = id
            self.ScrollData.touchStart.x = x
            self.ScrollData.touchStart.y = y
            self.ScrollData.scrolling = false
            self.ScrollData.velocity.x = 0
            self.ScrollData.velocity.y = 0
            self.ScrollData.lastVelocity.x = 0
            self.ScrollData.lastVelocity.y = 0
            self.ScrollData.sampleTimer = self.ScrollData.samplingInterval
            self.ScrollData.currentTouchPos.x = x
            self.ScrollData.currentTouchPos.y = y
            self.ScrollData.sampleTouchPos.x = x
            self.ScrollData.sampleTouchPos.y = y
            self.ScrollData.dragTime = 0
        end
    end
end

function MapScroll:TouchUp( x, y, id )
    if self.ScrollData.scrolling and id == self.ScrollData.touchId then
        self.ScrollData.scrolling = false
        self.ScrollData.touchId = -1
        self.ScrollData.sampleTimer = -1
        self.ScrollData.targetScrollOffset.x = self:ClampOffsetX( self.ScrollData.startScrollOffset.x + self.ScrollData.touchStart.x - x )
        self.ScrollData.targetScrollOffset.y = self:ClampOffsetY( self.ScrollData.startScrollOffset.y + self.ScrollData.touchStart.y - y )

        if self.ScrollData.dragTime < self.ScrollData.samplingInterval and self.ScrollData.dragTime > 0 then
            self.ScrollData.lastVelocity.x = (x - self.ScrollData.sampleTouchPos.x) / self.ScrollData.dragTime
            self.ScrollData.lastVelocity.y = (y - self.ScrollData.sampleTouchPos.y) / self.ScrollData.dragTime
        end
        self.ScrollData.velocity.x = self.ScrollData.lastVelocity.x
        self.ScrollData.velocity.y = self.ScrollData.lastVelocity.y
    else
        local c = screen:GetTouchableControl( x, y )
        if c ~= nil then
            MapAction( c:GetPath() )
        end
    end
end

function MapScroll:TouchMove( x, y, id )
    if id == self.ScrollData.touchId then
        if not self.ScrollData.scrolling then
            if GetLength( self.ScrollData.touchStart.x - x, self.ScrollData.touchStart.y - y ) > self.ScrollData.minScrollDist then
                self.ScrollData.touchStart.x = x
                self.ScrollData.touchStart.y = y

                self.ScrollData.startScrollOffset.x = self.ScrollData.scrollOffset.x
                self.ScrollData.startScrollOffset.y = self.ScrollData.scrollOffset.y

                self.ScrollData.scrolling = true
            end
        else
            self.ScrollData.targetScrollOffset.x = self:ClampOffsetX( self.ScrollData.startScrollOffset.x + self.ScrollData.touchStart.x - x )
            self.ScrollData.targetScrollOffset.y = self:ClampOffsetY( self.ScrollData.startScrollOffset.y + self.ScrollData.touchStart.y - y )

            self.ScrollData.currentTouchPos.x = x
            self.ScrollData.currentTouchPos.y = y
        end
    end
end

function MapScroll:ClampOffsetX( offset )
    return math.min( math.max(offset, 0), self.ScrollData.maxScrollOffset.x )
end

function MapScroll:ClampOffsetY( offset )
    return math.min( math.max(offset, 0), self.ScrollData.maxScrollOffset.y )
end

function MapScroll:UpdateScrolling( dt )
    if math.abs(self.ScrollData.velocity.x) > self.ScrollData.velocityEpsilon then
        self.ScrollData.velocity.x = self.ScrollData.velocity.x - self.ScrollData.velocity.x * self.ScrollData.scrollAcceleration * dt
        self.ScrollData.targetScrollOffset.x = self:ClampOffsetX( self.ScrollData.targetScrollOffset.x - self.ScrollData.velocity.x * dt )
    end

    if math.abs(self.ScrollData.scrollOffset.x - self.ScrollData.targetScrollOffset.x) > self.ScrollData.velocityEpsilon then
        self.ScrollData.scrollOffset.x = self.ScrollData.targetScrollOffset.x
        self.areas:SetX( -self.ScrollData.scrollOffset.x )
    end

    if math.abs(self.ScrollData.velocity.y) > self.ScrollData.velocityEpsilon then
        self.ScrollData.velocity.y = self.ScrollData.velocity.y - self.ScrollData.velocity.y * self.ScrollData.scrollAcceleration * dt
        self.ScrollData.targetScrollOffset.y = self:ClampOffsetY( self.ScrollData.targetScrollOffset.y - self.ScrollData.velocity.y * dt )
    end

    if math.abs(self.ScrollData.scrollOffset.y - self.ScrollData.targetScrollOffset.y) > self.ScrollData.velocityEpsilon then
        self.ScrollData.scrollOffset.y = self.ScrollData.targetScrollOffset.y
        self.areas:SetY( -self.ScrollData.scrollOffset.y )
    end
end

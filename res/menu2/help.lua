HelpScreen = 
{       HelpLogin = 1,
        HelpLoginMethod = 2 ,
        HelpRegister = 3 ,
        HelpFriendInfo = 4 ,
        HelpSurvival = 5 ,
        HelpBackupTeam = 6,
        HelpFriends = 7 ,
        HelpAccount = 8 ,
        HelpRequests = 9,
        HelpPremissionMain = 10,
        HelpInvitePopup = 11,
        HelpNone = 12   }
        
local currentHelpScreen = HelpScreen.HelpNone
        
function HelpOnStart( screen )

    if screen == HelpScreen.HelpLoginMethod  then
        HelpLoginMethod()
    elseif screen == HelpScreen.HelpLogin then
        HelpLogin()
    elseif screen == HelpScreen.HelpRegister then
        HelpRegister()
    elseif screen == HelpScreen.HelpFriendInfo then
        HelpFriendInfo()
    elseif screen == HelpScreen.HelpSurvival then
        HelpSurvival()
    elseif screen == HelpScreen.HelpBackupTeam then
        HelpBackupTeam()
    elseif screen == HelpScreen.HelpFriends then
        HelpFriends()
    elseif screen == HelpScreen.HelpAccount then
        HelpAccount()
    elseif screen == HelpScreen.HelpRequests then
        HelpRequests()
    elseif screen == HelpScreen.HelpPremissionMain then
        HelpPremissionMain()
    elseif screen == HelpScreen.HelpInvitePopup then
        HelpInvitePopup()
    end
    
    currentHelpScreen = screen
    
    OnHelpShow()
end        

function EndHelp()
    HelpOnEnd( currentHelpScreen )
end

function HelpOnEnd( screen )

    if screen == HelpScreen.HelpLoginMethod  then
        HelpLoginMethodCompleted()
    elseif screen == HelpScreen.HelpLogin then
        HelpLoginCompleted()
    elseif screen == HelpScreen.HelpRegister then
        HelpRegisterCompleted()
    elseif screen == HelpScreen.HelpFriendInfo then
        HelpFriendInfoCompleted()
    elseif screen == HelpScreen.HelpSurvival then
        HelpSurvivalCompleted()
    elseif screen == HelpScreen.HelpBackupTeam then
        HelpBackupTeamCompleted()
    elseif screen == HelpScreen.HelpFriends then
        HelpFriendsCompleted()
    elseif screen == HelpScreen.HelpAccount then
        HelpAccountCompleted()
    elseif screen == HelpScreen.HelpRequests then
        HelpRequestsCompleted()
    elseif screen == HelpScreen.HelpPremissionMain then
        HelpPremissionMainCompleted()
    elseif screen == HelpScreen.HelpInvitePopup then
        HelpInvitePopupCompleted()
    end
    
    OnHelpHide()
end

local delayHelpPopups = 0.6

local LoginMethodLoginPath = "/LoginMethodPopup/Login"
--local LoginMethodCreatePath = "/LoginMethodPopup/CreateAccount"
local LoginMethodLoginFbPath = "/LoginMethodPopup/LoginWithFB"
--local LoginMethodLoginGpPath = "/LoginMethodPopup/LoginWithGoogle"

function HelpLoginMethod()
    local button = screen:GetControl( LoginMethodLoginPath )
    local x = button:GetAbsX() + button:GetWidth()
    local y = button:GetAbsY() + button:GetHeight()/2

     TutorialTooltipShow( "/", "TutorialHelpLogin", "TEXT_HELP1_LOGIN", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
     --button = screen:GetControl( LoginMethodCreatePath )
     --x = button:GetAbsX()
     --y = button:GetAbsY() + button:GetHeight()/2

    --TutorialTooltipShow( "/", "TutorialHelpCreate", "TEXT_HELP1_CREATE_ACCOUNT", x, y, TutorialAnchor.Right, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )

    local buttonFb = screen:GetControl( LoginMethodLoginFbPath )
    --local buttonGp = screen:GetControl( LoginMethodLoginGpPath )


    --if buttonFb:GetVisibility() and buttonGp:GetVisibility() then
        --x = buttonFb:GetAbsX() + buttonFb:GetWidth()
        ---y = buttonFb:GetAbsY() + buttonFb:GetHeight() + R(5)
        --TutorialTooltipShow( "/", "TutorialHelpFb", "TEXT_HELP1_LOGIN_SOCIAL", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
   -- else
        if buttonFb:GetVisibility() then
            x = buttonFb:GetAbsX() + buttonFb:GetWidth()
            y = buttonFb:GetAbsY() + buttonFb:GetHeight()/2
            TutorialTooltipShow( "/", "TutorialHelpFb", "TEXT_HELP1_LOGIN_FB", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
        end

        --if buttonGp:GetVisibility() then
            --x = buttonGp:GetAbsX() + buttonGp:GetWidth()
            --y = buttonGp:GetAbsY() + buttonGp:GetHeight()/2
            --TutorialTooltipShow( "/", "TutorialHelpGoogle", "TEXT_HELP1_LOGIN_GP", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
        --end
    --end
end


function HelpLoginMethodCompleted()
    TutorialTooltipHide( "/TutorialHelpLogin" )
    --TutorialTooltipHide( "/TutorialHelpCreate" )
    TutorialTooltipHide( "/TutorialHelpFb" )
    TutorialTooltipHide( "/TutorialHelpGoogle" )
end


local FriendInfoLevel = "/FriendInfo/LevelInfo/LevelIcon/LevelValue"
local FriendInfoStats = "/FriendInfo/InfoRoot/EventsCompletedInfo/EventsCompletedBar"
local FriendInfoWeapon = "/FriendInfo/InfoRoot/WeaponInfoRoot/WeaponSlot/SlotUnderLine"
local FriendInfoGift = "/FriendInfo/SendGift"


function HelpFriendInfo()
    local button = screen:GetControl( FriendInfoLevel )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY()
    TutorialTooltipShow( "/", "TutorialHelpFriendInfoLevel", "TEXT_HELP_FRIEND_INFO_LEVEL", x, y, TutorialAnchor.Bottom, delayHelpPopups, true, 0, SCREEN_WIDTH/2, true )
    
    button = screen:GetControl( FriendInfoStats )
    x = button:GetAbsX() + button:GetWidth() /2
    y = button:GetAbsY()  
    TutorialTooltipShow( "/", "TutorialHelpFriendInfoStats", "TEXT_HELP_FRIEND_INFO_STATS", x, y, TutorialAnchor.Bottom, delayHelpPopups, true , 0, SCREEN_WIDTH/2, true )
    
    button = screen:GetControl( FriendInfoWeapon )
    x = button:GetAbsX() + R( 51 )
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpFriendInfoWeapon", "TEXT_HELP_FRIEND_INFO_WEAPON", x, y, TutorialAnchor.Top, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( FriendInfoGift )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() 
    TutorialTooltipShow( "/", "TutorialHelpFriendInfoGift", "TEXT_HELP_FRIEND_INFO_GIFT", x, y, TutorialAnchor.Bottom, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
end

function HelpFriendInfoCompleted()
    TutorialTooltipHide( "/TutorialHelpFriendInfoLevel" )
    TutorialTooltipHide( "/TutorialHelpFriendInfoStats" )
    TutorialTooltipHide( "/TutorialHelpFriendInfoWeapon" )
    TutorialTooltipHide( "/TutorialHelpFriendInfoGift" )
end


local RegisterHeader = "/RegisterPopup/Login/LoginInputButton"
local RegisterUserName = "/RegisterPopup/Login/LoginText"
local RegisterPass = "/RegisterPopup/Pass/PassText"
local RegisterEmail = "/RegisterPopup/Email/EmailText"
local RegisterRegister = "/RegisterPopup/RegisterSend"
local RegisterToLogin = "/RegisterPopup/LoginButton"

function HelpRegister()
    local button = screen:GetControl( RegisterHeader )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() - button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpRegisterHeader", "TEXT_HELP_REGISTER_HEADER", x, y, TutorialAnchor.Bottom, delayHelpPopups, false, 0, SCREEN_WIDTH/2, true )
    
    button = screen:GetControl( RegisterUserName )
    x = button:GetAbsX() 
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpRegisterUserName", "TEXT_HELP_REGISTER_USERNAME", x, y, TutorialAnchor.Right, delayHelpPopups, true, 0, SCREEN_WIDTH/4, true )
    
    button = screen:GetControl( RegisterPass )
    x = button:GetAbsX() + button:GetWidth() - R(10)
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpRegisterPass", "TEXT_HELP_REGISTER_PASS", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/4, true )
    
    button = screen:GetControl( RegisterEmail )
    x = button:GetAbsX() 
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpRegisterEmail", "TEXT_HELP_REGISTER_EMAIL", x, y, TutorialAnchor.Right, delayHelpPopups, true, 0, SCREEN_WIDTH/4, true )
    
    button = screen:GetControl( RegisterRegister )
    x = button:GetAbsX() + button:GetWidth()
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpRegisterRegister", "TEXT_HELP_REGISTER_REGISTER", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( RegisterToLogin )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpRegisterRegisterToLogin", "TEXT_HELP_REGISTER_TO_LOGIN", x, y, TutorialAnchor.Top, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
end

function HelpRegisterCompleted()
    TutorialTooltipHide( "/TutorialHelpRegisterHeader" )
    TutorialTooltipHide( "/TutorialHelpRegisterUserName" )
    TutorialTooltipHide( "/TutorialHelpRegisterPass" )
    TutorialTooltipHide( "/TutorialHelpRegisterEmail" )
    TutorialTooltipHide( "/TutorialHelpRegisterRegister" )
    TutorialTooltipHide( "/TutorialHelpRegisterRegisterToLogin" )
end

local LoginHeader = "/LoginPopup/Login/LoginUsernameInputButton"
local LoginUserName = "/LoginPopup/Login/LoginUsernameInputButton"
local LoginPass = "/LoginPopup/Pass/LoginPasswordInputButton"
local LoginLogin = "/LoginPopup/LoginButton"
local LoginRecoverPass = "/LoginPopup/RecoverPass"
local LoginRegister = "/LoginPopup/CreateAccountButton"

function HelpLogin()
    local button = screen:GetControl( LoginHeader )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() - button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpLoginHeader", "TEXT_HELP_LOGIN_HEADER", x, y, TutorialAnchor.Bottom, delayHelpPopups, false, 0,  SCREEN_WIDTH*2/5, true )
    
    button = screen:GetControl( LoginUserName )
    x = button:GetAbsX() 
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpLoginUserName", "TEXT_HELP_LOGIN_USERNAME", x, y, TutorialAnchor.Right, delayHelpPopups, true, 0, SCREEN_WIDTH/4, true )
    
    button = screen:GetControl( LoginPass )
    x = button:GetAbsX() 
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpLoginPass", "TEXT_HELP_LOGIN_PASS", x, y, TutorialAnchor.Right, delayHelpPopups, true , 0, SCREEN_WIDTH/4, true )
    
    button = screen:GetControl( LoginLogin )
    x = button:GetAbsX() + button:GetWidth()
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpLoginLogin", "TEXT_HELP_LOGIN_LOGIN", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( LoginRecoverPass )
    x = button:GetAbsX()
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpLoginRecoverPass", "TEXT_HELP_LOGIN_RECOVER", x, y, TutorialAnchor.Right, delayHelpPopups, true,0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( LoginRegister )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpLoginRegister", "TEXT_HELP_LOGIN_CREATE", x, y, TutorialAnchor.Top, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
end

function HelpLoginCompleted()
    TutorialTooltipHide( "/TutorialHelpLoginHeader" )
    TutorialTooltipHide( "/TutorialHelpLoginUserName" )
    TutorialTooltipHide( "/TutorialHelpLoginPass" )
    TutorialTooltipHide( "/TutorialHelpLoginLogin" )
    TutorialTooltipHide( "/TutorialHelpLoginRecoverPass" )
    TutorialTooltipHide( "/TutorialHelpLoginRegister" )
end

local SurvivalLeaderboard = "/Survival/SurvivalListRoot/SurvivalList/Survivals/1/SurvivalItem/LeaderboardRoot/Tabs/TabGlobal/Button"
local SurvivalPlay = "/Survival/SurvivalListRoot/SurvivalList/Survivals/1/SurvivalItem/Play/Button"
local SurvivalLocked = "/Survival/SurvivalListRoot/SurvivalList"

function HelpSurvival()
    local button = screen:GetControl( SurvivalLeaderboard )
    local x = button:GetAbsX() + button:GetWidth()/2 
    local y = button:GetAbsY()
    TutorialTooltipShow( "/", "TutorialHelpLeaderborads", "TEXT_HELP_SURVIVAL_LEADERBOARD", x, y, TutorialAnchor.Bottom, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( SurvivalPlay )
    x = button:GetAbsX() + button:GetWidth()/2 
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpSurvivalPlay", "TEXT_HELP_SURVIVAL_PLAY", x, y, TutorialAnchor.Top, delayHelpPopups, true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( SurvivalLocked )
    x = button:GetAbsX()
    y = button:GetAbsY() + R( 250 )
    TutorialTooltipShow( "/", "TutorialHelpSurvivalLocked", "TEXT_HELP_SURVIVAL_LOCKED", x, y, TutorialAnchor.Bottom, delayHelpPopups, false, 0, SCREEN_WIDTH/3, true )
end


function HelpSurvivalCompleted()
    TutorialTooltipHide( "/TutorialHelpLeaderborads" )
    TutorialTooltipHide( "/TutorialHelpSurvivalPlay" )
    TutorialTooltipHide( "/TutorialHelpSurvivalLocked" )
end

local BackupTeamHeader = "/BackupTeam/BackupTeamRoot"
local BackupTeamSendButton = "/BackupTeam/BackupTeamRoot/BackupList/Backups/1/BackupItem/InfoRoot/Buttons/SelectFriend"


function HelpBackupTeam()
    local button = screen:GetControl( BackupTeamHeader )
    local x = button:GetAbsX()
    local y = button:GetAbsY() + R( 26 )
    local delay = delayHelpPopups
    local stepDelay = 0
    
    TutorialTooltipShow( "/", "TutorialHelpBackupTeamHeader", "TEXT_HELP_BACKUP_TEAM_HEADER", x, y, TutorialAnchor.Bottom, delay, false, 0, SCREEN_WIDTH/3, true )
    delay = delay + stepDelay
    button = screen:GetControl( BackupTeamSendButton )
    
    if button then
        x = button:GetAbsX() + button:GetWidth()
        y = button:GetAbsY() + button:GetHeight()/2
        TutorialTooltipShow( "/", "TutorialHelpBackupTeamSendButton", "TEXT_HELP_BACKUP_TEAM_BUTTON", x, y, TutorialAnchor.Left, delayHelpPopups, true, 0, SCREEN_WIDTH/2, true )
    end
end


function HelpBackupTeamCompleted()
    TutorialTooltipHide( "/TutorialHelpBackupTeamHeader" )
    if screen:GetControl( BackupTeamSendButton ) then
        TutorialTooltipHide( "/TutorialHelpBackupTeamSendButton" )
    end
end

local FriendsInbox = "/Social/ResistanceHq/Inbox/Button"
local FriendsCheckProfile = "/Social/ResistanceHq/FriendsScreenRoot/ResistanceTeam/FriendsListRoot/FriendList/Friends/1/SocialItem/InfoRoot/Buttons/More"
local FriendsInvite = "/Social/ResistanceHq/Invite/Button"
local FriendsProfile = "/Social/ResistanceHq/Profile/Button"
local FriendsInviteCode = "/Social/ResistanceHq/InvitationCode"

function HelpFriends()
    local delay = delayHelpPopups
    local stepDelay = 0
    
    local button = screen:GetControl( FriendsInbox )
    local x = button:GetAbsX() + button:GetWidth()
    local y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpFriendsInbox", "TEXT_HELP_SOCIAL_INBOX", x, y, TutorialAnchor.Left, delay, true , 0, SCREEN_WIDTH/3, true  )
    delay = delay + stepDelay
    
    button = screen:GetControl( FriendsCheckProfile )
    if button then
        x = button:GetAbsX() + button:GetWidth()
        y = button:GetAbsY() + button:GetHeight()/2
        TutorialTooltipShow( "/", "TutorialHelpFriendsCheckProfile", "TEXT_HELP_SOCIAL_CHECK_PROFILE", x, y, TutorialAnchor.Left, delay, true , 0, SCREEN_WIDTH/3, true )
        delay = delay + stepDelay
    end
    
    button = screen:GetControl( FriendsInvite )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpFriendsInvite", "TEXT_HELP_SOCIAL_ADD", x, y, TutorialAnchor.Top, delay , true , 0, SCREEN_WIDTH/5, true )
    delay = delay + stepDelay
    
    button = screen:GetControl( FriendsProfile )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY()
    TutorialTooltipShow( "/", "TutorialHelpFriendsProfile", "TEXT_HELP_SOCIAL_PROFILE", x, y, TutorialAnchor.Bottom, delay , true , 0, SCREEN_WIDTH/3, true )

    button = screen:GetControl( FriendsInviteCode )
    x = button:GetAbsX()
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpFriendsInviteCode", "TEXT_HELP_ACCOUNT_INIVITE_CODE", x, y, TutorialAnchor.Right, delay , true , 0, SCREEN_WIDTH/4, true )
end

function HelpFriendsCompleted()
    TutorialTooltipHide( "/TutorialHelpFriendsInbox" )

    if screen:GetControl( FriendsCheckProfile ) then
        TutorialTooltipHide( "/TutorialHelpFriendsCheckProfile" )
    end

    TutorialTooltipHide( "/TutorialHelpFriendsInvite" )

    TutorialTooltipHide( "/TutorialHelpFriendsProfile" )
    TutorialTooltipHide( "/TutorialHelpFriendsInviteCode" )
end



local AccountEdit = "/Social/ResistanceAccount/PassVerify/PassVerifyInputButton"
--local AccountInviteCode = "/Social/ResistanceAccount/UserInfoRoot/InvitationCode"

function HelpAccount()
    local delay = delayHelpPopups
    local stepDelay = 0

    local button = screen:GetControl( AccountEdit )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + button:GetHeight() + R(6)
    TutorialTooltipShow( "/", "TutorialHelpAccountEdit", "TEXT_HELP_ACCOUNT_EDIT", x, y, TutorialAnchor.Top, delay, true , 0, SCREEN_WIDTH/2, true )
    delay = delay + stepDelay

    -- button = screen:GetControl( AccountInviteCode )
    -- x = button:GetAbsX() + button:GetWidth()/2
    -- y = button:GetAbsY() + button:GetHeight()
    -- TutorialTooltipShow( "/", "TutorialHelpAccountInviteCode", "TEXT_HELP_ACCOUNT_INIVITE_CODE", x, y, TutorialAnchor.Top, delay,  true, 0, SCREEN_WIDTH/2, true )
    -- delay = delay + stepDelay
end

function HelpAccountCompleted()
    TutorialTooltipHide( "/TutorialHelpAccountEdit" )
    --TutorialTooltipHide( "/TutorialHelpAccountInviteCode" )
end

local RequestsHeader = "/ResistanceHq/Tabs/Account/Button"
local RequestsAvatar =  "/ResistanceHq/RequestsScreenRoot/ResistanceRequests/ResistanceRequestsListRoot/RequestsList/Requests/2/FriendItem/InfoRoot/FriendInfo/Avatar"
local RequestsAccept = "/ResistanceHq/RequestsScreenRoot/ResistanceRequests/ResistanceRequestsListRoot/RequestsList/Requests/1/FriendItem/InfoRoot/Buttons/RejectInvitation"
local RequestsGet =    "/ResistanceHq/RequestsScreenRoot/ResistanceRequests/ResistanceRequestsListRoot/RequestsList/Requests/2/FriendItem/InfoRoot/Buttons/AcceptGift"


function HelpRequests()

    local button = screen:GetControl( RequestsHeader )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + R( 5 )
    TutorialTooltipShow( "/", "TutorialHelpRequestsHeader", "TEXT_HELP_FRIENDS_HEADER", x, y, TutorialAnchor.Bottom, 0.1 , true, 0,  SCREEN_WIDTH/2, true )
    
    button = screen:GetControl( RequestsAvatar )
    x = button:GetAbsX() + button:GetWidth()
    y = button:GetAbsY() + button:GetHeight() / 2
    TutorialTooltipShow( "/", "TutorialHelpRequestsAvatar", "TEXT_HELP_REQUESTS_AVATAR", x, y, TutorialAnchor.Left, 0.1 , true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( RequestsAccept )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpRequestsAccept", "TEXT_HELP_REQUESTS_ACCEPT", x, y, TutorialAnchor.Top, 0.1 , true, 0, SCREEN_WIDTH/3, true )
    
    button = screen:GetControl( RequestsGet )
    x = button:GetAbsX() + button:GetWidth()
    y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpRequestsGift", "TEXT_HELP_REQUESTS_GET", x, y, TutorialAnchor.Left, 0.1 , true, 0, SCREEN_WIDTH/3, true )
end

function HelpRequestsCompleted()
    ResistanceRequestsHideHelp( function() ResistanceRequestsShow() end )
    TutorialTooltipHide( "/TutorialHelpRequestsHeader" )
    TutorialTooltipHide( "/TutorialHelpRequestsAvatar" )
    TutorialTooltipHide( "/TutorialHelpRequestsAccept" )
    TutorialTooltipHide( "/TutorialHelpRequestsGift" )
end

local PremissionFuel = "/Premission/Premission/Mission/Header/Title"
local PremissionReward =  "/Premission/Premission/Mission/Rewards/Xp/Value"
local PremissionBackup = "/Premission/Premission/Mission/Friend/Add"
local PremissionSlot =  "/Premission/Premission/Weapon02/Window"
local PremissionMonster =  "/Premission/Premission/Monsters/Icons/2/Bg"


function HelpPremissionMain()

    local button = screen:GetControl( PremissionFuel )
    local vip_status = registry:Get( "/monstaz/subscription" )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() 

    if vip_status == false then
        TutorialTooltipShow( "/", "TutorialHelpPremissionFuel", "TEXT_HELP_PREMISSION_FUEL", x, y, TutorialAnchor.Bottom, delayHelpPopups , true, 0, SCREEN_WIDTH/2, true )
    end

    button = screen:GetControl( PremissionReward )
    if button:GetVisibility() then
        x = button:GetAbsX() + button:GetWidth() / 2
        y = button:GetAbsY() + button:GetHeight()
        TutorialTooltipShow( "/", "TutorialHelpPremissionReward", "TEXT_HELP_PREMISSION_REWARD", x, y, TutorialAnchor.Top, delayHelpPopups , true , 0, SCREEN_WIDTH/4, true )
    end
    
    button = screen:GetControl( PremissionBackup )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() + button:GetHeight()
    TutorialTooltipShow( "/", "TutorialHelpPremissionBackup", "TEXT_HELP_PREMISSION_BACKUP", x, y, TutorialAnchor.Top, delayHelpPopups , true, 0, SCREEN_WIDTH/4, true )
    
    button = screen:GetControl( PremissionSlot )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() 
    TutorialTooltipShow( "/", "TutorialHelpPremissionSlot", "TEXT_HELP_PREMISSION_WEAPON", x, y, TutorialAnchor.Bottom, delayHelpPopups , true, 0, SCREEN_WIDTH/3, true )

    button = screen:GetControl( PremissionMonster )
    x = button:GetAbsX() + button:GetWidth()/2
    y = button:GetAbsY() 
    TutorialTooltipShow( "/", "TutorialHelpPremissionMonster", "TEXT_HELP_PREMISSION_MONSTERS", x, y, TutorialAnchor.Bottom, delayHelpPopups , true, 0, SCREEN_WIDTH/3, true )
end

function HelpPremissionMainCompleted()
    local vip_status = registry:Get( "/monstaz/subscription" )
    if vip_status == false then
        TutorialTooltipHide( "/TutorialHelpPremissionFuel" )
    end
    TutorialTooltipHide( "/TutorialHelpPremissionReward" )
    TutorialTooltipHide( "/TutorialHelpPremissionBackup" )
    TutorialTooltipHide( "/TutorialHelpPremissionSlot" )
    TutorialTooltipHide( "/TutorialHelpPremissionMonster" )
end

local InvitePopupField = "/InvitePopup/FriendIDInput/FriendIDInputImg"

function HelpInvitePopup()

    local button = screen:GetControl( InvitePopupField )
    local x = button:GetAbsX()
    local y = button:GetAbsY() + button:GetHeight()/2
    TutorialTooltipShow( "/", "TutorialHelpInvitePopup", "TEXT_HELP_INVITE_POPUP", x, y, TutorialAnchor.Right, delayHelpPopups , true, 0, SCREEN_WIDTH/4, true )

end

function HelpInvitePopupCompleted()
    TutorialTooltipHide( "/TutorialHelpInvitePopup" )
end



require( "menu2/animation.lua" )

local border = R(1)
local margin = R(15)
local padding = { x = R(7), y = R(2) }

local tooltips = {}

function TutorialTooltipShow( path, name, message, x, y, anchor, delay, showArrow, hideDelay , maxWidth , disabledAnim, representation, ignoreMargin )
    local parent = screen:GetControl( path )
    if not parent then return end

    TutorialTooltipRemove( path .. "/" .. name )

    local tooltip = parent:InsertControl( name )
    tooltip:InsertFromXml( "menu2/tutorial_tooltip.xml" )
    tooltip:SetRelative( true )

    local text = tooltip:GetControl( "Text" )
    text:SetText( message )

    local tooltipWidth = SCREEN_WIDTH/3

    if maxWidth ~= nil then
        tooltipWidth = maxWidth
    end

    if text:GetTextWidth() > tooltipWidth then
        text:SetTextWidth( tooltipWidth )
    else
        text:SetTextWidth( text:GetTextWidth() )
    end

    local width = text:GetWidth() + padding.x*2
    local height = text:GetHeight() + padding.y*2

    local outline = tooltip:GetControl( "Outline" )
    outline:GetGraphic( "r" ):SetWidth( width )
    outline:GetGraphic( "r" ):SetHeight( height )

    local arrow = tooltip:GetControl( "Arrow" )
    local bg = tooltip:GetControl( "Background" )

    if representation then
        arrow:SetRepresentation( representation )
        bg:SetRepresentation( representation )
    end

    bg:GetGraphic( "r" ):SetWidth( width - border*2 )
    bg:GetGraphic( "r" ):SetHeight( height - border*2 )

    if showArrow == nil then showArrow = true end
    arrow:SetVisibility( showArrow )

    local fadeOffsetX = R(30)
    local fadeOffsetY = R(30)
    if not showArrow then fadeOffsetY = -fadeOffsetY end

    if anchor == TutorialAnchor.Bottom then
        fadeOffsetX = 0
        fadeOffsetY = -fadeOffsetY

        arrow:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_VERTICAL )
        arrow:SetX( ( width - arrow:GetWidth() )/2 )
        arrow:SetY( height - border )

        outline:SetX( 0 )
        outline:SetY( 0 )

        bg:SetX( border )
        bg:SetY( border )

        text:SetX( padding.x )
        text:SetY( padding.y )

        tooltip:SetX( x - width/2 )
        tooltip:SetY( y - height - arrow:GetHeight() )
    elseif anchor == TutorialAnchor.Top then
        fadeOffsetX = 0

        arrow:SetX( ( width - arrow:GetWidth() )/2 )
        arrow:SetY( 0 )

        outline:SetX( 0 )
        outline:SetY( arrow:GetHeight() - border )

        bg:SetX( border )
        bg:SetY( arrow:GetHeight() )

        text:SetX( padding.x )
        text:SetY( arrow:GetHeight() - border + padding.y )

        tooltip:SetX( x - width/2 )
        tooltip:SetY( y )
    elseif anchor == TutorialAnchor.Right then
        fadeOffsetX = -fadeOffsetX
        fadeOffsetY = 0

        arrow:GetGraphic( "TS" ):SetPivot( arrow:GetWidth()/2, arrow:GetHeight() )
        arrow:GetGraphic( "TS" ):SetAngle( math.pi/2 )
        arrow:SetX( width - border )
        arrow:SetY( height/2 )

        outline:SetX( 0 )
        outline:SetY( 0 )

        bg:SetX( border )
        bg:SetY( border )

        text:SetX( padding.x )
        text:SetY( padding.y )

        tooltip:SetX( x - width - arrow:GetHeight() + border )
        tooltip:SetY( y - height/2 )
    elseif anchor == TutorialAnchor.Left then
        fadeOffsetY = 0
        arrow:GetGraphic( "TS" ):SetPivot( arrow:GetWidth()/2, 0 )
        arrow:GetGraphic( "TS" ):SetAngle( -math.pi/2 )
        arrow:SetX( 0 )
        arrow:SetY( height/2 )

        outline:SetX( arrow:GetHeight() - border )
        outline:SetY( 0 )

        bg:SetX( arrow:GetHeight() )
        bg:SetY( border )

        text:SetX( padding.x + arrow:GetHeight() - border )
        text:SetY( padding.y )

        tooltip:SetX( x )
        tooltip:SetY( y - height/2 )
    end

    if disabledAnim then
        fadeOffsetY = 0
        fadeOffsetX = 0
    end

    if not ignoreMargin then
        local x = tooltip:GetAbsX()
        if x < margin then
            local offset = margin - x
            tooltip:SetX( tooltip:GetX() + offset )
            arrow:SetX( arrow:GetX() - offset )
        elseif x + width > SCREEN_WIDTH - margin then
            local offset = x + width - SCREEN_WIDTH + margin
            tooltip:SetX( tooltip:GetX() - offset )
            arrow:SetX( arrow:GetX() + offset )
        end
    end

    local fullPath = tooltip:GetPath()

    local px = tooltip:GetX()
    local py = tooltip:GetY()
    tooltip:SetAlpha( 0 )
    anim:Add(
        Delay:New( delay ),
        function()
            local tooltip = screen:GetControl( fullPath )
            if not tooltip then return end
            anim:Add(
                Slider:New( 0.5,
                    function( t )
                        tooltip:SetX( math.floor( 0.5 + px + fadeOffsetX*( 1 - SmoothStep( t ) ) ) )
                        tooltip:SetY( math.floor( 0.5 + py + fadeOffsetY*( 1 - SmoothStep( t ) ) ) )
                        tooltip:SetAlpha( 255*SmoothStep( t ) )
                    end
                ),
                function()
                    if not showArrow then
                        return
                    end

                    local bounceOffsetX = fadeOffsetX/10
                    local bounceOffsetY = fadeOffsetY/10
                    anim:Add(
                        Perpetual:New(
                            function( t )
                                tooltip:SetX( math.floor( 0.5 + px + bounceOffsetX*( 1 - math.cos( t*5 ) ) ) )
                                tooltip:SetY( math.floor( 0.5 + py + bounceOffsetY*( 1 - math.cos( t*5 ) ) ) )
                            end
                        ),
                        nil,
                        fullPath
                    )
                end,
                fullPath
            )
        end,
        fullPath
    )

    if hideDelay and hideDelay > 0 then
        anim:Add(
            Delay:New( hideDelay ),
            function()
                TutorialTooltipHide( fullPath )
            end,
            fullPath
        )
    end

    tooltips[fullPath] = tooltip

    return tooltip
end

function TutorialTooltipHide( path )
    anim:RemoveAll( path )

    local tooltip = tooltips[path]
    if not tooltip then return end

    tooltips[path] = nil

    local alpha = tooltip:GetAlpha()
    local time = 0.3*alpha/255
    anim:Add(
        Slider:New( time,
            function( t )
                local tooltip = screen:GetControl( path )
                if tooltip then tooltip:SetAlpha( alpha*( 1 - SmoothStep( t ) ) ) end
            end
        ),
        function()
            screen:RemoveControl( tooltip )
        end
    )
end

function TutorialTooltipRemove( path )
    anim:RemoveAll( path )

    local tooltip = tooltips[path]
    if not tooltip then return end

    tooltips[path] = nil
    screen:RemoveControl( tooltip )
end

function TutorialTooltipRemoveAll()
    local tooltips = TutorialGetTooltips()
    for path, t in pairs( tooltips ) do
        anim:RemoveAll( path )
        tooltips[path] = nil
        screen:RemoveControl( t )
    end
end

function TutorialGetTooltips()
    return tooltips
end

function TutorialGetOverlayVisibility()
    return TutorialActive and TutorialManager:GetCurrentChapter() ~= TutorialChapter.Outro
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/popup_login.lua" )
require( "menu2/inbox_item.lua" )
require( "menu2/requests_list.lua" )
require( "menu2/timeout.lua" )
require( "ResistanceDB.lua" )

local active = false
local syncing = false
local timeoutIndicatorActive = false

local x = R( 10 )
local y = R( 45 )

local width = SCREEN_WIDTH - R(10)
local height = SCREEN_HEIGHT - R(60)


local popup = nil
local root = nil

local dot1 = nil
local dot2 = nil
local dot3 = nil

local rootTimeout = nil
local inboxEmptyInfo = nil

function ResistanceRequestsKeyDown( code )
    if not active or IsSyncIndicatorVisible() then
        return
    end

    if code == keys.KEY_ESCAPE then
        ResistanceRequestsHide( ResistanceHqShow )
        return true
    end

    return false
end

function ResistanceRequestsInit( parent )

    parent:InsertFromXml( "menu2/resistance_requests.xml" )

    root = parent
    popup =  parent:GetControl( "ResistanceRequests" )

    x = ( SCREEN_WIDTH - width )/2
    y = R(10)

    popup:SetX( x )
    popup:SetY( y )
    popup:SetVisibility( false )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    dot1 = popup:GetControl( "SyncIndicator/SyncIndicatorDot1" )
    dot2 = popup:GetControl( "SyncIndicator/SyncIndicatorDot2" )
    dot3 = popup:GetControl( "SyncIndicator/SyncIndicatorDot3" )

    local w, h = dot1:GetGraphic( "TS" ):GetSize()
    local margin = R ( 5 )
    dot1:SetX( - margin - w - w/2)
    dot2:SetX( -w/2 )
    dot3:SetX( margin + w - w/2)

  -- button positioning
    local backBtn = popup:GetControl("Back")

    local bw, bh = backBtn:GetGraphic( "TS" ):GetSize()

    local topMargin = R(18)
    local sideMargin = R(35)
    local bottomMargin = R(6)

    backBtn:SetY( height + bottomMargin )
    backBtn:SetX( sideMargin )

    -- header position
    local header = popup:GetControl("InboxHeader")

    header:SetX( ( width - header:GetWidth() )/2 )
    header:SetY( bh/2 )

    -- list position
    local listRoot = popup:GetControl("ResistanceRequestsListRoot")

    local requestsListWidth = RequestsConsts.itemWidth

    listRoot:SetY( bh/2 + header:GetHeight() + bottomMargin )
    listRoot:SetX(( width - requestsListWidth )/2)

    local requestsListHeight = height - listRoot:GetY() - 4*bottomMargin
    --creating list of all item
    RequestsListInit( popup:GetControl( "ResistanceRequestsListRoot" ), requestsListWidth, requestsListHeight ) 

    popup:GetControl( "SyncIndicator"):SetX( listRoot:GetX() + requestsListWidth / 2 )
    popup:GetControl( "SyncIndicator"):SetY( listRoot:GetY() + requestsListHeight / 2 )


    --TimeoutRoot item init
    rootTimeout = popup:GetControl( "TimeoutRoot" )
    FillTimeoutIndicator( popup , width , height )
    rootTimeout:SetVisibility( false )

    inboxEmptyInfo = popup:GetControl( "InboxEmptyInfo" )
    inboxEmptyInfo:SetY(( height - inboxEmptyInfo:GetHeight() ) / 2 )
    inboxEmptyInfo:SetX(( width - inboxEmptyInfo:GetWidth() ) / 2 )

    inboxEmptyInfo:SetVisibility( false )
end

function ResistanceRequestsAction( name )
    if not active then
        return
    end

    if string.find( name, "/ResistanceRequests/Back" ) then
        ResistanceRequestsHide( ResistanceHqShow() )
    elseif string.find( name, "/ResistanceRequests/TimeoutRoot/Refresh/Button" ) then
        TimeoutRequestsIndicatorHide()
        callback:ResistanceUpdateAll()
    end
end

function ResistanceRequestsPress( control, path )

    if not active then
        return
    end

    if string.find( path,"/ResistanceRequests/Back" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceRequests/TimeoutRoot/Refresh/Button" ) then
        ButtonPress( control )
    end
end

function ResistanceRequestsRefresh()
    ResistanceRequestsHide( function() ResistanceRequestsShow() end )
end

function ResistanceRequestsShow()

    active = true

    if ( UpdateInfo.Requests > 0 ) then
        TimeoutRequestsIndicatorHide( nil, true )
        inboxEmptyInfo:SetVisibility( false )

        local numberOfItems = #Invitations

        if numberOfItems == 0 then
            RequestsListDropAllItems()
            RequestsListHidePosIndicator()
            inboxEmptyInfo:SetVisibility( true )
        else
            inboxEmptyInfo:SetVisibility( false )
            RequestsListSetContent( Invitations )
            RequestsListShowPosIndicator()
        end

        popup:GetControl("SyncIndicator"):SetVisibility( false ) 
      
    elseif UpdateInfo.Requests < 0 then
        popup:GetControl("SyncIndicator"):SetVisibility( false ) 
        inboxEmptyInfo:SetVisibility( false )
        RequestsListDropAllItems()
        RequestsListHidePosIndicator()
        TimeoutRequestsIndicatorShow()
    else
        TimeoutRequestsIndicatorHide( nil, true )
        inboxEmptyInfo:SetVisibility( false )
        ResistanceRequestsSyncingStart()
    end

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetAlpha( SmoothStep( t ) * 255 )
        end
        )
    )
end

function ResistanceRequestsHide( callback )
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end
     )
end

function ResistanceRequestsUpdate( dt )
    if not active then return end
    RequestsListUpdate( dt )

    if UpdateInfo.Requests > 0 and IsResistanceRequestsSyncing() then
        EndResistanceRequestsSyncingSucces()
    end

    if not IsResistanceRequestsSyncing() and UpdateInfo.Requests == 0 then
        if timeoutIndicatorActive == true then
            TimeoutRequestsIndicatorHide( function() ResistanceRequestsSyncingStart() end )
        else
            ResistanceRequestsSyncingStart()
        end
    end

    if UpdateInfo.Requests < 0 and IsResistanceRequestsSyncing() then
        EndResistanceRequestsSyncingFail()
        TimeoutRequestsIndicatorShow()
    end

end

function ResistanceRequestsTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    RequestsListTouchDown( x, y, id, c )

    local path = c:GetPath()
    --if string.find(path, "/ShopRoot/Buttons/") then
    --	ButtonPress( c )
    --end
end

function ResistanceRequestsTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    RequestsListTouchUp( x, y, id, c )
end

function ResistanceRequestsTouchMove( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    RequestsListTouchMove( x, y, id, c )
end

function IsResistanceRequestsSyncing()
   return syncing 
end

function IsResistanceRequestsActive()
   return active 
end

function EndResistanceRequestsSyncingSucces()

    syncing = false
    TimeoutRequestsIndicatorHide( nil, true )
    inboxEmptyInfo:SetVisibility( false )
    ResistanceRequestsSyncIndicatorHide()

    popup:SetVisibility( false )

    local numberOfItems = #Invitations

    if numberOfItems == 0 then
        inboxEmptyInfo:SetVisibility( true )
    else
        inboxEmptyInfo:SetVisibility( false )
        RequestsListSetContent( Invitations )
    end

    popup:SetAlpha( 0 )
    popup:SetVisibility( true )
    anim:Add( Slider:New(0.3,
        function( t )
            popup:SetAlpha( SmoothStep( t ) * 255 )
        end
        )
    )
end

function EndResistanceRequestsSyncingFail()
    ResistanceRequestsSyncIndicatorHide()
    syncing = false
end

function ResistanceRequestsSyncingStart()
    RequestsListDropAllItems()
    RequestsListHidePosIndicator()
    syncing = true
    ResistanceRequestsSyncIndicatorShow()
end

function ResistanceRequestsSyncIndicatorShow()

    local indicator  = popup:GetControl("SyncIndicator")

    indicator:SetVisibility( true )
    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "ResistanceRequestsPopup" )
    anim:Add( Slider:New( (time or 0.3) * (255-startAlpha)/255,
        function( t )
            indicator:SetAlpha( startAlpha + SmoothStep( t ) * (255-startAlpha) )
        end
        ),
        nil,
        "ResistanceRequestsSyncPopup"
    )

    anim:RemoveAll( "ResistanceRequestsSyncBlink" )

    local lt = 0
    local timer = 0
    local speed = 5
    anim:Add( Perpetual:New(
        function( t )
            local dt = t - lt
            lt = t
            if ( timer > 3) then 
                timer = 0
            else
                timer = timer + speed*dt
            end

            if timer <= 1 then
                dot1:SetRepresentation( "active" )
            else
                dot1:SetRepresentation( "default" )
            end

            if timer >= 1 and timer <= 2 then
                dot2:SetRepresentation( "active" )
            else
                dot2:SetRepresentation( "default" )
            end

            if timer >= 2 and timer <= 3 then
                dot3:SetRepresentation( "active" )
            else
                dot3:SetRepresentation( "default" )
            end
        end
        ),
        nil,
        "ResistanceRequestsSyncBlink"
    )
end

function ResistanceRequestsSyncIndicatorHide( callback )

    local indicator  = popup:GetControl("SyncIndicator")

    indicator:SetVisibility( true )

    local startAlpha = indicator:GetAlpha()
    anim:RemoveAll( "ResistanceRequestsSyncPopup" )
    anim:RemoveAll( "ResistanceRequestsSyncBlink" )
    anim:Add( Slider:New( (time or 0.3) * startAlpha/255,
        function( t )
            indicator:SetAlpha( startAlpha - SmoothStep( t ) * startAlpha )
        end
        ),
        function()
            indicator:SetVisibility( false )
        end,
        "ResistanceRequestsSyncPopup"
    )

    if callback then callback() end
end

function TimeoutRequestsIndicatorShow()

    timeoutIndicatorActive = true
    rootTimeout:SetAlpha( 0 )
    rootTimeout:SetVisibility( true ) 

    anim:Add( Slider:New( 0.3,
            function( t )
            rootTimeout:SetAlpha( SmoothStep( t ) * 255 )
        end),
             function() StartStarAnimation( rootTimeout:GetControl( "Icon/Star" ) ) end
        )
end

function TimeoutRequestsIndicatorHide( callback , instant )

    if timeoutIndicatorActive == false then return end

    timeoutIndicatorActive = false

    if instant == true then
        StopStarAnimation( rootTimeout:GetControl( "Icon/Star" )  )  
        rootTimeout:SetVisibility( false ) 
        if callback then callback() end
    else
        anim:Add( Slider:New( 0.3,
                    function( t )
                    rootTimeout:SetAlpha( 255 - SmoothStep( t ) * 255 )
                end ),
                    function() 
                        StopStarAnimation( rootTimeout:GetControl( "Icon/Star" )  )  
                        rootTimeout:SetVisibility( false )  
                        if callback then callback() end
                    end
                )
    end
end
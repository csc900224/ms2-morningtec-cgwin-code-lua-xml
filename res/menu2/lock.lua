Lock = nil
Unlock = nil
CheckLock = nil

do
    local lockgui = 0
    Lock = function() lockgui = lockgui + 1 end
    Unlock = function() lockgui = lockgui - 1 end
    CheckLock = function() return lockgui > 0 end
end

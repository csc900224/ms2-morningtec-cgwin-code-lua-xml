require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 300 )
local height = R( 270 )

local closeCallback = nil

FreeStuffRewards = {
    Facebook = { cash = 0, gold = 20 },
    Twitter = { cash = 0, gold = 20 },
    RateUs = { cash = 0, gold = 20 }
}

function FreeStuffPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_free_stuff.xml" )
    popup = screen:GetControl( "/FreeStuffPopup" )

    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    popup:SetVisibility( false )
end

function FreeStuffPopupFillTexts()
    FreeStuffPopupFillText( popup:GetControl( "Content/FB/Description" ), "TEXT_FREE_STUFF_LIKE", FreeStuffRewards.Facebook )
    FreeStuffPopupFillText( popup:GetControl( "Content/Twitter/Description" ), "TEXT_FREE_STUFF_FOLLOW", FreeStuffRewards.Twitter )
    FreeStuffPopupFillText( popup:GetControl( "Content/RateUs/Description" ), "TEXT_FREE_STUFF_RATE", FreeStuffRewards.RateUs )
end

function FreeStuffPopupFillText( control, textPrefix, reward )
    local rewardValue = 0
    local icon = ""
    
    if reward.cash > 0 then
        rewardValue = reward.cash
        icon = "TEXT_ICON_CASH"
    elseif reward.gold > 0 then 
        rewardValue = reward.gold
        icon = "TEXT_ICON_GOLD"
    end
    
    if rewardValue > 0 then
        control:SetTextWithVariables( textPrefix .. "_DESC", { VALUE = tostring( rewardValue ), ICON = icon } )
    else
        control:SetText( textPrefix .. "_DESC_NO_REWARD" )
    end    
end

function FreeStuffPopupShow( callback )
    active = true
    PopupLock()
    closeCallback = callback
    
    FreeStuffPopupFillTexts()
    FreeStuffUpdateButtonsState()
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function FreeStuffPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        FreeStuffPopupHide()
        return true
    end

    return false
end

function FreeStuffPopupPress( control, path )
    if not active then
        return
    end

    if path == "/FreeStuffPopup/Back/Button" then
        ButtonPress( control )
    elseif  path == "/FreeStuffPopup/Content/FB/Button" then
        registry:Set( "/monstaz/settings/free-stuff/fb-active", false )
        FreeStuffButtonPressCommon( FreeStuffRewards.Facebook.cash > 0 or FreeStuffRewards.Facebook.gold > 0 )
        callback:OnFacebookButton( FreeStuffRewards.Facebook.cash, FreeStuffRewards.Facebook.gold )
    elseif  path == "/FreeStuffPopup/Content/Twitter/Button" then
        registry:Set( "/monstaz/settings/free-stuff/twitter-active", false )
        FreeStuffButtonPressCommon( FreeStuffRewards.Twitter.cash > 0 or FreeStuffRewards.Twitter.gold > 0 )
        callback:OnTwitterButton( FreeStuffRewards.Twitter.cash, FreeStuffRewards.Twitter.gold )
    elseif  path == "/FreeStuffPopup/Content/RateUs/Button" then
        registry:Set( "/monstaz/settings/free-stuff/rateus-active", false )
        FreeStuffButtonPressCommon( FreeStuffRewards.RateUs.cash > 0 or FreeStuffRewards.RateUs.gold > 0 )
        callback:OnRateUsButton( FreeStuffRewards.RateUs.cash, FreeStuffRewards.RateUs.gold )
    end
end

function FreeStuffButtonPressCommon( reward )
    FreeStuffUpdateButtonsState()
    ButtonPressPlaySound()
    if reward == true then
        anim:Add(
            Delay:New( 3.0 ),
            function()
                AudioManager:Play( Sfx.SFX_CASH )
            end,
            nil
        )
    end
end

function FreeStuffPopupAction( name )
    if not active then
        return
    end

    if name == "/FreeStuffPopup/Back/Button" then
        FreeStuffPopupHide()
    end
end

function FreeStuffUpdateButtonsState()
    local fbActive = registry:Get( "/monstaz/settings/free-stuff/fb-active" )
    local twitterActive = registry:Get( "/monstaz/settings/free-stuff/twitter-active" )
    local rateUsActive = registry:Get( "/monstaz/settings/free-stuff/rateus-active" )  
    
    FreeStuffUpdateButtonState( "Content/FB/Button", fbActive )
    FreeStuffUpdateButtonState( "Content/Twitter/Button", twitterActive )
    FreeStuffUpdateButtonState( "Content/RateUs/Button", rateUsActive )
end

function FreeStuffIsActive()
    local fbActive = registry:Get( "/monstaz/settings/free-stuff/fb-active" )
    local twitterActive = registry:Get( "/monstaz/settings/free-stuff/twitter-active" )
    local rateUsActive = registry:Get( "/monstaz/settings/free-stuff/rateus-active" ) 
    return fbActive or twitterActive or rateUsActive
end

function FreeStuffUpdateButtonState( buttonPath, active )
    local button = popup:GetControl( buttonPath )
    local icon = button:GetControl( "Icon" )
    local label = button:GetControl( "Label" )

    button:SetRepresentation( active and "default" or "pressed" )
    button:SetTouchable( active )
    icon:SetAlpha( active and 255 or 128 )
    label:SetAlpha( active and 255 or 128 )
end

function FreeStuffPopupHide()
    active = false
    PopupUnlock()
    
    if closeCallback then
        closeCallback()
    end

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
    )
end

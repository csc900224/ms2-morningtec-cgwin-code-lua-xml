require( "menu2/common.lua" )

PopupFrameMargin = R(4)
PopupFrameTopMargin = R(12)

PopupFrameBlue = {
    corner1 = "menu2/corner_01.png@linear",
    corner2 = "menu2/corner_02.png@linear",
    fill1 = "menu2/corner_fill.png@linear",
    fill2 = "menu2/corner_fill_bottom.png@linear",
    color = { r = 2, g = 4, b = 37, a = 230 }
}

PopupFrameGreen = {
    corner1 = "menu2/corner_01_green.png@linear",
    corner2 = "menu2/corner_02_green.png@linear",
    fill1 = "menu2/corner_fill_green.png@linear",
    fill2 = "menu2/corner_fill_bottom_green.png@linear",
    color = { r = 1, g = 1, b = 5, a = 230 }
}

PopupFrameRed = {
    corner1 = "menu2/corner_01_red.png@linear",
    corner2 = "menu2/corner_02_red.png@linear",
    fill1 = "menu2/corner_fill_red.png@linear",
    fill2 = "menu2/corner_fill_bottom_red.png@linear",
    color = { r = 20, g = 1, b = 16, a = 230 }
}

PopupFrameGray = {
    corner1 = "menu2/corner_01_gray.png@linear",
    corner2 = "menu2/corner_02_gray.png@linear",
    fill1 = "menu2/corner_fill_gray.png@linear",
    fill2 = "menu2/corner_fill_bottom_gray.png@linear",
    color = { r = 10, g = 10, b = 10, a = 230 }
}

function PopupIsLocked( overlayPath )
    local overlay = screen:GetControl( overlayPath or "/Overlay" )
    return overlay:GetVisibility()
end

function PopupLock( time, overlayPath )
    local path = overlayPath or "/Overlay"
    local overlay = screen:GetControl( path )
    overlay:SetVisibility( true )
    local startAlpha = overlay:GetAlpha()
    anim:RemoveAll( "PopupLockAnim" )
    anim:Add( Slider:New( (time or 0.3) * (255-startAlpha)/255,
        function( t )
            overlay:SetAlpha( startAlpha + SmoothStep( t ) * (255-startAlpha) )
        end
        ),
        nil,
        "PopupLockAnim" .. path
    )
end

function PopupUnlock( time, overlayPath )
    local path = overlayPath or "/Overlay"
    local overlay = screen:GetControl( path )
    overlay:SetVisibility( true )
    local startAlpha = overlay:GetAlpha()
    anim:RemoveAll( "PopupLockAnim" )
    anim:Add( Slider:New( (time or 0.3) * startAlpha/255,
        function( t )
            overlay:SetAlpha( startAlpha - SmoothStep( t ) * startAlpha )
        end
        ),
        function()
            overlay:SetVisibility( false )
        end,
        "PopupLockAnim" .. path
    )
end

function PopupCreateFrame( root, width, height, assets, opaque )
    if assets == nil then
        assets = PopupFrameBlue
    end

    width = math.floor(width + 0.5)
    height = math.floor(height + 0.5)

    local fill = root:InsertControl("fill")

    local lt = root:InsertControl("lt")
    lt:AddRepresentation( "default", "menu2/timage.xml" )
    lt:SetRelative( true )
    lt:GetGraphic( "TS" ):SetImage( assets.corner1 )

    local lb = root:InsertControl("lb")
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( assets.corner2 )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_VERTICAL + flipModes.FM_HORIZONTAL )

    local rt = root:InsertControl("rt")
    rt:AddRepresentation( "default", "menu2/timage.xml" )
    rt:SetRelative( true )
    rt:GetGraphic( "TS" ):SetImage( assets.corner2 )

    local rb = root:InsertControl("rb")
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( assets.corner2 )
    rb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_VERTICAL )

    ltw, lth = lt:GetGraphic( "TS" ):GetSize()
    lbw, lbh = lb:GetGraphic( "TS" ):GetSize()
    rtw, rth = rt:GetGraphic( "TS" ):GetSize()
    rbw, rbh = rb:GetGraphic( "TS" ):GetSize()

    lb:SetY( height - lbh )
    rt:SetX( width - rtw )
    rb:SetX( width - rbw )
    rb:SetY( height - rbw )

    fill:AddRepresentation( "default", "menu2/rect.xml" )
    fill:SetRelative( true )
    fill:SetX( PopupFrameMargin )
    fill:SetY( opaque and PopupFrameTopMargin or lth )

    local r = fill:GetGraphic( "r" )
    r:SetWidth( width - 2*PopupFrameMargin )
    r:SetHeight( opaque and (height - PopupFrameMargin - PopupFrameTopMargin) or(height - lth - lbh + 1) )
    r:SetColor( assets.color.r, assets.color.g, assets.color.b, opaque and 255 or assets.color.a )

    local top = root:InsertControl("top")
    top:AddRepresentation( "default", "menu2/tile.xml" )
    top:SetRelative( true )
    top:SetX( ltw )
    top:GetGraphic( "s" ):SetImage( assets.fill1 )
    top:GetGraphic( "s" ):SetClipRect( 0, 0, width - ltw - rtw, lth )

    local bottom = root:InsertControl("bottom")
    bottom:AddRepresentation( "default", "menu2/tile.xml" )
    bottom:SetRelative( true )
    bottom:SetX( lbw )
    bottom:SetY( height - lbh )
    bottom:GetGraphic( "s" ):SetImage( assets.fill2 )
    bottom:GetGraphic( "s" ):SetClipRect( 0, 0, width - lbw - rbw, lbh )
end

function PopupChangeFrame( root, assets )
    root:GetControl("lt"):GetGraphic( "TS" ):SetImage( assets.corner1 )
    root:GetControl("lb"):GetGraphic( "TS" ):SetImage( assets.corner2 )
    root:GetControl("rt"):GetGraphic( "TS" ):SetImage( assets.corner2 )
    root:GetControl("rb"):GetGraphic( "TS" ):SetImage( assets.corner2 )
    root:GetControl("fill"):GetGraphic( "r" ):SetColor( assets.color.r, assets.color.g, assets.color.b, assets.color.a )

    local rX, rY, rW, rH  = root:GetControl("top"):GetGraphic( "s" ):GetClipRect()
    root:GetControl("top"):GetGraphic( "s" ):SetImage( assets.fill1 )
    root:GetControl("top"):GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH )

    rX, rY, rW, rH  = root:GetControl("bottom"):GetGraphic( "s" ):GetClipRect()
    root:GetControl("bottom"):GetGraphic( "s" ):SetImage( assets.fill2 )
    root:GetControl("bottom"):GetGraphic( "s" ):SetClipRect( rX, rY, rW, rH )
end


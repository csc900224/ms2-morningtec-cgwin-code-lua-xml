require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/sync_indicator.lua" )

local active = false
local popup = nil
local retryLock = false

local x = 0
local y = 0
local width = R( 300 )
local height = R( 190 )

function NoNetworkPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_no_network.xml" )
    popup = screen:GetControl( "/NoNetworkPopup" )

    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    if IS_IPHONE then
        local exit = popup:GetControl( "Exit" )
        exit:SetVisibility( false )
        
        local retry = popup:GetControl( "Retry" )
        local retryW, retryH = retry:GetControl( "Button" ):GetGraphic( "TS" ):GetSize()
        retry:SetX( (width - retryW)/2 )
    end
end

function NoNetworkPopupShowWithLock()
    NoNetworkPopupShow( true )
end

function NoNetworkPopupShow( shouldLock )
    if active and not IsSyncIndicatorVisible() then
        return
    end
    active = true
    retryLock = shouldLock
    SyncIndicatorHide()
    PopupLock( 0.3, "/NoNetworkPopup/Overlay" )

    popup:SetVisibility( true )
    local startY = popup:GetY()
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( startY - y ) )
        end
        )
    )
end

function NoNetworkPopupPress( c, path )
    if not active then return end
    
    if name == "/NoNetworkPopup/Retry/Button" or
       name == "/NoNetworkPopup/Exit/Button" then
        ButtonPress( c )
    end
end

function NoNetworkPopupAction( name )
    if not active then return end

    if name == "/NoNetworkPopup/Retry/Button" then
        NoNetworkPopupHide("retry")
    elseif name == "/NoNetworkPopup/Exit/Button" then
        NoNetworkPopupHide("exit")
    end
end

function NoNetworkPopupConnected()
    if active then
        msg = screen:GetControl("/ConnectingMessage")
        if msg then
            msg:SetVisibility( false )
        end
        NoNetworkPopupHide("continue")
        return
    end
    NoNetworkCallback:NoNetworkContinue()
end

function NoNetworkPopupHide(whatNext)
    local shouldUnlock = not retryLock or whatNext ~= "retry"
    
    if shouldUnlock then
        active = false
        PopupUnlock( 0.3, "/NoNetworkPopup/Overlay" )
        SyncIndicatorHide()
    end

    local startY = popup:GetY()
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( startY + SmoothStep( t ) * ( SCREEN_HEIGHT - startY ) )
        end
        ),
        function()
            if shouldUnlock then
                popup:SetVisibility(false)
            else
                SyncIndicatorShow(true)
            end
            if whatNext == "retry" then
                NoNetworkCallback:NoNetworkRetry()
            elseif whatNext == "exit" then
                NoNetworkCallback:NoNetworkClose()
            elseif whatNext == "continue" then
                NoNetworkCallback:NoNetworkContinue()
            end
        end
    )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil
local socialType = nil
local TypesOfSocial = { FB = "facebook" , GP = "googleplus" }

local x = 0
local y = 0
local width = R( 170 )
local height = R( 175 )

local username = ""
local socialId = ""

function RegisterSocialPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_register_social.xml" )
    popup = screen:GetControl( "/RegisterSocialPopup" )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( - SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

end

function RegisterSocialPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        RegisterSocialPopupHide( LoginMethodPopupShow )
        return true
    end

    return false
end

function RegisterSocialPopupAction( name )
    if not active then
        return
    end

    if name == "/RegisterSocialPopup/Register" then

        username = InputFields.username
        
        local currentSocialId = callback:ResistanceGetGpId()
        local isGp = socialType == TypesOfSocial.GP

        if not isGp then
            currentSocialId = callback:ResistanceGetFbId()
        end
      

        if not IsNameValid( username ) then
            RegisterSocialPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_6", function()  RegisterSocialPopupShow( currentSocialId , isGp )  end) end )
            return
        end

        if IsNameTaken( username ) then
            RegisterSocialPopupHide( function() ErrorPopupShow( "TEXT_FAIL_USERNAME_USED", function()  RegisterSocialPopupShow( currentSocialId , isGp ) end ) end )
            return
        end

        if isGp then
            RegisterSocialPopupHide()
            callback:ResistanceCreateUserViaFb( username , currentSocialId )
        elseif socialType == TypesOfSocial.FB then 
            local faceId = callback:ResistanceGetFbId()
            if faceId == "unknown" then
             -- wait for authenication
            else
                RegisterSocialPopupHide()
                callback:ResistanceCreateUserViaFb( username , faceId )
            end
        end
        
    elseif name == "/RegisterSocialPopup/Back" then
        RegisterSocialPopupHide( LoginMethodPopupShow )
    elseif name == "/RegisterSocialPopup/Username/UsernameInputButton" then
        SetInputTextToUpdate( popup:GetControl("Username/UsernameText"),"TEXT_RESISTANCE_USER_NAME", InputFieldsType.username )
        UpdateInput( username )
        callback:OpenVkb( username , InputFieldsMaxLenght.username ) 
    end
end

function RegisterSocialPopupPress( control, path )
    if not active then
        return
    end

    if path == "/RegisterSocialPopup/Register" or path == "/RegisterSocialPopup/Back" then
        ButtonPress( control )
    end
end

function RegisterSocialPopupShow( playerSocialId , isGooglePlus )
    active = true

    if isGooglePlus == true  then
        socialType = TypesOfSocial.GP
    else
        socialType = TypesOfSocial.FB
    end

    socialId = playerSocialId 

    callback:ResistanceRefreshInputs()
    SyncIndicatorHide()
    PopupRegisterSocialResetInputFields()
    popup:SetVisibility( true )
    
    anim:Add( Slider:New( 0.3,
        function( t )
                popup:SetY( - height + SmoothStep( t ) * ( y + height) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function RegisterSocialPopupActionMorningTec()

    active = true
    SyncIndicatorHide()

    local faceId = callback:ResistanceGetFbId()
    username = "useless"

    if faceId == "unknown" then
     -- wait for authenication
    else
        RegisterSocialPopupHide()
        callback:ResistanceCreateUserViaFb( username , faceId )
    end
end


function RegisterSocialPopupHide( callback )
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
        popup:SetY( y +  SmoothStep( t ) * ( y - SCREEN_HEIGHT) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end


function LoginViaFacebookSuccess( socialId )
   RegisterSocialPopupShow( socialId , false )
end

function LoginViaSocialFail()
   SyncIndicatorHide( LoginMethodPopupShow() )
end


function PopupRegisterSocialResetInputFields()
    username = ""
    InputFields.username = ""
    SetInputTextToUpdate( popup:GetControl("Username/UsernameText"),"TEXT_RESISTANCE_USER_NAME", InputFieldsType.username )
    UpdateInput( username )
end
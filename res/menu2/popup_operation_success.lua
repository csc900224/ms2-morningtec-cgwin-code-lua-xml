require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/star_animation.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 215 )

local iconFile = "menu2/cat_01.png@linear"

local callbackReturn = nil


function OperationSuccessPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_operation_success.xml" )
    popup = screen:GetControl( "/OperationSuccessPopup" )
    popup:SetVisibility( false )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

end

function OperationSuccessPopupFillData( iconPath , text )

    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconPath )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )

    local message =  popup:GetControl( "Message" )
    message:SetText( text )
end

function OperationSuccessPopupShow( text , callback )
    if active then
        return
    end

    SyncIndicatorHide()
    active = true

    callbackReturn = callback

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    OperationSuccessPopupFillData( iconFile , text )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
end

function OperationSuccessPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        OperationSuccessPopupHide( callbackReturn )
        callbackReturn = nil
        return true
    end

    return false
end

function OperationSuccessPopupPress( control, path )
    if not active then
        return
    end

    if path == "/OperationSuccessPopup/OK/Button" then
        ButtonPress( control )
    end
end

function OperationSuccessPopupAction( name )
    if not active then
        return
    end

    if name == "/OperationSuccessPopup/OK/Button" then
        OperationSuccessPopupHide( callbackReturn )
        callbackReturn = nil
    end
end

function OperationSuccessPopupHide( callback )
    active = false

    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Icon/Star" ) )
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

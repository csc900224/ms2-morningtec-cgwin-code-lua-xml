require( "menu2/common.lua" )
require( "menu2/animation.lua" )
require( "menu2/delay.lua" )
require( "menu2/slider.lua" )
require( "menu2/perpetual.lua" )
require( "menu2/math.lua" )

require( "menu2/popup.lua" )
require( "menu2/popup_no_network.lua" )

local baseText = nil
local timer = 0
local counter = 0
local text = nil
local dir = 1

function Screen:Update( dt )
    anim:Update( dt )
    
    timer = timer + dt
    if timer > 0.3 then
        counter = counter + dir * 1
        if counter >= 3 or counter <= 0 then
            dir = -dir
        end
        timer = 0
        
        txt = baseText
        for i=1, counter do
            txt = " " .. txt .. "."
        end
        text:SetTextRaw( txt );
    end
end

function Screen:OnKeyDown( code )
end

function Screen:OnTouchDown( x, y, id )
end

function Screen:OnTouchUp( x, y, id )
    local c = self:GetTouchableControl( x, y )
    if not c then
        return
    end

    local path = c:GetPath()
    NoNetworkPopupAction(path)
end

function Screen:OnTouchMove( x, y, id )
end

function CalculateLayout()
    logo = screen:GetControl("/GlLogo")
    w, h = logo:GetGraphic( "TS" ):GetSize()
    x = (SCREEN_WIDTH - w) / 2
    y = (SCREEN_HEIGHT - h) / 2
    logo:SetX(x)
    logo:SetY(y)

    text = screen:GetControl("/ConnectingMessage")
    baseText = screen:GetText( "TEXT_28" )
    text:SetTextRaw( baseText )
    text:SetX( (SCREEN_WIDTH - text:GetTextWidth()) / 2 )
    text:SetY( y + h )
end

screen:GetControl( "/" ):InsertFromXml( "menu2/no_network.xml" )
NoNetworkPopupInit( screen )
CalculateLayout()

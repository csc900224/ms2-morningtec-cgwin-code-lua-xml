function TutorialFingerShow( path, name, x, y, delay, press )
    local parent = screen:GetControl( path )
    local finger = parent:InsertControl( name )
    finger:AddRepresentation( "default", "menu2/timage.xml" )
    finger:SetRelative( true )
    finger:GetGraphic( "TS" ):SetImage( "menu2/finger.png@linear" )
    finger:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    finger:GetGraphic( "TS" ):SetPivot( finger:GetWidth()/2, finger:GetHeight()/2 )

    x = x - finger:GetWidth()/2
    y = y + finger:GetHeight()/2

    local offset = R(50)
    local fullpath = finger:GetPath()

    local animation = nil
    animation = function()
        finger:SetAlpha( 0 )
        finger:SetX( x - offset )
        finger:SetY( y + offset )
        finger:GetGraphic( "TS" ):SetAngle( 0 )

        anim:RemoveAll( fullpath )
        anim:Add(
            Slider:New(
                1,
                function( t )
                    finger:SetAlpha( 255*SmoothStep( t*2 ) )
                    finger:SetX( x - offset*( 1 - SmoothStep( t ) ) )
                    finger:SetY( y + offset*( 1 - SmoothStep( t ) ) )
                end
            ),
            function()
                anim:Add(
                    Slider:New(
                        0.3,
                        function( t )
                            finger:GetGraphic( "TS" ):SetAngle( SmoothStep( t )*math.pi/16 )
                        end
                    ),
                    function()
                        animation()
                    end,
                    fullpath
                )

                if press then
                    anim:Add(
                        Delay:New( 0.1 ),
                        function()
                            ButtonPress( parent, nil, false )
                        end,
                        fullpath
                    )
                end

                anim:Add(
                    Delay:New( 0.2 ),
                    function()
                        anim:Add(
                            Slider:New(
                                0.1,
                                function( t )
                                    finger:SetAlpha( 255*( 1 - SmoothStep( t ) ) )
                                end
                            )
                        )
                    end,
                    fullpath
                )
            end,
            fullpath
        )
    end

    if delay and delay > 0 then
        finger:SetVisibility( false )
        anim:Add(
            Delay:New( delay ),
            function()
                finger:SetVisibility( true )
                animation()
            end,
            fullpath
        )
    else
        animation()
    end
end

function TutorialFingerHide( path )
    anim:RemoveAll( path )

    local finger = screen:GetControl( path )
    screen:RemoveControl( finger )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 240 )
local height = R( 250 )

local pass = ""
local username = ""
local email = ""

local loginInputControl =  nil
local passwordInputControl =  nil
local emailInputControl =  nil

function RegisterPopupInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_register.xml" )
    popup = screen:GetControl( "/RegisterPopup" )
    
    
    
    x = math.floor(( SCREEN_WIDTH - width )/2)
    y = math.floor(( SCREEN_HEIGHT - height )/2)

    popup:SetX( x )
    popup:SetY( - SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
    
    HelpButtonInit( popup , width )
    
    loginInputControl =  popup:GetControl("Login/LoginText")
    passwordInputControl =  popup:GetControl("Pass/PassText")
    emailInputControl =  popup:GetControl("Email/EmailText")



    local loginInputButton = popup:GetControl("Login/LoginInputButton")
    local lW = loginInputButton:GetWidth()

    local loginInput = popup:GetControl("Login")
    local passInput = popup:GetControl("Pass")
    local emailInput = popup:GetControl("Email")

    local loginInputImg = popup:GetControl("LoginImg")
    local passInputImg = popup:GetControl("PassImg")
    local emailInputImg = popup:GetControl("EmailImg")

    loginInputImg:SetX( ( width - lW ) /2 )
    passInputImg:SetX( ( width - lW ) /2 )
    emailInputImg:SetX( ( width - lW ) /2 )

    loginInput:SetX( ( width - lW ) /2 )
    passInput:SetX( ( width - lW ) /2 )
    emailInput:SetX( ( width - lW ) /2 )

    local registerBtn = popup:GetControl("RegisterSend")
    local backButton = popup:GetControl("RegisterBack")
    local loginButton = popup:GetControl("LoginButton")


    local w, h = backButton:GetGraphic( "TS" ):GetSize()

    registerBtn:SetX( ( width - w ) /2 )
    backButton:SetX( ( width - w ) /2 )
    loginButton:SetX( ( width - loginButton:GetWidth() ) /2 )
end

function RegisterPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        RegisterPopupHide( LoginMethodPopupShow )
        return true
    end

    return false
end

function RegisterPopupAction( name )
    if not active then
        return
    end

    if name == "/RegisterPopup/RegisterSend" then

        username = InputFields.username
        pass = InputFields.password
        email = InputFields.email
        
        --username = "login2"
        --pass = "login2"
        --email = "login1@game-lion.com"
        
        if not IsNameValid( username ) then
            RegisterPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_6", RegisterPopupShow ) end )
            return
        end

        if IsNameTaken( username ) then
            RegisterPopupHide( function() ErrorPopupShow( "TEXT_FAIL_USERNAME_USED", RegisterPopupShow ) end )
            return
        end
        
        if  not IsPassValid ( pass ) then
            RegisterPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_4", RegisterPopupShow ) end)
            return
        end

        if not IsEmailValid( email ) then
            RegisterPopupHide( function() ErrorPopupShow( "TEXT_RESISTANCE_5", RegisterPopupShow ) end )
            return
        end

        RegisterPopupHide()    
        callback:ResistanceCreateUser( username, email, pass )
        
    elseif name == "/RegisterPopup/RegisterBack" then
        RegisterPopupHide( LoginMethodPopupShow )
    elseif name == "/RegisterPopup/LoginButton" then
        RegisterPopupHide( LoginPopupShow )
    elseif name == "/RegisterPopup/Login/LoginInputButton" then
        SetInputTextToUpdate( loginInputControl, "TEXT_RESISTANCE_USER_NAME", InputFieldsType.username )
        UpdateInput( username )
        callback:OpenVkb( username , InputFieldsMaxLenght.username )
    elseif name == "/RegisterPopup/Pass/PassInputButton" then
        SetInputTextToUpdate( passwordInputControl , "TEXT_RESISTANCE_PASSWORD", InputFieldsType.password )
        UpdateInput( pass )
        callback:OpenVkb( pass , InputFieldsMaxLenght.password )
    elseif name == "/RegisterPopup/Email/EmailInputButton" then
        SetInputTextToUpdate( emailInputControl , "TEXT_RESISTANCE_EMAIL", InputFieldsType.email )
        UpdateInput( email )
        callback:OpenVkb( email , InputFieldsMaxLenght.email )
    end
end

function RegisterPopupPress( control, path )
    if not active then
        return
    end

    if path == "/RegisterPopup/RegisterBack" then
        ButtonPress( control )
    elseif path == "/RegisterPopup/RegisterSend" then
        ButtonPress( control )
    end
end

function RegisterPopupShow()
    active = true
    PopupRegisterResetInputFileds()
    callback:ResistanceRefreshInputs()
    popup:SetVisibility( true )
    
    anim:Add( Slider:New( 0.3,
        function( t )
                popup:SetY( - height + SmoothStep( t ) * ( y + height) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function RegisterPopupHide( callback )
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
        popup:SetY( y +  SmoothStep( t ) * ( y - SCREEN_HEIGHT) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function AccountCreateSuccess()
    OperationSuccessPopupShow( "TEXT_SUCCESS_CREATE_ACCOUNT" , PopupUnlock )
end

function AccountCreateFailed( isNameAlreadyUsed , viaSocial , socialId , viaGooglePlus )

    if isNameAlreadyUsed == true then 
        if viaSocial == true then
            ErrorPopupShow( "TEXT_FAIL_USERNAME_USED" , function() RegisterSocialPopupShow( socialId , viaGooglePlus ) end )
        else
            ErrorPopupShow( "TEXT_FAIL_USERNAME_USED" , RegisterPopupShow )
        end
    else
        ErrorPopupShow( "TEXT_FAIL_EMAIL_USED" , RegisterPopupShow )
    end
end

function AccountCreateFailedBadWords( viaSocial , socialId , viaGooglePlus )
    if viaSocial == true then
        ErrorPopupShow( "TEXT_FAIL_USERNAME_BADWORD" , function() RegisterSocialPopupShow( socialId , viaGooglePlus ) end )
    else
        ErrorPopupShow( "TEXT_FAIL_USERNAME_BADWORD" , RegisterPopupShow )
    end
end


function AccountCreateFailedNoNetwork()
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" )
end

function PopupRegisterResetInputFileds()

    username = ""
    pass = ""
    email= ""
        
    InputFields.username = ""
    InputFields.password = ""
    InputFields.email = ""
        
    SetInputTextToUpdate( loginInputControl, "TEXT_RESISTANCE_USER_NAME", InputFieldsType.username )
    UpdateInput( username )

    SetInputTextToUpdate( passwordInputControl , "TEXT_RESISTANCE_PASSWORD", InputFieldsType.password )
    UpdateInput( pass )

    SetInputTextToUpdate( emailInputControl , "TEXT_RESISTANCE_EMAIL", InputFieldsType.email )
    UpdateInput( email )

end



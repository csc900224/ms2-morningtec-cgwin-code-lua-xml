require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/help.lua" )

local active = false
local root = nil
local button = nil

local buttonMarginX = R( 12 )
local buttonMarginY = R( 11 )

function HelpButtonInit( parent , width )

    parent:InsertFromXml( "menu2/help_button.xml" )

    root = parent
    button =  root:GetControl( "HelpButton" )
    
    local w, h = button:GetGraphic( "s" ):GetSize()

    button:SetX( width - buttonMarginX - w )
    button:SetY ( buttonMarginY )

    active = true
end

function HelpButtonInitBottomLeft( parent , height, margin )
    if margin == nil then
        margin = {
            x = buttonMarginX,
            y = buttonMarginY
        }
    end

    parent:InsertFromXml( "menu2/help_button.xml" )

    root = parent
    button =  root:GetControl( "HelpButton" )
    
    local w, h = button:GetGraphic( "s" ):GetSize()

    button:SetY( height - margin.y - h )
    button:SetX ( margin.x )

    active = true
end

function HelpButtonAction( c, name )

    if not active then
        return
    end

    if string.find( name, "/HelpButton" ) then
            OnHelpShow()
            local path = c:GetPath()
            ClawMsg( name ) 

             local helpScreen = 0

            if path == "/FriendInfo/HelpButton" then
                helpScreen = HelpScreen.HelpFriendInfo
            elseif path == "/RegisterPopup/HelpButton" then
                helpScreen = HelpScreen.HelpRegister
            elseif path == "/LoginPopup/HelpButton" then
                helpScreen = HelpScreen.HelpLogin	
            elseif path == "/LoginMethodPopup/HelpButton" then
                helpScreen = HelpScreen.HelpLoginMethod
            elseif path == "/Survival/HelpButton" then
                SurvivalListResetScroll()
                helpScreen = HelpScreen.HelpSurvival
            elseif path == "/Social/ResistanceHq/HelpButton" then
                FriendsListResetScroll()
                helpScreen = HelpScreen.HelpFriends
            elseif path == "/Social/ResistanceAccount/HelpButton" then
                helpScreen = HelpScreen.HelpAccount
            elseif path == "/BackupTeam/HelpButton" then
                BackupListResetScroll()
                helpScreen = HelpScreen.HelpBackupTeam
            elseif path == "/Premission/Premission/Mission/HelpButton" then
                helpScreen = HelpScreen.HelpPremissionMain
            elseif path == "/InvitePopup/HelpButton" then
                helpScreen = HelpScreen.HelpInvitePopup
            end

            HelpOnStart( helpScreen )
    end

end

function HelpButtonPress( control, path )

    if not active then
        return
    end

    if string.find( path, "/HelpButton" ) then
        if control:GetRepresentation() == "default" then
            ButtonPress( control )
        end
    end
end

require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil
local scroll = nil

local x = 0
local y = 0
local width = SCREEN_WIDTH
local height = SCREEN_HEIGHT - R(50)



function HelpPopupInit( screen, isingame )
    ingame = isingame

    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_help.xml" )
    popup = screen:GetControl( "/HelpPopup" )

    x = 0
    y = 0


    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

   
end







function HelpPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        HelpPopupHide()
        return true
    end

    return false
end

function HelpPopupPress( control, path )
    if not active then
        return
    end

    if path == "/HelpPopup/Back" then
        HelpPopupHide()
    end
end

function HelpPopupAction( control, path )
    if not active then
        return
    end

    if path == "/HelpPopup/Back" then
        HelpPopupHide()
    end
end

function HelpPopupShow()
    active = true
    callback:Playhaven( "options" )
    PopupLock()
    SettingsUpdateVersion()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )

    SettingsTab( 1 )
end

function HelpPopupHide()
    active = false
    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

function SettingsUpdateVersion()
    if versionText then
        versionText:SetText( callback:GetVersionString() )
    end
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/avatar_social.lua" )

local margin = R(6)
local leftMargin = R(20)
local startY = R( 22 )

local item = nil

function InboxItemInit( parent, width, height , friend )

    if not friend then return end
    -- Insert from xml and get item root
    parent:InsertFromXml( "menu2/inbox_item.xml" )
    root = parent:GetControl( "InboxItem" )
    
    -- bg gradient and bottom line creation
    local bgGradient = parent:GetControl( "InboxItem/Bg" )


    local bgGradientW, bgGradientH = bgGradient:GetGraphic( "s" ):GetSize()
    bgGradient:SetX( margin )
    bgGradient:SetY( height - bgGradientH )
    bgGradient:GetGraphic( "s" ):SetClipRect( 0, 0, width - 2 * margin , bgGradientH )

    local bottomUnderline = parent:GetControl( "InboxItem/BottomLine" )
    bottomUnderline:SetX( 0 )
    CreateInboxItemBottomLine( bottomUnderline, width, height )

    local playerAvatarPath = GetAvatarImgPath( friend )

    local avatar = parent:GetControl( "InboxItem/InfoRoot/SocialInfo/Avatar" )
    local scale  =  1

    avatar:GetGraphic( "TS" ):SetImage( playerAvatarPath )
    local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
    
    scale = height / ( ah + 2* margin)
    avatar:GetGraphic( "TS" ):SetScale( scale )

    avatar:SetX( leftMargin )
    avatar:SetY(  ( height - ah*scale )/2  )

    -- bg - avatar img bottom line
    -- local bg = parent:GetControl( "InboxItem/InfoRoot/SocialInfo/BG" )
    -- bg:SetX( leftMargin + R(2) )
    -- bg:SetY( avatar:GetY() )
    -- CreateInboxAvatarBottomLine( bg, aw * scale - R(4), ah * scale )




    -- img version
    local nameImg = parent:GetControl( "InboxItem/InfoRoot/SocialInfo/NameImage" )
    callback:ResistanceSetUsernameImg( nameImg:GetPath() , friend.Name )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 
    nameImg:SetX( avatar:GetX() + aw * scale + 3 * margin )
    nameImg:SetY( ( height - h )/2  )
    nameImg:SetVisibility( true )

    -- vip frame
    local vip_frame = parent:GetControl( "InboxItem/InfoRoot/SocialInfo/VIP1" )
    vip_frame:SetX( avatar:GetX() )
    vip_frame:SetY( avatar:GetY() )

    local vip_badge = parent:GetControl( "InboxItem/InfoRoot/SocialInfo/VIP2" )
    vip_badge:SetX( avatar:GetX() +( aw * scale - parent:GetControl( "InboxItem/InfoRoot/SocialInfo/VIP2/Badge"):GetWidth() ) /2)
    vip_badge:SetY( avatar:GetY() + ah * scale - parent:GetControl( "InboxItem/InfoRoot/SocialInfo/VIP2/Badge"):GetHeight()/2  )

    local isFriendVip = tonumber( friend.IsVip ) == 1
    vip_frame:SetVisibility( isFriendVip )
    vip_badge:SetVisibility( isFriendVip )

    -- infoItem
    local infoItem = parent:GetControl( "InboxItem/InfoRoot/SocialInfo/ItemInfo" )
    infoItem:SetText( "TEXT_RESISTANCE_INBOX_INVITE" )
    infoItem:SetX( 2 * width / 5 )
    infoItem:SetY( ( height - infoItem:GetHeight() )/2 )

    -- buttons
    local acceptButton = parent:GetControl( "InboxItem/InfoRoot/Buttons/Accept" )
    local rejectButton = parent:GetControl( "InboxItem/InfoRoot/Buttons/Reject" )

    local buttonW , buttonH = acceptButton:GetGraphic( "TS" ):GetSize()
    local rejectButtonW , rejectButtonH = rejectButton:GetGraphic( "TS" ):GetSize()

    local marginName = R(8)


    rejectButton:SetX( width -  rejectButtonW - marginName - margin)
    rejectButton:SetY( ( height - buttonH )/2 )

    acceptButton:SetX( width - 2*rejectButtonW - 3*marginName - margin )
    acceptButton:SetY( ( height - buttonH )/2 )

    acceptButton:InsertControl( parent:GetName() )
    rejectButton:InsertControl( parent:GetName() )
end

local itemSelected = nil

function InboxItemAction( control, path )

    if string.find( path, "/Accept" ) then
        local idxInvite = tonumber ( control:GetChildren()[1]:GetName() )
        local invite = Invitations[  idxInvite  ]

        AcceptInvitation( invite )
        RemoveInvitation ( idxInvite ) 

    elseif string.find( path, "/Reject" ) then
        local idxInvite = tonumber ( control:GetChildren()[1]:GetName() )
        local invite = Invitations[ idxInvite ]

        RejectInvitation( invite )
        RemoveInvitation( idxInvite ) 
    end

end

function CreateInboxItemBottomLine( root,width,height )

    local lb = root:InsertControl("lb")
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( LineBlue.lineEnd )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )

    lbw, lbh = lb:GetGraphic( "TS" ):GetSize()
    rbw, rbh = lb:GetGraphic( "TS" ):GetSize()

    lb:SetX( 0 )
    lb:SetY( height - lbh/2 )

    local bottom = root:InsertControl("bottom")
    bottom:AddRepresentation( "default", "menu2/tile.xml" )
    bottom:SetRelative( true )

    bottom:SetX( lbw )
    bottom:SetY( height - lbh/2 )

    bottom:GetGraphic( "s" ):SetImage( LineBlue.lineFill )
    bottom:GetGraphic( "s" ):SetClipRect( 0, 0, width - 2*lbw , lbh )

    local rb = root:InsertControl("rb")
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( LineBlue.lineEnd )
    rb:SetY( height - lbh/2 )
    rb:SetX( width - lbw )
end

function CreateInboxAvatarBottomLine( root , width , height , scale )

    local lb = root:InsertControl("lb")
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( LineBlue.lineEnd )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )

    local bottom = root:InsertControl("bottom")
    bottom:AddRepresentation( "default", "menu2/tile.xml" )
    bottom:SetRelative( true )
    bottom:GetGraphic( "s" ):SetImage( LineBlue.lineFill )

    local rb = root:InsertControl("rb")
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( LineBlue.lineEnd )


    lbw, lbh = lb:GetGraphic( "TS" ):GetSize()
    rbw, rbh = lb:GetGraphic( "TS" ):GetSize()

    lb:SetX( -lbw )
    lb:SetY( height - lbh/2 )

    bottom:GetGraphic( "s" ):SetClipRect( 0, 0, width+R(1), lbh )
    bottom:SetX( 0 )
    bottom:SetY( height - lbh/2 )

    rb:SetY( height - lbh/2)
    rb:SetX( width )
end

function AcceptInvitation( invitation )

    --AddFriend( invitation.UniqueId, invitation.Name , invitation.Level , invitation.FbId , "0" )
    --AddFriend( name , level , fbId, kills , events , weaponId)
    callback:ResistanceOnAcceptedInvite( invitation.UniqueId , invitation.Name , invitation.Level , invitation.FbId )
    
end

function RejectInvitation( invitation )
    local fromUserId = invitation.UniqueId;
    callback:ResistanceOnRejectInvite( fromUserId )
end

function FriendResponseSuccess()
    SyncIndicatorHide()
    ResistanceRequestsHide( function() ResistanceRequestsShow() end )
    PopupUnlock()
end

function FriendResponseFailed()
    SyncIndicatorHide()
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" )
end


Delay = {}

function Delay:New( time )
    local obj = { t = time }

    setmetatable( obj, { __index = Delay } )
    return obj
end

function Delay:Play( dt )
    self.t = self.t - dt
end

function Delay:IsFinished()
    return self.t < 0
end

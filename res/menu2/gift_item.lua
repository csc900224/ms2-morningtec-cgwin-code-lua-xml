require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/avatar_social.lua" )

local margin = R( 5 )
local startY = R( 9 )

local item = nil
local iconFileFuel = "menu2/fuel_ico.png@linear"
local iconFileCash = "menu2/cash_ico.png@linear"
local iconFileGold = "menu2/gold_ico.png@linear"

function GiftItemInit( parent, width, height, friend )

    if not friend then return end
    -- Insert from xml and get item root
    parent:InsertFromXml( "menu2/gift_item.xml" )
    root = parent:GetControl( "GiftItem" )

    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    -- avatar img 
    local playerAvatarPath = GetAvatarImgPath( friend )

    local avatar = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Avatar" )
    local scale  = SetAvatarImg( avatar , playerAvatarPath )
    

    local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
    avatar:SetX( ( width - aw*scale ) /2)
    avatar:SetY( startY + margin )

    -- vip frame
    local vip_frame = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/VIP1" )
    vip_frame:SetX( ( width - aw*scale ) /2)
    vip_frame:SetY( startY + margin )

    local vip_badge = parent:GetControl( "GiftItem/InfoRoot/VIP2" )
    vip_badge:SetX( ( width - parent:GetControl( "GiftItem/InfoRoot/VIP2/Badge"):GetWidth() ) /2)
    vip_badge:SetY( startY + ah * scale )

    local isFriendVip = tonumber(friend.IsVip) == 1
    vip_frame:SetVisibility( isFriendVip )
    vip_badge:SetVisibility( isFriendVip )

    -- level info 
    local lvlInfo  = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/LevelInfo")
    lvlInfo:GetControl( "TextLevel"):SetText("TEXT_LVL")
    lvlInfo:GetControl( "LevelValue"):SetText( friend.Level )
    lvlInfo:SetY( startY - R(2) )
    lvlInfo:SetX( avatar:GetX() - R( 12 ) )

    -- gifts from gamelion have no level
    if IsGamelionUser( friend ) then
        lvlInfo:SetVisibility( false )
    end

    -- bg - avatar img bottom line
    local bg = parent:GetControl( "GiftItem/InfoRoot/BG" )
    bg:SetX( ( width - aw * scale )/2 )
    CreateBottomLine( bg, aw * scale, startY + ah * scale )

    -- player Name
    -- text vesrion disabled - used for debug 
    local name = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Name" )
    name:SetY( startY + ah * scale + 2.5 * margin )
    name:SetTextWidth( width );
    name:SetText( friend.Name )
    name:SetVisibility( false )

    -- img version
    local nameImg = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/NameImage" )

    callback:ResistanceSetUsernameImg( nameImg:GetPath() , friend.Name )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 
    local scaleName = 1
    if w > width then
        scaleName =  width / (w + R(6))
        nameImg:GetGraphic( "TS" ):SetScale( scaleName, 1 )
    end

    nameImg:SetX( ( width - w*scaleName )/2)
    nameImg:SetY( startY + ah * scale + 2.5 * margin )
    nameImg:SetVisibility( true )

    local label = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Label" )
    local times = friend.Count

    if friend.GiftType == GiftType.giftFuelPack or friend.GiftType == GiftType.giftGamelionGold or friend.GiftType == GiftType.giftGamelionCash then
        if times == 1 then
            label:SetText("TEXT_GIFT_ITEM_LABEL")
        else
            label:SetTextWithVariables( "TEXT_GIFT_ITEM_LABEL_MANY", { VALUE = tostring( times ) } )
        end
    else
        if times == 1 then
            label:SetText("TEXT_REWARD_ITEM_LABEL_ONE")
        else
            label:SetTextWithVariables( "TEXT_REWARD_ITEM_LABEL_MANY", { VALUE = tostring( times ) } )
        end
    end

    label:SetX( ( width - label:GetWidth() )/2 )
    label:SetY( nameImg:GetY() + margin + nameImg:GetHeight() )

    local val = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon/Value" )
    local margin = R(8)
    local w = 0
    local h = 0
    local wholeW = 0

    if registry:Get( "/monstaz/subscription" ) == true then
    -- vip users - cash instead fuelpacks
        if friend.GiftType == GiftType.giftFuelPack then
            val:SetText("+" .. tostring( friend.Quantity * CashForFuelRatio ) )
        else
            val:SetText("+" .. tostring( friend.Quantity) )
        end
    else
        -- normal usage
        val:SetText("+" .. tostring( friend.Quantity) )
    end

    val:SetTextWidth( val:GetTextWidth() )
    val:SetX(0)
    val:SetY(0)

    local icon = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon/Image" )
    local icon2 = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon/Image2" )
    local iconCash = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon/ImageCash" )
    local iconGold = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon/ImageGold" )

    iconCash:GetGraphic( "TS" ):SetImage( iconFileCash )
    icon:GetGraphic( "TS" ):SetImage( iconFileFuel )
    icon2:GetGraphic( "TS" ):SetImage( iconFileFuel )
    iconGold:GetGraphic( "TS" ):SetImage( iconFileGold )

    if friend.GiftType == GiftType.giftFuelPack then
        iconGold:SetVisibility( false )
        if registry:Get( "/monstaz/subscription" ) == true then
            icon:SetVisibility( false )
            icon2:SetVisibility( false )
            iconGold:SetVisibility( false )
            w, h = iconCash:GetGraphic( "TS" ):GetSize()
            iconCash:SetY( R(4) )
            iconCash:SetX( val:GetX() + val:GetWidth() + margin/2 )
            wholeW = val:GetWidth() + margin/2 + w
        else
            w, h = icon:GetGraphic( "TS" ):GetSize()
            icon:SetVisibility( true )
            icon2:SetVisibility( true )
            icon:SetX( val:GetX() + val:GetWidth() + margin + R(5) )
            icon:SetY( - R(3) )
            icon2:SetX( val:GetX() + val:GetWidth() + margin - R(5) )
            icon2:SetY( R(2) )
            iconCash:SetVisibility( false )
            wholeW = val:GetWidth() + margin + R(5) + w
        end
    elseif friend.GiftType == GiftType.giftGamelionGold then
        icon:SetVisibility( false )
        icon2:SetVisibility( false )
        iconCash:SetVisibility( false )
        iconGold:SetVisibility( true )
        w, h = iconGold:GetGraphic( "TS" ):GetSize()
        iconGold:SetY( R(6) )
        iconGold:SetX( val:GetX() + val:GetWidth() + margin/2 - R(1) )
        wholeW = val:GetWidth() + margin/2 + w
    else
        icon:SetVisibility( false )
        icon2:SetVisibility( false )
        iconGold:SetVisibility( false )
        w, h = iconCash:GetGraphic( "TS" ):GetSize()
        iconCash:SetY( R(4) )
        iconCash:SetX( val:GetX() + val:GetWidth() + margin/2 )
        wholeW = val:GetWidth() + margin/2 + w
    end

    local star = parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon/Star")
    local wS, hS = star:GetGraphic( "TS" ):GetSize()
    star:SetX( ( wholeW - wS ) / 2 )
    star:SetY( ( val:GetHeight() - hS ) / 2 )
    parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon"):SetX( (width - wholeW) / 2 )
    parent:GetControl( "GiftItem/InfoRoot/GiftInfo/Icon"):SetY( nameImg:GetY() + R(55) )

end

function GiftItemGetRoot( c )
    local root = c
    while root and root:GetName() ~= "GiftItem" do
        root = root:GetControl( ".." )
    end
    return root
end


require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "ResistanceDB.lua" )

local active = false
local root = nil
local avatarRoot = nil

local x = 0
local y = 0
local width = SCREEN_WIDTH - R(10)
local height = SCREEN_HEIGHT - R(60)


local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local pass = ""
local passVerify = ""
local email = ""

function ResistanceAccountInit( parent )
    parent:InsertFromXml( "menu2/resistance_account.xml" )

    root = parent:GetControl( "ResistanceAccount")
    x = ( SCREEN_WIDTH - width )/2
    y = R(10)

    root:SetX( x )
    root:SetY( y )
    root:SetVisibility( false )
    
    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    avatarRoot = root:GetControl( "UserInfoRoot/AvatarRoot" )
    avatarRoot:InsertFromXml( "menu2/social_item.xml" )

    local btnLogout = root:GetControl( "Logout" ) 
    local btnTOU = root:GetControl( "TermsOfUse" ) 
    local btnBack = root:GetControl( "Back" ) 

    local bw, bh = btnLogout:GetGraphic( "TS" ):GetSize()

    local topMargin = R(20)
    local sideMargin = R(35)
    local bottomMargin = R(6)

    btnTOU:SetY( height + bottomMargin  )
    btnTOU:SetX(  width - sideMargin - bw )

    btnBack:SetY( height + bottomMargin  )
    btnBack:SetX( sideMargin )

    btnLogout:SetY( height + bottomMargin )
    btnLogout:SetX( ( width - bw )/2 ) 

    if not IS_DEBUG then
        btnLogout:SetVisibility( false )
    end
    -- header position
    local header = root:GetControl("AccountHeader")

    header:SetX( ( width - header:GetWidth() )/2 )
    header:SetY( bottomMargin )

    local emailInputField = root:GetControl( "Email" )
    local widthInput = emailInputField:GetControl( "EmailText" ):GetWidth()

    local passInputField = root:GetControl( "Pass" )
    local passVerifyInputField = root:GetControl( "PassVerify" )
    
    local margin = R(12)

    emailInputField:SetX( width - margin - widthInput )
    emailInputField:GetControl("EmailLabel"):SetText( "TEXT_RESISTANCE_EMAIL_LABEL" )

    passInputField:SetX( width - margin - widthInput )
    passInputField:GetControl("PassLabel"):SetText("TEXT_RESISTANCE_PASSWORD_LABEL")

    passVerifyInputField:SetX( width - margin - widthInput )
    passVerifyInputField:GetControl( "PassVerifyLabel" ):SetText( "TEXT_RESISTANCE_VERIFY_LABEL" )

    root:GetControl( "Save" ):SetX( passVerifyInputField:GetX() + ( widthInput - bw )/2 )
    root:GetControl( "Save" ):SetY( passVerifyInputField:GetY() + 3 * margin )

    -- help init
    HelpButtonInit( root , width )

end


local passOnEnter = nil
local emailOnEnter = nil

function ResistanceAccountKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        ResistanceAccountHide( ResistanceHqShow )
        return true
    end

    return false
end

function ResistanceAccountAction( name )
    if not active then
        return
    end

    if string.find( name,"/ResistanceAccount/Save" )then

        email = InputFields.email
        pass = InputFields.password
        passVerify = InputFields.passwordVerify

        --email = "bbb2b5hsshert@com.pl"
        --pass = "haslo1"
        --passVerify = "haslo1"

        local passNotChanged =  ( (pass == "") ) or ( pass == passOnEnter ) 
        local emailNotChanged =  ( (email == "") ) or ( email == emailOnEnter )

        if not passNotChanged then
            if not IsPassValid ( pass ) then 
                 ErrorPopupShow( "TEXT_RESISTANCE_4", function() PopupUnlock() end  )
                return
            end

            if not ( pass == passVerify ) then
                 ErrorPopupShow( "TEXT_RESISTANCE_VERIFY_ERORR", function() PopupUnlock() end  )
                return
            end
        end

        if not emailNotChanged then
            if not IsEmailValid( email ) then
                --ResistanceHqHide( function() ErrorPopupShow( "TEXT_RESISTANCE_5", ResistanceHqShow ) end )
                ErrorPopupShow( "TEXT_RESISTANCE_5", function() PopupUnlock() end )
                return
            end
        end

        callback:ResistanceSaveAccountSettings( email, emailNotChanged , pass , passNotChanged)

    elseif string.find( name,  "/ResistanceAccount/TermsOfUse" ) then
        callback:ResistanceShowTermsOfUse()
    elseif string.find( name,  "/ResistanceAccount/Back" ) then
        ResistanceAccountHide( ResistanceHqShow() )
    elseif string.find( name,  "/ResistanceAccount/Logout" ) then
        UserLoggedOut()
        ResistanceAccountHide( function() OnResistanceHqHide(true) end )
    elseif string.find( name, "/ResistanceAccount/Email/EmailInputButton" ) then
        SetInputTextToUpdate( root:GetControl( "Email/EmailText"), "", InputFieldsType.email )
        UpdateInput( email )
        callback:OpenVkb( email,  InputFieldsMaxLenght.email )
    elseif string.find( name,"/ResistanceAccount/Pass/PassInputButton" ) then
        SetInputTextToUpdate( root:GetControl( "Pass/PassText") , "", InputFieldsType.password )
        UpdateInput( pass )
        callback:OpenVkb( pass , InputFieldsMaxLenght.password )
    elseif string.find( name, "/ResistanceAccount/PassVerify/PassVerifyInputButton" ) then
        SetInputTextToUpdate( root:GetControl( "PassVerify/PassVerifyText") , "",InputFieldsType.passwordVerify )
        UpdateInput( passVerify )
        callback:OpenVkb( passVerify , InputFieldsMaxLenght.passwordVerify )
    elseif string.find( name, "/ResistanceAccount/UserInfoRoot/Copy" ) then
        callback:SetClipboard( tostring ( User.UniqueId ) )
    end
end

function ResistanceAccountPress( control, path )
    if not active then
        return
    end

    if string.find( path, "/ResistanceAccount/Back" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceAccount/TermsOfUse" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceAccount/Logout" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceAccount/UserInfoRoot/Copy" ) then
        ButtonPress( control )
    elseif string.find( path, "/ResistanceAccount/Save" ) then
        if ( control:GetRepresentation() == "default" ) then
            ButtonPress( control )
        end
    end
end

function ResistanceAccountShow()

    FillTextAndCalcLayout()
    
    SaveButtonDeactivate()

    InputFields.email = ""
    InputFields.password = ""
    InputFields.passwordVerify =""

    email = User.Email
    pass = User.Pass
    passVerify = User.Pass

    SetInputTextToUpdate( root:GetControl( "Email/EmailText"), "", InputFieldsType.email )
    UpdateInput( User.Email )

    SetInputTextToUpdate( root:GetControl( "Pass/PassText") , "", InputFieldsType.password )
    UpdateInput( User.Pass )

    SetInputTextToUpdate( root:GetControl( "PassVerify/PassVerifyText") , "", InputFieldsType.passwordVerify )
    UpdateInput( User.Pass )

    callback:ResistanceRefreshInputs()
    active = true

    root:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( SmoothStep( t ) * 255 )
        end
        )
    )
end

function ResistanceAccountHide( callback )
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            root:SetAlpha( 255 - SmoothStep( t ) * 255 )
        end
        ),
        function()
            if callback then callback() end
            root:SetVisibility( false )
        end
     )
end

function FillTextAndCalcLayout()

    local avatarWidth = R( 132 )
    local avatarHeight = R( 120 )

    -- fill texts and calc layout
    local inviteCode = root:GetControl( "UserInfoRoot/InvitationCode" )
    local copyButton = root:GetControl( "UserInfoRoot/Copy" )    
    local emailInputField = root:GetControl( "Email" )
    local widthInput = emailInputField:GetControl( "EmailText" ):GetWidth()

    local widthInvite = inviteCode:GetWidth()
    local copyW, copyH = copyButton:GetGraphic( "TS" ):GetSize()

    local inviteCodeText = tostring ( User.UniqueId )

    local onScreenIniteText = string.sub(inviteCodeText,1,3) .. " " .. string.sub(inviteCodeText,4,6).. " " .. string.sub(inviteCodeText, 7 )

    inviteCode:SetTextWithVariables( "TEXT_ACCOUNT_INVITATION_CODE", { VALUE = onScreenIniteText } )

    inviteCode:SetX( (width - widthInvite - copyW)/2 )
    inviteCode:SetY( height - copyH - R( 15 ) )
    inviteCode:SetVisibility( false )
  
    local xCopy = inviteCode:GetX() + widthInvite
    local yCopy = inviteCode:GetY()
    
    copyButton:SetX( xCopy )
    copyButton:SetY( yCopy )
    copyButton:SetVisibility( false )

    SocialAvatarFillAccount( avatarRoot, avatarWidth, avatarHeight, User )

    local avatarLeftMargin =  R( 10 )
    local xCoord = avatarLeftMargin--( width - avatarWidth )/4

    avatarRoot:SetX( ( width - ( widthInput + R(80) + avatarWidth ) )/2 )
    avatarRoot:SetY( ( height - avatarHeight )/2 - 2*avatarLeftMargin )

    local nameImg =  root:GetControl( "UserInfoRoot/UserNameImage" )

    -- local path = callback:ResistanceGetUsernamePath( User.Name );
    -- nameImg:GetGraphic( "TS" ):SetImage( path )
    -- local w, h = nameImg:GetGraphic( "TS" ):GetSize()

    -- nameImg:SetX( xCoord + ( avatarWidth -w ) / 2 )
    -- nameImg:SetY( avatarHeight + R( 15 ) + h/2)

     nameImg:SetVisibility( false )

     local userNameRoot = root:GetControl( "UserInfoRoot/UserName" )
    -- userNameRoot:SetText( User.Name )
    -- userNameRoot:SetX( xCoord )
    -- userNameRoot:SetY( avatarHeight + R( 15 ) )

    userNameRoot:SetVisibility( false )


    local passInputField = root:GetControl( "Pass" )
    local passVerifyInputField = root:GetControl( "PassVerify" )

    local margin = R(12)
    local xCoordOfInputFileds =  avatarRoot:GetX() + avatarWidth + R(80)--width - margin - widthInput 

    emailInputField:SetX( xCoordOfInputFileds )
    passInputField:SetX( xCoordOfInputFileds )
    passVerifyInputField:SetX( xCoordOfInputFileds )

    local offsetY = R(30)

    emailInputField:SetY( avatarRoot:GetY() )
    passInputField:SetY( emailInputField:GetY() + offsetY)
    passVerifyInputField:SetY( passInputField:GetY() + offsetY)


    root:GetControl( "Save" ):SetX( passVerifyInputField:GetX() + ( widthInput - copyW )/2 )
    root:GetControl( "Save" ):SetY( passVerifyInputField:GetY() + 3 * margin )
    
    emailOnEnter = User.Email
    passOnEnter = User.Pass

    pass = passOnEnter
    email = emailOnEnter
end

function AccountDataSaveSucces()
    --ResistanceHqHide( function() OperationSuccessPopupShow( "TEXT_ACCOUNT_DATA_SAVED" , ResistanceHqShow  ) end )

    OperationSuccessPopupShow( "TEXT_ACCOUNT_DATA_SAVED" )

    User.Email =  registry:Get( "/internal/mail")
    User.Pass = registry:Get( "/internal/pass")

    registry:Set( "/monstaz/resistance/pass",  User.Pass )
    registry:Set( "/monstaz/resistance/mail", User.Email )

    UserInfoRefresh()
    callback:Save()

    SaveButtonDeactivate()

    InputFields.email = ""
    InputFields.password = ""
    InputFields.passwordVerify =""

    email = User.Email
    pass = User.Pass
    passVerify = User.Pass

    SetInputTextToUpdate( root:GetControl( "Email/EmailText"), "", InputFieldsType.email )
    UpdateInput( User.Email )

    SetInputTextToUpdate( root:GetControl( "Pass/PassText") , "", InputFieldsType.password )
    UpdateInput( User.Pass )

    SetInputTextToUpdate( root:GetControl( "PassVerify/PassVerifyText") , "", InputFieldsType.passwordVerify )
    UpdateInput( User.Pass )


    emailOnEnter = User.Email
    passOnEnter = User.Pass

    pass = passOnEnter
    email = emailOnEnter

    callback:ResistanceRefreshInputs()
end

function AccountDataSaveFailed()
    --ResistanceHqHide( function() ErrorPopupShow( "TEXT_FAIL_EMAIL_USED" ) end )
    ErrorPopupShow( "TEXT_FAIL_EMAIL_USED" )
end

function AccountDataSaveFailedNoNetwork()
    --ResistanceHqHide( function() ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" ) end )
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" )
end

function UpdateAccountSettings( dt )
    
    if not active then
        return
    end

    if not ( InputFields.email == User.Email ) then
        SaveButtonActivate()
        --if not (InputFields.email == "") then  end
    end

    if not ( InputFields.password == User.Pass ) then
        --if not (InputFields.password == "") then SaveButtonActivate() end
        SaveButtonActivate()
    end

    if ( (InputFields.password == User.Pass or (InputFields.password == "")) and  ( InputFields.email == User.Email or (InputFields.email == "")) ) then 
        SaveButtonDeactivate() 
    end
    --callback:ResistanceRefreshInputs()
end

function SaveButtonDeactivate() 
    root:GetControl( "Save"):SetRepresentation( "pressed" )
end

function SaveButtonActivate()
    root:GetControl( "Save"):SetRepresentation( "default" )
end

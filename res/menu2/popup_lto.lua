require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/shop/shop_item.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = SCREEN_WIDTH
local height = SCREEN_HEIGHT - R(50)

local LineBlue = {
    e = "menu2/line_01_end.png@linear",
    f = "menu2/line_01_fill.png@linear"
}

local offers = { nil, nil, nil }

local function AddHighlight( base, _w )
    local w = R(_w)

    local get = base:GetControl( "Get" )
    get:SetY( height - R(50) )

    local item = base:InsertControl( "down", 0 )
    item:SetRelative( true )
    item:SetY( height - R(70) )

    local lb = item:InsertControl()
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( LineBlue.e )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    lb:SetX( -w/2 - lb:GetWidth() )

    local rb = item:InsertControl()
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( LineBlue.e )
    rb:SetX( w/2 )

    local hb = item:InsertControl()
    hb:AddRepresentation( "default", "menu2/tile.xml" )
    hb:SetRelative( true )
    hb:GetGraphic( "s" ):SetImage( "menu2/color_fill_med.png@linear" )
    hb:GetGraphic( "s" ):SetClipRect( 0, 0, w, hb:GetHeight() )
    hb:SetX( -w/2 )
    hb:SetY( -hb:GetHeight() + lb:GetHeight() / 2 )

    local bb = item:InsertControl()
    bb:AddRepresentation( "default", "menu2/tile.xml" )
    bb:SetRelative( true )
    bb:GetGraphic( "s" ):SetImage( LineBlue.f )
    bb:GetGraphic( "s" ):SetClipRect( 0, 0, w, bb:GetHeight() )
    bb:SetX( -w/2 )

    anim:Add( Perpetual:New(
        function( t )
            hb:SetAlpha( ( 0.75 + 0.125 * ( 1 + math.sin( t * 4 ) ) ) * 255 )
        end
        ) )
end

function LimitedTimeOffersInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_lto.xml" )
    popup = screen:GetControl( "/LimitedTimeOffers" )

    x = 0
    y = 0

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    local header = popup:GetControl( "Header" )
    local hw = header:GetTextWidth()
    local underline = header:GetControl( "Underline" )

    local lb = underline:InsertControl()
    lb:AddRepresentation( "default", "menu2/timage.xml" )
    lb:SetRelative( true )
    lb:GetGraphic( "TS" ):SetImage( LineBlue.e )
    lb:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
    lb:SetX( -hw/2 - lb:GetWidth() )

    local rb = underline:InsertControl()
    rb:AddRepresentation( "default", "menu2/timage.xml" )
    rb:SetRelative( true )
    rb:GetGraphic( "TS" ):SetImage( LineBlue.e )
    rb:SetX( hw/2 )

    local bb = underline:InsertControl()
    bb:AddRepresentation( "default", "menu2/tile.xml" )
    bb:SetRelative( true )
    bb:GetGraphic( "s" ):SetImage( LineBlue.f )
    bb:GetGraphic( "s" ):SetClipRect( 0, 0, hw, bb:GetHeight() )
    bb:SetX( -hw/2 )

    -- left
    local left = popup:GetControl( "Left" )
    AddHighlight( left, 113 )
    left:GetControl( "Icon" ):SetY( ( left:GetControl( "Header" ):GetY() + left:GetControl( "down" ):GetY() ) / 2 - R(10) )
    left:GetControl( "Price" ):SetY( left:GetControl( "down" ):GetY() - R(30) )

    -- mid
    local mid = popup:GetControl( "Mid" )
    AddHighlight( mid, 181 )
    mid:GetControl( "Icon" ):SetY( ( mid:GetControl( "Header" ):GetY() + mid:GetControl( "down" ):GetY() ) / 2 - R(10) )
    mid:GetControl( "MiddleRoot" ):SetY(  ( mid:GetControl( "Header" ):GetY() + mid:GetControl( "down" ):GetY() ) / 2 - R(35) )
    mid:GetControl( "Price1" ):SetY( mid:GetControl( "down" ):GetY() - R(26) )
    mid:GetControl( "PriceSlash" ):SetY( mid:GetControl( "down" ):GetY() - R(22) )
    mid:GetControl( "PriceArrow" ):SetY( mid:GetControl( "down" ):GetY() - R(20) )
    mid:GetControl( "Price2" ):SetY( mid:GetControl( "down" ):GetY() - R(30) )
    mid:GetControl( "PriceOnlyNow" ):SetY( mid:GetControl( "down" ):GetY() - R(42) )

    for _,v in ipairs( { "Damage", "FireRate", "ClipSize", "Elements" } ) do
        mid:GetControl( "MiddleRoot/" .. v .. "/Label" ):SetShaderColors( 18, 151, 219, 6, 19, 59, 32, 193, 246 )
    end

    local assets = {
        starBg = "menu2/star_small_2.png@linear",
        elFireBg = "menu2/ammo_fire_blue.png@linear",
        elFrostBg = "menu2/ammo_snow_blue.png@linear",
        elElectricBg = "menu2/ammo_slash_blue.png@linear"
    }
    ShopItemCreateStars( mid:GetControl( "MiddleRoot/Damage/Stars" ), assets )
    ShopItemCreateStars( mid:GetControl( "MiddleRoot/FireRate/Stars" ), assets )
    ShopItemCreateStars( mid:GetControl( "MiddleRoot/ClipSize/Stars" ), assets )
    local infoElements = mid:GetControl( "MiddleRoot/Elements/Icons" )
    ShopItemUpdateRepresentation( infoElements:GetControl( "Electric" ), "default",  assets.elElectricBg )
    ShopItemUpdateRepresentation( infoElements:GetControl( "Frost" ), "default",  assets.elFrostBg )
    ShopItemUpdateRepresentation( infoElements:GetControl( "Fire" ), "default",  assets.elFireBg )

    -- right
    local right = popup:GetControl( "Right" )
    AddHighlight( right, 113 )
    right:GetControl( "Icon" ):SetY( ( right:GetControl( "Header" ):GetY() + right:GetControl( "down" ):GetY() ) / 2 - R(10) )
    right:GetControl( "Price1" ):SetY( right:GetControl( "down" ):GetY() - R(45) )
    right:GetControl( "PriceSlash" ):SetY( right:GetControl( "down" ):GetY() - R(41) )
    right:GetControl( "Price2" ):SetY( right:GetControl( "down" ):GetY() - R(25) )

    local item = {}
    table.insert( item, mid:GetControl( "Icon" ):GetGraphic( "TS" ) )
    table.insert( item, left:GetControl( "Icon" ):GetGraphic( "TS" ) )
    table.insert( item, right:GetControl( "Icon" ):GetGraphic( "TS" ) )
    anim:Add( Perpetual:New(
        function( t )
            for i=1,3 do
                item[i]:SetAngle( t )
                item[i]:SetScale( 1 + math.sin( i + t ) * 0.1 )
            end
        end
        ) )
end

function LimitedTimeOffersKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        LimitedTimeOffersHide()
        return true
    end

    return false
end

function LimitedTimeOffersPress( control, path )
    if not active then
        return
    end

    if path == "/LimitedTimeOffers/Back" then
        ButtonPress( control )
    elseif path == "/LimitedTimeOffers/Left/Get" then
        ButtonPress( control )
    elseif path == "/LimitedTimeOffers/Mid/Get" then
        ButtonPress( control )
    elseif path == "/LimitedTimeOffers/Right/Get" then
        ButtonPress( control )
    end
end

function LimitedTimeOffersAction( path )
    if not active then
        return
    end

    if path == "/LimitedTimeOffers/Back" then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_LTO_ACTION, GameEventParam.GEP_LTO_BACK )
        LimitedTimeOffersHide()
    elseif path == "/LimitedTimeOffers/Left/Get" then
        local item = offers[1]
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_LTO_ACTION, GameEventParam.GEP_LTO_GET_REGULAR, item.id )
        ShopShow( item.cat == ItemCategory.weapons and ShopTab.Weapons or ShopTab.Items, nil, nil, item )
        LimitedTimeOffersHide()
    elseif path == "/LimitedTimeOffers/Mid/Get" then
        local item = offers[2]
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_LTO_ACTION, GameEventParam.GEP_LTO_GET_HARD, item.id )
        ShopSpecialOffer( item, 0.75, 1 )
        ShopShow( ShopTab.Weapons, nil, nil, item )
        LimitedTimeOffersHide()
    elseif path == "/LimitedTimeOffers/Right/Get" then
        local item = offers[3]
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_LTO_ACTION, GameEventParam.GEP_LTO_GET_SOFT, item.id )
        ShopSpecialOffer( item, 0.9, 2 )
        ShopShow( item.cat == ItemCategory.weapons and ShopTab.Weapons or ShopTab.Items, nil, nil, item )
        LimitedTimeOffersHide()
    end
end

local function GetOfferLeft()
    local w = ItemsCategory[ItemCategory.weapons]
    local bestName = ItemDbGetBestOwnedWeaponId( true )
    local level = registry:Get( "/monstaz/player/level" )

    local best = 1
    for i=1,#w do
        if w[i].id == bestName then
            best = i
            break
        end
    end

    ClawMsg( "best: " .. best .. " " .. w[best].id )

    if best < #w then
        local blvl = 1
        for i=1,#w do
            if w[i].lvl > level then
                break
            end
            blvl = i
        end
        ClawMsg( "best within level: " .. blvl .. " " .. w[blvl].id )

        if blvl > best then
            best = blvl
        else
            best = best + 1
        end

        ClawMsg( "chosen: " .. best .. " " .. w[best].id )

        if w[best] ~= offers[2] and w[best] ~= offers[3] then
            offers[1] = w[best]
            return true
        end
    end

    for _,v in ipairs( ItemsCategory[ItemCategory.items] ) do
        if Shop:IsBought( v.id ) < v.max and v ~= offers[2] and v ~= offers[3] then
            offers[1] = v
            return true
        end
    end

    return false
end

local function GetOfferMid1()
    local w = ItemsCategory[ItemCategory.weapons]
    local level = registry:Get( "/monstaz/player/level" )

    for i=1,#w do
        if w[i].lvl > level and Shop:IsBought( w[i].id ) == 0 then
            offers[2] = w[i]
            return true
        end
    end
    return false
end

local function GetOfferMid2()
    local t = {}
    for i=1,2 do
        local v = registry:Get( "/monstaz/weaponselection/" .. i )
        if v == "" then
            break
        else
            table.insert( t, v )
        end
    end
    if #t > 1 then
        if math.random() < 0.5 then
            local tmp = t[1]
            t[1] = t[2]
            t[2] = t[1]
        end
    end
    for i=1,#t do
        if ItemDbCanUpgrade( t[i] ) then
            local item = ItemDbGetItemById( t[i] )
            local s, h = ItemDbGetItemUpgradePrice( item.id )
            if s > 1 or h > 1 then
                offers[2] = item
                return true
            end
        end
    end
    return false
end

local function GetOfferMid3()
    local w = ItemsCategory[ItemCategory.weapons]
    local t = {}

    for i=1,#w do
        if Shop:IsBought( w[i].id ) == 0 then
            table.insert( t, w[i] )
        end
    end

    if #t > 0 then
        offers[2] = t[math.random( #t )]
        return true
    end

    return false
end

local function GetOfferMid4()
    local w = ItemsCategory[ItemCategory.weapons]
    local t = {}

    for i=1,#w do
        if Shop:IsBought( w[i].id ) > 0 and ItemDbCanUpgrade( w[i].id ) then
            local s, h = ItemDbGetItemUpgradePrice( w[i].id )
            if s > 1 or h > 1 then
                table.insert( t, w[i] )
            end
        end
    end

    if #t > 0 then
        offers[2] = t[math.random( #t )]
        return true
    end

    return false
end

local function GetOfferMid()
    local f = {}
    if math.random() < 0.5 then
        table.insert( f, GetOfferMid1 )
        table.insert( f, GetOfferMid2 )
    else
        table.insert( f, GetOfferMid2 )
        table.insert( f, GetOfferMid1 )
    end
    if math.random() < 0.5 then
        table.insert( f, GetOfferMid3 )
        table.insert( f, GetOfferMid4 )
    else
        table.insert( f, GetOfferMid4 )
        table.insert( f, GetOfferMid3 )
    end

    for i=1,#f do
        if f[i]() then return true end
    end

    return false
end

local function GetOfferRight()
    local n = registry:Get( "/app-config/lto/offer-right/num" )
    local t = {}

    for i=1,n do
        local id = registry:Get( "/app-config/lto/offer-right/" .. i )
        local item = nil
        for _,v in ipairs( AllItems ) do
            if v.id == id then
                if v ~= offers[2] then
                    item = v
                end
                break
            end
        end
        if item then
            if item.cat == ItemCategory.weapons then
                if Shop:IsBought( id ) == 0 then
                    table.insert( t, item )
                end
            elseif item.cat == ItemCategory.items then
                if ( item.cs or item.ch > 1 ) and Shop:IsBought( id ) < item.max then
                    table.insert( t, item )
                end
            end
        end
    end

    if #t > 0 then
        offers[3] = t[math.random( #t )]
        return true
    end

    return false
end

local function GetOffers()
    return GetOfferMid() and GetOfferRight() and GetOfferLeft()
end

function LimitedTimeOffersShow()
    local level = registry:Get( "/monstaz/player/level" )
    if not GetOffers() then return end
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_LTO_SHOWN )

    -- left
    local ol = offers[1]

    local left = popup:GetControl( "Left" )
    left:GetControl( "Icon/Img" ):GetGraphic( "s" ):SetImage( ol.f )
    left:GetControl( "Header" ):SetText( ol.d )
    if ol.cat == ItemCategory.weapons then
        left:GetControl( "Price" ):SetText( ol.lvl > level and ol.cu .. "\194\158" or ol.cs[1] .. "\194\156" )
    else
        left:GetControl( "Price" ):SetText( ol.cs .. "\194\156" )
    end

    -- mid
    local ol = offers[2]
    local isUpdate = Shop:IsBought( ol.id ) > 0
    local isHard
    local amount
    local left = popup:GetControl( "Mid" )
    if isUpdate then
        local level = Shop:GetUpgrades( ol.id ) + 1
        left:GetControl( "Header" ):SetText( TextDict:Get( ol.d ) .. " - " .. TextDict:Get( "TEXT_UPGRADE_LEVEL" ):gsub( "($%(%NUM)%)", tostring( level ) ) )
        local s, h = ItemDbGetItemUpgradePrice( ol.id )
        isHard = s == 0
        amount = isHard and h or s
        left:GetControl( "Icon" ):SetX( -R(45) )
        left:GetControl( "Get/Text" ):SetText( "TEXT_UPGRADE_EXCLAMATION" )

        local item = ol
        local dmgFrom, dmgTo = 0, 0
        local fireFrom, fireTo = 0, 0
        local clipFrom, clipTo = 0, 0
        local elementsOwned = {}
        local elementsAvailable = {}

        local maxIdx = math.min( #item.upgrades.Elements, level )
        for i=1,maxIdx do
            if item.upgrades.Elements[i] > 0 then
                table.insert( elementsOwned, item.upgrades.Elements[i] )
            end
        end
        if level < #item.upgrades.Elements then
            local element = item.upgrades.Elements[level+1]
            if element > 0 then
                table.insert( elementsAvailable, element )
            end
        end
        if item.dmg then
            dmgFrom = item.dmg[level]
            dmgTo = item.dmg[level + 1]
        end
        if item.rate then
            fireFrom = item.rate[level]
            fireTo = item.rate[level + 1]
        end
        if item.clip then
            clipFrom = item.clip[level]
            clipTo = item.clip[level + 1]
        end

        ShopItemMarkStars( left:GetControl( "MiddleRoot/Damage/Stars" ), dmgFrom, dmgTo, item.dmg[#item.dmg] )
        ShopItemMarkStars( left:GetControl( "MiddleRoot/FireRate/Stars" ), fireFrom, fireTo, item.rate[#item.rate] )
        ShopItemMarkStars( left:GetControl( "MiddleRoot/ClipSize/Stars" ), clipFrom, clipTo, item.clip[#item.clip] )
        ShopItemMarkElements( item, left:GetControl( "MiddleRoot/Elements/Icons" ), elementsOwned, elementsAvailable )
    else
        left:GetControl( "Header" ):SetText( ol.d )
        isHard = ol.lvl > level
        amount = isHard and ol.cu or ol.cs[1]
        left:GetControl( "Icon" ):SetX( 0 )
        left:GetControl( "Get/Text" ):SetText( "TEXT_22" )
    end
    left:GetControl( "MiddleRoot" ):SetVisibility( isUpdate )
    left:GetControl( "Icon/Img" ):GetGraphic( "s" ):SetImage( ol.f )
    local currency = isHard and "\194\158" or "\194\156"
    left:GetControl( "Price1" ):SetText( amount .. currency )
    left:GetControl( "Price2" ):SetText( math.floor( amount * 0.75 ) .. currency )

    -- right
    local ol = offers[3]

    local left = popup:GetControl( "Right" )
    left:GetControl( "Icon/Img" ):GetGraphic( "s" ):SetImage( ol.f )
    left:GetControl( "Header" ):SetText( ol.d )
    if ol.cat == ItemCategory.weapons then
        left:GetControl( "Price1" ):SetText( ol.lvl > level and ol.cu .. "\194\158" or ol.cs[1] .. "\194\156" )
        left:GetControl( "Price2" ):SetText( ol.lvl > level and math.floor( ol.cu * 0.9 ) .. "\194\158" or math.floor( ol.cs[1] * 0.9 ) .. "\194\156" )
    else
        left:GetControl( "Price1" ):SetText( ol.cs and ol.cs .. "\194\156" or ol.ch .. "\194\158" )
        left:GetControl( "Price2" ):SetText( ol.cs and math.floor( ol.cs * 0.9 ) .. "\194\156" or math.floor( ol.ch * 0.9 ) .. "\194\158" )
    end

    -- other stuff
    active = true
    PopupLock()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function LimitedTimeOffersHide()
    active = false
    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

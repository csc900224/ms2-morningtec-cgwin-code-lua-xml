require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local window = nil
local dumDum = nil

local x = 0
local y = 0
local width = R( 110 )
local defaultHeight = R( 190 )
local height = R( 190 )
local dumDumPosRartioY = 0.35

local glow = {
    c = nil,
    x = 0,
    y = 0,
    offset = R(8)
}

function AvatarInit( parent, h )
    parent:InsertFromXml( "menu2/avatar.xml" )
    window = parent:GetControl( "Avatar" )

    height = h ~= nil and h or defaultHeight

    x = math.floor( X(16) + ( SCREEN_WIDTH - R(480) )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    window:SetX( -width )
    window:SetY( y )

    local frame = window:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    dumDum = window:GetControl( "DumDum" )
    dumDum:SetY( height * dumDumPosRartioY )

    glow.c = dumDum:GetControl( "Glow" )
    glow.x = glow.c:GetX()
    glow.y = glow.c:GetY()
end

function AvatarShow()
    active = true

    window:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            window:SetX( SmoothStep( t ) * ( x + width ) - width )
        end
        )
    )

    AvatarRefresh()
end

function AvatarRefresh()
    window:GetControl( "Header/Level" ):SetText( registry:Get( "/monstaz/player/level" ) )
end

function AvatarHide()
    active = false

    anim:Add( Slider:New( 0.3,
        function( t )
            window:SetX( x - ( x + width ) * SmoothStep( t ) )
        end
        ),
        function()
            window:SetVisibility( false )
        end
    )
end

function AvatarSetWeapon( weaponAvatar )
    local weapon = dumDum:GetControl("Weapon")
    local charcter = dumDum:GetControl("Character")
    weapon:GetGraphic("TS"):SetImage( weaponAvatar.file )
    local w, h = weapon:GetGraphic("TS"):GetSize()
    weapon:GetGraphic("TS"):SetPivot( R(weaponAvatar.px), R(weaponAvatar.py) )
    charcter:SetVisibility( weaponAvatar.hideDumDum == nil or weaponAvatar.hideDumDum == false )

    glow.c:SetY( glow.y + ( not charcter:GetVisibility() and glow.offset or 0 ) )
end

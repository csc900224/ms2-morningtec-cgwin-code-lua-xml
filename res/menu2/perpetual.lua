Perpetual = {}

function Perpetual:New( setter )
    local obj = { t = 0, f = setter }
    setmetatable( obj, { __index = Perpetual } )
    return obj
end

function Perpetual:Play( dt )
    self.t = self.t + dt
    self.f( self.t )
end

function Perpetual:IsFinished()
    return false
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/star_animation.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 260 )
local height = R( 215 )

local iconFile = "menu2/cat_01.png@linear"

local callbackReturn = nil


function ConfirmRemovePopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_confirm_remove.xml" )
    popup = screen:GetControl( "/ConfirmRemovePopup" )
    popup:SetVisibility( false )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

    local removeBtn = popup:GetControl( "OK" )
    local backBtn = popup:GetControl( "Back" )
    local aw, ah = removeBtn:GetControl("Button"):GetGraphic( "TS" ):GetSize()

    local margin = R(15)
    removeBtn:SetX( width / 2 - aw - margin )
    backBtn:SetX( width / 2 + margin )

    popup:GetControl( "Icon" ):SetX( width / 2 )
    popup:GetControl( "Message" ):SetX( (width - popup:GetControl( "Message" ):GetWidth() )/2)
end

function ConfirmRemovePopupFillData( iconPath  )

    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconPath )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )
end

function ConfirmRemovePopupShow( callback )
    if active then
        return
    end

    active = true

    callbackReturn = callback

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    ConfirmRemovePopupFillData( iconFile , text )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
end

function ConfirmRemovePopupPress( control, path )
    if not active then
        return
    end

    if path == "/ConfirmRemovePopup/OK/Button" then
        ButtonPress( control )
    elseif path == "/ConfirmRemovePopup/Back/Button" then
        ButtonPress( control )
    end
end

function ConfirmRemovePopupAction( name )
    if not active then
        return
    end

    if name == "/ConfirmRemovePopup/OK/Button" then
        ConfirmRemovePopupHide( callbackReturn )
        callbackReturn = nil
    elseif name == "/ConfirmRemovePopup/Back/Button" then
        ConfirmRemovePopupHide()
        callbackReturn = nil
    end
end

function ConfirmRemovePopupHide( callback )
    active = false

    PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Icon/Star" ) )
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 300 )
local height = R( 190 )

function VersionPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_version.xml" )
    popup = screen:GetControl( "/VersionPopup" )

    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    if callback:IsUpdateMandatory() then
        continueButton = popup:GetControl( "Continue" )
        continueButton:SetVisibility( false )
        
        updateButton = popup:GetControl( "Update" )
        updateButton:SetVisibility( true )
        w, h = updateButton:GetControl( "Button" ):GetGraphic( "TS" ):GetSize()
        updateButton:SetX( (width - w) / 2 )
    end

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
end

function VersionPopupShow()
    active = true
    --PopupLock()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function VersionPopupAction( name )
    if not active then
        return
    end

    if name == "/VersionPopup/Update/Button" then
        if IS_IPHONE then
            callback:Update()
        else
            VersionPopupHide(true)
        end
    elseif name == "/VersionPopup/Continue/Button" then
        VersionPopupHide(false)
    end
end

function VersionPopupHide(update)
    active = false
    --PopupUnlock()

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            popup:SetVisibility( false )
            if update then
                callback:Update()
                callback:Exit()
            else
                callback:Continue()
            end
        end
    )
end

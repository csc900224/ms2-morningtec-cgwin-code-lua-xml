require( "menu2/tutorial_finger.lua" )

local SummaryContinuePath = "/Summary/Menu/Continue"
local PerksButtonPath = "/PerkArea"
local PerksIconPath = "/Perks/Icon"
local PerksItemsPath = "/Perks/Items/"
local LevelUpClosePath = "/LevelupPopup/Close"
local QuickShopPath = "/PauseRoot/PausePopup/ItemsRoot"
local GrenadeBuyPath = QuickShopPath .. "/ShopItemList/Items/grenade/ShopItem/Buttons/Buy"
local GrenadeBuyConfirmPath = QuickShopPath .. "/ShopItemList/Items/grenade/ShopItem/Buttons/Confirm"
local MineBuyPath = QuickShopPath .. "/ShopItemList/Items/mine/ShopItem/Buttons/Buy"
local MineBuyConfirmPath = QuickShopPath .. "/ShopItemList/Items/mine/ShopItem/Buttons/Confirm"
local PauseButtons = "/PauseRoot/PausePopup/Buttons"
local ResumePath = PauseButtons .. "/Resume"

local IntroActions = {
    {
        allowed = function( path ) return path == SummaryContinuePath and TutorialManager:IsTaskActive( TutorialTask.IntroSummary ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.IntroSummary ) end
    }
}

local GameplayActions = {
    {
        allowed = function( path ) return path == PerksButtonPath and TutorialManager:IsTaskActive( TutorialTask.GameplayRage ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.GameplayRage ) end
    },
    {
        allowed = function( path ) return string.find( path, PerksItemsPath ) and TutorialManager:IsTaskActive( TutorialTask.GameplaySelectPerk ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.GameplaySelectPerk ) end
    },
    {
        allowed = function( path ) return path == LevelUpClosePath and TutorialManager:IsTaskActive( TutorialTask.GameplayLevelUp ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.GameplayLevelUp ) end
    },
    {
        allowed = function( path ) return path == SummaryContinuePath and TutorialManager:IsTaskActive( TutorialTask.GameplaySummary ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.GameplaySummary ) end
    }
}

local ItemsActions = {
    {
        allowed = function( path ) return path == GrenadeBuyPath and TutorialManager:IsTaskActive( TutorialTask.ItemsBuyGrenade ) end,
        completed = function() end
    },
    {
        allowed = function( path ) return path == GrenadeBuyConfirmPath and TutorialManager:IsTaskActive( TutorialTask.ItemsBuyGrenade ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ItemsBuyGrenade ) end
    },
    {
        allowed = function( path ) return path == MineBuyPath and TutorialManager:IsTaskActive( TutorialTask.ItemsBuyMine ) end,
        completed = function() end
    },
    {
        allowed = function( path ) return path == MineBuyConfirmPath and TutorialManager:IsTaskActive( TutorialTask.ItemsBuyMine ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ItemsBuyMine ) end
    },
    {
        allowed = function( path ) return path == ResumePath and TutorialManager:IsTaskActive( TutorialTask.ItemsResume ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ItemsResume ) end
    },
    {
        allowed = function( path ) return path == SummaryContinuePath and TutorialManager:IsTaskActive( TutorialTask.ItemsSummary ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.ItemsSummary ) end
    }
}

local FinalActions = {
    {
        allowed = function( path ) return path == SummaryContinuePath and TutorialManager:IsTaskActive( TutorialTask.FinalSummary ) end,
        completed = function() TutorialManager:SetTaskCompleted( TutorialTask.FinalSummary ) end
    }
}

local Actions = {}
Actions[TutorialChapter.Intro] = IntroActions
Actions[TutorialChapter.Gameplay] = GameplayActions
Actions[TutorialChapter.Items] = ItemsActions
Actions[TutorialChapter.Final] = FinalActions

function TutorialIsResumeAllowed()
    local chapter = TutorialManager:GetCurrentChapter()
    if chapter ~= TutorialChapter.Items then
        return true
    end

    return TutorialManager:IsTaskCompleted( TutorialTask.ItemsResume )
end

function TutorialIsActionAllowed( path )
    local chapter = TutorialManager:GetCurrentChapter()
    local actions = Actions[chapter]
    if actions then
        local action = actions[1]
        if action and action.allowed( path ) then
            return true
        end
    end

    if path == ResumePath then
        return TutorialIsResumeAllowed()
    end

    return path == PerksButtonPath or string.find( path, PerksItemsPath )
end

function TutorialAction( path )
    if not TutorialActive then return end

    local chapter = TutorialManager:GetCurrentChapter()
    local actions = Actions[chapter]
    if actions then
        local action = actions[1]
        if action and action.allowed( path ) then
            action.completed()
            table.remove( actions, 1 )
            return
        end
    end

    if path == ResumePath then
        TutorialResume()
    end
end

function TutorialHide()
    local tooltips = TutorialGetTooltips()
    for k, v in pairs( tooltips ) do
        v:SetVisibility( false )
    end
end

function TutorialShow( delay )
    local tooltips = TutorialGetTooltips()
    for k, v in pairs( tooltips ) do
        anim:Add(
            Delay:New( delay or 0 ),
            function()
                v:SetVisibility( true )
            end,
            v:GetPath()
        )
    end
end

-- Tutorial events

function TutorialOnLevelupPopup()
    local chapter = TutorialManager:GetCurrentChapter()
    if chapter == TutorialChapter.Gameplay and not TutorialManager:IsTaskActive( TutorialTask.GameplayLevelUp ) then
        TutorialManager:SetTaskActive( TutorialTask.GameplayLevelUp )
    end
end

function TutorialOnPauseMenu()
    local chapter = TutorialManager:GetCurrentChapter()
    if chapter == TutorialChapter.Items and TutorialManager:IsTaskActive( TutorialTask.ItemsBuy ) then
        TutorialManager:SetTaskActive( TutorialTask.ItemsBuyGrenade )
    end
end

function TutorialOnPerkMenuShow()
    local chapter = TutorialManager:GetCurrentChapter()
    local chapter = TutorialManager:GetCurrentChapter()
    if chapter == TutorialChapter.Gameplay and TutorialManager:IsTaskActive( TutorialTask.GameplayUseSmg ) then
        TutorialHide()
    end
end

function TutorialOnPerkMenuHide()
    local chapter = TutorialManager:GetCurrentChapter()
    if chapter == TutorialChapter.Gameplay and TutorialManager:IsTaskActive( TutorialTask.GameplayUseSmg ) then
        TutorialShow()
    end
end

-- Tutorial tasks

function TutorialPause()
    local button = screen:GetControl( ResumePath )
    local x = button:GetX() - R(2)
    local y = button:GetY() + button:GetHeight()/2 - R(1)

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialHide()
    TutorialTooltipShow( PauseButtons, "TutorialPause", "TEXT_TUTORIAL_16", x, y, TutorialAnchor.Right, 0.5, true )
end

function TutorialResume()
    local overlay = screen:GetControl( ResumePath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( PauseButtons .. "/TutorialPause" )
    TutorialShow( 0.45 )
end

function TutorialIntroSummary()
    local summary = SummaryPopupGetRoot()
    local menu = summary:GetControl( "Menu" )
    local button = menu:GetControl( "Back" )

    TutorialTooltipShow( menu:GetPath(), "Tutorial", "TEXT_TUTORIAL_17", button:GetX() + button:GetWidth()/2, button:GetY(), TutorialAnchor.Bottom, 2, true )
end

function TutorialGameplayRage()
    local x, y = PerksMenuGetPosition()

    local icon = screen:GetControl( PerksIconPath )
    x = x + PerksMenuGetIconWidth()
    y = y + icon:GetY()

    TutorialTooltipShow( "/", "TutorialGameplayRage", "TEXT_TUTORIAL_08", x, y, TutorialAnchor.Left, 0.5, true )
end

function TutorialGameplayRageCompleted()
    TutorialTooltipHide( "/TutorialGameplayRage" )
end

function TutorialGameplaySelectPerk()
    local x, y = PerksMenuGetPosition()
    local w, h = PerksMenuGetSize()
    local offset = PerksMenuGetIconWidth()

    TutorialTooltipShow( "/", "TutorialGameplaySelectPerk", "TEXT_TUTORIAL_18", x + ( w - offset )/2, y + h, TutorialAnchor.Top, 0.25, true )
end

function TutorialGameplaySelectPerkCompleted()
    TutorialTooltipHide( "/TutorialGameplaySelectPerk" )
end

function TutorialGameplayLevelUp()
    local button = screen:GetControl( LevelUpClosePath )
    TutorialFingerShow( LevelUpClosePath, "TutorialFinger", button:GetWidth()/2, R(0), 0.25, true )
    TutorialTooltipShow( LevelUpClosePath, "TutorialGameplayLevelUp", "TEXT_TUTORIAL_19", button:GetWidth(), button:GetHeight()/2, TutorialAnchor.Left, 0.5, true, 0, R(110) )
end

function TutorialGameplayLevelUpCompleted()
    TutorialFingerHide( LevelUpClosePath .. "/TutorialFinger" )
    TutorialTooltipHide( LevelUpClosePath .. "/TutorialGameplayLevelUp" )
end

function TutorialGameplaySummary()
    local summary = SummaryPopupGetRoot()
    local menu = summary:GetControl( "Menu" )
    local button = menu:GetControl( "Back" )

    TutorialTooltipShow( menu:GetPath(), "TutorialGameplaySummary", "TEXT_TUTORIAL_20", button:GetX() + button:GetWidth()/2, button:GetY(), TutorialAnchor.Bottom, 1, true )
end

function TutorialItemsPause()
    TutorialFingerShow( "/", "TutorialFinger", SCREEN_WIDTH/2, SCREEN_HEIGHT/2 )
end

function TutorialItemsPauseCompleted()
    TutorialFingerHide( "/TutorialFinger" )
end

function TutorialItemsBuyGrenade()
    local button = screen:GetControl( GrenadeBuyPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + button:GetHeight()

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialItemsBuyGrenade", "TEXT_TUTORIAL_21", x, y, TutorialAnchor.Top, 0.25, true )
end

function TutorialItemsBuyGrenadeCompleted()
    local overlay = screen:GetControl( GrenadeBuyPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/TutorialItemsBuyGrenade" )
end

function TutorialItemsBuyMine()
    local button = screen:GetControl( MineBuyPath )
    local x = button:GetAbsX() + button:GetWidth()/2
    local y = button:GetAbsY() + button:GetHeight()

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialItemsBuyMine", "TEXT_TUTORIAL_22", x, y, TutorialAnchor.Top, 0.25, true )
end

function TutorialItemsBuyMineCompleted()
    local overlay = screen:GetControl( MineBuyPath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/TutorialItemsBuyMine" )
end

function TutorialItemsResume()
    local button = screen:GetControl( ResumePath )
    local x = button:GetAbsX() - R(2)
    local y = button:GetAbsY() + button:GetHeight()/2 - R(1)

    local overlay = button:GetControl( "TutorialOverlay" )
    overlay:SetVisibility( false )

    TutorialTooltipShow( "/", "TutorialItemsResume", "TEXT_TUTORIAL_23", x, y, TutorialAnchor.Right, 0.25, true )
end

function TutorialItemsResumeCompleted()
    local overlay = screen:GetControl( ResumePath .. "/TutorialOverlay" )
    overlay:SetVisibility( true )

    TutorialTooltipHide( "/TutorialItemsResume" )
end

function TutorialItemsSummary()
    local summary = SummaryPopupGetRoot()
    local menu = summary:GetControl( "Menu" )
    local button = menu:GetControl( "Back" )

    TutorialTooltipShow( menu:GetPath(), "TutorialItemsSummary", "TEXT_TUTORIAL_24", button:GetX() + button:GetWidth()/2, button:GetY(), TutorialAnchor.Bottom, 2, true )
end

function TutorialFinalSummary()
    local summary = SummaryPopupGetRoot()
    local menu = summary:GetControl( "Menu" )
    local button = menu:GetControl( "Back" )

    TutorialTooltipShow( menu:GetPath(), "TutorialFinalSummary", "TEXT_TUTORIAL_48", button:GetX() + button:GetWidth()/2, button:GetY(), TutorialAnchor.Bottom, 2, true )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/gift_item.lua" )
require( "menu2/new_gifts_scroll_indicator.lua" )

local root = nil
local clip = nil
local width = nil
local height = nil
local items = nil


NewGiftsConsts = {
    itemHeight = R( 168 ),
    itemWidth = R(110),
    itemSpacing = R(-3),
    itemXOffset = R(5) 
}

local NewGiftsScrollData = {
    touchId = -1,
    minScrollDist = R(5),
    maxScrollOffset = 0,
    scrollOffset = 0,
    sampleTouchPos = 0,
    currentTouchPos = 0,
    startScrollOffset = 0,
    touchStart = 0,
    scrolling = false,    
    targetScrollOffset = 0,
    scrollAcceleration = 1,
    samplingInterval = 0.025,
    sampleTimer = -1,
    velocityEpsilon = 0.1,
    velocity = 0,
    lastVelocity = 0,
    draggTime = 0
}

function NewGiftsListInit( parent, listWidth, listHeight )
    parent:InsertFromXml( "menu2/new_gifts_list.xml" )
    clip = parent:GetControl( "NewGiftsList" )
    root = parent:GetControl( "NewGiftsList/NewGifts" )
    indicator = parent:GetControl( "NewGiftsScrollIndicatorRoot" )
    width = listWidth
    height = listHeight

    NewGiftsScrollIndicatorInit( indicator, 0, NewGiftsConsts.itemWidth )
    NewGiftsListCaluclateLayout()

    clip:GetGraphic( "r" ):SetWidth( width )
    clip:GetGraphic( "r" ):SetHeight( NewGiftsConsts.itemHeight )

end

function NewGiftsListCaluclateLayout()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    --NewGiftsConsts.itemHeight = math.min( height - iH, NewGiftsConsts.itemHeight)
    indicator:SetY( NewGiftsConsts.itemHeight )
    indicator:SetX( (width - iW)/2 )
end

function NewGiftsListUpdateIndicatorPos()
    local iW, iH = callback:GetControlSize( indicator:GetPath() )
    indicator:SetX( (width - iW)/2 )
end

function NewGiftsListSetContent( itemList, resetScroll )
    if resetScroll == nil or resetScroll == true then
        NewGiftsListResetScroll()
    end

    NewGiftsListCreateClipBorder()

    local x = NewGiftsConsts.itemXOffset

    local allItemsWidth = #itemList * ( NewGiftsConsts.itemWidth + NewGiftsConsts.itemSpacing ) - NewGiftsConsts.itemSpacing

    if allItemsWidth < width then
        x = ( width - allItemsWidth ) / 2
        NewGiftsListHideClipBorder()
    else
        NewGiftsListShowClipBorder()
    end

    NewGiftsListDropAllItems()
    
    items = itemList

    local idx = 1
    for _, item in ipairs( itemList ) do
        local itemRoot = root:InsertControl( tostring( idx ) )
        idx=idx+1
        itemRoot:SetRelative( true )
        itemRoot:SetX( x )
        GiftItemInit( itemRoot, NewGiftsConsts.itemWidth, NewGiftsConsts.itemHeight, item )
        x = x + NewGiftsConsts.itemWidth + NewGiftsConsts.itemSpacing
    end
    NewGiftsScrollData.maxScrollOffset = math.max( x - NewGiftsConsts.itemSpacing - width, 0 )
    
    NewGiftsScrollIndicatorUpdateMaxScrollOffset( NewGiftsScrollData.maxScrollOffset )
    NewGiftsListUpdateIndicatorPos()

end

function NewGiftsListResetScroll()
    NewGiftsScrollData.scrollOffset = 0
    root:SetX( 0 )
    NewGiftsScrollData.scrolling = false
    NewGiftsScrollData.touchId = -1
    NewGiftsScrollData.velocity = 0
    NewGiftsScrollData.lastVelocity = 0
    NewGiftsScrollData.sampleTimer = -1
    NewGiftsScrollData.draggTime = 0
    NewGiftsScrollData.targetScrollOffset = 0
    NewGiftsScrollData.scrollOffset = 0
end

function NewGiftsListUpdate( dt )
    if NewGiftsScrollData.touchId >= 0 then
        NewGiftsScrollData.draggTime = NewGiftsScrollData.draggTime + dt
    end

    NewGiftsListSampleVelocity( dt )
    NewGiftsListUpdateScrolling( dt )
    
    NewGiftsScrollIndicatorUpdateScrollOffset( NewGiftsScrollData.scrollOffset )
end

function NewGiftsListSampleVelocity( dt )
    if NewGiftsScrollData.touchId >= 0 and NewGiftsScrollData.sampleTimer >= 0 then
        NewGiftsScrollData.sampleTimer = NewGiftsScrollData.sampleTimer - dt
        if NewGiftsScrollData.sampleTimer < 0 then
            NewGiftsScrollData.sampleTimer = NewGiftsScrollData.samplingInterval
            NewGiftsScrollData.lastVelocity = (NewGiftsScrollData.currentTouchPos - NewGiftsScrollData.sampleTouchPos) / NewGiftsScrollData.samplingInterval
            NewGiftsScrollData.sampleTouchPos = NewGiftsScrollData.currentTouchPos
        end
    end
end

function NewGiftsListTouchDown( x, y, id, c )
    if items == nil then return end

    local path = c:GetPath()
    if string.find( path, "/NewGiftsList" ) then
        if NewGiftsScrollData.touchId < 0 then
            NewGiftsScrollData.touchId = id
            NewGiftsScrollData.touchStart = x
            NewGiftsScrollData.scrolling = false
            NewGiftsScrollData.velocity = 0
            NewGiftsScrollData.lastVelocity = 0
            NewGiftsScrollData.sampleTimer = NewGiftsScrollData.samplingInterval
            NewGiftsScrollData.currentTouchPos = x
            NewGiftsScrollData.sampleTouchPos = x
            NewGiftsScrollData.draggTime = 0
        end
    end
    
end

function NewGiftsListTouchUp( x, y, id, c )
    if items == nil then return end

    if NewGiftsScrollData.scrolling and id == NewGiftsScrollData.touchId then
        NewGiftsScrollData.scrolling = false
        NewGiftsScrollData.touchId = -1
        NewGiftsScrollData.sampleTimer = -1
        NewGiftsScrollData.targetScrollOffset = NewGiftsItemClampOffset( NewGiftsScrollData.startScrollOffset + NewGiftsScrollData.touchStart - x )
        
        -- Check if dragg time is shorter than sampling rate. If so recalculate velocity manualny
        if NewGiftsScrollData.draggTime < NewGiftsScrollData.samplingInterval and NewGiftsScrollData.draggTime > 0 then
            NewGiftsScrollData.lastVelocity = (x - NewGiftsScrollData.sampleTouchPos) / NewGiftsScrollData.draggTime
        end
        NewGiftsScrollData.velocity = NewGiftsScrollData.lastVelocity
    end
end

function NewGiftsListTouchMove( x, y, id, c )
    if items == nil then return end

    if id == NewGiftsScrollData.touchId then
        if not NewGiftsScrollData.scrolling then
            if math.abs( NewGiftsScrollData.touchStart - x ) > NewGiftsScrollData.minScrollDist then
                -- Start real scrolling
                NewGiftsScrollData.touchStart = x
                NewGiftsScrollData.startScrollOffset = NewGiftsScrollData.scrollOffset
                NewGiftsScrollData.scrolling = true
            end
        else
            NewGiftsScrollData.targetScrollOffset = NewGiftsItemClampOffset( NewGiftsScrollData.startScrollOffset + NewGiftsScrollData.touchStart - x )
            NewGiftsScrollData.currentTouchPos = x
        end    
    end
end

function NewGiftsItemClampOffset( offset )
    return math.min( math.max(offset, 0), NewGiftsScrollData.maxScrollOffset )
end

function NewGiftsListUpdateScrolling( dt )
    if math.abs(NewGiftsScrollData.velocity) > NewGiftsScrollData.velocityEpsilon then
        NewGiftsScrollData.velocity = NewGiftsScrollData.velocity - NewGiftsScrollData.velocity * NewGiftsScrollData.scrollAcceleration * dt
        NewGiftsScrollData.targetScrollOffset = NewGiftsItemClampOffset( NewGiftsScrollData.targetScrollOffset - NewGiftsScrollData.velocity * dt )
    end
    
    if math.abs(NewGiftsScrollData.scrollOffset - NewGiftsScrollData.targetScrollOffset) > NewGiftsScrollData.velocityEpsilon then
        NewGiftsScrollData.scrollOffset = NewGiftsScrollData.targetScrollOffset
        root:SetX( -NewGiftsScrollData.scrollOffset )
    end  
    
    NewGiftsListClipItems()
end

function NewGiftsListClipItems()
    for _, item in ipairs( root:GetChildren() ) do
        local visible = -NewGiftsScrollData.scrollOffset + item:GetX() <= width and -NewGiftsScrollData.scrollOffset + item:GetX() + NewGiftsConsts.itemWidth >= 0
        item:SetVisibility( visible )
    end
end

function NewGiftsListDropAllItems()
    for _, item in ipairs( root:GetChildren() ) do
        item:Remove()
    end
end

function NewGiftsListCreateClipBorder()

    if clip:GetControl("ls") == nil then
        local ls = clip:InsertControl("ls")
        ls:AddRepresentation( "default", "menu2/timage.xml" )
        ls:SetRelative( true )
        ls:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
        ls:GetGraphic( "TS" ):SetScale( 1 , 1.4 )
        ls:SetY( - R( 8 ) )
        
        local lsw, lsh = ls:GetGraphic( "TS" ):GetSize()

        local rs = clip:InsertControl("rs")
        rs:AddRepresentation( "default", "menu2/timage.xml" )
        rs:SetRelative( true )
        rs:GetGraphic( "TS" ):SetFlipMode( flipModes.FM_HORIZONTAL )
        rs:GetGraphic( "TS" ):SetImage( "menu2/belt_shop.png@linear" )
        rs:GetGraphic( "TS" ):SetScale( 1 , 1.4 )
        rs:SetX( width - lsw )
        rs:SetY( - R( 8 ) )
    end
end

function NewGiftsListShowClipBorder()
    clip:GetControl("ls"):SetVisibility( true )
    clip:GetControl("rs"):SetVisibility( true )
end

function NewGiftsListHideClipBorder()
    clip:GetControl("ls"):SetVisibility( false )
    clip:GetControl("rs"):SetVisibility( false )
end

function NewGiftsListHidePosIndicator()
    indicator:SetVisibility( false )
end

function NewGiftsListShowPosIndicator()
    indicator:SetVisibility( true )
end

require( "menu2/animation.lua" )
require( "menu2/button.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/lock.lua" )

local active = false
local weapon = nil

local popup = nil
local x = 0
local y = 0

function WeaponUnlockedPopupInit( parent )
    parent:InsertFromXml( "menu2/popup_weapon_unlocked.xml" )
    popup = parent:GetControl( "WeaponUnlockedPopup" )

    x = math.floor( ( SCREEN_WIDTH - WeaponUnlockedPopupW )/2 )
    y = math.floor( ( SCREEN_HEIGHT - WeaponUnlockedPopupH )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    PopupCreateFrame( popup:GetControl( "Frame" ), WeaponUnlockedPopupW, WeaponUnlockedPopupH )
end

function WeaponUnlockedPopupSetup( w )
    weapon = w

    local item = ItemsById[weapon]
    popup:GetControl( "Weapon/Label" ):SetText( item.d )
    popup:GetControl( "Weapon/Item/Image" ):GetGraphic( "s" ):SetImage( item.f )

    local bg = popup:GetControl( "Weapon/Bg" )
    local glow = popup:GetControl( "Weapon/Item/Glow" )
    anim:Add(
        Perpetual:New(
            function( t )
                bg:SetAlpha( ( 0.75 + 0.125 * ( 1 + math.sin( t * 4 ) ) ) * 255 )
                glow:GetGraphic( "TS" ):SetAngle( t )
                glow:GetGraphic( "TS" ):SetScale( 1 + math.cos( t )*0.1 )
            end
        ),
        nil,
        "WeaponUnlockedPopup"
    )
end

function WeaponUnlockedPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        WeaponUnlockedPopupHide()
        return true
    end

    return false
end

function WeaponUnlockedPopupPress( control, path )
    if not active then
        return
    end

    if path == "/WeaponUnlockedPopup/Close" or path == "/WeaponUnlockedPopup/Get" then
        ButtonPress( control )
    end
end

function WeaponUnlockedPopupAction( path )
    if not active then
        return
    end

    if path == "/WeaponUnlockedPopup/Close" then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_NEW_WEAPON_ACTION, GameEventParam.GEP_NEW_WEAPON_BACK )
        WeaponUnlockedPopupHide()
    elseif path == "/WeaponUnlockedPopup/Get" then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_NEW_WEAPON_ACTION, GameEventParam.GEP_NEW_WEAPON_GET )
        WeaponUnlockedPopupHide()
        ShopShow( ShopTab.Weapons, nil, nil, ItemDbGetItemById( weapon ) )
    end
end

function WeaponUnlockedPopupShow()
    Lock()
    PopupLock( 0.3, "/WeaponUnlockedPopup/Overlay" )
    GameEventDispatcher:HandleGameEvent( GameEvent.GEI_NEW_WEAPON_SHOWN, 1, weapon )

    popup:SetVisibility( true )
    anim:Add(
        Slider:New( 0.3,
            function( t )
                popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
            end
        ),
        function()
            Unlock()
            active = true
        end
    )
end

function WeaponUnlockedPopupHide()
    active = false

    Lock()
    PopupUnlock( 0.3, "/WeaponUnlockedPopup/Overlay" )

    anim:Add(
        Slider:New( 0.3,
            function( t )
                popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
            end
        ),
        function()
            anim:RemoveAll( "WeaponUnlockedPopup" )
            popup:SetVisibility( false )
            Unlock()
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/star_animation.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 215 )

local iconFile = "menu2/fuel_ico.png@linear"


function NewFriendsPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_new_friends.xml" )
    popup = screen:GetControl( "/NewFriendsPopup" )
    popup:SetVisibility( false )
    
    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

end

function NewFriendsPopupFillData( iconPath , newFriendsCount )

    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconPath )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )

    local message = popup:GetControl( "Message" )
    if newFriendsCount > 1  then
        message:SetTextWithVariables( "TEXT_NEW_FRIENDS", { VALUE = tostring( newFriendsCount )} )
        else
        message:SetText( "TEXT_NEW_FRIEND" )
    end
    
    local messageReward = popup:GetControl( "MessageReward" )
    messageReward:SetTextWithVariables( "TEXT_NEW_FRIENDS_REWARD", { VALUE = tostring( newFriendsCount * InviteRewardValue)} )
    
end

function NewFriendsPopupShow( newFriendsCount )
    if active then
        return
    end
    
    active = true
    
    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    NewFriendsPopupFillData( iconFile , newFriendsCount )
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
end

function NewFriendsPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        NewFriendsPopupHide( function() ResistanceHqShow() end )
        return true
    end

    return false
end

function NewFriendsPopupPress( control, path )
    if not active then
        return
    end

    if path == "/NewFriendsPopup/OK/Button" then
        ButtonPress( control )
    end
    
end

function NewFriendsPopupAction( name )
    if not active then
        return
    end

    if name == "/NewFriendsPopup/OK/Button" then
        NewFriendsPopupHide( function() ResistanceHqShow() end )
    end
end

function NewFriendsPopupHide( callback )
    active = false
    
    
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Icon/Star" ) )
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

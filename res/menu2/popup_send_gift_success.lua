require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/star_animation.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 215 )
local height = R( 190 )

local iconFile = "menu2/fuel_ico.png@linear"
local star = nil


function SendGiftSuccessPopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_send_gift_success.xml" )
    popup = screen:GetControl( "/SendGiftSuccessPopup" )
    popup:SetVisibility( false )

    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )

    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
    star = BackgroundStar( popup, width/2, height/2, 0 )
end

function SendGiftSuccessPopupFillData( iconPath )
    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconPath )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )

    local icon2 = popup:GetControl( "Icon/Image2" )
    icon2:GetGraphic( "TS" ):SetImage( iconPath )
    local w, h = icon2:GetGraphic( "TS" ):GetSize()
    icon2:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon2:SetVisibility( true )

    icon:SetX( R(5) )
    icon:SetY( - R(3) )
    icon2:SetX(  - R(5) )
    icon2:SetY(  R(2) )

    local message =  popup:GetControl( "Message" )
    message:SetText( "TEXT_SEND_GIFT_SUCCESS" )
end

function SendGiftSuccessPopupShow()
    if active then
        return
    end
    SyncIndicatorHide()

    active = true

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    SendGiftSuccessPopupFillData( iconFile )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP )
    BackgroundStarStart( star, 0.3 )
    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
end

function SendGiftSuccessPopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        SendGiftSuccessPopupHide( function() ResistanceHqShow() end )
        return true
    end

    return false
end

function SendGiftSuccessPopupPress( control, path )
    if not active then
        return
    end

    if path == "/SendGiftSuccessPopup/OK/Button" then
        ButtonPress( control )
    end
    
end

function SendGiftSuccessPopupAction( name )
    if not active then
        return
    end

    if name == "/SendGiftSuccessPopup/OK/Button" then
        SendGiftSuccessPopupHide( function() ResistanceHqShow() end )
    end
end

function SendGiftSuccessPopupHide( callback )
    active = false

    PopupUnlock()
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
    BackgroundStarStop( star, 0.3 )

    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Icon/Star" ) )
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )
    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

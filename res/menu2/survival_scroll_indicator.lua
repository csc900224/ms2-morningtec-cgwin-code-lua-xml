require( "menu2/animation.lua" )
require( "menu2/common.lua" )

local segmentsNum = 0
local root = nil
local itemH = 0
local y = 0
local spacing = R(-7)
local lastOffset = -1
local maxOffset = -1
local activeSegment = 0

function SurvivalScrollIndicatorInit( parent, maxScrollOffset, itemHeight )
    itemH = itemHeight
    
    root = parent:GetControl( "ScrollIndicator" )
    if root == nil then
        root = parent:InsertControl( "ScrollIndicator" )
        root:SetRelative( true )
    end
    
    root:SetRelative( true )
    
    SurvivalScrollIndicatorUpdateMaxScrollOffset( maxScrollOffset )
    SurvivalScrollIndicatorUpdateScrollOffset( 0 )
end

function SurvivalScrollIndicatorUpdateMaxScrollOffset( maxScrollOffset )
    if maxOffset ~= maxScrollOffset then
        local newSegmentsNum = math.floor( maxScrollOffset / itemH ) + 1
        
        if newSegmentsNum ~= segmentsNum then
            SurvivalScrollIndicatorSetSegments( newSegmentsNum )
        end
        maxOffset = maxScrollOffset
    end
end

function SurvivalScrollIndicatorUpdateScrollOffset( scrollOffset )
    if lastOffset ~= scrollOffset then
        lastOffset = scrollOffset
        
        local newActiveSegment = 1
        if maxOffset > 0 then
            newActiveSegment = 1 + math.floor((scrollOffset / maxOffset) * (segmentsNum - 1) + 0.5)
        end
        if newActiveSegment ~= activeSegment then
            if activeSegment > 0 and activeSegment <= segmentsNum then
                root:GetChildren()[activeSegment]:GetChildren()[1]:SetRepresentation( "default" )
            end
            root:GetChildren()[newActiveSegment]:GetChildren()[1]:SetRepresentation( "active" )
            activeSegment = newActiveSegment
        end
    end
end

function SurvivalScrollIndicatorSetSegments( newSegmentsNum )
    if newSegmentsNum > segmentsNum then
        for i=1,newSegmentsNum - segmentsNum do
            local segmentRoot = root:InsertControl( tostring(segmentsNum + i) )
            segmentRoot:SetRelative( true )
            segmentRoot:InsertFromXml( "menu2/survival_scroll_indicator.xml" )
            segmentRoot:SetY( y )
            y = y + segmentRoot:GetChildren()[1]:GetGraphic( "TS" ):GetSize() + spacing
        end
    elseif newSegmentsNum < segmentsNum then
        for idx, segment in ipairs( root:GetChildren() ) do
            if idx > newSegmentsNum then
                y = y - (segment:GetChildren()[1]:GetGraphic( "TS" ):GetSize() + spacing)
                segment:Remove()
            end
        end
    end
    segmentsNum = newSegmentsNum
end
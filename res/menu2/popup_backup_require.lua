require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/star_animation.lua" )

local active = false
local popup = nil

local x = 0
local y = 0
local width = R( 235 )
local height = R( 225 )

local iconFile = "menu2/cat_02.png@linear"

function BackupRequirePopupInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_backup_require.xml" )
    popup = screen:GetControl( "/BackupRequirePopup" )
    popup:SetVisibility( false )
    
    x = math.floor( ( SCREEN_WIDTH - width )/2 )
    y = math.floor( ( SCREEN_HEIGHT - height )/2 )
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
    
    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )

end

function BackupRequirePopupFillData()

    local icon = popup:GetControl( "Icon/Image" )
    icon:GetGraphic( "TS" ):SetImage( iconFile )
    local w, h = icon:GetGraphic( "TS" ):GetSize()
    icon:GetGraphic( "TS" ):SetPivot( w/2, h/2 )
    icon:SetVisibility( true )

    local header =  popup:GetControl( "Header" )
    header:SetText( "TEXT_BACKUP_USER_OFFLINE" )
    
    local message =  popup:GetControl( "Message" )
    message:SetText( "TEXT_BACKUP_REQUIRE_LOGIN" )
end

function BackupRequirePopupShow()
    if active then
        return
    end
    
    active = true
    
    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end

    BackupRequirePopupFillData()
    
    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
    
    StartStarAnimation( popup:GetControl( "Icon/Star" ) )
    AudioManager:Play( Sfx.SFX_MENU_POPUP )
end

function BackupRequirePopupKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        BackupRequirePopupHide( PopupUnlock )
        return true
    end

    return false
end

function BackupRequirePopupPress( control, path )
    if not active then
        return
    end

    if path == "/BackupRequirePopup/Back/Button" then
        ButtonPress( control )
    elseif path == "/BackupRequirePopup/Login/Button" then
        ButtonPress( control )
    end
    
end

function BackupRequirePopupAction( name )
    if not active then
        return
    end

    if name == "/BackupRequirePopup/Back/Button" then
        BackupRequirePopupHide( PopupUnlock )
    elseif name == "/BackupRequirePopup/Login/Button" then
        BackupRequirePopupHide( LoginMethodPopupShow )
    end
    
end

function BackupRequirePopupHide( callback )
    active = false
    
    
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            StopStarAnimation( popup:GetControl( "Icon/Star" ) )
            popup:SetVisibility( false )
            if callback then callback() end
        end
    )

    AudioManager:Play( Sfx.SFX_MENU_POPUP_CLOSE )
end

local active = false

local popup = nil
local refilltext = nil
local waittext = nil

local x = 0
local y = 0

local gifts = 1

function OutOfFuelInit( screen )
    screen:GetControl( "/" ):InsertFromXml( "menu2/popup_outoffuel.xml" )
    popup = screen:GetControl( "/OutOfFuelPopup" )
    refilltext = popup:GetControl( "Refill/Text" )
    waittext = popup:GetControl( "Wait/Text" )

    x = ( SCREEN_WIDTH - OutOfFuelW )/2
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )
end

function OutOfFuelSetup( notrequired )
    local frame = popup:GetControl( "Frame" )
    for _,item in ipairs( frame:GetChildren() ) do
        item:Remove()
    end

    local header = popup:GetControl( "Header" )
    if notrequired then
        header:SetText( "TEXT_OOF_HEADER_NOTREQUIRED" )
    else
        header:SetText( "TEXT_OOF_HEADER" )
    end

    gifts = registry:Get( "/monstaz/resistance/fuelpackcount")

    popup:GetControl( "Use" ):SetVisibility( true )
    popup:GetControl( "Wait" ):SetY( R( 205 ) )

    y = math.max( ( SCREEN_HEIGHT - OutOfFuelH )/2, R(56) )
    PopupCreateFrame( frame, OutOfFuelW, OutOfFuelH )

end

function OutOfFuelShow( notrequired, back )
    active = true

    if not back then
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_SHOWN, notrequired and GameEventParam.GEP_FUEL_HUD or GameEventParam.GEP_FUEL_RAN_OUT )
        OutOfFuelSetup( notrequired )

        CountersButtonsEnable( false )
    end

    gifts = registry:Get( "/monstaz/resistance/fuelpackcount" )
    FillUseButtonInfo( popup:GetControl( "Use" ), gifts )

    PopupLock( 0.3, "/Counters/Overlay" )

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function OutOfFuelHide( invite )
    active = false
    if not invite then
        PopupUnlock( 0.3, "/Counters/Overlay" )

        anim:Add( Delay:New( 0.3 ),
            function()
                CountersButtonsEnable( true )
            end
            )
    end
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( 1-t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function OutOfFuelKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        OutOfFuelHide( false )
        return true
    end

    return false
end

function OutOfFuelPress( control, path )
    if not active then return end

    if path == "/OutOfFuelPopup/Refill" then
        ButtonPress( control )
    elseif path == "/OutOfFuelPopup/Invite" then
        ButtonPress( control )
    elseif path == "/OutOfFuelPopup/Use" then
        if gifts > 0 then
            ButtonPress( control )
        end
    elseif path == "/OutOfFuelPopup/Wait" then
        ButtonPress( control )
    end
end

function OutOfFuelAction( path )
    if not active then return end

    if path == "/OutOfFuelPopup/Refill" then
        local fuel = registry:Get( "/monstaz/cash/fuel" )
        local max = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]
        local diff = max - fuel
        local cost = diff * registry:Get( "/app-config/fuel/refillcost" )
        local soft,gold = Shop:GetCash()
        if gold < cost then
            if FuelShopCallback then FuelShopCallback() end
            OutOfFuelHide( false )
            ShopShow( ShopTab.Cash )
        else
            Shop:SetCash( soft, gold - cost )
            registry:Set( "/monstaz/cash/fuel", max )
            callback:Save()
        end
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_REFIL )
    elseif path == "/OutOfFuelPopup/Invite" then
        -- if IsLoggedIn() then
            -- OutOfFuelHide( true )
            -- anim:Add( Delay:New( 0.3 ),
                -- function()
                    -- OutOfFuelInviteShow()
                -- end
                -- )
        -- else
            -- OutOfFuelHide( false )
            -- anim:Add( Delay:New( 0.3 ),
                -- function()
                    -- LoginMethodPopupShow()
                -- end
                -- )
        -- end
		--add by csc
		OutOfFuelHide( false )
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_WAIT )
    elseif path == "/OutOfFuelPopup/Use" then
        local fuel = registry:Get( "/monstaz/cash/fuel" )
        local max = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]
        gifts = registry:Get( "/monstaz/resistance/fuelpackcount" )
        if gifts > 0 then
            registry:Set( "/monstaz/cash/fuel", math.min( max, fuel + registry:Get( "/app-config/fuel/energypack" ) ) )
            gifts  = math.max(0, gifts - 1 )
            registry:Set( "/monstaz/resistance/fuelpackcount", gifts  )
            callback:Save()
        end	

        FillUseButtonInfo( popup:GetControl( "Use" ), gifts )
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_USE_GIFT )
    elseif path == "/OutOfFuelPopup/Wait" then
        OutOfFuelHide( false )
        GameEventDispatcher:HandleGameEvent( GameEvent.GEI_FUEL_ACTION, GameEventParam.GEP_FUEL_WAIT )
    end
end

function OutOfFuelUpdate()
    if not active then return end

    local fuel = registry:Get( "/monstaz/cash/fuel" )
    local target = FuelLevels[registry:Get( "/monstaz/cash/fuelmax" )]

    local diff = target - fuel
    if diff <= 0 then
        OutOfFuelHide( false )
        return
    end

    refilltext:SetTextWithVariables( "TEXT_OOF_REFILL", { COST = tostring( diff * registry:Get( "/app-config/fuel/refillcost" ) ) } )

    local last = registry:Get( "/monstaz/cash/fueltime" )
    local now = callback:GetNetworkTime()
    local regen = registry:Get( "/app-config/fuel/regen" )
    if now == 0 then
        waittext:SetText( "TEXT_OOF_WAIT_NOTIME" )
    else
        waittext:SetTextWithVariables( "TEXT_OOF_WAIT", { TIME = TimeFormat( regen - now + last ) } )
    end
end

function FillUseButtonInfo( button , count )

    local infoBg = button:GetControl( "RedBg" )
    local valueText = infoBg:GetControl("GiftsCounter")

    local bw, bh = button:GetGraphic( "s" ):GetSize()

    infoBg:SetY( - button:GetHeight()/2 - infoBg:GetHeight()/2 + R(4) )
    infoBg:SetX( button:GetWidth()/2 - R(15) )

    local bgw, bgh = infoBg:GetGraphic( "TS" ):GetSize()

    valueText:SetText( tostring(count) )

    local scale = ( (valueText:GetTextWidth() + R(14)) / bgw ) 

    if scale > 1 then
        infoBg:GetGraphic( "TS" ):SetScale( scale, 1 )
        valueText:SetX( R(7) )
    else
        valueText:SetX( (bgw - valueText:GetTextWidth() )/2 )
    end

    if count == 0 then
        button:SetAlpha( 130 )
    else
        button:SetAlpha( 255 )
    end

end

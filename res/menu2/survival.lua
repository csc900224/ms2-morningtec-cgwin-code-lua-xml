require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/survival_item.lua" )
require( "menu2/survival_list.lua" )
require( "SurvivalDB.lua" )
local active = false

local x = R( 10 )
local y = R( 45 )

local width = R( 448 )
local height = math.min( R( 284 ), SCREEN_HEIGHT*0.86 )

local hideX = -( x + width)
local hideY = SCREEN_HEIGHT ;

local popup = nil

function SurvivalKeyDown( code )
    if not active then
        return
    end

    if code == keys.KEY_ESCAPE then
        SurvivalHide( PopupUnlock )
        return true
    end

    return false
end

function SurvivalAction( name )

    if not active then
        return
    end

    if name == "/Survival/Back/Button" then
        SurvivalHide( PopupUnlock )
    end

end

function SurvivalPress( control, path )

    if not active then
        return
    end

    if path == "/Survival/Back/Button" then
        ButtonPress( control )
    end

end

function SurvivalShow()
    active = true
    callback:Playhaven( "survival" )

    local overlay = screen:GetControl( "/Overlay" )
    if overlay:GetVisibility() == false then
        PopupLock()
    end


    popup:GetControl( "LevelInfo/SurvivalLevelInfo"):SetTextWithVariables( "TEXT_SURVIVAL_LEVEL", { VALUE = tostring(registry:Get( "/monstaz/player/level") )} )

    SurvivalListSetContent( SurvivalLevelsInfo )

    OnSurvivalShow()

    popup:SetVisibility( true )
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( SCREEN_HEIGHT - SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        )
    )
end

function SurvivalHide( callback )
    active = false
    OnSurvivalHide()
    anim:Add( Slider:New( 0.3,
        function( t )
            popup:SetY( y + SmoothStep( t ) * ( SCREEN_HEIGHT - y ) )
        end
        ),
        function()
            if callback then callback() end
            popup:SetVisibility( false )
        end
    )
end

function SurvivalInit( screen )

    screen:GetControl( "/" ):InsertFromXml( "menu2/survival.xml" )

    popup = screen:GetControl( "/Survival" )

    x = ( SCREEN_WIDTH - width )/2
    y = ( SCREEN_HEIGHT - height )/2 - R(10)
    
    popup:SetX( x )
    popup:SetY( SCREEN_HEIGHT )

    local frame = popup:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )
    
    HelpButtonInit( popup , width )

    local buttonIcon = popup:GetControl( "Back/Button")
    local w, h = buttonIcon:GetGraphic( "TS" ):GetSize()
    
    local buttonBack = popup:GetControl( "Back")
    
    buttonBack:SetX(  width  - w )
    buttonBack:SetY( height + R(3) )
    

    local backupListWidth = width - R(50)
    local backupListHeight = height - R( 60 )
    
    SurvivalListInit( popup:GetControl( "SurvivalListRoot"), backupListWidth, backupListHeight )
    
    popup:SetVisibility( false )
end

function SurvivalUpdate( dt )
    if not active then return end
    SurvivalListUpdate( dt )
end

function SurvivalTouchDown( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end

    SurvivalListTouchDown( x, y, id, c )
    
    local path = c:GetPath()
    
    if string.find(path, "/Buttons/") then
        --ButtonPress( c )
    end
end

function SurvivalTouchUp( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    SurvivalListTouchUp( x, y, id, c )
end

function SurvivalTouchMove( x, y, id )
    if not active then return end
    local c = screen:GetTouchableControl( x, y )
    if not c then return end
    SurvivalListTouchMove( x, y, id, c )
end

function IsSurvivalListActive()
    return active
end

require( "menu2/animation.lua" )
require( "menu2/common.lua" )
require( "menu2/popup.lua" )
require( "menu2/avatar_social.lua" )

local margin = R(5)
local startY = R( 22 )

local item = nil

function SocialItemInit( parent, width, height, friend )

    if not friend then return end
    -- Insert from xml and get item root
    parent:InsertFromXml( "menu2/social_item.xml" )
    root = parent:GetControl( "SocialItem" )

    local frame = root:GetControl( "Frame" )
    PopupCreateFrame( frame, width, height )


    -- avatar img 
    local playerAvatarPath = GetAvatarImgPath( friend )

    local avatar = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/Avatar" )
    local scale = SetAvatarImg( avatar , playerAvatarPath )

    if IsBot( friend ) then 
        avatar:GetGraphic( "TS" ):SetImage( avatarDefultPathPremission )
    end

    local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
    avatar:SetX( ( width - aw*scale ) /2)
    avatar:SetY( startY + margin )

    -- vip frame
    local vip_frame = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/VIP1" )
    vip_frame:SetX( ( width - aw*scale ) /2  )
    vip_frame:SetY( startY + margin )

    local vip_badge = parent:GetControl( "SocialItem/InfoRoot/VIP2" )
    vip_badge:SetX( ( width - parent:GetControl( "SocialItem/InfoRoot/VIP2/Badge"):GetWidth() ) /2)
    vip_badge:SetY( startY + ah * scale )

    local isUserVip = ( tonumber( friend.IsVip ) == 1 )
    vip_frame:SetVisibility( isUserVip )
    vip_badge:SetVisibility( isUserVip )

    -- level info 
    local lvlInfo  = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/LevelInfo")
    lvlInfo:GetControl( "TextLevel"):SetText("TEXT_LVL")
    lvlInfo:GetControl( "LevelValue"):SetText( friend.Level )
    lvlInfo:SetY( startY - R(2) )
    lvlInfo:SetX( avatar:GetX() - R( 12 ) )

    -- bg - avatar img bottom line
    local bg = parent:GetControl( "SocialItem/InfoRoot/BG" )
    bg:SetX( ( width - aw * scale )/2 )
    CreateBottomLine( bg, aw * scale, startY + ah * scale )

    -- player Name
    -- text vesrion disabled - used for debug 
    local name = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/Name" )
    name:SetY( startY + ah * scale + 2.5 * margin )
    name:SetTextWidth( width );
    name:SetText( friend.Name )
    name:SetVisibility( false )

    -- img version
    local nameImg = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/NameImage" )

    callback:ResistanceSetUsernameImg( nameImg:GetPath() , friend.Name )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 

    nameImg:SetX( ( width - w )/2)
    nameImg:SetY( startY + ah * scale + 2.5 * margin )
    nameImg:SetVisibility( true )

    -- buttons
    local moreButton = parent:GetControl( "SocialItem/InfoRoot/Buttons/More" )
    local removeButton = parent:GetControl( "SocialItem/InfoRoot/Buttons/Remove" )

    local buttonW , buttonH = moreButton:GetGraphic( "TS" ):GetSize()

    local marginName =  R(8)
    local posX = ( width - buttonW ) / 2
    local posY =  startY + ah * scale + margin + buttonH + marginName

    moreButton:SetX( posX )
    moreButton:SetY( posY )

    local removeButton = parent:GetControl( "SocialItem/InfoRoot/Buttons/Remove" )
    local removeButtonW , removeButtonH = removeButton:GetGraphic( "TS" ):GetSize()
    removeButton:SetX( width -  removeButtonW - marginName/2)
    removeButton:SetY( marginName )
    
    -- remove button disabled for bot
    if IsBot( friend ) then
        removeButton:SetVisibility( false )
    end

    moreButton:InsertControl( parent:GetName() )
    removeButton:InsertControl( parent:GetName() )
end

function SocialAvatarFill( parent, width, height, friend )
    -- used for filling avatar in friend info screen
    if not friend then return end

    --disable small level info
    parent:GetControl( "SocialItem/InfoRoot/SocialInfo/LevelInfo"):SetVisibility( false )

    --disable button
    parent:GetControl( "SocialItem/InfoRoot/Buttons/More" ):SetVisibility( false )
    parent:GetControl( "SocialItem/InfoRoot/Buttons/Remove" ):SetVisibility( false )

    --NAME disable small name 
    parent:GetControl( "SocialItem/InfoRoot/SocialInfo/Name" ):SetVisibility( false )
    --parent:GetControl( "SocialItem/InfoRoot/SocialInfo/NameImage" ):SetVisibility( false )

    -- AVATAR 
    local playerAvatarPath = GetAvatarImgPath( friend )

    local avatar = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/Avatar" )
    local scale  =  1

    if not string.find(playerAvatarPath,avatarDefultPath) then
        avatar:GetGraphic( "TS" ):SetImage( avatarDefultPath )
        local defaultW, defaultH = avatar:GetGraphic( "TS" ):GetSize()

        avatar:GetGraphic( "TS" ):SetImage( playerAvatarPath )
        local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
        scale = defaultW / aw
    else
        if IsBot( friend ) then 
            avatar:GetGraphic( "TS" ):SetImage( avatarDefultPathPremission )
        else
            avatar:GetGraphic( "TS" ):SetImage( playerAvatarPath )
        end
    end 

    local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
    -- avatar scale
    avatar:GetGraphic( "TS" ):SetScale( scale )

    avatar:SetX( margin )
    avatar:SetY( startY + margin )

    -- vip frame
    local vip_frame = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/VIP1" )
    vip_frame:SetX( margin )
    vip_frame:SetY( startY + margin )

    local vip_badge = parent:GetControl( "SocialItem/InfoRoot/VIP2" )
    vip_badge:SetX( ( width - parent:GetControl( "SocialItem/InfoRoot/VIP2/Badge"):GetWidth() ) /2)
    vip_badge:SetY( startY + ah * scale )

    local isPlayerVip = ( tonumber( friend.IsVip ) == 1 )
    vip_frame:SetVisibility( isPlayerVip )
    vip_badge:SetVisibility( isPlayerVip )

   --bottom line of avatar
    local bg = parent:GetControl( "SocialItem/InfoRoot/BG" )

    if bg:GetControl("lb") == nil then
        CreateBottomLine( bg, aw * scale, startY + ah * scale )
    end
    bg:SetX( margin )
    -- img version
    local nameImg = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/NameImage" )
    callback:ResistanceSetUsernameImg( nameImg:GetPath() , friend.Name )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 

    nameImg:SetX( ( width - w )/2)
    nameImg:SetY( startY + ah * scale + 2.5 * margin )
    nameImg:SetVisibility( true )
end

local itemSelected = nil

function SocialItemAction( control, path )

        if string.find( path, "/More" ) then
            local idxFriend = control:GetChildren()[2]:GetName()
            local friend = Friends[tonumber ( idxFriend )]
            itemSelected = friend
            GetInfo( friend )
        elseif string.find( path, "/Remove" ) then
            local idxFriend = control:GetChildren()[1]:GetName()
            local friend = Friends[tonumber ( idxFriend )]
            ConfirmRemovePopupShow( function() callback:ResistanceRemoveFriend( friend.UniqueId ) SyncIndicatorShow() end )
        end
end

function GetInfo( friend )
    --callback:ResistanceOnGetFriendInfo( friend.UniqueId )
    FriendInfoShow( friend )
end

function GetInfoSucces( uniqueId )
    SyncIndicatorHide()
    FriendInfoShow( itemSelected )
end

function GetInfoFailed()
    SyncIndicatorHide()
    FriendInfoShow( itemSelected )
end

function SocialAvatarFillAccount( parent, width, height, friend  )
    -- used for filling avatar in friend info screen
    if not friend then return end

    -- AVATAR 
    local frame = parent:GetControl( "SocialItem/Frame" )
    PopupCreateFrame( frame, width, height )

    local playerAvatarPath = GetAvatarImgPath( friend )

    local avatar = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/Avatar" )
    local scale  =  1

    if not string.find(playerAvatarPath,avatarDefultPath) then
        avatar:GetGraphic( "TS" ):SetImage( avatarDefultPath )
        local defaultW, defaultH = avatar:GetGraphic( "TS" ):GetSize()

        avatar:GetGraphic( "TS" ):SetImage( playerAvatarPath )
        local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
        scale = defaultW / aw
    else
        avatar:GetGraphic( "TS" ):SetImage( playerAvatarPath )
    end 

    local aw, ah = avatar:GetGraphic( "TS" ):GetSize()
    -- avatar scale
    avatar:GetGraphic( "TS" ):SetScale( scale )

    local yStart = R(25)

    avatar:SetX( ( width - aw*scale ) /2)
    avatar:SetY( yStart + margin )

    -- vip frame
    local vip_frame = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/VIP1" )
    vip_frame:SetX( ( width - aw*scale ) /2)
    vip_frame:SetY( yStart + margin )

    local vip_badge = parent:GetControl( "SocialItem/InfoRoot/VIP2" )
    vip_badge:SetX( ( width - parent:GetControl( "SocialItem/InfoRoot/VIP2/Badge"):GetWidth() ) /2)
    vip_badge:SetY( yStart + ah * scale )

    local vip_status = registry:Get( "/monstaz/subscription" )
    vip_frame:SetVisibility( vip_status )
    vip_badge:SetVisibility( vip_status )

    --bottom line of avatar
    local bg = parent:GetControl( "SocialItem/InfoRoot/BG" )

    if bg:GetControl("lb") == nil then
        CreateBottomLine( bg, aw * scale, yStart + ah * scale )
    end

    bg:SetX( ( width - aw * scale )/2 )
    -- player Name
    -- text vesrion disabled - used for debug 
    local name = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/Name" )
    name:SetY( yStart + ah * scale + margin )
    name:SetTextWidth( width );
    name:SetText( friend.Name )
    name:SetVisibility( false )

    -- img version
    local nameImg = parent:GetControl( "SocialItem/InfoRoot/SocialInfo/NameImage" )
    callback:ResistanceSetUsernameImg( nameImg:GetPath() , friend.Name )
    local w, h = nameImg:GetGraphic( "TS" ):GetSize() 

    nameImg:SetX( ( width - w )/2)
    nameImg:SetY( yStart + ah * scale + 2.5 * margin )
    nameImg:SetVisibility( true )

    --disable small level info
    parent:GetControl( "SocialItem/InfoRoot/SocialInfo/LevelInfo"):SetVisibility( false )

    --disable button
    parent:GetControl( "SocialItem/InfoRoot/Buttons/More" ):SetVisibility( false )
    parent:GetControl( "SocialItem/InfoRoot/Buttons/Remove" ):SetVisibility( false )
end

function FriendRemoveSucces( friendId )
    SyncIndicatorHide()
    PopupUnlock()
    RefreshFriendList( friendId )
end

function FriendRemoveFailed()
    SyncIndicatorHide()
    ErrorPopupShow( "TEXT_RESISTANCE_TIMED_OUT" )
end

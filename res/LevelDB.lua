LevelIcons = {
    "menu2/mission_kill.png@linear",
    "menu2/mission_cat.png@linear",
    "menu2/mission_card.png@linear",
    "menu2/mission_walk.png@linear",
    "menu2/mission_anten.png@linear",
    "menu2/mission_ufo.png@linear",
    "menu2/mission_boss.png@linear",
}

Tutorial01 = {
    f = "maps/tutorial01.xml",
    n = "TEXT_LEVEL_TUTORIAL_01_NAME",
    d = "TEXT_LEVEL_TUTORIAL_01_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Tutorial02 = {
    f = "maps/tutorial02.xml",
    n = "TEXT_LEVEL_TUTORIAL_02_NAME",
    d = "TEXT_LEVEL_TUTORIAL_02_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Tutorial03 = {
    f = "maps/tutorial03.xml",
    n = "TEXT_LEVEL_TUTORIAL_03_NAME",
    d = "TEXT_LEVEL_TUTORIAL_03_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

Tutorial04 = {
    f = "maps/tutorial04.xml",
    n = "TEXT_LEVEL_TUTORIAL_04_NAME",
    d = "TEXT_LEVEL_TUTORIAL_04_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

TutorialLevels = {
    Tutorial01,
    Tutorial02,
    Tutorial03,
    Tutorial04
}

Suburbs01 = {
    f = "maps/suburbs01.xml",
    n = "TEXT_LEVEL_SUBURBS_01_NAME",
    d = "TEXT_LEVEL_SUBURBS_01_DESC",
    i = 2,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs02 = {
    f = "maps/suburbs02.xml",
    n = "TEXT_LEVEL_SUBURBS_02_NAME",
    d = "TEXT_LEVEL_SUBURBS_02_DESC",
    i = 5,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs03 = {
    f = "maps/suburbs03.xml",
    n = "TEXT_LEVEL_SUBURBS_03_NAME",
    d = "TEXT_LEVEL_SUBURBS_03_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs04 = {
    f = "maps/suburbs04.xml",
    n = "TEXT_LEVEL_SUBURBS_04_NAME",
    d = "TEXT_LEVEL_SUBURBS_04_DESC",
    i = 3,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs05 = {
    f = "maps/suburbs05.xml",
    n = "TEXT_LEVEL_SUBURBS_05_NAME",
    d = "TEXT_LEVEL_SUBURBS_05_DESC",
    i = 5,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs06 = {
    f = "maps/suburbs06.xml",
    n = "TEXT_LEVEL_SUBURBS_06_NAME",
    d = "TEXT_LEVEL_SUBURBS_06_DESC",
    i = 3,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs07 = {
    f = "maps/suburbs07.xml",
    n = "TEXT_LEVEL_SUBURBS_07_NAME",
    d = "TEXT_LEVEL_SUBURBS_07_DESC",
    i = 5,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs08 = {
    f = "maps/suburbs08.xml",
    n = "TEXT_LEVEL_SUBURBS_08_NAME",
    d = "TEXT_LEVEL_SUBURBS_08_DESC",
    i = 2,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs09 = {
    f = "maps/suburbs09.xml",
    n = "TEXT_LEVEL_SUBURBS_09_NAME",
    d = "TEXT_LEVEL_SUBURBS_09_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs10 = {
    f = "maps/suburbs10.xml",
    n = "TEXT_LEVEL_SUBURBS_10_NAME",
    d = "TEXT_LEVEL_SUBURBS_10_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs11 = {
    f = "maps/suburbs11.xml",
    n = "TEXT_LEVEL_SUBURBS_11_NAME",
    d = "TEXT_LEVEL_SUBURBS_11_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs12 = {
    f = "maps/suburbs12.xml",
    n = "TEXT_LEVEL_SUBURBS_12_NAME",
    d = "TEXT_LEVEL_SUBURBS_12_DESC",
    i = 3,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs13 = {
    f = "maps/suburbs13.xml",
    n = "TEXT_LEVEL_SUBURBS_13_NAME",
    d = "TEXT_LEVEL_SUBURBS_13_DESC",
    i = 3,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs14 = {
    f = "maps/suburbs14.xml",
    n = "TEXT_LEVEL_SUBURBS_14_NAME",
    d = "TEXT_LEVEL_SUBURBS_14_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs15 = {
    f = "maps/suburbs15.xml",
    n = "TEXT_LEVEL_SUBURBS_15_NAME",
    d = "TEXT_LEVEL_SUBURBS_15_DESC",
    i = 5,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs16 = {
    f = "maps/suburbs16.xml",
    n = "TEXT_LEVEL_SUBURBS_16_NAME",
    d = "TEXT_LEVEL_SUBURBS_16_DESC",
    i = 5,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs17 = {
    f = "maps/suburbs17.xml",
    n = "TEXT_LEVEL_SUBURBS_17_NAME",
    d = "TEXT_LEVEL_SUBURBS_17_DESC",
    i = 2,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs18 = {
    f = "maps/suburbs18.xml",
    n = "TEXT_LEVEL_SUBURBS_18_NAME",
    d = "TEXT_LEVEL_SUBURBS_18_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Suburbs19 = {
    f = "maps/suburbs19.xml",
    n = "TEXT_LEVEL_SUBURBS_19_NAME",
    d = "TEXT_LEVEL_SUBURBS_19_DESC",
    i = 1,
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

SuburbsLevels = {
    Suburbs01,
    Suburbs02,
    Suburbs03,
    Suburbs04,
    Suburbs05,
    Suburbs06,
    Suburbs07,
    Suburbs08,
    Suburbs09,
    Suburbs10,
    Suburbs11,
    Suburbs12,
    Suburbs13,
    Suburbs14,
    Suburbs15,
    Suburbs16,
    Suburbs17,
    Suburbs18,
    Suburbs19
}

City01 = {
    f = "maps/city01.xml",
    n = "TEXT_LEVEL_CITY_01_NAME",
    d = "TEXT_LEVEL_CITY_01_DESC",
    i = 2,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City02 = {
    f = "maps/city02.xml",
    n = "TEXT_LEVEL_CITY_02_NAME",
    d = "TEXT_LEVEL_CITY_02_DESC",
    i = 5,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City03 = {
    f = "maps/city03.xml",
    n = "TEXT_LEVEL_CITY_03_NAME",
    d = "TEXT_LEVEL_CITY_03_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City04 = {
    f = "maps/city04.xml",
    n = "TEXT_LEVEL_CITY_04_NAME",
    d = "TEXT_LEVEL_CITY_04_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City05 = {
    f = "maps/city05.xml",
    n = "TEXT_LEVEL_CITY_05_NAME",
    d = "TEXT_LEVEL_CITY_05_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City06 = {
    f = "maps/city06.xml",
    n = "TEXT_LEVEL_CITY_06_NAME",
    d = "TEXT_LEVEL_CITY_06_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City07 = {
    f = "maps/city07.xml",
    n = "TEXT_LEVEL_CITY_07_NAME",
    d = "TEXT_LEVEL_CITY_07_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City08 = {
    f = "maps/city08.xml",
    n = "TEXT_LEVEL_CITY_08_NAME",
    d = "TEXT_LEVEL_CITY_08_DESC",
    i = 5,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City09 = {
    f = "maps/city09.xml",
    n = "TEXT_LEVEL_CITY_09_NAME",
    d = "TEXT_LEVEL_CITY_09_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City10 = {
    f = "maps/city10.xml",
    n = "TEXT_LEVEL_CITY_10_NAME",
    d = "TEXT_LEVEL_CITY_10_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City11 = {
    f = "maps/city11.xml",
    n = "TEXT_LEVEL_CITY_11_NAME",
    d = "TEXT_LEVEL_CITY_11_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City12 = {
    f = "maps/city12.xml",
    n = "TEXT_LEVEL_CITY_12_NAME",
    d = "TEXT_LEVEL_CITY_12_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City13 = {
    f = "maps/city13.xml",
    n = "TEXT_LEVEL_CITY_13_NAME",
    d = "TEXT_LEVEL_CITY_13_DESC",
    i = 5,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City14 = {
    f = "maps/city14.xml",
    n = "TEXT_LEVEL_CITY_14_NAME",
    d = "TEXT_LEVEL_CITY_14_DESC",
    i = 3,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City15 = {
    f = "maps/city15.xml",
    n = "TEXT_LEVEL_CITY_15_NAME",
    d = "TEXT_LEVEL_CITY_15_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City16 = {
    f = "maps/city16.xml",
    n = "TEXT_LEVEL_CITY_16_NAME",
    d = "TEXT_LEVEL_CITY_16_DESC",
    i = 1,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City17 = {
    f = "maps/city17.xml",
    n = "TEXT_LEVEL_CITY_17_NAME",
    d = "TEXT_LEVEL_CITY_17_DESC",
    i = 5,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City18 = {
    f = "maps/city18.xml",
    n = "TEXT_LEVEL_CITY_18_NAME",
    d = "TEXT_LEVEL_CITY_18_DESC",
    i = 4,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

City19 = {
    f = "maps/city19.xml",
    n = "TEXT_LEVEL_CITY_19_NAME",
    d = "TEXT_LEVEL_CITY_19_DESC",
    i = 5,
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

CityLevels = {
    City01,
    City02,
    City04,
    City06,
    City07,
    City08,
    City09,
    City10,
    City11,
    City12,
    City13,
    City14,
    City15,
    City16,
    City17,
    City18,
    City19
}

Desert01 = {
    f = "maps/desert01.xml",
    n = "TEXT_LEVEL_DESERT_01_NAME",
    d = "TEXT_LEVEL_DESERT_01_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert02 = {
    f = "maps/desert02.xml",
    n = "TEXT_LEVEL_DESERT_02_NAME",
    d = "TEXT_LEVEL_DESERT_02_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert03 = {
    f = "maps/desert03.xml",
    n = "TEXT_LEVEL_DESERT_03_NAME",
    d = "TEXT_LEVEL_DESERT_03_DESC",
    i = 3,
    a = { AtlasSet.EnvDesert }
}

Desert04 = {
    f = "maps/desert04.xml",
    n = "TEXT_LEVEL_DESERT_04_NAME",
    d = "TEXT_LEVEL_DESERT_04_DESC",
    i = 3,
    a = { AtlasSet.EnvDesert }
}

Desert05 = {
    f = "maps/desert05.xml",
    n = "TEXT_LEVEL_DESERT_05_NAME",
    d = "TEXT_LEVEL_DESERT_05_DESC",
    i = 6,
    a = { AtlasSet.EnvDesert }
}

Desert06 = {
    f = "maps/desert06.xml",
    n = "TEXT_LEVEL_DESERT_06_NAME",
    d = "TEXT_LEVEL_DESERT_06_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert07 = {
    f = "maps/desert07.xml",
    n = "TEXT_LEVEL_DESERT_07_NAME",
    d = "TEXT_LEVEL_DESERT_07_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert08 = {
    f = "maps/desert08.xml",
    n = "TEXT_LEVEL_DESERT_08_NAME",
    d = "TEXT_LEVEL_DESERT_08_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert09 = {
    f = "maps/desert09.xml",
    n = "TEXT_LEVEL_DESERT_09_NAME",
    d = "TEXT_LEVEL_DESERT_09_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert10 = {
    f = "maps/desert10.xml",
    n = "TEXT_LEVEL_DESERT_10_NAME",
    d = "TEXT_LEVEL_DESERT_10_DESC",
    i = 3,
    a = { AtlasSet.EnvDesert }
}

Desert11 = {
    f = "maps/desert11.xml",
    n = "TEXT_LEVEL_DESERT_11_NAME",
    d = "TEXT_LEVEL_DESERT_11_DESC",
    i = 6,
    a = { AtlasSet.EnvDesert }
}

Desert12 = {
    f = "maps/desert12.xml",
    n = "TEXT_LEVEL_DESERT_12_NAME",
    d = "TEXT_LEVEL_DESERT_12_DESC",
    i = 1,
    a = { AtlasSet.EnvDesert }
}

Desert13 = {
    f = "maps/desert13.xml",
    n = "TEXT_LEVEL_DESERT_13_NAME",
    d = "TEXT_LEVEL_DESERT_13_DESC",
    i = 5,
    a = { AtlasSet.EnvDesert }
}

Desert14 = {
    f = "maps/desert14.xml",
    n = "TEXT_LEVEL_DESERT_14_NAME",
    d = "TEXT_LEVEL_DESERT_14_DESC",
    i = 4,
    a = { AtlasSet.EnvDesert }
}

DesertLevels = {
    Desert01,
    Desert02,
    Desert03,
    Desert04,
    Desert05,
    Desert06,
    Desert07,
    Desert08,
    Desert09,
    Desert10,
    Desert11,
    Desert12,
    Desert13,
    Desert14
}

Ice01 = {
    f = "maps/ice01.xml",
    n = "TEXT_LEVEL_ICE_01_NAME",
    d = "TEXT_LEVEL_ICE_01_DESC",
    i = 4,
    a = { AtlasSet.EnvIce }
}

Ice02 = {
    f = "maps/ice02.xml",
    n = "TEXT_LEVEL_ICE_02_NAME",
    d = "TEXT_LEVEL_ICE_02_DESC",
    i = 1,
    a = { AtlasSet.EnvIce }
}

Ice03 = {
    f = "maps/ice03.xml",
    n = "TEXT_LEVEL_ICE_03_NAME",
    d = "TEXT_LEVEL_ICE_03_DESC",
    i = 3,
    a = { AtlasSet.EnvIce }
}

Ice04 = {
    f = "maps/ice04.xml",
    n = "TEXT_LEVEL_ICE_04_NAME",
    d = "TEXT_LEVEL_ICE_04_DESC",
    i = 1,
    a = { AtlasSet.EnvIce }
}

Ice05 = {
    f = "maps/ice05.xml",
    n = "TEXT_LEVEL_ICE_05_NAME",
    d = "TEXT_LEVEL_ICE_05_DESC",
    i = 4,
    a = { AtlasSet.EnvIce }
}

Ice06 = {
    f = "maps/ice06.xml",
    n = "TEXT_LEVEL_ICE_06_NAME",
    d = "TEXT_LEVEL_ICE_06_DESC",
    i = 4,
    a = { AtlasSet.EnvIce }
}

Ice07 = {
    f = "maps/ice07.xml",
    n = "TEXT_LEVEL_ICE_07_NAME",
    d = "TEXT_LEVEL_ICE_07_DESC",
    i = 4,
    a = { AtlasSet.EnvIce }
}

Ice08 = {
    f = "maps/ice08.xml",
    n = "TEXT_LEVEL_ICE_08_NAME",
    d = "TEXT_LEVEL_ICE_08_DESC",
    i = 4,
    a = { AtlasSet.EnvIce }
}

Ice09 = {
    f = "maps/ice09.xml",
    n = "TEXT_LEVEL_ICE_09_NAME",
    d = "TEXT_LEVEL_ICE_09_DESC",
    i = 4,
    a = { AtlasSet.EnvIce }
}

Ice10 = {
    f = "maps/ice10.xml",
    n = "TEXT_LEVEL_ICE_10_NAME",
    d = "TEXT_LEVEL_ICE_10_DESC",
    i = 1,
    a = { AtlasSet.EnvIce }
}

IceLevels = {
    Ice01,
    Ice02,
    Ice03,
    Ice04,
    Ice05,
    Ice06,
    Ice07,
    Ice08,
    Ice09,
    Ice10
}

Boss01 = {
    f = "maps/boss01.xml",
    n = "TEXT_LEVEL_BOSS_01_NAME",
    d = "TEXT_LEVEL_BOSS_01_DESC",
    i = 7,
    m = { EntityType.MechaBoss, EntityType.OctopusSimple, EntityType.SectoidSimple },
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

Boss02 = {
    f = "maps/boss02.xml",
    n = "TEXT_LEVEL_BOSS_02_NAME",
    d = "TEXT_LEVEL_BOSS_02_DESC",
    i = 7,
    m = { EntityType.SowerBoss, EntityType.FloaterSower },
    a = { AtlasSet.EnvDesert }
}

Boss03 = {
    f = "maps/boss03.xml",
    n = "TEXT_LEVEL_BOSS_03_NAME",
    d = "TEXT_LEVEL_BOSS_03_DESC",
    i = 7,
    m = { EntityType.OctobrainBoss, EntityType.OctobrainBossClone, EntityType.OctopusSimple },
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Boss04 = {
    f = "maps/boss04.xml",
    n = "TEXT_LEVEL_BOSS_04_NAME",
    d = "TEXT_LEVEL_BOSS_04_DESC",
    i = 7,
    m = { EntityType.MechaBoss, EntityType.OctopusSimple, EntityType.SectoidSimple },
    a = { AtlasSet.EnvIce }
}

BossLevels = {
    Boss01,
    Boss02,
    Boss03,
    Boss04
}

Survival01 = {
    f = "maps/survival01.xml",
    n = "TEXT_SURVIVAL_HEADER_1",
    d = "TEXT_SURVIVAL_DESC",
    i = 1,
    m = { EntityType.SectoidSimple, EntityType.Crab, EntityType.KillerWhaleSimple },
    e = { 0, 0, 0 },
    a = { AtlasSet.EnvSuburbs, AtlasSet.EnvUrban }
}

Survival02 = {
    f = "maps/survival02.xml",
    n = "TEXT_SURVIVAL_HEADER_2",
    d = "TEXT_SURVIVAL_DESC",
    i = 1,
    m = { EntityType.SectoidShooting, EntityType.Nerval, EntityType.OctopusSimple },
    e = { 0, 1, 1 },
    a = { AtlasSet.EnvCity, AtlasSet.EnvUrban }
}

Survival03 = {
    f = "maps/survival03.xml",
    n = "TEXT_SURVIVAL_HEADER_3",
    d = "TEXT_SURVIVAL_DESC",
    i = 1,
    m = { EntityType.OctopusShotAware, EntityType.KillerWhale, EntityType.SqueezerTurning },
    e = { 2, 1, 0 },
    a = { AtlasSet.EnvDesert }
}

Survival04 = {
    f = "maps/survival04.xml",
    n = "TEXT_SURVIVAL_HEADER_4",
    d = "TEXT_SURVIVAL_DESC",
    i = 1,
    m = { EntityType.HoundSimple, EntityType.FishThrowing, EntityType.FloaterSimple },
    e = { 2, 1, 3 },
    a = { AtlasSet.EnvIce }
}

SurvivalLevels = {
    Survival01,
    Survival02,
    Survival03,
    Survival04
}

LevelGroup = {}
LevelGroup.Suburbs = 1
LevelGroup.City = 2
LevelGroup.Desert = 3
LevelGroup.Tutorial = 4
LevelGroup.Ice = 5
LevelGroup.Boss = 6
LevelGroup.Survival = 7

Levels = {}
Levels[LevelGroup.Suburbs] = SuburbsLevels
Levels[LevelGroup.City] = CityLevels
Levels[LevelGroup.Desert] = DesertLevels
Levels[LevelGroup.Tutorial] = TutorialLevels
Levels[LevelGroup.Ice] = IceLevels
Levels[LevelGroup.Boss] = BossLevels
Levels[LevelGroup.Survival] = SurvivalLevels

LevelsLUT = {}
function ParseLevels( levels )
    for _, l in ipairs( levels ) do
        if l.f == nil then
            ParseLevels( l )
        else
            LevelsLUT[l.f] = l
        end
    end
end
ParseLevels( Levels )

function LevelCheckAtlas( mapPath, atlasSet )
    local lvl = LevelsLUT[mapPath]
    if not lvl then return false end
    local retVal = false
    for _, as in ipairs( lvl.a ) do
        if as == atlasSet then
            retVal = true
            break
        end
    end
    return retVal
end

Element = {
    electric = 1,
    fire = 2,
    frost = 4
}

UpgradeUzi = {
    Delay = {1, 1, 0.85, 0.8, 0.8, 0.75},
    Damage = {0.10, 0.11, 0.12, 0.13, 0.14, 0.15},
    Clip = {120, 160, 240, 320, 400, -1},
    Elements = {0, Element.electric, 0, 0, 0, 0}
}

UpgradeShotgun = {
    Delay = {40, 38, 36, 34, 30, 30},
    Damage = {0.16, 0.18, 0.19, 0.21, 0.22, 0.24},
    Clip = {2, 3, 4, 5, 6, -1},
    Bullets = {5, 7, 7, 8, 8, 9},
    Elements = {0, Element.electric, 0, 0, 0, 0}
}

UpgradePistol = {
    Delay = {7, 7, 6.5, 6.5, 6.5, 5.5},
    Damage = {2.5, 2.75, 3.00, 3.25, 3.50, 3.75},
    Clip = {25, 25, 35, 50, 70, -1},
    Elements = {0, Element.electric, 0, 0, 0, 0}
}

UpgradeNailer = {
    Delay = {38, 34, 30, 27, 25, 20},
    Damage = {0.58, 0.64, 0.70, 0.76, 0.82, 0.88},
    LifeTime = {25, 30, 35, 45, 60, 80},
    Clip = {4, 5, 7, 10, 15, -1},
    Elements = {0, Element.electric, 0, 0, 0, 0}
}

UpgradeRipper = {
    Delay = {12, 11, 10.5, 9, 8, 8},
    Damage = {4.80, 5.28, 5.76, 6.24, 6.72, 7.20},
    Clip = {100, 140, 140, 200, 200, -1},
    Elements = {0, Element.electric, 0, 0, 0, 0}
}

UpgradeSpiral = {
    Delay = {17, 17, 15, 12, 10, 10},
    Damage = {3.67, 4.03, 4.40, 4.77, 5.13, 5.50},
    Clip = {30, 40, 40, 60, 100, -1},
    Elements = {Element.fire, 0, 0, Element.electric, 0, 0}
}

UpgradeRocketLauncher = {
    Delay = {45, 40, 35, 35, 30, 25},
    Damage = {13.00, 14.30, 15.60, 16.90, 18.20, 19.50},
    Clip = {6, 9, 9, 12, 18, -1},
    Elements = {Element.fire, 0, 0, Element.electric, 0, 0}
}

UpgradeElectricity = {
    Delay = {0, 0, 0, 0, 0, 0},
    Damage = {0.50, 0.55, 0.60, 0.65, 0.70, 0.75},
    Clip = {150, 170, 190, 190, 220, -1},
    Elements = {Element.fire, 0, 0, Element.electric, 0, 0}
}

UpgradeFlamer = {
    Delay = {0, 0, 0, 0, 0, 0},
    Damage = {0.85, 0.94, 1.02, 1.11, 1.19, 1.28},
    Clip = {100, 120, 140, 140, 180, -1},
    Elements = {Element.fire, 0, 0, Element.electric, 0, 0}
}

UpgradeMagnum = {
    Delay = {22, 19, 19, 17, 17, 15},
    Damage = {10.77, 11.84, 12.92, 14, 15.07, 16.15},
    Clip = {80, 90, 110, 120, 150, -1},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeLineGun = {
    Delay = {36, 34, 32, 30, 28, 26},
    Damage = {2.80, 3.08, 3.36, 3.64, 3.92, 4.20},
    Clip = {10, 15, 15, 20, 50, -1},
    Elements = {Element.frost, 0, Element.electric, 0, 0, Element.fire}
}

UpgradeRailgun = {
    Delay = {50, 47, 44, 41, 38, 35},
    Damage = {61.33, 67.47, 73.60, 79.73, 85.87, 92.00},
    Clip = {5, 7, 10, 15, 25, -1},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeCombatShotgun = {
    Delay = {12, 11, 10, 10, 9.5, 9},
    Damage = {4.67, 5.12, 5.6, 6.07, 6.53, 7},
    Clip = {10, 15, 25, 40, 40, -1},
    Bullets = {8, 8, 8, 9, 10, 10},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeChaingun = {
    Delay = {0.2, 0.15, 0.1, 0.5, 0, 0},
    Damage = {6.30, 6.93, 7.56, 8.19, 8.82, 9.45},
    Clip = {300, 300, 400, 500, 700, -1},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeVortex = {
    Delay = {35, 33, 32, 32, 31, 30},
    Damage = {0.97, 1.06, 1.16, 1.26, 1.35, 1.45},
    Clip = {5, 5, 6, 7, 10, -1},
	LifeTime = {60, 70, 80, 90, 100, 110},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeChainsaw = {
    Delay = {0, 0, 0, 0, 0, 0},
    Damage = {10.33, 11.37, 12.40, 13.43, 14.47, 15.50},
    Clip = {200, 220, 250, 300, 400, -1},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeMechFlamer = {
    Delay = {0, 0, 0, 0, 0, 0},
    Damage = {5.50, 6.05, 6.60, 7.15, 7.70, 8.25},
    Clip = {200, 220, 250, 300, 666, -1},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

UpgradeMechRocket = {
    Delay = {45, 40, 35, 35, 30, 25},
    Damage = {88.00, 96.80, 105.60, 114.40, 123.20, 132.00},
    Clip = {15, 18, 25, 50, 70, -1},
    Elements = {Element.frost, 0, 0, Element.electric, 0, Element.fire}
}

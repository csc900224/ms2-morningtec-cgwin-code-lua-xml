EntityLeveling = {}
EntityLeveling[EntityType.SectoidSimple] = { l = 1, d = 2, m = 2.5, e = { 4, 12, 20 } }
EntityLeveling[EntityType.SectoidShooting] = { l = 1, d = 2, m = 2.5, e = { 5, 12, 20 } }
EntityLeveling[EntityType.NautilSimple] = { l = 1, d = 3, m = 2.5, e = { 6, 12, 20 } }
EntityLeveling[EntityType.Crab] = { l = 2, d = 4, m = 2.5, e = { 6, 13, 20 } }
EntityLeveling[EntityType.NautilTurning] = { l = 3, d = 3, m = 3, e = { 7, 13, 21 } }
EntityLeveling[EntityType.OctopusSimple] = { l = 4, d = 4, m = 2.5, e = { 7, 13, 21 } }
EntityLeveling[EntityType.SqueezerSimple] = { l = 7, d = 5, m = 2.8, e = { 9, 14, 22 } }
EntityLeveling[EntityType.KillerWhaleSimple] = { l = 9, d = 5, m = 2.9, e = { 10, 15, 22 } }
EntityLeveling[EntityType.OctopusChaser] = { l = 10, d = 6, m = 2.9, e = { 10, 15, 22 } }
EntityLeveling[EntityType.Nerval] = { l = 11, d = 6, m = 2.9, e = { 11, 16, 23 } }
EntityLeveling[EntityType.KillerWhale] = { l = 12, d = 6, m = 3, e = { 12, 16, 23 } }
EntityLeveling[EntityType.SqueezerTurning] = { l = 13, d = 7, m = 3, e = { 13, 16, 23 } }
EntityLeveling[EntityType.FishSimple] = { l = 15, d = 8, m = 3.5, e = { 15, 17, 24 } }
EntityLeveling[EntityType.HoundSimple] = { l = 16, d = 8, m = 4, e = { 16, 17, 24 } }
EntityLeveling[EntityType.OctopusShotAware] = { l = 17, d = 9, m = 4, e = { 17, 18, 24 } }
EntityLeveling[EntityType.FishThrowing] = { l = 18, d = 9, m = 5, e = { 18, 18, 24 } }
EntityLeveling[EntityType.FloaterSimple] = { l = 19, d = 10, m = 6, e = { 19, 19, 25 } }
EntityLeveling[EntityType.FloaterElectric] = { l = 20, d = 15, m = 1, e = { 20, 20, 25 } }
EntityLeveling[EntityType.HoundShooting] = { l = 21, d = 15, m = 1, e = { 21, 21, 25 } }
EntityLeveling[EntityType.Lobster] = { l = 22, d = 15, m = 1, e = { 22, 22, 25 } }

if not AtlasSet then return end

EntityAtlas = {}
EntityAtlas[EntityType.OctopusSimple] = {AtlasSet.Boss1Shared}
EntityAtlas[EntityType.OctopusShotAware] = {AtlasSet.Enemy1}
EntityAtlas[EntityType.OctopusChaser] = {AtlasSet.Enemy1}
EntityAtlas[EntityType.SqueezerSimple] = {AtlasSet.Enemy4}
EntityAtlas[EntityType.SqueezerTurning] = {AtlasSet.Enemy4}
EntityAtlas[EntityType.FishSimple] = {AtlasSet.Enemy2}
EntityAtlas[EntityType.FishThrowing] = {AtlasSet.Enemy2}
EntityAtlas[EntityType.FloaterSimple] = {AtlasSet.Enemy4}
EntityAtlas[EntityType.FloaterElectric] = {AtlasSet.Enemy3}
EntityAtlas[EntityType.FloaterSower] = {AtlasSet.Boss2}
EntityAtlas[EntityType.HoundSimple] = {AtlasSet.Enemy3}
EntityAtlas[EntityType.HoundShooting] = {AtlasSet.Enemy3}
EntityAtlas[EntityType.SectoidSimple] = {AtlasSet.Boss1Shared}
EntityAtlas[EntityType.SectoidShooting] = {AtlasSet.Enemy2}
EntityAtlas[EntityType.KillerWhale] = {AtlasSet.Enemy5}
EntityAtlas[EntityType.KillerWhaleSimple] = {AtlasSet.Enemy5}
EntityAtlas[EntityType.Crab] = {AtlasSet.Enemy5}
EntityAtlas[EntityType.MechaBoss] = {AtlasSet.Boss1, AtlasSet.Boss1Shared}
EntityAtlas[EntityType.SowerBoss] = {AtlasSet.Boss2}
EntityAtlas[EntityType.OctobrainBoss] = {AtlasSet.Boss3}
EntityAtlas[EntityType.OctobrainBossClone] = {AtlasSet.Boss3}
EntityAtlas[EntityType.Lobster] = {AtlasSet.Enemy6}
EntityAtlas[EntityType.NautilSimple] = {AtlasSet.Enemy7}
EntityAtlas[EntityType.NautilTurning] = {AtlasSet.Enemy4, AtlasSet.Enemy7}
EntityAtlas[EntityType.Nerval] = {AtlasSet.Enemy8}

function GetLevelMonsters()
    local level = registry:Get( "/monstaz/player/level" )
    local available = {}

    for i,v in pairs( EntityLeveling ) do
        if v.l <= level then
            table.insert( available, i )
        end
    end

    local set = {}
    while #set < 3 and #available > 0 do
        local idx = math.random( #available )
        table.insert( set, table.remove( available, idx ) )
    end

    table.sort( set, function(a,b) return EntityLeveling[a].d < EntityLeveling[b].d end )

    local elements = {}
    for i,v in ipairs( set ) do
        table.insert( elements, GetRandomElement( v, level ) )
    end

    return set, elements
end

function GetRandomElement( monster, level )
    local pool = {}
    for i=1,3 do
        local data = EntityLeveling[monster]
        if data and data.e[i] <= level then
            table.insert( pool, i )
        end
    end

    local e = 0
    if #pool > 0 then
        e = pool[math.random( #pool )]
    end

    return e
end

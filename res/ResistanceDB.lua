require( "menu2/validation.lua" )
require( "SurvivalDB.lua" )
Friends = {}
Invitations = {}
Gifts = {}
BackupItems = {}
GiftsGroup = {}

BotName ="Wilson"
GamelionUserName = "Gamelion"
ItemType = { "itemTypeFriend"  , "itemTypeInvitation" , "itemTypeGift", "itemTypeUser", "itemTypeAvatar", Backup = "itemTypeBackup" }
GiftType = { giftGamelionGold = "2" , giftGamelionCash = "3" , giftSoftCurrency = "4", giftFuelPack = "5" }

User = nil

GiftSoftCurrencyQuantity = 50
CashForFuelRatio = 100
GiftFuelPackQuantity = 1
InviteRewardValue = 5

NewFriendsCount = 0

function SetUserInfo( uniqueId, name , pass, email , fbId  )

    newUser = {
            Type = ItemType[4],
            Name = name,
            Pass = pass,
            UniqueId = uniqueId,
            Email = email,
            FbId = fbId
            }

    User = newUser
end

function AddFriendFull( userID, name , level , fbId , kills , events , weaponId , weaponLevel , canSendGifts , canHelp , isVip )

    Friend = {
        Type = ItemType[1],
        UniqueId = userID,
        Name = name,
        Level = level,
        FbId = fbId,
        Kills = kills,
        CompletedEvents = events,
        UsedWeaponId = weaponId,
        UsedWeaponUpgrade = weaponLevel,
        CanSendGifts = canSendGifts,
        CanHelp = canHelp,
        IsVip = isVip
    }

    table.insert( Friends, Friend )
end

function AddFriend( userID, name , level , fbId , canHelp )

    Friend = {
        Type = ItemType[1],
        UniqueId = userID,
        Name = name,
        Level = level,
        FbId = fbId,
        Kills = 0,
        CompletedEvents = 0,
        UsedWeaponId = "smg",
        UsedWeaponUpgrade = "0",
        CanSendGifts = "0",
        CanHelp = canHelp,
        IsVip = "0"
    }

    table.insert( Friends, Friend )
end

function FillFriendInfo( userID , kills , completedEvents , bestWeaponId, bestWeaponUpgrade, canSendGift , canHelp )
    for i=1,# Friends do
         if Friends[i].UniqueId ==  userID then
         
            Friends[i].Kills = kills
            Friends[i].CompletedEvents = completedEvents
            Friends[i].UsedWeaponId = bestWeaponId
            Friends[i].UsedWeaponUpgrade = bestWeaponUpgrade
            Friends[i].CanSendGifts = canSendGift + TimeSinceUpdateDB
            Friends[i].CanHelp = canHelp + TimeSinceUpdateDB
            
            break
         end
    end
end

function AddInvitation( name , level , fbId , fromUniqueId , canHelp, canSendGifts ,age , isVip )

    Invitation = {
        Type = ItemType[2],
        Name = name,
        Level = level,
        FbId = fbId,
        UniqueId = fromUniqueId,
        CanSendGifts = canSendGifts,
        CanHelp = canHelp,
        Age = age,
        IsVip = isVip
    }

    table.insert( Invitations, Invitation )
end

function RemoveInvitation( idx )
    table.remove( Invitations, idx )
end

function AddGift( fromUniqueId, name, fbId, level, giftType, quantity, giftUniqueId , age , isVip )

    Gift = {
        Type = ItemType[3],
        GivenbById = fromUniqueId,
        GiftType = giftType,
        Name = name,
        Level = level,
        FbId = fbId,
        Quantity = quantity,
        GiftId = giftUniqueId,
        Age = age,
        IsVip = isVip
    }

    table.insert( Gifts, Gift )
end

function RemoveGift( idx )
    table.remove( Gifts, idx )
end

function FindByNameAndRemove( item )

    if item.Type == ItemType[2] then

        for i=1,# Invitations do
            if Invitations[i].Name == item.Name then
                table.remove( Invitations , i )
                break
            end
        end
    elseif item.Type == ItemType[3] then
        for i=1,# Gifts do
            if Gifts[i].Name == item.Name then
                table.remove( Gifts , i )
                break
            end
        end

    end	
end

function AddBackupMember( userID, name , level , fbId , kills , events , weaponId , weaponLevel , canSendGifts , canHelp , isVip )

    Friend = {
        Type = ItemType[5],
        UniqueId = userID,
        Name = name,
        Level = level,
        FbId = fbId,
        Kills = kills,
        CompletedEvents = events,
        UsedWeaponId = weaponId,
        UsedWeaponUpgrade = weaponLevel,
        CanSendGifts = canSendGifts,
        CanHelp = canHelp,
        IsVip = isVip
    }

    table.insert( BackupItems, Friend )
end

function RemoveBackupMember( idx )
    table.remove( BackupItems, idx )
end

function UpdateBackupMember()

    FlushBackup()

    for i=1,# Friends do
        local friend = Friends[i]
        AddBackupMember( friend.UniqueId , friend.Name , friend.Level, friend.FbId , friend.Kills , friend.CompletedEvents , friend.UsedWeaponId, friend.UsedWeaponUpgrade , friend.CanSendGifts, friend.CanHelp , friend.IsVip )
    end

end

function UserLoggedIn( userUniqueId, name, mail,fbId  )
    UserDataSetup( userUniqueId, name, mail, fbId )
    OperationSuccessPopupShow( "TEXT_SUCCESS_LOGIN" , PopupUnlock )
end

function UserDataSetup( userUniqueId, name, mail, fbId )
    registry:Set( "/internal/loggedIn",true)

    registry:Set( "/internal/login", name )
    registry:Set( "/internal/pass", InputFields.password )
    registry:Set( "/internal/uniqueId", userUniqueId )
    registry:Set( "/internal/mail", mail )
    registry:Set( "/internal/fbId", fbId )

    registry:Set( "/monstaz/resistance/successLogin",true)
    registry:Set( "/monstaz/resistance/login", name )
    registry:Set( "/monstaz/resistance/uniqueId", userUniqueId )
    registry:Set( "/monstaz/resistance/pass", InputFields.password )
    registry:Set( "/monstaz/resistance/mail", mail )
    registry:Set( "/monstaz/resistance/fbId", fbId )

    UserInfoRefresh()
    callback:Save()
end

function UserInfoRefresh()
    if IsLoggedIn() then
        SetUserInfo(  registry:Get( "/internal/uniqueId") ,registry:Get( "/internal/login") , registry:Get( "/internal/pass"), registry:Get( "/internal/mail") , registry:Get( "/internal/fbId") )
    end
end


function SetNewFriendsCount( newFriendsCount )
    NewFriendsCount = newFriendsCount
end

function ResetNewFriendsCount()
    NewFriendsCount = 0
end

function FlushInvitations()
    Invitations = {}
end

function FlushGifts()
    Gifts = {}
end

function FlushFriends()
    Friends = {}
end

function FlushBackup()
    BackupItems = {}
end

function IsLoggedIn()
    local isLogin = registry:Get( "/internal/loggedIn" )

    if isLogin == nil then
        isLogin = false
    end
    
    return isLogin
end

function SetBackupFlag( uniqueId , value )

    for i=1,# Friends do
        if Friends[i].UniqueId == uniqueId then 
            Friends[i].CanHelp = value --24h
            break
        end
    end

end

function GetBackupFlag( uniqueId )

    for i=1,# Friends do
        if Friends[i].UniqueId == uniqueId then 
            return math.max( Friends[i].CanHelp - TimeSinceUpdateDB , 0 )
        end
    end

    return 0
end

function SetGiftFlag( uniqueId , value ) -- test purposes only
    for i=1,# Friends do
        if Friends[i].UniqueId == uniqueId then 
            Friends[i].CanSendGifts = value --24h
            break
        end
    end

end

TimeSinceUpdateDB = 0

local updated = false

function UpdateDB( dt )
    if updated then
        TimeSinceUpdateDB = TimeSinceUpdateDB + dt
    end
end


function ResetUpdateDBTimer()
    updated = true 
    TimeSinceUpdateDB = 0
end

function FindFriendByUniqueId( uniqueId )

    for i=1,# Friends do
        if Friends[i].UniqueId == uniqueId then
            return Friends[i]
        end
    end
end

UpdateInfo = {
    Friends = 0,
    Requests = 0,
    LeaderboardsFriends = 0,
    LeaderboardsGlobal = 0,
    Gifts = 0
    }

function ResetSyncTimeUpdateInfo()
    UpdateInfo.Friends = 0
    UpdateInfo.Requests = 0
    UpdateInfo.LeaderboardsFriends = 0
    UpdateInfo.LeaderboardsGlobal = 0
    UpdateInfo.Gifts = 0
end  

function ResetSyncTimeUpdateInfoFriends()
    UpdateInfo.Friends = 0
    UpdateInfo.Requests = 0
    UpdateInfo.Gifts = 0
end 

function SetLastSyncTimeFriends( timestamp )
    UpdateInfo.Friends = timestamp
end

function SetLastSyncTimeRequests( timestamp )
    UpdateInfo.Requests = timestamp
end

function SetLastSyncTimeGifts( timestamp )
    UpdateInfo.Gifts = timestamp
end

function SetLastSyncTimeLeaderboardsFriends( timestamp )
    UpdateInfo.LeaderboardsFriends = timestamp
end

function SetLastSyncTimeLeaderboardsGlobal( timestamp )
    UpdateInfo.LeaderboardsGlobal = timestamp
end

function GetUpdateInfoTimes()
    return UpdateInfo.Friends ,UpdateInfo.Requests ,UpdateInfo.LeaderboardsFriends ,UpdateInfo.LeaderboardsGlobal ,UpdateInfo.Gifts 
end

function GetInvitesNumber()
    return #Invitations
end

function GetGiftsNumber()
    return #Gifts
end

function GetCashFromGiftsNumber()
    local cashAmount = 0
    for i=1,# Gifts do
        if Gifts[i].GiftType == GiftType.giftSoftCurrency then
            cashAmount = cashAmount + Gifts[i].Quantity
        end
        -- for vip users fuelpacks as cash
        if registry:Get( "/monstaz/subscription" ) == true then
            if Gifts[i].GiftType == GiftType.giftFuelPack then
                cashAmount = cashAmount + Gifts[i].Quantity * CashForFuelRatio
            end
        end

        if Gifts[i].GiftType == GiftType.giftGamelionCash then
            cashAmount = cashAmount + Gifts[i].Quantity
        end

    end

    return cashAmount
end

function GetGoldFromGiftsNumber()
    local goldAmount = 0

    for i=1,# Gifts do
        if Gifts[i].GiftType == GiftType.giftGamelionGold then
            goldAmount = goldAmount + Gifts[i].Quantity
        end
    end

    return goldAmount
end

function GetFuelPacksFromGiftsNumber()
    -- for vip users fuelpacks always 0 ( cash instead )
    if registry:Get( "/monstaz/subscription" ) == true then
        return 0
    end

    local fuelPacksAmount = 0
    for i=1,# Gifts do
        if Gifts[i].GiftType == GiftType.giftFuelPack then
            fuelPacksAmount = fuelPacksAmount + 1
        end
    end

    return fuelPacksAmount
end

function FriendSortItems()
    function GreaterLevelFirst( item0, item1 )
        local lvl0 = item0.Level
        local lvl1 = item1.Level
        if ( lvl0 == lvl1 ) then
            return Alphabetical( item0, item1 )
        else
            return lvl0 > lvl1
        end	
    end

    table.sort( Friends, GreaterLevelFirst )
end

function Alphabetical( item0, item1 )
        local nm0 = item0.Name
        local nm1 = item1.Name
        return string.lower(nm0) < string.lower(nm1)
end

function InviteSortItems()
    function YoungerFirst( item0, item1 )
        local lvl0 = tonumber( item0.Age )
        local lvl1 = tonumber( item1.Age )
        if ( lvl0 == lvl1 ) then
            return Alphabetical( item0, item1 )
        else
            return lvl0 < lvl1
        end	
    end

    table.sort( Invitations, YoungerFirst )
end

function GroupGifts()

    local alreadyIn = false

    for i=1,# Gifts do
        alreadyIn = false
        local idx = 0

        for j=1,# GiftsGroup do
            if Gifts[i].GivenbById == GiftsGroup[j].GivenbById and Gifts[i].GiftType == GiftsGroup[j].GiftType then
                alreadyIn = true
                idx = j
                break
            end

        end

        if alreadyIn == false then 
            Gift = {
                Type = ItemType[3],
                GivenbById = Gifts[i].GivenbById,
                GiftType = Gifts[i].GiftType,
                Name = Gifts[i].Name,
                Level = Gifts[i].Level,
                FbId = Gifts[i].FbId,
                Quantity = Gifts[i].Quantity,
                GiftId = Gifts[i].GiftId,
                Age = Gifts[i].Age,
                IsVip = Gifts[i].IsVip,
                Count = 1
            }

            table.insert( GiftsGroup, Gift)
        else
            if GiftsGroup[idx].GiftType  == GiftType.giftSoftCurrency and Gifts[i].GiftType == GiftType.giftSoftCurrency then
                GiftsGroup[idx].Quantity  = GiftsGroup[idx].Quantity + Gifts[i].Quantity
            else
                GiftsGroup[idx].Quantity = GiftsGroup[idx].Quantity + Gifts[i].Quantity
            end

            --ClawMsg( "sort " .. GiftsGroup[idx].GiftType .. "sas ".. Gifts[i].GiftType .. " " .. GiftsGroup[idx].Quantity )
            GiftsGroup[idx].Count = GiftsGroup[idx].Count + 1
            
            if Gifts[i].Age < GiftsGroup[idx].Age then
                GiftsGroup[idx].Age = Gifts[i].Age
            end
        end
    end
end

function GiftsSortItems()
    function YoungerFirst( item0, item1 )
        local lvl0 = tonumber( item0.Age )
        local lvl1 = tonumber( item1.Age )
        if ( lvl0 == lvl1 ) then
            return Alphabetical( item0, item1 )
        else
            return lvl0 < lvl1
        end	
    end

    table.sort( Gifts, YoungerFirst )
    GiftsGroup = {}
    GroupGifts()
    table.sort( GiftsGroup, YoungerFirst )
end

function IsBot( friend )
    return friend.Name == BotName 
end

function IsGamelionUser( friend )
    return string.lower( friend.Name ) == string.lower( GamelionUserName )
end
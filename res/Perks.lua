function PerkRunner()
    EntityPlayer.MoveSpeed = EntityPlayer.MoveSpeed * 1.1
    local v = EntityPlayer
    EntityManager:SetEntityData( EntityType.Player, v.RotationSpeed, v.MoveSpeed, v.Orbs, v.XP, v.Damage, v.HP, v.MoneyChance, v.MoneyAmount, v.Score, 0, 1 )
end

function PerkAmmoManiac()
    for i,v in ipairs( Weapons ) do
        if v.Clip ~= -1 then
            v.Clip = math.floor( v.Clip * 1.2 )
        end
    end
end

function PerkFastHands()
    for i,v in ipairs( Weapons ) do
        v.Reload = math.floor( v.Reload * 0.8 )
    end
end

function PerkComeGetSome()
    for i,v in ipairs( Weapons ) do
        local d = math.floor( v.Delay * 0.85 )
        if d == v.Delay then
            v.Bullets = v.Bullets * 1.15
        else
            v.Delay = d
        end
    end
end

function PerkRegeneration()
    playerRegeneration = true
end

function PerkRage()
    playerRage = true
end

function PerkEndoskeleton()
    local inc = player:GetMaxHitPoints() * 0.5
    player:SetHitPoints( player:GetHitPoints() + inc )
    player:SetMaxHitPoints( player:GetMaxHitPoints() + inc )
    GameManager:SetPlayerHP( player:GetMaxHitPoints() )
end

function PerkTeamPlayer()
    if AIFriend then
        local inc = AIFriend:GetMaxHitPoints() * 0.5
        AIFriend:SetHitPoints( AIFriend:GetHitPoints() + inc )
        AIFriend:SetMaxHitPoints( AIFriend:GetMaxHitPoints() + inc )
    end
end

function PerkMuscleFever()
    for i,v in ipairs( EntityParams ) do
        if v ~= EntityPlayer and v ~= EntityAIFriend and v ~= EntityFriend then
            EntityManager:SetEntityData( i, v.RotationSpeed * 0.8, v.MoveSpeed * 0.8, v.Orbs, v.XP, v.Damage, v.HP, v.MoneyChance, v.MoneyAmount, v.Score, v.e, v.ShieldScale )
        end
    end
end

function PerkCat()
    local e = EntityManager:GetCat()
    local inc = e:GetMaxHitPoints() * 0.5
    e:SetHitPoints( e:GetHitPoints() + inc )
    e:SetMaxHitPoints( e:GetMaxHitPoints() + inc )
end

require( "WeaponUpgrades.lua" )
require( "ShopItem.lua" )
require( "LevelDB.lua" )

MissionDB = 
{
    { -- Daily Challanges
        type    = "Daily",
        id      = "DailyMissions",
        groups  = 
        {
            { -- Warm-up
                type        = "Random",
                id          = "kill-aliens",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_KILL_ALIENS",
                        desc        = "TEXT_MISSION_KILL_ALIENS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "KillEnemy", target = 100 }},
                        rewards     = {{ type = "Cash", value = 50 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_KILL_ALIENS",
                        desc        = "TEXT_MISSION_KILL_ALIENS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "KillEnemy", target = 150 }},
                        rewards     = {{ type = "Cash", value = 75 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_KILL_ALIENS",
                        desc        = "TEXT_MISSION_KILL_ALIENS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "KillEnemy", target = 250 }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    }
                }
            },
            { -- Fine mess
                type        = "Random",
                id          = "destroy-objects",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_DESTROY_OBJECTS_D",
                        desc        = "TEXT_MISSION_DESTROY_OBJECTS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "DestroyObject", target = 10 }},
                        rewards     = {{ type = "Cash", value = 50 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_DESTROY_OBJECTS_D",
                        desc        = "TEXT_MISSION_DESTROY_OBJECTS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "DestroyObject", target = 25 }},
                        rewards     = {{ type = "Cash", value = 75 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_DESTROY_OBJECTS_D",
                        desc        = "TEXT_MISSION_DESTROY_OBJECTS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "DestroyObject", target = 50 }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    }
                }
            },
            { -- Saving the Earth
                type        = "Random",
                id          = "complete-levels",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_COMPLETE_LEVELS",
                        desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "CompleteLevel", target = 3, story = true, tutorial = true }},
                        rewards     = {{ type = "Cash", value = 50 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_COMPLETE_LEVELS",
                        desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "CompleteLevel", target = 5, story = true, tutorial = true }},
                        rewards     = {{ type = "Cash", value = 75 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_COMPLETE_LEVELS",
                        desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "CompleteLevel", target = 7, story = true, tutorial = true }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    }
                }
            },
            { -- And stay down
                type        = "Random",
                id          = "complete-boss-level",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_COMPLETE_BOSS_LEVEL",
                        desc        = "TEXT_MISSION_COMPLETE_BOSS_LEVEL_DESC",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "CompleteLevel", target = 1, boss = true }},
                        rewards     = {{ type = "Cash", value = 150 }}
                    }
                }
            },
            { -- Cosmic rage
                type        = "Random",
                id          = "collect-vengance",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_COLLECT_VENGANCE",
                        desc        = "TEXT_MISSION_COLLECT_VENGANCE_ONCE_DESC",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Pickup", target = 1, object_type=PickupType.WeaponBoost }},
                        rewards     = {{ type = "Cash", value = 50 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_COLLECT_VENGANCE",
                        desc        = "TEXT_MISSION_COLLECT_VENGANCE_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Pickup", target = 3, object_type=PickupType.WeaponBoost }},
                        rewards     = {{ type = "Cash", value = 75 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_COLLECT_VENGANCE",
                        desc        = "TEXT_MISSION_COLLECT_VENGANCE_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Pickup", target = 7, object_type=PickupType.WeaponBoost }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    }
                }
            },
            { -- A job like any other
                type        = "Random",
                id          = "collect-cash",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_COLLECT_CASH_DAILY",
                        desc        = "TEXT_MISSION_COLLECT_CASH_DAILY_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Pickup", target = 100, object_type=PickupType.Cash }},
                        rewards     = {{ type = "Cash", value = 50 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_COLLECT_CASH_DAILY",
                        desc        = "TEXT_MISSION_COLLECT_CASH_DAILY_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Pickup", target = 250, object_type=PickupType.Cash }},
                        rewards     = {{ type = "Cash", value = 75 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_COLLECT_CASH_DAILY",
                        desc        = "TEXT_MISSION_COLLECT_CASH_DAILY_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Pickup", target = 500, object_type=PickupType.Cash }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    }
                }
            },
            { -- Hot potatoe
                type        = "Random",
                id          = "kill-with-granade",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_KILL_WITH_GRANADE",
                        desc        = "TEXT_MISSION_KILL_WITH_GRANADE_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "KillEnemy", target = 3, shot = ShotType.Grenade }},
                        rewards     = {{ type = "Cash", value = 60 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_KILL_WITH_GRANADE",
                        desc        = "TEXT_MISSION_KILL_WITH_GRANADE_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "KillEnemy", target = 7, shot = ShotType.Grenade }},
                        rewards     = {{ type = "Cash", value = 80 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_KILL_WITH_GRANADE",
                        desc        = "TEXT_MISSION_KILL_WITH_GRANADE_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "KillEnemy", target = 15, shot = ShotType.Grenade }},
                        rewards     = {{ type = "Cash", value = 120 }}
                    }
                }
            },
            { -- Survival instinct
                type        = "Random",
                id          = "survive-survival",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_SURVIVE_SURVIVAL",
                        desc        = "TEXT_MISSION_SURVIVE_SURVIVAL_DESC_1",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Survive", target = 60, survival = true }},
                        rewards     = {{ type = "Cash", value = 80 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_SURVIVE_SURVIVAL",
                        desc        = "TEXT_MISSION_SURVIVE_SURVIVAL_DESC_2",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Survive", target = 120, survival = true }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_SURVIVE_SURVIVAL",
                        desc        = "TEXT_MISSION_SURVIVE_SURVIVAL_DESC_3",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "Survive", target = 180, survival = true }},
                        rewards     = {{ type = "Cash", value = 150 }}
                    }
                }
            },
            { -- Trickster
                type        = "Random",
                id          = "use-perks",
                missions    = 
                {
                    {
                        id          = "low",
                        title       = "TEXT_MISSION_USE_PERKS",
                        desc        = "TEXT_MISSION_USE_PERKS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "UsePerk", target = 10 }},
                        rewards     = {{ type = "Cash", value = 50 }}
                    },
                    {
                        id          = "med",
                        title       = "TEXT_MISSION_USE_PERKS",
                        desc        = "TEXT_MISSION_USE_PERKS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "UsePerk", target = 25 }},
                        rewards     = {{ type = "Cash", value = 75 }}
                    },
                    {
                        id          = "high",
                        title       = "TEXT_MISSION_USE_PERKS",
                        desc        = "TEXT_MISSION_USE_PERKS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_calendar.png@linear",
                        timer       = true,
                        objectives  = {{ type = "UsePerk", target = 50 }},
                        rewards     = {{ type = "Cash", value = 100 }}
                    }
                }
            }
        }
    },
    { -- Main Missions
        type    = "Sequence",
        id      = "MainMissions",
        groups  = 
        {
            { -- Area 1
                type        = "Uniform",
                id          = "stage-1",
                missions    = 
                {
                    { -- Lesson Learned
                        id          = "tutorial",
                        title       = "TEXT_MISSION_COMPLETE_TUTORIAL",
                        desc        = "TEXT_MISSION_COMPLETE_TUTORIAL_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "CompleteTutorial" }},
                        rewards     = {{ type = "Cash", value = 25 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Downtown
                        id          = "suburbs",
                        title       = "TEXT_MISSION_SUBURBS",
                        desc        = "TEXT_MISSION_SUBURBS_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "CompleteLevel", target = 1, boss = true, story = true, enviroment = AtlasSet.EnvSuburbs, stage = 1 }},
                        rewards     = {{ type = "Cash", value = 20 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Concrete Jungle
                        id          = "city",
                        title       = "TEXT_MISSION_CITY",
                        desc        = "TEXT_MISSION_CITY_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "CompleteLevel", target = 1, boss = true, story = true, enviroment = AtlasSet.EnvCity, stage = 1 }},
                        rewards     = {{ type = "Cash", value = 20 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Bang, Bang
                        id          = "kill-monsters",
                        title       = "TEXT_MISSION_KILL_MONSTERS_A1",
                        desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillEnemy", target = 100 }},
                        rewards     = {{ type = "Cash", value = 40 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Boom, Boom
                        id          = "destroy-objects",
                        title       = "TEXT_MISSION_DESTROY_OBJECTS_A1",
                        desc        = "TEXT_MISSION_DESTROY_OBJECTS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "DestroyObject", target = 10 }},
                        rewards     = {{ type = "Cash", value = 30 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Advanced Tactics
                        id          = "use-perks",
                        title       = "TEXT_MISSION_USE_PERKS_A1",
                        desc        = "TEXT_MISSION_USE_PERKS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "UsePerk", target = 10 }},
                        rewards     = {{ type = "Cash", value = 15 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Golden Hand
                        id          = "upgrade-weapon",
                        title       = "TEXT_MISSION_UPGRADE_WEAPON",
                        desc        = "TEXT_MISSION_UPGRADE_WEAPON_DESC",
                        icon        = "menu2/icon_cash_gun.png@linear",
                        objectives  = {{ type = "ShopItemUpgrade" }},
                        rewards     = {{ type = "Cash", value = 40 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Unstoppable!
                        id          = "collect-orbs",
                        title       = "TEXT_MISSION_COLLECT_ORBS_A1",
                        desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "Pickup", target = 100, object_type=PickupType.MagicOrb }},
                        rewards     = {{ type = "Cash", value = 75 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Firepower Check
                        id          = "survive",
                        title       = "TEXT_MISSION_SURVIVE_SURVIVAL_S1",
                        desc        = "TEXT_MISSION_SURVIVE_SURVIVAL_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "Survive", target = 30, survival = true }},
                        rewards     = {{ type = "Cash", value = 35 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Brainwash-proof
                        id          = "kill-octobrain",
                        title       = "TEXT_MISSION_KILL_OCTOBRAIN",
                        desc        = "TEXT_MISSION_KILL_OCTOBRAIN_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillBoss", boss = EntityType.OctobrainBoss, stage = 1 }},
                        rewards     = {{ type = "Cash", value = 100 }, { type = "Bullets", value = 3 }}
                    }
                }
            },
            { -- Area 2
                type        = "Uniform",
                id          = "stage-2",
                missions    = 
                {
                    { -- Nomad
                        id          = "desert",
                        title       = "TEXT_MISSION_DESERT",
                        desc        = "TEXT_MISSION_DESERT_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "CompleteLevel", target = 1, boss = true, story = true, enviroment = AtlasSet.EnvDesert, stage = 2 }},
                        rewards     = {{ type = "Cash", value = 30 }, { type = "Bullets", value = 1 }}
                    },
                    { -- DumDum Wasn't Alone
                        id          = "play-with-friend",
                        title       = "TEXT_MISSION_PLAY_WITH_FRIEND",
                        desc        = "TEXT_MISSION_PLAY_WITH_FRIEND_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "PlayWithFriend" }},
                        rewards     = {{ type = "Cash", value = 50 }, { type = "Bullets", value = 2 }}
                    },
                    { -- $$$
                        id          = "collect-cash",
                        title       = "TEXT_MISSION_COLLECT_CASH",
                        desc        = "TEXT_MISSION_COLLECT_CASH_DESC",
                        icon        = "menu2/icon_cash_gun.png@linear",
                        objectives  = {{ type = "CollectCash", target = 50 }},
                        rewards     = {{ type = "Cash", value = 30 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Pacifist
                        id          = "hold-fire",
                        title       = "TEXT_MISSION_HOLD_FIRE",
                        desc        = "TEXT_MISSION_HOLD_FIRE_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "HoldFire", target = 30 }},
                        rewards     = {{ type = "Cash", value = 60 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Boosted Up
                        id          = "use-perks",
                        title       = "TEXT_MISSION_USE_PERKS_SINGLE_ROUND",
                        desc        = "TEXT_MISSION_USE_PERKS_SINGLE_ROUND_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "UsePerk", target = 3, single_round = true }},
                        rewards     = {{ type = "Cash", value = 35 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Shocker
                        id          = "kill-with-electric",
                        title       = "TEXT_MISSION_KILL_WITH_ELECTRIC",
                        desc        = "TEXT_MISSION_KILL_WITH_ELECTRIC_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillEnemy", target = 1, elements = Element.electric }},
                        rewards     = {{ type = "Cash", value = 55 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Show-off
                        id          = "fire-both",
                        title       = "TEXT_MISSION_FIRE_BOTH",
                        desc        = "TEXT_MISSION_FIRE_BOTH_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "FireBothWeapons" }},
                        rewards     = {{ type = "Cash", value = 60 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Destructive!
                        id          = "collect-orbs",
                        title       = "TEXT_MISSION_COLLECT_ORBS_A2",
                        desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "Pickup", target = 200, object_type=PickupType.MagicOrb }},
                        rewards     = {{ type = "Cash", value = 130 }, { type = "Bullets", value = 3 }}
                    },
                    { -- Remove the pin
                        id          = "throw-granade",
                        title       = "TEXT_MISSION_THROW_GRANADE",
                        desc        = "TEXT_MISSION_THROW_GRANADE_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "ShopItemUse", item = ShopItem.Grenade }},
                        rewards     = {{ type = "Cash", value = 70 }, { type = "Bullets", value = 2 }}
                    },
                    { -- ...Til They Exploded
                        id          = "kill-sower",
                        title       = "TEXT_MISSION_KILL_SOWER",
                        desc        = "TEXT_MISSION_KILL_SOWER_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillBoss", boss = EntityType.SowerBoss, stage = 2 }},
                        rewards     = {{ type = "Cash", value = 150 }, { type = "Bullets", value = 3 }}
                    }
                }
            },
            { -- Area 3
                type        = "Uniform",
                id          = "stage-3",
                missions    = 
                {
                    { -- Snowman
                        id          = "ice",
                        title       = "TEXT_MISSION_ICE",
                        desc        = "TEXT_MISSION_ICE_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "CompleteLevel", target = 1, boss = true, story = true, enviroment = AtlasSet.EnvIce, stage = 3 }},
                        rewards     = {{ type = "Cash", value = 40 }, { type = "Bullets", value = 2 }}
                    },
                    { -- DoomDoom
                        id          = "vengance",
                        title       = "TEXT_MISSION_COLLECT_VENGANCE_A3",
                        desc        = "TEXT_MISSION_COLLECT_VENGANCE_A3_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "Pickup", target = 1, object_type=PickupType.WeaponBoost }},
                        rewards     = {{ type = "Cash", value = 45 }, { type = "Bullets", value = 2 }}
                    },
                    { -- DoomDoom
                        id          = "score-survival",
                        title       = "TEXT_MISSION_SCORE_SURVIVAL",
                        desc        = "TEXT_MISSION_SCORE_SURVIVAL_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "Score", target = 50000, single_round = true, map = Survival02.f }},
                        rewards     = {{ type = "Cash", value = 90 }, { type = "Bullets", value = 3 }}
                    },
                    { -- Death From Below
                        id          = "kill-with-mine",
                        title       = "TEXT_MISSION_KILL_WITH_MINE",
                        desc        = "TEXT_MISSION_KILL_WITH_MINE_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillEnemy", shot = ShotType.Mine }},
                        rewards     = {{ type = "Cash", value = 85 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Off the air
                        id          = "destroy-antenas",
                        title       = "TEXT_MISSION_DESTROY_ANTENAS",
                        desc        = "TEXT_MISSION_DESTROY_ANTENAS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "DestroyObject", target = 3, object = "broadcaster" }},
                        rewards     = {{ type = "Cash", value = 180 }, { type = "Bullets", value = 3 }}
                    },
                    { -- Who you gonna call?
                        id          = "kill-monsters",
                        title       = "TEXT_MISSION_KILL_MONSTERS_A3",
                        desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillEnemy", target = 250 }},
                        rewards     = {{ type = "Cash", value = 50 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Firestarter
                        id          = "kill-with-fire",
                        title       = "TEXT_MISSION_KILL_WITH_FIRE",
                        desc        = "TEXT_MISSION_KILL_WITH_FIRE_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillEnemy", target = 1, elements = Element.fire }},
                        rewards     = {{ type = "Cash", value = 75 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Under Control
                        id          = "manual-aiming",
                        title       = "TEXT_MISSION_MANUAL_AIMING",
                        desc        = "TEXT_MISSION_MANUAL_AIMING_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "ManualAiming" }},
                        rewards     = {{ type = "Cash", value = 80 }, { type = "Bullets", value = 3 }}
                    },
                    { -- Ironfisted
                        id          = "kill-octomech",
                        title       = "TEXT_MISSION_KILL_OCTOMECH",
                        desc        = "TEXT_MISSION_KILL_OCTOMECH_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillBoss", boss = EntityType.MechaBoss, stage = 3 }},
                        rewards     = {{ type = "Cash", value = 200 }, { type = "Bullets", value = 3 }}
                    }
                }
            },
            { -- Area 4
                type        = "Uniform",
                id          = "stage-4",
                missions    = 
                {
                    { -- Mr Freeze
                        id          = "kill-with-frost",
                        title       = "TEXT_MISSION_KILL_WITH_FROST",
                        desc        = "TEXT_MISSION_KILL_WITH_FROST_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "KillEnemy", target = 1, elements = Element.frost }},
                        rewards     = {{ type = "Cash", value = 120 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Bloodthirsty!
                        id          = "collect-orbs",
                        title       = "TEXT_MISSION_COLLECT_ORBS_A4",
                        desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "Pickup", target = 400, object_type=PickupType.MagicOrb }},
                        rewards     = {{ type = "Cash", value = 250 }, { type = "Bullets", value = 3 }}
                    },
                    { -- Spinach!
                        id          = "buy-perk",
                        title       = "TEXT_MISSION_BUY_PERK",
                        desc        = "TEXT_MISSION_BUY_PERK_DESC",
                        icon        = "menu2/icon_cash_gun.png@linear",
                        objectives  = {{ type = "ShopItemBuy", item1 = ShopItem.SpeedBonus, item2 = ShopItem.DamageBonus, item3 = ShopItem.HealthBonus, item4 = ShopItem.CashBonus, item5 = ShopItem.ReloadBonus, item6 = ShopItem.AmmoBonus, item7 = ShopItem.MedKitBonus }},
                        rewards     = {{ type = "Cash", value = 95 }, { type = "Bullets", value = 1 }}
                    },
                    { -- Looter
                        id          = "collect-cash",
                        title       = "TEXT_MISSION_COLLECT_CASH_A4",
                        desc        = "TEXT_MISSION_COLLECT_CASH_DESC",
                        icon        = "menu2/icon_cash_gun.png@linear",
                        objectives  = {{ type = "CollectCash", target = 100 }},
                        rewards     = {{ type = "Cash", value = 130 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Too Pretty To Die
                        id          = "revive",
                        title       = "TEXT_MISSION_REVIVE",
                        desc        = "TEXT_MISSION_REVIVE_DESC",
                        icon        = "menu2/icon_cash_gun.png@linear",
                        objectives  = {{ type = "Revive" }},
                        rewards     = {{ type = "Cash", value = 200 }, { type = "Bullets", value = 3 }}
                    },
                    { -- These Guys Again
                        id          = "credits",
                        title       = "TEXT_MISSION_CREDITS",
                        desc        = "TEXT_MISSION_CREDITS_DESC",
                        icon        = "menu2/icon_medal.png@linear",
                        objectives  = {{ type = "SettingsTab", tab = 2 }},
                        rewards     = {{ type = "Cash", value = 110 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Can't Touch This
                        id          = "shield",
                        title       = "TEXT_MISSION_SHIELD",
                        desc        = "TEXT_MISSION_SHIELD_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "ShopItemUse", item = ShopItem.Shield }},
                        rewards     = {{ type = "Cash", value = 140 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Mr Know-it-all
                        id          = "use-perks",
                        title       = "TEXT_MISSION_USE_PERKS_SINGLE_ROUND_A4",
                        desc        = "TEXT_MISSION_USE_PERKS_SINGLE_ROUND_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "UsePerk", target = 5, single_round = true }},
                        rewards     = {{ type = "Cash", value = 220 }, { type = "Bullets", value = 3 }}
                    },
                    { -- Hooligan
                        id          = "destroy-objects",
                        title       = "TEXT_MISSION_DESTROY_OBJECTS_A4",
                        desc        = "TEXT_MISSION_DESTROY_OBJECTS_BLOW_DESC",
                        progress    = "TEXT_MISSIONS_PROGRESS",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "DestroyObject", target = 25 }},
                        rewards     = {{ type = "Cash", value = 120 }, { type = "Bullets", value = 2 }}
                    },
                    { -- Hello, World!
                        id          = "kill-octomechs",
                        title       = "TEXT_MISSION_KILL_OCTOMECHS",
                        desc        = "TEXT_MISSION_KILL_OCTOMECHS_DESC",
                        icon        = "menu2/icon_skull.png@linear",
                        objectives  = {{ type = "EnterEndless", endless_stage = 6, last_level = "maps/boss04.xml" }},
                        rewards     = {{ type = "Cash", value = 300 }, { type = "Bullets", value = 3 }}
                    }
                }
            },
            { -- Endless
                type        = "Uniform",
                id          = "Endless",
                groups      = 
                {
                    { -- Kill monsters
                        type        = "Sequence",
                        id          = "kill-monsters",
                        missions    =
                        {
                            { -- Activist
                                id          = "kill-1",
                                title       = "TEXT_MISSION_KILL_MONSTERS_E1",
                                desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 100 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Peace Bringer
                                id          = "kill-2",
                                title       = "TEXT_MISSION_KILL_MONSTERS_E2",
                                desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 250 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Human Enthusiast
                                id          = "kill-3",
                                title       = "TEXT_MISSION_KILL_MONSTERS_E3",
                                desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 500 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Earth Savior
                                id          = "kill-4",
                                title       = "TEXT_MISSION_KILL_MONSTERS_E4",
                                desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 1000 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Monster Shooter
                                id          = "kill-5",
                                title       = "TEXT_MISSION_KILL_MONSTERS_E5",
                                desc        = "TEXT_MISSION_KILL_MONSTERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 5000 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Kill sectoids
                        type        = "Sequence",
                        id          = "kill-sectoids",
                        missions    =
                        {
                            { -- Grey Matter I
                                id          = "kill-1",
                                title       = "TEXT_MISSION_KILL_SECTOIDS_E1",
                                desc        = "TEXT_MISSION_KILL_SECTOIDS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 50, entity1 = EntityType.SectoidSimple, entity2 = EntityType.SectoidShooting }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Grey Matter II
                                id          = "kill-2",
                                title       = "TEXT_MISSION_KILL_SECTOIDS_E2",
                                desc        = "TEXT_MISSION_KILL_SECTOIDS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 75, entity1 = EntityType.SectoidSimple, entity2 = EntityType.SectoidShooting }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Grey Matter III
                                id          = "kill-3",
                                title       = "TEXT_MISSION_KILL_SECTOIDS_E3",
                                desc        = "TEXT_MISSION_KILL_SECTOIDS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 120, entity1 = EntityType.SectoidSimple, entity2 = EntityType.SectoidShooting }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Grey Matter IV
                                id          = "kill-4",
                                title       = "TEXT_MISSION_KILL_SECTOIDS_E4",
                                desc        = "TEXT_MISSION_KILL_SECTOIDS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 200, entity1 = EntityType.SectoidSimple, entity2 = EntityType.SectoidShooting }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Grey Matter V
                                id          = "kill-5",
                                title       = "TEXT_MISSION_KILL_SECTOIDS_E5",
                                desc        = "TEXT_MISSION_KILL_SECTOIDS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 500, entity1 = EntityType.SectoidSimple, entity2 = EntityType.SectoidShooting }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Kill rollers
                        type        = "Sequence",
                        id          = "kill-rollers",
                        missions    =
                        {
                            { -- Rolled Out I
                                id          = "kill-1",
                                title       = "TEXT_MISSION_KILL_ROLLERS_E1",
                                desc        = "TEXT_MISSION_KILL_ROLLERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 50, entity1 = EntityType.SqueezerSimple, entity2 = EntityType.SqueezerTurning, entity3 = EntityType.NautilSimple, entity4 = EntityType.NautilTurning }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Rolled Out II
                                id          = "kill-2",
                                title       = "TEXT_MISSION_KILL_ROLLERS_E2",
                                desc        = "TEXT_MISSION_KILL_ROLLERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 75, entity1 = EntityType.SqueezerSimple, entity2 = EntityType.SqueezerTurning, entity3 = EntityType.NautilSimple, entity4 = EntityType.NautilTurning }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Rolled Out III
                                id          = "kill-3",
                                title       = "TEXT_MISSION_KILL_ROLLERS_E3",
                                desc        = "TEXT_MISSION_KILL_ROLLERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 120, entity1 = EntityType.SqueezerSimple, entity2 = EntityType.SqueezerTurning, entity3 = EntityType.NautilSimple, entity4 = EntityType.NautilTurning }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Rolled Out IV
                                id          = "kill-4",
                                title       = "TEXT_MISSION_KILL_ROLLERS_E4",
                                desc        = "TEXT_MISSION_KILL_ROLLERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 200, entity1 = EntityType.SqueezerSimple, entity2 = EntityType.SqueezerTurning, entity3 = EntityType.NautilSimple, entity4 = EntityType.NautilTurning }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Rolled Out V
                                id          = "kill-5",
                                title       = "TEXT_MISSION_KILL_ROLLERS_E5",
                                desc        = "TEXT_MISSION_KILL_ROLLERS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 500, entity1 = EntityType.SqueezerSimple, entity2 = EntityType.SqueezerTurning, entity3 = EntityType.NautilSimple, entity4 = EntityType.NautilTurning }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Kill whales
                        type        = "Sequence",
                        id          = "kill-whales",
                        missions    =
                        {
                            { -- Free Willy I
                                id          = "kill-1",
                                title       = "TEXT_MISSION_KILL_WHALES_E1",
                                desc        = "TEXT_MISSION_KILL_WHALES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 50, entity1 = EntityType.KillerWhale, entity2 = EntityType.KillerWhaleSimple }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Free Willy II
                                id          = "kill-2",
                                title       = "TEXT_MISSION_KILL_WHALES_E2",
                                desc        = "TEXT_MISSION_KILL_WHALES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 75, entity1 = EntityType.KillerWhale, entity2 = EntityType.KillerWhaleSimple }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Free Willy III
                                id          = "kill-3",
                                title       = "TEXT_MISSION_KILL_WHALES_E3",
                                desc        = "TEXT_MISSION_KILL_WHALES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 120, entity1 = EntityType.KillerWhale, entity2 = EntityType.KillerWhaleSimple }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Free Willy IV
                                id          = "kill-4",
                                title       = "TEXT_MISSION_KILL_WHALES_E4",
                                desc        = "TEXT_MISSION_KILL_WHALES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 200, entity1 = EntityType.KillerWhale, entity2 = EntityType.KillerWhaleSimple }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Free Willy V
                                id          = "kill-5",
                                title       = "TEXT_MISSION_KILL_WHALES_E5",
                                desc        = "TEXT_MISSION_KILL_WHALES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 500, entity1 = EntityType.KillerWhale, entity2 = EntityType.KillerWhaleSimple }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Kill flying
                        type        = "Sequence",
                        id          = "kill-flying",
                        missions    =
                        {
                            { -- Angry Rays I
                                id          = "kill-1",
                                title       = "TEXT_MISSION_KILL_FLYING_E1",
                                desc        = "TEXT_MISSION_KILL_FLYING_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 50, entity1 = EntityType.FloaterSimple, entity2 = EntityType.FloaterElectric, entity3 = EntityType.FloaterSower }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Angry Rays II
                                id          = "kill-2",
                                title       = "TEXT_MISSION_KILL_FLYING_E2",
                                desc        = "TEXT_MISSION_KILL_FLYING_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 75, entity1 = EntityType.FloaterSimple, entity2 = EntityType.FloaterElectric, entity3 = EntityType.FloaterSower }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Angry Rays III
                                id          = "kill-3",
                                title       = "TEXT_MISSION_KILL_FLYING_E3",
                                desc        = "TEXT_MISSION_KILL_FLYING_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 120, entity1 = EntityType.FloaterSimple, entity2 = EntityType.FloaterElectric, entity3 = EntityType.FloaterSower }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Angry Rays IV
                                id          = "kill-4",
                                title       = "TEXT_MISSION_KILL_FLYING_E4",
                                desc        = "TEXT_MISSION_KILL_FLYING_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 200, entity1 = EntityType.FloaterSimple, entity2 = EntityType.FloaterElectric, entity3 = EntityType.FloaterSower }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Angry Rays V
                                id          = "kill-5",
                                title       = "TEXT_MISSION_KILL_FLYING_E5",
                                desc        = "TEXT_MISSION_KILL_FLYING_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 500, entity1 = EntityType.FloaterSimple, entity2 = EntityType.FloaterElectric, entity3 = EntityType.FloaterSower }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Kill octopuses
                        type        = "Sequence",
                        id          = "kill-octopuses",
                        missions    =
                        {
                            { -- Sushi Master I
                                id          = "kill-1",
                                title       = "TEXT_MISSION_KILL_OCTOPUSES_E1",
                                desc        = "TEXT_MISSION_KILL_OCTOPUSES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 50, entity1 = EntityType.OctopusSimple, entity2 = EntityType.OctopusShotAware, entity3 = EntityType.OctopusChaser }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Sushi Master II
                                id          = "kill-2",
                                title       = "TEXT_MISSION_KILL_OCTOPUSES_E2",
                                desc        = "TEXT_MISSION_KILL_OCTOPUSES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 75, entity1 = EntityType.OctopusSimple, entity2 = EntityType.OctopusShotAware, entity3 = EntityType.OctopusChaser }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Sushi Master III
                                id          = "kill-3",
                                title       = "TEXT_MISSION_KILL_OCTOPUSES_E3",
                                desc        = "TEXT_MISSION_KILL_OCTOPUSES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 120, entity1 = EntityType.OctopusSimple, entity2 = EntityType.OctopusShotAware, entity3 = EntityType.OctopusChaser }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Sushi Master IV
                                id          = "kill-4",
                                title       = "TEXT_MISSION_KILL_OCTOPUSES_E4",
                                desc        = "TEXT_MISSION_KILL_OCTOPUSES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 200, entity1 = EntityType.OctopusSimple, entity2 = EntityType.OctopusShotAware, entity3 = EntityType.OctopusChaser }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Sushi Master V
                                id          = "kill-5",
                                title       = "TEXT_MISSION_KILL_OCTOPUSES_E5",
                                desc        = "TEXT_MISSION_KILL_OCTOPUSES_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "KillEnemy", target = 500, entity1 = EntityType.OctopusSimple, entity2 = EntityType.OctopusShotAware, entity3 = EntityType.OctopusChaser }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Add a friend
                        type        = "Sequence",
                        id          = "add-afriend",
                        missions    =
                        {
                            { -- Never Alone
                                id          = "add",
                                title       = "TEXT_MISSION_ADD_FRIEND",
                                desc        = "TEXT_MISSION_ADD_FRIEND_DESC",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "AddFriend" }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            }
                        }
                    },
                    { -- Updgrade to max
                        type        = "Sequence",
                        id          = "upgrade-max",
                        missions    =
                        {
                            { -- Full Potential
                                id          = "max",
                                title       = "TEXT_MISSION_ITEM_UPGRADE_MAX",
                                desc        = "TEXT_MISSION_ITEM_UPGRADE_MAX_DESC",
                                icon        = "menu2/icon_cash_gun.png@linear",
                                objectives  = {{ type = "ShopItemUpgradeMax" }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Collect orbs
                        type        = "Sequence",
                        id          = "collect-orbs",
                        missions    =
                        {
                            { -- Magnet I
                                id          = "collect-1",
                                title       = "TEXT_MISSION_COLLECT_ORBS_E1",
                                desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "Pickup", target = 150, object_type=PickupType.MagicOrb }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Magnet II
                                id          = "collect-2",
                                title       = "TEXT_MISSION_COLLECT_ORBS_E2",
                                desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "Pickup", target = 250, object_type=PickupType.MagicOrb }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Magnet III
                                id          = "collect-3",
                                title       = "TEXT_MISSION_COLLECT_ORBS_E3",
                                desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "Pickup", target = 500, object_type=PickupType.MagicOrb }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Magnet IV
                                id          = "collect-4",
                                title       = "TEXT_MISSION_COLLECT_ORBS_E4",
                                desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "Pickup", target = 750, object_type=PickupType.MagicOrb }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Magnet V
                                id          = "collect-5",
                                title       = "TEXT_MISSION_COLLECT_ORBS_E5",
                                desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "Pickup", target = 1000, object_type=PickupType.MagicOrb }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            },
                            { -- Magnet VI
                                id          = "collect-6",
                                title       = "TEXT_MISSION_COLLECT_ORBS_E6",
                                desc        = "TEXT_MISSION_COLLECT_ORBS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "Pickup", target = 1500, object_type=PickupType.MagicOrb }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Complete levels
                        type        = "Sequence",
                        id          = "complete-levels",
                        missions    =
                        { 
                            { -- Taking Back The Earth I
                                id          = "complete-1",
                                title       = "TEXT_MISSION_COMPLETE_LEVELS_E1",
                                desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "CompleteLevel", target = 5, story = true, boss = true }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Taking Back The Earth II
                                id          = "complete-2",
                                title       = "TEXT_MISSION_COMPLETE_LEVELS_E2",
                                desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "CompleteLevel", target = 10, story = true, boss = true }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Taking Back The Earth III
                                id          = "complete-3",
                                title       = "TEXT_MISSION_COMPLETE_LEVELS_E3",
                                desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "CompleteLevel", target = 15, story = true, boss = true }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Taking Back The Earth IV
                                id          = "complete-4",
                                title       = "TEXT_MISSION_COMPLETE_LEVELS_E4",
                                desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "CompleteLevel", target = 20, story = true, boss = true }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Taking Back The Earth V
                                id          = "complete-5",
                                title       = "TEXT_MISSION_COMPLETE_LEVELS_E5",
                                desc        = "TEXT_MISSION_COMPLETE_LEVELS_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "CompleteLevel", target = 25, story = true, boss = true }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Send gift
                        type        = "Sequence",
                        id          = "send-gift",
                        missions    =
                        {
                            { -- Friendly
                                id          = "send",
                                title       = "TEXT_MISSION_SEND_GIFT",
                                desc        = "TEXT_MISSION_SEND_GIFT_DESC",
                                icon        = "menu2/icon_cash_gun.png@linear",
                                objectives  = {{ type = "SendGift" }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            }
                        }
                    },
                    { -- Recive gift
                        type        = "Sequence",
                        id          = "recive-gift",
                        missions    =
                        {
                            { -- Good Karma
                                id          = "recive",
                                title       = "TEXT_MISSION_RECIVE_GIFT",
                                desc        = "TEXT_MISSION_RECIVE_GIFT_DESC",
                                icon        = "menu2/icon_cash_gun.png@linear",
                                objectives  = {{ type = "ReciveGift" }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            }
                        }
                    },
                    { -- Destroy objects
                        type        = "Sequence",
                        id          = "destroy-objects",
                        missions    =
                        {
                            { -- Watch the World Burn I
                                id          = "destroy-1",
                                title       = "TEXT_MISSION_DESTROY_OBJECTS_E1",
                                desc        = "TEXT_MISSION_DESTROY_OBJECTS_BLOW_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "DestroyObject", target = 30 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Watch the World Burn II
                                id          = "destroy-2",
                                title       = "TEXT_MISSION_DESTROY_OBJECTS_E2",
                                desc        = "TEXT_MISSION_DESTROY_OBJECTS_BLOW_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "DestroyObject", target = 50 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            },
                            { -- Watch the World Burn III
                                id          = "destroy-3",
                                title       = "TEXT_MISSION_DESTROY_OBJECTS_E3",
                                desc        = "TEXT_MISSION_DESTROY_OBJECTS_BLOW_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "DestroyObject", target = 75 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Watch the World Burn IV
                                id          = "destroy-4",
                                title       = "TEXT_MISSION_DESTROY_OBJECTS_E4",
                                desc        = "TEXT_MISSION_DESTROY_OBJECTS_BLOW_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "DestroyObject", target = 100 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 2 }}
                            },
                            { -- Watch the World Burn V
                                id          = "destroy-5",
                                title       = "TEXT_MISSION_DESTROY_OBJECTS_E5",
                                desc        = "TEXT_MISSION_DESTROY_OBJECTS_BLOW_DESC",
                                progress    = "TEXT_MISSIONS_PROGRESS",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "DestroyObject", target = 150 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    },
                    { -- Complete survival
                        type        = "Sequence",
                        id          = "complete-survival",
                        missions    =
                        {
                            { -- Mayhem Lover
                                id          = "all",
                                title       = "TEXT_MISSION_COMPLETE_SURVIVAL",
                                desc        = "TEXT_MISSION_COMPLETE_SURVIVAL_DESC",
                                icon        = "menu2/icon_medal.png@linear",
                                objectives  = {{ type = "PlaySurvival", levels = 4 }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 1 }}
                            }
                        }
                    },
                    { -- Score 10M
                        type        = "Sequence",
                        id          = "score-survival",
                        missions    =
                        {
                            { -- Doomsday
                                id          = "10m",
                                title       = "TEXT_MISSION_SCORE_SURVIVAL_E",
                                desc        = "TEXT_MISSION_SCORE_SURVIVAL_E_DESC",
                                icon        = "menu2/icon_skull.png@linear",
                                objectives  = {{ type = "Score", target = 50000, single_round = true }},
                                rewards     = {{ type = "Gold", value = 10 }, { type = "Bullets", value = 3 }}
                            }
                        }
                    }
                }
            }
        }
    },
    { -- Player Ranks
        id          = "Ranks",
        type        = "Sequence",
        missions    = 
        {
            {
                id          = "dumdum",
                title       = "TEXT_MISSIONS_RANK_0",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 7 }},
                rewards     = {{ type = "SubscriptionGold", value = 10 }}
            },
            {
                id          = "rookie",
                title       = "TEXT_MISSIONS_RANK_1",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 8 }},
                rewards     = {{ type = "SubscriptionGold", value = 20 }}
            },
            {
                id          = "cruel",
                title       = "TEXT_MISSIONS_RANK_2",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 9 }},
                rewards     = {{ type = "SubscriptionGold", value = 30 }}
            },
            {
                id          = "brutal",
                title       = "TEXT_MISSIONS_RANK_3",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 10 }},
                rewards     = {{ type = "SubscriptionGold", value = 40 }}
            },
            {
                id          = "unstoppable",
                title       = "TEXT_MISSIONS_RANK_4",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 10 }},
                rewards     = {{ type = "SubscriptionGold", value = 50 }}
            },
            {
                id          = "bloodthirsty",
                title       = "TEXT_MISSIONS_RANK_5",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 11 }},
                rewards     = {{ type = "SubscriptionGold", value = 60 }}
            },
            {
                id          = "exterminator",
                title       = "TEXT_MISSIONS_RANK_6",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 11 }},
                rewards     = {{ type = "SubscriptionGold", value = 70 }}
            },
            {
                id          = "ripper",
                title       = "TEXT_MISSIONS_RANK_7",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 12 }},
                rewards     = {{ type = "SubscriptionGold", value = 80 }}
            },
            {
                id          = "dominator",
                title       = "TEXT_MISSIONS_RANK_8",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 12 }},
                rewards     = {{ type = "SubscriptionGold", value = 90 }}
            },
            {
                id          = "utilizator",
                title       = "TEXT_MISSIONS_RANK_9",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 13 }},
                rewards     = {{ type = "SubscriptionGold", value = 100 }}
            },
            {
                id          = "annihilator",
                title       = "TEXT_MISSIONS_RANK_10",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 13 }},
                rewards     = {{ type = "SubscriptionGold", value = 110 }}
            },
            {
                id          = "unbeatable",
                title       = "TEXT_MISSIONS_RANK_11",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 14 }},
                rewards     = {{ type = "SubscriptionGold", value = 120 }}
            },
            {
                id          = "apocalyptic",
                title       = "TEXT_MISSIONS_RANK_12",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 14 }},
                rewards     = {{ type = "SubscriptionGold", value = 130 }}
            },
            {
                id          = "mrmayhem",
                title       = "TEXT_MISSIONS_RANK_13",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 15 }},
                rewards     = {{ type = "SubscriptionGold", value = 140 }}
            },
            {
                id          = "indestructible",
                title       = "TEXT_MISSIONS_RANK_14",
                autoreward  = true,
                objectives  = {{ type = "Bullets", target = 15 }},
                rewards     = {{ type = "SubscriptionGold", value = 150 }}
            },
            {
                id          = "thewalkingdeath",
                title       = "TEXT_MISSIONS_RANK_15",
                objectives  = {},
                rewards     = {}
            }
        }
    }
}

MissionsAchievementsID =
{
	{
		id = "dumdum",
		googleId = ""
	},
	{
		id = "rookie",
		googleId = "CgkIzq7_ovcHEAIQBg"
	},
	{
		id = "cruel",
		googleId = "CgkIzq7_ovcHEAIQBw"
	},
	{
		id = "brutal",
		googleId = "CgkIzq7_ovcHEAIQCA"
	},
	{
		id = "unstoppable",
		googleId = "CgkIzq7_ovcHEAIQCQ"
	},
	{
		id = "bloodthirsty",
		googleId = "CgkIzq7_ovcHEAIQCg"
	},
	{
		id = "exterminator",
		googleId = "CgkIzq7_ovcHEAIQCw"
	},
	{
		id = "ripper",
		googleId = "CgkIzq7_ovcHEAIQDA"
	},
	{
		id = "dominator",
		googleId = "CgkIzq7_ovcHEAIQDQ"
	},
	{
		id = "utilizator",
		googleId = "CgkIzq7_ovcHEAIQDg"
	},
	{
		id = "annihilator",
		googleId = "CgkIzq7_ovcHEAIQDw"
	},
	{
		id = "unbeatable",
		googleId = "CgkIzq7_ovcHEAIQEA"
	},
	{
		id = "apocalyptic",
		googleId = "CgkIzq7_ovcHEAIQEQ"
	},
	{
		id = "mrmayhem",
		googleId = "CgkIzq7_ovcHEAIQEg"
	},
	{
		id = "indestructible",
		googleId = "CgkIzq7_ovcHEAIQEw"
	},
	{
		id = "thewalkingdeath",
		googleId = "CgkIzq7_ovcHEAIQFA"
	}
}

function GetAchievementId( entryId )
	for _, entry in ipairs( MissionsAchievementsID ) do
		if( entryId == entry.id ) then
			return entry.googleId
		end
	end
	return ""
end

function MissionLoad()
    for _, group in ipairs( MissionDB ) do
        MissionCreateGroup( group )
    end
end

function MissionCreateGroup( group )
    MissionFactory:StartGroup( group.type, group.id )
    if group.missions then
        for _, mission in ipairs( group.missions ) do
            MissionCreateMission( mission )
        end
    end

    if group.groups then
        for _, subgroup in ipairs( group.groups ) do
            MissionCreateGroup( subgroup )
        end
    end
    MissionFactory:EndGroup()
end

function MissionCreateMission( mission )
    MissionFactory:StartMission( mission.id, mission.autoreward )
    for _, objective in ipairs( mission.objectives ) do
        MissionCreateObjective( objective )
    end
    for _, reward in ipairs( mission.rewards ) do
        MissionCreateReward( reward )
    end
    MissionFactory:EndMission()
end

function MissionCreateObjective( objective )
    MissionFactory:CreateObjective( objective.type, objective )
end

function MissionCreateReward( reward )
    MissionFactory:CreateReward( reward.type, reward )
end

function MissionInit()
    if Missions == nil then
        Missions = {}
        Groups = {}
        MissionInitProcess( MissionDB, nil )
    end
end

function MissionInitProcess( items, parentPath )
    for _, item in ipairs( items ) do
        local itemPath = parentPath ~= nil and (parentPath .. "/" .. item.id) or item.id
        
        if item.groups then
            MissionInitProcess( item.groups, itemPath )
            Groups[itemPath] = item
        elseif item.missions then
            MissionInitProcess( item.missions, itemPath )
            Groups[itemPath] = item
        else
            Missions[itemPath] = item
        end
    end
end

MissionInit()

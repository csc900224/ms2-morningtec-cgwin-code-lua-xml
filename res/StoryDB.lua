StoryLevels = {}

StoryLevels[1] = {}
StoryLevels[1][1] = {}
StoryLevels[1][1][1] = { l = Tutorial02, m = { EntityType.SectoidSimple, EntityType.SectoidShooting, EntityType.NautilSimple }, e = { 1, 0, 0 }, r = { s = 152, xp = 120 } }
StoryLevels[1][1][2] = { l = Tutorial03, m = { EntityType.SectoidSimple, EntityType.SectoidShooting }, e = { 0, 0, 0 }, r = { s = 153, xp = 120 } }
StoryLevels[1][1][3] = { l = Tutorial04, m = { EntityType.KillerWhaleSimple, EntityType.OctopusChaser, EntityType.Nerval }, e = { 0, 0, 0 }, r = { s = 155, xp = 120 } }

StoryLevels[2] = {
    order = { 1, 2, 3, 4, 5 },
    boss = { { l = Boss03, m = Boss03.m, e = { 1, 1, 1 }, r = { s = 400, xp = 140 }, news = "TEXT_NEWSBAR_PREDEF_1", fl = false } }
}
StoryLevels[2][1] = {}
StoryLevels[2][1][1] = { l = Suburbs04, m = { EntityType.SectoidSimple, EntityType.Crab, EntityType.NautilSimple }, e = { 0, 0, 0 }, r = { s = 230, xp = 140 }, cutscene = "startmission.mp4", fl = false }
StoryLevels[2][1][2] = { l = Suburbs02, m = { EntityType.SectoidSimple, EntityType.SectoidShooting, EntityType.NautilSimple }, e = { 0, 0, 0 }, r = { s = 233, xp = 140 }, fl = false }
StoryLevels[2][1][3] = { l = Suburbs10, m = { EntityType.Crab, EntityType.SectoidSimple, EntityType.NautilTurning }, e = { 0, 0, 0 }, r = { s = 237, xp = 140 }, fl = false }
StoryLevels[2][2] = {}
StoryLevels[2][2][1] = { l = Suburbs06, m = { EntityType.SectoidSimple, EntityType.OctopusSimple, EntityType.NautilSimple }, e = { 1, 0, 0 }, r = { s = 240, xp = 140 }, fl = false }
StoryLevels[2][2][2] = { l = Suburbs11, m = { EntityType.SectoidSimple, EntityType.Crab, EntityType.NautilTurning }, e = { 0, 1, 0 }, r = { s = 245, xp = 140 }, fl = false }
StoryLevels[2][2][3] = { l = Suburbs01, m = { EntityType.SectoidShooting, EntityType.NautilSimple, EntityType.OctopusSimple }, e = { 1, 0, 1 }, r = { s = 248, xp = 140 }, fl = false }
StoryLevels[2][2][4] = { l = Suburbs12, m = { EntityType.SectoidSimple, EntityType.SectoidShooting, EntityType.NautilTurning }, e = { 1, 1, 0 }, r = { s = 250, xp = 140 }, fl = false }
StoryLevels[2][3] = {}
StoryLevels[2][3][1] = { l = Suburbs13, m = { EntityType.SectoidSimple, EntityType.Crab, EntityType.NautilTurning }, e = { 1, 1, 1 }, r = { s = 250, xp = 140 }, fl = false }
StoryLevels[2][3][2] = { l = Suburbs03, m = { EntityType.Crab, EntityType.NautilSimple, EntityType.SectoidSimple }, e = { 1, 1, 1 }, r = { s = 252, xp = 140 }, fl = false }
StoryLevels[2][4] = {}
StoryLevels[2][4][1] = { l = City04, m = { EntityType.SectoidSimple, EntityType.Crab, EntityType.KillerWhaleSimple }, e = { 1, 1, 1 }, r = { s = 255, xp = 140 }, fl = false }
StoryLevels[2][4][2] = { l = City02, m = { EntityType.OctopusSimple, EntityType.SectoidShooting, EntityType.NautilSimple }, e = { 1, 1, 1 }, r = { s = 260, xp = 140 }, fl = false }
StoryLevels[2][4][3] = { l = City06, m = { EntityType.OctopusSimple, EntityType.KillerWhaleSimple, EntityType.Crab }, e = { 1, 1, 1 }, r = { s = 263, xp = 140 }, fl = false }
StoryLevels[2][5] = {}
StoryLevels[2][5][1] = { l = City13, m = { EntityType.SectoidSimple, EntityType.SqueezerSimple, EntityType.Crab }, e = { 1, 1, 1 }, r = { s = 267, xp = 140 }, fl = false }
StoryLevels[2][5][2] = { l = City09, m = { EntityType.KillerWhaleSimple, EntityType.SqueezerSimple, EntityType.SectoidShooting }, e = { 1, 1, 1 }, r = { s = 270, xp = 140 }, fl = false }

StoryLevels[3] = {
    order = { 4, 2, 3, 5, 1 },
    boss = { { l = Boss02, m = Boss02.m, e = { 2, 2, 2 }, r = { s = 720, xp = 160 }, news = "TEXT_NEWSBAR_PREDEF_2", fl = false } }
}
StoryLevels[3][1] = {}
StoryLevels[3][1][1] = { l = Suburbs05, m = { EntityType.Nerval, EntityType.SectoidSimple, EntityType.Crab }, e = { 1, 2, 0 }, r = { s = 635, xp = 160 }, fl = false }
StoryLevels[3][1][2] = { l = Suburbs08, m = { EntityType.SectoidShooting, EntityType.KillerWhaleSimple, EntityType.OctopusSimple }, e = { 0, 2, 1 }, r = { s = 640, xp = 160 }, fl = false }
StoryLevels[3][2] = {}
StoryLevels[3][2][1] = { l = Suburbs14, m = { EntityType.SectoidShooting, EntityType.OctopusChaser, EntityType.Crab }, e = { 1, 1, 1 }, r = { s = 580, xp = 160 }, fl = false }
StoryLevels[3][2][2] = { l = Suburbs16, m = { EntityType.OctopusChaser, EntityType.SqueezerSimple, EntityType.SectoidShooting }, e = { 1, 1, 1 }, r = { s = 590, xp = 160 }, fl = false }
StoryLevels[3][3] = {}
StoryLevels[3][3][1] = { l = City10, m = { EntityType.SqueezerTurning, EntityType.Nerval, EntityType.Crab }, e = { 1, 1, 1 }, r = { s = 595, xp = 160 }, fl = false }
StoryLevels[3][3][2] = { l = City02, m = { EntityType.OctopusChaser, EntityType.SqueezerTurning, EntityType.Crab }, e = { 1, 1, 1 }, r = { s = 600, xp = 160 }, fl = false }
StoryLevels[3][3][3] = { l = City12, m = { EntityType.Nerval, EntityType.OctopusChaser, EntityType.SectoidSimple }, e = { 2, 1, 1 }, r = { s = 605, xp = 160 }, fl = false }
StoryLevels[3][3][4] = { l = City14, m = { EntityType.SectoidShooting, EntityType.NautilTurning, EntityType.OctopusChaser }, e = { 1, 2, 1 }, r = { s = 610, xp = 160 }, fl = false }
StoryLevels[3][4] = {}
StoryLevels[3][4][1] = { l = Desert03, m = { EntityType.KillerWhale, EntityType.Nerval, EntityType.KillerWhaleSimple }, e = { 1, 1, 0 }, r = { s = 560, xp = 160 }, fl = false }
StoryLevels[3][4][2] = { l = Desert08, m = { EntityType.SqueezerSimple, EntityType.Crab, EntityType.SectoidShooting }, e = { 1, 1, 1 }, r = { s = 565, xp = 160 }, fl = false }
StoryLevels[3][4][3] = { l = Desert12, m = { EntityType.OctopusChaser, EntityType.KillerWhaleSimple, EntityType.SectoidShooting }, e = { 1, 1, 1 }, r = { s = 570, xp = 160 }, fl = false }
StoryLevels[3][4][4] = { l = Desert05, m = { EntityType.SectoidSimple, EntityType.OctopusSimple, EntityType.FishSimple }, e = { 1, 1, 1 }, r = { s = 575, xp = 160 }, fl = false }
StoryLevels[3][5] = {}
StoryLevels[3][5][1] = { l = City08, m = { EntityType.FishSimple, EntityType.OctopusSimple, EntityType.SqueezerTurning }, e = { 2, 1, 2 }, r = { s = 620, xp = 160 }, fl = false }
StoryLevels[3][5][2] = { l = City01, m = { EntityType.SectoidSimple, EntityType.OctopusChaser, EntityType.OctopusSimple }, e = { 1, 2, 2 }, r = { s = 625, xp = 160 }, fl = false }
StoryLevels[3][5][3] = { l = City07, m = { EntityType.KillerWhale, EntityType.Crab, EntityType.OctopusChaser }, e = { 2, 2, 1 }, r = { s = 630, xp = 160 }, fl = false }

StoryLevels[4] = {
    order = { 2, 3, 4, 1, 5 },
    boss = { { l = Boss01, m = Boss01.m, e = { 2, 2, 2 }, r = { s = 1080, xp = 180 }, news = "TEXT_NEWSBAR_PREDEF_3", fl = false } }
}
StoryLevels[4][3] = {}
StoryLevels[4][3][1] = { l = Suburbs09, m = { EntityType.KillerWhale, EntityType.Nerval, EntityType.OctopusSimple }, e = { 2, 1, 1 }, r = { s = 880, xp = 180 }, fl = false }
StoryLevels[4][3][2] = { l = Suburbs15, m = { EntityType.SectoidShooting, EntityType.NautilTurning, EntityType.FishSimple }, e = { 1, 2, 2 }, r = { s = 890, xp = 180 }, fl = false }
StoryLevels[4][3][3] = { l = Suburbs17, m = { EntityType.Nerval, EntityType.HoundSimple, EntityType.Crab }, e = { 2, 2, 1 }, r = { s = 895, xp = 180 }, fl = false }
StoryLevels[4][4] = {}
StoryLevels[4][4][1] = { l = City15, m = { EntityType.FishSimple, EntityType.SqueezerTurning, EntityType.OctopusSimple }, e = { 2, 1, 1 }, r = { s = 900, xp = 180 }, fl = false }
StoryLevels[4][4][2] = { l = City11, m = { EntityType.NautilTurning, EntityType.SectoidSimple, EntityType.KillerWhaleSimple }, e = { 1, 2, 1 }, r = { s = 905, xp = 180 }, fl = false }
StoryLevels[4][1] = {}
StoryLevels[4][1][1] = { l = Desert04, m = { EntityType.OctopusShotAware, EntityType.KillerWhale, EntityType.Nerval }, e = { 2, 1, 2 }, r = { s = 910, xp = 180 }, fl = false }
StoryLevels[4][1][2] = { l = Desert09, m = { EntityType.OctopusShotAware, EntityType.SectoidShooting, EntityType.Crab }, e = { 1, 2, 1 }, r = { s = 920, xp = 180 }, fl = false }
StoryLevels[4][1][3] = { l = Desert06, m = { EntityType.NautilTurning, EntityType.Nerval, EntityType.KillerWhale }, e = { 1, 1, 2 }, r = { s = 925, xp = 180 }, fl = false }
StoryLevels[4][5] = {}
StoryLevels[4][5][1] = { l = Desert07, m = { EntityType.FishThrowing, EntityType.SectoidShooting, EntityType.SqueezerSimple }, e = { 1, 1, 2 }, r = { s = 930, xp = 180 }, fl = false }
StoryLevels[4][5][2] = { l = Desert01, m = { EntityType.OctopusChaser, EntityType.KillerWhale, EntityType.SectoidSimple }, e = { 1, 1, 2 }, r = { s = 935, xp = 180 }, fl = false }
StoryLevels[4][5][3] = { l = Desert11, m = { EntityType.FishThrowing, EntityType.HoundSimple, EntityType.NautilTurning }, e = { 2, 1, 1 }, r = { s = 940, xp = 180 }, fl = false }
StoryLevels[4][5][4] = { l = Desert13, m = { EntityType.OctopusChaser, EntityType.FishSimple, EntityType.HoundSimple }, e = { 2, 2, 1 }, r = { s = 950, xp = 180 }, fl = false }
StoryLevels[4][2] = {}
StoryLevels[4][2][1] = { l = Ice03, m = { EntityType.FloaterSimple, EntityType.SectoidSimple, EntityType.SqueezerTurning }, e = { 1, 2, 1 }, r = { s = 860, xp = 180 }, fl = false }
StoryLevels[4][2][2] = { l = Ice02, m = { EntityType.FloaterSimple, EntityType.Crab, EntityType.NautilTurning }, e = { 2, 1, 1 }, r = { s = 865, xp = 180 }, fl = false }
StoryLevels[4][2][3] = { l = Ice01, m = { EntityType.FishThrowing, EntityType.Nerval, EntityType.SectoidShooting }, e = { 1, 2, 2 }, r = { s = 870, xp = 180 }, fl = false }
StoryLevels[4][2][4] = { l = Ice04, m = { EntityType.FloaterSimple, EntityType.HoundSimple, EntityType.KillerWhaleSimple }, e = { 2, 1, 2 }, r = { s = 875, xp = 180 }, fl = false }

StoryLevels[5] = {
    order = { 1, 4, 2, 3, 5 },
    boss = { { l = Boss04, m = Boss04.m, e = { 3, 3, 3 }, r = { s = 1200, xp = 200 }, news = "TEXT_NEWSBAR_PREDEF_4", fl = false } }
}
StoryLevels[5][1] = {}
StoryLevels[5][1][1] = { l = Suburbs07, m = { EntityType.NautilTurning, EntityType.Nerval, EntityType.OctopusChaser }, e = { 1, 3, 1 }, r = { s = 1150, xp = 200 }, fl = false }
StoryLevels[5][1][2] = { l = Suburbs18, m = { EntityType.OctopusShotAware, EntityType.SectoidShooting, EntityType.KillerWhale }, e = { 3, 1, 2 }, r = { s = 1160, xp = 200 }, fl = false }
StoryLevels[5][1][3] = { l = Suburbs19, m = { EntityType.HoundSimple, EntityType.KillerWhaleSimple, EntityType.SqueezerTurning }, e = { 2, 2, 3 }, r = { s = 1165, xp = 200 }, fl = false }
StoryLevels[5][3] = {}
StoryLevels[5][3][1] = { l = City18, m = { EntityType.FloaterElectric, EntityType.SectoidSimple, EntityType.OctopusShotAware }, e = { 3, 3, 1 }, r = { s = 1210, xp = 200 }, fl = false }
StoryLevels[5][3][2] = { l = City16, m = { EntityType.SqueezerSimple, EntityType.Nerval, EntityType.KillerWhale }, e = { 3, 2, 3 }, r = { s = 1220, xp = 780 }, fl = false }
StoryLevels[5][3][3] = { l = City17, m = { EntityType.FloaterElectric, EntityType.OctopusChaser, EntityType.Crab }, e = { 1, 1, 3 }, r = { s = 1225, xp = 200 }, fl = false }
StoryLevels[5][3][4] = { l = City19, m = { EntityType.KillerWhaleSimple, EntityType.NautilSimple, EntityType.HoundSimple }, e = { 2, 1, 3 }, r = { s = 1230, xp = 200 }, fl = false }
StoryLevels[5][2] = {}
StoryLevels[5][2][1] = { l = Desert02, m = { EntityType.Lobster, EntityType.SectoidSimple, EntityType.KillerWhaleSimple }, e = { 1, 3, 1 }, r = { s = 1195, xp = 200 }, fl = false }
StoryLevels[5][2][2] = { l = Desert10, m = { EntityType.HoundSimple, EntityType.OctopusShotAware, EntityType.Crab }, e = { 3, 2, 3 }, r = { s = 1200, xp = 200 }, fl = false }
StoryLevels[5][2][3] = { l = Desert14, m = { EntityType.Lobster, EntityType.OctopusChaser, EntityType.Nerval }, e = { 3, 3, 3 }, r = { s = 1205, xp = 200 }, fl = false }
StoryLevels[5][4] = {}
StoryLevels[5][4][1] = { l = Ice05, m = { EntityType.HoundShooting, EntityType.NautilTurning, EntityType.KillerWhaleSimple }, e = { 3, 1, 2 }, r = { s = 1170, xp = 200 }, fl = false }
StoryLevels[5][4][2] = { l = Ice06, m = { EntityType.KillerWhale, EntityType.Nerval, EntityType.Crab }, e = { 2, 3, 1 }, r = { s = 1175, xp = 200 }, fl = false }
StoryLevels[5][4][3] = { l = Ice07, m = { EntityType.HoundShooting, EntityType.SqueezerSimple, EntityType.HoundSimple }, e = { 1, 3, 2 }, r = { s = 1180, xp = 200 }, fl = false }
StoryLevels[5][4][4] = { l = Ice08, m = { EntityType.HoundSimple, EntityType.NautilTurning, EntityType.SectoidShooting }, e = { 1, 2, 3 }, r = { s = 1190, xp = 200 }, fl = false }
StoryLevels[5][5] = {}
StoryLevels[5][5][1] = { l = Ice09, m = { EntityType.FloaterSimple, EntityType.HoundShooting, EntityType.SectoidSimple }, e = { 2, 1, 3 }, r = { s = 1235, xp = 200 }, fl = false }
StoryLevels[5][5][2] = { l = Ice10, m = { EntityType.Lobster, EntityType.KillerWhaleSimple, EntityType.Nerval }, e = { 2, 3, 1 }, r = { s = 1240, xp = 200 }, fl = false }
StoryLevels[5][5][3] = { l = Ice03, m = { EntityType.HoundShooting, EntityType.Nerval, EntityType.SectoidSimple }, e = { 3, 2, 1 }, r = { s = 1250, xp = 200 }, fl = false }

function StoryGetMissionIdx( map, area )
    local m = StoryLevels[map]
    if not m then return nil end

    local a = StoryLevels[map][area]
    if not a then return nil end

    local completed = registry:Get( "/maps/" .. map .. "/" .. area .. "/completed" ) or 0
    return completed + 1
end

function StoryGetMission( map, area )
    local idx = StoryGetMissionIdx( map, area )
    if idx == nil then return nil end
    return StoryLevels[map][area][idx]
end

function StroyGetAreasCount( map )
    local m = StoryLevels[map]
    if not m then return nil end
    return #m
end

function StroyGetAreaIdx( map, area )
    local areas = StoryLevels[map]
    if areas == nil or areas.order == nil then
        return area
    end
        
    for idx, a in ipairs( areas.order ) do
        if a == area then
            return idx
        end
    end
    return 0
end

function StoryGetMissionsCount( map, area )
    local m = StoryLevels[map]
    if not m then return nil end

    local a = StoryLevels[map][area]
    if not a then return nil end

    return #a
end

function StoryIsLocked( map, area )
    local m = StoryLevels[map]
    if not m then
        return false
    end

    local order = m.order
    if not order then
        return false
    end

    local index = 0
    for i, v in pairs( order ) do
        if v == area then
            index = i
            break
        end
    end

    if index > 1 then
        local previousArea = order[index - 1]
        local missions = StoryGetMissionsCount( map, previousArea )
        local completed = registry:Get( "/maps/" .. map .. "/" .. previousArea .. "/completed" )
        if completed < missions then
            return true
        end
    end

    return false
end

function StoryIsEndless( map )
    return map > #StoryLevels
end

function StoryHasBoss( map )
    local m = StoryLevels[map]
    if not m then return nil end

    return m.boss ~= nil
end

function StoryGetBossProgress( map, offset )
    local m = StoryLevels[map]
    if not m then return nil end

    local count = 0
    for i, area in ipairs( m ) do
        count = count + #area
    end

    local completed = registry:Get( "/maps/" .. map .. "/completed-missions-count" ) or 0
    if offset then
        completed = completed + offset
    end

    if completed < 0 then
        completed = 0
    elseif completed > count then
        completed = count
    end

    return completed/count
end

function StoryIsCompleted( map )
    local m = StoryLevels[map]
    if not m then return nil end

    local count = 0
    for i, area in ipairs( m ) do
        count = count + #area
    end

    local completed = registry:Get( "/maps/" .. map .. "/completed-missions-count" ) or 0
    return count == completed
end

function StoryIsBossCompleted( map )
    if StoryIsEndless( map ) then
        return nil
    end

    return registry:Get( "/maps/" .. map .. "/boss/completed" ) > 0
end

function StoryIsBossAreaUnlocked( map )
    return StoryGetFirstUnlockedArea( map ) == "boss"
end

function StoryGetFirstUnlockedArea( map )
    local m = StoryLevels[map]
    if not m then
        return 0
    end
    
    local area = 0
    if m.order ~= nil then
        local ordsize = #m.order
        local rorder = {}
        for i, v in ipairs( m.order ) do
            rorder[1 + ordsize-i] = v
        end

        for _, areaIdx in ipairs( rorder ) do
            if not StoryIsLocked( map, areaIdx ) then
                area = areaIdx
                break
            end
        end
    else
        if not StoryIsLocked( m, 1 ) then
            area = 1
        end
    end
    
    if m.boss ~= nil and area == StroyGetAreasCount( map ) then
        if registry:Get( "/maps/" .. map .. "/" .. area .. "/completed" ) == StoryGetMissionsCount( map, area ) then
            area = "boss"
        end
    end
   
    return area
end

function StoryIsEndlessMode()
    local currentMap = registry:Get( "/maps/current" ) or 0
    return currentMap > #StoryLevels
end

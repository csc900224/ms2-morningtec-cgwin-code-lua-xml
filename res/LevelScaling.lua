LevelScalingData = {}
LevelScalingData[EntityType.SectoidSimple]        = { hpBase = 1.02,    hpLvl = 1,  hpLvlRatio = 0.07, moneyLvl = 5,  moneyRatio = 0.1, moneyDecrease = true }
LevelScalingData[EntityType.SectoidShooting]      = { hpBase = 1.28,    hpLvl = 1,  hpLvlRatio = 0.07, moneyLvl = 5,  moneyRatio = 0.1, moneyDecrease = true }
LevelScalingData[EntityType.NautilSimple]         = { hpBase = 1.70,    hpLvl = 1,  hpLvlRatio = 0.07, moneyLvl = 5,  moneyRatio = 0.1, moneyDecrease = true }
LevelScalingData[EntityType.NautilTurning]        = { hpBase = 3.49,    hpLvl = 3,  hpLvlRatio = 0.07, moneyLvl = 5,  moneyRatio = 0.1, moneyDecrease = true }
LevelScalingData[EntityType.Crab]                 = { hpBase = 1.45,    hpLvl = 1,  hpLvlRatio = 0.07, moneyLvl = 5,  moneyRatio = 0.2, moneyDecrease = true }
LevelScalingData[EntityType.OctopusSimple]        = { hpBase = 6.07,    hpLvl = 4,  hpLvlRatio = 0.07, moneyLvl = 5,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.SqueezerSimple]       = { hpBase = 11.94,   hpLvl = 7,  hpLvlRatio = 0.07, moneyLvl = 6,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.SqueezerTurning]      = { hpBase = 17.31,   hpLvl = 11, hpLvlRatio = 0.07, moneyLvl = 6,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.KillerWhale]          = { hpBase = 26.62,   hpLvl = 10, hpLvlRatio = 0.07, moneyLvl = 6,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.KillerWhaleSimple]    = { hpBase = 17.67,   hpLvl = 6,  hpLvlRatio = 0.07, moneyLvl = 6,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.OctopusChaser]        = { hpBase = 17.72,   hpLvl = 8,  hpLvlRatio = 0.07, moneyLvl = 8,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.Nerval]               = { hpBase = 22.14,   hpLvl = 8,  hpLvlRatio = 0.07, moneyLvl = 8,  moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.FishSimple]           = { hpBase = 43.53,   hpLvl = 12, hpLvlRatio = 0.07, moneyLvl = 10, moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.FishThrowing]         = { hpBase = 42.68,   hpLvl = 14, hpLvlRatio = 0.07, moneyLvl = 10, moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.HoundSimple]          = { hpBase = 31.09,   hpLvl = 12, hpLvlRatio = 0.07, moneyLvl = 12, moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.OctopusShotAware]     = { hpBase = 33.33,   hpLvl = 13, hpLvlRatio = 0.07, moneyLvl = 13, moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.FloaterSimple]        = { hpBase = 30.24,   hpLvl = 15, hpLvlRatio = 0.07, moneyLvl = 15, moneyRatio = 0.2, moneyDecrease = false }
LevelScalingData[EntityType.FloaterElectric]      = { hpBase = 50.73,   hpLvl = 17, hpLvlRatio = 0.07, moneyLvl = 17, moneyRatio = 0.5, moneyDecrease = false }
LevelScalingData[EntityType.HoundShooting]        = { hpBase = 56.10,   hpLvl = 19, hpLvlRatio = 0.07, moneyLvl = 18, moneyRatio = 0.5, moneyDecrease = false }
LevelScalingData[EntityType.Lobster]              = { hpBase = 56.10,   hpLvl = 19, hpLvlRatio = 0.07, moneyLvl = 18, moneyRatio = 0.5, moneyDecrease = false }
LevelScalingData[EntityType.OctopusFriend]        = { hpLvlRatio = 2.5 }
LevelScalingData[EntityType.OctobrainBoss]        = { hpBase = 936.84, hpLvl = 6,  hpLvlRatio = 0.07, moneyLvl = 7,  moneyRatio = 0.5, moneyDecrease = false }
LevelScalingData[EntityType.SowerBoss]            = { hpBase = 1357.89, hpLvl = 10, hpLvlRatio = 0.07, moneyLvl = 11, moneyRatio = 0.5, moneyDecrease = false }
LevelScalingData[EntityType.MechaBoss]            = { hpBase = 1884.21, hpLvl = 15, hpLvlRatio = 0.07, moneyLvl = 16, moneyRatio = 0.5, moneyDecrease = false }
LevelScalingData[EntityType.OctobrainBossClone]   = { hpBase = 5.00,    hpLvl = 7,  hpLvlRatio = 0.07, moneyLvl = 7,  moneyRatio = 0.5, moneyDecrease = false }

function ScaleLevels( data, monster, level )
    local saclingData = LevelScalingData[monster]
    if saclingData then
        if monster == EntityType.OctopusFriend then
            data.HP = data.HP + level * saclingData.hpLvlRatio
        else
            data.HP = saclingData.hpBase + ((level - saclingData.hpLvl) * (level * saclingData.hpLvlRatio))
            if saclingData.moneyDecrease then
                data.MoneyAmount = data.MoneyAmount + (level - saclingData.moneyLvl) * saclingData.moneyRatio
            else
                data.MoneyAmount = data.MoneyAmount + math.max(0, level - saclingData.moneyLvl) * saclingData.moneyRatio
            end
        end
    end
end

SurvivalLevelsInfo = {}

SurvivalLevelInfoSuburbs = {
Level = 1,
LevelTitle = "TEXT_SURVIVAL_HEADER_1",
LevelIcon = "menu2/surv_surburbs.png@linear",
LevelRequred = 0,
StatsFriends = {},
StatsGlobal = {},
TabActive = nil
}

SurvivalLevelInfoCityRampage = {
Level = 2,
LevelTitle = "TEXT_SURVIVAL_HEADER_2",
LevelIcon = "menu2/surv_city.png@linear",
LevelRequred = 10,
StatsFriends = {},
StatsGlobal = {},
TabActive = nil
}

SurvivalLevelInfoDesert = {
Level = 3,
LevelTitle = "TEXT_SURVIVAL_HEADER_3",
LevelIcon = "menu2/surv_desert.png@linear",
LevelRequred = 20,
StatsFriends = {},
StatsGlobal = {},
TabActive = nil
}

SurvivalLevelInfoIce = {
Level = 4,
LevelTitle = "TEXT_SURVIVAL_HEADER_4",
LevelIcon = "menu2/surv_ice.png@linear",
LevelRequred = 30,
StatsFriends = {},
StatsGlobal = {},
TabActive = nil
}

function AddFriendsStatsForLevel( level ,  place , name , score )

    StatEntry = { LevelId = level, Place = place, Name = name , Score = score }
    
    if name == User.Name then 
    
        local currHighscore = registry:Get( "/monstaz/hiscore/" .. level .. "/score")
        
        local isNewHighscore = tonumber(score) > currHighscore;
        
        if isNewHighscore then
            registry:Set( "/monstaz/hiscore/" .. level .. "/score", tonumber(score))
            GameEventDispatcher:HandleGameEvent( GameEvent.GEI_GAME_HISCORE_CHANGED, level )
        end
    end
    
    for i=1,# SurvivalLevelsInfo do
        if SurvivalLevelsInfo[i].Level == StatEntry.LevelId then
            table.insert( SurvivalLevelsInfo[i].StatsFriends, StatEntry )
            break;
        end
    end
    
end

function AddGlobalStatsForLevel( level ,  place , name , score )

    StatEntry = { LevelId = level, Place = place, Name = name , Score = score }

    if name == User.Name then 
    
        local currHighscore = registry:Get( "/monstaz/hiscore/" .. level .. "/score")
        
        local isNewHighscore = tonumber(score) > currHighscore;
        
        if isNewHighscore then
            registry:Set( "/monstaz/hiscore/" .. level .. "/score", tonumber(score))
            GameEventDispatcher:HandleGameEvent( GameEvent.GEI_GAME_HISCORE_CHANGED, level )
        end
    end
    
    
    for i=1,# SurvivalLevelsInfo do
        if SurvivalLevelsInfo[i].Level ==  StatEntry.LevelId then
            table.insert( SurvivalLevelsInfo[i].StatsGlobal, StatEntry )
            break;
        end
    end
end


function CheckStats( level )
            local played = false
            
            for i=1,# level.StatsGlobal do 
                if level.StatsGlobal[i] then 
                    if ( level.StatsGlobal[i].Name == User.Name ) then
                        played = true
                    end
                end
            end
            
            if played == false then
                StatEntry = { LevelId = level.Level , Place = #level.StatsGlobal+1, Name = User.Name , Score = StatsLocalPlayer[ level.Level ].Score }
                table.insert( level.StatsGlobal, StatEntry )
            end
            
            played = false
            
            for z=1,# level.StatsFriends do 
                if level.StatsFriends[z] then 
                    if ( level.StatsFriends[z].Name == User.Name ) then
                        played = true
                    end
                end
            end
            
            if played == false then
                StatEntry = { LevelId = level.Level , Place = #level.StatsFriends +1, Name = User.Name , Score = StatsLocalPlayer[ level.Level ].Score  }
                table.insert( level.StatsFriends, StatEntry )
            end

end

function ResetStats()

    for i=1,# SurvivalLevelsInfo do
        SurvivalLevelsInfo[i].StatsGlobal = {}
        SurvivalLevelsInfo[i].StatsFriends = {}
    end
end

function ResetStatsFriends( levelID )

    for i=1,# SurvivalLevelsInfo do
        if SurvivalLevelsInfo[i].Level == levelID then
            SurvivalLevelsInfo[i].StatsFriends = {}
        end
    end
end

function ResetStatsGlobal( levelID )

    for i=1,# SurvivalLevelsInfo do
        if SurvivalLevelsInfo[i].Level == levelID then
            SurvivalLevelsInfo[i].StatsGlobal = {}
        end
    end
end

function RefreshLocalStats()
    StatsLocalPlayer = {}

    for i=1,# SurvivalLevelsInfo do
        StatEntryPlayer = { LevelId = SurvivalLevelsInfo[i].Level, Place = 1, Name = "TEXT_YOU" , Score = registry:Get( "/monstaz/hiscore/" .. SurvivalLevelsInfo[i].Level .. "/score") }
        table.insert( StatsLocalPlayer, StatEntryPlayer )
    end	
end

function InitSurvival()
    table.insert( SurvivalLevelsInfo, SurvivalLevelInfoSuburbs )
    table.insert( SurvivalLevelsInfo, SurvivalLevelInfoCityRampage )
    table.insert( SurvivalLevelsInfo, SurvivalLevelInfoDesert )
    table.insert( SurvivalLevelsInfo, SurvivalLevelInfoIce )
    RefreshLocalStats()
    --AddFriendsStatsForLevel( 2, 1, "Chuck", 33889134332234 )
    --AddFriendsStatsForLevel( 2, 2, "Wilson" , 24444332234 )
    --AddFriendsStatsForLevel( 2, 3, "Dumdum217" ,  132234 )
    --AddGlobalStatsForLevel(  2, 1, "Somebody" ,  67544565656 )
    --AddGlobalStatsForLevel(  2, 2, "Anybody" ,  99954456 )
    --AddGlobalStatsForLevel(  2, 3, "Gustaw" ,  565656 )
end

InitSurvival()




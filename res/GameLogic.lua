require( "EntityLevels.lua" )
require( "EntityDB.lua" )
require( "Settings.lua" )
require( "CommonLibrary.lua" )
require( "Spawns.lua" )
require( "Weapons.lua" )
require( "Perks.lua" )

playerShot = false
playerRegeneration = false
playerRage = false
playerSniper = false

AIFriendShot = false

LevelTickPause = false

local TickNum = 0
AIFriend = nil

function PickupWeapon( id )
    SelectWeapon( WeaponPickups[id] )
end

ShakePower = 0

playerBackup = nil
reviveLock = false

-- Note: function is called only during active gameplay (without menu displayed) - do not place here any idle animations, menu stuff, tweens etc.
-- Althrought function (entities behaviours) may work even after player death its update is disabled 2 secs after player death (design request).
function Tick()
    if reviveLock then return end

    TickNum = TickNum + 1

    ShotManager:ElectricityStep()
    GameManager:FillSegmentTable()
    ShotManager:ElectricitySpawn()
    -- Calling order is important here cause below methods change world map segmentation which is used by every following method
    GameManager:ProcessShots( SheathedHitMultiplier )
    if player then
        GameManager:MonstersEatPlayer( WeaponSelected == 26 )   -- chainsaw
        GameManager:MonstersEatFriends()
    end
    GameManager:CalculateShotAvoidance( ShotRepulsionForce )
    GameManager:CalculateAvoidance( RepulsionDistance, RepulsionForce )
    GameManager:CalculateObstacles( ObstacleBorder, ObstacleForce )
    GameManager:ProcessExplosions()

    EntityManager:UpdateBehaviors()

    if player then
        WeaponTick( playerShot )

        if ShakePower > 0 then
            local x, y = player:GetPos()
            x = x - ShakePower + 2 * math.random() * ShakePower
            y = y - ShakePower + 2 * math.random() * ShakePower
            Map:MoveCamera( x, y )
            ShakePower = ShakePower - 1
        else
            Map:MoveCamera( player:GetPos() )
        end

        if playerRegeneration then
            player:SetHitPoints( math.min( player:GetHitPoints() + 0.01, player:GetMaxHitPoints() ) )
        end
        if player:GetHitPoints() <= 0 then
--            GameOver()
            StopFiring()
            reviveLock = true
            AudioManager:Play( Sfx.SFX_PLAYER_DEATH )
            registry:Set( "/internal/revivecat", false )
            GameManager:StartRevive()
        end
    end

    if AIFriend then
        WeaponTickAI( AIFriendShot, AIFriend )
    end

    EntityManager:GrimReaper()

    if not LevelTickPause then
        LevelTick()
    end

    EntityManager:SpawnTickFinished()
end

function GameOver()
    StopFiring()
    GameManager:SummaryScreen( true, TickNum )
    playerBackup = player
    player = nil
    LevelTick = function() end
end

function GameWon()
    PickupManager:CollectCash()
    StopFiring()
    GameManager:SummaryScreen( false, TickNum )
    playerBackup = player
    player = nil
    LevelTick = function() end
    AudioManager:MusicVolumeOverride( 2 )
    if math.random() < 0.5 then
        AudioManager:Play( Sfx.SFX_VO_ANOTHER_VICTORY )
    else
        AudioManager:Play( Sfx.SFX_VO_TOTAL_DOMINATION )
    end

    local level = registry:Get( "/internal/storylevel" )
    if level ~= 0 then
        local nextlevel = level + 1
        if registry:Get( "/monstaz/lock/" .. nextlevel ) then
            GameManager:Analytics( "Story " .. nextlevel .. " unlocked" )
            registry:Set( "/monstaz/lock/" .. nextlevel, false )
        end
    end
end

function ReportPoints( points, fail )
    local level = registry:Get( "/internal/storylevel" )
    if level == 0 then return end

    local stars = 0
    for i=3,1,-1 do
        if points > StarPoints[i] then
            stars = i
            break
        end
    end

    local oldstars = 0
    if not fail then
        local path = "/monstaz/stars/" .. level
        oldstars = registry:Get( path )
        if stars > oldstars then
            registry:Set( path, stars )
        end
    end

    registry:Set( "/internal/stars", stars )
    registry:Set( "/internal/oldstars", oldstars )
end

function Shot( val )
    if not val and playerShot then
        local p = player or playerBackup
        local sfx = Weapons[WeaponSelected].SfxSpindown
        if sfx and not Reload then
            AudioManager:Play( sfx )
        end
        if Weapons[WeaponSelected] and Weapons[WeaponSelected].SfxLoop and SfxLoop[p] and SfxLoop[p][Weapons[WeaponSelected].SfxShot] then
            AudioManager:StopLooped( SfxLoop[p][Weapons[WeaponSelected].SfxShot] )
            SfxLoop[p][Weapons[WeaponSelected].SfxShot] = nil
        end
    end

    if playerShot ~= val then
        playerShot = val
        GameEventDispatcher:HandleGameEvent( playerShot and GameEvent.GEI_SHOT_PLAYER_START or GameEvent.GEI_SHOT_PLAYER_STOP, WeaponSelected )
    end
end

function AIShot( val )
    if not val and AIFriendShot then
        local ex, ey = AIFriend:GetPos()
        local sfx = Weapons[WeaponSelectedAI].SfxSpindown
        if sfx and not ReloadAI then
            AudioManager:Play( sfx, ex, ey )
        end
        if Weapons[WeaponSelectedAI].SfxLoop and SfxLoop[AIFriend][Weapons[WeaponSelectedAI].SfxShot] then
            AudioManager:StopLooped( SfxLoop[AIFriend][Weapons[WeaponSelectedAI].SfxShot] )
            SfxLoop[AIFriend][Weapons[WeaponSelectedAI].SfxShot] = nil
        end
    end

    AIFriendShot = val
end

function SetMapSize( w, h )
    MapWidth = w
    MapHeight = h
end

function SetWeapon( weapon )
    SelectWeapon( weapon )
end

function WeaponBoost( val )
    weaponboost = val
end

StarPoints = { 5000, 50000, 500000 }

function SetStars( one, two, three )
    StarPoints = { one, two, three }
end

function PauseLevelTick( pause )
    LevelTickPause = pause
end

function CheckRage()
    return player and playerRage and player:GetHitPoints() < player:GetMaxHitPoints() * 0.15
end

function AISetWeapon()
    local weaponShopItemId = registry:Get( "/internal/friendWeapon" )
    local upgradeLevel = registry:Get( "/internal/friendWeaponLevel" )

    AIWeaponSelect( WeaponsAIIdxById[weaponShopItemId], upgradeLevel )
end

require( "LevelDB.lua" )

local EnemySets = {
    { m = { EntityType.HoundSimple, EntityType.FishThrowing, EntityType.FloaterSimple }, e = { 1, 2, 3 } },
    { m = { EntityType.OctopusShotAware, EntityType.KillerWhale, EntityType.SqueezerTurning }, e = { 2, 1, 3 } },
    { m = { EntityType.SectoidShooting, EntityType.Nerval, EntityType.OctopusSimple }, e = { 2, 1, 3 } },
    { m = { EntityType.SectoidSimple, EntityType.Crab , EntityType.KillerWhaleSimple }, e = { 1, 3, 2 } },
    { m = { EntityType.HoundSimple, EntityType.NautilTurning, EntityType.SectoidShooting }, e = { 1, 2, 3 } },
    { m = { EntityType.KillerWhaleSimple, EntityType.FloaterSimple, EntityType.OctopusChaser }, e = { 3, 1, 2 } },
    { m = { EntityType.Lobster, EntityType.Crab, EntityType.SqueezerSimple }, e = { 3, 2, 1 } },
    { m = { EntityType.NautilSimple, EntityType.FishSimple, EntityType.FloaterSimple }, e = { 2, 1, 3 } },
    { m = { EntityType.HoundShooting, EntityType.NautilTurning, EntityType.Nerval }, e = { 1, 3, 2 } }
}

local Areas = {}
Areas[1] = { missions = 0 }
Areas[2] = { missions = 0 }
Areas[3] = { missions = 0 }
Areas[4] = { missions = 4, group = LevelGroup.City, anchor = TutorialAnchor.Right }
Areas[5] = { missions = 3, group = LevelGroup.Suburbs, anchor = TutorialAnchor.Left }
Areas[6] = { missions = 4, group = LevelGroup.Desert, anchor = TutorialAnchor.Right }
Areas[7] = { missions = 3, group = LevelGroup.City, anchor = TutorialAnchor.Right }
Areas[8] = { missions = 5, group = LevelGroup.Ice, anchor = TutorialAnchor.Bottom }
Areas[9] = { missions = 4, group = LevelGroup.Ice, anchor = TutorialAnchor.Left }
Areas[10] = { missions = 3, group = LevelGroup.Suburbs, anchor = TutorialAnchor.Left }
Areas[11] = { missions = 4, group = LevelGroup.Suburbs, anchor = TutorialAnchor.Right }
Areas[12] = { missions = 3, group = LevelGroup.City, anchor = TutorialAnchor.Left }
Areas[13] = { missions = 5, group = LevelGroup.Desert, anchor = TutorialAnchor.Right }
Areas[14] = { missions = 3, group = LevelGroup.City, anchor = TutorialAnchor.Left }
Areas[15] = { missions = 3, group = LevelGroup.Desert, anchor = TutorialAnchor.Left }
Areas[16] = { missions = 4, group = LevelGroup.City, anchor = TutorialAnchor.Right }
Areas["boss"] = { group = LevelGroup.Boss }

function EndlessGetTooltipAnchor( area )
    local a = Areas[area]
    if not a then return nil end

    return a.anchor
end

function EndlessGetMissionsCount( area )
    local a = Areas[area]
    if not a then return nil end

    return a.missions
end

function EndlessGetMission( map, area )
    local a = Areas[area]
    if not a then return nil end

    local monsters = {}
    local elements = {}

    local group = a.group
    local level = registry:Get( "/maps/" .. map .. "/" .. area .. "/level" )
    local friendlock = false
    if not level or level == -1 then
        level = EndlessGetRandomLevel( group )
        registry:Set( "/maps/" .. map .. "/" .. area .. "/level", level )

        local data = Levels[group][level]
        if data.m then
            monsters = data.m
            local element = math.random( 3 )
            for i = 1, #monsters do
                elements[i] = element
            end
        else
            monsters, elements = EndlessGetRandomMonsters()
        end

        local fl = registry:Get( "/app-config/friendlock/enabled" )
        local flm = registry:Get( "/app-config/friendlock/endless" )
        local flc = registry:Get( "/monstaz/endlessfriendlock" )

        if fl then
            flc = flc + 1
            if flc >= flm then
                flc = 0
                friendlock = true
            end
            registry:Set( "/monstaz/endlessfriendlock", flc )
        end

        registry:Set( "/maps/" .. map .. "/" .. area .. "/friendlock", friendlock )

        for i, v in ipairs( monsters ) do
            registry:Set( "/maps/" .. map .. "/" .. area .. "/monsters/" .. i, v )
            registry:Set( "/maps/" .. map .. "/" .. area .. "/elements/" .. i, elements[i] or 0 )
        end
    else
        friendlock = registry:Get( "/maps/" .. map .. "/" .. area .. "/friendlock" )
        local i = 1
        while true do
            local monster = registry:Get( "/maps/" .. map .. "/" .. area .. "/monsters/" .. i )
            local element = registry:Get( "/maps/" .. map .. "/" .. area .. "/elements/" .. i ) or 0
            if not monster then break end

            table.insert( monsters, monster )
            table.insert( elements, element )

            i = i + 1
        end
    end

    local reward = EndlessGetReward( map )
    local mission = {
        l = Levels[group][level],
        m = monsters,
        e = elements,
        r = reward,
        fl = friendlock
    }

    return mission
end

function EndlessGetRandomLevel( group )
    -- Prepare pool of available levels
    local levels = Levels[group]
    local pool = {}
    for i = 1, #levels do
        local locked = registry:Get( "/levels/" .. group .. "/" .. i )
        if not locked then
            table.insert( pool, i )
        end
    end

    -- Reset pool
    if #pool == 0 then
        for i = 1, #levels do
            registry:Set( "/levels/" .. group .. "/" .. i, false )
            table.insert( pool, i )
        end
    end

    local level = pool[math.random( #pool )]
    registry:Set( "/levels/" .. group .. "/" .. level, true )

    return level
end

function EndlessGetRandomMonsters()
    local set = EnemySets[math.random( #EnemySets )]
    return set.m, set.e
end

function EndlessGetReward( map )
    local missions = 0
    local completed = 0

    for i, v in ipairs( Areas ) do
        missions = missions + v.missions
        completed = completed + registry:Get( "/maps/" .. map .. "/" .. i .. "/completed" ) or 0
    end

    local reward = {
        s = registry:Get( "/app-config/endless/reward/value/soft" ) or 0,
        h = registry:Get( "/app-config/endless/reward/value/hard" ) or 0,
        xp = registry:Get( "/app-config/endless/reward/value/xp" ) or 0
    }

    local progress = completed/missions
    reward.s = math.floor( reward.s * ( progress + registry:Get( "/app-config/endless/reward/modifier/soft" ) or 1 ) )
    reward.h = math.floor( reward.s * ( progress + registry:Get( "/app-config/endless/reward/modifier/hard" ) or 1 ) )

    return reward
end

function EndlessGetBossProgress( map, offset )
    local progress = registry:Get( "/maps/" .. map .. "/boss/progress" )
    if offset then
        progress = progress + offset
    end

    local required = registry:Get( "/app-config/endless/boss-required-missions" )
    if progress > required then
        progress = required
    elseif progress < 0 then
        progress = 0
    end

    return progress/required
end

function EndlessIsBossActive( map )
    local required = registry:Get( "/app-config/endless/boss-required-missions" )
    local progress = registry:Get( "/maps/" .. map .. "/boss/progress" )
    return progress >= required
end

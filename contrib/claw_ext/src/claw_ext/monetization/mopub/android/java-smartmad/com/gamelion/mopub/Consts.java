package com.gamelion.mopub;

/**
 * This class holds global constants that are used throughout the application
 * to support mopub
 */
public class Consts
{
    public static final boolean DEBUG        = false;
    public static final String  TAG          = "Mopub";
    public static int SMART_MAD_REFRESH_TIME = 200;
}

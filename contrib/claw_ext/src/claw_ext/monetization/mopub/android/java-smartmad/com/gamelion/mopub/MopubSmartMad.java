package com.gamelion.mopub;

import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;

import com.mopub.mobileads.MoPubView;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.DisplayMetrics;

import com.gamelion.mopub.Consts;
import com.Claw.Android.ClawActivityCommon;

public class MopubSmartMad implements AdListener
{
    private AdView mAdView;
    private String mAppId;
    private String mSpaceId;
    private MoPubView mMopubView;
    private Mopub mMopub;

    public MopubSmartMad(Mopub mopub, String appId, String spaceId)
    {
        mAppId = appId;
        mSpaceId = spaceId;
        mMopub = mopub;
        
        AdManager.setApplicationId(ClawActivityCommon.mActivity, mAppId);
    }

    public void loadSMSDK(final MoPubView view)
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "loadSMSDK()");
        
        mMopubView = view;
        final MopubSmartMad instance = this;
                
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            mAdView = new AdView(ClawActivityCommon.mActivity, null, 0, mSpaceId, Consts.SMART_MAD_REFRESH_TIME, AdView.PHONE_AD_MEASURE_AUTO, AdView.BANNER_ANIMATION_TYPE_NONE, Consts.DEBUG);
            mAdView.setListener(instance);
        }});
    }

    @Override
    public void onAdEvent(final AdView arg0, final int arg1) 
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (arg1 == AdView.EVENT_NEWAD) {
                if (Consts.DEBUG) Log.i(Consts.TAG, "SmartmMad ad loaded");
                mMopubView.customEventDidLoadAd();
                mMopubView.setAdContentView(mAdView);
                mMopub.OnAdLoaded(mMopubView);
            } else if (arg1 == AdView.EVENT_INVALIDAD) {
                if (Consts.DEBUG) Log.i(Consts.TAG, "SmartmMad ad failed to load");
                mMopubView.customEventDidFailToLoadAd();
            } else if (arg1 == AdView.EVENT_INCOMPLETE_PERMISSION) {
                if (Consts.DEBUG) Log.e(Consts.TAG, "SmartmMad missing permissions");
            }
        }});
    }

    @Override
    public void onAdFullScreenStatus(boolean arg0) 
    {
        mAdView.skipCurrentFullscreenAd();
    }
}
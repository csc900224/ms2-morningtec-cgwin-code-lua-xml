//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/android/AndroidMopub.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__ANDROIDMOPUB_HPP__
#define __INCLIDED__ANDROIDMOPUB_HPP__

#include "claw_ext/monetization/mopub/Mopub.hpp"

namespace ClawExt
{
    class AndroidMopub : public Mopub
    {
    public:
        virtual      ~AndroidMopub();
        virtual void Initialize( const AdUnitDefinition& definition, StringMap* optParams = NULL );
        virtual void ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams = NULL );
        virtual void RequestBanner();
        virtual void SetBannerVisibility( bool visible );
        virtual void SetHorizontalAlignment( bool left );
        virtual void HideBanner();

    private:
        void SetAdUnit( const AdUnitDefinition& definition, StringMap* optParams, bool initialize );
    };
} // namespace ClawExt

#endif // __INCLIDED__ANDROIDMOPUB_HPP__

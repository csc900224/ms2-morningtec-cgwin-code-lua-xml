package com.gamelion.mopub;

import android.util.Log;
import android.view.View;
import android.view.Display;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.view.animation.ScaleAnimation;
import android.util.DisplayMetrics;

import com.mopub.mobileads.MoPubView;

import com.gamelion.mopub.Consts;
import com.Claw.Android.ClawActivityCommon;

public class Mopub implements MoPubView.OnAdLoadedListener
{
    private static Mopub    sInstance           = null;
    private MoPubView       mAdView             = null;
    private String          mAdUnit             = null;
    private RelativeLayout  mBannerRoot         = null;
    private int             mAdWidth            = -1;
    private int             mAdHeight           = -1;
    private boolean         mLayoutLeft         = false;
    private boolean         mRequestedLayoutL   = true;
    
    public static Mopub getInstance()
    {
        if (sInstance == null)
        {
            sInstance = new Mopub();
            if (Consts.DEBUG) Log.i(Consts.TAG, "create instance");
        }
        return sInstance;
    }
    
    public static void Initialize(String adUnitID, int adWidth, int adHeight, String smartMadAppId, String smartMadSpaceId)
    {
        getInstance().InitializeImpl( adUnitID, adWidth, adHeight, smartMadAppId, smartMadSpaceId );
    }
    
    private void InitializeImpl(final String adUnitID, final int adWidth, final int adHeight, final String smartMadAppId, final String smartMadSpaceId)
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (Consts.DEBUG) Log.i(Consts.TAG, "InitalizeImpl with adUnitID: " + adUnitID);
            
            DisplayMetrics metrics = new DisplayMetrics();
            ClawActivityCommon.mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            mAdUnit             = adUnitID;
            mAdWidth            = (int)(adWidth * metrics.density + 0.5);
            mAdHeight           = (int)(adHeight * metrics.density + 0.5);

            // Configure banner root 
            mBannerRoot = new RelativeLayout(ClawActivityCommon.mActivity);
            SetLayout( true );

            ClawActivityCommon.mLayout.addView(mBannerRoot);
        }});
    }
    
    public static void ChangeAdUnit(String adUnitID, int adWidth, int adHeight, String smartMadAppId, String smartMadSpaceId)
    {
        getInstance().ChangeAdUnitImpl( adUnitID, adWidth, adHeight, smartMadAppId, smartMadSpaceId );
    }
    
    private void ChangeAdUnitImpl(final String adUnitID, final int adWidth, final int adHeight, final String smartMadAppId, final String smartMadSpaceId)
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (Consts.DEBUG) Log.i(Consts.TAG, "ChangeAdUnitImpl with adUnitID: " + adUnitID);
            
            DisplayMetrics metrics = new DisplayMetrics();
            ClawActivityCommon.mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            mAdUnit             = adUnitID;
            mAdWidth            = (int)(adWidth * metrics.density + 0.5);
            mAdHeight           = (int)(adHeight * metrics.density + 0.5);
        }});
    }
    
    public static void RequestBanner()
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "RequestBanner");
        getInstance().RequestBannerImpl();
    }
    
    private void RequestBannerImpl()
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (Consts.DEBUG) Log.i(Consts.TAG, "RequestBannerImpl");
                
            HideBannerImpl();
            
            mAdView = new MoPubView(ClawActivityCommon.mActivity);
            mAdView.setAdUnitId(mAdUnit);
            mAdView.loadAd();
            mAdView.setOnAdLoadedListener(getInstance());
            
            mBannerRoot.addView( mAdView );
        }});
    }
    
    @Override
    public void OnAdLoaded(MoPubView m)
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "OnAdLoaded - width: " + m.getAdWidth());
        if (Consts.DEBUG) Log.i(Consts.TAG, "OnAdLoaded - height: " + m.getAdHeight());
    }

    public static void SetBannerVisibility( final boolean visible )
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "SetBannerVisibility(): " + visible);
        getInstance().SetBannerVisibilityImpl( visible );
    }
    
    private void SetBannerVisibilityImpl( final boolean visible )
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (Consts.DEBUG) Log.i(Consts.TAG, "SetBannerVisibilityImpl(): " + visible);
            SetLayout( mRequestedLayoutL );
            mBannerRoot.setVisibility( visible ? View.VISIBLE : View.GONE );
            if (mAdView != null) {
                mAdView.setAutorefreshEnabled( visible );
            }
        }});
    }
    
    public static void HideBanner()
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (Consts.DEBUG) Log.i(Consts.TAG, "HideBanner");
            getInstance().HideBannerImpl();
        }});
    }
    
    private void HideBannerImpl()
    {     
        if (mAdView != null) {
            mBannerRoot.removeView(mAdView);
            mAdView.destroy();
            mAdView = null;
        }
    }
        
    public static void Destroy()
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            getInstance().HideBannerImpl();
            ClawActivityCommon.mLayout.removeView( getInstance().mBannerRoot );
        }});
    }

    public static void SetHorizontalAlignment( final boolean left )
    {
        getInstance().mRequestedLayoutL = left;
    }

    private void SetLayout( final boolean left )
    {
        if( left != mLayoutLeft )
        {
            if( left )
            {
                mBannerRoot.setGravity(Gravity.LEFT | Gravity.BOTTOM);
            }
            else
            {
                mBannerRoot.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
            }

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mAdWidth, mAdHeight);

            int screenHeight = ClawActivityCommon.mLayout.getHeight();
            int screenWidth = ClawActivityCommon.mLayout.getWidth();

            if( screenWidth < screenHeight )
            {
                int tmp = screenHeight;
                screenHeight = screenWidth;
                screenWidth = tmp;
            }

            params.topMargin = screenHeight - mAdHeight;
            if( !left )
            {
                params.leftMargin = screenWidth - mAdWidth;
            }
            mBannerRoot.setLayoutParams(params);
            mBannerRoot.requestLayout();
            // req layout maybe

            if (Consts.DEBUG) Log.i(Consts.TAG, "params.topMargin: " + params.topMargin);
            if (Consts.DEBUG) Log.i(Consts.TAG, "mAdWidth: " + mAdWidth);
            if (Consts.DEBUG) Log.i(Consts.TAG, "mAdHeight: " + mAdHeight);
            if (Consts.DEBUG) Log.i(Consts.TAG, "screenHeight: " + screenHeight);
            mLayoutLeft = left;
        }
    }

    public int getAdWidth() 
    {
        return mAdWidth;
    }
    
    public int getAdHeight()
    {
        return mAdHeight;
    }
}

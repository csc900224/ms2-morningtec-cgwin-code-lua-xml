//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/android/AndroidMopub.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// External includes
#include "claw/base/Errors.hpp"
#include "claw/system/android/JniAttach.hpp"

// Internal includes
#include "claw_ext/monetization/mopub/android/AndroidMopub.hpp"

namespace ClawExt
{
    static AndroidMopub* s_instance = NULL;

    /*static*/ Mopub* Mopub::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidMopub;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Mopub::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    AndroidMopub::~AndroidMopub()
    {
        CLAW_MSG("AndroidMopub::~AndroidMopub()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/mopub/Mopub", "Destroy", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidMopub::ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams )
    {
        CLAW_MSG("AndroidMopub::ChangeAdUnit()");
        SetAdUnit( definition, optParams, false );
    }

    void AndroidMopub::Initialize( const AdUnitDefinition& definition, StringMap* optParams )
    {
        CLAW_MSG("AndroidMopub::Initialize()");
        SetAdUnit( definition, optParams, true );
    }

    void AndroidMopub::SetAdUnit( const AdUnitDefinition& definition, StringMap* optParams, bool initialize )
    {
        CLAW_MSG("AndroidMopub::SetAdUnit()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring id, smartMadAppId, smartMadSpaceId;
        if( optParams )
        {
            StringMap::iterator appId   = optParams->find("app_id");
            StringMap::iterator spaceId = optParams->find("space_id");
            CLAW_ASSERT( appId != optParams->end() && spaceId != optParams->end() );
            if( appId != optParams->end() && spaceId != optParams->end() )
            {
                smartMadAppId   = Claw::JniAttach::GetStringFromChars( env, appId->second.c_str() );
                smartMadSpaceId = Claw::JniAttach::GetStringFromChars( env, spaceId->second.c_str() );
            }
        }
        else
        {
            smartMadAppId = NULL;
            smartMadSpaceId = NULL;
        }

        CLAW_ASSERT( definition.adUnitID );
        if( definition.adUnitID )
        {
            id = Claw::JniAttach::GetStringFromChars( env, definition.adUnitID );
            Claw::JniAttach::StaticVoidMethodCall( 
                env, 
                "com/gamelion/mopub/Mopub",
                initialize ? "Initialize" : "ChangeAdUnit",
                "(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V",
                id,
                definition.adWidth,
                definition.adHeight,
                smartMadAppId,
                smartMadSpaceId
            );
            Claw::JniAttach::ReleaseString( env, id );
        }

        Claw::JniAttach::Detach( attached );
    }

    void AndroidMopub::RequestBanner()
    {
        CLAW_MSG("AndroidMopub::RequestBanner()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/mopub/Mopub", "RequestBanner", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidMopub::SetHorizontalAlignment( bool left )
    {
        CLAW_MSG("AndroidMopub::SetHorizontalAlignment()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/mopub/Mopub", "SetHorizontalAlignment", "(Z)V", left );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidMopub::SetBannerVisibility( bool visible )
    {
        CLAW_MSG( "AndroidMopub::SetBannerVisibility(): " << visible );
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/mopub/Mopub", "SetBannerVisibility", "(Z)V", visible );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidMopub::HideBanner()
    {
        CLAW_MSG("AndroidMopub::HideBanner()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/mopub/Mopub", "HideBanner", "()V" );
        Claw::JniAttach::Detach( attached );
    }
} // namespace ClawExt

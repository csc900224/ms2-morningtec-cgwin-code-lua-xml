//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/Mopub.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MOPUB_HPP__
#define __INCLUDED__MOPUB_HPP__

#include <stdlib.h>
#include <map>

namespace ClawExt
{
    class Mopub
    {
    public:
        struct AdUnitDefinition
        {
            AdUnitDefinition()
                : adUnitID( NULL )
                , adWidth( 0 )
                , adHeight( 0 )
            {}
        
            AdUnitDefinition( const char* adUnitID, int adWidth, int adHeight )
                : adUnitID( adUnitID )
                , adWidth( adWidth )
                , adHeight( adHeight )
            {}

            const char* adUnitID; 
            int         adWidth;
            int         adHeight;
        };

        typedef std::map<std::string, std::string> StringMap;

        virtual      ~Mopub() {}

        //! Initialize mopub instance with initial aduint definition.
        virtual void Initialize( const AdUnitDefinition& definition, StringMap* optParams = NULL ) = 0;

        //! Cahnge currently used ad unit configuration.
        virtual void ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams = NULL ) = 0;

        //! Request banner form mopub - it can take a while
        virtual void RequestBanner() = 0;

        //! Lightweight visibility change (does not release resources)
        virtual void SetBannerVisibility( bool visible ) = 0;

        //! Set horizontal banner position alignment ( default left if not called )
        virtual void SetHorizontalAlignment( bool left ) = 0;

        //! Fully hide (release) banner
        virtual void HideBanner() = 0;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Heyzap object implementation.
        */
        static Mopub* QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void   Release();
    };
} // namespace ClawExt

#endif // __INCLUDED__MOPUB_HPP__

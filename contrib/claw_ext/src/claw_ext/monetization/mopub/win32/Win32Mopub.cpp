//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/win32/Win32Mopub.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// External includes
#include "claw/base/Errors.hpp"

// Internal includes
#include "claw_ext/monetization/mopub/win32/Win32Mopub.hpp"

namespace ClawExt
{
    static Win32Mopub* s_instance = NULL;

    /*static*/ Mopub* Mopub::QueryInterface()
    {
        if( !s_instance )
            s_instance = new Win32Mopub;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Mopub::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void Win32Mopub::Initialize( const AdUnitDefinition& definition, StringMap* optParams )
    {
        CLAW_MSG( "Win32Mopub::Initialize() adUnitID: " << definition.adUnitID << " widht: " << definition.adWidth << " height: " << definition.adHeight );
    }

    void Win32Mopub::ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams )
    {
        CLAW_MSG( "Win32Mopub::ChangeAdUnit() adUnitID: " << definition.adUnitID << " widht: " << definition.adWidth << " height: " << definition.adHeight );
    }

    void Win32Mopub::RequestBanner()
    {
        CLAW_MSG( "Win32Mopub::RequestBanner()" );
    }

    void Win32Mopub::SetBannerVisibility( bool visible )
    {
        CLAW_MSG( "Win32Mopub::SetBannerVisibility(): " << visible );
    }

    void Win32Mopub::HideBanner()
    {
        CLAW_MSG( "Win32Mopub::HideBanner()" );
    }

    void Win32Mopub::SetHorizontalAlignment( bool left )
    {
        CLAW_MSG( "Win32Mopub::SetHorizontalAlignment()" << left );
    }
} // namespace ClawExt

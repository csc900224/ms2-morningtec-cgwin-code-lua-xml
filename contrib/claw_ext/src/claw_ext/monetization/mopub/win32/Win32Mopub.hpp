//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/win32/Win32Mopub.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__WIN32MOPUB_HPP__
#define __INCLIDED__WIN32MOPUB_HPP__

#include "claw_ext/monetization/mopub/Mopub.hpp"

namespace ClawExt
{
    class Win32Mopub : public Mopub
    {
    public:
        virtual void Initialize( const AdUnitDefinition& definition, StringMap* optParams = NULL );
        virtual void ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams = NULL );
        virtual void RequestBanner();
        virtual void SetBannerVisibility( bool visible );
        virtual void HideBanner();
        virtual void SetHorizontalAlignment( bool left );
    };
} // namespace ClawExt

#endif // __INCLIDED__WIN32MOPUB_HPP__

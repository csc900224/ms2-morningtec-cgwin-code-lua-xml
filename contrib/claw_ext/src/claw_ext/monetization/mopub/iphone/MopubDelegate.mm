//
//  MopubDelegate.mm
//  MonstazAI
//
//  Created by user on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "claw_ext/monetization/mopub/iphone/MopubDelegate.h"
#import "claw/application/iphone/IPhoneAppDelegate.h"
#import "claw/sound/mixer/Mixer.hpp"
#import "claw/application/AbstractApp.hpp"

@implementation MopubDelegate

- (void)adViewDidLoadAd:(MPAdView *)view
{
    CLAW_MSG("MopubDelegate::adViewDidLoadAd()");

    m_view = view;

    [self SetHorizontalAlignment:m_alignmentLeft];

    // Force portrait mode
    [view rotateToOrientation:UIInterfaceOrientationPortrait];
}

- (id) init
{
    if( self == [super init] )
    {
        m_alignmentLeft = YES;
    }
    return self;
}

- (void) SetHorizontalAlignment:(BOOL)left
{
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* viewController = [appDelegate getViewController];

    CGSize size = m_view.frame.size;
    CGRect newFrame = m_view.frame;

    newFrame.size = size;
    newFrame.origin.y = 0;
    newFrame.origin.x = 0;
    m_view.frame = newFrame;

    newFrame.origin.y = viewController.view.bounds.size.height - size.height;
    if( !left )
    {
        newFrame.origin.x = viewController.view.bounds.size.width - size.width;
    }

    m_view.superview.frame = newFrame;
    m_alignmentLeft = left;
}

#pragma mark MPAdViewDelegate Methods

// Implement MPAdViewDelegate's required method, -viewControllerForPresentingModalView. 
- (UIViewController *)viewControllerForPresentingModalView 
{
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    return [appDelegate getViewController];
}

- (void)willPresentModalViewForAd:(MPAdView *)view
{
    Claw::Mixer::Get()->Pause(true);
    Claw::AbstractApp::GetInstance()->OnFocusChange(false);
}

- (void)didDismissModalViewForAd:(MPAdView *)view
{
    Claw::Mixer::Get()->Pause(false);
    Claw::AbstractApp::GetInstance()->OnFocusChange(true);
}

@end

//
//  MopubDelegate.h
//  MonstazAI
//
//  Created by Jacek Nijaki on 6/15/12.
//  Copyright (c) 2012 Gamelion. All rights reserved.
//

#ifndef MonstazAI_MopubDelegate_h
#define MonstazAI_MopubDelegate_h

#import "MPAdView.h"

@interface MopubDelegate : NSObject <MPAdViewDelegate> 
{
    MPAdView* m_view;
    BOOL m_alignmentLeft;
}

-(id) init;
-(void) SetHorizontalAlignment: (BOOL) left;

@end

#endif

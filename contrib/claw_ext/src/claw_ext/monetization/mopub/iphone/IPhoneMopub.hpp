//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/iphone/IPhoneMopub.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__IPHONEMOPUB_HPP__
#define __INCLIDED__IPHONEMOPUB_HPP__

#include "claw_ext/monetization/mopub/Mopub.hpp"

@class MPAdView;
@class MopubDelegate;
@class UIView;

namespace ClawExt
{
    class IPhoneMopub : public Mopub
    {
    public:
                        IPhoneMopub();
        virtual         ~IPhoneMopub();
    
        virtual void    Initialize( const AdUnitDefinition& definition, StringMap* optParams = NULL );
        virtual void    ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams = NULL );
        virtual void    RequestBanner();
        virtual void    SetBannerVisibility( bool visible );
        virtual void    HideBanner();

        void SetHorizontalAlignment( bool left );

    private:
        MPAdView*           m_adView;
        MopubDelegate*      m_delegate;
        bool                m_visible;
        UIView*             m_bannerRoot;
        AdUnitDefinition    m_definition;
    };
} // namespace ClawExt

#endif // __INCLIDED__IPHONEMOPUB_HPP__

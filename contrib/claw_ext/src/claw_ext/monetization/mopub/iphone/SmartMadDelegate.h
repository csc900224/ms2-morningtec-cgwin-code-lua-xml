//
//  SmardMadDelegate.h
//  MonstazAI
//
//  Created by Jacek Nijaki on 6/15/12.
//  Copyright (c) 2012 Gamelion. All rights reserved.
//

#import "MopubDelegate.h"
#import "SmartMadAdView.h"

@interface SmartMadDelegate : MopubDelegate<SmartMadAdViewDelegate, SmartMadAdEventDelegate>
{
    NSString* m_appId;
    NSString* m_spaceId;
}
@property (nonatomic,retain)SmartMadAdView* smartMadAdView;
@property (nonatomic,retain) MPAdView *mpAdView;

-(id)initWithAppId:(NSString*)appId spaceId:(NSString*)spaceId;

@end

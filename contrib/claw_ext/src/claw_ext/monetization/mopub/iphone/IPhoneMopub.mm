//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/mopub/iphone/IPhoneMopub.mm
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#define USE_SMARTMAD    0

// External includes
#include "claw/base/Errors.hpp"

#import "claw/application/iphone/IPhoneAppDelegate.h"
#import "MPAdView.h"

// Internal includes
#include "claw_ext/monetization/mopub/iphone/IPhoneMopub.hpp"

#if USE_SMARTMAD
#include "claw_ext/monetization/mopub/iphone/SmartMadDelegate.h"
#else
#include "claw_ext/monetization/mopub/iphone/MopubDelegate.h"
#endif

namespace ClawExt
{
    static IPhoneMopub* s_instance = NULL;

    /*static*/ Mopub* Mopub::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneMopub;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Mopub::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    IPhoneMopub::IPhoneMopub()
        : m_adView(NULL)
        , m_visible(true)
    {}

    IPhoneMopub::~IPhoneMopub()
    {
        CLAW_MSG("IPhoneMopub::~IPhoneMopub()");
    
        HideBanner();
        [m_delegate release];
        [m_bannerRoot release];
    }

    void IPhoneMopub::Initialize( const AdUnitDefinition& definition, StringMap* optParams )
    {
        CLAW_MSG("IPhoneMopub::Initialize()");
        CLAW_ASSERT(definition.adUnitID);
        
#if USE_SMARTMAD
        CLAW_ASSERT(optParams);
    
        StringMap::iterator appid = optParams->find("app_id");
        StringMap::iterator space = optParams->find("space_id");
        CLAW_ASSERT( appid != optParams->end() && space != optParams->end() );

        NSString* appIdStr   = [NSString stringWithUTF8String:appid->second.c_str()];
        NSString* spaceIdStr = [NSString stringWithUTF8String:space->second.c_str()];
        
        m_delegate = [[SmartMadDelegate alloc] initWithAppId:appIdStr spaceId:spaceIdStr];
#else // USE_SMARTMAD
        m_delegate = [[MopubDelegate alloc] init];
#endif // USE_SMARTMAD
        
        m_definition = definition;
        
        m_bannerRoot = [[UIView alloc]init];
        IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
        IPhoneViewController* viewController = [appDelegate getViewController];
        [viewController.view addSubview:m_bannerRoot];
    }

    void IPhoneMopub::ChangeAdUnit( const AdUnitDefinition& definition, StringMap* optParams )
    {
        CLAW_MSG("IPhoneMopub::ChangeAdUnit()");
        CLAW_ASSERT(definition.adUnitID);

#if USE_SMARTMAD
        CLAW_ASSERT(optParams);
    
        StringMap::iterator appid = optParams->find("app_id");
        StringMap::iterator space = optParams->find("space_id");
        CLAW_ASSERT( appid != optParams->end() && space != optParams->end() );

        NSString* appIdStr   = [NSString stringWithUTF8String:appid->second.c_str()];
        NSString* spaceIdStr = [NSString stringWithUTF8String:space->second.c_str()];

        if( m_delegate )
        {
            [m_delegate release];
            m_delegate = NULL;
        }
        m_delegate = [[SmartMadDelegate alloc] initWithAppId:appIdStr spaceId:spaceIdStr];
#endif // USE_SMARTMAD

        m_definition = definition;
        if( m_adView )
        {
            HideBanner();
            [m_adView release];
            m_adView = nil;
        }
    }

    void IPhoneMopub::RequestBanner()
    {
        CLAW_MSG("IPhoneMopub::RequestBanner()");

        HideBanner();

        if( !m_adView )
        {
            m_adView = [[MPAdView alloc] initWithAdUnitId:[NSString stringWithUTF8String:m_definition.adUnitID]
                                                     size:CGSizeMake(m_definition.adWidth, m_definition.adHeight)];
        }
           
        m_adView.ignoresAutorefresh = false;
        m_adView.delegate = m_delegate;
        [m_adView loadAd];

        [m_bannerRoot addSubview:m_adView];
    }

    void IPhoneMopub::SetBannerVisibility( bool visible )
    {
        m_bannerRoot.hidden = !visible;
        if( m_adView )
        {
            m_adView.ignoresAutorefresh = !visible;
            if( visible ) 
            {
                [m_adView forceRefreshAd];
            }
        }
    }

    void IPhoneMopub::HideBanner()
    {
        CLAW_MSG("IPhoneMopub::HideBanner()");
    
        if( m_adView )
        {
            if( m_adView.superview != nil ) 
            {
                [m_adView removeFromSuperview];
            }
#if USE_SMARTMAD
            ((SmartMadDelegate*)m_delegate).mpAdView = NULL;
#endif            
        }
    }

    void IPhoneMopub::SetHorizontalAlignment( bool left )
    {
        BOOL l = left?YES:NO;
        [m_delegate SetHorizontalAlignment: l];
    }

} // namespace ClawExt

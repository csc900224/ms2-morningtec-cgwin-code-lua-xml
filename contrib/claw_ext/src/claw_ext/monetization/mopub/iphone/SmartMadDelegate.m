//
//  SmardMadDelegate.m
//  MonstazAI
//
//  Created by Jacek Nijaki on 6/15/12.
//  Copyright (c) 2012 Gamelion. All rights reserved.
//

#import "SmartMadDelegate.h"

@implementation SmartMadDelegate

@synthesize smartMadAdView;
@synthesize mpAdView;

-(id)initWithAppId:(NSString*)appId spaceId:(NSString*)spaceId
{
    if(self == [super init])
    {
        [appId retain];
        [spaceId retain];
        
        m_appId = appId;
        m_spaceId = spaceId;
    }
    return self;
}

-(void)dealloc
{
    [m_appId release];
    [m_spaceId release];
    [smartMadAdView release];
    [mpAdView release];
    [super dealloc];
}

-(void)loadSMSDK:(MPAdView*)theAdView
{
    mpAdView = theAdView;
    [SmartMadAdView setApplicationId:m_appId];
    if (smartMadAdView) {
        [smartMadAdView release];
    }
    smartMadAdView=[[SmartMadAdView alloc] initRequestAdWithDelegate:self];
    [smartMadAdView setEventDelegate:self];
}

-(NSString*)adPositionId
{
    return m_spaceId;
}

- (void)adFullScreenStatus:(BOOL)isFullScreen
{
}

- (void)adEvent:(SmartMadAdView*)adview  adEventCode:(AdEventCodeType)eventCode
{
    if (!mpAdView) return;
    
    if (eventCode==EVENT_NEWAD) 
    {
        [mpAdView customEventDidLoadAd];
        
        // Place the smartmad into our MPAdView.
        [mpAdView setAdContentView:adview];
        [self adViewDidLoadAd:mpAdView];
    }
    else
    {
        [mpAdView customEventDidFailToLoadAd];
        [self adViewDidFailToLoadAd:mpAdView];
    }
}

- (void)adViewDidFailToLoadAd:(MPAdView *)adView
{
    [self loadSMSDK:nil];
}

@end

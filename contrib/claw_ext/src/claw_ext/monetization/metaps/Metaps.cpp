#include "claw_ext/monetization/metaps/Metaps.hpp"

namespace ClawExt
{
    void Metaps::RegisterObserver( MetapsObserver* callback )
    {
        m_observers.insert( callback );
    }

    void Metaps::UnregisterObserver( MetapsObserver* callback )
    {
        m_observers.erase( callback );
    }

    void Metaps::NotifyCurrencyEarned( int currencyAmount ) const
    {
        std::set<MetapsObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->MetapsCurrencyEarned( currencyAmount );
        }
    }

    void Metaps::NotifyError(  const Claw::NarrowString& errorCode ) const
    {
        std::set<MetapsObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->MetapsOnError( errorCode );
        }
    }
} // namespace ClawExt

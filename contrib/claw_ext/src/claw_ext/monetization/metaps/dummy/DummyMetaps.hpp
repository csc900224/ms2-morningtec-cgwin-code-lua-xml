#ifndef __INCLUDED__WIN32METAPS_HPP__
#define __INCLUDED__WIN32METAPS_HPP__

#include "claw_ext/monetization/metaps/Metaps.hpp"

namespace ClawExt
{
    class DummyMetaps : public Metaps
    {
    public:
        virtual bool Initialize( const Claw::NarrowString& userId, const Claw::NarrowString& appId, const Claw::NarrowString& appKey );
        virtual bool ShowOfferWall( const Claw::NarrowString& endUserId, const Claw::NarrowString& scenario ) const;
        virtual bool RunInstallReport() const;
    }; // class Win32Metaps
} // namespace ClawExt

#endif // __INCLUDED__WIN32METAPS_HPP__

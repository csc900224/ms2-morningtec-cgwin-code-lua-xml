#include "claw/base/Errors.hpp"
#include "claw_ext/monetization/metaps/dummy/DummyMetaps.hpp"

namespace ClawExt
{
    static DummyMetaps* s_instance = NULL;

    /*static*/ Metaps* Metaps::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyMetaps;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Metaps::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    bool DummyMetaps::Initialize( const Claw::NarrowString& userId, const Claw::NarrowString& appId, const Claw::NarrowString& appKey )
    {
        return true;
    }

    bool DummyMetaps::ShowOfferWall( const Claw::NarrowString& endUserId, const Claw::NarrowString& scenario ) const
    {
        Metaps::QueryInterface()->NotifyCurrencyEarned( 100 );
        return true;
    }

    bool DummyMetaps::RunInstallReport() const
    {
        Metaps::QueryInterface()->NotifyCurrencyEarned( 100 );
        return true;
    }
} // namespace ClawExt

package com.gamelion.metaps;

/**
 * This class holds global constants that are used throughout the application
 * to support tapjoy
 */
public class Consts
{
    public static final boolean DEBUG = false;
}

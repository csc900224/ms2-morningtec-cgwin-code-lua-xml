package com.gamelion.metaps;

import android.util.Log;
import android.view.View;
import android.view.Display;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;

import java.util.ArrayList;

import net.metaps.sdk.Factory;
import net.metaps.sdk.Receiver;
import net.metaps.sdk.Offer;
import net.metaps.sdk.Const;

import com.gamelion.metaps.Consts;
import com.Claw.Android.ClawActivityCommon;

public class Metaps implements Receiver
{
    private static final boolean TEST_MODE = false;

    private static Metaps s_instance = null;
    public static final String TAG = "Metaps";

    public static Metaps getInstance()
    {
        if (s_instance == null)
        {
            s_instance = new Metaps();
            if (Consts.DEBUG) Log.i(TAG, "create instance");
        }
        return s_instance;
    }

    public static void initialize(String userId, String appId, String appKey)
    {
        if (Consts.DEBUG) Log.i(TAG, "initialize(): UserID(" + userId + ") AppID(" + appId + ") AppKey(" + appKey + ")");
        try 
        {
            Factory.initialize(ClawActivityCommon.mActivity, getInstance(), userId, appId, appKey, TEST_MODE ? Const.SDK_MODE_TEST : Const.SDK_MODE_PRODUCTION);
        } 
        catch (Exception e)
        {
            if (Consts.DEBUG) Log.i(TAG, "initialize excception: errorCode(" + e.toString() + ")");
            nativeOnError(e.toString());
        }
    }

    public static void showOfferWall(String endUserId, String scenario)
    {
        if (Consts.DEBUG) Log.i(TAG, "showOfferWall(): endUserId(" + endUserId + ") scenario(" + scenario + ")");
        try 
        {
            Factory.launchOfferWall(ClawActivityCommon.mActivity, endUserId, scenario);
        } 
        catch (Exception e)
        {
            if (Consts.DEBUG) Log.i(TAG, "showOfferWall excception: errorCode(" + e.toString() + ")");
            nativeOnError(e.toString());
        }
    }
    
    public static void runInstallReport()
    {
        if (Consts.DEBUG) Log.i(TAG, "runInstallReport()");
        try 
        {
            Factory.runInstallReport();
        } 
        catch (Exception e)
        {
            if (Consts.DEBUG) Log.i(TAG, "runInstallReport excception: errorCode(" + e.toString() + ")");
            nativeOnError(e.toString());
        }
    }

    public boolean retrieve(int currencyAmount, Offer offer)
    {
        if (Consts.DEBUG) Log.i(TAG, "retrieve(): currency(" + currencyAmount + ")");
        nativeOnCurrencyEarned( currencyAmount );
        return true;
    }
    
    public boolean finalizeOnError(Offer offer)
    {
        if (Consts.DEBUG) Log.i(TAG, "finalizeOnError(): errorCode(" + offer.getErrorCode() + ")");
        nativeOnError(offer.getErrorCode());
        return true;
    }

    // JNI Methods
    private static native void nativeOnCurrencyEarned(int currencyAmount);
    private static native void nativeOnError(String errorCode);
}
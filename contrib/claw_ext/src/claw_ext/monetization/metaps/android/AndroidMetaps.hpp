#ifndef __INCLUDED__ANDROIDMETAPS_HPP__
#define __INCLUDED__ANDROIDMETAPS_HPP__

#include "claw_ext/monetization/metaps/Metaps.hpp"

namespace ClawExt
{
    class AndroidMetaps : public Metaps
    {
    public:
        virtual bool Initialize( const Claw::NarrowString& userId, const Claw::NarrowString& appId, const Claw::NarrowString& appKey );
        virtual bool ShowOfferWall( const Claw::NarrowString& endUserId, const Claw::NarrowString& scenario ) const;
        virtual bool RunInstallReport() const;
    }; // class AndroidMetaps
} // namespace ClawExt

#endif // __INCLUDED__ANDROIDMETAPS_HPP__

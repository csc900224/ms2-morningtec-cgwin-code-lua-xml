#include "claw/system/android/JniAttach.hpp"

#include "claw_ext/monetization/metaps/android/AndroidMetaps.hpp"

namespace ClawExt
{
    static AndroidMetaps* s_instance = NULL;

    /*static*/ Metaps* Metaps::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidMetaps();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Metaps::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    bool AndroidMetaps::Initialize( const Claw::NarrowString& userId, const Claw::NarrowString& appId, const Claw::NarrowString& appKey )
    {
        CLAW_MSG("AndroidMetaps::Initialize()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring juserId, jappId, jappKey;

        CLAW_ASSERT( !userId.empty() );
        if( !userId.empty() )
            juserId = Claw::JniAttach::GetStringFromChars( env, userId.c_str() );

        CLAW_ASSERT( !appId.empty() );
        if( !appId.empty() )
            jappId = Claw::JniAttach::GetStringFromChars( env, appId.c_str() );

        CLAW_ASSERT( !appKey.empty() );
        if( !appKey.empty() )
            jappKey = Claw::JniAttach::GetStringFromChars( env, appKey.c_str() );

        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/metaps/Metaps", "initialize", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", juserId, jappId, jappKey );

        Claw::JniAttach::ReleaseString( env, juserId );
        Claw::JniAttach::ReleaseString( env, jappId );
        Claw::JniAttach::ReleaseString( env, jappKey );

        Claw::JniAttach::Detach( attached );
        return ret;
    }

    bool AndroidMetaps::ShowOfferWall( const Claw::NarrowString& endUserId, const Claw::NarrowString& scenario ) const
    {
        CLAW_MSG("AndroidMetaps::ShowOfferWall()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jendUserId, jscenario;

        CLAW_ASSERT( !endUserId.empty() );
        if( !endUserId.empty() )
            jendUserId = Claw::JniAttach::GetStringFromChars( env, endUserId.c_str() );

        CLAW_ASSERT( !scenario.empty() );
        if( !scenario.empty() )
            jscenario = Claw::JniAttach::GetStringFromChars( env, scenario.c_str() );

        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/metaps/Metaps", "showOfferWall", "(Ljava/lang/String;Ljava/lang/String;)V", jendUserId, jscenario );
        
        Claw::JniAttach::ReleaseString( env, jendUserId );
        Claw::JniAttach::ReleaseString( env, jscenario );

        Claw::JniAttach::Detach( attached );
        return ret;
    }

    bool AndroidMetaps::RunInstallReport() const
    {
        CLAW_MSG("AndroidMetaps::RunInstallReport()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/metaps/Metaps", "runInstallReport", "()V" );
       
        Claw::JniAttach::Detach( attached );
        return ret;
    }
} // namespace ClawExt

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_metaps_Metaps_nativeOnCurrencyEarned( JNIEnv* env, jclass clazz, jint currency )
    {
        ClawExt::Metaps::QueryInterface()->NotifyCurrencyEarned( currency );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_metaps_Metaps_nativeOnError( JNIEnv* env, jclass clazz, jstring error )
    {
        Claw::NarrowString errorString = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, error, buffer, buffSize );
        errorString = buffer;

        ClawExt::Metaps::QueryInterface()->NotifyError( errorString );
    }
};

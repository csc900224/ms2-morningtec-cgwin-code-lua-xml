#ifndef __INCLUDED__METAPS_HPP__
#define __INCLUDED__METAPS_HPP__

#include "claw/base/String.hpp"
#include <set>

namespace ClawExt
{
    class Metaps
    {
    public:
        class MetapsObserver
        {
        public:
            virtual void MetapsCurrencyEarned( int currencyAmount ) = 0;
            virtual void MetapsOnError( const Claw::NarrowString& errorCode ) = 0;
        };

        virtual bool Initialize( const Claw::NarrowString& userId, const Claw::NarrowString& appId, const Claw::NarrowString& appKey ) = 0;

        virtual bool ShowOfferWall( const Claw::NarrowString& endUserId, const Claw::NarrowString& scenario ) const = 0;

        virtual bool RunInstallReport() const = 0;

        void RegisterObserver( MetapsObserver* callback );

        void UnregisterObserver( MetapsObserver* callback );

        void NotifyCurrencyEarned( int currencyAmount ) const;

        void NotifyError(  const Claw::NarrowString& errorCode ) const;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Tapjoy object implementation.
        */
        static Metaps* QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void Release();

    private:
        std::set<MetapsObserver*> m_observers;
    }; // class Metaps
} // namespace ClawExt

#endif // __INCLUDED__METAPS_HPP__

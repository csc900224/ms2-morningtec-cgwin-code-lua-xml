//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/InAppProduct.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_PRODUCT_HPP__
#define __NETWORK_IN_APP_PRODUCT_HPP__

#include "claw/base/String.hpp"

namespace ClawExt
{
    class InAppProduct
    {
    public:
        typedef Claw::NarrowString      Id;
        typedef Claw::NarrowString      Text;
        typedef float                   Price;
        typedef unsigned int            Quantity;

        InAppProduct( const Id& id, const Text& name, const Text& desc, const Price& price, const Text& priceFmt = "%.2f" )
            : m_id( id )
            , m_name( name )
            , m_desc( desc )
            , m_price( price )
            , m_priceFormater( priceFmt )
        {}

        const Id&           GetId() const               { return m_id; }

        const Text&         GetName() const             { return m_name; }

        const Text&         GetDescription() const      { return m_desc; }

        const Price&        GetPrice() const            { return m_price; }

        const Text&         GetPriceFormat() const      { return m_priceFormater; }

    private:
        Id            m_id;
        Text          m_name;
        Text          m_desc;
        Price         m_price;
        Text          m_priceFormater;

    }; // class InAppProduct
} // namespace ClawExt
#endif // !defined __NETWORK_IN_APP_PRODUCT_HPP__
// EOF

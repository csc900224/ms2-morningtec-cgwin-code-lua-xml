//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/InAppStore.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_STORE_HPP__
#define __NETWORK_IN_APP_STORE_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"
#include "claw_ext/monetization/inappstore/InAppTransaction.hpp"

#include "claw/base/String.hpp"

// External includes
#include <list>
#include <vector>
#include <set>

#ifdef IAP_SERVER_VERIFICATION
#include "claw/base/SmartPtr.hpp"
#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

#define IAP_SERVER_ADDR(url) namespace ClawExt { const char* InAppVerifierAction::SERVER_URL = url; }

#endif

namespace ClawExt
{
    class InAppStore
    {
    public:
        enum SystemType
        {
            ST_DEFAULT = 0,
            ST_AMAZON,
            ST_GOOGLE,

            ST_NUM,
        };

        // Forward declarations
        class TransactionsObserver;
        class SubscriptionObserver;

        // Message container type
        typedef Claw::NarrowString              Message;
        typedef Claw::NarrowString              Key;

        //! Virtual destructor for derived classes.
        virtual             ~InAppStore()       {};

        //! Check if transactions billing is supported.
        /**
        * Checks system parental control, environment settings etc.
        * It is recommended to not perform any transactions (BuyProduct calls) before
        * checking if transactions are actually supported on the desired system, the
        * response is comming asynchronously in a TransactionObserver::TransactionSupport()
        * method invocation. Override this method to retreive query result, is may be also possible
        * that this query is automatically perfomed by InAppStore implementation before taking any
        * transaction into accout, so you may receive TransactionSupport response automatically (without checking).
        * 
        * \param key optional key used to initialize billing system.
        * \param reason optional message container if query fails immediatelly returning false.
        * \return false if query can not be performed, then reason message will contain info
        * about desired problem.
        */
        virtual bool        CheckBillingSupport( const Key* key = NULL, Message* reason = 0 ) = 0;

        //! Check if there are any pending transactions that need to be restored.
        /**
        * Transaction could be finalized after application was closed or crashed.
        * In such scenario some transaction could be pending and need to be restored
        * (processed by the application). This check should be done as soon as application 
        * is ready for transaction processing and data saving.
        */
        virtual bool        CheckPendingTransactions( Message* reason = 0 ) = 0;

        //! Purchase subscription and wait for transaction confirmation.
        /**
        * Query subscription purchase and retreive confirmation via Listener::SubscriptionBought
        * on success or Listener::SubscriptionFailed when purchase failures.
        * \param id subscription id, caller must ensure that id is valid
        * \return false if operation can not be performed for some reasons (no network,etc.).
        */
        bool BuySubscription( const Claw::NarrowString& id );

        //! Asynchronously check if user owns the subscription. (the information is passed via Listener:SubscriptionStatus)
        bool CheckSubscription( const Claw::NarrowString& id );

        //! Register store transactions observer.
        /**
        * \return false if observer was already registered, true if successfully registered.
        */
        bool                RegisterTransObserver( TransactionsObserver* observer );

        //! Unregister transactions observer.
        /**
        * \return false if observer was not registered to this store.
        */
        bool                UnregisterTransObserver( TransactionsObserver* observer );

        //! Register store subscription observer.
        /**
        * \return false if observer was already registered, true if successfully registered.
        */
        bool                RegisterSubscriptionObserver( SubscriptionObserver* observer );

        //! Unregister subscription observer.
        /**
        * \return false if observer was not registered to this store.
        */
        bool                UnregisterSubscriptionObserver( SubscriptionObserver* observer );

        //! Update in app store
        void                Update();

        //! Purchase product and wait for transaction confirmation.
        /**
        * Query product purchase and retreive confirmation via Listener::TransactionComplete
        * on success or Listener::TransactionFailed when purchase failures.
        * \param id purchased product id, caller must ensure that id is valid
        * \param amount number of purchased products of the same type
        * \param reason optional message delivered to the used if purchase can not be performed
        * \return false if operation can not be performed for some reasons (no network,etc.).
        */
        bool                BuyProduct( const InAppProduct::Id& id, const InAppProduct::Quantity& amount = 1, Message* reason = 0 );

        //! Purchase consumable product and wait for transaction confirmation.
        /**
        * Query product purchase and retreive confirmation via Listener::TransactionComplete
        * on success or Listener::TransactionFailed when purchase failures.
        * \param id purchased product id, caller must ensure that id is valid
        * \param amount number of purchased products of the same type
        * \param reason optional message delivered to the used if purchase can not be performed
        * \return false if operation can not be performed for some reasons (no network,etc.).
        */
        bool                BuyConsumableProduct( const InAppProduct::Id& id, const InAppProduct::Quantity& amount = 1, Message* reason = 0 );

        virtual void RestorePurchases() {}

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate plaftorm InAppStore object implementation.
        * Remember to use IAP_SERVER_ADDR macro to specify verification server address.
        */
#ifdef IAP_SERVER_VERIFICATION
        static InAppStore*  QueryInterface( Claw::VfsMount* mount, const char* fileName = NULL, SystemType type = ST_DEFAULT );
#else
        static InAppStore*  QueryInterface( SystemType type = ST_DEFAULT );
#endif

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void         Release( InAppStore* store );

        class TransactionsObserver
        {
        public:
            //! Called when performed transaction fails.
            virtual void    TransactionFailed( const InAppTransaction& trans )      = 0;
            //! Called when performed transaction is canceled by the user. No messages should be presented to the user here.
            virtual void    TransactionCancel( const InAppTransaction& trans )      = 0;
            //! Called on successful product purchase.
            virtual void    TransactionComplete( const InAppTransaction& trans )    = 0;
            //! Called when product is restored from database.
            virtual void    TransactionRestore( const InAppTransaction& trans )     = 0;
            //! Called when product is refunded.
            virtual void    TransactionRefund( const InAppTransaction& trans )      = 0;
            //! Called to inform user if transactions and billing is actually supported.
            virtual void    TransactionSupport( bool supported )                    = 0;
        }; // class TransactionsObserver

        class SubscriptionObserver
        {
        public:
            //! Called when subscription status was checked
            virtual void    SubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active ) = 0;
            //! Called when performed transaction is cancelled by the user. No messages should be presented to the user here.
            virtual void    SubscriptionCancel( const Claw::NarrowString&  subscriptionId ) = 0;
            //! Called on successful subscription purchase.
            virtual void    SubscriptionBought( const Claw::NarrowString&  subscriptionId ) = 0;
            //! Called when user tries to buy already owened subscription (application should not allow this behaviour, but if it happens, turn on the subscription)
            virtual void    SubscriptionAlreadyOwned( const Claw::NarrowString& subscriptionId ) = 0;
            //! Called when performed transaction fails.
            virtual void    SubscriptionFailed( const Claw::NarrowString&  subscriptionId ) = 0;
        }; // class SubscriptionObserver
    protected:
#ifdef IAP_SERVER_VERIFICATION
        InAppStore();
        InAppStore( Claw::VfsMount* mount, const char* fileName );
#endif

        virtual bool BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& amount = 1, Message* reason = 0 ) = 0;
        virtual bool CheckSubscriptionInternal( const Claw::NarrowString& subscriptionId ) = 0;
        virtual bool BuySubscriptionInternal( const Claw::NarrowString& subscriptionId ) = 0;

        void                BroadcastTransactionFailed( const InAppTransaction& trans );
        void                BroadcastTransactionComplete( const InAppTransaction& trans );

        void                NotifyTransactionFailed( const InAppTransaction& trans );
        void                NotifyTransactionCancel( const InAppTransaction& trans );
        void                NotifyTransactionComplete( const InAppTransaction& trans );
        void                NotifyTransactionRestore( const InAppTransaction& trans );
        void                NotifyTransactionRefund( const InAppTransaction& trans );
        void                NotifyTransactionSupport( bool supported );

        typedef std::list<TransactionsObserver*>    TransObservers;
        typedef TransObservers::iterator            TransObserversIt;
        typedef TransObservers::const_iterator      TransObserversConstIt;

        TransObservers      m_transObservers;

        void BroadcastSubscriptionStatus( const InAppTransaction& trans, bool active );
        void BroadcastSubscriptionBought( const InAppTransaction& trans );
        void BroadcastSubscriptionAlreadyOwned( const InAppTransaction& trans );
        void BroadcastSubscriptionFailed( const InAppTransaction& trans );

        void                NotifySubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active, const unsigned char* data = NULL, unsigned int len = 0 );
        void                NotifySubscriptionCancel( const Claw::NarrowString& subscriptionId );
        void                NotifySubscriptionBought( const Claw::NarrowString& subscriptionId, const unsigned char* data = NULL, unsigned int len = 0 );
        void                NotifySubscriptionAlreadyOwned( const Claw::NarrowString& subscriptionId, const unsigned char* data = NULL, unsigned int len = 0 );
        void                NotifySubscriptionFailed( const Claw::NarrowString& subscriptionId );

        typedef std::list<SubscriptionObserver*>      SubscriptionObservers;
        typedef SubscriptionObservers::iterator       SubscriptionObserversIt;
        typedef SubscriptionObservers::const_iterator SubscriptionObserversConstIt;

        SubscriptionObservers m_subscriptionObservers;

        typedef std::set<InAppProduct::Id>              ConsumableProducts;
        ConsumableProducts                              m_consumableProducts;

#ifdef IAP_SERVER_VERIFICATION
        typedef std::vector<InAppVerifierActionPtr>     TransactionVerifiers;
        typedef TransactionVerifiers::iterator          TransactionVerifiersIt;
        TransactionVerifiers                            m_verifierActions;

        void NotifyTransactionVerified( const InAppTransaction& trans );
        void StoreTransaction( const InAppTransaction& trans );
        void PopStoredTransaction( const InAppTransaction& trans );
        void RestoreTransactions();
        std::vector<InAppTransaction> LoadTransactions();
        void SaveTransactions( const std::vector<InAppTransaction>& transactions );
        Claw::Mutex m_storeMutex;
        bool m_transactionsRestored;

        Claw::VfsMountPtr m_vfsMount;
        Claw::NarrowString m_fileName;

#endif
    }; // class InAppStore
} // namespace ClawExt

#endif // !defined __NETWORK_IN_APP_STORE_HPP__
// EOF

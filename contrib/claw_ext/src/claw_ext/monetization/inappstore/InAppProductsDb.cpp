//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/InAppProductsDb.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/inappstore/InAppProductsDb.hpp"

// External includes
#include <algorithm>

namespace ClawExt
{
    bool InAppProductsDb::QueryProductInfo( const InAppProduct::Id& id, Message* reason /* = 0 */ )
    {
        ProductsIds ids;
        ids.push_back( id );
        return QueryProductsInfos(ids, reason);
    }

    bool InAppProductsDb::RegisterProdObserver( InAppProductsDb::ProductsObserver* observer )
    {
        // Already registered
        if( std::find( m_prodObservers.begin(), m_prodObservers.end(), observer ) != m_prodObservers.end() )
            return false;
        m_prodObservers.push_back( observer );
        return true;
    }

    bool InAppProductsDb::UnregisterProdObserver( InAppProductsDb::ProductsObserver* observer )
    {
        // Not yet registered
        ProdObserversIt it = std::find( m_prodObservers.begin(), m_prodObservers.end(), observer );
        if( it == m_prodObservers.end() )
            return false;
        m_prodObservers.erase( it );
        return true;
    }

    void InAppProductsDb::NotifyProductsIds( const ProductsIds& ids )
    {
        ProdObserversIt it = m_prodObservers.begin();
        ProdObserversIt end = m_prodObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->RetreiveProductsIds( ids );
        }
    }

    void InAppProductsDb::NotifyProductsInfos( const Products& products )
    {
        ProdObserversIt it = m_prodObservers.begin();
        ProdObserversIt end = m_prodObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->RetreiveProductsInfos( products );
        }
    }
} // namespace ClawExt
// EOF

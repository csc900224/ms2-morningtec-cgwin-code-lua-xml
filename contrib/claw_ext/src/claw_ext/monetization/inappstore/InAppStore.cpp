//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/InAppStore.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/inappstore/InAppStore.hpp"

// External includes
#include "claw/network/Network.hpp"
#include <algorithm>

namespace ClawExt
{

#define XOR_26(k) { k[0]^0xaa,k[1]^0xab,k[2]^0xac,k[3]^0xad,k[4]^0xae,k[5]^0xaf,k[6]^0xb0,k[7]^0xb1,k[8]^0xb2,k[9]^0xb3, \
                    k[10]^0xb4,k[11]^0xb5,k[12]^0xb6,k[13]^0xb7,k[14]^0xb8,k[15]^0xb9,k[16]^0xba,k[17]^0xbb,k[18]^0xbc,k[19]^0xbd, \
                    k[20]^0xbe,k[21]^0xbf,k[22]^0xc0,k[23]^0xc1,k[24]^0xc2,k[25]^0xc3 }

    const unsigned char FILE_KEY1[] = XOR_26("ZVKWiF26EfhMoIzkI2ERhM24kv");
    const unsigned char FILE_KEY2[] = XOR_26("Vfp1SV9fLQ1iLpgWOywiq7jyEZ");
    const unsigned char FILE_KEY3[] = "hkhG";

#if defined IAP_SERVER_VERIFICATION
    InAppStore::InAppStore( Claw::VfsMount* mount, const char* fileName )
        : m_vfsMount( mount )
        , m_fileName( fileName )
        , m_transactionsRestored( false )
    {
        CLAW_ASSERT( Claw::Network::GetInstance() );
        CLAW_ASSERT( mount == NULL || (m_vfsMount && m_vfsMount->QueryFlag() & Claw::MF_SAVEGAME) );
    }
#endif

    bool InAppStore::RegisterTransObserver( InAppStore::TransactionsObserver* observer )
    {
        // Already registered
        if( std::find( m_transObservers.begin(), m_transObservers.end(), observer ) != m_transObservers.end() )
            return false;
        m_transObservers.push_back( observer );
        return true;
    }

    bool InAppStore::UnregisterTransObserver( InAppStore::TransactionsObserver* observer )
    {
        // Not yet registered
        TransObserversIt it = std::find( m_transObservers.begin(), m_transObservers.end(), observer );
        if( it == m_transObservers.end() )
            return false;
        m_transObservers.erase( it );
        return true;
    }

    bool InAppStore::RegisterSubscriptionObserver( SubscriptionObserver* observer )
    {
        if( std::find( m_subscriptionObservers.begin(), m_subscriptionObservers.end(), observer ) != m_subscriptionObservers.end() )
            return false;
        m_subscriptionObservers.push_back( observer );
        return true;
    }

    bool InAppStore::UnregisterSubscriptionObserver( SubscriptionObserver* observer )
    {
        SubscriptionObserversIt it = std::find( m_subscriptionObservers.begin(), m_subscriptionObservers.end(), observer );
        if( it == m_subscriptionObservers.end() )
            return false;
        m_subscriptionObservers.erase( it );
        return true;
    }

    void InAppStore::Update()
    {
#ifdef IAP_SERVER_VERIFICATION
        if( !m_transactionsRestored )
        {
            RestoreTransactions();
            m_transactionsRestored = true;
            return;
        }
        for( TransactionVerifiersIt it = m_verifierActions.begin(); it != m_verifierActions.end(); )
        {
            // need a copy
            InAppTransaction trans = (*it)->GetTransaction();

            switch( (*it)->Update() )
            {
            case InAppVerifierAction::RT_VERIFY_BUY_SUBSCRIPTION_READY:
                CLAW_MSG( "Subscription " << trans.GetProductId() << " is ready for purchasing. Making real request to in-app store..." );

                it = m_verifierActions.erase( it );

                BuySubscriptionInternal( trans.GetProductId() );
                return; // m_verifierActions can be modified
            case InAppVerifierAction::RT_VERIFY_CHECK_SUBSCRIPTION_READY:
                CLAW_MSG( "Subscription " << trans.GetProductId() << " is ready for check. Making real request to in-app store..." );

                it = m_verifierActions.erase( it );

                CheckSubscriptionInternal( trans.GetProductId() );
                return; // m_verifierActions can be modified
            case InAppVerifierAction::RT_VERIFY_READY:
                CLAW_MSG( "Product " << trans.GetProductId() << " is ready for purchasing. Making real request to in-app store..." );

                it = m_verifierActions.erase( it );

                BuyProductInternal( trans.GetProductId(), trans.GetQuantity(), (Message*)( trans.GetOtherData() ) );
                return; // m_verifierActions can be modified
            case InAppVerifierAction::RT_VERIFIED_CHECK_SUBSCRIPTION:
                CLAW_MSG( "Check of subscription " << trans.GetProductId() << " verified by server." );

                it = m_verifierActions.erase( it );

                BroadcastSubscriptionStatus( trans, true );

                break;
            case InAppVerifierAction::RT_VERIFIED_BUY_SUBSCRIPTION:
                CLAW_MSG( "Purchase of subscription " << trans.GetProductId() << " verified by server." );

                it = m_verifierActions.erase( it );

                BroadcastSubscriptionBought( trans );

                break;
            case InAppVerifierAction::RT_VERIFIED_BUY_ALREADY_OWNED_SUBSCRIPTION:
                CLAW_MSG( "Subscription " << trans.GetProductId() << " is indeed already owned." );

                it = m_verifierActions.erase( it );

                BroadcastSubscriptionAlreadyOwned( trans );

                break;
            case InAppVerifierAction::RT_VERIFIED:
                CLAW_MSG( "Purchase of product " << trans.GetProductId() << " verified by server." );

                it = m_verifierActions.erase( it );

                BroadcastTransactionComplete( trans );

                PopStoredTransaction( trans );

                if( m_consumableProducts.find( trans.GetProductId() ) != m_consumableProducts.end() )
                {
                    CLAW_MSG( "Consuming product " << trans.GetProductId() << " on veryfication server..." );
                    InAppVerifierActionPtr action( InAppVerifierAction::CreateActionConsume( trans ) );
                    m_verifierActions.push_back( action );
                    action->Run();

                    return; // m_verifierActions is modified
                }

                break;
            case InAppVerifierAction::RT_PREPARE_BROADCAST_CONNECT_ERROR:
                CLAW_MSG( "Preparation failed because of connection error." );

                BroadcastSubscriptionFailed( trans );

                it = m_verifierActions.erase( it );
                break;
            case InAppVerifierAction::RT_PREPARE_CONNECT_ERROR:
                CLAW_MSG( "Preparation failed because of connection error." );

                BroadcastTransactionFailed( trans );

                it = m_verifierActions.erase( it );
                break;
            case InAppVerifierAction::RT_VERIFY_CONNECT_ERROR:
                CLAW_MSG( "Verification failed because of connection error." );

                BroadcastTransactionFailed( trans );

                it = m_verifierActions.erase( it );

                // TODO: serialize and save the transaction and try to verify it until response is received
                break;
            case InAppVerifierAction::RT_VERIFY_SUBSCRIPTION_CONNECT_ERROR:
                CLAW_MSG( "Verification failed because of connection error." );

                BroadcastSubscriptionFailed( trans );

                it = m_verifierActions.erase( it );

                break;
            case InAppVerifierAction::RT_CONSUME_CONNECT_ERROR:
                CLAW_MSG( "Consume failed because of connection error." );

                it = m_verifierActions.erase( it );

                // TODO: serialize and save the transaction and try to consume it later
                break;
            case InAppVerifierAction::RT_PREPARE_ERROR:
                CLAW_MSG( "Product " << trans.GetProductId() << " rejected by verification server." );

                BroadcastTransactionFailed( trans );

                it = m_verifierActions.erase( it );
                break;
            case InAppVerifierAction::RT_PREPARE_SUBSCRIPTION_ERROR:
                CLAW_MSG( "Product " << trans.GetProductId() << " rejected by verification server." );

                BroadcastSubscriptionFailed( trans );

                it = m_verifierActions.erase( it );
                break;
            case InAppVerifierAction::RT_VERIFY_ERROR:
                CLAW_MSG( "Product purchase " << trans.GetProductId() << " didn't pass verification by server." );

                BroadcastTransactionFailed( trans );

                it = m_verifierActions.erase( it );
                break;
            case InAppVerifierAction::RT_VERIFY_ERROR_CHECK_SUBSCRIPTION:
                CLAW_MSG( "Check subscription " << trans.GetProductId() << " didn't pass verification by server." );

                BroadcastSubscriptionStatus( trans, false );

                it = m_verifierActions.erase( it );
                break;
            case InAppVerifierAction::RT_VERIFY_ERROR_BUY_SUBSCRIPTION:
                CLAW_MSG( "Subscription purchase " << trans.GetProductId() << " didn't pass verification by server." );

                BroadcastSubscriptionFailed( trans );

                it = m_verifierActions.erase( it );
                break;
            default:
                ++it;
                break;
            }
        }
#endif
    }

    bool InAppStore::BuyProduct( const InAppProduct::Id& id, const InAppProduct::Quantity& amount, Message* reason )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Preparing verification server for purchase of product: " << id );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionPrepare( InAppTransaction( id, amount, 0, 0, reason ) ) );
        m_verifierActions.push_back( action );
        action->Run();
        return true;
#else
        return BuyProductInternal( id, amount, reason );
#endif
    }

    bool InAppStore::BuyConsumableProduct( const InAppProduct::Id& id, const InAppProduct::Quantity& amount, Message* reason )
    {
        m_consumableProducts.insert( id );
        return BuyProduct( id, amount, reason );
    }

    bool InAppStore::CheckSubscription( const Claw::NarrowString& subscriptionId )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Preparing verification server for purchase of subscription: " << subscriptionId );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionPrepareCheckSubscription( subscriptionId ) );
        m_verifierActions.push_back( action );
        action->Run();
        return true;
#else
        return CheckSubscriptionInternal( subscriptionId );
#endif
    }

    bool InAppStore::BuySubscription( const Claw::NarrowString& subscriptionId )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Preparing verification server for purchase of subscription: " << subscriptionId );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionPrepareBuySubscription( subscriptionId ) );
        m_verifierActions.push_back( action );
        action->Run();
        return true;
#else
        return BuySubscriptionInternal( subscriptionId );
#endif
    }

    void InAppStore::BroadcastTransactionFailed( const InAppTransaction& trans )
    {
        TransObserversIt it = m_transObservers.begin();
        TransObserversIt end = m_transObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->TransactionFailed( trans );
        }
    }

    void InAppStore::BroadcastTransactionComplete( const InAppTransaction& trans )
    {
        TransObserversIt it = m_transObservers.begin();
        TransObserversIt end = m_transObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->TransactionComplete( trans );
        }
    }

    void InAppStore::NotifyTransactionFailed( const InAppTransaction& trans )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Product " << trans.GetProductId() << " purchase failed." );
#endif
        BroadcastTransactionFailed( trans );
    }

    void InAppStore::NotifyTransactionCancel( const InAppTransaction& trans )
    {
        TransObserversIt it = m_transObservers.begin();
        TransObserversIt end = m_transObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->TransactionCancel( trans );
        }
    }

    void InAppStore::NotifyTransactionComplete( const InAppTransaction& trans )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Product " << trans.GetProductId() << " purchased. Veryfing transaction on server..." );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionVerify( trans ) );
        m_verifierActions.push_back( action );
        action->Run();

        StoreTransaction( trans );
        // TODO: serialize and save the transaction and try to verify it until response is received
#else
        BroadcastTransactionComplete( trans );
#endif
    }

    void InAppStore::NotifyTransactionRestore( const InAppTransaction& trans )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Product " << trans.GetProductId() << " restored. Veryfing transaction on server." );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionVerify( trans ) );
        m_verifierActions.push_back( action );
        action->Run();
#else
        TransObserversIt it = m_transObservers.begin();
        TransObserversIt end = m_transObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->TransactionRestore( trans );
        }
#endif
    }

    void InAppStore::NotifyTransactionRefund( const InAppTransaction& trans )
    {
        TransObserversIt it = m_transObservers.begin();
        TransObserversIt end = m_transObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->TransactionRefund( trans );
        }
    }

    void InAppStore::NotifyTransactionSupport( bool supported )
    {
        TransObserversIt it = m_transObservers.begin();
        TransObserversIt end = m_transObservers.end();
        for( ; it != end; ++it )
        {
            (*it)->TransactionSupport( supported );
        }
    }

    void InAppStore::NotifySubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active, const unsigned char* data, unsigned int len )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Subscription " << subscriptionId << " status: " << active << "Veryfing transaction on server." );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionVerifyCheckSubscription( InAppTransaction(subscriptionId, 1, data, len ) ) );
        m_verifierActions.push_back( action );
        action->Run();
#else
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionStatus( subscriptionId, active );
        }
#endif
    }

    void InAppStore::BroadcastSubscriptionStatus( const InAppTransaction& trans, bool active )
    {
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionStatus( trans.GetProductId(), active );
        }
    }

    void InAppStore::NotifySubscriptionCancel( const Claw::NarrowString& subscriptionId )
    {
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionCancel(subscriptionId);
        }
    }

    void InAppStore::NotifySubscriptionBought( const Claw::NarrowString& subscriptionId, const unsigned char* data, unsigned int len )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Subscription " << subscriptionId << " bought. Veryfing transaction on server." );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionVerifyBuySubscription( InAppTransaction( subscriptionId, 1, data, len ), false ) );
        m_verifierActions.push_back( action );
        action->Run();
#else
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionBought(subscriptionId);
        }
#endif
    }

    void InAppStore::BroadcastSubscriptionBought( const InAppTransaction& trans )
    {
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionBought(trans.GetProductId());
        }
    }

    void InAppStore::NotifySubscriptionAlreadyOwned( const Claw::NarrowString& subscriptionId, const unsigned char* data, unsigned int len )
    {
#ifdef IAP_SERVER_VERIFICATION
        CLAW_MSG( "Subscription " << subscriptionId << " bought. Veryfing transaction on server." );

        InAppVerifierActionPtr action( InAppVerifierAction::CreateActionVerifyBuySubscription( InAppTransaction( subscriptionId, 1, data, len ), true ) );
        m_verifierActions.push_back( action );
        action->Run();
#else
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionAlreadyOwned(subscriptionId);
        }
#endif
    }

    void InAppStore::BroadcastSubscriptionAlreadyOwned( const InAppTransaction& trans )
    {
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionAlreadyOwned(trans.GetProductId());
        }
    }

    void InAppStore::NotifySubscriptionFailed( const Claw::NarrowString& subscriptionId )
    {
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionFailed(subscriptionId);
        }
    }

    void InAppStore::BroadcastSubscriptionFailed( const InAppTransaction& trans )
    {
        for (SubscriptionObserversIt it = m_subscriptionObservers.begin(), end = m_subscriptionObservers.end();
            it != end; ++it )
        {
            (*it)->SubscriptionFailed(trans.GetProductId());
        }
    }

#ifdef IAP_SERVER_VERIFICATION

    std::vector<InAppTransaction> InAppStore::LoadTransactions()
    {
        // prepare key
        unsigned char key[56];
        unsigned char* keyPtr = key;
        memcpy( keyPtr, FILE_KEY1, 26 );
        Claw::UInt8 x = 0xaa;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;
        memcpy( keyPtr, FILE_KEY3, 4 );
        keyPtr += 4;
        memcpy( keyPtr, FILE_KEY2, 26 );
        x -= 26;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;
        Claw::NarrowString preparedKey((char*)key, 56);

        std::vector<InAppTransaction> transactions;
        Claw::FilePtr f( Claw::OpenEncryptedFile( m_fileName, preparedKey, true ) );
        if( f )
        {
            int count;
            f->Read( &count, sizeof( int ) );
            for( int i = 0; i < count; ++i )
            {
                InAppTransaction t;
                t.Load( f );
                transactions.push_back( t );
            }
        }
        return transactions;
    }

    void InAppStore::SaveTransactions( const std::vector<InAppTransaction>& transactions )
    {
        // prepare key
        unsigned char key[56];
        unsigned char* keyPtr = key;
        memcpy( keyPtr, FILE_KEY1, 26 );
        Claw::UInt8 x = 0xaa;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;
        memcpy( keyPtr, FILE_KEY3, 4 );
        keyPtr += 4;
        memcpy( keyPtr, FILE_KEY2, 26 );
        x -= 26;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;
        Claw::NarrowString preparedKey((char*)key, 56);

        Claw::FilePtr f( Claw::VfsCreateEncryptedFile( m_fileName, preparedKey, true ) );
        if( f )
        {
            int count = transactions.size();
            f->Write( &count, sizeof( int ) );

            for( int i = 0; i < count; ++i )
            {
                transactions[i].Save( f );
            }
        }

    }

    void InAppStore::StoreTransaction( const InAppTransaction& trans )
    {
        Claw::LockGuard<Claw::Mutex> lock( m_storeMutex );

        std::vector<InAppTransaction> transactions = LoadTransactions();
        std::vector<InAppTransaction>::iterator it = std::find( transactions.begin(), transactions.end(), trans );
        if( it == transactions.end() )
        {
            transactions.push_back( trans );
        }
        SaveTransactions( transactions );
    }

    void InAppStore::PopStoredTransaction( const InAppTransaction& trans )
    {
        Claw::LockGuard<Claw::Mutex> lock( m_storeMutex );

        std::vector<InAppTransaction> transactions = LoadTransactions();
        std::vector<InAppTransaction>::iterator it = std::find( transactions.begin(), transactions.end(), trans );
        if( it != transactions.end() )
        {
            transactions.erase( it );
        }
        SaveTransactions( transactions );
    }

    void InAppStore::RestoreTransactions()
    {
        Claw::LockGuard<Claw::Mutex> lock( m_storeMutex );

        std::vector<InAppTransaction> transactions = LoadTransactions();

        for( std::vector<InAppTransaction>::iterator it = transactions.begin(), end = transactions.end(); it != end; ++it )
        {
            InAppVerifierActionPtr action( InAppVerifierAction::CreateActionVerify( *it ) );
            m_verifierActions.push_back( action );
            action->Run();
        }

        transactions.clear();
        SaveTransactions( transactions );
    }
#endif

}

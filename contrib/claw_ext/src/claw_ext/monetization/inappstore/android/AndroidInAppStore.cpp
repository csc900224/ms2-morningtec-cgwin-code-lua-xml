//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/android/AndroidInAppStore.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/inappstore/android/google/AndroidGoogleInAppStore.hpp"
#include "claw_ext/monetization/inappstore/android/amazon/AndroidAmazonInAppStore.hpp"

#include "claw/base/Errors.hpp"

namespace ClawExt
{
    static AndroidGoogleInAppStore* s_googleInstance = NULL;
    static AndroidAmazonInAppStore* s_amazonInstance = NULL;

    static InAppStore* s_instance[InAppStore::ST_NUM] = { NULL };

#if defined IAP_SERVER_VERIFICATION
    InAppStore* InAppStore::QueryInterface( Claw::VfsMount* mount, const char* fileName, SystemType type )
#else
    InAppStore* InAppStore::QueryInterface( SystemType type )
#endif
    {
        if( type < 0 || type > ST_NUM ) return NULL;
        if( type == ST_DEFAULT ) 
            type = ST_GOOGLE;

        // Force singleton pattern
        CLAW_ASSERT( s_instance[type] == NULL );

        if( type == ST_GOOGLE )
        {
#if defined IAP_SERVER_VERIFICATION
            s_instance[type] = new AndroidGoogleInAppStore( mount, fileName );
#else
            s_instance[type] = new AndroidGoogleInAppStore();
#endif
        }
        else if( type == ST_AMAZON )
        {
#if defined IAP_SERVER_VERIFICATION
            s_instance[type] = new AndroidAmazonInAppStore( mount, fileName );
#else
            s_instance[type] = new AndroidAmazonInAppStore();
#endif
        }

        return s_instance[type];
    }

    void InAppStore::Release( InAppStore* instance )
    {
        CLAW_ASSERT( instance );

        for( int i = 0; i < ST_NUM; ++i )
        {
            if( instance == s_instance[i] )
            {
                s_instance[i] = NULL;
                delete instance;
                return;
            }
        }
        CLAW_ASSERT( !"Instance not found!" );
    }
} // namespace ClawExt

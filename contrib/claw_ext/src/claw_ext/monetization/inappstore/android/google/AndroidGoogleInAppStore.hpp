//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/android/AndroidGoogleInAppStore.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_ANDROID_GOOGLE_IN_APP_STORE_HPP__
#define __NETWORK_ANDROID_GOOGLE_IN_APP_STORE_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

#include <jni.h>

namespace ClawExt
{
    class AndroidGoogleInAppStore : public InAppStore
    {
    public:
#if defined IAP_SERVER_VERIFICATION
        AndroidGoogleInAppStore( Claw::VfsMount* mount, const char* fileName );
#else
                            AndroidGoogleInAppStore();
#endif
                            ~AndroidGoogleInAppStore();

        virtual bool        CheckBillingSupport( const Key* key = NULL, Message* reason = 0 );
        virtual bool        CheckPendingTransactions( Message* reason = 0 );
        virtual bool        BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity = 1, Message* reason = 0 );
        virtual bool        BuySubscriptionInternal( const Claw::NarrowString& id );
        virtual bool        CheckSubscriptionInternal( const Claw::NarrowString& id );

        // Public wrappers for JNI methods access.
        void                RetreiveTransactionComplete( const InAppTransaction& transaction );
        void                RetreiveTransactionRestore( const InAppTransaction& transaction );
        void                RetreiveTransactionFail( const InAppTransaction& transaction );
        void                RetreiveTransactionCancel( const InAppTransaction& transaction );
        void                RetreiveTransactionRefund( const InAppTransaction& transaction );
        void                RetreiveTransactionSupport( bool supported );
        void                RetrieveSubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active, const char* token );
        void                RetrieveSubscriptionCancel( const Claw::NarrowString& subscriptionId );
        void                RetrieveSubscriptionBought( const Claw::NarrowString& subscriptionId, const char* token );
        void                RetrieveSubscriptionAlreadyOwned( const Claw::NarrowString& subscriptionId, const char* token );
        void                RetrieveSubscriptionFailed( const Claw::NarrowString& subscriptionId );

    }; // class AndroidGoogleInAppStore
} // namespace ClawExt

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
Java_com_gamelion_inapp_InAppStore_nativeOnPurchaseComplete(JNIEnv * env, jclass thiz, jstring productId, jint quantity);

JNIEXPORT void JNICALL
Java_com_gamelion_inapp_InAppStore_nativeOnPurchaseRestore(JNIEnv * env, jclass thiz, jstring productId, jint quantity);

JNIEXPORT void JNICALL
Java_com_gamelion_inapp_InAppStore_nativeOnPurchaseFail(JNIEnv *, jclass thiz, jstring productId, jint quantity);

JNIEXPORT void JNICALL
Java_com_gamelion_inapp_InAppStore_nativeOnPurchaseCancel(JNIEnv *, jclass thiz, jstring productId, jint quantity);

JNIEXPORT void JNICALL
Java_com_gamelion_inapp_InAppStore_nativeOnPurchaseRefund(JNIEnv *, jclass thiz, jstring productId, jint quantity);

JNIEXPORT void JNICALL
Java_com_gamelion_inapp_InAppStore_nativeOnPurchaseSupport(JNIEnv *, jclass, jboolean);

#ifdef __cplusplus
}
#endif

#endif // !defined __NETWORK_ANDROID_GOOGLE_IN_APP_STORE_HPP__
// EOF

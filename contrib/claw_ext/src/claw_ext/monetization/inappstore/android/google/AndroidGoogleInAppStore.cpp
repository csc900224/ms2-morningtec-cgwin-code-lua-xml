//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/android/AndroidGoogleInAppStore.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/inappstore/android/google/AndroidGoogleInAppStore.hpp"
#include "claw/base/Errors.hpp"

// External includes
#include "claw/system/android/JniAttach.hpp"

static ClawExt::AndroidGoogleInAppStore* s_instance = NULL;

namespace ClawExt
{

#if defined IAP_SERVER_VERIFICATION
    AndroidGoogleInAppStore::AndroidGoogleInAppStore( Claw::VfsMount* mount, const char* fileName )
        : InAppStore( mount, fileName )
    {
        s_instance = this;

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "initBilling", "()V" );
        Claw::JniAttach::Detach( attached );
    }
#else
    AndroidGoogleInAppStore::AndroidGoogleInAppStore()
    {
        s_instance = this;

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "initBilling", "()V" );
        Claw::JniAttach::Detach( attached );
    }
#endif

    AndroidGoogleInAppStore::~AndroidGoogleInAppStore()
    {
        s_instance = NULL;

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "release", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    bool AndroidGoogleInAppStore::CheckBillingSupport( const Key* key, Message* message )
    {
        CLAW_ASSERT( key );
        JNIEnv* env;

        bool attached = Claw::JniAttach::Attach( &env );

        jstring secret = Claw::JniAttach::GetStringFromChars( env, key->c_str() );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "CheckBillingSupport", "(Ljava/lang/String;)V", secret );
        Claw::JniAttach::ReleaseString( env, secret );
        
        Claw::JniAttach::Detach( attached );
        
        return true;
    }

    bool AndroidGoogleInAppStore::CheckPendingTransactions( Message* reason /*= 0*/ )
    {
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "CheckPendingTransactions", "()V" );
        Claw::JniAttach::Detach( attached );
        return true;
    }

    bool AndroidGoogleInAppStore::BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity /* = 1 */, Message* reason /* = 0 */ )
    {
        CLAW_MSG_ASSERT( quantity == 1, "Buying more than one product isn't supported yet." );
        if( quantity != 1 ) return false;

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring productId = Claw::JniAttach::GetStringFromChars( env, id.c_str() );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "PurchaseProduct", "(Ljava/lang/String;I)V", productId, quantity );

        Claw::JniAttach::ReleaseString( env, productId );
        Claw::JniAttach::Detach( attached );
        return true;
    }

    bool AndroidGoogleInAppStore::BuySubscriptionInternal( const Claw::NarrowString& id )
    {
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring productId = Claw::JniAttach::GetStringFromChars( env, id.c_str() );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "PurchaseSubscription", "(Ljava/lang/String;)V", productId );

        Claw::JniAttach::ReleaseString( env, productId );
        Claw::JniAttach::Detach( attached );
        return true;
    }

    bool AndroidGoogleInAppStore::CheckSubscriptionInternal( const Claw::NarrowString& id )
    {
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring productId = Claw::JniAttach::GetStringFromChars( env, id.c_str() );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/google/InAppStore", "CheckSubscription", "(Ljava/lang/String;)V", productId );

        Claw::JniAttach::ReleaseString( env, productId );
        Claw::JniAttach::Detach( attached );
        return true;
    }

    void AndroidGoogleInAppStore::RetreiveTransactionComplete( const InAppTransaction& transaction )
    {
        NotifyTransactionComplete( transaction );
    }

    void AndroidGoogleInAppStore::RetreiveTransactionRestore( const InAppTransaction& transaction )
    {
        NotifyTransactionRestore( transaction );
    }

    void AndroidGoogleInAppStore::RetreiveTransactionFail( const InAppTransaction& transaction )
    {
        NotifyTransactionFailed( transaction );
    }

    void AndroidGoogleInAppStore::RetreiveTransactionCancel( const InAppTransaction& transaction )
    {
        NotifyTransactionCancel( transaction );
    }

    void AndroidGoogleInAppStore::RetreiveTransactionRefund( const InAppTransaction& transaction )
    {
        NotifyTransactionRefund( transaction );
    }

    void AndroidGoogleInAppStore::RetreiveTransactionSupport( bool supported )
    {
        NotifyTransactionSupport( supported );
    }

    void AndroidGoogleInAppStore::RetrieveSubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active, const char* token )
    {
        if( active )
        {
            CLAW_MSG( "Retrieve subscription status: " << subscriptionId << Claw::NarrowString( token ) );
            NotifySubscriptionStatus( subscriptionId, active, (const unsigned char*)token, strlen(token) );
        }
        else
        {
            NotifySubscriptionStatus( subscriptionId, active, NULL, 0 );
        }
    }

    void AndroidGoogleInAppStore::RetrieveSubscriptionCancel( const Claw::NarrowString& subscriptionId )
    {
        NotifySubscriptionCancel( subscriptionId );
    }

    void AndroidGoogleInAppStore::RetrieveSubscriptionBought( const Claw::NarrowString& subscriptionId, const char* token )
    {
        if( token )
        {
            NotifySubscriptionBought( subscriptionId, (const unsigned char*)token, strlen(token) );
        }
        else
        {
            NotifySubscriptionBought( subscriptionId, NULL, 0 );
        }
    }

    void AndroidGoogleInAppStore::RetrieveSubscriptionAlreadyOwned( const Claw::NarrowString& subscriptionId, const char* token )
    {
        if( token )
        {
            NotifySubscriptionAlreadyOwned( subscriptionId, (const unsigned char*) token, strlen(token) );
        }
        else
        {
            NotifySubscriptionAlreadyOwned( subscriptionId );
        }
    }

    void AndroidGoogleInAppStore::RetrieveSubscriptionFailed( const Claw::NarrowString& subscriptionId )
    {
        NotifySubscriptionFailed( subscriptionId );
    }
} // namespace ClawExt

extern "C"
{
    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnPurchaseComplete(JNIEnv * env, jclass thiz, jstring productId, jint quantity, jstring token)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);
        ClawExt::InAppProduct::Id id( nativeString );
        env->ReleaseStringUTFChars(productId, nativeString);

        const char* tokenCstr = env->GetStringUTFChars(token, 0);
        Claw::NarrowString tokenString = tokenCstr;
        env->ReleaseStringUTFChars(token, tokenCstr );

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseSuccessful " << id );
        s_instance->RetreiveTransactionComplete( ClawExt::InAppTransaction( id, quantity, (unsigned char*)tokenString.c_str(), tokenString.size() ) );
    
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnPurchaseRestore(JNIEnv * env, jclass thiz, jstring productId, jint quantity, jstring token)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);
        ClawExt::InAppProduct::Id id( nativeString );
        env->ReleaseStringUTFChars(productId, nativeString);

        const char* tokenCstr = env->GetStringUTFChars(token, 0);
        Claw::NarrowString tokenString = tokenCstr;
        env->ReleaseStringUTFChars(token, tokenCstr );

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseRestore " << id );
        s_instance->RetreiveTransactionRestore( ClawExt::InAppTransaction( id, quantity, (unsigned char*)tokenString.c_str(), tokenString.size() ) );
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnPurchaseFail(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseFail: " << id );
        s_instance->RetreiveTransactionFail( ClawExt::InAppTransaction( id, quantity ) );
    
        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnPurchaseCancel(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseCancel: " << id );
        s_instance->RetreiveTransactionCancel( ClawExt::InAppTransaction( id, quantity ) );
    
        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnPurchaseRefund(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseRefund: " << id );
        s_instance->RetreiveTransactionRefund( ClawExt::InAppTransaction( id, quantity ) );

        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnPurchaseSupport(JNIEnv * env, jclass thiz, jboolean supported)
    {
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseSupport: " << supported );
        s_instance->RetreiveTransactionSupport( supported );
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnSubscriptionStatus(JNIEnv * env, jclass thiz, jstring subscriptionId, jboolean supported, jstring token )
    {
        const char *nativeString = env->GetStringUTFChars(subscriptionId, 0);

        const char* tokenString = (supported)?(env->GetStringUTFChars(token, 0)):NULL;

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnSubscriptionStatus: " << nativeString << " supported: " << supported );
        s_instance->RetrieveSubscriptionStatus( nativeString, supported, tokenString );

        env->ReleaseStringUTFChars(subscriptionId, nativeString);
        if( supported )
        {
            env->ReleaseStringUTFChars(token, tokenString);
        }
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnSubscriptionBought(JNIEnv * env, jclass thiz, jstring subscriptionId, jstring token)
    {
        const char *nativeString = env->GetStringUTFChars(subscriptionId, 0);
        const char* tokenString = (token)?(env->GetStringUTFChars(token, 0)):NULL;

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnSubscriptionBought: " << nativeString );
        s_instance->RetrieveSubscriptionBought( nativeString, tokenString );

        env->ReleaseStringUTFChars(subscriptionId, nativeString);
        if( tokenString )
        {
            env->ReleaseStringUTFChars(token, tokenString);
        }
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnSubscriptionAlreadyOwned(JNIEnv * env, jclass thiz, jstring subscriptionId, jstring token)
    {
        const char *nativeString = env->GetStringUTFChars(subscriptionId, 0);
        const char* tokenString = (token)?(env->GetStringUTFChars(token, 0)):NULL;

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnSubscriptionAlreadyOwned: " << nativeString );
        s_instance->RetrieveSubscriptionAlreadyOwned( nativeString, tokenString );

        env->ReleaseStringUTFChars(subscriptionId, nativeString);
        if( tokenString )
        {
            env->ReleaseStringUTFChars(token, tokenString);
        }
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnSubscriptionCancel(JNIEnv * env, jclass thiz, jstring subscriptionId)
    {
        const char *nativeString = env->GetStringUTFChars(subscriptionId, 0);

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnSubscriptionCancel: " << nativeString );
        s_instance->RetrieveSubscriptionCancel( nativeString );

        env->ReleaseStringUTFChars(subscriptionId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_google_InAppStore_nativeOnSubscriptionFail(JNIEnv * env, jclass thiz, jstring subscriptionId)
    {
        const char *nativeString = env->GetStringUTFChars(subscriptionId, 0);

        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnSubscriptionFail: " << nativeString );
        s_instance->RetrieveSubscriptionFailed( nativeString );

        env->ReleaseStringUTFChars(subscriptionId, nativeString);
    }
};

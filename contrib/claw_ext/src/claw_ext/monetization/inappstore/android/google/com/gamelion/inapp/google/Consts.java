package com.gamelion.inapp.google;

/**
 * This class holds global constants that are used throughout the application
 * to support in-app billing.
 */
public class Consts 
{        
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST         = 10001;
    public static final boolean DEBUG   = false;
    public static final String TAG      = "InAppStore";
}

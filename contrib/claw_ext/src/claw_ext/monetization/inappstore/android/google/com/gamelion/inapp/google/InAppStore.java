package com.gamelion.inapp.google;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Queue;

import android.os.Handler;
import android.util.Log;
import android.content.Intent;

import com.gamelion.inapp.google.Consts;

import com.gamelion.inapp.google.util.IabHelper;
import com.gamelion.inapp.google.util.IabResult;
import com.gamelion.inapp.google.util.Inventory;
import com.gamelion.inapp.google.util.Purchase;
import com.gamelion.inapp.google.util.IabException;

import com.Claw.Android.ClawActivityCommon;
import com.Claw.Android.ClawActivityListener;

public class InAppStore implements ClawActivityListener
{
    private class RunOnUiThreadQueue {
        private LinkedList<Runnable> mQueue = new LinkedList<Runnable>();
        private boolean mEnabled = false;
        
        public void setEnabled( boolean enabled ) {
            if (Consts.DEBUG) Log.d(Consts.TAG, "RunOnUiThreadQueue.setEnabled(" + enabled +")");
            if( enabled != mEnabled ) {
                mEnabled = enabled;
                
                if( mEnabled ) {
                    call( mQueue.peek() );
                }
            }
        }
        
        public void callNext() {
            if (Consts.DEBUG) Log.d(Consts.TAG, "RunOnUiThreadQueue.callNext()");
            if( mEnabled ) {
                mQueue.poll();
                call( mQueue.peek() );
            }
        }
        
        public void enqueue( Runnable runnable ) {
            if (Consts.DEBUG) Log.d(Consts.TAG, "RunOnUiThreadQueue.enqueue():" + runnable);
            mQueue.offer( runnable );
            if( mEnabled && mQueue.size() == 1 ) {
                call( mQueue.peek() );
            }
        }
        
        private void call( Runnable runnable ) {
            if (Consts.DEBUG) Log.d(Consts.TAG, "RunOnUiThreadQueue.call():" + runnable);
            if( runnable != null ) {
                ClawActivityCommon.mActivity.runOnUiThread( runnable );
            }
        }
    }

    // The helper object
    private static InAppStore s_instance = null;
    public boolean mIsInAppBillingSupported = false;
    private IabHelper mHelper = null;
    private static String sPublicKey = null;
    private String mPurchaseSku = null;
    private String mPurchaseSubscriptionSku = null;
    RunOnUiThreadQueue mCallQueue = new RunOnUiThreadQueue();
    
    protected void onCreate()
    {
        if (Consts.DEBUG) Log.i( "InAppStoreActivity", "onCreate()" );

        // CheckBillingSupport() was called before creation completed - so test once again.
        if (sPublicKey != null) {
            CheckBillingSupportImpl(sPublicKey);
        }
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if (Consts.DEBUG) { Log.i( "InAppStoreActivity", "requestCode: " + requestCode + " resultCode: " + resultCode ); }
        if (mHelper != null)
            mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    protected void onDestroy()
    {
        // very important:
        if (Consts.DEBUG) Log.d(Consts.TAG, "Destroying helper.");
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public static InAppStore getInstance()
    {
        if (s_instance == null)
        {
            if (Consts.DEBUG) { Log.i( Consts.TAG, "getInstance() - creating instance"); }
            s_instance = new InAppStore();
            s_instance.onCreate();
            ClawActivityCommon.AddListener(s_instance);
        }
        return s_instance;
    }

    public static void release()
    {
        if (Consts.DEBUG) { Log.i( Consts.TAG, "release()"); }
        if( s_instance != null )
        {
            s_instance.onDestroy();
            ClawActivityCommon.RemoveListener(s_instance);
            s_instance = null;
        }
    }

    public static void initBilling()
    {
        if (Consts.DEBUG) { Log.i( Consts.TAG, "initBilling()"); }
        getInstance();
    }

    public static boolean IsAvailable()
    {
        if (s_instance != null) {
            return InAppStore.getInstance().mIsInAppBillingSupported;
        } else {
            return false;
        }
    }

    public static void CheckBillingSupport(final String publicKey)
    {
        if (Consts.DEBUG) Log.d(Consts.TAG, "CheckBillingSupport(): " + publicKey);
        sPublicKey = publicKey;

        if (!IsAvailable()) {
            InAppStore.getInstance().CheckBillingSupportImpl( publicKey );
        }
    }

    public static void CheckPendingTransactions()
    {
        if (Consts.DEBUG) Log.d(Consts.TAG, "CheckPendingTransactions()");
        if (IsAvailable()) {
            InAppStore.getInstance().CheckPendingTransactionsImpl();
        }
    }

    private void CheckPendingTransactionsImpl()
    {
        if (Consts.DEBUG) Log.d(Consts.TAG, "CheckPendingTransactionsImpl()");
        if(mHelper != null) {
            mCallQueue.enqueue( new Runnable() { public void run() {
                // Query whole inventory
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }});
        }
    }

    private void CheckBillingSupportImpl(final String publicKey)
    {
        if (Consts.DEBUG) Log.d(Consts.TAG, "CheckBillingSupportImpl(): " + publicKey);

        mHelper = new IabHelper(ClawActivityCommon.mActivity, publicKey);
        mHelper.enableDebugLogging(Consts.DEBUG);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Setup finished.");

                mIsInAppBillingSupported = result.isSuccess();
                nativeOnPurchaseSupport(mIsInAppBillingSupported);
                mCallQueue.setEnabled( true );

                if(Consts.DEBUG) {
                    if (mIsInAppBillingSupported) {
                        // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
                        Log.d(Consts.TAG, "Setup successful. You can now use billing system");
                    } else {
                        // Oh noes, there was a problem.
                        Log.e(Consts.TAG, "Problem setting up in-app billing: " + result);
                    }
                }
            }
        });
    }

    // Listener that's called when we finish querying the items we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (Consts.DEBUG) Log.d(Consts.TAG, "Query inventory finished.");
            if (result.isFailure()) {
                if (Consts.DEBUG) Log.e(Consts.TAG, "Failed to query inventory: " + result);
                return;
            }
            if (Consts.DEBUG) Log.d(Consts.TAG, "Query inventory was successful.");

            // Consume pending items
            if (inventory.getAllPurchases().size() > 0) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Pending items found - consume whole list.");
                mHelper.consumeAsync(inventory.getAllPurchases(), mConsumeInitialFinishedListener);
            } else {
                mCallQueue.callNext();
            }
        }
    };

    IabHelper.OnConsumeMultiFinishedListener mConsumeInitialFinishedListener = new IabHelper.OnConsumeMultiFinishedListener() {
        public void onConsumeMultiFinished(List<Purchase> purchases, List<IabResult> results) {
            for (int i = 0; i < purchases.size(); i++) {
                if(results.get(i).isSuccess()) {
                    nativeOnPurchaseRestore(purchases.get(i).getSku(), 1, purchases.get(i).getToken());
                } else {
                    if (Consts.DEBUG) Log.e(Consts.TAG, "Error while consuming item: " + purchases.get(i).getSku() + " result: " + results.get(i));
                }
            }
            mCallQueue.callNext();
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, final IabResult result) {
            if (Consts.DEBUG) Log.d(Consts.TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            final String sku = purchase != null ? purchase.getSku() : mPurchaseSku;
            final Handler handler = new Handler();
            final String token = purchase != null ? purchase.getToken() : null;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (result.isSuccess()) {
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Consumption successfull. Purchase completed.");
                        nativeOnPurchaseComplete(sku, 1, token);
                    } else if (result.getResponse() == IabHelper.IABHELPER_USER_CANCELLED || result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_USER_CANCELED) {
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Consumption canceled");
                        nativeOnPurchaseCancel(sku, 1);
                    } else {
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Consumption failed");
                        nativeOnPurchaseFail(sku, 1);
                    }
                }
            }, 250);
            mCallQueue.callNext();
        }
    };

    public static void CheckSubscription(final String subscriptionId)
    {
        if (Consts.DEBUG) Log.d(Consts.TAG, "CheckSubscription() " + subscriptionId);
        if (IsAvailable()) {
            InAppStore.getInstance().CheckSubscriptionImpl(subscriptionId);
        }
    }

    private void CheckSubscriptionImpl(final String subscriptionId)
    {
        if (Consts.DEBUG) Log.d(Consts.TAG, "CheckSubscriptionImpl()");
        if(mHelper != null) {
            mCallQueue.enqueue( new Runnable() { public void run() {

                IabHelper.QueryInventoryFinishedListener subscriptionListener = new IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Query subscription finished.");
                        if (result.isFailure()) {
                            if (Consts.DEBUG) Log.e(Consts.TAG, "Failed to query subscription: " + result);
                            mCallQueue.callNext();
                            return;
                        }
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Query subscription was successful.");

                        Purchase subscriptionPurchase = inventory.getPurchase(subscriptionId);
                        if (Consts.DEBUG) Log.d(Consts.TAG, subscriptionId + " subscription status: " + (subscriptionPurchase != null));
                        String token = (subscriptionPurchase==null)?(null):(subscriptionPurchase.getToken());
                        nativeOnSubscriptionStatus( subscriptionId, subscriptionPurchase != null, token );
                        
                        mCallQueue.callNext();
                    }
                };

                mHelper.querySubscriptionAsync(subscriptionListener);
            }});
        }
    }

    public static void PurchaseProduct(final String productId, final int quantity)
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "PurchaseProduct(): " + productId + " quantity: " + quantity);
        if (IsAvailable()) {
            InAppStore.getInstance().PurchaseProductImpl( productId, quantity );
        }
    }

    private void PurchaseProductImpl(final String productId, final int quantity)
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "PurchaseProductImpl(): " + productId + " quantity: " + quantity);
        mPurchaseSku = productId;
        mCallQueue.enqueue( new Runnable() { public void run() {
            mHelper.launchPurchaseFlow(ClawActivityCommon.mActivity, mPurchaseSku, Consts.RC_REQUEST, mPurchaseFinishedListener);
        }});
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(Consts.TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            String sku = purchase != null ? purchase.getSku() : mPurchaseSku;
            if (result.isSuccess()) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Purchase successfull. Starting consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if(result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Item already owned. Consume it.");
                try {
                    final Inventory inv = mHelper.queryInventory(false, null);
                    final Purchase ownedPurchase = inv.getPurchase(sku);
                    if (Consts.DEBUG) Log.d(Consts.TAG, "Getting purchase of owned item sku: " + sku + " purchase: " + ownedPurchase);
                    if (ownedPurchase == null) {
                        throw new Exception( "Could not obtain purchase for sku: " + sku + " from inventory!" );
                    }
                    ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
                        mHelper.consumeAsync(ownedPurchase, mConsumeFinishedListener);
                    }});
                } catch (Exception ex) {
                    if (Consts.DEBUG) Log.e(Consts.TAG, ex.getMessage());
                    nativeOnPurchaseFail(sku, 1);
                    mCallQueue.callNext();
                }
            } else if (result.getResponse() == IabHelper.IABHELPER_USER_CANCELLED || result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_USER_CANCELED) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Purchase canceled");
                nativeOnPurchaseCancel(sku, 1);
                mCallQueue.callNext();
            } else {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Purchase failed");
                nativeOnPurchaseFail(sku, 1);
                mCallQueue.callNext();
            }
        }
    };

    public static void PurchaseSubscription(final String subscriptionId)
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "PurchaseSubscription(): " + subscriptionId);
        if (IsAvailable()) {
            InAppStore.getInstance().PurchaseSubscriptionImpl( subscriptionId );
        }
    }

    private void PurchaseSubscriptionImpl(final String subscriptionId)
    {
        if (Consts.DEBUG) Log.i(Consts.TAG, "PurchaseSubscriptionImpl(): " + subscriptionId);

        //We try to buy the subscription only if user has not bought it already
        //(API should return ALREADY_OWNED response in such case, but in case of a single account
        //on mutliple devices, it returns UNKNOWN_ERROR so we must check it manually)
        if(mHelper != null) {
            mCallQueue.enqueue( new Runnable() { public void run() {

                IabHelper.QueryInventoryFinishedListener subscriptionListener = new IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Pre-purchase subscription check finished.");

                        Purchase subscriptionPurchase = (inventory == null) ? null : inventory.getPurchase(subscriptionId);
                        String token = (subscriptionPurchase==null)?(null):(subscriptionPurchase.getToken());
                        if (Consts.DEBUG) Log.d(Consts.TAG, "Pre-purchase subscription check status: " + (subscriptionPurchase != null));
                        if (subscriptionPurchase != null) {
                            nativeOnSubscriptionAlreadyOwned( subscriptionPurchase.getSku(), token );
                            mCallQueue.callNext();
                        } else {
                            mPurchaseSubscriptionSku = subscriptionId;
                            mHelper.launchSubscriptionPurchaseFlow(ClawActivityCommon.mActivity, mPurchaseSubscriptionSku, Consts.RC_REQUEST, mSubscriptionFinishedListener);
                        }
                    }
                };

                mHelper.querySubscriptionAsync(subscriptionListener);
            }});
        }
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mSubscriptionFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(Consts.TAG, "Subscription purchase finished: " + result + ", purchase: " + purchase);

            String sku = purchase != null ? purchase.getSku() : mPurchaseSubscriptionSku;
            String token = (purchase==null)?(null):(purchase.getToken());
            if (result.isSuccess()) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Subscription purchase successfull.");
                nativeOnSubscriptionBought(sku, token);
            } else if(result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Subscription already owned.");
                nativeOnSubscriptionAlreadyOwned(sku, token);
            } else if (result.getResponse() == IabHelper.IABHELPER_USER_CANCELLED || result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_USER_CANCELED) {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Subscription purchase canceled");
                nativeOnSubscriptionCancel(sku);
            } else {
                if (Consts.DEBUG) Log.d(Consts.TAG, "Subscription purchase failed");
                nativeOnSubscriptionFail(sku);
            }
            mCallQueue.callNext();
        }
    };

    // JNI methods:
    private static native void nativeOnPurchaseComplete(String code, int quantity, String token);
    private static native void nativeOnPurchaseRestore(String code, int quantity, String token);
    private static native void nativeOnPurchaseFail(String code, int quantity);
    private static native void nativeOnPurchaseCancel(String code, int quantity);
    private static native void nativeOnPurchaseRefund(String code, int quantity);
    private static native void nativeOnPurchaseSupport(boolean supported);
    private static native void nativeOnSubscriptionStatus(String subscriptionId, boolean active, String token);
    private static native void nativeOnSubscriptionBought(String subscriptionId, String token);
    private static native void nativeOnSubscriptionAlreadyOwned(String subscriptionId, String token);
    private static native void nativeOnSubscriptionCancel(String subscriptionId);
    private static native void nativeOnSubscriptionFail(String subscriptionId);
}

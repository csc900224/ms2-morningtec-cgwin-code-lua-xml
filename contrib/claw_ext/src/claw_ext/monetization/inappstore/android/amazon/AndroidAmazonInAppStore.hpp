//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/android/amazon/AndroidAmazonInAppStore.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_ANDROID_AMAZON_IN_APP_STORE_HPP__
#define __NETWORK_ANDROID_AMAZON_IN_APP_STORE_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

namespace ClawExt
{
    class AndroidAmazonInAppStore : public InAppStore
    {
    public:
#if defined IAP_SERVER_VERIFICATION
                            AndroidAmazonInAppStore( Claw::VfsMount* mount, const char* fileName );
#else
                            AndroidAmazonInAppStore();
#endif
                            ~AndroidAmazonInAppStore();

        virtual bool        CheckBillingSupport( const Key* key = NULL, Message* reason = 0 );
        virtual bool        CheckPendingTransactions( Message* reason = 0 ) {}
        virtual bool        BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity = 1, Message* reason = 0 );
        virtual bool        BuySubscriptionInternal( const Claw::NarrowString& id );
        virtual bool        CheckSubscriptionInternal( const Claw::NarrowString& id );

        // Public wrappers for JNI methods access.
        void                RetreiveTransactionComplete( const InAppTransaction& transaction );
        void                RetreiveTransactionRestore( const InAppTransaction& transaction );
        void                RetreiveTransactionFail( const InAppTransaction& transaction );
        void                RetreiveTransactionCancel( const InAppTransaction& transaction );
        void                RetreiveTransactionRefund( const InAppTransaction& transaction );
        void                RetreiveTransactionSupport( bool supported );

    }; // class AndroidAmazonInAppStore
} // namespace ClawExt
#endif // !defined __NETWORK_ANDROID_AMAZON_IN_APP_STORE_HPP__
// EOF

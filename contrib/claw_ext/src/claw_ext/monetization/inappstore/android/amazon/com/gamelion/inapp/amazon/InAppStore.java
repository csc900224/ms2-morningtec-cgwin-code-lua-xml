package com.gamelion.inapp.amazon;

import android.util.Log;
import android.os.Handler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.amazon.inapp.purchasing.BasePurchasingObserver;
import com.amazon.inapp.purchasing.GetUserIdResponse;
import com.amazon.inapp.purchasing.GetUserIdResponse.GetUserIdRequestStatus;
import com.amazon.inapp.purchasing.Item;
import com.amazon.inapp.purchasing.ItemDataResponse;
import com.amazon.inapp.purchasing.Offset;
import com.amazon.inapp.purchasing.PurchaseResponse;
import com.amazon.inapp.purchasing.PurchaseUpdatesResponse;
import com.amazon.inapp.purchasing.PurchasingManager;
import com.amazon.inapp.purchasing.Receipt;
import com.amazon.inapp.purchasing.SubscriptionPeriod;

import com.Claw.Android.ClawActivityCommon;

public class InAppStore extends BasePurchasingObserver
{
    private static InAppStore s_instance;
    private boolean mBilingSupport = false;
    private boolean mInitInProgress = false;
    private boolean mBilingSupportDelayed = false;
    private HashMap<String, String> mPendingSkuMap = new HashMap<String, String>();
    
    private final String RESTART_PURCHASE_TAG = "WaitingForSdkAvailabity";

    public static InAppStore getInstance()
    {
        if (s_instance == null) {
            s_instance = new InAppStore();
            if (Consts.DEBUG) { Log.i(Consts.TAG, "Amazon InAppStore create instance"); }
            s_instance.initBilling();
        }
        return s_instance;
    }
    
    private InAppStore() {
        super( ClawActivityCommon.mActivity );
    }
    
    @Override
    public void onSdkAvailable(final boolean isSandboxMode) {
        if (Consts.DEBUG) Log.i(Consts.TAG, "onSdkAvailable() isSandboxMode: " + isSandboxMode);
        mBilingSupport = true;
        mInitInProgress = false;
        if( mBilingSupportDelayed ) {
            mBilingSupportDelayed = false;
            checkBillingSupportImpl();
        }
        
        restoreTransactions();
        
        String restartPurchaseProductId = mPendingSkuMap.get( RESTART_PURCHASE_TAG );
        if( restartPurchaseProductId != null ) {
            mPendingSkuMap.remove( RESTART_PURCHASE_TAG );
            purchaseProductImp( restartPurchaseProductId, 1 );
        }
    }
    
    @Override
    public void onPurchaseResponse(final PurchaseResponse purchaseResponse) {
        if (Consts.DEBUG) Log.i(Consts.TAG, "onPurchaseResponse() status: " + purchaseResponse.getPurchaseRequestStatus());
        
        String productId = mPendingSkuMap.get( purchaseResponse.getRequestId() );
        if( productId == null ) {
            productId = "NULL";
        }
        
        switch( purchaseResponse.getPurchaseRequestStatus() ) {
            case SUCCESSFUL:
                nativeOnPurchaseComplete( purchaseResponse.getReceipt().getSku() , 1 );
                break;
            default:
                nativeOnPurchaseFail( productId, 1 );
                break;
        };
        
        mPendingSkuMap.remove( purchaseResponse.getRequestId() );
    }
    
    @Override
    public void onPurchaseUpdatesResponse(PurchaseUpdatesResponse purchaseUpdatesResponse) {
        if (Consts.DEBUG) Log.i(Consts.TAG, "onPurchaseUpdatesResponse() status: " + purchaseUpdatesResponse.getPurchaseUpdatesRequestStatus());

        for (final String sku : purchaseUpdatesResponse.getRevokedSkus()) {
            if (Consts.DEBUG) Log.i(Consts.TAG, "revoked SKU: " + sku);
            nativeOnPurchaseRefund( sku, 1 );
        }
            
        if( purchaseUpdatesResponse.getPurchaseUpdatesRequestStatus() == PurchaseUpdatesResponse.PurchaseUpdatesRequestStatus.SUCCESSFUL ) {
            for (final Receipt receipt : purchaseUpdatesResponse.getReceipts()) {
                nativeOnPurchaseRestore( receipt.getSku(), 1 );
            }
                
            final Offset newOffset = purchaseUpdatesResponse.getOffset();
            if( purchaseUpdatesResponse.isMore() ) {
                if (Consts.DEBUG) Log.i(Consts.TAG, "Initiating Another Purchase Updates with offset: " + newOffset.toString());
                PurchasingManager.initiatePurchaseUpdatesRequest(newOffset);
            }
        }
    }
        
    private void initBilling() {
        if (Consts.DEBUG) Log.i(Consts.TAG, "initBilling()" );
        mInitInProgress = true;
        PurchasingManager.registerObserver( this );
        
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if( mInitInProgress ) {
                        if (Consts.DEBUG) Log.e(Consts.TAG, "Initialization Timeout!");
                        mInitInProgress = false;
                        if( mBilingSupportDelayed ) {
                            mBilingSupportDelayed = false;
                            checkBillingSupportImpl();
                        }
                    }
                }
            }, 3000); // 3 seconds timeout for sdk initailization
        }});
    }    
    
    public static void CheckBillingSupport() {
        if (Consts.DEBUG) Log.i(Consts.TAG, "CheckBillingSupport()" );
        getInstance().checkBillingSupportImpl();
    }
    
    private void checkBillingSupportImpl() {
        if( !mInitInProgress )
        {
            if (Consts.DEBUG) Log.i(Consts.TAG, "checkBillingSupportImpl(): " + mBilingSupport );
            nativeOnPurchaseSupport( mBilingSupport );
        }
        else
        {
            mBilingSupportDelayed = true;
        }
    }
    
    public static void PurchaseProduct(final String productId, final int quantity) {
        if (Consts.DEBUG) Log.i(Consts.TAG, "PurchaseProduct()" );
        getInstance().purchaseProductImp( productId, quantity );
    }
    
    private void purchaseProductImp(final String productId, final int quantity) {
        if( mBilingSupport ) {
            if (Consts.DEBUG) Log.i(Consts.TAG, "purchaseProductImp()" );
            String requestId = PurchasingManager.initiatePurchaseRequest( productId );
            mPendingSkuMap.put( requestId, productId );
        } else {
            // Wait for billing activation
            mPendingSkuMap.put( RESTART_PURCHASE_TAG, productId );
        }        
    }
    
    private void restoreTransactions() {
        if( mBilingSupport ) {
            if (Consts.DEBUG) Log.i(Consts.TAG, "restoreTransactions()" );
            PurchasingManager.initiatePurchaseUpdatesRequest( Offset.BEGINNING );
        }
    }
    
    // JNI methods:
    private static native void nativeOnPurchaseComplete(String code, int quantity);
    private static native void nativeOnPurchaseRestore(String code, int quantity);
    private static native void nativeOnPurchaseFail(String code, int quantity);
    private static native void nativeOnPurchaseCancel(String code, int quantity);
    private static native void nativeOnPurchaseRefund(String code, int quantity);
    private static native void nativeOnPurchaseSupport(boolean supported);
}

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/android/amazon/AndroidAmazonInAppStore.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/inappstore/android/amazon/AndroidAmazonInAppStore.hpp"

// External includes
#include "claw/system/android/JniAttach.hpp"

static ClawExt::AndroidAmazonInAppStore* s_instance = NULL;

namespace ClawExt
{

#if defined IAP_SERVER_VERIFICATION
    AndroidAmazonInAppStore::AndroidAmazonInAppStore( Claw::VfsMount* mount, const char* fileName )
        : InAppStore( mount, fileName )
    {
        s_instance = this;
    }
#else
    AndroidAmazonInAppStore::AndroidAmazonInAppStore()
    {
        s_instance = this;
    }
#endif

    AndroidAmazonInAppStore::~AndroidAmazonInAppStore()
    {
        s_instance = NULL;
    }

    bool AndroidAmazonInAppStore::CheckBillingSupport( const Key* key, Message* message )
    {
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/amazon/InAppStore", "CheckBillingSupport", "()V" );

        Claw::JniAttach::Detach( attached );
        return true;
    }

    bool AndroidAmazonInAppStore::BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity /* = 1 */, Message* reason /* = 0 */ )
    {
        CLAW_MSG_ASSERT( quantity == 1, "Buying more than one product isn't supported yet." );
        if( quantity != 1 ) return false;

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring productId = Claw::JniAttach::GetStringFromChars( env, id.c_str() );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/inapp/amazon/InAppStore", "PurchaseProduct", "(Ljava/lang/String;I)V", productId, quantity );

        Claw::JniAttach::ReleaseString( env, productId );
        Claw::JniAttach::Detach( attached );
        return true;
    }

    bool AndroidAmazonInAppStore::BuySubscriptionInternal( const Claw::NarrowString& id )
    {
        CLAW_MSG_ASSERT(false, "Amazon Subscriptions are not supported");
        return false;
    }

    bool AndroidAmazonInAppStore::CheckSubscriptionInternal( const Claw::NarrowString& id )
    {
        CLAW_MSG_ASSERT(false, "Amazon Subscriptions are not supported");
        return false;
    }

    void AndroidAmazonInAppStore::RetreiveTransactionComplete( const InAppTransaction& transaction )
    {
        NotifyTransactionComplete( transaction );
    }

    void AndroidAmazonInAppStore::RetreiveTransactionRestore( const InAppTransaction& transaction )
    {
        NotifyTransactionRestore( transaction );
    }

    void AndroidAmazonInAppStore::RetreiveTransactionFail( const InAppTransaction& transaction )
    {
        NotifyTransactionFailed( transaction );
    }

    void AndroidAmazonInAppStore::RetreiveTransactionCancel( const InAppTransaction& transaction )
    {
        NotifyTransactionCancel( transaction );
    }

    void AndroidAmazonInAppStore::RetreiveTransactionRefund( const InAppTransaction& transaction )
    {
        NotifyTransactionRefund( transaction );
    }

    void AndroidAmazonInAppStore::RetreiveTransactionSupport( bool supported )
    {
        NotifyTransactionSupport( supported );
    }
} // namespace ClawExt

extern "C"
{
    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_amazon_InAppStore_nativeOnPurchaseComplete(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseSuccessful " << id );
        s_instance->RetreiveTransactionComplete( ClawExt::InAppTransaction( id, quantity ) );
    
        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_amazon_InAppStore_nativeOnPurchaseRestore(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseRestore " << id );
        s_instance->RetreiveTransactionRestore( ClawExt::InAppTransaction( id, quantity ) );
    
        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_amazon_InAppStore_nativeOnPurchaseFail(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseFail: " << id );
        s_instance->RetreiveTransactionFail( ClawExt::InAppTransaction( id, quantity ) );
    
        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_amazon_InAppStore_nativeOnPurchaseCancel(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseCancel: " << id );
        s_instance->RetreiveTransactionCancel( ClawExt::InAppTransaction( id, quantity ) );
    
        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_amazon_InAppStore_nativeOnPurchaseRefund(JNIEnv * env, jclass thiz, jstring productId, jint quantity)
    {
        const char *nativeString = env->GetStringUTFChars(productId, 0);

        ClawExt::InAppProduct::Id id( nativeString );
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseRefund: " << id );
        s_instance->RetreiveTransactionRefund( ClawExt::InAppTransaction( id, quantity ) );

        env->ReleaseStringUTFChars(productId, nativeString);
    }

    JNIEXPORT void JNICALL
    Java_com_gamelion_inapp_amazon_InAppStore_nativeOnPurchaseSupport(JNIEnv * env, jclass thiz, jboolean supported)
    {
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "nativeOnPurchaseSupport: " << supported );
        s_instance->RetreiveTransactionSupport( supported );
    }

}; // extern "C"

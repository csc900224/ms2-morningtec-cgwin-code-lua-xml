//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/InAppProductsDb.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_PRODUCTS_DB_HPP__
#define __NETWORK_IN_APP_PRODUCTS_DB_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

#include "claw/base/String.hpp"

// External includes
#include <vector>
#include <list>

namespace ClawExt
{
    class InAppProductsDb
    {
    public:
        // Forward declarations
        class ProductsObserver;

        // Products' identifiers list
        typedef ::std::vector<InAppProduct::Id>     ProductsIds;
        // Products' informations list
        typedef ::std::vector<InAppProduct>         Products;
        // Message container type
        typedef Claw::NarrowString                  Message;

        //! Virtual destructor for derived classes.
        virtual             ~InAppProductsDb()       {};

        //! Ask for all available products ids.
        /**
        * Calls ProductsObserver::RetreiveProductIds asynchronously after retreiving product list.
        * List may be read from disk, downloaded from server or even build in the bundle,
        * depending on implementation.
        * \param reason message to be delivered to the used if query fails
        * \return immediately false if query can not be performed for some reasons (no network,etc.).
        */
        virtual bool        QueryProductsIds( Message* reason = 0 ) = 0;

        //! Get specified product info.
        /**
        * Retreive product localized information from a server or other external resources.
        * Calls ProductsObserver::RetreiveProductInfo on all registered listeners asynchronously.
        * \param reason optional message delivered to the used if query fails
        * \return false if query can not be performed for some reasons (no network,etc.).
        */
        virtual bool        QueryProductInfo( const InAppProduct::Id& id, Message* reason = 0 );

        //! Get information about products specified via ids list.
        /**
        * Simmilar to QueryProductInfo, but tries to retreive information about few products specified
        * on the ids list.
        */
        virtual bool        QueryProductsInfos( const ProductsIds& ids, Message* reason = 0 ) = 0;

        //! Register store transactions observer.
        /**
        * \return false if observer was already registered, true if successfully registered.
        */
        bool                RegisterProdObserver( ProductsObserver* observer );

        //! Unregister transactions observer.
        /**
        * \return false if observer was not registered to this store.
        */
        bool                UnregisterProdObserver( ProductsObserver* observer );

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate plaftorm InAppStore object implementation.
        */
        static InAppProductsDb*  QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void         Release( InAppProductsDb* store );

        class ProductsObserver
        {
        public:
            virtual void    RetreiveProductsIds( const ProductsIds& ids )           = 0;
            virtual void    RetreiveProductsInfos( const Products& products )       = 0;
        }; // class ProductsObserver

    protected:
        void                NotifyProductsIds( const ProductsIds& ids );
        void                NotifyProductsInfos( const Products& products );

        typedef std::list<ProductsObserver*>        ProdObservers;
        typedef ProdObservers::iterator             ProdObserversIt;
        typedef ProdObservers::const_iterator       ProdObserversConstIt;

        ProdObservers       m_prodObservers;

    }; // class InAppProductsDb
} // namespace ClawExt
#endif // !defined __NETWORK_IN_APP_PRODUCTS_DB_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDb.mm
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import "claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDbImpl.h"

#include "claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDb.hpp"

#include "claw/base/Errors.hpp"

#import <StoreKit/StoreKit.h>

namespace ClawExt
{
    static IPhoneBuildInProductsDbImpl* m_impl = NULL;

    IPhoneBuildInProductsDb::IPhoneBuildInProductsDb()
    {
        CLAW_MSG_ASSERT( !m_impl, "Using Objective-C PIMPL doesn't allow for multiple object instances (impl static)." );

        if( !m_impl )
            m_impl = [[IPhoneBuildInProductsDbImpl alloc] initWithObserver: this];
    }

    IPhoneBuildInProductsDb::~IPhoneBuildInProductsDb()
    {
        [m_impl dealloc];
        m_impl = NULL;
    }

    bool IPhoneBuildInProductsDb::QueryProductsIds( Message* reason /* = 0 */ )
    {
        *reason = "Build in products database assumes precompiled and already known products' ids";
        return false;
    }

    bool IPhoneBuildInProductsDb::QueryProductsInfos( const ProductsIds& ids, Message* reason /* = 0 */ )
    {
        NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:ids.size()];
        ProductsIds::const_iterator it = ids.begin();
        ProductsIds::const_iterator end = ids.end();
        for( ; it != end; ++it )
        {
            [arr addObject: [NSString stringWithUTF8String:(*it).c_str()] ];
        }
        return [m_impl queryProductsInfos:arr];
    }

    void IPhoneBuildInProductsDb::RetreiveProductsIds( const ProductsIds& ids )
    {
        NotifyProductsIds( ids );
    }

    void IPhoneBuildInProductsDb::RetreiveProductsInfos( const Products& products )
    {
        NotifyProductsInfos( products );
    }
}

// EOF

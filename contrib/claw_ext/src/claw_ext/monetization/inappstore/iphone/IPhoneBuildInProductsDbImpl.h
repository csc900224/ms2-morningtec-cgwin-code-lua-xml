//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDbImpl.h
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

namespace ClawExt
{
    class IPhoneBuildInProductsDb;
} // namespace ClawExt

@interface IPhoneBuildInProductsDbImpl : NSObject<SKProductsRequestDelegate> 
{
@private
    // Instance variables
    ClawExt::IPhoneBuildInProductsDb* observer;
}

// Constructor
- (id) initWithObserver:(ClawExt::IPhoneBuildInProductsDb*) observer;

// Helper
- (void) setObserver:(ClawExt::IPhoneBuildInProductsDb*) observer;

// Methods
- (bool) queryProductsIds;

- (bool) queryProductsInfos:(NSArray*)productsIds;

@end
// EOF

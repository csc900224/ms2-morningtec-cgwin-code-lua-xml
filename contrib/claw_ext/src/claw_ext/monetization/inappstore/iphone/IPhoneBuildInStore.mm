//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInStore.mm
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import "claw_ext/monetization/inappstore/iphone/IPhoneBuildInStoreImpl.h"

#include "claw_ext/monetization/inappstore/iphone/IPhoneBuildInStore.hpp"

#include "claw/base/Errors.hpp"

namespace ClawExt
{
    static IPhoneBuildInStoreImpl* m_impl = NULL;

#ifdef IAP_SERVER_VERIFICATION
    IPhoneBuildInStore::IPhoneBuildInStore( Claw::VfsMount* mount, const char* fileName )
        : InAppStore( mount, fileName )
    {
        CLAW_MSG_ASSERT( !m_impl, "Using Objective-C PIMPL doesn't allow for multiple object instances (impl static)." );

        if( !m_impl )
            m_impl = [[IPhoneBuildInStoreImpl alloc] initWithObserver: this];
    }
#endif

    IPhoneBuildInStore::IPhoneBuildInStore()
        : InAppStore()
    {
        CLAW_MSG_ASSERT( !m_impl, "Using Objective-C PIMPL doesn't allow for multiple object instances (impl static)." );

        if( !m_impl )
            m_impl = [[IPhoneBuildInStoreImpl alloc] initWithObserver: this];
    }

    IPhoneBuildInStore::~IPhoneBuildInStore()
    {
        [m_impl dealloc];
        m_impl = NULL;
    }

    bool IPhoneBuildInStore::CheckBillingSupport( const Key* key /*= NULL*/, Message* reason /* = 0 */ )
    {
        // On iphone it may be checked synchronously, call notifier immediatelly
        bool supported = [m_impl checkBillingSupport];
        NotifyTransactionSupport( supported );
        return true;
    }

    bool IPhoneBuildInStore::CheckPendingTransactions( Message* reason /* = 0 */ )
    {
        return true;
    }

    bool IPhoneBuildInStore::BuyProductInternal( const InAppProduct::Id& productId, const InAppProduct::Quantity& quantity /* =1 */, Message* reason /* =0 */ )
    {
        NSString* idStr = [NSString stringWithUTF8String:productId.c_str()];
        return [m_impl buyProduct:idStr amount:quantity];
    }

    bool IPhoneBuildInStore::BuySubscriptionInternal( const Claw::NarrowString& sId )
    {
        return [m_impl buySubscription:[NSString stringWithUTF8String:sId.c_str()]];
    }

    bool IPhoneBuildInStore::CheckSubscriptionInternal( const Claw::NarrowString& sId )
    {
        [m_impl checkSubscription:[NSString stringWithUTF8String:sId.c_str()]];
        return true;
    }

    void IPhoneBuildInStore::RestorePurchases()
    {
        [m_impl restorePurchases];
    }

    void IPhoneBuildInStore::RetreiveTransactionFail( const InAppTransaction& trans )
    {
        NotifyTransactionFailed( trans );
    }

    void IPhoneBuildInStore::RetreiveTransactionCancel( const InAppTransaction& trans )
    {
        NotifyTransactionCancel( trans );
    }

    void IPhoneBuildInStore::RetreiveTransactionComplete( const InAppTransaction& trans )
    {
        NotifyTransactionComplete( trans );
    }

    void IPhoneBuildInStore::RetreiveTransactionRestore( const InAppTransaction& trans )
    {
        NotifyTransactionRestore( trans );
    }

    void IPhoneBuildInStore::RetreiveTransactionRefund( const InAppTransaction& trans )
    {
        NotifyTransactionRefund( trans );
    }

    void IPhoneBuildInStore::RetrieveSubscriptionStatus( const Claw::NarrowString& sId, bool status, const unsigned char* data, unsigned int len )
    {
        NotifySubscriptionStatus(sId, status, data, len);
    }

    void IPhoneBuildInStore::RetrieveSubscriptionBought( const Claw::NarrowString& sId, const unsigned char* data, unsigned int len )
    {
        NotifySubscriptionBought( sId, data, len );
    }

    void IPhoneBuildInStore::RetrieveSubscriptionFailed( const Claw::NarrowString& sId )
    {
        NotifySubscriptionFailed( sId );
    }

    void IPhoneBuildInStore::RetrieveSubscriptionCancel( const Claw::NarrowString& sId )
    {
        NotifySubscriptionCancel( sId );
    }


}

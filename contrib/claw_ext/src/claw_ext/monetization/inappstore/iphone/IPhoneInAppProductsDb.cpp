#include "claw_ext/monetization/inappstore/InAppProductsDb.hpp"
#include "claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDb.hpp"

namespace ClawExt
{
    InAppProductsDb* InAppProductsDb::QueryInterface()
    {
        return new IPhoneBuildInProductsDb();
    }

    void InAppProductsDb::Release( InAppProductsDb* instance )
    {
        delete instance;
    }
}

// EOF

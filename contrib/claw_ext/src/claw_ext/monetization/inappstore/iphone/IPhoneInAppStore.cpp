#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/inappstore/iphone/IPhoneBuildInStore.hpp"

namespace ClawExt
{
#ifdef IAP_SERVER_VERIFICATION
    InAppStore* InAppStore::QueryInterface( Claw::VfsMount* mount, const char* fileName, SystemType type )
    {
        if( type == ST_DEFAULT )
            return new IPhoneBuildInStore( mount, fileName );
        else
            return NULL;
    }
#else
    InAppStore* InAppStore::QueryInterface( InAppStore::SystemType type )
    {
        if( type == ST_DEFAULT )
            return new IPhoneBuildInStore();
        else
            return NULL;
    }
#endif

    void InAppStore::Release( InAppStore* instance )
    {
        delete instance;
    }
}

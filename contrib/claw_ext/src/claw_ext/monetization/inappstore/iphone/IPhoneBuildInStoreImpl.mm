//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInStoreImpl.mm
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import  "claw_ext/monetization/inappstore/iphone/IPhoneBuildInStoreImpl.h"
#include "claw_ext/monetization/inappstore/iphone/IPhoneBuildInStore.hpp"
#include "claw/application/Application.hpp"

#import <UIKit/UIAlertView.h>

#define MAX_PRODUCT_ID_LENGTH 256

@implementation IPhoneBuildInStoreImpl
@synthesize m_buySubscriptions;
@synthesize m_keychain;
@synthesize m_iaps;
@synthesize m_restore;

// Constructor
- (id) init
{
    if( (self = [super init]) )
    {
        [self setObserver: nil];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        m_buySubscriptions = [[NSMutableArray array] retain];
        [self initKeychain];
        m_restore = nil;
    }
    return self;
}

- (id) initWithObserver:(ClawExt::IPhoneBuildInStore*) obs
{
    if( (self = [super init]) )
    {
        [self setObserver: obs];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        m_buySubscriptions = [[NSMutableArray array] retain];
        [self initKeychain];
        m_restore = nil;
    }
    return self;        
}

- (void)dealloc
{
    [m_buySubscriptions release];
    m_buySubscriptions = nil;
    [self deallocKeychain];
    [super dealloc];
}

- (void)initKeychain
{
    m_keychain = [[[KeychainItemWrapper alloc] initWithIdentifier:[NSString stringWithUTF8String:Claw::IPhoneApplication::GetInstance()->GetPackageName().c_str()] accessGroup:nil] retain];
    NSString* store = [m_keychain objectForKey:(id)kSecValueData];
    id obj = [NSPropertyListSerialization propertyListWithData:[store dataUsingEncoding:NSUTF8StringEncoding] options:0 format:nil error:nil];
    m_iaps = [[NSMutableDictionary dictionaryWithDictionary:obj] retain];
}

- (void) deallocKeychain
{
    [m_keychain release];
    m_keychain = nil;
}

- (NSData*) getReceipt:(NSString*)sId
{
    NSString* ret = [m_iaps objectForKey:sId];
    if( ret )
    {
        return [ret dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}

- (void) setReceipt:(NSData*) receipt for:(NSString*)sId
{
    NSString* str = [[[NSString alloc] initWithData:receipt encoding:NSUTF8StringEncoding] autorelease];
    [m_iaps setObject:str forKey:sId];

    NSData* data = [NSPropertyListSerialization dataWithPropertyList:m_iaps format:NSPropertyListXMLFormat_v1_0 options:0 error:nil];
    if( data )
    {
        NSString* store = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        [m_keychain setObject:store forKey:(id)kSecValueData];
    }
}

- (void) setObserver:(ClawExt::IPhoneBuildInStore*) obs
{
    observer = obs;
}

- (bool) queryProductsIds
{
    return false;
}

- (bool) queryProductsInfos:(NSArray*)productsIds
{
    return false;
}

- (bool) checkBillingSupport
{
    return [SKPaymentQueue canMakePayments];
}

- (bool) buyProduct:(NSString*)productId amount:(NSUInteger)quantity
{
    // Check parental control
    if( ![SKPaymentQueue canMakePayments] )
    {
        UIAlertView* popup = [[UIAlertView alloc] 
                              initWithTitle:NSLocalizedString(@"In-App Purchasing disabled", @"") 
                              message:NSLocalizedString(@"Check your parental control settings and try again later", @"")
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")
                              otherButtonTitles:nil];
        [popup show];
        [popup release];

        return false;
    }
    // Add payment transaction to queue
    else
    {
        if( quantity > 1 )
        {
            SKMutablePayment* pay = [SKMutablePayment paymentWithProductIdentifier:productId];
            pay.quantity = quantity;
            [[SKPaymentQueue defaultQueue] addPayment:pay];

        }
        else
        {
            SKPayment* pay = [SKPayment paymentWithProductIdentifier:productId];
            [[SKPaymentQueue defaultQueue] addPayment:pay];
        }
        return true;
    }
}

// Derived from SKPaymentTransactionObserver
- (void) paymentQueue:(SKPaymentQueue*)queue updatedTransactions:(NSArray*)transactions
{
    for ( SKPaymentTransaction* trans in transactions ) 
    {
        switch (trans.transactionState)
        {
            case SKPaymentTransactionStateFailed:
                [self transactionFailed:trans];
                break;
            case SKPaymentTransactionStateRestored:
                [self transactionRestored:trans];
                break;
            case SKPaymentTransactionStatePurchased:
                [self transactionCompleted:trans];
                break;
            case SKPaymentTransactionStatePurchasing:
            default:
                // Remove transaction from the payment queue, nothing else will be done with it
                //[[SKPaymentQueue defaultQueue] finishTransaction: trans];
                break;
        }
    }
}

- (void) paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [m_restore release];
    m_restore = nil;
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
//    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    for( NSString* iap in m_restore.allKeys )
    {
        NSData* receipt = [[m_restore objectForKey:iap] objectForKey:@"receipt"];
        const unsigned char* data = (const unsigned char*)([receipt bytes]);

        int quantity = [[[m_restore objectForKey:iap] objectForKey:@"quantity"] intValue];
        observer->RetreiveTransactionRestore(
            ClawExt::InAppTransaction( [iap UTF8String], quantity, data, [receipt length] ) );
        [self setReceipt:receipt for:iap];
    }

    [m_restore release];
    m_restore = nil;
//    [pool drain];
//    [pool release];
}

- (bool) buySubscription:(NSString*) sId
{
    [m_buySubscriptions addObject:sId];
    return [self buyProduct:sId amount:1];
}

// Transaction handlers
- (void) transactionCompleted:(SKPaymentTransaction*)transaction
{
    char productId[MAX_PRODUCT_ID_LENGTH];
    [transaction.payment.productIdentifier getCString: productId maxLength: MAX_PRODUCT_ID_LENGTH encoding:NSASCIIStringEncoding];

    assert( observer );
    NSData* receipt = transaction.transactionReceipt;
    const unsigned char* data = (const unsigned char*)([receipt bytes]);

    NSUInteger idx = [m_buySubscriptions indexOfObject:transaction.payment.productIdentifier];
    if( idx == NSNotFound )
    {
        observer->RetreiveTransactionComplete( ClawExt::InAppTransaction(
            productId, transaction.payment.quantity, data, [receipt length]) );
    }
    else
    {
        observer->RetrieveSubscriptionBought( productId, data, [receipt length]);
        [self setReceipt:receipt for:transaction.payment.productIdentifier];
        [m_buySubscriptions removeObjectAtIndex:idx];
    }

    // After completing remove transaction from the payment queue
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) transactionRestored:(SKPaymentTransaction*)transaction
{
    char productId[MAX_PRODUCT_ID_LENGTH];
    [transaction.originalTransaction.payment.productIdentifier getCString: productId maxLength: MAX_PRODUCT_ID_LENGTH encoding:NSASCIIStringEncoding];

    assert( observer );
    if( m_restore )
    {
        NSData* receipt = [NSData dataWithData: transaction.transactionReceipt];

        NSString* key = transaction.originalTransaction.payment.productIdentifier;
        NSMutableDictionary* dict;
        if( [m_restore objectForKey:key] )
        {
            dict = [m_restore objectForKey:transaction.originalTransaction.payment.productIdentifier];
            if( [transaction.transactionDate timeIntervalSince1970] > [[dict objectForKey:@"date"] timeIntervalSince1970] )
            {
                [dict setObject:transaction.transactionDate forKey:@"date"];
                [dict setObject:receipt forKey:@"receipt"];
                [dict setObject:[NSNumber numberWithInt:transaction.payment.quantity] forKey:@"quantity"];
            }
        }
        else
        {
            dict = [NSMutableDictionary dictionary];
            [dict setObject:transaction.transactionDate forKey:@"date"];
            [dict setObject:receipt forKey:@"receipt"];
            [dict setObject:[NSNumber numberWithInt:transaction.payment.quantity] forKey:@"quantity"];
        }

        [m_restore setObject:dict forKey:key];
    }
    else
    {
        NSData* receipt = transaction.transactionReceipt;
        const unsigned char* data = (const unsigned char*)([receipt bytes]);

        observer->RetreiveTransactionRestore( ClawExt::InAppTransaction(
                                                                        productId ,
                                                                        transaction.payment.quantity ,
                                                                        data ,
                                                                        [receipt length]
                                                                        ) );
        [self setReceipt:receipt for:transaction.originalTransaction.payment.productIdentifier];
    }

    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

-(void) transactionFailed:(SKPaymentTransaction*)transaction
{
    if( transaction.error.code != SKErrorPaymentCancelled )
    {
        UIAlertView* popup = [[UIAlertView alloc] 
                              initWithTitle:transaction.error.localizedDescription
                              message:transaction.error.localizedFailureReason
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")
                              otherButtonTitles:nil];
        [popup show];
        [popup release];
    }

    char productId[MAX_PRODUCT_ID_LENGTH];
    [transaction.payment.productIdentifier getCString: productId maxLength: MAX_PRODUCT_ID_LENGTH encoding:NSASCIIStringEncoding];

    assert( observer );

    NSUInteger idx = [m_buySubscriptions indexOfObject:transaction.payment.productIdentifier];
    if( idx == NSNotFound )
    {
        if( transaction.error.code == SKErrorPaymentCancelled )
        {
            observer->RetreiveTransactionCancel( ClawExt::InAppTransaction(
                                                                    productId ,
                                                                    transaction.payment.quantity
                                                                    ) );
        }
        else
        {
            observer->RetreiveTransactionFail( ClawExt::InAppTransaction(
                                                            productId ,
                                                            transaction.payment.quantity
                                                            ) );
        }
    }
    else
    {
        if( transaction.error.code == SKErrorPaymentCancelled )
        {
            observer->RetrieveSubscriptionCancel(productId);
        }
        else
        {
            observer->RetrieveSubscriptionFailed(productId);
        }
        [m_buySubscriptions removeObjectAtIndex:idx];
    }
    // Remove from queue
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) checkSubscription:(NSString*) sId;
{
    if( [self getReceipt:sId] == nil )
    {
        observer->RetrieveSubscriptionStatus([sId UTF8String], false);
    }
    else
    {
        NSData* receipt = [self getReceipt:sId];
        if( receipt == nil )
        {
            observer->RetrieveSubscriptionStatus([sId UTF8String], false);
        }
        else
        {
            const unsigned char* data = (const unsigned char*)([receipt bytes]);
            observer->RetrieveSubscriptionStatus([sId UTF8String], true, data, [receipt length]);
        }
    }
}

- (void) restorePurchases
{
    if( !m_restore )
    {
        m_restore = [[NSMutableDictionary dictionary] retain];
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
}

@end

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInStore.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IPHONE_BUILD_IN_STORE_HPP__
#define __NETWORK_IPHONE_BUILD_IN_STORE_HPP__

#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

namespace ClawExt
{
    class IPhoneBuildInStore : public InAppStore
    {
    public:
                            IPhoneBuildInStore();

        virtual             ~IPhoneBuildInStore();

        virtual bool        CheckBillingSupport( const Key* key = NULL, Message* reason = 0 );

        virtual bool        CheckPendingTransactions( Message* reason = 0 );

        virtual bool        BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity = 1, Message* reason = 0 );

        virtual bool        BuySubscriptionInternal( const Claw::NarrowString& id );

        virtual bool        CheckSubscriptionInternal( const Claw::NarrowString& id );

        virtual void RestorePurchases();

        // Just base class methods wrappers to alow pimpl class calls in callbacks
        void                RetreiveTransactionFail( const InAppTransaction& trans );
        void                RetreiveTransactionCancel( const InAppTransaction& trans );
        void                RetreiveTransactionComplete( const InAppTransaction& trans );
        void                RetreiveTransactionRestore( const InAppTransaction& trans );
        void                RetreiveTransactionRefund( const InAppTransaction& trans );

        void RetrieveSubscriptionStatus( const Claw::NarrowString& sId, bool status, const unsigned char* data = NULL, unsigned int len = 0 );
        void RetrieveSubscriptionBought( const Claw::NarrowString& sId, const unsigned char* data = NULL, unsigned int len = 0 );
        void RetrieveSubscriptionFailed( const Claw::NarrowString& sId );
        void RetrieveSubscriptionCancel( const Claw::NarrowString& sId );


#ifdef IAP_SERVER_VERIFICATION
        IPhoneBuildInStore( Claw::VfsMount* mount, const char* fileName = NULL );
#endif

    };
}

#endif

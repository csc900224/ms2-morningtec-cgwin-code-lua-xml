//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDb.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IPHONE_BUILD_IN_PRODUCTS_DB_HPP__
#define __NETWORK_IPHONE_BUILD_IN_PRODUCTS_DB_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppProductsDb.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

// Forward declaration for PIMPL
//class IPhoneBuildInProductsDbImpl;

namespace ClawExt
{
    class IPhoneBuildInProductsDb : public InAppProductsDb
    {
    public:
                            IPhoneBuildInProductsDb();
    
                            ~IPhoneBuildInProductsDb();
    
        virtual bool        QueryProductsIds( Message* reason = 0 );
    
        virtual bool        QueryProductsInfos( const ProductsIds& ids, Message* reason = 0 );
        
        // Just base class methods wrappers to alow pimpl class calls in callbacks
        void                RetreiveProductsIds( const ProductsIds& ids );
        void                RetreiveProductsInfos( const Products& products );

    private:
        //IPhoneBuildInProductsDbImpl*    m_impl;
    
    }; // class IPhoneBuildInProductsDb
} // namespace ClawExt

#endif // !defined __NETWORK_IPHONE_BUILD_IN_PRODUCTS_DB_HPP__
// EOF

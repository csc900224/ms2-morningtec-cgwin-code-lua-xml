//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneBuildInStoreImpl.h
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "claw_ext/monetization/inappstore/iphone/KeychainItemWrapper.h"

namespace ClawExt
{
	class IPhoneBuildInStore;
} // namespace ClawExt

@interface IPhoneBuildInStoreImpl : NSObject<SKPaymentTransactionObserver> 
{
@private
    // Instance variables
    ClawExt::IPhoneBuildInStore* observer;
}
@property (retain, nonatomic) NSMutableArray* m_buySubscriptions;
@property (retain, nonatomic) KeychainItemWrapper* m_keychain;
@property (retain, nonatomic) NSMutableDictionary* m_iaps;
@property (retain, nonatomic) NSMutableDictionary* m_restore;

// Constructor
- (id) initWithObserver:(ClawExt::IPhoneBuildInStore*) observer;

// Helper
- (void) setObserver:(ClawExt::IPhoneBuildInStore*) observer;

// Methods
- (bool) queryProductsIds;

- (bool) queryProductsInfos:(NSArray*)productsIds;

- (bool) checkBillingSupport;

- (bool) buyProduct:(NSString*)productId amount:(NSUInteger)quantity;

// Derived from SKPaymentTransactionObserver
- (void) paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;

// Transaction handlers
- (void) transactionCompleted:(SKPaymentTransaction*) transaction;
- (void) transactionFailed:(SKPaymentTransaction*) transaction;
- (void) transactionRestored:(SKPaymentTransaction*) transaction;

// Subscriptions
- (void) checkSubscription:(NSString*) sId;
- (bool) buySubscription:(NSString*) sId;

- (void) restorePurchases;

@end
// EOF

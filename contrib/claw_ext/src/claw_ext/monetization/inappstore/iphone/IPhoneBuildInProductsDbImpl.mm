//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/iphone/IPhoneInAppProductsDbImpl.mm
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#import  "claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDbImpl.h"

#include "claw_ext/monetization/inappstore/iphone/IPhoneBuildInProductsDb.hpp"

// External includes
#include "claw/base/Errors.hpp"

#define MAX_PRODUCT_ID_LENGTH           256
#define MAX_PRODUCT_NAME_LENGTH         256
#define MAX_PRODUCT_DESC_LENGTH         256
#define MAX_PRODUCT_PRICE_FMT_LENGTH    256

@implementation IPhoneBuildInProductsDbImpl

// Constructor
- (id) init
{
    if( (self = [super init]) )
    {
        [self setObserver: nil];
    }
    return self;
}

- (id) initWithObserver:(ClawExt::IPhoneBuildInProductsDb*) obs
{
    if( (self = [super init]) )
    {
        [self setObserver: obs];
    }
    return self;        
}

// Helper
- (void) setObserver:(ClawExt::IPhoneBuildInProductsDb*) obs
{
    observer = obs;
}

// Methods
- (bool) queryProductsIds
{
    return false;
}

- (bool) queryProductsInfos:(NSArray*)productsIds
{
    NSSet* set = [[NSSet alloc] initWithArray:productsIds];
    SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: set];
    request.delegate = self;
    [request start];
    return true;
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    char productId[MAX_PRODUCT_ID_LENGTH];
    char name[MAX_PRODUCT_NAME_LENGTH];
    char desc[MAX_PRODUCT_DESC_LENGTH];
    float price;
    ClawExt::InAppProductsDb::Products products;
    for (SKProduct* product in response.products)
    {
        [product.productIdentifier getCString: productId maxLength: MAX_PRODUCT_ID_LENGTH encoding:NSASCIIStringEncoding];
        [product.localizedTitle getCString: name maxLength: MAX_PRODUCT_NAME_LENGTH encoding:NSASCIIStringEncoding];
        [product.localizedDescription getCString: desc maxLength: MAX_PRODUCT_DESC_LENGTH encoding:NSASCIIStringEncoding];
        price = (float)product.price.doubleValue;
        //priceLocale
        CLAW_MSG( "Retreived product info: " << productId );
        CLAW_MSG( "- name: " << name );
        CLAW_MSG( "- description: " << desc );
        CLAW_MSG( "- price: " << price );
        products.push_back( ClawExt::InAppProduct( productId, name, desc, price ) );
    }
    observer->RetreiveProductsInfos( products );
    [request autorelease];
}

@end
// EOF

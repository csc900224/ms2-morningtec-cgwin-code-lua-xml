//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/win32/Win32InAppProductsDb.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/inappstore/win32/Win32InAppProductsDb.hpp"

static const char*  PRODUCT_ID          = "com.gamelion.ms.1000";
static const char*  PRODUCT_NAME        = "Cash";
static const char*  PRODUCT_DESC        = "Money for buying weapons and armory";
static const float  PRODUCT_PRICE       = 1.5f;
static const char*  PRODUCT_PRICE_FRMT  = "%.1f eu";

namespace ClawExt
{
    InAppProductsDb* InAppProductsDb::QueryInterface()
    {
        return new Win32InAppProductsDb;
    }

    void InAppProductsDb::Release( InAppProductsDb* instance )
    {
        delete instance;
    }

    bool Win32InAppProductsDb::QueryProductsIds( Message* reason /*= 0 */ )
    {
        ProductsIds prodIds;
        prodIds.push_back( InAppProduct::Id( PRODUCT_ID ) );

        NotifyProductsIds( prodIds );
        return true;
    }

    bool Win32InAppProductsDb::QueryProductsInfos( const ProductsIds& ids, Message* reason /* = 0 */ )
    {
        Products prodInfos;
        prodInfos.push_back( InAppProduct( PRODUCT_ID, PRODUCT_NAME, PRODUCT_DESC, PRODUCT_PRICE, PRODUCT_PRICE_FRMT ) );

        NotifyProductsInfos( prodInfos );
        return true;
    }
} // namespace ClawExt
// EOF

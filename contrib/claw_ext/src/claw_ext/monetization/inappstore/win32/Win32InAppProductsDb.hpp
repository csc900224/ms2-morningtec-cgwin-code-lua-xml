//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/win32/Win32InAppProductsDb.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_WIN32_IN_APP_PRODUCTS_DB_HPP__
#define __NETWORK_WIN32_IN_APP_PRODUCTS_DB_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppProductsDb.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

namespace ClawExt
{
    class Win32InAppProductsDb : public InAppProductsDb
    {
    public:
        virtual bool        QueryProductsIds( Message* reason = 0 );

        virtual bool        QueryProductsInfos( const ProductsIds& ids, Message* reason = 0 );

    }; // class Win32InAppProductsDb
} // namespace ClawExt
#endif // !defined __NETWORK_WIN32_IN_APP_PRODUCTS_DB_HPP__
// EOF

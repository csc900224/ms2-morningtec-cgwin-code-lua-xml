//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/win32/Win32InAppStore.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw/application/Time.hpp"
#include "claw_ext/monetization/inappstore/win32/Win32InAppStore.hpp"

// External includes
#include <stdlib.h>

namespace ClawExt
{
#ifdef IAP_SERVER_VERIFICATION
    InAppStore* InAppStore::QueryInterface( Claw::VfsMount* mount, const char* name, SystemType type )
    {
        return new Win32InAppStore( mount, name );
    }
#else
    InAppStore* InAppStore::QueryInterface( SystemType type )
    {
        return new Win32InAppStore();
    }
#endif

    void InAppStore::Release( InAppStore* instance )
    {
        delete instance;
    }

#ifdef IAP_SERVER_VERIFICATION
    Win32InAppStore::Win32InAppStore( Claw::VfsMount* mount, const char* fileName )
        : InAppStore( mount, fileName )
    {
    }
#endif

    bool Win32InAppStore::CheckBillingSupport( const Key* key /*= NULL*/, Message* reason /* = 0 */ )
    {
        // Always allow transactions
        NotifyTransactionSupport( true );
        return true;
    }

    bool Win32InAppStore::CheckPendingTransactions( Message* reason /*= 0*/ )
    {
        return true;
    }

    bool Win32InAppStore::BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity /* = 1 */, Message* reason /* = 0 */ )
    {
        // Simply pass approximatelly half of requested transactions, other half will fail
        if( rand() > RAND_MAX / 2)
        {
            Claw::StdOStringStream os;
            os << Claw::Time::GetTime();
            os << "_";
            os << id;

            NotifyTransactionComplete( InAppTransaction( id, quantity, (unsigned char*)os.str().c_str(), os.str().size() ) );
        }
        else
            NotifyTransactionFailed( InAppTransaction( id, quantity ) );
        return true;
    }

    bool Win32InAppStore::BuySubscriptionInternal( const Claw::NarrowString& id )
    {
        // Simply pass approximatelly half of requested transactions, other half will fail
        if( rand() > RAND_MAX / 2)
            NotifySubscriptionBought( id );
        else
            NotifySubscriptionFailed( id );
        return true;
    }

    bool Win32InAppStore::CheckSubscriptionInternal( const Claw::NarrowString& id )
    {
        NotifySubscriptionStatus( id, rand() > RAND_MAX / 2 );
        return true;
    }

} // namespace ClawExt
// EOF

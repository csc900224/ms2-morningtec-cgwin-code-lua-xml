//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/win32/Win32InAppStore.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_WIN32_IN_APP_STORE_HPP__
#define __NETWORK_WIN32_IN_APP_STORE_HPP__

// Internal includes
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

namespace ClawExt
{
    class Win32InAppStore : public InAppStore
    {
    public:
        virtual bool        CheckBillingSupport( const Key* key = NULL, Message* reason = 0 );
        virtual bool        CheckPendingTransactions( Message* reason = 0 );
        virtual bool        BuyProductInternal( const InAppProduct::Id& id, const InAppProduct::Quantity& quantity = 1, Message* reason = 0 );
        virtual bool        BuySubscriptionInternal( const Claw::NarrowString& id );
        virtual bool        CheckSubscriptionInternal( const Claw::NarrowString& id );

#ifdef IAP_SERVER_VERIFICATION
        Win32InAppStore( Claw::VfsMount* mount, const char* fileName );
#endif
    }; // class Win32InAppStore
} // namespace ClawExt
#endif // !defined __NETWORK_WIN32_IN_APP_STORE_HPP__
// EOF

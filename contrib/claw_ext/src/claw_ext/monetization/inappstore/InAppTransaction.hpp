//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/InAppTransaction.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_TRANSACTION_HPP__
#define __NETWORK_IN_APP_TRANSACTION_HPP__

#include "claw/vfs/Vfs.hpp"
#include "claw/vfs/EncryptedFile.hpp"
#include "claw_ext/monetization/inappstore/InAppProduct.hpp"

#include <string.h>

namespace ClawExt
{
    class InAppTransaction
    {
    public:
        InAppTransaction()
            : m_valid( false )
            , m_quantity( 0 )
            , m_data( NULL )
            , m_dataSize( 0 )
            , m_otherData( NULL )
        {
        }

        InAppTransaction( const InAppProduct::Id& productId, InAppProduct::Quantity quantity, const unsigned char* data = 0, int dataSize = 0, const void* otherData = 0 )
            : m_valid( true )
            , m_productId( productId )
            , m_quantity( quantity )
            , m_data( NULL )
            , m_dataSize( 0 )
            , m_otherData( otherData )
        {
            if( data != NULL && dataSize > 0 )
            {
                m_dataSize = dataSize;
                m_data = new unsigned char[ m_dataSize ];
                memcpy( m_data, data, m_dataSize );
            }
        }

        InAppTransaction( const InAppTransaction& other )
            : m_valid( other.m_valid )
            , m_productId( other.m_productId )
            , m_quantity( other.m_quantity )
            , m_data( NULL )
            , m_dataSize( other.m_dataSize )
            , m_otherData( other.m_otherData )
        {
            if( m_dataSize > 0 )
            {
                m_data = new unsigned char[ m_dataSize ];
                memcpy( m_data, other.m_data, m_dataSize );
            }
        }

        InAppTransaction &operator=( const InAppTransaction &other )
        {
            m_valid = other.m_valid;
            m_productId = other.m_productId;
            m_quantity = other.m_quantity;
            m_dataSize = other.m_dataSize;
            m_otherData = other.m_otherData;
            if( m_dataSize > 0 )
            {
                m_data = new unsigned char[ m_dataSize ];
                memcpy( m_data, other.m_data, m_dataSize );
            }
            else
            {
                m_data = NULL;
            }

            return *this;
        }

        ~InAppTransaction()
        {
            delete[] m_data;
        }

        bool Save( Claw::IOStream* file ) const
        {
            CLAW_ASSERT( file );
            Claw::StreamOff wrote;

            int version = VERSION;
            wrote = file->Write( &version, sizeof( version ) );
            if( wrote != sizeof( version ) )
                return false;

            wrote = file->Write( &m_valid, sizeof( m_valid ) );
            if( wrote != sizeof( m_valid ) )
                return false;

            int size = m_productId.size();
            wrote = file->Write( &size, sizeof( size ) );
            if( wrote != sizeof( size ) )
                return false;

            if( size > 0 )
            {
                wrote = file->Write( m_productId.c_str(), size );
                if( wrote != size )
                    return false;
            }

            wrote = file->Write( &m_quantity, sizeof( m_quantity ) );
            if( wrote != sizeof( m_quantity ) )
                return false;

            wrote = file->Write( &m_dataSize, sizeof( m_dataSize ) );
            if( wrote != sizeof( m_dataSize ) )
                return false;
            if( m_dataSize > 0 )
            {
                wrote = file->Write( m_data, m_dataSize );
                if( wrote != m_dataSize )
                    return false;
            }

            return true;
        }

        void Load( Claw::IOStream* file )
        {
            CLAW_ASSERT( file );
            int version = 0;
            file->Read( &version, sizeof( version ) );
            if( version == VERSION )
            {
                file->Read( &m_valid, sizeof( m_valid ) );
                int size = 0;
                file->Read( &size, sizeof( size ) );
                if( size > 0 )
                {
                    char* temp = new char[ size + 1 ];
                    temp[ size ] = 0;
                    file->Read( temp, size );
                    m_productId = temp;
                    delete[] temp;
                }
                file->Read( &m_quantity, sizeof( m_quantity ) );
                file->Read( &m_dataSize, sizeof( m_dataSize ) );
                if( m_dataSize > 0 )
                {
                    m_data = new unsigned char[ m_dataSize ];
                    file->Read( m_data, m_dataSize );
                }
            }
        }

        bool                    IsValid() const             { return m_valid; }

        const InAppProduct::Id& GetProductId() const        { return m_productId; }

        InAppProduct::Quantity  GetQuantity() const         { return m_quantity; }

        const unsigned char*    GetData() const             { return m_data; }

        const int               GetDataSize() const         { return m_dataSize; }

        const void*             GetOtherData() const        { return m_otherData; }

        bool operator==( const InAppTransaction& other )
        {
            if( other.m_productId != m_productId || other.m_quantity != m_quantity || other.m_dataSize != m_dataSize )
            {
                return false;
            }
            else
            {
                return memcmp( other.m_data, m_data, m_dataSize ) == 0;
            }
        }

    private:
        enum
        {
            VERSION = 1
        };

        bool                            m_valid;
        InAppProduct::Id                m_productId;
        InAppProduct::Quantity          m_quantity;
        unsigned char*                  m_data;
        int                             m_dataSize;
        const void*                     m_otherData;

    }; // class InAppTransaction
} // namespace ClawExt
#endif // !defined __NETWORK_IN_APP_TRANSACTION_HPP__
// EOF

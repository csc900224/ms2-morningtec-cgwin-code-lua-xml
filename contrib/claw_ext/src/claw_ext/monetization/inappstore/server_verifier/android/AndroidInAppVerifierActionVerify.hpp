#ifndef __NETWORK_ANDROID_IN_APP_VERIFIER_ACTION_VERIFY_HPP__
#define __NETWORK_ANDROID_IN_APP_VERIFIER_ACTION_VERIFY_HPP__

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

namespace ClawExt
{
    class AndroidInAppVerifierActionVerify: public InAppVerifierAction
    {
    public:
        enum VerifyType
        {
            VT_CONSUMABLE,
            VT_CHECK_SUBSCRIPTION,
            VT_BUY_SUBSCRIPTION,
        };

        AndroidInAppVerifierActionVerify( const InAppTransaction& trans );
        AndroidInAppVerifierActionVerify( const InAppTransaction& trans, VerifyType verifyType, bool alreadyOwned );

        ResultType Update();
    private:
        VerifyType m_verifyType;
        bool m_alreadyOwned;
    };
}
#endif

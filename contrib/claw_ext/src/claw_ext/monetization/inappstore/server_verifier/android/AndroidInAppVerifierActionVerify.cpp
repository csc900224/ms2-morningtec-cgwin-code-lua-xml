#include "claw_ext/monetization/inappstore/server_verifier/android/AndroidInAppVerifierActionVerify.hpp"
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw/application/Application.hpp"

namespace ClawExt
{

    InAppVerifierAction* InAppVerifierAction::CreateActionVerify( const InAppTransaction& trans )
    {
        return new AndroidInAppVerifierActionVerify( trans );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionVerifyCheckSubscription( const InAppTransaction& trans )
    {
        return new AndroidInAppVerifierActionVerify( trans, AndroidInAppVerifierActionVerify::VT_CHECK_SUBSCRIPTION, false );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionVerifyBuySubscription( const InAppTransaction& trans, bool alreadyOwned )
    {
        return new AndroidInAppVerifierActionVerify( trans, AndroidInAppVerifierActionVerify::VT_BUY_SUBSCRIPTION, alreadyOwned );
    }

    AndroidInAppVerifierActionVerify::AndroidInAppVerifierActionVerify( const InAppTransaction& trans )
        : InAppVerifierAction( trans, "VerifyAndroid", RT_VERIFY_CONNECT_ERROR )
        , m_verifyType( VT_CONSUMABLE )
        , m_alreadyOwned( false )
    {
        // purchase token
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );

        const Claw::NarrowString& pkgName = Claw::Application::GetPackageName();
        AddParam( "package", EncodeBase64( (const unsigned char*)pkgName.c_str(), pkgName.length() ).c_str() );

        const Claw::NarrowString& productId = trans.GetProductId();
        AddParam( "id", EncodeBase64( (const unsigned char*)productId.c_str(), productId.length() ).c_str() );

        const Claw::NarrowString& type = "consumable";
        AddParam( "type", EncodeBase64( (const unsigned char*)type.c_str(), type.length() ).c_str() );
    }

    AndroidInAppVerifierActionVerify::AndroidInAppVerifierActionVerify( const InAppTransaction& trans, VerifyType verifyType, bool alreadyOwned )
        : InAppVerifierAction( trans, "VerifyAndroid", RT_VERIFY_SUBSCRIPTION_CONNECT_ERROR )
        , m_verifyType( verifyType )
        , m_alreadyOwned( alreadyOwned )
    {
        // purchase token
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );

        const Claw::NarrowString& pkgName = Claw::Application::GetPackageName();
        AddParam( "package", EncodeBase64( (const unsigned char*)pkgName.c_str(), pkgName.length() ).c_str() );

        const Claw::NarrowString& productId = trans.GetProductId();
        AddParam( "id", EncodeBase64( (const unsigned char*)productId.c_str(), productId.length() ).c_str() );

        const Claw::NarrowString& type = "subscription";
        AddParam( "type", EncodeBase64( (const unsigned char*)type.c_str(), type.length() ).c_str() );
    }

    InAppVerifierAction::ResultType AndroidInAppVerifierActionVerify::Update()
    {
        InAppVerifierAction::ResultType result = InAppVerifierAction::Update();
        if( result != RT_NONE )
        {
            return result;
        }
        m_mutex.Enter();
        if( m_responseReceived )
        {
            result = RT_VERIFY_ERROR;
            if( m_trans.GetDataSize() + m_trans.GetProductId().size() + 1 == m_response.size() )
            {
                Claw::NarrowString token = Claw::NarrowString( (const char*)m_trans.GetData(), m_trans.GetDataSize() );
                if( m_response == m_trans.GetProductId() + "_" + token )
                {
                    switch( m_verifyType )
                    {
                    case VT_CONSUMABLE:
                        result = RT_VERIFIED;
                        break;
                    case VT_BUY_SUBSCRIPTION:
                        if( m_alreadyOwned )
                            result = RT_VERIFIED_BUY_ALREADY_OWNED_SUBSCRIPTION;
                        else
                            result = RT_VERIFIED_BUY_SUBSCRIPTION;
                        break;
                    case VT_CHECK_SUBSCRIPTION:
                        result = RT_VERIFIED_CHECK_SUBSCRIPTION;
                        break;
                    default:
                        result = RT_VERIFY_ERROR;
                        break;
                    }
                }
            }
            else
            {
                switch( m_verifyType )
                {
                case VT_CONSUMABLE:
                    result = RT_VERIFY_ERROR;
                    break;
                case VT_BUY_SUBSCRIPTION:
                    result = RT_VERIFY_ERROR_BUY_SUBSCRIPTION;
                    break;
                case VT_CHECK_SUBSCRIPTION:
                    result = RT_VERIFY_ERROR_CHECK_SUBSCRIPTION;
                    break;
                default:
                    result = RT_VERIFY_ERROR;
                    break;
                }
            }
        }
        m_mutex.Leave();

        return result;
    }

}

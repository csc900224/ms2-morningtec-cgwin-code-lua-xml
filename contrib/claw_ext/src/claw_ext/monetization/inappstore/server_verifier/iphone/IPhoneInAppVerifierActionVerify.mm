//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/iphone/IPhoneInAppVerifierActionVerify.mm
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/inappstore/server_verifier/iphone/IPhoneInAppVerifierActionVerify.hpp"
#import <UIKit/UIKit.h>

namespace ClawExt
{
    InAppVerifierAction* InAppVerifierAction::CreateActionVerify( const InAppTransaction& trans )
    {
        return new IPhoneInAppVerifierActionVerify( trans );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionVerifyCheckSubscription( const InAppTransaction& trans )
    {
        return new IPhoneInAppVerifierActionVerify( trans, IPhoneInAppVerifierActionVerify::VT_CHECK_SUBSCRIPTION );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionVerifyBuySubscription( const InAppTransaction& trans, bool alreadyOwned )
    {
        return new IPhoneInAppVerifierActionVerify( trans, IPhoneInAppVerifierActionVerify::VT_BUY_SUBSCRIPTION );
    }

    IPhoneInAppVerifierActionVerify::IPhoneInAppVerifierActionVerify( const InAppTransaction& trans )
    : InAppVerifierAction( trans, "VerifyiOS", RT_VERIFY_CONNECT_ERROR )
    , m_verifyType( VT_CONSUMABLE )
    {
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );

        const char* type = "consumable";
        AddParam( "type", EncodeBase64( (const unsigned char*)type, strlen(type)).c_str() );
    }

    IPhoneInAppVerifierActionVerify::IPhoneInAppVerifierActionVerify( const InAppTransaction& trans, IPhoneInAppVerifierActionVerify::VerifyType verifyType )
    : InAppVerifierAction( trans, "VerifyiOS", RT_VERIFY_SUBSCRIPTION_CONNECT_ERROR )
    , m_verifyType( verifyType )
    {
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );

        const char* type = "subscription";
        AddParam( "type", EncodeBase64( (const unsigned char*)type, strlen(type)).c_str() );

        const Claw::NarrowString& subId = trans.GetProductId();
        AddParam( "id", EncodeBase64((const unsigned char*)subId.c_str(), subId.size()).c_str());
    }

    InAppVerifierAction::ResultType IPhoneInAppVerifierActionVerify::Update()
    {
        InAppVerifierAction::ResultType result = InAppVerifierAction::Update();
        if( result != RT_NONE )
        {
            return result;
        }
        m_mutex.Enter();
        if( m_responseReceived )
        {
            switch ( m_verifyType )
            {
                case VT_CONSUMABLE:
                    result = RT_VERIFY_ERROR;
                    break;

                case VT_BUY_SUBSCRIPTION:
                    result = RT_VERIFY_ERROR_BUY_SUBSCRIPTION;
                    break;

                case VT_CHECK_SUBSCRIPTION:
                    result = RT_VERIFY_ERROR_CHECK_SUBSCRIPTION;
                    break;

                default:
                    result = RT_VERIFY_ERROR;
                    break;
            }
            NSError* error = nil;
            id object = [NSJSONSerialization JSONObjectWithData:[NSData dataWithBytes:m_response.c_str() length:m_response.size()] options:0 error:&error];
            if( error )
            {
                CLAW_MSG( "json parse error");
            }
            else
            {
                if([object isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary* dict = object;
                    NSNumber* code = [dict objectForKey:@"status"];

                    // make further call to server to check for subscription status - this happens during iap restore
                    if( m_verifyType == VT_CONSUMABLE && [code isKindOfClass:[NSNumber class]] && [code longValue] == 21004 )
                    {
                        result = RT_VERIFY_CHECK_SUBSCRIPTION_READY;
                        m_verifyType = VT_CHECK_SUBSCRIPTION;
                        m_mutex.Leave();
                        return result;
                    }

                    NSDictionary* receipt = [dict objectForKey:@"receipt"];
                    if([receipt isKindOfClass:[NSDictionary class]])
                    {
                        NSString* productId = [receipt objectForKey:@"product_id"];
                        if( productId && m_trans.GetProductId() == Claw::NarrowString( [productId UTF8String] ))
                        {
                            switch( m_verifyType )
                            {
                                case VT_CHECK_SUBSCRIPTION:
                                    result = RT_VERIFIED_CHECK_SUBSCRIPTION;
                                    break;

                                case VT_BUY_SUBSCRIPTION:
                                    result = RT_VERIFIED_BUY_SUBSCRIPTION;
                                    break;

                                case VT_CONSUMABLE:
                                    result = RT_VERIFIED;
                                    break;
                            }
                        }
                    }
                }
            }
        }
        m_mutex.Leave();

        return result;
    }
} // namespace ClawExt

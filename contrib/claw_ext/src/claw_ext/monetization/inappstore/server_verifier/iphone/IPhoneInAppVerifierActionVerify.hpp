//////////////////////////////////////////////////////////////////////////
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IPHONE_IN_APP_VERIFIER_ACTION_VERIFY_HPP__
#define __NETWORK_IPHONE_IN_APP_VERIFIER_ACTION_VERIFY_HPP__

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

namespace ClawExt
{
    class IPhoneInAppVerifierActionVerify: public InAppVerifierAction
    {
    public:
        enum VerifyType
        {
            VT_CONSUMABLE,
            VT_CHECK_SUBSCRIPTION,
            VT_BUY_SUBSCRIPTION,
        };

        IPhoneInAppVerifierActionVerify( const InAppTransaction& trans );
        IPhoneInAppVerifierActionVerify( const InAppTransaction& trans, VerifyType verifyType );

        ResultType Update();
    private:
        VerifyType m_verifyType;
    };
}

#endif

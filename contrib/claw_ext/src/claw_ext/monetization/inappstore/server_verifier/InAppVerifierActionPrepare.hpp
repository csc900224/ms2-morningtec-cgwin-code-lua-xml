//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/InAppVerifierActionPrepare.hpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_VERIFIER_ACTION_PREPARE_HPP__
#define __NETWORK_IN_APP_VERIFIER_ACTION_PREPARE_HPP__

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

namespace ClawExt
{
    class InAppVerifierActionPrepare: public InAppVerifierAction
    {
    public:
        enum PrepareType
        {
            PT_CONSUMABLE,
            PT_CHECK_SUBSCRIPTION,
            PT_BUY_SUBSCRIPTION,
        };

        InAppVerifierActionPrepare( const InAppTransaction& trans );
        InAppVerifierActionPrepare( const Claw::NarrowString& subscriptionId, PrepareType pt );

        ResultType Update();
    private:
         PrepareType m_prepType;
    };
}

#endif

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/InAppVerifierActionConsume.cpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierActionConsume.hpp"
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#if defined CLAW_ANDROID
#  include "claw/application/Application.hpp"
#endif

namespace ClawExt
{
    InAppVerifierAction* InAppVerifierAction::CreateActionConsume( const InAppTransaction& trans )
    {
        return new InAppVerifierActionConsume( trans );
    }

    InAppVerifierActionConsume::InAppVerifierActionConsume( const InAppTransaction& trans )
#ifdef CLAW_SDL
        : InAppVerifierAction( trans, "ConsumeWin32", RT_CONSUME_CONNECT_ERROR )
#elif defined CLAW_IPHONE
        : InAppVerifierAction( trans, "ConsumeiOS", RT_CONSUME_CONNECT_ERROR )
#elif defined CLAW_ANDROID
        : InAppVerifierAction( trans, "ConsumeAndroid", RT_CONSUME_CONNECT_ERROR )
#else
#error not supported platform
#endif

    {
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );
#if defined CLAW_ANDROID

        const Claw::NarrowString& pkgName = Claw::Application::GetPackageName();
        AddParam( "package", EncodeBase64( (const unsigned char*)pkgName.c_str(), pkgName.length() ).c_str() );

        const Claw::NarrowString& productId = trans.GetProductId();
        AddParam( "id", EncodeBase64( (const unsigned char*)productId.c_str(), productId.length() ).c_str() );

        const Claw::NarrowString& type = "consumable";
        AddParam( "type", EncodeBase64( (const unsigned char*)type.c_str(), type.length() ).c_str() );

#endif
    }
} // namespace ClawExt

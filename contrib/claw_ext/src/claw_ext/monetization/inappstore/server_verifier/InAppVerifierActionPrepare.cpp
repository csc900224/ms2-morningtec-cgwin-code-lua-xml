//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/InAppVerifierActionPrepare.cpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierActionPrepare.hpp"
#include "claw_ext/monetization/inappstore/InAppStore.hpp"

namespace ClawExt
{
    InAppVerifierAction* InAppVerifierAction::CreateActionPrepare( const InAppTransaction& trans )
    {
        return new InAppVerifierActionPrepare( trans );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionPrepareBuySubscription( const Claw::NarrowString& id )
    {
        return new InAppVerifierActionPrepare( id, InAppVerifierActionPrepare::PT_BUY_SUBSCRIPTION );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionPrepareCheckSubscription( const Claw::NarrowString& id )
    {
        return new InAppVerifierActionPrepare( id, InAppVerifierActionPrepare::PT_CHECK_SUBSCRIPTION );
    }

    InAppVerifierActionPrepare::InAppVerifierActionPrepare( const InAppTransaction& trans )
        : InAppVerifierAction( trans, "Prepare", RT_PREPARE_CONNECT_ERROR )
        , m_prepType( PT_CONSUMABLE )
    {
        AddParam( "id", trans.GetProductId().c_str() );
    }

    InAppVerifierActionPrepare::InAppVerifierActionPrepare( const Claw::NarrowString& subscriptionId, PrepareType pt )
        : InAppVerifierAction( InAppTransaction(subscriptionId, 1), "Prepare", RT_PREPARE_BROADCAST_CONNECT_ERROR )
        , m_prepType( pt )
    {
        AddParam( "id", subscriptionId.c_str() );
    }

    InAppVerifierAction::ResultType InAppVerifierActionPrepare::Update()
    {
        InAppVerifierAction::ResultType result = InAppVerifierAction::Update();
        if( result != RT_NONE )
        {
            return result;
        }
        m_mutex.Enter();
        if( m_responseReceived )
        {
            if( m_trans.GetProductId() == m_response )
            {
                switch( m_prepType )
                {
                    case PT_CHECK_SUBSCRIPTION:
                        result = RT_VERIFY_CHECK_SUBSCRIPTION_READY;
                        break;
                    case PT_BUY_SUBSCRIPTION:
                        result = RT_VERIFY_BUY_SUBSCRIPTION_READY;
                        break;
                    case PT_CONSUMABLE:
                        result = RT_VERIFY_READY;
                        break;
                    default:
                        result = RT_PREPARE_ERROR;
                        break;
                }
            }
            else
            {
                switch( m_prepType )
                {
                    case PT_CHECK_SUBSCRIPTION:
                        result = RT_PREPARE_SUBSCRIPTION_ERROR;
                        break;
                    case PT_BUY_SUBSCRIPTION:
                        result = RT_PREPARE_SUBSCRIPTION_ERROR;
                        break;
                    case PT_CONSUMABLE:
                        result = RT_PREPARE_ERROR;
                        break;
                    default:
                        result = RT_PREPARE_ERROR;
                        break;
                }
            }
        }
        m_mutex.Leave();

        return result;
    }
}

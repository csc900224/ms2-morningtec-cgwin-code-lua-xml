//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.cpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw/base/Crypto.hpp"
#include "claw/base/RNG.hpp"
#include "claw/base/Thread.hpp"
#include "claw/network/HttpRequest.hpp"

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

#define XOR_26(k) { k[0]^0xaa,k[1]^0xab,k[2]^0xac,k[3]^0xad,k[4]^0xae,k[5]^0xaf,k[6]^0xb0,k[7]^0xb1,k[8]^0xb2,k[9]^0xb3, \
                    k[10]^0xb4,k[11]^0xb5,k[12]^0xb6,k[13]^0xb7,k[14]^0xb8,k[15]^0xb9,k[16]^0xba,k[17]^0xbb,k[18]^0xbc,k[19]^0xbd, \
                    k[20]^0xbe,k[21]^0xbf,k[22]^0xc0,k[23]^0xc1,k[24]^0xc2,k[25]^0xc3 }

namespace ClawExt
{
    const char* InAppVerifierAction::BASE64_CHARS =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz"
                                "0123456789+/";

    const unsigned char InAppVerifierAction::SERVER_KEY1[] = XOR_26("qUtR6cr9swu6aja6PumAfrehuc");
    const unsigned char InAppVerifierAction::SERVER_KEY2[] = XOR_26("7aheq3JuCHehevuvetRefReswA");
    const char* InAppVerifierAction::KEY_ACTION = "action";

    void InAppVerifierAction::Run()
    {
        m_thread = new Claw::Thread( ThreadEntry, this );
    }

    InAppVerifierAction::ResultType InAppVerifierAction::Update()
    {
        m_mutex.Enter();
        if( m_error )
        {
            m_mutex.Leave();
            return m_connectErrorType;
        }

        m_mutex.Leave();
        return RT_NONE;
    }
    
    InAppVerifierAction::InAppVerifierAction( const InAppTransaction& trans, const char* action, const ResultType connectErrorType )
        : m_trans( trans )
        , m_thread( NULL )
        , m_error( false )
        , m_connectErrorType( connectErrorType )
        , m_randomKey( Claw::RNG().GetInt() )
        , m_responseReceived( false )
    {
        AddParam( KEY_ACTION, action );
    }

    InAppVerifierAction::~InAppVerifierAction()
    {
        delete m_thread;
    }

    void InAppVerifierAction::AddParam( const char* key, const char* value )
    {
        if( m_postData.length() > 0 )
        {
            m_postData << "&";
        }
        m_postData << key << "=" << value;
    }

    Claw::NarrowString InAppVerifierAction::EncodeBase64( unsigned char const* data, unsigned int len)
    {
        Claw::NarrowString ret;
        int i = 0;
        int j = 0;
        unsigned char char_array_3[3];
        unsigned char char_array_4[4];
 
        while (len--) {
            char_array_3[i++] = *(data++);
            if (i == 3) {
                char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
                char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
                char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
                char_array_4[3] = char_array_3[2] & 0x3f;
 
                for(i = 0; (i <4) ; i++) ret += BASE64_CHARS[char_array_4[i]];
                i = 0;
            }
        }
 
        if (i) {
            for(j = i; j < 3; j++) char_array_3[j] = '\0';
 
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;
 
            for (j = 0; (j < i + 1); j++) ret += BASE64_CHARS[char_array_4[j]];
 
            while((i++ < 3)) ret += '=';
        }
 
        return ret;
    }

    int InAppVerifierAction::ThreadEntry( void* data )
    {
        static_cast< InAppVerifierAction* >( data )->ThreadRun();
        return 0;
    }

    void InAppVerifierAction::ThreadRun()
    {
        CLAW_MSG( "Executing in app verifier action: " << m_postData.str() );

        Claw::HttpRequest request( SERVER_URL );
        request.SetMode( Claw::HttpRequest::M_POST );
        request.SetPostData( Encrypt() );
        request.Connect();
        if( request.CheckError() )
        {
            m_mutex.Enter();
            m_error = true;
            m_mutex.Leave();
            return;
        }
        request.Download();
        if( request.CheckError() )
        {
            m_mutex.Enter();
            m_error = true;
            m_mutex.Leave();
            return;
        }
        m_mutex.Enter();
        m_response = Decrypt( (const Claw::UInt8*)request.GetData(), request.GetLength() );
        m_responseReceived = true;
        m_mutex.Leave();
    }

    Claw::NarrowString InAppVerifierAction::Encrypt()
    {
        // prepare key
        Claw::UInt8 key[56];
        Claw::UInt8* keyPtr = key;
        memcpy( keyPtr, SERVER_KEY2, 26 );
        Claw::UInt8 x = 0xaa;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;
        memcpy( keyPtr, &m_randomKey, 4 );
        keyPtr += 4;
        memcpy( keyPtr, SERVER_KEY1, 26 );
        x -= 26;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;

        Claw::RawCrypto crypto;
        crypto.SetKey( (const char*)key, 56 );

        // prepare data
        Claw::NarrowString postData = m_postData.str();
        Claw::UInt32 originalSize = postData.size();
        Claw::UInt32 encryptedSize = originalSize + 4; // 4 bytes for length
        Claw::UInt32 padding = 8 - (encryptedSize % 8);
        Claw::UInt32 size = encryptedSize + padding;
        Claw::UInt8* data = new Claw::UInt8[ size ];
        Claw::UInt8* ptr = data;
        memset( ptr, 0, size );

        // add original size
        // keep it endianess independent
        *ptr++ = (originalSize >> 24) & 0xff;
        *ptr++ = (originalSize >> 16) & 0xff;
        *ptr++ = (originalSize >> 8) & 0xff;
        *ptr++ = originalSize & 0xff;

        // add post data
        memcpy( ptr, postData.data(), originalSize );

        // encrypt whole data
        Claw::UInt8* encryptedData = new Claw::UInt8[ size ];
        crypto.Encrypt( data, size, encryptedData );

        // create final message
        Claw::UInt32 messageSize = size + 4;
        Claw::UInt8* message = new Claw::UInt8[ messageSize ];
        memcpy( message, encryptedData, size );
        delete[] encryptedData;
        memcpy( message + size, &m_randomKey, 4 );

        // convert encrypted data to string
        Claw::NarrowString encryptedString( (const char*)message, messageSize );
        delete[] message;

        return encryptedString;
    }

    Claw::NarrowString InAppVerifierAction::Decrypt( const Claw::UInt8* data, Claw::UInt32 size )
    {
        if( size == 0 )
        {
            return Claw::NarrowString();
        }

        // prepare key
        unsigned char key[56];
        unsigned char* keyPtr = key;
        memcpy( keyPtr, SERVER_KEY1, 26 );
        Claw::UInt8 x = 0xaa;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;
        memcpy( keyPtr, &m_randomKey, 4 );
        keyPtr += 4;
        memcpy( keyPtr, SERVER_KEY2, 26 );
        x -= 26;
        for( int i = 0; i < 26; i++ )
            *keyPtr++ ^= x++;

        Claw::RawCrypto crypto;
        crypto.SetKey( (const char*)key, 56 );

        // decrypt data
        Claw::UInt8* decrypted = new Claw::UInt8[ size ];
        crypto.Decrypt( data, size, decrypted );

        // read original size without padding
        Claw::UInt32 originalSize = (decrypted[0] << 24) | (decrypted[1] << 16) | (decrypted[2] << 8) | decrypted[3];

        // convert decrypted data to string
        Claw::NarrowString decryptedString( (const char*)&decrypted[4], originalSize );
        delete[] decrypted;

        return decryptedString;
    }
} // namespace ClawExt

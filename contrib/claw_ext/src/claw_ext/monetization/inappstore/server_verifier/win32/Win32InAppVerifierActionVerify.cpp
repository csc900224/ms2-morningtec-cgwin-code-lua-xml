//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/win32/Win32InAppVerifierActionVerify.cpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/inappstore/server_verifier/win32/Win32InAppVerifierActionVerify.hpp"
#include "claw_ext/monetization/inappstore/InAppStore.hpp"

namespace ClawExt
{
    InAppVerifierAction* InAppVerifierAction::CreateActionVerify( const InAppTransaction& trans )
    {
        return new Win32InAppVerifierActionVerify( trans );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionVerifyBuySubscription( const InAppTransaction& trans, bool alreadyOwned )
    {
        return new Win32InAppVerifierActionVerify( trans, Win32InAppVerifierActionVerify::VT_BUY_SUBSCRIPTION );
    }

    InAppVerifierAction* InAppVerifierAction::CreateActionVerifyCheckSubscription( const InAppTransaction& trans )
    {
        return new Win32InAppVerifierActionVerify( trans, Win32InAppVerifierActionVerify::VT_CHECK_SUBSCRIPTION );
    }

    Win32InAppVerifierActionVerify::Win32InAppVerifierActionVerify( const InAppTransaction& trans )
        : InAppVerifierAction( trans, "VerifyWin32", RT_VERIFY_CONNECT_ERROR )
        , m_verifyType( VT_CONSUMABLE )
    {
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );
    }

    Win32InAppVerifierActionVerify::Win32InAppVerifierActionVerify( const InAppTransaction& trans, VerifyType type )
        : InAppVerifierAction( trans, "VerifyWin32", RT_VERIFY_SUBSCRIPTION_CONNECT_ERROR )
        , m_verifyType( type )
    {
        AddParam( "data", EncodeBase64( trans.GetData(), trans.GetDataSize() ).c_str() );
    }

    InAppVerifierAction::ResultType Win32InAppVerifierActionVerify::Update()
    {
        InAppVerifierAction::ResultType result = InAppVerifierAction::Update();
        if( result != RT_NONE )
        {
            return result;
        }
        m_mutex.Enter();
        if( m_responseReceived )
        {
            if( m_trans.GetDataSize() == m_response.size() )
            {
                switch( m_verifyType )
                {
                case VT_CONSUMABLE:
                    result = RT_VERIFIED;
                    break;
                case VT_BUY_SUBSCRIPTION:
                    result = RT_VERIFIED_BUY_SUBSCRIPTION;
                    break;
                case VT_CHECK_SUBSCRIPTION:
                    result = RT_VERIFIED_CHECK_SUBSCRIPTION;
                    break;
                default:
                    result = RT_VERIFY_ERROR;
                    break;
                }
            }
            else
            {
                switch( m_verifyType )
                {
                case VT_CONSUMABLE:
                    result = RT_VERIFY_ERROR;
                    break;
                case VT_BUY_SUBSCRIPTION:
                    result = RT_VERIFY_ERROR_BUY_SUBSCRIPTION;
                    break;
                case VT_CHECK_SUBSCRIPTION:
                    result = RT_VERIFY_ERROR_CHECK_SUBSCRIPTION;
                    break;
                default:
                    result = RT_VERIFY_ERROR;
                    break;
                }
            }
        }
        m_mutex.Leave();

        return result;
    }
} // namespace ClawExt

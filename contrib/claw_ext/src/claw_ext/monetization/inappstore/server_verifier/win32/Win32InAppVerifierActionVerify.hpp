//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/win32/Win32InAppVerifierActionVerify.hpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_WIN32_IN_APP_VERIFIER_ACTION_VERIFY_HPP__
#define __NETWORK_WIN32_IN_APP_VERIFIER_ACTION_VERIFY_HPP__

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

namespace ClawExt
{
    class Win32InAppVerifierActionVerify: public InAppVerifierAction
    {
    public:
        enum VerifyType
        {
            VT_CONSUMABLE,
            VT_CHECK_SUBSCRIPTION,
            VT_BUY_SUBSCRIPTION,
        };

        Win32InAppVerifierActionVerify( const InAppTransaction& trans );
        Win32InAppVerifierActionVerify( const InAppTransaction& trans, VerifyType );

        ResultType Update();

    private:
        VerifyType m_verifyType;

    };
}
#endif

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_VERIFIER_ACTION_HPP__
#define __NETWORK_IN_APP_VERIFIER_ACTION_HPP__

#include <vector>

#include "claw/base/RefCounter.hpp"
#include "claw/base/Mutex.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/base/String.hpp"
#include "claw/base/Thread.hpp"

#include "claw_ext/monetization/inappstore/InAppTransaction.hpp"

namespace ClawExt
{
    class InAppVerifierAction: public Claw::RefCounter
    {
    public:
        enum ResultType
        {
            RT_NONE,
            RT_PREPARE_CONNECT_ERROR,
            RT_PREPARE_BROADCAST_CONNECT_ERROR,
            RT_VERIFY_READY,
            RT_VERIFY_CHECK_SUBSCRIPTION_READY,
            RT_VERIFY_BUY_SUBSCRIPTION_READY,
            RT_PREPARE_ERROR,
            RT_PREPARE_SUBSCRIPTION_ERROR,
            RT_VERIFY_CONNECT_ERROR,
            RT_VERIFY_SUBSCRIPTION_CONNECT_ERROR,
            RT_VERIFIED,
            RT_VERIFIED_CHECK_SUBSCRIPTION,
            RT_VERIFIED_BUY_SUBSCRIPTION,
            RT_VERIFIED_BUY_ALREADY_OWNED_SUBSCRIPTION,
            RT_VERIFY_ERROR_CHECK_SUBSCRIPTION,
            RT_VERIFY_ERROR_BUY_SUBSCRIPTION,
            RT_VERIFY_ERROR,
            RT_CONSUME_CONNECT_ERROR
        };

        void Run();
        virtual ResultType Update();
        inline const InAppTransaction& GetTransaction() const
        {
            return m_trans;
        }

        static InAppVerifierAction* CreateActionPrepare( const InAppTransaction& trans );
        static InAppVerifierAction* CreateActionVerify( const InAppTransaction& trans );
        static InAppVerifierAction* CreateActionConsume( const InAppTransaction& trans );
        static InAppVerifierAction* CreateActionPrepareBuySubscription( const Claw::NarrowString& id );
        static InAppVerifierAction* CreateActionPrepareCheckSubscription( const Claw::NarrowString& id );
        static InAppVerifierAction* CreateActionVerifyBuySubscription( const InAppTransaction& id, bool alreadyOwned );
        static InAppVerifierAction* CreateActionVerifyCheckSubscription( const InAppTransaction& id );
    protected:
        InAppVerifierAction( const InAppTransaction& trans, const char* action, const ResultType connectErrorType );
        virtual ~InAppVerifierAction();

        void AddParam( const char* key, const char* value );
        Claw::NarrowString EncodeBase64( unsigned char const* data, unsigned int len );

        InAppTransaction            m_trans;
        Claw::Mutex                 m_mutex;
        Claw::NarrowString          m_response;
        bool                        m_responseReceived;
        bool                        m_error;
        const ResultType            m_connectErrorType;
    private:
        static const char* BASE64_CHARS;
        static const char* SERVER_URL;
        static const unsigned char SERVER_KEY1[];
        static const unsigned char SERVER_KEY2[];
        static const char* KEY_ACTION;

        static int ThreadEntry( void* data );
        void ThreadRun();
        Claw::NarrowString Encrypt();
        Claw::NarrowString Decrypt( const Claw::UInt8* data, Claw::UInt32 size );

        Claw::StdOStringStream      m_postData;
        Claw::Thread*               m_thread;
        const Claw::UInt32          m_randomKey;
    }; // class InAppVerifier

    typedef Claw::SmartPtr< InAppVerifierAction>        InAppVerifierActionPtr;
} // namespace ClawExt
#endif // !defined __NETWORK_IN_APP_VERIFIER_ACTION_HPP__
// EOF

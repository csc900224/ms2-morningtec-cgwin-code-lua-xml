//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/inappstore/server_verifier/InAppVerifierActionConsume.hpp
//
//  AUTHOR(S):
//      Stefan Olczak <stefan.olczak@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IN_APP_VERIFIER_ACTION_CONSUME_HPP__
#define __NETWORK_IN_APP_VERIFIER_ACTION_CONSUME_HPP__

#include "claw_ext/monetization/inappstore/server_verifier/InAppVerifierAction.hpp"

namespace ClawExt
{
    class InAppVerifierActionConsume: public InAppVerifierAction
    {
    public:
        InAppVerifierActionConsume( const InAppTransaction& trans );
    }; // class InAppVerifierActionConsume
} // namespace ClawExt
#endif // !defined __NETWORK_IN_APP_VERIFIER_ACTION_CONSUME_HPP__
// EOF
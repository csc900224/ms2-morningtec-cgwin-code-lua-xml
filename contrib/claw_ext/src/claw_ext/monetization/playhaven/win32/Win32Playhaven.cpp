//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/win32/Win32Playhaven.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/playhaven/win32/Win32Playhaven.hpp"

// External includes
#include "claw/base/Errors.hpp"

namespace ClawExt
{
    static Win32Playhaven* s_instance = NULL;

    /*static*/ Playhaven* Playhaven::QueryInterface()
    {
        if( !s_instance )
            s_instance = new Win32Playhaven;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Playhaven::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void Win32Playhaven::Initialize( const char* token, const char* secret )
    {
        CLAW_MSG( "Win32Playhaven::Initialize() token: " << token << " secret: " << secret );
    }

    void Win32Playhaven::CancelRequest()
    {
        CLAW_MSG( "Win32Playhaven::CancelRequest()" );
    }

    void Win32Playhaven::ContentPlacement( const char* place )
    {
        CLAW_MSG( "Win32Playhaven::ContentPlacement() place: " << place );
    }

    void Win32Playhaven::ReportOpen()
    {
        CLAW_MSG( "Win32Playhaven::ReportOpen()" );
    }

    Win32Playhaven::PlayhavenState Win32Playhaven::GetState() const
    {
        CLAW_MSG( "Win32Playhaven::GetState()" );
        return PS_CLOSED;
    }

    void Win32Playhaven::NotifyPurchaseResult( PurchaseResult result )
    {
        CLAW_MSG( "Win32Playhaven::NotifyPurchaseResult(): " << result );
    }

} // namespace ClawExt

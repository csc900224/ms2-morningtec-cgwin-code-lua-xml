//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/iphone/IPhonePlayhaven.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/playhaven/iphone/IPhonePlayhaven.hpp"

// External includes
#include "claw/base/Errors.hpp"

#import "PlayHavenSDK.h"

namespace ClawExt
{
    static IPhonePlayhaven* s_instance = NULL;
}
static ClawExt::Playhaven::PlayhavenState state = ClawExt::Playhaven::PS_CLOSED;
static PHPurchase* s_purchase = NULL;

@interface PHHandler : NSObject<PHPublisherContentRequestDelegate>
{
}
-(void)requestWillGetContent:(PHPublisherContentRequest*)request;
-(void)requestDidGetContent:(PHPublisherContentRequest*)request;
-(void)request:(PHPublisherContentRequest*)request contentWillDisplay:(PHContent*)content;
-(void)request:(PHPublisherContentRequest*)request contentDidDisplay:(PHContent*)content;
-(void)request:(PHPublisherContentRequest*)request contentDidDismissWithType:(PHPublisherContentDismissType*)type;
-(void)request:(PHPublisherContentRequest*)request didFailWithError:(NSError*)error;
-(void)request:(PHPublisherContentRequest*)request makePurchase:(PHPurchase *)purchase;
-(void)request:(PHPublisherContentRequest *)request unlockedReward:(PHReward *)reward;
@end

@implementation PHHandler
-(void)requestWillGetContent:(PHPublisherContentRequest*)request
{
    CLAW_MSG( "playhaven request will get content" );
}
-(void)requestDidGetContent:(PHPublisherContentRequest*)request
{
    CLAW_MSG( "playhaven did get content" );
}
-(void)request:(PHPublisherContentRequest*)request contentWillDisplay:(PHContent*)content
{
    CLAW_MSG( "playhaven content will display" );
}
-(void)request:(PHPublisherContentRequest*)request contentDidDisplay:(PHContent*)content
{
    CLAW_MSG( "playhaven content did display" );
    state = ClawExt::Playhaven::PS_SHOWN;

    Claw::NarrowString placement( [request.placement UTF8String] );
    ClawExt::s_instance->NotifyPlacementShown( placement );
}
-(void)request:(PHPublisherContentRequest*)request contentDidDismissWithType:(PHPublisherContentDismissType*)type
{
    CLAW_MSG( "playhaven did dismiss with type" );
    state = ClawExt::Playhaven::PS_CLOSED;

    Claw::NarrowString placement( [request.placement UTF8String] );
    ClawExt::s_instance->NotifyPlacementClosed( placement );
}
-(void)request:(PHPublisherContentRequest*)request didFailWithError:(NSError*)error
{
    CLAW_MSG( "playhaven did fail with error" );
    state = ClawExt::Playhaven::PS_FAILED;

    Claw::NarrowString placement( [request.placement UTF8String] );
    ClawExt::s_instance->NotifyPlacementFailed( placement );
}
-(void)request:(PHPublisherContentRequest*)request makePurchase:(PHPurchase *)purchase
{
    CLAW_MSG( "make purchase" );
    state = ClawExt::Playhaven::PS_CLOSED;
    [purchase retain];
    s_purchase = purchase;
    Claw::NarrowString iapId( [purchase.productIdentifier UTF8String] );
    ClawExt::s_instance->NotifyMakePurchase( iapId );
}
-(void)request:(PHPublisherContentRequest *)request unlockedReward:(PHReward *)reward
{
    CLAW_MSG( "unlock reward" );
    Claw::NarrowString rewardName( [reward.name UTF8String] );
    ClawExt::s_instance->NotifyUnlockReward( rewardName, reward.quantity );
}
@end

namespace ClawExt
{
    static PHPublisherContentRequest* s_request = NULL;
    static PHHandler* s_handler = NULL;

    /*static*/ Playhaven* Playhaven::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhonePlayhaven;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Playhaven::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    IPhonePlayhaven::~IPhonePlayhaven()
    {
        if( s_handler )
        {
            [s_handler release];
            s_handler = NULL;
        }

        if( s_request )
        {
            [s_request release];
            s_request = NULL;
        }
    }

    void IPhonePlayhaven::Initialize( const char* token, const char* secret )
    {
        CLAW_MSG( "IPhonePlayhaven::Initialize() token: " << token << " secret: " << secret );

        m_token = token;
        m_secret = secret;

        ReportOpen();
    }

    void IPhonePlayhaven::ReportOpen()
    {
        CLAW_MSG( "IPhonePlayhaven::ReportOpen()" );

        NSString* phToken = [NSString stringWithCString:m_token encoding:NSASCIIStringEncoding];
        NSString* phSecret = [NSString stringWithCString:m_secret encoding:NSASCIIStringEncoding]; 

        [[PHPublisherOpenRequest requestForApp:phToken secret:phSecret] send];
    }

    void IPhonePlayhaven::CancelRequest()
    {
        CLAW_MSG( "IPhonePlayhaven::CancelRequest()" );

        if( s_request )
        {
            [s_request cancel];
            s_request = NULL;
        }
    }

    void IPhonePlayhaven::ContentPlacement( const char* place )
    {
        CLAW_MSG( "IPhonePlayhaven::ContentPlacement() place: " << place );
        if( s_handler == NULL )
        {
            s_handler = [[PHHandler alloc] init];
        }
        NSString* str = [NSString stringWithUTF8String:place];
        NSString* phToken = [NSString stringWithCString:m_token encoding:NSASCIIStringEncoding];
        NSString* phSecret = [NSString stringWithCString:m_secret encoding:NSASCIIStringEncoding]; 

        s_request = [PHPublisherContentRequest requestForApp:phToken secret:phSecret placement:str delegate:s_handler];

        //req.showsOverlayImmediately = YES;
        state = PS_WAITING;
        [s_request send];
    }

    IPhonePlayhaven::PlayhavenState IPhonePlayhaven::GetState() const
    {
        CLAW_MSG( "IPhonePlayhaven::GetState()" );
        return state;
    }

    void IPhonePlayhaven::NotifyPurchaseResult( PurchaseResult result )
    {
        CLAW_MSG( "IPhonePlayhaven::NotifyPurchaseResult()" );
        if( s_purchase )
        {
            PHPurchaseResolutionType resolution;
            switch(result)
            {
                case PR_COMPLETED:
                    resolution = PHPurchaseResolutionBuy;
                    break;
                case PR_CANCELED:
                    resolution = PHPurchaseResolutionCancel;
                    break;
                case PR_FAILED:
                    resolution = PHPurchaseResolutionFailure;
                    break;
                default:
                    CLAW_ASSERT( !"Unknown purchase result" );
            }

            [s_purchase reportResolution:resolution];
            PHPublisherIAPTrackingRequest* req = [PHPublisherIAPTrackingRequest requestForApp:[NSString stringWithUTF8String:m_token] secret:[NSString stringWithUTF8String:m_secret] product:s_purchase.productIdentifier quantity:s_purchase.quantity resolution:resolution];
            [req send];

            [s_purchase release];
            s_purchase = NULL;
        }
    }

} // namespace ClawExt

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/iphone/IPhonePlayhaven.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__IPHONEPLAYHAVEN_HPP__
#define __INCLIDED__IPHONEPLAYHAVEN_HPP__

#include "claw_ext/monetization/playhaven/Playhaven.hpp"

namespace ClawExt
{
    class IPhonePlayhaven : public Playhaven
    {
    public:
                                ~IPhonePlayhaven();
        virtual void            Initialize( const char* token, const char* secret );
        virtual void            ReportOpen();
        virtual void            CancelRequest();
        virtual void            ContentPlacement( const char* place );
        virtual PlayhavenState  GetState() const;
        virtual void            NotifyPurchaseResult( PurchaseResult result );

    private:
        const char* m_token;
        const char* m_secret;

    }; // class IPhonePlayhaven
} // namespace ClawExt

#endif // __INCLIDED__IPHONEPLAYHAVEN_HPP__

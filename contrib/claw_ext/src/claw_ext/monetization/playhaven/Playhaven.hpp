//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/Playhaven.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__PLAYHAVEN_HPP__
#define __INCLUDED__PLAYHAVEN_HPP__

#include "claw/base/String.hpp"
#include <stdlib.h>
#include <set>

namespace ClawExt
{
    class Playhaven
    {
    public:
        class PlayhavenObserver
        {
        public:
            virtual void OnMakePurchase( const Claw::NarrowString& iapId ) = 0;
            virtual void OnUnlockReward( const Claw::NarrowString& rewardName, int rewardQuantity ) = 0;
        };

        class PlacementObserver
        {
        public:
            virtual void OnPlacementClosed( const Claw::NarrowString& placement ) = 0;
            virtual void OnPlacementShown( const Claw::NarrowString& placement ) = 0;
            virtual void OnPlacementFailed( const Claw::NarrowString& placement ) = 0;
        };

        enum PlayhavenState
        {
            PS_CLOSED,
            PS_WAITING,
            PS_SHOWN,
            PS_FAILED,
        };

        enum PurchaseResult
        {
            PR_CANCELED,
            PR_COMPLETED,
            PR_FAILED
        };

        virtual void            Initialize( const char* token, const char* secret ) = 0;
        virtual void            ReportOpen() = 0;
        virtual void            CancelRequest() = 0;
        virtual void            ContentPlacement( const char* place ) = 0;
        virtual PlayhavenState  GetState() const = 0;
        virtual void            NotifyPurchaseResult( PurchaseResult result ) = 0;

        void                    RegisterObserver( PlayhavenObserver* callback );
        void                    UnregisterObserver( PlayhavenObserver* callback );

        void                    RegisterPlacementObserver( PlacementObserver* callback );
        void                    UnregisterPlacementObserver( PlacementObserver* callback );

        void                    NotifyMakePurchase( const Claw::NarrowString& iapId ) const;
        void                    NotifyUnlockReward( const Claw::NarrowString& rewardName, int rewardQuantity ) const;
        void                    NotifyPlacementClosed( const Claw::NarrowString& placement );
        void                    NotifyPlacementShown( const Claw::NarrowString& placement );
        void                    NotifyPlacementFailed( const Claw::NarrowString& placement );

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Chartboost object implementation.
        */
        static Playhaven*       QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void             Release();

    private:
        std::set<PlayhavenObserver*>    m_observers;
        std::set<PlacementObserver*>    m_placementObservers;

    }; // class Playhaven
} // namespace ClawExt

#endif // __INCLUDED__PLAYHAVEN_HPP__

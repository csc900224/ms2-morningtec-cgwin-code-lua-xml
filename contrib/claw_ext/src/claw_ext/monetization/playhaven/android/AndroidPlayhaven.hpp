//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/android/AndroidPlayhaven.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__ANDROIDPLAYHAVEN_HPP__
#define __INCLIDED__ANDROIDPLAYHAVEN_HPP__

#include "claw_ext/monetization/playhaven/Playhaven.hpp"

namespace ClawExt
{
    class AndroidPlayhaven : public Playhaven
    {
    public:
        virtual void            Initialize( const char* token, const char* secret );
        virtual void            ReportOpen();
        virtual void            CancelRequest();
        virtual void            ContentPlacement( const char* place );
        virtual PlayhavenState  GetState() const;
        virtual void            NotifyPurchaseResult( PurchaseResult result );

    }; // class AndroidPlayhaven
} // namespace ClawExt

#endif // __INCLIDED__ANDROIDPLAYHAVEN_HPP__

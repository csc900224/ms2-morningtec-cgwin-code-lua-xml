//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/android/AndroidPlayhaven.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/playhaven/android/AndroidPlayhaven.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/system/android/JniAttach.hpp"

static ClawExt::Playhaven::PlayhavenState state = ClawExt::Playhaven::PS_CLOSED;

namespace ClawExt
{
    static AndroidPlayhaven* s_instance = NULL;

    /*static*/ Playhaven* Playhaven::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidPlayhaven;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Playhaven::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void AndroidPlayhaven::Initialize( const char* token, const char* secret )
    {
        CLAW_MSG( "AndroidPlayhaven::Initialize() token: " << token << " secret: " << secret );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring user, pass;
        user = Claw::JniAttach::GetStringFromChars( env, token );
        pass = Claw::JniAttach::GetStringFromChars( env, secret );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/playhaven/Playhaven", "initialize", "(Ljava/lang/String;Ljava/lang/String;)V", user, pass );
       
        Claw::JniAttach::ReleaseString( env, user );
        Claw::JniAttach::ReleaseString( env, pass );

        Claw::JniAttach::Detach( attached );

        ReportOpen();
    }

    void AndroidPlayhaven::ReportOpen()
    {
        CLAW_MSG( "AndroidPlayhaven::ReportOpen()" );
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/playhaven/Playhaven", "reportopen", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidPlayhaven::CancelRequest()
    {
        CLAW_MSG( "AndroidPlayhaven::CancelRequest()" );
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/playhaven/Playhaven", "cancel", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidPlayhaven::ContentPlacement( const char* place )
    {
        CLAW_MSG( "AndroidPlayhaven::ContentPlacement() place: " << place );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        jstring placeStr = Claw::JniAttach::GetStringFromChars( env, place );

        state = PS_WAITING;
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/playhaven/Playhaven", "placement", "(Ljava/lang/String;Z)V", placeStr, true );

        Claw::JniAttach::ReleaseString( env, placeStr );
        Claw::JniAttach::Detach( attached );
    }

    AndroidPlayhaven::PlayhavenState AndroidPlayhaven::GetState() const
    {
        //CLAW_MSG( "AndroidPlayhaven::GetState()" );
        return state;
    }

    void AndroidPlayhaven::NotifyPurchaseResult( PurchaseResult result )
    {
        CLAW_MSG( "AndroidPlayhaven::NotifyPurchaseResult(): " << result );

        const char* method = "";

        switch( result )
        {
        case PR_CANCELED:
            method = "purchaseResultCanceled";
            break;
        case PR_COMPLETED:
            method = "purchaseResultCompleted";
            break;
        case PR_FAILED:
            method = "purchaseResultFailed";
            break;
        default:
            CLAW_ASSERT( !"Unknown purchase result" );
        }

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/playhaven/Playhaven", method, "()V" );
        Claw::JniAttach::Detach( attached );
    }

} // namespace ClawExt

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_playhaven_Playhaven_ClosedPHAd( JNIEnv* env, jclass clazz, jstring placement )
    {
        CLAW_MSG( "Playhaven ad closed." );
        state = ClawExt::Playhaven::PS_CLOSED;

        Claw::NarrowString placementStr = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, placement, buffer, buffSize );
        placementStr = buffer;

        ClawExt::s_instance->NotifyPlacementClosed( placementStr );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_playhaven_Playhaven_FailedPHAd( JNIEnv* env, jclass clazz, jstring placement )
    {
        CLAW_MSG( "Playhaven ad failed." );
        state = ClawExt::Playhaven::PS_FAILED;

        Claw::NarrowString placementStr = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, placement, buffer, buffSize );
        placementStr = buffer;

        ClawExt::s_instance->NotifyPlacementFailed( placementStr );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_playhaven_Playhaven_DisplayedPHAd( JNIEnv* env, jclass clazz, jstring placement )
    {
        CLAW_MSG( "Playhaven ad displayed." );
        state = ClawExt::Playhaven::PS_SHOWN;

        Claw::NarrowString placementStr = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, placement, buffer, buffSize );
        placementStr = buffer;

        ClawExt::s_instance->NotifyPlacementShown( placementStr );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_playhaven_Playhaven_MakePurchase( JNIEnv* env, jclass clazz, jstring iapId )
    {
        CLAW_MSG( "Playhaven make purchase." );

        Claw::NarrowString iapIdStr = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, iapId, buffer, buffSize );
        iapIdStr = buffer;

        ClawExt::s_instance->NotifyMakePurchase( iapIdStr );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_playhaven_Playhaven_UnlockReward( JNIEnv* env, jclass clazz, jstring reward, int quantity )
    {
        CLAW_MSG( "Playhaven unlock reward." );

        Claw::NarrowString rewardStr = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, reward, buffer, buffSize );
        rewardStr = buffer;

        ClawExt::s_instance->NotifyUnlockReward( rewardStr, quantity );
    }
}

package com.gamelion.playhaven;

import org.json.JSONObject;
import android.util.Log;

import com.playhaven.src.common.PHConfig;
import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.publishersdk.open.PHPublisherOpenRequest;
import com.playhaven.src.publishersdk.content.PHContent;
import com.playhaven.src.publishersdk.content.PHPurchase;
import com.playhaven.src.publishersdk.content.PHPublisherContentRequest;
import com.playhaven.src.publishersdk.purchases.PHPublisherIAPTrackingRequest;
import com.playhaven.src.publishersdk.content.PHReward;
import com.Claw.Android.ClawActivityCommon;

public class Playhaven
{
    private static final String TAG = "Playhaven";
    private static final boolean DEBUG = false;
    private static PHPublisherContentRequest request = null;
    private static PHPurchase purchase = null;
    
    public static void initialize( final String key, final String secret )
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (DEBUG) Log.i( TAG, "initialize: " + key + "; " + secret );
            PHConfig.token = new String(key);
            PHConfig.secret = new String(secret);
        }});
    }
    
    public static void reportopen()
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (DEBUG) Log.i( TAG, "reportopen" );
            PHPublisherOpenRequest req = new PHPublisherOpenRequest( ClawActivityCommon.mActivity );
            req.send();
        }});
    }
    
    public static void cancel()
    {
        if( request != null )
        {
            request.setTargetState( PHPublisherContentRequest.PHRequestState.Preloaded );
            request = null;
        }
    }
    
    public static void placement( final String place, final boolean callback )
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if (DEBUG) Log.i( TAG, "placement: " + place );
            request = new PHPublisherContentRequest( ClawActivityCommon.mActivity, place );
            if (DEBUG) Log.i( TAG, "placement: request created" );
            if( callback )
            {
                if( DEBUG ) Log.i( TAG, "placement: registering callback" );
                request.setDelegates( phdelegate );
            }
            request.send();
            if (DEBUG) Log.i( TAG, "placement: request sent" );
        }});
    }

    public static void purchaseResultCanceled()
    {
        if (purchase != null)
        {
            if (DEBUG) Log.i( TAG, "purchaseResultCanceled()" );
            reportPurchase(PHPurchase.Resolution.Cancel);
        }
    }

    public static void purchaseResultCompleted()
    {
        if (purchase != null)
        {
            if (DEBUG) Log.i( TAG, "purchaseResultCompleted()" );
            reportPurchase(PHPurchase.Resolution.Buy);
        }
    }

    public static void purchaseResultFailed()
    {
        if (purchase != null)
        {
            if (DEBUG) Log.i( TAG, "purchaseResultFailed()" );
            reportPurchase(PHPurchase.Resolution.Error);
        }
    }
    
    private static void reportPurchase(PHPurchase.Resolution resolution)
    {
        if (purchase != null)
        {
            purchase.reportResolution(resolution, ClawActivityCommon.mActivity);
            PHPublisherIAPTrackingRequest trackingRequest = new PHPublisherIAPTrackingRequest(ClawActivityCommon.mActivity, purchase);
            trackingRequest.send();
            purchase = null;
        }
    }

    private static class PHDelegate implements PHPublisherContentRequest.ContentDelegate, PHPublisherContentRequest.FailureDelegate, PHPublisherContentRequest.PurchaseDelegate, PHPublisherContentRequest.RewardDelegate
    {
        @Override
        public void didDismissContent( PHPublisherContentRequest request, PHPublisherContentRequest.PHDismissType content )
        {
            if (DEBUG) Log.i( TAG, "didDismissContent()" );
            Playhaven.ClosedPHAd( request.placement );
        }

        @Override
        public void didFail( PHPublisherContentRequest request, String error )
        {
            if (DEBUG) Log.i( TAG, "didFail()" );
            Playhaven.FailedPHAd( request.placement );
        }

        @Override
        public void contentDidFail( PHPublisherContentRequest request, Exception e )
        {
            if (DEBUG) Log.i( TAG, "contentDidFail()" );
            Playhaven.FailedPHAd( request.placement );
        }

        @Override
	    public void willGetContent( PHPublisherContentRequest request )
        {
            if (DEBUG) Log.i( TAG, "willGetContent()" );
        }

        @Override
        public void willDisplayContent( PHPublisherContentRequest request, PHContent content )
        {
            if (DEBUG) Log.i( TAG, "willDisplayContent()" );
        }

        @Override
        public void didDisplayContent( PHPublisherContentRequest request, PHContent content )
        {
            if (DEBUG) Log.i( TAG, "didDisplayContent()" );
            Playhaven.DisplayedPHAd( request.placement );
        }

	    public void requestSucceeded( PHAPIRequest request, JSONObject responseData )
        {
            if (DEBUG) Log.i( TAG, "requestSucceeded()" );
        }

        public void requestFailed( PHAPIRequest request, Exception e )
        {
            if (DEBUG) Log.i( TAG, "requestFailed()" );
        }
        
        public void shouldMakePurchase( PHPublisherContentRequest request, PHPurchase purchase )
        {
            if (DEBUG) Log.i( TAG, "shouldMakePurchase(): " + request );
            Playhaven.purchase = purchase;
            MakePurchase(purchase.product);
        }
        
        public void unlockedReward( PHPublisherContentRequest request, PHReward reward )
        {
            if (DEBUG) Log.i( TAG, "unlockedReward(): " + request );
            UnlockReward(reward.name, reward.quantity);
        }
    };

    private static PHDelegate phdelegate = new PHDelegate();

    private static native void ClosedPHAd(String placement);
    private static native void FailedPHAd(String placement);
    private static native void DisplayedPHAd(String placement);
    private static native void MakePurchase(String iapId);
    private static native void UnlockReward(String reward, int quantity);
}

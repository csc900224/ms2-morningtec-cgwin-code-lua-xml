//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/playhaven/Playhaven.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/playhaven/Playhaven.hpp"

namespace ClawExt
{
    void Playhaven::RegisterObserver( PlayhavenObserver* callback )
    {
        m_observers.insert( callback );
    }

    void Playhaven::UnregisterObserver( PlayhavenObserver* callback )
    {
        m_observers.erase( callback );
    }

    void Playhaven::RegisterPlacementObserver( PlacementObserver* callback )
    {
        m_placementObservers.insert( callback );
    }

    void Playhaven::UnregisterPlacementObserver( PlacementObserver* callback )
    {
        m_placementObservers.erase( callback );
    }

    void Playhaven::NotifyMakePurchase( const Claw::NarrowString& iapId ) const
    {
        std::set<PlayhavenObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnMakePurchase( iapId );
        }
    }

    void Playhaven::NotifyUnlockReward( const Claw::NarrowString& rewardName, int rewardQuantity ) const
    {
        std::set<PlayhavenObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnUnlockReward( rewardName, rewardQuantity );
        }
    }

    void Playhaven::NotifyPlacementClosed( const Claw::NarrowString& placement )
    {
        std::set<PlacementObserver*>::const_iterator it, end;
        it = m_placementObservers.begin();
        end = m_placementObservers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnPlacementClosed( placement );
        }
    }

    void Playhaven::NotifyPlacementShown( const Claw::NarrowString& placement )
    {
        std::set<PlacementObserver*>::const_iterator it, end;
        it = m_placementObservers.begin();
        end = m_placementObservers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnPlacementShown( placement );
        }
    }

    void Playhaven::NotifyPlacementFailed( const Claw::NarrowString& placement )
    {
        std::set<PlacementObserver*>::const_iterator it, end;
        it = m_placementObservers.begin();
        end = m_placementObservers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnPlacementFailed( placement );
        }
    }
}

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/iphone/IPhoneChartboost.mm
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/chartboost/iphone/IPhoneChartboost.hpp"

// External includes
#include "claw/base/Errors.hpp"

#import <Foundation/Foundation.h>
#import "claw_ext/monetization/chartboost/iphone/ChartboostSDK/Chartboost.h"


static ClawExt::IPhoneChartboost::CharboostState state = ClawExt::IPhoneChartboost::CS_CLOSED;

@interface ChartboostCallback : NSObject<ChartboostDelegate>
{
}
- (BOOL)shouldDisplayInterstitial:(NSString *)location;
- (void)didFailToLoadInterstitial:(NSString *)location;
- (void)didDismissInterstitial:(NSString *)location;
@end

@implementation ChartboostCallback
- (BOOL)shouldDisplayInterstitial:(NSString *)location
{
    CLAW_MSG( "SHOULD DISPLAY CHARTBOOST AD?" );
    if( state == ClawExt::IPhoneChartboost::CS_WAITING )
    {
        state = ClawExt::IPhoneChartboost::CS_SHOWN;
        return YES;
    }
    return NO;
}

- (void)didFailToLoadInterstitial:(NSString *)location
{
    CLAW_MSG( "Chartboost ad failed." );
    state = ClawExt::IPhoneChartboost::CS_FAILED;
}

- (void)didDismissInterstitial:(NSString *)location
{
    CLAW_MSG( "Chartboost ad closed." );
    state = ClawExt::IPhoneChartboost::CS_CLOSED;
}
@end

namespace ClawExt
{
    static IPhoneChartboost* s_instance = NULL;

    /*static*/ Chartboost* Chartboost::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneChartboost;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Chartboost::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void IPhoneChartboost::Initialize( const char* appId, const char* appSignature )
    {
        CLAW_MSG( "IPhoneChartboost::Initialize() appId: " << appId << " singature: " << appSignature );
        
        // Configure cahrtboost
        ::Chartboost *cb = [::Chartboost sharedChartboost];
        cb.appId = [NSString stringWithCString:appId encoding:NSASCIIStringEncoding];
        cb.appSignature = [NSString stringWithCString:appSignature encoding:NSASCIIStringEncoding];
        
        // Set delegate
        [cb setDelegate:[[ChartboostCallback alloc] init]];

        // Start Chartboost session
        [cb startSession];
    }

    bool IPhoneChartboost::RequestAd()
    {
        CLAW_MSG( "IPhoneChartboost::RequestAd()" );
        [[::Chartboost sharedChartboost] showInterstitial];
        state = CS_WAITING;
        return true;
    }

    void IPhoneChartboost::CancelRequest()
    {
        CLAW_MSG( "IPhoneChartboost::CancelRequest()" );
        state = CS_FAILED;
    }

    Chartboost::CharboostState IPhoneChartboost::GetState() const
    {
        CLAW_MSG( "IPhoneChartboost::GetState()" );
        return state;
    }

    void IPhoneChartboost::OnResume()
    {
        CLAW_MSG( "IPhoneChartboost::OnResume()" );
    }

} // namespace ClawExt

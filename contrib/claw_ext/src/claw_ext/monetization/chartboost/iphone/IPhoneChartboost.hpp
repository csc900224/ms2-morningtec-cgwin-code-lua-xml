//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/iphone/IPhoneChartboost.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__IPHONECHARTBOOST_HPP__
#define __INCLIDED__IPHONECHARTBOOST_HPP__

#include "claw_ext/monetization/chartboost/Chartboost.hpp"

namespace ClawExt
{
    class IPhoneChartboost : public Chartboost
    {
    public:
        virtual void            Initialize( const char* appId, const char* appSignature );
        virtual bool            RequestAd();
        virtual void            CancelRequest();
        virtual CharboostState  GetState() const;
        virtual void            OnResume();

    }; // class IPhoneChartboost
} // namespace ClawExt

#endif // __INCLIDED__IPHONECHARTBOOST_HPP__

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/win32/Win32Chartboost.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/chartboost/win32/Win32Chartboost.hpp"
#include "claw/base/Errors.hpp"

namespace ClawExt
{
    static Win32Chartboost* s_instance = NULL;

    /*static*/ Chartboost* Chartboost::QueryInterface()
    {
        if( !s_instance )
            s_instance = new Win32Chartboost;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Chartboost::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void Win32Chartboost::Initialize( const char* appId, const char* appSignature )
    {
        CLAW_MSG( "Chartboost::Initialize() appId: " << appId << " singature: " << appSignature );
    }

    bool Win32Chartboost::RequestAd()
    {
        CLAW_MSG( "Chartboost::RequestAd()" );
        return false;
    }

    void Win32Chartboost::CancelRequest()
    {
        CLAW_MSG( "Chartboost::CancelRequest()" );
    }

    Chartboost::CharboostState Win32Chartboost::GetState() const
    {
        CLAW_MSG( "Chartboost::GetState()" );
        return CS_CLOSED;
    }

    void Win32Chartboost::OnResume()
    {
        CLAW_MSG( "Chartboost::OnResume()" );
    }

} // namespace ClawExt

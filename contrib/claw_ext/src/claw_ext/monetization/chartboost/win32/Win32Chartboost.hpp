//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/win32/Win32Chartboost.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__WIN32CHARTBOOST_HPP__
#define __INCLIDED__WIN32CHARTBOOST_HPP__

#include "claw_ext/monetization/chartboost/Chartboost.hpp"

namespace ClawExt
{
    class Win32Chartboost : public Chartboost
    {
    public:
        virtual void            Initialize( const char* appId, const char* appSignature );
        virtual bool            RequestAd();
        virtual void            CancelRequest();
        virtual CharboostState  GetState() const;
        virtual void            OnResume();

    }; // class Win32Chartboost
} // namespace ClawExt

#endif // __INCLIDED__WIN32CHARTBOOST_HPP__

package com.gamelion.chartboost;

import android.util.Log;
import android.os.Looper;
import android.view.View;

import com.chartboost.sdk.*;

import com.gamelion.chartboost.Consts;
import com.Claw.Android.ClawActivityCommon;

public class Chartboost
{
    public static final String TAG = "ChartBoost";

    public static void initialize(final String appId, final String appSignature)
    {
        ClawActivityCommon.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (Consts.DEBUG) Log.i(TAG, "initialize(): appId: " + appId + " signature: " + appSignature);

                // Configure ChartBoost
                ChartBoost cb = ChartBoost.getSharedChartBoost(ClawActivityCommon.mActivity);
                cb.setAppId(appId);
                cb.setAppSignature(appSignature);

                cb.setDelegate( Chartboost.chartboostDelegate );

                // Notify an install
                cb.install();
            }
        });
    }

    public static void requestAd()
    {
        ClawActivityCommon.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (Consts.DEBUG) Log.i(TAG, "requestAd()");

                // Load an interstitial
                ChartBoost.getSharedChartBoost(ClawActivityCommon.mActivity).showInterstitial();
            }
        });
    }

    public static void onResume()
    {
        ClawActivityCommon.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (Consts.DEBUG) Log.i(TAG, "onResume()");
                ChartBoost.getSharedChartBoost(ClawActivityCommon.mActivity);
            }
        });
    }

    private static ChartBoostDelegate chartboostDelegate = new ChartBoostDelegate()
    {
        @Override
        public boolean shouldDisplayInterstitial( View view )
        {
            return Chartboost.ShouldDisplayCBAd();
        }

        @Override
        public void didFailToLoadInterstitial()
        {
            Chartboost.FailedCBAd();
        }

        @Override
        public void didDismissInterstitial( View view )
        {
            Chartboost.ClosedCBAd();
        }

        @Override
        public void didCloseInterstitial( View view )
        {
            Chartboost.ClosedCBAd();
        }

        @Override
        public void didClickInterstitial( View view )
        {
            Chartboost.ClosedCBAd();
        }
    };

    private static native boolean ShouldDisplayCBAd();
    private static native void ClosedCBAd();
    private static native void FailedCBAd();
}

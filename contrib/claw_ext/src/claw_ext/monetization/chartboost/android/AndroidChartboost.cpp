//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/android/AndroidCharboost.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw/system/android/JniAttach.hpp"
#include "claw/base/Errors.hpp"

#include "claw_ext/monetization/chartboost/android/AndroidChartboost.hpp"

namespace ClawExt
{
    static AndroidChartboost* s_instance = NULL;
    static AndroidChartboost::CharboostState state = AndroidChartboost::CS_CLOSED;

    /*static*/ Chartboost* Chartboost::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidChartboost;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Chartboost::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    AndroidChartboost::AndroidChartboost()
    {}

    void AndroidChartboost::Initialize( const char* appId, const char* appSignature )
    {
        CLAW_MSG( "AndroidChartboost::Initialize()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring appIdString = Claw::JniAttach::GetStringFromChars( env, appId );
        jstring appSignatureString = Claw::JniAttach::GetStringFromChars( env, appSignature );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/chartboost/Chartboost", "initialize", "(Ljava/lang/String;Ljava/lang/String;)V", appIdString, appSignatureString );
    
        Claw::JniAttach::ReleaseString( env, appIdString );
        Claw::JniAttach::ReleaseString( env, appSignatureString );

        Claw::JniAttach::Detach( attached );
    }

    bool AndroidChartboost::RequestAd()
    {
        CLAW_MSG( "AndroidChartboost::RequestAd()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/chartboost/Chartboost", "requestAd", "()V" );
        Claw::JniAttach::Detach( attached );
        
        state = CS_WAITING;
        return true;
    }

    void AndroidChartboost::CancelRequest()
    {
        CLAW_MSG( "AndroidChartboost::CancelRequest()" );
        state = CS_FAILED;
    }

    void AndroidChartboost::OnResume()
    {
        CLAW_MSG( "AndroidChartboost::OnResume()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/chartboost/Chartboost", "onResume", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    Chartboost::CharboostState AndroidChartboost::GetState() const
    {
        CLAW_MSG( "AndroidChartboost::GetState()" );
        return state;
    }

} // namespace ClawExt

extern "C"
{
    JNIEXPORT bool JNICALL Java_com_gamelion_chartboost_Chartboost_ShouldDisplayCBAd( JNIEnv* env )
    {
        CLAW_MSG( "SHOULD DISPLAY CHARTBOOST AD?" );
        if( ClawExt::state == ClawExt::AndroidChartboost::CS_WAITING )
        {
            ClawExt::state = ClawExt::AndroidChartboost::CS_SHOWN;
            return true;
        }
        return false;
    }

    JNIEXPORT void JNICALL Java_com_gamelion_chartboost_Chartboost_ClosedCBAd( JNIEnv* env )
    {
        CLAW_MSG( "Chartboost ad closed." );
        ClawExt::state = ClawExt::AndroidChartboost::CS_CLOSED;
    }

    JNIEXPORT void JNICALL Java_com_gamelion_chartboost_Chartboost_FailedCBAd( JNIEnv* env )
    {
        CLAW_MSG( "Chartboost ad failed." );
        ClawExt::state = ClawExt::AndroidChartboost::CS_FAILED;
    }
}

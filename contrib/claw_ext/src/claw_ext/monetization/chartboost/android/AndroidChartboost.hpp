//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/android/AndroidCharboost.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__ANDROIDCHARTBOOST_HPP__
#define __INCLIDED__ANDROIDCHARTBOOST_HPP__

#include "claw_ext/monetization/chartboost/Chartboost.hpp"

namespace ClawExt
{
    class AndroidChartboost : public Chartboost
    {
    public:
                                AndroidChartboost();
        virtual void            Initialize( const char* appId, const char* appSignature );
        virtual bool            RequestAd();
        virtual void            CancelRequest();
        virtual CharboostState  GetState() const;
        virtual void            OnResume();

    }; // class AndroidChartboost
} // namespace ClawExt

#endif // __INCLIDED__ANDROIDCHARTBOOST_HPP__

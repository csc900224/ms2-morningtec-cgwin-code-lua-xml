//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/chartboost/Chartboost.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__CHARTBOOST_HPP__
#define __INCLUDED__CHARTBOOST_HPP__

#include <stdlib.h>

namespace ClawExt
{
    class Chartboost
    {
    public:
        enum CharboostState
        {
            CS_CLOSED,
            CS_WAITING,
            CS_SHOWN,
            CS_FAILED,
        };

        virtual void Initialize( const char* appId, const char* appSignature ) = 0;

        virtual bool            RequestAd() = 0;
        virtual void            CancelRequest() = 0;
        virtual CharboostState  GetState() const = 0;

        virtual void            OnResume() = 0;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Chartboost object implementation.
        */
        static Chartboost*      QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void             Release();

    }; // class Chartboost
} // namespace ClawExt

#endif // __INCLUDED__CHARTBOOST_HPP__

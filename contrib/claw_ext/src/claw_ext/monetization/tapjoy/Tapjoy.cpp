//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/Tapjoy.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/tapjoy/Tapjoy.hpp"

namespace ClawExt
{
    void Tapjoy::RegisterObserver( TapjoyObserver* callback )
    {
        m_observers.insert( callback );
    }

    void Tapjoy::UnregisterObserver( TapjoyObserver* callback )
    {
        m_observers.erase( callback );
    }

    void Tapjoy::NotifyPointsEarned( int points ) const
    {
        std::set<TapjoyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->TapjoyPointsEarned( points );
        }
    }

    void Tapjoy::NotifyPointsUpdate( const Claw::NarrowString& currencyName, int pointsTotal ) const
    {
        std::set<TapjoyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->TapjoyPointsUpdate( currencyName, pointsTotal );
        }
    }

    void Tapjoy::NotifyFullScreenAdReceived() const
    {
        std::set<TapjoyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->TapjoyFullScreenAdReceived();
        }
    }

    void Tapjoy::NotifyFullScreenAdFailed( int error ) const
    {
        std::set<TapjoyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->TapjoyFullScreenAdFailed( error );
        }
    }
    
    void Tapjoy::NotifyTapjoyClosed() const
    {
        std::set<TapjoyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();
        
        for( ; it != end; ++it )
        {
            (*it)->TapjoyClosed();
        }
    }
} // namespace ClawExt

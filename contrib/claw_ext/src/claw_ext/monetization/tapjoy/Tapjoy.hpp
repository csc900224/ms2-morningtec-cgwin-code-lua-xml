//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/Tapjoy.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__TAPJOY_HPP__
#define __INCLUDED__TAPJOY_HPP__

#include "claw/base/String.hpp"
#include <set>

namespace ClawExt
{
    class Tapjoy
    {
    public:
        class TapjoyObserver
        {
        public:
            virtual void TapjoyPointsEarned( int points ) = 0;
            virtual void TapjoyClosed() = 0;
            virtual void TapjoyPointsUpdate( const Claw::NarrowString& currencyName, int pointsTotal ) = 0;
            virtual void TapjoyFullScreenAdReceived() = 0;
            virtual void TapjoyFullScreenAdFailed( int error ) = 0;
        };

        virtual bool        Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey ) = 0;

        virtual bool        ShowOffers() const = 0;

        virtual bool        SetBannerAdVisibility( bool visible ) const = 0;

        virtual bool        ShowFullScreenAd() const = 0;

        virtual bool        CheckPoints() const = 0;

        virtual void        FlushCash( int points ) const = 0;

        virtual bool        ReportActionComplete( const Claw::NarrowString& actionId ) const = 0;

        virtual void        OnFocusChange( bool focus ) const = 0;


        void                RegisterObserver( TapjoyObserver* callback );

        void                UnregisterObserver( TapjoyObserver* callback );

        void                NotifyPointsEarned( int points ) const;

        void                NotifyPointsUpdate( const Claw::NarrowString& currencyName, int pointsTotal ) const;

        void                NotifyFullScreenAdReceived() const;

        void                NotifyFullScreenAdFailed( int error ) const;
        
        void                NotifyTapjoyClosed() const;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Tapjoy object implementation.
        */
        static Tapjoy*      QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void         Release();

    private:
        std::set<TapjoyObserver*>    m_observers;
    }; // class Tapjoy
} // namespace ClawExt

#endif // __INCLUDED__TAPJOY_HPP__

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/win32/Win32Tapjoy.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__WIN32TAPJOY_HPP__
#define __INCLUDED__WIN32TAPJOY_HPP__

#include "claw_ext/monetization/tapjoy/Tapjoy.hpp"

namespace ClawExt
{
    class Win32Tapjoy : public Tapjoy
    {
    public:
        virtual bool        Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey );
        virtual bool        ShowOffers() const;
        virtual bool        SetUiColor( int argbColor ) const;
        virtual bool        SetBannerAdVisibility( bool visible ) const;
        virtual bool        ShowFullScreenAd() const;
        virtual bool        CheckPoints() const;
        virtual void        FlushCash( int points ) const;
        virtual bool        ReportActionComplete( const Claw::NarrowString& actionId ) const;
        virtual void        OnFocusChange( bool focus ) const;
    }; // class Win32Tapjoy
} // namespace ClawExt

#endif // __INCLUDED__WIN32TAPJOY_HPP__

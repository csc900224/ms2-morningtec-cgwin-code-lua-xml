//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/win32/Win32Tapjoy.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw/base/Errors.hpp"
#include "claw_ext/monetization/tapjoy/win32/Win32Tapjoy.hpp"

namespace ClawExt
{
    static Win32Tapjoy* s_instance = NULL;
    static int tjpoints = 0;

    /*static*/ Tapjoy* Tapjoy::QueryInterface()
    {
        if( !s_instance )
            s_instance = new Win32Tapjoy;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Tapjoy::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    bool Win32Tapjoy::Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey )
    {
        CheckPoints();
        return true;
    }

    bool Win32Tapjoy::ShowOffers() const
    {
        tjpoints += 100;
        return true;
    }

    bool Win32Tapjoy::SetUiColor( int argbColor ) const
    {
        return false;
    }

    bool Win32Tapjoy::SetBannerAdVisibility( bool visible ) const
    {
        return false;
    }

    bool Win32Tapjoy::CheckPoints() const
    {
        Tapjoy::QueryInterface()->NotifyPointsEarned( tjpoints );
        tjpoints = 0;
        return true;
    }

    void Win32Tapjoy::FlushCash( int points ) const
    {
    }

    bool Win32Tapjoy::ShowFullScreenAd() const
    {
        return false;
    }
    
    bool Win32Tapjoy::ReportActionComplete( const Claw::NarrowString& actionId ) const
    {
        return false;
    }
    
    void Win32Tapjoy::OnFocusChange( bool focus ) const
    {
    }

} // namespace ClawExt

package com.gamelion.tapjoy;

import android.util.Log;
import android.view.View;
import android.view.Display;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;

import java.util.ArrayList;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;
import com.tapjoy.TapjoyEarnedPointsNotifier;
import com.tapjoy.TapjoyNotifier;
import com.tapjoy.TapjoyFullScreenAdNotifier;
import com.tapjoy.TapjoyViewNotifier;

import com.gamelion.tapjoy.Consts;
import com.Claw.Android.ClawActivityCommon;

public class Tapjoy implements TapjoyDisplayAdNotifier, TapjoyEarnedPointsNotifier, TapjoyNotifier, TapjoyFullScreenAdNotifier, TapjoyViewNotifier
{
    private static Tapjoy s_instance = null;
    public static final String TAG = "Tapjoy";

    public static Tapjoy getInstance()
    {
        if (s_instance == null)
        {
            s_instance = new Tapjoy();
            if (Consts.DEBUG) Log.i(TAG, "create instance");
        }
        return s_instance;
    }

    public static void initialize(String appId, String secretKey)
    {
        if (Consts.DEBUG) Log.i(TAG, "initialize(): AppID(" + appId + ") Secret(" + secretKey + ")");
        TapjoyConnect.requestTapjoyConnect(ClawActivityCommon.mActivity, appId, secretKey);
        TapjoyConnect.getTapjoyConnectInstance().setEarnedPointsNotifier(getInstance());
        TapjoyConnect.getTapjoyConnectInstance().setTapjoyViewNotifier(getInstance());
    }

    public static void showOffers()
    {
        if (Consts.DEBUG) Log.i(TAG, "showOffers()");
        TapjoyConnect.getTapjoyConnectInstance().showOffers();
    }

    public static void checkPoints()
    {
        if (Consts.DEBUG) Log.i(TAG, "checkPoints()");
        TapjoyConnect.getTapjoyConnectInstance().getTapPoints(getInstance());
    }

    public void earnedTapPoints(int amount)
    {
        if (Consts.DEBUG) Log.i(TAG, "earnedTapPoints(): " + amount + " points");
        nativeOnPointsEarned(amount);
    }

    public void getUpdatePoints(String currencyName, int pointsTotal)
    {
        if (Consts.DEBUG) Log.i(TAG, "getUpdatePoints(): cucurrencyName(" + currencyName + ") pointsToral(" + pointsTotal + ")");
        nativeOnPointsUpdate(currencyName, pointsTotal);
    }

    public void getUpdatePointsFailed(java.lang.String error)
    {
        if (Consts.DEBUG) Log.i(TAG, "getUpdatePointsFailed(): " + error );
    }

    public static void showBannerAd()
    {
        Tapjoy.getInstance().showBannerAdImpl();
    }

    public static void hideBannerAd()
    {
        Tapjoy.getInstance().hideBannerAdImpl();
    }

    private View mBannerAd = null;
    private boolean showBanner = true;

    private void showBannerAdImpl()
    {
        if (Consts.DEBUG) Log.i(TAG, "showBannerAdImpl()");
        if (mBannerAd == null)
        {
            if (Consts.DEBUG) Log.i(TAG, "showBannerAdImpl() : requesting banner");
            TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(ClawActivityCommon.mActivity, this);
        }
        showBanner = true;
    }

    private void hideBannerAdImpl()
    {
        if (Consts.DEBUG) Log.i(TAG, "hideBannerAdImpl()");
        if (mBannerAd != null)
        {
            ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
                ClawActivityCommon.mLayout.removeView(mBannerAd);
                mBannerAd = null;
                if (Consts.DEBUG) Log.i(TAG, "Banner removed");
            }});
            
        }
        showBanner = false;
    }

    public void getDisplayAdResponse(final View view)
    {
        if (Consts.DEBUG) Log.i(TAG, "getDisplayAdResponse()");
        if (mBannerAd == null && showBanner)
        {
            double bannerScale = 0.6;
            int bannerWidth = (int)(view.getLayoutParams().width * bannerScale);
            int bannerHeight = (int)(view.getLayoutParams().height * bannerScale);

            final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(bannerWidth, bannerHeight);

            // TODO: Add method for position selection
            params.leftMargin = 0;
            params.topMargin = ClawActivityCommon.mLayout.getHeight() - bannerHeight;

            ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
                mBannerAd = view;
                ClawActivityCommon.mLayout.addView(mBannerAd, params);
                if (Consts.DEBUG) Log.i(TAG, "Banner addded");
            }});
        }
    }

    public void getDisplayAdResponseFailed(String error)
    {
        mBannerAd = null;
        if (Consts.DEBUG) Log.i(TAG, "getDisplayAdResponseFailed(): " + error);
    }

    public static void showFullScreenAd()
    {
        if (Consts.DEBUG) Log.i(TAG, "showFeaturedApp()");
        TapjoyConnect.getTapjoyConnectInstance().getFullScreenAd(getInstance());
    }

    public void getFullScreenAdResponse() 
    {
        if (Consts.DEBUG) Log.i(TAG, "getFullScreenAdResponse()");
        nativeOnFullScreenAdResponse();
    }

    public void getFullScreenAdResponseFailed(int error)
    {
        if (Consts.DEBUG) Log.i(TAG, "getFullScreenAdResponseFailed(): " + error);
        nativeOnFullScreenAdResponseFailed(error);
    }

    public static void reportActionComplete(String actionId)
    {
        if (Consts.DEBUG) Log.i(TAG, "reportActionComplete(): " + actionId);
        TapjoyConnect.getTapjoyConnectInstance().actionComplete(actionId);
    }
    
    public static void appPause()
    {
        if (Consts.DEBUG) Log.i(TAG, "appPause()");
        TapjoyConnect.getTapjoyConnectInstance().appPause();
    }
    
    public static void appResume()
    {
        if (Consts.DEBUG) Log.i(TAG, "appResume()");
        TapjoyConnect.getTapjoyConnectInstance().appResume();
    }

    public void viewDidClose(int viewType)
    {
        nativeOnViewDidClose(viewType);
    }

    public void viewDidOpen(int viewType)
    {
        nativeOnViewDidOpen(viewType);
    }

    public void viewWillClose(int viewType)
    {}

    public void viewWillOpen(int viewType)
    {}

    // JNI Methods
    private static native void nativeOnFullScreenAdResponse();
    private static native void nativeOnFullScreenAdResponseFailed(int error);
    private static native void nativeOnPointsUpdate(String currencyName, int points);
    private static native void nativeOnPointsEarned(int points);
    private static native void nativeOnViewDidOpen(int viewType);
    private static native void nativeOnViewDidClose(int viewType);
}
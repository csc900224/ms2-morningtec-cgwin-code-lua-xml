//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/android/AndroidTapjoy.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__ANDROIDTAPJOY_HPP__
#define __INCLUDED__ANDROIDTAPJOY_HPP__

#include "claw_ext/monetization/tapjoy/Tapjoy.hpp"

namespace ClawExt
{
    class AndroidTapjoy : public Tapjoy
    {
    public:
        virtual bool        Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey );
        virtual bool        ShowOffers() const;
        virtual bool        SetBannerAdVisibility( bool visible ) const;
        virtual bool        ShowFullScreenAd() const;
        virtual bool        CheckPoints() const;
        virtual void        FlushCash( int points ) const {}
        virtual bool        ReportActionComplete( const Claw::NarrowString& actionId ) const;
        virtual void        OnFocusChange( bool focus ) const;
    }; // class AndroidTapjoy
} // namespace ClawExt

#endif // __INCLUDED__ANDROIDTAPJOY_HPP__

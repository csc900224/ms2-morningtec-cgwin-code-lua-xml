//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//     claw_ext/monetization/tapjoy/android/AndroidTapjoy.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/tapjoy/android/AndroidTapjoy.hpp"
#include "claw/system/android/JniAttach.hpp"
#include "claw/sound/mixer/Mixer.hpp"

namespace ClawExt
{
    static AndroidTapjoy* s_instance = NULL;

    /*static*/ Tapjoy* Tapjoy::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidTapjoy();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Tapjoy::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    bool AndroidTapjoy::Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey )
    {
        CLAW_MSG("AndroidTapjoy::Initialize()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring id, key;

        CLAW_ASSERT( !appId.empty() );
        if( !appId.empty() )
            id = Claw::JniAttach::GetStringFromChars( env, appId.c_str() );

        CLAW_ASSERT( !secretKey.empty() );
        if( !secretKey.empty() )
            key = Claw::JniAttach::GetStringFromChars( env, secretKey.c_str() );

        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "initialize", "(Ljava/lang/String;Ljava/lang/String;)V", id, key );

        Claw::JniAttach::ReleaseString( env, id );
        Claw::JniAttach::ReleaseString( env, key );

        Claw::JniAttach::Detach( attached );
        return ret;
    }

    bool AndroidTapjoy::ShowOffers() const
    {
        CLAW_MSG("AndroidTapjoy::ShowOffers()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "showOffers", "()V" );
        Claw::JniAttach::Detach( attached );
        return ret;
    }

    bool AndroidTapjoy::SetBannerAdVisibility( bool visible ) const
    {
        CLAW_MSG("AndroidTapjoy::SetBannerAdVisibility(): " << visible);
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        bool ret = false;
    
        if( visible )
        {
            ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "showBannerAd", "()V" );
        }
        else
        {
            ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "hideBannerAd", "()V" );
        }

        Claw::JniAttach::Detach( attached );
        return ret;
    }

    bool AndroidTapjoy::CheckPoints() const
    {
        CLAW_MSG("AndroidTapjoy::CheckPoints()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "checkPoints", "()V" );
        Claw::JniAttach::Detach( attached );
        return ret;
    }

    bool AndroidTapjoy::ShowFullScreenAd() const
    {
        CLAW_MSG("AndroidTapjoy::ShowFullScreenAd()");
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "showFullScreenAd", "()V" );
        Claw::JniAttach::Detach( attached );
        return ret;
    }
    
    bool AndroidTapjoy::ReportActionComplete( const Claw::NarrowString& actionId ) const
    {
        CLAW_MSG("AndroidTapjoy::ReportActionComplete(): " << actionId);
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        
        jstring id;
        CLAW_ASSERT( !actionId.empty() );
        if( !actionId.empty() )
            id = Claw::JniAttach::GetStringFromChars( env, actionId.c_str() );
        
        bool ret = Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", "reportActionComplete", "(Ljava/lang/String;)V", id );
        Claw::JniAttach::Detach( attached );
        return ret;
    }

    void AndroidTapjoy::OnFocusChange( bool focus ) const
    {
        CLAW_MSG("AndroidTapjoy::OnFocusChange(): " << focus);
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/tapjoy/Tapjoy", focus ? "appResume" : "appPause", "()V" );
        Claw::JniAttach::Detach( attached );
    }

} // namespace ClawExt

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_tapjoy_Tapjoy_nativeOnPointsEarned( JNIEnv* env, jclass clazz, jint points )
    {
        ClawExt::Tapjoy::QueryInterface()->NotifyPointsEarned( points );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_tapjoy_Tapjoy_nativeOnPointsUpdate( JNIEnv* env, jclass clazz, jstring currency, jint points )
    {
        Claw::NarrowString currencyName = "";

        const int buffSize = 512;
        char buffer[buffSize] = "";
        Claw::JniAttach::GetCharsFromString( env, currency, buffer, buffSize );
        currencyName = buffer;

        ClawExt::Tapjoy::QueryInterface()->NotifyPointsUpdate( currencyName, points );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_tapjoy_Tapjoy_nativeOnFullScreenAdResponse( JNIEnv* env, jclass clazz )
    {
        ClawExt::Tapjoy::QueryInterface()->NotifyFullScreenAdReceived();
    }

    JNIEXPORT void JNICALL Java_com_gamelion_tapjoy_Tapjoy_nativeOnFullScreenAdResponseFailed( JNIEnv* env, jclass clazz, jint error )
    {
        ClawExt::Tapjoy::QueryInterface()->NotifyFullScreenAdFailed( error );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_tapjoy_Tapjoy_nativeOnViewDidOpen( JNIEnv* env, jclass clazz, jint viewType )
    {
        Claw::Mixer::Get()->Pause(true);
    }

    JNIEXPORT void JNICALL Java_com_gamelion_tapjoy_Tapjoy_nativeOnViewDidClose( JNIEnv* env, jclass clazz, jint viewType )
    {
        Claw::Mixer::Get()->Pause(false);
        ClawExt::Tapjoy::QueryInterface()->NotifyTapjoyClosed();
    }
};

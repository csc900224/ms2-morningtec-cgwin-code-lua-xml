//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/iphone/IPhoneTapjoy.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__IPHONETAPJOY_HPP__
#define __INCLUDED__IPHONETAPJOY_HPP__

#include "claw_ext/monetization/tapjoy/Tapjoy.hpp"

namespace ClawExt
{
    class IPhoneTapjoy : public Tapjoy
    {
    public:
                            IPhoneTapjoy();
                            ~IPhoneTapjoy();

        virtual bool        Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey );
        virtual bool        ShowOffers() const;
        virtual bool        SetBannerAdVisibility( bool visible ) const;
        virtual bool        ShowFullScreenAd() const;
        virtual bool        CheckPoints() const;
        virtual void        FlushCash( int points ) const;
        virtual bool        ReportActionComplete( const Claw::NarrowString& actionId ) const;
        virtual void        OnFocusChange( bool focus ) const {}
    }; // class IPhoneTapjoy
} // namespac ClawExt

#endif // __INCLUDED__WIN32TAPJOY_HPP__

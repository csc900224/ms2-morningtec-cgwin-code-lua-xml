//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/tapjoy/iphone/IPhoneTapjoy.mm
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import "claw_ext/monetization/tapjoy/iphone/IPhoneTapjoy.hpp"

#import "claw/application/iphone/IPhoneAppDelegate.h"
#import "claw/base/Errors.hpp"
#import <Tapjoy/Tapjoy.h>


@interface TapjoyObserver: NSObject
{
}
-(void)getUpdatedPoints:(NSNotification*)notifyObj;
-(void)getFeaturedApp:(NSNotification*)notifyObj;
-(void)onClose:(NSNotification*)notifyObj;
-(void)onShow:(NSNotification*)notifyObj;
@end

@implementation TapjoyObserver
-(void)getUpdatedPoints:(NSNotification*)notifyObj
{
    NSNumber* tapPoints = notifyObj.object;
    int val = [tapPoints intValue];
    if( val != 0 )
    {
        ClawExt::Tapjoy::QueryInterface()->NotifyPointsEarned( val );
    }
}

-(void)onClose:(NSNotification *)notifyObj
{
    Claw::Mixer::Get()->Pause(false);
    ClawExt::Tapjoy::QueryInterface()->NotifyTapjoyClosed();
}

-(void)onShow:(NSNotification *)notifyObj
{
    Claw::Mixer::Get()->Pause(true);
}


-(void)getFullScreenAd:(NSNotification*)notifyObj
{
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* viewController = [appDelegate getViewController];
    [Tapjoy showFullScreenAdWithViewController:viewController];

    ClawExt::Tapjoy::QueryInterface()->NotifyFullScreenAdReceived();
}
@end

namespace ClawExt
{
    static IPhoneTapjoy* s_instance = NULL;

    /*static*/ Tapjoy* Tapjoy::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneTapjoy();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void Tapjoy::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    IPhoneTapjoy::IPhoneTapjoy()
    {}

    IPhoneTapjoy::~IPhoneTapjoy()
    {}

    bool IPhoneTapjoy::Initialize( const Claw::NarrowString& appId, const Claw::NarrowString& secretKey )
    {
        const char* c1 = appId.c_str();
        const char* c2 = secretKey.c_str();

        NSString* s1 = [NSString stringWithUTF8String:c1];
        NSString* s2 = [NSString stringWithUTF8String:c2];

        ::TapjoyObserver* observer = [[::TapjoyObserver alloc] init];
        [::Tapjoy requestTapjoyConnect:s1 secretKey:s2];
        [[NSNotificationCenter defaultCenter] addObserver:observer selector:@selector(getUpdatedPoints:) name:TJC_TAP_POINTS_RESPONSE_NOTIFICATION object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:observer selector:@selector(getFullScreenAd:) name:TJC_FULL_SCREEN_AD_RESPONSE_NOTIFICATION  object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:observer selector:@selector(onClose:) name:TJC_VIEW_CLOSED_NOTIFICATION object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:observer selector:@selector(onShow:) name:TJC_OFFERS_RESPONSE_NOTIFICATION object:nil];

        return true;
    }

    bool IPhoneTapjoy::ShowOffers() const
    {
        IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
        IPhoneViewController* viewController = [appDelegate getViewController];

        [::Tapjoy showOffersWithViewController: viewController];
        return true;
    }

    bool IPhoneTapjoy::SetBannerAdVisibility( bool visible ) const
    {
        return false;
    }

    bool IPhoneTapjoy::CheckPoints() const
    {
        [::Tapjoy getTapPoints];
        return true;
    }

    void IPhoneTapjoy::FlushCash( int points ) const
    {
        [::Tapjoy spendTapPoints:points];
    }

    bool IPhoneTapjoy::ShowFullScreenAd() const
    {
        [::Tapjoy getFullScreenAd];
        return true;
    }

    bool IPhoneTapjoy::ReportActionComplete( const Claw::NarrowString& actionId ) const
    {
        const char* c1 = actionId.c_str();
        
        NSString* s1 = [NSString stringWithUTF8String:c1];
        
        [::Tapjoy actionComplete:s1];
        return true;
    }

} // namespace ClawExt





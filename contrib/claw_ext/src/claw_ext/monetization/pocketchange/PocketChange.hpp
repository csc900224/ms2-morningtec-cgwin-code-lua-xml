//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/pocketchange/PocketChange.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__POCKETCHANGE_HPP__
#define __INCLIDED__POCKETCHANGE_HPP__

namespace ClawExt
{
    class PocketChange
    {
    public:
        //! Initialize SDK with given application id.
        virtual void            Initialize( const char* appId ) = 0;

        //! Check if any reward should be displayed. If yes, reward will be presented to the user.
        virtual bool            DisplayReward() = 0;

        //! Open PocketChange shop offers.
        virtual void            OpenShop() = 0;

        //! Check if item with given sku was purchased by the user.
        virtual bool            HasPurchasedProduct( const char* sku ) = 0;

        //! Returns the total quantity purchased for the product identified by sku. No way to reset this value. Must save value and compare.
        virtual int             GetQuantityPurchasedForProduct( const char* sku ) = 0;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate object implementation.
        */
        static PocketChange*    QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void             Release();

    }; // class PocketChange
} // namespace ClawExt

#endif // __INCLIDED__POCKETCHANGE_HPP__

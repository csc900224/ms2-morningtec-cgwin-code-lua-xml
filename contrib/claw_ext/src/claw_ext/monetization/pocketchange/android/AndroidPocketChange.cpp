//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/pocketchange/android/AndroidPocketChange.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/pocketchange/android/AndroidPocketChange.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/system/android/JniAttach.hpp"

namespace ClawExt
{
    static AndroidPocketChange* s_instance = NULL;

    /*static*/ PocketChange* PocketChange::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidPocketChange;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void PocketChange::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void AndroidPocketChange::Initialize( const char* appId )
    {
        CLAW_MSG( "AndroidPocketChange::Initialize(): " << appId );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        jstring appIdStr = Claw::JniAttach::GetStringFromChars( env, appId );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/pocketchange/PocketChange", "Initialize", "(Ljava/lang/String;)V", appIdStr );
        
        Claw::JniAttach::ReleaseString( env, appIdStr );
        Claw::JniAttach::Detach( attached );
    }

    bool AndroidPocketChange::DisplayReward()
    {
        CLAW_MSG( "AndroidPocketChange::DisplayReward()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        bool result = Claw::JniAttach::StaticBooleanMethodCall( env, "com/gamelion/pocketchange/PocketChange", "DisplayReward", "()Z" );
        Claw::JniAttach::Detach( attached );

        return result;
    }

    void AndroidPocketChange::OpenShop()
    {
        CLAW_MSG( "AndroidPocketChange::OpenShop()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/pocketchange/PocketChange", "OpenShop", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    bool AndroidPocketChange::HasPurchasedProduct( const char* sku )
    {
        CLAW_MSG( "AndroidPocketChange::HasPurchasedProduct(): " << sku );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        jstring skuStr = Claw::JniAttach::GetStringFromChars( env, sku );

        bool result = Claw::JniAttach::StaticBooleanMethodCall( env, "com/gamelion/pocketchange/PocketChange", "HasPurchasedProduct", "(Ljava/lang/String;)Z", skuStr );

        Claw::JniAttach::ReleaseString( env, skuStr );
        Claw::JniAttach::Detach( attached );

        return result;
    }

    int AndroidPocketChange::GetQuantityPurchasedForProduct( const char* sku )
    {
        CLAW_MSG( "AndroidPocketChange::GetQuantityPurchasedForProduct(): " << sku );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        jstring skuStr = Claw::JniAttach::GetStringFromChars( env, sku );

        int result = Claw::JniAttach::StaticIntMethodCall( env, "com/gamelion/pocketchange/PocketChange", "GetQuantityPurchasedForProduct", "(Ljava/lang/String;)I", skuStr );

        Claw::JniAttach::ReleaseString( env, skuStr );
        Claw::JniAttach::Detach( attached );

        return result;
    }

} // namespace ClawExt

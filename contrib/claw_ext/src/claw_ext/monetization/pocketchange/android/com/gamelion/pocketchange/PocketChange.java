package com.gamelion.pocketchange;

import android.util.Log;
import android.os.Looper;
import android.view.View;
import android.content.Intent;

import com.Claw.Android.ClawActivityCommon;

class PocketChange
{
    private static String TAG    = "PocketChange";
    private static boolean DEBUG = false;
    
    public static void Initialize( final String appId )
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if( DEBUG ) Log.i( TAG, "Initialize(): " + appId );
            com.pocketchange.android.PocketChange.initialize( ClawActivityCommon.mActivity, appId, DEBUG );
        }});
    }
    
    public static boolean DisplayReward()
    {
        if( DEBUG ) Log.i( TAG, "DisplayReward()" );
        Intent rewardIntent = com.pocketchange.android.PocketChange.getDisplayRewardIntent();
        if (rewardIntent != null) {
            ClawActivityCommon.mActivity.startActivity( rewardIntent );
        }
        return rewardIntent != null;
    }
    
    public static void OpenShop()
    {
        ClawActivityCommon.mActivity.runOnUiThread( new Runnable() { public void run() {
            if( DEBUG ) Log.i( TAG, "OpenShop()" );
            com.pocketchange.android.PocketChange.openShop();
        }});
    }
    
    public static boolean HasPurchasedProduct( String sku )
    {
        if( DEBUG ) Log.i( TAG, "HasPurchasedProduct(): " + sku );
        return com.pocketchange.android.PocketChange.hasPurchasedProduct( sku );
    }
    
    public static int GetQuantityPurchasedForProduct( String sku )
    {
        if( DEBUG ) Log.i( TAG, "GetQuantityPurchasedForProduct(): " + sku );
        return com.pocketchange.android.PocketChange.getQuantityPurchasedForProduct( sku );
    }
}
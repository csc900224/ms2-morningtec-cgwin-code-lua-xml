//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/pocketchange/android/AndroidPocketChange.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__ANDROIDPOCKETCHANGE_HPP__
#define __INCLIDED__ANDROIDPOCKETCHANGE_HPP__

#include "claw_ext/monetization/pocketchange/PocketChange.hpp"

namespace ClawExt
{
    class AndroidPocketChange : public PocketChange
    {
    public:
        virtual void            Initialize( const char* appId );
        virtual bool            DisplayReward();
        virtual void            OpenShop();
        virtual bool            HasPurchasedProduct( const char* sku );
        virtual int             GetQuantityPurchasedForProduct( const char* sku );

    }; // class AndroidPockateChange
} // namespace ClawExt

#endif // __INCLIDED__ANDROIDPOCKETCHANGE_HPP__

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/pocketchange/dummy/DummyPockateChange.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__DUMMYPOCKETCHANGE_HPP__
#define __INCLIDED__DUMMYPOCKETCHANGE_HPP__

#include "claw_ext/monetization/pocketchange/PocketChange.hpp"

namespace ClawExt
{
    class DummyPocketChange : public PocketChange
    {
    public:
        virtual void            Initialize( const char* appId );
        virtual bool            DisplayReward();
        virtual void            OpenShop();
        virtual bool            HasPurchasedProduct( const char* sku );
        virtual int             GetQuantityPurchasedForProduct( const char* sku );

    }; // class PocketChange
} // namespace ClawExt

#endif // __INCLIDED__DUMMYPOCKETCHANGE_HPP__

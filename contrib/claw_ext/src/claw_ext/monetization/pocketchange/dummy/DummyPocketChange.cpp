//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/pocketchange/dummy/DummyPocketChange.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/pocketchange/dummy/DummyPocketChange.hpp"

// External includes
#include "claw/base/Errors.hpp"

namespace ClawExt
{
    static DummyPocketChange* s_instance = NULL;

    /*static*/ PocketChange* PocketChange::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyPocketChange;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void PocketChange::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void DummyPocketChange::Initialize( const char* appId )
    {
        CLAW_MSG( "DummyPocketChange::Initialize(): " << appId );
    }

    bool DummyPocketChange::DisplayReward()
    {
        CLAW_MSG( "DummyPocketChange::DisplayReward()" );
        return false;
    }

    void DummyPocketChange::OpenShop()
    {
        CLAW_MSG( "DummyPocketChange::OpenShop()" );
    }

    bool DummyPocketChange::HasPurchasedProduct( const char* sku )
    {
        CLAW_MSG( "DummyPocketChange::HasPurchasedProduct(): " << sku );
        return false;
    }

    int DummyPocketChange::GetQuantityPurchasedForProduct( const char* sku )
    {
        CLAW_MSG( "DummyPocketChange::GetQuantityPurchasedForProduct(): " << sku );
        return 0;
    }

} // namespace ClawExt

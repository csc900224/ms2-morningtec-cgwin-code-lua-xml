//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/adcolony/dummy/IPhoneAdColony.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/adcolony/iphone/IPhoneAdColony.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/sound/mixer/Mixer.hpp"
#import <Foundation/Foundation.h>

@class MyAdColonyDelegate;

static ClawExt::IPhoneAdColony* s_instance = NULL;
static NSString* s_appId = NULL;
static NSMutableDictionary* s_zoneDict = NULL;
static MyAdColonyDelegate* s_delegate = NULL;

namespace ClawExt
{
    /*static*/ AdColony* AdColony::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneAdColony();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void AdColony::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }
}

// AdColony includes
#import "AdColonyPublic.h"

@interface MyAdColonyDelegate : NSObject<AdColonyDelegate, AdColonyTakeoverAdDelegate>
{
}
- ( NSString * ) adColonyApplicationID;
- ( NSDictionary * ) adColonyAdZoneNumberAssociation;
- ( void ) adColonyNoVideoFillInZone:( NSString * )zone;
- ( void ) adColonyVideoAdsReadyInZone:( NSString * )zone;
- ( void ) adColonyVideoAdsNotReadyInZone:( NSString * )zone;
- ( void ) adColonyVirtualCurrencyAwardedByZone:( NSString * )zone currencyName:( NSString * )name currencyAmount:( int )amount;
- ( void ) adColonyTakeoverBeganForZone:( NSString * )zone;
- ( void ) adColonyTakeoverEndedForZone:( NSString * )zone withVC:( BOOL )withVirtualCurrencyAward;
- ( void ) adColonyVideoAdNotServedForZone:( NSString * )zone;
@end


@implementation MyAdColonyDelegate
- ( NSString * ) adColonyApplicationID
{
    return s_appId;
}

- ( NSDictionary * ) adColonyAdZoneNumberAssociation
{
    return s_zoneDict;
}

- ( void ) adColonyNoVideoFillInZone:( NSString * )zone
{
    if( s_instance )
        s_instance->NotiffyVideoAdsNotReady([zone UTF8String]);
}

- ( void ) adColonyVideoAdsReadyInZone:( NSString * )zone
{
    if( s_instance )
        s_instance->NotiffyVideoAdsReady([zone UTF8String]);
}

- ( void ) adColonyVideoAdsNotReadyInZone:( NSString * )zone
{
    if( s_instance )
        s_instance->NotiffyVideoAdsNotReady([zone UTF8String]);
}

- ( void ) adColonyVirtualCurrencyAwardedByZone:( NSString * )zone currencyName:( NSString * )name currencyAmount:( int )amount
{
    if( s_instance )
        s_instance->NotiffyVirtualCurrencyAward([zone UTF8String], [name UTF8String], amount);
}

- ( void ) adColonyTakeoverBeganForZone:( NSString * )zone
{
    if( s_instance )
        s_instance->NotiffyVideoBegin([zone UTF8String]);
    Claw::Mixer::Get()->Pause(true);
}

- ( void ) adColonyTakeoverEndedForZone:( NSString * )zone withVC:( BOOL )withVirtualCurrencyAward
{
    if( s_instance )
        s_instance->NotiffyVideoEnd([zone UTF8String]);
    Claw::Mixer::Get()->Pause(false);
}

- ( void ) adColonyVideoAdNotServedForZone:( NSString * )zone
{
    if( s_instance )
        s_instance->NotiffyVideoError([zone UTF8String]);
    Claw::Mixer::Get()->Pause(false);
}
@end


namespace ClawExt
{
    IPhoneAdColony::IPhoneAdColony()
    {
        s_delegate = [[MyAdColonyDelegate alloc] init];
        s_zoneDict = [[NSMutableDictionary alloc] init];
    }

    IPhoneAdColony::~IPhoneAdColony()
    {
        [s_delegate release];
        [s_zoneDict release];
        if( s_appId ) [s_appId release];
    }

    void IPhoneAdColony::Initialize( const Claw::NarrowString& applicationId )
    {
        if( !s_appId )
        {
            s_appId = [[NSString stringWithUTF8String:applicationId.c_str()] retain];
            [AdColony initAdColonyWithDelegate:s_delegate];
        }
    }

    bool IPhoneAdColony::AddZoneDefinitionImpl( int zondeIdx, const Claw::NarrowString& zoneId )
    {
        NSNumber* key = [NSNumber numberWithInt:zondeIdx];

        if( s_zoneDict == nil || [[s_zoneDict allKeys] containsObject:key] )
            return false;

        [s_zoneDict setObject:[NSString stringWithUTF8String:zoneId.c_str()] forKey:key];
        return true;
    }

    bool IPhoneAdColony::PlayVideo( int zondeIdx, bool defaultPrePopup /*= false*/, bool defaultPostPopup /*= false*/ )
    {
        [AdColony playVideoAdForSlot:zondeIdx
                        withDelegate:s_delegate
                    withV4VCPrePopup:defaultPrePopup
                    andV4VCPostPopup:defaultPostPopup];
        return true;
    }

    bool IPhoneAdColony::IsRewardAvailable( int zoneIdx ) const
    {
        return [AdColony virtualCurrencyAwardAvailableForSlot:zoneIdx];
    }

    Claw::NarrowString IPhoneAdColony::GetCurrencyRewardName( int zondeIdx ) const
    {
        return [[AdColony getVirtualCurrencyNameForSlot:zondeIdx] UTF8String];
    }

    int IPhoneAdColony::GetCurrencyRewardAmount( int zondeIdx ) const
    {
        return [AdColony getVirtualCurrencyRewardAmountForSlot:zondeIdx];
    }

    int IPhoneAdColony::GetVideosPerReward( int zondeIdx ) const
    {
        return [AdColony getVideosPerReward:[NSString stringWithUTF8String:GetCurrencyRewardName(zondeIdx).c_str()]];
    }

    int IPhoneAdColony::GetVideosCreditBalance( int zondeIdx ) const
    {
        return [AdColony getVideoCreditBalance:[NSString stringWithUTF8String:GetCurrencyRewardName(zondeIdx).c_str()]];
    }
} //namespace ClawExt

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/adcolony/AdColony.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/adcolony/AdColony.hpp"

namespace ClawExt
{
    void AdColony::RegisterObserver( AdColonyObserver* callback )
    {
        m_observers.insert( callback );
    }

    void AdColony::UnregisterObserver( AdColonyObserver* callback )
    {
        m_observers.erase( callback );
    }

    int AdColony::GetVideosRemaining( int zondeIdx ) const
    {
        return GetVideosPerReward( zondeIdx ) - GetVideosCreditBalance( zondeIdx );
    }

    bool AdColony::AddZoneDefinition( int zondeIdx, const Claw::NarrowString& zoneId )
    {
        if( AddZoneDefinitionImpl( zondeIdx, zoneId ) )
        {
            m_reverseZoneMap.insert( StringToIntMap::value_type( zoneId, zondeIdx ) );
            return true;
        }
        return false;
    }

    bool AdColony::AreVideoAdsAvailable( int zoneIdx ) const
    {
        IntToBoolMap::const_iterator it = m_videAdsReady.find( zoneIdx );
        if( it != m_videAdsReady.end() )
            return it->second;
        return false;
    }

    void AdColony::NotiffyVirtualCurrencyAward( const Claw::NarrowString& zondeId, const Claw::NarrowString& currencyName, int amount )
    {
        std::set<AdColonyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnVirtualCurrencyAward( zondeId, currencyName, amount );
        }
    }

    void AdColony::NotiffyVideoBegin( const Claw::NarrowString& zondeId )
    {
        std::set<AdColonyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnVideoBegin( zondeId );
        }
    }

    void AdColony::NotiffyVideoEnd( const Claw::NarrowString& zondeId )
    {
        std::set<AdColonyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnVideoEnd( zondeId );
        }
    }

    void AdColony::NotiffyVideoError( const Claw::NarrowString& zondeId )
    {
        std::set<AdColonyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnVideoError( zondeId );
        }
    }

    void AdColony::NotiffyVideoAdsReady( const Claw::NarrowString& zondeId )
    {
        m_videAdsReady[m_reverseZoneMap[zondeId]] = true;

        std::set<AdColonyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnVideoAdsReady( zondeId );
        }
    }

    void AdColony::NotiffyVideoAdsNotReady( const Claw::NarrowString& zondeId )
    {
        m_videAdsReady[m_reverseZoneMap[zondeId]] = false;

        std::set<AdColonyObserver*>::const_iterator it, end;
        it = m_observers.begin();
        end = m_observers.end();

        for( ; it != end; ++it )
        {
            (*it)->OnVideoAdsNotReady( zondeId );
        }
    }

} // namespace ClawExt
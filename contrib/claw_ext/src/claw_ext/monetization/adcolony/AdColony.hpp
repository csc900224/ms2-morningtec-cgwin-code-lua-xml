//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/adcolony/AdColony.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__ADCOLONY_HPP__
#define __INCLUDED__ADCOLONY_HPP__

#include "claw/base/String.hpp"
#include <set>
#include <map>

namespace ClawExt
{
    //! Wrapper class for AdColony video ads provider.
    class AdColony
    {
    public:
        //! Observe for AdColony state changes.
        class AdColonyObserver
        {
        public:
            //! Called when you should give an award to the player.
            virtual void OnVirtualCurrencyAward( const Claw::NarrowString& zondeId, const Claw::NarrowString& currencyName, int amount ) = 0;

            //! Vieo playback was started.
            virtual void OnVideoBegin( const Claw::NarrowString& zondeId ) = 0;

            //! Video playback was finished.
            virtual void OnVideoEnd( const Claw::NarrowString& zondeId ) = 0;

            //! Video could not be played.
            virtual void OnVideoError( const Claw::NarrowString& zondeId ) = 0;

            //! Video ads become ready for playback.
            virtual void OnVideoAdsReady( const Claw::NarrowString& zondeId ) = 0;

            //! Video ads are not available any more.
            virtual void OnVideoAdsNotReady( const Claw::NarrowString& zondeId ) = 0;
        };

        //! Initialize video ads using application id.
        virtual void        Initialize( const Claw::NarrowString& applicationId ) = 0;

        //! Define video playback zone. Use arbitrary int value as index for unique zone id;
        virtual bool        AddZoneDefinition( int zondeIdx, const Claw::NarrowString& zoneId );

        //! Play vide ad for given zonde idx. Decide if default pre and post popus should be used.
        virtual bool        PlayVideo( int zondeIdx, bool defaultPrePopup = false, bool defaultPostPopup = false ) = 0;

        //! Check if reward is available for the user (if he reached daily reward cap or not).
        virtual bool        IsRewardAvailable( int zoneIdx ) const = 0;

        //! Check if video ads are ready to display in given zone
        virtual bool        AreVideoAdsAvailable( int zoneIdx ) const;

        //! Get name of the currency reward available int given zone.
        virtual 
        Claw::NarrowString  GetCurrencyRewardName( int zondeIdx ) const = 0;

        //! Get amount of the currency reward available int given zone.
        virtual int         GetCurrencyRewardAmount( int zondeIdx ) const = 0;

        //! Check how many video should be watched in given zone to get a reward.
        virtual int         GetVideosPerReward( int zondeIdx ) const = 0;

        //! Check how many videos were watched in given zone since last reward.
        virtual int         GetVideosCreditBalance( int zondeIdx ) const = 0;

        //! Check how many videos still need to be watched to get a reward.
        virtual int         GetVideosRemaining( int zondeIdx ) const;


        //! Register observer to watch AdColony state changes.
        void                RegisterObserver( AdColonyObserver* callback );

        //! Unregister observer.
        void                UnregisterObserver( AdColonyObserver* callback );

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate AdColony object implementation.
        */
        static AdColony*    QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void         Release();

    public:
        void                NotiffyVirtualCurrencyAward( const Claw::NarrowString& zondeId, const Claw::NarrowString& currencyName, int amount );
        void                NotiffyVideoBegin( const Claw::NarrowString& zondeId );
        void                NotiffyVideoEnd( const Claw::NarrowString& zondeId );
        void                NotiffyVideoError( const Claw::NarrowString& zondeId );
        void                NotiffyVideoAdsReady( const Claw::NarrowString& zondeId );
        void                NotiffyVideoAdsNotReady( const Claw::NarrowString& zondeId );

    protected:
        virtual bool        AddZoneDefinitionImpl( int zondeIdx, const Claw::NarrowString& zoneId ) = 0;
        
    private:
        typedef std::map<Claw::NarrowString, int> StringToIntMap;
        typedef std::map<int, bool> IntToBoolMap;

        std::set<AdColonyObserver*> m_observers;
        StringToIntMap              m_reverseZoneMap;
        IntToBoolMap                m_videAdsReady;

    }; // class AdColony
} // namespace ClawExt

#endif // __INCLUDED__ADCOLONY_HPP__

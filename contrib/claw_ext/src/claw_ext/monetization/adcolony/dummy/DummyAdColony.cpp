//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/adcolony/dummy/DummyAdColony.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/monetization/adcolony/dummy/DummyAdColony.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include <sstream>

namespace ClawExt
{
    static DummyAdColony* s_instance = NULL;

    /*static*/ AdColony* AdColony::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyAdColony;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void AdColony::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void DummyAdColony::Initialize( const Claw::NarrowString& applicationId )
    {}

    bool DummyAdColony::AddZoneDefinitionImpl( int zondeIdx, const Claw::NarrowString& zoneId )
    {
        return true;
    }

    bool DummyAdColony::PlayVideo( int zondeIdx, bool defaultPrePopup /*= false*/, bool defaultPostPopup /*= false*/ )
    {
        return false;
    }

    bool DummyAdColony::IsRewardAvailable( int zoneIdx ) const
    {
        return false;
    }

    Claw::NarrowString DummyAdColony::GetCurrencyRewardName( int zondeIdx ) const
    {
        std::ostringstream ss;
        ss << "DummyCurrency:" << zondeIdx;
        return ss.str();
    }

    int DummyAdColony::GetCurrencyRewardAmount( int zondeIdx ) const
    {
        return zondeIdx;
    }

    int DummyAdColony::GetVideosPerReward( int zondeIdx ) const
    {
        return zondeIdx;
    }

    int DummyAdColony::GetVideosCreditBalance( int zondeIdx ) const
    {
        return 0;
    }
} //namespace ClawExt

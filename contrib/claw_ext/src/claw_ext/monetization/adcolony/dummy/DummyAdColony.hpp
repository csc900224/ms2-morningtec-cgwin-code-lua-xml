//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/adcolony/dummy/DummyAdColony.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__DUMMYADCOLONY_HPP__
#define __INCLUDED__DUMMYADCOLONY_HPP__

// Internal includes
#include "claw_ext/monetization/adcolony/AdColony.hpp"

namespace ClawExt
{
    class DummyAdColony : public AdColony
    {
        void                Initialize( const Claw::NarrowString& applicationId );
        bool                AddZoneDefinitionImpl( int zondeIdx, const Claw::NarrowString& zoneId );
        bool                PlayVideo( int zondeIdx, bool defaultPrePopup = false, bool defaultPostPopup = false );
        bool                IsRewardAvailable( int zoneIdx ) const;
        Claw::NarrowString  GetCurrencyRewardName( int zondeIdx ) const;
        int                 GetCurrencyRewardAmount( int zondeIdx ) const;
        int                 GetVideosPerReward( int zondeIdx ) const;
        int                 GetVideosCreditBalance( int zondeIdx ) const;
    }; // class DummyAdColony
} // namespace ClawExt

#endif // __INCLUDED__DUMMYADCOLONY_HPP__
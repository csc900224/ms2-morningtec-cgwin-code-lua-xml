//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/appsflyer/android/AndroidAppsFlyer.mm
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw/system/android/JniAttach.hpp"
#include "claw/base/Errors.hpp"

#include "claw_ext/monetization/appsflyer/android/AndroidAppsFlyer.hpp"

namespace ClawExt
{
    static AndroidAppsFlyer* s_instance = NULL;

    /*static*/ AppsFlyer* AppsFlyer::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidAppsFlyer;

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void AppsFlyer::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    void AndroidAppsFlyer::Initialize( const char* appIdDevKeyString, const char* isoCurrencyCode /*= NULL*/ )
    {
        CLAW_MSG( "AndroidAppsFlyer::Initialize(): " << appIdDevKeyString << " currency: " << isoCurrencyCode );
        CLAW_ASSERT( appIdDevKeyString );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jappIdDevKeyString = NULL;
        jstring jisoCurrencyCode = NULL;
        if( isoCurrencyCode )
        {
            jisoCurrencyCode = Claw::JniAttach::GetStringFromChars( env, isoCurrencyCode );
        }
        jappIdDevKeyString = Claw::JniAttach::GetStringFromChars( env, appIdDevKeyString );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/appsflyer/AppsFlyer", "initialize", "(Ljava/lang/String;Ljava/lang/String;)V", jappIdDevKeyString, jisoCurrencyCode );

        Claw::JniAttach::ReleaseString( env, jappIdDevKeyString );
        Claw::JniAttach::ReleaseString( env, jisoCurrencyCode );

        Claw::JniAttach::Detach( attached );
    }

    void AndroidAppsFlyer::ReportInstall()
    {
        CLAW_MSG( "AndroidAppsFlyer::ReportInstall()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/appsflyer/AppsFlyer", "reportInstall", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidAppsFlyer::ReportConversionEvent( const char* eventName, const char* eventValue /*= ""*/ )
    {
        CLAW_MSG( "AndroidAppsFlyer::ReportConversionEvent(): " << eventName << " eventValue: " << eventValue  );
        CLAW_ASSERT( eventName );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jeventName = NULL;
        jstring jeventValue = NULL;
        if( eventValue )
        {
            jeventValue = Claw::JniAttach::GetStringFromChars( env, eventValue );
        }
        jeventName = Claw::JniAttach::GetStringFromChars( env, eventName );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/appsflyer/AppsFlyer", "reportConversionEvent", "(Ljava/lang/String;Ljava/lang/String;)V", jeventName, jeventValue );

        Claw::JniAttach::ReleaseString( env, jeventName );
        Claw::JniAttach::ReleaseString( env, jeventValue );

        Claw::JniAttach::Detach( attached );
    }
} // namespace ClawExt

package com.gamelion.appsflyer;

import android.util.Log;
import android.os.Looper;
import android.view.View;

import com.appsflyer.AppsFlyerLib;

import com.Claw.Android.ClawActivityCommon;

public class AppsFlyer
{
    private static final String TAG = "AppsFlyer";
    private static final boolean DEBUG = false;

    public static void initialize(String appIdDevKeyString, String isoCurrencyCode)
    {
        if (DEBUG) Log.i( TAG, "initialize: " + appIdDevKeyString + "; " + isoCurrencyCode );
        AppsFlyerLib.setAppsFlyerKey(appIdDevKeyString);
        if (!"".equals(isoCurrencyCode)) {
            AppsFlyerLib.setCurrencyCode(isoCurrencyCode);
        }
    }
    
    public static void reportInstall()
    {
        if (DEBUG) Log.i( TAG, "reportInstall" );
        AppsFlyerLib.sendTracking(ClawActivityCommon.mActivity);
    }
    
    public static void reportConversionEvent(String eventName, String eventValue)
    {
        if (DEBUG) Log.i( TAG, "reportConversionEvent: " + eventName + "; " + eventValue );
        AppsFlyerLib.sendTrackingWithEvent(ClawActivityCommon.mActivity, eventName, eventValue);
    }
}

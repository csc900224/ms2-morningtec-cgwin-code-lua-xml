//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/appsflyer/dummy/DummyAppsFlyer.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/appsflyer/dummy/DummyAppsFlyer.hpp"

namespace ClawExt
{
    static DummyAppsFlyer* s_instance = NULL;
    
    /*static*/ AppsFlyer* AppsFlyer::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyAppsFlyer;
        
        CLAW_ASSERT( s_instance );
        return s_instance;
    }
    
    /*static*/ void AppsFlyer::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }
    
    void DummyAppsFlyer::Initialize( const char* appIdDevKeyString, const char* isoCurrencyCode /*= NULL*/ )
    {
        CLAW_MSG( "DummyAppsFlyer::Initialize(): " << appIdDevKeyString << " currency: " << isoCurrencyCode );
    }
    
    void DummyAppsFlyer::ReportInstall()
    {
        CLAW_MSG( "DummyAppsFlyer::ReportInstall()" );
    }
    
    void DummyAppsFlyer::ReportConversionEvent( const char* eventName, const char* eventValue /*= ""*/ )
    {
        CLAW_MSG( "DummyAppsFlyer::ReportConversionEvent(): " << eventName << " eventValue: " << eventValue  );
    }
} // namespace ClawExt

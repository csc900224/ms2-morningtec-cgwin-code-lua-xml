//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/appsflyer/dummy/DummyAppsFlyer.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__DUMMYAPPSFLYER_HPP__
#define __INCLUDED__DUMMYAPPSFLYER_HPP__

#include "claw_ext/monetization/appsflyer/AppsFlyer.hpp"

namespace ClawExt
{
    class DummyAppsFlyer : public AppsFlyer
    {
    public:
        void Initialize( const char* appIdDevKeyString, const char* isoCurrencyCode = NULL );
        void ReportInstall();
        void ReportConversionEvent( const char* eventName, const char* eventValue = "" );

    }; // class DummyAppsFlyer
} // namespace ClawExt

#endif // __INCLUDED__DUMMYAPPSFLYER_HPP__

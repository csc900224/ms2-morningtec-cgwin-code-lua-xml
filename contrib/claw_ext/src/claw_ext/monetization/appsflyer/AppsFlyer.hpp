//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/appsflyer/AppsFlyer.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__APPSFLYER_HPP__
#define __INCLUDED__APPSFLYER_HPP__

#include "claw/base/Errors.hpp"

namespace ClawExt
{
    class AppsFlyer
    {
    public:
        //! Initilize SDK with App_ID;Dev_Key string and option currency code (USD is default).
        virtual void Initialize( const char* appIdDevKeyString, const char* isoCurrencyCode = NULL ) = 0;
        
        //! Notify application install.
        virtual void ReportInstall() = 0;
        
        //! Report conversion/business evnet (eg. IAP purchase).
        virtual void ReportConversionEvent( const char* eventName, const char* eventValue = "" ) = 0;


        //! Retreive platfrom dependend implementation.
        /**
         * This method should be implemented and linked once in platform dependend object,
         * returning appropriate AppsFlyer object implementation.
         */
        static AppsFlyer*   QueryInterface();
        
        //! Release platform specific implementation.
        /**
         * Call destructors, release memory, make cleanup.
         */
        static void         Release();

    }; // class AppsFlyer
} // namespace ClawExt

#endif // __INCLUDED__APPSFLYER_HPP__

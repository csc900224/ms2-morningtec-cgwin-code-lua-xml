//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/appsflyer/dummy/IPhoneAppsFlyer.mm
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw_ext/monetization/appsflyer/iphone/IPhoneAppsFlyer.hpp"

#import <Foundation/Foundation.h>
#import "AppsFlyer.h"

namespace ClawExt
{
    static IPhoneAppsFlyer* s_instance = NULL;
    
    /*static*/ AppsFlyer* AppsFlyer::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneAppsFlyer;
        
        CLAW_ASSERT( s_instance );
        return s_instance;
    }
    
    /*static*/ void AppsFlyer::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }
        
    void IPhoneAppsFlyer::Initialize( const char* appIdDevKeyString, const char* isoCurrencyCode /*= NULL*/ )
    {
        CLAW_MSG( "IPhoneAppsFlyer::Initialize(): " << appIdDevKeyString << " currency: " << isoCurrencyCode );
        
        m_appId = appIdDevKeyString;
        
        if( isoCurrencyCode )
        {
            [::AppsFlyer setCurrencyCode:[NSString stringWithUTF8String:isoCurrencyCode]];
        }
    }
    
    void IPhoneAppsFlyer::ReportInstall()
    {
        CLAW_MSG( "IPhoneAppsFlyer::ReportInstall()" );
        [::AppsFlyer notifyAppID:[NSString stringWithUTF8String:m_appId.c_str()]];
    }
    
    void IPhoneAppsFlyer::ReportConversionEvent( const char* eventName, const char* eventValue /*= ""*/ )
    {
        CLAW_MSG( "IPhoneAppsFlyer::ReportConversionEvent(): " << eventName << " eventValue: " << eventValue  );
        [::AppsFlyer notifyAppID:[NSString stringWithUTF8String:m_appId.c_str()]
                           event:[NSString stringWithUTF8String:eventName]
                      eventValue:[NSString stringWithUTF8String:eventValue]];
    }
} // namespace ClawExt

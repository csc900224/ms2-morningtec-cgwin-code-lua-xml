//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/appsflyer/iphone/IPhoneAppsFlyer.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__IPHONEAPPSFLYER_HPP__
#define __INCLUDED__IPHONEAPPSFLYER_HPP__

#include "claw_ext/monetization/appsflyer/AppsFlyer.hpp"
#include "claw/base/String.hpp"

namespace ClawExt
{
    class IPhoneAppsFlyer : public AppsFlyer
    {
    public:
        void Initialize( const char* appIdDevKeyString, const char* isoCurrencyCode = NULL );
        void ReportInstall();
        void ReportConversionEvent( const char* eventName, const char* eventValue = "" );

    private:
        Claw::NarrowString m_appId;

    }; // class IPhoneAppsFlyer
} // namespace ClawExt

#endif // __INCLUDED__IPHONEAPPSFLYER_HPP__

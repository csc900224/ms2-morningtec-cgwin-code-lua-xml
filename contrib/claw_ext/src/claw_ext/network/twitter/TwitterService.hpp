#ifndef __INCLUDED__TWITTERSERVICE_HPP__
#define __INCLUDED__TWITTERSERVICE_HPP__

#include "claw/base/RefCounter.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/compat/Platform.h"

class TwitterServiceListener;

class TwitterServiceBase : public Claw::RefCounter
{
public:
    virtual bool CanSendTweet() const = 0;
    virtual void SendTweet( const Claw::NarrowString& message ) = 0;
    virtual void SetListener( TwitterServiceListener* listener ) = 0;
};

class TwitterServiceListener
{
public:
    virtual void OnTweetSent( bool success ) {}
};

#if defined CLAW_IPHONE
#include "claw_ext/network/twitter/iphone/IPhoneTwitterService.hpp"
typedef IPhoneTwitterService TwitterService;
#elif defined CLAW_ANDROID
class AndroidTwitterService;
#include "claw_ext/network/twitter/android/AndroidTwitterService.hpp"
typedef AndroidTwitterService TwitterService;
#else
#include "claw_ext/network/twitter/dummy/DummyTwitterService.hpp"
typedef DummyTwitterService TwitterService;
#endif

typedef Claw::SmartPtr<TwitterService> TwitterServicePtr;

#endif // __INCLUDED__TWITTERSERVICE_HPP__

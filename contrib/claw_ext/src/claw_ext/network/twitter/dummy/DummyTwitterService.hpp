#ifndef __INCLUDED__TWITTERSMSSERVICE_HPP__
#define __INCLUDED__TWITTERSMSSERVICE_HPP__

#include "claw_ext/network/twitter/TwitterService.hpp"

class DummyTwitterService : public TwitterServiceBase
{
public:
    DummyTwitterService();

    bool CanSendTweet() const;
    void SendTweet( const Claw::NarrowString& message );
    void SetListener( TwitterServiceListener* listener );

private:
    TwitterServiceListener* m_listener;

}; // class DummyTwitterService

#endif // __INCLUDED__TWITTERSMSSERVICE_HPP__

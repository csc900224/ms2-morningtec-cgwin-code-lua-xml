#include "claw_ext/network/twitter/TwitterService.hpp"
#include "claw_ext/network/twitter/dummy/DummyTwitterService.hpp"

DummyTwitterService::DummyTwitterService()
    : m_listener( NULL )
{}

bool DummyTwitterService::CanSendTweet() const
{
    return true;
}

void DummyTwitterService::SendTweet( const Claw::NarrowString& message )
{
    if ( m_listener )
    {
        m_listener->OnTweetSent( true );
    }
}

void DummyTwitterService::SetListener( TwitterServiceListener* listener )
{
    m_listener = listener;
}

#ifndef __INCLUDED__IPHONETWITTERSERVICE_HPP__
#define __INCLUDED__IPHONETWITTERSERVICE_HPP__

#include "claw_ext/network/twitter/TwitterService.hpp"

class IPhoneTwitterService : public TwitterServiceBase
{
public:
    IPhoneTwitterService();

    bool CanSendTweet() const;
    void SendTweet( const Claw::NarrowString& message );
    void SetListener( TwitterServiceListener* listener );

private:
    TwitterServiceListener* m_listener;

}; // class IPhoneTwitterService

#endif // __INCLUDED__IPHONETWITTERSERVICE_HPP__

#include "claw_ext/network/twitter/TwitterService.hpp"
#include "claw_ext/network/twitter/iphone/IphoneTwitterService.hpp"

#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>

#import "claw/application/iphone/IPhoneAppDelegate.h"

IPhoneTwitterService::IPhoneTwitterService()
    : m_listener( NULL )
{
}

bool IPhoneTwitterService::CanSendTweet() const
{
    return [TWTweetComposeViewController canSendTweet] == YES;
}

void IPhoneTwitterService::SendTweet( const Claw::NarrowString& message )
{
    CLAW_ASSERT( CanSendTweet() );

    TWTweetComposeViewController *tweetViewController = [[[TWTweetComposeViewController alloc] init] autorelease];
    [tweetViewController setInitialText:[NSString stringWithCString:message.c_str() encoding:NSUTF8StringEncoding]];

    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* rootViewController = [appDelegate getViewController];

    [tweetViewController setCompletionHandler:^(TWTweetComposeViewControllerResult result)
     {
        if ( m_listener )
        {
            m_listener->OnTweetSent( result == TWTweetComposeViewControllerResultDone );
        }

        [rootViewController dismissModalViewControllerAnimated:YES];
     }];

    [rootViewController presentModalViewController:tweetViewController animated:YES];
}

void IPhoneTwitterService::SetListener( TwitterServiceListener* listener )
{
    m_listener = listener;
}

package com.gamelion.twitter;

import com.Claw.Android.ClawActivityCommon;

import android.util.Log;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.ActivityInfo;
import android.content.ComponentName;

import com.gamelion.twitter.Consts;
import com.gamelion.twitter.TwitterActivity;

public class TwitterService
{     
    public static boolean CanSendTweet()
    {
        if (Consts.DEBUG) { Log.i( Consts.TAG, "TwitterService.CanSendTweet()" ); }
        boolean result = false;
        try{
            PackageInfo info = ClawActivityCommon.mActivity.getPackageManager().getPackageInfo( Consts.TWITTER_PACKAGE, PackageManager.GET_ACTIVITIES  );
            for( ActivityInfo activityInfo : info.activities ){
                if (Consts.DEBUG) { Log.i( Consts.TAG, "package: " + Consts.TWITTER_PACKAGE + " activity found: " + activityInfo.name ); }
                if( activityInfo.name.equals(Consts.TWITTER_ACTIVITY) ){
                    result = true;
                    break;
                }
            }
        } catch( PackageManager.NameNotFoundException e ){}
        
        if (Consts.DEBUG) { Log.i( Consts.TAG, "TwitterService.CanSendTweet(): " + result ); }
        return result;
    }
    
    public static void SendTweet( String message )
    {       
        if (Consts.DEBUG) { Log.i( Consts.TAG, "TwitterService.SendTweet(): " + message ); }
        Intent sendIntent = new Intent( Intent.ACTION_VIEW );
        sendIntent.setClass( ClawActivityCommon.mActivity, TwitterActivity.class );
        sendIntent.putExtra( Intent.EXTRA_TEXT, message );
        ClawActivityCommon.mActivity.startActivity( sendIntent );
    }    
    
    public static native void TweetSent(boolean success);
}

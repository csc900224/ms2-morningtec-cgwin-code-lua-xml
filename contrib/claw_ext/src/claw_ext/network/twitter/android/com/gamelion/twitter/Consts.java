package com.gamelion.twitter;

/**
 * This class holds global constants that are used throughout the application
 * to support twitter
 */
public class Consts
{
    public static final boolean DEBUG                   = false;
    public final static String  TAG                     = "TwitterService";
    public final static String  TWITTER_PACKAGE         = "com.twitter.android";
    public final static String  TWITTER_ACTIVITY        = "com.twitter.android.PostActivity";
    public final static int     TWITTER_REQUEST_CODE    = 80085;
}

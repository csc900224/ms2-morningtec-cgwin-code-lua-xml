package com.gamelion.twitter;

import com.Claw.Android.ClawActivityCommon;

import android.util.Log;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.ActivityInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.net.Uri;
import android.view.Window;
import android.view.WindowManager;

import com.gamelion.twitter.Consts;
import com.gamelion.twitter.TwitterService;

public class TwitterActivity extends Activity
{   
    @Override  
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        
        setTheme( android.R.style.Theme_Translucent );
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    
        if (Consts.DEBUG) { Log.i( "TwitterActivity", "onCreate()" ); }
        
        Intent sendIntent = new Intent( Intent.ACTION_VIEW );
        sendIntent.setClassName( Consts.TWITTER_PACKAGE, Consts.TWITTER_ACTIVITY );
        sendIntent.putExtra( Intent.EXTRA_TEXT, getIntent().getExtras().getString( Intent.EXTRA_TEXT ) );
        sendIntent.setType( "text/plain" );
        
        startActivityForResult( sendIntent, Consts.TWITTER_REQUEST_CODE );
        
        if (Consts.DEBUG) { Log.i( "TwitterActivity", "onCreate() - intent send" ); }      
    }
    
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if (Consts.DEBUG) { Log.i( "TwitterActivity", "requestCode: " + requestCode + " resultCode: " + resultCode ); }
        
        if (requestCode == Consts.TWITTER_REQUEST_CODE)
        {
            TwitterService.TweetSent( resultCode == RESULT_OK );
            
            setResult(RESULT_OK, new Intent());
            finish();
        }
    }
}

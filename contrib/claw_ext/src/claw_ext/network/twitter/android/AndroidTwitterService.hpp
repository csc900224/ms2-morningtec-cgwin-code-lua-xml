#ifndef __INCLUDED__ANDROIDTWITTERSERVICE_HPP__
#define __INCLUDED__ANDROIDTWITTERSERVICE_HPP__

#include "claw_ext/network/twitter/TwitterService.hpp"

class AndroidTwitterService : public TwitterServiceBase
{
public:
    AndroidTwitterService();

    bool CanSendTweet() const;
    void SendTweet( const Claw::NarrowString& message );
    void SetListener( TwitterServiceListener* listener );

private:
    TwitterServiceListener* m_listener;

}; // class AndroidTwitterService

#endif // __INCLUDED__ANDROIDTWITTERSERVICE_HPP__

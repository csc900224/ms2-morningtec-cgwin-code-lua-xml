#include "claw_ext/network/twitter/TwitterService.hpp"
#include "claw_ext/network/twitter/android/AndroidTwitterService.hpp"

// External include
#include "claw/system/android/JniAttach.hpp"

static TwitterServiceListener* s_listener = NULL;

AndroidTwitterService::AndroidTwitterService()
    : m_listener( NULL )
{}

bool AndroidTwitterService::CanSendTweet() const
{
    CLAW_MSG( "AndroidTwitterService::CanSendTweet()" );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );
    bool canSend = Claw::JniAttach::StaticBooleanMethodCall( env, "com/gamelion/twitter/TwitterService", "CanSendTweet", "()Z" );
    Claw::JniAttach::Detach( attached );
    return canSend;
}

void AndroidTwitterService::SendTweet( const Claw::NarrowString& message )
{
    // Make listener visible to JNIEXPORT
    s_listener = m_listener;

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );
    jstring messageString = Claw::JniAttach::GetStringFromChars( env, message.c_str() );

    Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/twitter/TwitterService", "SendTweet", "(Ljava/lang/String;)V", messageString );
    
    Claw::JniAttach::ReleaseString( env, messageString );
    Claw::JniAttach::Detach( attached );
}

void AndroidTwitterService::SetListener( TwitterServiceListener* listener )
{
    m_listener = listener;
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_twitter_TwitterService_TweetSent( JNIEnv* env, jclass clazz, jboolean success )
    {
        CLAW_MSG( "Native TweetSent()" );
        if( s_listener )
        {
            s_listener->OnTweetSent( success );
            s_listener = NULL;
        }
    }
}
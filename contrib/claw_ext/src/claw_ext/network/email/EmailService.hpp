#ifndef __INCLUDED__EMAILSERVICE_HPP__
#define __INCLUDED__EMAILSERVICE_HPP__

#include "claw/base/RefCounter.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/compat/Platform.h"

class EmailServiceListener;

class EmailServiceBase : public Claw::RefCounter
{
public:
    virtual bool CanSendEmail() const = 0;
    virtual void SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html ) = 0;
    virtual void SetListener( EmailServiceListener* listener ) = 0;
};

class EmailServiceListener
{
public:
    virtual void OnEmailSent( bool success ) {}
};

#if defined CLAW_IPHONE
class IPhoneEmailService;
#include "claw_ext/network/email/iphone/IPhoneEmailService.hpp"
typedef IPhoneEmailService EmailService;
#elif defined CLAW_ANDROID
class AndroidEmailService;
#include "claw_ext/network/email/android/AndroidEmailService.hpp"
typedef AndroidEmailService EmailService;
#else
class DummyEmailService;
#include "claw_ext/network/email/dummy/DummyEmailService.hpp"
typedef DummyEmailService EmailService;
#endif

typedef Claw::SmartPtr<EmailService> EmailServicePtr;

#endif // __INCLUDED__EMAILSERVICE_HPP__

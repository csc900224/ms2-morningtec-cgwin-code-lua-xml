//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/email/android/AndroidEmailService.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__ANDROIDEMAILSERVICE_HPP__
#define __INCLUDED__ANDROIDEMAILSERVICE_HPP__

#include "claw_ext/network/email/EmailService.hpp"

class AndroidEmailService : public EmailServiceBase
{
public:
            AndroidEmailService();

    bool    CanSendEmail() const;
    void    SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html );
    void    SetListener( EmailServiceListener* listener );

private:
    EmailServiceListener* m_listener;

}; // class AndroidEmailService

#endif // __INCLUDED__ANDROIDEMAILSERVICE_HPP__

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/email/android/AndroidEmailService.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/email/android/AndroidEmailService.hpp"

// External include
#include "claw/system/android/JniAttach.hpp"

static EmailServiceListener* s_listener = NULL;

AndroidEmailService::AndroidEmailService()
    : m_listener( NULL )
{}

bool AndroidEmailService::CanSendEmail() const
{
    CLAW_MSG( "AndroidEmailService::CanSendEmail()" );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );
    bool canSend = Claw::JniAttach::StaticBooleanMethodCall( env, "com/gamelion/email/EmailService", "CanSendEmail", "()Z" );
    Claw::JniAttach::Detach( attached );
    return canSend;
}

void AndroidEmailService::SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html )
{
    // Make listener visible to JNIEXPORT
    s_listener = m_listener;

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );
    jstring messageString = Claw::JniAttach::GetStringFromChars( env, message.c_str() );
    jstring subjectString = Claw::JniAttach::GetStringFromChars( env, subject.c_str() );

    Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/email/EmailService", "SendEmail", "(Ljava/lang/String;Ljava/lang/String;Z)V", subjectString, messageString, html );
    
    Claw::JniAttach::ReleaseString( env, messageString );
    Claw::JniAttach::ReleaseString( env, subjectString );
    Claw::JniAttach::Detach( attached );
}

void AndroidEmailService::SetListener( EmailServiceListener* listener )
{
    m_listener = listener;
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_email_EmailService_EmailSent( JNIEnv* env, jclass clazz, jboolean success )
    {
        CLAW_MSG( "Native EmailSent()" );
        if( s_listener )
        {
            s_listener->OnEmailSent( success );
            s_listener = NULL;
        }
    }
}
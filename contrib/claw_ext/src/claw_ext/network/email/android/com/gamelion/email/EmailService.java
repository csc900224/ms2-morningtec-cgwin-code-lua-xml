package com.gamelion.email;

import android.util.Log;
import com.Claw.Android.ClawActivityCommon;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.Html;

import com.gamelion.sms.Consts;

public class EmailService
{
    public static boolean CanSendEmail()
    {
        return true;
    }

    public static void SendEmail( String subject, String message, boolean html )
    {
        try {
            if (Consts.DEBUG) { Log.i( Consts.TAG, "EmailService.SendEmail(): " + message ); }
            Intent sendIntent = new Intent( Intent.ACTION_SENDTO );
            sendIntent.setType( html ? "text/html" : "text/plain" );
            sendIntent.setData( Uri.parse("mailto:") );
            sendIntent.putExtra( Intent.EXTRA_SUBJECT, subject );
            sendIntent.putExtra( Intent.EXTRA_TEXT, html ? Html.fromHtml(message) : message );

            ClawActivityCommon.mActivity.startActivity( sendIntent );

            // No way to reliably chceck if emaik was sent
            EmailSent( true );
        } catch( Exception ex ) {
            EmailSent( false );
        }
    }

    public static native void EmailSent(boolean success);
}

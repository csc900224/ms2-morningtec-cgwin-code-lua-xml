package com.gamelion.email;

/**
 * This class holds global constants that are used throughout the application
 * to support chartboost
 */
public class Consts
{
    public static final boolean DEBUG   = true;
    public final static String TAG      = "EmailService";
}

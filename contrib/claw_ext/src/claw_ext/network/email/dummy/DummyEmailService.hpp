#ifndef __INCLUDED__DUMMYEMAILSERVICE_HPP__
#define __INCLUDED__DUMMYEMAILSERVICE_HPP__

#include "claw_ext/network/email/EmailService.hpp"

class DummyEmailService : public EmailServiceBase
{
public:
    DummyEmailService();

    bool CanSendEmail() const;
    void SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html );
    void SetListener( EmailServiceListener* listener );

private:
    EmailServiceListener* m_listener;

}; // class DummyEmailService

#endif // __INCLUDED__DUMMYEMAILSERVICE_HPP__

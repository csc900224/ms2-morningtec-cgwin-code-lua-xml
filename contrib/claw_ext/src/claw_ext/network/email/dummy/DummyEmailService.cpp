#include "claw_ext/network/email/EmailService.hpp"
#include "claw_ext/network/email/dummy/DummyEmailService.hpp"

DummyEmailService::DummyEmailService()
    : m_listener( NULL )
{}

bool DummyEmailService::CanSendEmail() const
{
    return true;
}

void DummyEmailService::SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html )
{
    if ( m_listener )
    {
        m_listener->OnEmailSent( true );
    }
}

void DummyEmailService::SetListener( EmailServiceListener* listener )
{
    m_listener = listener;
}

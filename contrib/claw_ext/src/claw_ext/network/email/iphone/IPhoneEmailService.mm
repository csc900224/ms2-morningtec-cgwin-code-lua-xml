#include "claw_ext/network/email/EmailService.hpp"
#include "claw_ext/network/email/iphone/IphoneEmailService.hpp"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "claw/application/iphone/IPhoneAppDelegate.h"
#import "claw_ext/network/email/iphone/IPhoneEmailServiceDelegate.h"

class IPhoneEmailServiceDelegateWrapper
{
public:
    IPhoneEmailServiceDelegateWrapper( IPhoneEmailService* service )
        : m_delegate( nil )
    {
        m_delegate = [[IPhoneEmailServiceDelegate alloc] initWithService:service];
    }

    ~IPhoneEmailServiceDelegateWrapper()
    {
        [m_delegate release];
    }

    IPhoneEmailServiceDelegate* Get() const
    {
        return m_delegate;
    }

private:
    IPhoneEmailServiceDelegate* m_delegate;

};

IPhoneEmailService::IPhoneEmailService()
    : m_listener( NULL )
    , m_delegate( NULL )
{
    m_delegate = new IPhoneEmailServiceDelegateWrapper( this );
}

IPhoneEmailService::~IPhoneEmailService()
{
    delete m_delegate;
}

bool IPhoneEmailService::CanSendEmail() const
{
    return [MFMailComposeViewController canSendMail] == YES;
    return false;
}

void IPhoneEmailService::SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html )
{
    CLAW_ASSERT( CanSendEmail() );

    MFMailComposeViewController *controller = [[[MFMailComposeViewController alloc] init] autorelease];
    controller.mailComposeDelegate = m_delegate->Get();

    [controller setSubject:[NSString stringWithCString:subject.c_str() encoding:NSUTF8StringEncoding]];

    [controller setMessageBody:[NSString stringWithCString:message.c_str() encoding:NSUTF8StringEncoding]
                        isHTML:html];

    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* viewController = [appDelegate getViewController];
    [viewController presentModalViewController:controller animated:YES];
}

void IPhoneEmailService::SetListener( EmailServiceListener* listener )
{
    m_listener = listener;
}

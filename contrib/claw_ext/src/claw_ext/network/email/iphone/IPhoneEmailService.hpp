#ifndef __INCLUDED__IPHONEEMAILSERVICE_HPP__
#define __INCLUDED__IPHONEEMAILSERVICE_HPP__

#include "claw_ext/network/email/EmailService.hpp"

class IPhoneEmailServiceDelegateWrapper;

class IPhoneEmailService : public EmailServiceBase
{
    friend class IPhoneEmailServiceAdapter;

public:
    IPhoneEmailService();
    ~IPhoneEmailService();

    bool CanSendEmail() const;
    void SendEmail( const Claw::NarrowString& subject, const Claw::NarrowString& message, bool html );
    void SetListener( EmailServiceListener* listener );

private:
    EmailServiceListener* m_listener;

    IPhoneEmailServiceDelegateWrapper* m_delegate;

}; // class IphoneEmailService

#endif // __INCLUDED__IPHONEEMAILSERVICE_HPP__

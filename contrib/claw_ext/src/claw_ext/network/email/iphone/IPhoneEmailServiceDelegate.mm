#import "claw_ext/network/email/iphone/IPhoneEmailServiceDelegate.h"

#import "claw/application/iphone/IPhoneAppDelegate.h"

#include "claw_ext/network/email/EmailService.hpp"
#include "claw_ext/network/email/iphone/IphoneEmailService.hpp"

class IPhoneEmailServiceAdapter
{
public:
    static void OnEmailSent( IPhoneEmailService* service, bool success )
    {
        EmailServiceListener* listener = service->m_listener;
        if ( listener )
        {
            listener->OnEmailSent( success );
        }
    }
};

#pragma mark -

@interface IPhoneEmailServiceDelegate()

@property IPhoneEmailService* service;

@end

#pragma mark -

@implementation IPhoneEmailServiceDelegate

@synthesize service = _service;

- (id)initWithService:(IPhoneEmailService *)service
{
    CLAW_ASSERT( service );

    if ( ( self = [super init] ) )
    {
        self.service = service;
    }

    return self;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* viewController = [appDelegate getViewController];
    [viewController dismissModalViewControllerAnimated:YES];

    IPhoneEmailServiceAdapter::OnEmailSent( self.service, result == MFMailComposeResultSent );
}

@end

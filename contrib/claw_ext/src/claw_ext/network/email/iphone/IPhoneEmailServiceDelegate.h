#import <MessageUI/MFMailComposeViewController.h>

class IPhoneEmailService;

@interface IPhoneEmailServiceDelegate : NSObject <MFMailComposeViewControllerDelegate>

- (id)initWithService:(IPhoneEmailService *)service;

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;

@end
#ifndef __INCLUDED_NETWORK_TIME_HPP__
#define __INCLUDED_NETWORK_TIME_HPP__

#include "claw/network/NtpRequest.hpp"
#include "claw/base/Thread.hpp"

namespace ClawExt
{

    class NetworkTime: public Claw::RefCounter
    {
    public:
        NetworkTime();
        virtual ~NetworkTime();

        unsigned int GetTime() const { return m_epoch ? m_epoch + (int)m_time : 0; }

        void OnUpdate( float dt );
        void OnFocusChange( bool focused );

    private:
        Claw::NtpRequestPtr m_ntp;
        Claw::Thread* m_thread;
        unsigned int m_epoch;
        float m_time;
        bool m_error;

        void Sync();
        static int SyncThreaded( void* data ) { static_cast< NetworkTime* >( data )->SyncThreaded(); return 0; }
        void SyncThreaded();

        void Cleanup();
    };

    typedef Claw::SmartPtr< NetworkTime > NetworkTimePtr;

}

#endif

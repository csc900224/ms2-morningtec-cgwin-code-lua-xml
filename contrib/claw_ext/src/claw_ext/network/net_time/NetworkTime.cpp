#include "claw_ext/network/net_time/NetworkTime.hpp"

namespace ClawExt
{

    static const int CONNECTION_RETRY_PERIOD_MINUTES = 60;

    // -------------------------------- //

    NetworkTime::NetworkTime()
        : m_epoch( 0 )
        , m_time( 0 )
        , m_error( false )
        , m_thread( NULL )
    {
        Sync();
    }

    NetworkTime::~NetworkTime()
    {
        Cleanup();
    }

    void NetworkTime::Cleanup()
    {
        m_ntp.Release();
        delete m_thread;
        m_thread = NULL;
    }

    void NetworkTime::OnUpdate( float dt )
    {
        m_time += dt;

        if( m_ntp )
        {
            if( m_epoch || m_error )
            {
                m_time = 0;
                Cleanup();
            }
        }
        else if( !m_epoch && m_time > CONNECTION_RETRY_PERIOD_MINUTES * 60 )
        {
            m_time = 0;
            Sync();
        }
    }

    void NetworkTime::OnFocusChange( bool focused )
    {
        if( focused )
        {
            Sync();
        }
    }

    void NetworkTime::Sync()
    {
        if( !m_ntp )
        {
            m_ntp.Reset( new Claw::NtpRequest() );
            m_epoch = 0;
            m_error = false;
            CLAW_ASSERT( !m_thread )
            m_thread = new Claw::Thread( SyncThreaded, this );
        }
    }

    void NetworkTime::SyncThreaded()
    {
        m_ntp->Connect();

        if( m_ntp->CheckError() )
        {
            m_error = true;
            return;
        }
        else
        {
            m_epoch = m_ntp->GetEpoch();
            CLAW_ASSERT( m_epoch > 0 )
        }
    }

}

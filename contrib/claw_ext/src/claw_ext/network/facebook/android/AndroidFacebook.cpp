#include "claw_ext/network/facebook/android/AndroidFacebook.hpp"

#include "claw/base/Errors.hpp"
#include "claw/system/android/JniAttach.hpp"

static AndroidFacebook* s_instance = NULL;

FaceBook* FaceBook::QueryInterface( const char* appId, const char* fbUrlSchemeSuffix )
{
    // Force singleton pattern
    CLAW_ASSERT( s_instance == NULL );
    s_instance = new AndroidFacebook( appId );
    return s_instance;
}

void FaceBook::Release( FaceBook* instance )
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;
    delete instance;
}

AndroidFacebook::AndroidFacebook( const char* appId )
{
    CLAW_ASSERT( appId );

    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "<init>", "(Ljava/lang/String;)V" );
    CLAW_ASSERT( mid != NULL );

    jobject jfacebook = env->NewObject( cls, mid, Claw::JniAttach::GetStringFromChars( env, appId ) );
    CLAW_ASSERT( jfacebook != NULL );

    m_facebook = env->NewGlobalRef( jfacebook );
    CLAW_ASSERT( m_facebook != NULL );

    env->DeleteLocalRef( jfacebook );

    Claw::JniAttach::Detach( isAttached );
}

AndroidFacebook::~AndroidFacebook()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    env->DeleteGlobalRef( m_facebook );

    Claw::JniAttach::Detach( isAttached );
}

bool AndroidFacebook::Authenticate( bool allowLoginUi, Claw::NarrowString* err /* = NULL */ )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "authenticate", "(Z)V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod( m_facebook, mid, allowLoginUi );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::PublishFeed()
{
    CLAW_MSG_ASSERT( false, "Not implemented" );
    return false;
}

bool AndroidFacebook::PublishFeed( const FeedData& feed )
{
    CLAW_MSG_ASSERT( false, "Not implemented" );
    return false;
}

bool AndroidFacebook::PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message )
{
    PngBuffer buffer;
    buffer.data = NULL;
    buffer.size = 0;

    // Compress image data to PNG format
    PngCompress( &buffer, pixels, width, height );

    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jbyteArray data = env->NewByteArray( buffer.size );
    env->SetByteArrayRegion( data, 0, buffer.size, (jbyte*)buffer.data );

    // Release original PNG data
    if ( buffer.data )
    {
        free( buffer.data );
    }

    jmethodID mid = env->GetMethodID( cls, "publishImage", "([BLjava/lang/String;)V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod( m_facebook, mid, data, Claw::JniAttach::GetStringFromChars( env, message.c_str() ) );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

void AndroidFacebook::PngCompress( PngBuffer* buffer, const char* image, int width, int height )
{
    CLAW_ASSERT( buffer );

    png_structp png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    png_infop info_ptr = png_create_info_struct( png_ptr );
    setjmp( png_jmpbuf( png_ptr ) );

    png_set_write_fn( png_ptr, buffer, &AndroidFacebook::PngWrite, NULL );
    png_set_IHDR( png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE );
    png_write_info( png_ptr, info_ptr );

    const char* ptr = image;
    for( int i=0; i<height; i++ )
    {
        png_write_rows( png_ptr, (png_bytepp)(&ptr), 1 );
        ptr += width*4;
    }

    png_write_end( png_ptr, info_ptr );
    png_destroy_write_struct( &png_ptr, &info_ptr );
}

void AndroidFacebook::PngWrite( png_structp png_ptr, png_bytep data, png_size_t length )
{
    PngBuffer* buffer = static_cast<PngBuffer*>( png_ptr->io_ptr );
    if ( !buffer->data )
    {
        buffer->data = (char*)malloc( length );
    }
    else
    {
        buffer->data = (char*)realloc( buffer->data, buffer->size + length );
    }

    memcpy( buffer->data + buffer->size, data, length );
    buffer->size += length;
}

bool AndroidFacebook::PublishAction( const char* graphPath, const char* objectName, const char* objectUrl )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "publishAction", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod(
        m_facebook,
        mid,
        Claw::JniAttach::GetStringFromChars( env, graphPath ),
        Claw::JniAttach::GetStringFromChars( env, objectName ),
        Claw::JniAttach::GetStringFromChars( env, objectUrl )
    );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::SendRequest( const RequestData& appRequest )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "sendRequest", "(Lcom/gamelion/AndroidRequestData;)V" );
    CLAW_ASSERT( mid != NULL );

    jclass requestDataCls = env->FindClass( "com/gamelion/AndroidRequestData" );
    CLAW_ASSERT( requestDataCls != NULL );

    jmethodID requestDataMid = env->GetMethodID( requestDataCls, "<init>", "()V" );
    CLAW_ASSERT( requestDataMid != NULL );

    jobject requestData = env->NewObject( requestDataCls, requestDataMid );
    CLAW_ASSERT( requestData != NULL );

    // At least request message must be specified
    CLAW_ASSERT( !appRequest.message.empty() );
    {
        jfieldID field = env->GetFieldID( requestDataCls, "message", "Ljava/lang/String;" );
        CLAW_ASSERT( field != NULL );

        jstring message = Claw::JniAttach::GetStringFromChars( env, appRequest.message.c_str() );
        env->SetObjectField( requestData, field, message );
        Claw::JniAttach::ReleaseString( env, message );
    }

    if ( !appRequest.uid.empty() )
    {
        jfieldID field = env->GetFieldID( requestDataCls, "userId", "Ljava/lang/String;" );
        CLAW_ASSERT( field != NULL );

        jstring uid = Claw::JniAttach::GetStringFromChars( env, appRequest.uid.c_str() );
        env->SetObjectField( requestData, field, uid );
        Claw::JniAttach::ReleaseString( env, uid );
    }

    if ( !appRequest.properties.empty() )
    {
        jclass requestPropertyCls = env->FindClass( "com/gamelion/AndroidRequestProperty" );
        CLAW_ASSERT( requestPropertyCls != NULL );

        jmethodID requestPropertyMid = env->GetMethodID( requestPropertyCls, "<init>", "()V" );
        CLAW_ASSERT( requestPropertyMid != NULL );

        jfieldID keyField = env->GetFieldID( requestPropertyCls, "key", "Ljava/lang/String;" );
        CLAW_ASSERT( keyField != NULL );

        jfieldID valueField = env->GetFieldID( requestPropertyCls, "value", "Ljava/lang/String;" );
        CLAW_ASSERT( valueField != NULL );

        jobjectArray properties = env->NewObjectArray( appRequest.properties.size(), requestPropertyCls, 0 );
        CLAW_ASSERT( properties != NULL );

        RequestPropertyList::const_iterator it = appRequest.properties.begin();
        RequestPropertyList::const_iterator end = appRequest.properties.end();
        for ( int i = 0; it != end; ++i, ++it )
        {
            jobject property = env->NewObject( requestPropertyCls, requestPropertyMid );
            CLAW_ASSERT( property != NULL );

            jstring key = Claw::JniAttach::GetStringFromChars( env, it->key.c_str() );
            env->SetObjectField( property, keyField, key );
            Claw::JniAttach::ReleaseString( env, key );

            jstring value = Claw::JniAttach::GetStringFromChars( env, it->value.c_str() );
            env->SetObjectField( property, valueField, value );
            Claw::JniAttach::ReleaseString( env, value );

            env->SetObjectArrayElement( properties, i, property );
            env->DeleteLocalRef( property );
        }

        jfieldID field = env->GetFieldID( requestDataCls, "properties", "[Lcom/gamelion/AndroidRequestProperty;" );
        CLAW_ASSERT( field != NULL );

        env->SetObjectField( requestData, field, properties );
        env->DeleteLocalRef( properties );
    }

    env->CallVoidMethod( m_facebook, mid, requestData );
    env->DeleteLocalRef( requestData );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::SendRequest( const RequestData& appRequest, const RequestCallbackData& callback )
{
    CLAW_ASSERT( callback.function );

    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "sendRequest", "(Lcom/gamelion/AndroidRequestData;Lcom/gamelion/AndroidRequestCallback;)V" );
    CLAW_ASSERT( mid != NULL );

    jclass requestCallbackCls = env->FindClass( "com/gamelion/AndroidRequestCallback" );
    CLAW_ASSERT( requestCallbackCls != NULL );

    jmethodID requestCallbackMid = env->GetMethodID( requestCallbackCls, "<init>", "()V" );
    CLAW_ASSERT( requestCallbackMid != NULL );

    jobject requestCallback = env->NewObject( requestCallbackCls, requestCallbackMid );
    CLAW_ASSERT( requestCallback != NULL );

    // Set callback function ptr
    {
        jfieldID field = env->GetFieldID( requestCallbackCls, "function", "J" );
        CLAW_ASSERT( field != NULL );

        long function = (long)callback.function;
        env->SetLongField( requestCallback, field, function );
    }

    // Set callback data ptr
    {
        jfieldID field = env->GetFieldID( requestCallbackCls, "ptr", "J" );
        CLAW_ASSERT( field != NULL );

        long ptr = (long)callback.ptr;
        env->SetLongField( requestCallback, field, ptr );
    }

    jclass requestDataCls = env->FindClass( "com/gamelion/AndroidRequestData" );
    CLAW_ASSERT( requestDataCls != NULL );

    jmethodID requestDataMid = env->GetMethodID( requestDataCls, "<init>", "()V" );
    CLAW_ASSERT( requestDataMid != NULL );

    jobject requestData = env->NewObject( requestDataCls, requestDataMid );
    CLAW_ASSERT( requestData != NULL );

    // At least request message must be specified
    CLAW_ASSERT( !appRequest.message.empty() );
    {
        jfieldID field = env->GetFieldID( requestDataCls, "message", "Ljava/lang/String;" );
        CLAW_ASSERT( field != NULL );

        jstring message = Claw::JniAttach::GetStringFromChars( env, appRequest.message.c_str() );
        env->SetObjectField( requestData, field, message );
        Claw::JniAttach::ReleaseString( env, message );
    }

    if ( !appRequest.uid.empty() )
    {
        jfieldID field = env->GetFieldID( requestDataCls, "userId", "Ljava/lang/String;" );
        CLAW_ASSERT( field != NULL );

        jstring uid = Claw::JniAttach::GetStringFromChars( env, appRequest.uid.c_str() );
        env->SetObjectField( requestData, field, uid );
        Claw::JniAttach::ReleaseString( env, uid );
    }

    if ( !appRequest.properties.empty() )
    {
        jclass requestPropertyCls = env->FindClass( "com/gamelion/AndroidRequestProperty" );
        CLAW_ASSERT( requestPropertyCls != NULL );

        jmethodID requestPropertyMid = env->GetMethodID( requestPropertyCls, "<init>", "()V" );
        CLAW_ASSERT( requestPropertyMid != NULL );

        jfieldID keyField = env->GetFieldID( requestPropertyCls, "key", "Ljava/lang/String;" );
        CLAW_ASSERT( keyField != NULL );

        jfieldID valueField = env->GetFieldID( requestPropertyCls, "value", "Ljava/lang/String;" );
        CLAW_ASSERT( valueField != NULL );

        jobjectArray properties = env->NewObjectArray( appRequest.properties.size(), requestPropertyCls, 0 );
        CLAW_ASSERT( properties != NULL );

        RequestPropertyList::const_iterator it = appRequest.properties.begin();
        RequestPropertyList::const_iterator end = appRequest.properties.end();
        for ( int i = 0; it != end; ++i, ++it )
        {
            jobject property = env->NewObject( requestPropertyCls, requestPropertyMid );
            CLAW_ASSERT( property != NULL );

            jstring key = Claw::JniAttach::GetStringFromChars( env, it->key.c_str() );
            env->SetObjectField( property, keyField, key );
            Claw::JniAttach::ReleaseString( env, key );

            jstring value = Claw::JniAttach::GetStringFromChars( env, it->value.c_str() );
            env->SetObjectField( property, valueField, value );
            Claw::JniAttach::ReleaseString( env, value );

            env->SetObjectArrayElement( properties, i, property );
            env->DeleteLocalRef( property );
        }

        jfieldID field = env->GetFieldID( requestDataCls, "properties", "[Lcom/gamelion/AndroidRequestProperty;" );
        CLAW_ASSERT( field != NULL );

        env->SetObjectField( requestData, field, properties );
        env->DeleteLocalRef( properties );
    }

    env->CallVoidMethod( m_facebook, mid, requestData, requestCallback );
    env->DeleteLocalRef( requestData );
    env->DeleteLocalRef( requestCallback );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::GetRequests()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getRequests", "()V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod( m_facebook, mid );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::RemoveRequest( const char* identifier )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "removeRequest", "(Ljava/lang/String;)V" );
    CLAW_ASSERT( mid != NULL );

    jstring requestId = Claw::JniAttach::GetStringFromChars( env, identifier );
    env->CallVoidMethod( m_facebook, mid, requestId );
    Claw::JniAttach::ReleaseString( env, requestId );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::SendScore( int score )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "sendScore", "(I)V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod( m_facebook, mid, score );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

bool AndroidFacebook::GetScores( int limit )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getScores", "(I)V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod( m_facebook, mid, limit );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

Claw::NarrowString AndroidFacebook::GetUserId()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getUserId", "()Ljava/lang/String;" );
    CLAW_ASSERT( mid != NULL );

    jobject result = env->CallObjectMethod( m_facebook, mid );
    CLAW_ASSERT( result != NULL );

    const char* uid = env->GetStringUTFChars( (jstring)result, 0 );
    Claw::NarrowString ret( uid );
    env->ReleaseStringUTFChars( (jstring)result, uid );

    return ret;
}

Claw::NarrowString AndroidFacebook::GetUserName()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidFacebook" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getUserName", "()Ljava/lang/String;" );
    CLAW_ASSERT( mid != NULL );

    jobject result = env->CallObjectMethod( m_facebook, mid );
    CLAW_ASSERT( result != NULL );

    const char* uid = env->GetStringUTFChars( (jstring)result, 0 );
    Claw::NarrowString ret( uid );
    env->ReleaseStringUTFChars( (jstring)result, uid );

    return ret;
}

void AndroidFacebook::NotifyAuthenticationChange( bool authenticated )
{
    FaceBook::NotifyAuthenticationChange( authenticated );
}

void AndroidFacebook::NotifyFeedPublished( const char* identifier )
{
    FaceBook::NotifyFeedPublished( identifier );
}

void AndroidFacebook::NotifyImagePublished( const char* identifier )
{
    FaceBook::NotifyImagePublished( identifier );
}

void AndroidFacebook::NotifyActionPublished( const char* identifier )
{
    FaceBook::NotifyActionPublished( identifier );
}

void AndroidFacebook::NotifyRequestSent( const char* identifier )
{
    FaceBook::NotifyRequestSent( identifier );
}

void AndroidFacebook::NotifyRequestReceived( const RequestData& appRequest )
{
    FaceBook::NotifyRequestReceived( appRequest );
}

void AndroidFacebook::NotifyRequestRemoved()
{
    FaceBook::NotifyRequestRemoved();
}

void AndroidFacebook::NotifyScoreSent()
{
    FaceBook::NotifyScoreSent();
}

void AndroidFacebook::NotifyScoresReceived( std::vector<ScoreData>& scores )
{
    FaceBook::NotifyScoresReceived( scores );
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onAuthenticationChanged( JNIEnv* env, jclass clazz, jboolean authenticated )
    {
        CLAW_ASSERT( s_instance );
        s_instance->NotifyAuthenticationChange( authenticated );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onImagePublished( JNIEnv* env, jclass clazz, jstring identifier )
    {
        const char* id = env->GetStringUTFChars( identifier, 0 );

        CLAW_ASSERT( s_instance );
        s_instance->NotifyImagePublished( strlen( id ) ? id : NULL );

        env->ReleaseStringUTFChars( identifier, id );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onActionPublished( JNIEnv* env, jclass clazz, jstring identifier )
    {
        const char* id = env->GetStringUTFChars( identifier, 0 );

        CLAW_ASSERT( s_instance );
        s_instance->NotifyActionPublished( strlen( id ) ? id : NULL );

        env->ReleaseStringUTFChars( identifier, id );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onRequestSent( JNIEnv* env, jclass clazz, jstring identifier )
    {
        const char* id = env->GetStringUTFChars( identifier, 0 );

        CLAW_ASSERT( s_instance );
        s_instance->NotifyRequestSent( id );

        env->ReleaseStringUTFChars( identifier, id );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onRequestSentCallback( JNIEnv* env, jclass clazz, jstring identifier, jobject callback )
    {
        const char* id = env->GetStringUTFChars( identifier, 0 );

        FaceBook::RequestCallback fun = NULL;
        void* ptr = NULL;

        jclass cls = env->GetObjectClass( callback );

        // Get callback function
        {
            jfieldID field = env->GetFieldID( cls, "function", "J" );
            jlong value = env->GetLongField( callback, field );

            fun = (FaceBook::RequestCallback)( value );
        }

        // Get callback ptr
        {
            jfieldID field = env->GetFieldID( cls, "ptr", "J" );
            jlong value = env->GetLongField( callback, field );

            ptr = (void*)( value );
        }

        fun( id, ptr );

        env->ReleaseStringUTFChars( identifier, id );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onRequestReceived( JNIEnv* env, jclass clazz, jobject request )
    {
        FaceBook::RequestData requestData;

        jclass cls = env->GetObjectClass( request );

        // Get request ID
        {
            jfieldID field = env->GetFieldID( cls, "id", "Ljava/lang/String;" );
            jobject value = env->GetObjectField( request, field );

            const char* id = env->GetStringUTFChars( (jstring)value, 0 );
            requestData.id = id;
            env->ReleaseStringUTFChars( (jstring)value, id );
        }

        // Get user ID
        {
            jfieldID field = env->GetFieldID( cls, "userId", "Ljava/lang/String;" );
            jobject value = env->GetObjectField( request, field );

            const char* uid = env->GetStringUTFChars( (jstring)value, 0 );
            requestData.uid = uid;
            env->ReleaseStringUTFChars( (jstring)value, uid );
        }

        // Get user name
        {
            jfieldID field = env->GetFieldID( cls, "userName", "Ljava/lang/String;" );
            jobject value = env->GetObjectField( request, field );

            const char* username = env->GetStringUTFChars( (jstring)value, 0 );
            requestData.username = username;
            env->ReleaseStringUTFChars( (jstring)value, username );
        }

        // Get message
        {
            jfieldID field = env->GetFieldID( cls, "message", "Ljava/lang/String;" );
            jobject value = env->GetObjectField( request, field );

            const char* message = env->GetStringUTFChars( (jstring)value, 0 );
            requestData.message = message;
            env->ReleaseStringUTFChars( (jstring)value, message );
        }

        // Get request properties
        {
            jfieldID requestPropertiesField = env->GetFieldID( cls, "properties", "[Lcom/gamelion/AndroidRequestProperty;" );
            jobjectArray requestProperties = (jobjectArray)env->GetObjectField( request, requestPropertiesField );

            if ( requestProperties )
            {
                jclass requestPropertyCls = env->FindClass( "com/gamelion/AndroidRequestProperty" );
                CLAW_ASSERT( requestPropertyCls != NULL );

                jfieldID keyField = env->GetFieldID( requestPropertyCls, "key", "Ljava/lang/String;" );
                CLAW_ASSERT( keyField != NULL );

                jfieldID valueField = env->GetFieldID( requestPropertyCls, "value", "Ljava/lang/String;" );
                CLAW_ASSERT( valueField != NULL );

                jsize length = env->GetArrayLength( requestProperties );
                for ( int i = 0; i < length; ++i )
                {
                    jobject property = env->GetObjectArrayElement( requestProperties, i );
                    jobject propertyKey = env->GetObjectField( property, keyField );
                    jobject propertyValue = env->GetObjectField( property, valueField );

                    const char* key = env->GetStringUTFChars( (jstring)propertyKey, 0 );
                    const char* value = env->GetStringUTFChars( (jstring)propertyValue, 0 );

                    FaceBook::RequestProperty requestProperty;
                    requestProperty.key = key;
                    requestProperty.value = value;

                    env->ReleaseStringUTFChars( (jstring)propertyKey, key );
                    env->ReleaseStringUTFChars( (jstring)propertyValue, value );

                    requestData.properties.push_back( requestProperty );
                }
            }
        }

        CLAW_ASSERT( s_instance );
        s_instance->NotifyRequestReceived( requestData );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onRequestRemoved( JNIEnv* env, jclass clazz )
    {
        CLAW_ASSERT( s_instance );
        s_instance->NotifyRequestRemoved();
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onScoresReceived( JNIEnv* env, jclass clazz, jobjectArray scores )
    {
        std::vector<FaceBook::ScoreData> data;

        jsize length = env->GetArrayLength( scores );
        for ( int i = 0; i < length; ++i )
        {
            jobject entry = env->GetObjectArrayElement( scores, i );
            jclass cls = env->GetObjectClass( entry );

            FaceBook::ScoreData scoreData;

            // Get user id
            {
                jmethodID method = env->GetMethodID( cls, "GetUserId", "()Ljava/lang/String;" );
                jobject result = env->CallObjectMethod( entry, method );

                const char* uid = env->GetStringUTFChars( (jstring)result, 0 );
                scoreData.m_uid = uid;
                env->ReleaseStringUTFChars( (jstring)result, uid );
            }

            // Get user name
            {
                jmethodID method = env->GetMethodID( cls, "GetUserName", "()Ljava/lang/String;" );
                jobject result = env->CallObjectMethod( entry, method );

                const char* username = env->GetStringUTFChars( (jstring)result, 0 );
                scoreData.m_username = username;
                env->ReleaseStringUTFChars( (jstring)result, username );
            }

            // Get profile picture
            {
                jmethodID method = env->GetMethodID( cls, "GetPicture", "()Ljava/lang/String;" );
                jobject result = env->CallObjectMethod( entry, method );

                const char* picture = env->GetStringUTFChars( (jstring)result, 0 );
                scoreData.m_picture = picture;
                env->ReleaseStringUTFChars( (jstring)result, picture );
            }

            // Get score
            {
                jmethodID method = env->GetMethodID( cls, "GetScore", "()I" );
                jint result = env->CallIntMethod( entry, method );

                scoreData.m_score = result;
            }

            data.push_back( scoreData );
        }

        CLAW_ASSERT( s_instance );
        s_instance->NotifyScoresReceived( data );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidFacebook_onScoreSent( JNIEnv* env, jclass clazz )
    {
        CLAW_ASSERT( s_instance );
        s_instance->NotifyScoreSent();
    }
}

#ifndef __NETWORK_WIN32_FACEBOOK_HPP__
#define __NETWORK_WIN32_FACEBOOK_HPP__

#include "libpng/png.h"
#include "libpng/pngstruct.h"

#include "claw_ext/network/facebook/Facebook.hpp"

class Win32Facebook : public FaceBook
{
public:
    virtual bool        Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL );

    virtual bool        PublishFeed();

    virtual bool        PublishFeed( const FeedData& feed );

    virtual bool        PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message );

    virtual bool        PublishAction( const char* graphPath, const char* objectName, const char* objectUrl );

    virtual bool        SendRequest( const RequestData& appRequest );

    virtual bool        SendRequest( const RequestData& appRequest, const RequestCallbackData& callback );

    virtual bool        GetRequests();

    virtual bool        RemoveRequest( const char* requestId );

    virtual bool        SendScore( int score );

    virtual bool        GetScores( int limit );

    virtual Claw::NarrowString GetUserId();
    virtual Claw::NarrowString GetUserName();

private:
    static void         PngCreate( const char* filename, const char* pixels, int width, int height );
    static void         PngWrite( png_structp png_ptr, png_bytep data, png_size_t length );

}; // class Win32Facebook

#endif // !defined __NETWORK_WIN32_FACEBOOK_HPP__
// EOF

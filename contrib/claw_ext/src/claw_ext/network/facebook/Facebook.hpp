//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/facebook/Facebook.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_SOCIAL_FACEBOOK_HPP__
#define __NETWORK_SOCIAL_FACEBOOK_HPP__

// Internal includes

// External includes
#include "claw/base/String.hpp"

#include <list>
#include <vector>

class FaceBook
{
public:
    // Forward declare
    class Observer;

    struct FeedProperty
    {
        Claw::NarrowString key;
        Claw::NarrowString text;
        Claw::NarrowString href;
    };

    typedef std::list<FeedProperty> FeedPropertyList;

    struct FeedAction
    {
        Claw::NarrowString name;
        Claw::NarrowString link;
    };

    struct FeedData
    {
        Claw::NarrowString link;
        Claw::NarrowString picture;
        Claw::NarrowString name;
        Claw::NarrowString caption;
        Claw::NarrowString description;

        FeedAction action;
        FeedPropertyList properties;
    };

    enum RequestFilter
    {
        RF_ALL,
        RF_APP_USERS,
        RF_APP_NON_USERS,
    };

    struct RequestProperty
    {
        Claw::NarrowString key;
        Claw::NarrowString value;
    };

    typedef std::list<RequestProperty> RequestPropertyList;

    struct RequestData
    {
        Claw::NarrowString id;
        Claw::NarrowString uid;
        Claw::NarrowString username;
        Claw::NarrowString message;

        RequestFilter filter;
        RequestPropertyList properties;
    };

    typedef void (*RequestCallback)( const char* requestId, void* ptr );

    struct RequestCallbackData
    {
        RequestCallback function;
        void* ptr;
    };

    struct ScoreData
    {
        Claw::NarrowString m_uid;
        Claw::NarrowString m_username;
        Claw::NarrowString m_picture;
        int m_score;
    };

    virtual             ~FaceBook() {}

    //! Register Facebook observer.
    /**
     * \return false if observer was already registered, true if successfully registered.
     */
    bool                RegisterObserver( Observer* observer );

    //! Unregister Facebook observer.
    /**
     * \return false if observer was not registered to this game center.
     */
    bool                UnregisterObserver( Observer* observer );

    //! Check authentication status
    bool                IsAuthenticated() const;

    //! Try to authenticate and login to FB account.application to local player
    /**
    * Log in and grant application all neccessary permissions.
    * The result of authentication is send via OnAuthenticationChange() method to GameCenter::Observer
    * objects registered in the game center.
    */
    virtual bool        Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL ) = 0;

    //! Send feed to the wall.
    virtual bool        PublishFeed() = 0;

    //! Send feed with additional icon, link and text description.
    /*
    * FeedData structur defines feed details such as link title, url, caption below
    * the link, some more description (below caption) and the addditional picture url.
    */
    virtual bool        PublishFeed( const FeedData& feed ) = 0;

    //! Send image to users album
    /**
     * This methods expects image data in RGBA (8-bits per pixel) format
     */
    virtual bool        PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message ) = 0;

    //! Publish new action to users activity log
    virtual bool        PublishAction( const char* graphPath, const char* objectName, const char* objectUrl ) = 0;

    //! Publish application invitation.
    virtual bool        SendRequest( const RequestData& appRequest ) = 0;

    //! Send application request using custom callback without notifying observers
    virtual bool        SendRequest( const RequestData& appRequest, const RequestCallbackData& callback ) = 0;

    //! Get incoming application requests
    virtual bool        GetRequests() = 0;

    //! Remove request
    virtual bool        RemoveRequest( const char* requestId ) = 0;

    //! Saves score for current user
    virtual bool        SendScore( int score ) = 0;

    //! Get application scores
    virtual bool        GetScores( int limit = 0 ) = 0;

    virtual Claw::NarrowString GetUserId() = 0;
    virtual Claw::NarrowString GetUserName() = 0;

    //! Retreive platfrom dependend implementation.
    /**
     * This method should be implemented and linked once in platform dependend object,
     * returning appropriate FaceBook object implementation.
     */
    static FaceBook*    QueryInterface( const char* fbAppId, const char* fbUrlSchemeSuffix = NULL );

    //! Release platform specific implementation.
    /**
     * Call destructors, release memory, make cleanup.
     */
    static void         Release( FaceBook* fb );

    //! Observer class for receiving api callbacks.
    class Observer
    {
    public:
        //! Called in response to change in player authentication.
        virtual void    OnAuthenticationChange( bool authenticated ) {}

        //! Called after successfully publishing feed
        virtual void    OnFeedPublished( const char* id ) {}

        //! Called after publishing photo to users album (id will be NULL if there was an error)
        virtual void    OnImagePublished( const char* id ) {}

        //! Called after publishing OpenGraph action (id will be NULL if there was an error)
        virtual void    OnActionPublished( const char* id ) {}

        //! Called after successfully sending request
        virtual void    OnRequestSent( const char* id ) {}

        //! Called after reveiving application request
        virtual void    OnRequestReceived( const RequestData& appRequest ) {}

        //! Called after successfully removing application request
        virtual void    OnRequestRemoved() {}

        //! Called after successfully sending score for current user
        virtual void    OnScoreSent() {}

        //! Called after successfully received list of scores
        virtual void    OnScoresReceived( std::vector<ScoreData>& scores ) {}

    }; // class Observer

protected:
    //! Protected constructor to disallow manual creation.
                        FaceBook();

    void                NotifyAuthenticationChange( bool authenticated );
    void                NotifyFeedPublished( const char* id );
    void                NotifyImagePublished( const char* id );
    void                NotifyActionPublished( const char* id );
    void                NotifyRequestSent( const char* id );
    void                NotifyRequestReceived( const RequestData& appRequest );
    void                NotifyRequestRemoved();
    void                NotifyScoreSent();
    void                NotifyScoresReceived( std::vector<ScoreData>& scores );

    typedef std::list<Observer*>        Observers;
    typedef Observers::iterator         ObserversIt;
    typedef Observers::const_iterator   ObserversConstIt;

    Observers           m_observers;
    bool                m_authenticated;

}; // class FaceBook

#endif // !defined __NETWORK_SOCIAL_FACEBOOK_HPP__
// EOF

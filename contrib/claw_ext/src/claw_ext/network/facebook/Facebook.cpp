//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/facebook/Facebook.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/facebook/Facebook.hpp"

// External includes
#include <algorithm>    // std::find

bool FaceBook::RegisterObserver( FaceBook::Observer* observer )
{
    // Already registered
    if( std::find( m_observers.begin(), m_observers.end(), observer ) != m_observers.end() )
        return false;
    m_observers.push_back( observer );
    return true;
}

bool FaceBook::UnregisterObserver( FaceBook::Observer* observer )
{
    // Not yet registered
    ObserversIt it = std::find( m_observers.begin(), m_observers.end(), observer );
    if( it == m_observers.end() )
        return false;
    m_observers.erase( it );
    return true;
}

bool FaceBook::IsAuthenticated() const
{
    return m_authenticated;
}

FaceBook::FaceBook()
    : m_authenticated( false )
{
}

void FaceBook::NotifyAuthenticationChange( bool authenticated )
{
    m_authenticated = authenticated;

    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnAuthenticationChange( authenticated );
    }
}

void FaceBook::NotifyFeedPublished( const char* id )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnFeedPublished( id );
    }
}

void FaceBook::NotifyImagePublished( const char* id )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnImagePublished( id );
    }
}

void FaceBook::NotifyActionPublished( const char* id )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnActionPublished( id );
    }
}

void FaceBook::NotifyRequestSent( const char* id )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnRequestSent( id );
    }
}

void FaceBook::NotifyRequestReceived( const RequestData& appRequest )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnRequestReceived( appRequest );
    }
}

void FaceBook::NotifyRequestRemoved()
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnRequestRemoved();
    }
}

void FaceBook::NotifyScoreSent()
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnScoreSent();
    }
}

void FaceBook::NotifyScoresReceived( std::vector<ScoreData>& scores )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnScoresReceived( scores );
    }
}

// EOF

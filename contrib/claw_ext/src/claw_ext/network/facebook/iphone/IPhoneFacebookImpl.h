//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/facebook/iphone/IPhoneFacebookImpl.h
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Intrernal includes
#import <Foundation/Foundation.h>

// External includes
#import "FBConnect.h"

// Import Claw main App class
#import "claw/application/iphone/IPhoneAppDelegate.h"

#include "claw_ext/network/facebook/Facebook.hpp"

class IPhoneFacebook;

@interface IPhoneFacebookImpl : NSObject <FBDialogDelegate>
{
    IPhoneFacebook*     m_observer;
}

// Constructor
- (id) initWithAppId:(NSString*)appId andUrlSchemeSuffix:(NSString*)urlSchemeSuffix andObserver:(IPhoneFacebook*) observer;

- (void) dealloc;

// Call this method to prompt the user to log in to Facebook and grant the requested
// permissions to the application. Your current FBSessionDelegate delegate should be set up to handle
// the scenarios where the user grants or denies permissions, as well as any error scenarios
- (bool) authenticateWithLoginUI:(BOOL)allowLoginUI;
// Simply stop the facebook session
- (bool) logout;

// Send application request
- (bool) sendRequestWithParams:(NSMutableDictionary*) params;

// Send application request with custom callback
- (bool) sendRequestWithParams:(NSMutableDictionary*) params andCallback:(FaceBook::RequestCallbackData) callback;

// Get incoming requests
- (bool) getRequests;

// Remove request
- (bool) removeRequest:(NSString*)requestId;

// Simple feed without link and image
- (bool) publishFeed;

// Publish customized feed
// See: http://developers.facebook.com/docs/reference/dialogs/feed/
- (bool) publishFeedWithParams:(NSMutableDictionary*) params;

- (bool) publishImage:(UIImage*)image withMessage:(NSString*)message;

- (bool) publishAction:(NSString*)graphPath withObjectName:(NSString*)objectName andObjectUrl:(NSString*)objectUrl;

- (bool) getScoresWithLimit:(int)limit;

- (bool) sendScore:(int)score;

/////////////////////////////////////////////////////////////////////////
// From FBDialogDelegate:
/////////////////////////////////////////////////////////////////////////

// Called when the dialog succeeds and is about to be dismissed.
- (void)dialogDidComplete:(FBDialog *)dialog;
// Called when the dialog succeeds with a returning url.
- (void)dialogCompleteWithUrl:(NSURL *)url;
// Called when the dialog get canceled by the user.
- (void)dialogDidNotCompleteWithUrl:(NSURL *)url;
// Called when the dialog is cancelled and is about to be dismissed.
- (void)dialogDidNotComplete:(FBDialog *)dialog;
// Called when dialog failed to load due to an error.
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error;

@end

@interface IPhoneFacebookImpl (UserData)

@property (readonly, nonatomic) NSString* uid;
@property (readonly, nonatomic) NSString* username;

@end

@interface IPhoneAppDelegate(FacebookSupport)

// HANDLING THE SSO (Single-Sign-On) on the older systems that doesn't have Facebook App on board.

// To allow redirection from the Facebook authentication page opened in Safari on the older
// iOs systems, we need to define URL handling application methods. Here assume that
// main app interface is comming from Claw and its class is named "IPhoneAppDelegate".

// IMPORTANT:
// The last thing that needs to be accomplished to enable SSO support is a change to the .plist file
// that handles configuration for the app. Xcode creates this file automatically when the project is created.
// A specific URL needs to be registered in this file that uniquely identifies the app with iOS.
// Create a new row named URL types with a single item, URL Schemes, containing a single value,
// fbYOUR_APP_ID (the literal characters fb followed by your app ID). The following shows exactly how
// the row should appear in the .plist file.

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation;

@end
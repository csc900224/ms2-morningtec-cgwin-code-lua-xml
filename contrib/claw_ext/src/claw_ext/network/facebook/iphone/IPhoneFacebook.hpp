//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/facebook/iphone/IPhoneFacebook.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IPHONE_FACEBOOK_HPP__
#define __NETWORK_IPHONE_FACEBOOK_HPP__

// Internal includes
#include "claw_ext/network/facebook/Facebook.hpp"

// External includes

// Forward declaration for PIMPL
//class IPhoneFacebookImpl;

class IPhoneFacebook : public FaceBook
{
public:
                    IPhoneFacebook( const char* appId, const char* fbUrlSchemeSuffix );

                    ~IPhoneFacebook();

    //! Facebook authentiation, based on Single-Sign-On mechanizm (SSO).
    /**
    * SSO primarily works by redirecting the user to the Facebook app on his/her device.
    * Since the user is already logged into Facebook, they will not need to enter their
    * username and password to identify themselves. They will see the auth dialog with the permissions
    * that your app has asked for and if they allow then they will be redirected to your
    * app with the appropriate access_token.
    * Developers should be aware that Facebook SSO will behave slightly different depending on what is
    * installed on the user's device. This is what happens in certain configurations:
    * 1) If the app is running in a version of iOS that supports multitasking, and if the device has
    * the Facebook app of version 3.2.3 or greater installed, the SDK attempts to open the authorization
    * dialog within the Facebook app. After the user grants or declines the authorization, the Facebook app
    * redirects back to the calling app, passing the authorization token, expiration, and any other
    * parameters the Facebook OAuth server may return.
    * 2) If the device is running in a version of iOS that supports multitasking, but it doesn't have
    * the Facebook app of version 3.2.3 or greater installed, the SDK will open the authorization
    * dialog in Safari. After the user grants or revokes the authorization, Safari redirects back to
    * the calling app. Similar to the Facebook app based authorization, this allows multiple apps to share
    * the same Facebook user access_token through the Safari cookie.
    * 3) If the app is running a version of iOS that does not support multitasking, the SDK uses the old
    * mechanism of popping up an inline UIWebView, prompting the user to log in and grant access.
    * \note IMPORTANT: In 2) case to enable SSO support change the .plist file that handles configuration
    * for the app (Xcode creates this file automatically). A specific URL needs to be registered in this file
    * that uniquely identifies the app with iOS. Create a new row named URL types with a single item,
    * URL Schemes, containing a single value, fbYOUR_APP_ID (the literal characters fb followed by your app ID).
    * The following shows exactly how the row should appear in the .plist file
    * | URL types
    * |-- Item0
    *    |-- URL Schemes
    *       |-- Item 0 fbYOUR_APP_ID
    * \return true is successfuly TRIED to authenticate.
    */
    virtual bool    Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL );

    //! Close facebook session.
    /**
    * \note Note that logging out will not revoke your application's permissions, but will simply clear your
    * application's access_token. If a user that has previously logged out of your app returns,
    * they will simply see a notification that they are logging into your app, not a notification
    * to grant permissions.
    * \return true if actually attempted to logout,
    */
    virtual bool    Logout();

    virtual bool    PublishFeed();

    virtual bool    PublishFeed( const FeedData& feed );

    virtual bool    PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message );

    virtual bool    PublishAction( const char* graphPath, const char* objectName, const char* objectUrl );

    virtual bool    SendRequest( const RequestData& request );

    virtual bool    SendRequest( const RequestData& request, const RequestCallbackData& callback );

    virtual bool    GetRequests();

    virtual bool    RemoveRequest( const char* requestId );

    virtual bool    SendScore( int score );

    virtual bool    GetScores( int limit );

    virtual Claw::NarrowString GetUserId();
    virtual Claw::NarrowString GetUserName();

    //! For PIMPL internal usage - make it public for Objective-C interface.
    void            NotifyAuthenticationChange( bool authenticated );
    void            NotifyFeedPublished( const char* identifier );
    void            NotifyImagePublished( const char* identifier );
    void            NotifyActionPublished( const char* identifier );
    void            NotifyRequestSent( const char* identifier );
    void            NotifyRequestReceived( const RequestData& appRequest );
    void            NotifyRequestRemoved();
    void            NotifyScoreSent();
    void            NotifyScoresReceived( std::vector<ScoreData>& scores );


private:
    //IPhoneFacebookImpl* m_impl;

}; // class IPhoneFacebook
#endif // !defined __NETWORK_IPHONE_FACEBOOK_HPP__
// EOF

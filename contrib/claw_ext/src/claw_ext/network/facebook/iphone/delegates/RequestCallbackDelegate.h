#import "Facebook.h"

#include "claw_ext/network/facebook/Facebook.hpp"

@interface RequestCallbackDelegate : NSObject <FBDialogDelegate>
{
}

- (id) initWithCallback:(FaceBook::RequestCallbackData) callback;

- (void)dialogDidComplete:(FBDialog*) dialog;
- (void)dialogCompleteWithUrl:(NSURL*) url;
- (void)dialogDidNotCompleteWithUrl:(NSURL*) url;
- (void)dialogDidNotComplete:(FBDialog*) dialog;
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError*) error;

@end

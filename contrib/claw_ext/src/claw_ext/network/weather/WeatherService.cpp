#include "claw_ext/network/weather/WeatherService.hpp"
#include "claw_ext/network/weather/WeatherCodes.hpp"

#include "claw/network/HttpRequest.hpp"
#include "claw/network/Network.hpp"

static WeatherService* s_instance = NULL;

WeatherService::WeatherService()
    : m_thread( NULL )
    , m_syncing( false )
{
    // Default weather conditions - nice, sunny day
    m_conditions.weatherCode = WEATHER_CLEAR_SUNNY;
    m_conditions.temperature = 24;
    m_conditions.windSpeed = 0;
    m_conditions.windDirection = 0;
    m_conditions.precipation = 0;
    m_conditions.humidity = 50;
    m_conditions.visibility = 10;
    m_conditions.pressure = 1000;
    m_conditions.cloudCover = 0;
}

WeatherService::~WeatherService()
{
    if ( m_thread )
    {
        m_thread->Kill();

        delete m_thread;
        m_thread = NULL;
    }
}

WeatherService* WeatherService::CreateInstance()
{
    CLAW_ASSERT( s_instance == NULL );
    s_instance = new WeatherService();
    return s_instance;
}

void WeatherService::ReleaseInstance()
{
    CLAW_ASSERT( s_instance );
    delete s_instance;
    s_instance = NULL;
}

WeatherService* WeatherService::GetInstance()
{
    return s_instance;
}

void WeatherService::Sync()
{
    if ( !m_syncing )
    {
        if ( m_thread )
        {
            delete m_thread;
        }

        m_syncing = true;
        m_thread = new Claw::Thread( DoSync, this );
    }
}

int WeatherService::DoSync( void* data )
{
    WeatherService* service = static_cast<WeatherService*>( data );

    Claw::Network::Open();

    Claw::NarrowString ip = service->GetPublicIp();
    if ( !ip.empty() )
    {
        Claw::XmlPtr xml = service->GetWeatherXml( ip );
        if ( xml )
        {
            service->ParseWeatherXml( xml );
        }
    }

    Claw::Network::Close();

    service->m_syncing = false;

    return 0;
}

WeatherConditions WeatherService::GetCurrentConditions()
{
    m_mutex.Enter();
    WeatherConditions conditions = m_conditions;
    m_mutex.Leave();

    return conditions;
}

Claw::NarrowString WeatherService::GetPublicIp()
{
    Claw::NarrowString ip;

    Claw::Uri uri( "http://api.externalip.net/ip/" );
    Claw::HttpRequest request( uri );

    request.Connect();
    if ( !request.CheckError() )
    {
        request.Download();
        if ( !request.CheckError() )
        {
            ip.resize( request.GetLength() );
            memcpy( &ip[0], request.GetData(), request.GetLength() );
        }
    }

    return ip;
}

Claw::XmlPtr WeatherService::GetWeatherXml( const Claw::NarrowString& ip )
{
    Claw::XmlPtr xml;

    Claw::StdOStringStream url;
    url << "http://www.worldweatheronline.com/feed/premium-weather-v2.ashx?key=3c7517bd00120644122507&feedkey=15716f1a52120719122507&format=xml&q=" << ip;

    Claw::Uri uri( url.str() );
    Claw::HttpRequest request( uri );

    request.Connect();
    if ( !request.CheckError() )
    {
        request.Download();
        if ( !request.CheckError() )
        {
            // Claw::Xml takes ownership of data buffer so we need to make a copy of it
            char* buffer = new char[request.GetLength()];
            memcpy( buffer, request.GetData(), request.GetLength() );

            xml.Reset( Claw::Xml::Create( buffer, request.GetLength() ) );
        }
    }

    return xml;
}

void WeatherService::ParseWeatherXml( Claw::Xml* xml )
{
    Claw::XmlIt rootNode( *xml );
    if ( rootNode )
    {
        Claw::XmlIt currentConditionNode = rootNode.Child( "current_condition" );
        if ( currentConditionNode )
        {
            WeatherConditions conditions;

            // See: http://www.worldweatheronline.com/free-weather-feed.aspx?menu=xmldata

            Claw::XmlIt weatherCodeNode = currentConditionNode.Child( "weatherCode" );
            CLAW_VERIFY( weatherCodeNode.GetContent( conditions.weatherCode ) );

            Claw::XmlIt temperatureNode = currentConditionNode.Child( "temp_C" );
            CLAW_VERIFY( temperatureNode.GetContent( conditions.temperature ) );

            Claw::XmlIt windSpeedNode = currentConditionNode.Child( "windspeedKmph" );
            CLAW_VERIFY( windSpeedNode.GetContent( conditions.windSpeed ) );

            Claw::XmlIt windDirectionNode = currentConditionNode.Child( "winddirDegree" );
            CLAW_VERIFY( windDirectionNode.GetContent( conditions.windDirection ) );

            Claw::XmlIt precipationNode = currentConditionNode.Child( "precipMM" );
            CLAW_VERIFY( precipationNode.GetContent( conditions.precipation ) );

            Claw::XmlIt humidityNode = currentConditionNode.Child( "humidity" );
            CLAW_VERIFY( humidityNode.GetContent( conditions.humidity ) );

            Claw::XmlIt visibilityNode = currentConditionNode.Child( "visibility" );
            CLAW_VERIFY( visibilityNode.GetContent( conditions.visibility ) );

            Claw::XmlIt pressureNode = currentConditionNode.Child( "pressure" );
            CLAW_VERIFY( pressureNode.GetContent( conditions.pressure ) );

            Claw::XmlIt cloudCoverNode = currentConditionNode.Child( "cloudcover" );
            CLAW_VERIFY( cloudCoverNode.GetContent( conditions.cloudCover ) );

            m_mutex.Enter();
            m_conditions = conditions;
            m_mutex.Leave();
        }
    }
}

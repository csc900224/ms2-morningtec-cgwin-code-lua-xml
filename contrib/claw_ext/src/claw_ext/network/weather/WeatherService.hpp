#ifndef __INCLUDED_WEATHER_SERVICE_HPP__
#define __INCLUDED_WEATHER_SERVICE_HPP__

#include "claw/base/Mutex.hpp"
#include "claw/base/String.hpp"
#include "claw/base/Thread.hpp"
#include "claw/base/Xml.hpp"

struct WeatherConditions
{
    int weatherCode;
    int temperature; // C
    int windSpeed; // km/h
    int windDirection; // degree
    float precipation; // mm
    int humidity; // %
    int visibility; // km
    int pressure; // milibars
    int cloudCover; // %
};

class WeatherService
{
public:
    void Sync();

    WeatherConditions   GetCurrentConditions();

    //! Singleton like interface (explicit creation)
    static
    WeatherService*     CreateInstance();

    //! Release singleton instance
    static
    void                ReleaseInstance();

    //! Get previously create implementation
    static
    WeatherService*     GetInstance();

protected:
                        WeatherService();
                        ~WeatherService();

private:
    Claw::NarrowString  GetPublicIp();
    Claw::XmlPtr        GetWeatherXml( const Claw::NarrowString& ip );

    void                ParseWeatherXml( Claw::Xml* xml );

    static int          DoSync( void* data );

    WeatherConditions   m_conditions;

    Claw::Thread*       m_thread;
    Claw::Mutex         m_mutex;

    bool                m_syncing;

}; // class WeatherService

#endif // __INCLUDED_WEATHER_SERVICE_HPP__

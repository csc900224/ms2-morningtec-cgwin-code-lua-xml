//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/gamecenter/GameCenter.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_GAME_CENTER_HPP__
#define __NETWORK_GAME_CENTER_HPP__

// Internal includes

// External includes
#include "claw/base/String.hpp"

#include <map>
#include <list>

class GameCenter
{
public:
    enum Type
    {
        Default,
        Dummy,
        GameKit,
        OpenFeint
    };

    // Forward declaration of an internal class
    class Observer;

    //! Message container type
    typedef Claw::NarrowString              Message;

    //! Achievement name
    typedef Claw::NarrowString              AchievementId;

    //! Score category name
    typedef Claw::NarrowString              ScoreId;

    //! Player identifier
    typedef Claw::NarrowString              UserId;

    //! Achievements container
    typedef std::map<AchievementId, float>  Achievements;

    //! Crediterials types used for authentication
    enum AuthenticationCrediterials
    {
        AC_APP_NAME,
        AC_APP_ID,
        AC_PRODUCT_KEY,
        AC_PRODUCT_SECRET
    };

    //! Crediterials container
    typedef std::map<AuthenticationCrediterials, Claw::NarrowString> Crediterials;


    //! Virtual destructor for derived classes.
    virtual             ~GameCenter()       {};

    //! Register game center observer.
    /**
     * \return false if observer was already registered, true if successfully registered.
     */
    bool                RegisterObserver( Observer* observer );

    //! Unregister game center observer.
    /**
     * \return false if observer was not registered to this game center.
     */
    bool                UnregisterObserver( Observer* observer );

    //! Try to authenticate local player
    /**
    * The result of authentication is send via OnAuthenticationChange() method to GameCenter::Observer
    * objects registered in the game center.
    * \param crediterials data used to authenticate application. Not all platforms require such information
    * e.g. iOS does not require any information, but Android does.
    */
    virtual bool        Authenticate( const Crediterials* crediterials = NULL ) = 0;

    // Depreciated for public - will be moved to protected section just for internal use
    virtual bool        SubmitScore( const char* category, int score ) = 0;
    // Depreciated method gonna be protected - for internal use
    virtual bool        SubmitAchievement( const char* achievement, float progress = 100.0f ) = 0;
    // Depreciated method gonna be protected - for internal use
    virtual bool        ShowLeaderboard( const char* category = NULL ) = 0;

    virtual bool        ShowAchievements() = 0;

    inline bool         IsAuthenticated() const;

    inline bool         SubmitScore( const ScoreId& category, int score );

    inline bool         SubmitAchievement( const AchievementId& achievement, float progress = 100.0f );

    inline bool         ShowLeaderboard( const ScoreId& category = "" );

    //static bool IsGameCenterAvailable();

    //! Retreive platfrom dependend implementation.
    /**
    * This method should be implemented and linked once in platform dependend object,
    * returning appropriate GameCenter object implementation.
    */
    static GameCenter*  QueryInterface( Type type = Default );

    //! Release platform specific implementation.
    /**
    * Call destructors, release memory, make cleanup.
    */
    static void         Release( GameCenter* gc );

    //! Internal observer class.
    class Observer
    {
    public:
        //! Called when achievements are succesfully loaded.
        virtual void    OnAchievementsLoad( const Achievements& achievements ) = 0;

        //! Called in response to change in player authentication.
        virtual void    OnAuthenticationChange( bool authenticated ) = 0;

        //! Called after opening/closing leaderboard view.
        virtual void    OnLeaderboardView( bool opened ) = 0;

        //! Called as a result of achievements pane opening/closing.
        virtual void    OnAchievementsView( bool opened ) = 0;

    }; // class Observer

    //! Internal class for storing achievements
    class Achievement
    {
    public:
        typedef AchievementId       Id;
        typedef float               Value;

        Achievement( const Id& id )
            : m_id( id )
            , m_val( 100 )      // default progress - achievement fullfiled
        {}

        Achievement( const Id& id, const Value& v )
            : m_id( id )
            , m_val( v )
        {}

        inline Id       GetId() const       { return m_id; }
        inline Value    GetValue() const    { return m_val; }

    private:
        const Id        m_id;
        const Value     m_val;
    }; // Achievement

    //! Class for storing user score
    class Score
    {
    public:
        typedef ScoreId             Id;
        typedef int                 Value;

        Score( const Value& val )
            : m_id( "Highscore" )   // default score name
            , m_val ( val )
        {}

        Score( const Id& id, const Value& val )
            : m_id( id )
            , m_val( val )
        {}

        inline Id       GetId() const       { return m_id; }
        inline Value    GetValue() const    { return m_val; }

    private:
        const Id        m_id;
        const Value     m_val;
    }; // Score

protected:
    //! Do not allow public construction
                        GameCenter();

    void                NotifyAchievementsLoad( const Achievements& achievements );

    void                NotifyAuthenticationChange( bool authenticated );

    void                NotifyLeaderboardView( bool opened );

    void                NotifyAchievementsView( bool opened );

    typedef std::list<Observer*>        Observers;
    typedef Observers::iterator         ObserversIt;
    typedef Observers::const_iterator   ObserversConstIt;

    // Used internally to store offline data and link it to current players
    virtual bool        GetUserId( UserId& outUserId ) const = 0;

    Observers           m_observers;
    bool                m_authenticated;

}; // class GameCenter

inline bool GameCenter::IsAuthenticated() const
{
    return m_authenticated;
}

inline bool GameCenter::SubmitScore( const ScoreId& id, int score )
{
    return SubmitScore( id.c_str(), score );
}

inline bool GameCenter::SubmitAchievement( const AchievementId& id, float progress /* = 100.0f */ )
{
    return SubmitAchievement( id.c_str(), progress );
}

inline bool GameCenter::ShowLeaderboard( const ScoreId& category /* = "" */ )
{
    return ShowLeaderboard( category.length() == 0 ? NULL : category.c_str() );
}

#endif // __NETWORK_GAME_CENTER_HPP__
// EOF

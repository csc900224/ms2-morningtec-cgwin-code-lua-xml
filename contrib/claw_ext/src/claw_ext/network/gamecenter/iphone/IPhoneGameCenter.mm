#include "claw_ext/network/gamecenter/GameCenter.hpp"
#include "claw_ext/network/gamecenter/iphone/gamekit/GameKitGameCenter.hpp"

GameCenter* GameCenter::QueryInterface( GameCenter::Type type )
{
    switch ( type )
    {
        case GameCenter::Default:
        case GameCenter::GameKit:
            return new GameKitGameCenter();

        default:
            return NULL;
    }
}

void GameCenter::Release( GameCenter* instance )
{
    delete instance;
}

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/iphone/GameKitGameCenterImpl.mm
//
//  AUTHOR(S):
//      JakubWegrzyn      <jakub.wegrzyn@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#import "GameKitGameCenterImpl.h"

#import <sys/types.h>   // For checking availability
#import <sys/sysctl.h>

#include "claw_ext/network/gamecenter/iphone/gamekit/GameKitGameCenter.hpp"
#include "claw_ext/network/gamecenter/GameCenter.hpp"

// External includes
#include "claw/application/iphone/IPhoneAppDelegate.h"
#include "claw/base/Errors.hpp"

// Contst
static const int GAME_CENTER_DISABLED_TAG       = 23;
static const int GAME_CENTER_UNAVAILABLE_TAG    = 24;

// Forward declare
CGFloat GetAffineRotation( UIInterfaceOrientation interfaceOrientation );

@implementation GameKitGameCenterImpl

- (id) initWithObserver:(GameKitGameCenter*) obs
{
    if( ( self = [super init] ) )
    {
        m_authenticated = NO;
        m_authentication = NO;
        m_observer = obs;
        m_leaderboardsView = nil;
        m_achievementsView = nil;
        m_available = [self isAvailable];
        m_supportOrientationChange = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;

        // Register for orientation changes - only on iPad, other rotations are supported internally by Claw
        if( m_supportOrientationChange )
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
        }

        if( m_available )
        {
            // Listen for authentication change events
            NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
            [nc addObserver:self
                   selector:@selector(onAuthenticationChange)
                       name:GKPlayerAuthenticationDidChangeNotificationName
                     object:nil];
        }
    }
    return self;
}

- (void) dealloc
{
    if( m_supportOrientationChange || m_available )
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [super dealloc];
}

- (void) orientationDidChange:(NSNotification*) notification
{
    // Apply orientation change to any view that was opened
    if( m_achievementsView || m_leaderboardsView )
    {
        [UIView beginAnimations: nil context: NULL];
        [UIView setAnimationDuration: 0.2];
        CGAffineTransform r = CGAffineTransformMakeRotation( GetAffineRotation( [UIApplication sharedApplication].statusBarOrientation ) );
        if( m_leaderboardsView )
        {
            m_leaderboardsView.view.transform = r;
        }
        else if( m_achievementsView )
        {
            m_achievementsView.view.transform = r;
        }
        [UIView commitAnimations];
    }
}

- (bool) authenticate
{
    CLAW_MSG( "[GameKitGameCenterImpl] authenticate" );

    if( !m_available )
    {
        CLAW_MSG("[GameKitGameCenterImpl] Game center not available on this platform" );
        return false;
    }

    if( m_authenticated )
    {
        CLAW_MSG_ASSERT(false, "[GameKitGameCenterImpl] Player already authenticated" );
        return false;
    }

    if( m_authentication )
    {
        CLAW_MSG_WARNING(false, "[GameKitGameCenterImpl] authenticate called during authentication processing" );
        return false;
    }
    m_authentication = YES;

    // Authenticate local player
    [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error) 
     {
         CLAW_MSG( "[GameKitGameCenterImpl] authenticateWithCompletionHandler:" );

         if ( error == nil )
         {
             // Insert code here to handle a successful authentication.
             CLAW_MSG( "[GameKitGameCenterImpl] Player authenticated successfully" );
         }
         else
         {
             // Your application can process the error parameter to report the error to the player.
             CLAW_MSG( "[GameKitGameCenterImpl] Player couldn't be authenticated" );

             //[self showErrorNotification:error];
         }
         m_authentication = NO;
     }];

    return true;
}

- (void) onAuthenticationChange
{
    CLAW_MSG( "[GameKitGameCenterImpl] onAuthenticationChange" );

    m_authenticated = [GKLocalPlayer localPlayer].isAuthenticated;

    if ( m_authenticated )
    {
        // Authentication successful. Update local achievements to prevent depreciated submissions.
        CLAW_MSG( "[GameKitGameCenterImpl] Player authenticated, load achievements." );

        [self loadAchievements];
    }
    else
    {
        // Insert code here to clean up any outstanding Game Center-related classes.
        CLAW_MSG( "[GameKitGameCenterImpl] Player authentication failed." );
    }
    m_observer->NotifyAuthenticationChange( m_authenticated );
}

- (void) loadAchievements
{
    CLAW_MSG( "[GameKitGameCenterImpl] loadAchievements" );

    if( !m_available )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] loadAchievements(): Game center not available on this platform" );
        return;
    }

    if( !m_authenticated )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] loadAchievements(): Couldn't load, not authenticated" );
        return;
    }

    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray* achievements, NSError* error)
     {
         CLAW_MSG( "[GameKitGameCenterImpl] loadAchievementsWithCompletionHandler:" );

         if ( achievements != nil )
         {
             [self onLoadAchievements:achievements];
         }
     }];
}

- (void) onLoadAchievements:(NSArray*) achievements
{
    GameCenter::Achievements temp;
    for ( GKAchievement* achievement in achievements )
    {
        std::pair<Claw::NarrowString, float> item =
        std::pair<Claw::NarrowString, float>(
                                             [achievement.identifier UTF8String],
                                             achievement.percentComplete
                                             );

        temp.insert( item );

        CLAW_MSG( "[GameKitGameCenterImpl] " << item.first << " - " << (int)item.second );
    }

    CLAW_ASSERT( m_observer );
    m_observer->NotifyAchievementsLoad( temp );
}

- (bool) submitScore:(int64_t) score forCategory:(NSString*) category
{
    CLAW_MSG( "[GameKitGameCenterImpl] submitScore(): " << (int)score );

    if( !m_available )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Game center not available on this platform" );
        return false;
    }

    if ( !m_authenticated )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Player is not authenticated!" );
        return false;
    }

    GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
    scoreReporter.value = score;

    [scoreReporter reportScoreWithCompletionHandler:^(NSError* error)
     {
         CLAW_MSG( "[GameKitGameCenterImpl] reportScoreWithCompletionHandler:" );

         if ( error != nil )
         {
             CLAW_MSG( "[GameKitGameCenterImpl] Score submission error" );

             [self showErrorNotification:error];
         }
         else
         {
             CLAW_MSG( "[GameKitGameCenterImpl] Score submitted successfully" );
         }

     }];
    return true;
}

- (bool) submitAchievement:(NSString*) identifier withProgress:(float)progress
{
    CLAW_MSG( "[GameKitGameCenterImpl] submitAchievement: withProgress:" );

    if( !m_available )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Game center not available on this platform" );
        return false;
    }

    if ( !m_authenticated )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Couldn't submitAchievement - player is not authenticated!" );
        return false;
    }

    GKAchievement* achievement = [[[GKAchievement alloc] initWithIdentifier:identifier] autorelease];
    achievement.percentComplete = progress;

    [achievement reportAchievementWithCompletionHandler:^(NSError* error)
     {
         CLAW_MSG( "[GameKitGameCenterImpl] reportAchievementWithCompletionHandler:" );

         if ( error != nil )
         {
             CLAW_MSG( "[GameKitGameCenterImpl] achievement reporting error" );

             [self showErrorNotification:error];
         }
         else
         {
             CLAW_MSG( "[GameKitGameCenterImpl] achievement reported successfully" );
         }
     }];
    return true;
}

- (bool) showLeaderboard:(NSString*) category
{
    CLAW_MSG( "[GameKitGameCenterImpl] showLeaderboard" );

    if( !m_available )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Game center not available on this platform" );
        [self showUnavailableAlert];
        return false;
    }

    if ( !m_authenticated )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Couldn't showLeaderboard, player is not authenticated!" );
        [self showDisabledAlert];
        return false;
    }

    m_leaderboardsView = [[OrientedLeaderboardViewController alloc] init];
    if ( m_leaderboardsView != nil )
    {
        m_leaderboardsView.leaderboardDelegate = self;
        if( category != nil )
        {
            m_leaderboardsView.category = category;
        }

        // Get main view controller of Claw application
        IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
        IPhoneViewController* appViewController = [appDelegate getViewController];

        [appViewController presentModalViewController:m_leaderboardsView animated:YES];

        if( m_supportOrientationChange )
        {
            m_leaderboardsView.view.transform =
            CGAffineTransformMakeRotation( GetAffineRotation( [UIApplication sharedApplication].statusBarOrientation ) );
        }

        m_observer->NotifyLeaderboardView( true );
        return true;
    }
    return false;
}

- (void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController*) viewController
{
    CLAW_MSG( "[GameKitGameCenterImpl] leaderboardViewControllerDidFinish:" );

    // Get main view controller of Claw application
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* appViewController = [appDelegate getViewController];

    [appViewController dismissModalViewControllerAnimated:YES];
    [viewController release];

    m_leaderboardsView = nil;
    m_observer->NotifyLeaderboardView( false );
}

- (bool) showAchievements
{
    CLAW_MSG( "[GameKitGameCenterImpl] showAchievements" );

    if( !m_available )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Game center not available on this platform" );
        // TODO: Consider calling some application notifier like:
        // m_observer->NotifyAlertView( true );
        // that new view has been displayed and removed after alert confirmation
        [self showUnavailableAlert];
        return false;
    }

    if ( !m_authenticated )
    {
        CLAW_MSG( "[GameKitGameCenterImpl] Couldn't showAchievements, player is not authenticated!" );
        [self showDisabledAlert];
        return false;
    }

    m_achievementsView = [[OrientedAchievementViewController alloc] init];

    if ( m_achievementsView != nil )
    {
        m_achievementsView.achievementDelegate = self;

        // Get main view controller of Claw application
        IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
        IPhoneViewController* appViewController = [appDelegate getViewController];

        [appViewController presentModalViewController:m_achievementsView animated:YES];

        if( m_supportOrientationChange )
        {
            m_achievementsView.view.transform =
            CGAffineTransformMakeRotation( GetAffineRotation( [UIApplication sharedApplication].statusBarOrientation ) );
        }

        m_observer->NotifyAchievementsView( true );
        return true;
    }
    return false;
}

- (void) achievementViewControllerDidFinish:(GKAchievementViewController*) viewController
{
    CLAW_MSG( "[GameKitGameCenterImpl] achievementViewControllerDidFinish:" );

    // Get main view controller of Claw application
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* appViewController = [appDelegate getViewController];

    [appViewController dismissModalViewControllerAnimated:YES];
    [viewController release];

    m_achievementsView = nil;
    m_observer->NotifyAchievementsView( false );
}

- (void) showErrorNotification:(NSError*) error
{
    UIAlertView* popup = [[UIAlertView alloc]
                          initWithTitle:error.localizedDescription
                          message:error.localizedFailureReason
                          delegate:self
                          cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")
                          otherButtonTitles:nil];
    [popup show];
    [popup release];
}

- (NSString*) getUserId
{
    return [GKLocalPlayer localPlayer].playerID;
}

- (bool) isAvailable
{
    static bool available = false;
    static bool checked = false;

    if ( !checked )
    {
        // Check for presence of GKLocalPlayer API.
        Class gameCenterClass = NSClassFromString(@"GKLocalPlayer");

        // The device must be running running iOS 4.1 or later.
        NSString* systemVersion = @"4.1";
        NSString* currentSystemVersion = [[UIDevice currentDevice] systemVersion];
        BOOL systemVersionSupported = ( [currentSystemVersion compare:systemVersion options:NSNumericSearch] != NSOrderedAscending );

        // Device must be at least iPhone 3GS or iPod Touch 2G
        bool validDevice = true;
        {
            size_t size;
            if ( sysctlbyname( "hw.machine", NULL, &size, NULL, 0 ) != -1 )
            {
                char* name = (char*)malloc( size );
                if ( sysctlbyname( "hw.machine", name, &size, NULL, 0 ) != -1 )
                {
                    if ( !strcmp( name, "iPhone1,1" ) || !strcmp( name, "iPhone1,2" ) || !strcmp( name, "iPod1,1" ) )
                    {
                        validDevice = false;
                    }
                }

                free( name );
            }
        }

        available = ( gameCenterClass && systemVersionSupported && validDevice );
        checked = true;
    }

    return available;
}

- (bool) isAuthenticated
{
    if( m_available )
    {
        return m_authenticated;
    }
    return false;
}

- (void) showDisabledAlert
{
    UIAlertView* alert = [[[UIAlertView alloc] initWithTitle: @"Game Center Disabled"
                                                     message: @"Sign in with the Game Center application to enable"
                                                    delegate: self
                                           cancelButtonTitle: @"Cancel"
                                           otherButtonTitles: @"Sign In", nil] autorelease];

    [alert setTag:GAME_CENTER_DISABLED_TAG];
    [alert show];
}

- (void) showUnavailableAlert
{
    UIAlertView* alert = [[[UIAlertView alloc] initWithTitle: @"Game Center Unavailable"
                                                     message: @"Your system does not support Game Center"
                                                    delegate: nil
                                           cancelButtonTitle: @"OK"
                                           otherButtonTitles: nil] autorelease];

    [alert setTag:GAME_CENTER_UNAVAILABLE_TAG];
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( alertView.tag == GAME_CENTER_DISABLED_TAG )
    {
        NSString* title = [alertView buttonTitleAtIndex:buttonIndex];
        if ( [title isEqualToString:@"Sign In"] )
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"gamecenter:"]];
        }
    }
}

@end

/////////////////////////////////////////////////////////////////////////////////
// Extensions to GameCenter Leaderboard/ArchievementsViewController to support
// orientataion changes
/////////////////////////////////////////////////////////////////////////////////

@implementation OrientedAchievementViewController

- (BOOL) shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(UIInterfaceOrientation)interfaceOrientation
{
    return [UIApplication sharedApplication].statusBarOrientation;
}

@end

@implementation OrientedLeaderboardViewController

- (BOOL) shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(UIInterfaceOrientation)interfaceOrientation
{
    return [UIApplication sharedApplication].statusBarOrientation;
}

@end

CGFloat GetAffineRotation( UIInterfaceOrientation interfaceOrientation )
{
    switch( interfaceOrientation )
    {
        case UIInterfaceOrientationPortrait: return 0;
        case UIInterfaceOrientationLandscapeLeft: return M_PI * 1.5;
        case UIInterfaceOrientationPortraitUpsideDown: return M_PI;
        case UIInterfaceOrientationLandscapeRight: return M_PI * 0.5;

        default:
            return 0;
    }
}

// EOF

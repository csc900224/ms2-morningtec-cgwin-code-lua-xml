//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/gamecenter/win32/DummyGameCenter.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_WIN32_GAME_CENTER_HPP__
#define __NETWORK_WIN32_GAME_CENTER_HPP__

// Internal includes
#include "claw_ext/network/gamecenter/GameCenter.hpp"

class DummyGameCenter : public GameCenter
{
public:
    virtual bool        Authenticate( const Crediterials* crediterials = NULL );

    virtual bool        SubmitScore( const char* category, int score );

    virtual bool        SubmitAchievement( const char* achievement, float progress = 100.0f );

    virtual bool        ShowLeaderboard( const char* category = NULL );

    virtual bool        ShowAchievements();

protected:
    virtual bool        GetUserId( UserId& outUserId ) const;

}; // class DummyGameCenter

#endif
// EOF

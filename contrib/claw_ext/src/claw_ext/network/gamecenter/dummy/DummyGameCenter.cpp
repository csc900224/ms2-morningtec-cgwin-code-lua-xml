//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/monetization/gamecenter/win32/DummyGameCenter.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/gamecenter/dummy/DummyGameCenter.hpp"
#include "claw/base/Errors.hpp"

static DummyGameCenter* s_instance = NULL;

GameCenter* GameCenter::QueryInterface( GameCenter::Type type )
{
    // Force singleton pattern
    CLAW_ASSERT( s_instance == NULL );

    switch ( type )
    {
        case GameCenter::Default:
        case GameCenter::Dummy:
            s_instance = new DummyGameCenter;
            break;

        default:
            CLAW_MSG_ASSERT( false, "Unsupported type" );
            break;
    }

    return s_instance;
}

void GameCenter::Release( GameCenter* instance )
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;
    delete instance;
}

bool DummyGameCenter::Authenticate( const Crediterials* crediterials )
{
    return true;
}

bool DummyGameCenter::ShowLeaderboard( const char* category )
{
    return false;
}

bool DummyGameCenter::ShowAchievements()
{
    return false;
}

bool DummyGameCenter::SubmitScore( const char* category, int score /* Message* reason = 0 */ )
{
    return false;
}

bool DummyGameCenter::SubmitAchievement( const char* achievement, float progress )
{
    return false;
}

bool DummyGameCenter::GetUserId( UserId& outUserId ) const
{
    outUserId = "DummyGameCenterUser";
    return true;
}
// EOF

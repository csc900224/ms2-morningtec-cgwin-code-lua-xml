#include "claw_ext/network/UrlEncode.hpp"
#include <cstdio>

// Source:
// http://codepad.org/lCypTglt
//

//RFC 3986 section 2.3 Unreserved Characters (January 2005)
static const Claw::NarrowString UNRESERVED = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~";

Claw::NarrowString UrlEncode( const Claw::NarrowString& s )
{
    Claw::NarrowString escaped = "";
    for ( size_t i = 0; i < s.length(); i++ )
    {
        if ( UNRESERVED.find_first_of( s[i] ) != Claw::NarrowString::npos )
        {
            escaped.push_back( s[i] );
        }
        else
        {
            escaped.append( "%" );
            char buffer[3];
            sprintf( buffer, "%.2X", s[i] );
            escaped.append( buffer );
        }
    }

    return escaped;
}

Claw::NarrowString Sanitize( const Claw::NarrowString& s )
{
    Claw::NarrowString sanitized = "";
    for ( size_t i = 0; i < s.length(); i++ )
    {
        if ( UNRESERVED.find_first_of( s[i] ) != Claw::NarrowString::npos )
        {
            sanitized.push_back( s[i] );
        }
    }
    return sanitized;
}


#ifndef __INCLUDED__ANALYTICS_HPP__
#define __INCLUDED__ANALYTICS_HPP__

#include <list>
#include <map>

#include "claw/base/String.hpp"

class Analytics
{
public:
    enum AnalyticsSystem
    {
        AS_FLURRY = 0,
        AS_GOOGLE,
        AS_GAME_ANALYTICS,

        AS_NUM,
        AS_DEFAULT = AS_FLURRY
    };

    typedef std::pair<Claw::NarrowString, Claw::NarrowString> EventParam;
    typedef std::list<EventParam> EventParamList;
    typedef std::map<Claw::NarrowString, Claw::NarrowString> ParamsMap;

    virtual      ~Analytics() {}
    virtual void Initialize( const char* key, const char* secret, ParamsMap* params = NULL ) = 0;
    virtual void OnFocusChange( bool hasFocus ) = 0;

    virtual void StartSession( const char* key = NULL ) = 0;
    virtual void StopSession() = 0;

    virtual void LogEvent( const char* ev, const char* param = NULL, const char* value = NULL ) = 0;
    virtual void LogEvent( const char* ev, const EventParamList& params ) = 0;

    virtual void LogEventCategory( const char* category, const char* ev, const char* param = NULL, const char* value = NULL ) = 0;
    virtual void LogEventCategory( const char* category, const char* ev, const EventParamList& params ) = 0;

    virtual void StartEvent( const char* ev ) = 0;
    virtual void StopEvent( const char* ev ) = 0;

    //! Retreive platfrom dependend implementation.
    /**
    * This method should be implemented and linked once in platform dependend object,
    * returning appropriate Analytics object implementation.
    */
    static Analytics* QueryInterface( AnalyticsSystem system = AS_DEFAULT );

    //! Release platform specific implementation.
    /**
    * Call destructors, release memory, make cleanup.
    */
    static void Release( AnalyticsSystem system = AS_DEFAULT);
};

#endif // __INCLUDED__ANALYTICS_HPP__

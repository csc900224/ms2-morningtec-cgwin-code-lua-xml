package com.gamelion.analytics;
	
import android.util.Log;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.Claw.Android.ClawActivityCommon;

public class GoogleAnalytics
{
    public static final String TAG = "GoogleAnalytics";
    public static final boolean DEBUG = false;
    
    public static void startSession(final String key)
    {
        if (DEBUG) Log.i(TAG, "startSession() with key: " + key);
		Thread t = new Thread(new Runnable() {
		@Override
			public void run() {
				GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
				tracker.startNewSession(key, ClawActivityCommon.mActivity);
				tracker.trackPageView("/started");
				// this call does networking and could take a while
				tracker.dispatch();
			}
		}, TAG);
		t.setPriority(Thread.MIN_PRIORITY);
		t.setDaemon(true);
		t.start();
    }

    public static void logEvent(final String event)
    {
        if (DEBUG) Log.i(TAG, "logEvent(): " + event);
		Thread t = new Thread(new Runnable() {
		@Override
			public void run() {
				GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
				tracker.trackEvent("InGameEvent", event, "", 0);
				// this call does networking and could take a while
				tracker.dispatch();
			}
		}, TAG);
		t.setPriority(Thread.MIN_PRIORITY);
		t.setDaemon(true);
		t.start();
    }
	
	public static void stopSession()
	{
		if (DEBUG) Log.i(TAG, "stopSession()");
		GoogleAnalyticsTracker.getInstance().stopSession();
	}
}
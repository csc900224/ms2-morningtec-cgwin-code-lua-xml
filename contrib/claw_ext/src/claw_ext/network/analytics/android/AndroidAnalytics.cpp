//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/analytics/android/AndroidAnalytics.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include "claw/system/android/JniAttach.hpp"
#include "claw/base/Errors.hpp"
#include "claw/base/String.hpp"

#include "claw_ext/network/analytics/android/AndroidAnalytics.hpp"

#include <cstdlib>

static AndroidAnalytics* s_instance[Analytics::AS_NUM] = { NULL };

/*static*/ Analytics* Analytics::QueryInterface( AnalyticsSystem system )
{
    CLAW_ASSERT( system >= 0 && system < AS_NUM );
    if( !s_instance[system] )
        s_instance[system] = new AndroidAnalytics(system);

    CLAW_ASSERT( s_instance[system] );
    return s_instance[system];
}

/*static*/ void Analytics::Release( AnalyticsSystem system )
{
    CLAW_ASSERT( system >= 0 && system < AS_NUM );
    CLAW_ASSERT( s_instance[system] );
    delete s_instance[system];
    s_instance[system] = NULL;
}

AndroidAnalytics::AndroidAnalytics( AnalyticsSystem system )
    : m_system( system )
{}

void AndroidAnalytics::Initialize( const char* key, const char* secret, ParamsMap* params )
{
    CLAW_MSG( "AndroidAnalytics::Initialize()" );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );

    jstring keyStr;
    jstring secretStr;

    CLAW_ASSERT( key && secret );
    if( key && secret )
    {
        keyStr = Claw::JniAttach::GetStringFromChars( env, key );
        secretStr = Claw::JniAttach::GetStringFromChars( env, secret );
    }

    if( m_system == AS_GAME_ANALYTICS )
    {
        int sessionDuration = 0;
        jstring build;

        if( params )
        {
            ParamsMap::iterator it = params->find( "build" );
            if( it != params->end() )
            {
                build = Claw::JniAttach::GetStringFromChars( env, it->second.c_str() );
            }
            else
            {
                build = Claw::JniAttach::GetStringFromChars( env, "" );
            }

            it = params->find( "session-timeout" );
            if( it != params->end() )
            {
                sessionDuration = atoi( it->second.c_str() );
            }
        }
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GameAnalytics", "initialize", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V", keyStr, secretStr, build, sessionDuration );
        Claw::JniAttach::Detach( build );
    }
    else
        CLAW_MSG_ASSERT( false, "AndroidAnalytics::Initialize() not impleneted!" );

    Claw::JniAttach::ReleaseString( env, keyStr );
    Claw::JniAttach::ReleaseString( env, secretStr );
    Claw::JniAttach::Detach( attached );
}

void AndroidAnalytics::OnFocusChange( bool hasFocus )
{
    if( m_system == AS_GAME_ANALYTICS || 
        m_system == AS_FLURRY )
    {
        if( hasFocus )
        {
            StartSession();
        }
        else
        {
            StopSession();
        }
    }
}

void AndroidAnalytics::StartSession( const char* analyticsKey )
{
    CLAW_MSG( "AndroidAnalytics::StartSession()" );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );

    if( !analyticsKey ) analyticsKey = "";
    jstring key = Claw::JniAttach::GetStringFromChars( env, analyticsKey );

    if( m_system == AS_FLURRY )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "startSession", "(Ljava/lang/String;)V", key );
    else if( m_system == AS_GOOGLE )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GoogleAnalytics", "startSession", "(Ljava/lang/String;)V", key );
    else if( m_system == AS_GAME_ANALYTICS )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GameAnalytics", "startSession", "(Ljava/lang/String;)V", key );
    else
        CLAW_MSG_ASSERT( false, "AndroidAnalytics::StartSession() not impleneted!" );

    Claw::JniAttach::ReleaseString( env, key );
    Claw::JniAttach::Detach( attached );
}

void AndroidAnalytics::StopSession()
{
    CLAW_MSG( "AndroidAnalytics::StopSession()" );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );

    if( m_system == AS_FLURRY )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "stopSession", "()V" );
    else if( m_system == AS_GOOGLE )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GoogleAnalytics", "stopSession", "()V" );
    else if( m_system == AS_GAME_ANALYTICS )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GameAnalytics", "stopSession", "()V" );
    else
        CLAW_MSG_ASSERT( false, "AndroidAnalytics::StopSession() for system not impleneted!" );

    Claw::JniAttach::Detach( attached );
}

void AndroidAnalytics::LogEvent( const char* ev, const char* param, const char* value )
{
    CLAW_MSG( "AndroidAnalytics::LogEvent() " << ev );
    if( param || value )
    {
        if( m_system == AS_GAME_ANALYTICS )
        {
            LogEventCategory( "", ev, param, value );
        }
        else
        {
            if( !param ) param = "";
            if( !value ) value = "";

            EventParamList params;
            params.push_back( EventParam( param, value ) );
            LogEvent( ev, params );
        }
    }
    else
    {
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        CLAW_ASSERT( ev );
        if( !ev ) ev = "";
        jstring eventStr = Claw::JniAttach::GetStringFromChars( env, ev );

        if( m_system == AS_FLURRY )
            Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "logEvent", "(Ljava/lang/String;)V", eventStr );
        else if( m_system == AS_GOOGLE )
            Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GoogleAnalytics", "logEvent", "(Ljava/lang/String;)V", eventStr );
        else if( m_system == AS_GAME_ANALYTICS )
            Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/GameAnalytics", "logEvent", "(Ljava/lang/String;)V", eventStr );
        else
            CLAW_MSG_ASSERT( false, "AndroidAnalytics::LogEvent() for system not impleneted!" );

        Claw::JniAttach::ReleaseString( env, eventStr );
        Claw::JniAttach::Detach( attached );
    }
}

void AndroidAnalytics::LogEvent( const char* ev, const EventParamList& params )
{
    CLAW_MSG( "AndroidAnalytics::LogEvent(params) " << ev );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );

    CLAW_ASSERT( ev );
    if( !ev ) ev = "";
    jstring eventStr = Claw::JniAttach::GetStringFromChars( env, ev );

    if( m_system == AS_FLURRY )
    {
        EventParamList::const_iterator it = params.begin();
        EventParamList::const_iterator end = params.end();
        for( ; it != end; ++it )
        {
            jstring param = Claw::JniAttach::GetStringFromChars( env, it->first.c_str() );
            jstring value = Claw::JniAttach::GetStringFromChars( env, it->second.c_str() );

            Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "pushEventParams", "(Ljava/lang/String;Ljava/lang/String;)V", param, value );

            Claw::JniAttach::ReleaseString( env, param );
            Claw::JniAttach::ReleaseString( env, value );
        }
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "logEventWithParams", "(Ljava/lang/String;)V", eventStr );
    }
    else
    {
        CLAW_MSG_ASSERT( false, "AndroidAnalytics::LogEvent() for system not impleneted!" );
    }

    Claw::JniAttach::ReleaseString( env, eventStr );
    Claw::JniAttach::Detach( attached );
}

void AndroidAnalytics::LogEventCategory( const char* category, const char* ev, const char* param, const char* value )
{
    CLAW_MSG( "AndroidAnalytics::LogEventCategory() " << category << "|" << ev );

    if( m_system == AS_GAME_ANALYTICS )
    {
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        CLAW_ASSERT( category );
        if( !category ) category = "";
        CLAW_ASSERT( ev );
        if( !ev ) ev = "";
        if( !param ) param = "";
        if( !value ) value = "";

        jstring catStr = Claw::JniAttach::GetStringFromChars( env, category );
        jstring eventStr = Claw::JniAttach::GetStringFromChars( env, ev );
        jstring paramStr = Claw::JniAttach::GetStringFromChars( env, param );
        jstring valueStr = Claw::JniAttach::GetStringFromChars( env, value );

        Claw::JniAttach::StaticVoidMethodCall( 
            env, 
            "com/gamelion/analytics/GameAnalytics",
            "logEvent",
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
            catStr, 
            eventStr,
            paramStr,
            valueStr 
        );

        Claw::JniAttach::ReleaseString( env, catStr );
        Claw::JniAttach::ReleaseString( env, eventStr );
        Claw::JniAttach::ReleaseString( env, paramStr );
        Claw::JniAttach::ReleaseString( env, valueStr );
        Claw::JniAttach::Detach( attached );
    }
    else
    {
        Claw::NarrowString eventName = category;
        eventName.append( ":" );
        eventName.append( ev );
        LogEvent( eventName.c_str(), param, value );
    }
}

void AndroidAnalytics::LogEventCategory( const char* category, const char* ev, const EventParamList& params )
{
    CLAW_MSG( "AndroidAnalytics::LogEventCategory(params) " << category << "|" << ev );
    
    if( m_system == AS_GAME_ANALYTICS )
    {
        EventParamList::const_iterator it = params.begin();
        EventParamList::const_iterator end = params.end();
        for( ; it != end; ++it )
        {
            LogEventCategory( category, ev, it->first.c_str(), it->second.c_str() );
        }
    }
    else
    {
        Claw::NarrowString eventName = category;
        eventName.append( ":" );
        eventName.append( ev );
        LogEvent( eventName.c_str(), params );
    }
}

void AndroidAnalytics::StartEvent( const char* ev )
{
    CLAW_MSG( "AndroidAnalytics::StartEvent() " << ev );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );

    CLAW_ASSERT( ev );
    if( !ev ) ev = "";
    jstring eventStr = Claw::JniAttach::GetStringFromChars( env, ev );

    if( m_system == AS_FLURRY )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "startEvent", "(Ljava/lang/String;)V", eventStr );
    else
        CLAW_MSG_ASSERT( false, "AndroidAnalytics::StartEvent() for system not impleneted!" );

    Claw::JniAttach::ReleaseString( env, eventStr );
    Claw::JniAttach::Detach( attached );
}

void AndroidAnalytics::StopEvent( const char* ev )
{
    CLAW_MSG( "AndroidAnalytics::StopEvent() " << ev );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );

    CLAW_ASSERT( ev );
    if( !ev ) ev = "";
    jstring eventStr = Claw::JniAttach::GetStringFromChars( env, ev );

    if( m_system == AS_FLURRY )
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/analytics/FlurryAnalytics", "stopEvent", "(Ljava/lang/String;)V", eventStr );
    else
        CLAW_MSG_ASSERT( false, "AndroidAnalytics::StopEvent() for system not impleneted!" );

    Claw::JniAttach::ReleaseString( env, eventStr );
    Claw::JniAttach::Detach( attached );
}

package com.gamelion.analytics;

import android.util.Log;
import com.Claw.Android.ClawActivityCommon;

public class GameAnalytics
{
    private static final String TAG = "GameAnalytics";
    private static final boolean DEBUG = false;

    private static final String CATEGORY_BUSINESS = "business";
    private static final String CATEGORY_QUALITY = "quality";
    private static final String CATEGORY_DESIGN = "design";
    private static boolean s_sessionActive = false;
    
    public static void startSession(String key)
    {
        if( !s_sessionActive )
        {
            if (DEBUG) Log.i(TAG, "startSession()"); 
            com.gameanalytics.android.GameAnalytics.startSession(ClawActivityCommon.mActivity);
            s_sessionActive = true;
        }
    }
    
    public static void initialize(String key, String secret, String build, int sessionTimeout)
    {
        if (DEBUG) Log.i(TAG, "initialize() key: " + key + " secret: " + secret);
        
        if( build.equals("") ) { 
            com.gameanalytics.android.GameAnalytics.initialise(ClawActivityCommon.mActivity, secret, key);
        } else {
            com.gameanalytics.android.GameAnalytics.initialise(ClawActivityCommon.mActivity, secret, key, build);
        }
        if( sessionTimeout > 0 ) {
            com.gameanalytics.android.GameAnalytics.setSessionTimeOut(1000 * sessionTimeout);
        }
        com.gameanalytics.android.GameAnalytics.logUnhandledExceptions();
        
        if (DEBUG) com.gameanalytics.android.GameAnalytics.setDebugLogLevel(com.gameanalytics.android.GameAnalytics.VERBOSE); 
    }

    public static void stopSession()
    {
        if( s_sessionActive )
        {
            if (DEBUG) Log.i(TAG, "stopSession()");
            s_sessionActive = false;
            com.gameanalytics.android.GameAnalytics.stopSession();
        }
    }
    
    public static void logEvent(String event)
    {
         if (DEBUG) Log.i(TAG, "logEvent(): " + event );
         logEvent( CATEGORY_DESIGN, event, "", "" );
    }

    public static void logEvent(String category, String event, String param, String value)
    {
        if (DEBUG) Log.i(TAG, "logEvent() cat: " + category + " event: " + event + " pram: " + param + " value: " + value);
        
        if( !s_sessionActive )
        {
            startSession("");
        }
        
        category = category.toLowerCase();
        
        if( category.equals( CATEGORY_BUSINESS ) )
        {
            int intValue = 0;
            try
            {
                intValue = Integer.parseInt( value );
            }
            catch( NumberFormatException e )
            {}
            com.gameanalytics.android.GameAnalytics.newBusinessEvent(event, param, intValue);
        }
        else if( category.equals( CATEGORY_QUALITY ) )
        {
            com.gameanalytics.android.GameAnalytics.newQualityEvent(event, param);
        }
        else // Design
        {
            float floatValue = 0;
            try
            {
                floatValue = Float.parseFloat( param );
            }
            catch( NumberFormatException e )
            {}
            
            com.gameanalytics.android.GameAnalytics.newDesignEvent(event, floatValue);
        }
    }
}
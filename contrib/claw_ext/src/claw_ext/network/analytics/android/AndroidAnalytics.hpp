//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/analytics/android/AndroidAnalytics.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLIDED__ANDROIDANALYTICS_HPP__
#define __INCLIDED__ANDROIDANALYTICS_HPP__

#include "claw_ext/network/analytics/Analytics.hpp"

class AndroidAnalytics : public Analytics
{
public:
    AndroidAnalytics( AnalyticsSystem system );

    void Initialize( const char* key, const char* secret, ParamsMap* params = NULL );
    void OnFocusChange( bool hasFocus );

    void StartSession( const char* analyticsKey = NULL );
    void StopSession();

    void LogEvent( const char* ev, const char* param = NULL, const char* value = NULL );
    void LogEvent( const char* ev, const EventParamList& params );

    void LogEventCategory( const char* category, const char* ev, const char* param = NULL, const char* value = NULL );
    void LogEventCategory( const char* category, const char* ev, const EventParamList& params );

    void StartEvent( const char* ev );
    void StopEvent( const char* ev );

private:
    AnalyticsSystem m_system;
};

#endif // __INCLIDED__ANDROIDANALYTICS_HPP__

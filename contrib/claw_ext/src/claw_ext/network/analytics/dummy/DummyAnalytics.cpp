#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics/dummy/DummyAnalytics.hpp"

static DummyAnalytics* s_instance = NULL;

Analytics* Analytics::QueryInterface( AnalyticsSystem system )
{
    if( !s_instance )
        s_instance = new DummyAnalytics;

    CLAW_ASSERT( s_instance );
    return s_instance;
}

void Analytics::Release( AnalyticsSystem system )
{
    delete s_instance;
    s_instance = NULL;
}

void DummyAnalytics::Initialize( const char* key, const char* secret, ParamsMap* params )
{
    CLAW_MSG( "ANALYTICS Initialize with key: " << key << " secret: " << secret );
}

void DummyAnalytics::OnFocusChange( bool hasFocus )
{
    CLAW_MSG( "ANALYTICS OnFocusChange: " << hasFocus );
}

void DummyAnalytics::StartSession( const char* key )
{
    CLAW_MSG( "ANALYTICS session start with key: " << key );
}

void DummyAnalytics::StopSession()
{
    CLAW_MSG( "ANALYTICS session stop" );
}

void DummyAnalytics::LogEvent( const char* ev, const char* param, const char* value )
{
    CLAW_MSG( "ANALYTICS LogEvent: " << ev );
    if( param && value )
    {
        CLAW_MSG( "    - " << param << " = " << value );
    }
    else if( param )
    {
        CLAW_MSG( "    - " << param );
    }
}

void DummyAnalytics::LogEvent( const char* ev, const EventParamList& params )
{
    CLAW_MSG( "ANALYTICS LogEvent: " << ev );

    EventParamList::const_iterator it = params.begin();
    EventParamList::const_iterator end = params.end();
    for ( ; it != end; ++it )
    {
        CLAW_ASSERT( it->first.length() );
        if( it->second.empty() )
        {
            CLAW_MSG( "    - " << it->first << " = " << it->second );
        }
        else
        {
            CLAW_MSG( "    - " << it->first );
        }
    }
}

void DummyAnalytics::LogEventCategory( const char* category, const char* ev, const char* param, const char* value )
{
    CLAW_MSG( "ANALYTICS LogEventCategorty: " << category << " event: " << ev );
    if( param && value )
    {
        CLAW_MSG( "    - " << param << " = " << value );
    }
    else if( param )
    {
        CLAW_MSG( "    - " << param );
    }
}

void DummyAnalytics::LogEventCategory( const char* category, const char* ev, const EventParamList& params )
{
    CLAW_MSG( "ANALYTICS LogEventCategorty: " << category << " event: " << ev );

    EventParamList::const_iterator it = params.begin();
    EventParamList::const_iterator end = params.end();
    for ( ; it != end; ++it )
    {
        CLAW_ASSERT( it->first.length() );
        if( it->second.empty() )
        {
            CLAW_MSG( "    - " << it->first << " = " << it->second );
        }
        else
        {
            CLAW_MSG( "    - " << it->first );
        }
    }
}

void DummyAnalytics::StartEvent( const char* ev )
{
    CLAW_MSG( "ANALYTICS StartEvent: " << ev );
}

void DummyAnalytics::StopEvent( const char* ev )
{
    CLAW_MSG( "ANALYTICS StopEvent: " << ev );
}

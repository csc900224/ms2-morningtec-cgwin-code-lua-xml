#ifndef __INCLIDED__WIN32ANALYTICS_HPP__
#define __INCLIDED__WIN32ANALYTICS_HPP__

#include "claw_ext/network/analytics/Analytics.hpp"

class DummyAnalytics : public Analytics
{
public:
    void Initialize( const char* key, const char* secret, ParamsMap* params = NULL );
    void OnFocusChange( bool hasFocus );

    void StartSession( const char* key );
    void StopSession();

    void LogEvent( const char* ev, const char* param = NULL, const char* value = NULL );
    void LogEvent( const char* ev, const EventParamList& params );

    void LogEventCategory( const char* category, const char* ev, const char* param = NULL, const char* value = NULL );
    void LogEventCategory( const char* category, const char* ev, const EventParamList& params );

    void StartEvent( const char* ev );
    void StopEvent( const char* ev );
};

#endif // __INCLIDED__WIN32ANALYTICS_HPP__

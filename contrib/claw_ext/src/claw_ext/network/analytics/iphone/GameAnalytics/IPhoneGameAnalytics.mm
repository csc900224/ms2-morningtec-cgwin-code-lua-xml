#include "claw_ext/network/analytics/iphone/GameAnalytics/IPhoneGameAnalytics.hpp"
#include "claw/base/Errors.hpp"
#include "AppVersion.hpp"

#import "GameAnalytics.h"

#include <string>

static const NSString* CATEGORY_BUSINESS = @"business";
static const NSString* CATEGORY_QUALITY  = @"quality";

void IPhoneGameAnalytics::Initialize( const char* key, const char* secret, ParamsMap* params )
{
    Claw::NarrowString build = Claw::g_appVersion;
    if( params )
    {
        ParamsMap::iterator it = params->find( "build" );
        if( it != params->end() )
        {
            build = it->second;
        }
    }
    
    [GameAnalytics setGameKey:[[NSString stringWithUTF8String:key] retain]
                    secretKey:[[NSString stringWithUTF8String:secret] retain]
                        build:[[NSString stringWithUTF8String:build.c_str()] retain]];
    
    [GameAnalytics setArchiveDataEnabled:YES];
    [GameAnalytics setArchiveDataLimit:500];
    [GameAnalytics setBatchRequestsEnabled:NO];
#ifdef _DEBUG
    [GameAnalytics setDebugLogEnabled:YES];
#else
    [GameAnalytics setDebugLogEnabled:NO];
#endif
}

void IPhoneGameAnalytics::StartSession( const char* analyticsKey )
{
    [GameAnalytics updateSessionID];
}

void IPhoneGameAnalytics::LogEvent( const char* ev, const char* param, const char* value )
{
    LogEventCategory( "", ev, param, value );
}

void IPhoneGameAnalytics::LogEvent( const char* ev, const EventParamList& params )
{
    LogEventCategory( "", ev, params );
}

void IPhoneGameAnalytics::LogEventCategory( const char* category, const char* ev, const char* param, const char* value )
{    
    const NSString* cat = [[NSString stringWithUTF8String:category] lowercaseString];
    
    if( [cat isEqualToString:(NSString*)CATEGORY_BUSINESS] )
    {
        if( param && value )
        {
            [GameAnalytics logBusinessDataEvent:[NSString stringWithUTF8String:ev]
                                     withParams:@{@"currency" : [NSString stringWithUTF8String:param],
                                                    @"amount" : [NSNumber numberWithInteger:[[NSString stringWithUTF8String:value] integerValue]]}];
        }
        else
        {
            [GameAnalytics logBusinessDataEvent:[NSString stringWithUTF8String:ev] withParams:nil];
        }
    }
    else if( [cat isEqualToString:(NSString*)CATEGORY_QUALITY] )
    {
        if( param )
        {
            [GameAnalytics logQualityAssuranceDataEvent:[NSString stringWithUTF8String:ev]
                                             withParams:@{@"message" : [NSString stringWithUTF8String:param]}];
        }
        else
        {
            [GameAnalytics logQualityAssuranceDataEvent:[NSString stringWithUTF8String:ev] withParams:nil];
        }

    }
    else // Design
    {
        if( param )
        {
            [GameAnalytics logGameDesignDataEvent:[NSString stringWithUTF8String:ev]
                                             withParams:@{@"value" : [NSNumber numberWithFloat:[[NSString stringWithUTF8String:param] floatValue]]}];
        }
        else
        {
            [GameAnalytics logGameDesignDataEvent:[NSString stringWithUTF8String:ev] withParams:nil];
        }
    }
}

void IPhoneGameAnalytics::LogEventCategory( const char* category, const char* ev, const EventParamList& params )
{
    EventParamList::const_iterator it = params.begin();
    EventParamList::const_iterator end = params.end();
    for( ; it != end; ++it )
    {
        LogEventCategory( category, ev, it->first.c_str(), it->second.c_str() );
    }
}

void IPhoneGameAnalytics::StartEvent( const char* ev )
{
    CLAW_ASSERT( !"Not supported" );
}

void IPhoneGameAnalytics::StopEvent( const char* ev )
{
    CLAW_ASSERT( !"Not supported" );
}

void IPhoneGameAnalytics::StopSession()
{}

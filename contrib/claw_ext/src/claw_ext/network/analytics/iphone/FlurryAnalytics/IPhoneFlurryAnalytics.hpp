#ifndef __INCLIDED__IPHONEFLURRYANALYTICS_HPP__
#define __INCLIDED__IPHONEFLURRYANALYTICS_HPP__

#include "claw_ext/network/analytics/Analytics.hpp"

class IPhoneFlurryAnalytics : public Analytics
{
public:
    void Initialize( const char* key, const char* secret, ParamsMap* params = NULL ) {}
    void OnFocusChange( bool hasFocus ) {}
    
    void StartSession( const char* analyticsKey );
    void StopSession();

    void LogEvent( const char* ev, const char* param = NULL, const char* value = NULL );
    void LogEvent( const char* ev, const EventParamList& params );

    void LogEventCategory( const char* category, const char* ev, const char* param = NULL, const char* value = NULL );
    void LogEventCategory( const char* category, const char* ev, const EventParamList& params );

    void StartEvent( const char* event );
    void StopEvent( const char* event );
};

#endif // __INCLIDED__IPHONEFLURRYANALYTICS_HPP__

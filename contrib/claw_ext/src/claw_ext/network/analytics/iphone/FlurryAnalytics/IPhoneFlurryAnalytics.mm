#include "claw_ext/network/analytics/iphone/FlurryAnalytics/IPhoneFlurryAnalytics.hpp"
#include "claw/base/Errors.hpp"
#import "Flurry.h"

void IPhoneFlurryAnalytics::StartSession( const char* analyticsKey )
{
    NSString* str = [NSString stringWithUTF8String:analyticsKey];
    [Flurry startSession:str];
}

void IPhoneFlurryAnalytics::LogEvent( const char* ev, const char* param, const char* value )
{
    if( param || value )
    {
        NSString* str = [NSString stringWithUTF8String:ev];
        
        if( !param ) param = "";
        if( !value ) value = "";

        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:
             [NSString stringWithUTF8String:value],
             [NSString stringWithUTF8String:param],
             nil
        ];

        [Flurry logEvent:str withParameters:dict];
    }
    else
    {
        NSString* str = [NSString stringWithUTF8String:ev];
        [Flurry logEvent:str];
    }
}

void IPhoneFlurryAnalytics::LogEvent( const char* ev, const EventParamList& params )
{
    NSString* str = [NSString stringWithUTF8String:ev];

    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    EventParamList::const_iterator it = params.begin();
    EventParamList::const_iterator end = params.end();
    for ( ; it != end; ++it )
    {
        NSString* name = [NSString stringWithUTF8String:it->first.c_str()];
        NSString* value = [NSString stringWithUTF8String:it->second.c_str()];
        [dict setObject:value forKey:name];
    }

    [Flurry logEvent:str withParameters:dict];
}

void IPhoneFlurryAnalytics::LogEventCategory( const char* category, const char* ev, const char* param, const char* value )
{
    Claw::NarrowString eventName = category;
    eventName.append( ":" );
    eventName.append( ev );
    LogEvent( eventName.c_str(), param, value );
}

void IPhoneFlurryAnalytics::LogEventCategory( const char* category, const char* ev, const EventParamList& params )
{
    Claw::NarrowString eventName = category;
    eventName.append( ":" );
    eventName.append( ev );
    LogEvent( eventName.c_str(), params );
}

void IPhoneFlurryAnalytics::StartEvent( const char* ev )
{
    NSString* str = [NSString stringWithUTF8String:ev];
    [Flurry logEvent:str withParameters:nil timed:YES];
}

void IPhoneFlurryAnalytics::StopEvent( const char* ev )
{
    NSString* str = [NSString stringWithUTF8String:ev];
    [Flurry endTimedEvent:str withParameters:nil];
}

void IPhoneFlurryAnalytics::StopSession()
{}

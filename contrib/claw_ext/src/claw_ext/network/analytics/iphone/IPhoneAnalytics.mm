#import "claw_ext/network/analytics/iphone/FlurryAnalytics/IPhoneFlurryAnalytics.hpp"
#import "claw_ext/network/analytics/iphone/GameAnalytics/IPhoneGameAnalytics.hpp"
#include "claw/base/Errors.hpp"

static Analytics* s_instance[Analytics::AS_NUM] = {NULL};

Analytics* Analytics::QueryInterface( AnalyticsSystem system )
{
    CLAW_ASSERT( system == AS_FLURRY || system == AS_GAME_ANALYTICS );

    if( !s_instance[system] )
    {
        if( system == AS_FLURRY )
        {
            s_instance[system] = new IPhoneFlurryAnalytics;
        }
        else if( system == AS_GAME_ANALYTICS )
        {
            s_instance[system] = new IPhoneGameAnalytics;
        }
    }

    CLAW_ASSERT( s_instance[system] );
    return s_instance[system];
}

void Analytics::Release( AnalyticsSystem system )
{
    CLAW_ASSERT( system == AS_FLURRY || system == AS_GAME_ANALYTICS );
    CLAW_ASSERT( s_instance[system] );

    delete s_instance[system];
    s_instance[system] = NULL;
}
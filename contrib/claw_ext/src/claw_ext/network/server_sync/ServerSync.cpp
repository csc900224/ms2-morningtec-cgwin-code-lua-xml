/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      claw_ext/network/server_sync/ServerSync.cpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2012, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

// Internal includes
#include "claw_ext/network/server_sync/ServerSync.hpp"
#include "claw_ext/network/UrlEncode.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Xml.hpp"
#include "claw/base/Registry.hpp"
#include "claw/vfs/Vfs.hpp"
#include "claw/vfs/EncryptedFile.hpp"
#include "claw/network/HttpRequest.hpp"
#include "claw/application/Time.hpp"
#include "claw/application/HardwareKey.hpp"

namespace ClawExt
{
    static const char* BRANCH_DEFAULT       = "/server-sync";
    static const char* KEY_TIMESTAMP        = "lastsynctime";
    static const char* KEY_GROUP_NAME       = "lastgroupname";
    static const char* KEY_RES_URL_NAME     = "res-url";

    static const char* SAVE_FILE_DEFAULT    = "server-sync.xml";

    static const char* SCRIPT_NAME_SYNC     = "sync.wsgi";

    static const char* PARAM_APPLICATION    = "app";
    static const char* PARAM_FILE_NAME      = "file";
    static const char* PARAM_UID            = "uid";
    static const char* PARAM_PLATFORM       = "platform";

    static ServerSync* s_instance = NULL;

    ServerSync* ServerSync::GetInstance()
    {
        if( !s_instance )
            s_instance = new ServerSync();
        return s_instance;
    }

    void ServerSync::Release()
    {
        delete s_instance;
        s_instance = NULL;
    }

    ServerSync::ServerSync()
        : m_syncThread( NULL )
        , m_syncInProgress( false )
        , m_initialized( false )
        , m_enabled( true )
    {}

    ServerSync::~ServerSync()
    {
        delete m_syncThread;
        m_syncThread = NULL;
    }

    bool ServerSync::Initialize( const Config& config )
    {
        if( !m_initialized )
        {
            CLAW_ASSERT( config.applicationName );
            CLAW_ASSERT( config.hostURL );
            CLAW_ASSERT( config.platformName );
            CLAW_ASSERT( config.saveDirectory );

            // Save config
            m_config = config;

            // Load internal data
            Load();

            // Reset last sync time to force first synchronisation
            if( m_config.updateEveryStart )
                ResetLastSyncTime();

            m_initialized = true;
            return true;
        }
        return false;
    }

    bool ServerSync::AddTask( const TaskId& id, const char* fileName, const char* originalFilePath )
    {
        CLAW_ASSERT( m_initialized );
        if( !m_initialized ) return false;

        CLAW_ASSERT( fileName && originalFilePath );

        SyncTaskPtr newTask( new SyncTask );
        newTask->m_scriptName = m_config.saveDirectory;
        if( (*newTask->m_scriptName.rbegin()) != '/' ) newTask->m_scriptName.append( "/" );
        newTask->m_scriptName.append( fileName );

        newTask->m_scriptBackupName = newTask->m_scriptName;
        newTask->m_scriptBackupName.append( ".bkp" );

        GenerateDownloadUrl( newTask->m_remotePath, fileName );
        newTask->m_originalPath = originalFilePath;

        std::pair<Tasks::iterator, bool> result = m_tasks.insert( Tasks::value_type( id, newTask ) );
        CLAW_ASSERT( result.second );
        return result.second;
    }

    void ServerSync::GenerateDownloadUrl( Claw::NarrowString& url, const Claw::NarrowString& fileName )
    {
        CLAW_ASSERT( m_initialized );

        url.clear();

        if( m_config.enableABTesting )
        {
            url = fileName;
        }
        else
        {
            url.append( m_config.hostURL );
            if( (*url.rbegin()) != '/' ) url.append( "/" );

            url.append( m_config.applicationName );
            url.append( "/" );
            url.append( m_config.platformName );
            url.append( "/" );
            url.append( fileName );
        }
    }

    void ServerSync::GenerateGroupIdUrl( Claw::NarrowString& url )
    {
        CLAW_ASSERT( m_initialized );

        url.clear();

        url.append( m_config.hostURL );
        if( (*url.rbegin()) != '/' ) url.append( "/" );
        url.append( SCRIPT_NAME_SYNC );

        AppendUrlParam( url, PARAM_APPLICATION, m_config.applicationName, true );
        AppendUrlParam( url, PARAM_PLATFORM, m_config.platformName );
        AppendUrlParam( url, PARAM_UID, GetUID() );
    }

    void ServerSync::AppendUrlParam( Claw::NarrowString& url, const Claw::NarrowString& paramName, const Claw::NarrowString& paramValue, bool first /*= false*/ )
    {
        url.append( first ? "?" : "&" );
        url.append( paramName );
        url.append( "=" );
        url.append( paramValue );
    }

    Claw::FilePtr ServerSync::LockTaskFile( const TaskId& id )
    {
        Tasks::iterator taskIt = m_tasks.find( id );
        CLAW_ASSERT( taskIt != m_tasks.end() );
        if( taskIt != m_tasks.end() )
        {
            taskIt->second->m_mutex.Enter();

            // Try downloaded file
            Claw::FilePtr file;
            if( m_config.encryptionKey )
                file.Reset( Claw::OpenEncryptedFile( taskIt->second->m_scriptName, m_config.encryptionKey, m_config.useCrcProtectedFiles ) );
            else
                file.Reset( Claw::OpenFile( taskIt->second->m_scriptName ) );
            if( !file ) 
            {
                // Try backup
                if( m_config.encryptionKey )
                    file.Reset( Claw::OpenEncryptedFile( taskIt->second->m_scriptBackupName, m_config.encryptionKey, m_config.useCrcProtectedFiles ) );
                else
                    file.Reset( Claw::OpenFile( taskIt->second->m_scriptBackupName ) );

                if( !file )
                {
                    // No backup either - open original file
                    file.Reset( Claw::OpenFile( taskIt->second->m_originalPath ) );

                    // We can release mutex already
                    taskIt->second->m_mutex.Leave();
                }
            }
            return file;
        }
        return Claw::FilePtr();
    }

    void ServerSync::ReleaseTaskFile( const TaskId& id, Claw::FilePtr file )
    {
        CLAW_ASSERT( file );
        Tasks::iterator taskIt = m_tasks.find( id );
        CLAW_ASSERT( taskIt != m_tasks.end() );
        if( taskIt != m_tasks.end() )
        {
            file.Release();
            taskIt->second->m_mutex.Leave();
        }
    }

    bool ServerSync::SyncData()
    {
        if( m_initialized && !m_tasks.empty() && !m_syncInProgress && ShouldSync() )
        {
            delete m_syncThread;
            m_syncThread = new Claw::Thread( ServerSync::DownloadEntry, this );
            m_syncInProgress = true;
            NotifySynchronisationStart();
            return true;
        }

        NotifySynchronisationSkip();
        return false;
    }

    Claw::NarrowString ServerSync::GetSyncGroupId() const
    {
        Claw::NarrowString regGroupId;
        GetGroup( regGroupId );
        return regGroupId;
    }

    int ServerSync::DoGetGroup()
    {
        //! Skip getting user group when A/B tests are disabled
        if( !m_config.enableABTesting ) return true;

        bool success = false;

        Claw::NarrowString groupIdUrl;
        GenerateGroupIdUrl( groupIdUrl );

        CLAW_MSG( "ServerSync - Getting group id from url " << groupIdUrl );

        Claw::Uri uri( groupIdUrl );
        Claw::HttpRequest dl( uri );
        dl.Connect();

        if( !dl.CheckError() )
        {
            dl.Download();
            if( !dl.CheckError() )
            {
                Claw::NarrowString response( dl.GetData(),  dl.GetLength() );
                Claw::XmlPtr responseXml( Claw::Xml::Create( response ) );
                if( responseXml )
                {
                    Claw::XmlIt it( *responseXml );
                    if( it && !strcmp( it.GetName(), "sync-response" ) )
                    {
                        int responseCode = -1;
                        it.GetAttribute( "code", responseCode );
                        if( responseCode >= 0 )
                        {
                            Claw::XmlIt group = it.Child( "groupid" );
                            Claw::XmlIt url = it.Child( "res-url" );
                            if( group && url )
                            {
                                Claw::NarrowString groupId;
                                Claw::NarrowString resUrl;

                                success = group.GetContent( groupId ) && url.GetContent( resUrl );
                                if( success )
                                {
                                    groupId = Sanitize( groupId );
                                    GenerateFullGroupId( groupId );
                                    CLAW_MSG( "ServerSync - Group ID Recieved: " << groupId );

                                    // Get group saved in the registry
                                    Claw::NarrowString regGroupId;
                                    GetGroup( regGroupId );

                                    // Notify group change!
                                    if( regGroupId != groupId )
                                    {
                                        SaveGroup( groupId );
                                        NotifyGroupChanged( groupId, regGroupId.empty() );
                                    }

                                    // Update Res URL
                                    SaveResUrl( resUrl );
                                }
                            }
                        }
                        else
                        {
                            CLAW_MSG( "ServerSync error: " << responseCode ); 
                            it = it.Child( "error" );
                            if( it )
                            {
                                Claw::NarrowString msg;
                                it.GetContent( msg );
                                CLAW_MSG( "ServerSync response message: " << msg ); 
                            }
                        }
                    }
                }
            }
        }
        CLAW_MSG( "ServerSync - Getting group id finised with result: " << success );
        return success;
    }

    int ServerSync::DoSyncData()
    {
        Tasks::iterator it = m_tasks.begin();
        Tasks::iterator end = m_tasks.end();

        bool success = false;

        for( ; it != end; ++it )
        {
            Claw::NarrowString downloadUrl;
            if( m_config.enableABTesting )
            {
                GetResUrl( downloadUrl );
                downloadUrl += "/" + it->second->m_remotePath;
            }
            else
            {
                downloadUrl = it->second->m_remotePath;
            }

            CLAW_MSG( "ServerSync - processing task: " << it->first );
            CLAW_MSG( "ServerSync - remote path: " << downloadUrl );

            Claw::Uri uri( downloadUrl );
            Claw::HttpRequest dl( uri );
            CLAW_MSG( "ServerSync - HttpRequest created - > connecting" );
            dl.Connect();

            if( !dl.CheckError() )
            {
                CLAW_MSG( "ServerSync - Connection successfull -> Downloading" );

                dl.Download();
                if( !dl.CheckError() )
                {
                    it->second->m_mutex.Enter(); // Citical section begin
                    {
                        CLAW_MSG( "ServerSync - Download finished -> saving" );

                        MakeBackup( it->first );

                        Claw::FilePtr f( Claw::VfsCreateFile( it->second->m_scriptName ) );
                        CLAW_MSG_WARNING( f, "ServerSync - could not create script file!" );

                        if( f )
                        {
                            f->Write( dl.GetData(), dl.GetLength() );
                            CLAW_MSG( "ServerSync -  Saving finished" );
                            success = true;
                        }
                    }
                    it->second->m_mutex.Leave(); // Citical section end
                }
            }
            CLAW_MSG( "ServerSync - Download process finished with result: " << success );
            NotifyTaskFinished( it->first, success );
        }
        return success;
    }

    void ServerSync::MakeBackup( const TaskId& id )
    {
        Tasks::iterator taskIt = m_tasks.find( id );
        CLAW_ASSERT( taskIt != m_tasks.end() );
        if( taskIt != m_tasks.end() )
        {
            Claw::FilePtr file;
            if( m_config.encryptionKey )
                file.Reset( Claw::OpenEncryptedFile( taskIt->second->m_scriptName, m_config.encryptionKey, m_config.useCrcProtectedFiles ) );
            else
                file.Reset( Claw::OpenFile( taskIt->second->m_scriptName ) );
            if( file )
            {
                file.Release();

                // Current file is ok - make backup
                file.Reset( Claw::OpenFile( taskIt->second->m_scriptName ) );

                // Opend destinantion file
                Claw::FilePtr dst( Claw::VfsCreateFile( taskIt->second->m_scriptBackupName ) );
                CLAW_MSG_WARNING( dst, "ServerSync - could not create backup!" );

                // Copy content
                if( dst )
                {
                    const Claw::StreamOff bufferSize = 1024;
                    char buffer[bufferSize];
                    Claw::StreamOff read = 0;

                    do
                    {
                        read = file->Read( buffer, bufferSize );
                        dst->Write( buffer, read );
                    }
                    while( read == bufferSize );
                }
            }
        }
    }

    int ServerSync::DownloadEntry( void* ptr )
    {
        ServerSync* ss = (ServerSync*)ptr;
        bool result = ss->DoGetGroup() && ss->DoSyncData();

        if( result )
        {
            // Save last sync time on success
            ss->SaveSyncTime();
        }
        ss->m_syncInProgress = false;

        ss->NotifySynchronisationEnd( result );
        return result;
    }

    void ServerSync::GetRegistryBranch( Claw::NarrowString& branch ) const
    {
        if( m_config.registryBranch )
            branch = m_config.registryBranch;
        else
            branch = BRANCH_DEFAULT;
        if( branch.empty() || (*branch.rbegin()) != '/' )
            branch.append( "/" );
    }

    void ServerSync::GetSavePath( Claw::NarrowString& path ) const
    {
        path = m_config.saveDirectory;
        if( path.empty() || (*path.rbegin()) != '/' )
            path.append( "/" );
        if( m_config.saveFileName )
            path.append( m_config.saveFileName );
        else
            path.append( SAVE_FILE_DEFAULT );
    }

    void ServerSync::ResetLastSyncTime()
    {
        Claw::NarrowString registryFullPath;
        GetRegistryBranch( registryFullPath );
        registryFullPath.append( KEY_TIMESTAMP );
        Claw::Registry::Get()->Set( registryFullPath.c_str(), 0 );
    }

    bool ServerSync::ShouldSync()
    {
        if( m_enabled )
        {
            int lastSyncTime = 0;
            int now = int(Claw::Time::GetTime());
        
            Claw::NarrowString registryFullPath;
            GetRegistryBranch( registryFullPath );
            registryFullPath.append( KEY_TIMESTAMP );

            Claw::Registry::Get()->Get( registryFullPath.c_str(), lastSyncTime );
            return (now - lastSyncTime) > (int)m_config.updatePertiod;
        }
        return false;
    }

    void ServerSync::SaveSyncTime()
    {
        int now = int(Claw::Time::GetTime());
        
        Claw::NarrowString registryFullPath;
        GetRegistryBranch( registryFullPath );
        registryFullPath.append( KEY_TIMESTAMP );

        Claw::Registry::Get()->Set( registryFullPath.c_str(), now );
        Save();
    }

    void ServerSync::GenerateFullGroupId( Claw::NarrowString& groupName )
    {
        Claw::NarrowString fullGroupId = m_config.platformName;
        fullGroupId.append( "-" );
        fullGroupId.append( groupName );
        groupName = fullGroupId;
    }

    void ServerSync::SaveGroup( const Claw::NarrowString& groupName )
    {
        Claw::NarrowString registryFullPath;
        GetRegistryBranch( registryFullPath );
        registryFullPath.append( KEY_GROUP_NAME );

        Claw::Registry::Get()->Set( registryFullPath.c_str(), groupName.c_str() );
        Save();
    }

    void ServerSync::GetGroup( Claw::NarrowString& groupName ) const
    {
        Claw::NarrowString registryFullPath;
        GetRegistryBranch( registryFullPath );
        registryFullPath.append( KEY_GROUP_NAME );

        const char* name = "";
        Claw::Registry::Get()->Get( registryFullPath.c_str(), name );
        groupName = name;
    }

    void ServerSync::SaveResUrl( const Claw::NarrowString& resUrl )
    {
        Claw::NarrowString oldUrl;
        GetResUrl( oldUrl );

        if( oldUrl != resUrl )
        {
            Claw::NarrowString registryFullPath;
            GetRegistryBranch( registryFullPath );
            registryFullPath.append( KEY_RES_URL_NAME );

            Claw::Registry::Get()->Set( registryFullPath.c_str(), resUrl.c_str() );
            Save();
        }
    }

    void ServerSync::GetResUrl( Claw::NarrowString& resUrl ) const
    {
        Claw::NarrowString registryFullPath;
        GetRegistryBranch( registryFullPath );
        registryFullPath.append( KEY_RES_URL_NAME );

        const char* url = "";
        Claw::Registry::Get()->Get( registryFullPath.c_str(), url );
        resUrl = url;
    }

    void ServerSync::Save()
    {
        Claw::NarrowString savePath;
        GetSavePath( savePath );

        Claw::NarrowString branch;
        GetRegistryBranch( branch );

        Claw::Registry::Get()->SaveEncrypted( savePath, GetUID(), branch.c_str() );
    }

    void ServerSync::Load()
    {
        Claw::NarrowString savePath;
        GetSavePath( savePath );

        Claw::NarrowString branch;
        GetRegistryBranch( branch );

        Claw::Registry::Get()->LoadEncrypted( savePath, GetUID(), false, branch.c_str() );
    }

    const Claw::NarrowString ServerSync::GetUID() const
    {
        return Claw::HardwareKey::Get();
    }

    void ServerSync::RegisterObserver( Observer* callback )
    {
        m_observers.insert( callback );
    }

    void ServerSync::UnregisterObserver( Observer* callback )
    {
        m_observers.erase( callback );
    }

    void ServerSync::NotifySynchronisationStart()
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
            (*it)->OnSynchronisationStart();
    }

    void ServerSync::NotifySynchronisationEnd( bool success )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
            (*it)->OnSynchronisationEnd( success );
    }

    void ServerSync::NotifySynchronisationSkip()
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
            (*it)->OnSynchronisationSkip();
    }

    void ServerSync::NotifyTaskFinished( const TaskId& taskId, bool success )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
            (*it)->OnTaskFinished( taskId, success );
    }

    void ServerSync::NotifyGroupChanged( const GroupName& newGroupName, bool initial )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
            (*it)->OnGroupChanged( newGroupName, initial );
    }

} // namespace ClawExt
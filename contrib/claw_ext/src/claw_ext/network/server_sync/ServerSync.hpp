/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      claw_ext/network/server_sync/ServerSync.hpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2012, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

#ifndef __INCLUDED__NETWORK_SERVERSYNC_HPP__
#define __INCLUDED__NETWORK_SERVERSYNC_HPP__

#include "claw/base/String.hpp"
#include "claw/base/Lua.hpp"
#include "claw/base/Thread.hpp"
#include "claw/base/Mutex.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/vfs/File.hpp"

#include <map>

namespace ClawExt
{
    //! ServerSync client that can be used for e.g. updated config files downloads.
    class ServerSync
    {
    public:
        // Type definitions
        typedef Claw::NarrowString  TaskId;
        typedef Claw::NarrowString  GroupName;

        //! Synchronisation process observer class.
        class Observer
        {
        public:
            //! Synchronisation thread/process has been started.
            virtual void    OnSynchronisationStart() = 0;
            
            //! Synchronisation thread has ended his work.
            virtual void    OnSynchronisationEnd( bool success ) = 0;
            
            //! Synchronisation was skipped due tu not hitting updatePertiod from last update.
            virtual void    OnSynchronisationSkip() = 0;
            
            //! Taks with given id was just completed.
            virtual void    OnTaskFinished( const TaskId& taskId, bool success ) = 0;
            
            //! Group assignment was changed. Iniital param infroms if this was initial assignment.
            virtual void    OnGroupChanged( const GroupName& newGroupName, bool initial ) = 0;

        }; // class Observer

        //! Client configuration structure
        struct Config
        {
            Config()
                : updatePertiod( 0 )
                , updateEveryStart( false )
                , registryBranch( NULL )
                , platformName( NULL )
                , applicationName( NULL )
                , hostURL( NULL )
                , saveDirectory( NULL )
                , saveFileName( NULL )
                , encryptionKey( NULL )
                , useCrcProtectedFiles( true )
                , enableABTesting( false )
            {}

            //! Update interval in seconds.
            unsigned int    updatePertiod;

            //! Should synchronisation be performed every application start (no matter if updatePertiod is reached).
            bool            updateEveryStart;

            //! Optional path to the registry branch where internal data will be stored.
            /**
            *   If NULL - a default value will  be used (/server-sync).
            */
            const char*     registryBranch;

            //! Optional file name of save file keeping internal data
            /**
            *   If NULL - a default value will  be used (server-sync.xml).
            */
            const char*     saveFileName;

            //! URL with full path to the sync scripts location e.g.: http://foo-bar.net/game-data/sync-server
            const char*     hostURL;
            
            //! String identifying platform/build type.
            /**
            *   Note: Must be consistent with directory names put on server.
            *   e.g. "android", "iphone", "amazon"
            */
            const char*     platformName;

            //! Name of the application taht will be synchronized.
            /** 
            *   Note: Must be consistent with directory name on the server.
            */
            const char*     applicationName;

            //! Common save directory, where downloaded files will be stored (permanently).
            const char*     saveDirectory;

            //! Key used to decode downloaded files.
            /**
            *   Files put on the serever can be encrypted for security reasons.
            *   When encryption key will be defined an automatic and transparent
            *   decryption can be performed. 
            *   If NULL no encryption will take place, and files will be returned in
            *   as-is state.
            *   NOTE: Only encrypted files with CRC are supported!
            */
            const char*     encryptionKey;

            //! Are encrypted files additionally proteted with CRC checksum? (default is true).
            bool            useCrcProtectedFiles;

            //! Decide if server-sync should be used to perform A/B testing. (default is false).
            /**
            *   If set to YES:  full featured heavy-weight A/B tests will be performed (user grupuings etc.).
            *                   Resource path: [HOST_RES_DIRECTORY]/[application]/[platform]/[group]
            *   If set to NO:   Just plain light-weighted http file downloads (without any dynamic content, scripts and DB).
            *                   Resource path: [hostURL]/[application]/[platform]
            */
            bool            enableABTesting;
        }; // struct Config

        //! Retreive object instance.
        static ServerSync*          GetInstance();

        //! Release instance.
        static void                 Release();

        //! Virtual destructor.
        virtual                     ~ServerSync();

        //! Initialize client using given configuration. Return true on success.
        bool                        Initialize( const Config& config );

        //! Set whether synchronisation engine is enabled or not (enabled by defautl).
        void                        SetEnabled( bool enabled )  { m_enabled = enabled; }

        //! Check is synchronisation engine is enabled.
        bool                        IsEnabled() const           { return m_enabled; }

        //! Add synchronisation task to be processed.
        /**
        *   @param id:                  Unique identifier of synchronization task.
        *   @pram fileName:             Name of the file that need to synchronized (name on the remote file system).
        *                               Will be downloaded to Config.saveDirectory.
        *   @param originalFilePath:    Local path to the file beeing updated. It will be used as backfill e.g. when there is 
        *                               no internet connection on first app laucnh. fileName and originalFilePath don't 
        *                               need to have same file name.
        */ 
        bool                        AddTask( const TaskId& id, const char* fileName, const char* originalFilePath );

        //! Request for data synchronisation.
        /**
        *   Should be called form time to time (not necessary everey frame) e.g. on entering menu
        *   or on other common event. When Config.updatePertiod is reached a synchronisation thread
        *   will be started.
        */
        bool                        SyncData();

        //! Get synchronisation group id (user gruop assigned by sync server).
        Claw::NarrowString          GetSyncGroupId() const;

        //! Get (lock) file associated with given task. It transparently returns appropriate file (local or remote).
        Claw::FilePtr               LockTaskFile( const TaskId& id );

        //! Release file that was previously locked using LockTaskFile(). Use this method if you want to "close" the file.
        void                        ReleaseTaskFile( const TaskId& id, Claw::FilePtr file );


        //! Register observer to be notiffied about synchronisation events.
        void                        RegisterObserver( Observer* callback );

        //! Unregister observer
        void                        UnregisterObserver( Observer* callback );

    protected:
        //! Get Unique device/user identifier.
        const Claw::NarrowString    GetUID() const;

        // Event notifiers
        void                        NotifySynchronisationStart();
        void                        NotifySynchronisationEnd( bool success );
        void                        NotifySynchronisationSkip();
        void                        NotifyTaskFinished( const TaskId& taskId, bool success );
        void                        NotifyGroupChanged( const GroupName& newGroupName, bool initial );

    private:
        struct SyncTask : public Claw::RefCounter
        {
            Claw::NarrowString  m_scriptName;
            Claw::NarrowString  m_scriptBackupName;
            Claw::NarrowString  m_remotePath;
            Claw::NarrowString  m_originalPath;
            Claw::Mutex         m_mutex;
        };
        typedef Claw::SmartPtr<SyncTask>        SyncTaskPtr;
        typedef std::map<TaskId, SyncTaskPtr>   Tasks;
        typedef std::set<Observer*>             Observers;
        typedef Observers::iterator             ObserversIt;

                                    ServerSync();

        int                         DoSyncData();
        int                         DoGetGroup();
        void                        MakeBackup( const TaskId& id );
        bool                        ShouldSync();
        void                        ResetLastSyncTime();
        void                        SaveSyncTime();


        void                        SaveResUrl( const Claw::NarrowString& resUrl );
        void                        GetResUrl( Claw::NarrowString& resUrl ) const;

        void                        SaveGroup( const Claw::NarrowString& groupName );
        void                        GetGroup( Claw::NarrowString& groupName ) const;
        void                        GenerateFullGroupId( Claw::NarrowString& groupName );

        void                        GetRegistryBranch( Claw::NarrowString& branch ) const;
        void                        GetSavePath( Claw::NarrowString& path ) const;

        void                        Save();
        void                        Load();

        void                        GenerateDownloadUrl( Claw::NarrowString& url, const Claw::NarrowString& fileName );
        void                        GenerateGroupIdUrl( Claw::NarrowString& url );
        void                        AppendUrlParam( Claw::NarrowString& url, const Claw::NarrowString& paramName, const Claw::NarrowString& paramValue, bool first = false );

        static int                  DownloadEntry( void* ptr );

    private:
        Tasks                       m_tasks;
        volatile bool               m_syncInProgress;
        Claw::Thread*               m_syncThread;

        bool                        m_initialized;
        bool                        m_enabled;
        Config                      m_config;

        Observers                   m_observers;

    }; // class ServerSync
} // namespace ClawExt

#endif // __INCLUDED__SERVERSYNC_HPP__

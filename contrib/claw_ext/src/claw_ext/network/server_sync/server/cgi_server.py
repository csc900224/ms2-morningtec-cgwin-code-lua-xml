#!/usr/bin/env python

import BaseHTTPServer
import CGIHTTPServer
import cgitb; cgitb.enable()  ## This line enables CGI error reporting
 
server = BaseHTTPServer.HTTPServer
handler = CGIHTTPServer.CGIHTTPRequestHandler
server_address = ("", 8000)
handler.cgi_directories = ['/abtest-server']

try:
	print 'started cgi server...'
	httpd = server(server_address, handler)
	httpd.serve_forever()
except KeyboardInterrupt:
	print 'shutting down server!'
	httpd.socket.close()


#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Created on 06-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import random
from sqlalchemy import and_
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
from sqlalchemy.pool import NullPool
from config import db
from data_provider.models import Group, Base

class GeneratorMethod(object):
	FAST="fast"			 # Use _generate_group_id_fast() for group id generation
	ACCURATE="accurate"	 # Use _generate_group_id() for group id generation

class GroupManager(object):
	""" Class responsible for mappong user <-> gorup. """
	
	def _db_connect(self):
		""" Method that connects with DB and starts ORM session. """
		self.engine = create_engine("mysql://"+ db.USER + ":" + db.PASS + "@" + db.HOST + "/" + db.DB, encoding="utf-8", poolclass=NullPool)
		if not self.engine.dialect.has_table(self.engine.connect(), Group.__tablename__):
			self._create_schema()
		self.session = sessionmaker(bind=self.engine)()
	
	def _db_disconnect(self):
		self.session.close()
		
	def _create_schema(self):
		""" Create tables for all defined data models. """
		Base.metadata.create_all(self.engine)
		
	def get_group_id(self, uid, application, platform):
		""" Get group assignment.
		
		@param uid: unique user/device identifier.
		@param application: Application class instance.
		@param platform: platform identifier (String).
		@return: group id (String) that is mapped for gived uid .
		
		"""

		# Check number of application groups - if there is only one available, don't connect to the DB at all.
		app_groups = application.get_groups(platform) + application.get_frozen_groups(platform)
		if len(app_groups) == 1:
			return app_groups[0]

		# Connect with DB
		self._db_connect()

		# Get group stored in db, if not found or group no more defined - generate a new one 
		query = self.session.query(Group).filter(and_(Group.application == application.name, Group.device_id == uid, Group.platform == platform))
		group_record = query.first()
		
		return_group = None
		if group_record != None and group_record.group_id in app_groups:
			return_group = group_record.group_id
		elif application.group_generator == GeneratorMethod.ACCURATE:
			return_group = self._generate_group_id(uid, application, platform, group_record)
		elif application.group_generator == GeneratorMethod.FAST:
			return_group = self._generate_group_id_fast(uid, application, platform, group_record)

		# Close DB connection
		self._db_disconnect()

		# Return result
		if return_group == None:
			raise Exception("Unknown id generation method: '" + application.group_generator + "' for application: " + application.name)
		else:
			return return_group
		
	def _generate_group_id(self, uid, application, platform, record):
		""" Generate new gorup id for given user (perform user -> group assignment).
		
		Algorithm checks which group has less occurences in db. The rarest id is then selected.
		This porvides fully uniform id distribution - no matter of groups configuration changes.
		
		@param uid: unique user/device identifier.
		@param application: Application class instance.
		@param platform: platform identifier (String).
		@param record: optional ORM record (Group class instance) if not None, given record updated will be performed.
		@return: group id (String) that is mapped for gived uid.
		
		"""
		# Get groups for platform
		app_groups = application.get_groups(platform)
		
		# Get pair (count, group_id) from existing records (filter only enabled groups)
		results = self.session.query(func.count(Group.group_id).label('count'), Group.group_id)\
			.filter(and_(Group.group_id.in_(app_groups), Group.application == application.name, Group.platform == platform))\
			.group_by(Group.group_id)\
			.order_by('count')\
			.all()
		
		# Get list of stored groups
		def extract_group_id(pair): return pair[1]
		stored_groups = map(extract_group_id, results)
		
		# Check if we have group that is not yet in DB
		new_group = None
		for group in app_groups:
			if not group in stored_groups:
				new_group = group
				break
		
		# Everything is in already DB - get result with minimal ocurences
		if new_group == None:
			new_group = results[0][1]
		
		if record != None: # This is record update
			record.group_id = new_group
		else:  # This is new record 
			self.session.add(Group(uid, application.name, platform, new_group))
		
		# Commit changes
		self.session.commit()
		
		return new_group
	
	def _generate_group_id_fast(self, uid, application, platform, record):
		""" Same as _generate_group_id(), but uses lightweight algortihm that
		produce less accurate id distribution. Can be usefull when no change to
		group configuration is planned (no adding nor removing groups). 
		
		"""
		# Get data sets for platform
		groups = application.get_groups(platform)
		
		# Get numer of records for given application
		records_count = self.session.query(Group)\
			.filter(and_(Group.group_id.in_(groups), Group.application == application.name, Group.platform == platform))\
			.count()
		
		# Calculate group id
		if record != None:
			# This is record update = randomize selection
			new_group = random.choice(groups)
			record.group_id = new_group
		else:
			# This is new record - we can caluclate new value
			new_group = groups[records_count % len(groups)]
			self.session.add(Group(uid, application.name, platform, new_group))
			
		# Commit changes
		self.session.commit()
		
		return new_group

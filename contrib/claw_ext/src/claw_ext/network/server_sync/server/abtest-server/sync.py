#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
This script returns group name and resource URL 
that is mapped for given uid in application-platform pair.

Required srcipt params:
	@param app: name of the application for which group is requested.
	@param uid: unique identifier of device/user that sent a request.

Optional srcipt params:
	@param platform: platform from wich request was sent. By defualut 'default' is used.	

Output:
	@return: XML file containing group id and resource root url
			example: <sync-response code='0'>
						<groupid>A</groupid>
						<res-url>http://foo.bar/res</res-url>
					 </sync-response>
	
Created on 24-06-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import cgi
import os
from config import applications, general, consts
from data_provider.groups import GroupManager
from data_provider.application import get_enabled_apps
from urlparse import urljoin

class GroupIdException(Exception):
	def __init__(self, code, msg):
		super(GroupIdException, self).__init__(msg)
		self.code = code
		self.msg = msg

def generate_response_xml(code, content):
	return "<?xml version='1.0' encoding='utf-8'?><sync-response code='" + unicode(code) + "'>" + content + "</sync-response>"

def print_response_header(length):
	print "Content-type: text/xml"
	print "Content-Length:", unicode(length)
	print

def print_error(code, message):
	response = generate_response_xml(code, "<error>" + message + "</error>")
	print_response_header(len(response))
	print response
	
def print_success(application, platform, groupid):
	groupIdNode = "<groupid>" + unicode(groupid) + "</groupid>"
	resRootNode = "<res-url>" + get_res_url(application, platform, groupid) + "</res-url>"
	
	response = generate_response_xml(0,  groupIdNode +  resRootNode)
	print_response_header(len(response))
	print response
	
def get_res_url(application, platform, groupid):
	scriptURL = "http://" + os.environ['SERVER_NAME'] + ":" + os.environ['SERVER_PORT'] + os.environ['SCRIPT_NAME']
	return urljoin(scriptURL, general.DATA_URL) + "/" + application + "/" + platform + "/" + groupid

try:
	# Get argument list
	arguments = cgi.FieldStorage()
	
	# Check if application is requested
	if consts.PARAM_APPLICATION not in arguments:
		raise GroupIdException(-2, "Application not defined")
		
	# Check if uid is passed
	if consts.PARAM_UID not in arguments:
		raise GroupIdException(-3, "UID not defined")
	
	# Read all required params
	app_name = arguments[consts.PARAM_APPLICATION].value
	uid = arguments[consts.PARAM_UID].value
	
	# Check optional platform specifier
	if consts.PARAM_PLATFORM in arguments:
		platform = arguments[consts.PARAM_PLATFORM].value
	else:
		platform = consts.PLATFORM_DEFAULT
		
	# Validate aplication selection
	enabled_apps = get_enabled_apps()
	if app_name not in enabled_apps:
		raise GroupIdException(-4, "Application not defined: " + app_name)
		
	# Get data set id
	application = enabled_apps[app_name]
	manager = GroupManager()
	
	# Print output
	print_success(app_name, platform, manager.get_group_id(uid, application, platform))
	
except GroupIdException, ex:
	print_error(ex.code, ex.msg)

except Exception, ex:
	print_error(-1, unicode(ex))

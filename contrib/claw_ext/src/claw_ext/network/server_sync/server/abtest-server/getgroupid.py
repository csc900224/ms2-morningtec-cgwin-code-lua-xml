#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
This script returns group name that is mapped for given uid in application-platform pair.

Required srcipt params:
	@param app: name of the application for which group is requested.
	@param uid: unique identifier of device/user that sent a request.

Optional srcipt params:
	@param platform: platform from wich request was sent. By defualut 'default' is used.	

Otput:
	@return: Mapped group id (name). Printed in text mode.

	
Created on 07-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import cgi
import sys
from config import applications, consts
from data_provider.groups import GroupManager
from data_provider.application import get_enabled_apps
  
try:
	# Get argument list
	arguments = cgi.FieldStorage()
	
	# Check if application is requested
	if consts.PARAM_APPLICATION not in arguments:
		raise Exception("Application not defined")
		
	# Check if uid is passed
	if consts.PARAM_UID not in arguments:
		raise Exception("UID not defined")
	
	# Read all required params
	app_name = arguments[consts.PARAM_APPLICATION].value
	uid = arguments[consts.PARAM_UID].value
	
	# Check optional platform specifier
	if consts.PARAM_PLATFORM in arguments:
		platform = arguments[consts.PARAM_PLATFORM].value
	else:
		platform = consts.PLATFORM_DEFAULT
		
	# Validate aplication selection
	enabled_apps = get_enabled_apps()
	if app_name not in enabled_apps:
		raise Exception("Application not defined: " + app_name)
		
	# Get data set id
	application = enabled_apps[app_name]
	manager = GroupManager()
	
	# Print output
	print "Content-type: text/html"
	print
	sys.stdout.write(manager.get_group_id(uid, application, platform))
	
except Exception, ex:
	print "Status:404"
	print "Content-type: text/html"
	print "Transfer-Encoding: identity"
	print
	print "Unexpected error:", ex

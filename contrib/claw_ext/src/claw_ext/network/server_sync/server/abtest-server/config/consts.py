#!/usr/bin/python
# -*- coding: UTF-8 -*-,

"""
Created on 25-06-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import os

PARAM_APPLICATION="app"
PARAM_FILE_NAME="file"
PARAM_UID="uid"
PARAM_PLATFORM="platform"
PARAM_SET="set"

PLATFORM_DEFAULT="default"

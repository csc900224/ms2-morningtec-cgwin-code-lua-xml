#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Created on 05-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

from config import consts
from data_provider import groups

class Application(object):
	""" Class representing single application configuration.
	
	Allows to set application name, define groups for each platform 
	and select group id generation method.
	
	"""
	
	def __init__(self, name, group_generator=None, app_groups=None):
		""" Constructor.
		
		@param name: application name.
		@param group_generator: optional method of generation group ids for new new users. By default ACCURATE is used.
		@param app_groups: optional dictionary that contains platform to group list mapping.  
		
		""" 
		self.app_groups = {}
		self.app_groups_frozen = {}
		self.name = name
		
		if app_groups != None:
			self.add_groups(app_groups)
		
		if group_generator != None:
			self.group_generator = group_generator
		else:
			self.group_generator = groups.GeneratorMethod.ACCURATE
		
	def _sanitize_groups(self, groups):
		for i in range(len(groups)):
			groups[i] = str(groups[i])
		
	def add_groups(self, app_groups, platform=consts.PLATFORM_DEFAULT):
		""" Add groups definition for given platform.
		@param app_groups: group name list.
		@param platform: platform name, by defualt 'default' is used.
		 
		"""

		self.app_groups[platform] = app_groups
		self._sanitize_groups(self.app_groups[platform])
		
	def add_frozen_groups(self, app_groups, platform=consts.PLATFORM_DEFAULT):
		""" Add frozen groups definition for given platform.
		
		Frozen groups will not be taken under consideration when 
		performing group generation for new users, but allows to preserve 
		group classification for users, that are already asigned to them.
		
		@param app_groups: group name list.
		@param platform: platform name, by defualt 'default' is used.
		
		"""

		self.app_groups_frozen[platform] = app_groups
		self._sanitize_groups(self.app_groups_frozen[platform])
		
	def get_groups(self, platform=consts.PLATFORM_DEFAULT):
		""" Get groups definition for given platform.
		
		@param platform: platform name, by defualt 'default' is used.
		@return: group name list for given platform.
		 
		"""
		if platform in self.app_groups.keys():
			return self.app_groups[platform]
		else:
			raise Exception("Platform: '" + platform + "' not defined for application: " + self.name)
		
	def get_frozen_groups(self, platform=consts.PLATFORM_DEFAULT):
		""" Get fozen groups definition for given platform.
		
		@param platform: platform name, by defualt 'default' is used.
		@return: group name list for given platform.
		
		"""
		if platform in self.app_groups_frozen.keys():
			return self.app_groups_frozen[platform]
		else:
			return []
	
	def enable(self):
		""" Add application to list of enabled objects. """
		_enabled_apps[self.name]=self

_enabled_apps = {}
		
def get_enabled_apps():
	return _enabled_apps


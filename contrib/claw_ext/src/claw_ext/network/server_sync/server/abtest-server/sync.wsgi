#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
This script returns group name and resource URL 
that is mapped for given uid in application-platform pair.

Required srcipt params:
    @param app: name of the application for which group is requested.
    @param uid: unique identifier of device/user that sent a request.

Optional srcipt params:
    @param platform: platform from wich request was sent. By defualut 'default' is used.    

Output:
    @return: XML file containing group id and resource root url
            example: <sync-response code='0'>
                        <groupid>A</groupid>
                        <res-url>http://foo.bar/res</res-url>
                     </sync-response>

Created on 24-06-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import sys, os
sys.path.append(os.path.dirname(__file__))

from sys import exc_info
from traceback import format_tb
from cgi import parse_qs, escape
from config import applications, general, consts
from data_provider.groups import GroupManager
from data_provider.application import get_enabled_apps
from urlparse import urljoin

class GroupIdException(Exception):
    def __init__(self, code, msg):
        super(GroupIdException, self).__init__(msg)
        self.code = code
        self.msg = msg

def generate_response_xml(code, content):
    return "<?xml version='1.0' encoding='utf-8'?><sync-response code='" + str(code) + "'>" + content + "</sync-response>"

def print_response_header(length):
    return [('Content-type', 'text/xml'),
            ('Content-Length', str(length))]

def print_error(environ, code, message):
    response = generate_response_xml(code, "<error>" + message + "</error>")
    header = print_response_header(len(response))
    return (header, response)
    
def print_success(environ, application, platform, groupid):
    groupIdNode = "<groupid>" + str(groupid) + "</groupid>"
    resRootNode = "<res-url>" + get_res_url(environ, application, platform, groupid) + "</res-url>"
    
    response = generate_response_xml(0,  groupIdNode +  resRootNode)
    header = print_response_header(len(response))
    return (header, response)
    
def get_res_url(environ, application, platform, groupid):
    scriptURL = "http://" + environ.get('SERVER_NAME') + ":" + environ.get('SERVER_PORT') + environ.get('SCRIPT_NAME')
    return urljoin(scriptURL, general.DATA_URL) + "/" + application + "/" + platform + "/" + groupid

def application(environ, start_response):
    try:
        status = '200 OK'
        parameters = parse_qs(environ.get('QUERY_STRING', ''))
        (response_headers, output) = generate_output(environ, parameters)                 
        start_response(status, response_headers)
        yield output
    except GeneratorExit:
        raise
    except:
        try:
            import datetime
            f = open( os.path.join( os.path.dirname(os.path.realpath(__file__)), "..", "logs", "error.log" ), "a" )
            f.write( ">> [" + str(datetime.datetime.now()) + "]: " )
            
            try:
                e_type, e_value, tb = exc_info()
                traceback = ['Traceback (most recent call last):']
                traceback += format_tb(tb)
                traceback.append('%s: %s' % (e_type.__name__, e_value))
                # we might have not a stated response by now. try
                # to start one with the status code 500 or ignore an
                # raised exception if the application already started one.
                try:
                    start_response('500 INTERNAL SERVER ERROR', [('Content-Type', 'text/plain')])
                except:
                    pass
                
                f.write( '\n'.join(traceback) )
                yield '\n'.join(traceback)
                
            except Exception as ex:
                f.write( str(ex) )
                yield str(ex)
                
            f.write( '\n\n' )
            f.close()
            
        except Exception as ex:
            yield str(ex)

def generate_output(environ, arguments):
    try:        
        # Check if application is requested
        if consts.PARAM_APPLICATION not in arguments:
            raise GroupIdException(-2, "Application not defined")
            
        # Check if uid is passed
        if consts.PARAM_UID not in arguments:
            raise GroupIdException(-3, "UID not defined")
        
        # Read all required params
        app_name = escape(arguments[consts.PARAM_APPLICATION][0])
        uid = escape(arguments[consts.PARAM_UID][0])
        
        # Check optional platform specifier
        if consts.PARAM_PLATFORM in arguments:
            platform = escape(arguments[consts.PARAM_PLATFORM][0])
        else:
            platform = consts.PLATFORM_DEFAULT
            
        # Validate aplication selection
        enabled_apps = get_enabled_apps()
        if app_name not in enabled_apps:
            raise GroupIdException(-4, "Application not defined: " + app_name)
            
        # Get data set id
        application = enabled_apps[app_name]
        manager = GroupManager()
        
        # Print output
        return print_success(environ, app_name, platform, manager.get_group_id(uid, application, platform))
        
    except GroupIdException, ex:
        return print_error(environ, ex.code, ex.msg)

    except Exception, ex:
        return print_error(environ, -1, str(ex))

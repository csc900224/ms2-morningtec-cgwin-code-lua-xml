#!/usr/bin/python
# -*- coding: UTF-8 -*-,

"""
Created on 05-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import os

DATA_DIR=os.path.join("..", "data") # Deprecated: used by getfile.py and getroupid.py
DATA_URL="/data" # Relative to sync.py


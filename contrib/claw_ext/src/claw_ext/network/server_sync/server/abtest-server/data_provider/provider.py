#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Created on 06-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import sys
import os.path
from config import general

CONTENT_DATA="Content-type: application/octet-stream\n"
CONTENT_TEXT="Content-Type: text/plain;charset=utf-8\n"
CONTENT_XML="Content-Type: text/xml;charset=utf-8\n"
CONTENT_DISPOSITION="Content-Disposition: attachment; filename="
CONTENT_LENGTH="Content-Length: "

TEXT_EXT=('.txt','.csv')
XML_EXT=('.xml','.html')

def provide_data(app_name, platform, group, file_name):
	""" Provides requested file to the system output.
	Also sets appropriate content-type.
	
	File system structure:
		[general.DATA_DIR]
			|-[app_name]
				|-[platform]
					|-[group]
						|-[file_name]
	
	@param app_name: application name (application directory name).
	@param platform: platform name (platform directory name).
	@param group: group name (group directory name).
	@param file_name: file name (file name).
	
	"""
	# Open in and out files
	inpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), general.DATA_DIR, app_name, platform, str(group), file_name)
	in_file = open(inpath)
	out_file = sys.stdout 
	content_type = _get_content_type(inpath)

	# File length
	in_file.seek(0,2)
	file_size = in_file.tell()
	in_file.seek(0)

	# Write header
	out_file.write(content_type)
	out_file.write(CONTENT_DISPOSITION + file_name + "\n")
	out_file.write(CONTENT_LENGTH + unicode(file_size) + "\n")
	out_file.write("\n")
	
	# Write file content
	out_file.write(in_file.read())
	in_file.close()
	
def _get_content_type(file_path):
	""" Returns appropriate Content-Type based on file name extension. """ 
	
	ext = os.path.splitext(file_path)[1]
	if ext in TEXT_EXT:
		return CONTENT_TEXT
	elif ext in XML_EXT:
		return CONTENT_XML
	else:
		return CONTENT_DATA
	
#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Created on 06-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String

Base = declarative_base()

class Group(Base):
	""" Class represeting user group data model.
	
	It stores group_id for each device_id-application-platform tripple.
	
	"""
	__tablename__ = "device-groups"

	device_id = Column(String(255), primary_key=True)
	application = Column(String(127), primary_key=True)
	platform = Column(String(127), primary_key=True)
	group_id = Column(String(127), nullable=False, index=True)
	
	def __init__(self, device_id, application_name, platform, group_id):
		self.device_id = device_id
		self.application = application_name
		self.platform = platform
		self.group_id = group_id

#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Created on 05-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

from data_provider.application import Application

# LoH
loh = Application("loh")
loh.add_groups(["one", "two", "three", "four"], "android")
loh.add_groups([1, 3], "iphone")
loh.add_frozen_groups([2], "iphone")
loh.enable()

# MS2
ms2 = Application("ms2")
ms2.add_groups(["A","B","C"], "iphone")
ms2.enable()
#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
This script returns requested file from group that is mapped 
for given uid in application-platform pair.

Required srcipt params:
	@param app: name of the application for which group is requested.
	@param uid: unique identifier of device/user that sent a request.
	@param file: requested file name (including extension). 

Optional srcipt params:
	@param platform: platform from wich request was sent. By defualut 'default' is used.	

Otput:
	@return: File from appropriate group.
	

Created on 06-02-2013

@copyright: (c) 2012, Gamelion. All rights reserved.
@author: Jacek Nijaki <jacek@nijaki.pl>

"""

import cgi
from config import applications, consts
from data_provider import provider
from data_provider.groups import GroupManager
from data_provider.application import get_enabled_apps

try:
	# Get argument list
	arguments = cgi.FieldStorage()
	
	# Check if application is requested
	if consts.PARAM_APPLICATION not in arguments:
		raise Exception("Application not defined")
	
	# Check if file is requested
	if consts.PARAM_FILE_NAME not in arguments:
		raise Exception("File name not defined")
	
	# Check if uid is passed
	if consts.PARAM_UID not in arguments:
		raise Exception("UID not defined")
	
	# Read all required params
	app_name = arguments[consts.PARAM_APPLICATION].value
	file_name = arguments[consts.PARAM_FILE_NAME].value
	uid = arguments[consts.PARAM_UID].value
	
	# Check optional platform specifier
	if consts.PARAM_PLATFORM in arguments:
		platform = arguments[consts.PARAM_PLATFORM].value
	else:
		platform = consts.PLATFORM_DEFAULT
		
	# Validate aplication selection
	enabled_apps = get_enabled_apps()
	if app_name not in enabled_apps:
		raise Exception("Application not defined: " + app_name)
		
	# Get data set id
	if consts.PARAM_SET not in arguments:
		# If set is not provided as GET param - look for it in DB
		application = enabled_apps[app_name]
		manager = GroupManager()
		set_id = manager.get_group_id(uid, application, platform)
	else:
		set_id = arguments[consts.PARAM_SET].value

	# Provide requested data
	provider.provide_data(app_name, platform, set_id, file_name);
	
except Exception, ex:
	print "Status:404"
	print "Content-type: text/html"
	print "Transfer-Encoding: identity"
	print
	print "Unexpected error:", ex

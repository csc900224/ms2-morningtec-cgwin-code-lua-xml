#include "claw_ext/network/push/LocalNotification.hpp"

#include "claw/base/Errors.hpp"
#include "claw/base/Registry.hpp"

namespace LocalNotification
{
    void Ask( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& yes, const Claw::NarrowString& no )
    {
        CLAW_VERIFY( Claw::Registry::Get()->Set( "/settings/local-notifications-enabled", true ) );
    }

    bool Schedule( const char* message, const char* button, const char* sound, Claw::UInt32 epoch )
    {
        bool enabled = false;
        Claw::Registry::Get()->Get( "/settings/local-notifications-enabled", enabled );

        if ( enabled )
        {
            CLAW_MSG( "Schedule local notification:" );
            CLAW_MSG( "  - message = " << message );
            CLAW_MSG( "  - button = " << button );
            CLAW_MSG( "  - sound = " << sound );
            CLAW_MSG( "  - time = " << epoch );

            return true;
        }

        return false;
    }

    void CancelAll()
    {
        CLAW_MSG( "Cancel all local notifications" );
    }

}

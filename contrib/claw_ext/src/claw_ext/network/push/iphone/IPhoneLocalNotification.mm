#include "claw_ext/network/push/LocalNotification.hpp"

#include "claw/base/Errors.hpp"
#include "claw/base/Registry.hpp"

#import "UIKit/UIApplication.h"
#import "UIKit/UILocalNotification.h"

#import "UIAlertView+Blocks.h"

namespace LocalNotification
{
    void Ask( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& yes, const Claw::NarrowString& no )
    {
        // User was already asked
        bool enabled = false;
        if ( Claw::Registry::Get()->Get( "/settings/local-notifications-enabled", enabled ) )
        {
            return;
        }

        DismissBlock dismissBlock = ^( int buttonIndex )
        {
            Claw::Registry::Get()->Set( "/settings/local-notifications-enabled", buttonIndex == -1 );
        };

        NSArray* buttons = [NSArray arrayWithObjects:
            [NSString stringWithCString:yes.c_str() encoding:NSUTF8StringEncoding],
            [NSString stringWithCString:no.c_str() encoding:NSUTF8StringEncoding],
            nil
        ];

        [UIAlertView alertViewWithTitle:[NSString stringWithCString:title.c_str() encoding:NSUTF8StringEncoding]
                                message:[NSString stringWithCString:message.c_str() encoding:NSUTF8StringEncoding]
                      cancelButtonTitle:nil
                      otherButtonTitles:buttons
                              onDismiss:dismissBlock
                               onCancel:nil
        ];
    }

    bool Schedule( const char* message, const char* button, const char* sound, Claw::UInt32 epoch )
    {
        CLAW_ASSERT( message );
        CLAW_ASSERT( button );

        bool enabled = false;
        Claw::Registry::Get()->Get( "/settings/local-notifications-enabled", enabled );

        if ( enabled )
        {
            UILocalNotification* notification = [[UILocalNotification alloc] init];
            notification.fireDate = [[NSDate alloc] initWithTimeIntervalSince1970: epoch];
            notification.timeZone = [NSTimeZone defaultTimeZone];

            notification.alertBody = [NSString stringWithCString:message encoding:NSUTF8StringEncoding];
            notification.alertAction = [NSString stringWithCString:button encoding:NSUTF8StringEncoding];

            if ( sound )
            {
                notification.soundName = [NSString stringWithCString:sound encoding:NSUTF8StringEncoding];
            }
            else
            {
                notification.soundName = UILocalNotificationDefaultSoundName;
            }

            [[UIApplication sharedApplication] scheduleLocalNotification: notification];
            [notification release];

            return true;
        }

        return false;
    }

    void CancelAll()
    {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }

}

#ifndef __INCLUDED__LOCALNOTIFICATION_HPP__
#define __INCLUDED__LOCALNOTIFICATION_HPP__

#include "claw/base/String.hpp"
#include "claw/compat/ClawTypes.hpp"

namespace LocalNotification
{
    void Ask( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& yes, const Claw::NarrowString& no );
    bool Schedule( const char* message, const char* button, const char* sound, Claw::UInt32 time );
    void CancelAll();
}

#endif // __INCLUDED__LOCALNOTIFICATION_HPP__

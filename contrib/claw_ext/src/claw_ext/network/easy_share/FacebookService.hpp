//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/FacebookService.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_FACEBOOK_SERVICE_HPP__
#define __NETWORK_FACEBOOK_SERVICE_HPP__

// Internal includes
#include "claw_ext/network/easy_share/Service.hpp"
#include "claw_ext/network/easy_share/ServiceCreator.hpp"
#include "claw_ext/network/easy_share/ServiceAction.hpp"

#include "claw_ext/network/facebook/Facebook.hpp"

// External includes
#include "claw/base/String.hpp"


// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    class FacebookCreator : public ServiceCreator
    {
    public:
                            FacebookCreator();

        virtual Service*    Create( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc );

    }; // FacebookCreator

    class FacebookService : public Service
    {
    public:
                            FacebookService( const char* fbAppId );

        virtual Action*     CreateAction( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc );

        FaceBook*           GetApi();

    protected:
        FaceBook*           m_api;

    }; // class FacebookService

    class FacebookPostWall : public ServiceAction, public FaceBook::Observer
    {
    public:
                            FacebookPostWall( const Text& id, const Text& desc );

        virtual bool        Perform( Service* onService );

        //! Derived from FaceBook::Observer for waiting on authentication confirmation.
        virtual void        OnAuthenticationChange( bool authenticated );

        //! Setup message being Tweet.
        void                SetMsg( const Text& postMsg )   { m_msg = postMsg; }

        //! Get Tweet message being posted.
        const Text&         GetMsg() const                  { return m_msg; }

    private:
        Text                m_msg;
        FaceBook*           m_api;
        bool                m_authentication;

    }; // class FacebookPostWall

    class FacebookAppRequest: public ServiceAction
    {
    public:
                            FacebookAppRequest( const Text& id, const Text& desc );

        virtual bool        Perform( Service* onService );

    }; // class FacebookAppRequest

} // namespace ClawExt

#endif // !defined __NETWORK_FACEBOOK_SERVICE_HPP__
// EOF

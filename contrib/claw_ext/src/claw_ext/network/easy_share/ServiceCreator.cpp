//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/ServiceCreator.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/easy_share/ServiceCreator.hpp"

// External includes
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    ServiceCreator::ServiceCreator( const Text& serviceId )
        : m_serviceId( serviceId )
    {}

    ServiceCreator::~ServiceCreator()
    {}

} // namespace ClawExt
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/ServiceCreator.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_SERVICE_CREATOR_HPP__
#define __NETWORK_SERVICE_CREATOR_HPP__

// Internal includes

// External includes
#include "claw/base/String.hpp"

#include <list>
#include <map>

// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    // Forward declarations
    class Service;

    //! Utitlity class that is registered with EasyShare::RegisterService to instantiate given service type.
    class ServiceCreator
    {
    public:
        typedef Claw::NarrowString  Text;

                            ServiceCreator( const Text& serviceId );

        //! Virtual destructor for derived implementations.
        virtual             ~ServiceCreator();

        //! Return unique service id being created in this instance.
        inline const Text&  GetServiceId() const        { return m_serviceId; }

        //! Service creation method.
        /*!
        * Implementation depenend method to be implemented in derived classes intended to serve as Sevices factory.
        * Non-abstract implementation of ServiceCreator may be then registered (ServiceRegister) in EasyShare to
        * allow corresponding type of service instalation (\see EasyShare::Install() method).
        */
        virtual Service*    Create( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc ) = 0;

    private:
        Text                m_serviceId;

    }; // class ServiceCreator

} // namespace ClawExt

#endif // !defined __NETWORK_SERVICE_CREATOR_HPP__
// EOF

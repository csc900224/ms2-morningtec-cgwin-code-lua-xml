//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/ServiceAction.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/easy_share/ServiceAction.hpp"

// External includes

namespace ClawExt
{
    ServiceAction::ServiceAction( const Text& id, const Text& desc )
        : m_id( id )
        , m_desc( desc )
    {}

    ServiceAction::~ServiceAction()
    {}

} // namespace ClawExt
// EOF

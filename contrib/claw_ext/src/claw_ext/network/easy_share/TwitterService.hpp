//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/TwitterService.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_TWITTER_SERVICE_HPP__
#define __NETWORK_TWITTER_SERVICE_HPP__

// Internal includes
#include "claw_ext/network/easy_share/Service.hpp"
#include "claw_ext/network/easy_share/ServiceCreator.hpp"
#include "claw_ext/network/easy_share/ServiceAction.hpp"

#include "claw_ext/network/twitter/TwitterService.hpp"

// External includes
#include "claw/base/String.hpp"


// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    class TwitterCreator : public ServiceCreator
    {
    public:
                            TwitterCreator();

        virtual Service*    Create( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc );

    }; // TwitterCreator

    class TwitterService : public Service
    {
    public:
                            TwitterService( const char* appKey, const char* appSecret );

        virtual Action*     CreateAction( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc );

        Twitter*            GetApi();

    private:
        Twitter*            m_api;

    }; // class TwitterService

    class TwitterPost : public ServiceAction, public Twitter::Observer
    {
    public:
                            TwitterPost( const Text& id, const Text& desc );

        //! Post message on the Twitter account.
        virtual bool        Perform( Service* onService );

        //! Derived from Twitter::Observer to support postponed Twitter post.
        virtual void        OnAuthenticationChange( bool authenticated );

        //! Setup message beeing posted on Perform call.
        void                SetMsg( const Text& postMsg )   { m_postMsg = postMsg; }

        //! Get Tweet message beeing posted.
        const Text&         GetMsg() const                  { return m_postMsg; }

    private:
        Text                m_postMsg;
        Twitter*            m_api;
        bool                m_authenticating;

    }; // class TwitterPost

} // namespace ClawExt

#endif // !defined __NETWORK_TWITTER_SERVICE_HPP__
// EOF

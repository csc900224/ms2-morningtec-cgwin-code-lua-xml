//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/EasyShare.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/easy_share/EasyShare.hpp"
#include "claw_ext/network/easy_share/Service.hpp"
#include "claw_ext/network/easy_share/ServiceCreator.hpp"
#include "claw_ext/network/easy_share/ServiceAction.hpp"

#include "claw_ext/ui/UIList.hpp"
#include "claw_ext/ui/UIPanel.hpp"
#include "claw_ext/ui/UIFrame.hpp"
#include "claw_ext/ui/UILabel.hpp"
#include "claw_ext/ui/UIButton.hpp"
#include "claw_ext/ui/UIFunctor.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/base/Xml.hpp"
#include "claw/application/Application.hpp"

#include <algorithm>

#define _DEBUG_TEST_EVENTS  0   // Enable buttons events debuging, each button will display last event related to itself.

namespace ClawExt
{
    static const UIButton::Size     UI_BUTTON_SIZE( 200, 40 );
    static const Claw::Color        UI_BUTTON_COLOR( 73, 107, 135 );
    static const Claw::Color        UI_BUTTON_COLOR_ROLL_OVER( 176, 95, 61 );

    class ButtonPress : public UIFunctor
    {
    public:
                            ButtonPress( Service* service, const ServiceAction::Id& action )
                                : m_service( service )
                                , m_action( action )
                            {}

        virtual void        Call( UIComponent* cmp, UIComponent::CursorEvent e, UIComponent::CursorIdx idx, const Pos& p ) { m_service->PerformAction( m_action ); }

    private:
        ServiceAction::Id   m_action;
        Service*            m_service;
    }; // class ButtonPress

    class ButtonRollOut : public UIFunctor
    {
    public:
        virtual void        Call( UIComponent* cmp, UIComponent::CursorEvent e, UIComponent::CursorIdx idx, const Pos& p ) { ((UIButton*)cmp)->SetBgColor( UI_BUTTON_COLOR ); }
    }; // class ButtonOut

    class ButtonRollIn : public UIFunctor
    {
    public:
        virtual void        Call( UIComponent* cmp, UIComponent::CursorEvent e, UIComponent::CursorIdx idx, const Pos& p ) { ((UIButton*)cmp)->SetBgColor( UI_BUTTON_COLOR_ROLL_OVER ); }
    }; // class ButtonRollIn

#if _DEBUG_TEST_EVENTS
    class ButtonEventTest : public UIFunctor
    {
    public:
                            ButtonEventTest( const char* txt )
                                : m_text( txt )
                            {}

                            ~ButtonEventTest() { }

        virtual void        Call( UIComponent* cmp, UIComponent::CursorEvent e, UIComponent::CursorIdx idx, const Pos& p ) { ((UIButton*)cmp)->SetText( m_text ); }
    private:
        Claw::String        m_text;

    }; // class ButtonEventText

#endif // _DEBUG_TEST_EVENTS

    /* static */
    EasyShare* EasyShare::QueryInterface()
    {
        return new EasyShare();
    }

    /* static */
    void EasyShare::Release( EasyShare* es )
    {
        delete es;
    }

    bool EasyShare::RegisterObserver( Observer* observer )
    {
        // Already registered
        if( std::find( m_observers.begin(), m_observers.end(), observer ) != m_observers.end() )
            return false;
        m_observers.push_back( observer );
        return true;
    }

    bool EasyShare::UnregisterObserver( Observer* observer )
    {
        // Not yet registered
        ObserversIt it = std::find( m_observers.begin(), m_observers.end(), observer );
        if( it == m_observers.end() )
            return false;
        m_observers.erase( it );
        return true;
    }

    bool EasyShare::Open( ErrorMessage* err /*= NULL*/ )
    {
        return true;
    }

    void EasyShare::Render( Claw::Surface* target )
    {
        if( m_layout )
        {
            m_layout->Render( target );
        }
    }

    void EasyShare::Update( float dt )
    {

    }

    void EasyShare::OnCursorMove( int x, int y )
    {
        m_layout->OnCursorMove( UIComponent::Pos( x, y ), 0 );
    }

    void EasyShare::OnCursorPress( int x, int y )
    {
        m_layout->OnCursorPress( UIComponent::Pos( x, y ), 0 );
    }

    void EasyShare::OnCursorRelease( int x, int y )
    {
        m_layout->OnCursorRelease( UIComponent::Pos( x, y ), 0 );
    }

    bool EasyShare::Install( const Text& configPath )
    {
        Claw::XmlPtr xmlDoc( Claw::Xml::LoadFromFile( configPath ) );
        Claw::XmlIt xmlRoot( *xmlDoc );

        Claw::XmlIt xmlServices = xmlRoot.Child( "services" );
        Claw::XmlIt serviceIt = xmlServices.Child();
        while( serviceIt )
        {
            Service* service = ServiceCreate( &serviceIt );

            if( service )
            {
                // Append actions to each already created service
                Claw::XmlIt xmlActions = serviceIt.Child( "actions" );
                Claw::XmlIt actionIt = xmlActions.Child();
                while( actionIt )
                {
                    ServiceAddAction( service->GetId(), &actionIt );
                    ++actionIt;
                }
            }
            else
            {
                CLAW_MSG_ASSERT( false, "[EasyShare]->Cstruct(): Couldn't create a service." );
                return false;
            }

            ++serviceIt;
        }

        // Load layout data
        LoadLayout( &xmlRoot );

        return true;
    }

    void EasyShare::LoadLayout( const Claw::XmlIt* xmlConfig )
    {
        Claw::XmlIt xmlLayout = xmlConfig->Child( "layout" );
        {
            Claw::XmlIt xmlPanel = xmlLayout.Child( "panel" );
        }

        UIFrame::BorderSettings border;
        border.m_top = "ui/easy_share/button_top.png";
        border.m_down = "ui/easy_share/button_down.png";
        border.m_left = "ui/easy_share/button_left.png";
        border.m_right = "ui/easy_share/button_right.png";
        border.m_cornerLeftTop = "ui/easy_share/button_left_top.png";
        border.m_cornerLeftDown = "ui/easy_share/button_left_down.png";
        border.m_cornerRightTop = "ui/easy_share/button_right_top.png";
        border.m_cornerRightDown = "ui/easy_share/button_right_down.png";
        border.m_offRightDown.Set( -5, -6 );

        const int appWidth = Claw::Application::GetInstance()->GetDisplay()->GetWidth();
        const int appHeight = Claw::Application::GetInstance()->GetDisplay()->GetHeight();
        const Claw::NarrowString fontPath = "fonts/font_normal.xml";
        UIComponent::Align align;
        align.m_horiz = UIComponent::AH_CENTER;
        align.m_vert = UIComponent::AV_CENTER;

        // Just for center screen alignment
        UIPanel* screenPanel = new UIPanel( UIPanel::Pos( 0, 0 ), UIPanel::Size( appWidth, appHeight ) );

        // For whole popup
        UIPanel* popupPanel = new UIPanel( UIPanel::Pos( 0, 0 ), UIPanel::Size( 250, 250 ) );
        popupPanel->SetAlign( align );

        UIPanel* headerPanel = new UIPanel( UIFrame::Pos(0, 0),
                                            UIPanel::Size( 250, 40 ) );
        UIPanel* menuPanel = new UIPanel(   UIFrame::Pos(0, headerPanel->GetSize().m_y),
                                            UIFrame::Size( 250, 250 - headerPanel->GetSize().m_y ) );

        // Create header content
        UIFrame* headerFrame = new UIFrame( UIFrame::Pos(0, 0),
                                            UIFrame::Size( 250, 40 - border.m_offRightDown.m_y - border.m_offLeftTop.m_y ),
                                            Claw::Color( 255, 14, 15 ) );
        headerFrame->LoadBorder( &border );
        headerPanel->AddComponent( headerFrame );

        UILabel* headerLabel = new UILabel( UILabel::Pos( 0, 0 ), fontPath );
        headerLabel->SetText( Claw::String("EasyShare") );
        headerLabel->SetAlign( align );
        headerPanel->AddComponent( headerLabel );

        popupPanel->AddComponent( headerPanel );

        // Create menu panel content
        UIFrame* bgFrame = new UIFrame( UIFrame::Pos(0, 0), menuPanel->GetSize(), Claw::Color( 79, 86, 94 ) );
        bgFrame->LoadBorder( &border );
        menuPanel->AddComponent( bgFrame );

        UIList* list = new UIList();
        align.m_vert = UIList::AV_CENTER;
        list->SetSize( UIList::Size( 200, 0 ) );
        list->SetAlign( align );
        list->SetOrientation( UIList::LO_VERTICAL );
        list->SetSpacing( UIList::LS_FIXED, 10 );

        ServicesIt serviceIt = m_services.begin();
        ServicesIt serviceEnd = m_services.end();
        for( ; serviceIt != serviceEnd; ++serviceIt )
        {
            const Service::Actions& actions = (*serviceIt).second->GetActions();
            Service::ActionsConstIt actionIt = actions.begin();
            Service::ActionsConstIt actionEnd = actions.end();
            for( ; actionIt != actionEnd; ++actionIt )
            {
                UIButton* btn = new UIButton( UIButton::Pos( 0, 0 ), UI_BUTTON_SIZE, fontPath, UI_BUTTON_COLOR );
                btn->LoadFrameBorder( &border );
                btn->SetText( Claw::String( actionIt->second->GetDescription() ) );
                btn->SetEventFunc( UIComponent::CE_ROLL_OUT, UIFunctorPtr(new ButtonRollOut) );
                btn->SetEventFunc( UIComponent::CE_ROLL_IN,  UIFunctorPtr(new ButtonRollIn) );
                btn->SetEventFunc( UIComponent::CE_PRESS, UIFunctorPtr(new ButtonPress( serviceIt->second, actionIt->second->GetId() ) ) );

#if _DEBUG_TEST_EVENTS
                btn->SetEventFunc( UIComponent::CE_PRESS, UIFunctorPtr(new ButtonEventTest( "Press" ) ) );
                btn->SetEventFunc( UIComponent::CE_RELEASE, UIFunctorPtr(new ButtonEventTest( "Release" ) ) );
                btn->SetEventFunc( UIComponent::CE_ROLL_IN, UIFunctorPtr(new ButtonEventTest( "RollIn" ) ) );
                btn->SetEventFunc( UIComponent::CE_ROLL_OVER, UIFunctorPtr(new ButtonEventTest( "RollOver" ) ) );
                btn->SetEventFunc( UIComponent::CE_ROLL_OUT, UIFunctorPtr(new ButtonEventTest( "RollOut" ) ) );
                btn->SetEventFunc( UIComponent::CE_DRAG_IN, UIFunctorPtr(new ButtonEventTest( "DragIn" ) ) );
                btn->SetEventFunc( UIComponent::CE_DRAG_OVER, UIFunctorPtr(new ButtonEventTest( "DragOver" ) ) );
                btn->SetEventFunc( UIComponent::CE_DRAG_OUT, UIFunctorPtr(new ButtonEventTest( "DragOut" ) ) );
                btn->SetEventFunc( UIComponent::CE_MOVE_OUTSIDE, UIFunctorPtr(new ButtonEventTest( "MoveOutside" ) ) );
                btn->SetEventFunc( UIComponent::CE_DRAG_OUTSIDE, UIFunctorPtr(new ButtonEventTest( "DragOutside" ) ) );
#endif
                list->AddComponent( btn );
            }
        }
        menuPanel->AddComponent( list );
        popupPanel->AddComponent( menuPanel );
        screenPanel->AddComponent( popupPanel );
        m_layout = screenPanel;
        return;
    }

    EasyShare::EasyShare()
        : m_layout( NULL )
    {
    }

    EasyShare::~EasyShare()
    {
        delete m_layout;
    }

    void EasyShare::ServiceRegister( ServiceCreator* cr )
    {
        CLAW_MSG_ASSERT( m_creators.find( cr->GetServiceId() ) == m_creators.end(), "[EasyShare]->ServiceCreate(): Service creator already registered." );

        m_creators.insert( Creators::value_type( cr->GetServiceId(), cr ) );
    }

    Service* EasyShare::ServiceCreate( const Claw::XmlIt* xmlConfig )
    {
        CLAW_ASSERT( xmlConfig->HasAttribute( "id" ) );
        CLAW_ASSERT( xmlConfig->HasAttribute( "desc" ) );

        const Text serviceId = Text( xmlConfig->GetAttribute( "id" ) );
        ServicesIt serviceIt = m_services.find( serviceId );
        if( serviceIt != m_services.end() )
        {
            CLAW_MSG_WARNING( false, "[EasyShare]->ServiceCreate(): Service (" << serviceId << ") already created." );
            return serviceIt->second;
        }

        // Find creator designed for this type of service
        CreatorsIt creatorIt = m_creators.find( Text( serviceId ) );
        if( creatorIt != m_creators.end() )
        {
            const Text serviceDesc = Text( xmlConfig->GetAttribute( "desc" ) );

            // If corresponding creator was found try to instantiate service with an given xml data
            ServiceCreator* creator = creatorIt->second;
            Service* service = creator->Create( xmlConfig, serviceId, serviceDesc );
            m_services.insert( Services::value_type( serviceId, service ) );
            return service;
        }
        else
        {
            CLAW_MSG_WARNING( false, "[EasyShare]->ServiceCreate(): Unknown service (" << Text(serviceId) << ") found in config file, skipping it." );
            return NULL;
        }
    }

    void EasyShare::ServiceAddAction( const Text& serviceId, const Claw::XmlIt* xmlConfig )
    {
        ServicesIt serviceIt = m_services.find( serviceId );
        if( serviceIt != m_services.end() )
        {
            serviceIt->second->RegisterAction( xmlConfig );
        }
        else
        {
            CLAW_MSG_WARNING( false, "[EasyShare]->ServiceAddAction(): Service was not registered" );
        }
    }

} // namespace ClawExt
// EOF

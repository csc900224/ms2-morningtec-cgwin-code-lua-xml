//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/ServiceAction.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_SERVICE_ACTION_HPP__
#define __NETWORK_SERVICE_ACTION_HPP__

// Internal includes

// External includes
#include "claw/base/String.hpp"

namespace ClawExt
{
    // Forward declare Service class.
    class Service;

    //! Abstract action class
    class ServiceAction
    {
    public:
        typedef Claw::NarrowString      Text;
        typedef Text                    Id;

        //! Simply set identifier and descriptor.
                            ServiceAction( const Id& id, const Text& desc );

        //! Virtual for derived classes.
        virtual             ~ServiceAction();

        //! Return unique identifier.
        inline const Id&    GetId() const           { return m_id; }

        //! Return service descriptor.
        /*!
        * This method is virtual intentionaly. By default returns service description string passed in
        * constructor, but you may want to override it to allow texts localization.
        */
        virtual Text        GetDescription() const  { return m_desc; }

        //! Perform actual action using corresponding service.
        virtual bool        Perform( Service* onService ) = 0;

    private:
        Id                  m_id;
        Text                m_desc;
    };

} // namespace ClawExt

#endif // !defined __NETWORK_SERVICE_ACTION_HPP__
// EOF

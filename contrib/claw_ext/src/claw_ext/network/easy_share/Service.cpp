//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/Service.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/easy_share/Service.hpp"
#include "claw_ext/network/easy_share/ServiceAction.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    Service::Service( const Text& id, const Text& desc )
        : m_id( id )
        , m_desc( desc )
    {}

    Service::~Service()
    {}

    bool Service::RegisterAction( const Claw::XmlIt* xmlConfig )
    {
        CLAW_ASSERT( xmlConfig->HasAttribute( "id" ) );
        CLAW_ASSERT( xmlConfig->HasAttribute( "desc" ) );

        const Text actionId = xmlConfig->GetAttribute( "id" );
        ActionsIt actionIt = m_actionsRegistry.find( actionId );
        if( actionIt == m_actionsRegistry.end() )
        {
            const Text actionDesc = xmlConfig->GetAttribute( "desc" );
            Action* action = CreateAction( xmlConfig, actionId, actionDesc );
            CLAW_MSG_ASSERT( action, "[Service]->RegisterAction(): Action was not created for the service: " << GetId() );

            m_actionsRegistry.insert( Actions::value_type( actionId, action ) );
            return true;
        }
        else
        {
            CLAW_MSG_WARNING( false, "[Service]->RegisterAction(): Action already registered" );
            return false;
        }
    }

     bool Service::PerformAction( const Text& actionId )
     {
         ActionsIt actionIt = m_actionsRegistry.find( actionId );
         if( actionIt != m_actionsRegistry.end() )
         {
             return actionIt->second->Perform( this );
         }
         return false;
     }

} // namespace ClawExt
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/TwitterService.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/easy_share/TwitterService.hpp"
#include "claw_ext/network/social/Twitter.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    TwitterCreator::TwitterCreator()
        : ServiceCreator( "twitter" )
    {}

    Service* TwitterCreator::Create( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc )
    {
        Claw::XmlIt xmlData = xmlConfig->Child( "data" );
        Claw::XmlIt xmlAppKey = xmlData.Child( "appKey" );
        Claw::XmlIt xmlAppSecret = xmlData.Child( "appSecret" );
        Claw::NarrowString appKey;
        Claw::NarrowString appSecret;
        if( xmlAppKey.GetContent( appKey ) && xmlAppSecret.GetContent( appSecret ) )
        {
            return new TwitterService( appKey.c_str(), appSecret.c_str() );
        }
        else
        {
            return NULL;
        }
    }

    TwitterService::TwitterService( const char* appKey, const char* appSecret )
        : Service( "twitter", "Twitter" )
    {
        m_api = Twitter::QueryInterface( appKey, appSecret );
    }

    TwitterService::Action* TwitterService::CreateAction( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc )
    {
        // TODO: Provide similar flexibility as ServiceCreator gives for adding new ServiceActions.
        // Probably via ServiceCreator::CreateAction()
        return new TwitterPost( id, desc );
    }

    Twitter* TwitterService::GetApi()
    {
        return m_api;
    }

    TwitterPost::TwitterPost( const Text& id, const Text& desc )
        : ServiceAction( id, desc )
        , m_api( NULL )
        , m_authenticating( false )
    {
    }

    bool TwitterPost::Perform( Service* onService )
    {
        CLAW_ASSERT( onService );
        m_api = ((TwitterService*)onService)->GetApi();
        CLAW_ASSERT( m_api );

        m_api->RegisterObserver( this );
        if( m_api->IsAuthenticated() )
        {
            m_api->PublishStatus( m_postMsg );
        }
        else
        {
            m_authenticating = true;
            m_api->Authenticate();
        }
        return true;
    }

    void TwitterPost::OnAuthenticationChange( bool authenticated )
    {
        CLAW_ASSERT( m_api );

        if( authenticated && m_authenticating )
        {
            m_api->PublishStatus( m_postMsg );
        }
        else
        {
            CLAW_MSG_WARNING( false, "User not authenticated" );
        }
    }

} // namespace ClawExt
// EOF

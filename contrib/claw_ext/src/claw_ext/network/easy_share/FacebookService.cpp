//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/FacebookService.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/easy_share/FacebookService.hpp"
#include "claw_ext/network/facebook/Facebook.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    FacebookCreator::FacebookCreator()
        : ServiceCreator( "facebook" )
    {}

    Service* FacebookCreator::Create( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc )
    {
        Claw::XmlIt xmlData = xmlConfig->Child( "data" );
        Claw::XmlIt xmlAppId = xmlData.Child( "appId" );
        Claw::NarrowString appId;
        if( xmlAppId.GetContent( appId ) )
        {
            return new FacebookService( appId.c_str() );
        }
        else
        {
            return NULL;
        }
    }

    FacebookService::FacebookService( const char* fbAppId )
        : Service( "facebook", "Facebook" )
    {
        m_api = FaceBook::QueryInterface( fbAppId );
    }

    FacebookService::Action* FacebookService::CreateAction( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc )
    {
        return new FacebookPostWall( id, desc );
    }

    FaceBook* FacebookService::GetApi()
    {
        return m_api;
    }

    FacebookPostWall::FacebookPostWall( const Text& id, const Text& desc )
        : ServiceAction( id, desc )
        , m_api( NULL )
        , m_authentication( false )
    {
    }

    bool FacebookPostWall::Perform( Service* onService )
    {
        CLAW_ASSERT( onService );
        m_api = ((FacebookService*)onService)->GetApi();

        if( m_api->IsAuthenticated() )
        {
            FaceBook::FeedData data;
            data.m_caption = m_msg;
            return ((FacebookService*)onService)->GetApi()->PublishFeed( data );
        }
        else
        {
            m_authentication = true;
            m_api->RegisterObserver( this );
            m_api->Authenticate();
            return true;
        }
    }

    void FacebookPostWall::OnAuthenticationChange( bool authenticated )
    {
        if( authenticated && m_authentication )
        {
            CLAW_ASSERT( m_api );
            FaceBook::FeedData data;
            data.m_caption = m_msg;
            m_api->PublishFeed( data );
        }
        else
        {
            CLAW_MSG_WARNING( false, "User not authenticated" );
        }
    }

    FacebookAppRequest::FacebookAppRequest( const Text& id, const Text& desc )
        : ServiceAction( id, desc )
    {
    }

    bool FacebookAppRequest::Perform( Service* onService )
    {
        CLAW_ASSERT( onService );

        FaceBook::AppRequestData data;
        return ((FacebookService*)onService)->GetApi()->SendRequest( data );
    }

} // namespace ClawExt
// EOF

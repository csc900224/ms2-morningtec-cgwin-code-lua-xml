//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/EasyShare.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_EASY_SHARE_HPP__
#define __NETWORK_EASY_SHARE_HPP__

// Internal includes

// External includes
#include "claw/base/String.hpp"

#include <list>
#include <map>

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    // Forward declarations
    class Service;
    class ServiceCreator;
    class ServiceAction;

    class UIContainer;

    //! Component gathering different network services in a simple ui form.
    /*!
    * EasyShare allows for registering variouse services and actions concerned with them. Each available service and
    * action is then displayed in a simple Gui to allow user to choose and service and perform registered action on
    * that service.
    */
    class EasyShare
    {
    public:
        // Forward declare
        class Observer;

        // Error message container type
        typedef Claw::NarrowString      ErrorMessage;
        // Text string type
        typedef Claw::NarrowString      Text;

        //! Retreive platfrom dependend implementation.
        /**
         * This method should be implemented and linked once in platform dependend object,
         * returning appropriate object implementation.
         */
        static EasyShare*   QueryInterface();

        //! Release platform specific implementation.
        /**
         * Call destructors, release memory, make cleanup.
         */
        static void         Release( EasyShare* es );

        //! Load configuration and install services.
        /*!
        * This must be called before you will try to use any services.
        * Keep note that all services installed via xml file should be previously registered
        * via ServiceRegister() method. Otherwise installation of unrecognized (unregistered services)
        * will cause assertion.
        */
        bool                Install(  const Text& configPath );

        //! Try to load layout data from configuration xml.
        void                LoadLayout( const Claw::XmlIt* xmlConfig );

        //! Register service observer.
        /**
         * \return false if observer was already registered, true if successfully registered.
         */
        bool                RegisterObserver( Observer* observer );

        //! Unregister observer.
        /**
         * \return false if observer was not registered to this service.
         */
        bool                UnregisterObserver( Observer* observer );

        //! Try to open easy share screen.
        /**
        * This only changes the easy share logic, to actually display and see this screen you must call
        * EasyShare::Render() method and EasyShare::Update() inside your appliation loop.
        */
        virtual bool        Open( ErrorMessage* err = NULL );

        //! Render the screen on the top of current display buffer.
        void                Render( Claw::Surface* target );

        //! Perform service tick.
        void                Update( float dt );

        //! Process cursor movement, passing new position.
        void                OnCursorMove( int x, int y );

        //! Process cursor pressing event.
        void                OnCursorPress( int x, int y );

        //! Process cursor release event.
        void                OnCursorRelease( int x, int y );

        //! Register custom service creator - that allows us to use this service if specified in xml config file.
        void                ServiceRegister( ServiceCreator* cr );

        //! Observer class for receiving api callbacks.
        class Observer
        {

        }; // class Observer

    protected:
        //! Just to protect from manual construction.
                            EasyShare();

        //! Protect from manual destruction. EasyShare::Release should be used instead.
                            ~EasyShare();

       Service*             ServiceCreate( const Claw::XmlIt* xmlConfig );

       void                 ServiceAddAction( const Text& serviceName, const Claw::XmlIt* xmlConfig );

    private:
        typedef std::list< Observer* >              Observers;
        typedef Observers::iterator                 ObserversIt;
        typedef Observers::const_iterator           ObserversConstIt;

        typedef std::map< Text, ServiceCreator*>    Creators;
        typedef Creators::iterator                  CreatorsIt;
        typedef Creators::const_iterator            CreatorsConstIt;

        typedef std::map< Text, Service*>           Services;
        typedef Services::iterator                  ServicesIt;
        typedef Services::const_iterator            ServicesConstIt;

        Observers       m_observers;

        Services        m_services;
        Creators        m_creators;

        UIContainer*    m_layout;

    }; // class EasyShare

} // namespace ClawExt

#endif // !defined __NETWORK_EASY_SHARE_HPP__
// EOF

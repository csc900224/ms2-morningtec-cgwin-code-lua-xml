//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/Service.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_SERVICE_HPP__
#define __NETWORK_SERVICE_HPP__

// Internal includes

// External includes
#include "claw/base/String.hpp"

#include <map>

// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    // Forward declarations
    class ServiceAction;

    //! Abstract service class.
    /*!
    * Allows to perform and register different types of activities concerned with this service.
    * For example user may want to "post a wall" on Facebook or send "app request". Morever this
    * implementation gives you more flexibility in defining actions types i.e. "post level sucess on the wall" or
    * even "post 50 points record on the wall". The only limit is you imagination, service allowable actions
    * and giving each action unique id.
    */
    class Service
    {
    public:
        typedef ServiceAction                       Action;
        typedef Claw::NarrowString                  Text;

        typedef std::map< Text, ServiceAction* >    Actions;
        typedef Actions::iterator                   ActionsIt;
        typedef Actions::const_iterator             ActionsConstIt;

                            Service( const Text& id, const Text& desc );

        virtual             ~Service();

        inline const Text&  GetId() const                               { return m_id; }

        //! Return service descriptor - used in UI.
        /*!
        * Method is virtual intentionaly, by default returns service description string passed to
        * constructor, but you may want to override it to allow texts localization. In short terms
        * this virtuality allows for project specific formating and localizations.
        */
        virtual Text        GetDescription() const                      { return m_desc; }

        //! Return reference to actions register, it may be iterated to acquire actions ids and descriptions.
        const Actions&      GetActions() const                          { return m_actionsRegistry; }

        //! Parse action xml node and add to it to registered actions set.
        bool                RegisterAction( const Claw::XmlIt* xmlConfig );

        //! Perform action with specified actionId key (assuming that it was registered).
        /*!
        * This method finds specified (by actionId) action in the registry and calls its ServiceAction::Perform()
        * method. This way you may put all your action logic into custom action class. If you don't want to
        * prepare separate classes for each actions, you may also preapre dummy action implementation and move all
        * the logic to Service::PerformAction() method - this is the main reason why it is virtual while having
        * basic implementation. Just remember to call base class (Service) PerformAction() implementation then.
        */
        virtual bool        PerformAction( const Text& actionId );

    protected:
        //! This method need to be implemenented in derived classes to provide case specific data loading and Actions creation.
        /*
        * \note In the future consider using factory pattern for creating actions for a given service, in simmilar way
        * the services creators are registered. This will give the programmer flexibility to add custom actions to
        * the existing service, now the service need to know all actions related with it.
        */
        virtual Action*     CreateAction( const Claw::XmlIt* xmlConfig, const Text& id, const Text& desc ) = 0;

        Actions             m_actionsRegistry;

    private:
        Text                m_id;
        Text                m_desc;

    }; // class Service

} // namespace ClawExt

#endif // !defined __NETWORK_SERVICE_HPP__
// EOF

#include "claw_ext/network/analytics2/flurry/iphone/IPhoneFlurryAnalytics.hpp"

#include "claw/base/Errors.hpp"
#include "AppVersion.hpp"

#import "Flurry.h"

namespace ClawExt
{
    static IPhoneFlurryAnalytics* s_instance = NULL;

    FlurryAnalytics* FlurryAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneFlurryAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void FlurryAnalytics::Release()
    {
        CLAW_ASSERT( s_instance );

        delete s_instance;
        s_instance = NULL;
    }

    IPhoneFlurryAnalytics::IPhoneFlurryAnalytics()
    {}

    IPhoneFlurryAnalytics::~IPhoneFlurryAnalytics()
    {}

    void IPhoneFlurryAnalytics::StartSession( const char* analyticsKey )
    {
        NSString* str = [NSString stringWithUTF8String:analyticsKey];
        
        [Flurry startSession:str];
    }

    void IPhoneFlurryAnalytics::LogEvent( const char* ev )
    {
        NSString* str = [NSString stringWithUTF8String:ev];

        [Flurry logEvent:str];
    }

    void IPhoneFlurryAnalytics::LogEvent( const char* ev, const char* param, const char* value )
    {
        NSString* str = [NSString stringWithUTF8String:ev];

        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:
             [NSString stringWithUTF8String:value],
             [NSString stringWithUTF8String:param],
             nil
        ];
        
        [Flurry logEvent:str withParameters:dict];
    }

    void IPhoneFlurryAnalytics::LogEvent( const char* ev, const EventParamList& params )
    {
        NSString* str = [NSString stringWithUTF8String:ev];

        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        EventParamList::const_iterator it = params.begin();
        EventParamList::const_iterator end = params.end();
        for ( ; it != end; ++it )
        {
            NSString* name = [NSString stringWithUTF8String:it->first.c_str()];
            NSString* value = [NSString stringWithUTF8String:it->second.c_str()];
            [dict setObject:value forKey:name];
        }

        [Flurry logEvent:str withParameters:dict];
    }

    void IPhoneFlurryAnalytics::StartEvent( const char* ev )
    {
        NSString* str = [NSString stringWithUTF8String:ev];
        
        [Flurry logEvent:str withParameters:nil timed:YES];
    }

    void IPhoneFlurryAnalytics::StopEvent( const char* ev )
    {
        NSString* str = [NSString stringWithUTF8String:ev];

        [Flurry endTimedEvent:str withParameters:nil];
    }

    void IPhoneFlurryAnalytics::StopSession()
    {
    }
}

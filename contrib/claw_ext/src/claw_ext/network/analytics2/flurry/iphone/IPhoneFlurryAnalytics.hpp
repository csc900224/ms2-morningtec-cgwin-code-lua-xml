#ifndef __INCLUDED__IPHONE_FLURRY_ANALYTICS_HPP__
#define __INCLUDED__IPHONE_FLURRY_ANALYTICS_HPP__

#include "claw_ext/network/analytics2/flurry/FlurryAnalytics.hpp"

namespace ClawExt
{
    class IPhoneFlurryAnalytics : public FlurryAnalytics
    {
    public:
        IPhoneFlurryAnalytics();
        ~IPhoneFlurryAnalytics();
        
        void StartSession( const char* analyticsKey );
        void StopSession();

        void LogEvent( const char* event );
        void LogEvent( const char* ev, const char* param, const char* value );
        void LogEvent( const char* ev, const EventParamList& params );
        void StartEvent( const char* event );
        void StopEvent( const char* event );
    };
} // namespace ClawExt

#endif // __INCLUDED__IPHONE_FLURRY_ANALYTICS_HPP__

#include "claw/system/android/JniAttach.hpp"
#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/flurry/android/AndroidFlurryAnalytics.hpp"

namespace ClawExt
{
    static AndroidFlurryAnalytics* s_instance = NULL;

    /*static*/ FlurryAnalytics* FlurryAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidFlurryAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void FlurryAnalytics::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    AndroidFlurryAnalytics::AndroidFlurryAnalytics()
    {}

    void AndroidFlurryAnalytics::StartSession( const char* analyticsKey )
    {
        CLAW_MSG( "AndroidFlurryAnalytics::StartSession()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring key;

        key = Claw::JniAttach::GetStringFromChars( env, analyticsKey );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "startSession", "(Ljava/lang/String;)V", key );

        Claw::JniAttach::ReleaseString( env, key );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidFlurryAnalytics::StopSession()
    {
        CLAW_MSG( "AndroidFlurryAnalytics::StopSession()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "stopSession", "()V" );

        Claw::JniAttach::Detach( attached );
    }

    void AndroidFlurryAnalytics::LogEvent( const char* event )
    {
        CLAW_MSG( "AndroidFlurryAnalytics::LogEvent() " << event );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring eventStr;

        CLAW_ASSERT( event );
        if( event )
            eventStr = Claw::JniAttach::GetStringFromChars( env, event );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "logEvent", "(Ljava/lang/String;)V", eventStr );

        Claw::JniAttach::ReleaseString( env, eventStr );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidFlurryAnalytics::LogEvent( const char* ev, const char* param, const char* value )
    {
        EventParamList params;
        params.push_back( EventParam( param, value ) );
        LogEvent( ev, params );
    }

    void AndroidFlurryAnalytics::LogEvent( const char* ev, const EventParamList& params )
    {
        CLAW_MSG( "AndroidFlurryAnalytics::LogEvent(params) " << ev );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring eventStr;

        CLAW_ASSERT( ev );
        if( ev )
            eventStr = Claw::JniAttach::GetStringFromChars( env, ev );

        EventParamList::const_iterator it = params.begin();
        EventParamList::const_iterator end = params.end();
        for( ; it != end; ++it )
        {
            jstring param = Claw::JniAttach::GetStringFromChars( env, it->first.c_str() );
            jstring value = Claw::JniAttach::GetStringFromChars( env, it->second.c_str() );

            Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "pushEventParams", "(Ljava/lang/String;Ljava/lang/String;)V", param, value );

            Claw::JniAttach::ReleaseString( env, param );
            Claw::JniAttach::ReleaseString( env, value );
        }
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "logEventWithParams", "(Ljava/lang/String;)V", eventStr );

        Claw::JniAttach::ReleaseString( env, eventStr );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidFlurryAnalytics::StartEvent( const char* event )
    {
        CLAW_MSG( "AndroidFlurryAnalytics::StartEvent() " << event );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring eventStr;

        CLAW_ASSERT( event );
        if( event )
            eventStr = Claw::JniAttach::GetStringFromChars( env, event );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "startEvent", "(Ljava/lang/String;)V", eventStr );

        Claw::JniAttach::ReleaseString( env, eventStr );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidFlurryAnalytics::StopEvent( const char* event )
    {
        CLAW_MSG( "AndroidFlurryAnalytics::StopEvent() " << event );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring eventStr;

        CLAW_ASSERT( event );
        if( event )
            eventStr = Claw::JniAttach::GetStringFromChars( env, event );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/flurry/FlurryAnalytics", "stopEvent", "(Ljava/lang/String;)V", eventStr );

        Claw::JniAttach::ReleaseString( env, eventStr );
        Claw::JniAttach::Detach( attached );
    }
}

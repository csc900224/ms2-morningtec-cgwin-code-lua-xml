package com.gamelion.flurry;

import android.util.Log;
import java.util.HashMap;
import com.flurry.android.FlurryAgent;

import com.Claw.Android.ClawActivityCommon;

public class FlurryAnalytics
{
    public static final boolean DEBUG = false;

    public static final String TAG = "FlurryAnalytics";
    private static HashMap<String, String> paramsMap = new HashMap<String, String>();

    public static void startSession(String key)
    {
        if (DEBUG) Log.i(TAG, "startSession() with key: " + key);
        FlurryAgent.onStartSession(ClawActivityCommon.mActivity, key);
    }

    public static void stopSession()
    {
        if (DEBUG) Log.i(TAG, "stopSession()");
        FlurryAgent.onEndSession(ClawActivityCommon.mActivity);
    }

    public static void logEvent(String event)
    {
        if (DEBUG) Log.i(TAG, "logEvent(): " + event);
        FlurryAgent.logEvent(event);
    }
    
    public static void pushEventParams(String param, String value)
    {
        paramsMap.put( new String(param), new String(value) );
    }
    
    public static void logEventWithParams(String event)
    {
        HashMap<String, String> mapToSend = new HashMap<String, String>(paramsMap);
        paramsMap.clear();
        
        if (DEBUG) Log.i(TAG, "logEventWithParams(): " + event + " " + mapToSend);
        FlurryAgent.logEvent(event, mapToSend);
    }
    
    public static void startEvent(String event)
    {
        if (DEBUG) Log.i(TAG, "startEvent(): " + event);
        FlurryAgent.logEvent(event, true);
    }
    
    public static void stopEvent(String event)
    {
        if (DEBUG) Log.i(TAG, "stopEvent(): " + event);
        FlurryAgent.endTimedEvent(event);
    }
}
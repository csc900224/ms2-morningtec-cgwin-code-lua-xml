#ifndef __INCLUDED__ANDROID_FLURRY_ANALYTICS_HPP__
#define __INCLUDED__ANDROID_FLURRY_ANALYTICS_HPP__

#include "claw_ext/network/analytics2/flurry/FlurryAnalytics.hpp"

namespace ClawExt
{
    class AndroidFlurryAnalytics : public FlurryAnalytics
    {
    public:
        AndroidFlurryAnalytics();

        virtual void StartSession( const char* analyticsKey );
        virtual void StopSession();

        virtual void LogEvent( const char* event );
        virtual void LogEvent( const char* ev, const char* param, const char* value );
        virtual void LogEvent( const char* ev, const EventParamList& params );
        virtual void StartEvent( const char* event );
        virtual void StopEvent( const char* event );
    };
} // namespace ClawExt

#endif // __INCLUDED__ANDROID_FLURRY_ANALYTICS_HPP__

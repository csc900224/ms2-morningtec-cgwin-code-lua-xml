#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/flurry/dummy/DummyFlurryAnalytics.hpp"

namespace ClawExt
{
    static DummyFlurryAnalytics* s_instance = NULL;

    FlurryAnalytics* FlurryAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyFlurryAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void FlurryAnalytics::Release()
    {
        if( s_instance ) 
        {
            delete s_instance;
            s_instance = NULL;
        }
    }

    DummyFlurryAnalytics::DummyFlurryAnalytics()
    {}

    DummyFlurryAnalytics::~DummyFlurryAnalytics()
    {}

    void DummyFlurryAnalytics::StartSession( const char* key )
    {
        CLAW_MSG( "FLURRY ANALYTICS session start wih key: " << key );
    }

    void DummyFlurryAnalytics::StopSession()
    {
        CLAW_MSG( "FLURRY ANALYTICS session stop" );
    }

    void DummyFlurryAnalytics::LogEvent( const char* ev )
    {
        CLAW_MSG( "FLURRY ANALYTICS LogEvent: " << ev );
    }

    void DummyFlurryAnalytics::LogEvent( const char* ev, const char* param, const char* value )
    {
        CLAW_MSG( "FLURRY ANALYTICS LogEvent: " << ev );
        CLAW_MSG( "    - " << param << " = " << value );
    }

    void DummyFlurryAnalytics::LogEvent( const char* ev, const EventParamList& params )
    {
        CLAW_MSG( "FLURRY ANALYTICS LogEvent: " << ev );

        EventParamList::const_iterator it = params.begin();
        EventParamList::const_iterator end = params.end();
        for ( ; it != end; ++it )
        {
            CLAW_MSG( "    - " << it->first << " = " << it->second );
        }
    }

    void DummyFlurryAnalytics::StartEvent( const char* ev )
    {
        CLAW_MSG( "FLURRY ANALYTICS StartEvent: " << ev );
    }

    void DummyFlurryAnalytics::StopEvent( const char* ev )
    {
        CLAW_MSG( "FLURRY ANALYTICS StopEvent: " << ev );
    }
}

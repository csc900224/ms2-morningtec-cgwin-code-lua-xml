#ifndef __INCLUDED__DUMMY_FLURRY_ANALYTICS_HPP__
#define __INCLUDED__DUMMY_FLURRY_ANALYTICS_HPP__

#include "claw_ext/network/analytics2/flurry/FlurryAnalytics.hpp"

namespace ClawExt
{
    class DummyFlurryAnalytics : public FlurryAnalytics
    {
    public:
        DummyFlurryAnalytics();
        ~DummyFlurryAnalytics();

        void StartSession( const char* key );
        void StopSession();

        void LogEvent( const char* ev );
        void LogEvent( const char* ev, const char* param, const char* value );
        void LogEvent( const char* ev, const EventParamList& params );
        void StartEvent( const char* ev );
        void StopEvent( const char* ev );
    };
} // namespace ClawExt

#endif // __INCLUDED__DUMMY_FLURRY_ANALYTICS_HPP__

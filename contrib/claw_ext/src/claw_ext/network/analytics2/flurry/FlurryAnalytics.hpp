#ifndef __INCLUDED__FLURRY_ANALYTICS_HPP__
#define __INCLUDED__FLURRY_ANALYTICS_HPP__

#include <list>

#include "claw/base/String.hpp"

namespace ClawExt
{
    class FlurryAnalytics
    {
    public:
        typedef std::pair<Claw::NarrowString, Claw::NarrowString> EventParam;
        typedef std::list<EventParam> EventParamList;

        virtual void StartSession( const char* key ) = 0;
        virtual void StopSession() = 0;

        virtual void LogEvent( const char* ev ) = 0;
        virtual void LogEvent( const char* ev, const char* param, const char* value ) = 0;
        virtual void LogEvent( const char* ev, const EventParamList& params ) = 0;
        virtual void StartEvent( const char* ev ) = 0;
        virtual void StopEvent( const char* ev ) = 0;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Analytics object implementation.
        */
        static FlurryAnalytics* QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void Release();
    };
} // namespace ClawExt

#endif // __INCLUDED__FLURRY_ANALYTICS_HPP__

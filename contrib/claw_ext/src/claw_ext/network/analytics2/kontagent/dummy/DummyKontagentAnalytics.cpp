#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/kontagent/dummy/DummyKontagentAnalytics.hpp"

namespace ClawExt
{
    static DummyKontagentAnalytics* s_instance = NULL;

    KontagentAnalytics* KontagentAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyKontagentAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void KontagentAnalytics::Release()
    {
        if( s_instance ) 
        {
            delete s_instance;
            s_instance = NULL;
        }
    }

    DummyKontagentAnalytics::DummyKontagentAnalytics()
    {}

    DummyKontagentAnalytics::~DummyKontagentAnalytics()
    {}

    void DummyKontagentAnalytics::SendDeviceInfo( const char* build )
    {
        CLAW_MSG( "KONTAGENT ANALYTICS initialize for build: " << build );
    }

    void DummyKontagentAnalytics::StartSession( const char* apiKey, bool testMode )
    {
        CLAW_MSG( "KONTAGENT ANALYTICS start session with key: " << apiKey << " testMode: " << testMode );
    }

    void DummyKontagentAnalytics::StopSession()
    {
        CLAW_MSG( "KONTAGENT ANALYTICS stop session" );
    }

    void DummyKontagentAnalytics::LogEvent( const char* st1, const char* st2, const char* st3, const char* name, Claw::Int32 value, Claw::UInt8 lvl, const char* json )
    {
        CLAW_MSG( "KONTAGENT ANALYTICS log event: " << st1 << "/" << st2 << "/" << st3 << "/" << name <<" with value: " << value << " lvl: " << lvl << " json: " << json );
    }

    void DummyKontagentAnalytics::TrackRevenue( int cents, const char* revenueType )
    {
        CLAW_MSG( "KONTAGENT ANALYTICS log revenue of type: " << revenueType << " cents: " << cents );
    }

}

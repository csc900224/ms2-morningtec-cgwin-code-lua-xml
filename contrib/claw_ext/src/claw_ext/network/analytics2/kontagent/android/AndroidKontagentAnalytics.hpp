#ifndef __INCLUDED__ANDROID_KONTAGENT_ANALYTICS_HPP__
#define __INCLUDED__ANDROID_KONTAGENT_ANALYTICS_HPP__

#include "claw_ext/network/analytics2/kontagent/KontagentAnalytics.hpp"

namespace ClawExt
{
    class AndroidKontagentAnalytics : public KontagentAnalytics
    {
    public:
        AndroidKontagentAnalytics();
        ~AndroidKontagentAnalytics();

        void SendDeviceInfo( const char* build );

        void StartSession( const char* apiKey, bool testMode = false );
        void StopSession();

        void LogEvent( const char* st1, const char* st2, const char* st3, const char* name, Claw::Int32 value = 1, Claw::UInt8 lvl = 1, const char* json = NULL );

    protected:
        void TrackRevenue( int cents, const char* revenueType );
    };
}

#endif // __INCLUDED__ANDROID_KONTAGENT_ANALYTICS_HPP__

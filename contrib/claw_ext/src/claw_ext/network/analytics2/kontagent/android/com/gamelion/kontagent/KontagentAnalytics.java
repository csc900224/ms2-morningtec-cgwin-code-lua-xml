package com.gamelion.kontagent;

import android.util.Log;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONException;

import com.kontagent.Kontagent;
import com.Claw.Android.ClawActivityCommon;

public class KontagentAnalytics
{
    public static final boolean DEBUG = true;

    public static final String TAG = "KontagentAnalytics";

    public static void sendDeviceInfo(String build)
    {
        if (DEBUG) Log.i(TAG, "sendDeviceInfo() with build: " + build);
        
        if (DEBUG) {
            Kontagent.enableDebug();
        } else {
            Kontagent.disableDebug();
        }
        
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("v_maj", build);
        Kontagent.sendDeviceInformation(params);
    }

    public static void startSession(String key, boolean testMode)
    {
        if (DEBUG) Log.i(TAG, "startSession() with key: " + key + " testMode: " + testMode);
        Kontagent.startSession(key, ClawActivityCommon.mActivity, testMode ? Kontagent.TEST_MODE : Kontagent.PRODUCTION_MODE );
    }

    public static void stopSession()
    {
        if (DEBUG) Log.i(TAG, "stopSession()");
        Kontagent.stopSession();
    }
    
    public static void logEvent(String st1, String st2, String st3, String name, int value, int level, String json)
    {
        if (DEBUG) Log.i(TAG, "logEvent(): " + st1 + "/" + st2 + "/" + st3 + "/" + name + " v: " + value + " lvl: " + level + " json: " + json);
        
        HashMap<String, String> params = new HashMap<String, String>();
        if (st1 != null)
            params.put("st1", st1);
        if (st2 != null)
            params.put("st2", st2);
        if (st3 != null)
            params.put("st3", st3);
        params.put("l", Integer.toString(level));
        params.put("v", Integer.toString(value));
        if (json != null)
            params.put("data", json);

        Kontagent.customEvent(name, params);
    }
    
    public static void trackRevenue(int cents, String revenueType)
    {
        if (DEBUG) Log.i(TAG, "trackRevenue(): " + revenueType + " cents: " + cents);
        
        HashMap<String, String> params = new HashMap<String, String>();
        if (revenueType != null)
            params.put("tu", revenueType);

        Kontagent.revenueTracking(cents, params);
    }
}
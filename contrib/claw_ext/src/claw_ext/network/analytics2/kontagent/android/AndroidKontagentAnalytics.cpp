#include "claw/system/android/JniAttach.hpp"
#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/kontagent/android/AndroidKontagentAnalytics.hpp"

namespace ClawExt
{
    static AndroidKontagentAnalytics* s_instance = NULL;

    KontagentAnalytics* KontagentAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidKontagentAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void KontagentAnalytics::Release()
    {
        if( s_instance ) 
        {
            delete s_instance;
            s_instance = NULL;
        }
    }

    AndroidKontagentAnalytics::AndroidKontagentAnalytics()
    {}

    AndroidKontagentAnalytics::~AndroidKontagentAnalytics()
    {}

    void AndroidKontagentAnalytics::SendDeviceInfo( const char* build )
    {
        CLAW_MSG( "AndroidKontagentAnalytics::Initialize()" );
        
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jbuild;

        jbuild = Claw::JniAttach::GetStringFromChars( env, build );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/kontagent/KontagentAnalytics", "sendDeviceInfo", "(Ljava/lang/String;)V", jbuild );

        Claw::JniAttach::ReleaseString( env, jbuild );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidKontagentAnalytics::StartSession( const char* apiKey, bool testMode )
    {
        CLAW_MSG( "AndroidKontagentAnalytics::StartSession()" );
        
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring japiKey;

        japiKey = Claw::JniAttach::GetStringFromChars( env, apiKey );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/kontagent/KontagentAnalytics", "startSession", "(Ljava/lang/String;Z)V", japiKey, jboolean(testMode) );

        Claw::JniAttach::ReleaseString( env, japiKey );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidKontagentAnalytics::StopSession()
    {
        CLAW_MSG( "AndroidKontagentAnalytics::StopSession()" );
        
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );


        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/kontagent/KontagentAnalytics", "stopSession", "()V" );

        Claw::JniAttach::Detach( attached );
    }

    void AndroidKontagentAnalytics::LogEvent( const char* st1, const char* st2, const char* st3, const char* name, Claw::Int32 value, Claw::UInt8 lvl, const char* json )
    {
        CLAW_MSG( "AndroidKontagentAnalytics::LogEvent()" );
        
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jst1 = NULL;
        jstring jst2 = NULL;
        jstring jst3 = NULL;
        jstring jname = NULL;
        jstring jjson = NULL;

        if(st1)
            jst1 = Claw::JniAttach::GetStringFromChars( env, st1 );
        if(st2)
            jst2 = Claw::JniAttach::GetStringFromChars( env, st2 );
        if(st3)
            jst3 = Claw::JniAttach::GetStringFromChars( env, st3 );
            
        jname = Claw::JniAttach::GetStringFromChars( env, name );
        
        if(json)
            jjson = Claw::JniAttach::GetStringFromChars( env, json );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/kontagent/KontagentAnalytics", "logEvent", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V", jst1, jst2, jst3, jname, jint(value), jint(lvl), jjson );

        Claw::JniAttach::ReleaseString( env, jst1 );
        Claw::JniAttach::ReleaseString( env, jst2 );
        Claw::JniAttach::ReleaseString( env, jst3 );
        Claw::JniAttach::ReleaseString( env, jname );
        Claw::JniAttach::ReleaseString( env, jjson );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidKontagentAnalytics::TrackRevenue( int cents, const char* revenueType )
    {
        CLAW_MSG( "AndroidKontagentAnalytics::TrackRevenue()" );
        
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jrevenueType = NULL;

        if (revenueType)
            jrevenueType = Claw::JniAttach::GetStringFromChars( env, revenueType );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/kontagent/KontagentAnalytics", "trackRevenue", "(ILjava/lang/String;)V", jint(cents), jrevenueType );

        Claw::JniAttach::ReleaseString( env, jrevenueType );
        Claw::JniAttach::Detach( attached );
    }
}

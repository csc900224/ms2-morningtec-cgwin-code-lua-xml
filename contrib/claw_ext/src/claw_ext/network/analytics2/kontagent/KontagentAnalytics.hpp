#ifndef __INCLUDED__KONTAGENT_ANALYTICS_HPP__
#define __INCLUDED__KONTAGENT_ANALYTICS_HPP__

#include <list>

#include "claw/base/String.hpp"

namespace ClawExt
{

    class KontagentAnalytics
    {
    public:
        enum RevenueType 
        {
            RT_DIRECT,
            RT_INDIRECT,
            RT_ADVERTISEMENT,
            RT_CREDITS,
            RT_OTHER,
            RT_NONE
        };
        
        virtual void StartSession( const char* apiKey, bool testMode = false ) = 0;
        virtual void StopSession() = 0;

        // SendDeviceInfo should be called only after starting session
        virtual void SendDeviceInfo( const char* build ) = 0;

        virtual void LogEvent( const char* st1, const char* st2, const char* st3, const char* name, Claw::Int32 value = 1, Claw::UInt8 lvl = 1, const char* json = NULL ) = 0;
        
        void LogEvent( const char* st1, const char* st2, const char* name, Claw::Int32 value = 1, Claw::UInt8 lvl = 1, const char* json = NULL ) 
        {
            LogEvent(st1, st2, NULL, name, value, lvl, json);
        }

        void LogEvent( const char* st1, const char* name, Claw::Int32 value = 1, Claw::UInt8 lvl = 1, const char* json = NULL )
        {
            LogEvent(st1, NULL, NULL, name, value, lvl, json);
        }

        void LogEvent( const char* name, Claw::Int32 value = 1, Claw::UInt8 lvl = 1, const char* json = NULL )
        {
            LogEvent(NULL, NULL, NULL, name, value, lvl, json);
        }

        void TrackRevenue( int cents, RevenueType revenueType = RT_NONE );

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Analytics object implementation.
        */
        static KontagentAnalytics* QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void Release();

    protected:
        virtual void TrackRevenue( int cents, const char* revenueType ) = 0;
    };

}

#endif // __INCLUDED__KONTAGENT_ANALYTICS_HPP__

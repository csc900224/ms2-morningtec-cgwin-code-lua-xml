#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/kontagent/iphone/IphoneKontagentAnalytics.hpp"

namespace ClawExt
{
    static IphoneKontagentAnalytics* s_instance = NULL;

    KontagentAnalytics* KontagentAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IphoneKontagentAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void KontagentAnalytics::Release()
    {
        if( s_instance ) 
        {
            delete s_instance;
            s_instance = NULL;
        }
    }

    IphoneKontagentAnalytics::IphoneKontagentAnalytics()
    : kontagentSession(NULL)
    {}

    IphoneKontagentAnalytics::~IphoneKontagentAnalytics()
    {}

    void IphoneKontagentAnalytics::SendDeviceInfo( const char* build )
    {
        KTParamMap* paramMap = [[[KTParamMap alloc] init] autorelease];
        [paramMap put:@"v_maj" value:[[NSString stringWithUTF8String:build] retain]];
        [kontagentSession sendDeviceInformation:paramMap];
    }

    void IphoneKontagentAnalytics::StartSession( const char* apiKey, bool testMode )
    {
        if (!kontagentSession)
        {
            if (testMode)
            {
                [Kontagent setMode:kKontagentSDKMode_TEST];
            }
            else
            {
                [Kontagent setMode:kKontagentSDKMode_PRODUCTION];
            }
            
            kontagentSession = [Kontagent createSession:[NSString stringWithUTF8String:apiKey] senderId:nil];
        }
        [kontagentSession start];
    }

    void IphoneKontagentAnalytics::StopSession()
    {
        [kontagentSession stop];
    }

    void IphoneKontagentAnalytics::LogEvent( const char* st1, const char* st2, const char* st3, const char* name, Claw::Int32 value, Claw::UInt8 lvl, const char* json )
    {
        KTParamMap* paramMap = [[[KTParamMap alloc] init] autorelease];
        
        if(st1)
            [paramMap put:@"st1" value:[NSString stringWithUTF8String:st1]];
        if(st2)
            [paramMap put:@"st2" value:[NSString stringWithUTF8String:st2]];
        if(st3)
            [paramMap put:@"st3" value:[NSString stringWithUTF8String:st3]];
        if(json)
            [paramMap put:@"data" value:[NSString stringWithUTF8String:json]];
            
        [paramMap put:@"l" value:[NSString stringWithFormat:@"%d",lvl]];
        [paramMap put:@"v" value:[NSString stringWithFormat:@"%d",value]];

        [kontagentSession customEvent:[NSString stringWithUTF8String:name] optionalParams:paramMap];
    }

    void IphoneKontagentAnalytics::TrackRevenue( int cents, const char* revenueType )
    {
        [kontagentSession revenueTracking:cents optionalParams:nil];
    }

}

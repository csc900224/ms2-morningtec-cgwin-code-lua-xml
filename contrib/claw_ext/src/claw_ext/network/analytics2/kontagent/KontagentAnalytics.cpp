#include "claw_ext/network/analytics2/kontagent/KontagentAnalytics.hpp"

namespace ClawExt
{
    static const char* REVENUE_TYPE_STRINGS[] =
    {
        "direct",
        "indirect",
        "advertisement",
        "credits",
        "other",
        NULL
    };

    void KontagentAnalytics::TrackRevenue( int cents, RevenueType revenueType )
    {
        TrackRevenue(cents, REVENUE_TYPE_STRINGS[ revenueType ]);
    }

}

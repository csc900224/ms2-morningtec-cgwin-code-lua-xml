#ifndef __INCLUDED__GAME_ANALYTICS_HPP__
#define __INCLUDED__GAME_ANALYTICS_HPP__

#include <list>

#include "claw/base/String.hpp"

namespace ClawExt
{
    class GameAnalytics
    {
    public:
        struct AreaInfo
        {
            AreaInfo( const char* area = "", float x = 0.f, float y = 0.f, float z = 0.f ) : area( area ), x( x ), y( y ), z( z ) {}

            const char* area;
            float x;
            float y;
            float z;
        };

        virtual void Initialize( const char* key, const char* secret, const char* build ) = 0;
        virtual void ClearCache() = 0;

        virtual void StartSession() = 0;
        virtual void StopSession() = 0;

        virtual void LogBusinessEvent( const char* ev, const char* currency, int amount, const AreaInfo& area = AreaInfo() ) = 0;
        virtual void LogQualityEvent( const char* ev, const char* message, const AreaInfo& area = AreaInfo() ) = 0;
        virtual void LogDesignEvent( const char* ev, float value, const AreaInfo& area = AreaInfo() ) = 0;

        //! Retreive platfrom dependend implementation.
        /**
        * This method should be implemented and linked once in platform dependend object,
        * returning appropriate Analytics object implementation.
        */
        static GameAnalytics* QueryInterface();

        //! Release platform specific implementation.
        /**
        * Call destructors, release memory, make cleanup.
        */
        static void Release();
    };
} // namespace ClawExt

#endif // __INCLUDED__GAME_ANALYTICS_HPP__

#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/gameanalytics/dummy/DummyGameAnalytics.hpp"

namespace ClawExt
{
    static DummyGameAnalytics* s_instance = NULL;

    GameAnalytics* GameAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new DummyGameAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void GameAnalytics::Release()
    {
        if( s_instance ) 
        {
            delete s_instance;
            s_instance = NULL;
        }
    }

    DummyGameAnalytics::DummyGameAnalytics()
    {}

    DummyGameAnalytics::~DummyGameAnalytics()
    {}

    void DummyGameAnalytics::Initialize( const char* key, const char* secret, const char* build )
    {
        CLAW_MSG( "GAME ANALYTICS session initialized with key: " << key << " secret: " << secret << " build: " << build );
    }

    void DummyGameAnalytics::ClearCache()
    {
        CLAW_MSG( "GAME ANALYTICS clear chahe" );
    }

    void DummyGameAnalytics::StartSession()
    {
        CLAW_MSG( "GAME ANALYTICS session start" );
    }

    void DummyGameAnalytics::StopSession()
    {
        CLAW_MSG( "GAME ANALYTICS session stop" );
    }

    void DummyGameAnalytics::LogBusinessEvent( const char* ev, const char* currency, int amount, const AreaInfo& area )
    {
        CLAW_MSG( "GAME ANALYTICS LogBusinessEvent: " << ev << " currency: " << currency << " amount: " << amount << " area: " << area.area << " pos: (" << area.x << ", " << area.y << ", " << area.z << ")" );
    }

    void DummyGameAnalytics::LogQualityEvent( const char* ev, const char* message, const AreaInfo& area )
    {
        CLAW_MSG( "GAME ANALYTICS LogQualityEvent: " << ev << " message: " << message << " area: " << area.area << " pos: (" << area.x << ", " << area.y << ", " << area.z << ")" );
    }

    void DummyGameAnalytics::LogDesignEvent( const char* ev, float value, const AreaInfo& area )
    {
        CLAW_MSG( "GAME ANALYTICS LogDesignEvent: " << ev << " value: " << value << " area: " << area.area << " pos: (" << area.x << ", " << area.y << ", " << area.z << ")" );
    }
}

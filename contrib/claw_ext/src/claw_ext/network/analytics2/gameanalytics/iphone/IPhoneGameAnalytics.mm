#include "claw_ext/network/analytics2/gameanalytics/iphone/IPhoneGameAnalytics.hpp"
#include "claw/base/Errors.hpp"

#import "GameAnalytics.h"
#import "QuartzCore/CABase.h"

namespace ClawExt
{
    static IPhoneGameAnalytics* s_instance = NULL;

    GameAnalytics* GameAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new IPhoneGameAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    void GameAnalytics::Release()
    {
        CLAW_ASSERT( s_instance );

        delete s_instance;
        s_instance = NULL;
    }

    IPhoneGameAnalytics::IPhoneGameAnalytics()
        : m_stopSessionMs(0)
    {}

    IPhoneGameAnalytics::~IPhoneGameAnalytics()
    {}

    void IPhoneGameAnalytics::Initialize( const char* key, const char* secret, const char* build )
    {
        [::GameAnalytics setGameKey:[[NSString stringWithUTF8String:key] retain]
                        secretKey:[[NSString stringWithUTF8String:secret] retain]
                            build:[[NSString stringWithUTF8String:build] retain]];
        
        [::GameAnalytics setArchiveDataEnabled:YES];
        [::GameAnalytics setArchiveDataLimit:500];
        [::GameAnalytics setBatchRequestsEnabled:NO];
    #ifdef _DEBUG
        [::GameAnalytics setDebugLogEnabled:YES];
    #else
        [::GameAnalytics setDebugLogEnabled:NO];
    #endif
    }

    void IPhoneGameAnalytics::ClearCache()
    {
    }

    void IPhoneGameAnalytics::StartSession()
    {
        if (CACurrentMediaTime() - m_stopSessionMs >= 5 * 60.)
        {
            [::GameAnalytics updateSessionID];
        }
    }

    void IPhoneGameAnalytics::LogBusinessEvent( const char* ev, const char* currency, int amount, const AreaInfo& area )
    {
        [::GameAnalytics logBusinessDataEvent:[NSString stringWithUTF8String:ev]
                                 withParams:@{@"currency" : [NSString stringWithUTF8String:currency],
                                                @"amount" : [NSNumber numberWithInt:amount],
                                                  @"area" : [NSString stringWithUTF8String:area.area],
                                                     @"x" : [NSNumber numberWithFloat:area.x],
                                                     @"y" : [NSNumber numberWithFloat:area.y],
                                                     @"z" : [NSNumber numberWithFloat:area.z]}];
    }

    void IPhoneGameAnalytics::LogQualityEvent( const char* ev, const char* message, const AreaInfo& area )
    {
        [::GameAnalytics logQualityAssuranceDataEvent:[NSString stringWithUTF8String:ev]
                                         withParams:@{@"message": [NSString stringWithUTF8String:message],
                                                        @"area" : [NSString stringWithUTF8String:area.area],
                                                           @"x" : [NSNumber numberWithFloat:area.x],
                                                           @"y" : [NSNumber numberWithFloat:area.y],
                                                           @"z" : [NSNumber numberWithFloat:area.z]}];
    }

    void IPhoneGameAnalytics::LogDesignEvent( const char* ev, float value, const AreaInfo& area )
    {
        [::GameAnalytics logGameDesignDataEvent:[NSString stringWithUTF8String:ev]
                                   withParams:@{@"value": [NSNumber numberWithFloat:value],
                                                @"area" : [NSString stringWithUTF8String:area.area],
                                                   @"x" : [NSNumber numberWithFloat:area.x],
                                                   @"y" : [NSNumber numberWithFloat:area.y],
                                                   @"z" : [NSNumber numberWithFloat:area.z]}];
    }

    void IPhoneGameAnalytics::StopSession()
    {
        m_stopSessionMs = CACurrentMediaTime();
    }
}

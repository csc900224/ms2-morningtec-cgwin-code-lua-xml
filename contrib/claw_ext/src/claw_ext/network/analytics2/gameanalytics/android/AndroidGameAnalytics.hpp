#ifndef __INCLUDED__ANDROID_GAME_ANALYTICS_HPP__
#define __INCLUDED__ANDROID_GAME_ANALYTICS_HPP__

#include "claw_ext/network/analytics2/gameanalytics/GameAnalytics.hpp"

namespace ClawExt
{
    class AndroidGameAnalytics : public GameAnalytics
    {
    public:
        AndroidGameAnalytics();

        void Initialize( const char* key, const char* secret, const char* build );
        void ClearCache();

        void StartSession();
        void StopSession();

        void LogBusinessEvent( const char* ev, const char* currency, int amount, const AreaInfo& area = AreaInfo() );
        void LogQualityEvent( const char* ev, const char* message, const AreaInfo& area = AreaInfo() );
        void LogDesignEvent( const char* ev, float value, const AreaInfo& area = AreaInfo() );
    };
} // namespace ClawExt

#endif // __INCLUDED__ANDROID_GAME_ANALYTICS_HPP__

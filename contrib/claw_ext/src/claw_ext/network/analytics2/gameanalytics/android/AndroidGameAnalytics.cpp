#include "claw/system/android/JniAttach.hpp"
#include "claw/base/Errors.hpp"

#include "claw_ext/network/analytics2/gameanalytics/android/AndroidGameAnalytics.hpp"

namespace ClawExt
{
    static AndroidGameAnalytics* s_instance = NULL;

    /*static*/ GameAnalytics* GameAnalytics::QueryInterface()
    {
        if( !s_instance )
            s_instance = new AndroidGameAnalytics();

        CLAW_ASSERT( s_instance );
        return s_instance;
    }

    /*static*/ void GameAnalytics::Release()
    {
        CLAW_ASSERT( s_instance );
        delete s_instance;
        s_instance = NULL;
    }

    AndroidGameAnalytics::AndroidGameAnalytics()
    {}

    void AndroidGameAnalytics::Initialize( const char* key, const char* secret, const char* build )
    {
        CLAW_MSG( "AndroidGameAnalytics::Initialize()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring keyStr;
        jstring secretStr;
        jstring buildStr;

        CLAW_ASSERT( key && secret && build );
        if( key && secret && build)
        {
            keyStr      = Claw::JniAttach::GetStringFromChars( env, key );
            secretStr   = Claw::JniAttach::GetStringFromChars( env, secret );
            buildStr    = Claw::JniAttach::GetStringFromChars( env, build );
        }

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/gameanalytics/GameAnalytics", "initialize", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", keyStr, secretStr, buildStr );

        Claw::JniAttach::ReleaseString( env, keyStr );
        Claw::JniAttach::ReleaseString( env, secretStr );
        Claw::JniAttach::ReleaseString( env, buildStr );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidGameAnalytics::ClearCache()
    {
        CLAW_MSG( "AndroidGameAnalytics::ClearCache()" );
        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );
        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/gameanalytics/GameAnalytics", "clearCache", "()V" );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidGameAnalytics::StartSession()
    {
        CLAW_MSG( "AndroidGameAnalytics::StartSession()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/gameanalytics/GameAnalytics", "startSession", "()V" );

        Claw::JniAttach::Detach( attached );
    }

    void AndroidGameAnalytics::StopSession()
    {
        CLAW_MSG( "AndroidGameAnalytics::StopSession()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/gameanalytics/GameAnalytics", "stopSession", "()V" );

        Claw::JniAttach::Detach( attached );
    }

    void AndroidGameAnalytics::LogBusinessEvent( const char* ev, const char* currency, int amount, const AreaInfo& area )
    {
        CLAW_MSG( "AndroidGameAnalytics::LogBusinessEvent() ev: " << ev << " currency: " << currency << " amount: " << amount << " area: " << area.area << " pos: (" << area.x << ", " << area.y << ", " << area.z << ")" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jev         = Claw::JniAttach::GetStringFromChars( env, ev );
        jstring jcurrency   = Claw::JniAttach::GetStringFromChars( env, currency );
        jstring jarea       = Claw::JniAttach::GetStringFromChars( env, area.area );

        Claw::JniAttach::StaticVoidMethodCall( 
            env, 
            "com/gamelion/gameanalytics/GameAnalytics",
            "logBusinessEvent",
            "(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;FFF)V",
            jev,
            jcurrency,
            jint(amount),
            jarea,
            jfloat(area.x),
            jfloat(area.y),
            jfloat(area.z)
        );

        Claw::JniAttach::ReleaseString( env, jev );
        Claw::JniAttach::ReleaseString( env, jcurrency );
        Claw::JniAttach::ReleaseString( env, jarea );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidGameAnalytics::LogQualityEvent( const char* ev, const char* message, const AreaInfo& area )
    {
        CLAW_MSG( "AndroidGameAnalytics::LogQualityEvent() ev: " << ev << " message: " << message << " area: " << area.area << " pos: (" << area.x << ", " << area.y << ", " << area.z << ")" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jev         = Claw::JniAttach::GetStringFromChars( env, ev );
        jstring jmessage    = Claw::JniAttach::GetStringFromChars( env, message );
        jstring jarea       = Claw::JniAttach::GetStringFromChars( env, area.area );

        Claw::JniAttach::StaticVoidMethodCall( 
            env,
            "com/gamelion/gameanalytics/GameAnalytics",
            "logQualityEvent",
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FFF)V",
            jev,
            jmessage,
            jarea,
            jfloat(area.x),
            jfloat(area.y),
            jfloat(area.z)
        );

        Claw::JniAttach::ReleaseString( env, jev );
        Claw::JniAttach::ReleaseString( env, jmessage );
        Claw::JniAttach::ReleaseString( env, jarea );
        Claw::JniAttach::Detach( attached );
    }

    void AndroidGameAnalytics::LogDesignEvent( const char* ev, float value, const AreaInfo& area )
    {
        CLAW_MSG( "AndroidGameAnalytics::LogDesignEvent() ev: " << ev << " value: " << value << " area: " << area.area << " pos: (" << area.x << ", " << area.y << ", " << area.z << ")" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring jev     = Claw::JniAttach::GetStringFromChars( env, ev );
        jstring jarea   = Claw::JniAttach::GetStringFromChars( env, area.area );

        Claw::JniAttach::StaticVoidMethodCall( 
            env,
            "com/gamelion/gameanalytics/GameAnalytics",
            "logDesignEvent",
            "(Ljava/lang/String;FLjava/lang/String;FFF)V",
            jev,
            jfloat(value),
            jarea,
            jfloat(area.x),
            jfloat(area.y),
            jfloat(area.z)
        );

        Claw::JniAttach::ReleaseString( env, jev );
        Claw::JniAttach::Detach( attached );
    }
}
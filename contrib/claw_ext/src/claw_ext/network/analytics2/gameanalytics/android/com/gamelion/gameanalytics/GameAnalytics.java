package com.gamelion.gameanalytics;

import android.util.Log;
import com.Claw.Android.ClawActivityCommon;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class GameAnalytics
{
    private static final String TAG = "GameAnalytics";
    private static final boolean DEBUG = false;

    private static boolean s_sessionActive = false;

    private static String DB_NAME = "GameAnalytics";
    private static String DB_TABLE_NAME = "events";
    
    public static void startSession()
    {
        if( !s_sessionActive )
        {
            if (DEBUG) Log.i(TAG, "startSession()"); 
            com.gameanalytics.android.GameAnalytics.startSession(ClawActivityCommon.mActivity);
            s_sessionActive = true;
        }
    }
    
    public static void initialize(String key, String secret, String build)
    {
        if (DEBUG) Log.i(TAG, "initialize() key: " + key + " secret: " + secret + " build: " + build);
        
        com.gameanalytics.android.GameAnalytics.initialise(ClawActivityCommon.mActivity, secret, key, build);
        com.gameanalytics.android.GameAnalytics.logUnhandledExceptions();
        com.gameanalytics.android.GameAnalytics.setSessionTimeOut(5 * 60 * 1000);
        
        if (DEBUG) com.gameanalytics.android.GameAnalytics.setDebugLogLevel(com.gameanalytics.android.GameAnalytics.VERBOSE); 
    }

    public static void clearCache()
    {
        if (DEBUG) Log.i(TAG, "clearCache()" );
        
        try {
            String dbPath = ClawActivityCommon.mActivity.getDatabasePath(DB_NAME).getAbsolutePath();
            if (DEBUG) Log.i(TAG, "clearCache(): " + DB_NAME + " DB path: " + dbPath);
            
            SQLiteDatabase db = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
            int numDeleted = db.delete(DB_TABLE_NAME, null, null);
            if (DEBUG) Log.i(TAG, "clearCache(): deleted: " + numDeleted + " events from table: " + DB_TABLE_NAME);
            db.close();
        } catch (SQLiteException e) {
            if (DEBUG) Log.i(TAG, "clearCache(): " + DB_NAME + " DB can't be opened!");
        }
        if (DEBUG) Log.i(TAG, "clearCache() - finihsed" );  
    }

    public static void stopSession()
    {
        if( s_sessionActive )
        {
            if (DEBUG) Log.i(TAG, "stopSession()");
            s_sessionActive = false;
            com.gameanalytics.android.GameAnalytics.stopSession();
        }
    }
    
    public static void logBusinessEvent(String eventId, String currency, int amount, String area, float x, float y, float z)
    {
        if (DEBUG) Log.i(TAG, "logBusinessEvent() eventId: " + eventId + " currency: " + currency + " amount: " + amount + " area: " + area + " pos: (" + x + "," + y + "," + z + ")");
        
        if( !s_sessionActive )
        {
            startSession();
        }
        
        com.gameanalytics.android.GameAnalytics.newBusinessEvent(eventId, currency, amount, area, x, y, z );
    }
    
    public static void logQualityEvent(String eventId, String message, String area, float x, float y, float z)
    {
        if (DEBUG) Log.i(TAG, "logQualityEvent() eventId: " + eventId + " message: " + message + " area: " + area + " pos: (" + x + "," + y + "," + z + ")");
        
        if( !s_sessionActive )
        {
            startSession();
        }
        
        com.gameanalytics.android.GameAnalytics.newQualityEvent(eventId, message, area, x, y, z);
    }
    
    public static void logDesignEvent(String eventId, float value, String area, float x, float y, float z)
    {
        if (DEBUG) Log.i(TAG, "logDesignEvent() eventId: " + eventId + " value: " + value + " area: " + area + " pos: (" + x + "," + y + "," + z + ")");
        
        if( !s_sessionActive )
        {
            startSession();
        }
            
        com.gameanalytics.android.GameAnalytics.newDesignEvent(eventId, value, area, x, y, z);
    }
}
#ifndef INCLUDED_URLENCODE_HPP
#define INCLUDED_URLENCODE_HPP

#include "claw/base/String.hpp"

Claw::NarrowString UrlEncode( const Claw::NarrowString& s );
Claw::NarrowString Sanitize( const Claw::NarrowString& s );

#endif

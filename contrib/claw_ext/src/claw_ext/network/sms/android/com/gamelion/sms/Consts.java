package com.gamelion.sms;

/**
 * This class holds global constants that are used throughout the application
 * to support chartboost
 */
public class Consts
{
    public static final boolean DEBUG   = false;
    public final static String TAG      = "SmsService";
}

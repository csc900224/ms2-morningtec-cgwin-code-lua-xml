package com.gamelion.sms;

import android.util.Log;
import com.Claw.Android.ClawActivityCommon;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.gamelion.sms.Consts;

public class SmsService
{
    public static boolean CanSendSms()
    {
        boolean result = ClawActivityCommon.mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        if (Consts.DEBUG) { Log.i( Consts.TAG, "SmsService.CanSendSms(): " + result ); }
        return result;
    }

    public static void SendSms( String message )
    {
        try {
            if (Consts.DEBUG) { Log.i( Consts.TAG, "SendSmsActivity.SendSms(): " + message ); }
            Intent sendIntent = new Intent( Intent.ACTION_SENDTO );
            sendIntent.setData( Uri.parse("smsto:") );
            sendIntent.putExtra( Intent.EXTRA_TEXT, message );
            sendIntent.putExtra( "sms_body", message );

            ClawActivityCommon.mActivity.startActivity( sendIntent );

            // No way to reliably chceck if sms was sent without using android.telephony.gsm.SmsManager (creating own layout for sms sending)
            SmsSent( true );
        } catch( Exception ex ) {
            SmsSent( false );
        }
    }

    public static native void SmsSent(boolean success);
}

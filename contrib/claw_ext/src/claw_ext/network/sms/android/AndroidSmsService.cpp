//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/sms/android/AndroidSmsService.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/network/sms/SmsService.hpp"
#include "claw_ext/network/sms/android/AndroidSmsService.hpp"

// External include
#include "claw/system/android/JniAttach.hpp"

static SmsServiceListener* s_listener = NULL;

AndroidSmsService::AndroidSmsService()
    : m_listener( NULL )
{}

bool AndroidSmsService::CanSendSms() const
{
    CLAW_MSG( "AndroidSmsService::CanSendSms()" );

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );
    bool canSend = Claw::JniAttach::StaticBooleanMethodCall( env, "com/gamelion/sms/SmsService", "CanSendSms", "()Z" );
    Claw::JniAttach::Detach( attached );
    return canSend;
}

void AndroidSmsService::SendSms( const Claw::NarrowString& message )
{
    // Make listener visible to JNIEXPORT
    s_listener = m_listener;

    JNIEnv* env;
    bool attached = Claw::JniAttach::Attach( &env );
    jstring messageString = Claw::JniAttach::GetStringFromChars( env, message.c_str() );

    Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/sms/SmsService", "SendSms", "(Ljava/lang/String;)V", messageString );
    
    Claw::JniAttach::ReleaseString( env, messageString );
    Claw::JniAttach::Detach( attached );
}

void AndroidSmsService::SetListener( SmsServiceListener* listener )
{
    m_listener = listener;
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_sms_SmsService_SmsSent( JNIEnv* env, jclass clazz, jboolean success )
    {
        CLAW_MSG( "Native SmsSent()" );
        if( s_listener )
        {
            s_listener->OnSmsSent( success );
            s_listener = NULL;
        }
    }
}
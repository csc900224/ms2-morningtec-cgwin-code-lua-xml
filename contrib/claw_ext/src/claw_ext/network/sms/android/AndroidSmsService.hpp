//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/sms/android/AndroidSmsService.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__ANDROIDSMSSERVICE_HPP__
#define __INCLUDED__ANDROIDSMSSERVICE_HPP__

#include "claw_ext/network/sms/SmsService.hpp"

class AndroidSmsService : public SmsServiceBase
{
public:
            AndroidSmsService();

    bool    CanSendSms() const;
    void    SendSms( const Claw::NarrowString& message );
    void    SetListener( SmsServiceListener* listener );

private:
    SmsServiceListener* m_listener;

}; // class AndroidSmsService

#endif // __INCLUDED__ANDROIDSMSSERVICE_HPP__

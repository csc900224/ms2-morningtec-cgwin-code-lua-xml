#include "claw_ext/network/sms/SmsService.hpp"
#include "claw_ext/network/sms/dummy/DummySmsService.hpp"

DummySmsService::DummySmsService()
    : m_listener( NULL )
{}

bool DummySmsService::CanSendSms() const
{
    return true;
}

void DummySmsService::SendSms( const Claw::NarrowString& message )
{
    if ( m_listener )
    {
        m_listener->OnSmsSent( true );
    }
}

void DummySmsService::SetListener( SmsServiceListener* listener )
{
    m_listener = listener;
}

#ifndef __INCLUDED__DUMMYSMSSERVICE_HPP__
#define __INCLUDED__DUMMYSMSSERVICE_HPP__

#include "claw_ext/network/sms/SmsService.hpp"

class DummySmsService : public SmsServiceBase
{
public:
    DummySmsService();

    bool CanSendSms() const;
    void SendSms( const Claw::NarrowString& message );
    void SetListener( SmsServiceListener* listener );

private:
    SmsServiceListener* m_listener;

}; // class DummySmsService

#endif // __INCLUDED__DUMMYSMSSERVICE_HPP__

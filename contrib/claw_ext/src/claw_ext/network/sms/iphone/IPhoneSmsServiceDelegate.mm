#import "claw_ext/network/sms/iphone/IPhoneSmsServiceDelegate.h"

#import "claw/application/iphone/IPhoneAppDelegate.h"

#include "claw_ext/network/sms/SmsService.hpp"
#include "claw_ext/network/sms/iphone/IphoneSmsService.hpp"

class IPhoneSmsServiceAdapter
{
public:
    static void OnSmsSent( IPhoneSmsService* service, bool success )
    {
        SmsServiceListener* listener = service->m_listener;
        if ( listener )
        {
            listener->OnSmsSent( success );
        }
    }
};

#pragma mark -

@interface IPhoneSmsServiceDelegate()

@property IPhoneSmsService* service;

@end

#pragma mark -

@implementation IPhoneSmsServiceDelegate

@synthesize service = _service;

- (id)initWithService:(IPhoneSmsService *)service
{
    CLAW_ASSERT( service );

    if ( ( self = [super init] ) )
    {
        self.service = service;
    }

    return self;
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* viewController = [appDelegate getViewController];
    [viewController dismissModalViewControllerAnimated:YES];

    IPhoneSmsServiceAdapter::OnSmsSent( self.service, result == MessageComposeResultSent );
}

@end

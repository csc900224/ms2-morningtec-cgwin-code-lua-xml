#ifndef __INCLUDED__IPHONESMSSERVICE_HPP__
#define __INCLUDED__IPHONESMSSERVICE_HPP__

#include "claw_ext/network/sms/SmsService.hpp"

class IPhoneSmsServiceDelegateWrapper;

class IPhoneSmsService : public SmsServiceBase
{
    friend class IPhoneSmsServiceAdapter;

public:
    IPhoneSmsService();
    ~IPhoneSmsService();

    bool CanSendSms() const;
    void SendSms( const Claw::NarrowString& message );
    void SetListener( SmsServiceListener* listener );

private:
    SmsServiceListener* m_listener;

    IPhoneSmsServiceDelegateWrapper* m_delegate;

}; // class IphoneSmsService

#endif // __INCLUDED__IPHONESMSSERVICE_HPP__

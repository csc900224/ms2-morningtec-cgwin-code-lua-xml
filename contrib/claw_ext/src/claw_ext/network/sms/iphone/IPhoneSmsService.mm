#include "claw_ext/network/sms/SmsService.hpp"
#include "claw_ext/network/sms/iphone/IphoneSmsService.hpp"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>

#import "claw/application/iphone/IPhoneAppDelegate.h"
#import "claw_ext/network/sms/iphone/IPhoneSmsServiceDelegate.h"

class IPhoneSmsServiceDelegateWrapper
{
public:
    IPhoneSmsServiceDelegateWrapper( IPhoneSmsService* service )
        : m_delegate( nil )
    {
        m_delegate = [[IPhoneSmsServiceDelegate alloc] initWithService:service];
    }

    ~IPhoneSmsServiceDelegateWrapper()
    {
        [m_delegate release];
    }

    IPhoneSmsServiceDelegate* Get() const
    {
        return m_delegate;
    }

private:
    IPhoneSmsServiceDelegate* m_delegate;

};

IPhoneSmsService::IPhoneSmsService()
    : m_listener( NULL )
    , m_delegate( NULL )
{
    m_delegate = new IPhoneSmsServiceDelegateWrapper( this );
}

IPhoneSmsService::~IPhoneSmsService()
{
    delete m_delegate;
}

bool IPhoneSmsService::CanSendSms() const
{
    return [MFMessageComposeViewController canSendText] == YES;
    return false;
}

void IPhoneSmsService::SendSms( const Claw::NarrowString& message )
{
    CLAW_ASSERT( CanSendSms() );

    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    controller.wantsFullScreenLayout = NO;
    controller.body = [NSString stringWithCString:message.c_str() encoding:NSUTF8StringEncoding];
    controller.messageComposeDelegate = m_delegate->Get();

    IPhoneAppDelegate* appDelegate = [IPhoneAppDelegate instance];
    IPhoneViewController* viewController = [appDelegate getViewController];
    [viewController presentModalViewController:controller animated:YES];

    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

void IPhoneSmsService::SetListener( SmsServiceListener* listener )
{
    m_listener = listener;
}

#import <MessageUI/MFMessageComposeViewController.h>

class IPhoneSmsService;

@interface IPhoneSmsServiceDelegate : NSObject <MFMessageComposeViewControllerDelegate>

- (id)initWithService:(IPhoneSmsService *)service;

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result;

@end
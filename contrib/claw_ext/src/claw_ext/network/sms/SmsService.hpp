#ifndef __INCLUDED__SMSSERVICE_HPP__
#define __INCLUDED__SMSSERVICE_HPP__

#include "claw/base/RefCounter.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/compat/Platform.h"

class SmsServiceListener;

class SmsServiceBase : public Claw::RefCounter
{
public:
    virtual bool CanSendSms() const = 0;
    virtual void SendSms( const Claw::NarrowString& message ) = 0;
    virtual void SetListener( SmsServiceListener* listener ) = 0;
};

class SmsServiceListener
{
public:
    virtual void OnSmsSent( bool success ) {}
};

#if defined CLAW_IPHONE
#include "claw_ext/network/sms/iphone/IPhoneSmsService.hpp"
typedef IPhoneSmsService SmsService;
#elif defined CLAW_ANDROID
#include "claw_ext/network/sms/android/AndroidSmsService.hpp"
typedef AndroidSmsService SmsService;
#else
#include "claw_ext/network/sms/dummy/DummySmsService.hpp"
typedef DummySmsService SmsService;
#endif

typedef Claw::SmartPtr<SmsService> SmsServicePtr;

#endif // __INCLUDED__SMSSERVICE_HPP__

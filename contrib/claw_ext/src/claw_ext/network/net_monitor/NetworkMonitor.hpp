/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      claw_ext/network/net_monitor/NetworkMonitor.hpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2013, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

#ifndef __INCLUDED__NETWORK_NETWORKMONITOR_HPP__
#define __INCLUDED__NETWORK_NETWORKMONITOR_HPP__

#include "claw/base/Thread.hpp"
#include "claw/network/NtpRequest.hpp"

#include <set>

namespace ClawExt
{
    //! Class responsible for checking network connection status.
    class NetworkMonitor
    {
    public:
        //! Connection status.
        enum NetworkStatus
        {
            NS_UNKNOWN,
            NS_CONNECTED,
            NS_DISCONNECTED
        };
        
        //! Callback for getting check results.
        class Observer
        {
        public:
            virtual void OnNetworkCheckResult( NetworkStatus status ) = 0;
        }; // calss NetworkObserver

        //! Manual and instant network connection check request.
        void                        ConnectionCheck();

        //! Check if any check is currently processed.
        bool                        IsConnectionCheckInProgress() const { return m_checkInProgress; }

        //! Set interval for automatic connection rechecks.
        /**
        * Callback form observes will be called only if connection status chages.
        * If interval == 0, then no automatic check will be performed.
        */
        void                        SetConnectionCheckInterval( unsigned int seconds );

        //! Update monitor staus. Should be called every frame.
        void                        Update( float dt );

        //! Destructor
        virtual                     ~NetworkMonitor();

        //! Retreive object instance.
        static NetworkMonitor*      GetInstance();

        //! Release instance.
        static void                 Release();

        //! Register observer to be notiffied about connection related events.
        void                        RegisterObserver( Observer* callback );

        //! Unregister observer.
        void                        UnregisterObserver( Observer* callback );

    protected:
                                    NetworkMonitor();

        void                        ConnectionCheck( bool automatic );

        void                        NotifyNetworkCheckResult( NetworkStatus status );

    private:
        typedef std::set<Observer*> Observers;
        typedef Observers::iterator ObserversIt;

        static int                  ConnectionCheckEntry( void* ptr );
        int                         DoConnectionCheck();

        volatile bool               m_checkInProgress;
        Claw::Thread*               m_checkcThread;
        unsigned int                m_checkInterval;
        float                       m_checkTimer;

        volatile float              m_requestTimer;
        Claw::NtpRequest*           m_request;
        
        NetworkStatus               m_lastStatus;
        volatile bool               m_forceNotify;

        Observers                   m_observers;

    }; // class NetworkMonitor
} // namespace CalwExt

#endif // __INCLUDED__NETWORK_NETWORKMONITOR_HPP__


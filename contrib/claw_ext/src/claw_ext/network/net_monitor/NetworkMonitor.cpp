/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      claw_ext/network/net_monitor/NetworkMonitor.cpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2013, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

#include "claw_ext/network/net_monitor/NetworkMonitor.hpp"

namespace ClawExt
{
    static const int    RECHECK_CKOUNT     = 2;
    static const float  REQUEST_TIMEOUT    = 10.f;

    static NetworkMonitor* s_instance   = NULL;

    NetworkMonitor* NetworkMonitor::GetInstance()
    {
        if( !s_instance )
            s_instance = new NetworkMonitor();
        return s_instance;
    }

    void NetworkMonitor::Release()
    {
        delete s_instance;
        s_instance = NULL;
    }

    NetworkMonitor::NetworkMonitor()
        : m_checkcThread( NULL )
        , m_checkInProgress( false )
        , m_checkInterval( 0 )
        , m_checkTimer( 0 )
        , m_requestTimer( 0 )
        , m_forceNotify( false )
        , m_lastStatus( NS_UNKNOWN )
        , m_request( NULL )
    {}

    NetworkMonitor::~NetworkMonitor()
    {
        delete m_checkcThread;
        m_checkcThread = NULL;
    }

    void NetworkMonitor::ConnectionCheck( bool automatic )
    {
        if( !m_checkInProgress )
        {
            // No check in progress - start new one
            m_forceNotify = !automatic;
            delete m_checkcThread;
            m_checkcThread = new Claw::Thread( NetworkMonitor::ConnectionCheckEntry, this );
            m_checkInProgress = true;
        }
        else if( !automatic )
        {
            // If this was manual call - force status notification
            m_forceNotify = true;
        }
    }

    void NetworkMonitor::ConnectionCheck()
    {
        ConnectionCheck( false );
    }

    void NetworkMonitor::SetConnectionCheckInterval( unsigned int seconds )
    {
        m_checkInterval = seconds;
        m_checkTimer = (float)seconds;
    }

    void NetworkMonitor::Update( float dt )
    {
        // Check if automatic recheck is needed
        if( m_checkInterval > 0 )
        {
            m_checkTimer -= dt;
            if( m_checkTimer <= 0 && !m_checkInProgress )
            {
                ConnectionCheck( true );
                m_checkTimer = (float)m_checkInterval;
            }
        }

        // Check for request timeout
        if( m_request && m_requestTimer > 0 )
        {
            m_requestTimer -= dt;
            if( m_requestTimer <= 0 )
            {
                m_request->Abort();
            }
        }
    }

    int NetworkMonitor::ConnectionCheckEntry( void* ptr )
    {
        return ((NetworkMonitor*)ptr)->DoConnectionCheck();
    };

    int NetworkMonitor::DoConnectionCheck()
    {
        NetworkStatus status = NS_UNKNOWN;

        // If no connection detected - retry RECHECK_CKOUNT times
        for( int tryId = 0; tryId < RECHECK_CKOUNT; ++tryId )
        {
            // Create request
            m_request = new Claw::NtpRequest();
            m_requestTimer = REQUEST_TIMEOUT;

            // Run request
            m_request->Connect();

            if( m_request->CheckError() && tryId == RECHECK_CKOUNT - 1 )
            {
                // Last try and still no connection
                status = NS_DISCONNECTED;
            }
            else if( !m_request->CheckError() )
            {
                // Conection ok! We can filalize check
                status = NS_CONNECTED;
                break;
            }
        }

        // Release request
        delete m_request;
        m_request = NULL;

        // Notify status changes
        CLAW_ASSERT( status != NS_UNKNOWN );
        if( status != NS_UNKNOWN && (m_lastStatus != status || m_forceNotify) )
        {
            NotifyNetworkCheckResult( status );
            m_lastStatus = status;
        }

        // Done!
        m_checkInProgress = false;
        return 0;
    }

    void NetworkMonitor::RegisterObserver( Observer* callback )
    {
        m_observers.insert( callback );
    }

    void NetworkMonitor::UnregisterObserver( Observer* callback )
    {
        m_observers.erase( callback );
    }

    void NetworkMonitor::NotifyNetworkCheckResult(  NetworkStatus status )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
            (*it)->OnNetworkCheckResult( status );
    }

} // namespace ClawExt


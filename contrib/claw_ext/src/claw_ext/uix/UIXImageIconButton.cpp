//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXImageIconButton.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXImageIconButton.hpp"
#include "claw_ext/ui/UIImage.hpp"

namespace ClawExt
{
    UIXImageIconButton::UIXImageIconButton( ButtonType type /*= BT_NORMAL*/ )
        : UIXImageButton( type )
        , m_icon( new UIImage )
    {
        m_icon->SetAlign( Align( AH_CENTER, AV_CENTER ) );

        AddComponent( m_icon );
    }

    void UIXImageIconButton::SetIconImage( const Claw::SurfacePtr& ptr )
    {
        m_icon->SetImage( ptr );

        // Invalidate container size
        Invalidate();
    }

    void UIXImageIconButton::LoadIconImage( const char* iconPath )
    {
        m_icon->LoadImg( iconPath );

        // Invalidate container size
        Invalidate();
    }

    void UIXImageIconButton::SetIconAlign( const Align& align, bool keepPos /*= false*/ )
    {
        m_icon->SetAlign( align, keepPos );

        // Invalidate icon position
        Invalidate();
    }

} // namespace ClawExt
// EOF

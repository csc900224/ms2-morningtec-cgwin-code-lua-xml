//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXAnimatedButton.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_ANIMATED_BUTTON_HPP__
#define __UI_UIX_ANIMATED_BUTTON_HPP__

// Internal includes
#include "claw_ext/uix/UIXImageButton.hpp"
#include "claw_ext/ui/UIImage.hpp"
#include "claw_ext/ui/UIAnimatedImage.hpp"
#include "claw/base/WeakPtr.hpp"

// External includes
#include <map>

namespace ClawExt
{
    //! Extension of UIXImageButton that allows to animate certain button states.
    /*!
    * There can be 3 different animations or images attached to a single button, depending on current
    * button state:
    * - Idle image/animation
    * - Cursor over
    * - Button pressed
    */
    class UIXAnimatedButton : public UIXImageButton
    {
    public:
        //! Default constuctor setting postion to 0,0.
                            UIXAnimatedButton( ButtonType type = BT_NORMAL );

        //! Perform animation update.
        virtual void        Update( float dt );

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Load image from the path and invalidate component size.
        void                LoadAnim( const char* animPath );

        //! Setup image content to be displayed.
        /*
        * Same as calling: SetImageForButtonState( BS_IDLE, ptr );
        * \note For internal (compatibility) reasons method need to be overriden in this class.
        */
        virtual void        SetImage( const Claw::SurfacePtr& ptr );

        //! Setup image content for specified ButtonState.
        /*!
        * \note Method overriden here only for internal compatibility purposes.
        */
        virtual void        SetImageForButtonState( ButtonState buttonState, const Claw::SurfacePtr& ptr );

        //! Setup image content to be displayed.
        /*
        * Same as calling: SetAnimForButtonState( BS_IDLE, ptr );
        */
        void                SetAnim( const Claw::SurfacePtr& ptr );

        //! Setup image content for specified ButtonState.
        void                SetAnimForButtonState( ButtonState buttonState, const Claw::SurfacePtr& ptr );

        //! Acquire image surface for specified ButtonState (weak pointer is returned).
        SurfaceWPtr         GetAnimForButtonState( ButtonState buttonState ) const;

        //! Load image (immediately) via AssetDict for specified ButtonState.
        void                LoadAnimForButtonState( ButtonState buttonState, const char* animPath );

        //! Pause animation
        void                PauseAnim( int frameIdx = -1 );

        //! Resume animation
        void                ResumeAnim( int frameIdx = -1 );

    protected:
        // Declare internal press callback class
        class AnimChangeFunctor : public UIXImageButton::ImageChangeFunctor
        {
        public:
                            AnimChangeFunctor( const Claw::SurfacePtr& changeToImage );
            virtual void    Call( UIComponent* cmp, UIComponent::CursorEvent ev, UIComponent::CursorIdx idx, const UIComponent::Pos& cursorPos );

        }; // class AnimChangeFunctor

        // Declare animation wrapper class
        class UIAnimationWrapper : public UIAnimatedImage
        {
        public:
            //! Default constructor.
                            UIAnimationWrapper();

        virtual void        Render( Claw::Surface* target );

        inline void         SetAnimated( bool animated )    { m_animated = animated; }
        inline bool         IsAnimated() const              { return m_animated;    }

        private:
            bool            m_animated;

        }; // class UIAnimationWrapper

    private:
        bool                m_buttonStateAnim[BS_NUM];

    }; // class UIXAnimatedButton

} // namespace ClawExt

#endif // define __UI_UIX_ANIMATED_BUTTON_HPP__
// EOF

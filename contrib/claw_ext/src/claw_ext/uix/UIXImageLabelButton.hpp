//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIXImageLabelButton.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_IMAGE_LABEL_BUTTON_HPP__
#define __UI_UIX_IMAGE_LABEL_BUTTON_HPP__

// Internal includes
#include "claw_ext/uix/UIXImageButton.hpp"

// External includes
#include "claw/graphics/Color.hpp"

namespace ClawExt
{
    // Forward declarations
    class UILabel;

    //! UIXImageButton with additional text label.
    class UIXImageLabelButton : public UIXImageButton
    {
    public:
        //! Constructor defining font andt toggle mode.
                                UIXImageLabelButton( const char* fontXmlPath, ButtonType type = BT_NORMAL, const Claw::Color clr = Claw::Color( 255, 255, 255 ) );

        //! Set label text.
        void                    SetLabelText( const Claw::String& text );

        //! Set label text color.
        void                    SetLabelColor( const Claw::Color& clr );

        //! Set label alignment (default is center).
        void                    SetLabelAlign( const Align& align, bool keepPos = false );

        //! Set font label base line ratio (form font top edge).
        /*! 
        * It is used to additionaly offset label in Y-axis, when label is vericaly centered.
        * Used to fix visual apperance e.g. when ony upercase texst are used.
        */
        void                    SetLabelBaseLine( float baseLine );

        //! Get current label base line.
        inline float            GetLabelBaseLine() const;

    private:
        UILabel*                m_label;
        float                   m_baseLine;

    }; /// class UIXImageLabelButton

    inline float UIXImageLabelButton::GetLabelBaseLine() const
    {
        return m_baseLine;
    }

} // namespace ClawExt

#endif // define __UI_UIX_IMAGE_LABEL_BUTTON_HPP__
// EOF

#include "claw_ext/uix/UIXTextArea.hpp"

#include "claw/base/Xml.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/graphics/FontEx.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/text/FontSet.hpp"
#include "claw/graphics/text/Typesetter.hpp"

namespace ClawExt
{
    UIXTextArea::UIXTextArea( const Claw::NarrowString& fontXmlPath, const Claw::Color color )
        : m_text( "UIXTextArea" )
    {
        Claw::FontExPtr font = Claw::AssetDict::Get<Claw::FontEx>( fontXmlPath );

        Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
        fontSet->AddFont( "default", font );

        m_format.SetFontSet( fontSet );
        m_format.SetFontId( "default" );
        m_format.SetColor( color );

        Invalidate();
    }

    void UIXTextArea::Load( const Claw::XmlIt* xmlConfig )
    {

    }

    void UIXTextArea::Render( Claw::Surface* target )
    {
        if ( m_screenText )
        {
            const Pos& pos = GetViewPos();
            m_screenText->Draw( target, pos.m_x, pos.m_y );
        }
    }

    void UIXTextArea::SetColor( const Claw::Color& clr )
    {
        m_format.SetColor( clr );

        Invalidate();
    }

    void UIXTextArea::SetText( const Claw::String& text )
    {
        m_text = text;

        Invalidate();
    }

    void UIXTextArea::SetTextAlign( Claw::Text::Format::HorAlign align )
    {
        m_format.SetHorizontalAlign( align );

        Invalidate();
    }

    void UIXTextArea::Invalidate()
    {
        if ( m_size.m_x > 0 )
        {
            m_screenText.Reset(
                new Claw::ScreenText(
                    m_format,
                    m_text,
                    Claw::Extent( m_size.m_x, 0 )
                )
            );

            m_size.m_y = m_screenText->GetHeight();
        }
        else
        {
            m_screenText.Release();
        }
    }

} // namespace ClawExt

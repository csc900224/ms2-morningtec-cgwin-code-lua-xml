//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIXImageIconButton.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_IMAGE_ICON_BUTTON_HPP__
#define __UI_UIX_IMAGE_ICON_BUTTON_HPP__

// Internal includes
#include "claw_ext/uix/UIXImageButton.hpp"

// External includes
#include "claw/graphics/Color.hpp"

namespace ClawExt
{
    // Forward declarations
    class UIImage;

    //! UIXImageButton with additional text label.
    class UIXImageIconButton : public UIXImageButton
    {
    public:
        //! Constructor defining font andt toggle mode.
                                UIXImageIconButton( ButtonType type = BT_NORMAL );

        //! Set surface representing button icon
        void                    SetIconImage( const Claw::SurfacePtr& ptr );

        //! Load icon form file with given path.
        void                    LoadIconImage( const char* iconPath );

        //! Set icon alignment (default is center).
        void                    SetIconAlign( const Align& align, bool keepPos = false );

    private:
        UIImage*                m_icon;

    }; /// class UIXImageIconButton

} // namespace ClawExt

#endif // define __UI_UIX_IMAGE_ICON_BUTTON_HPP__
// EOF

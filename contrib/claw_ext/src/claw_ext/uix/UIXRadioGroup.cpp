//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXRadioGroup.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXRadioGroup.hpp"
#include "claw_ext/uix/UIXImageButton.hpp"

namespace ClawExt
{
    UIXRadioGroup::UIXRadioGroup()
        : m_initialSelectIdx( -1 )
        , m_selectedButton( NULL )
    {}

    UIXRadioGroup::UIXRadioGroup( int initialSelectIdx )
        : m_initialSelectIdx( initialSelectIdx )
        , m_selectedButton( NULL )
    {}

    void UIXRadioGroup::AddComponent( UIXImageButton* button )
    {
        CLAW_ASSERT( button );
        if( button )
        {
            UIList::AddComponent( button );

            button->SetRadioGroup( this );
            button->SetType( UIXImageButton::BT_RADIO );

            // This button should be selected
            if( m_initialSelectIdx == 0 )
            {
                button->SetSelected( true );
                m_selectedButton = button;
            }
            else
            {
                button->SetSelected( false );
            }

            // Decrese selection index if necessary
            if( m_initialSelectIdx >= 0 )
            {
                --m_initialSelectIdx;
            }
        }
    }

    void UIXRadioGroup::AddComponent( UIComponent* component )
    {
        UIList::AddComponent( component );
    }

    void UIXRadioGroup::OnButtonSelectionChanged( UIXImageButton* selectedButton )
    {
        if( selectedButton && selectedButton->IsSelected() && selectedButton != m_selectedButton )
        {
            if( m_selectedButton )
            {
                // Unpress previous button
                m_selectedButton->SetSelected( false );
            }
            m_selectedButton = selectedButton;
        }
    }
} // namespace ClawExt
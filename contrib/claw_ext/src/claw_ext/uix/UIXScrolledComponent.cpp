//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXScrolledComponent.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXScrolledComponent.hpp"

// External includes
#include "claw/graphics/Color.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/math/Vector.hpp"
#include "claw/application/AbstractApp.hpp"

#define _DEBUG_SCROLLED_COMPONENT   0 // Set 1 for debug mode

static const float      DRAG_FILTER_STRENGTH        = 0.5f;
static const float      SCROLL_ACCELERATION         = 1.0f;
static const float      SAMPLING_INTERVAL           = 0.025f;
static Claw::Vector2f   VELOCITY_EPSILON            ( 0.1f );
static float            SCROLL_RELEASE_PERCENT      = 0.005f;

template< class T, class V >
inline Claw::Vector2< T > VecConv( const V& v )
{
    return Claw::Vector2< T >( T(v.m_x), T(v.m_y) );
}

namespace ClawExt
{
    UIXScrolledComponent::UIXScrolledComponent()
        : m_content( NULL )
        , m_scrollIdx( -1 )
        , m_offsetFilter( Claw::Vector2f( 0.f, 0.f ) )
        , m_offsetFilterInit( false )
        , m_sampleTimer( -1 )
        , m_draggTime( 0 )
        , m_clippingOffset( 0, 0 )
    {
        for( int i = 0; i < CURSORS_NUM; ++i )
        {
            m_pressedCursors[i] = false;
        }

        m_scrollOffset.Zero();
        m_dragBegin.Zero();
        m_velocity.Zero();
        m_velocity.Zero();
        m_lastVelocity.Zero();
        m_currentPos.Zero();
        m_alignOffset.Zero();
    }

    void UIXScrolledComponent::Load( const Claw::XmlIt* xmlConfig )
    {}

    void UIXScrolledComponent::Render( Claw::Surface* target )
    {
        const Pos& pos   = GetViewPos();
        const Size& size = GetSize();

        if( m_content && m_content->IsVisible() )
        {
            // Store current clip rect
            const Claw::Rect prevClipRect = target->GetClipRect();

            // Set new clip rect
            Claw::Rect clipRect( pos.m_x, pos.m_y, size.m_x, size.m_y );
            clipRect.m_x += m_clippingOffset.left;
            clipRect.m_w += -m_clippingOffset.left + m_clippingOffset.right;
            clipRect.m_y += m_clippingOffset.top;
            clipRect.m_h += -m_clippingOffset.top + m_clippingOffset.bottom;
            target->SetClipRect( clipRect );

            // Render content
            m_content->Render( target );

            // Restore previous clip rect
            target->SetClipRect( prevClipRect );
        }

#if _DEBUG_SCROLLED_COMPONENT
        // Just for debug
        target->DrawRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x, pos.m_y + size.m_y, Claw::Color( 0, 255, 0 ) );
#endif
    }

    void UIXScrolledComponent::Update( float dt )
    {
        UIContainer::Update( dt );

        // Calculate dragg time when cursor was pressed
        if( m_scrollIdx >= 0 )
        {
            m_draggTime += dt;
        }

        // Sampe swipe velocity
        if( m_scrollIdx >= 0 && m_sampleTimer >= 0 )
        {
            m_sampleTimer -= dt;
            if( m_sampleTimer < 0 )
            {
                m_sampleTimer = SAMPLING_INTERVAL;
                m_lastVelocity = (m_currentPos - m_samplePos) / SAMPLING_INTERVAL;
                m_samplePos = m_currentPos;
            }
        }

        // Update velocity changes if nesessary
        if( abs(m_velocity.m_x) > VELOCITY_EPSILON.m_x || abs(m_velocity.m_y) > VELOCITY_EPSILON.m_y )
        {
            // Decrease velocity a little (slow down movement)
            m_velocity -= m_velocity * SCROLL_ACCELERATION * dt;

            // Update current offset
            m_targetOffset += m_velocity * dt;
            ClampOffset( m_targetOffset );
        }

        // If target offset is different that what we have now
        const Claw::Vector2f diff = m_targetOffset - m_scrollOffset;
        if( abs(diff.m_x) > VELOCITY_EPSILON.m_x || abs(diff.m_y) > VELOCITY_EPSILON.m_y )
        {
            RefreshContentOffset();
        }
    }

    void UIXScrolledComponent::RefreshContentOffset()
    {
        // Update low pass filter
        if( !m_offsetFilterInit )
        {
            m_offsetFilter = m_targetOffset;
            m_offsetFilterInit = true;
        }
        else
        {
            m_offsetFilter = m_targetOffset * DRAG_FILTER_STRENGTH + m_offsetFilter * ( 1.f - DRAG_FILTER_STRENGTH );
        }

        // Updte current offset
        m_scrollOffset = m_offsetFilter;

        // Update content position
        m_alignOffset = CalcAlignOffset(m_content);
        m_content->SetParentPos( GetViewPos() );
        m_content->SetParentOff( VecConv<int>(m_scrollOffset) + m_padding + m_alignOffset );
        NotifyDownTree( CN_POS_CHANGED );
    }

    void UIXScrolledComponent::Invalidate()
    {
        if( m_content )
        {
            m_alignOffset = CalcAlignOffset(m_content);
            m_content->SetParentPos( GetViewPos() );
            m_content->SetParentOff( VecConv<int>(m_scrollOffset) + m_padding + m_alignOffset );
            m_content->Invalidate();
        }
    }

    UIComponent* UIXScrolledComponent::SetContent( UIComponent* content, bool resetOffset )
    {
        UIComponent* prevContent = NULL;
        if( m_content != content )
        {
            prevContent = m_content;

            // Reset state
            m_content = content;
            if( resetOffset )
            {
                m_scrollOffset.Zero();
                m_dragBegin.Zero();
                m_velocity.Zero();
                m_scrollIdx = -1;
                m_velocity.Zero();
            }

            // Calculate contents relative layout with some padding magic
            Size prevSize = m_size;
            m_size -= m_padding * 2;
            m_content->CalculateRelativeLayout( this );
            m_size = prevSize;

            // Delete all children and add new subcomponent
            CLAW_MSG_ASSERT( m_children.size() < 2, "More than one child component was attached to UIXScrollComponent!" );
            m_children.clear();
            m_children.push_back( content );

            // Make sure current offset matches everything
            ClampOffset( m_scrollOffset );
            m_targetOffset = m_scrollOffset;
            m_offsetFilter = m_targetOffset;
            m_offsetFilterInit = true;

            Invalidate();
        }
        return prevContent;
    }

    void UIXScrolledComponent::AddComponent( UIComponent* component )
    {
        UIContainer::AddComponent( component );
    }

    void UIXScrolledComponent::ProcessEvent( CursorEvent ev, CursorIdx idx, const Pos& pos )
    {
        UIContainer::ProcessEvent( ev, idx, pos );

        if( !m_content ) return;

        // Dragg will start
        if( ev == CE_PRESS )
        {
            // General initialization
            m_scrollIdx = idx;
            m_dragBegin = VecConv<float>( pos );

            // Global offset setup
            if( !m_offsetFilterInit )
            {
                m_offsetFilter = m_targetOffset;
                m_offsetFilterInit = true;
            }
            else
            {
                m_offsetFilter = m_targetOffset * DRAG_FILTER_STRENGTH + m_offsetFilter * ( 1.f - DRAG_FILTER_STRENGTH );
            }
            m_targetOffset = m_scrollOffset;
            m_scrollOffsetBegin = m_scrollOffset;

            // Velocity calcualtaion related initializations
            m_velocity.Zero();
            m_lastVelocity.Zero();
            m_sampleTimer = SAMPLING_INTERVAL;
            m_samplePos = m_dragBegin;
            m_currentPos = m_samplePos;
            m_draggTime = 0;
        }
        else if( (ev == CE_DRAG_OVER || ev == CE_DRAG_OUTSIDE) && idx == m_scrollIdx )
        {
            // Calcualte current dragg vector
            Claw::Vector2f dragOffset = m_dragBegin - VecConv<float>( pos );

            // Update target offset
            m_targetOffset = m_scrollOffsetBegin - dragOffset;
            ClampOffset( m_targetOffset );

            // Store current pointer position
            m_currentPos = VecConv<float>( pos );
        }
        else if( (ev == CE_RELEASE || ev == CE_RELEASE_DRAG_OUT) && idx == m_scrollIdx )
        {
            // Reset some stuff
            m_scrollIdx = -1;
            m_sampleTimer = -1;

            // Calcualte overal dragg vector
            Claw::Vector2f dragOffset = m_dragBegin - VecConv<float>( pos );
            
            // Update target offset
            m_targetOffset = m_scrollOffsetBegin - dragOffset;
            ClampOffset( m_targetOffset );

            // Check if dragg time is shorter than sampling rate. If so recalculate velocity manualny
            if( m_draggTime < SAMPLING_INTERVAL && m_draggTime > 0 )
            {
                m_lastVelocity = (m_currentPos - m_samplePos) / m_draggTime;
            }

            // Set new velocity
            m_velocity = m_lastVelocity;
        }
    }

    void UIXScrolledComponent::SetOffset( const Claw::Vector2f& newOffset, bool animated /*= true*/ )
    {
        m_targetOffset = newOffset;
        ClampOffset( m_targetOffset );

        if( !animated )
        {
            m_offsetFilter = m_targetOffset;
            m_offsetFilterInit = true;

            RefreshContentOffset();

            m_velocity.Zero();
            m_lastVelocity.Zero();
        }
    }

    const Claw::Vector2f& UIXScrolledComponent::GetCurrentOffset() const
    {
        return m_offsetFilter;
    }

    void UIXScrolledComponent::ClampOffset( Claw::Vector2f& offset )
    {
        if( m_content )
        {
            // Calculate minimal and maximal offsets thar are allowed
            Claw::Vector2f minOffset = -VecConv<float>(m_content->GetLocalPos() + m_alignOffset + m_content->GetSize() - GetSize() + m_padding * 2);
            Claw::Vector2f maxOffset = VecConv<float>(m_content->GetLocalPos() - m_alignOffset);

            // X-axis offset
            if( minOffset.m_x <= maxOffset.m_x )
                offset.m_x = Claw::MinMax( offset.m_x, minOffset.m_x, maxOffset.m_x );
            else
                offset.m_x = 0;

            // Y-axis offset
            if( minOffset.m_y <= maxOffset.m_y )
                offset.m_y = Claw::MinMax( offset.m_y, minOffset.m_y, maxOffset.m_y );
            else
                offset.m_y = 0;
        }
    }

    void UIXScrolledComponent::SetPadding( const Pos& padding )
    {
        if( m_padding != padding )
        {
            m_padding = padding;
            Invalidate();
        }
    }

    void UIXScrolledComponent::CalculateRelativeLayout( const UIComponent* relativeTo /*= NULL*/ )
    {
        UIComponent::CalculateRelativeLayout( relativeTo );

        // Decrese size so children will see size-padding rect only
        Size prevSize = m_size;
        m_size -= m_padding * 2;
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->CalculateRelativeLayout( this );
        }
        // Restore original size
        m_size = prevSize;
    }

    void UIXScrolledComponent::NotifyDownTree( ComponentNotification cn )
    {
        if( cn == CN_SIZE_CHANGED )
        {
            // Decrese size so children will see size-padding rect only
            Size prevSize = m_size;
            m_size -= m_padding * 2;

            UIContainer::NotifyDownTree( cn );

            // Restore original size
            m_size = prevSize;
        }
        else
        {
            UIContainer::NotifyDownTree( cn );
        }
    }

    void UIXScrolledComponent::OnCursorMove( const Pos& cursorPos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );
        float offX = abs(m_scrollOffsetBegin.m_x - m_targetOffset.m_x);
        float offY = abs(m_scrollOffsetBegin.m_y - m_targetOffset.m_y);
        bool release = offX > Claw::AbstractApp::GetInstance()->GetDisplay()->GetWidth() * SCROLL_RELEASE_PERCENT ||
                       offY > Claw::AbstractApp::GetInstance()->GetDisplay()->GetHeight() * SCROLL_RELEASE_PERCENT;

        // If move is inside cliping rect
        const ViewRect& viewRect = GetViewRect();
        if( !release 
            && cursorPos.m_x >= viewRect.left 
            && cursorPos.m_x <= viewRect.right 
            && cursorPos.m_y >= viewRect.top
            && cursorPos.m_y <= viewRect.bottom )
        {
            // Just move
            UIContainer::OnCursorMove( cursorPos, idx );
        }
        // Moved outside cliprect - fake release
        else if( release || m_pressedCursors[idx] )
        {
            m_pressedCursors[idx] = false;
            UIComponent::OnCursorMove( cursorPos, idx );

            // Fake cursor release outside components child
            Pos fakePos( Claw::NumberTraits<int>::Maximum() );
            SubComponentsIt it = m_children.begin();
            SubComponentsIt end = m_children.end();
            for( ; it != end; ++it )
            {
                (*it)->OnCursorRelease( fakePos, idx );
            }
        }
        // Permanent move outside clip rect
        else
        {
            // Dont pass move event to child components
            UIComponent::OnCursorMove( cursorPos, idx );
        }
    }

    void UIXScrolledComponent::OnCursorPress( const Pos& cursorPos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );

        // Cut off events outside visible volume
        const ViewRect& viewRect = GetViewRect();
        if( cursorPos.m_x >= viewRect.left 
            && cursorPos.m_x <= viewRect.right 
            && cursorPos.m_y >= viewRect.top
            && cursorPos.m_y <= viewRect.bottom )
        {
            UIContainer::OnCursorPress( cursorPos, idx );
            m_pressedCursors[idx] = true;
        }
        else
        {
            UIComponent::OnCursorPress( cursorPos, idx );
        }
    }

    UIXScrolledComponent::Pos UIXScrolledComponent::CalcAlignOffset( const UIComponent* comp ) const
    {
        Pos off( 0, 0 );
        if( comp->GetAlign().m_horiz == UIComponent::AH_RIGHT )
        {
            off.m_x = GetSize().m_x - comp->GetSize().m_x;
        }
        else if( comp->GetAlign().m_horiz == UIComponent::AH_CENTER )
        {
            off.m_x = (int)(GetSize().m_x / 2.f - comp->GetSize().m_x / 2.f);
        }

        if( comp->GetAlign().m_vert == UIComponent::AV_BOTTOM )
        {
            off.m_y = GetSize().m_y - comp->GetSize().m_y;
        }
        else if( comp->GetAlign().m_vert == UIComponent::AV_CENTER )
        {
            off.m_y = (int)(GetSize().m_y / 2.f - comp->GetSize().m_y / 2.f);
        }
        return off;
    }

} // namespace ClawExt

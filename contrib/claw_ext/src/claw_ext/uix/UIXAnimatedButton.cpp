//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXAnimatedButton.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXAnimatedButton.hpp"
#include "claw_ext/uix/UIXRadioGroup.hpp"
#include "claw_ext/ui/UIFunctor.hpp"

// External includes
#include "claw/base/AssetDict.hpp"
#include "claw/graphics/AnimatedSurface.hpp"
#include "claw/graphics/TriangleEngine.hpp"

namespace ClawExt
{

    /////////////////////////////////////////////////////////
    // UIXAnimatedButton::AnimChangeFunctor internal class
    /////////////////////////////////////////////////////////
    UIXAnimatedButton::AnimChangeFunctor::AnimChangeFunctor( const Claw::SurfacePtr& changeToImage )
        : UIXImageButton::ImageChangeFunctor( changeToImage )
    {}

    void UIXAnimatedButton::AnimChangeFunctor::Call( UIComponent* cmp, UIComponent::CursorEvent ev, UIComponent::CursorIdx idx, const UIComponent::Pos& cursorPos )
    {
        CLAW_ASSERT(cmp);
        ((UIXAnimatedButton*)cmp)->SetAnim( m_imageToChange );
    }

    /////////////////////////////////////////////////////////
    // UIXAnimatedButton::UIAnimationWrapper internal class
    /////////////////////////////////////////////////////////
    UIXAnimatedButton::UIAnimationWrapper::UIAnimationWrapper()
        : UIAnimatedImage()
        , m_animated( true )
    {}

    void UIXAnimatedButton::UIAnimationWrapper::Render( Claw::Surface* target )
    {
        if( IsAnimated())
            UIAnimatedImage::Render( target );
        else
            UIImage::Render( target );
    }

    /////////////////////////////////////////////////////////
    // UIXAnimatedButton
    /////////////////////////////////////////////////////////
    UIXAnimatedButton::UIXAnimatedButton( ButtonType type /*= BT_NORMAL*/ )
        : UIXImageButton( type, new UIAnimationWrapper )
    {
    }

    void UIXAnimatedButton::Update( float dt )
    {
        if( IsVisible() )
            m_image->Update( dt );
    }

    void UIXAnimatedButton::Load( const Claw::XmlIt* xmlConfig )
    {
        // TODO: Implement me please!
    }

    void UIXAnimatedButton::LoadAnim( const char* animPath )
    {
        using namespace Claw;
        SurfacePtr surfPtr( NULL );
        if( animPath == NULL )
        {
            // Setup empty image - no graphics will be displayed
            SetAnim( surfPtr );
            return;
        }

        surfPtr = AssetDict::Get< Surface >( animPath );

        SetAnim( surfPtr );
    }

    void UIXAnimatedButton::SetImage( const Claw::SurfacePtr& ptr )
    {
        UIXImageButton::SetImage( ptr );

        ((UIAnimationWrapper*)m_image)->SetAnimated( false );
    }

    void UIXAnimatedButton::SetImageForButtonState( ButtonState buttonState, const Claw::SurfacePtr& ptr )
    {
        UIXImageButton::SetImageForButtonState( buttonState, ptr );

        m_buttonStateAnim[buttonState] = false;
    }

    void UIXAnimatedButton::SetAnim( const Claw::SurfacePtr& ptr )
    {
        m_image->SetImage( ptr );
        ((UIAnimationWrapper*)m_image)->SetLooped( true );
        ((UIAnimationWrapper*)m_image)->SetAnimated( true );

        // Invalidate container size
        Invalidate();

        if( !m_buttonStateFunc[BS_IDLE] )
        {
            // Set Functor to change button back to original state
            m_buttonStateFunc[BS_IDLE] = UIFunctorPtr( new AnimChangeFunctor( ptr ) );
            m_buttonStateAnim[BS_IDLE] = true;
        }
    }

    void UIXAnimatedButton::SetAnimForButtonState( ButtonState buttonState, const Claw::SurfacePtr& ptr )
    {
        CLAW_ASSERT( buttonState >= 0 && buttonState < BS_NUM );

        // Register functors changing button images
        m_buttonStateFunc[buttonState] = UIFunctorPtr( new AnimChangeFunctor( ptr ) );
        m_buttonStateAnim[buttonState] = true;

        // Additionally SetAnim() in idle state
        if( buttonState == BS_IDLE )
        {
            SetAnim( ptr );
        }
    }

    SurfaceWPtr UIXAnimatedButton::GetAnimForButtonState( ButtonState buttonState ) const
    {
        CLAW_ASSERT( buttonState >= 0 && buttonState < BS_NUM );
        if( m_buttonStateFunc[buttonState] && m_buttonStateAnim[buttonState] )
        {
            // Return Surface kept by fuctor for given state
            return ((UIXImageButton::ImageChangeFunctor*)m_buttonStateFunc[buttonState].GetPtr())->m_imageToChange;
        }
        return NULL;
    }

    void UIXAnimatedButton::LoadAnimForButtonState( ButtonState buttonState, const char* animPath )
    {
        SetAnimForButtonState( buttonState, Claw::AssetDict::Get< Claw::Surface >(animPath) );
    }

    void UIXAnimatedButton::PauseAnim( int frameIdx /*= -1*/ )
    {
        if ( frameIdx >= 0 && ((UIAnimationWrapper*)m_image)->IsAnimated() )
        {
            ((UIAnimationWrapper*)m_image)->SetFrame( frameIdx );
        }

        ((UIAnimationWrapper*)m_image)->Pause();
    }

    void UIXAnimatedButton::ResumeAnim( int frameIdx /*= -1*/ )
    {
        if ( frameIdx >= 0 && ((UIAnimationWrapper*)m_image)->IsAnimated() )
        {
            ((UIAnimationWrapper*)m_image)->SetFrame( frameIdx );
        }

        ((UIAnimationWrapper*)m_image)->Play();
    }

} // namespace ClawExt
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXImageLabelButton.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXImageLabelButton.hpp"
#include "claw_ext/ui/UILabel.hpp"

namespace ClawExt
{
    UIXImageLabelButton::UIXImageLabelButton( const char* fontXmlPath, ButtonType type /*= BT_NORMAL*/, const Claw::Color clr /*= Claw::Color( 255, 255, 255 )*/ )
        : UIXImageButton( type )
        , m_baseLine( 1.0f )
    {
        m_label = new UILabel( 0, fontXmlPath, clr );
        m_label->SetAlign( Align( AH_CENTER, AV_CENTER ) );

        AddComponent( m_label );
    }

    void UIXImageLabelButton::SetLabelText( const Claw::String& text )
    {
        m_label->SetSize( 0 );
        m_label->SetText( text );

        Invalidate();
    }

    void UIXImageLabelButton::SetLabelColor( const Claw::Color& clr )
    {
        m_label->SetColor( clr );
    }

    void UIXImageLabelButton::SetLabelAlign( const Align& align, bool keepPos /*= false*/ )
    {
        m_label->SetAlign( align, keepPos );

        // Invalidate label position
        m_label->SetParentOff( CalcAlignOffset( m_label ) );
    }

    void UIXImageLabelButton::SetLabelBaseLine( float baseLine )
    {
        if( m_baseLine != baseLine )
        {
            m_baseLine = baseLine;

            Invalidate();
        }
    }

} // namespace ClawExt
// EOF

#ifndef __UI_UIX_TEXT_AREA_HPP__
#define __UI_UIX_TEXT_AREA_HPP__

#include "claw/graphics/Color.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/graphics/text/Format.hpp"

#include "claw_ext/ui/UIComponent.hpp"

namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    class UIXTextArea : public UIComponent
    {
    public:
        UIXTextArea( const Claw::NarrowString& fontXmlPath, const Claw::Color color = Claw::Color( 255, 255, 255 ) );

        void Load( const Claw::XmlIt* xmlConfig );

        void Render( Claw::Surface* target );

        void SetColor( const Claw::Color& clr );
        void SetText( const Claw::String& text );
        void SetTextAlign( Claw::Text::Format::HorAlign align );

        void Invalidate();

    protected:
        Claw::String m_text;

        Claw::Text::Format m_format;
        Claw::ScreenTextPtr m_screenText;

    }; // class UIXTextArea

} // namespace ClawExt

#endif // __UI_UIX_TEXT_AREA_HPP__

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXImageButton.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_IMAGE_BUTTON_HPP__
#define __UI_UIX_IMAGE_BUTTON_HPP__

// Internal includes
#include "claw_ext/ui/UIPanel.hpp"
#include "claw_ext/ui/UIImage.hpp"
#include "claw_ext/ui/UIFunctor.hpp"

#include "claw/base/WeakPtr.hpp"

// External includes
#include <map>

namespace ClawExt
{
    // Forward declarations
    typedef Claw::WeakPtr<Claw::Surface> SurfaceWPtr;
    class UIXRadioGroup;

    //! Class defining simple or toggle button composed form single images.
    /*!
    * There can be 3 different images attached to a single button:
    * - Ilde image
    * - Cursor over image
    * - Pressed image
    */
    class UIXImageButton : public UIPanel
    {
        friend class UIXRadioGroup;
    public:
        enum ButtonState
        {
            BS_IDLE = 0,        //!< Button is in idle/unpressed state.
            BS_CURSOR_OVER,     //!< Cursor rolled over the button (hover state).
            BS_PRESSED,         //!< Button is pressed/selected.

            BS_NUM
        }; // enum ButtonEvent

        enum ButtonType
        {
            BT_NORMAL,          //!< Button can be pressed. Unpress is done when key is released.
            BT_TOGGLE,          //!< Button is toggled presees/unpressed state every time key is released.
            BT_RADIO            //!< Button can be pressed only once. To unpress button SetSelected() method must be used
        }; // enum ButtonType

        //! Default constuctor setting postion to 0,0.
                            UIXImageButton( ButtonType type = BT_NORMAL );

        //! Constructor defining image position.
                            UIXImageButton( const Pos& pos, ButtonType type = BT_NORMAL );

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Set button type.
        inline void         SetType( ButtonType type );

        //! Get button type.
        inline ButtonType   GetType() const;

        //! Set button selection state.
        void                SetSelected( bool selected );

        //! Check if button is selected/pressed.
        inline bool         IsSelected() const;

        //! Load image from the path and invalidate component size.
        void                LoadImg( const char* imgPath );

        //! Setup image content to be displayed.
        /*
        * Same as calling: SetImageForButtonState( BS_IDLE, ptr );
        */
        virtual void        SetImage( const Claw::SurfacePtr& ptr );

        //! Acquire image surface smart pointer.
        Claw::SurfacePtr    GetImage() const;

        //! Setup image content for specified ButtonState.
        virtual void        SetImageForButtonState( ButtonState buttonState, const Claw::SurfacePtr& ptr );

        //! Acquire image surface for specified ButtonState (weak pointer is returned).
        SurfaceWPtr         GetImageForButtonState( ButtonState buttonState ) const;

        //! Load image (immediately) via AssetDict for specified ButtonState.
        void                LoadImageForButtonState( ButtonState buttonState, const char* imgPath );

        //! Set image fliping mode.
        inline void         SetFlipMode( UIImage::FlipMode flipMode );

        //! Get current image fliping mode.
        inline
        UIImage::FlipMode   GetFlipMode() const;

    protected:
        //! Constuctor with an image component passed to, for derived implementations.
                            UIXImageButton( ButtonType type, UIImage* image );

        //! Override base class implementations
        virtual void        ProcessEvent( CursorEvent ev, CursorIdx idx, const Pos& pos );

        // Declare internal class
        class ImageChangeFunctor : public UIFunctor
        {
        public:
                                ImageChangeFunctor( const Claw::SurfacePtr& changeToImage );
            virtual void        Call( UIComponent* cmp, UIComponent::CursorEvent ev, UIComponent::CursorIdx idx, const UIComponent::Pos& cursorPos );

            Claw::SurfacePtr    m_imageToChange;

        }; // class ImageChangeFunctor

    protected:
        void                SetRadioGroup( UIXRadioGroup* parent );

        UIImage*            m_image;
        UIFunctorPtr        m_buttonStateFunc[BS_NUM];

    private:
        ButtonType          m_type;
        bool                m_selected;
        bool                m_cursorOver;
        bool                m_draggedOut;
        bool                m_togglePressed;
        UIXRadioGroup*      m_parentRadioGroup;

    }; // class UIImageButton

    inline void UIXImageButton::SetType( ButtonType type )
    {
        m_type = type;
    }

    inline UIXImageButton::ButtonType UIXImageButton::GetType() const
    {
        return m_type;
    }

    inline bool UIXImageButton::IsSelected() const
    {
        return m_selected;
    }

    inline Claw::SurfacePtr UIXImageButton::GetImage() const
    {
        return m_image->GetImage();
    }

    inline void UIXImageButton::SetFlipMode( UIImage::FlipMode flipMode )
    {
        m_image->SetFlipMode( flipMode );
    }

    inline UIImage::FlipMode UIXImageButton::GetFlipMode() const
    {
        return m_image->GetFlipMode();
    }

} // namespace ClawExt

#endif // define __UI_UIX_IMAGE_BUTTON_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXScrolledComponent.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_SCROLLED_COMPONENT_HPP__
#define __UI_UIX_SCROLLED_COMPONENT_HPP__

// Internal includes
#include "claw_ext/ui/UIContainer.hpp"

// Extermal includes

namespace ClawExt
{
    //! Component that is capable to perform conttent scrolling using cursor (draggs and swipes).
    class UIXScrolledComponent : public UIContainer
    {
    public:
        //! Default constructor.
                                UIXScrolledComponent();

        //! Load content form xml definition.
        virtual void            Load( const Claw::XmlIt* xmlConfig );

        //! Render on the top of current display buffer.
        virtual void            Render( Claw::Surface* target );

        //! Update scrolling logic.
        virtual void            Update( float dt );

        //! Method called internally to refresh components/containers content during hierarchy component attach.
        virtual void            Invalidate();

        //! Set content component.
        /*!
        * Relative layouting is suppored. But should be used only in one axis to take 
        * advantage of using UIXScrolledComponent. Returns previous content if presetn.
        */
        UIComponent*            SetContent( UIComponent* content, bool resetOffset = true );

        //! Get current content component.
        inline UIComponent*     GetContent() const;

        //! Set inner padding in pixels.
        void                    SetPadding( const Pos& padding );

        //! Get current padding settings.
        inline Pos              GetPadding() const;

        //! Set offset for clipping region boundaries.
        inline void             SetClippingOffset( const ViewRect& offset );

        //! Get current offset of clipping region.
        inline const ViewRect&  GetClippingOffset() const;

        //! Perform calcuatlion of relative layout.
        virtual void            CalculateRelativeLayout( const UIComponent* relativeTo = NULL );
        
        //! Override UIComponent cursor movement callback to propagate it to childs.
        virtual void            OnCursorMove( const Pos& newPos, CursorIdx idx );

        //! Override UIComponent cursor press callback to propagate it to childs.
        virtual void            OnCursorPress( const Pos& newPos, CursorIdx idx );

        //! Manually set scroll offset. Second param if position change should be animated.
        void                    SetOffset( const Claw::Vector2f& newOffset, bool animated = true );

        //! Get current content offset vector.
        const Claw::Vector2f&   GetCurrentOffset() const;

    protected:
        //! Process cursor events.
        virtual void            ProcessEvent( CursorEvent ev, CursorIdx idx, const Pos& pos );

        //! Clam scroll offset so content will nicely fit clipping area.
        void                    ClampOffset( Claw::Vector2f& offset );

        //! Hide superclass implementation.
        virtual void            AddComponent( UIComponent* component );

        //! Calculates alignment offset.
        Pos                     CalcAlignOffset( const UIComponent* component ) const;

        void                    RefreshContentOffset();

        virtual void            NotifyDownTree( ComponentNotification cn );

    private:
        Claw::Vector2f      m_scrollOffset;                 // Current scroll offset
        Claw::Vector2f      m_scrollOffsetBegin;            // Scroll offset when new dragg has begun
        Claw::Vector2f      m_dragBegin;                    // Dragging - cursor start position
        Claw::Vector2f      m_targetOffset;                 // Offset that is desired - m_scrollOffset will be interpolated to it
        Pos                 m_alignOffset;                  // Offset for horizontal and vertical alignment
        CursorIdx           m_scrollIdx;                    // Scrolling currsor index

        Claw::Vector2f      m_samplePos;                    // Cursor position on last velocity sample
        Claw::Vector2f      m_currentPos;                   // Current cursor possition
        float               m_sampleTimer;                  // Timer user to calculate velocity sampling frequency
        float               m_draggTime;                    // How long current drag is performed
        Claw::Vector2f      m_lastVelocity;                 // Last sampled velocity
        Claw::Vector2f      m_velocity;                     // Current scrolling velocity (inertial scrolling)

        Pos                 m_padding;                      // Inner padding
        ViewRect            m_clippingOffset;               // Offset for clipping region

        UIComponent*        m_content;                      // Content component
        bool                m_pressedCursors[CURSORS_NUM];  // Press status of all possible cursors

        Claw::Vector2f      m_offsetFilter;                 // Filtered offset value used to smooth dragging movement
        bool                m_offsetFilterInit;

    }; // class UIXRadioGroup


    inline UIComponent* UIXScrolledComponent::GetContent() const
    {
        return m_content;
    }

    inline UIComponent::Pos UIXScrolledComponent::GetPadding() const
    {
        return m_padding;
    }

    inline const UIXScrolledComponent::ViewRect& UIXScrolledComponent::GetClippingOffset() const
    {
        return m_clippingOffset;
    }

    inline void UIXScrolledComponent::SetClippingOffset( const ViewRect& offset )
    {
        m_clippingOffset = offset;
    }

} // namespace ClawExt

#endif // __UI_UIX_SCROLLED_COMPONENT_HPP__

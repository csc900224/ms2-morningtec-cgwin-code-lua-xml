//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXPageIndicator.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXPageIndicator.hpp"
#include "claw_ext/uix/UIXScrolledComponent.hpp"
#include "claw_ext/ui/UIImage.hpp"

namespace ClawExt
{
    UIXPageIndicator::UIXPageIndicator( UIList::ListOrientation orientation, const char* idleSegment, const char* currentSegment, int numSegments /*= 3*/ )
        : m_idleImage( idleSegment )
        , m_selectedImage( currentSegment )
        , m_animTime( 0 )
        , m_selectedIdx( -1 )
        , m_watchedComponent( NULL )
    {
        // Initialize component
        SetOrientation( orientation );
        SetNumSegments( numSegments );
        SetProgress( 0 );
    }

    void UIXPageIndicator::SetWatchedComponent( const UIXScrolledComponent* scroll )
    {
        m_watchedComponent = scroll;
        UpdateWatchedComponent();
    }

    void UIXPageIndicator::UpdateWatchedComponent()
    {
        // If there is watched component with conent set
        if( m_watchedComponent && m_watchedComponent->GetContent() )
        {
            // Convert vector2 based offset to <0-1> float based progress value
            const Claw::Vector2f& offset = m_watchedComponent->GetCurrentOffset();
            const Size& contentSize = m_watchedComponent->GetContent()->GetSize();
            const Size& componentSize = m_watchedComponent->GetSize();
            const Claw::Vector2i maxOffset = contentSize - componentSize;

            // Set current progress
            SetProgress( offset.Length() / maxOffset.Length() );
        }
    }

    void UIXPageIndicator::SetNumSegments( int numSegments )
    {
        // If number of segments differs
        if( numSegments != GetNumSegments() )
        {
            // Delete all sub components
            ClearComponents(true);
            SetSize( 0 );

            // Recreate desired number of segments
            for( int i = 0; i < numSegments; ++i )
            {
                IndicatorSegment* segment = new IndicatorSegment( m_idleImage, m_selectedImage, m_animTime );
                AddComponent( segment );
            }

            // Refres status
            m_selectedIdx = -1;
            UpdateWatchedComponent();
        }
    }

    void UIXPageIndicator::SetProgress( float progress )
    {
        // Convert <0-1> progrss value to page index
        progress = Claw::MinMax( progress, 0.0f, 1.0f );
        SetPage( (int)(progress * (GetNumSegments() - 1) + 0.5f) );
    }

    void UIXPageIndicator::SetPage( int page )
    {
        // If selected idx differs
        if( m_selectedIdx != page )
        {
            m_selectedIdx = page;

            // Mark appropriate segment as selected
            for( size_t i = 0; i < m_children.size(); ++i )
            {
                ((IndicatorSegment*)m_children[i])->SetSelected( i == m_selectedIdx );
            }
        }
    }

    void UIXPageIndicator::SetAnimTime( float animTime )
    {
        // If anim time differs
        if( animTime != m_animTime )
        {
            SubComponentsIt it = m_children.begin();
            SubComponentsIt end = m_children.end();

            // Set same anim time for each segment
            for( ; it != end; ++it )
            {
                ((IndicatorSegment*)(*it))->SetAnimTime( animTime );
            }

            m_animTime = animTime;
        }
    }

    void UIXPageIndicator::Update( float dt )
    {
        UIList::Update( dt );
        UpdateWatchedComponent();
    }

    UIXPageIndicator::IndicatorSegment::IndicatorSegment( const char* idleImage, const char* selectedImage, float animTime )
        : m_animTime( animTime )
        , m_timer( 0 )
        , m_targetProgress( 0 )
        , m_animProgress( 0 )
    {
        CreateLayout( idleImage, selectedImage );
        UpdateImageAlpha();
    }

    void UIXPageIndicator::IndicatorSegment::CreateLayout( const char* idleImage, const char* selectedImage )
    {
        // Selected image
        m_selectedImg = new UIImage();
        m_selectedImg->LoadImg( selectedImage );
        m_selectedImg->SetAlign( Align( AH_CENTER, AV_CENTER ) );

        // Idle image
        m_idleImg = new UIImage();
        m_idleImg->LoadImg( idleImage );
        m_idleImg->SetAlign( Align( AH_CENTER, AV_CENTER ) );

        // Add all images
        AddComponent( m_selectedImg );
        AddComponent( m_idleImg );
    }

    void UIXPageIndicator::IndicatorSegment::SetSelected( bool selected )
    {
        // Set correct m_targetProgress and m_timer depending on current selection state
        if( m_targetProgress == 1.0f && !selected || m_targetProgress == 0.0f && selected )
        {
            m_targetProgress = selected ? 1.0f : 0.0f;
            m_timer = m_animTime - m_timer;
        }
    }

    void UIXPageIndicator::IndicatorSegment::Update( float dt )
    {
        // Base component update
        UIPanel::Update( dt );

        if( m_timer > 0 )
        {
            // Update animation
            m_timer = Claw::Max( m_timer - dt, 0.0f );
            float tmpProgress = m_timer / m_animTime;
            m_animProgress = m_targetProgress * (1.0f - tmpProgress) + (1 - m_targetProgress) * tmpProgress;
            UpdateImageAlpha();
        }
        else if( m_targetProgress != m_animProgress )
        {
            // Should enter here, only if animTime == 0
            m_animProgress = m_targetProgress;
            UpdateImageAlpha();
        }
    }

    void UIXPageIndicator::IndicatorSegment::UpdateImageAlpha()
    {
        m_selectedImg->SetAlpha( m_animProgress );

        //// Image size can be animated, bu size must be float based!
        //Claw::Vector2f scaledSize(m_selectedImg->GetImageSize().m_x, m_selectedImg->GetImageSize().m_y);
        //scaledSize *= (0.75f + m_animProgress * 0.25f);
        //m_idleImg->SetSize( Size( scaledSize.m_x, scaledSize.m_y ) );
        //Invalidate();
    }

} // namespace ClawExt

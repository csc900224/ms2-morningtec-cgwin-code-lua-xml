//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXPageIndicator.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_PAGE_INDICATOR_HPP__
#define __UI_UIX_PAGE_INDICATOR_HPP__

// Internal includes
#include "claw_ext/ui/UIList.hpp"
#include "claw_ext/ui/UIPanel.hpp"

namespace ClawExt
{
    // Forward declarations
    class UIXScrolledComponent;
    class UIImage;

    //! Class responsible for rendering indicator of current page (e.g. of scrolled component).
    /*!
    * Can be used to render e.g. iOS-like dots indicator below scrolled desktops.
    * You can define number of segments and images for those segments.
    * One segment consists of two images:
    *   1) Idle - always visible (place holder),
    *   2) Selected - rendered above Idle only on segement indicating current page.
    */
    class UIXPageIndicator : public UIList
    {
    public:
        //! Constructor that takes: indicator orientation, segment images and initial numer of segments.
                        UIXPageIndicator( UIList::ListOrientation orientation, const char* idleSegment, const char* currentSegment, int numSegments = 3 );

        //! Sets UIXScrolledComponent which offset is watched every frame and translated to page idx.
        void            SetWatchedComponent( const UIXScrolledComponent* scroll );

        //! Set number of segments (e.g. number of dots).
        void            SetNumSegments( int numSegments );

        //! Get number of segments.
        inline int      GetNumSegments() const;

        //! Set indicator progress <0-1>. Its translated to appropriate page idx.
        void            SetProgress( float progress );

        //! Get current indicator progress <0-1>.
        inline float    GetProgress() const;

        //! Set active page idx. Page param is 0 indexed and has to be lower than number of segments.
        void            SetPage( int page );

        //! Get active page idx.
        inline int      GetPage() const;

        //! Set duration of active/inactive segment transition animiation (alpha cross-fade). 
        void            SetAnimTime( float animTime );

        //! Get duration of transition animation.
        inline float    GetAnimTime() const;

        //! Update component.
        void            Update( float dt );

    protected:
        //! Update page idx using offset of watched component.
        void            UpdateWatchedComponent();

        //! Internal class representing single segment of UIXPageIndicator.
        class IndicatorSegment : public UIPanel
        {
        public:
            //! Constructor that takes: segment images and animation duration.
                        IndicatorSegment( const char* idleImage, const char* selectedImage, float animTime );

            //! Change segment selection state - transition will be alpha-animated.
            void        SetSelected( bool selected );

            //! Check if segment is selected.
            inline bool IsSelected() const;

            //! Change transition animation duration.
            inline void SetAnimTime( float animTime );

            //! Updated component.
            void        Update( float dt );

        protected:
            //! Create layout of segment component.
            void    CreateLayout( const char* idleImage, const char* selectedImage );

            //! Synchronize sub-images alpha value using animation progress.
            void    UpdateImageAlpha();

        private:
            UIImage*    m_idleImg;
            UIImage*    m_selectedImg;

            float       m_animTime;
            bool        m_selected;
            float       m_timer;
            float       m_targetProgress;
            float       m_animProgress;
        }; // class IndicatorSegment

    private:
        const UIXScrolledComponent* m_watchedComponent;
        const char*                 m_idleImage;
        const char*                 m_selectedImage;
        float                       m_animTime;
        int                         m_selectedIdx;

    }; // class UIXPageIndicator


    inline int UIXPageIndicator::GetNumSegments() const
    {
        return (int)m_children.size();
    }

    inline float UIXPageIndicator::GetProgress() const
    {
        return Claw::Max( 0.0f, float(m_selectedIdx) / (GetNumSegments() - 1) );
    }

    inline int UIXPageIndicator::GetPage() const
    {
        return m_selectedIdx;
    }

    inline float UIXPageIndicator::GetAnimTime() const
    {
        return m_animTime;
    }

    inline void UIXPageIndicator::IndicatorSegment::SetAnimTime( float animTime )
    {
        m_animTime = animTime;
    }

    inline bool UIXPageIndicator::IndicatorSegment::IsSelected() const
    {
        return m_selected;
    }

} // namespace ClawExt

#endif // __UI_UIX_PAGE_INDICATOR_HPP__

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXImageButton.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/uix/UIXImageButton.hpp"
#include "claw_ext/uix/UIXRadioGroup.hpp"

// External includes
#include "claw/base/AssetDict.hpp"

namespace ClawExt
{

    UIXImageButton::ImageChangeFunctor::ImageChangeFunctor( const Claw::SurfacePtr& changeToImage )
        : m_imageToChange( changeToImage )
    {}

    void UIXImageButton::ImageChangeFunctor::Call( UIComponent* cmp, UIComponent::CursorEvent ev, UIComponent::CursorIdx idx, const UIComponent::Pos& cursorPos )
    {
        CLAW_ASSERT(cmp);
        ((UIXImageButton*)cmp)->SetImage( m_imageToChange );
    }


    UIXImageButton::UIXImageButton( ButtonType type /*= BT_NORMAL*/ )
        : m_image( new UIImage )
        , m_type( type )
        , m_selected( false )
        , m_cursorOver( false )
        , m_draggedOut( false )
        , m_togglePressed( false )
        , m_parentRadioGroup( NULL )
    {
        AddComponent( m_image );
    }

    UIXImageButton::UIXImageButton( const Pos& pos, ButtonType type /*= BT_NORMAL*/ )
        : m_image( new UIImage( pos ) )
        , m_type( type )
        , m_selected( false )
        , m_cursorOver( false )
        , m_draggedOut( false )
        , m_togglePressed( false )
        , m_parentRadioGroup( NULL )
    {
        AddComponent( m_image );
    }

    UIXImageButton::UIXImageButton( ButtonType type /*= BT_NORMAL*/, UIImage* image )
        : m_image( image )
        , m_type( type )
        , m_selected( false )
        , m_cursorOver( false )
        , m_draggedOut( false )
        , m_togglePressed( false )
        , m_parentRadioGroup( NULL )
    {
        AddComponent( m_image );
    }

    void UIXImageButton::SetSelected( bool selected )
    {
        if( m_selected != selected )
        {
            m_selected = selected;
            if( selected && m_buttonStateFunc[BS_PRESSED] )
            {
                (*m_buttonStateFunc[BS_PRESSED])( this, CE_PRESS, -1, UIComponent::Pos(-1, -1) );
            }
            else if( !selected && m_buttonStateFunc[BS_IDLE] )
            {
                (*m_buttonStateFunc[BS_IDLE])( this, CE_RELEASE, -1, UIComponent::Pos(-1, -1) );
            }

            // Notiffy radio group that selection state was changed
            if( m_parentRadioGroup )
            {
                m_parentRadioGroup->OnButtonSelectionChanged( this );
            }
        }
    }

    void UIXImageButton::Load( const Claw::XmlIt* xmlConfig )
    {
        // TODO: Implement me please!
    }

    void UIXImageButton::LoadImg( const char* imgPath )
    {
        using namespace Claw;
        SurfacePtr surfPtr( NULL );
        if( imgPath == NULL )
        {
            // Setup empty image - no graphics will be displayed
            SetImage( surfPtr );
            return;
        }

        surfPtr = AssetDict::Get< Surface >( imgPath );

        SetImage( surfPtr );
    }

    void UIXImageButton::SetImage( const Claw::SurfacePtr& ptr )
    {
        m_image->SetImage( ptr );

        // Invalidate container size
        Invalidate();

        if( !m_buttonStateFunc[BS_IDLE] )
        {
            // Set Functor to change button back to original state
            m_buttonStateFunc[BS_IDLE] = UIFunctorPtr( new ImageChangeFunctor( ptr ) );
        }
    }

    void UIXImageButton::SetImageForButtonState( ButtonState buttonState, const Claw::SurfacePtr& ptr )
    {
        CLAW_ASSERT( buttonState >= 0 && buttonState < BS_NUM );

        // Register functors changing button images
        m_buttonStateFunc[buttonState] = UIFunctorPtr( new ImageChangeFunctor( ptr ) );

        if( buttonState == BS_IDLE )
        {
            // Additionally SetImage() for idle state
            m_image->SetImage( ptr );

            // Invalidate container size
            Invalidate();
        }
    }

    SurfaceWPtr UIXImageButton::GetImageForButtonState( ButtonState buttonState ) const
    {
        CLAW_ASSERT( buttonState >= 0 && buttonState < BS_NUM );
        if( m_buttonStateFunc[buttonState] )
        {
            // Return Surface kept by fuctor for given state
            return ((ImageChangeFunctor*)m_buttonStateFunc[buttonState].GetPtr())->m_imageToChange;
        }
        return NULL;
    }

    void UIXImageButton::LoadImageForButtonState( ButtonState buttonState, const char* imgPath )
    {
        SetImageForButtonState( buttonState, Claw::AssetDict::Get< Claw::Surface >(imgPath) );
    }

    void UIXImageButton::ProcessEvent( CursorEvent ev, CursorIdx idx, const Pos& pos )
    {
        ButtonState be = (ButtonState)-1;

        const bool prevSelected = m_selected;

        // Ordinary button
        if( m_type == BT_NORMAL )
        {
            switch( ev )
            {
            case CE_PRESS:
                m_selected = true;
                be = BS_PRESSED;
                break;

            case CE_ROLL_IN:
                m_cursorOver = true;
                if( !m_selected )
                    be = BS_CURSOR_OVER;
                break;

            case CE_ROLL_OUT:
                m_cursorOver = false;
                if( !m_selected )
                    be = BS_IDLE;
                break;

            case CE_RELEASE:
            case CE_RELEASE_DRAG_OUT:
                m_selected = false;
                m_draggedOut = false;
                if( m_cursorOver && m_buttonStateFunc[BS_CURSOR_OVER] )
                    be = BS_CURSOR_OVER;
                else
                    be = BS_IDLE;
                break;

            case CE_DRAG_OUT:
                m_cursorOver = false;
                if( m_selected )
                {
                    m_draggedOut = true;
                    be = BS_IDLE;
                }
                break;

            case CE_DRAG_IN:
                m_cursorOver = true;
                if( m_draggedOut )
                {
                    m_draggedOut = false;
                    if( m_buttonStateFunc[BS_PRESSED] )
                        be = BS_PRESSED;
                    else
                        be = BS_CURSOR_OVER;
                }
                break;
            }
        }
        // Toggle button
        else
        {
            switch( ev )
            {
            case CE_PRESS:
                m_togglePressed = true;
                break;

            case CE_ROLL_IN:
                m_cursorOver = true;
                be = BS_CURSOR_OVER;
                break;

            case CE_ROLL_OUT:
                m_cursorOver = false;
                be = m_selected ? BS_PRESSED : BS_IDLE;
                break;

            case CE_RELEASE_DRAG_OUT:
                m_togglePressed = false;
                break;

            case CE_RELEASE:
                if( m_type == BT_TOGGLE )
                {
                    m_selected = !m_selected;
                }
                else
                {
                    m_selected = true;
                }
                m_togglePressed = false;
                be = m_selected ? BS_PRESSED : BS_IDLE;
                break;

            case CE_DRAG_OUT:
                m_cursorOver = false;
                if( m_togglePressed )
                {
                    m_draggedOut = true;
                    be = m_selected ? BS_PRESSED : BS_IDLE;
                }
                break;

            case CE_DRAG_IN:
                m_cursorOver = true;
                m_draggedOut = false;
                break;
            }
        }

        // Call appropriate functor
        if( be >= 0 && m_buttonStateFunc[be] )
        {
            (*m_buttonStateFunc[be])( this, ev, idx, pos );
        }

        // Base class implementation
        UIPanel::ProcessEvent( ev, idx, pos );

        // Notiffy radio group that selection state was changed
        if( m_parentRadioGroup && prevSelected != m_selected )
        {
            m_parentRadioGroup->OnButtonSelectionChanged( this );
        }
    }

    void UIXImageButton::SetRadioGroup( UIXRadioGroup* parent )
    {
        m_parentRadioGroup = parent;
    }

} /// namespace ClawExt

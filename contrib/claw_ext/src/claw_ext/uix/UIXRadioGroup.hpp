//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/uix/UIXRadioGroup.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UIX_RADIO_GROUP_HPP__
#define __UI_UIX_RADIO_GROUP_HPP__

// Internal includes
#include "claw_ext/ui/UIList.hpp"

namespace ClawExt
{
    // Forward declarations
    class UIXImageButton;

    //! Typical radio group that allows only one button to be selected at same time.
    class UIXRadioGroup : public UIList
    {
    public:
        //! Default constructor. All buttons will be unchecked during add.
                                UIXRadioGroup();

        //! Contructor with initial selection option. Button with given index will be selected by default (0 for first one).
                                UIXRadioGroup( int initialSelectIdx );

        //! Get currently selected button.
        inline UIXImageButton*  GetSelectedButton() const;

        //! Add sub-button.
        virtual void            AddComponent( UIXImageButton* button );

        //! Callback for child button when its selection state changes.
        void                    OnButtonSelectionChanged( UIXImageButton* selectedButton );

    private:
        //! Hide default implementation.
        virtual void            AddComponent( UIComponent* component );

        int                     m_initialSelectIdx;
        UIXImageButton*         m_selectedButton;

    }; // class UIXRadioGroup


    inline UIXImageButton* UIXRadioGroup::GetSelectedButton() const
    {
        return m_selectedButton;
    }

} // namespace ClawExt

#endif // __UI_UIX_RADIO_GROUP_HPP__
#include "claw_ext/simple_ui/extensions/BasicMenuSystem.hpp"

namespace SimpleUIExt
{

    Screen::Screen( const Claw::NarrowString& name, float designWidth, float designHeight, float showHideAnimationTime )
        : m_name( name )
        , m_designWidth( designWidth )
        , m_designHeight( designHeight )
        , m_showHideAnimationTime( showHideAnimationTime )
        , m_stack( NULL )
        , m_transition( false )
        , m_userData( NULL )
    {}

    void Screen::Reload()
    {
        PrivateReload();
        OnReload();
    }

    void Screen::Resize()
    {
        PrivateReload();
        OnReload();
    }

    void Screen::Show( bool animate )
    {
        Lock();
        OnShowStarted();

        if( animate )
        {
            m_ui->Find< SimpleUIExt::Animation< float > >( "showHideAnim" )->Play( false, OnShowAnimFinished, this );
        }
        else
        {
            OnShowAnimFinished( NULL, this );
        }
    }

    void Screen::Hide( bool animate )
    {
        Lock();
        OnHideStarted();

        if( animate )
        {
            m_ui->Find< SimpleUIExt::Animation< float > >( "showHideAnim" )->Play( true, OnHideAnimFinished, this );
        }
        else
        {
            OnHideAnimFinished( NULL, this );
        }
    }

    void Screen::Update( float dt )
    {
        m_ui->Update( dt );
    }

    void Screen::Render( Claw::Surface* target )
    {
        m_ui->Render( target );
    }

    void Screen::TouchDown( int x, int y )
    {
        if( !m_transition )
        {
            m_touch->TouchDown( x, y );
        }
    }

    void Screen::TouchUp( int x, int y )
    {
        if( !m_transition )
        {
            m_touch->TouchUp( x, y );
        }
    }

    void Screen::TouchMove( int x, int y )
    {
        if( !m_transition )
        {
            m_touch->TouchMove( x, y );
        }
    }

    void Screen::PrivateReload()
    {
        m_ui.Release();
        m_ui.Reset( new SimpleUI::Instance( m_designWidth, m_designHeight ) );
        m_ui->SetUserData( this );

        new SimpleUIExt::Animation< float >( "showHideAnim", 0, 1, m_showHideAnimationTime, OnShowHideAnim, this, m_ui->GetRootFrame() );

        m_touch.Reset( ( new SimpleUIExt::BasicTouch( m_ui ) )->SetOnTouchDownFunc( OnTouchDown, this )->SetOnTouchUpFunc( OnTouchUp, this )->SetOnTouchMoveFunc( OnTouchMove, this ) );
    }

    void Screen::OnShowAnimFinished( SimpleUIExt::Animation< float >* animation, void* userData )
    {
        Screen* s = static_cast< Screen* >( userData );
        if( s->m_stack ) { s->m_stack->OnShowAnimFinished( s ); }
        s->OnShowFinished();
        s->Unlock();
    }

    void Screen::OnHideAnimFinished( SimpleUIExt::Animation< float >* animation, void* userData )
    {
        Screen* s = static_cast< Screen* >( userData );
        if( s->m_stack ) { s->m_stack->OnHideAnimFinished( s ); }
        s->OnHideFinished();
        s->Unlock();
    }

    void Screen::Lock()
    {
        //CLAW_ASSERT( !m_transition )
        m_transition = true;
    }

    void Screen::Unlock()
    {
        CLAW_ASSERT( m_transition )
        m_transition = false;
    }

    // -------------------------------- //

    ScreenStack::ScreenStack()
        : m_push( NULL )
        , m_pop( NULL )
        , m_doPop( false )
        , m_doPush( false )
        , m_userData( NULL )
    {}

    void ScreenStack::Add( ScreenPtr screen )
    {
        CLAW_MSG_VERIFY( m_screens.insert( std::make_pair( screen->GetName(), screen ) ).second, "Duplicate screen name: " << screen->GetName() )
        CLAW_MSG_VERIFY( !screen->m_stack, "Screen already added." )
        screen->m_stack = this;
        CLAW_MSG_VERIFY( !screen->m_ui, "Dirty screen." )
        screen->Reload();
    }

    void ScreenStack::Init( const Claw::NarrowString& firstScreen )
    {
        CLAW_MSG_ASSERT( m_stack.empty(), "Already initialized." )
        m_stack.push_back( GetScreen( firstScreen ) );
        m_stack.back()->Show( false );
    }

    Screen* ScreenStack::GetScreen( const Claw::NarrowString& name )
    {
        std::map< Claw::NarrowString, ScreenPtr >::const_iterator it = m_screens.find( name );
        CLAW_MSG_ASSERT( it != m_screens.end(), "Screen not found: " << name )
        return it->second;
    }

    void ScreenStack::Push( const Claw::NarrowString& name )
    {
        Screen* screen = GetScreen( name );
        if ( m_push && m_push == screen )
            return;

        if ( m_pop && m_pop == screen )
        {
            m_pop = NULL;
            m_doPop = false;
        }

        CLAW_MSG_VERIFY( !m_push, "Previous push didn't finished yet." )
        CLAW_MSG_VERIFY( !m_pop, "Previous pop didn't finished yet." )

        OnPushStarted( name );
        m_push = screen;
        m_doPush = true;

        if ( m_stack.size() )
        {
            m_stack.back()->Hide( true );
        }
    }

    void ScreenStack::Pop( const Claw::NarrowString& name )
    {
        Screen* screen = GetScreen( name );
        if ( m_pop && m_pop == screen )
            return;

        CLAW_MSG_VERIFY( m_stack.back() == screen, "Screen is not on top." )

        if ( m_push && m_push == screen )
        {
            m_push = NULL;
            m_doPush = false;
        }

        CLAW_MSG_VERIFY( !m_push, "Previous push didn't finished yet." )
        CLAW_MSG_VERIFY( !m_pop, "Previous pop didn't finished yet." )

        OnPopStarted( name );
        m_pop = screen;
        m_pop->Hide( true );

        if( m_stack.size() > 1 )
        {
            m_stack[ m_stack.size() - 2 ]->Show( true );
        }
    }

    Screen* ScreenStack::Top() const
    {
        return m_stack.size() ? m_stack.back() : NULL;
    }

    void ScreenStack::OnShowAnimFinished( Screen* screen )
    {
        if( screen == m_push )
        {
            m_push = NULL;
            OnPushFinished( screen->GetName() );
        }
    }

    void ScreenStack::OnHideAnimFinished( Screen* screen )
    {
        if( screen == m_pop )
        {
            m_doPop = true; // actually popped in Update
        }
    }

    void ScreenStack::Resize()
    {
        for( std::vector< Screen* >::iterator it = m_stack.begin(); it != m_stack.end(); it++ )
        {
            (*it)->Resize();
        }
    }

    void ScreenStack::Update( float dt )
    {
        if( m_doPop )
        {
            CLAW_MSG_VERIFY( m_stack.back() == m_pop, "Unexpected situation." )
            m_stack.pop_back();

            const Claw::NarrowString& name = m_pop->GetName();

            m_pop = NULL;
            m_doPop = false;

            OnPopFinished( name );
        }

        if( m_doPush )
        {
            CLAW_ASSERT( m_push );
            if ( m_stack.back() != m_push )
            {
                m_stack.push_back( m_push );
            }

            m_stack.back()->Show( true );
            m_doPush = false;
        }

        for( std::vector< Screen* >::iterator it = m_stack.begin(); it != m_stack.end(); it++ )
        {
            (*it)->Update( dt );
        }
    }

    void ScreenStack::Render( Claw::Surface* target )
    {
        for( std::vector< Screen* >::iterator it = m_stack.begin(); it != m_stack.end(); it++ )
        {
            (*it)->Render( target );
        }
    }

    void ScreenStack::TouchDown( int x, int y )
    {
        if( m_stack.size() )
        {
            m_stack.back()->TouchDown( x, y );
        }
    }

    void ScreenStack::TouchUp( int x, int y )
    {
        if( m_stack.size() )
        {
            m_stack.back()->TouchUp( x, y );
        }
    }

    void ScreenStack::TouchMove( int x, int y )
    {
        if( m_stack.size() )
        {
            m_stack.back()->TouchMove( x, y );
        }
    }

    void ScreenStack::FocusChange( bool focused )
    {
        if( m_stack.size() )
        {
            m_stack.back()->FocusChange( focused );
        }
    }
}

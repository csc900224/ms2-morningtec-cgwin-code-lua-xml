#ifndef __INCLUDED_SIMPLE_UI_BASIC_COMPONENTS_HPP__
#define __INCLUDED_SIMPLE_UI_BASIC_COMPONENTS_HPP__

#include "claw_ext/simple_ui/SimpleUI.hpp"

namespace SimpleUIExt
{

    class Button: public SimpleUI::Item
    {
    public:
        Button( const Claw::NarrowString& name, const Claw::NarrowString& normal, const Claw::NarrowString& pressed, const Claw::NarrowString& disabled, Item* parent );

        enum State
        {
            STATE_NORMAL,
            STATE_PRESSED,
            STATE_DISABLED
        };

        Button* SetState( State state );
        State GetState() const;

        virtual float GetWidth() const { return m_normalImage->GetWidth(); }
        virtual float GetHeight() const { return m_normalImage->GetHeight(); }

        SimpleUI::Image* GetNormalImage() const { return m_normalImage; }
        SimpleUI::Image* GetPressedImage() const { return m_pressedImage; }
        SimpleUI::Image* GetDisabledImage() const { return m_disabledImage; }

    protected:
        Button( const Claw::NarrowString& name, Item* parent )
            : SimpleUI::Item( name, parent )
        {}

        SimpleUI::Image* m_normalImage;
        SimpleUI::Image* m_pressedImage;
        SimpleUI::Image* m_disabledImage;
    };

    // -------------------------------- //

    class Checkbox: public SimpleUI::Item
    {
    public:
        Checkbox( const Claw::NarrowString& name, const Claw::NarrowString& normal, const Claw::NarrowString& checked, Item* parent );

        Checkbox* SetChecked( bool checked );
        bool IsChecked() const { return m_checkedImage->IsEnabled(); }

        virtual float GetWidth() const { return m_normalImage->GetWidth(); }
        virtual float GetHeight() const { return m_normalImage->GetHeight(); }

        SimpleUI::Image* GetNormalImage() const { return m_normalImage; }
        SimpleUI::Image* GetCheckedImage() const { return m_checkedImage; }

    protected:
        Checkbox( const Claw::NarrowString& name, Item* parent )
            : SimpleUI::Item( name, parent )
        {}

        SimpleUI::Image* m_normalImage;
        SimpleUI::Image* m_checkedImage;
    };

    // -------------------------------- //

    class RadioButtonOption: public SimpleUI::Item
    {
    public:
        class LinkedSet: public Claw::RefCounter
        {
        public:
            const std::set< RadioButtonOption* >& GetAllOptions() const { return m_all; }
            RadioButtonOption* GetSelectedOption() const { return m_selected; }

        private:
            LinkedSet( RadioButtonOption* option );

            std::set< RadioButtonOption* > m_all;
            RadioButtonOption* m_selected;

            friend class RadioButtonOption;
        };

        RadioButtonOption( const Claw::NarrowString& name, const Claw::NarrowString& normal, const Claw::NarrowString& selected, Item* parent );

        RadioButtonOption* SetSelected();
        bool IsSelected() const { return m_linkedSet->m_selected == this; }

        RadioButtonOption* Link( RadioButtonOption* otherOption );

        virtual float GetWidth() const { return m_normalImage->GetWidth(); }
        virtual float GetHeight() const { return m_normalImage->GetHeight(); }

        SimpleUI::Image* GetNormalImage() const { return m_normalImage; }
        SimpleUI::Image* GetSelectedImage() const { return m_selectedImage; }
        const LinkedSet* GetLinkedSet() const { return m_linkedSet; }

    protected:
        RadioButtonOption( const Claw::NarrowString& name, Item* parent )
            : SimpleUI::Item( name, parent )
        {}

        SimpleUI::Image* m_normalImage;
        SimpleUI::Image* m_selectedImage;
        Claw::SmartPtr< LinkedSet > m_linkedSet;
    };

}

#endif

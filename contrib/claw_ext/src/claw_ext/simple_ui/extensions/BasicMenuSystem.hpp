#ifndef __INCLUDED_SIMPLE_UI_BASIC_MENU_SYSTEM_HPP__
#define __INCLUDED_SIMPLE_UI_BASIC_MENU_SYSTEM_HPP__

#include "claw_ext/simple_ui/SimpleUI.hpp"
#include "claw_ext/simple_ui/extensions/Animation.hpp"
#include "claw_ext/simple_ui/extensions/BasicTouch.hpp"

namespace SimpleUIExt
{

    class ScreenStack;

    // -------------------------------- //

    class Screen: public Claw::RefCounter
    {
    public:
        Screen( const Claw::NarrowString& name, float designWidth, float designHeight, float showHideAnimationTime );
        virtual ~Screen() {}

        virtual void Show( bool animate );
        virtual void Hide( bool animate );

        virtual void Reload();
        virtual void Resize();
        virtual void Update( float dt );
        virtual void Render( Claw::Surface* target );
        virtual void TouchDown( int x, int y );
        virtual void TouchUp( int x, int y );
        virtual void TouchMove( int x, int y );
        virtual void FocusChange( bool focused ) {}

        const Claw::NarrowString& GetName() { return m_name; }
        float GetDesignWidth() const { return m_designWidth; }
        float GetDesignHeight() const { return m_designHeight; }
        float GetShowHideAnimationTime() const { return m_showHideAnimationTime; }
        SimpleUI::Instance* GetUi() const { return m_ui; }
        SimpleUIExt::BasicTouch* GetTouch() const { return m_touch; }

        void SetUserData( void* userData ) { m_userData = userData; }
        void* GetUserData() const { return m_userData; }

    protected:
        Claw::NarrowString m_name;
        float m_designWidth;
        float m_designHeight;
        float m_showHideAnimationTime;
        SimpleUI::InstancePtr m_ui;
        SimpleUIExt::BasicTouchPtr m_touch;
        ScreenStack* m_stack;
        bool m_transition;
        void* m_userData;

        virtual void PrivateReload();

    private:
        static void OnShowHideAnim( SimpleUIExt::Animation< float >* animation, const float& t, void* userData ) { static_cast< Screen* >( userData )->OnShowHideAnim( t ); }
        static void OnShowAnimFinished( SimpleUIExt::Animation< float >* animation, void* userData );
        static void OnHideAnimFinished( SimpleUIExt::Animation< float >* animation, void* userData );

        static void OnTouchDown( SimpleUI::Item* item, int x, int y, void* userData ) { static_cast< Screen* >( userData )->OnTouchDown( item, x, y ); }
        static void OnTouchUp( SimpleUI::Item* item, int x, int y, void* userData ) { static_cast< Screen* >( userData )->OnTouchUp( item, x, y ); }
        static void OnTouchMove( SimpleUI::Item* item, int x, int y, void* userData ) { static_cast< Screen* >( userData )->OnTouchMove( item, x, y ); }

        void Lock();
        void Unlock();

        // ---------------- //

        virtual void OnReload() {}

        virtual void OnShowStarted() {}
        virtual void OnShowFinished() {}
        virtual void OnHideStarted() {}
        virtual void OnHideFinished() {}
        virtual void OnShowHideAnim( float t ) {}

        virtual void OnTouchDown( SimpleUI::Item* item, int x, int y ) {}
        virtual void OnTouchUp( SimpleUI::Item* item, int x, int y ) {}
        virtual void OnTouchMove( SimpleUI::Item* item, int x, int y ) {}

        friend class ScreenStack;
    };

    typedef Claw::SmartPtr< Screen > ScreenPtr;

    // -------------------------------- //

    class ScreenStack: public Claw::RefCounter
    {
    public:
        ScreenStack();

        void Add( ScreenPtr screen );
        void Init( const Claw::NarrowString& firstScreen );

        Screen* GetScreen( const Claw::NarrowString& name );
        template< class T > T* GetScreen( const Claw::NarrowString& name ) { return static_cast< T* >( GetScreen( name ) ); }
        void Push( const Claw::NarrowString& name );
        void Pop( const Claw::NarrowString& name );
        Screen* Top() const;

        void Resize();
        void Update( float dt );
        void Render( Claw::Surface* target );
        void TouchDown( int x, int y );
        void TouchUp( int x, int y );
        void TouchMove( int x, int y );
        void FocusChange( bool focused );

        void SetUserData( void* userData ) { m_userData = userData; }
        void* GetUserData() const { return m_userData; }

    private:
        std::map< Claw::NarrowString, ScreenPtr > m_screens;
        std::vector< Screen* > m_stack;
        Screen* m_push;
        Screen* m_pop;
        bool m_doPop;
        bool m_doPush;
        void* m_userData;

        void OnShowAnimFinished( Screen* screen );
        void OnHideAnimFinished( Screen* screen );

        virtual void OnPushStarted( const Claw::NarrowString& m_screenName ) {}
        virtual void OnPushFinished( const Claw::NarrowString& m_screenName ) {}
        virtual void OnPopStarted( const Claw::NarrowString& m_screenName ) {}
        virtual void OnPopFinished( const Claw::NarrowString& m_screenName ) {}

        friend class Screen;
    };

    typedef Claw::SmartPtr< ScreenStack > ScreenStackPtr;

}

#endif

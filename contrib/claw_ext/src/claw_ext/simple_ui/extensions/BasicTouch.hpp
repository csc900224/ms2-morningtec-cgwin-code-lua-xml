#ifndef __INCLUDED_SIMPLE_UI_BASIC_TOUCH_HPP__
#define __INCLUDED_SIMPLE_UI_BASIC_TOUCH_HPP__

#include "claw_ext/simple_ui/SimpleUI.hpp"

namespace SimpleUIExt
{

    class BasicTouch: public Claw::RefCounter
    {
    public:
        typedef void (*CallbackFunc)( SimpleUI::Item* item, int x, int y, void* userParam );

        // -------------------------------- //

        BasicTouch( SimpleUI::Instance* instance );

        void TouchDown( int x, int y );
        void TouchUp( int x, int y );
        void TouchMove( int x, int y );

        BasicTouch* SetOnTouchDownFunc( CallbackFunc onTouchDownFunc, void* onTouchDownFuncUserParam ) { m_onTouchDownFunc = onTouchDownFunc; m_onTouchDownFuncUserParam = onTouchDownFuncUserParam; return this; }
        BasicTouch* SetOnTouchUpFunc( CallbackFunc onTouchUpFunc, void* onTouchUpFuncUserParam ) { m_onTouchUpFunc = onTouchUpFunc; m_onTouchUpFuncUserParam = onTouchUpFuncUserParam; return this; }
        BasicTouch* SetOnTouchMoveFunc( CallbackFunc onTouchMoveFunc, void* onTouchMoveFuncUserParam ) { m_onTouchMoveFunc = onTouchMoveFunc; m_onTouchMoveFuncUserParam = onTouchMoveFuncUserParam; return this; }
        void SetReportNull( bool reportNull ) { m_reportNull = reportNull; }
        void SetEnabled( bool enabled ) { m_enabled = enabled; }

        bool IsReportNull() const { return m_reportNull; }
        bool IsEnabled() const { return m_enabled; }

    private:
        SimpleUI::Instance* m_instance;
        CallbackFunc m_onTouchDownFunc;
        void* m_onTouchDownFuncUserParam;
        CallbackFunc m_onTouchUpFunc;
        void* m_onTouchUpFuncUserParam;
        CallbackFunc m_onTouchMoveFunc;
        void* m_onTouchMoveFuncUserParam;
        bool m_reportNull;
        bool m_enabled;
        SimpleUI::Item* m_touchedDownItem;
    };

    typedef Claw::SmartPtr< BasicTouch > BasicTouchPtr;

}

#endif

#ifndef __INCLUDED_SIMPLE_UI_ANIMATION_HPP__
#define __INCLUDED_SIMPLE_UI_ANIMATION_HPP__

#include "claw_ext/simple_ui/SimpleUI.hpp"

namespace SimpleUIExt
{

    template< class T >
    class Animation: public SimpleUI::Item
    {
    public:
        typedef float (*InterpolateFunc)( float t );
        typedef void (*SetFunc)( Animation* animation, const T& value, void* setFuncUserData );
        typedef void (*OnFinishedFunc)( Animation* animation, void* onFinishedFuncUserData );

        // -------------------------------- //

        static float InterpolateFuncCubic( float t )
        {
            return t * t * ( 3 - 2 * t );
        }

        // -------------------------------- //

        Animation( const Claw::NarrowString& name, const T& from, const T& to, float time, SetFunc setFunc, void* setFuncUserData, Item* parent )
            : Item( name, parent )
            , m_time( 0 ), m_invTime( 0 ), m_t( 0 )
            , m_from( 0 ), m_to( 0 ), m_diff( 0 )
            , m_reverse( false )
            , m_interpolateFunc( InterpolateFuncCubic )
            , m_setFunc( setFunc )
            , m_setFuncUserData( setFuncUserData )
            , m_onFinishedFunc( NULL )
            , m_onFinishedFuncUserData( NULL )
        {
            SetRange( from, to );
            SetTime( time );
            SetActive( false );
        }

        void Update( float dt )
        {
            m_t += dt;

            if( m_t >= m_time )
            {
                m_setFunc( this, m_reverse ? m_from : m_to, m_setFuncUserData );

                if( m_onFinishedFunc )
                {
                    m_onFinishedFunc( this, m_onFinishedFuncUserData );
                }

                SetActive( false );
            }
            else
            {
                float t = Claw::MinMax( m_t * m_invTime, 0.0f, 1.0f );
                m_setFunc( this, static_cast< T >( m_from + m_diff * (*m_interpolateFunc)( m_reverse ? 1 - t : t ) ), m_setFuncUserData );
            }
        }

        Animation* Play( bool reverse = false, OnFinishedFunc onFinishedFunc = NULL, void* onFinishedFuncUserData = NULL )
        {
            if( !IsActive() )
            {
                m_t = 0;
                m_reverse = reverse;
                m_onFinishedFunc = onFinishedFunc;
                m_onFinishedFuncUserData = onFinishedFuncUserData;

                SetActive( true );
            }
            else if ( m_reverse != reverse )
            {
                m_t = m_time - m_t;
                m_reverse = reverse;
                m_onFinishedFunc = onFinishedFunc;
                m_onFinishedFuncUserData = onFinishedFuncUserData;
            }

            return this;
        }

        Animation* SetFrom( const T& from ) { m_from = from; m_diff = m_to - m_from; return this; }
        Animation* SetTo( const T& to ) { m_to = to; m_diff = m_to - m_from; return this; }
        Animation* SetRange( const T& from, const T& to ) { m_from = from; m_to = to; m_diff = m_to - m_from; return this; }
        Animation* SetTime( float time ) { CLAW_ASSERT( time > 0 ) m_time = time; m_invTime = 1 / time; return this; }
        Animation* SetInterpolateFunc( InterpolateFunc interpolateFunc ) { m_interpolateFunc = interpolateFunc; return this; }
        Animation* SetSetFunc( SetFunc setFunc, void* setFuncUserData ) { m_setFunc = setFunc; m_setFuncUserData = setFuncUserData; return this; }

        const T& GetFrom() const { return m_from; }
        const T& GetTo() const { return m_to; }
        float GetTime() const { return m_time; }

    private:
        float m_time, m_invTime, m_t;
        T m_from, m_to, m_diff;
        bool m_reverse;

        InterpolateFunc m_interpolateFunc;
        SetFunc m_setFunc;
        void* m_setFuncUserData;
        OnFinishedFunc m_onFinishedFunc;
        void* m_onFinishedFuncUserData;
    };

}

#endif

#include "claw_ext/simple_ui/extensions/BasicComponents.hpp"

using namespace SimpleUI;

namespace SimpleUIExt
{

    Button::Button( const Claw::NarrowString& name, const Claw::NarrowString& normal, const Claw::NarrowString& pressed, const Claw::NarrowString& disabled, Item* parent )
        : Item( name, parent )
    {
        m_type = "Button";

        m_normalImage = new Image( name + "_normal", normal, this );
        m_pressedImage = pressed.size() ? new Image( name + "_pressed", pressed, this ) : NULL;
        m_disabledImage = disabled.size() ? new Image( name + "_disabled", disabled, this ) : NULL;

        SetTouchable( true );
        SetState( STATE_NORMAL );
    }

    Button* Button::SetState( State state )
    {
        switch( state )
        {
            case STATE_NORMAL:
                m_normalImage->SetEnabled( true );
                if( m_pressedImage ) { m_pressedImage->SetEnabled( false ); }
                if( m_disabledImage ) { m_disabledImage->SetEnabled( false ); }
                break;

            case STATE_PRESSED:
                CLAW_ASSERT( m_pressedImage )
                m_normalImage->SetEnabled( false );
                m_pressedImage->SetEnabled( true );
                if( m_disabledImage ) { m_disabledImage->SetEnabled( false ); }
                break;

            case STATE_DISABLED:
                CLAW_ASSERT( m_disabledImage )
                m_normalImage->SetEnabled( false );
                if( m_pressedImage ) { m_pressedImage->SetEnabled( false ); }
                m_disabledImage->SetEnabled( true );
                break;

            default:
                CLAW_ASSERT( false )
        }

        SetTouchable( state != STATE_DISABLED );
        return this;
    }

    Button::State Button::GetState() const
    {
        if( m_disabledImage && m_disabledImage->IsEnabled() ) { return STATE_DISABLED; }
        if( m_pressedImage && m_pressedImage->IsEnabled() ) { return STATE_PRESSED; }
        return STATE_NORMAL;
    }

    // -------------------------------- //

    Checkbox::Checkbox( const Claw::NarrowString& name, const Claw::NarrowString& normal, const Claw::NarrowString& checked, Item* parent )
        : Item( name, parent )
    {
        m_type = "Checkbox";

        m_normalImage = new Image( name + "_normal", normal, this );
        m_checkedImage = new Image( name + "_checked", checked, this );

        SetTouchable( true );
        SetChecked( false );
    }

    Checkbox* Checkbox::SetChecked( bool checked )
    {
        m_normalImage->SetEnabled( !checked );
        m_checkedImage->SetEnabled( checked );
        return this;
    }

    // -------------------------------- //

    RadioButtonOption::LinkedSet::LinkedSet( RadioButtonOption* option )
        : m_selected( option )
    {
        m_all.insert( option );
    }

    RadioButtonOption::RadioButtonOption( const Claw::NarrowString& name, const Claw::NarrowString& normal, const Claw::NarrowString& selected, Item* parent )
        : Item( name, parent )
    {
        m_type = "RadioButtonOption";

        m_normalImage = new Image( name + "_normal", normal, this );
        m_selectedImage = new Image( name + "_selected", selected, this );
        m_linkedSet.Reset( new LinkedSet( this ) );

        SetTouchable( true );
        SetSelected();
    }

    RadioButtonOption* RadioButtonOption::Link( RadioButtonOption* otherOption )
    {
        if( otherOption != this )
        {
            CLAW_MSG_ASSERT( m_linkedSet->m_all.size() == 1, "The option is already linked." )
            m_linkedSet = otherOption->m_linkedSet;
            CLAW_MSG_VERIFY( m_linkedSet->m_all.insert( this ).second, "The option is already linked." )
            m_linkedSet->m_selected->SetSelected();
        }

        return this;
    }

    RadioButtonOption* RadioButtonOption::SetSelected()
    {
        for( std::set< RadioButtonOption* >::iterator it = m_linkedSet->m_all.begin(); it != m_linkedSet->m_all.end(); it++ )
        {
            (*it)->m_normalImage->SetEnabled( true );
            (*it)->m_selectedImage->SetEnabled( false );
        }

        m_normalImage->SetEnabled( false );
        m_selectedImage->SetEnabled( true );
        m_linkedSet->m_selected = this;
        return this;
    }

}

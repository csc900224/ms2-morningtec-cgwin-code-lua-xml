#include "claw_ext/simple_ui/extensions/BasicTouch.hpp"

using namespace SimpleUI;

namespace SimpleUIExt
{

    BasicTouch::BasicTouch( SimpleUI::Instance* instance )
        : m_instance( instance )
        , m_onTouchDownFunc( NULL )
        , m_onTouchDownFuncUserParam( NULL )
        , m_onTouchUpFunc( NULL )
        , m_onTouchUpFuncUserParam( NULL )
        , m_reportNull( false )
        , m_enabled( false )
        , m_touchedDownItem( NULL )
    {}

    void BasicTouch::TouchDown( int x, int y )
    {
        if( !m_touchedDownItem )
        {
            m_touchedDownItem = m_instance->Find( x, y );

            if( m_onTouchDownFunc && ( m_touchedDownItem || m_reportNull ) )
            {
                (*m_onTouchDownFunc)( m_touchedDownItem, x, y, m_onTouchDownFuncUserParam );
            }
        }
    }

    void BasicTouch::TouchUp( int x, int y )
    {
        if( m_onTouchUpFunc && ( m_touchedDownItem || m_reportNull ) )
        {
            (*m_onTouchUpFunc)( m_touchedDownItem, x, y, m_onTouchUpFuncUserParam );
            m_touchedDownItem = NULL;
        }
    }

    void BasicTouch::TouchMove( int x, int y )
    {
        if( m_onTouchMoveFunc && ( m_touchedDownItem || m_reportNull ) )
        {
            (*m_onTouchMoveFunc)( m_touchedDownItem, x, y, m_onTouchMoveFuncUserParam );
        }
    }

}

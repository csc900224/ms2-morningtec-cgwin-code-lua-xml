#include "claw_ext/simple_ui/SimpleUI.hpp"

#include "claw/application/AbstractApp.hpp"
#include "claw/base/TextDict.hpp"
#include "claw/graphics/TriangleEngine.hpp"

#include <algorithm>

namespace SimpleUI
{

    Instance::Instance( float designWidth, float designHeight, bool fitScreen )
        : m_fitScale( 1 )
        , m_extraGraphicsScale( 1 )
        , m_extraFontScale( 1 )
        , m_debugRender( false )
        , m_subpixelRender( true )
        , m_enableTouch( true )
        , m_userData( NULL )
    {
        m_rootFrame.Reset( new Frame( "rootFrame", designWidth, designHeight, NULL ) );
        AddItem( m_rootFrame );

        if( fitScreen )
        {
            FitScreen();
        }
    }

    Instance* Instance::FitScreen()
    {
        m_fitScale = Claw::AbstractApp::GetInstance()->GetDisplay()->GetHeight() / m_rootFrame->GetHeight();
        m_fitShift.m_x = ( Claw::AbstractApp::GetInstance()->GetDisplay()->GetWidth() - m_rootFrame->GetWidth() * m_fitScale ) * 0.5f;
        return this;
    }

    Item* Instance::Find( const char* name, bool assertOnNull ) const
    {
        std::map< Claw::NarrowString, ItemPtr >::const_iterator it = m_items.find( name );

        if( it == m_items.end() )
        {
            CLAW_MSG_ASSERT( !assertOnNull, "Item not found: " << name )
            return NULL;
        }

        return it->second;
    }

    Item* Instance::Find( int x, int y, bool touchableOnly ) const
    {
        for( std::vector< Item* >::const_reverse_iterator it = m_renderedItems.rbegin(); it != m_renderedItems.rend(); it++ )
        {
            const Item* i = *it;

            if( ( !touchableOnly || i->IsTouchable() ) &&
                Claw::Rectf( i->GetRenderX(), i->GetRenderY(), i->GetScaled( i->GetWidth() ), i->GetScaled( i->GetHeight() ) ).IsInRect( (float)x, (float)y ) )
            {
                return *it;
            }
        }

        return NULL;
    }

    void Instance::Update( float dt )
    {
        m_rootFrame->RootUpdate( dt );
    }

    void Instance::Render( Claw::Surface* target )
    {
        m_rootFrame->RootRender( target );
    }

    void Instance::AddItem( Item* item )
    {
        CLAW_ASSERT( !item->m_instance )
        item->m_instance = this;
        CLAW_MSG_VERIFY( m_items.insert( std::make_pair( item->GetName(), ItemPtr( item ) ) ).second, "Duplicate item name: " << item->GetName() )
    }

    // -------------------------------- //

    Item::Item( const Claw::NarrowString& name, Item* parent )
        : m_type( "Item" )
        , m_name( name )
        , m_instance( NULL )
        , m_parent( parent )
        , m_enabled( true )
        , m_active( true )
        , m_visible( true )
        , m_touchable( false )
        , m_started( false )
        , m_alignHoriz( HA_LEFT )
        , m_alignVert( VA_TOP )
    {
        CLAW_MSG_ASSERT( parent || name == "rootFrame", "There's only one parentless root item stored in Instance." )

        if( parent )
        {
            CLAW_MSG_ASSERT( !parent->m_started, "Dude, this is static and simple, want more - use guif." )
            parent->m_instance->AddItem( this );
            parent->m_children.push_back( this );
        }
    }

    void Item::RootUpdate( float dt )
    {
        if( m_enabled && m_active )
        {
            Start();
            Update( dt );

            for( std::vector< Item* >::iterator it = m_children.begin(); it != m_children.end(); it++ )
            {
                (*it)->RootUpdate( dt );
            }
        }
    }

    void Item::RootRender( Claw::Surface* target )
    {
        if( m_enabled && m_visible )
        {
            Start();
            Render( target );

            if( m_instance->GetRootFrame() == this )
            {
                m_instance->m_renderedItems.clear();
            }

            m_instance->m_renderedItems.push_back( this );

            if( m_instance->IsDebugRender() )
            {
                target->DrawRectangle( GetRenderX(), GetRenderY(), GetRenderX() + GetScaled( GetWidth() ), GetRenderY() + GetScaled( GetHeight() ), Claw::Color( 0, 0, 255 ) );
            }

            for( std::vector< Item* >::iterator it = m_children.begin(); it != m_children.end(); it++ )
            {
                (*it)->RootRender( target );
            }
        }
    }

    Item* Item::SetPos( float x, float y )
    {
        CLAW_MSG_ASSERT( GetInstance()->GetRootFrame() != this, "Don't move the root frame (fitting screen relies on it), move a child item instead." )
        m_pos = Claw::Point2f( x, y );
        m_globalPos = m_parent ? m_parent->m_globalPos + m_pos : m_pos;
        UpdateChildrenPos();
        return this;
    }

    Item* Item::SetGlobalPos( float x, float y )
    {
        CLAW_MSG_ASSERT( GetInstance()->GetRootFrame() != this, "Don't move the root frame (fitting screen relies on it), move a child item instead." )
        m_globalPos = Claw::Point2f( x, y );
        m_pos = m_parent ? m_globalPos - m_parent->m_globalPos : m_globalPos;
        UpdateChildrenPos();
        return this;
    }

    void Item::UpdateChildrenPos()
    {
        for( std::vector< Item* >::iterator it = m_children.begin(); it != m_children.end(); it++ )
        {
            (*it)->m_globalPos = m_globalPos + (*it)->m_pos;
            (*it)->UpdateChildrenPos();
        }
    }

    void Item::Start()
    {
        if( !m_started )
        {
            UpdateChildrenPos();
            m_started = true;
        }
    }

    Item* Item::Align( HorizAlign horizAlign, VertAlign vertAlign, float padX, float padY, bool screen )
    {
        Claw::Display* display = Claw::AbstractApp::GetInstance()->GetDisplay();

        float parentWidth = screen ? display->GetWidth() : m_parent ? m_parent->GetWidth() : GetInstance()->GetRootFrame()->GetWidth();
        float parentHeight = screen ? display->GetHeight() : m_parent ? m_parent->GetHeight() : GetInstance()->GetRootFrame()->GetHeight();

        CLAW_MSG_ASSERT( parentWidth && parentHeight, "Parent has 0 size." )
        CLAW_MSG_ASSERT( GetWidth() && GetHeight(), "Item has 0 size." )

        float x = 0;
        float y = 0;

        switch( horizAlign )
        {
            case HA_LEFT: break;
            case HA_CENTER: x += ( parentWidth - GetWidth() * ( screen ? GetInstance()->m_fitScale : 1 ) ) * 0.5f; break;
            case HA_RIGHT: x += parentWidth - GetWidth() * ( screen ? GetInstance()->m_fitScale : 1 ); break;

            default:
                CLAW_ASSERT( false )
        }

        switch( vertAlign )
        {
            case VA_TOP: break;
            case VA_CENTER: y += ( parentHeight - GetHeight() * ( screen ? GetInstance()->m_fitScale : 1 ) ) * 0.5f; break;
            case VA_BOTTOM: y += parentHeight - GetHeight() * ( screen ? GetInstance()->m_fitScale : 1 ); break;

            default:
                CLAW_ASSERT( false )
        }

        if( screen )
        {
            SetGlobalPos( GetGlobalPos() + ( Claw::Point2f( x, y ) - Claw::Point2f( GetRenderX(), GetRenderY() ) ) / GetInstance()->m_fitScale + Claw::Point2f( padX, padY ) );
        }
        else
        {
            SetPos( x + padX, y + padY );
        }

        m_alignHoriz = horizAlign;
        m_alignVert = vertAlign;

        return this;
    }

    // -------------------------------- //

    void Image::Render( Claw::Surface* target )
    {
        CLAW_ASSERT( m_surface )
        m_surface->SetAlpha( GetAlpha() );

        float xShift = 0.0f;
        float yShift = 0.0f;
        float angle = m_angle;

        if( m_flip == FLIP_BOTH )
        {
            xShift = (float)m_surface->GetWidth() - m_pivot.GetX();
            yShift = (float)m_surface->GetHeight() - m_pivot.GetY();
            angle = m_angle + M_PI;
        }

        Claw::TriangleEngine::FlipMode flipMode = m_flip == FLIP_HORIZONTAL ? Claw::TriangleEngine::FM_HORIZONTAL : m_flip == FLIP_VERTICAL ? Claw::TriangleEngine::FM_VERTICAL : Claw::TriangleEngine::FM_NONE;

        if( m_color == Claw::Color( 255, 255, 255 ) )
        {
            Claw::TriangleEngine::Blit( target, m_surface, GetRenderX() + xShift, GetRenderY() + yShift, angle, GetInstance()->GetFitScale() * m_scale * GetInstance()->GetExtraGraphicsScale(), m_pivot, flipMode, m_surface->GetClipRect() );
        }
        else
        {
            Claw::TriangleEngine::BlitAlpha( target, m_surface, GetRenderX() + xShift, GetRenderY() + yShift, angle, GetInstance()->GetFitScale() * m_scale * GetInstance()->GetExtraGraphicsScale(), m_pivot, flipMode, m_surface->GetClipRect(), m_color );
        }
    }

    // -------------------------------- //

    void AnimatedImage::Update( float dt )
    {
        CLAW_ASSERT( m_surface )
        GetSurface< Claw::AnimatedSurface >()->Update( dt );
    }

    // -------------------------------- //

    Text::Text( const Claw::NarrowString& name, const Claw::String& text, bool translate, const Claw::NarrowString& fontFile, Item* parent, Claw::Extent* extent /*= NULL*/, float scale /*= 1.0f*/ )
        : Item( name, parent )
        , m_rawText( text )
        , m_translate( translate )
        , m_font( Claw::AssetDict::Get< Claw::FontEx >( fontFile ) )
        , m_scale( scale )
        , m_clipRequired( false )
        , m_scrollY( 0 )
    {
        if( extent != NULL ) m_extent = *extent;
        m_type = "Text";
        UpdateText();
    }

    void Text::UpdateText()
    {
        m_text = m_translate ? Claw::TextDict::Get()->GetText( Claw::NarrowString( m_rawText ) ) : m_rawText;
        if( m_extent.m_height != 0 && m_extent.m_width != 0 )
        {
            Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
            fontSet->AddFont( "default", m_font );
            fontSet->SetDefaultFont( "default" );

            Claw::Text::Format format;
            format.SetHorizontalAlign( GetHorizontalAlign() == HA_CENTER ? Claw::Text::Format::HA_CENTER : GetHorizontalAlign() == HA_LEFT ? Claw::Text::Format::HA_FLUSH_LEFT : Claw::Text::Format::HA_FLUSH_RIGHT );
            format.SetVerticalAlign( GetVerticalAlign() == VA_CENTER ? Claw::Text::Format::VA_CENTER : GetVerticalAlign() == VA_TOP ? Claw::Text::Format::VA_TOP : Claw::Text::Format::VA_BOTTOM );
            format.SetFontSet( fontSet );

            Claw::Extent screenTextExtent;
            screenTextExtent.m_width = (int)(m_extent.m_width * GetInstance()->GetFitScale() * m_scale);
            screenTextExtent.m_height = 0;
            m_screenText.Reset( new Claw::ScreenText( format, m_text, screenTextExtent ) );

            m_clipRequired = (m_screenText->GetExtent().m_height > GetHeight());
        }
        else
        {
            m_screenText.Reset( new Claw::ScreenText( m_font, m_text ) );
        }
    }

    void Text::Render( Claw::Surface* target )
    {
        if (m_clipRequired)
        {
            const Claw::Rect prevClip(target->GetClipRect());
            const Claw::Rect clip( static_cast<int>( GetRenderX() ), static_cast<int>( GetRenderY() ),
                static_cast<int>( GetWidth() * GetInstance()->GetFitScale() ), static_cast<int>( GetHeight() * GetInstance()->GetFitScale() ) );
            target->SetClipRect(clip);
            m_screenText->Draw( target, Claw::Round( GetRenderX() ), Claw::Round( GetRenderY() + m_scrollY ), 0, GetInstance()->GetFitScale() * m_scale * GetInstance()->GetExtraFontScale(), Claw::Point2f( 0, 0 ) );
            target->SetClipRect(prevClip);
        }
        else
        {
            m_screenText->Draw( target, Claw::Round( GetRenderX() ), Claw::Round( GetRenderY() + m_scrollY ), 0, GetInstance()->GetFitScale() * m_scale * GetInstance()->GetExtraFontScale(), Claw::Point2f( 0, 0 ) );
        }
    }

    Item* Text::Align( HorizAlign horizAlign, VertAlign vertAlign, float padX, float padY, bool screen )
    {
        bool updateText = horizAlign != GetHorizontalAlign() || vertAlign != GetVerticalAlign();
        Item* ret = Item::Align( horizAlign, vertAlign, padX, padY, screen );
        if( updateText )
        {
            UpdateText();
        }
        return ret;
    }

    void Text::SetScrollPosition(float y)
    {
        m_scrollY = std::min(0.0f, std::max(GetHeight() * GetInstance()->GetFitScale() - m_screenText->GetExtent().m_height, y) );
    }

    // -------------------------------- //

    void Rectangle::Render( Claw::Surface* target )
    {
        if( m_filled )
        {
            target->DrawFilledRectangle( GetRenderX(), GetRenderY(), GetRenderX() + GetScaled( m_width ), GetRenderY() + GetScaled( m_height ), m_color );
        }
        else
        {
            target->DrawRectangle( GetRenderX(), GetRenderY(), GetRenderX() + GetScaled( m_width ), GetRenderY() + GetScaled( m_height ), m_color );
        }
    }

    // -------------------------------- //

    void Clipper::Render( Claw::Surface* target )
    {
        if( m_width == 0 && m_height == 0 )
        {
            target->ResetClipRect();
        }
        else
        {
            target->SetClipRect( Claw::Rect( (int)Claw::Round( GetRenderX() ), (int)Claw::Round( GetRenderY() ), (int)Claw::Round( GetScaled( m_width ) ), (int)Claw::Round( GetScaled( m_height ) ) ) );
        }
    }

}

#ifndef __INCLUDED_SIMPLE_UI_SIMPLE_UIHPP__
#define __INCLUDED_SIMPLE_UI_SIMPLE_UIHPP__

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/AnimatedSurface.hpp"
#include "claw/graphics/ScreenText.hpp"

#include <map>

namespace SimpleUI
{

    class Item;
    class Frame;

    // -------------------------------- //

    class Instance: public Claw::RefCounter
    {
    public:
        Instance( float designWidth, float designHeight, bool fitScreen = true );

        Item* Find( const char* name, bool assertOnNull = true ) const;
        Item* Find( int x, int y, bool touchableOnly = true ) const;
        template< class T > T* Find( const char* name, bool assertOnNull = true ) const { return static_cast< T* >( Find( name, assertOnNull ) ); }
        template< class T > T* Find( int x, int y, bool touchableOnly = true ) const { return static_cast< T* >( Find( x, y, touchableOnly ) ); }

        void Update( float dt );
        void Render( Claw::Surface* target );

        // support for fitting screen size
        Instance* FitScreen();
        Instance* SetFitScale( float fitScale ) { m_fitScale = fitScale; return this; }
        Instance* SetFitShift( float x, float y ) { m_fitShift.m_x = x; m_fitShift.m_y = y; return this; }

        // support for mismatched asset size
        Instance* SetExtraGraphicsScale( float extraGraphicsScale ) { m_extraGraphicsScale = extraGraphicsScale; return this; }
        Instance* SetExtraFontScale( float extraFontScale ) { m_extraFontScale = extraFontScale; return this; }

        Instance* SetDebugRender( bool debugRender ) { m_debugRender = debugRender; return this; }
        Instance* SetSubpixelRender( bool subpixelRender ) { m_subpixelRender = subpixelRender; return this; }
        Instance* SetUserData( void* userData ) { m_userData = userData; return this; }

        Frame* GetRootFrame() const { return m_rootFrame; }
        const std::map< Claw::NarrowString, Claw::SmartPtr< Item > >& GetItems() const { return m_items; }
        const std::vector< Item* >& GetRenderedItems() const { return m_renderedItems; }
        float GetFitScale() const { return m_fitScale; }
        const Claw::Point2f& GetFitShift() const { return m_fitShift; }
        float GetExtraGraphicsScale() const { return m_extraGraphicsScale; }
        float GetExtraFontScale() const { return m_extraFontScale; }
        bool IsDebugRender() const { return m_debugRender; }
        bool IsSubpixelRender() const { return m_subpixelRender; }
        void* GetUserData() const { return m_userData; }

    private:
        Claw::SmartPtr< Frame > m_rootFrame;
        std::map< Claw::NarrowString, Claw::SmartPtr< Item > > m_items;
        std::vector< Item* > m_renderedItems;
        float m_fitScale;
        Claw::Point2f m_fitShift;
        float m_extraGraphicsScale;
        float m_extraFontScale;
        bool m_debugRender;
        bool m_subpixelRender;
        bool m_enableTouch;
        void* m_userData;

        void AddItem( Item* item );

        friend class Item;
    };

    typedef Claw::SmartPtr< Instance > InstancePtr;

    // -------------------------------- //

    enum HorizAlign
    {
        HA_LEFT,
        HA_CENTER,
        HA_RIGHT,
    };

    enum VertAlign
    {
        VA_TOP,
        VA_CENTER,
        VA_BOTTOM,
    };

    // -------------------------------- //

    class Item: public Claw::RefCounter
    {
    public:
        Item( const Claw::NarrowString& name, Item* parent );

        Item* SetPos( float x, float y );
        Item* SetPos( const Claw::Point2f& pos ) { return SetPos( pos.m_x, pos.m_y ); }
        Item* SetGlobalPos( float x, float y );
        Item* SetGlobalPos( const Claw::Point2f& globalPos ) { return SetGlobalPos( globalPos.m_x, globalPos.m_y ); }
        Item* SetEnabled( bool enabled ) { m_enabled = enabled; return this; }
        Item* SetActive( bool active ) { m_active = active; return this; }
        Item* SetVisible( bool visible ) { m_visible = visible; return this; }
        Item* SetTouchable( bool touchable ) { m_touchable = touchable; return this; }

        const Claw::Point2f& GetPos() const { return m_pos; }
        const Claw::Point2f& GetGlobalPos() const { return m_globalPos; }
        bool IsEnabled() const { return m_enabled; }
        bool IsActive() const { return m_active; }
        bool IsVisible() const { return m_visible; }
        bool IsTouchable() const { return m_touchable; }
        const Claw::NarrowString& GetType() const { return m_type; }
        const Claw::NarrowString& GetName() const { return m_name; }
        Item* GetParent() const { return m_parent; }
        Instance* GetInstance() const { return m_instance; }
        const std::vector< Item* >& GetChildren() const { return m_children; }
        Claw::Point2f GetSize() const { return Claw::Point2f( GetWidth(), GetHeight() ); }

        virtual float GetWidth() const { return 0; }
        virtual float GetHeight() const { return 0; }

        HorizAlign GetHorizontalAlign() const { return m_alignHoriz; }
        VertAlign GetVerticalAlign() const { return m_alignVert; }

        Claw::Vector2f GetDiagonal() const { return Claw::Vector2f( GetWidth(), GetHeight() ); }
        Claw::Point2f GetCenter() const { return GetPos() + GetDiagonal() * 0.5f; }
        Claw::Point2f GetGlobalCenter() const { return GetGlobalPos() + GetDiagonal() * 0.5f; }

        virtual Item* Align( HorizAlign horizAlign, VertAlign vertAlign, float padX, float padY, bool screen = false );
        Item* Align( HorizAlign horizAlign, VertAlign vertAlign, const Claw::Point2f& padding, bool screen = false ) { return Align( horizAlign, vertAlign, padding.m_x, padding.m_y, screen ); }

    protected:
        Claw::NarrowString m_type;

        float GetRenderX() const { float x = m_instance->m_fitShift.m_x + GetGlobalPos().m_x * m_instance->m_fitScale; return m_instance->IsSubpixelRender() ? x : Claw::Round( x ); }
        float GetRenderY() const { float y = m_instance->m_fitShift.m_y + GetGlobalPos().m_y * m_instance->m_fitScale; return m_instance->IsSubpixelRender() ? y : Claw::Round( y ); }
        float GetScaled( float size ) const { return size * m_instance->m_fitScale; }

    private:
        Claw::NarrowString m_name;
        Instance* m_instance;
        Item* m_parent;
        std::vector< Item* > m_children;

        Claw::Point2f m_pos;
        Claw::Point2f m_globalPos;

        HorizAlign m_alignHoriz;
        VertAlign m_alignVert;

        bool m_enabled;
        bool m_active;
        bool m_visible;
        bool m_touchable;
        bool m_started;

        void UpdateChildrenPos();
        void Start();
        void RootUpdate( float dt );
        void RootRender( Claw::Surface* target );

        virtual void Update( float dt ) {}
        virtual void Render( Claw::Surface* target ) {}

        friend class Instance;
    };

    typedef Claw::SmartPtr< Item > ItemPtr;

    // -------------------------------- //

    class Image: public Item
    {
    public:
        enum Flip
        {
            FLIP_NONE,
            FLIP_HORIZONTAL,
            FLIP_VERTICAL,
            FLIP_BOTH
        };

        Image( const Claw::NarrowString& name, Claw::Surface* surface, Item* parent )
            : Item( name, parent )
            , m_surface( surface )
            , m_color( 255, 255, 255, 255 )
            , m_scale( 1 )
            , m_flip( FLIP_NONE )
            , m_angle( 0 )
        {
            m_type = "Image";
            CLAW_ASSERT( m_surface )
        }

        Image( const Claw::NarrowString& name, const Claw::NarrowString& fileName, Item* parent )
            : Item( name, parent )
            , m_surface( Claw::AssetDict::Get< Claw::Surface >( fileName ) )
            , m_color( 255, 255, 255, 255 )
            , m_scale( 1 )
            , m_flip( FLIP_NONE )
        {
            m_type = "Image";
            CLAW_ASSERT( m_surface )
        }

        Image* SetSurface( Claw::Surface* surface ) { m_surface.Reset( surface ); CLAW_ASSERT( m_surface ) return this; }
        Image* SetSurface( const Claw::NarrowString& fileName ) { m_surface = Claw::AssetDict::Get< Claw::Surface >( fileName ); CLAW_ASSERT( m_surface ) return this; }
        Image* SetColor( const Claw::Color& color ) { m_color = color; return this; }
        Image* SetAlpha( int alpha ) { m_color.SetA( alpha ); return this; }
        Image* SetScale( float scale ) { CLAW_ASSERT( scale >= 0 ) CLAW_MSG_ASSERT( GetPos() == Claw::Point2f( 0, 0 ), "Set scale first." ) m_scale = scale; return this; }
        Image* SetFlip( Flip flip ) { m_flip = flip; return this; }
        Image* SetAngle( float angle ) { m_angle = angle; return this; }
        Image* SetPivot( const Claw::Point2f& pivot ) { m_pivot = pivot; return this; }


        Claw::Surface* GetSurface() const { return m_surface; }
        template< class T > T* GetSurface() const { return static_cast< T* >( GetSurface() ); }
        const Claw::Color& GetColor() const { return m_color; }
        int GetAlpha() const { return m_color.GetA(); }
        float GetScale() const { return m_scale; }
        float GetAngle() const { return m_angle; }
        const Claw::Point2f& GetPivot() const { return m_pivot; }

        virtual float GetWidth() const { CLAW_ASSERT( m_surface ) return m_surface->GetWidth() * m_scale * GetInstance()->GetExtraGraphicsScale(); }
        virtual float GetHeight() const { CLAW_ASSERT( m_surface ) return m_surface->GetHeight() * m_scale * GetInstance()->GetExtraGraphicsScale(); }

    protected:
        Claw::SurfacePtr m_surface;
        Claw::Color m_color;
        float m_scale;
        float m_angle;
        Claw::Point2f m_pivot;
        Flip m_flip;

        virtual void Render( Claw::Surface* target );
    };

    // -------------------------------- //

    class AnimatedImage: public Image
    {
    public:
        AnimatedImage( const Claw::NarrowString& name, Claw::AnimatedSurface* surface, Item* parent )
            : Image( name, surface, parent )
            , m_surfaceHandle( m_surface )
        {
            m_type = "AnimatedImage";
            m_surface.Reset( new Claw::AnimatedSurface( *(Claw::AnimatedSurface*)m_surface.GetPtr() ) );
        }

        AnimatedImage( const Claw::NarrowString& name, const Claw::NarrowString& fileName, Item* parent )
            : Image( name, fileName, parent )
            , m_surfaceHandle( m_surface )
        {
            m_type = "AnimatedImage";
            m_surface.Reset( new Claw::AnimatedSurface( *(Claw::AnimatedSurface*)m_surface.GetPtr() ) );
        }

    protected:
        virtual void Update( float dt );

    private:
        Claw::SurfacePtr m_surfaceHandle;
    };

    // -------------------------------- //

    class Text: public Item
    {
    public:
        Text( const Claw::NarrowString& name, const Claw::String& text, bool translate, const Claw::NarrowString& fontFile, Item* parent, Claw::Extent* extent = NULL, float scale = 1.0f );

        Text* SetText( const Claw::String& text ) { m_rawText = text; UpdateText(); return this; }
        Text* SetTranslate( bool translate ) { m_translate = translate; UpdateText(); return this; }
        Text* SetFont( const Claw::NarrowString& fontFile ) { m_font = Claw::AssetDict::Get< Claw::FontEx >( fontFile ); UpdateText(); return this; }
        Text* SetScale( float scale ) { CLAW_ASSERT( scale >= 0 ) CLAW_MSG_ASSERT( GetPos() == Claw::Point2f( 0, 0 ), "Set scale first." ) m_scale = scale; UpdateText(); return this; }
        Text* SetExtent( const Claw::Extent& extent ) { m_extent = extent; UpdateText(); return this; }

        const Claw::String& GetText() const { return m_text; }
        bool IsTranslate() const { return m_translate; }
        Claw::FontEx* GetFont() const { return m_font; }
        float GetScale() const { return m_scale; }

        virtual Item* Align( HorizAlign horizAlign, VertAlign vertAlign, float padX, float padY, bool screen = false );
        virtual float GetWidth() const { return (float)m_screenText->GetWidth() * m_scale * GetInstance()->GetExtraFontScale(); }
        virtual float GetHeight() const { return (m_extent.m_height > 0 ? (float)m_extent.m_height : (float)m_screenText->GetHeight() * GetInstance()->GetExtraFontScale()) * m_scale; }

        void SetScrollPosition(float y);
        void ScrollBy(float dy) { SetScrollPosition(m_scrollY + dy); }
        float GetScroll() const { return m_scrollY; }

    private:
        Claw::String m_rawText;
        Claw::String m_text;
        bool m_translate;
        float m_scale;

        Claw::FontExPtr m_font;
        Claw::Extent m_extent;
        Claw::ScreenTextPtr m_screenText;

        float m_scrollY;
        bool m_clipRequired;

        virtual void Render( Claw::Surface* target );

        void UpdateText();
    };

    // -------------------------------- //

    class Rectangle: public Item
    {
    public:
        Rectangle( const Claw::NarrowString& name, float width, float height, bool filled, const Claw::Color& color, Item* parent )
            : Item( name, parent )
            , m_width( width )
            , m_height( height )
            , m_filled( filled )
            , m_color( color )
        {
            m_type = "Rectangle";
        }

        Rectangle* SetWidth( float width ) { m_width = width; return this; }
        Rectangle* SetHeight( float height ) { m_height = height; return this; }
        Rectangle* SetFilled( bool filled ) { m_filled = filled; return this; }
        Rectangle* SetColor( const Claw::Color& color ) { m_color = color; return this; }
        Rectangle* SetAlpha( int alpha ) { m_color.SetA( alpha ); return this; }

        virtual float GetWidth() const { return m_width; }
        virtual float GetHeight() const { return m_height; }
        bool IsFilled() const { return m_filled; }
        const Claw::Color& GetColor() const { return m_color; }
        int GetAlpha() const { return m_color.GetA(); }

    private:
        float m_width;
        float m_height;
        bool m_filled;
        Claw::Color m_color;

        virtual void Render( Claw::Surface* target );
    };

    // -------------------------------- //

    class Frame: public Item
    {
    public:
        Frame( const Claw::NarrowString& name, float width, float height, Item* parent )
            : Item( name, parent )
            , m_width( width )
            , m_height( height )
        {
            m_type = "Frame";
        }

        Frame* SetWidth( float width ) { m_width = width; return this; }
        Frame* SetHeight( float height ) { m_height = height; return this; }

        virtual float GetWidth() const { return m_width; }
        virtual float GetHeight() const { return m_height; }

    private:
        float m_width;
        float m_height;
    };

    // -------------------------------- //

    class Clipper: public Item
    {
    public:
        Clipper( const Claw::NarrowString& name, float width, float height, Item* parent )
            : Item( name, parent )
            , m_width( width )
            , m_height( height )
        {
            m_type = "Clipper";
        }

        Clipper* SetWidth( float width ) { m_width = width; return this; }
        Clipper* SetHeight( float height ) { m_height = height; return this; }

        virtual float GetWidth() const { return m_width; }
        virtual float GetHeight() const { return m_height; }

    private:
        float m_width;
        float m_height;

        virtual void Render( Claw::Surface* target );
    };

}

#endif

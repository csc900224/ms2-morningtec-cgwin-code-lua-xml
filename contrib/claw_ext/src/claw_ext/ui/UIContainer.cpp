//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/network/easy_share/UIComponent.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIContainer.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Xml.hpp"

// Just for tests
#include "claw/graphics/Surface.hpp"

#define _DEBUG_UI_CONTAINER     0   // Set to 1 if you want to see container boundaries
#define _DEBUG_UI_COMPONENTS    0   // Set to 1 to see sub-components boundaries

namespace ClawExt
{
    UIContainer::~UIContainer()
    {
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            delete (*it);
        }
    }

    void UIContainer::Load( const Claw::XmlIt* xmlConfig )
    {
    }

    void UIContainer::Render( Claw::Surface* target )
    {
        if( IsVisible() )
        {
            if( m_renderOrder == RO_FORWARD )
            {
                SubComponentsIt it = m_children.begin();
                SubComponentsIt end = m_children.end();
                for( ; it != end; ++it )
                {
                    if( (*it)->IsVisible() )
                    {
                        (*it)->Render( target );
#if _DEBUG_UI_COMPONENTS
                        const Pos& pos = (*it)->GetViewPos();
                        const Size& size = (*it)->GetSize();
                        target->DrawRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x, pos.m_y + size.m_y, Claw::Color( 255, 0, 0 ) );
#endif
                    }
                }
            }
            else
            {
                SubComponents::reverse_iterator it = m_children.rbegin();
                SubComponents::reverse_iterator end = m_children.rend();
                for( ; it != end; ++it )
                {
                    if( (*it)->IsVisible() )
                    {
                        (*it)->Render( target );
#if _DEBUG_UI_COMPONENTS
                        const Pos& pos = (*it)->GetViewPos();
                        const Size& size = (*it)->GetSize();
                        target->DrawRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x, pos.m_y + size.m_y, Claw::Color( 255, 0, 0 ) );
#endif
                    }
                }
            }
#if _DEBUG_UI_CONTAINER
        // Just for debug
        const Pos& pos = GetViewPos();
        const Size& size = GetSize();

        target->DrawRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x, pos.m_y + size.m_y, Claw::Color( 0, 255, 0 ) );
#endif
        }
    }

    void UIContainer::SetActive( bool active )
    {
        UIComponent::SetActive( active );

        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->SetActive( active );
        }
    }

    void UIContainer::Update( float dt )
    {
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->Update( dt );
        }
    }

    void UIContainer::AddComponent( UIComponent* component )
    {
        // Register as an component parent - may be only one
        component->SetParent( this );

        // Calculate added component layout
        component->CalculateRelativeLayout( this );

        m_children.push_back( component );

        Invalidate();
    }

    void UIContainer::ClearComponents( bool release )
    {
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            if( release )
                delete (*it);
            else
                (*it)->SetParent( NULL );
        }
        m_children.clear();
        Invalidate();
    }

    const UIContainer::SubComponents& UIContainer::GetComponents() const
    {
        return m_children;
    }

    UIContainer::UIContainer()
        : UIComponent()
        , m_renderOrder( RO_FORWARD )
    {
    }

    UIContainer::UIContainer( const Pos& pos )
        : UIComponent( pos )
        , m_renderOrder( RO_FORWARD )
    {
    }

    UIContainer::UIContainer( const Pos& pos, const Size& size )
        : UIComponent( pos, size )
        , m_renderOrder( RO_FORWARD )
    {
    }

    void UIContainer::SetSize( const Size& size )
    {
        UIComponent::SetSize( size );

        // Instead of calling it only for the childs notify down tree
        //CalculateRelativeLayout( this );
        NotifyDownTree( CN_SIZE_CHANGED );
    }

    void UIContainer::SetLocalPos( const Pos& pos )
    {
        UIComponent::SetLocalPos( pos );

        // Update all child components parent position
        NotifyDownTree( CN_POS_CHANGED );
    }

    void UIContainer::Invalidate()
    {
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->Invalidate();
        }
    }

    void UIContainer::CalculateRelativeLayout( const UIComponent* relativeTo )
    {
        UIComponent::CalculateRelativeLayout( relativeTo );

        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->CalculateRelativeLayout( this );
        }
    }

    void UIContainer::OnCursorMove( const Pos& cursorPos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );

        UIComponent::OnCursorMove( cursorPos, idx );

        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->OnCursorMove( cursorPos, idx );
        }
    }

    void UIContainer::OnCursorPress( const Pos& cursorPos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );

        UIComponent::OnCursorPress( cursorPos, idx );

        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->OnCursorPress( cursorPos, idx );
        }
    }

    void UIContainer::OnCursorRelease( const Pos& cursorPos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );

        UIComponent::OnCursorRelease( cursorPos, idx );

        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->OnCursorRelease( cursorPos, idx );
        }
    }

    void UIContainer::NotifyDownTree( ComponentNotification cn )
    {
        // Notify all children about this event, pass it down tree
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();
        for( ; it != end; ++it )
        {
            (*it)->ParentNotification( cn, this );
        }
    }

    void UIContainer::ParentNotification( ComponentNotification cn, UIContainer* cont )
    {
        UIComponent::ParentNotification( cn, cont );

        // Notify all childrens after updating myself
        NotifyDownTree( cn );
    }

    void UIContainer::ChildNotification( ComponentNotification cn, UIComponent* cont )
    {
        // TODO: Implement me
        if( cn == CN_SIZE_CHANGED )
        {
        }
        else if( cn == CN_POS_CHANGED )
        {
            CLAW_ASSERT( true );
        }

        // Notify my parent and all its parents up in hierarchy
        NotifyUpTree( cn );
    }

} // namespace ClawExt
// EOF

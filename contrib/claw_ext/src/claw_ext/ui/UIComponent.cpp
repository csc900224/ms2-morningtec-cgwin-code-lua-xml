//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIComponent.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"
#include "claw_ext/ui/UIContainer.hpp"
#include "claw_ext/ui/UIFunctor.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Xml.hpp"
#include "claw/graphics/Rect.hpp"
#include "claw/application/Application.hpp"

namespace ClawExt
{
    UIComponent::~UIComponent()
    {
        //for( int ef = 0; ef < CE_NUM; ++ef )
        {
            // Fuction pointers are auto release via SmartPointer implementation
            // delete m_eventFunc[ef];
        }
    }

    void UIComponent::Update( float dt )
    {
    }

    void UIComponent::SetSize( const Size& size )
    {
        m_size = size;

        NotifyUpTree( CN_SIZE_CHANGED );
    }

    void UIComponent::SetAlign( const Align& align, bool keepPos /*= false */ )
    {
        SetAlignHoriz( align.m_horiz, keepPos );
        SetAlignVert( align.m_vert, keepPos );
    }

    void UIComponent::SetAlignHoriz( AlignHoriz align, bool keepPos /*= false */ )
    {
        m_align.m_horiz = align;

        // Reset local positioning if required
        if( !keepPos )
        {
            // Check if corresponding alignment is set, if so reset position
            if( m_align.m_horiz != AH_NONE )    m_posLocal.m_x = 0;
        }
    }

    void UIComponent::SetAlignVert( AlignVert align, bool keepPos /*= false */ )
    {
        m_align.m_vert = align;

        // Reset positioning if required
        if( !keepPos )
        {
            if( m_align.m_vert != AV_NONE )    m_posLocal.m_y = 0;
        }
    }

    void UIComponent::SetParent( UIContainer* parent )
    {
        m_parent = parent;
        if( parent )
            m_posParent = parent->GetViewPos();
        else
            m_posParent.Zero();
    }

    void UIComponent::SetEventFunc( CursorEvent ev, UIFunctorPtr fnc )
    {
        // Release is done automatically via SmartPointer
        //delete m_eventFunc[ev];
        m_eventFunc[ev].Reset( fnc );
    }

    void UIComponent::OnCursorMove( const Pos& newPos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );
        if( !m_active ) return;

        const Pos& viewPos = GetViewPos();
        Claw::Rect bounds( viewPos.m_x, viewPos.m_y, GetSize().m_x, GetSize().m_y );
        bool inBounds = bounds.IsInRect( newPos.m_x, newPos.m_y );
        int newCursorPos = inBounds ? CS_INSIDE : CS_OUTSIDE;
        // Cursor has entered or released component area (relative position has changed)
        if( newCursorPos != (m_cursorState[idx] & CS_POS_MASK) )
        {
            if( inBounds )
            {
                m_cursorState[idx] |= CS_INSIDE;

                if( CS_PRESSED == (m_cursorState[idx] & CS_PRESS_MASK) )
                    ProcessEvent( CE_DRAG_IN, idx, newPos );
                else
                    ProcessEvent( CE_ROLL_IN, idx, newPos );
            }
            else
            {
                m_cursorState[idx] &= ~CS_POS_MASK;

                if( CS_PRESSED == (m_cursorState[idx] & CS_PRESS_MASK) )
                    ProcessEvent( CE_DRAG_OUT, idx, newPos );
                else
                    ProcessEvent( CE_ROLL_OUT, idx, newPos );
            }
        }
        // Cursor relative position has not changed
        else
        {
            if( inBounds )
            {
                if( CS_PRESSED == (m_cursorState[idx] & CS_PRESS_MASK) )
                    ProcessEvent( CE_DRAG_OVER, idx, newPos );
                else
                    ProcessEvent( CE_ROLL_OVER, idx, newPos );
            }
            else
            {
                if( CS_PRESSED == (m_cursorState[idx] & CS_PRESS_MASK) )
                    ProcessEvent( CE_DRAG_OUTSIDE, idx, newPos );
                else
                    ProcessEvent( CE_MOVE_OUTSIDE, idx, newPos );
            }
        }
    }

    void UIComponent::OnCursorPress( const Pos& pos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );
        if( !m_active ) return;

        const Pos& viewPos = GetViewPos();
        Claw::Rect bounds( viewPos.m_x, viewPos.m_y, GetSize().m_x, GetSize().m_y );
        bool inBounds = bounds.IsInRect( pos.m_x, pos.m_y );
        m_cursorState[idx] |= CS_PRESSED;
        if( inBounds )
        {
            m_cursorState[idx] |= CS_INSIDE;
            m_cursorState[idx] |= CS_PRESS_POS_IN;

            ProcessEvent( CE_PRESS, idx, pos );
        }
        else
        {
            m_cursorState[idx] &= ~CS_POS_MASK;
            m_cursorState[idx] &= ~CS_PRESS_POS_MASK;

            ProcessEvent( CE_PRESS_OUTSIDE, idx, pos );
        }
    }

    void UIComponent::OnCursorRelease( const Pos& pos, CursorIdx idx )
    {
        CLAW_ASSERT( idx < CURSORS_NUM );
        if( !m_active ) return;

        const Pos& viewPos = GetViewPos();
        Claw::Rect bounds( viewPos.m_x, viewPos.m_y, GetSize().m_x, GetSize().m_y );
        bool inBounds = bounds.IsInRect( pos.m_x, pos.m_y );
        if( ( m_cursorState[idx] & CS_PRESS_MASK ) == CS_PRESSED )
        {
            bool pressedInside = ( m_cursorState[idx] & CS_PRESS_POS_MASK ) == CS_PRESS_POS_IN;

            m_cursorState[idx] &= ~CS_PRESS_MASK;
            m_cursorState[idx] &= ~CS_PRESS_POS_MASK;

            // Setting position mask may be neccessary if the application goes out of focus after OnCursorPress
            // and OnCursorMove hanlder might not update relative position state during out of focus cursor move,
            // otherwise this setup would be not neccessary.
            if( inBounds )
            {
                m_cursorState[idx] |= CS_INSIDE;

                if( pressedInside )
                    ProcessEvent( CE_RELEASE, idx, pos );
                else
                    ProcessEvent( CE_RELEASE_DRAG_IN, idx, pos );
            }
            else
            {
                m_cursorState[idx] &= ~CS_POS_MASK;

                if( pressedInside )
                    ProcessEvent( CE_RELEASE_DRAG_OUT, idx, pos );
                else
                    ProcessEvent( CE_RELEASE_OUTSIDE, idx, pos );
            }
        }
    }

    UIComponent::UIComponent()
        : m_posLocal( 0, 0 )
        , m_posParent( 0, 0 )
        , m_posParentOff( 0, 0 )
        , m_size( 0, 0 )
        , m_visible( true )
        , m_active( true )
        , m_relativeLayoutFlags( RL_NONE )
        , m_parent( NULL )
        , m_alpha( 1.0f )
    {
        for( int ci = 0; ci < CURSORS_NUM; ++ci )
        {
            m_cursorState[ci] = ( CS_OUTSIDE | CS_RELEASED );
        }
        for( int ef = 0; ef < CE_NUM; ++ef )
        {
            m_eventFunc[ef].Reset( NULL );
        }
    }

    UIComponent::UIComponent( const Pos& pos )
        : m_posLocal( pos )
        , m_posParent( 0, 0 )
        , m_posParentOff( 0, 0 )
        , m_size( 0, 0 )
        , m_visible( true )
        , m_active( true )
        , m_relativeLayoutFlags( RL_NONE )
        , m_parent( NULL )
        , m_alpha( 1.0f )
    {
        for( int ci = 0; ci < CURSORS_NUM; ++ci )
        {
            m_cursorState[ci] = ( CS_OUTSIDE | CS_RELEASED );
        }
        for( int ef = 0; ef < CE_NUM; ++ef )
        {
            m_eventFunc[ef].Reset( NULL );
        }
    }

    UIComponent::UIComponent( const Pos& pos, const Size& size )
        : m_posLocal( pos )
        , m_posParent( 0, 0 )
        , m_posParentOff( 0, 0 )
        , m_size( size )
        , m_visible( true )
        , m_active( true )
        , m_relativeLayoutFlags( RL_NONE )
        , m_parent( NULL )
        , m_alpha( 1.0f )
    {
        for( int ci = 0; ci < CURSORS_NUM; ++ci )
        {
            m_cursorState[ci] = ( CS_OUTSIDE | CS_RELEASED );
        }
        for( int ef = 0; ef < CE_NUM; ++ef )
        {
            m_eventFunc[ef].Reset( NULL );
        }
    }

    void UIComponent::ProcessEvent( CursorEvent ev, CursorIdx idx, const Pos& cursorPos )
    {
        if( m_active && m_eventFunc[ev] )
            m_eventFunc[ev]->Call( this, ev, idx, cursorPos );
    }

    void UIComponent::CalculateRelativeLayout( const UIComponent* relativeTo )
    {
        // Check relative layout flags 
        const int relativeFlags = GetRelativeLayout();
        if( relativeFlags != RL_NONE && relativeTo != this )
        {
            // Checkout size that will be our reference
            Size referenceSize;
            if( relativeTo )
            {
                // Get "parent" size
                referenceSize = relativeTo->GetSize();
            }
            else
            {
                // Get screen size
                referenceSize.m_x = Claw::Application::GetInstance()->GetDisplay()->GetWidth();
                referenceSize.m_y = Claw::Application::GetInstance()->GetDisplay()->GetHeight();
            }

            // Update size if necessary
            if( (relativeFlags & RL_SIZE) != 0 )
            {
                if( (relativeFlags & RL_WIDTH) != 0 )
                {
                    m_size.m_x = (int)(referenceSize.m_x * m_sizeRelative.m_x + 0.5f);
                    if( m_sizeRelativeMax.m_x > 0 )
                        m_size.m_x = Claw::Min( m_size.m_x, m_sizeRelativeMax.m_x );
                    if( m_sizeRelativeMin.m_x > 0 )
                        m_size.m_x = Claw::Max( m_size.m_x, m_sizeRelativeMin.m_x );
                }
                if( (relativeFlags & RL_HEIGHT) != 0 )
                {
                    m_size.m_y = (int)(referenceSize.m_y * m_sizeRelative.m_y + 0.5f);
                    if( m_sizeRelativeMax.m_y > 0 )
                        m_size.m_y = Claw::Min( m_size.m_y, m_sizeRelativeMax.m_y );
                    if( m_sizeRelativeMin.m_y > 0 )
                        m_size.m_y = Claw::Max( m_size.m_y, m_sizeRelativeMin.m_y );
                }
            }

            // Update local position if necessary
            if( (relativeFlags & RL_POS) != 0 )
            {
                if( (relativeFlags & RL_POS_X) != 0 )
                {
                    m_posLocal.m_x = (int)(referenceSize.m_x * m_posRelative.m_x + 0.5f);
                }
                if( (relativeFlags & RL_POS_Y) != 0 )
                {
                    m_posLocal.m_y = (int)(referenceSize.m_y * m_posRelative.m_y + 0.5f);
                }
            }
        }
    }

    void UIComponent::NotifyUpTree( ComponentNotification cn )
    {
        if( m_parent )
        {
            m_parent->ChildNotification( cn, this );
        }
    }

    void UIComponent::ParentNotification( ComponentNotification cn, UIContainer* cont )
    {
        // If parent size changed invalidate relative size and position if neccessary
        if( cn == CN_SIZE_CHANGED )
        {
            CalculateRelativeLayout( cont );
        }
        else if( cn == CN_POS_CHANGED )
        {
            SetParentPos( cont->GetViewPos() );
        }
    }

    float UIComponent::GetFinalAlpha() const
    {
        if( m_parent )
        {
            return m_parent->GetFinalAlpha() * m_alpha;
        }
        else
        {
            return m_alpha;
        }
    }

} // namespace ClawExt
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIGrid.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_GRID_HPP__
#define __UI_UI_GRID_HPP__

// Internal includes
#include "claw_ext/ui/UIPanel.hpp"

// External includes
#include "claw/base/SmartPtr.hpp"

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    //! Class defining row/column container that can be comosed into full grid.
    /*!
    * Each grid is composed of UICell's (derived from UIPanel) to which futher 
    * components can be added by user.
    * By adding e.g. Horizontal container to Vertical container cell a full grid can be created.
    * UIGrid is usefull for creating general screen layouts.
    */
    class UIGrid : public UIContainer
    {
    public:
        //! Subcontainer used to represent cell in UIGrid parent container
        class UICell : public UIPanel
        {
        public:
            enum SizeType
            {
                ST_NONE = -1,
                ST_FIXED,
                ST_RELATIVE
            }; // enum SizeType

            //! Default constructor creating relative sized cell wit size == 1.0.
                                UICell();

            //! Set fixed size in pixels.
            virtual void        SetFixedSize( int size );

            //! Get fixed size of this cell (0 returned if cell has relative size)
            virtual int         GetFixedSize() const;

            //! Set relative size ratio of this cell (1.0 is default value)
            virtual void        SetRelativeSize( float size );

            //! Get relative size ratio (0 is returned if cell has fixed size)
            virtual float       GetRelativeSize() const;

            //! Get sizing mode of this cell (relative/fixed)
            inline SizeType     GetSizeType() const;

            //! Set cell minimal size if relative size is selected
            virtual inline void  SetMinSize( int minSize );

            //! Get cell minimal size - 0 if not set
            virtual inline  int  GetMinSize() const;

        private:
            // Hide some methods from base class that should not be touched
            inline void         SetRelativePos( const RelPos& pos );
            inline void         SetRelativeSize( const RelSize& size );
            inline void         SetRelativeLayout( int relativeLayoutFlags );
            inline void         SetLocalPos( const Pos& pos ) {};
            inline void         SetParentOff( const Pos& off );

            float               m_size;
            int                 m_minSize;
            SizeType            m_sizeType;

        }; // class UICell

        enum GridOrientation
        {
            GO_VERTICAL,
            GO_HORIZONTAL,

            GO_NUM
        }; // enum GridOrientation


        //! Constructor creating grid with given orientation.
                                UIGrid( GridOrientation orientation );

        //! Constructor creating grid with given orientation and cells number.
        /*!
        * By default cells created are relative size with aspect == 1.0.
        */
                                UIGrid( GridOrientation orientation, int numberOfCells );

        //! Try to load layout data from configuration xml.
        virtual void            Load( const Claw::XmlIt* xmlConfig );

        //! Set number of cells in the grid (also can reduce number of cells).
        virtual void            SetCellsNum( int numberOfCells );

        //! Get current cells count.
        virtual int             GetCellsNum() const;

        //! Get cell with given id (0-based indexing).
        virtual UICell*         GetCell( int cellId ) const;

        //! Shortcut method for adding components to given cell.
        virtual void            AddComponentToCell( int cellId, UIComponent* component, bool invalidate = true );

        //! Shortcut method to fix size (in pixels) of given cell.
        virtual void            SetCellFixedSize( int cellId, int size, bool invalidate = true );

        //! Get fixed size of given cell (0 returned if no fixed size was set).
        virtual int             GetCellFixedSize( int cellId ) const;

        //! Shortcut method to set relative size aspect of a given cell (1.0 is default).
        /*!
        * If changing one cell aspect, make sure that average aspect of all relative cels is equal to 1.0.
        */
        virtual void            SetCellRelativeSize( int cellId, float size, bool invalidate = true );

        //! Get relative aspect of given cell (0 returned if cell has fixed sie).
        virtual float           GetCellRelativeSize( int cellId ) const;

        //! Shortuct method to set minial size of given cell.
        virtual void            SetCellMinSize( int cellId, int minSize, bool invalidate = true );

        //! Get minimal size of given cell.
        virtual int             GetCellMinSize( int cellId ) const;

        //! Set grid orientation GO_VERTICAL == Column / GO_HORIZONTAL == row.
        virtual void            SetOrientation( GridOrientation orientation, bool invalidate = true );

        //! Recalcualte all cell sizes.
        virtual void            Invalidate();

    protected:
        GridOrientation         m_orientation;

    }; // class UIList


    inline UIGrid::UICell::SizeType UIGrid::UICell::GetSizeType() const
    {
        return m_sizeType;
    }

    inline void UIGrid::UICell::SetMinSize( int minSize )
    {
        m_minSize = minSize;
    }

    inline int UIGrid::UICell::GetMinSize() const
    {
        return m_minSize;
    }

} // namespace ClawExt

#endif // ifndef __UI_UI_GRID_HPP__
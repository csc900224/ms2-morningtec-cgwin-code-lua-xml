//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIImage.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIImage.hpp"

// External includes
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    // Some helper static constants
    static const float              S_ANGLE( 0.f );
    static const Claw::Point2f      S_PIVOT( 0.f, 0.f );
    static const UIComponent::Size  S_DEFAULT_SIZE( -1, -1 );

    UIImage::UIImage( const Pos& pos )
        : UIComponent( pos, S_DEFAULT_SIZE )
        , m_image( NULL )
        , m_imageSize( 0, 0 )
        , m_flipMode( FM_NONE )
        , m_color( Claw::MakeRGB( 255, 255, 255 ) )
    {
    }

    UIImage::UIImage( const Pos& pos, const Size& size )
        : UIComponent( pos, size )
        , m_image( NULL )
        , m_imageSize( 0, 0 )
        , m_flipMode( FM_NONE )
    {
    }

    void UIImage::Load( const Claw::XmlIt* xmlConfig )
    {

    }

    void UIImage::Render( Claw::Surface* target )
    {
        using namespace Claw;

        const Pos& p = GetViewPos();
        const Size& compSize = GetSize();
        const Size& imgSize = GetImageSize();
        const SurfacePtr& src = m_image;

        if( imgSize.m_x > 0 && imgSize.m_y > 0 )
        {
            TriangleEngine::FlipMode fm = TriangleEngine::FM_NONE;
            if( (m_flipMode & FM_VERTICAL) != 0 )
                fm |= TriangleEngine::FM_VERTICAL;
            if( (m_flipMode & FM_HORIZONTAL) != 0 )
                fm |= TriangleEngine::FM_HORIZONTAL;

            Vector2f scale( (float)(compSize.m_x) / imgSize.m_x, (float)(compSize.m_y) / imgSize.m_y );
            Claw::Rect srcRect( 0, 0, src->GetWidth(), src->GetHeight() );

            src->SetAlpha( (int)(255 * GetFinalAlpha()) );
            TriangleEngine::BlitAlpha( target, src, (float)p.m_x, (float)p.m_y, S_ANGLE, scale, S_PIVOT, fm, srcRect, m_color );
            src->SetAlpha( 255 );
        }
    }

    void UIImage::SetImage( const Claw::SurfacePtr& img )
    {
        m_image = img;
        if( m_image )
        {
            m_imageSize.Set( img->GetWidth(), img->GetHeight() );
            // If default size used replace with image size.
            if( GetSize() == S_DEFAULT_SIZE )
            {
                SetSize( m_imageSize );
            }
        }
        else
        {
            m_imageSize.Set( 0, 0 );
        }
    }

    Claw::SurfacePtr UIImage::GetImage() const
    {
        return m_image;
    }

    void UIImage::LoadImg( const char* imgPath )
    {
        using namespace Claw;
        SurfacePtr surfPtr( NULL );
        if( imgPath == NULL )
        {
            // Setup empty image - no graphics will be displayed
            SetImage( surfPtr );
            return;
        }

        surfPtr = AssetDict::Get< Surface >( imgPath );

        SetImage( surfPtr );
    }

} // namespace ClawExt
// EOF

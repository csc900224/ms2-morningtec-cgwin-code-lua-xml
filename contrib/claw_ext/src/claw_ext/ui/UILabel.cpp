//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UILabel.cpp
//
//  AUTHOR(S):
//      Remigiusz Osowski <remigiusz.osowski@game-lion.com>
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UILabel.hpp"
#include "claw_ext/ui/UIContainer.hpp"

// External includes
#include "claw/graphics/Surface.hpp"
#include "claw/base/Xml.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/graphics/text/Typesetter.hpp"

#define _DEBUG_TEXT_AREA    0   //! Set to 1 if you want to render entire text area.

namespace ClawExt
{
    UILabel::UILabel( const Pos& pos, const Claw::NarrowString& fontXmlPath, const Claw::Color clr /* = Claw::Color( 255, 255, 255 ) */ )
        : UIComponent( pos )
        , m_text( "" )
        , m_alignHoriz( AH_NONE )
        , m_alignVert( AV_NONE )
        , m_scale( 1.0f )
        , m_autoScale( 1.0f )
        , m_autoScaling( false )
        , m_useFixedSize( false )
    {
        m_font = Claw::AssetDict::Get<Claw::FontEx>( fontXmlPath );

        m_fontSet.Reset( new Claw::Text::FontSet() );
        m_fontSet->AddFont( "default", m_font );

        m_format.SetFontSet( m_fontSet );
        m_format.SetFontId( "default" );
        m_format.SetColor( clr );

        Invalidate();
    }

    void UILabel::Load( const Claw::XmlIt* xmlConfig )
    {

    }

    void UILabel::Render( Claw::Surface* target )
    {
        const Pos& pos = GetViewPos();
        const Size& size = GetSize();

#if _DEBUG_TEXT_AREA
        target->DrawRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x, pos.m_y + size.m_y, m_format.GetColor() );
#endif
        Claw::UInt8 alpha = m_font->GetSurface()->GetAlpha();
        m_font->GetSurface()->SetAlpha( (int)(255 * GetFinalAlpha()) );
        m_screenText->Draw( target, pos.m_x, pos.m_y, 0.f, m_scale * m_autoScale, m_pivot );
        m_font->GetSurface()->SetAlpha( alpha );
    }

    void UILabel::SetText( const Claw::String& text )
    {
        m_text = text;

        Invalidate();
    }

    void UILabel::SetColor( const Claw::Color& clr )
    {
        m_format.SetColor( clr );

        Invalidate();
    }

    void UILabel::SetTextAlign( AlignHoriz hAlign, AlignVert vAlign /*= AV_NONE*/ )
    {
        m_alignHoriz = hAlign;
        m_alignVert = vAlign;

        Invalidate();
    }

    void UILabel::SetScale( float scale )
    {
        m_scale = scale;

        Invalidate();
    }

    void UILabel::UseFixedSize( bool useFixedSize, const Size& fixedSize /*= Size()*/ )
    {
        m_useFixedSize = useFixedSize;
        m_fixedSize = fixedSize;

        if ( !m_useFixedSize )
        {
            m_autoScaling = false;
            m_autoScale = 1.0f;
        }

        Invalidate();
    }

    void UILabel::SetAutoScalingToFitArea( bool autoScaling )
    {
        m_autoScaling = autoScaling;
        m_autoScale = 1.0f;

        if ( m_autoScaling )
        {
            m_useFixedSize = true;
        }

        Invalidate();
    }

    void UILabel::Invalidate()
    {
        Claw::RichString richText( m_text, m_format );

        Claw::Text::Typesetter ts;
        Claw::Text::DrawBit* bits = ts.TypesetRich( richText, Claw::Rect( 0, 0, Claw::NumberTraits<int>::Maximum(), 0 ) );

        Size textSize;
        textSize.m_x = ts.CalcWidthReal( bits );
        textSize.m_y = m_font->GetHeight();

        delete [] bits;

        m_screenText.Reset( new Claw::ScreenText(
                            m_format,
                            m_text,
                            Claw::Extent( textSize.m_x, textSize.m_y )
            )
        );

        if ( m_useFixedSize )
        {
            m_size = m_fixedSize;

            if ( GetParent() )
            {
                CalculateRelativeLayout( GetParent() );
            }

            if ( m_autoScaling && textSize.m_x > 0.0f && textSize.m_y > 0.0f )
            {
                float xScale = m_size.m_x > 0 ? m_size.m_x / (m_scale * textSize.m_x) : 1.0f;
                float yScale = m_size.m_y > 0 ? m_size.m_y / (m_scale * textSize.m_y) : 1.0f;
                if ( xScale < 1.0f || yScale < 1.0f )
                {
                    m_autoScale = Claw::Min( xScale, yScale );
                }
                else
                {
                    m_autoScale = 1.0f;
                }
            }

            float scaleTotal = m_autoScale * m_scale;

            AlignHoriz hAlign = m_alignHoriz == AH_NONE ? m_align.m_horiz : m_alignHoriz;
            if ( hAlign == AH_LEFT )
            {
                m_pivot.m_x = 0.0f;
            }
            else if ( hAlign == AH_RIGHT )
            {
                m_pivot.m_x = (float)(scaleTotal * textSize.m_x - m_size.m_x);
            }
            else
            {
                m_pivot.m_x = (float)(scaleTotal * textSize.m_x - m_size.m_x) * 0.5f;
            }

            AlignVert vAlign = m_alignVert == AV_NONE ? m_align.m_vert : m_alignVert;
            if ( vAlign == AV_TOP )
            {
                m_pivot.m_y = 0.0f;
            }
            else if ( vAlign == AV_BOTTOM )
            {
                m_pivot.m_y = (float)(scaleTotal * textSize.m_y - m_size.m_y);
            }
            else
            {
                m_pivot.m_y = (float)(scaleTotal * textSize.m_y - m_size.m_y) * 0.5f;
            }

            // Scale pivot by totalScale here instead doing it in the Render function every tick
            m_pivot /= scaleTotal;

            // Fix size
            Size newSize = m_size;
            if ( newSize.m_x == 0 )
            {
                newSize.m_x = textSize.m_x;
            }
            if ( newSize.m_y == 0 )
            {
                newSize.m_y = textSize.m_y;
            }
            SetSize( newSize );
        }
        else
        {
            m_pivot.Zero();
            SetSize( Size( static_cast<int>( m_scale * textSize.m_x ), static_cast<int>( m_scale * textSize.m_y ) ) );
        }
    }

} // namespace ClawExt
// EOF

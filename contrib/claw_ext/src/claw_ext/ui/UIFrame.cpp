//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIFrame.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIFrame.hpp"

// External includes
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/base/Xml.hpp"

#define F( a )  (float)( (a) )

namespace ClawExt
{
    // Some helper static constants
    static const float          ANGLE( 0.f );
    static const float          SCALE( 1.f );
    static const Claw::Point2f  PIVOT( 0.f, 0.f );

    UIFrame::UIFrame( const Pos& pos, Claw::Color clr /* = Claw::Color( 255, 0, 0 ) */ )
        : UIComponent( pos )
        , m_color( clr )
    {
    }

    UIFrame::UIFrame( const Pos& pos, const Size& size, const Claw::Color clr /* = Claw::Color( 255, 0, 0 ) */ )
        : UIComponent( pos, size )
        , m_color( clr )
    {

    }

    void UIFrame::Load( const Claw::XmlIt* xmlConfig )
    {

    }

    void UIFrame::Render( Claw::Surface* target )
    {
        const Pos& pos = GetViewPos();
        const Size& size = GetSize();
        const Border& brd = m_border;

        if( m_border.IsValid() )
        {
            target->DrawFilledRectangle(F(pos.m_x + brd.m_offLeftTop.m_x),
                                        F(pos.m_y + brd.m_offLeftTop.m_y),
                                        F(pos.m_x + size.m_x + brd.m_offRightDown.m_x),
                                        F(pos.m_y + size.m_y + brd.m_offRightDown.m_y),
                                        m_color );

            RenderBorder( target );
        }
        else
        {
            target->DrawFilledRectangle( F(pos.m_x), F(pos.m_y), F(pos.m_x + size.m_x), F(pos.m_y + size.m_y), m_color );
        }
    }

    void UIFrame::SetColor( const Claw::Color& clr )
    {
        m_color = clr;
    }

    const Claw::Color& UIFrame::GetColor() const
    {
        return m_color;
    }

    void UIFrame::SetBorder( const Border* border )
    {
        // Check if user wants to remove border graphics
        if( border->m_top.GetPtr() == NULL )
        {
            m_border = Border();
            return;
        }

        // Otherwise everything needs to be perfect :).
        CLAW_ASSERT( border->m_top );
        CLAW_ASSERT( border->m_down );
        CLAW_ASSERT( border->m_left );
        CLAW_ASSERT( border->m_right );

        CLAW_ASSERT( border->m_cornerLeftTop );
        CLAW_ASSERT( border->m_cornerLeftDown );
        CLAW_ASSERT( border->m_cornerRightTop );
        CLAW_ASSERT( border->m_cornerRightDown );

        m_border = *border;
    }

    void UIFrame::LoadBorder( const BorderSettings* sett )
    {
        Border border;
        if( sett == NULL )
        {
            // Setup empty border - no border display
            SetBorder( &border );
            return;
        }

        using namespace Claw;
        border.m_top = AssetDict::Get< Surface >( NarrowString( sett->m_top + "@" ) );
        border.m_down = AssetDict::Get< Surface >( NarrowString( sett->m_down + "@" ) );
        border.m_left = AssetDict::Get< Surface >( NarrowString( sett->m_left + "@" ) );
        border.m_right = AssetDict::Get< Surface >( NarrowString( sett->m_right + "@" ) );
        border.m_cornerLeftTop = AssetDict::Get< Surface >( NarrowString( sett->m_cornerLeftTop + "@" ) );
        border.m_cornerLeftDown = AssetDict::Get< Surface >( NarrowString( sett->m_cornerLeftDown + "@" ) );
        border.m_cornerRightTop = AssetDict::Get< Surface >( NarrowString( sett->m_cornerRightTop + "@" ) );
        border.m_cornerRightDown = AssetDict::Get< Surface >( NarrowString( sett->m_cornerRightDown + "@" ) );

        border.m_offLeftTop = sett->m_offLeftTop;
        border.m_offRightDown = sett->m_offRightDown;

        CLAW_ASSERT( border.IsValid() );

        SetBorder( &border );
    }

    void UIFrame::RenderBorder( Claw::Surface* target ) const
    {
        const Pos& p = GetViewPos();
        const Size& b = GetSize();
        const Border& gfx = m_border;

        // Render corners
        // Left-top
        RenderBorderCorner( target, gfx.m_cornerLeftTop, p.m_x, p.m_y );
        // Left-down
        RenderBorderCorner( target, gfx.m_cornerLeftDown, p.m_x, p.m_y + b.m_y - gfx.m_cornerLeftDown->GetHeight() );
        // Right-top (flip if just an miror of left-top)
        RenderBorderCorner( target, gfx.m_cornerRightTop, p.m_x + b.m_x - gfx.m_cornerRightTop->GetWidth(),
                                                          p.m_y,
                                                          gfx.m_cornerRightTop == gfx.m_cornerLeftTop ? Border::BF_HORIZ : Border::BF_NONE );
        // Right-down (flip if mirror of left-down used)
        RenderBorderCorner( target, gfx.m_cornerRightDown, p.m_x + b.m_x - gfx.m_cornerRightDown->GetWidth(),
                                                           p.m_y + b.m_y - gfx.m_cornerRightDown->GetHeight(),
                                                           gfx.m_cornerRightDown == gfx.m_cornerLeftDown ? Border::BF_HORIZ : Border::BF_NONE );

        // Horizontal stripes:
        // Top
        RenderBorderStripeH( target, gfx.m_top, p.m_x + gfx.m_cornerLeftTop->GetWidth(), p.m_y, b.m_x - gfx.m_cornerLeftTop->GetWidth() - gfx.m_cornerRightTop->GetWidth() );
        // Down
        RenderBorderStripeH( target, gfx.m_down, p.m_x + gfx.m_cornerLeftDown->GetWidth(), p.m_y + b.m_y - gfx.m_down->GetHeight(), b.m_x - gfx.m_cornerLeftDown->GetWidth() - gfx.m_cornerRightDown->GetWidth() );

        // Vertical stripes:
        // Left
        RenderBorderStripeV( target, gfx.m_left, p.m_x, p.m_y + gfx.m_cornerLeftTop->GetHeight(), b.m_y - gfx.m_cornerLeftTop->GetHeight() - gfx.m_cornerLeftDown->GetHeight() );
        // Right
        RenderBorderStripeV( target, gfx.m_right, p.m_x + b.m_x - gfx.m_right->GetWidth() + 1, p.m_y + gfx.m_cornerRightTop->GetHeight(), b.m_y - gfx.m_cornerRightTop->GetHeight() - gfx.m_cornerRightDown->GetHeight() );
    }

    void UIFrame::RenderBorderCorner( Claw::Surface* target, Claw::Surface* src, int x, int y, Border::Flip flip ) const
    {
        using namespace Claw;

        if( flip == Border::BF_NONE )
        {
            target->Blit( F(x), F(y), src );
        }
        else
        {
            TriangleEngine::Blit( target, src, F(x), F(y), ANGLE, SCALE, PIVOT, flip == Border::BF_HORIZ ? TriangleEngine::FM_HORIZONTAL : TriangleEngine::FM_VERTICAL );
        }
    }

    void UIFrame::RenderBorderStripeH( Claw::Surface* target, Claw::Surface* src, int x, int y, int w, bool flip ) const
    {
        using namespace Claw;

        if( !flip )
        {
            while( w > src->GetWidth() )
            {
                target->Blit( F(x), F(y), src );
                w -= src->GetWidth();
                x += src->GetWidth();
            }
            target->Blit( F(x), F(y), src, Rect( w, src->GetHeight() ) );
        }
        else
        {
            const TriangleEngine::FlipMode f = TriangleEngine::FM_VERTICAL;
            while( w > src->GetWidth() )
            {
                TriangleEngine::Blit( target, src, F(x), F(y), ANGLE, SCALE, PIVOT, f );
                w -= src->GetWidth();
                x += src->GetWidth();
            }
            TriangleEngine::Blit( target, src, F(x), F(y), ANGLE, SCALE, PIVOT, f, Rect( w, src->GetHeight() ) );
        }
    }

    void UIFrame::RenderBorderStripeV( Claw::Surface* target, Claw::Surface* src, int x, int y, int h, bool flip ) const
    {
        using namespace Claw;

        if( !flip )
        {
            while( h > src->GetHeight() )
            {
                target->Blit( F(x), F(y), src );
                h -= src->GetHeight();
                y += src->GetHeight();
            }
            target->Blit( F(x), F(y), src, Rect( src->GetWidth(), h ) );
        }
        else
        {
            TriangleEngine::FlipMode f = TriangleEngine::FM_HORIZONTAL;
            while( h > src->GetHeight() )
            {
                TriangleEngine::Blit( target, src, F(x), F(y), ANGLE, SCALE, PIVOT, f );
                h -= src->GetHeight();
                y += src->GetHeight();
            }
            TriangleEngine::Blit( target, src, F(x), F(y), ANGLE, SCALE, PIVOT, f, Rect( src->GetWidth(), h ) );
        }
    }

    UIFrame::Border::Border()
        : m_top( NULL )
        , m_down( NULL )
        , m_left( NULL )
        , m_right( NULL )
        , m_cornerLeftTop( NULL )
        , m_cornerLeftDown( NULL )
        , m_cornerRightTop( NULL )
        , m_cornerRightDown( NULL )
    {}

    bool UIFrame::Border::IsValid() const
    {
        return  (m_top || m_down) &&
                (m_left || m_right) &&
                (m_cornerLeftTop || m_cornerRightTop) &&
                (m_cornerLeftDown || m_cornerRightDown);
    }

} // namespace ClawExt
// EOF

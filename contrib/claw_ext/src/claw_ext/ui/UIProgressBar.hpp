//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIProgressBar.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_PROGRESS_BAR_HPP__
#define __UI_UI_PROGRESS_BAR_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include "claw/graphics/Color.hpp"
#include "claw/graphics/Surface.hpp"

// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    //! Progress bar renders colored stripe that fills predefined area, depending on progress factor defined from 0 to 1.
    /*!
    * This UI component allows to use graphics image replication for rendering both progress and bar background as also
    * provides mechanism for rendering semi-opaque stripes filled with colored background below them. You can also
    * use the simplest scenario where progress bar is rendered using just simple colored rectangles.
    * If you don't wont to see the entire bar area but progress stripe only simply call SetFillBg() method to disable
    * bar background filling.
    */
    class UIProgressBar : public UIComponent
    {
    public:
        enum BarOrientation
        {
            BO_HORIZ,
            BO_VERT,

            BO_NUM
        }; // enum BarOrientation

        //! Constructor defining bar position and color.
                            UIProgressBar( const Pos& pos, const Claw::Color clr = Claw::Color( 255, 0, 0 ) );

        //! Constructor defining bar position, dimensions and color.
                            UIProgressBar( const Pos& pos, const Size& size, const Claw::Color clr = Claw::Color( 255, 0, 0 ) );

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Render on the top of current display buffer.
        virtual void        Render( Claw::Surface* target );

        //! Setup bar color.
        /*!
        * This color will be used to fill progress rectangle or supply semi-opaque bar stripe with background.
        * \see LoadStripe() method.
        */
        void                SetColor( const Claw::Color& clr );

        //! Acquire current bar color.
        /*!
        * \see SetColor() for details.
        */
        const Claw::Color&  GetColor() const;

        //! Setup color of the background filling entire bar area.
        /*!
        * This color fills entiter bar area whatever the progress will be.
        * It may be also used in conjunction with background stripe.
        * \see LoadBgStripe() method for further details.
        */
        void                SetBgColor( const Claw::Color& clr );

        //! Acquire current bar color.
        /*!
        * \see SetColor() for details.
        */
        const Claw::Color&  GetBgColor() const;

        //! Setup color for foreground filling of entire bar area.
        /*!
        * Color may be used for blending foreground image if this option was enabled
        * \see LoadFgStripe() for details.
        */
        void                SetFgColor( const Claw::Color& clr );

        //! Acquire foreground/overlay color.
        /*!
        * \see SetColor() for details.
        */
        const Claw::Color&  GetFgColor() const;


        //! Setup orientation: horizontal or vertical.
        void                SetOrientation( BarOrientation orient );

        //! Current orientation.
        inline
        BarOrientation      GetOrientation() const;

        //! Enable/disable background filling of entire bar area.
        void                SetBgFill( bool enable );

        //! Check if background is currently filled.
        inline bool         GetBgFill() const;

        //! Enable/disable drawing foreground filling of entire bar area.
        void                SetFgFill( bool enable );

        //! Check if background is currently filled.
        inline bool         GetFgFill() const;

        //! Setup progress as fraction between 0 and 1 (inclusively).
        void                SetProgress( float progress );

        //! Get current progress set.
        inline float        GetProgress() const;

        //! Setup progress bar graphics replicated during progress rendering.
        /*!
        * We may also use semi-opaque sprite for details rendering (light effects, shadows) while filling background with
        * predefined color - to enable this option specify true for useBgColor parameter.
        * \param gfxPath path to resource that should be already preloaded into AssetDict.
        * \param useBgColor allows to render colored background under stripe area.
        */
        void                LoadStripe( const char* gfxPath, bool useBlendColor = false );

        //! Load background graphics which will be displayed on the whole bar area under progress stripe.
        /*!
        * We may also use semi-opaque sprite for details rendering (light effects, shadows) and fill
        * its background with another color - to enable this option specify true for useBgColor parameter.
        * \see LoadStripe() method.
        * \param gfxPath path to resource that should be already preloaded into AssetDict.
        * \param useBgColor allows to render colored background under stripe area.
        */
        void                LoadBgStripe( const char* gfxPath, bool useBlendColor = false );

        //! Load foreground (bar overlay) graphics which will be displayed on the whole bar area over progress stripe.
        /*!
        * Semi-opaque sprites may be used light effects, shadows, etc. Graphics may be also defined in the
        * grayscale for runtime coloring  - to enable this option specify true for useBlendColor parameter.
        * \see LoadBgStripe() method.
        * \param gfxPath path to resource that should be already preloaded into AssetDict.
        * \param useBlendColor modifies graphics during rendering with a color blending.
        */
        void                LoadFgStripe( const char* gfxPath, bool useBlendColor = false );

    protected:
        //! Load stripe graphics based for replication during bar rendering.
        void                SetStripe( const Claw::SurfacePtr& gfxSurface, bool useBgColor = false );

        //! Load graphics overlay that will be rendered on top of progress stripe.
        void                SetFgStripe( const Claw::SurfacePtr& gfxSurface, bool useBgColor = false );

        //! Load stripe graphics based for replication during bar rendering.
        void                SetBgStripe( const Claw::SurfacePtr& gfxSurface, bool useBgColor = false );

        void                RenderProgress( Claw::Surface* target );

        void                RenderBg( Claw::Surface* target );

        void                RenderFg( Claw::Surface* target );

        void                RenderStripeH( Claw::Surface* target, Claw::Surface* src, float x, float y, float w, bool colorBlend, const Claw::Color& c, bool flip = false ) const;

        void                RenderStripeV( Claw::Surface* target, Claw::Surface* src, float x, float y, float h, bool colorBlend, const Claw::Color& c, bool flip = false ) const;

        BarOrientation      m_orientation;
        bool                m_bgFill;
        bool                m_fgFill;
        float               m_progress;

        Claw::Color         m_progColor;
        Claw::SurfacePtr    m_progStripe;
        bool                m_progUseColoring;

        Claw::Color         m_bgColor;
        Claw::SurfacePtr    m_bgStripe;
        bool                m_bgUseColoring;

        Claw::Color         m_fgColor;
        Claw::SurfacePtr    m_fgStripe;
        bool                m_fgUseColoring;

    }; // class UIProgressBar

    inline const Claw::Color& UIProgressBar::GetColor() const
    {
        return m_progColor;
    }

    inline const Claw::Color& UIProgressBar::GetBgColor() const
    {
        return m_bgColor;
    }

    inline const Claw::Color& UIProgressBar::GetFgColor() const
    {
        return m_fgColor;
    }

    inline UIProgressBar::BarOrientation UIProgressBar::GetOrientation() const
    {
        return m_orientation;
    }

    inline bool UIProgressBar::GetBgFill() const
    {
        return m_bgFill;
    }

    inline bool UIProgressBar::GetFgFill() const
    {
        return m_fgFill;
    }

    inline float UIProgressBar::GetProgress() const
    {
        return m_progress;
    }

} // namespace ClawExt

#endif // !defined __UI_UI_PROGRESS_BAR_HPP__
// EOF

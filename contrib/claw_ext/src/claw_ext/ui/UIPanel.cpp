//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIPanel.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIPanel.hpp"

// External includes
#include "claw/graphics/Surface.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    UIPanel::UIPanel()
        : UIContainer( Pos() )
    {}

    UIPanel::UIPanel( const Pos& pos )
        : UIContainer( pos )
    {
    }

    UIPanel::UIPanel( const Pos& pos, const Size& size )
        : UIContainer( pos, size )
    {
    }

    void UIPanel::Load( const Claw::XmlIt* xmlConfig )
    {
    }

    void UIPanel::Invalidate()
    {
        // Extend container boundaries
        const Pos& viewPos = GetViewPos();
        const Size& volCurr = GetSize();
        SubComponentsConstIt it = m_children.begin();
        SubComponentsConstIt end = m_children.end();
        UIComponent* comp;
        for( ; it != end; ++it )
        {
            comp = *it;
            m_size.m_x = Claw::Max( volCurr.m_x, comp->GetLocalPos().m_x + comp->GetSize().m_x );
            m_size.m_y = Claw::Max( volCurr.m_y, comp->GetLocalPos().m_y + comp->GetSize().m_y );
        }

        it = m_children.begin();
        end = m_children.end();
        for( ; it != end; ++it )
        {
            comp = *it;
            comp->SetParentPos( viewPos );
            comp->SetParentOff( CalcAlignOffset( comp ) );
        }

        // Process base class invalidation
        UIContainer::Invalidate();
    }

    UIPanel::Pos UIPanel::CalcAlignOffset( const UIComponent* comp ) const
    {
        Pos off( 0, 0 );
        if( comp->GetAlign().m_horiz == UIComponent::AH_RIGHT )
        {
            off.m_x = GetSize().m_x - comp->GetSize().m_x;
        }
        else if( comp->GetAlign().m_horiz == UIComponent::AH_CENTER )
        {
            off.m_x = (int)(GetSize().m_x / 2.f - comp->GetSize().m_x / 2.f);
        }

        if( comp->GetAlign().m_vert == UIComponent::AV_BOTTOM )
        {
            off.m_y = GetSize().m_y - comp->GetSize().m_y;
        }
        else if( comp->GetAlign().m_vert == UIComponent::AV_CENTER )
        {
            off.m_y = (int)(GetSize().m_y / 2.f - comp->GetSize().m_y / 2.f);
        }
        return off;
    }

} // namespace ClawExt
// EOF

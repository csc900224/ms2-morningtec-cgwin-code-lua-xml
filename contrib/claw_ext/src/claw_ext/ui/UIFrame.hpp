//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIFrame.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_FRAME_HPP__
#define __UI_UI_FRAME_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include "claw/graphics/Color.hpp"
#include "claw/graphics/Surface.hpp"

// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    //! Simple rectangular panel with an give color.
    class UIFrame : public UIComponent
    {
    public:
        // Forward declare internal sturcture
        struct Border;
        struct BorderSettings;

        //! Constructor defining frame position and color.
                            UIFrame( const Pos& pos, const Claw::Color clr = Claw::Color( 255, 0, 0 ) );

        //! Constructor defining frame position, dimensions and color.
                            UIFrame( const Pos& pos, const Size& size, const Claw::Color clr = Claw::Color( 255, 0, 0 ) );

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Render on the top of current display buffer.
        virtual void        Render( Claw::Surface* target );

        //! Setup frame background color.
        void                SetColor( const Claw::Color& clr );

        //! Acquire current background color.
        const Claw::Color&  GetColor() const;

        //! Setup border graphics.
        /*!
        * Simply pass NULL pointer to remove border graphics.
        */
        void                SetBorder( const Border* border );

        //! Load border graphics based on the border settings.
        void                LoadBorder( const BorderSettings* sett );

        struct Border
        {
            enum Flip
            {
                BF_NONE = 0,
                BF_HORIZ,
                BF_VERT,
            }; // enum Flip

                                Border();

            bool                IsValid() const;


            Claw::SurfacePtr    m_top;
            Claw::SurfacePtr    m_down;
            Claw::SurfacePtr    m_left;
            Claw::SurfacePtr    m_right;

            Claw::SurfacePtr    m_cornerLeftTop;
            Claw::SurfacePtr    m_cornerLeftDown;
            Claw::SurfacePtr    m_cornerRightTop;
            Claw::SurfacePtr    m_cornerRightDown;

            Pos                 m_offLeftTop;
            Pos                 m_offRightDown;

        }; // struct Border

        struct BorderSettings
        {
            Claw::NarrowString  m_top;
            Claw::NarrowString  m_down;
            Claw::NarrowString  m_left;
            Claw::NarrowString  m_right;

            Claw::NarrowString  m_cornerLeftTop;
            Claw::NarrowString  m_cornerLeftDown;
            Claw::NarrowString  m_cornerRightTop;
            Claw::NarrowString  m_cornerRightDown;

            Pos                 m_offLeftTop;
            Pos                 m_offRightDown;

        }; // struct BorderSettings

    protected:
        void                RenderBorder( Claw::Surface* target ) const;

        void                RenderBorderCorner( Claw::Surface* target, Claw::Surface* src, int x, int y, Border::Flip flip = Border::BF_NONE ) const;

        void                RenderBorderStripeH( Claw::Surface* target, Claw::Surface* src, int x, int y, int w, bool flip = false ) const;

        void                RenderBorderStripeV( Claw::Surface* target, Claw::Surface* src, int x, int y, int h, bool flip = false ) const;

        Claw::Color         m_color;
        Border              m_border;

    }; // class UIFrame

} // namespace ClawExt

#endif // !defined __UI_UI_FRAME_HPP__
// EOF

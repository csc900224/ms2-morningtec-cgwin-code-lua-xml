//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UILabel.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_LABEL_HPP__
#define __UI_UI_LABEL_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include "claw/graphics/Color.hpp"
#include "claw/graphics/FontEx.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/graphics/text/Format.hpp"
#include "claw/graphics/text/FontSet.hpp"

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    //! Simple label text.
    class UILabel : public UIComponent
    {
    public:
        //! Constructor defining label position, font and color.
                                UILabel( const Pos& pos, const Claw::NarrowString& fontXmlPath, const Claw::Color clr = Claw::Color( 255, 255, 255 ) );

        //! Try to load layout data from configuration xml.
        virtual void            Load( const Claw::XmlIt* xmlConfig );

        //! Render on the top of current display buffer.
        virtual void            Render( Claw::Surface* target );

        //! Setup text being displayed.
        void                    SetText( const Claw::String& text );

        //! Setup text color.
        void                    SetColor( const Claw::Color& clr );

        //! Setup text alignment (used only when fixed size is enabled)
        void                    SetTextAlign( AlignHoriz hAlign, AlignVert vAlign = AV_NONE );

        //! Setup text scale
        void                    SetScale( float scale );

        //! Downscale text size to fit component's area (enables using fixed size)
        void                    SetAutoScalingToFitArea( bool autoScaling );

        //! Use fixed size (component's BV won't be dependent on it's text area)
        void                    UseFixedSize( bool useFixedSize, const Size& fixedSize = Size() );

        //! Refresh screen text being displayed.
        virtual void            Invalidate();

    protected:
        Claw::String            m_text;

        Claw::FontExPtr         m_font;
        Claw::Text::FontSetPtr  m_fontSet;
        Claw::Text::Format      m_format;
        Claw::ScreenTextPtr     m_screenText;
        Claw::Point2f           m_pivot;

        AlignHoriz              m_alignHoriz;
        AlignVert               m_alignVert;

        float                   m_scale;
        float                   m_autoScale;
        bool                    m_autoScaling;
        bool                    m_useFixedSize;
        Size                    m_fixedSize;

    }; // class UILabel

} // namespace ClawExt

#endif // !defined __UI_UI_LABEL_HPP__
// EOF

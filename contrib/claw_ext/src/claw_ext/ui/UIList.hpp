//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIList.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_LIST_HPP__
#define __UI_UI_LIST_HPP__

// Internal includes
#include "claw_ext/ui/UIContainer.hpp"

// External includes

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    //! List of sub-components rendered in the order specified during components adding.
    /*!
    * Rendering of the list is perfomed with the orientation beeing considered and custom sub-components
    * alignements', in this way if list is vertically oriented subcomponents horizontal alignment
    * is beeing considered, otherwise if list has horizontal orientation only vertical sub-components'
    * alignment is taken into account. Local offsets of all sub-components' are also beeing considered in such
    * way that each item with local y offset in the vertical list moves down all items below with y + list spacing.
    * Local horizontal offset in such case, affects only this item horizontal position in the list.
    */
    class UIList : public UIContainer
    {
    public:
        enum ListOrientation
        {
            LO_VERTICAL,
            LO_HORIZONTAL,

            LO_NUM
        }; // enum ListOrientation

        enum ListSpacing
        {
            LS_FIXED,
            LS_AUTO,

            LS_NUM
        }; // enum ListSpacing

        //! Default constructor sets, list orientation to vertical while spacing (fixed) and size are set zero.
                            UIList();

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Overrride sub-component addition in order to add list positioning to the child components.
        virtual void        AddComponent( UIComponent* component );

        //! Setup list orientation.
        void                SetOrientation( ListOrientation lo );

        //! Setup list elements spacing strategy.
        void                SetSpacing( ListSpacing ls, int off = 0 );

    protected:
        //! Invalidate all sub-components positions.
        void                Invalidate();

        //! Recalcuate container volume - boundaries.
        void                InvalidateSize();

        //! Calculate minimum are that must be covered by childs boundaries taking into account current orientation.
        Size                CalcChildsVolume() const;

        //! Calculate alignment offset for a given sub-component.
        int                 CalcAlignOffset( const UIComponent* comp ) const;

        //! Calculates number of children that are active or visible.
        int                 CalcChildsVisibleNum();

        ListOrientation     m_orientation;
        ListSpacing         m_spacing;
        int                 m_spacingOff;
    }; // class UIList

} // namespace ClawExt

#endif // !defined __NETWORK_EASY_SHARE_HPP__
// EOF

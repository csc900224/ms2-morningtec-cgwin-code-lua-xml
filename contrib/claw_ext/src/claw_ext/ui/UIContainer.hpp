//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIContainer.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_CONTAINER_HPP__
#define __UI_UI_CONTAINER_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include <vector>

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    //! Component for representing various user interface items.
    class UIContainer : public UIComponent
    {
    public:
        enum RenderOrder
        {
            RO_FORWARD,
            RO_BACKWARD
        };

        //! Sub-components container
        typedef std::vector< UIComponent* > SubComponents;

        //! Relase all sub-components
        virtual                 ~UIContainer();

        //! Try to load layout data from configuration xml.
        virtual void            Load( const Claw::XmlIt* xmlConfig );

        //! Render all subcomponents.
        virtual void            Render( Claw::Surface* target );

        //! Perform subtree update (self update, and sub-components).
        virtual void            Update( float dt );

        //! Set active state (e.g. if comonent is enabled for cursor events).
        virtual void            SetActive( bool active );

        //! Setup new boundaries.
        virtual void            SetSize( const Size& size );

        //! Set local position related to parent container.
        virtual void            SetLocalPos( const Pos& pos );

        //! Add sub-component.
        virtual void            AddComponent( UIComponent* component );

        //! Remove all sub-components. If param is true, then all children will be released.
        virtual void            ClearComponents( bool release );

        //! Get all sub componetns of this container
        const SubComponents&    GetComponents() const;

        //! Override UIComponent cursor movement callback to propagate it to childs.
        virtual void            OnCursorMove( const Pos& newPos, CursorIdx idx );

        //! Override UIComponent cursor press callback to propagate it to childs.
        virtual void            OnCursorPress( const Pos& newPos, CursorIdx idx );

        //! Override UIComponent cursor release cursor callback to propagate this event to childs.
        virtual void            OnCursorRelease( const Pos& newPos, CursorIdx idx );

        //! Perform all childs invalidation (repositioning, size update, etc.)
        virtual void            Invalidate();

        //! Perform calcuatlion of relative layout
        virtual void            CalculateRelativeLayout( const UIComponent* relativeTo = NULL );

        //! Decides if childredn should be render in ordinary (bottom-first) order or should list be rendered backwards.
        inline void             SetRenderOrder( RenderOrder newOrder );

    protected:
        //! Default constructor sets position, parent offset and dimensions to zero.
                                UIContainer();

        //! Constructor defining container position while size is set to zero.
                                UIContainer( const Pos& pos );

        //! Constructor defining container position and fixed dimensions.
                                UIContainer( const Pos& pos, const Size& size );

        virtual void            NotifyDownTree( ComponentNotification cn );

        // Declare friendship to allow below methods calls between clasess.
        friend class UIComponent;

        virtual void            ParentNotification( ComponentNotification cn, UIContainer* cont );

        virtual void            ChildNotification( ComponentNotification cn, UIComponent* cont );

        typedef SubComponents::iterator         SubComponentsIt;
        typedef SubComponents::const_iterator   SubComponentsConstIt;

        SubComponents           m_children;
        RenderOrder             m_renderOrder;

    }; // class UIContainer


    inline void UIContainer::SetRenderOrder( RenderOrder newOrder )
    {
        m_renderOrder = newOrder;
    }

} // namespace ClawExt

#endif // !defined __NETWORK_EASY_SHARE_HPP__
// EOF

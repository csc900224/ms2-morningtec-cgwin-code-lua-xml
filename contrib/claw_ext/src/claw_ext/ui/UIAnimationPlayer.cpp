//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIAnimationPlayer.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIAnimationPlayer.hpp"

// External includes

namespace ClawExt
{
    static const UIComponent::Size  S_DEFAULT_SIZE( -1, -1 );

    UIAnimationPlayer::UIAnimationPlayer( const Pos& pos, UIAnimation* anim )
        : UIComponent( pos, S_DEFAULT_SIZE )
        , m_animation( anim )
        , m_playing( true )
    {
        SetSize( m_animation->GetSize() );
    }

    UIAnimationPlayer::~UIAnimationPlayer()
    {
        delete m_animation;
    }

    void UIAnimationPlayer::Load( const Claw::XmlIt* xmlConfig )
    {
    }

    void UIAnimationPlayer::Update( float dt )
    {
        if( m_playing && m_animation )
        {
            m_animation->UpdateAnim( dt );
        }
    }

    void UIAnimationPlayer::Render( Claw::Surface* target )
    {
        if( m_animation )
        {
            Claw::Vector2f pos( (float)GetViewPos().m_x, (float)GetViewPos().m_y );
            m_animation->RenderAnim( target, pos );
        }
    }

    bool UIAnimationPlayer::Play( unsigned int frameIdx, unsigned int frameEnd, bool loop )
    {
        bool ret = m_animation->Play( frameIdx, frameEnd, loop );
        m_playing = true;
        return ret;
    }

    bool UIAnimationPlayer::Pause()
    {
        if( !m_playing )
            return false;

        m_playing = false;
        return true;
    }

    bool UIAnimationPlayer::SetCurrentFrame( unsigned int frameIdx )
    {
        CLAW_ASSERT( m_animation );
        if( m_animation )
            return m_animation->SeekFrame( frameIdx );
        return false;
    }

    bool UIAnimationPlayer::SetCurrentTime( float time )
    {
        CLAW_ASSERT( m_animation );
        if( m_animation )
            return m_animation->SeekTime( time );
        return false;
    }

    UIAnimationPlayer::UIAnimation::~UIAnimation()
    {
    }

    float UIAnimationPlayer::UIAnimation::GetCurrentTime() const
    {
        return GetFrameRate() * (float)GetCurrentFrame();
    }

    bool UIAnimationPlayer::UIAnimation::SeekTime( float time )
    {
        CLAW_ASSERT( time >= 0.f );

        return SeekFrame( static_cast<unsigned int>( GetFrameRate() * time ) );
    }

} // namespace ClawExt
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIPanel.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_PANEL_HPP__
#define __UI_UI_PANEL_HPP__

// Internal includes
#include "claw_ext/ui/UIContainer.hpp"

// External includes

// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    //! Simplest container that collects and renders items in the adding order.
    class UIPanel : public UIContainer
    {
    public:
        //! Default panel constructor. Position is initialized to 0,0
                            UIPanel();

        //! Constructor defining panel position.
                            UIPanel( const Pos& pos );

        //! Constructor defining panel position and dimensions.
                            UIPanel( const Pos& pos, const Size& size );

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Refresh panel boundaries and sub-items alignment/positioning.
        virtual void        Invalidate();

    protected:
        Pos                 CalcAlignOffset( const UIComponent* component ) const;

    }; // class UIPanel

} // namespace ClawExt

#endif // !defined __UI_UI_PANEL_HPP__
// EOF

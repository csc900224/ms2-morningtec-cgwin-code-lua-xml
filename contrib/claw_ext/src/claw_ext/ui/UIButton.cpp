//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIButton.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIButton.hpp"
#include "claw_ext/ui/UILabel.hpp"
#include "claw_ext/ui/UIFrame.hpp"
#include "claw_ext/ui/UIPanel.hpp"

// External includes
#include "claw/graphics/Surface.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    UIButton::UIButton( const Pos& pos, const Size& size, const Claw::NarrowString& fontXmlPath, const Claw::Color clr /* = Claw::Color( 0, 255, 0 ) */ )
        : UIPanel( pos, size )
    {
        m_frame = new UIFrame( UIFrame::Pos( 0, 0 ), size, clr );
        //m_frame->SetAlignHoriz( UILabel::AH_CENTER );
        //m_frame->SetAlignVert( UILabel::AV_CENTER );
        m_label = new UILabel( UILabel::Pos( 0, 0 ), fontXmlPath );
        m_label->SetAlignHoriz( UILabel::AH_CENTER );
        m_label->SetAlignVert( UILabel::AV_CENTER );

        // This will also Invalidate component via UIContainer::AddComponent method.
        AddComponent( m_frame );
        AddComponent( m_label );
    }

    void UIButton::Load( const Claw::XmlIt* xmlConfig )
    {

    }

    void UIButton::SetText( const Claw::String& text )
    {
        m_label->SetText( text );

        // Update frame size and boundaries if necessary
        Size size( m_frame->GetSize() );
        if( size.m_x < m_label->GetSize().m_x )
        {
            size.m_x = m_label->GetSize().m_x;

            m_frame->SetSize( size );
            SetSize( size );

            UIPanel::Invalidate();
        }
    }

    void UIButton::SetTextColor( const Claw::Color& clr )
    {
        m_label->SetColor( clr );
    }

    void UIButton::SetBgColor( const Claw::Color& clr )
    {
        m_frame->SetColor( clr );
    }

    void UIButton::SetFrameBorder( const UIFrame::Border* border )
    {
        m_frame->SetBorder( border );
    }

    void UIButton::LoadFrameBorder( const UIFrame::BorderSettings* sett )
    {
        m_frame->LoadBorder( sett );
    }

} // namespace ClawExt
// EOF

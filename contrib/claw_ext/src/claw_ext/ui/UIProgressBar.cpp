//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIProgressBar.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIProgressBar.hpp"

// External includes
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/base/Xml.hpp"

#define F( a )  (float)( (a) )

namespace ClawExt
{
    // Some helper static constants
    static const float          ANGLE( 0.f );
    static const float          SCALE( 1.f );
    static const Claw::Point2f  PIVOT( 0.f, 0.f );

    UIProgressBar::UIProgressBar( const Pos& pos, Claw::Color clr /* = Claw::Color( 255, 0, 0 ) */ )
        : UIComponent( pos )
        , m_orientation( BO_HORIZ )
        , m_progress( 0.f )
        , m_progColor( clr )
        , m_progStripe( NULL )
        , m_progUseColoring( false )
        , m_bgFill( false )
        , m_bgColor( Claw::Color( 0, 0, 0 ) )
        , m_bgStripe( NULL )
        , m_bgUseColoring( false )
        , m_fgFill( false )
        , m_fgColor( Claw::Color( 0, 0, 0 ) )
        , m_fgStripe( NULL )
        , m_fgUseColoring( false )
    {
    }

    UIProgressBar::UIProgressBar( const Pos& pos, const Size& size, const Claw::Color clr /* = Claw::Color( 255, 0, 0 ) */ )
        : UIComponent( pos, size )
        , m_orientation( BO_HORIZ )
        , m_progress( 0.f )
        , m_progColor( clr )
        , m_progStripe( NULL )
        , m_progUseColoring( false )
        , m_bgFill( false )
        , m_bgColor( Claw::Color( 0, 0, 0 ) )
        , m_bgStripe( NULL )
        , m_bgUseColoring( false )
        , m_fgFill( false )
        , m_fgColor( Claw::Color( 0, 0, 0 ) )
        , m_fgStripe( NULL )
        , m_fgUseColoring( false )
    {

    }

    void UIProgressBar::Load( const Claw::XmlIt* xmlConfig )
    {

    }

    void UIProgressBar::Render( Claw::Surface* target )
    {
        RenderBg( target );

        RenderProgress( target );

        RenderFg( target );
    }

    void UIProgressBar::SetColor( const Claw::Color& clr )
    {
        m_progColor = clr;
    }

    void UIProgressBar::SetBgColor( const Claw::Color& clr )
    {
        m_bgColor = clr;
    }

    void UIProgressBar::SetFgColor( const Claw::Color& clr )
    {
        m_fgColor = clr;
    }

    void UIProgressBar::SetOrientation( BarOrientation orient )
    {
        m_orientation = orient;
    }

    void UIProgressBar::SetBgFill( bool enable )
    {
        m_bgFill = enable;
    }

    void UIProgressBar::SetFgFill( bool enable )
    {
        m_fgFill = enable;
    }

    void UIProgressBar::SetProgress( float progress )
    {
        CLAW_ASSERT( progress >= 0.f );
        CLAW_ASSERT( progress <= 1.f );

        m_progress = progress;
    }

    void UIProgressBar::LoadStripe( const char* gfxPath, bool useColoring /* = false */ )
    {
        if( gfxPath )
        {
            Claw::SurfacePtr gfx = Claw::AssetDict::Get< Claw::Surface >( gfxPath );
            CLAW_ASSERT( gfx );
            SetStripe( gfx, useColoring );
        }
        else
        {
            SetStripe( Claw::SurfacePtr( NULL ), useColoring );
        }
    }

    void UIProgressBar::LoadBgStripe( const char* gfxPath, bool useColoring /* = false */ )
    {
        if( gfxPath )
        {
            Claw::SurfacePtr gfx = Claw::AssetDict::Get< Claw::Surface >( gfxPath );
            CLAW_ASSERT( gfx );
            SetBgStripe( gfx, useColoring );
        }
        else
        {
            SetBgStripe( Claw::SurfacePtr( NULL ), useColoring );
        }
    }

    void UIProgressBar::LoadFgStripe( const char* gfxPath, bool useColoring /* = false */ )
    {
        if( gfxPath )
        {
            Claw::SurfacePtr gfx = Claw::AssetDict::Get< Claw::Surface >( gfxPath );
            CLAW_ASSERT( gfx );
            SetFgStripe( gfx, useColoring );
        }
        else
        {
            SetFgStripe( Claw::SurfacePtr( NULL ), useColoring );
        }
    }

    void UIProgressBar::SetStripe( const Claw::SurfacePtr& gfxSurf, bool useColoring /* =false */ )
    {
        Claw::PixelData* pd = gfxSurf->GetPixelData();

        m_progStripe = gfxSurf;
        m_progUseColoring = useColoring;

        if( m_progStripe )
        {
            Size size = GetSize();
            Size expSize( m_progStripe->GetWidth(), m_progStripe->GetHeight() );
            if( m_bgStripe )
            {
                expSize.m_x = Claw::Max( expSize.m_x, m_bgStripe->GetWidth() );
                expSize.m_y = Claw::Max( expSize.m_y, m_bgStripe->GetHeight() );
            }
            if( m_fgStripe )
            {
                expSize.m_x = Claw::Max( expSize.m_x, m_fgStripe->GetWidth() );
                expSize.m_y = Claw::Max( expSize.m_y, m_fgStripe->GetHeight() );
            }
            if( GetOrientation() == BO_HORIZ )
                SetSize( Size( size.m_x, expSize.m_y ) );
            else
                SetSize( Size( expSize.m_x, size.m_y ) );
        }
    }

    void UIProgressBar::SetBgStripe( const Claw::SurfacePtr& gfxSurf, bool useColoring /* = false */ )
    {
        Claw::PixelData* pd = gfxSurf->GetPixelData();

        m_bgStripe = gfxSurf;
        m_bgUseColoring = useColoring;

        if( m_bgStripe )
        {
            Size size = GetSize();
            Size expSize( m_bgStripe->GetWidth(), m_bgStripe->GetHeight() );
            if( m_progStripe )
            {
                expSize.m_x = Claw::Max( expSize.m_x, m_progStripe->GetWidth() );
                expSize.m_y = Claw::Max( expSize.m_y, m_progStripe->GetHeight() );
            }
            if( m_fgStripe )
            {
                expSize.m_x = Claw::Max( expSize.m_x, m_fgStripe->GetWidth() );
                expSize.m_y = Claw::Max( expSize.m_y, m_fgStripe->GetHeight() );
            }
            if( GetOrientation() == BO_HORIZ )
                SetSize( Size( size.m_x, expSize.m_y ) );
            else
                SetSize( Size( expSize.m_x, size.m_y ) );
        }
    }

    void UIProgressBar::SetFgStripe( const Claw::SurfacePtr& gfxSurf, bool useColoring /* =false */ )
    {
        Claw::PixelData* pd = gfxSurf->GetPixelData();

        m_fgStripe = gfxSurf;
        m_fgUseColoring = useColoring;

        if( m_fgStripe )
        {
            Size size = GetSize();
            Size expSize( m_fgStripe->GetWidth(), m_fgStripe->GetHeight() );
            if( m_progStripe )
            {
                expSize.m_x = Claw::Max( expSize.m_x, m_progStripe->GetWidth() );
                expSize.m_y = Claw::Max( expSize.m_y, m_progStripe->GetHeight() );
            }
            if( m_bgStripe )
            {
                expSize.m_x = Claw::Max( expSize.m_x, m_bgStripe->GetWidth() );
                expSize.m_y = Claw::Max( expSize.m_y, m_bgStripe->GetHeight() );
            }
            if( GetOrientation() == BO_HORIZ )
                SetSize( Size( size.m_x, expSize.m_y ) );
            else
                SetSize( Size( expSize.m_x, size.m_y ) );
        }
    }

    void UIProgressBar::RenderProgress( Claw::Surface* target )
    {
        RelPos pos( (float)GetViewPos().m_x, (float)GetViewPos().m_y );
        RelSize size( (float)GetSize().m_x, (float)GetSize().m_y );

        // Progress stripe should be centered withing the bar area
        // calculate align offset and maximum width/height that may be filled with rect
        float align = 0.f;
        if( m_progStripe )
        {
            if( m_orientation == BO_HORIZ )
            {
                align = (float)( size.m_y - m_progStripe->GetHeight() );
                pos.m_y += align * 0.5f;
                size.m_y -= align;
            }
            else
            {
                align = (float)( size.m_x - m_progStripe->GetWidth() );
                pos.m_x += align * 0.5f;
                size.m_x -= align;
            }
        }

        if( !m_progStripe )
        {
            if( m_orientation == BO_HORIZ )
            {
                target->DrawFilledRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x * m_progress, pos.m_y + size.m_y, m_progColor );
            }
            else
            {
                target->DrawFilledRectangle( pos.m_x, pos.m_y, pos.m_x + size.m_x, pos.m_y + size.m_y * m_progress, m_progColor );
            }
        }
        else
        {
            if( m_orientation == BO_HORIZ )
            {
                RenderStripeH( target, m_progStripe, pos.m_x, pos.m_y, size.m_x * m_progress, m_progUseColoring, m_progColor );
            }
            else
            {
                RenderStripeV( target, m_progStripe, pos.m_x, pos.m_y, size.m_y * m_progress, m_progUseColoring, m_progColor );
            }
        }
    }

    void UIProgressBar::RenderBg( Claw::Surface* target )
    {
        const Pos& pos = GetViewPos();
        const Size& size = GetSize();

        if( m_bgFill )
        {
            if( m_orientation == BO_HORIZ )
            {
                target->DrawFilledRectangle( F(pos.m_x), F(pos.m_y), F(pos.m_x + size.m_x), F(pos.m_y + size.m_y), m_bgColor );
            }
            else
            {
                target->DrawFilledRectangle( F(pos.m_x), F(pos.m_y), F(pos.m_x + size.m_x), F(pos.m_y + size.m_y), m_bgColor );
            }
        }

        if( m_bgStripe )
        {
            if( m_orientation == BO_HORIZ )
            {
                RenderStripeH( target, m_bgStripe, F(pos.m_x), F(pos.m_y), F(size.m_x), m_bgUseColoring, m_bgColor );
            }
            else
            {
                RenderStripeV( target, m_bgStripe, F(pos.m_x), F(pos.m_y), F(size.m_y), m_bgUseColoring, m_bgColor );
            }
        }
    }

    void UIProgressBar::RenderFg( Claw::Surface* target )
    {
        const Pos& pos = GetViewPos();
        const Size& size = GetSize();

        if( m_fgFill )
        {
            if( m_orientation == BO_HORIZ )
            {
                target->DrawFilledRectangle( F(pos.m_x), F(pos.m_y), F(pos.m_x + size.m_x), F(pos.m_y + size.m_y), m_fgColor );
            }
            else
            {
                target->DrawFilledRectangle( F(pos.m_x), F(pos.m_y), F(pos.m_x + size.m_x), F(pos.m_y + size.m_y), m_fgColor );
            }
        }

        if( m_fgStripe )
        {
            if( m_orientation == BO_HORIZ )
            {
                RenderStripeH( target, m_fgStripe, F(pos.m_x), F(pos.m_y), F(size.m_x), m_fgUseColoring, m_fgColor );
            }
            else
            {
                RenderStripeV( target, m_fgStripe, F(pos.m_x), F(pos.m_y), F(size.m_y), m_fgUseColoring, m_fgColor );
            }
        }
    }

    void UIProgressBar::RenderStripeH( Claw::Surface* target, Claw::Surface* src, float x, float y, float w, bool colorBlend, const Claw::Color& c, bool flip /* = false */ ) const
    {
        using namespace Claw;

        TriangleEngine::FlipMode f = TriangleEngine::FM_NONE;
        Rect clipRect( 0, 0, src->GetWidth(), src->GetHeight() );
        if( !flip )
        {
            while( w > src->GetWidth() )
            {
                if( colorBlend )
                    TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
                else
                    target->Blit( x, y, src );

                w -= src->GetWidth();
                x += src->GetWidth();
            }

            clipRect.m_w = (int)w;
            clipRect.m_h = (int)src->GetHeight();

            if( colorBlend )
                TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
            else
                target->Blit( x, y, src, clipRect );
        }
        else
        {
            f = TriangleEngine::FM_VERTICAL;

            while( w > src->GetWidth() )
            {
                if( colorBlend )
                    TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
                else
                    TriangleEngine::Blit( target, src, x, y, ANGLE, SCALE, PIVOT, f );

                w -= src->GetWidth();
                x += src->GetWidth();
            }

            clipRect.m_w = (int)w;
            clipRect.m_h = (int)src->GetHeight();

            if( colorBlend )
                TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
            else
                TriangleEngine::Blit( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect );
        }
    }

    void UIProgressBar::RenderStripeV( Claw::Surface* target, Claw::Surface* src, float x, float y, float h, bool colorBlend, const Claw::Color& c, bool flip /* = false */ ) const
    {
        using namespace Claw;

        TriangleEngine::FlipMode f = TriangleEngine::FM_NONE;
        Rect clipRect( 0, 0, src->GetWidth(), src->GetHeight() );
        if( !flip )
        {
            while( h > src->GetHeight() )
            {
                if( colorBlend )
                    TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
                else
                    target->Blit( x, y, src );

                h -= src->GetHeight();
                y += src->GetHeight();
            }

            clipRect.m_w = (int)src->GetWidth();
            clipRect.m_h = (int)h;

            if( colorBlend )
                TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
            else
                target->Blit( x, y, src, clipRect );
        }
        else
        {
            f = TriangleEngine::FM_HORIZONTAL;
            while( h > src->GetHeight() )
            {
                if( colorBlend )
                    TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
                else
                    TriangleEngine::Blit( target, src, x, y, ANGLE, SCALE, PIVOT, f );

                h -= src->GetHeight();
                y += src->GetHeight();
            }

            clipRect.m_w = (int)src->GetWidth();
            clipRect.m_h = (int)h;

            if( colorBlend )
                TriangleEngine::BlitAlpha( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect, c );
            else
                TriangleEngine::Blit( target, src, x, y, ANGLE, SCALE, PIVOT, f, clipRect );
        }
    }

} // namespace ClawExt
// EOF

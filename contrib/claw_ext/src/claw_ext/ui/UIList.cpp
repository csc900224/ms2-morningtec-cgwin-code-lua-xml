//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIList.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIList.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/base/Math.hpp"
#include "claw/base/Xml.hpp"

namespace ClawExt
{
    UIList::UIList()
        : UIContainer()
        , m_orientation( LO_VERTICAL )
        , m_spacing( LS_FIXED )
        , m_spacingOff( 0 )
    {
    }

    void UIList::Load( const Claw::XmlIt* xmlConfig )
    {
    }

    void UIList::AddComponent( UIComponent* component )
    {
        // TODO: Consider calling base class method - UIContainer::AddComponent

        // Register as an component parent - may be only one
        component->SetParent( this );

        // Calculate relative layout of added component
        component->CalculateRelativeLayout( this );

        // Check type of spacing used and apply it to the final relative position
        if( m_spacing == LS_FIXED )
        {
            // Calculate new item position base on list orientation
            Pos off( 0, 0 );
            SubComponentsConstIt it = m_children.begin();
            SubComponentsConstIt end = m_children.end();
            for( ; it != end; ++it )
            {
                if( m_orientation == LO_HORIZONTAL )
                    off.m_x += (*it)->GetLocalPos().m_x + (*it)->GetSize().m_x;
                else
                    off.m_y += (*it)->GetLocalPos().m_y + (*it)->GetSize().m_y;
            }

            if( m_orientation == LO_HORIZONTAL )
            {
                off.m_x += m_children.size() * m_spacingOff;
                off.m_y += CalcAlignOffset( component );
            }
            else
            {
                off.m_x += CalcAlignOffset( component );
                off.m_y += m_children.size() * m_spacingOff;
            }
            // This must be called here to fix component eventual position setup by UIContainer
            //UIContainer::AddComponent( component );
            m_children.push_back( component );

            // Setup item offset
            component->SetParentOff( off );

            // Only container boundaries need to be recalculated
            InvalidateSize();

            // Invalidate only childs
            UIContainer::Invalidate();
        }
        // m_spacing == LS_AUTO
        else
        {
            // This must be called here so Invalidate will have all items already in and
            // may fix components positioning if performed in the base class.
            //UIContainer::AddComponent( component );
            m_children.push_back( component );

            // All items need to be repositioned and size recalculated
            Invalidate();
        }
    }

    void UIList::SetOrientation( ListOrientation lo )
    {
        m_orientation = lo;

        Invalidate();
    }

    void UIList::SetSpacing( ListSpacing ls, int off /* = 0 */ )
    {
        CLAW_ASSERT( ls != LS_NUM );

        if( ls == LS_FIXED )
        {
            m_spacingOff = off;
        }
        else
        {
            m_spacingOff = 0;
        }
        m_spacing = ls;

        Invalidate();
    }

    void UIList::Invalidate()
    {
        if( m_children.empty() )
            return;

        Size volChilds = CalcChildsVolume();
        Size volCurr = GetSize();
        const int childsNum = CalcChildsVisibleNum();

        // Calculate spacing for auto spacing option
        if( m_spacing == LS_AUTO )
        {
            // Extend size if required, otherwise leave untouched and adjust spacing equally
            volCurr.m_x = Claw::Max( volChilds.m_x, volCurr.m_x );
            volCurr.m_y = Claw::Max( volChilds.m_y, volCurr.m_y );

            // Reset spacing to zero - still true for one item list
            m_spacingOff = 0;
            // More then one item, calculate spacing between items
            if( childsNum > 1 )
            {
                if( m_orientation == LO_HORIZONTAL )
                {
                    // TODO: Check what will happed if fraction will be returned
                    m_spacingOff = (volCurr.m_x - volChilds.m_x) / ( childsNum - 1 );
                }
                else
                {
                    m_spacingOff = (volCurr.m_y - volChilds.m_y ) / ( childsNum - 1 );
                }
            }
        }
        // m_spacing == LS_FIXED
        else
        {
            // Extend size to allow fixed spaces between items
            if( m_orientation == LO_HORIZONTAL )
            {
                volChilds.m_x += m_spacingOff * ( childsNum - 1 );
            }
            else
            {
                volChilds.m_y += m_spacingOff * ( childsNum - 1 );
            }
            volCurr.m_x = Claw::Max( volChilds.m_x, volCurr.m_x );
            volCurr.m_y = Claw::Max( volChilds.m_y, volCurr.m_y );
        }
        m_size = volCurr;

        // Calculate new items position base on list orientation
        Pos viewPos( GetViewPos() );
        Pos off( 0, 0 );

        SubComponentsConstIt it = m_children.begin();
        SubComponentsConstIt end = m_children.end();

        // First item without spacing
        if( m_orientation == LO_HORIZONTAL )
        {
            off.m_y += CalcAlignOffset( *it );
        }
        else
        {
            off.m_x += CalcAlignOffset( *it );
        }
        (*it)->SetParentPos( viewPos );
        (*it)->SetParentOff( off );

        // Other items position based on the previous items locations and boundaries
        SubComponentsConstIt prev = it++;
        for( ; it != end; ++it, ++prev )
        {
            if( m_orientation == LO_HORIZONTAL )
            {
                if( (*prev)->IsActive() || (*prev)->IsVisible() )
                    off.m_x += (*prev)->GetLocalPos().m_x + (*prev)->GetSize().m_x + m_spacingOff;
                off.m_y = CalcAlignOffset( *it );
            }
            else
            {
                off.m_x = CalcAlignOffset( *it );
                if( (*prev)->IsActive() || (*prev)->IsVisible() )
                    off.m_y += (*prev)->GetLocalPos().m_y + (*prev)->GetSize().m_y + m_spacingOff;
            }
            (*it)->SetParentPos( viewPos );
            (*it)->SetParentOff( off );
        }

        // Invalidate all childs
        UIContainer::Invalidate();
    }

    void UIList::InvalidateSize()
    {
        Size newSize = CalcChildsVolume();
        if( m_orientation == LO_HORIZONTAL )
        {
            newSize.m_x += m_spacingOff * ( m_children.size() - 1 );
        }
        else
        {
            newSize.m_y += m_spacingOff * ( m_children.size() - 1 );
        }
        // Allow wider list boundaries if all items may fix to them (this will enable internal items align support)
        m_size.m_x = Claw::Max( m_size.m_x, newSize.m_x );
        m_size.m_y = Claw::Max( m_size.m_y, newSize.m_y );
    }

    int UIList::CalcChildsVisibleNum()
    {
        int num = 0;
        SubComponentsConstIt it = m_children.begin();
        SubComponentsConstIt end = m_children.end();
        for( ; it != end; ++it )
        {
            if( !(*it)->IsActive() && !(*it)->IsVisible() ) continue;
            ++num;
        }
        return num;
    }

    UIList::Size UIList::CalcChildsVolume() const
    {
        SubComponentsConstIt it = m_children.begin();
        SubComponentsConstIt end = m_children.end();
        Size vol( 0, 0 );
        if( m_orientation == LO_HORIZONTAL )
        {
            for( ; it != end; ++it )
            {
                if( !(*it)->IsActive() && !(*it)->IsVisible() ) continue;
                vol.m_x += (*it)->GetLocalPos().m_x + (*it)->GetSize().m_x;
                vol.m_y = Claw::Max( vol.m_y, (*it)->GetLocalPos().m_y + (*it)->GetSize().m_y );
            }
        }
        else
        {
            for( ; it != end; ++it )
            {
                if( !(*it)->IsActive() && !(*it)->IsVisible() ) continue;
                vol.m_x = Claw::Max( vol.m_x, (*it)->GetLocalPos().m_x + (*it)->GetSize().m_x );
                vol.m_y += (*it)->GetLocalPos().m_y + (*it)->GetSize().m_y;
            }
        }
        return vol;
    }

    int UIList::CalcAlignOffset( const UIComponent* comp ) const
    {
        if( m_orientation == LO_VERTICAL )
        {
            if( comp->GetAlign().m_horiz == UIComponent::AH_RIGHT )
            {
                return GetSize().m_x - comp->GetSize().m_x;
            }
            else if( comp->GetAlign().m_horiz == UIComponent::AH_CENTER )
            {
                return (int)(GetSize().m_x / 2.f - comp->GetSize().m_x / 2.f);
            }
        }
        else if( m_orientation == LO_HORIZONTAL )
        {
            if( comp->GetAlign().m_vert == UIComponent::AV_BOTTOM )
            {
                return GetSize().m_y - comp->GetSize().m_y;
            }
            else if( comp->GetAlign().m_vert == UIComponent::AV_CENTER )
            {
                return (int)(GetSize().m_y / 2.f - comp->GetSize().m_y / 2.f);
            }
        }
        return 0;
    }
} // namespace ClawExt
// EOF

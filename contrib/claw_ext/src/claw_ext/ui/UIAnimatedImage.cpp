//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIAnimatedImage.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIAnimatedImage.hpp"

// External includes
#include "claw/base/AssetDict.hpp"
#include "claw/graphics/TriangleEngine.hpp"

namespace ClawExt
{
    static const float              S_ANGLE( 0.f );
    static const Claw::Point2f      S_PIVOT( 0.f, 0.f );

    UIAnimatedImage::UIAnimatedImage()
        : UIImage( 0 )
        , m_frameIdx( 0 )
        , m_playing( true )
    {}

    void UIAnimatedImage::Update( float dt )
    {
        if( m_playing )
        {
            m_image->Update( dt );
        }
    }

    void UIAnimatedImage::SetLooped( bool looped )
    {
        ((Claw::AnimatedSurface*)m_image.GetPtr())->SetLoop( looped );
    }

    int UIAnimatedImage::GetFrameCount() const
    {
        return ((Claw::AnimatedSurface*)m_image.GetPtr())->GetNumFrames();
    }

    void UIAnimatedImage::SetFrame( int frameIdx )
    {
        m_frameIdx = frameIdx;
        ((Claw::AnimatedSurface*)m_image.GetPtr())->SetFrame( m_frameIdx );
        
        m_imageSize.Set( m_image->GetWidth(), m_image->GetHeight() );
        SetSize( m_imageSize );
    }

    void UIAnimatedImage::Render( Claw::Surface* target )
    {
        using namespace Claw;

        const Pos& p = GetViewPos();
        const Size& compSize = GetSize();
        const Size& imgSize = GetImageSize();
        const SurfacePtr& src = m_image;

        const int offX = ((Claw::AnimatedSurface*)m_image.GetPtr())->GetRawFrame( m_frameIdx ).x;
        const int offY = ((Claw::AnimatedSurface*)m_image.GetPtr())->GetRawFrame( m_frameIdx ).y;

        if( imgSize.m_x > 0 && imgSize.m_y > 0 )
        {
            TriangleEngine::FlipMode fm = TriangleEngine::FM_NONE;
            if( (m_flipMode & FM_VERTICAL) != 0 )
                fm |= TriangleEngine::FM_VERTICAL;
            if( (m_flipMode & FM_HORIZONTAL) != 0 )
                fm |= TriangleEngine::FM_HORIZONTAL;

            Vector2f scale( (float)(compSize.m_x) / imgSize.m_x, (float)(compSize.m_y) / imgSize.m_y );

            src->SetAlpha( (int)(255 * GetFinalAlpha()) );
            TriangleEngine::Blit( target, src, (float)p.m_x - offX, (float)p.m_y - offY , S_ANGLE, scale, S_PIVOT, fm );
            src->SetAlpha( 255 );
        }
    }

} // namespace ClawExt

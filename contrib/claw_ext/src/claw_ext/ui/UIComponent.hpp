//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIComponent.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_COMPONENT_HPP__
#define __UI_UI_COMPONENT_HPP__

// Internal includes

// External includes
#include "claw/base/SmartPtr.hpp"
#include "claw/math/Vector2.hpp"

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    // Forward declarations
    class UIFunctor;
    typedef Claw::SmartPtr< UIFunctor > UIFunctorPtr;

    class UIContainer;

    //! Component for representing various user interface items.
    class UIComponent
    {
    public:
        typedef Claw::Vector2f  RelSize;
        typedef Claw::Vector2f  RelPos;

        typedef Claw::Vector2i  Size;
        typedef Claw::Vector2i  Pos;

        struct ViewRect
        {
            ViewRect( int l, int t, int r, int b )
                : left( l )
                , top( t )
                , right( r )
                , bottom( b )
            {}

            ViewRect( const Pos& position, const Size& size )
                : left( position.m_x )
                , top( position.m_y )
                , right( position.m_x + size.m_x )
                , bottom( position.m_y + size.m_y )
            {}

            int left;
            int top;
            int right;
            int bottom;
        }; // struct ViewRect

        enum AlignHoriz
        {
            AH_NONE,
            AH_LEFT,
            AH_CENTER,
            AH_RIGHT,

            AH_NUM
        }; // enum AlignHoriz

        enum AlignVert
        {
            AV_NONE,
            AV_TOP,
            AV_CENTER,
            AV_BOTTOM,

            AV_NUM
        }; // enum AlignVert

        //! Enum used to define relative layouting mechanism behaviour using SetRelativeLayout() method.
        enum RelativeLayout
        {
            RL_NONE         = 0 << 0,               //!< Use static layout (odrinary pixel perfect positioning and sizing).
            RL_WIDTH        = 1 << 0,               //!< Calculate components width relatively - as a size fraction of the component given to to CalculateRelativeLayout() method - most cases this will be a parent component.
            RL_HEIGHT       = 1 << 1,               //!< Calculate components height relatively to the parent component.
            RL_POS_X        = 1 << 2,               //!< X position will be caculated relatively to the parent component
            RL_POS_Y        = 1 << 3,               //!< Y position will be caculated relatively to the parent component

            RL_SIZE         = RL_WIDTH | RL_HEIGHT, //!< Component size will be calculated relatively to the parent component
            RL_POS          = RL_POS_X | RL_POS_Y,  //!< Component position will be calculated relatively to the parent component
            RL_ALL          = RL_SIZE  | RL_POS     //!< Both component size and position will be calculated relatively to the parent component

        }; // RelativeLayout

        struct Align
        {
                        Align()
                            : m_vert( AV_TOP )
                            , m_horiz( AH_LEFT )
                        {}

                        Align(  AlignHoriz horiz, AlignVert vert )
                            : m_vert( vert )
                            , m_horiz( horiz )
                        {}

            AlignVert   m_vert;
            AlignHoriz  m_horiz;
        }; // struct Align

        enum CursorEvent
        {
            CE_PRESS,           //!< Defines an event when user clicks/touches the cursor over the component area.
            CE_RELEASE,         //!< Release click (touch up) over component area.

            CE_ROLL_IN,         //!< Unpressed cursor has reached component area (not supported on touch devices).
            CE_ROLL_OVER,       //!< Cursor (unpressed) moves over the area of component while still keeping its boundaries (not supported on touch screens).
            CE_ROLL_OUT,        //!< Cursor (unpressed) has moved outside the component boundaries (no touch devices support).

            CE_DRAG_IN,         //!< Cursor has reached component area while beeing pressed (touch down) - only if previously was outside of it.
            CE_DRAG_OVER,       //!< Pressed cursor (touch down) moves over the component while keeping its boundaries (previously also inside component).
            CE_DRAG_OUT,        //!< Move outside component boundaries while pressed (touched down).

            CE_DRAG_OUTSIDE,    //!< Continous event dispatched when cursor moved outside componend while beeing pressed.
            CE_MOVE_OUTSIDE,    //!< Continous event dispatched when cursor moved outside component boundaries while not pressed (not supported on touch devices).
            CE_PRESS_OUTSIDE,   //!< Touch or mouse was pressed outside the component area.
            CE_RELEASE_OUTSIDE, //!< Touch or mouse was released outside the component area while previously also pressed outside its boundaries.
            CE_RELEASE_DRAG_IN, //!< Touch or mouse was released inside the component area, after beeing dragged over from outside press.
            CE_RELEASE_DRAG_OUT,//!< Touch or mouse was released outside the component area, after beeing pressed inside its boundaries.

            CE_NUM
        }; // enum CursorEvent

        enum CursorState
        {
            CS_OUTSIDE          = 0 << 0,
            CS_INSIDE           = 1 << 0,
            CS_POS_MASK         = 1 << 0,

            CS_RELEASED         = 0 << 1,
            CS_PRESSED          = 1 << 1,
            CS_PRESS_MASK       = 1 << 1,

            CS_PRESS_POS_OUT    = 0 << 2,
            CS_PRESS_POS_IN     = 1 << 2,
            CS_PRESS_POS_MASK   = 1 << 2

        }; // enum CursorState

        typedef int             CursorIdx;

        enum
        {
            CURSORS_NUM  = 10   //!< Maximum number of supported multi-touches/cursors
        };

        //! Release functors.
        virtual                 ~UIComponent();

        //! Try to load layout data from configuration xml.
        virtual void            Load( const Claw::XmlIt* xmlConfig ) = 0;

        //! Render on the top of current display buffer.
        virtual void            Render( Claw::Surface* target ) = 0;

        //! Perform component update (animation, state switches, etc.).
        virtual void            Update( float dt );

        //! Set visibility.
        inline void             SetVisible( bool visible );

        //! Check visibility.
        inline bool             IsVisible() const;

        //! Set components normalized alpha value (0-1).
        inline virtual void     SetAlpha( float alpha );

        //! Get current alpha value.
        inline float            GetAplha() const;

        //! Set active state (e.g. if comonent is enabled for cursor events).
        inline virtual void     SetActive( bool active );

        //! Check active state.
        inline bool             IsActive() const;

        //! Setup new boundaries.
        virtual void            SetSize( const Size& size );

        //! Get local component boundaries
        inline const Size&      GetSize() const;

        //! Set relative component size that will be used to calculate final size
        /*!
        * Final size will be calculated on calling CalculateRelativeLayout().
        * Normalized values must be used as size: 0 - 1. New size is calculated relatively
        * to parent container size.
        */
        inline void             SetRelativeSize( const RelSize& size );

        //! Get relative component size.
        inline const RelSize&   GetRelativeSize() const;

        //! Set maximal component size when using relative layouting. If value == 0 then there is no limit.
        inline void             SetMaxRelativeSize( const Size& size );

        //! Get current maximum relative size.
        inline const Size&      GetMaxRelativeSize() const;

        //! Set minimal component size when using relative layouting. If value == 0 then there is no limit.
        inline void             SetMinRelativeSize( const Size& size );

        //! Get current minimal realative size.
        inline const Size&      GetMinRelativeSize() const;

        //! Setup new alignment.
        void                    SetAlign( const Align& align, bool keepPos = false );

        //! Setup only horizontal alignment.
        virtual void            SetAlignHoriz( AlignHoriz align, bool keepPos = false );

        //! Setup vertical alignment exclusivelly.
        virtual void            SetAlignVert( AlignVert align, bool keepPos = false );

        //! Get alignment.
        inline const Align&     GetAlign() const;

        //! Get world/view coordinates position.
        inline Pos              GetViewPos() const;

        //! Get rectangle represending visble components boundaries.
        inline ViewRect         GetViewRect() const;

        //! Set local position related to parent container/component.
        virtual void            SetLocalPos( const Pos& pos );

        //! Get local position in relation to parent container if any or to the world origin.
        inline const Pos&       GetLocalPos() const;

        //! Set relative postition to parent container.
        /*!
        * Final position will be calculated on calling CalculateRelativeLayout().
        * Used to automaticaly calculate local postion. Normalized values must be used: 0 - 1.
        * Local position is calculated relatively (as a size fraction) to parent container size.
        * e.g. If parent has 200x300 size, defining relative pos as a value of RelPos(0.1, 0.2) 
        * will result in final LocalPos to be calculated as Pos(20, 60).
        */
        inline void             SetRelativePos( const RelPos& pos );

        //! Get relative position to parent container.
        inline const RelPos&    GetRelativePos() const;

        //! Setup parent container pointer.
        void                    SetParent( UIContainer* parent );

        //! Get parent container pointer.
        inline
        const UIContainer*      GetParent() const;

        //! Setup reference parent position
        inline void             SetParentPos( const Pos& pos );

        //! Set parent offset.
        inline void             SetParentOff( const Pos& off );

        //! Get derived parent offset.
        inline const Pos&       GetParentOff() const;

        //! Setup functor used in response to a given event.
        /*
        * Previous functor will be autoreleased.
        */
        virtual void            SetEventFunc( CursorEvent ev, UIFunctorPtr fnc );

        //! Acquire component relative cursor state bit mask.
        /*!
        * You can retreive corresponding cursor status using CursorState enumerator,
        * which may be bitwise and'ed to get information about cursor position:
        * bool cursorInside = (GetCursorState(idx) & CS_POS_MASK) != 0;
        * or mask it with CS_PRESS_MASK to get info about pressing the component:
        * bool componentPressed = (GetCursorState(idx) & CS_PRESS_MASK) != 0;
        */
        inline int              GetCursorState( CursorIdx idx ) const;

        //! Call it to inform component about new cursor position.
        virtual void            OnCursorMove( const Pos& newPos, CursorIdx idx );

        //! Call it to inform component about pressing the cursor at desired position.
        virtual void            OnCursorPress( const Pos& pos, CursorIdx idx );

        //! Call it to inform component that cursor has been released at position specified.
        virtual void            OnCursorRelease( const Pos& pos, CursorIdx idx );

        //! Method called internally to refresh components/containers content during hierarchy component attach.
        virtual void            Invalidate() {}

        //! Set flags to determine which layout parameters should be calculated relatively to parent container.
        /*!
        * \param relativeLayoutFlags is a bitfield to be initialized with RelativeLayout enum values.
        */
        inline void             SetRelativeLayout( int relativeLayoutFlags );

        //! Get current relative layout calculation flags
        inline int              GetRelativeLayout() const;

        //! Perform calcuatlion of relative layout
        /*!
        * Size and local postion will be racalculated depending on flags passed to SetRelativeLayout().
        * relativeTo is a compontent to wich our is considered to be ralative - most cases it is a pointer
        * to parent container. If NULL is passed, display boundaries are treated as parent container.
        */
        virtual void            CalculateRelativeLayout( const UIComponent* relativeTo = NULL );

    protected:
        //! Internal notification flags that allows components information exchange.
        /*!
        * Notification flags are used to support lightweight components and containers
        * update (invalidation). When some object in hierarchy is modified and this change
        * may affect other UI hierarchy nodes, internal notification messagess are dispatched to
        * underlaying and parent containers/components to inform them to update their positions,
        * sizes, relative alignments, etc.
        */
        enum ComponentNotification
        {
            CN_SIZE_CHANGED     = 0,
            CN_POS_CHANGED,

            CN_NUM
        }; // enun ComponentNotification

        //! Default constructor sets position, parent offset and dimensions to zero.
                                UIComponent();

        //! Constructor defining component position and size to zero.
                                UIComponent( const Pos& pos );

        //! Constructor defining component position and dimensions.
                                UIComponent( const Pos& pos, const Size& size );

        virtual void            ProcessEvent( CursorEvent ev, CursorIdx idx, const Pos& pos );

        virtual void            NotifyUpTree( ComponentNotification cn );

        // Declare friendship to allow below methods calls between clasess.
        friend class UIContainer;

        virtual void            ParentNotification( ComponentNotification cn, UIContainer* cont );

        //! Calculate alpha value that shuld be used for rendering (respects alpha of parent components).
        virtual float           GetFinalAlpha() const;

        Size                    m_size;
        Align                   m_align;
        bool                    m_visible;
        bool                    m_active;
        float                   m_alpha;

        Pos                     m_posLocal;
        Pos                     m_posParent;
        Pos                     m_posParentOff;

        int                     m_relativeLayoutFlags;
        RelPos                  m_posRelative;
        RelSize                 m_sizeRelative;
        Size                    m_sizeRelativeMin;
        Size                    m_sizeRelativeMax;

        int                     m_cursorState[CURSORS_NUM];

        UIFunctorPtr            m_eventFunc[CE_NUM];

        UIContainer*            m_parent;

    }; // class UIComponent


    inline void UIComponent::SetVisible( bool visible )
    {
        m_visible = visible;
    }

    inline bool UIComponent::IsVisible() const
    {
        return m_visible;
    }

    inline void  UIComponent::SetActive( bool active )
    {
        m_active = active;
    }

    inline bool  UIComponent::IsActive() const
    {
        return m_active;
    }

    inline const UIComponent::Size& UIComponent::GetSize() const
    {
        return m_size;
    }

    inline void UIComponent::SetRelativeSize( const RelSize& size )
    {
        m_sizeRelative = size;
        m_relativeLayoutFlags |= RL_SIZE;
    }

    inline const UIComponent::RelSize& UIComponent::GetRelativeSize() const
    {
        return m_sizeRelative;
    }
    
    inline const UIComponent::Align& UIComponent::GetAlign() const
    {
        return m_align;
    }

    inline UIComponent::Pos UIComponent::GetViewPos() const
    {
        return m_posLocal + m_posParent + m_posParentOff;
    }

    inline UIComponent::ViewRect UIComponent::GetViewRect() const
    {
        return ViewRect( GetViewPos(), m_size );
    }

    inline void UIComponent::SetLocalPos( const Pos& pos )
    {
        m_posLocal = pos;
    }

    inline const UIComponent::Pos& UIComponent::GetLocalPos() const
    {
        return m_posLocal;
    }

    inline void UIComponent::SetRelativePos( const RelPos& pos )
    {
        m_posRelative = pos;
        m_relativeLayoutFlags |= RL_POS;
    }

    inline const UIComponent::RelPos& UIComponent::GetRelativePos() const
    {
        return m_posRelative;
    }

    inline const UIContainer* UIComponent::GetParent() const
    {
        return m_parent;
    }

    inline void UIComponent::SetParentPos( const Pos& pos )
    {
        m_posParent = pos;
    }

    inline void UIComponent::SetParentOff( const Pos& off )
    {
        m_posParentOff = off;
    }

    inline const UIComponent::Pos& UIComponent::GetParentOff() const
    {
        return m_posParentOff;
    }

    inline int UIComponent::GetCursorState( CursorIdx idx ) const
    {
        return m_cursorState[idx];
    }

    inline void UIComponent::SetRelativeLayout( int relativeLayoutFlags )
    {
        m_relativeLayoutFlags = relativeLayoutFlags;
    }

    inline int UIComponent::GetRelativeLayout() const
    {
        return m_relativeLayoutFlags;
    }

    inline void UIComponent::SetMaxRelativeSize( const Size& size )
    {
        m_sizeRelativeMax = size;
    }

    inline const UIComponent::Size& UIComponent::GetMaxRelativeSize() const
    {
        return m_sizeRelativeMax;
    }

    inline void UIComponent::SetMinRelativeSize( const Size& size )
    {
        m_sizeRelativeMin = size;
    }

    inline const UIComponent::Size& UIComponent::GetMinRelativeSize() const
    {
        return m_sizeRelativeMin;
    }

    inline void UIComponent::SetAlpha( float alpha )
    {
        m_alpha = Claw::MinMax( alpha, 0.0f, 1.0f );
    }

    inline float UIComponent::GetAplha() const
    {
        return m_alpha;
    }

} // namespace ClawExt

#endif // !defined __UI_UI_COMPONENT_HPP__
// EOF

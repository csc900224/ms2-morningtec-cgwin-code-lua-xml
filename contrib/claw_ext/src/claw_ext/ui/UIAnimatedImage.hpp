//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIAnimatedImage.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_ANIMATED_IMAGE_HPP__
#define __UI_UI_ANIMATED_IMAGE_HPP__

// Internal includes
#include "claw_ext/ui/UIImage.hpp"

// External includes
#include "claw/graphics/AnimatedSurface.hpp"

namespace ClawExt
{
    class UIAnimatedImage : public UIImage
    {
    public:
        //! Default constructor.
                            UIAnimatedImage();

        //! Perform animation update.
        virtual void        Update( float dt );

        //! Set if animation should be looped or not.
        void                SetLooped( bool looped );

        //! Get total number of frames in animation.
        int                 GetFrameCount() const;

        //! Changes current animation frame. Component boundaries are recalculated using this frame size.
        void                SetFrame( int frameIdx );

        //! Get current animation frame index.
        int                 GetCurrentFrameIdx();

        //! Check if animation ic currently playing.
        bool                IsPlaying() const { return m_playing; }

        //! Play animation.
        void                Play() { m_playing = true; }

        //! Pause animation.
        void                Pause() { m_playing = false; }

         //! Render on the top of current display buffer.
        virtual void        Render( Claw::Surface* target );

    private:
        int                 m_frameIdx;
        bool                m_playing;

    }; // clas UIAnimatedImage

} // namespace ClawExt

#endif // __UI_UI_ANIMATED_IMAGE_HPP__

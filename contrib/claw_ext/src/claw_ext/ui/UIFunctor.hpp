//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIFunctor.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_FUNCTOR_HPP__
#define __UI_UI_FUNCTOR_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include "claw/base/SmartPtr.hpp"
#include "claw/math/Vector2.hpp"

// Forward declarations

namespace ClawExt
{
    //! Class for instancing UI actions such as onRollOver, onRollOut, etc...
    class UIFunctor : public Claw::RefCounter
    {
    public:
        typedef Claw::Vector2i  Pos;

        //! Virtual destructor for derived implementations.
        virtual         ~UIFunctor() {};

        //! Simple funtion like operator.
        inline void     operator()( UIComponent* cmp, UIComponent::CursorEvent ev, UIComponent::CursorIdx idx, const Pos& cursorPos );

        //! Wrapped implementation processing.
        virtual void    Call( UIComponent* cmp, UIComponent::CursorEvent ev, UIComponent::CursorIdx idx, const Pos& cursorPos ) = 0;

    }; // class UIFunctor

    // Common typedefs
    typedef Claw::SmartPtr< UIFunctor> UIFunctorPtr;

    inline void UIFunctor::operator()( UIComponent* cmp, UIComponent::CursorEvent e, UIComponent::CursorIdx idx, const Pos& cursorPos )
    {
        Call( cmp, e, idx, cursorPos );
    }

} // namespace ClawExt

#endif // !defined __UI_UI_FUNCTOR_HPP__
// EOF

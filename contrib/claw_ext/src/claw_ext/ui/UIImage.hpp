//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIImage.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_IMAGE_HPP__
#define __UI_UI_IMAGE_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include "claw/graphics/Surface.hpp"

// Forward declarations
namespace Claw
{
    class XmlIt;
}

namespace ClawExt
{
    //! Simple image rendering component.
    class UIImage : public UIComponent
    {
    public:
        typedef unsigned short FlipMode;
        enum _FlipMode
        {
            FM_NONE         = 0,
            FM_VERTICAL     = 1 << 0,
            FM_HORIZONTAL   = 1 << 1,
            FM_BOTH         = FM_VERTICAL | FM_HORIZONTAL
        };

        //! Constructor defining image position.
                            UIImage( const Pos& pos = 0 );

        //! Constructor defining image position and its rendering dimensions.
                            UIImage( const Pos& pos, const Size& size );

        //! Try to load layout data from configuration xml.
        virtual void        Load( const Claw::XmlIt* xmlConfig );

        //! Render on the top of current display buffer.
        virtual void        Render( Claw::Surface* target );

        //! Setup image content to be displayed.
        virtual void        SetImage( const Claw::SurfacePtr& ptr );

        //! Acquire image surface smart pointer.
        Claw::SurfacePtr    GetImage() const;

        //! Load image via AssetDict
        virtual void        LoadImg( const char* imgPath );

        //! Return containing image size or (0, 0) if no image was loaded.
        inline const Size&  GetImageSize() const;

        //! Set image fliping mode.
        inline void         SetFlipMode( FlipMode flipMode );

        //! Get current image fliping mode.
        inline FlipMode     GetFlipMode() const;

        //! Set blit color
        inline void         SetColor( const Claw::Color& color );

        //! Get current blit color
        inline const Claw::Color& GetColor() const;

    protected:
        Claw::SurfacePtr    m_image;
        Size                m_imageSize;
        FlipMode            m_flipMode;
        Claw::Color         m_color;

    }; // class UIImage

    inline const UIComponent::Size& UIImage::GetImageSize() const
    {
        return m_imageSize;
    }

    inline void UIImage::SetFlipMode( FlipMode flipMode )
    {
        m_flipMode = flipMode;
    }

    inline UIImage::FlipMode UIImage::GetFlipMode() const
    {
        return m_flipMode;
    }

    inline void UIImage::SetColor( const Claw::Color& color )
    {
        m_color = color;
    }

    inline const Claw::Color& UIImage::GetColor() const
    {
        return m_color;
    }

} // namespace ClawExt

#endif // !defined __UI_UI_IMAGE_HPP__
// EOF

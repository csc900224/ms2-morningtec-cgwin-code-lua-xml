//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIAnimationPlayer.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2013, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_ANIMATION_PLAYER_HPP__
#define __UI_UI_ANIMATION_PLAYER_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"

// External includes
#include "claw/compat/ClawTypes.hpp"

namespace ClawExt
{
    class UIAnimationPlayer : public UIComponent
    {
    public:
        //! Interface for animation container that actually displays animation.
        class UIAnimation
        {
        public:
            virtual         ~UIAnimation();

            //! Progress animation in time
            virtual void    UpdateAnim( float dt ) = 0;

            //! Perfrom rendering using underlaying animation container.
            virtual void    RenderAnim( Claw::Surface* target, const Claw::Vector2f& pos ) const = 0;

            //! Get animation frames per second.
            virtual float   GetFrameRate() const = 0;

            //! Return index of frame being played
            virtual
            unsigned int    GetFramesCount() const = 0;

            //! Return index of frame being played
            virtual
            unsigned int    GetCurrentFrame() const = 0;

            //! Return animation playback time.
            float           GetCurrentTime() const;

            //! Start animation playback in the frame range, definie if looping should be used.
            virtual bool    Play( unsigned int frameIdx, unsigned int frameEnd, bool loop = false ) = 0;

            //! Move to desired frame.
            virtual bool    SeekFrame( unsigned int frameIdx ) = 0;

            //! Move to specified time.
            bool            SeekTime( float time );

            //! Get actual animation bounding rectangle size.
            virtual
            const Size&     GetSize() const = 0;

        }; // class UIAnimation

        //! Default constructor.
                            UIAnimationPlayer( const Pos& pos, UIAnimation* anim );

        //! Release animation.
                            ~UIAnimationPlayer();

        //! Perform xml load.
        void                Load( const Claw::XmlIt* xmlConfig );

        //! Perform animation update.
        virtual void        Update( float dt );

        //! Perfrom rendering using underlaying animation container.
        virtual void        Render( Claw::Surface* target );

        //! Play animation.
        bool                Play( unsigned int frameIdx, unsigned int frameEnd, bool loop = false );

        //! Pause animation.
        bool                Pause();

        //! Changes current animation frame. Component boundaries are recalculated using this frame size.
        bool                SetCurrentFrame( unsigned int frameIdx );

        //! Get current animation frame index.
        inline
        unsigned int        GetCurrentFrame() const;

        //! Get total number of frames in animation.
        inline
        unsigned int        GetFramesCount() const;

        //! Setup playback position in time domain.
        bool                SetCurrentTime( float time );

        //! Get playback time position.
        inline float        GetCurrentTime() const;

        //! Check if animation ic currently playing.
        inline bool         IsPlaying() const;

    private:
        UIAnimation*        m_animation;
        bool                m_playing;

    }; // clas UIAnimationPlayer

    inline bool UIAnimationPlayer::IsPlaying() const
    {
        return m_playing;
    }

    inline unsigned int UIAnimationPlayer::GetCurrentFrame() const
    {
        CLAW_ASSERT( m_animation );
        if( m_animation )
            return m_animation->GetCurrentFrame();
        else
            return 0;
    }

    inline unsigned int UIAnimationPlayer::GetFramesCount() const
    {
        CLAW_ASSERT( m_animation );
        if( m_animation )
            return m_animation->GetFramesCount();
        else
            return 0;
    }

    inline float UIAnimationPlayer::GetCurrentTime() const
    {
        CLAW_ASSERT( m_animation );
        if( m_animation )
            return m_animation->GetCurrentTime();
        else
            return 0;
    }

} // namespace ClawExt

#endif // !defined __UI_UI_ANIMATION_PLAYER_HPP__
// EOF

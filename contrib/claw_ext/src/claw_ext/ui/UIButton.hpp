//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIButton.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UI_UI_BUTTON_HPP__
#define __UI_UI_BUTTON_HPP__

// Internal includes
#include "claw_ext/ui/UIComponent.hpp"
#include "claw_ext/ui/UIPanel.hpp"
#include "claw_ext/ui/UIFrame.hpp"

// External includes
#include "claw/graphics/Color.hpp"

// Forward declarations
namespace Claw
{
    class Surface;
    class XmlIt;
}

namespace ClawExt
{
    // Forward declare internal classes
    class UILabel;

    //! Button component, with frame, and label text.
    /*!
    * Button derives from UIPanel container and thus may be extended and add some case specific elements,
    * such as icons, additional graphics etc. Simply use it as an other containers.
    */
    class UIButton : public UIPanel
    {
    public:
        //! Constructor defining button position, dimensions, font and color.
                                UIButton( const Pos& pos, const Size& size, const Claw::NarrowString& fontXmlPath, const Claw::Color clr = Claw::Color( 0, 255, 0 ) );

        //! Try to load layout data from configuration xml.
        virtual void            Load( const Claw::XmlIt* xmlConfig );

        //! Setup text being displayed.
        void                    SetText( const Claw::String& text );

        //! Setup text color.
        void                    SetTextColor( const Claw::Color& clr );

        //! Setup frame background color.
        void                    SetBgColor( const Claw::Color& clr );

        //! Setup border graphics/decorations.
        /*!
        * Simply pass NULL if you want to unload and remove border graphics from being rendered.
        */
        void                    SetFrameBorder( const UIFrame::Border* border );

        //! Load border graphics based on the border settings.
        void                    LoadFrameBorder( const UIFrame::BorderSettings* sett );

    protected:
        UILabel*                m_label;
        UIFrame*                m_frame;

    }; // class UIButton

} // namespace ClawExt

#endif // !defined __UI_UI_BUTTON_HPP__
// EOF

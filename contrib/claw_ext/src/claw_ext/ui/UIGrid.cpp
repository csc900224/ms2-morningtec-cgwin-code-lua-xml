//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/ui/UIGrid.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//                   <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/ui/UIGrid.hpp"

// Consts
static const float EPSILON = 0.00000001f;

namespace ClawExt
{
    UIGrid::UICell::UICell()
        : m_size( 0 )
        , m_minSize( 0 )
    {
        SetRelativeSize( 1 );
    }

    void UIGrid::UICell::SetFixedSize( int size )
    {
        m_sizeType = ST_FIXED;
        m_size = (float)size;
    }

    int UIGrid::UICell::GetFixedSize() const
    {
        if( m_sizeType == ST_FIXED )
        {
            return (int)m_size;
        }
        return 0;
    }

    void UIGrid::UICell::SetRelativeSize( float size )
    {
        m_sizeType = ST_RELATIVE;
        m_size = size;
    }

    float UIGrid::UICell::GetRelativeSize() const
    {
        if( m_sizeType == ST_RELATIVE )
        {
            return m_size;
        }
        return 0;
    }


    UIGrid::UIGrid( GridOrientation orientation ) 
        : m_orientation( orientation )
    {}

    UIGrid::UIGrid( GridOrientation orientation, int numberOfCells )
        : m_orientation( orientation )
    {
        SetCellsNum( numberOfCells );
    }

    void UIGrid::Load( const Claw::XmlIt* xmlConfig )
    {}

    void UIGrid::SetCellsNum( int numberOfCells )
    {
        CLAW_ASSERT( numberOfCells >= 0 );
        int cellsDiff = numberOfCells - GetCellsNum();

        if( cellsDiff )
        {
            // Add more cells
            while( cellsDiff > 0 )
            {
                AddComponent( new UICell() );
                --cellsDiff;
            }

            // Remove redundand cells
            if( cellsDiff < 0 )
            {
                for( unsigned int i = m_children.size() + cellsDiff; i < m_children.size() ; ++i )
                {
                    // Free cell memory
                    delete m_children[i];
                }

                // Remove cells
                SubComponentsIt it = m_children.end();
                std::advance( it, cellsDiff );
                m_children.erase( it, m_children.end() );
            }

            Invalidate();
        }
    }

    int UIGrid::GetCellsNum() const
    {
        return m_children.size();
    }

    UIGrid::UICell* UIGrid::GetCell( int cellId ) const
    {
        CLAW_ASSERT( cellId >= 0 && cellId < GetCellsNum() );
        if( cellId >= 0 && cellId < GetCellsNum() )
        {
            return (UICell*)m_children[cellId];
        }
        return NULL;
    }
    
    void UIGrid::AddComponentToCell( int cellId, UIComponent* component, bool invalidate /*= true*/ )
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            cell->AddComponent( component );
            if( invalidate ) Invalidate();
        }
    }

    void UIGrid::SetCellFixedSize( int cellId, int size, bool invalidate /*= true*/ )
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            cell->SetFixedSize( size );
            if( invalidate ) Invalidate();
        }
    }

    int UIGrid::GetCellFixedSize( int cellId ) const
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            return cell->GetFixedSize();
        }
        return 0;
    }
    
    void UIGrid::SetCellRelativeSize( int cellId, float size, bool invalidate /*= true*/ )
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            cell->SetRelativeSize( size );
            if( invalidate ) Invalidate();
        }
    }

    float UIGrid::GetCellRelativeSize( int cellId ) const
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            return cell->GetRelativeSize();
        }
        return 0;
    }

    void UIGrid::SetCellMinSize( int cellId, int minSize, bool invalidate /*= true*/ )
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            return cell->SetMinSize( minSize );
            if( invalidate ) Invalidate();
        }
    }

    int UIGrid::GetCellMinSize( int cellId ) const
    {
        UICell* cell = GetCell( cellId );
        if( cell )
        {
            return cell->GetMinSize();
        }
        return 0;
    }

    void UIGrid::SetOrientation( GridOrientation orientation, bool invalidate /*= true*/ )
    {
        if( m_orientation != orientation )
        {
            m_orientation = orientation;
            if( invalidate ) Invalidate();
        }
    }

    void UIGrid::Invalidate()
    {
        SubComponentsIt it = m_children.begin();
        SubComponentsIt end = m_children.end();

        const int cellsNum = m_children.size();
        const int gridSize = m_orientation == GO_HORIZONTAL ? m_size.m_x : m_size.m_y;
        
        float relativeSize = (float)gridSize;
        int relativeCells = cellsNum;

        // Calculate fixed size "allocated" by cells
        for( ; it != end; ++it )
        {
            UICell* cell = (UICell*)*it;
            if( cell->GetSizeType() == UICell::ST_FIXED )
            {
                relativeSize -= cell->GetFixedSize();
                --relativeCells;
            }
        }

        // Calculate average relative cell size
        const int relativeCellSize = (int)(relativeSize / relativeCells + 0.5);
        int offset = 0;
        float avarageAspect = 0;
        Pos viewPos( GetViewPos() );

        it = m_children.begin();
        for( int i = 1; it != end; ++it, ++i )
        {
            UICell* cell = (UICell*)*it;

            // Current cell size
            int cellSize = 0;

            if( cell->GetSizeType() == UICell::ST_RELATIVE )
            {
                // Relative cell
                cellSize = (int)(relativeCellSize * cell->GetRelativeSize());
                avarageAspect += cell->GetRelativeSize();
            }
            else
            {
                // Fixed size cell
                cellSize = cell->GetFixedSize();
            }

            // Last cell - use all space left - no matter if it was fixed size or not
            if( i == cellsNum )
            {
                cellSize = gridSize - offset;
            }

            // Clamp cell size to protect against overflows
            int min = cell->GetMinSize();
            int max = gridSize - offset;
            if( min > max )
            {
                CLAW_MSG_WARNING( false, "Overflow detected in UIGrid!" );
                std::swap( min, max );
            }
            cellSize = Claw::MinMax( cellSize, min, max);

            // Set final cell size and position
            if( m_orientation == GO_HORIZONTAL )
            {
                (*it)->SetSize( UIComponent::Size( cellSize, m_size.m_y ) );
                (*it)->SetParentPos( viewPos );
                (*it)->SetParentOff( UIComponent::Pos( offset, 0 ) );
            }
            else
            {
                (*it)->SetSize( UIComponent::Size( m_size.m_x, cellSize ) );
                (*it)->SetParentPos( viewPos );
                (*it)->SetParentOff( UIComponent::Pos( 0, offset ) );
            }

            // Move offset for next cell
            offset += cellSize;
        }

        // Validate cells aspects
        avarageAspect /= relativeCells;
        CLAW_WARNING( avarageAspect >= 1.0f - EPSILON &&  avarageAspect <= 1.0f + EPSILON );

        UIContainer::Invalidate();
    }
}

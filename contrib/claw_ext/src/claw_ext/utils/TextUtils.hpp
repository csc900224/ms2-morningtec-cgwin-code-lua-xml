#ifndef __INCLUDED__TEXTUITLS_HPP__
#define __INCLUDED__TEXTUITLS_HPP__

#include "claw/graphics/Surface.hpp"

//
// Simple utility that creates surface using provided text,
// name and size of system font. Currently supported only on iOS and Android.
//
// IMPORTANT:
// On iOS it must be called on main thread.
//
// Sample usage:
//
// Claw::SurfacePtr text( TextUtils::CreateSurfaceFromText( "Hello World!", "Georgia", 12 ) );
// ...
// target->BlitAlpha( 0, 0, Claw::MakeRGB( 255, 0, 0 ), text );
//

namespace TextUtils
{
    Claw::Surface* CreateSurfaceFromText( const char* text, const char* fontName, int fontSize );
}

#endif // __INCLUDED__TEXTUITLS_HPP__

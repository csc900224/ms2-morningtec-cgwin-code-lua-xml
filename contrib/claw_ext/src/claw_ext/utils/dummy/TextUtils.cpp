#include "claw_ext/utils/TextUtils.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/graphics/ScreenText.hpp"

namespace TextUtils
{
    Claw::Surface* CreateSurfaceFromText( const char* text, const char* fontName, int fontSize )
    {
        Claw::FontExPtr font = Claw::AssetDict::Get<Claw::FontEx>( fontName );
        Claw::ScreenTextPtr screenText( new Claw::ScreenText( font, Claw::String( text ) ) );

        Claw::Surface* surface = new Claw::Surface( screenText->GetWidth(), screenText->GetHeight(), Claw::PF_RGBA_8888 ) ;
        surface->Clear(0);
        screenText->Draw(surface,0,0);

        return surface;
    }
}

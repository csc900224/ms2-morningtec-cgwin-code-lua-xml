#include "claw_ext/utils/TextUtils.hpp"

#import <UIKit/UIKit.h>

#include "claw/graphics/pixeldata/MemPixelData.hpp"

namespace TextUtils
{
    Claw::Surface* CreateSurfaceFromText( const char* text, const char* fontName, int fontSize )
    {
        UIFont* font = [UIFont fontWithName:[NSString stringWithCString:fontName encoding:NSASCIIStringEncoding] size:fontSize];
        NSString* txt = [NSString stringWithCString:text encoding:NSUTF8StringEncoding];
        CGSize size = [txt sizeWithFont:font];

        // Allocate pixel data
        Claw::MemPixelDataPtr pixelData( new Claw::MemPixelData( size.width, size.height, Claw::PF_A_8 ) );

        // Create grayscale bitmap context
        CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceGray();
        CGContextRef contextRef = CGBitmapContextCreate( pixelData->GetData(), size.width, size.height, 8, size.width, colorSpaceRef, kCGImageAlphaNone );
        CGColorSpaceRelease( colorSpaceRef );

        // Clear context
        CGContextClearRect( contextRef, CGRectMake( 0, 0, size.width, size.height ) );

        // Set drawing color (white)
        CGContextSetGrayFillColor( contextRef, 1.0f, 1.0f );

        // Flip
        CGContextTranslateCTM( contextRef, 0.0f, size.height );
        CGContextScaleCTM( contextRef, 1.0f, -1.0f );

        // Make context current
        UIGraphicsPushContext( contextRef );

        // Draw text
        [txt drawInRect:CGRectMake( 0, 0, size.width, size.height )
               withFont:font
          lineBreakMode:NSLineBreakByWordWrapping
              alignment:NSTextAlignmentLeft];

        // Revert previous context and release local one
        UIGraphicsPopContext();
        CGContextRelease( contextRef );

        Claw::Surface* surface = new Claw::Surface( pixelData );
        return surface;
    }

}

package com.gamelion.textutils;

/**
 * This class holds global constants that are used throughout the application
 * to support TextUtils
 */
public class Consts
{
    public static final boolean DEBUG = false;
}

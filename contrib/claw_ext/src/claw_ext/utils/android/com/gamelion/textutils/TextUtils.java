package com.gamelion.textutils;

import android.util.Log;
import android.os.Looper;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.Color;
import android.graphics.Rect;

import java.lang.Math;

import com.gamelion.textutils.Consts;

public class TextUtils
{
    public static final String TAG = "TextUtils";

    public static void RenderText(final String text, final String typeFaceName, final int fontSize)
    {   
        if( Consts.DEBUG ) { Log.i( TAG, "RenderText: " + text + " typeFaceName: " + typeFaceName + " fontSize: " + fontSize ); }
        
        // Create font paint
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.WHITE);
        p.setTextSize(fontSize);
        p.setTypeface(Typeface.create(typeFaceName, Typeface.NORMAL));
        
        // Measure text
        Rect bounds = new Rect();
        p.getTextBounds(text, 0, text.length(), bounds);
        if( Consts.DEBUG ) { Log.i( TAG, "text bounds: " + bounds ); }
        
        // Create bitmap and canvas
        Bitmap textBitmap = Bitmap.createBitmap(Math.max(bounds.width(), 2), Math.max(bounds.height(), 2), Bitmap.Config.ALPHA_8);
        Canvas drawCanvas = new Canvas(textBitmap);       
        
        // Draw text
        drawCanvas.drawText(text, -bounds.left, -bounds.top, p);
        
        // Push bitmap to native cde
        OnRederFinished(textBitmap, bounds.width(), bounds.height());
    }

    private static native void OnRederFinished(Bitmap bitmap, int widht, int height);
}

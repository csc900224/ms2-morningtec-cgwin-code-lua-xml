//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      claw_ext/utils/textutils/android/TextUtils.cpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek.nijaki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "claw_ext/utils/TextUtils.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/system/android/JniAttach.hpp"
#include "claw/graphics/pixeldata/MemPixelData.hpp"

#include <android/bitmap.h>

namespace TextUtils
{
    Claw::Surface* resultSurface = NULL;

    Claw::Surface* CreateSurfaceFromText( const char* text, const char* fontName, int fontSize )
    {
        CLAW_MSG( "TextUtils::CreateSurfaceFromText()" );

        JNIEnv* env;
        bool attached = Claw::JniAttach::Attach( &env );

        jstring textString = Claw::JniAttach::GetStringFromChars( env, text );
        jstring fontNameString = Claw::JniAttach::GetStringFromChars( env, fontName );

        Claw::JniAttach::StaticVoidMethodCall( env, "com/gamelion/textutils/TextUtils", "RenderText", "(Ljava/lang/String;Ljava/lang/String;I)V", textString, fontNameString, fontSize );
    
        Claw::JniAttach::ReleaseString( env, textString );
        Claw::JniAttach::ReleaseString( env, fontNameString );

        Claw::JniAttach::Detach( attached );

        return resultSurface;
    }
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_textutils_TextUtils_OnRederFinished( JNIEnv* env, jclass clazz, jobject bitmap, jint width, jint height )
    {
        CLAW_MSG( "TextUtils::OnRederFinished()" );

        AndroidBitmapInfo   info;
        void*               pixels;
        int                 ret;

        // Get bitmap info
        if ((ret = AndroidBitmap_getInfo(env, bitmap, &info)) < 0) {
            CLAW_MSG_ASSERT( false, "AndroidBitmap_getInfo() failed ! error=" << ret );
            return;
        }

        // Check bitmap format
        if (info.format != ANDROID_BITMAP_FORMAT_A_8  ) {
            CLAW_MSG_ASSERT( false, "Bitmap format is not A_8!" ) ;
            return;
        }

        // Get bitmap data
        if ((ret = AndroidBitmap_lockPixels(env, bitmap, &pixels)) < 0) {
            CLAW_MSG_ASSERT( false, "AndroidBitmap_lockPixels() failed ! error=" << ret );
            return;
        }

        // Allocate claw pixel data
        Claw::MemPixelDataPtr pixelData( new Claw::MemPixelData( width, height, Claw::PF_A_8 ) );

        // Copy data form java bitmap
        memcpy( pixelData->GetData(), pixels, pixelData->m_height * pixelData->m_pitch );

        // Release java bitmap pixels
        AndroidBitmap_unlockPixels(env, bitmap);

        // Create output surface
        TextUtils::resultSurface = new Claw::Surface( pixelData );
    }
}
#ifndef ALERTBOX_HPP
#define ALERTBOX_HPP

#include "claw/base/String.hpp"

class AlertBox
{
public:
    typedef void (*Callback)( int buttonIndex, void* data );

    static void Show( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& positiveButtonLabel, const Claw::NarrowString& negativeButtonLabel, Callback callback = NULL, void* data = NULL );

private:
    AlertBox();
};

#endif

#include "AlertBox.hpp"

#include <sstream>

#include "claw/system/android/JniAttach.hpp"

void AlertBox::Show( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& positiveButtonLabel, const Claw::NarrowString& negativeButtonLabel, Callback callback, void* data )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass alertBoxCallbackCls = env->FindClass( "com/gamelion/AlertBoxCallback" );
    CLAW_ASSERT( alertBoxCallbackCls != NULL );

    jmethodID alertBoxCallbackMid = env->GetMethodID( alertBoxCallbackCls, "<init>", "()V" );
    CLAW_ASSERT( alertBoxCallbackMid != NULL );

    jobject alertBoxCallback = env->NewObject( alertBoxCallbackCls, alertBoxCallbackMid );
    CLAW_ASSERT( alertBoxCallback != NULL );

    // Set callback function ptr
    {
        jfieldID field = env->GetFieldID( alertBoxCallbackCls, "function", "J" );
        CLAW_ASSERT( field != NULL );

        long function = (long)callback;
        env->SetLongField( alertBoxCallback, field, function );
    }

    // Set callback data ptr
    {
        jfieldID field = env->GetFieldID( alertBoxCallbackCls, "ptr", "J" );
        CLAW_ASSERT( field != NULL );

        long ptr = (long)data;
        env->SetLongField( alertBoxCallback, field, ptr );
    }

    jstring jTitle = Claw::JniAttach::GetStringFromChars( env, title.c_str() );
    jstring jMessage = Claw::JniAttach::GetStringFromChars( env, message.c_str() );
    jstring jPositiveButtonLabel = Claw::JniAttach::GetStringFromChars( env, positiveButtonLabel.c_str() );
    jstring jNegativeButtonLabel = Claw::JniAttach::GetStringFromChars( env, negativeButtonLabel.c_str() );

    JNI_GET_STATIC_METHOD( "com/gamelion/AlertBox", "show", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/gamelion/AlertBoxCallback;)V", cls, mid );
    env->CallStaticVoidMethod( cls, mid, jTitle, jMessage, jPositiveButtonLabel, jNegativeButtonLabel, alertBoxCallback );

    Claw::JniAttach::ReleaseString( env, jTitle );
    Claw::JniAttach::ReleaseString( env, jMessage );
    Claw::JniAttach::ReleaseString( env, jPositiveButtonLabel );
    Claw::JniAttach::ReleaseString( env, jNegativeButtonLabel );

    Claw::JniAttach::Detach( isAttached );
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_AlertBox_onButton( JNIEnv* env, jclass clazz, jint button, jobject callback )
    {
        AlertBox::Callback fun = NULL;
        void* ptr = NULL;

        jclass cls = env->GetObjectClass( callback );

        // Get callback function
        {
            jfieldID field = env->GetFieldID( cls, "function", "J" );
            jlong value = env->GetLongField( callback, field );

            fun = (AlertBox::Callback)( value );
        }

        // Get callback ptr
        {
            jfieldID field = env->GetFieldID( cls, "ptr", "J" );
            jlong value = env->GetLongField( callback, field );

            ptr = (void*)( value );
        }

        if ( fun )
        {
            fun( button, ptr );
        }
    }
}

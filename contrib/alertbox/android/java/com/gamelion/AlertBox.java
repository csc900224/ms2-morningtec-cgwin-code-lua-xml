package com.gamelion;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.Claw.Android.ClawActivity;
import com.Claw.Android.ClawActivityCommon;

public class AlertBox
{
    public static void show( final String title, final String message, final String positiveButton, final String negativeButton, final AlertBoxCallback callback )
    {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder;
                if ( Integer.parseInt( android.os.Build.VERSION.SDK ) < 14 )
                {
                    builder = new AlertDialog.Builder( ClawActivityCommon.mActivity );
                }
                else
                {
                    builder = new AlertDialog.Builder( ClawActivityCommon.mActivity, AlertDialog.THEME_DEVICE_DEFAULT_DARK );
                }

                builder.setTitle( title );
                builder.setMessage( message );
                builder.setCancelable( false );
                builder.setPositiveButton( positiveButton, new OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int whichButton ) {
                        onButton( 0, callback );
                    }
                } );

                if ( negativeButton.length() > 0 )
                {
                    builder.setNegativeButton( negativeButton, new OnClickListener() {
                        @Override
                        public void onClick( DialogInterface dialog, int whichButton ) {
                            onButton( 1, callback );
                        }
                    } );
                }

                builder.create().show();
            }
        };

        ClawActivityCommon.mActivity.runOnUiThread( runnable );
    }

    private static native void onButton( int button, AlertBoxCallback callback );
}

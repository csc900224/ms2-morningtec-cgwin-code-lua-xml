#include "AlertBox.hpp"

#include "windows.h"
#include "claw/base/Errors.hpp"

void AlertBox::Show( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& positiveButtonLabel, const Claw::NarrowString& negativeButtonLabel, Callback callback, void* data )
{
    const int ret = MessageBox( NULL, message.c_str(), title.c_str(), MB_YESNO );
    CLAW_ASSERT( ret != 0 );

    if ( callback )
    {
        if ( ret == IDYES )
        {
            callback( 0, data );
        }
        else if ( ret == IDNO )
        {
            callback( 1, data );
        }
    }
}

#include "AlertBox.hpp"

#import "UIAlertView+Blocks.h"

void AlertBox::Show( const Claw::NarrowString& title, const Claw::NarrowString& message, const Claw::NarrowString& positiveButton, const Claw::NarrowString& negativeButton, Callback callback, void* data )
{
	RIButtonItem *positiveItem = [RIButtonItem item];
	positiveItem.label = [NSString stringWithCString:positiveButton.c_str() encoding:NSUTF8StringEncoding];
	positiveItem.action = ^
	{
		if ( callback )
		{
			callback( 0, data );
		}
	};

	UIAlertView *alertView =
		[[UIAlertView alloc] initWithTitle:[NSString stringWithCString:title.c_str() encoding:NSUTF8StringEncoding]
                                   message:[NSString stringWithCString:message.c_str() encoding:NSUTF8StringEncoding]
                          cancelButtonItem:positiveItem
                          otherButtonItems:nil];

	if ( negativeButton.length() > 0 )
	{
		RIButtonItem *negativeItem = [RIButtonItem item];
		negativeItem.label = [NSString stringWithCString:negativeButton.c_str() encoding:NSUTF8StringEncoding];
		negativeItem.action = ^
		{
			if ( callback )
			{
				callback( 1, data );
			}
		};

		[alertView addButtonItem:negativeItem];
	}

	[alertView performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
	[alertView release];
}

#######################################################################
# JUNGLE
#######################################################################

Mutliplatform set of usefull C++ classes and utillities.

Library was historically named Jungle because it was designed to contain loose and
sometimes not related classes set that would be potentially used in game projects.

Threat it as a code jungle, but do not mess here as in the real amazonian jungle :).

Feel free to comment it, fix and submit new content with a little of supervision of
one of below mentioned guys, they will be happy to hear some news from You:

- Krystian Kostecki <krystian.kostecki@game-lion.com, krystian.kostecki@gmail.com>
- Rafal Kocisz <rafal.kocisz@game-lion.com, rafal.kocisz@gmail.com>

Currently whole library may be simply "included" in you project like a BOOST lib without
any linking required, but to use mathematics and utils modules you would need just to
compile jungle/math/NumberTraits.cpp file in your project.

# EOF

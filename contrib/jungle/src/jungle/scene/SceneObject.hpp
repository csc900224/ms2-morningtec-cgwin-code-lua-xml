////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/SceneObject.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_SCENE_OBJECT_HPP__
#define __SCENE_SCENE_OBJECT_HPP__

// Internal includes
#include <jungle/scene/RenderObject.hpp>
#include <jungle/scene/CollisionObject.hpp>

namespace Jungle
{
namespace Scene
{
    template < class TBoundingVolume >
    class SceneObject : public RenderObject< TBoundingVolume >, public CollisionObject< TBoundingVolume >
    {
    public:
        typedef RenderObject< TBoundingVolume >     RenderObj;
        typedef CollisionObject< TBoundingVolume >  CollisionObj;

        virtual CollisionObj*           GetCollisionObj() = 0;
        virtual RenderObj*              GetRenderObj() = 0;

    }; // SceneObject

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE_SCENE_OBJECT_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/SceneDebug.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SCENE_DEBUG_HPP__
#define __SCENE__SCENE_DEBUG_HPP__

// Internal includes
#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
namespace Debug
{
    //! Renderer abstract class exposes interface for on platform rendering.
    /*!
    * Provide Renderer implementation and register it via Renderer::Register to allow
    * scene primitives debug drawing.
    */
    class Renderer
    {
    public:
        
        virtual void    DrawSegment( const Vector2& x0, const Vector2& v0, unsigned int color, void* user /*= 0*/ ) = 0;

        virtual void    DrawRect( const Vector2& center, const Vector2& extentX, const Vector2& extentY, unsigned int color, void* user /*= 0*/ ) = 0;

        virtual void    DrawCircle( const Vector2& pos, const Real& rad, unsigned int color, void* user /*= 0*/ ) = 0;

        //! Register renderer instance.
        /*!
        * Takes ownership over passed object, so renderer should not be deallocated before Release() call.
        * \return previosly registered renderer or NULL if nothing yet registered.
        */
        static Renderer* Register( Renderer* renderer );

        //! Release previously registered debug Renderer from beeing used for rendering.
        /*!
        * \return previously registered renderer pointer (for manual deallocation) or NULL
        * if nothing was registered yet.
        */
        static Renderer* Release();

        //! Create color type object based on the color channels.
        static Color MakeRgb( unsigned char r, unsigned char g, unsigned char b );

    }; // class Renderer

    void RenderSegment( const Vector2& x0, const Vector2& v0, unsigned int color, void* user /*= 0*/ );

    void RenderRect( const Vector2& center, const Vector2& extentX, const Vector2& extentY, unsigned int color, void* user /*= 0*/ );

    void RenderCircle( const Vector2& pos, const Real& rad, unsigned int color, void* user /*= 0*/ );

} // namespace Debug

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE__SCENE_DEBUG_HPP__

// EOF

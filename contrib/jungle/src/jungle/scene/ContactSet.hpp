////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/ContactSet.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_CONTACT_SET_HPP__
#define __SCENE_CONTACT_SET_HPP__

// Internal includes
#include <jungle/scene/ContactPoint.hpp>
#include <jungle/base/Errors.hpp>

// External includes
#include <vector>
#include <algorithm>

namespace Jungle
{
namespace Scene
{
    template < class TVector >
    class ContactSet
    {
    public:
        typedef ContactPoint< TVector >                 Point;
        typedef typename std::vector< Point >           Contacts;

        //! Flags enumerator for setting contact configuration, in the future may be extended with CG_POS flag if neccessary.
        enum ContactGather
        {
            CG_NORMAL           = 1 << 0,   //!< Store contact "surface" normal.
            CG_DISTANCE         = 1 << 1,   //!< Store contact point distance (penetration distance if negative).

            CG_FIRST_POINT      = 1 << 2,   //!< Gather first accuired contact only.
            CG_CLOSEST_POINT    = 1 << 3,   //!< Gather closest contact in the "direction" of collision line, used now for lines, rays, segments.

            CG_DEFAULT          = CG_NORMAL
        }; // enum ContactGather

                                    ContactSet()
                                    : m_config( CG_DEFAULT )                            // setup default config
                                    {
                                        JUNGLE_ASSERT_MSG( !GatherClosestOnly() || GatherDistance(), "To collect closest points only, you need to enable distance gathering too.\n" );
                                        m_contacts.reserve( GatherOneOnly() ? 1 : 2 );  // reserve space based on default configuration
                                    }

                                    ContactSet( unsigned int gatherConfig )
                                    : m_config( gatherConfig )
                                    {
                                        JUNGLE_ASSERT_MSG( !GatherClosestOnly() || GatherDistance(), "To collect closest points only, you need to enable distance gathering too.\n" );
                                        m_contacts.reserve( GatherOneOnly() ? 1 : 2 );  // in case of multiple contacts detection usualy 2 contact points are found
                                    }

        inline void                 Add( const Point& cp );

        inline const Point&         Get( unsigned int id ) const    { JUNGLE_ASSERT( id < GetSize() ); return m_contacts[id]; }

        inline void                 Clear()                         { m_contacts.clear();  }

        inline bool                 IsEmpty() const                 { return m_contacts.empty(); }

        inline size_t               GetSize() const                 { return m_contacts.size(); }

        inline bool                 GatherNormal() const            { return (m_config & CG_NORMAL) != 0; }

        //! Distance gather is always enabled if we want to collect closest points only.
        inline bool                 GatherDistance() const          { return (m_config & (CG_DISTANCE | CG_CLOSEST_POINT)) != 0; }

        inline bool                 GatherFirstOnly() const         { return (m_config & CG_FIRST_POINT) != 0; }

        inline bool                 GatherClosestOnly() const       { return (m_config & CG_CLOSEST_POINT) != 0; }

        inline void                 SortByDistance();

    private:
        //! This is private to prevent form missunderstandings.
        /*!
        * When closest point is required and implementation developer would check this as public he would probalby add,
        * only one point to the set, wherewher all pointas are required to find the closest one.
        */
        inline bool                 GatherOneOnly() const           { return (m_config & (CG_FIRST_POINT | CG_CLOSEST_POINT) ) != 0; }

        unsigned int                m_config;       //!< Sum ("ored") of ContactGather flags
        Contacts                    m_contacts;     //!< ContactPoints container

    }; // class ContactSet

    template < class TVector >
    inline void ContactSet< TVector >::Add( const Point& cp )
    {
        if( !GatherOneOnly() || IsEmpty() )
        {
            m_contacts.push_back( cp );
        }
        else if( GatherClosestOnly() )
        {
            JUNGLE_ASSERT_MSG( cp.IsDistanceValid(), "Tried to add contact point to \"closest contact\" set without valid distance parameter.\n" );

            Point& currPoint = *m_contacts.begin();
            if( currPoint.m_dist < cp.m_dist )
            {
                currPoint.m_dist = cp.m_dist;
                currPoint.m_normal = cp.m_normal;
                currPoint.m_pos = cp.m_pos;
            }
        }
    }

    template < class TVector >
    bool DistanceSort( const typename ContactSet< TVector >::Point& p0, const typename ContactSet< TVector >::Point& p1 )
    {
        return p0.m_dist < p1.m_dist;
    }

    template < class TVector >
    inline void ContactSet< TVector >::SortByDistance()
    {
        std::sort( m_contacts.begin(), m_contacts.end(), DistanceSort< TVector > );
    }

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE_CONTACT_SET_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/ContactPoint.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_CONTACT_POINT_HPP__
#define __SCENE_CONTACT_POINT_HPP__

// Internal includes
#include <jungle/base/NumberTraits.hpp>

// External includes

namespace Jungle
{
namespace Scene
{
    template < class TVector >
    class ContactPoint
    {
    public:
        inline      ContactPoint()
                    : m_pos( 0, 0 )
                    , m_normal( 0, 0 )
                    , m_dist( 0 )
        {}

        inline      ContactPoint( const TVector& rPos )
                    : m_pos( rPos )
                    , m_normal( 0, 0 )
                    , m_dist( 0 )
        {}

        inline      ContactPoint( const TVector& rPos, const TVector& rNorm )
                    : m_pos( rPos )
                    , m_normal( rNorm )
                    , m_dist( NumberTraits< float >::Maximum() )
        {}

        inline      ContactPoint( const TVector& rPos, float dist )
                    : m_pos( rPos )
                    , m_normal( 0, 0 )
                    , m_dist( dist )
        {}

        inline      ContactPoint( const TVector& rPos, const TVector& rNorm, float dist )
                    : m_pos( rPos )
                    , m_normal( rNorm )
                    , m_dist( dist )
        {}

        inline bool IsDistanceValid() const
        {
            return m_dist != NumberTraits< float >::Maximum();
        }

    public:
        TVector     m_pos;
        TVector     m_normal;
        float       m_dist;

    }; // class ContactPoint

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE_CONTACT_POINT_HPP__
// EOF

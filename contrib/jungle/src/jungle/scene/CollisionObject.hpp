////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/CollisionObject.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_COLLISION_OBJECT_HPP__
#define __SCENE_COLLISION_OBJECT_HPP__

namespace Jungle
{
namespace Scene
{
    template < class TBoundingVolume >
    class CollisionObject
    {
    protected:

        enum CollisionFlag
        {
            CF_COLLIDABLE   = (1 << 0)

        }; // enum CollisionFlag

        typedef TBoundingVolume     Bv;

    public:
        inline              CollisionObject();

        /*!
        * Checks if entity should be used to determine collisions
        * \return     true if entity should be considered in collisions calculus.
        */
        inline bool         IsCollidable() const;

        /*!
        * Determines if entity will be considered in collisions calculations.
        * \return     true if a collision should be encoutered when in contact with this entity.
        */
        inline void         SetCollidable( bool collidable );

        //! Acquire collision bounding volume geometry in world space.
        virtual const Bv*   GetCollisionWorldBV() const             = 0;

        //! Get local collision bounding volume
        /*!
        * Implementation of this method is not mandatory for overal architecture, but may be
        * required in project specific cases such as scene graph implementation.
        */
        virtual const Bv*   GetCollisionLocalBV() const             { return NULL; }

        //! Render collision geometry
        virtual void        RenderCollisionBV(void* user) const     {};

    protected:
        //! Collision info flags
        unsigned int        m_collisionFlags;

    }; // class CollisionObject

    template < class TBoundingVolume >
    inline CollisionObject< TBoundingVolume >::CollisionObject()
        : m_collisionFlags( 0 )
    {
    }

    template < class TBoundingVolume >
    inline bool CollisionObject< TBoundingVolume >::IsCollidable() const
    {
        return (m_collisionFlags & CF_COLLIDABLE) == 1;
    }

    template < class TBoundingVolume >
    inline void CollisionObject< TBoundingVolume >::SetCollidable( bool collidable )
    {
        if(collidable)
            m_collisionFlags  |= CF_COLLIDABLE;
        else
            m_collisionFlags  &= ~CF_COLLIDABLE;
    }

} // namespace Scene

} // namespace Jungle

#endif  // !defined __SCENE_COLLISION_OBJECT_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/SceneDebug.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/SceneDebug.hpp>
#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
namespace Debug
{
    static Renderer* s_renderer = 0;

    void RenderSegment( const Vector2& v0, const Vector2& v1, unsigned int color, void* user /*= 0*/ )
    {
        if( s_renderer )
            s_renderer->DrawSegment( v0, v1, color, user );
    }

    void RenderRect( const Vector2& center, const Vector2& extentX, const Vector2& extentY, unsigned int color, void* user /*= 0*/ )
    {
        if( s_renderer )
            s_renderer->DrawRect( center, extentX, extentY, color, user );
    }

    void RenderCircle( const Vector2& pos, const Real& radius, unsigned int color, void* user /*= 0*/ )
    {
        if( s_renderer )
            s_renderer->DrawCircle( pos, radius, color, user );
    }

    Renderer* Renderer::Register( Renderer* instance )
    {
        Renderer* ret = s_renderer;
        s_renderer = instance;
        return ret;
    }

    Renderer* Renderer::Release()
    {
        Renderer* ret = s_renderer;
        s_renderer = NULL;
        return ret;
    }

    /* static */
    Color Renderer::MakeRgb( unsigned char r, unsigned char g, unsigned char b )
    {
        Color rgb = (Color(r) << 16) | (Color(g) << 8) | (Color(b));
        return rgb;
    }

} // namespace Debug

} // namespace Scene

} // namespace Jungle

// EOF

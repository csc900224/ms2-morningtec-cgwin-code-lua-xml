////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/CollisionQuery.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_COLLISION_QUERY_HPP__
#define __SCENE_COLLISION_QUERY_HPP__

// Internal includes
#include <jungle/scene/ContactSet.hpp>
#include <jungle/base/Errors.hpp>

// External includes
#include <vector>

namespace Jungle
{
namespace Scene
{
    /*
    * Need to know
    class TCollisionObject;
    class TBoundingVolume;
    class TRay;
    class TRay::Vector;
    */

    //! Helper class template for storing collision query results.
    template< class TCollisionObject, class TBoundingVolume >
    class CollisionCache
    {
    public:
        typedef TBoundingVolume                 Bv;
        typedef TCollisionObject                Collider;
        typedef std::vector<TCollisionObject*>  CollidersList;

        //! Add bounding volume to queried collides
        inline void         Add( Collider* pCollider )      { m_colliders.push_back(pCollider); }

        //! Clear collision cache
        inline void         Clear()                         { m_colliders.clear(); }

        //! Return pointer to collided objects vector
        inline
        CollidersList*      GetColliders()                  { return &m_colliders; }

    private:
        //! Collision query result
        CollidersList       m_colliders;

    }; // class CollisionCache

    //! Class template for quering collision of specified BoundingVolume type with CollisionObjects collection.
    template < class TCollisionObject, class TBoundingVolume >
    class CollisionQuery
    {
    public:
        typedef TBoundingVolume                                         Bv;
        typedef TCollisionObject                                        Collider;
        typedef CollisionCache< TCollisionObject, TBoundingVolume >     Cache;
        typedef typename Cache::CollidersList                           Colliders;

        //! Defult constructor, initialize data.
                            CollisionQuery();

        //! Set collision bounding volume being queried
        inline void         SetQueryBV(const Bv* pBV)               { InitQuery(); m_pBV = pBV; }

        //! Get colliding bounding volume
        inline const Bv*    GetQueryBV() const                      { return m_pBV; }

        //! Get cashed collision data
        inline Cache*       GetCache()                              { return &m_pCache; }

        //! Get colliding objects vector pointer
        inline Colliders*   GetColliders()                          { return m_pCache.GetColliders(); }

        //! Initialize new query
        inline void         InitQuery()                             { m_pCache.Clear(); }

    private:
        //! Collision bounding volume
        const Bv*           m_pBV;
        //! Collision cache
        Cache               m_pCache;

    }; // class CollisionQuery

    template < class TCollisionObject, class TBoundingVolume, class TRay >
    class RayTraceQuery
    {
    public:
        // Derive vector type from TRay specification
        typedef TRay                                                    Ray;
        typedef typename TRay::Vector                                   Vector;
        typedef CollisionCache< TCollisionObject, TBoundingVolume >     Cache;
        typedef ContactSet< Vector >                                    Contact;
        typedef typename std::vector< Contact >                         Contacts;
        typedef TCollisionObject                                        Collider;
        typedef typename Cache::CollidersList                           Colliders;

        enum QueryConfig
        {
            QC_RECEIVE_ALL_CONTACTS    = 0,
            QC_RECEIVE_FIRST_ONLY,
            QC_RECEIVE_CLOSEST_ONLY,

            QC_NUM
        }; // enum QueryConfig

        //! By default query all collisions, do not gather contact points.
        /*!
        * You may use configuration parameter to retreive only the first contact (QC_RECEIVE_ALL_CONTACTS)
        * which will speed up calcualtions, query all collision (QC_RECEIVE_FIRST_ONLY) or even
        * request searching for the closest collision volume (QC_RECEIVE_CLOSEST_ONLY).
        * Query constructed this way will not receive collision points unless you specify QC_RECEIVE_CLOSEST_ONLY
        * mode which forces collision distances calculation.
        */
                            RayTraceQuery( QueryConfig queryConfig = QC_RECEIVE_ALL_CONTACTS );

        //! Setup query configuration and enable/disable contact point gathering.
        /*!
        * If you choose to gatherContacts (store contact points) by default all contact points will be stored
        * for each collision detected, but you may specify contact points gathering configuration mask which
        * allows to retreive the fist point only (CG_FIRST_POINT) or even the closest one (CG_CLOSEST_POINT).
        * If you configure the query to store search for the closest colliders (QC_RECEIVE_CLOSEST_ONLY) note
        * that contact points with distances will be automatically calculated, altrought both points for colliding
        * volume will be stored (if line/ray intersect it in two places), it is wise to use QC_RECEIVE_CLOSEST_ONLY
        * query config with CG_CLOSEST_POINT if you don't bother, this way only one collision point will be stored
        * for the closest collider.
        * \param queryConfig enumerator specifing if we want to search for all colliders, only the first or the closest
        * \param gatherContacts boolean flag that controls contact points calculation, note that in QC_RECEIVE_CLOSEST_ONLY mode
        * contact points (their distances) must be calculated anyway
        * \param gatherConfig contact points gather configuration \see ContactSet::ContactGather, you may specify the
        * bit mask to retreive contacts normals (CG_NORMAL), distances (CG_DISTANCE), retreive only first encountered intersection
        * (CG_FIRST_POINT) or the cosest point only (CG_CLOSEST_POINT).
        */
                            RayTraceQuery( QueryConfig queryConfig, bool gatherContacts, unsigned int gatherConfig = 0 );

        //! Set collision ray being traced
        inline void         SetQueryRay( const Ray* pRay)           { InitQuery(); m_pRay = pRay; }

        //! Get tracing ray
        inline const Ray*   GetQueryRay() const                     { return m_pRay; }

        //! Get cashed collision data
        inline Cache*       GetCache()                              { return &m_pCache; }

        //! Get intersected objects' vector pointer
        inline Colliders*   GetColliders()                          { return m_pCache.GetColliders(); }

        //! Get intersected objects' vector pointer
        inline
        const Contacts*     GetContacts()                           { return &m_contacts; }

        //! Initialize new query
        inline void         InitQuery()                             { m_pCache.Clear(); m_contacts.clear(); }

        //! Setup base query mode.
        /*!
        * Query configuration implicates if we want to receive all collisions( QC_RECEIVE_ALL_CONTACT),
        * first only (QC_RECEIVE_CLOSEST_ONLY) or the closest collider (QC_RECEIVE_CLOSEST_ONLY),
        * note that in this variant contact points will be automatically calculated with their distances.
        * \param config query configuration enumerator.
        */
        inline void         SetQueryConfig( QueryConfig config );

        //! Acquire query configuration.
        inline QueryConfig  GetQueryConfig() const                  { return m_queryConfig; }

        //! Setup contact points calculation config.
        /*!
        * Enable/disable contact points gathering and setup calculated contact points properties via bit flags:
        * normals (CG_NORMAL), distances (CG_DISTANCE). Decide if to store all intersection points
        * (no need to add bit flags to the mast), or the first encountered (or flags with CG_FIRST_POINT) or
        * the closest intersetion with each volume (CG_CLOSEST_POINT).
        */
        inline void         SetContactsGather( bool enable, unsigned int flags );

        //! Check if query should store contact points.
        inline bool         IsContactsGather() const                { return m_contactsGather; }

        //! Return contact points gathering configuration
        /*!
        * \see SetContactsGather for more info.
        */
        unsigned int        GetContactsGatherConfig() const         { return m_contactsGatherConfig; }

        //! Check if we should only test collisions until the first collision encountered.
        inline bool         ReceiveFirstOnly() const                { return (m_queryConfig == QC_RECEIVE_FIRST_ONLY); }

        //! Check if we should collect only closest collision.
        inline bool         ReceiveClosestOnly() const                { return (m_queryConfig == QC_RECEIVE_CLOSEST_ONLY); }

        //! Add single collider and contact set
        inline void         Add( Collider* o, const Contact* cont );

        //! Add single collider, method allowed to call when contact gather is disabled
        inline void         Add( Collider* o )                      { JUNGLE_ASSERT( !IsContactsGather() ); GetCache()->Add( o ); }

    private:
        //! Ray being traced for intersection
        const Ray*          m_pRay;
        //! Collision cache
        Cache               m_pCache;
        //! Contacts list
        Contacts            m_contacts;
        //! Query configuration enumerator
        QueryConfig         m_queryConfig;
        //! Flag indicating if ContactSet (collision points) should be calculated and gathered.
        bool                m_contactsGather;
        //! ContactSet configuration mask, it controlls how contact point are stored and calculated.
        unsigned int        m_contactsGatherConfig;

    }; // class RayTraceQuery

    template < class TCollisionObject, class TBoundingVolume, class TRay >
    inline void RayTraceQuery< TCollisionObject, TBoundingVolume, TRay >::Add( Collider* o, const Contact* cont )
    {
        JUNGLE_ASSERT( IsContactsGather() == (cont != NULL) );

        if( !IsContactsGather() )
        {
            if( GetCache()->GetColliders()->empty() || !ReceiveFirstOnly() )
            {
                GetCache()->Add( o );
            }
        }
        else
        {
            JUNGLE_ASSERT( !cont->IsEmpty() );

            // First point always goes in
            if( m_contacts.empty() )
            {
                GetCache()->Add( o );
                m_contacts.push_back( *cont );
            }
            else
            {
                // Store only closest contact
                if( ReceiveClosestOnly() )
                {
                    JUNGLE_ASSERT( cont->GatherDistance() );
                    JUNGLE_ASSERT( cont->Get(0).IsDistanceValid() );

                    // Compare distances and swap the only contact set if new is closer
                    int closestPointIdx = 0;
                    if( cont->GetSize() == 2 )
                    {
                        // Find the closest point in the set
                        closestPointIdx = cont->Get( 0 ).m_dist < cont->Get( 1 ).m_dist ? 0 : 1;
                    }
                    // Check if closest point in the new set is closer then the last one
                    if( m_contacts[0].Get(0).m_dist > cont->Get( closestPointIdx ).m_dist )
                    {
                        // Swap ContactSet
                        m_contacts[0] = *cont;
                        // Swap collider
                        GetCache()->Clear();
                        GetCache()->Add( o );

                        // If second point was closer, sort them to get the proper order for further comparisons
                        if( closestPointIdx != 0 )
                        {
                            m_contacts[0].SortByDistance();
                        }
                    }
                }
                // Store point if multiple points gather enabled
                else if( !ReceiveFirstOnly() )
                {
                    m_contacts.push_back( *cont );
                    GetCache()->Add( o );
                }
            }
        }
    }

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/CollisionQuery.inl>

#endif // !defined __SCENE_COLLISION_QUERY_HPP__
// EOF

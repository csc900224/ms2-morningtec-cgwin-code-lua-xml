////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/SceneTypes.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SCENE_TYPES_HPP__
#define __SCENE__SCENE_TYPES_HPP__

// Internal includes
#include <jungle/scene/ContactSet.hpp>

// External includes - other modules
#include <jungle/math/Vector2.hpp>
#include <jungle/math/Vector3.hpp>

/*!
* File provided to easily change base types for Scene namespace.
* This allow to extract only Scene Jungle submodule without need to use other modules
* and allows to detach thoose dependencies.
* \note Currently we still have some Jungle::Math dependencies (Mul, Div, Sin, Cos functions).
*/
namespace Jungle
{
namespace Scene
{
    typedef float                   Real;
    typedef unsigned int            Color;

    typedef Math::Vector2f          Vector2;
    typedef ContactSet<Vector2>     ContactSet2;
    typedef ContactPoint<Vector2>   ContactPoint2;

    typedef Math::Vector3f          Vector3;
    typedef ContactSet<Vector3>     ContactSet3;
    typedef ContactPoint<Vector3>   ContactPoint3;

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE__SCENE_TYPES_HPP__
// EOF

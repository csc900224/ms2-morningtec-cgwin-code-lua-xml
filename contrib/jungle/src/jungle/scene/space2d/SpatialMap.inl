////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/SpatialMap.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Krystian Kostecki. All rights reserved.
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>

// External includes
#include <algorithm>        // std::find

#define SHMAP_XMASK         (0xFFFF0000)
#define SHMAP_YMASK         (0x0000FFFF)

#define SHMAP_XSHIFT        16
#define SHMAP_YSHIFT        0

namespace Jungle
{
namespace Scene
{
    template < class TEntity >
    bool SpatialMap< TEntity >::IsEmpty() const
    {
        // No buckets fast exit
        if( m_map.empty() )
            return true;

        typename BucketsMap::const_iterator it = m_map.begin();
        typename BucketsMap::const_iterator end = m_map.end();
        for( ; it != end; ++it )
        {
            // Check if bucket has some entities: Bucket::IsEmpty()
            if( !it->second.IsEmpty() )
                return false;
        }
        return true;
    }

    template < class TEntity >
    void SpatialMap< TEntity >::SetCellSize( SMReal cellSize )
    {
        JUNGLE_ASSERT_MSG( cellSize > NumberTraits< SMReal >::ZERO, "Cell size must be always positive number.\n" );
        JUNGLE_ASSERT_MSG( NumberTraits< SMReal >::Maximum() / cellSize > NumberTraits< BucketId >::Epsilon(), "Cell size too small to generate all possible world buckets.\n" );

        SetCellSizeImpl( cellSize );

        // Rebuild whole map if not empty
        if( !IsEmpty() )
            Rebuild();
    }

    template < class TEntity >
    SMReal SpatialMap< TEntity >::GetCellSize() const
    {
        return m_cellSize;
    }

    template < class TEntity >
    typename SpatialMap< TEntity >::CellId SpatialMap< TEntity >::GetMaxCells() const
    {
        return static_cast<CellId>(NumberTraits< SMReal >::Maximum() / m_cellSize);
    }

    template < class TEntity >
    void SpatialMap< TEntity >::Clear()
    {
        m_map.clear();
    }

    template < class TEntity >
    unsigned int SpatialMap< TEntity >::GetCellsNum() const
    {
        return m_map.size();
    }

    template < class TEntity >
    unsigned int SpatialMap< TEntity >::GetEntitiesNum() const
    {
        return m_entities.size();
    }

    template < class TEntity >
    void SpatialMap< TEntity >::SetCellSizeImpl( SMReal cellSize )
    {
        JUNGLE_ASSERT( cellSize > 0 );

        m_cellSize = cellSize;
        // We use ceil functions just for ease of debugging.
        // This way map will always offset with integer values (1, 2,..) while ceil size will accrue
        // with multiply of half value (N * 0.5)
        m_growthOffset = Math::Ceil( cellSize );
        m_growthScale = Math::Ceil( cellSize ) / 2;
    }

    template < class TEntity >
    inline typename SpatialMap< TEntity >::Bucket& SpatialMap< TEntity >::GetBucket( const CellId& id )
    {
        return m_map[id];
    }

    template < class TEntity >
    inline typename SpatialMap< TEntity >::Bucket& SpatialMap< TEntity >::GetBucket( const SMVector& pos )
    {
        return m_map[ GetCellId( pos ) ];
    }

    template < class TEntity >
    typename SpatialMap< TEntity >::CellId SpatialMap< TEntity >::GetCellId( const SMVector& pos ) const
    {
        return GetCellId( pos.x, pos.y );
    }

    template < class TEntity >
    typename SpatialMap< TEntity >::CellId SpatialMap< TEntity >::GetCellId( const SMReal& x, const SMReal& y ) const
    {
        CellId cell = (CellId)Math::Floor( (x + m_mapOffset.x) / m_cellSize);
        cell <<= SHMAP_XSHIFT;
        cell |= ( (CellId)Math::Floor( (y + m_mapOffset.y) / m_cellSize) & SHMAP_YMASK );
        return cell;
    }

    template < class TEntity >
    SMVector SpatialMap< TEntity >::GetCellPos( const CellId& cellId ) const
    {
        SMVector pos;
        pos.x = (cellId >> SHMAP_XSHIFT) * m_cellSize - m_mapOffset.x;
        pos.y = (cellId & SHMAP_YMASK) * m_cellSize - m_mapOffset.y;
        return pos;
    }

    template < class TEntity >
    typename SpatialMap< TEntity >::CellId SpatialMap< TEntity >::GetCellX( const SMVector& pos ) const
    {
        return (CellId)ceil((float)pos.m_x / m_cellSize);
    }

    template < class TEntity >
    typename SpatialMap< TEntity >::CellId SpatialMap< TEntity >::GetCellY( const SMVector& pos ) const
    {
        return (CellId)ceil((float)pos.m_x / m_cellSize);
    }

    template < class TEntity >
    typename SpatialMap< TEntity >::CellId SpatialMap< TEntity >::GetNeighbourCellId( CellId cellId, int xCeilOff, int yCeilOff ) const
    {
        cellId += xCeilOff << SHMAP_XSHIFT;
        cellId += yCeilOff;
        return cellId;
    }

} // namespace Scene

} // namespace Jungle

// EOF

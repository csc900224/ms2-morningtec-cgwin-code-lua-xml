////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/QuadTreeConfig.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SPACE2D__QUAD_TREE_CONFIG_HPP__
#define __SCENE__SPACE2D__QUAD_TREE_CONFIG_HPP__

#include <jungle/scene/SceneTypes.hpp>

#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/AbstractLine2.hpp>

/*!
* QuadTree configuration file. Contains settings and swithes that allows
* quad tree processing and rendering control. Enable/disable debug functions here.
*/
namespace Jungle
{
namespace Scene
{

    #define _DEBUG_QUAD_TREE_RENDER_ENTITY_BV       0

    // QuadTree used types definitions.
    typedef Real            QTReal;
    typedef Vector2         QTVector;

    typedef BoundingArea2   QTBoundingVolume;
    typedef AABB2           QTAABB;
    typedef AbstractLine2   QTRay;
    typedef QTAABB          QTViewFrustum;

    //! Maximum depth of quad tree nodes
    static
    const unsigned int      QT_NODES_MAX_DEPTH = 10;

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE__SPACE2D__QUAD_TREE_CONFIG_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/PotentialMapConfig.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SPACE2D__POTENTIAL_MAP_CONFIG_HPP__
#define __SCENE__SPACE2D__POTENTIAL_MAP_CONFIG_HPP__

#include <jungle/scene/SceneTypes.hpp>

/*!
* PotentialMap configuration file. Contains base types settings and debug swithes that allows
* case specific module integration and rendering control. Enable/disable debug functions here.
*/
namespace Jungle
{
namespace Scene
{

    // PotentialMap used types definitions.
    typedef Real            PMReal;
    typedef Vector2         PMVector;

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE__SPACE2D__POTENTIAL_MAP_CONFIG_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/PotentialMap.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Krystian Kostecki. All rights reserved.
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>

// External includes

namespace Jungle
{
namespace Scene
{
    /*
    template < class TValue >
    void PotentialMap< TValue >::SetFieldSize( PMReal cellSize )
    {
        JUNGLE_ASSERT_MSG( cellSize > NumberTraits< PMReal >::ZERO, "Field size must be always positive number.\n" );
        JUNGLE_ASSERT_MSG( NumberTraits< PMReal >::Maximum() / cellSize > NumberTraits< BucketId >::Epsilon(), "Field size too small to generate all possible world buckets.\n" );

        SetFieldSizeImpl( cellSize );

        // Rebuild whole map if not empty
        if( !IsEmpty() )
            Rebuild();
    }
    */

    template < class TValue >
    PMReal PotentialMap< TValue >::GetFieldSize() const
    {
        return m_fieldSize;
    }

    template < class TValue >
    void PotentialMap< TValue >::Clear()
    {
        m_fields->clear();
    }

    template < class TValue >
    void PotentialMap< TValue >::SetOffset( const PMVector& mapOffset )
    {
        m_mapOffset = mapOffset;
    }

    template < class TValue >
    typename PotentialMap< TValue >::FieldId PotentialMap< TValue >::GetFieldsNum() const
    {
        return m_fields->size();
    }

    template < class TValue >
    typename PotentialMap< TValue >::PMValue& PotentialMap< TValue >::GetField( const PMVector& pos )
    {
        JUNGLE_ASSERT( pos.x >= m_mapOffset.x && pos.y >= m_mapOffset.y );
        JUNGLE_ASSERT( pos.x - m_mapOffset.x < m_mapSize.x && pos.y - m_mapOffset.y < m_mapSize.y );
        return m_fields->operator[]( GetFieldId( pos.x, pos.y ) );
    }

    template < class TValue >
    const TValue& PotentialMap< TValue >::GetField( const PMVector& pos ) const
    {
        JUNGLE_ASSERT( pos.x >= m_mapOffset.x && pos.y >= m_mapOffset.y );
        JUNGLE_ASSERT( pos.x - m_mapOffset.x < m_mapSize.x && pos.y - m_mapOffset.y < m_mapSize.y );
        return m_fields->operator[]( GetFieldId( pos.x, pos.y ) );
    }

    template < class TValue >
    typename PotentialMap< TValue >::FieldId PotentialMap< TValue >::GetFieldId( const PMVector& pos ) const
    {
        return GetFieldId( pos.x, pos.y );
    }

    template < class TValue >
    typename PotentialMap< TValue >::FieldId PotentialMap< TValue >::GetFieldId( const PMReal& x, const PMReal& y ) const
    {
        FieldId fieldAddr = (FieldId)Math::Floor( (y - m_mapOffset.y) / m_fieldSize);
        fieldAddr *= m_fieldsDim[0];
        fieldAddr += (FieldId)Math::Floor( (x - m_mapOffset.x) / m_fieldSize );
        return fieldAddr;
    }

    template < class TValue >
    PMVector PotentialMap< TValue >::GetFieldPos( const FieldId& fieldId ) const
    {
        PMVector pos;
        FieldId row = Math::Floor(fieldId / m_fieldsDim[0]);
        FieldId col = fieldId - row * m_fieldsDim[0];

        pos.y = row * m_fieldSize + m_mapOffset.x;
        pos.x = col * m_fieldSize + m_mapOffset.y;
        return pos;
    }

    template < class TValue >
    typename PotentialMap< TValue >::FieldId PotentialMap< TValue >::GetNeighbourFieldId( FieldId fieldId, int xOff, int yOff ) const
    {
        fieldId += xOff;
        fieldId += yOff * m_fieldsDim[0];
        return fieldId;
    }

} // namespace Scene

} // namespace Jungle

// EOF

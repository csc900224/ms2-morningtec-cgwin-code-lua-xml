////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/SpatialMapConfig.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_SPATIAL_MAP_CONFIG_HPP__
#define __SCENE_SPATIAL_MAP_CONFIG_HPP__

#include <jungle/math/Vector2.hpp>

#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>

#include <jungle/scene/SceneObject.hpp>

namespace Jungle
{
namespace Scene
{
    typedef float                               SMReal;
    typedef Math::Vector2<SMReal>               SMVector;

    typedef BoundingArea2                       SMBoundingVolume;
    typedef AABB2                               SMAABB;
    typedef AABB2                               SMViewFrustum;

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE_SPATIAL_MAP_CONFIG_HPP__
// EOF

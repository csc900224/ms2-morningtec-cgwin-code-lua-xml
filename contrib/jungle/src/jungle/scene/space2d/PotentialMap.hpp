////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/PotentalMap.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Krystian Kostecki. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SPACE2D_POTENTIAL_MAP_HPP__
#define __SCENE__SPACE2D_POTENTIAL_MAP_HPP__

// Internal includes
#include <jungle/base/Errors.hpp>
#include <jungle/scene/space2d/PotentialMapConfig.hpp>

// External includes

namespace Jungle
{
namespace Scene
{
    template < class TValue >
    class PotentialMap
    {
    public:
        // Forward declare internal classes
        class Visitor;
        class VisitorKernel5;
        class VisitorKernel9;

        typedef void    (*RenderFieldFunc)(const TValue& v, const PMVector& pos, const PMVector& dim, void* userData);

        typedef void    (*AttenutationFunc)(const TValue& v, void* userData);

        //! This determines maximum number of the fields beeing possible to manage.
        typedef unsigned long       FieldId;

        typedef TValue              PMValue;

                        PotentialMap( const PMVector& dimensions, PMReal fieldSize );

                        ~PotentialMap();

        //! Setup new single cell size.
        inline void     SetFieldSize( PMReal size );

        //! Acquire current cell size.
        inline PMReal   GetFieldSize() const;

        //! Reset all potientials.
        inline void     Clear();

        //! Setup world map offset.
        inline void     SetOffset( const PMVector& mapOffset );

        //! Get total number of cells.
        inline FieldId  GetFieldsNum() const;

        //! Reset potential in a field under specified position.
        void            SetPotential( const PMVector& pos, const PMValue& val );

        //! Get current potential in a field under specified position.
        const PMValue&  GetPotential( const PMVector& pos ) const;

        //! Get potential value for modification
        PMValue&        AcquirePotential( const PMVector& pos );

        //! Accumulate potential in a given field.
        void            ApplyPotential( const PMVector& pos, const PMValue& val );

        //! Attenuate all fields by adding the value passed to the function.
        void            AttenuateAdd( const PMValue& dimmingComp, const PMValue& min, const PMValue& max );

        //! Attenuate all fields multipling them by the value passed to the function.
        void            AttenuateMul( const PMValue& dimmingFactor );

        //! Attenuate all fields using function.
        void            AttenuateFunc( AttenutationFunc pfnFunc, void* user );

        //! Smooth using 4 neighbours and current value kernel.
        template < class TWeight >
        void            Smooth( const TWeight weights[5] );

        //! Perform visit thrue all potential fields with user defined visitor.
        void            Process( Visitor* visitor, void* user );

        //! Process all potential fields using 5 elements kernel.
        /*!
        * Each field will be "visited" with special kernel visitor (VisitorKenel5).
        * This type of visitor has automatically set kernel data before each field is
        * visited. This kernel contains 5 elements which will be filled with
        * corresponding neighbourhood fields values, respectivelly north (0 element),
        * east (1 el.), west (2), center(3), south (4rd element).
        * When no field is available at the map borders, kernel will be filled with the border
        * value passed.
        */
        void            ProcessKernel5( VisitorKernel5* visitor, const PMValue& border, void* user );

        //! Process fields with 9 values kernel visitor.
        /*!
        * \see ProcessKernel9() method.
        */
        void            ProcessKernel9( VisitorKernel9* visitor, const PMValue& border, void* user );

        //! Process filter using convolution methods.
        /*
        * Filter is applied horizontically and then vertically.
        */
        void            ProcessConvolution5( VisitorKernel5* visitor, void* user );

        template< class TWeight >
        void            ProcessConvolutionNxN( TWeight* kernel, int kRows, int kCols );

        //! Render all cells potentials using rendering method provided.
        void            Render( RenderFieldFunc pfnRenderFunc, void* userData = NULL ) const;

        //! Abstract interface for visitor class.
        class Visitor
        {
        public:
            virtual         ~Visitor() {};
            virtual void    Visit( PMValue* val, void* user ) = 0;
        }; // class Visitor

        //! Extended visitor with 5 values kernel (north, west, center, east, south).
        class VisitorKernel5 : public Visitor
        {
            friend class PotentialMap;
        public:
            virtual void    Visit( PMValue* val, void* user ) = 0;
        protected:
            PMValue         m_kernel[5];
        }; // class VisitorKernel5

        //! Extended visitor with 9 parameters kernel.
        class VisitorKernel9 : public Visitor
        {
            friend class PotentialMap;
        public:
            virtual void    Visit( PMValue* val, void* user ) = 0;
        protected:
            PMValue         m_kernel[9];
        }; // class VisitorKernel9

    private:
        // Forward declare private internal class
        class FieldsMap;

        inline PMValue&     GetField( const PMVector& pos );

        inline
        const PMValue&      GetField( const PMVector& pos ) const;

        inline FieldId      GetFieldId( const PMVector& pos ) const;

        inline FieldId      GetFieldId( const PMReal& x, const PMReal& y ) const;

        inline PMVector     GetFieldPos( const FieldId& fieldId ) const;

        inline FieldId      GetNeighbourFieldId( FieldId fieldId, int xOff, int yOff ) const;

        /*
        void                VisitSurroundings( const FieldId& fieldId, Visitor* visitor, void* user ) const;
        */

        PMReal              m_fieldSize;
        FieldId             m_fieldsDim[2];

        PMVector            m_mapSize;
        PMVector            m_mapOffset;

        FieldsMap*          m_fields;

        class FieldsMap
        {
        public:
            typedef TValue              value_type;
            typedef value_type*         iterator;
            typedef const value_type*   const_iterator;
            typedef unsigned long       size_type;
            typedef unsigned long       index_type;

                                    FieldsMap( size_type size )
                                    : m_data( new TValue[ size ] )
                                    , m_size( size )
            {
                clear();
            }
                                    ~FieldsMap()
            {
                delete [] m_data;
            }

            inline iterator         begin()         { return m_data; }
            inline const_iterator   begin() const   { return m_data; }

            inline iterator         end()           { return m_data + m_size; }
            inline const_iterator   end() const     { return m_data + m_size; }

            inline void             clear()         { std::memset( m_data, 0, m_size * sizeof(value_type) ); }
            inline size_type        size() const    { return m_size; }

            inline
            value_type&             operator[](index_type idx)           { return m_data[idx]; }
            inline
            const value_type&       operator[](index_type idx) const     { return m_data[idx]; }

        private:
            TValue*                 m_data;
            const size_type         m_size;

        }; // class FieldsMap

        template < class TWeight >
        class SmoothKernel : public PotentialMap::VisitorKernel5
        {
            typedef typename PotentialMap::VisitorKernel5   base;

        public:
            SmoothKernel( const TWeight weights[5] )
                : m_weightSum( 0 )
            {
                int i = 5;
                TWeight* dst = m_weights;
                const TWeight* src = weights;
                while( i-- > 0 )
                {
                    *dst = *src++;
                    m_weightSum += *dst++;
                }
            }

            virtual void    Visit( PMValue* val, void* user )
            {
                int i = 5;
                const TWeight* w = m_weights;
                const PMValue* k = base::m_kernel;
                *val = PMValue() - PMValue();
                while( i-- > 0 )
                {
                    *val += (*w++) * (*k++);
                }
                *val /= m_weightSum;
            }

        private:
            TWeight         m_weights[5];
            TWeight         m_weightSum;
        }; // SmoothKernel

        template < class TWeight >
        class ConvolutionFilter5 : public PotentialMap::VisitorKernel5
        {
            typedef typename PotentialMap::VisitorKernel5   base;
        public:
            ConvolutionFilter5( const TWeight weights[5] )
            {
                int i = 5;
                TWeight* dst = m_weights;
                const TWeight* src = weights;
                while( i-- > 0 )
                {
                    *dst++ = *src++;
                }
            }

            virtual void    Visit( PMValue* val, void* user )
            {
                int i = 4;
                const TWeight* w = m_weights;
                const PMValue* k = base::m_kernel;
                *val = (*w++) * (*k++);
                while( i-- > 0 )
                {
                    *val += (*w++) * (*k++);
                }
            }

        private:
            TWeight         m_weights[5];
        }; // ConvolutionFilter5

    }; // class PotentialMap

    template < class TValue >
    PotentialMap< TValue >::PotentialMap( const PMVector& worldSize, PMReal fieldSize )
        : m_mapSize( worldSize )
        , m_mapOffset( 0, 0 )
        , m_fieldSize( fieldSize )
        , m_fields( NULL )
    {
        m_fieldsDim[0] = (FieldId)Math::Ceil( worldSize.x / fieldSize );
        m_fieldsDim[1] = (FieldId)Math::Ceil( worldSize.y / fieldSize );
        m_fields = new FieldsMap( (typename FieldsMap::size_type)(m_fieldsDim[0] * m_fieldsDim[1]) );
    }

    template < class TValue >
    PotentialMap< TValue >::~PotentialMap()
    {
        delete m_fields;
    }

    template < class TValue >
    void PotentialMap< TValue >::SetPotential( const PMVector& pos, const PMValue& val )
    {
        GetField( pos ) = val;
    }

    template < class TValue >
    const TValue& PotentialMap< TValue >::GetPotential( const PMVector& pos ) const
    {
        return GetField( pos );
    }

    template < class TValue >
    TValue& PotentialMap< TValue >::AcquirePotential( const PMVector& pos )
    {
        return GetField( pos );
    }

    template < class TValue >
    void PotentialMap< TValue >::ApplyPotential( const PMVector& pos, const PMValue& val )
    {
        GetField( pos ) += val;
    }

    template < class TValue >
    void PotentialMap< TValue >::AttenuateAdd( const PMValue& dimmingComp, const PMValue& min, const PMValue& max )
    {
        typename FieldsMap::iterator it = m_fields->begin();
        typename FieldsMap::iterator end = m_fields->end();
        for( ; it != end; ++it )
        {
            *it = Math::Min( Math::Max(*it + dimmingComp, min), max );
        }
    }

    template < class TValue >
    void PotentialMap< TValue >::AttenuateMul( const PMValue& dimmingFactor )
    {
        typename FieldsMap::iterator it = m_fields->begin();
        typename FieldsMap::iterator end = m_fields->end();
        for( ; it != end; ++it )
        {
            *it *= dimmingFactor;
        }
    }

    template < class TValue >
    void PotentialMap< TValue >::AttenuateFunc( AttenutationFunc pfnFunc, void* user )
    {
        typename FieldsMap::iterator it = m_fields->begin();
        typename FieldsMap::iterator end = m_fields->end();
        for( ; it != end; ++it )
        {
            pfnFunc( *it, user );
        }
    }

    template < class TValue >
    void PotentialMap< TValue >::Render( RenderFieldFunc pfnRenderFunc, void* userData /* = NULL */ ) const
    {
        typename FieldsMap::const_iterator it = m_fields->begin();
        typename FieldsMap::const_iterator end = m_fields->end();
        PMVector pos = m_mapOffset;
        PMVector rectSize( GetFieldSize(), GetFieldSize() );
        FieldId x, y;
        for( x = 0, y = 0; it != end; ++it )
        {
            pfnRenderFunc( *it, pos, rectSize, userData );

            if( ++x < m_fieldsDim[0] )
            {
                pos.x += GetFieldSize();
            }
            else
            {
                pos.x = m_mapOffset.x;
                pos.y += GetFieldSize();
                x = 0;
                ++y;
            }
        }
    }

    template < class TValue >
    template < class TWeight >
    void PotentialMap< TValue >::Smooth( const TWeight weights[5] )
    {
        ConvolutionFilter5< TWeight > smoothingKernel( weights );

        ProcessConvolution5( &smoothingKernel, NULL );
    }

    template < class TValue >
    void PotentialMap< TValue >::Process( Visitor* visitor, void* user )
    {
        typename FieldsMap::const_iterator it = m_fields->begin();
        typename FieldsMap::const_iterator end = m_fields->end();

        for( ; it != end; ++it )
        {
            visitor->Visit( *it, user );
        }
    }

    template < class TValue >
    void PotentialMap< TValue >::ProcessKernel5( VisitorKernel5* visitor, const PMValue& border, void* user )
    {
        typename FieldsMap::iterator it = m_fields->begin();
        typename FieldsMap::iterator end = m_fields->end();
        FieldId x, y;
        for( x = 0, y = 0; it != end; ++it )
        {
            visitor->m_kernel[0] = (y == 0) ? border : *(it - m_fieldsDim[0]);                  // north
            visitor->m_kernel[1] = (x == 0) ? border : *(it - 1);                               // west
            visitor->m_kernel[2] = *it;                                                         // center
            visitor->m_kernel[3] = (x == m_fieldsDim[0] - 1) ? border : *(it + 1);              // east
            visitor->m_kernel[4] = (y == m_fieldsDim[1] - 1) ? border : *(it + m_fieldsDim[0]); // south

            visitor->Visit( it, user );

            if( ++x < m_fieldsDim[0] )
            {
                ;
            }
            else
            {
                x = 0;
                ++y;
            }
        }
    }

    template < class TValue >
    void PotentialMap< TValue >::ProcessKernel9( VisitorKernel9* visitor, const PMValue& border, void* user )
    {
        typename FieldsMap::iterator it = m_fields->begin();
        typename FieldsMap::iterator end = m_fields->end();
        FieldId x, y;
        for( x = 0, y = 0; it != end; ++it )
        {
            visitor->m_kernel[0] = (y == 0 || x == 0)                   ? border : *(it - m_fieldsDim[0] - 1);  // north-west
            visitor->m_kernel[1] = (y == 0)                             ? border : *(it - m_fieldsDim[0]);      // north
            visitor->m_kernel[2] = (y == 0 || x == m_fieldsDim[0] - 1)   ? border : *(it - m_fieldsDim[0] + 1);  // north-east

            visitor->m_kernel[3] = (x == 0)                             ? border : *(it - 1);                   // west
            visitor->m_kernel[4] = *it;                                                                         // center
            visitor->m_kernel[5] = (x == m_fieldsDim[0] - 1)             ? border : *(it + 1);                  // east

            visitor->m_kernel[6] = (y == m_fieldsDim[1] - 1 || x == 0)   ? border : *(it + m_fieldsDim[0] - 1); // south-west
            visitor->m_kernel[7] = (y == m_fieldsDim[1] - 1)             ? border : *(it + m_fieldsDim[0]);     // south
            visitor->m_kernel[8] = (y == m_fieldsDim[1] - 1 || x == m_fieldsDim[0] - 1) ?
                                                                          border : *(it + m_fieldsDim[0] - 1);  // south-east

            visitor->Visit( it, user );

            if( ++x < m_fieldsDim[0] )
            {
                ;
            }
            else
            {
                x = 0;
                ++y;
            }
        }
    }

    template < class TValue >
    void PotentialMap< TValue >::ProcessConvolution5( VisitorKernel5* visitor, void* user )
    {
        typename FieldsMap::iterator it = m_fields->begin();
        typename FieldsMap::iterator end = m_fields->end();
        FieldId x, y;
        // Horizontal filtering
        for( x = 0, y = 0; it != end; ++it )
        {
            visitor->m_kernel[0] = (x < 2) ? *it : *(it - 2);
            visitor->m_kernel[1] = (x < 1) ? *it : *(it - 1);
            visitor->m_kernel[2] = *it;
            visitor->m_kernel[3] = (x > m_fieldsDim[0] - 2) ? *it : *(it + 1);
            visitor->m_kernel[4] = (x > m_fieldsDim[0] - 3) ? *it : *(it + 2);

            visitor->Visit( it, user );

            if( ++x < m_fieldsDim[0] )
            {
                ;
            }
            else
            {
                x = 0;
            }
        }
        // Vertical filtering
        for( x = 0, y = 0; it != end; ++it )
        {
            visitor->m_kernel[0] = (y < 2) ? *it : *(it - m_fieldsDim[0] * 2);
            visitor->m_kernel[1] = (y < 1) ? *it : *(it - m_fieldsDim[0]);
            visitor->m_kernel[2] = *it;
            visitor->m_kernel[3] = (y > m_fieldsDim[1] - 2) ? *it : *(it + m_fieldsDim[0]);
            visitor->m_kernel[4] = (y > m_fieldsDim[1] - 3) ? *it : *(it + m_fieldsDim[0] * 2);

            visitor->Visit( it, user );

            if( ++x < m_fieldsDim[0] )
            {
                ;
            }
            else
            {
                x = 0;
                ++y;
            }
        }
    }

    template < class TValue >
    template < class TWeight >
    void PotentialMap< TValue >::ProcessConvolutionNxN( TWeight* kernel, int kRows, int kCols )
    {
        // find center position of kernel (half of kernel size)
        const int kCenterX = kCols / 2;
        const int kCenterY = kRows / 2;
        const int rows = m_fieldsDim[1];
        const int cols = m_fieldsDim[0];
        int inR, inC;   // input field row and column
        int outR, outC; // output signal coordinates
        int kR, kC;     // kernel row and column indexes
        TValue sum;

        //m_backbuffer->clear();

        // Output rows
        for( outR = 0; outR < rows; ++outR )
        {
            // Output columns
            for( outC = 0; outC < cols; ++outC )
            {
                sum = TValue() - TValue();

                // Kernel rows
                for( kR = 0 ; kR < kRows; ++kR )
                {
                    // Kernel columns
                    for( kC = 0; kC < kCols; ++kC )
                    {
                        // index of input signal, used for checking boundary
                        inR = outR + (kR - kCenterY);
                        inC = outC + (kC - kCenterX);

                        // ignore input samples which are out of bound
                        if( inR >= 0 && inR < rows && inC >= 0 && inC < cols )
                            //(*m_backbuffer)[outR * cols + outC]
                            sum += (*m_fields)[inR * cols + inC] * kernel[kR * kCols + kC];
                    }
                }
                (*m_fields)[outR * cols + outC] = sum;
            }
        }
        // Swap backbuffer fields map with current one
        //FieldsMap* tmp = m_fields;
        //m_fields = m_backbuffer;
        //m_backbuffer = tmp;
    }

    /*
    template < class TValue >
    void PotentialMap< TValue >::VisitSurroundings( const CellId& cellId, Visitor* visitor, void* user ) const
    {
        VisitEntities( GetNeighbourCellId( cellId, -1,  0 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId, -1, -1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  0, -1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  1, -1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  1,  0 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  1,  1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  0,  1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId, -1,  1 ), visitor, user );
    }

    template < class TValue >
    PotentialMap< TValue >::GatherVisitor::GatherVisitor( FieldsList* list )
        : m_list( list )
        , m_count( 0 )
    {}

    template < class TValue >
    void PotentialMap< TValue >::GatherVisitor::Visit( PMPotential* val, void* user )
    {
        m_list->push_back( val );
        ++m_count;
    }
    */
} // namespace Scene

} // namespace Jungle

#include <jungle/scene/space2d/PotentialMap.inl>

#endif // !defined __SCENE__SPACE2D_POTENTIAL_MAP_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/SpatialMap.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Krystian Kostecki. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SPACE2D_SPATIAL_MAP_HPP__
#define __SCENE__SPACE2D_SPATIAL_MAP_HPP__

// Internal includes
#include <jungle/scene/space2d/SpatialMapConfig.hpp>
#include <jungle/base/Errors.hpp>

// External includes
#include <vector>
#include <map>

#define SPATIAL_MAP_CACHE_BUCKETS 1

namespace Jungle
{
namespace Scene
{
    template < class TEntity >
    class SpatialMap
    {
    public:
        typedef unsigned long long  CellId;
        typedef TEntity             SMEntity;

        typedef SMAABB  (*AABBProvider)(const SMEntity* entity);

        typedef void    (*RenderFunc)(const SMEntity* entity, void* userData);

        typedef void    (*CollisionCallback)(SMEntity* ent0, SMEntity* ent1, void* userData );

        typedef void    (*IntersectionCallback)( const SMBoundingVolume* bv, SMEntity* ent, void* userData );

                        SpatialMap( AABBProvider bvProvider );

                        SpatialMap( AABBProvider bvProvider, SMReal cellSize );

        //! Check if map contains any entities.
        inline bool     IsEmpty() const;

        //! Setup new single cell size.
        inline void     SetCellSize( SMReal size );

        //! Acquire current cell size.
        inline SMReal   GetCellSize() const;

        //! Get maximum possible number of cells to be created using current configuration and cell size.
        inline CellId   GetMaxCells() const;

        //! Remove all object references.
        inline void     Clear();

        //! Get total number of occupied cells.
        inline
        unsigned int    GetCellsNum() const;

        //! Get number of registered entities.
        inline
        unsigned int    GetEntitiesNum() const;

        //! Add single entity reference to be placed in spatial map.
        void            AddEntity( SMEntity* entity );

        //! Remove entity reference without releasing object itself.
        void            RemoveEntity( SMEntity* entity );

        //! Assuming that all object pointers are still valid, recalculate their buckets/spatial hashes and rebuild space map.
        /*!
        * \param expand decides it spatial map boundaries should be exanded if some entity goes beyond min/max boundary point.
        * \note if expand param is set to false (default) and some entity exceed map boundaries this entity won't be
        * calculated in any collision tests withing this SpatialMap.
        */
        void            Rebuild( bool expand = false );

        //! Render all entities using rendering method provided.
        void            Render( RenderFunc pfnRenderFunc, void* userData = NULL ) const;

        //! Usefull for rendering occupied cells.
        void            RenderCells( void* userData = NULL ) const;

        //! Process collision of passed entity with other entities in the space.
        void            Collide( SMEntity* ent, CollisionCallback callback, void* userData = NULL ) const;

        //! Process collision between entities in the space and passed bounding volume.
        void            Intersect( const SMBoundingVolume* bv, IntersectionCallback callback, void* userData = NULL ) const;

    private:

        //! Abstract interface for visitor class.
        class Visitor
        {
        public:
            virtual void    Visit( SMEntity* entity, void* user ) = 0;
        }; // class Visitor

        //! Internal cell bucket, stores entites in the single cell.
        class Bucket
        {
        public:
            typedef std::vector<SMEntity*>              container;
            typedef typename container::iterator        iterator;
            typedef typename container::const_iterator  const_iterator;

            inline void             Push( SMEntity* entity )    { m_container.push_back( entity ); }
            inline void             Clear()                     { m_container.clear(); }
            inline size_t           GetSize() const             { return m_container.size(); }
            inline bool             IsEmpty() const             { return m_container.empty(); }

            inline iterator         Begin()                     { return m_container.begin(); }
            inline const_iterator   Begin() const               { return m_container.begin(); }

            inline iterator         End()                       { return m_container.end(); }
            inline const_iterator   End() const                 { return m_container.end(); }

            inline void             Erase( SMEntity* entity )
            {
                iterator it = std::find( m_container.begin(), m_container.end(), entity );
                if( it != m_container.end() )   m_container.erase( it );
                else                            JUNGLE_ASSERT( !"Entity should be in this bucket, check if map was rebuilded" );
            }

        private:
            container               m_container;

        }; // class SpatialMap::Bucket

        typedef CellId                              BucketId;

        typedef std::map<BucketId, Bucket>          BucketsMap;
        typedef typename BucketsMap::iterator       BucketsMapIt;
        typedef typename BucketsMap::const_iterator BucketsMapConstIt;

        typedef std::vector<SMEntity*>              Entities;
        typedef typename Entities::iterator         EntitiesIt;
        typedef typename Entities::const_iterator   EntitiesConstIt;

        bool                Invalidate( const SMAABB& bv );

        //! Internal method for setting up cell size and updating cell related values.
        inline void         SetCellSizeImpl( SMReal cellSize );

        inline Bucket&      GetBucket( const SMVector& pos );

        inline Bucket&      GetBucket( const BucketId& id );

        inline CellId       GetCellId( const SMVector& v ) const;

        inline CellId       GetCellId( const SMReal& x, const SMReal& y ) const;

        inline SMVector     GetCellPos( const CellId& cellId ) const;

        inline CellId       GetCellX( const SMVector& v ) const;

        inline CellId       GetCellY( const SMVector& v ) const;

        CellId              GetNeighbourCellId( CellId cellId, int xCeilOff, int yCeilOff ) const;

        unsigned int        GatherEntities( CellId cellId, Entities& outList ) const;

        unsigned int        GatherNeighbours( const SMVector& pos, Entities& outList ) const;

        void                VisitEntities( const CellId& cellId, Visitor* visitor, void* user ) const;

        void                VisitSurroundings( const CellId& cellId, Visitor* visitor, void* user ) const;

        class IntersectionVisitor : public Visitor
        {
        public:
                                    IntersectionVisitor( const SMBoundingVolume* bv, IntersectionCallback cb );

            virtual void            Visit( SMEntity* entity, void* user );

        private:
            const SMBoundingVolume* m_bv;
            IntersectionCallback    m_callback;

        }; // class IntersectionVisitor

        class CollisionVisitor : public Visitor
        {
        public:
                                    CollisionVisitor( SMEntity* collider, CollisionCallback cb );

            virtual void            Visit( SMEntity* entity, void* user );

        private:
            SMEntity*               m_collider;
            CollisionCallback       m_callback;

        }; // class CollisionVisitor

        class GatherVisitor : public Visitor
        {
        public:
                                GatherVisitor( Entities* list );

            virtual void        Visit( SMEntity* entity, void* user );
            inline unsigned int GetCount()  { return m_count; }
        private:
            Entities*           m_list;
            unsigned int        m_count;

        }; // class GatherVisitor

        SMReal              m_cellSize;
        SMReal              m_growthScale;
        SMReal              m_growthOffset;

        SMVector            m_mapOffset;
        BucketsMap          m_map;
        Entities            m_entities;

        AABBProvider        m_pfAABBProvider;

    }; // class SpatialMap

    template < class TEntity >
    SpatialMap< TEntity >::SpatialMap( AABBProvider bvProvider )
        : m_pfAABBProvider( bvProvider )
        , m_mapOffset( 0, 0 )
    {
        SetCellSize( NumberTraits< SMReal >::ONE );
    }

    template < class TEntity >
    SpatialMap< TEntity >::SpatialMap( AABBProvider bvProvider, SMReal cellSize )
        : m_pfAABBProvider( bvProvider )
    {
        SetCellSize( cellSize );
    }

    template < class TEntity >
    void SpatialMap< TEntity >::AddEntity( SMEntity* entity )
    {
        const SMAABB& bv = m_pfAABBProvider( entity );
        bool rebuild = Invalidate( bv );

        if( rebuild && !IsEmpty() )
            Rebuild();

        // Now we can add to entities list
        JUNGLE_ASSERT_MSG( std::find( m_entities.begin(), m_entities.end(), entity ) == m_entities.end(), "Entity already in space" );
        m_entities.push_back( entity );

        // And assign it to proper cell bucket
        Bucket& bucket = GetBucket( m_pfAABBProvider( entity ).GetCenter() );
        bucket.Push( entity );
    }

    template < class TEntity >
    void SpatialMap< TEntity >::RemoveEntity( SMEntity* entity )
    {
        if( IsEmpty() )
            return;

        // Check if on the entities list
        EntitiesIt entityIt = std::find( m_entities.begin(), m_entities.end(), entity );
        if( entityIt == m_entities.end() )
            return;

        // Remove from current bucket
        const SMAABB& bv = m_pfAABBProvider( entity );
        Bucket& bucket = GetBucket( m_pfAABBProvider( entity ).GetCenter() );
        bucket.Erase( entity );

        // Remove from list
        m_entities.erase( entityIt );
    }

    template < class TEntity >
    void SpatialMap< TEntity >::Rebuild( bool expand /* = false */ )
    {
        // We may remove all buckets and thus fully clear the map or remove entites from buckets
#if SPATIAL_MAP_CACHE_BUCKETS
        BucketsMapIt bucketIt = m_map.begin();
        BucketsMapIt bucketEnd = m_map.end();
        for( ; bucketIt != bucketEnd; ++bucketIt )
        {
            bucketIt->second.Clear();
        }
#else
        m_map.clear();
#endif

        EntitiesIt it = m_entities.begin();
        EntitiesIt end = m_entities.end();

        if( expand )
        {
            bool rebuild = false;
            for( ; it != end; ++it )
            {
                const SMAABB& bv = m_pfAABBProvider( *it );
                if( Invalidate( bv ) )
                {
                    rebuild = true;
                }
                else if( !rebuild )
                {
                    Bucket& bucket = GetBucket( bv.GetCenter() );
                    bucket.Push( *it );
                }
            }
            // Repeat full process again
            if( rebuild )
            {
                Rebuild( false );
            }
        }
        else
        {
            for( ; it != end; ++it )
            {
                const SMAABB& bv = m_pfAABBProvider( *it );

                JUNGLE_ASSERT_MSG( !Invalidate( bv ), "Entity should already fit into spatial map." );

                Bucket& bucket = GetBucket( bv.GetCenter() );
                bucket.Push( *it );
            }
        }
    }

    template < class TEntity >
    bool SpatialMap< TEntity >::Invalidate( const SMAABB& bv )
    {
        bool rebuild = false;

        // Check if entity bounding volume is not bigger then hash space cell size
        const SMVector& extents = bv.GetExtents();
        SMReal maxExtent = extents.x > extents.y ? extents.x : extents.y;

        // Expand cell size (this will rebuild spatial map) and add entity to the matching bucket
        if( GetCellSize() < maxExtent )
        {
            JUNGLE_WARNING_MSG( "[SpatialMap]::Invalidate(): Volume size bigger then cell size need to extend map cells and rebuild it." );

            SetCellSizeImpl( Math::Round( maxExtent) + m_growthScale );
            rebuild = true;
        }

        // Check if map need offseting
        const SMVector& center = bv.GetCenter();
        if( center.x < -m_mapOffset.x )
        {
            m_mapOffset.x = Math::Floor( -center.x ) + m_growthOffset;
            rebuild = true;
        }
        if( center.y < -m_mapOffset.y )
        {
            m_mapOffset.y = Math::Floor( -center.y ) + m_growthOffset;
            rebuild = true;
        }
        return rebuild;
    }

    template < class TEntity >
    void SpatialMap< TEntity >::Render( RenderFunc pfnRenderFunc, void* userData /* = NULL */ ) const
    {
        EntitiesConstIt it = m_entities.begin();
        EntitiesConstIt end = m_entities.end();
        for(; it != end; ++it )
        {
            pfnRenderFunc( *it, userData );
        }
    }

    template < class TEntity >
    void SpatialMap< TEntity >::RenderCells( void* userData /* = NULL */ ) const
    {
        BucketsMapConstIt mapIt = m_map.begin();
        BucketsMapConstIt mapEnd = m_map.end();
        SMAABB bv;
        SMVector extent( GetCellSize(), GetCellSize() );
        SMVector min;
        for(; mapIt != mapEnd; ++mapIt )
        {
            min = GetCellPos( mapIt->first );
            bv.SetMinMax( min, min + extent );
            bv.Render( 0xFF0000, userData );
        }
    }

    template < class TEntity >
    void SpatialMap< TEntity >::Intersect( const SMBoundingVolume* bv, IntersectionCallback callback, void* userData /* = NULL */ ) const
    {
        const SMVector& pos = SMAABB( bv ).GetCenter();
        const CellId cellId = GetCellId( pos );

        IntersectionVisitor visitor( bv, callback );

        VisitEntities( cellId, &visitor, userData );

        VisitSurroundings( cellId, &visitor, userData );
    }

    template < class TEntity >
    void SpatialMap< TEntity >::Collide( SMEntity* ent, CollisionCallback callback, void* userData /* = NULL */ ) const
    {
        const SMVector& pos = m_pfAABBProvider( ent ).GetCenter();
        const CellId cellId = GetCellId( pos );

        CollisionVisitor visitor( ent, callback );

        VisitEntities( cellId, &visitor, userData );

        VisitSurroundings( cellId, &visitor, userData );
    }

    template < class TEntity >
    unsigned int SpatialMap< TEntity >::GatherNeighbours( const SMVector& v, Entities& outList ) const
    {
        CellId cellId = GetCellId( v );

        GatherVisitor visitor( &outList );

        VisitEntities( cellId, &visitor, NULL );
        return visitor.GetCount();
    }

    template < class TEntity >
    void SpatialMap< TEntity >::VisitEntities( const CellId& cellId, Visitor* visitor, void* user ) const
    {
        BucketsMapConstIt mapIt = m_map.find( cellId );
        if( mapIt == m_map.end() )
            return;

        const Bucket& bucket = mapIt->second;
        typename Bucket::const_iterator it = bucket.Begin();
        typename Bucket::const_iterator end = bucket.End();

        for( ; it != end; ++it )
        {
            visitor->Visit( *it, user );
        }
    }

    template < class TEntity >
    void SpatialMap< TEntity >::VisitSurroundings( const CellId& cellId, Visitor* visitor, void* user ) const
    {
        VisitEntities( GetNeighbourCellId( cellId, -1,  0 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId, -1, -1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  0, -1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  1, -1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  1,  0 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  1,  1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId,  0,  1 ), visitor, user );
        VisitEntities( GetNeighbourCellId( cellId, -1,  1 ), visitor, user );
    }

    template < class TEntity >
    SpatialMap< TEntity >::IntersectionVisitor::IntersectionVisitor( const SMBoundingVolume* intersectBv, IntersectionCallback cb )
        : Visitor()
        , m_bv( intersectBv )
        , m_callback( cb )
    {}

    template < class TEntity >
    void SpatialMap< TEntity >::IntersectionVisitor::Visit( SMEntity* ent, void* user )
    {
        // Do not test entities with themselfs
        if( ent->GetCollisionWorldBV() == m_bv )
            return;

        // TODO: Consider using more generic approach to acquire collision bounding volume
        if( ent->GetCollisionWorldBV()->Intersect( m_bv ) )
        {
            m_callback( m_bv, ent, user );
        }
    }

    template < class TEntity >
    SpatialMap< TEntity >::CollisionVisitor::CollisionVisitor( SMEntity* collider, CollisionCallback cb )
        : Visitor()
        , m_collider( collider )
        , m_callback( cb )
    {}

    template < class TEntity >
    void SpatialMap< TEntity >::CollisionVisitor::Visit( SMEntity* ent, void* user )
    {
        // Do not test entities with themselfs
        if( ent == m_collider )
            return;

        // TODO: Consider using more generic approach to acquire collision bounding volume
        if( ent->GetCollisionWorldBV()->Intersect( m_collider->GetCollisionWorldBV() ) )
        {
            m_callback( m_collider, ent, user );
        }
    }

    template < class TEntity >
    SpatialMap< TEntity >::GatherVisitor::GatherVisitor( Entities* list )
        : m_list( list )
        , m_count( 0 )
    {}

    template < class TEntity >
    void SpatialMap< TEntity >::GatherVisitor::Visit( SMEntity* entity, void* user )
    {
        m_list->push_back( entity );
        ++m_count;
    }

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/space2d/SpatialMap.inl>

#endif // !defined __SCENE__SPACE2D_SPATIAL_MAP_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/QuadTree.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__SPACE2D__QUADTREE_HPP__
#define __SCENE__SPACE2D__QUADTREE_HPP__

// Internal includes
#include <jungle/scene/space2d/QuadTreeConfig.hpp>

#include <jungle/scene/BoundingObject.hpp>
#include <jungle/scene/SceneObject.hpp>
#include <jungle/scene/CollisionObject.hpp>
#include <jungle/scene/RenderObject.hpp>
#include <jungle/scene/CollisionQuery.hpp>

namespace Jungle
{
namespace Scene
{
    template < class TEntity >
    class QuadTreeNode
    {
    protected:
        // Forward declare entities iterator class.
        class QuadTreeEntityIt;

    public:
        typedef QuadTreeNode                                                self_type;

        typedef TEntity                                                     QTEntity;
        typedef CollisionObject< QTBoundingVolume >                         QTCollisionObj;
        typedef RenderObject< QTBoundingVolume >                            QTRenderObj;
        typedef CollisionQuery< QTEntity, QTBoundingVolume >                QTCollisionQuery;
        typedef RayTraceQuery< QTEntity, QTBoundingVolume, QTRay >          QTRayTraceQuery;

        //! Function pointer for providing bounding volume from case specific entity type.
        typedef const QTBoundingVolume*     (*BvProvider)(const QTEntity* entity);

        //! Function pointer for getting RenderObject interface from project specific game entity.
        typedef QTRenderObj*                (*RenderObjProvider)(QTEntity* entity);

        //! Function used to render objects - this function type may be passed to Render() method.
        typedef void                        (*RenderFunc)(const QTEntity* entity, void* userData);

        //! Simple structure for holding visibility query data.
        struct QTVisibilityQuery
        {
            const QTViewFrustum*    m_frustum;
            RenderObjProvider       m_pfnRenderObjProvider;

        }; // struct QTVisiblityQuery

        //! Default constructor
                                QuadTreeNode( BvProvider pfnBvProvider );

        //! Destructor
                                ~QuadTreeNode();

        //! Build tree
        void                    BuildTree(QTEntity** entitiesList, unsigned int entitiesNum);

        //! Build tree based on currently handled nodes.
        /*!
        * This method will recalculate global volume for the root node and apply new quad sizes to the child nodes if
        * neccessary. Current policy is to only extend the global volume thus it won't change to smaller one. This
        * simplification results in less full rebuilds (changing the size of whole nodes hierarchy) thus bigger global volume
        * is considered to be preffered. This imply the possibility to manually resize global BV to the world dimensions and
        * prevent from full tree rebuilds at all. In such situation only SceneEntities change their places (node ownership)
        * without changing quad tree nodes (position and size) at all.
        */
        void                    RebuildTree();

        //! Adds scene node/entity to quad tree
        /*!
        * If enity dimensions are bigger then global bounding volume, quad tree need to perform almost full rebuild.
        * The only difference in such situation is that global volume is simple extended without the need to recalculate it
        * basic on all entities actually attached.
        */
        inline void             AddEntity(QTEntity* sceneEntity);

        //! Remove entity from the quadtree without rebuilding whole structure.
        /*!
        * Will leave dirty (empty) node, but this way takes less time to proceed.
        */
        inline bool             RemEntity(QTEntity* sceneEntity);

        //! Returns number of child nodes in given node hierarchy
        inline unsigned int     GetNodesNum()   const;

        //! Returns bounding volume pointer
        inline const QTAABB*    GetBV()         const;

        //! Query collisions starting from current node
        inline bool             QueryCollision(QTCollisionQuery* query) const;

        //! Query ray trace test starting from current node
        inline bool             QueryRayTrace(QTRayTraceQuery* query) const;

        //! Visibility query - uses frustum culling method to determine scene entities visibility.
        /*!
        * Marks all rendering objects in the three with up-to-date culling flags, relative to furstum passed
        * in the query.
        * \see RenderObject::GetFrustumCullingStatus()
        * \see RenderObject::IsFrustumCulled()
        */
        inline bool             QueryVisibility(QTVisibilityQuery* query) const;

        //! Iterate thru all entities in quad tree passing them to render function provided.
        void                    Render( RenderFunc pfnRenderFunc, void* user = NULL ) const;

        //! Rendering cells for debug purposes
        /*!
        * Each depth level will be rendered with different color, in addition
        * nodes containing game entities will be marked with their specific color.
        */
        void                    RenderCells(bool recurse, void* user = NULL ) const;

        //! Rendering debug method
        void                    RenderCells(int depth, void* user )   const;

        /**
        * User-callback, called for each node by the walking code.
        * \param    current     [in]    current node
        * \param    userData    [in]    user-defined data used in all tree levels
        * \return   false       to recurse through children, else true to bypass them
        */
        typedef bool            (*WalkerCallback)(const self_type* currentNode, void* userData);

        /**
        * Generic user-callback, allowing for passing in/out callback parameters.
        * \param    current         [in]    current node
        * \param    userData        [in]    data used on all levels
        * \param    userStackInput  [in]    input data for the current level processing
        * \param    userStackOutput [out]   user data output passed as an input for the next hierarchy level
        * \return   false       to recurse through children, else true to bypass them
        */
        typedef bool            (*StackWalkerCallback)(const self_type* currentNode, void* userData, unsigned int& userStackInput, unsigned int& userStackOutput);

        //! Walker method allows to process tree using callback passed to it with data passed in
        static bool             Walker(const self_type* currentNode, WalkerCallback callback, void* userData);

        //! Walker method with additional input/output passed between hierarchy levels
        static bool             StackWalker(const self_type* currentNode, StackWalkerCallback callback, void* userData, unsigned int& userEntryStackData);

    protected:
        enum QTNodeIdx
        {
            QT_NODE_UP_LEFT     = 0,
            QT_NODE_UP_RIGHT,
            QT_NODE_DOWN_LEFT,
            QT_NODE_DOWN_RIGHT,

            QT_NODES_NUM,
        }; // enum QTNodeIdx

        typedef QuadTreeEntityIt    QTEntityIt;

        //! Definition constructor used only by the class itself
                                QuadTreeNode(self_type* parent, const QTVector& center, const QTVector& extents);

        //! Adds quad tree entity to node hierarchy used internally in rebuild process
        void                    AddEntity(QTEntityIt* entity, bool updateTree = false );

        //! Check if new entity expands root bounding volume, and rebuild tree if neccessary.
        /*!
        * Method checks if adding new entity requires tree rebuild and performs it if so, otherwise
        * returns false and do nothing else.
        * \return true if new entity expanded first level node and thus forced tree rebuild, false
        * if new node fits into existing tree, then use AddEntity method to add new object.
        */
        bool                    ExpandTree(QTEntity* entity);

        //! Recalculate all current node childs bounding volumes and recurse down hierarchy.
        void                    ComputeChildVolumes();

        //! Recalculate global bounding volume surrounding all nodes passed to
        void                    ComputeGlobalVolume(QTEntity** entitiesList, unsigned int entitiesNum);

        //! Recalculate global bounding volume surrounding entities on the passed list
        void                    ComputeGlobalVolume(QTEntityIt* entitiesList);

        //! Method removes entities from node and add them to the passed list, returns last element pointer
        QTEntityIt*             StashEntitiesList(QTEntityIt** nodesList);

        //! Check if node is leaf
        inline bool             IsLeaf()        const;

        //! Returns first entity pointer or NULL if no entities attached to node
        inline QTEntityIt*      GetNodesList()  const;

        //! Checks if entity is inside desired quad space
        inline bool             IsInsideQuad(const QTEntityIt* entity, const QTAABB& quadBV) const;

        //! Check if a quad with center and extent specified in arguments overlaps entity bounding volume
        inline bool             IsInsideQuad(QTEntityIt* entity, const QTVector& center, const QTVector& extents) const;

        //! Acquire entity bounding volume corresponding to the quad tree type, which may be rendering volume or collision geometry.
        inline const
        QTBoundingVolume*       GetEntityWorldBV(QTEntity* entity) const;

        //! Setup rendering status of node enclosed entities/models - optionally recurse down tree to mark all entities in hierarchy
        void                    SetRenderStatus(RenderObjProvider pfnRenderObjProvider, const RenderFlag& flag, bool recursive = true)  const;

        /**
        * Performs collision testing until leaf in given hierarchy is reached.
        * \param  node          [in]    Node for which callback is invoked
        * \param  userData      [in]    Collision cache
        * \return true          to recurse through children, else false to bypass them
        */
        static bool             CollisionCallback(const self_type* node, QTCollisionQuery* query);

        /**
        * Performs ray intersection testing until leaf in given hierarchy is reached.
        * \param  node          [in]    Node for which callback is invoked
        * \param  userData      [in]    Ray trace cache
        * \return true          to recurse through children, else false to bypass them
        */
        static bool             RayTraceCallback(const self_type* node, QTRayTraceQuery* query);

        /**
        * Performs AABB based frustum culling. Any other types of bounding volumes hierarchies are converted to 
        * appropriate AABB volumes. AABB frustum culling method uses frustum plane masking algorithm and P-N vertex theorem.
        * \param  node          [in]    Entry node
        * \param  frustum       [in]    Frustum volume to be tested with
        * \param  inputMask     [in]    Plane masking input mask
        * \param  outputMask    [out]   Frustum collision output mask used in lower hierarchy nodes tests
        * \return true          If culling is finnished at the current level, no further test needed for current node branch
        */
        static bool             FrustumCullingCallback(const self_type* node, const QTVisibilityQuery* query, unsigned int& inputMask, unsigned int& outputMask);

    protected:
        self_type*              m_parent;
        self_type*              m_children[QT_NODES_NUM];

        QTEntityIt*             m_entities;

        QTAABB                  m_bv;
        unsigned int            m_depth;

        BvProvider              m_pfnBvProvider;

        static
        const unsigned int      MAX_DEPTH;      //!< Maximum number of subidivision levels.

        //! Internal class for iterating and storing entities list.
        class QuadTreeEntityIt
        {
        public:
            typedef QuadTreeEntityIt    self_type;
            typedef self_type*          iterator;
            typedef TEntity             value_type;

                                QuadTreeEntityIt();
                                QuadTreeEntityIt( value_type* node );

            inline iterator     operator++()                { m_next = m_next->GetNext(); return this; }
            inline iterator     operator++(int)             {const iterator it(this); ++*this; return it; }

            inline iterator     GetNext() const             { return m_next; }

            void                PushFront( self_type* it );
            void                PushBack( self_type* it );

            value_type*         m_entity;
            self_type*          m_next;

        }; // class QuadTreeEntityIt

    }; // class QuadTreeNode

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/space2d/QuadTree.inl>

#endif // !define __SCENE__SPACE2D__QUADTREE_HPP__
// EOF

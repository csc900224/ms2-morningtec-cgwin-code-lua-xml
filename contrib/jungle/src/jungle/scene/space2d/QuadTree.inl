////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/space2d/QuadTree.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

namespace Jungle
{
namespace Scene
{
    template < class TEntity >
    const unsigned int QuadTreeNode< TEntity >::MAX_DEPTH = QT_NODES_MAX_DEPTH;

    template < class TEntity >
    QuadTreeNode< TEntity >::QuadTreeNode( BvProvider pfnBvProvider )
        : m_parent(NULL)
    {
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
            m_children[i] = NULL;
        m_depth = 0;
        m_entities = NULL;
        m_bv.SetEmpty();

        m_pfnBvProvider = pfnBvProvider;
    }

    template < class TEntity >
    QuadTreeNode< TEntity >::~QuadTreeNode()
    {
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(m_children[i])
                delete m_children[i];
        }
        QTEntityIt* it;
        while((it = m_entities) != NULL)
        {
            m_entities = it->m_next;
            delete it;
        }
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::BuildTree(QTEntity** entitiesList, unsigned int entitiesNum)
    {
        if( !(entitiesList && entitiesNum) ) // in other words (entitiesNum <= 0 || entitiesList == NULL)
            return;

        // Calculate global bounding volume
        ComputeGlobalVolume(entitiesList, entitiesNum);
        // Recalculate all child nodes volumes
        ComputeChildVolumes();

        for(unsigned int i = 0; i < entitiesNum; ++i)
        {
            AddEntity(entitiesList[i]);
        }
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::RebuildTree()
    {
        // Create temporary list of all entities in hierarchy
        QTEntityIt* nodesList = NULL;//GetNodesList();
        StashEntitiesList(&nodesList);

        // After list is build reset current node list
        //m_entities = NULL; // KKO: Was disabled

        // Check if global volume should be expanded
        QTAABB oldBV( m_bv );
        ComputeGlobalVolume( nodesList );
        // This check allows to restore customized global volume that use might set in order to
        // save time on extending global bv
        if( oldBV.Overlaps( &m_bv ) )
        {
            // Store old bv if was bigger then calculated one
            m_bv = oldBV;
        }
        else
        {
            // Recalculate volumes down hierarchy cause root bv has changed
            ComputeChildVolumes();
        }

        // Create some helper variables
        QTEntityIt* listItem = nodesList;
        QTEntityIt* helperItem;
        while(listItem)
        {
            // Save item pointer to pass it into building method
            helperItem = listItem;
            // Save next item cause it will be changed in AddEntity method
            listItem = listItem->m_next;
            // Try to add entity to some hierarchy node
            AddEntity(helperItem);
        }
    }

    template < class TEntity >
    inline void QuadTreeNode< TEntity >::AddEntity(QTEntity* sceneEntity)
    {
        // On the first level check if entity expands bounding volume
        if( !ExpandTree( sceneEntity ) )
        {
            AddEntity( new QTEntityIt(sceneEntity) );
        }
    }

    template < class TEntity >
    inline bool QuadTreeNode< TEntity >::RemEntity(QTEntity* sceneEntity)
    {
        QTEntityIt* it = GetNodesList();
        QTEntityIt* prev = NULL;
        while( it )
        {
            if( it->m_entity == sceneEntity )
            {
                if( prev )
                    prev->m_next = it->m_next;
                else
                    m_entities = it->m_next;

                delete it;
                return true;
            }
            prev = it;
            it = it->m_next;
        }

        for( int i = 0; i < QT_NODES_NUM; ++i )
        {
            if( m_children[i] && m_children[i]->RemEntity( sceneEntity ) )
                return true;
        }
        return false;
    }

    template < class TEntity >
    inline unsigned int QuadTreeNode< TEntity >::GetNodesNum()   const
    {
        unsigned int nodesNum = 1;
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(m_children[i])
                nodesNum += m_children[i]->GetNodesNum();
        }
        return nodesNum;
    }

    template < class TEntity >
    inline const QTAABB* QuadTreeNode< TEntity >::GetBV() const
    {
        return &m_bv;
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::QueryCollision(QTCollisionQuery* query) const
    {
        // It is up to caller to reset/init query (from performance reasons)
        //query->InitQuery();
        return QuadTreeNode::Walker(this, (WalkerCallback)QuadTreeNode::CollisionCallback, query);
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::QueryRayTrace(QTRayTraceQuery* query) const
    {
        // It is up to caller to reset/init query (from performance reasons)
        //query->InitQuery();
        return QuadTreeNode::Walker(this, (WalkerCallback)QuadTreeNode::RayTraceCallback, query);
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::QueryVisibility(QTVisibilityQuery* query) const
    {
        unsigned int mask = 0x1;
        return QuadTreeNode::StackWalker(this, (StackWalkerCallback)QuadTreeNode::FrustumCullingCallback, query, mask);
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::Render( RenderFunc pfnRenderFunc, void* user /* = NULL */ ) const
    {
        QTEntityIt* it = m_entities;
        while(it)
        {
            pfnRenderFunc( it->m_entity, user );
            it = it->m_next;
        }
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::RenderCells( bool recursive, void* user /* = NULL */ ) const
    {
        QTEntityIt* it = GetNodesList();
        bool hasEntity = false;
        while(it)
        {
            if(it->m_entity)
            {
            #if _DEBUG_QUAD_TREE_RENDER_ENTITY_BV
                GetEntityWorldBV( it->m_entity )->Render(0xFF0000, user);
            #endif
                hasEntity = true;
            }
            it = it->m_next;
        }
        m_bv.Render( (255 - (m_depth * 50)) << (hasEntity ? 16 : 8), user );

        // Recursively loop down hierarchy
        if(recursive)
        {
            for(int i = QT_NODES_NUM - 1; i > -1; --i)
            {
                if(m_children[i])
                    m_children[i]->RenderCells(recursive, user);
            }
        }
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::RenderCells( int depth, void* user /* = NULL */ ) const
    {
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(m_children[i])
                m_children[i]->Render(depth);
        }

        if(m_depth != depth)
            return;

        QTEntityIt* it = m_entities;
        while(it)
        {
            GetEntityWorldBV( it->m_entity )->Render( 0xFF0000, user );
            it = it->m_next;
        }
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::Walker(const QuadTreeNode* currentNode, WalkerCallback callback, void* userData)
    {
        // Safety check
        if(!currentNode)
            return true;

        // Process callback invocation for current node and finish processing if not required
        if(callback)
            if((callback)(currentNode, userData))
                return true;

        // Call method for all childes nodes
        bool ret = false;
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(currentNode->m_children[i])
            {
                if(QuadTreeNode::Walker(currentNode->m_children[i], callback, userData))
                    ret = true;
            }
        }
        return ret;
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::StackWalker(const QuadTreeNode* currentNode, StackWalkerCallback callback, void* userData, unsigned int& userStackData)
    {
        // Safety check
        if(!currentNode)
            return true;

        unsigned int userDataOut = 0;
        // Process callback invocation for current node and finish processing if not required
        if(callback)
            if((callback)(currentNode, userData, userStackData, userDataOut))
                return true;

        // Call method for all childes nodes
        bool ret = false;
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(currentNode->m_children[i])
            {
                if(QuadTreeNode::StackWalker(currentNode->m_children[i], callback, userData, userDataOut))
                    ret = true;
            }
        }
        return ret;
    }

    template < class TEntity >
    QuadTreeNode< TEntity >::QuadTreeNode(QuadTreeNode* parent, const Vector2& center, const Vector2& extents)
        : m_parent( parent )
        , m_pfnBvProvider( parent->m_pfnBvProvider )
    {
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            m_children[i] = NULL;
        }
        if(m_parent)
        {
            m_depth = m_parent->m_depth + 1;
        }
        m_bv.SetCenterExtents(center, extents);
        m_entities = NULL;
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::AddEntity(QTEntityIt* entity, bool updateTree /*= false */)
    {
        // Check child nodes to see if object fits inside one of them.
        if( m_depth < MAX_DEPTH )
        {
            const QTVector& nodeExtents = m_bv.GetExtents();
            const QTVector& nodeCenter = m_bv.GetCenter();

            QTVector childNodeExtents(nodeExtents / Real(2)); // WORLD_SIZE / (2 << q->depth);
            QTVector childNodeCenter(nodeCenter);

            int i, j;
            // j iterates up/down node position
            for (j = 0; j < 2; ++j)
            {
                // i iterates left/right nodes position
                for (i = 0; i < 2; ++i)
                {
                    int childIdx = (j*2) + i;

                    childNodeCenter.x = nodeCenter.x + ((i == 0) ? -childNodeExtents.x : childNodeExtents.x);
                    childNodeCenter.y = nodeCenter.y + ((j == 0) ? -childNodeExtents.y : childNodeExtents.y);

                    // Update child node volume
                    if( m_children[childIdx] && updateTree )
                    {
                        m_children[childIdx]->m_bv.SetCenterExtents(childNodeCenter, childNodeExtents);
                    }
                    // If enetity fits the quad
                    if( IsInsideQuad(entity, childNodeCenter, childNodeExtents) )
                    {
                        // Recurse into this node.
                        if (m_children[childIdx] == 0)
                        {
                            m_children[childIdx] = new QuadTreeNode(this, childNodeCenter, childNodeExtents);
                        }
                        return m_children[childIdx]->AddEntity(entity, updateTree);
                    }
                }
            }
        }
        // Keep object in this node.
        entity->m_next = m_entities;
        m_entities = entity;
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::ExpandTree( QTEntity* sceneEntity )
    {
        // Only root level nodes may be expanded
        if( m_depth != 0 )
        {
            return false;
        }

        // Entity bounding volume is smaller then quad tree root bv, no need to rebuild root and underlaying nodes
        if( m_bv.Overlaps( GetEntityWorldBV( sceneEntity ) ) )
        {
            return false;
        }
        else
        {
            // No need to compute new global bv, just expand root bounding volume to include new entity
            m_bv.Merge( GetEntityWorldBV( sceneEntity ) );

            // Recalculate child volumes down the hierarchy
            ComputeChildVolumes();
        }

        // Create temporary list of all entities in hierarchy
        QTEntityIt* entitiesList = NULL;//GetNodesList();
        StashEntitiesList(&entitiesList);

        // After list is build reset current node list
        //m_entities = NULL; // KKO: Was disabled

        // Create some helper variables
        QTEntityIt* listItem = entitiesList;
        QTEntityIt* helperItem;
        while(listItem)
        {
            // Save item pointer to pass it into building method
            helperItem = listItem;
            // Save next item cause it will be changed in AddEntity method
            listItem = listItem->m_next;
            // Try to add entity to some hierarchy node
            AddEntity(helperItem);
        }
        // At the end add new entity to the tree
        AddEntity( new QTEntityIt( sceneEntity) );
        return true;
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::ComputeChildVolumes()
    {
        const QTVector& nodeExtents = m_bv.GetExtents();
        const QTVector& nodeCenter = m_bv.GetCenter();

        QTVector childNodeExtents(nodeExtents / Real(2) ); // WORLD_SIZE / (2 << q->depth);
        QTVector childNodeCenter(nodeCenter);

        int i, j;
        // j iterates up/down node position
        for (j = 0; j < 2; ++j)
        {
            // i iterates left/right nodes position
            for (i = 0; i < 2; ++i)
            {
                int childIdx = (j*2) + i;

                // Update child node volume
                if( m_children[childIdx] )
                {
                    childNodeCenter.x = nodeCenter.x + ((i == 0) ? -childNodeExtents.x : childNodeExtents.x);
                    childNodeCenter.y = nodeCenter.y + ((j == 0) ? -childNodeExtents.y : childNodeExtents.y);

                    m_children[childIdx]->m_bv.SetCenterExtents(childNodeCenter, childNodeExtents);
                    m_children[childIdx]->ComputeChildVolumes();
                }
            }
        }
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::ComputeGlobalVolume(QTEntity** entitiesList, unsigned int entitiesNum)
    {
        if(entitiesNum > 0)
        {
            // Setup first entity bounding volume as a startup size
            m_bv.Generate( GetEntityWorldBV( entitiesList[0] ) ); 
        }
        for(unsigned int e = 1; e < entitiesNum; ++e)
        {
            // Acquire only nodes entity volume without lower hierarchy nodes
            m_bv.Merge( GetEntityWorldBV( entitiesList[e] ) );
        }
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::ComputeGlobalVolume(QTEntityIt* entitiesList)
    {
        QTEntityIt* it = entitiesList;
        if(it)
        {
            // Acquire only nodes entity volume without lower hierarchy nodes
            m_bv.Generate( GetEntityWorldBV( it->m_entity ) );
            it = it->m_next;
        }
        while(it)
        {
            // Acquire only nodes entity volume without lower hierarchy nodes
            m_bv.Merge( GetEntityWorldBV( it->m_entity ) );
            it = it->m_next;
        }
    }

    template < class TEntity >
    typename QuadTreeNode< TEntity >::QTEntityIt* QuadTreeNode< TEntity >::StashEntitiesList(QTEntityIt** nodesList)
    {
        // Be sure to attach nodes to the last element in the list
        QTEntityIt* it = *nodesList;
        // Find last entity without next node attached
        // TODO: Consider using PushFront iterator method to save time on last element iteration.
        // Will it give the same results (number of entities won't change anyway?) ?
        while(it != NULL && it->m_next != NULL)
        {
            it = it->m_next;
        }
        // Attach first entity to the end of list 
        if(*nodesList != NULL)
            it->m_next = m_entities;
        // If list is empty attach current entities list as the first list item
        else
            it = *nodesList = m_entities;

        // Reset current node list
        m_entities = NULL;

        // Find last element pointer
        // TODO: Is it really neccessary, this iteration will be perfomed in the next recursion
        // at the start of this method.
        while(it != NULL && it->m_next != NULL)
        {
            it = it->m_next;
        }
        // Recurse into sub-levels
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(m_children[i])
                it = m_children[i]->StashEntitiesList(*nodesList == NULL ? nodesList : &it);
        }
        // Return last element
        return it;
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::SetRenderStatus( RenderObjProvider pfnRenderObjProvider, const RenderFlag& flag, bool recursive) const
    {
        JUNGLE_ASSERT( pfnRenderObjProvider );

        // Setup all entities in hierarchy culling status given in parameter
        QTEntityIt* it = GetNodesList();
        while(it)
        {
            QTRenderObj* renderObj = pfnRenderObjProvider( it->m_entity );
            if( renderObj )
            {
                renderObj->SetFrustumCullingStatus(flag);
            }
            it = it->m_next;
        }
        // Recursively loop down hierarchy
        if(recursive)
        {
            for(int i = QT_NODES_NUM - 1; i > -1; --i)
            {
                if(m_children[i])
                    m_children[i]->SetRenderStatus(pfnRenderObjProvider, flag);
            }
        }
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::CollisionCallback(const QuadTreeNode* node, QTCollisionQuery* query)
    {
        // Check node collision
        if(node->GetBV()->Intersect(query->GetQueryBV()))
        {
            // Check collisions with entities gathered inside this node
            QTEntityIt* it = node->GetNodesList();
            while(it)
            {
                // Second check allows to store objects that have no collision representation.
                if(it->m_entity && node->GetEntityWorldBV(it->m_entity) )
                {
                    if( query->GetQueryBV()->Intersect( node->GetEntityWorldBV(it->m_entity) ) )
                        query->GetCache()->Add(it->m_entity);
                }
                it = it->m_next;
            }
            // Check out all childes in further walker steps
            return false;
        }
        // End processing
        else
        {
            return true;
        }
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::RayTraceCallback(const QuadTreeNode* node, QTRayTraceQuery* query)
    {
        // Check node collision
        if(query->GetQueryRay()->Intersect(node->GetBV()))
        {
            // Check collisions with entities gathered inside this node
            QTEntityIt* it = node->GetNodesList();
            while(it)
            {
                // Second check allows to store objects that have no collision representation.
                if(it->m_entity && node->GetEntityWorldBV(it->m_entity) )
                {
                    if( !query->IsContactsGather() )
                    {
                        if( query->GetQueryRay()->Intersect( node->GetEntityWorldBV( it->m_entity) ) )
                        {
                            query->Add(it->m_entity);
                            if( query->ReceiveFirstOnly() )
                            {
                                // End processing - first contact found.
                                return true;
                            }
                        }
                    }
                    else
                    {
                        typename QTRayTraceQuery::Contact contact( query->GetContactsGatherConfig() );
                        if( query->GetQueryRay()->Intersect( node->GetEntityWorldBV( it->m_entity), &contact ) )
                        {
                            query->Add(it->m_entity, &contact);
                            if( query->ReceiveFirstOnly() )
                            {
                                // No need to process anymore, first collision found.
                                return true;
                            }
                        }
                    }
                }
                it = it->m_next;
            }
            // Check out all childes in further walker steps
            return false;
        }
        // End processing
        else
            return true;
    }

    template < class TEntity >
    bool QuadTreeNode< TEntity >::FrustumCullingCallback(const QuadTreeNode* node, const QTVisibilityQuery* query, unsigned int& inputMask, unsigned int& outputMask)
    {
        const QTViewFrustum* frustum = query->m_frustum;
        RenderObjProvider pfnRenderObjProvider = query->m_pfnRenderObjProvider;

        //ViewFrustumCulling cullingFlag = frustum->AABBFrustumCulling(node->GetBV(), inputMask, outputMask);
        ViewFrustumCulling cullingFlag = frustum->Overlaps(node->GetBV()) ? VFC_FRUSTUM_INSIDE : VFC_FRUSTUM_INTERSECT;

        if(cullingFlag & VFC_FRUSTUM_INSIDE)
        {
            // All entities in this node should be rendered - mark all entities to render
            node->SetRenderStatus(pfnRenderObjProvider, RF_INSIDE_FRUSTUM, true);
            //node->Render(NULL, true);

            // Still need to perform testing child nodes down tree return false, 
            // exceptionally if node is a leaf return true
            return node->IsLeaf();
        }

        cullingFlag = frustum->Intersect(node->GetBV()) ? VFC_FRUSTUM_INTERSECT : VFC_FRUSTUM_OUTSIDE;

        if(cullingFlag & VFC_FRUSTUM_OUTSIDE)
        {
            // No need for rendering
            // Mark all entities not to render
            node->SetRenderStatus(pfnRenderObjProvider, RF_OUTSIDE_FRUSTUM, true);

            // Do not test child nodes
            return true;
        }
        else if(cullingFlag & VFC_FRUSTUM_INTERSECT )
        {
            // Worst case need to test every entity with frustum
            QTEntityIt* it = node->GetNodesList();
            while(it)
            {
                // Second check secures us to handle objects without renderable representation
                QTRenderObj* renderObj = NULL;
                if( it->m_entity && (renderObj = pfnRenderObjProvider( it->m_entity )) != NULL )
                {
                    if( frustum->Intersect( node->GetEntityWorldBV( it->m_entity ) ) )
                    {
                        renderObj->SetFrustumCullingStatus(RF_INTERSECT_FRUSTUM);
                    #if _DEBUG_QUAD_TREE_RENDER_ENTITY_BV
                        node->GetEntityWorldBV( it->m_entity )->Render(0x00FF00);
                    #endif
                    }
                    else
                    {
                        renderObj->SetFrustumCullingStatus(RF_OUTSIDE_FRUSTUM);
                    }
                }
                it = it->m_next;
            }
            return node->IsLeaf();
        }
        return false;
    }

    template < class TEntity >
    inline bool QuadTreeNode< TEntity >::IsLeaf() const
    {
        for(int i = QT_NODES_NUM - 1; i > -1; --i)
        {
            if(m_children[i])
                return false;
        }
        return true;
    }

    template < class TEntity >
    inline typename QuadTreeNode< TEntity >::QTEntityIt* QuadTreeNode< TEntity >::GetNodesList() const
    {
        return m_entities;
    }

    template < class TEntity >
    inline bool QuadTreeNode< TEntity >::IsInsideQuad(const QTEntityIt* entity, const QTAABB& quadBV) const
    {
        return quadBV.Overlaps( GetEntityWorldBV( entity->m_entity ) );
    }

    template < class TEntity >
    inline bool QuadTreeNode< TEntity >::IsInsideQuad(QTEntityIt* entity, const QTVector& center, const QTVector& extents) const
    {
        // Acquire only nodes entity volume without lower hierarchy nodes
        QTAABB entityBV;
        entityBV.Generate( GetEntityWorldBV( entity->m_entity ) );
        const QTVector& min = entityBV.GetMin();
        const QTVector& max = entityBV.GetMax();
        if(min.x < center.x - extents.x)    return false;
        if(max.x > center.x + extents.x)    return false;
        if(min.y < center.y - extents.y)    return false;
        if(max.y > center.y + extents.y)    return false;
        return true;
    }

    template < class TEntity >
    inline const QTBoundingVolume* QuadTreeNode< TEntity >::GetEntityWorldBV( QTEntity* entity ) const
    {
        return m_pfnBvProvider( entity );
    }

    template < class TEntity >
    QuadTreeNode< TEntity >::QuadTreeEntityIt::QuadTreeEntityIt()
        : m_entity( 0 )
        , m_next( 0 )
    {
    }

    template < class TEntity >
    QuadTreeNode< TEntity >::QuadTreeEntityIt::QuadTreeEntityIt( value_type* entity )
        : m_entity( entity )
        , m_next( 0 )
    {
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::QuadTreeEntityIt::PushFront( self_type* it )
    {
        self_type* last = it;
        while( last->GetNext() )
            last = last->GetNext();
        last->m_next = this;
    }

    template < class TEntity >
    void QuadTreeNode< TEntity >::QuadTreeEntityIt::PushBack( self_type* it )
    {
        self_type* last = this;
        while( last->GetNext() )
            last = last->GetNext();
        last->m_next = it;
    }

} // namespace Scene

} // namespace Jungle

// EOF

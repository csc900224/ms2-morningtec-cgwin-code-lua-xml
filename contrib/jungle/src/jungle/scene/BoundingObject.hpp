////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/BoundingObject.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_BOUNDING_OBJECT_HPP__
#define __SCENE_BOUNDING_OBJECT_HPP__

// Internal includes

namespace Jungle
{
namespace Scene
{
    template < class TVector >
    class BoundingObject
    {
    public:
        /*!
        * Set bounding object to smallest possible size - do not contain anything
        */
        virtual void    SetEmpty() = 0;

        /*!
        * Check if object is totally empty and can not contain anything
        * Scaling and rotating shape doesn't affect it's empty status
        */
        virtual bool    IsEmpty() const = 0;

        /*!
        * Set bounding object to contain single point with coordinates passed to method
        * \param rPos   new bounding object coordinates center
        */
        virtual void    SetPoint(const TVector& rPos ) = 0;

        /*!
        * Check if object is minimal size - may contain only point.
        * Scaling and rotating shape doesn't affect it's status
        */
        virtual bool    IsPoint() const = 0;

    }; // class BoundingObject

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE_BOUNDING_OBJECT_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/CollisionQuery.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes

// External includes

namespace Jungle
{
namespace Scene
{
    template < class TCollisionObject, class TBoundingVolume >
    CollisionQuery< TCollisionObject, TBoundingVolume >::CollisionQuery()
        : m_pBV( NULL )
    {}

    template < class TCollisionObject, class TBoundingVolume, class TRay >
    RayTraceQuery< TCollisionObject, TBoundingVolume, TRay >::RayTraceQuery( QueryConfig queryConfig /* = QC_RECEIVE_ALL_CONTACTS */ )
        : m_queryConfig( queryConfig )
        , m_contactsGather( ReceiveClosestOnly() ? true : false )                       // Force contacts gather when closest collisions are queried
        , m_contactsGatherConfig( ReceiveClosestOnly() ? Contact::CG_DISTANCE : 0 )     // Distance calculation must be enabled to support closest point query
        , m_contacts( 0 )
        , m_pRay( NULL )
    {}

    template < class TCollisionObject, class TBoundingVolume, class TRay >
    RayTraceQuery< TCollisionObject, TBoundingVolume, TRay >::RayTraceQuery( QueryConfig queryConfig, bool gatherContacts, unsigned int contactConfig /* = 0 */ )
        : m_queryConfig( queryConfig )
        , m_contactsGather( ReceiveClosestOnly() ? true : gatherContacts )                              // Force contacts gather in closest collision mode
        , m_contactsGatherConfig( contactConfig | (ReceiveClosestOnly() ? Contact::CG_DISTANCE : 0) )   // Force distance calculation in closest collision mode.
        , m_pRay( NULL )
    {
        JUNGLE_WARNING_EXP_MSG( gatherContacts || !ReceiveClosestOnly(), "Can't disable contact points gather in closest collision mode, disable it first." );

        if( IsContactsGather() )
        {
            m_contacts.reserve( 10 );
        }
    }

    template < class TCollisionObject, class TBoundingVolume, class TRay >
    void RayTraceQuery< TCollisionObject, TBoundingVolume, TRay >::SetQueryConfig( QueryConfig config )
    {
        m_queryConfig = config;
        // Automatically add distance calculation in closest collision mode
        m_contactsGatherConfig |= ReceiveClosestOnly() ? Contact::CG_DISTANCE : 0;
    }

    template < class TCollisionObject, class TBoundingVolume, class TRay >
    void RayTraceQuery< TCollisionObject, TBoundingVolume, TRay >::SetContactsGather( bool enable, unsigned int flags )
    {
        JUNGLE_WARNING_EXP_MSG( enable || !ReceiveClosestOnly(), "Can't disable contact points gather in closest collision mode, disable it first." );

        // Always force contact points calculation in closest collision mode
        m_contactsGather = ReceiveClosestOnly() ? true : enable;
        m_contactsGatherConfig = flags;

        if( IsContactsGather() )
        {
            m_contacts.reserve( 10 );

            // Add proper contact set configuration flags to allows closest collision calculation.
            m_contactsGatherConfig |= ReceiveClosestOnly() ? Contact::CG_DISTANCE : 0;
        }
    }

} // namespace Scene

} // namespace Jungle

// EOF

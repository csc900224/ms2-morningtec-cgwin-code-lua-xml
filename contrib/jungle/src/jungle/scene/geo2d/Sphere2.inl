////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Sphere2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>

namespace Jungle
{
namespace Scene
{
    bool Sphere2::Intersect(const BoundingArea2* pBA) const
    {
        if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Intersect((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Intersect((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Intersect((OBB2*)pBA);
        return false;
    }

    bool Sphere2::Intersect(const Sphere2* pSphere2) const
    {
        Real distSqr =  Math::Sqr( GetCenter().x - pSphere2->GetCenter().x ) +
                        Math::Sqr( GetCenter().y - pSphere2->GetCenter().y );
        Real radiusSqr = Math::Sqr( GetRadius() + pSphere2->GetRadius() );
        return (distSqr < radiusSqr);
    }

    bool Sphere2::Intersect(const OBB2* pOBB2) const
    {
        return pOBB2->Intersect( this );
    }

    bool Sphere2::Overlaps(const BoundingArea2* pBA) const
    {
        if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Overlaps((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Overlaps((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Overlaps((OBB2*)pBA);
        return false;
    }

    bool Sphere2::Overlaps(const Sphere2* pSphere2) const
    {
        // TODO: To be tested
        if( GetRadius() < pSphere2->GetRadius() )
            return false;

        Vector2 off = pSphere2->GetCenter() - GetCenter();
        Real distSqr = off.x * off.x + off.y * off.y;
        if( distSqr > Math::Sqr( GetRadius() -  pSphere2->GetRadius() ) )
            return false;

        return true;
    }

    bool Sphere2::Overlaps(const AABB2* pAABB2) const
    {
        using namespace Math;

        // TODO: To be tested
        // Choose the farthes rectangle vertex
        const Vector2& ext = pAABB2->GetExtents();
        Vector2 farVtx = pAABB2->GetCenter() - GetCenter();
        // Rect center on the right add horizontal extens, on the left substract it
        farVtx.x += farVtx.x > 0 ? Abs( ext.x ) : -Abs( ext.x );
        // Simillary with the vertical extents
        farVtx.y += farVtx.y > 0 ? Abs( ext.y ) : -Abs( ext.y );
        // Check if farthest verticle is farther then circle radius
        Real distSqr = farVtx.x * farVtx.x + farVtx.y * farVtx.y;
        if( distSqr > Sqr( GetRadius() ) )
            return false;

        return true;
    }

    bool Sphere2::Overlaps(const OBB2* pOBB2) const
    {
        AABB2 aabb( pOBB2 );
        return Overlaps( &aabb );
    }

    bool Sphere2::Overlaps(const Vector2* pPoint) const
    {
        Vector2 diff = GetCenter() - *pPoint;
        if( diff.LengthSqr() > Math::Sqr( GetRadius() ) )
            return false;
        return true;
    }

    void Sphere2::Generate(const Sphere2* pSphere2)
    {
        SetCenter( pSphere2->GetCenter() );
        SetRadius( pSphere2->GetRadius() );
    }

    void Sphere2::Generate(const AABB2* pAABB2)
    {
        SetCenter( pAABB2->GetCenter() );
        SetRadius( pAABB2->GetExtents().Length() );
    }

    void Sphere2::Generate(const OBB2* pOBB2)
    {
        AABB2 aabb( pOBB2 );
        Generate( &aabb );
    }

    void Sphere2::Merge(const AABB2* pAABB2 )
    {
        Sphere2 circle( pAABB2 );
        Merge(&circle);
    }

    void Sphere2::Merge(const OBB2* pOBB2 )
    {
        Sphere2 circle( pOBB2 );
        Merge(&circle);
    }

    void Sphere2::Translate(const Vector2& rOff)
    {
        SetCenter( GetCenter() + rOff );
    }

    void Sphere2::Scale(Real scale)
    {
        SetRadius( GetRadius() * scale );
    }

    void Sphere2::Rotate(Real angle)
    {
        // Sphere2 rotation doesn't affect it's shape and bounded area
    }

    void Sphere2::SetEmpty()
    {
        base::SetEmpty();
    }

    bool Sphere2::IsEmpty() const
    {
        return base::IsEmpty();
    }

    void Sphere2::SetPoint(const Vector2& rPos)
    {
        base::SetPoint( rPos );
    }

    inline bool Sphere2::IsPoint() const
    {
        return base::IsPoint();
    }

} // namespace Scene

} // namespace Jungle

// EOF

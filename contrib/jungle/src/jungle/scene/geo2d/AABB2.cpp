////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/AABB2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>

#include <jungle/scene/SceneDebug.hpp>

// External modules includes
#include <jungle/base/NumberTraits.hpp>
#include <jungle/math/Arithmetic.hpp>       // Mul, Abs
#include <jungle/math/Trigonometry.hpp>     // Sin, Cos

namespace Jungle
{
namespace Scene
{
    AABB2::AABB2()
        : BoundingArea2( BA_AABB2 )
        , base()
    {
    }

    AABB2::AABB2(const AABB2* pAABB2)
        : BoundingArea2( BA_AABB2 )
    {
        Generate(pAABB2);
    }

    AABB2::AABB2(const BoundingArea2* pBA)
        : BoundingArea2( BA_AABB2 )
    {
        Generate(pBA);
    }

    AABB2::AABB2(const Vector2& rCenter, const Vector2& rExtents)
        : BoundingArea2( BA_AABB2 )
        , base( rCenter, rExtents )
    {
    }

    AABB2::AABB2(Real x, Real y, Real width, Real height)
        : BoundingArea2( BA_AABB2 )
        , base( Vector2( x, y ), Vector2( width / 2, height / 2 ) )
    {
    }

    AABB2::AABB2(const Vector2& rExtents)
        : BoundingArea2( BA_AABB2 )
        , base( rExtents )
    {
    }

    bool AABB2::Overlaps(const Vector2* pPoint) const
    {
        const Vector2& min = GetMin();
        if( pPoint->x < min.x )    return false;
        if( pPoint->y < min.y )    return false;

        const Vector2& max = GetMax();
        if( pPoint->x > max.x )    return false;
        if( pPoint->y > max.y )    return false;
        return true;
    }

    void AABB2::Generate( const BoundingArea2* pBA)
    {
        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            Generate((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            Generate((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            Generate((OBB2*)pBA);
    }

    void AABB2::Merge(const BoundingArea2* pBA)
    {
        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            Merge((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            Merge((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            Merge((OBB2*)pBA);
    }

    void AABB2::Merge(const AABB2* pAABB2 )
    {
        if(IsEmpty())
        {
            Generate(pAABB2);
            return;
        }
        const Vector2& mergedMin = pAABB2->GetMin();
        const Vector2& mergedMax = pAABB2->GetMax();
        Vector2 min = GetMin();
        Vector2 max = GetMax();
        if(min.x > mergedMin.x)
            min.x = mergedMin.x;
        if(min.y > mergedMin.y)
            min.y = mergedMin.y;
        if(max.x < mergedMax.x)
            max.x = mergedMax.x;
        if(max.y < mergedMax.y)
            max.y = mergedMax.y;
        SetMinMax(min, max);
    }

    void AABB2::Merge(const Sphere2* pSphere2)
    {
        if( IsEmpty() )
        {
            Generate(pSphere2);
            return;
        }
        const Vector2& mergedMin = pSphere2->GetMin();
        const Vector2& mergedMax = pSphere2->GetMax();
        Vector2 min = GetMin();
        Vector2 max = GetMax();
        if(min.x > mergedMin.x)
            min.x = mergedMin.x;
        if(min.y > mergedMin.y)
            min.y = mergedMin.y;
        if(max.x < mergedMax.x)
            max.x = mergedMax.x;
        if(max.y < mergedMax.y)
            max.y = mergedMax.y;
        SetMinMax(min, max);
    }

    void AABB2::Merge(const OBB2* pOBB2)
    {
        if( IsEmpty() )
        {
            Generate(pOBB2);
            return;
        }
        const Vector2 vPExt = pOBB2->GetAABBExtent();
        const Vector2& mergedMin = pOBB2->GetCenter() - vPExt;
        const Vector2& mergedMax = pOBB2->GetCenter() + vPExt;
        Vector2 min = GetMin();
        Vector2 max = GetMax();
        if(min.x > mergedMin.x)
            min.x = mergedMin.x;
        if(min.y > mergedMin.y)
            min.y = mergedMin.y;
        if(max.x < mergedMax.x)
            max.x = mergedMax.x;
        if(max.y < mergedMax.y)
            max.y = mergedMax.y;
        SetMinMax(min, max);
    }

    void AABB2::Rotate(Real angle)
    {
        using namespace Math;

        register Real sin, cos;
        sin = Sin(angle);
        cos = Cos(angle);

        Real fMaxX = NumberTraits< Real >::ZERO;
        Real fMaxY = NumberTraits< Real >::ZERO;

        Vector2 kX, kY;
        Vector2 worldExtents( GetExtents() );
        kX.x = Mul(cos, worldExtents.x);
        kX.y = Mul(-sin, worldExtents.x);
        kY.x = Mul(sin, worldExtents.y);
        kY.y = Mul(cos, worldExtents.y);

        Vector2 kTest = kX + kY;
        if( Abs( kTest.x ) > fMaxX )
            fMaxX = Abs( kTest.x );
        if( Abs( kTest.y ) > fMaxY )
            fMaxY = Abs( kTest.y );

        kTest = kX - kY;

        if( Abs( kTest.x ) > fMaxX )
            fMaxX = Abs( kTest.x );
        if( Abs( kTest.y ) > fMaxY )
            fMaxY = Abs( kTest.y );

        worldExtents.x = fMaxX;
        worldExtents.y = fMaxY;

        SetExtents( worldExtents );
    }

    void AABB2::Render(unsigned int color /*= 0xFF0000 */, void* user /* = NULL */ ) const
    {
        const Vector2& pos = GetCenter();
        Vector2 axisX( GetExtents() );
        Vector2 axisY( GetExtents() );
        axisX.y = 0;
        axisY.x = 0;

        Debug::RenderRect( pos, axisX, axisY, color, user );
    }

} // namespace Scene

} // namespace Jungle

// EOF

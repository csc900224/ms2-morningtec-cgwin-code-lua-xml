////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/AABB2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>

// External modules includes
#include <jungle/base/NumberTraits.hpp>
#include <jungle/math/Arithmetic.hpp>       // Abs

namespace Jungle
{
namespace Scene
{
    inline void AABB2::Translate(const Vector2& rOffset)
    {
        SetCenter( GetCenter() + rOffset );
    }

    inline void AABB2::Scale(Real scale)
    {
        SetExtents( GetExtents() * scale );
    }

    inline void AABB2::SetEmpty()
    {
        return Math::AABB2< Real, Vector2 >::SetEmpty();
    }

    inline bool AABB2::IsEmpty() const
    {
        return Math::AABB2< Real, Vector2 >::IsEmpty();
    }

    inline void AABB2::SetPoint(const Vector2& rPos)
    {
        Math::AABB2< Real, Vector2 >::SetPoint( rPos );
    }

    inline bool AABB2::IsPoint() const
    {
        return Math::AABB2< Real, Vector2 >::IsPoint();
    }

    bool AABB2::Intersect(const BoundingArea2* pBA) const
    {
        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Intersect((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Intersect((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Intersect((OBB2*)pBA);

        return false;
    }

    bool AABB2::Intersect(const AABB2* pAABB2) const
    {
#ifdef AABB_USE_MIN_MAX
        return  GetMin().x < pAABB2->GetMax().x &&
                GetMin().y < pAABB2->GetMax().y &&
                GetMax().x > pAABB2->GetMin().x &&
                GetMax().y > pAABB2->GetMin().y;
#else
        Real distX = GetCenter().x - pAABB2->GetCenter().x;
        Real distY = GetCenter().y - pAABB2->GetCenter().y;
        return  (Math::Abs(distX) < GetExtents().x + pAABB2->GetExtents().x) &&
                (Math::Abs(distY) < GetExtents().y + pAABB2->GetExtents().y);
#endif
    }

    bool AABB2::Intersect(const Sphere2* pSphere2) const
    {
        return pSphere2->Intersect(this);
    }

    bool AABB2::Intersect(const OBB2* pOBB2) const
    {
        return pOBB2->Intersect(this);
    }

    bool AABB2::Overlaps(const BoundingArea2* pBA) const
    {
        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Overlaps((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Overlaps((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Overlaps((OBB2*)pBA);

        return false;
    }

    bool AABB2::Overlaps(const AABB2* pAABB2) const
    {
        const Vector2& min = GetMin();
        const Vector2& rMin = pAABB2->GetMin();
        if( rMin.x < min.x )    return false;
        if( rMin.y < min.y )    return false;

        const Vector2& max = GetMax();
        const Vector2& rMax = pAABB2->GetMax();
        if( rMax.x > max.x )    return false;
        if( rMax.y > max.y )    return false;
        return true;
    }

    bool AABB2::Overlaps(const Sphere2* pSphere2) const
    {
        const Vector2& min = GetMin();
        const Vector2& rMin = pSphere2->GetMin();
        if( rMin.x < min.x )    return false;
        if( rMin.y < min.y )    return false;

        const Vector2& max = GetMax();
        const Vector2& rMax = pSphere2->GetMax();
        if( rMax.x > max.x )    return false;
        if( rMax.y > max.y )    return false;
        return true;
    }

    bool AABB2::Overlaps(const OBB2* pOBB2) const
    {
        const Vector2& min = GetMin();
        const Vector2& pn = pOBB2->GetAABBExtent();
        const Vector2& rMin = pOBB2->GetCenter() - pn;
        if( rMin.x < min.x )    return false;
        if( rMin.y < min.y )    return false;

        const Vector2& max = GetMax();
        const Vector2& rMax = pOBB2->GetCenter() + pn;
        if( rMax.x > max.x )    return false;
        if( rMax.y > max.y )    return false;
        return true;
    }

    void AABB2::Generate(const AABB2* pAABB2 )
    {
#ifdef AABB_USE_MIN_MAX
        SetMinMax( pAABB2->GetMin(), pAABB2->GetMax() );
#else
        SetCenter( pAABB2->GetCenter() );
        SetExtents( pAABB2->GetExtents() );
#endif
    }

    void AABB2::Generate(const Sphere2* pSphere2 )
    {
        SetCenterExtents( pSphere2->GetCenter(), Vector2( pSphere2->GetRadius(), pSphere2->GetRadius() ) );
    }

    void AABB2::Generate(const OBB2* pOBB2 )
    {
        SetCenterExtents( pOBB2->GetCenter(), pOBB2->GetAABBExtent() );
    }

    bool AABB2::BoundToArea(Vector2& rPos) const
    {
        bool ret = false;
        const Vector2& min = GetMin();
        const Vector2& max = GetMax();

        if(rPos.x < min.x)      { rPos.x = min.x; ret = true; }
        else if(rPos.x > max.x) { rPos.x = max.x; ret = true; }

        if(rPos.y < min.y)      { rPos.y = min.y; ret = true; }
        else if(rPos.y > max.y) { rPos.y = max.y; ret = true; }

        return ret;
    }

} // namespace Scene

} // namespace Jungle

// EOF

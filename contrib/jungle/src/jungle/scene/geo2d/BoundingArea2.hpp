////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/BoundingArea2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__BOUNDING_AREA_HPP__
#define __SCENE__GEO2D__BOUNDING_AREA_HPP__

// Internal includes
#include <jungle/scene/BoundingObject.hpp>
#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
    class BoundingArea2 : public BoundingObject< Vector2 >
    {
    public:
        enum BAType
        {
            //! Axis-aligned rectangle
            BA_AABB2,
            //! Object oriented rectangle
            BA_OBB2,
            //! Bounding circle
            BA_SPHERE2,
            //! Closed polygon shape
            BA_POLY2,
        }; // enum BAType

        /*!
        * Get bounding area type identifier
        * \return type of bounding area
        */
        inline BAType   GetType() const;

        /*!
        * Check intersection with other bounding object
        * \param pBA    Tested bounding object pointer
        */
        virtual bool    Intersect(const BoundingArea2* pBA) const = 0;

        /*!
        * Check if passed object fully fits inside (may be enclosed in) the current volume
        * \paream pBA   bounding object tested for beeing overlapped
        * \return true if this object totally overlaps volume of the object passed in parameter
        */
        virtual bool    Overlaps(const BoundingArea2* pBA) const = 0;

        /*!
        * Check if point passed is inside bounding volume
        * \paream pPoint point tested for beeing overlapped
        * \return true if this object totally overlaps point passed as the parameter
        */
        virtual bool    Overlaps(const Vector2* pPoint) const = 0;

        /*!
        * Generate from bounding object from any other volume
        * \param pBA    Source bounding area
        */
        virtual void    Generate( const BoundingArea2* pBA) = 0;

        /*!
        * Merge bounding object with another volume of any type
        * \param pBA    Merged bounding volume
        */
        virtual void    Merge(const BoundingArea2* pBA) = 0;

        /*!
        * Translates bounding area center
        * \param rPos offset to apply to geometric area center
        */
        virtual void    Translate(const Vector2& rPos) = 0;

        /*!
        * Scales bounding area size
        * \param scale factor
        */
        virtual void    Scale(Real scale) = 0;

        /*!
        * Sets bounding area rotation angle
        * \param fixed angle varying from <0, 180>
        */
        virtual void    Rotate(Real angle) = 0;

        //! Helper method to show rectangle representation.
        /*!
        * This will use Scene::DebugRenderer for shape rendering.
        * Before use make sure that you attached some RendererDebug instance
        * to Scene module via Scene::Debug::Renderer::Register() method.
        */
        virtual void    Render(unsigned int color = 0x00FF00, void* user = NULL) const = 0;

        //! Factory method
        static
        BoundingArea2*  CreateInstance(BAType baType = BA_AABB2);

    protected:
        inline          BoundingArea2(BAType baType);

        const BAType    m_type;

    }; // class BoundingArea2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/BoundingArea2.inl>

#endif // !defined __SCENE__GEO2D__BOUNDING_AREA_HPP__
// EOF

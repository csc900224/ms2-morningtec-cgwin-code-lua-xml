////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Segment2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__SEGMENT2_HPP__
#define __SCENE__GEO2D__SEGMENT2_HPP__

// Internal includes
#include <jungle/scene/geo2d/AbstractLine2.hpp>
#include <jungle/math/geo/Segment2.hpp>

#include <jungle/scene/SceneTypes.hpp>

// External includes

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class BoundingArea2;
    class Sphere2;
    class AABB2;
    class OBB2;

    class Segment2 : public AbstractLine2, public Math::Segment2< Real, Vector2 >
    {
        typedef Math::Segment2< Real, Vector2 >     base;

    public:
        /*!
        * Default constructor, doesn't reset segment direction, origin or end
        */
        inline                  Segment2()
                                : base()
        {}

        /*!
        * Initializing constructor, sets segment direction, position and size
        * \param origin         segment start position
        * \param dir            direction
        * \param extent         length
        */
        inline                  Segment2(const Vector2& rOrigin, const Vector2& rDir, const Real& length)
                                : base( rOrigin, rDir, length )
        {}

        /*!
        * Initializing constructor, sets segment origin in (0,0), its' direction using x and y coordinates and length.
        * \param x              x direction
        * \param y              y direction coordinate
        * \param lenght         segment length
        */
        inline                  Segment2(Real xDir, Real yDir, Real length)
                                : base( Vector2( 0, 0 ), Vector2( xDir, yDir ), length )
        {}

        /*!
        * Check intersection with bounding shape of any type
        * \param pBA            tested bounding shape pointer
        * \param pContactSet    contact info pointer to be set if instersection detected
        */
        virtual bool            Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet = NULL) const;

        /*!
        * Segment intersection with a circle area
        * \param pSphere2       circle tested for intersection
        * \param pContactSet    contact info pointer to be set if instersection detected
        */
        bool                    Intersect(const Sphere2* pSphere2, ContactSet2* pContactSet = NULL) const;

        /*!
        * Segment intersection test with axis aligned rectangle
        * \param pAABB2         tested axis aligned box
        * \param pContactSet    output contact set info pointer
        */
        bool                    Intersect(const AABB2* pAABB2, ContactSet2* pContactSet = NULL) const;

        /*!
        * Intersection test with oriented rectangle
        * \param pOBB2          oriented rectangle tested for intersection with
        * \param pContactSet    output contact set info pointer
        */
        bool                    Intersect(const OBB2* pOBB2, ContactSet2* pContactSet = NULL) const;

        /*!
        * Helper method to draw symbolic segment representation as a vector pointing from the orgin to its end
        */
        virtual void            Render(unsigned int color = 0xFF0000, void* user = NULL ) const;

    }; // class Segment2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/Segment2.inl>

#endif // !defined __SCENE__GEO2D__SEGMENT2_HPP__
// EOF

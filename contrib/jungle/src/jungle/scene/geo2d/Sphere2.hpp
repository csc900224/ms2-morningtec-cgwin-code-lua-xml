////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Sphere2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__SPHERE2_HPP__
#define __SCENE__GEO2D__SPHERE2_HPP__

// Internal includes
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/math/geo/Sphere2.hpp>

#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
    class AABB2;
    class OBB2;

    class Sphere2 : public BoundingArea2, public Math::Sphere2< Real, Vector2 >
    {
        typedef Math::Sphere2< Real, Vector2 >  base;

    public:
        /*!
        * Default constructor, sets circle position to (0, 0) and radius to 1
        */
                        Sphere2();

        /*!
        * Coping constructor
        * \param pSphere2 pointer to another boudning circle
        */
                        Sphere2(const Sphere2* pSphere2);

        /*!
        * Generator. Constructor that creates bounding area based on another bounding shape
        * \param pBA reference bounding shape to be copied
        */
                        Sphere2(const BoundingArea2* pBA);

        /*!
        * Construct bounding circle based on a center position and radius passed to it.
        * \param rCenter rectangle center position
        * \param radius  circle size 
        */
                        Sphere2(const Vector2& rCenter, Real radius);

        /*!
        * Construct bounding circle based on a center position and radius passed to it.
        * \param xPos center position x coordinate
        * \param yPos center position on Y axis
        * \param radius  circle size 
        */
                        Sphere2(Real xPos, Real yPos, Real radius);

        /*!
        * Construct bounding circle centered in (0, 0) point with specified radius
        * given by constructor parameter
        * \param radius  circle size
        */ 
        explicit        Sphere2(Real radius);

        /*!
        * Check intersection with other bounding shape
        * \param pBA    Tested bounding area pointer
        */
        inline
        virtual bool    Intersect(const BoundingArea2* pBA) const;

        /*!
        * Intersection test with another circle area
        * \param pSphere2 Tested shape
        */
        inline bool     Intersect(const Sphere2* pSphere2) const;

        /*!
        * Intersection test with axis aligned rectangle
        * \param pAABB2 Tested shape
        */
        bool            Intersect(const AABB2* pAABB2) const;

        /*!
        * Intersection test with object oriented rectangle
        * \param pOBB2  Tested rectangle
        */
        inline bool     Intersect(const OBB2* pOBB2) const;

        /*!
        * Overlapping test with any other bounding geometry
        * \param pBA    Tested bounding geometry pointer
        */
        inline
        virtual bool    Overlaps(const BoundingArea2* pBA) const;

        /*!
        * Overlapping test with another circle
        * \param pSphere2 Tested circle pointer
        */
        inline bool     Overlaps(const Sphere2* pSphere2) const;

        /*!
        * Overlapping test with axis aligned rectangle
        * \param pAABB2 Tested rectangle pointer
        */
        inline bool     Overlaps(const AABB2* pAABB2) const;

        /*!
        * Overlapping test with object oriented rectangle
        * \param pOBB2  Tested rectangle pointer
        */
        inline bool     Overlaps(const OBB2* pOBB2) const;

        /*!
        * Check if point lays inside the bounding area
        * \param pPoint Tested point position
        */
        inline
        virtual bool    Overlaps(const Vector2* pPoint) const;

        /*!
        * Generate from bounding surface from any type of surface
        * \param pBA    Source bounding area object
        */
        virtual void    Generate( const BoundingArea2* pBA );

        /*!
        * Generate circle shape from another circle.
        * Basically this method works similarly to copying constructor.
        * \param pSphere2 Source circle
        */
        inline void     Generate( const Sphere2* pSphere2 );

        /*!
        * Generate bounding area from axis aligned rectangle
        * Resulting bounding shape will be circumcircled onto rectangle creating 
        * bigger area and of course less accurately describing reference shape.  
        * \param pAABB2 Source axis aligned rectangle
        */
        inline void     Generate(const AABB2* pAABB2);

        /*!
        * Generate bounding area from object oriented rectangle
        * \param pOBB2  Source rectangle
        */
        inline void     Generate(const OBB2* pOBB2);

        /*!
        * Merge bounding surface with another surface of any type
        * \param pBA    Merged bounding surface
        */
        virtual void    Merge(const BoundingArea2* pBA);

        /*!
        * Merge circle with another one.
        * \param pSphere2 Merged circle
        */
        void            Merge(const Sphere2* pSphere2);

        /*!
        * Merge circle with an axis aligned rectangle
        * \param pAABB2 Merged rectangle
        */
        inline void     Merge(const AABB2* pAABB2);

        /*!
        * Merge circle with an object oriented rectangle
        * \param pOBB2  Merged rectangle
        */
        inline void     Merge(const OBB2* pOBB2);

        /*!
        * Moves center of a circle.
        * \param rOffset circle center translation
        */
        inline
        virtual void    Translate(const Vector2& rOffset);

        /*!
        * Method added for compatibility reasons. It doesn't actually change circle shape.
        * \param angle radius angle varing from <0, 2PI>
        */
        inline
        virtual void    Rotate(Real radAngle);

        /*!
        * Scales bounding area about specified scale factor.
        * \note This method cumulates scaling with previous transformations.
        * \param scale scaling factor
        */
        inline
        virtual void    Scale(Real scale);

        /*!
        * Set bounding area to smallest possible size - do not contain anything
        */
        inline
        virtual void    SetEmpty();

        /*!
        * Check if area is totally empty and can not contain anything. 
        * Scaling and rotating shape doesn't affect it's empty status.
        */
        inline
        virtual bool    IsEmpty() const;

        /*!
        * Set bounding area to contain single point with coordinates passed to method
        * \param rPos   new bounding area coordinates center 
        */
        inline
        virtual void    SetPoint(const Vector2& rPos );

        /*!
        * Check if area is point size.
        */
        inline
        virtual bool    IsPoint() const;

        /*!
        * Helper method to show sphere representation
        */
        virtual void    Render(unsigned int color = 0xFF0000, void* user = NULL ) const;

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for Sphere2 *= float. Scales the radius, keeps same center.
        inline Sphere2&  operator*=(Real s)
        {
            base::operator*=( s );
            return *this;
        }

        //! Operator for Sphere2 /= float. Scales the radius, keeps same center.
        inline Sphere2&  operator/=(Real s)
        {
            base::operator/=( s );
            return *this;
        }

        //! Operator for Sphere2 += rTrans. Translates shape keeping it's size.
        inline Sphere2&  operator+=(const Vector& rTrans)
        {
            base::operator+=( rTrans );
            return *this;
        }

        //! Operator for Sphere2 -= rTrans. Translates shape keeping it's radius.
        inline Sphere2&  operator-=(const Vector& rTrans)
        {
            base::operator-=( rTrans );
            return *this;
        }

        //! Merge operator, adds two bounding areas. Sphere2 += Sphere2.
        inline Sphere2& operator+=(const Sphere2& rSphere2)
        {
            Merge(&rSphere2);
            return *this;
        }

        //! Assignment operator
        inline Sphere2& operator=(const Sphere2& rSphere2)
        {
            Generate( &rSphere2 );
            return *this;
        }

    }; // class Sphere2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/Sphere2.inl>

#endif // !defined __SCENE__GEO2D__SPHERE2_HPP__
// EOF

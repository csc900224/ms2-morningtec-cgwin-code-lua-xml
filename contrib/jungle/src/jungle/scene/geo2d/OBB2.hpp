////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/OBB2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__OBB2_HPP__
#define __SCENE__GEO2D__OBB2_HPP__

// Internal includes
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/math/geo/OBB2.hpp>

#include <jungle/scene/SceneTypes.hpp>

// External includes

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class AABB2;
    class Sphere2;

    class OBB2 : public BoundingArea2, public Math::OBB2< Real, Vector2 >
    {
        typedef Math::OBB2< Real, Vector2 >  base;

    public:
        /*!
        * Default constructor, sets rectangle position to (0, 0) and makes it empty (minimal size)
        */
        inline          OBB2();

        /*!
        * Coping constructor
        * \param pAABB2 pointer to another AABB2
        */
        inline          OBB2(const OBB2* pOBB2);

        /*!
        * Generator. Constructor that creates bounding area based on another bounding shape
        * \param pBA reference bounding shape to be copied
        */
        inline          OBB2(const BoundingArea2* pBA);


        /*!
        * Create empty oriented rectangle based on the center position.
        * \param extents rectangle extents size
        */
        explicit inline OBB2(const Vector2& extents);

        /*!
        * Create oriented rectangle based on the center position and its extent in neutral orientation (axis aligned).
        * \param center rectangle center
        * \param extent x,y lenghts stored in vector
        */
        inline          OBB2(const Vector2& center, const Vector2& extent);

        /*!
        * Create oriented rectangle based on the major/minor axis, center point and extents lenght.
        * \param center rectangle center
        * \param extent lenght stored in vector
        * \param axisX major axis vector
        * \param axisY minor axis vector
        */
        inline          OBB2(const Vector2& center, const Vector2& extent, const Vector2& axisX, const Vector2& axisY);

        /*!
        * Intersection test with abstract BoundingArea2 object, type determination and casting is preformed internally.
        * \param pObj   bounding volume object to test for intersection with
        * \return true if volumes intersect, false if not
        */
        inline
        virtual bool    Intersect( const BoundingArea2 *pBV ) const;

        /*!
        * Intersection with AABB2
        * \param pAABB2 Axis aligned rectangle
        * \return true if the AABB intersects with this object, false if not
        */
        inline bool     Intersect( const AABB2* pAABB2 ) const;

        /*!
        * Intersection with circle
        * \param pSphere2 Tested circle shape.
        * \return true if sphere intersects with this object, false if not
        */
        inline bool     Intersect( const Sphere2* pSphere2 ) const;

        /*!
        * Intersection test with another OBB2
        * \param pOBB2  other OBB2 volume
        * \return true if the OBB2 intersects with this object, false if not
        */
        bool            Intersect( const OBB2* pOBB2 ) const;

        /*!
        * Overlapping test with other bounding shape
        * \param pBA    Tested bounding geometry pointer
        */
        inline
        virtual bool    Overlaps(const BoundingArea2* pBA) const;

        /*!
        * Check if point lays inside the bounding area
        * \param pPoint Tested point position
        */
        inline
        virtual bool    Overlaps(const Vector2* pPoint) const;

        /*!
        * Generate OBB2 from any other recognizable bounding volume
        * \paream pBV   Pointer to abstract bounding area class.
        */
        virtual void    Generate( const BoundingArea2* pBV );

        /*!
        * Generate from axis aligned rectangle
        * \param pAABB2 Reference axis aligned rectangle.
        */
        void            Generate( const AABB2* pAABB2 );

        /*!
        * Generate oriented rectangle from Sphere2.
        * This resets all transforms and generates simple square (OBB2 without transfroms)
        * based on circle radius.
        * \param pSphere2 Source Sphere2 object
        */
        void            Generate( const Sphere2* pSphere2 );

        /*!
        * Generate OBB2 from another OBB2.
        * Works like copy constructor, inheriging all object properties (both with transformations).
        * \param pOBB2              Source OBB2 object
        */
        void            Generate( const OBB2* pOBB2 );

        /*!
        * Merge with any type of bounding volume
        * \param pBV    [in] Source bounding volume
        */
        virtual void    Merge( const BoundingArea2* pBV );

        /*!
        * Merge OBB2 with axis aligned rectangle
        * \param pAABB2 [in] merged area
        */
        void            Merge( const AABB2* pAABB2 );

        /*!
        * Merge oriented rectangle with other input Sphere2 shape
        * \param pSphere2 [in] merged area
        */
        void            Merge( const Sphere2* pSphere2 );

        /*!
        * Merge OBB2 with another oriented rectangle
        * \param pOBB2 [in] merged area
        */
        void            Merge( const OBB2* pOBB2 );

        /*!
        * Moves center of a bounding area.
        * \param rOffset box translation offset
        */
        inline
        virtual void    Translate(const Vector2& rOffset);

        /*!
        * Rotates bounding area about specified angle [radians].
        * \note This method uses current rotation angle and cumulates new rotation
        * with previous transformations.
        * \param angle varying from <0, 2*PI>
        */
        inline
        virtual void    Rotate(Real radAngle);

        /*!
        * Scales bounding area about specified scale factor.
        * \note This method cumulates scaling with previous transformations.
        * \param scale positive scaling factor
        */
        inline
        virtual void    Scale(Real scale);

        /*!
        * Sets bounding shape to zero size, axis aligned (not rotated) OBB2
        */
        inline
        virtual void    SetEmpty();

        /*!
        * Check if area is totally empty and can not contain anything
        * Scaling and rotating shape doesn't affect it's empty status.
        */
        inline
        virtual bool    IsEmpty() const;

        /*!
        * Set bounding area to contain single point with coordinates passed to method
        * \param rPos   new bounding area coordinates center 
        */
        inline
        virtual void    SetPoint(const Vector2& rPos );

        /*!
        * Check if bounding area forms a single point.
        * Rotation doesn't affect this status but only extents size.
        */
        inline
        virtual bool    IsPoint() const;

        /*!
        * Helper method to show rotated rectangle representation
        */
        virtual void    Render(unsigned int color = 0x00FF00, void* user = NULL) const;

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for OBB2 *= float. Scales the extents, keeps same center.
        inline OBB2&     operator*=(Real s)
        {
            base::operator/=( s );
            return *this;
        }

        //! Operator for OBB2 /= float. Scales the extents, keeps same center.
        inline OBB2&     operator/=(Real s)
        {
            base::operator/=( s );
            return *this;
        }

        //! Operator for OBB2 += rTrans. Translates the rectangle keeping the extents.
        inline OBB2&     operator+=(const Vector& rTrans)
        {
            base::operator+=( rTrans );
            return *this;
        }

        //! Operator for OBB2 -= rTrans. Translates the rectangle keeping it's extents.
        inline OBB2&     operator-=(const Vector& rTrans)
        {
            base::operator-=( rTrans );
            return *this;
        }

        //! Merge operator, adds two bounding areas. OBB2 += OBB2.
        inline OBB2&   operator+=(const OBB2& rOBB2)
        {
            Merge( &rOBB2 );
            return *this;
        }

        //! Assignment operator
        inline OBB2&    operator=(const OBB2& rOBB2)
        {
            Generate( &rOBB2 );
            return *this;
        }

    }; // class OBB2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/OBB2.inl>

#endif // !defined __SCENE__GEO2D__OBB2_HPP__
// EOF

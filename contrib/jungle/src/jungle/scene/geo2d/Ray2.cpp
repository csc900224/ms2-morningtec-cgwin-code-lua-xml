////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Ray2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/Ray2.hpp>
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>
#include <jungle/scene/geo2d/Line2.hpp>
#include <jungle/scene/geo2d/Segment2.hpp>

#include <jungle/scene/SceneDebug.hpp>

#include <jungle/base/Errors.hpp>       // JUNGLE_ASSERT

namespace Jungle
{
namespace Scene
{
    bool Ray2::Intersect(const Sphere2* pSphere2, ContactSet2* pContactSet /* = NULL */) const
    {
        using namespace Math;

        if( !pContactSet )
        {
            // Get distance vector
            Vector2 off = GetOrigin() - pSphere2->GetCenter();
            // Square distance calculation results in big numbers
            // with a strong possibility of overlfow - maybe use 64bit calculation
            Real offSqr = SqrSum2( off.x, off.y );
            Real radSqr = Sqr( pSphere2->GetRadius() );

            // Check if ray starts in the circle
            if(offSqr < radSqr)
            {
                return true;
            }
            // Project ray on the offset vector (calculate dot product)
            Real rayProj = Mul( m_dir.x, off.x ) + Mul( m_dir.y, off.y );
             // Ray is moving away from sphere
            if( rayProj >= 0 )
                return false;

    #ifndef USE_DIRECT_RAY_SPHERE_EQUATION
            // Build line equation with ray direction normalization
            // A*t^2 + B*t + C = 0 where:
            // A = 1 / (m_dir.x^2 + m_dir.y^2)
            // B = 2 * A * (m_dir.x * off.x + m_dir.y * off.y)
            // C = A * ((off.x)^2 + (off.y)^2 - rad^2)
            //Real l = RealSqrt( RealMulAdd(m_dir.x, m_dir.x, m_dir.y, m_dir.y) );
            //m_dir.x = RealDiv( m_dir.x, l );
            //m_dir.y = RealDiv( m_dir.y, l );
            Real A = Div( NumberTraits<Real>::ONE, SqrSum2( m_dir.x, m_dir.y ) );
            Real B = Mul( rayProj, Mul(A, Real(2)) );
            Real C = Mul( offSqr - radSqr, A );

            Real delta = Sqr(B) - Mul( C, Real(4) );
            if( delta < 0 )
                return false;

            Real deltaSqrt = Sqrt( delta );
            // Compute intersection points assuming that A == 1 (ray direction is not normalized)
            // Normalization is processed in A,B,C calculation
            // t0 = t1 = (- B + sqrt(delta)) / 2, t1 = (- B + sqrt(delta)) / 2
            Real T = Min( -Div2( B + deltaSqrt ), -Div2( B - deltaSqrt ) );
    #else  
            // This code in not so accurate and pains with overflows use above code if possible
            // Build line equation
            // A*t^2 + B*t + C = 0 where:
            // A = m_dir.x^2 + m_dir.y^2
            // B = 2 * (m_dir.x * off.x + m_dir.y * off.y)
            // C = (off.x)^2 + (off.y)^2 - rad^2
            Real A = Sqrt( SqrSum2(m_dir.x, m_dir.y) );
            Real B = Mul( rayProj, Real(2) );
            Real C = offSqr - radSqr;

            Real delta = Sqr(B) - Mul( Mul(A, C), Real(4) );
            if( delta < 0 )
                return false;

            Real deltaSqrt = Sqrt( delta );
            // Compute intersection points assuming that A == 1 (ray directory is not normalized)
            // Normalization is processed in A,B,C calculation
            // t0 = t1 = (- B + sqrt(delta)) / 2A, t1 = (- B - sqrt(delta)) / 2A
            Real invA = Div( NumberTraits<Real>::ONE, A );
            Real T = Max( -Div2( RealDiv( B + deltaSqrt, A ) ), -Div2( Div( B - deltaSqrt, A) ) );
    #endif

            if( T < 0 )
            {
                JUNGLE_ASSERT( false );
                return false; // this should happed - assert from here
            }
            //if( T > NumberTraits<Real>::ONE )
            //    return false;

            // TODO: Check T calculation, may it be optimized
            return true;
        }
        else
        {
            Real pointsDist[2];
            unsigned int pointsNum = 0;

            // Specify ray collision parameters
            Real tMin = 0;                                  // since we wil pass segment start point
            Real tMax = NumberTraits< Real >::Maximum();    // no upper distance limit

            bool intersect = Math::Line2< Real, Vector2 >::Intersect( GetOrigin(), GetDir(), pSphere2, tMin, tMax, pointsNum, pointsDist );

            // Gather contact points
            if( intersect && pContactSet )
            {
                // First point should be always closer then the second one, thus finding closest contact
                // doesn't require passing and checking all of them, but simply pushes first contact point.
                // Assertion below will be guarding it if implementation will change.
                JUNGLE_ASSERT( pointsNum < 2 || ( pointsDist[0] < pointsDist[1] ) );

                pointsNum = (pContactSet->GatherFirstOnly() || pContactSet->GatherClosestOnly()) ? 1 : pointsNum;

                if( pContactSet->GatherNormal() )
                {
                    Vector2 contactPos;
                    Vector2 contactNorm;
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        contactPos = GetOrigin() + pointsDist[i] * GetDir();
                        contactNorm = i == 0 ? contactPos - pSphere2->GetCenter() : pSphere2->GetCenter() - contactPos;
                        contactNorm.Normalize();
                        // Distance calculation is for free and required anyway so we are not checking:
                        // if pContactSet->GatherDistance() is enable but store contact distances anyway.
                        pContactSet->Add( ContactPoint2( contactPos, contactNorm, pointsDist[i] ) );
                    }
                }
                else
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // We already have distance parameter so always pass it to contact point without checking if
                        // pContactSet->GatherDistance() is enabled.
                        pContactSet->Add( ContactPoint2( GetOrigin() + pointsDist[i] * GetDir(), pointsDist[i] ) );
                    }
                }
            }
            return intersect;
        }
    }

    bool Ray2::Intersect(const AABB2* pAABB2, ContactSet2* pContactSet /* = NULL */) const
    {
        if( !pContactSet )
        {
            using namespace Math;

            register const Vector2& center = pAABB2->GetCenter();
            register const Vector2& extents = pAABB2->GetExtents();

            // Do not search for intersection - prove that there is no !!! 
            Real dX = GetOrigin().x - center.x;
            if( Abs(dX) > extents.x && Mul(dX, GetDir().x) >=0 )
                return false;

            Real dY = GetOrigin().y - center.y;
            if( Abs(dY) > extents.y && Mul(dY, GetDir().y) >=0 )
                return false;

            Real f = Mul( GetDir().x, dY ) - Mul( GetDir().y, dX );
            Vector2 absDir( Abs(GetDir().x), Abs(GetDir().y) );

            if( Abs(f) > Mul(extents.x, absDir.y) + Mul(extents.y, absDir.x) )
                return false;

            return true;
        }
        else
        {
            Real pointsDist[2];
            Vector pointsNorm[2];
            unsigned int pointsNum = 0;

            // Specify ray range limits
            Real tMin = 0;                                      // since we wil pass ray origin
            Real tMax = NumberTraits< Real >::Maximum();  // infinity, because there is no upper limit for ray

            bool intersect = Math::Line2< Real, Vector >::Intersect( GetOrigin(), GetDir(), pAABB2, tMin, tMax, pointsNum, pointsDist, pContactSet->GatherNormal() ? pointsNorm : NULL );

            // Sanity check only
            if( intersect && pContactSet )
            {
                // Code below assumes that first point is always closer then the second one, thus finding closest contact
                // doesn't require passing and checking all of them, but simply pushes first contact point.
                // Assertion below will guard this condition if somebody change the implementation.
                JUNGLE_ASSERT( pointsNum < 2 || ( pointsDist[0] < pointsDist[1] ) );

                pointsNum = (pContactSet->GatherFirstOnly() || pContactSet->GatherClosestOnly()) ? 1 : pointsNum;
                if( pContactSet->GatherNormal() )
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // We already have distance parameter so always pass it to contact point without checking if
                        // pContactSet->GatherDistance() is enabled.
                        pContactSet->Add( ContactPoint2( GetOrigin() + GetDir() * pointsDist[i], pointsNorm[i], pointsDist[i] ) );
                    }
                }
                else
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // Always pass contact distance cause it is for free - see above notes.
                        pContactSet->Add( ContactPoint2( GetOrigin() + GetDir() * pointsDist[i], pointsDist[i] ) );
                    }
                }
            }
            return intersect;
        }
    }

    bool Ray2::Intersect(const OBB2* pOBB2, ContactSet2* pContactSet /* = NULL */) const
    {
        if( !pContactSet )
        {
            using namespace Math;

            Real dirProj[2], dirProjAbs[2], offProj[2], offProjAbs[2], fRhs;

            const Vector2 vOff = GetOrigin() - pOBB2->GetCenter();
            const Vector2& vOBB2AxisX = pOBB2->GetAxis( 0 );

            dirProj[0] = GetDir().Dot( vOBB2AxisX );
            dirProjAbs[0] = Abs( dirProj[0] );
            offProj[0] = vOff.Dot( vOBB2AxisX );
            offProjAbs[0] = Abs(offProj[0]);
            if( offProjAbs[0] > pOBB2->GetExtentsSize().x && offProj[0] * dirProj[0] >= Real(0.0) )
            {
                return false;
            }

            const Vector2& vOBB2AxisY = pOBB2->GetAxis( 1 );

            dirProj[1] = GetDir().Dot( vOBB2AxisY );
            dirProjAbs[1] = Abs( dirProj[1] );
            offProj[1] = vOff.Dot( vOBB2AxisY );
            offProjAbs[1] = Abs( offProj[1] );
            if( offProjAbs[1] > pOBB2->GetExtentsSize().y && offProj[1] * dirProj[1] >= Real(0.0) )
            {
                return false;
            }

            const Vector2 kPerp = Vector2( -GetDir().y, GetDir().x );

            Real fLhs = Abs(kPerp.Dot(vOff));
            Real fPart0 = Abs(kPerp.Dot( vOBB2AxisX ) );
            Real fPart1 = Abs(kPerp.Dot( vOBB2AxisY ) );
            fRhs = pOBB2->GetExtentsSize().x * fPart0 + pOBB2->GetExtentsSize().y * fPart1;
            return fLhs <= fRhs;
        }
        else
        {
            Real tMin = Real( 0.0 );
            Real tMax = NumberTraits< Real >::Maximum();
            Real pointsDist[2];
            Vector pointsNorm[2];
            unsigned int pointsNum = 0;
            bool intersect = Math::Line2< Real, Vector >::Intersect( GetOrigin(), GetDir(), pOBB2, tMin, tMax, pointsNum, pointsDist, pContactSet->GatherNormal() ? pointsNorm : NULL );

            // Gather contact points
            if( intersect && pContactSet )
            {
                // First point should be always closer then the second one, thus finding closest contact
                // doesn't require passing and checking all of them, but simply pushes first contact point.
                // Assertion below will guard this condition if somebody change the implementation.
                JUNGLE_ASSERT( pointsNum < 2 || ( pointsDist[0] < pointsDist[1] ) );

                pointsNum = (pContactSet->GatherFirstOnly() || pContactSet->GatherClosestOnly()) ? 1 : pointsNum;
                if( pContactSet->GatherNormal() )
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // We already have distance parameter so always pass it to contact point without checking if
                        // pContactSet->GatherDistance() is enabled.
                        pContactSet->Add( ContactPoint2( GetOrigin() + GetDir() * pointsDist[i], pointsNorm[i], pointsDist[i] ) );
                    }
                }
                else
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // Contact distance is for free, alway pass it to contact point - see above notes.
                        pContactSet->Add( ContactPoint2( GetOrigin() + GetDir() * pointsDist[i], pointsDist[i] ) );
                    }
                }
            }
            return intersect;
        }
    }

    bool Ray2::Clip(const AABB2* pAABB2, Segment2* pClipedSegment) const
    {
        if( !pClipedSegment )
            return false;

        float tMin = 0.0f;
        float tMax = NumberTraits< Real >::Maximum();
        int clipCount = Math::Line2< Real, Vector >::Clip( GetOrigin(), GetDir(), pAABB2, tMin, tMax );

        if( clipCount == 0 )
        {
            pClipedSegment->SetOrigin( GetOrigin() );
            pClipedSegment->SetLength( NumberTraits< Real >::Maximum() );
        }
        else if( clipCount == 1 )
        {
            pClipedSegment->SetOrigin( GetOrigin() + tMin * GetDir() );
            pClipedSegment->SetLength( NumberTraits< Real >::Maximum() );
        }
        else // clipCound == 2
        {
            pClipedSegment->SetOrigin( GetOrigin() + tMin * GetDir() );
            pClipedSegment->SetLength( tMax - tMin );
        }
        pClipedSegment->SetDir( GetDir() );
        return clipCount != 0;
    }

    bool Ray2::Clip(const OBB2* pOBB2, Segment2* pClipedSegment) const
    {
        if( !pClipedSegment )
            return false;

        float tMin = 0.0f;
        float tMax = NumberTraits< Real >::Maximum();
        int clipCount = Math::Line2< Real, Vector >::Clip( GetOrigin(), GetDir(), pOBB2, tMin, tMax );

        if( clipCount == 0 )
        {
            pClipedSegment->SetOrigin( GetOrigin() );
            pClipedSegment->SetLength( NumberTraits< Real >::Maximum() );
        }
        else if( clipCount == 1 )
        {
            pClipedSegment->SetOrigin( GetOrigin() + tMin * GetDir() );
            pClipedSegment->SetLength( NumberTraits< Real >::Maximum() );
        }
        else // clipCound == 2
        {
            pClipedSegment->SetOrigin( GetOrigin() + tMin * GetDir() );
            pClipedSegment->SetLength( tMax - tMin );
        }
        pClipedSegment->SetDir( GetDir() );
        return clipCount != 0;
    }

    void Ray2::Render(unsigned int color /*= 0xFF0000*/, void* user /*= NULL*/ ) const
    {
#       define DEBUG_RAY_BASE_LENGTH    10
#       define DEBUG_RAY_LENGTH         250

        using namespace Math;

        const Vector2& origin = GetOrigin();
        Vector2 dest(GetDir());

        // Normalize direction - do not use normalize method of Vector2 - it will overflow
        Real len = Sqrt( SqrSum2(dest.x, dest.y) );
        dest.x = Div(dest.x, len);
        dest.y = Div(dest.y, len);

        Vector2 baseDiv2, base;
        base.x = -dest.y;
        base.y = dest.x;
        baseDiv2 = base;
        base *= Real(DEBUG_RAY_BASE_LENGTH);
        baseDiv2 *= Real(DEBUG_RAY_BASE_LENGTH/2);
        dest *= Real(DEBUG_RAY_LENGTH);

        Debug::RenderSegment( origin, origin + dest, color, user );
        Debug::RenderSegment( origin - baseDiv2, origin + baseDiv2, color, user );
    }

} // namespace Scene

} // namespace Jungle

// EOF


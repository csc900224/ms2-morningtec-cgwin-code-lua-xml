////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/ThickRay2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

namespace Jungle
{
namespace Scene
{
    inline void ThickRay2::SetThickness(Real thickness)
    {
        m_thickness = thickness;
    }

    inline Real ThickRay2::GetThickness() const
    {
        return m_thickness;
    }

} // namespace Scene

} // namespace Jungle
// EOF

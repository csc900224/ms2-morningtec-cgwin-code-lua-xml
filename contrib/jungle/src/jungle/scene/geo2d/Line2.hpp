////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Line2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__LINE2_HPP__
#define __SCENE__GEO2D__LINE2_HPP__

// Internal includes
#include <jungle/scene/geo2d/AbstractLine2.hpp>
#include <jungle/scene/SceneTypes.hpp>

#include <jungle/math/geo/Line2.hpp>

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class BoundingArea2;

    class Line2 : public AbstractLine2, public Math::Line2< Real, Vector2 >
    {
    public:
        /*!
        * Check for intersection with any bounding shape, optionaly find intersection points.
        * \param pBS            Tested bounding shape pointer.
        * \param pContactSet    if not NULL will store intersection points.
        */
        virtual bool            Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet = NULL) const = 0;

    }; // class Line2

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE__GEO2D__LINE2_HPP__
// EOF

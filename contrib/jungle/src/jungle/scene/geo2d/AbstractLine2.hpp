////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/AbstractLine2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__ABSTRACT_LINE2_HPP__
#define __SCENE__GEO2D__ABSTRACT_LINE2_HPP__

// Internal includes
#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class BoundingArea2;

    class AbstractLine2
    {
    public:
        typedef Vector2         Vector;

        /*!
        * Check for intersection with any bounding shape, optionaly find intersection points.
        * \param pBS            Tested bounding shape pointer.
        * \param pContactSet    if not NULL will store intersection points.
        */
        virtual bool            Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet = NULL) const = 0;

        /*!
        * Helper method to draw symbolic line representation, depending on the derived class it may have different origin and length.
        */
        virtual void            Render(unsigned int color = 0xFF0000, void* user = NULL ) const = 0;

    }; // class AbstractLine2

} // namespace Scene

} // namespace Jungle

#endif // !defined __SCENE__GEO2D__ABSTRACT_LINE2_HPP__
// EOF

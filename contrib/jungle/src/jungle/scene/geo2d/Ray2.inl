////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Ray2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/BoundingArea2.hpp>

namespace Jungle
{
namespace Scene
{
    bool Ray2::Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet /* = NULL */) const
    {
        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Intersect( (AABB2*)pBA, pContactSet );
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Intersect( (Sphere2*)pBA, pContactSet );
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Intersect( (OBB2*)pBA, pContactSet );
        return false;
    }

} // namespace Scene

} // namespace Jungle

// EOF

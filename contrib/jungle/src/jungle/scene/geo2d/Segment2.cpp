////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Segment2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/Segment2.hpp>
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>
#include <jungle/scene/geo2d/Line2.hpp>

#include <jungle/scene/SceneDebug.hpp>

#include <jungle/base/Errors.hpp>       // JUNGLE_ASSERT

namespace Jungle
{
namespace Scene
{
    bool Segment2::Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet /* = NULL */) const
    {
        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Intersect( (AABB2*)pBA, pContactSet );
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Intersect( (Sphere2*)pBA, pContactSet );
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Intersect( (OBB2*)pBA, pContactSet );
        return false;
    }

    bool Segment2::Intersect(const Sphere2* pSphere2, ContactSet2* pContactSet /* = NULL */) const
    {
        Real pointsDist[2];
        unsigned int pointsNum = 0;

        // Specify segment collision distance limits
        Real tMin = 0;              // since we wil pass segment start point
        Real tMax = GetLength();    // upper distance limit is the segment length

        bool intersect = Math::Line2< Real, Vector2 >::Intersect( GetStart(), GetDir(), pSphere2, tMin, tMax, pointsNum, pointsDist );

        // Sanity check only
        if( intersect && pContactSet )
        {
            // We assume that first point is always closer then the second one, thus finding closest contact
            // doesn't require passing and checking all of them, but simply pushes first contact point.
            // Assertion below will be guarding it if implementation will change.
            JUNGLE_ASSERT( pointsNum < 2 || ( pointsDist[0] < pointsDist[1] ) );

            pointsNum = (pContactSet->GatherFirstOnly() || pContactSet->GatherClosestOnly()) ? 1 : pointsNum;

            if( pContactSet->GatherNormal() )
            {
                Vector2 contactPos;
                Vector2 contactNorm;
                for( unsigned int i = 0; i < pointsNum; ++i )
                {
                    contactPos = GetOrigin() + pointsDist[i] * GetDir();
                    contactNorm = i == 0 ? contactPos - pSphere2->GetCenter() : pSphere2->GetCenter() - contactPos;
                    contactNorm.Normalize();
                    // Distance calculation is for free and required anyway so we are not checking:
                    // if pContactSet->GatherDistance() is enable but store contact distances anyway.
                    pContactSet->Add( ContactPoint2( contactPos, contactNorm, pointsDist[i] ) );
                }
            }
            else
            {
                for( unsigned int i = 0; i < pointsNum; ++i )
                {
                    // Distance calculation is for here free so always pass distance to param in the contact point.
                    pContactSet->Add( ContactPoint2( GetOrigin() + pointsDist[i] * GetDir(), pointsDist[i] ) );
                }
            }
        }
        return intersect;
    }

    bool Segment2::Intersect(const AABB2* pAABB2, ContactSet2* pContactSet /* = NULL */) const
    {
        // Simple and fast detection method
        if( !pContactSet )
        {
            using namespace Math;

            register Vector2 center = pAABB2->GetCenter();
            register Vector2 extents = pAABB2->GetExtents();

            // Do not search for intersection - prove that there is no !!! 
            Real Dx = m_origin.x - center.x;    if(Abs(Dx) > extents.x && Mul(Dx, m_dir.x) >=0) return false;
            Real Dy = m_origin.y - center.y;    if(Abs(Dy) > extents.y && Mul(Dy, m_dir.y) >=0) return false;

            Real f;
            Vector2 absDir;
            absDir.Set( Abs(m_dir.x), Abs(m_dir.y) );
            f = Mul(m_dir.x, Dy) - Mul(m_dir.y, Dx);
            if( Abs(f) > Mul(extents.x, absDir.y) + Mul(extents.y, absDir.x) )
                return false;

            return true;
        }
        // Generic approach with contact points calculation
        else
        {
            Real pointsDist[2];
            Vector2 pointsNorm[2];
            unsigned int pointsNum = 0;

            // Specify segment collision distance limits
            Real tMin = 0;              // since we wil pass segment start point
            Real tMax = GetLength();    // upper distance limit is the segment length

            bool intersect = Math::Line2< Real, Vector2 >::Intersect( GetStart(), GetDir(), pAABB2, tMin, tMax, pointsNum, pointsDist, pContactSet->GatherNormal() ? pointsNorm : NULL );

            // Sanity check only
            if( intersect && pContactSet )
            {
                // First point is always closer then the second one (see Line::Intersect algorithm),
                // thus finding closest contact doesn't require passing and checking all of them, but we may
                // simply push the first contact in the set.
                // Assertion below will be guarding it if implementation will change.
                JUNGLE_ASSERT( pointsNum < 2 || ( pointsDist[0] < pointsDist[1] ) );

                // Intersection points are naturally ordered in the Line::Intersect method, sush as first point is always closer,
                // so there is no need to sort them, even if closest point gathering is enabled (pContactSet->GatherClosestOnly() returns true).
                pointsNum = (pContactSet->GatherFirstOnly() || pContactSet->GatherClosestOnly()) ? 1 : pointsNum;

                if( pContactSet->GatherNormal() )
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // Distance calculation is for free and required anyway so we are not checking:
                        // if pContactSet->GatherDistance() is enable but store contact distances anyway.
                        pContactSet->Add( ContactPoint2( GetOrigin() + GetDir() * pointsDist[i], pointsNorm[i], pointsDist[i] ) );
                    }
                }
                else
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // Distance calculus is for free so do not check pContactSet->GatherDistance(), but pass it anyway.
                        pContactSet->Add( ContactPoint2( GetOrigin() + GetDir() * pointsDist[i], pointsDist[i] ) );
                    }
                }
            }
            return intersect;
        }
    }

    bool Segment2::Intersect(const OBB2* pOBB2, ContactSet2* pContactSet /* = NULL */) const
    {
        if( !pContactSet )
        {
            using namespace Math;

            const Vector2 vOff = GetCenter() - pOBB2->GetCenter();
            const Vector2& vOBBAxisX = pOBB2->GetAxis(0);

            Real dirProjAbs[2], offProjAbs[2], fRhs;
            dirProjAbs[0] = Abs( GetDir().Dot( vOBBAxisX ) );
            offProjAbs[0] = Abs( vOff.Dot( vOBBAxisX ) );
            fRhs = pOBB2->GetExtentsSize().x + GetHalfLength() * dirProjAbs[0];
            if( offProjAbs[0] > fRhs )
            {
                return false;
            }

            const Vector2& vOBBAxisY = pOBB2->GetAxis(1);

            dirProjAbs[1] = Abs( GetDir().Dot( vOBBAxisY ) );
            offProjAbs[1] = Abs( vOff.Dot( vOBBAxisY ) );
            fRhs = pOBB2->GetExtentsSize().y + GetHalfLength() * dirProjAbs[1];
            if( offProjAbs[1] > fRhs )
            {
                return false;
            }

            const Vector2 vPerp = GetDir().Perpendicular();
            Real fLhs = Abs( vPerp.Dot(vOff) );
            Real fPart0 = Abs( vPerp.Dot( vOBBAxisX ) );
            Real fPart1 = Abs( vPerp.Dot( vOBBAxisY ) );
            fRhs = pOBB2->GetExtentsSize().x * fPart0 + pOBB2->GetExtentsSize().y * fPart1;

            return fLhs <= fRhs;
        }
        else
        {
            Real tMax = GetLength();
            Real tMin = 0;
            Real pointsDist[2];
            Vector2 pointsNorm[2];
            unsigned int pointsNum = 0;
            const Vector2& origin = GetOrigin();

            bool intersection = Math::Line2< Real, Vector2 >::Intersect( origin, GetDir(), pOBB2, tMin, tMax, pointsNum, pointsDist, pContactSet->GatherNormal() ? pointsNorm : NULL );
            if( intersection )
            {
                // Assume that first point is always closer then the second one (Line::Intersect algorithm),
                // thus finding closest contact doesn't require passing and comparing their distances, but we may
                // simply push the first contact in the set.
                // Assertion below will be guarding it if implementation will change.
                JUNGLE_ASSERT( pointsNum < 2 || ( pointsDist[0] < pointsDist[1] ) );

                // Intersection points are naturally ordered in the Line::Intersect method, sush as first point is always closer,
                // so there is no need to sort them, even if closest point gathering is enabled (pContactSet->GatherClosestOnly() returns true).
                pointsNum = (pContactSet->GatherFirstOnly() || pContactSet->GatherClosestOnly()) ? 1 : pointsNum;
                pointsNum = pContactSet->GatherFirstOnly() ? 1 : pointsNum;

                if( pContactSet->GatherNormal() )
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // Distance calculation is for free and required anyway so we are not checking:
                        // if pContactSet->GatherDistance() is enable but store contact distances anyway.
                        pContactSet->Add( ContactPoint2( origin + GetDir() * pointsDist[i], pointsNorm[i], pointsDist[i] ) );
                    }
                }
                else
                {
                    for( unsigned int i = 0; i < pointsNum; ++i )
                    {
                        // Always pass the distance parameter cause it is for free - see above notes.
                        pContactSet->Add( ContactPoint2( origin + GetDir() * pointsDist[i], pointsDist[i] ) );
                    }
                }
            }
            return intersection;
        }
    }

    void Segment2::Render(unsigned int color /*= 0xFF0000*/, void* user /*= NULL*/ ) const
    {
#       define DEBUG_SEGMENT_BASE_LENGTH    8

        const Vector2& origin = GetOrigin();
        Vector2 dest( GetDir() );
        Vector2 base( GetDir().Perpendicular() );
        dest *= GetLength();
        base *= DEBUG_SEGMENT_BASE_LENGTH;
        Vector2 baseDiv2 = base / Real(2);

        Debug::RenderSegment( origin, origin + dest, color, user );
        Debug::RenderSegment( origin - baseDiv2, origin + baseDiv2, color, user );
    }

} // namespace Scene

} // namespace Jungle
// EOF

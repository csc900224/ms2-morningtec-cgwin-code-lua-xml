////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/ThickSegment2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

namespace Jungle
{
namespace Scene
{
    inline void ThickSegment2::SetThickness(Real thickness)
    {
        m_thickness = thickness;
    }

    inline Real ThickSegment2::GetThickness() const
    {
        return m_thickness;
    }

} // namespace Scene

} // namespace Jungle
// EOF

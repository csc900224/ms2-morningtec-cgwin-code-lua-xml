////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/BoundingArea2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

namespace Jungle
{
namespace Scene
{
    inline BoundingArea2::BAType BoundingArea2::GetType() const
    {
        return m_type;
    }

    inline BoundingArea2::BoundingArea2( BAType baType )
        :m_type( baType )
    {
    }

} // namespace Scene

} // namespace Jungle

// EOF

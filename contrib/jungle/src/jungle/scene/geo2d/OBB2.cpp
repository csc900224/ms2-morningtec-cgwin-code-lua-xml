////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/OBB2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>

#include <jungle/scene/SceneDebug.hpp>

// External includes
#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Scene
{
    bool OBB2::Intersect( const OBB2* pOBB2 ) const
    {
        JUNGLE_ASSERT_MSG( pOBB2, "NULL passed to function OBB2::Intersect" );

        using namespace Math;

        // convenience variables
        const Vector2 vAxesA[] = { GetAxis(0), GetAxis(1) };
        const Vector2 vAxesB[] = { pOBB2->GetAxis(0), pOBB2->GetAxis(1) };
        const Vector2& vExtentsA = GetExtentsSize();
        const Vector2& vExtentsB = pOBB2->GetExtentsSize();

        // compute difference of box centers, D = C1-C0
        Vector2 vDiff = pOBB2->GetCenter() - GetCenter();

        Real aafAbsAdB[2][2];
        Real fAbsAdD, fRSum;

        // axis C0+t*A0
        aafAbsAdB[0][0] = Abs( vAxesA[0].Dot(vAxesB[0]) );
        aafAbsAdB[0][1] = Abs( vAxesA[0].Dot(vAxesB[1]) );
        fAbsAdD = Abs( vAxesA[0].Dot(vDiff) );
        fRSum = vExtentsA[0] + vExtentsB[0]*aafAbsAdB[0][0] + vExtentsB[1]*aafAbsAdB[0][1];
        if (fAbsAdD > fRSum)
        {
            return false;
        }

        // axis C0+t*A1
        aafAbsAdB[1][0] = Abs( vAxesA[1].Dot(vAxesB[0]) );
        aafAbsAdB[1][1] = Abs( vAxesA[1].Dot(vAxesB[1]) );
        fAbsAdD = Abs(vAxesA[1].Dot(vDiff));
        fRSum = vExtentsA[1] + vExtentsB[0]*aafAbsAdB[1][0] + vExtentsB[1]*aafAbsAdB[1][1];
        if( fAbsAdD > fRSum )
        {
            return false;
        }

        // axis C0+t*B0
        fAbsAdD = Abs( vAxesB[0].Dot(vDiff) );
        fRSum = vExtentsB[0] + vExtentsA[0]*aafAbsAdB[0][0] + vExtentsA[1]*aafAbsAdB[1][0];
        if( fAbsAdD > fRSum )
        {
            return false;
        }

        // axis C0+t*B1
        fAbsAdD = Abs( vAxesB[1].Dot(vDiff) );
        fRSum = vExtentsB[1] + vExtentsA[0]*aafAbsAdB[0][1] + vExtentsA[1]*aafAbsAdB[1][1];
        if( fAbsAdD > fRSum )
        {
            return false;
        }

        return true;
    }

    void OBB2::Generate( const BoundingArea2* pBV )
    {
        JUNGLE_ASSERT_MSG( pBV, "NULL passed to function OBB2::Generate" );

        switch( pBV->GetType() )
        {
        case BA_AABB2:
            Generate( (AABB2*)pBV );
            break;

        case BA_SPHERE2:
            Generate( (Sphere2*)pBV );
            break;

        case BA_OBB2:
            Generate( (OBB2*)pBV );
            break;

        default:
            JUNGLE_ASSERT_MSG( false, "UNKNOWN BOUNDING VOLUME PASSED TO OBB2::GENERATE" );
        }
    }

    void OBB2::Generate( const AABB2* pAABB2 )
    {
        // TODO: To be tested
        JUNGLE_ASSERT_MSG( pAABB2, "NULL passed to function OBB2::Generate" );

        SetCenter( pAABB2->GetCenter() );

        // We will lost rotation info and simply copy extents
        SetExtentsSize( pAABB2->GetExtents() );
        // Rotation is lost
        ResetRotation();
    }

    void OBB2::Generate( const Sphere2* pSphere2 )
    {
        JUNGLE_ASSERT_MSG( pSphere2, "NULL passed to function OBB2::Generate" );

        SetCenter( pSphere2->GetCenter() );

        Real sphereRadius = pSphere2->GetRadius();
        SetExtentsSize( Vector2( sphereRadius, sphereRadius ) );

        // Rotation is lost so reset axis
        ResetRotation();
    }

    void OBB2::Generate( const OBB2* pOBB2 )
    {
        JUNGLE_ASSERT_MSG( pOBB2, "NULL passed to function OBB2::Generate" );

        // Simply copy out data
        SetCenterExtentsAndAxes( pOBB2->GetCenter(), pOBB2->GetExtentsSize(), pOBB2->GetAxis( 0 ), pOBB2->GetAxis( 1 ) );
    }

    void OBB2::Merge( const BoundingArea2* pBV )
    {
        JUNGLE_ASSERT_MSG( pBV, "NULL passed to OBB2:Merge" );

        switch( pBV->GetType() )
        {
        case BA_AABB2:
            Merge( (AABB2*)pBV );
        case BA_SPHERE2:
            Merge( (Sphere2*)pBV );
        case BA_OBB2:
            Merge( (OBB2*)pBV );
        default:
            JUNGLE_ASSERT_MSG( false, "Unsupported object type passed to OBB2::Merge method." );
        }
    }

    void OBB2::Merge( const AABB2* pAABB2 )
    {
        JUNGLE_ASSERT_MSG( false, "FUNCTION NOT IMPLEMENTED" );
        JUNGLE_ASSERT_MSG( pAABB2, "NULL passed to method OBB2::Merge" );
    }

    void OBB2::Merge( const Sphere2* pSphere2 )
    {
        JUNGLE_ASSERT_MSG( false, "FUNCTION NOT IMPLEMENTED" );
        JUNGLE_ASSERT_MSG( pSphere2, "NULL passed to method OBB2::Merge" );
    }

    void OBB2::Merge( const OBB2* pOBB2 )
    {
        JUNGLE_ASSERT_MSG( false, "FUNCTION NOT IMPLEMENTED" );
        JUNGLE_ASSERT_MSG( pOBB2, "NULL passed to method OBB2::Merge" );
    }

    void OBB2::Render(unsigned int color /*= 0xFF0000 */, void* user /* = NULL */ ) const
    {
        const Vector2& pos = GetCenter();
        Vector2 axisX( GetAxis(0) );
        Vector2 axisY( GetAxis(1) );
        axisX *= GetExtentsSize().x;
        axisY *= GetExtentsSize().y;

        Debug::RenderRect( pos, axisX, axisY, color, user );
    }

} // namespace Scene

} // namespace Jungle

// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Sphere2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>

#include <jungle/scene/SceneDebug.hpp>

// External modules includes
#include <jungle/base/NumberTraits.hpp>
#include <jungle/math/Arithmetic.hpp>   // Mul, Sqr, Sqrt, Div2

namespace Jungle
{
namespace Scene
{
    Sphere2::Sphere2()
        : BoundingArea2( BA_SPHERE2 )
        , base()
    {
    }

    Sphere2::Sphere2(const Sphere2* pSphere2)
        : BoundingArea2( BA_SPHERE2 )
    {
        Generate(pSphere2);
    }

    Sphere2::Sphere2(const BoundingArea2* pBA)
        : BoundingArea2( BA_SPHERE2 )
    {
        Generate(pBA);
    }

    Sphere2::Sphere2(const Vector2& rCenter, Real radius)
        : BoundingArea2( BA_SPHERE2 )
        , base( rCenter, radius )
    {
    }

    Sphere2::Sphere2(Real xPos, Real yPos, Real radius)
        : BoundingArea2( BA_SPHERE2 )
        , base( Vector2( xPos, yPos ), radius )
    {
    }

    Sphere2::Sphere2(Real radius)
        : BoundingArea2( BA_SPHERE2 )
        , base( radius )
    {
    }

    bool Sphere2::Intersect(const AABB2* pAABB2) const
    {
        using namespace Math;

        Real dist = 0; 
        Real radiusSqr = Mul(GetRadius(), GetRadius());

        Real centreDist = GetCenter().x - pAABB2->GetCenter().x;
        Real s = centreDist + pAABB2->GetExtents().x;

        if(s < 0)
        {
            dist += Sqr(s);
            if(dist > radiusSqr)        return false;
        }
        else
        {
            s = centreDist - pAABB2->GetExtents().x;
            if(s > 0)
            {
                dist += Sqr(s);
                if(dist > radiusSqr)    return false;
            }
        }

        centreDist = GetCenter().y - pAABB2->GetCenter().y;
        s = centreDist + pAABB2->GetExtents().y;

        if(s < 0)
        {
            dist += Sqr(s);
            if(dist > radiusSqr)        return false;
        }
        else
        {
            s = centreDist - pAABB2->GetExtents().y;
            if(s > 0)
            {
                dist += Sqr(s);
                if(dist > radiusSqr)    return false;
            }
        }
        return dist <= radiusSqr;
    }

    void Sphere2::Generate(const BoundingArea2* pBA)
    {
        if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            Generate((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_AABB2)
            Generate((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            Generate((OBB2*)pBA);
    }

    void Sphere2::Merge(const BoundingArea2* pBA)
    {
        if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            Merge((Sphere2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_AABB2)
            Merge((AABB2*)pBA);
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            Merge((OBB2*)pBA);
    }

    void Sphere2::Merge(const Sphere2* pSphere2 )
    {
        using namespace Math;

        Vector2 centerDiff = pSphere2->GetCenter() - GetCenter();
        Real centerDiffSqr = Sqr(centerDiff.x) + Sqr(centerDiff.y);

        Real radiusDiff    = GetRadius() - pSphere2->GetRadius();
        Real radiusDiffSqr = Sqr(radiusDiff);

        // If spheres overlaps each other
        if( radiusDiffSqr >= centerDiffSqr )
        {
            // Check if radius difference is positive - new sphere exceeds old one
            if( radiusDiff < 0 )
            {
                // Extend circle to merged circle size
                SetCenterRadius( pSphere2->GetCenter(), pSphere2->GetRadius() );
            }
            // Merged circle is inside current one
            else 
                ;
        }
        else
        {
            Real dist = Sqrt( centerDiffSqr );
            Real factor = Div2( dist - radiusDiff );
            SetCenterRadius( GetCenter() + centerDiff * Div(factor, dist),
                             factor + m_radius);
        }
    }

    void Sphere2::Render(unsigned int color /*= 0xFF0000*/, void* user /*= NULL*/ ) const
    {
        if ( GetRadius() > 0.0f )
        {
            Debug::RenderCircle( GetCenter(), GetRadius(), color, user );
        }
    }

} // namespace Scene

} // namespace Jungle

// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/ThickRay2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__THICK_RAY2_HPP__
#define __SCENE__GEO2D__THICK_RAY2_HPP__

// Internal includes
#include <jungle/scene/geo2d/Ray2.hpp>
#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class BoundingArea2;
    class Sphere2;
    class AABB2;
    class OBB2;

    class ThickRay2 : public Ray2
    {
    public:
        /*!
        * Default constructor, doesn't reset ray origin neigher direction or thickness
        */
        inline                  ThickRay2()
                                : Ray2()
        {}

        /*!
        * Initializing constructor, sets ray direction, position and thikness
        * \param origin         ray start position
        * \param dir            ray direction
        * \param thickness      ray thickness
        */
        inline                  ThickRay2(const Vector2& rOrigin, const Vector2& rDir, Real thickness)
                                : Ray2(rOrigin, rDir)
                                , m_thickness( thickness )
        {}

        /*!
        * Initializing constructor, sets ray direction using x and y coordinates and its' thickness
        * \param x              ray x direction
        * \param y              y direction coordinate
        * \param thickness      thickness
        */
        inline                  ThickRay2(Real xDir, Real yDir, Real thickness)
                                : Ray2( xDir, yDir )
                                , m_thickness( thickness )
        {}

        /*!
        * Set ray thickness. Thickness is defined as the distance from the ray half-line to the farthest
        * point in the ray area. In other words ThickRay2 defines area covered by the infinite number of
        * circles (with a radius equal to ray thickness) placed on the ray line.
        * \param thickness      ray thickness allows ray to cover more area then simple ray
        *                       it converts ray more into beam.
        */
        inline void             SetThickness(Real thickness);

        /*!
        * Get ray thickness.
        * \return ray half width.
        */
        inline Real             GetThickness() const;

        /*!
        * Check intersection with bounding shape of any type
        * \param pBS            tested bounding shape pointer
        * \param pContactSet    contact info pointer, must be always set to NULL, cause
        * contact points calculus is not available yet
        */
        virtual bool            Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet = NULL) const;

        /*!
        * Intersection test with a circle area
        * \param pSphere2       circle tested for intersection
        */
        bool                    Intersect(const Sphere2* pSphere2) const;

        /*!
        * Intersection test with another axis aligned rectangle
        * \param pAABB2         tested axis aligned box
        */
        bool                    Intersect(const AABB2* pAABB2) const;

        /*!
        * Intersection test with oriented rectangle
        * \param pOBB2          oriented rectangle tested for intersection with
        */
        bool                    Intersect(const OBB2* pOBB2) const;

        /*!
        * Helper method to draw symbolic thick ray representation as two pararell rays
        * distant on doubled thickness.
        */
        virtual void            Render(unsigned int color = 0xFF0000, void* user = NULL ) const;

    public:
        Real                    m_thickness;

    }; // class ThickRay2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/ThickRay2.inl>

#endif // !defined __SCENE__GEO2D__THICK_RAY2_HPP__
// EOF

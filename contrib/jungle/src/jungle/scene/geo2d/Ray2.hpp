////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/Ray2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__RAY2_HPP__
#define __SCENE__GEO2D__RAY2_HPP__

// Internal includes
#include <jungle/scene/geo2d/AbstractLine2.hpp>
#include <jungle/scene/SceneTypes.hpp>

#include <jungle/math/geo/Ray2.hpp>

// External includes

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class BoundingArea2;
    class Sphere2;
    class AABB2;
    class OBB2;
    class Segment2;

    class Ray2 : public AbstractLine2, public Math::Ray2< Real, Vector2 >
    {
        typedef Math::Ray2< Real, Vector2 >     base;

    public:
        typedef Vector2         Vector;

        /*!
        * Default constructor, doesn't reset ray origin neigher direction
        */
        inline                  Ray2()
                                : base()
        {}

        /*!
        * Initializing constructor, sets ray direction and position
        * \param origin         ray start position
        * \param dir            ray direction
        */
        inline                  Ray2(const Vector2& rOrigin, const Vector2& rDir)
                                : base( rOrigin, rDir )
        {}

        /*!
        * Initializing constructor, sets ray direction using x and y coordinates
        * \param x              ray x direction
        * \param y              y direction coordinate
        */
        inline                  Ray2(Real xDir, Real yDir)
                                : base( xDir, yDir )
        {}

        /*!
        * Check intersection with bounding shape of any type
        * \param pBS            tested bounding shape pointer
        * \param pContactSet    output contact set info pointer
        */
        inline
        virtual bool            Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet = NULL) const;

        /*!
        * Intersection test with another axis aligned rectangle
        * \param pAABB2         tested axis aligned box
        * \param pContactSet    output contact set info pointer
        */
        bool                    Intersect(const AABB2* pAABB2, ContactSet2* pContactSet = NULL) const;

        /*!
        * Intersection test with object oriented rectangle
        * \param pOBB2          oriented rectangle tested for intersection with this ray
        * \param pContactSet    output contact set info
        */
        bool                    Intersect(const OBB2* pOBB2, ContactSet2* pContactSet = NULL) const;

        /*!
        * Intersection test with a circle area
        * \param pSphere2       circle area tested for intersection
        * \param pContactSet    output contact set info pointer
        */
        bool                    Intersect(const Sphere2* pSphere2, ContactSet2* pContactSet = NULL) const;

        virtual bool            Clip(const AABB2* pAABB2, Segment2* pClipedSegment) const;

        virtual bool            Clip(const OBB2* pOBB2, Segment2* pClipedSegment) const;

        /*!
        * Helper method to draw symbolic ray representation as a vector pointing from the orgin to its direction
        */
        virtual void            Render(unsigned int color = 0xFF0000, void* user = NULL ) const;

    }; // class Ray2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/Ray2.inl>

#endif // !defined __SCENE__GEO2D_RAY2_HPP__
// EOF

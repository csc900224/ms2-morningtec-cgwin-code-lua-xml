////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/ThickSegment2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/ThickSegment2.hpp>
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>
#include <jungle/base/NumberTraits.hpp>

#include <jungle/scene/SceneDebug.hpp>
#include <jungle/base/Errors.hpp>       // JUNGLE_ASSERT

namespace Jungle
{
namespace Scene
{
    static OBB2 s_obbHelper;

    bool ThickSegment2::Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet /* = NULL */) const
    {
        JUNGLE_ASSERT_MSG( !pContactSet, "There is no support for contact points gather yet." );

        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Intersect( (AABB2*)pBA );
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Intersect( (OBB2*)pBA );
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Intersect( (Sphere2*)pBA );

        return false;
    }

    bool ThickSegment2::Intersect(const Sphere2* pSphere2) const
    {
        s_obbHelper.SetCenterExtentsAndAxes( GetCenter(), Vector2( GetLength() / 2, GetThickness() ), GetDir(), GetDir().Perpendicular() );
        return s_obbHelper.Intersect( pSphere2 );
    }

    bool ThickSegment2::Intersect(const AABB2* pAABB2) const
    {
        s_obbHelper.SetCenterExtentsAndAxes( GetCenter(), Vector2( GetLength() / 2, GetThickness() ), GetDir(), GetDir().Perpendicular() );
        return s_obbHelper.Intersect( pAABB2 );
    }

    bool ThickSegment2::Intersect(const OBB2* pOBB2) const
    {
        s_obbHelper.SetCenterExtentsAndAxes( GetCenter(), Vector2( GetLength() / 2, GetThickness() ), GetDir(), GetDir().Perpendicular() );
        return s_obbHelper.Intersect( pOBB2 );
    }

    void ThickSegment2::Render(unsigned int color /*= 0xFF0000*/, void* user /*= NULL*/ ) const
    {
        const Vector2& origin = GetOrigin();
        Vector2 extent(GetDir());

        // Always use normalized direction
        JUNGLE_ASSERT( Math::Abs( extent.Length() - NumberTraits< Real >::ONE ) < 0.0001f );

        extent *= GetLength();

        // Render center line
        Debug::RenderSegment( origin, origin + extent, color, user );

        // Render thick segment as an oriented rectangle
        s_obbHelper.SetCenterExtentsAndAxes( GetCenter(), Vector2( GetLength() / 2, GetThickness() ), GetDir(), GetDir().Perpendicular() );
        s_obbHelper.Render( color, user );
    }

} // namespace Scene

} // namespace Jungle
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/AABB2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__AABB2_HPP_
#define __SCENE__GEO2D__AABB2_HPP_

// Internal includes
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/math/geo/AABB2.hpp>

#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class Sphere2;
    class OBB2;

    class AABB2 : public BoundingArea2, public Math::AABB2< Real, Vector2 >
    {
        typedef Math::AABB2< Real, Vector2 >    base;

    public:
        /*!
        * Default constructor, sets rectangle position to (0, 0) and makes it empty (minimal size)
        */
                        AABB2();

        /*!
        * Coping constructor
        * \param pAABB2 pointer to another AABB2
        */
                        AABB2(const AABB2* pAABB2);

        /*!
        * Generator. Constructor that creates bounding area based on another bounding shape
        * \param pBA reference bounding shape to be copied
        */
                        AABB2(const BoundingArea2* pBA);

        /*!
        * Construct bounding rectangle based on the center position and dimensions vector
        * \param rCenter rectangle center position
        * \param rExtents rectangle extents its dimensions will be equal 2*extents
        */
                        AABB2(const Vector2& rCenter, const Vector2& rExtents);

        /*!
        * Construct bounding rectangle based on the center x,y position and dimensions passed as arguments.
        * \param x rectangle center X position
        * \param y rectangle center Y position
        * \param width rectangle size on X axis
        * \param height rectangle size on Y axis
        */
                        AABB2(Real x, Real y, Real width, Real height);

        /*!
        * Construct bounding rectangle starting from a world (0, 0) point to the point given by 
        * parameter - dimensions vector
        * \param rDim   rectangle dimensions (extents)
        */
        explicit        AABB2(const Vector2& rDim);

        /*!
        * Check intersection with other bounding shape
        * \param pBS    Tested bounding shape pointer
        */
        inline
        virtual bool    Intersect(const BoundingArea2* pBA) const;

        /*!
        * Intersection test with another axis aligned rectangle
        * \param pAABB2 Tested shape
        */
        inline bool     Intersect(const AABB2* pAABB2) const;

        /*!
        * Intersection test with a circle area
        * \param pSphere2 Tested shape
        */
        inline bool     Intersect(const Sphere2* pSphere2) const;

        /*!
        * Intersection test with oriented bounding box
        * \param pAABB2 Tested shape
        */
        inline  bool    Intersect(const OBB2* pOBB2) const;

        /*!
        * Overlapping test with other bounding shape
        * \param pBA    Tested bounding geometry pointer
        */
        inline
        virtual bool    Overlaps(const BoundingArea2* pBA) const;

        /*!
        * Overlapping test with another axis aligned rectangle
        * \param pAABB2 Tested rectangle pointer
        */
        inline bool     Overlaps(const AABB2* pAABB2) const;

        /*!
        * Overlapping test with circle area.
        * \param pSphere2 Tested circle pointer
        */
        inline bool     Overlaps(const Sphere2* pSphere2) const;

        /*!
        * Overlapping test with oriented bounding rectangle.
        * \param pOBB2  Tested rectangle pointer
        */
        inline bool     Overlaps(const OBB2* pOBB2) const;

        /*!
        * Check if point lays inside the bounding area
        * \param pPoint Tested point position
        */
        virtual bool    Overlaps(const Vector2* pPoint) const;

        /*!
        * Generate bounding area from any type of other bounding shape
        * \param pBS    Source bounding shape object
        */
        virtual void    Generate(const BoundingArea2* pBA);

        /*!
        * Generate bounding surface from another axis aligned rect
        * \param pAABB2 Reference axis aligned rectangle
        */
        inline void     Generate(const AABB2* pAABB2);

        /*!
        * Generate rectangle from circle.
        * Method uses pessimistic approach - meaning that newly formed bounding rectangle 
        * covers wider area then reference circle. In other words rectangle is circumscribed on circle.
        * \param pSphere2 Bounding area used as reference for rectangle generation
        */
        inline void     Generate(const Sphere2* pSphere2);

        /*!
        * Generate bounding surface from oriented rectangle
        * \param pOBB2 Reference rectangle
        */
        inline void     Generate(const OBB2* pOBB2);

        /*!
        * Merge bounding surface with another surface of any type
        * \param pBS    Merged bounding surface
        */
        virtual void    Merge(const BoundingArea2* pBA);

        /*!
        * Merge bounding surface with another axis aligned rectangle.
        * Method uses pessimistic approach and resulting area fully covers 
        * sum of rectangle and circle regions (\see Generate(const Sphere2* pSphere2) method).
        * \param pAABB2 Merged rectangle
        */
        void            Merge(const AABB2* pAABB2);

        /*!
        * Merge rectangle with circle bounding area
        * \param pSphere2 Merged circle bounding shape
        */
        void            Merge(const Sphere2* pSphere2);

        /*!
        * Merge axis aligned rectangle with oriented box area
        * \param pOBB2 Merged rectangle
        */
        void            Merge(const OBB2* pOBB2);

        /*!
        * Moves center of a bounding rectangle
        * \param rOffset geometric rectangle center translation
        */
        inline
        virtual void    Translate(const Vector2& rOffset);

        /*!
        * Rotates bounding area about specified angle.
        * \note This method uses current rotation angle and cumulates new rotation
        * with previous transformations.
        * \param fixed angle varying from <0, 180>
        */
        inline
        virtual void    Rotate(Real angle);

        /*!
        * Scales bounding area about specified scale factor.
        * \note This method cumulates scaling with previous transformations.
        * \param scale scale factor.
        */
        inline
        virtual void    Scale(Real scale);

        /*!
        * Set bounding area to smallest possible size - do not contain anything
        */
        inline
        virtual void    SetEmpty();

        /*!
        * Check if area is totally empty and can not contain anything
        * Scaling and rotating shape doesn't affect it's empty status.
        */
        inline
        virtual bool    IsEmpty() const;

        /*!
        * Set bounding area to contain single point with coordinates passed to method
        * \param rPos   new bounding area coordinates center 
        */
        inline
        virtual void    SetPoint(const Vector2& rPos);

        /*!
        * Check if volume is point size.
        */
        inline
        virtual bool    IsPoint() const;

        /*!
        * Bound position vector to AABB2 area
        * \param rPos   [in/out] position which needs to be clamped
        */
        inline bool     BoundToArea(Vector2& rPos) const;

        /*!
        * Helper method to show rectangle representation
        */
        virtual void    Render(unsigned int color = 0x00FF00, void* user = NULL) const;

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for AABB2 *= float. Scales the extents, keeps same center.
        inline AABB2&   operator*=(Real s)
        {
            base::operator*=( s );
            return *this;
        }

        //! Operator for AABB2 /= float. Scales the extents, keeps same center.
        inline AABB2&   operator/=(Real s)
        {
            base::operator/=( s );
            return *this;
        }

        //! Operator for AABB2 += rTrans. Translates the rectangle keeping the extents.
        inline AABB2&   operator+=(const Vector& rTrans)
        {
            base::operator+=( rTrans );
            return *this;
        }

        //! Operator for AABB -= rTrans. Translates the rectangle keeping it's extents.
        inline AABB2&   operator-=(const Vector& rTrans)
        {
            base::operator-=( rTrans );
            return *this;
        }

        //! Merge operator, adds two bounding areas. AABB2 += AABB2.
        inline AABB2&   operator+=(const AABB2& rAABB2)
        {
            Merge(&rAABB2);
            return *this;
        }

        //! Assignment operator
        inline AABB2&   operator=(const AABB2& rAABB2)
        {
            Generate( &rAABB2 );
            return *this;
        }

    }; // class AABB2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/AABB2.inl>

#endif // !defined __SCENE__GEO2D__AABB2_HPP_
// EOF

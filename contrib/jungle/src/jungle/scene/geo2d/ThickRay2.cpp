////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/ThickRay2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/ThickRay2.hpp>
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>
#include <jungle/base/NumberTraits.hpp>

#include <jungle/scene/SceneDebug.hpp>
#include <jungle/base/Errors.hpp>       // JUNGLE_ASSERT

namespace Jungle
{
namespace Scene
{
    bool ThickRay2::Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet /* = NULL */) const
    {
        JUNGLE_ASSERT_MSG( !pContactSet, "There is no support for contact points gather yet." );

        if(pBA->GetType() == BoundingArea2::BA_AABB2)
            return Intersect( (AABB2*)pBA );
        else if(pBA->GetType() == BoundingArea2::BA_OBB2)
            return Intersect( (OBB2*)pBA );
        else if(pBA->GetType() == BoundingArea2::BA_SPHERE2)
            return Intersect( (Sphere2*)pBA );

        return false;
    }

    bool ThickRay2::Intersect(const Sphere2* pSphere2) const
    {
        // Simply extend circle to reflect ray thickness
        Sphere2 circle( pSphere2->GetCenter(), pSphere2->GetRadius() + GetThickness() );
        return Ray2::Intersect( &circle, NULL );
    }

    bool ThickRay2::Intersect(const AABB2* pAABB2) const
    {
        const Vector2& dir = GetDir();
        Vector2 extents( pAABB2->GetExtents() );
        // Always use normalized direction
        JUNGLE_ASSERT( Math::Abs( dir.Length() - NumberTraits< Real >::ONE ) < NumberTraits< Real >::Epsilon() );

        // Project ray thickness to spread AABB extents
        extents.x += Math::Mul( Math::Abs(dir.y), GetThickness() );
        extents.y += Math::Mul( Math::Abs(dir.x), GetThickness() );

        AABB2 aabbExtended( pAABB2->GetCenter(), extents );
        return Ray2::Intersect( &aabbExtended, NULL );
    }

    bool ThickRay2::Intersect(const OBB2* pOBB2) const
    {
        const Vector2& dir = GetDir();
        Vector2 extents( pOBB2->GetExtentsSize() );
        // Always use normalized direction
        JUNGLE_ASSERT( Math::Abs( dir.Length() - NumberTraits< Real >::ONE ) < NumberTraits< Real >::Epsilon() );

        // Project ray thickness to spread AABB extents
        extents.x += Math::Mul( Math::Abs( dir.Dot( pOBB2->GetAxis( 1 ) ) ), GetThickness() );
        extents.y += Math::Mul( Math::Abs( dir.Dot( pOBB2->GetAxis( 0 ) ) ), GetThickness() );

        OBB2 obbExtended( pOBB2->GetCenter(), extents, pOBB2->GetAxis( 0 ), pOBB2->GetAxis( 1 ) );
        return Ray2::Intersect( &obbExtended, NULL );
    }

    void ThickRay2::Render(unsigned int color /*= 0xFF0000*/, void* user /*= NULL*/ ) const
    {
#       define DEBUG_RAY_LENGHT         500

        const Vector2& origin = GetOrigin();
        Vector2 start0(origin);
        Vector2 start1(origin);
        Vector2 dir(GetDir());

        // Always use normalized direction
        JUNGLE_ASSERT( Math::Abs( dir.Length() - NumberTraits< Real >::ONE ) < NumberTraits< Real >::Epsilon() );

        Vector2 baseDir( dir.Perpendicular() );
        Vector2 baseDiv2( baseDir );
        baseDiv2 *= GetThickness();

        dir *= Real(DEBUG_RAY_LENGHT);

        // Render center line
        Debug::RenderSegment( origin, origin + dir, color, user );

        start0 += baseDiv2;
        start1 -= baseDiv2;

        // Render thick ray borders
        Debug::RenderSegment( start0, start0 + dir, color, user );
        Debug::RenderSegment( start1, start1 + dir, color, user );
        Debug::RenderSegment( start0, start1, color, user );
    }

} // namespace Scene

} // namespace Jungle
// EOF

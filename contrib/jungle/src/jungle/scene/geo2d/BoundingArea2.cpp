////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/BoundingArea2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/BoundingArea2.hpp>
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>
#include <jungle/scene/geo2d/OBB2.hpp>

// External includes
#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Scene
{
    /* static */
    BoundingArea2* BoundingArea2::CreateInstance(BAType baType /* = BA_AABB2 */)
    {
        BoundingArea2* ba = 0;
        if(baType == BA_AABB2)
            ba = new AABB2();
        else if(baType == BA_SPHERE2)
            ba = new Sphere2();
        else if(baType == BA_OBB2)
            ba = new OBB2();
        else
            JUNGLE_ASSERT_MSG(false, "Bounding area type not supported");
        return ba;
    }

} // namespace Scene

} // namespace Jungle

// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/OBB2.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/scene/geo2d/AABB2.hpp>
#include <jungle/scene/geo2d/Sphere2.hpp>

namespace Jungle
{
namespace Scene
{
    inline OBB2::OBB2()
        : BoundingArea2( BA_OBB2 )
        , base()
    {
    }

    inline OBB2::OBB2(const OBB2* pOBB2)
        : BoundingArea2( BA_OBB2 )
    {
        Generate( pOBB2 );
    }

    inline OBB2::OBB2(const BoundingArea2* pBA)
        : BoundingArea2( BA_OBB2 )
    {
        Generate( pBA );
    }

    inline OBB2::OBB2(const Vector2& center)
        : BoundingArea2( BA_OBB2 )
        , base( center )
    {
    }

    inline OBB2::OBB2(const Vector2& center, const Vector2& extent)
        : BoundingArea2( BA_OBB2 )
        , base( center, extent )
    {
    }

    inline OBB2::OBB2(const Vector2& center, const Vector2& extent, const Vector2& axisX, const Vector2& axisY)
        : BoundingArea2( BA_OBB2 )
        , base( center, extent, axisX, axisY )
    {
    }

    inline bool OBB2::Intersect( const BoundingArea2* pBV ) const
    {
        JUNGLE_ASSERT_MSG( pBV, "NULL passed to OBB2::Intersect" );

        switch( pBV->GetType() )
        {
        case BA_AABB2:
            return Intersect( (AABB2*)pBV );
            break;

        case BA_SPHERE2:
            return Intersect( (Sphere2*)pBV );
            break;

        case BA_OBB2:
            return Intersect( (OBB2*)pBV );
            break;

        default:
            JUNGLE_ASSERT_MSG( false, "Unsuported bounding area type pased to intersection test OBB2::Intersect() method." );
            return false;
        }
    }

    inline bool OBB2::Intersect( const AABB2* pAABB2 ) const
    {
        JUNGLE_ASSERT_MSG( pAABB2, "NULL passed to function OBB2::Intersect" );

        const OBB2 obbHelper( pAABB2 );
        return Intersect( &obbHelper );
    }

    inline bool OBB2::Intersect( const Sphere2* pSphere2 ) const
    {
        JUNGLE_ASSERT_MSG( pSphere2, "NULL passed to function OBB2::Intersect" );

        Real sqrDistance = DistanceSqr( &pSphere2->GetCenter() );
        return sqrDistance <= Math::Sqr( pSphere2->GetRadius() );
    }

    inline bool OBB2::Overlaps( const BoundingArea2* pBV ) const
    {
        JUNGLE_ASSERT_MSG( false, "Not implemented yet." );
        return false;
    }

    inline bool OBB2::Overlaps( const Vector2* pPoint ) const
    {
        JUNGLE_ASSERT_MSG( pPoint, "NULL passed to function OBB2::Overlaps" );

        return IsInside( pPoint );
    }

    inline void OBB2::Translate( const Vector2& rOff )
    {
        *this += rOff;
    }

    inline void OBB2::Scale(Real scale)
    {
        *this *= scale;
    }

    inline void OBB2::Rotate(Real radAngle)
    {
        base::Rotate( radAngle );
    }

    inline void OBB2::SetEmpty()
    {
        return base::SetEmpty();
    }

    inline bool OBB2::IsEmpty() const
    {
        return base::IsEmpty();
    }

    inline void OBB2::SetPoint(const Vector2& rPos)
    {
        base::SetPoint( rPos );
    }

    bool OBB2::IsPoint() const
    {
        return base::IsPoint();
    }

} // namespace Scene

} // namespace Jungle

// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/geo2d/ThickSegment2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE__GEO2D__THICK_SEGMENT2_HPP__
#define __SCENE__GEO2D__THICK_SEGMENT2_HPP__

// Internal includes
#include <jungle/scene/geo2d/Segment2.hpp>
#include <jungle/scene/SceneTypes.hpp>

namespace Jungle
{
namespace Scene
{
    // Forward declarations
    class BoundingArea2;
    class Sphere2;
    class AABB2;
    class OBB2;

    class ThickSegment2 : public Segment2
    {
    public:
        /*!
        * Default constructor, doesn't reset segment origin and end, direction will be undefined too
        */
        inline                  ThickSegment2()
                                : Segment2()
        {}

        /*!
        * Initializing constructor, sets segment direction, position, length and thickness
        * \param origin         segment start position
        * \param dir            direction
        * \param length         extent
        * \param thickness      segment half width
        */
        inline                  ThickSegment2(const Vector2& rOrigin, const Vector2& rDir, Real length, Real thickness )
                                : Segment2( rOrigin, rDir, length )
                                , m_thickness( thickness )
        {}

        /*!
        * Initializing constructor, sets segment origin in (0,0), its' direction with x and y coordinates,
        * length and thickness.
        * \param x              x normalized direction coordinate
        * \param y              y direction coordinate
        * \param length         segment extent
        * \param thickness      half width
        */
        inline                  ThickSegment2(Real xDir, Real yDir, Real length, Real thickness)
                                : Segment2( xDir, yDir, length )
                                , m_thickness( thickness )
        {}

        /*!
        * Set segment thickness. Thickness is defined as the distance from infinitelly thin segment line to the farthest
        * point in the overlayed segment area.
        * \param thickness      segment thickness defines resulting beam width.
        */
        inline void             SetThickness(Real thickness);

        /*!
        * Get segment virtual thickness.
        * \return half width of segment beam.
        */
        inline Real             GetThickness() const;

        /*!
        * Check intersection with bounding shape of any type
        * \param pBA            tested bounding shape pointer
        * \param pContactSet    contact info pointer to be set if instersection detected, must be always NULL
        * cause contact points gather is not available yet for thick segments.
        */
        virtual bool            Intersect(const BoundingArea2* pBA, ContactSet2* pContactSet = NULL) const;

        /*!
        * Segment intersection with a circle area
        * \param pSphere2       circle tested for intersection
        */
        bool                    Intersect(const Sphere2* pSphere2) const;

        /*!
        * Segment intersection test with axis aligned rectangle
        * \param pAABB2         tested axis aligned box
        */
        bool                    Intersect(const AABB2* pAABB2) const;

        /*!
        * Intersection test with oriented rectangle
        * \param pOBB2          oriented rectangle tested for intersection with
        */
        bool                    Intersect(const OBB2* pOBB2) const;

        /*!
        * Helper method to draw thick segment area as an rectangle with and direction axis inside.
        */
        virtual void            Render(unsigned int color = 0xFF0000, void* user = NULL ) const;

    public:
        Real                    m_thickness;

    }; // class ThickSegment2

} // namespace Scene

} // namespace Jungle

#include <jungle/scene/geo2d/ThickSegment2.inl>

#endif // !defined __SCENE__GEO2D__THICK_SEGMENT2_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/scene/RenderObject.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

#ifndef __SCENE_RENDER_OBJECT_HPP__
#define __SCENE_RENDER_OBJECT_HPP__

namespace Jungle
{
namespace Scene
{
    enum RenderFlag
    {
        RF_FRUSTUM_CULLED       = (1<<0),
        RF_OUTSIDE_FRUSTUM      = (1<<0),
        RF_INTERSECT_FRUSTUM    = (1<<1),
        RF_INSIDE_FRUSTUM       = (1<<2),

        RF_INVISIBLE            = (1<<4),

        RF_FORCE_DWORD          = 0x7fffffff
    }; // enum RenderFlag

    enum ViewFrustumCulling
    {
        VFC_FRUSTUM_OUTSIDE     = (1<<0),
        VFC_FRUSTUM_INTERSECT   = (1<<1),
        VFC_FRUSTUM_INSIDE      = (1<<2),

        VFC_FRUSTUM_MASK = VFC_FRUSTUM_OUTSIDE | VFC_FRUSTUM_INTERSECT | VFC_FRUSTUM_INSIDE
    }; // enum ViewFrustumCulling

    template < class TBoundingVolume >
    class RenderObject
    {
    protected:
        typedef TBoundingVolume     Bv;
        typedef unsigned int        ShaderId;

    public:
        //! Constructor initializes renderable as visible
                                    RenderObject();

        //! Construct object with predefined shader id.
                                    RenderObject( ShaderId shaderId );

        //! Destructor
        virtual                     ~RenderObject();

        /*!
        * Gets flag status of frustum culling test determining if object is inside, outside or intersects view frustum.
        * \return frustrum culling flags
        */
        inline ViewFrustumCulling   GetFrustumCullingStatus() const;

        /*!
        * Sets view frustum culling status.
        */
        inline void                 SetFrustumCullingStatus(const ViewFrustumCulling& flag);

        /*!
        * Sets view frustum culling status.
        */
        inline void                 SetFrustumCullingStatus(const RenderFlag& flag);

        /*!
        * Determines if entity is frustum culled and shouldn't be rendered
        * \return     true if an object is outside the area of view frustum
        */
        inline bool                 IsFrustumCulled() const;

        /*!
        * Determines if entity is frustum culled and shouldn't be rendered
        * \return     true if a collision occurred
        */
        inline bool                 IsRendered() const;

        /*!
        * Determines if entity should be rendered
        * \return     true if visibility flag set to true
        */
        inline bool                 IsVisible() const;

        /*!
        * Determines if entity should be rendered or temporary hidden
        * \return     true if a collision occurred
        */
        inline void                 SetVisible( bool visibility );

        //! Acquire geometry bounding visual entity representation in world space.
        virtual const Bv*           GetRenderWorldBV() const    = 0;

        //! Get local space bounding geometry of visual entity representation.
        /*!
        * Method implementation is optional and may be overrided if needed by project specific architecture.
        */
        virtual const Bv*           GetRenderLocalBV() const    { return 0; }

        //! Return shader or texture atlas identification number (implementation specific).
        /*!
        * This id may be used to sort rendered objects in order to minimize state/texture swithches.
        */
        inline ShaderId             GetShaderId() const;

        //! Render geometry
        virtual void                Render(void* user) const    {};

    protected:
        //! Render flags
        unsigned int                m_renderFlags;

        //! Texture or shader identification used for texture sorting (optionally in rendering)
        ShaderId                    m_shaderId;

    }; // class RenderObject

    template < class TBoundingVolume >
    RenderObject< TBoundingVolume >::RenderObject()
        : m_renderFlags(0)
    {}

    template < class TBoundingVolume >
    RenderObject< TBoundingVolume >::RenderObject( unsigned int shaderId )
        : m_renderFlags( 0 )
        , m_shaderId( shaderId )
    {}

    template < class TBoundingVolume >
    RenderObject< TBoundingVolume >::~RenderObject()
    {}

    template < class TBoundingVolume >
    inline ViewFrustumCulling RenderObject< TBoundingVolume >::GetFrustumCullingStatus() const
    {
        return ViewFrustumCulling(m_renderFlags & 7);
    }

    template < class TBoundingVolume >
    inline void RenderObject< TBoundingVolume >::SetFrustumCullingStatus(const ViewFrustumCulling& flag)
    { 
        m_renderFlags |= (flag & VFC_FRUSTUM_MASK);
        if(!(flag & VFC_FRUSTUM_OUTSIDE))
            m_renderFlags &= ~RF_OUTSIDE_FRUSTUM;
    }

    template < class TBoundingVolume >
    inline void RenderObject< TBoundingVolume >::SetFrustumCullingStatus(const RenderFlag& flag)
    {
        m_renderFlags |= (flag & (RF_INTERSECT_FRUSTUM | RF_INSIDE_FRUSTUM | RF_OUTSIDE_FRUSTUM));
        if(!(flag & RF_OUTSIDE_FRUSTUM))
            m_renderFlags &= ~RF_OUTSIDE_FRUSTUM;
    }

    template < class TBoundingVolume >
    inline bool RenderObject< TBoundingVolume >::IsFrustumCulled() const
    {
        return (m_renderFlags & RF_FRUSTUM_CULLED) != 0;
    }

    template < class TBoundingVolume >
    inline bool RenderObject< TBoundingVolume >::IsRendered() const
    {
        return (IsVisible() && !IsFrustumCulled());
    }

    template < class TBoundingVolume >
    inline void RenderObject< TBoundingVolume >::SetVisible( bool visibility )
    {
        if(!visibility)
            m_renderFlags  |= RF_INVISIBLE;
        else
            m_renderFlags  &= ~RF_INVISIBLE;
    }

    template < class TBoundingVolume >
    inline bool RenderObject< TBoundingVolume >::IsVisible() const
    {
        return (m_renderFlags & RF_INVISIBLE) == 0;
    }

    template < class TBoundingVolume >
    inline typename RenderObject< TBoundingVolume >::ShaderId RenderObject< TBoundingVolume >::GetShaderId() const
    {
        return m_shaderId;
    }

} // namespace Scene

} // namespace Jungle

#endif  // !defined __SCENE_RENDER_OBJECT_HPP__
// EOF

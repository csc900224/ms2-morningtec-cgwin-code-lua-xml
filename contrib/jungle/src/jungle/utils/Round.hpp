//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/utils/Round.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UTILS_ROUND_HPP__
#define __UTILS_ROUND_HPP__

// Internal includes
#include <jungle/base/Types.hpp>

//! Collection of methods for performing various number rounding, truncation and ceil operation.
namespace Jungle
{
namespace Utils
{
    template< typename T >
    inline T        RoundUp( T val, uint32 alignment );

    template< typename T >
    inline T        RoundDown( T value, uint32 alignment );

    template < class T >
    inline int32    RoundInt( T value );

    template < class T >
    inline int64    RoundLong( T value );

} // namespace Utils

} // namespace Jungle

#include <jungle/utils/Round.inl>

#endif // !defined __UTILS_ROUND_HPP__
// EOF

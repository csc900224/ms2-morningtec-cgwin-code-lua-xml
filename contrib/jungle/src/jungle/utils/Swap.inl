//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/utils/Swap.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UTILS_SWAP_INL__
#define __UTILS_SWAP_INL__

// Internal includes

namespace Jungle
{
namespace Utils
{

namespace Details
{
    // Module function is not exposed in header file intentionaly, it is not the fastest method of getting Abs
    // use Math::Abs instead, here used to not generate Math module dependencies
    template< typename T >
    inline T Abs( T& val )
    {
        return val < 0 ? -val : val;
    }
}

    template< typename T >
    inline void Swap( T& val0, T& val1 )
    {
        T tmp( val0 );
        val0 = val1;
        val1 = tmp;
    }

    template< typename T >
    inline void GreaterFirst( T& val0, T& val1 )
    {
        if( val1 > val0 )
            Swap( val0, val1 );
    }

    template< typename T >
    inline void SmallerFirst( T& val0, T& val1 )
    {
        if( val1 < val0 )
            Swap( val0, val1 );
    }

    template< typename T >
    inline void GreaterAbsFirst( T& val0, T& val1 )
    {
        if( Details::Abs( val1 ) > Details::Abs( val0 ) )
            Swap( val0, val1 );
    }

    template< typename T >
    inline void SmallerAbsFirst( T& val0, T& val1 )
    {
        if( Details::Abs( val1 ) < Details::Abs( val0 ) )
            Swap( val0, val1 );
    }

} // namespace Utils

} // namespace Jungle

#endif // !defined __UTILS_SWAP_HPP__
// EOF

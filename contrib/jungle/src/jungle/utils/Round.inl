//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/utils/Round.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UTILS_ROUND_INL__
#define __UTILS_ROUND_INL__

// Internal includes
#include <jungle/base/Types.hpp>

namespace Jungle
{
namespace Utils
{
    template< typename T >
    inline T RoundUp( T value, uint32 alignment )
    {
        return T((uint32(value) + alignment -1) & ~(alignment -1));
    }

    template< typename T >
    inline T RoundDown( T value, uint32 alignment )
    {
        return T(uint32(value) & ~(alignment -1));
    }

    template < class T >
    inline int32 RoundInt( T a )
    {
        return static_cast< int >( a + ( a > 0 ? NumberTraits<T>::Half() : -NumberTraits<T>::Half() ) );
    }

    template < class T >
    inline int64 RoundLong( T a )
    {
        return static_cast< long >( a + ( a > 0 ? NumberTraits<T>::Half() : -NumberTraits<T>::Half() ) );
    }

} // namespace Utils

} // namespace Jungle

#endif // !defined __UTILS_ROUND_INL__
// EOF

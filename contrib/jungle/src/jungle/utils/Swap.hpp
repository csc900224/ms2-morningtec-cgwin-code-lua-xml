//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/utils/Swap.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __UTILS_SWAP_HPP__
#define __UTILS_SWAP_HPP__

// Internal includes

//! Collection of helper functions for values exchange with or without conditions, swap, ect.

namespace Jungle
{
namespace Utils
{
    template< typename T >
    inline void     Swap( T& val0, T& val1 );

    template< typename T >
    inline void     GreaterFirst( T& val0, T& val1 );

    template< typename T >
    inline void     SmallerFirst( T& val0, T& val1 );

    template< typename T >
    inline void     GreaterAbsFirst( T& val0, T& val1 );

    template< typename T >
    inline void     SmallerAbsFirst( T& val0, T& val1 );

} // namespace Utils

} // namespace Jungle

#include <jungle/utils/Swap.inl>

#endif // !defined __UTILS_SWAP_HPP__
// EOF

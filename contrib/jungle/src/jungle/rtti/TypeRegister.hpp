//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/rtti/TypeRegister.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED_RTTI_TYPE_REGISTER_HPP__
#define __INCLUDED_RTTI_TYPE_REGISTER_HPP__

// External includes
#include <list>

namespace Jungle
{
namespace Rtti
{
    //! Flat type identyfication class for custom RTTI implementation.
    /*!
    * This class is should be publicly extended with an registered class type as an template parameter:
    * Example:
    * File MyClass.hpp:
    *
    * namespace MyNamespace
    * {
    *   class MyClass : public Jungle::Rtti::TypeRegister< MyClass >
    *   {
    *   };
    *
    *   class MyOtherClass : public Jungle::Rtti::TypeRegister< MyOtherClass >
    *   {
    *   };
    * }
    *
    * After such declarations it is possible to recognize and compare objects types as in below example:
    * MyClass myClass;
    * MyOtherClass myOther;
    * myClass.IsTypeOf( myOther.GetTypeId() );
    * myOther.IsTypeOf( MyClass::TYPE );
    * myOther.GetTypeId() != MyClass::TYPE;
    * myOther != myClass;
    *
    * \note Namespace adhesion of every class does affect its type: such as if You declare MySpace1::MyClass and MySpace2::MyClass both
    * classes will definie different TypeRegisters.
    */
    class TypeRegister
    {
    public:
        typedef long            TypeId;

        //! Return object type identifier.
        inline TypeId           GetTypeId() const;

        //! Class method for getting type identifier.
        static inline TypeId    GetTypeId() const;

        //! Compare objects types identifiers.
        inline bool             IsTypeOf( const TypeId& typeId ) const;

        //! Compare objects types.
        template < class TBase >
        inline bool             IsTypeOf( const TypeRegister< TBase >& type ) const;

        static const TypeId     TYPE;

    private:
        static void             IdFactory();

    }; // class TypeHierarchy

} // namespace Rtti

} // namespace Jungle

#include <jungle/rtti/TypeRegister.inl>

#endif // !defined __INCLUDED_RTTI_TYPE_REGISTER_HPP__
// EOF

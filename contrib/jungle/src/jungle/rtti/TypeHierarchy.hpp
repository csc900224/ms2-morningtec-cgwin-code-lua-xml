//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/rtti/TypeHierarchy.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED_RTTI_TYPE_HIERARCHY_HPP__
#define __INCLUDED_RTTI_TYPE_HIERARCHY_HPP__

// External includes
#include <list>
#include <string>

namespace Jungle
{
namespace Rtti
{
    //! Hierarchial RTTI helper class, attached as an member to the classes requireing type extended type information.
    /*!
    * This class is never extended nor encapsulated manually. To include type information to you classes simply
    * call special macros in your class dec;aration and implementation as in example:
    *
    * File MyClass.hpp:
    *
    * namespace MyNamespace
    * {
    *   class MyBase
    *   {
    *       JUNGLE_DECLARE_TYPE_HIERARCHY;
    *   };
    *
    *   class MyClass : MyBase
    *   {
    *       JUNGLE_DECLARE_TYPE_HIERARCHY;
    *   };
    * }
    *
    * File MyBase.cpp
    * namespace MyNamespace
    * {
    *   JUNGLE_DEFINE_TYPE_HIERARCHY_BASE( MyNamespace, MyBase );
    *   // or JUNGLE_DEFINE_TYPE_HIERARCHY_0( MyNamespace, MyBase );
    * }
    *
    * File MyClass.cpp
    * namespace MyNamespace
    * {
    *   JUNGLE_DEFINE_TYPE_HIERARCHY( MyNamespace, MyClass, MyBase );
    *   // or JUNGLE_DEFINE_TYPE_HIERARCHY_1( MyNamespace, MyClass, MyBase );
    *   // or JUNGLE_DEFINE_TYPE_HIERARCHY_LIST( MyNamespace, MyClass, &MyBase::TYPE, NULL );
    * }
    *
    * This declarations allows you to compare types and check their coorelations, in this case, below expressions
    * will return true:
    * myClass.GetType().IsDerived( myBase.GetType() );
    * myClass.GetType().IsExactly( MyClass::TYPE );
    * myBase.GetType().IsDerived( MyBase::TYPE );
    *
    * \note When you have more sophisticated namespaces hierarchy it is recommended to split them with "::", as in
    * below example:
    * namespace MyNamespace
    * {
    *   namespace MySubNamespace
    *   {
    *       JUNGLE_DEFINE_TYPE_HIERARCHY( MyNamespace::MySubnamespace, MyClass, MyBase );
    *   }
    * }
    * \note Namespace declaration in TypeHierarchy is only informal and thus not used in actual type determination
    * so don't worry, actual types hierarchy will be checked when IsExactly(), IsDerived() are called. Nevertheless
    * when you read class name via GetName() the full type qualifier will be returned: "MyNamespace::MySubNamespace::MyClass"
    * in this case.
    * \note If you don't want to declare any namespace simply pass NULL as the first argument of JUNGLE_DEFINE_TYPE_HIERARCHY
    * macro call i.e.: JUNGLE_DEFINE_TYPE_HIERARCHY( NULL, MyClass, MyBase );
    */
    class TypeHierarchy
    {
    public:
        //! Class type identifier.
        typedef std::string     TypeId;

        //! Single base class constructor or even without base when called with NULL as the argument.
        /*!
        * Never call it manually, just for internal use.
        */
        explicit inline         TypeHierarchy( const char* className, const TypeHierarchy* baseClass );

        //! Variable base classes declaration, NULL is used as the classes list terminator
        /*!
        * Never call it manually, just for internal use.
        */
        explicit inline         TypeHierarchy( const char* className, ... );

        //! Return the class name in the convention 
        inline const char*      GetName() const;

        //! Compare the types using fast pointer comparison.
        inline bool             IsExactly( const TypeHierarchy& type ) const;

        //! Check if type derives from the other class - faster method.
        inline bool             IsDerived( const TypeHierarchy& type ) const;

        //! Slower but more flexible version of types comparison by their identifiers.
        inline bool             IsExactly( const TypeId& type ) const;

        //! Check type inheritance - slower version comparing types identifiers - more flexible.
        inline bool             IsDerived( const TypeId& type ) const;

    private:
        typedef std::list< const TypeHierarchy* >   TypesList;
        typedef TypesList::iterator                 TypesListIt;
        typedef TypesList::const_iterator           TypesListConstIt;

        TypeId                  m_className;
        TypesList               m_baseClasses;

    }; // class TypeHierarchy

    // Sample usage, which allows for using fastest types comparisons (in class declaration):
    /*
    * class ClassName
    * {
    *   JUNGLE_DECLARE_TYPE_HIERARCHY;
    * };
    */
    #define JUNGLE_DECLARE_TYPE_HIERARCHY \
    public: \
        static const Jungle::Rtti::TypeHierarchy TYPE; \
        \
        virtual const Jungle::Rtti::TypeHierarchy& GetType () const \
        { \
            return TYPE; \
        }

    // For internal use only
    #define JUNGLE_DEFINE_TYPE_CLASS(namespaceName, className) \
        #namespaceName != "0" ? #namespaceName"::"#className : #className

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY_BASE( MyNamespace, MyClass );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_BASE(namespaceName, className) \
        JUNGLE_DEFINE_TYPE_HIERARCHY_0( namespaceName, className )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY( MyNamespace, MyClass, MyBaseClass );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY(namespaceName, className, baseClass) \
        JUNGLE_DEFINE_TYPE_HIERARCHY_1( namespaceName, className, baseClass )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY_0( MyNamespace, MyClass );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_0(namespaceName, className) \
        const Jungle::Rtti::TypeHierarchy className::TYPE( JUNGLE_DEFINE_TYPE_CLASS( namespaceName, className ), NULL )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY_1( MyNamespace, MyClass, MyBaseClass );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_1(namespaceName, className, baseClass) \
        const Jungle::Rtti::TypeHierarchy className::TYPE( JUNGLE_DEFINE_TYPE_CLASS( namespaceName, className ), &baseClass::TYPE )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY_2( MyNamespace, MyClass, MyBase0, MyBase1 );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_2(namespaceName, className, base0, base1) \
        const Jungle::Rtti::TypeHierarchy className::TYPE( JUNGLE_DEFINE_TYPE_CLASS( namespaceName, className ), &base0::TYPE, &base1::TYPE, NULL )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY_3( MyNamespace, MyClass, MyBase0, MyBase1, MyBase2 );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_3(namespaceName, className, base0, base1, base2) \
        const Jungle::Rtti::TypeHierarchy className::TYPE( JUNGLE_DEFINE_TYPE_CLASS( namespaceName, className ), &base0::TYPE, &base1::TYPE, &base2::TYPE, NULL )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY_4( MyNamespace, MyClass, MyBase0, MyBase1, MyBase2, MyBase3 );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_4(namespaceName, className, base0, base1, base2, base3) \
        const Jungle::Rtti::TypeHierarchy className::TYPE( JUNGLE_DEFINE_TYPE_CLASS( namespaceName, className ), &base0::TYPE, &base1::TYPE, &base2::TYPE, &base3::TYPE, NULL )

    // Usage: JUNGLE_DEFINE_TYPE_HIERARCHY( MyNamespace, MyClass, &MyBaseClass0::TYPE, &MyBaseClass1::TYPE, ..., NULL );
    #define JUNGLE_DEFINE_TYPE_HIERARCHY_LIST(namespaceName, className, ... ) \
        const Jungle::Rtti::TypeHierarchy className::TYPE( JUNGLE_DEFINE_TYPE_CLASS( namespaceName, className ), __VA_ARGS__ )

} // namespace Rtti

} // namespace Jungle

#include <jungle/rtti/TypeHierarchy.inl>

#endif // !defined __INCLUDED_RTTI_TYPE_HIERARCHY_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/rtti/TypeRegister.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// External includes

namespace Jungle
{
namespace Rtti
{
    template < class TBase >
    const TypeRegister< TBase >::TypeId TypeRegister< TBase >::TYPE = (TypeId)TypeRegister< TBase >::IdFactory;

    template < class TBase >
    inline TypeId TypeRegister< TBase >::GetTypeId() const
    {
        return TYPE;
    }

    template < class TBase >
    inline TypeId TypeRegister< TBase >::GetTypeId() const
    {
        return TYPE;
    }

    template < class TBase >
    inline bool IsTypeOf( const TypeId& typeId ) const
    {
        return GetTypeId() == typeId;
    }

    template < class TBase >
    inline bool IsTypeOf( const TypeRegister< TBase >& type ) const
    {
        return GetTypeId() == type.GetTypeId();
    }

    void IdFactory()
    {
        // Just to generate unique pointer
    }

} // namespace Rtti

} // namespace Jungle
// EOF

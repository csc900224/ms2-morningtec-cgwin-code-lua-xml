//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/rtti/TypeHierarchy.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// External includes
#include <stdarg.h>

namespace Jungle
{
namespace Rtti
{
    TypeHierarchy::TypeHierarchy( const char* className, const TypeHierarchy* baseClass )
        : m_className( className )
    {
        if( baseClass )
            m_baseClasses.push_back( baseClass );
    }

    TypeHierarchy::TypeHierarchy( const char* className, ... )
        : m_className( className )
    {
        const TypeHierarchy* baseClass;
        va_list args;
        va_start( args, className );
        {
            // Process arguments until NULL value retreived - NULL is used as the list terminator
            while( baseClass = static_cast< const TypeHierarchy* >( va_arg( args, const TypeHierarchy* ) ) )
            {
                m_baseClasses.push_back( baseClass );
            }
        }
        va_end( args );
    }

    const char* TypeHierarchy::GetName() const
    {
        return m_className.c_str();
    }

    bool TypeHierarchy::IsExactly( const TypeHierarchy& type ) const
    {
        return this == &type;
    }

    bool TypeHierarchy::IsDerived( const TypeHierarchy& type ) const
    {
        // This is exactly same type ?
        if( this == &type )
            return true;

        TypesListConstIt baseIt = m_baseClasses.begin();
        TypesListConstIt baseEnd = m_baseClasses.end();
        while( baseIt != baseEnd )
        {
            if( (*baseIt)->IsDerived( type ) )
                return true;
            ++baseIt;
        }
        return false;
    }

    bool TypeHierarchy::IsExactly( const TypeId& typeId ) const
    {
        return m_className == typeId;
    }

    bool TypeHierarchy::IsDerived( const TypeId& typeId ) const
    {
        // This is exactly same type ?
        if( m_className == typeId )
            return true;

        TypesListConstIt baseIt = m_baseClasses.begin();
        TypesListConstIt baseEnd = m_baseClasses.end();
        while( baseIt != baseEnd )
        {
            if( (*baseIt)->IsDerived( typeId ) )
                return true;
            ++baseIt;
        }
        return false;
    }

} // namespace Rtti

} // namespace Jungle
// EOF

//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/SingletonPolices.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2008, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__SINGLETON_POLICES_HPP__
#define __PATTERNS__SINGLETON_POLICES_HPP__

// Internal includes
#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Patterns
{
    /**
    * Base singleton policy template.
    * Singleton base policy for holding singleton instance pointer.
    */
    template < typename T >
    class CreationPolicy
    {
    protected:
        static T*           s_pInstance;
    };

    /**
    * Static singleton pointer.
    * One for every policy defined, but inherited from CreationPolicy class template.
    */
    template< typename T >
    T* CreationPolicy< T >::s_pInstance = 0;

    /**
    * LazyCreation policy allows for singleton implicit creation
    * during singleton pointer access (GetInstance()), but requires
    * manual singleton release (Release() method)
    */
    template < typename T >
    class LazyCreation : public CreationPolicy< T >
    {
    public:
        /** Retrieve singleton pointer. */
        static T*           GetInstance();

        /** Create singleton instance. */
        static void         CreateInstance();

        /** Release the only instance created. */
        static void         Release();

    };

    /** Pointer access. */
    template< typename T >
    T* LazyCreation< T >::GetInstance()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            LazyCreation< T >::CreateInstance();
        }
        return CreationPolicy< T >::s_pInstance;
    }

    /** Instance creator. */
    template< typename T >
    void LazyCreation< T >::CreateInstance()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            new T();
        }
    }

    /** Release instance. */
    template< typename T >
    void LazyCreation< T >::Release()
    {
        delete CreationPolicy< T >::s_pInstance;
        CreationPolicy< T >::s_pInstance = 0;
    }


    /**
    * ExplicitCreation policy requires manual singleton initialization
    * via CreateInstance() method and manual singleton deallocation (Release()).
    * It disallows for multiple Release() call.
    */
    template < typename T >
    class ExplicitCreation : public CreationPolicy< T >
    {
    public:
        /** Retrieve singleton pointer. */
        static T*           GetInstance();

        /** Create singleton instance. */
        static void         CreateInstance();

        /** Release the only instance created. */
        static void         Release();

        /** Check if instance was already created. */
        static bool         IsCreated();
    };

    /** Pointer access. */
    template< typename T >
    T* ExplicitCreation< T >::GetInstance()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            // Assert here or allow instance creation
            JUNGLE_ASSERT_MSG( false, "Singleton wasn't instantiated call CreateInstance() first" );
        }
        return CreationPolicy< T >::s_pInstance;
    }

    /** Instance creator. */
    template< typename T >
    void ExplicitCreation< T >::CreateInstance()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            new T();
        }
        else
        {
            // Assert here - multiple singleton creation
            JUNGLE_ASSERT_MSG( false, "Singleton already created - multiple initializations" );
        }
    }

    /** Release instance. */
    template< typename T >
    void ExplicitCreation< T >::Release()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            // Assert here - multiple singleton deletion
            JUNGLE_ASSERT_MSG( false, "Singleton wasn't instantiated and can not be released" );
        }
        delete CreationPolicy< T >::s_pInstance;
        CreationPolicy< T >::s_pInstance = 0;
    }

    /** Check if instance was already created. */
    template< typename T >
    bool ExplicitCreation< T >::IsCreated()
    {
        return CreationPolicy< T >::s_pInstance != NULL;
    }


    /**
    * This policy gives you the most freedom you don't care about singleton
    * creation as also about its releasing.
    * StaticCreation policy base on compiler "magic" :).
    * Function static objects are initialized when the control flow is
    * first passing its definition if one of criteria it meet:
    * - the initializer is not a compile-time constant,
    * - the static variable is an object with a constructor.
    * In addition, the compiler generates code so that after
    * initialization, the runtime support registers the variable for
    * automatic destruction at exit.
    */
    template < typename T >
    class StaticCreation : public CreationPolicy< T >
    {
    public:
        /** Retrieve singleton pointer. */
        static T*           GetInstance();

        /** Create singleton instance. */
        static void         CreateInstance();

        /** Release the only instance created. */
        static void         Release();

        /** Check if instance was already destroyed. */
        static bool         IsDestroyed();

    protected:
        virtual             ~StaticCreation();

        static bool         s_destroyed;
    };

    template< typename T >
    bool StaticCreation< T >::s_destroyed = false;

    /** Pointer access. */
    template< typename T >
    T* StaticCreation< T >::GetInstance()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            // Assert here or allow instance creation
            if( s_destroyed )
            {
                JUNGLE_ASSERT_MSG( false, "Singleton dead reference detected" );
            }
            else
            {
                // First time initialize
                CreateInstance();
            }
        }
        return CreationPolicy< T >::s_pInstance;
    }

    /** Instance creator. */
    template< typename T >
    void StaticCreation< T >::CreateInstance()
    {
        if( !CreationPolicy< T >::s_pInstance )
        {
            static T instance;
            CreationPolicy< T >::s_pInstance = &instance;
        }
    }

    /** Release instance. */
    template< typename T >
    void StaticCreation< T >::Release()
    {
    }

    /** Special destructor for controlling dead references. */
    template< typename T >
    StaticCreation< T >::~StaticCreation()
    {
        CreationPolicy< T >::s_pInstance = 0;
        s_destroyed = true;
    }

    /** Check if instance was already destroyed. */
    template< typename T >
    bool StaticCreation< T >::IsDestroyed()
    {
        return s_destroyed;
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__SINGLETON_POLICES_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/EventHandler.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2011, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation and/or other materials
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors
//   may be used to endorse or promote products derived from this software without specific
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__EVENT_HANDLER_HPP__
#define __PATTERNS__EVENT_HANDLER_HPP__

// Internal includes

// External includes

namespace Jungle
{
namespace Patterns
{
    // Forward declarations
    template < class TEvent >
    class EventDispatcher;

    //! Typical events handler interface.
    /*!
    * Instances of class derived from it should be registered to event dispatcher in order
    * to retreive events notifications.
    */
    template< class TEvent >
    class EventHandler
    {
    public:
        // Just self definition
        typedef EventHandler< TEvent >          self_type;

        //! Wrapper on TEvent template parameter
        typedef TEvent                          EventType;
        //! Easier access to generated EventDispatcher type.
        typedef EventDispatcher< TEvent >       Dispatcher;

        //! Process event incomming from dispatcher.
        /*!
        * \return true if even was handled proper
        */
        virtual bool    HandleEvent( const TEvent& ev ) = 0;

    }; // class EventHandler

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__EVENT_HANDLER_HPP__
// EOF

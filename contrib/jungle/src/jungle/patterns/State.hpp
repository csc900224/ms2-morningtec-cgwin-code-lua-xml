//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/patterns/State.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED_PATTERNS_STATE_HPP__
#define __INCLUDED_PATTERNS_STATE_HPP__

// Internal includes
#include <jungle/base/Types.hpp>

namespace Jungle
{
namespace Patterns
{
    // Forward declaration
    template< class Actor, class StateId >
    class StackSM;

    //! Abstract state class template.
    /*!
    * Basic game logic State definition. State template serves as an easy way to define several states
    * for single game actor. States derived from this templates must use the same template parameter list
    * in order to be served with the same state machine class.
    * \sa Patterns::StackSM
    */
    template < class TActor, class TStateId = int >
    class State
    {
    public:
        //! Internal types definition - state id.
        typedef TStateId                Id;
        //! Actor controlled by state
        typedef TActor                  Actor;
        //! State machine type
        typedef StackSM< Actor, Id >    StateMachine;

    public:
        //! Virtual destructor to allow proper release of derived classes via StackSM::UnregisterState.
        virtual             ~State() {}

        //! State enter event handler.
        /*!
        * Handler called when state is pushed on the top of state machine stack or state replaces
        * currently active state (\sa StackSM::ChangeState() )
        */
        virtual void        OnEnter( Actor& actor, StateMachine& sm, const Id& previousState )  = 0;

        //! Method called every frame when state (state machine) is updated.
        /*!
        * In practice OnUpdate handler is called only when state is at the top of current states stack,
        * which means is the most actual state. OnUpdate is not called anymore when state is popped of the
        * stack or another states hides it on the stack.
        */
        virtual void        OnUpdate( Actor& actor, StateMachine& sm, uint32 tick )             = 0;

        //! Invoked when states is removed from the states stack.
        /*!
        * Handler called when state is changed or popped
        * from the top of state machine stack.
        */
        virtual void        OnExit( Actor& actor, StateMachine& sm, const Id& nextState )       = 0;

        //! Method called when state is moved down from the top of the state machine stack.
        virtual void        OnSuspend( Actor& /* actor */, StateMachine& /*sm*/, const Id& /* nextState */ )    { };
        //! Event handler when state is moved back to the top of state machine stack.
        virtual void        OnResume( Actor& /* actor */, StateMachine& /*sm*/, const Id& /* prevState */ )     { };
        //! Update method for states that are not on the top of state machine stack.
        virtual void        OnSleep( Actor& /*actor */, StateMachine& /*sm*/, uint32 /*tick*/ )                 { };

    }; // class State

} // namespace Patterns

} // namespace Jungle

#endif // !defined __INCLUDED_PATTERNS_STATE_HPP__
// EOF

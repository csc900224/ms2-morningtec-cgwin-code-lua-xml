//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/Controllable.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2012, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation and/or other materials
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors
//   may be used to endorse or promote products derived from this software without specific
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__CONTROLLABLE_HPP__
#define __PATTERNS__CONTROLLABLE_HPP__

// Internal includes

// External includes
#include <vector>
#include <algorithm>

namespace Jungle
{
namespace Patterns
{
    // Forward declarations
    template< class TControlledBase, class TId >
    class Controller;

    //! Basic Controllable pattern class template.
    /*!
    * Derive from this class to retreive easy to use interface for objects manipulation via
    * Controller interface. Controller (\see Controller) based objects may be then attached/detached to allow
    * multi-source object control (many controllers may manipulate single Controllable entity)/
    */
    template < class TBase, class TControllerId = int >
    class Controllable
    {
    public:
        typedef TBase                           self_type;
        typedef TControllerId                   CId;

        //! Controllable type may already predefine controller type.
        typedef Controller< self_type, CId >    ControllerType;

        //! Attach controller which may process current entity.
        void            AttachController( ControllerType* controller );

        //! Unregister controller from processing this entity.
        /*!
        * \see Take a look at AttachController( ControllerType* controller ) method.
        */
        void            DetachController( const ControllerType* controller );

        //! Perfrom actual control on entity via controllers interface.
        /*!
        * You must call this method in the derivied classes in order to perform Controllable
        * update and actual control on an object via Controller objects.
        */
        void            UpdateControllers( float dt = 0 );

    protected:
        typedef std::vector< ControllerType* >          Controllers;
        typedef typename Controllers::iterator          ControllersIt;
        typedef typename Controllers::const_iterator    ControllersConstIt;

        Controllers     m_controllers;

    }; // class Controllable

    template < class TBase, class TControllerId >
    void Controllable< TBase, TControllerId >::AttachController( ControllerType* controller )
    {
        m_controllers.push_back( controller );
        controller->OnAttach( (TBase*)( this ) );
    }

    template < class TBase, class TControllerId >
    void Controllable< TBase, TControllerId >::DetachController( const ControllerType* controller )
    {
        ControllersIt it = std::find( m_controllers.begin(), m_controllers.end(), controller );
        if( it != m_controllers.end() )
        {
            (*it)->OnDetach( (TBase*)( this ) );
            m_controllers.erase( it );
        }
    }

    template < class TBase, class TControllerId >
    void Controllable< TBase, TControllerId >::UpdateControllers( float dt /* = 0 */ )
    {
        ControllersIt it = m_controllers.begin();
        ControllersIt end = m_controllers.end();
        for( ; it != end ; ++it )
        {
            (*it)->PerformControl( (TBase*)( this ), dt );
        }
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__CONTROLLABLE_HPP__
// EOF

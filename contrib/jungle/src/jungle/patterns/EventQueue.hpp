//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/EventQueue.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2011, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation and/or other materials
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors
//   may be used to endorse or promote products derived from this software without specific
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__EVENT_QUEUE_HPP__
#define __PATTERNS__EVENT_QUEUE_HPP__

// Internal includes
#include <jungle/patterns/EventHandler.hpp>
#include <jungle/patterns/EventDispatcher.hpp>

// External includes
#include <vector>
#include <algorithm>

namespace Jungle
{
namespace Patterns
{
    //! Extended EventDispatcher with possibility to postpone events broadcats.
    /*!
    * This is extension of the EventDispatcher class which allows to postpone events broadcasting in
    * situation when event can not be directly pushed to all handlers. There are many cases when you
    * do not want to handle event immediatelly, just after their retreive, for example:
    * - underlaying system notifies about events via hardware interrupts and you should not perform
    * harder processing there (lightweight interrupts).
    * - events are asynchronous and you want to perform all events processing in update loop.
    * In all of this cases it is wise to use EventQueue instead of direct events dispatching.
    * Events are stored in queue and broadcasted after EventQueue::Flush() method call.
    * \note It is important to note that Event is passed by reference object and thus copied to
    * the queue, that is why you can not extend you custom Event type and use polymorphism to call
    * different implementations, because queue always stores only base type definition.
    */
    template< class TEvent >
    class EventQueue : public EventDispatcher< TEvent >
    {
    public:
        // Self type
        typedef EventQueue< TEvent >        self_type;
        typedef EventDispatcher< TEvent >   base_type;

        //! Enqueue event to be broadcasted on the next Flush() call.
        void            EnqueueEvent( const TEvent& ev );

        //! Perform events dispatching to all registered handlers.
        virtual void    Flush();

    protected:
        typedef typename base_type::Handlers        Handlers;
        typedef typename base_type::HandlersIt      HandlersIt;
        typedef typename base_type::HandlersConstIt HandlersConstIt;

        typedef std::vector< TEvent >               Queue;
        typedef typename Queue::iterator            QueueIt;
        typedef typename Queue::const_iterator      QueueConstIt;

        Queue           m_queue;

    }; // class EventDispatcher

    template< class TEvent >
    void EventQueue< TEvent >::EnqueueEvent( const TEvent& ev )
    {
        m_queue.push_back( ev );
    }

    template< class TEvent >
    void EventQueue< TEvent >::Flush()
    {
        QueueConstIt evIt = m_queue.begin();
        QueueConstIt evEnd = m_queue.end();
        HandlersConstIt hIt;
        HandlersConstIt hEnd = base_type::m_handlers.end();
        for( ; evIt != evEnd; ++evIt )
        {
            hIt = base_type::m_handlers.begin();
            while( hIt != hEnd )
            {
                (*hIt++)->HandleEvent( (*evIt) );
            }
        }
        m_queue.clear();
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__EVENT_QUEUE_HPP__
// EOF

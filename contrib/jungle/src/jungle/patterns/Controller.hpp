//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/Controller.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2012, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation and/or other materials
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors
//   may be used to endorse or promote products derived from this software without specific
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__CONTROLLER_HPP__
#define __PATTERNS__CONTROLLER_HPP__

// Internal includes

// External includes
#include <vector>
#include <algorithm>

namespace Jungle
{
namespace Patterns
{
    //! Simple controller class by default recognized via integer type id.
    /*!
    * Evert controller may be attached to an Controllable (\see Controllable) entity which will internally
    * perform its update passing the pointer to itself for object manipulation (\see Controllable::UpdateControllers()).
    * There may be many Controller objects controlling a single entity.
    * Each controller when attached receive OnAttach() callback, simillary OnDetach() method is called
    * in response on detaching controller from controlled entity.
    */
    template< class TControllable, class TId = int >
    class Controller
    {
    public:
        // Just self type shortcut
        typedef Controller< TControllable, TId >    self_type;

        //! Easy access to related Controllable class type.
        typedef TControllable                       ControlledType;

        //! Construct controller with a given identifier.
        inline              Controller( const TId& id );

        //! Process controll logic on controlled entity.
        virtual void        PerformControl( ControlledType* ct, float dt = 0 ) = 0;

        //! Called after controlled is actually attached to the controlled class.
        virtual void        OnAttach( ControlledType* ct )  {};

        //! Called just before controller is detached from controlled entity.
        virtual void        OnDetach( ControlledType* ct )  {};

        //! Get id.
        inline const TId&   GetId() const;

    protected:
        TId                 m_id;

    }; // class Controller

    template< class TDerived, class TId >
    inline Controller< TDerived, TId >::Controller( const TId& id )
        : m_id( id )
    {}

    template< class TDerived, class TId >
    inline const TId& Controller< TDerived, TId >::GetId() const
    {
        return m_id;
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__EVENT_DISPATCHER_HPP__
// EOF

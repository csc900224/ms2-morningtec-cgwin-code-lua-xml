//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/EventDispatcher.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2011, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation and/or other materials
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors
//   may be used to endorse or promote products derived from this software without specific
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__EVENT_DISPATCHER_HPP__
#define __PATTERNS__EVENT_DISPATCHER_HPP__

// Internal includes

// External includes
#include <list>
#include <algorithm>

namespace Jungle
{
namespace Patterns
{
    // Forward declarations
    template < class TEvent >
    class EventHandler;

    //! Basic event dispatching interface for immediate events broadcasting.
    /*!
    * Interface of an events manager class that allows dispatching information about certain
    * events to all registered EventHandler(s). Template class allows any type of events to be used.
    * Extend this class to implement custom case specific dispatchers.
    */
    template< class TEvent >
    class EventDispatcher
    {
    public:
        // Just self type shortcut
        typedef EventDispatcher< TEvent >   self_type;

        //! Easy access to related Event type.
        typedef TEvent                      EventType;
        //! Related Handler type easy access.
        typedef EventHandler< TEvent >      Handler;

        //! Virtual destructor for possible derived implementations.
        virtual         ~EventDispatcher() {}

        //! Register event handler to listen for events.
        /*!
        * \note Make note that registering class should also take care about handler
        * unregister before relasing handler object. Otherwise EventDispatcher may
        * direct to released memory region during events broadcast. Common technique
        * resolves it by registering handler in its constructor method and performing
        * unregister during object release (in destructor).
        */
        void            RegisterHandler( Handler* handler );

        //! Unregister handler from listening for events.
        /*!
        * \see Take a look at RegisterHandler( HandlerType* handler ) method.
        */
        void            UnregisterHandler( const Handler* handler );

        //! Broadcast event to the all handlers.
        void            DispatchEvent( const EventType& ev );

    protected:
        typedef std::list< Handler* >               Handlers;
        typedef typename Handlers::iterator         HandlersIt;
        typedef typename Handlers::const_iterator   HandlersConstIt;

        Handlers        m_handlers;

    }; // class EventDispatcher

    template< class TEvent >
    void EventDispatcher< TEvent >::RegisterHandler( Handler* handler )
    {
        m_handlers.push_back( handler );
    }

    template< class TEvent >
    void EventDispatcher< TEvent >::UnregisterHandler( const Handler* handler )
    {
        HandlersIt it = std::find( m_handlers.begin(), m_handlers.end(), handler );
        if( it != m_handlers.end() )
        {
            m_handlers.erase( it );
        }
    }

    template< class TEvent >
    void EventDispatcher< TEvent >::DispatchEvent( const TEvent& ev )
    {
        HandlersIt it = m_handlers.begin();
        HandlersIt next;
        for( ; it != m_handlers.end() ; )
        {
            // This trick will allow to modify handlers list inside the HandleEvent implementation,
            // such as EventHandler(s) may unregister themself from EventDispatcher (even in the HandleEvent body).
            // We use list container cause its iterators will be valid even after erase() call.
            next = it;
            ++next;
            (*it)->HandleEvent( ev );
            it = next;
        }
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__EVENT_DISPATCHER_HPP__
// EOF

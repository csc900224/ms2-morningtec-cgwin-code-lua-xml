//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//     jungle/patterns/StackAdapter.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED_PATTERNS_STACK_ADAPTER_HPP__
#define __INCLUDED_PATTERNS_STACK_ADAPTER_HPP__

// External includes
#include <deque>
#include <stack>

namespace Jungle
{
namespace Patterns
{
    //! Simple std::stack wrapper that allows for iterating stack elements.
    template < class T, class Container = std::deque< T > >
    class StackAdapter : public std::stack< T, Container >
    {
    public:
        // Types definitions
        typedef typename Container::iterator               iterator;
        typedef typename Container::const_iterator         const_iterator;
        typedef typename Container::reverse_iterator       reverse_iterator;
        typedef typename Container::const_reverse_iterator const_reverse_iterator;

        iterator begin()
        {
            return this->c.begin();
        }

        const_iterator begin() const
        {
            return const_iterator(this->c.begin());
        }

        iterator end()
        {
            return this->c.end();
        }

        const_iterator end() const
        {
            return const_iterator(this->c.end());
        }

        reverse_iterator rbegin()
        {
            return reverse_iterator(this->c.end());
        }

        const_reverse_iterator rbegin() const
        {
            return const_reverse_iterator(this->c.end());
        }

        reverse_iterator rend()
        {
            return reverse_iterator(this->c.begin());
        }

        const_reverse_iterator rend() const
        {
            return const_reverse_iterator(this->c.begin());
        }

    }; // class StackAdapter

} // namespace Patterns

} // namespace Jungle

#endif // !defined __INCLUDED_PATTERNS_STACK_ADAPTER_HPP__
// EOF

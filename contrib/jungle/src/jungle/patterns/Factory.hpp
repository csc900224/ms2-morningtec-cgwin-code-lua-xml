/* ///////////////////////////////////////////////////////////////////////////// 
 *  FILE:
 *      jungle/patterns/Factory.hpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2009, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

#ifndef __INCLUDED_PATTERNS__FACTORY_HPP__
#define __INCLUDED_PATTERNS__FACTORY_HPP__

#include <map>

namespace Jungle
{
namespace Patterns
{
    //! Simple Factory class that uses sinlge parameter object consturctors.
    template< typename TBaseType, typename TKeyType, typename TConstructorParam = void >
    class Factory
    {
    public:
        // Object Creator
        typedef TBaseType* (*CreateObjectFunc)( TConstructorParam );

        // Type definitions
        typedef std::map<TKeyType, CreateObjectFunc>    FactoryMap;
        typedef typename FactoryMap::const_iterator     FactoryMapIt;
        typedef TBaseType                               ProductionType;

        //! Registers object type with given key value. If key value is already taken - false is returned.
        template <typename TSpecificType >
        bool Register( TKeyType key );

        //! Creates objec for a given key value. If value could not be found - NULL is returned
        TBaseType* Create( TKeyType key, TConstructorParam param ) const;

    protected:
        //! Creates object of given class
        template< typename TClass >
        static TBaseType* CreateObject( TConstructorParam param );

    private:
        //! Map of <key, creator function pointer> pairs
        FactoryMap m_creatorMap;
    };

    //! Simple Factory class that uses parameterless object consturctors.
    template< typename TBaseType, typename TKeyType >
    class Factory<TBaseType, TKeyType, void>
    {
    public:
        // Object Creator
        typedef TBaseType* (*CreateObjectFunc)();

        // Type definitions
        typedef std::map<TKeyType, CreateObjectFunc>    FactoryMap;
        typedef typename FactoryMap::const_iterator     FactoryMapIt;
        typedef TBaseType                               ProductionType;

        //! Registers object type with given key value. If key value is already taken - false is returned.
        template <typename TSpecificType >
        bool Register( TKeyType key );

        //! Creates objec for a given key value. If value could not be found - NULL is returned
        TBaseType* Create( TKeyType key ) const;

    protected:
        //! Creates object of given class
        template< typename TClass >
        static TBaseType* CreateObject();

    private:
        //! Map of <key, creator function pointer> pairs
        FactoryMap m_creatorMap;
    };


    //! Register type
    template< typename TBaseType, typename TKeyType, typename TConstructorParam >
    template< typename TSpecificType >
    bool Factory<TBaseType,TKeyType,TConstructorParam>::Register( TKeyType key )
    {
        FactoryMapIt it = m_creatorMap.find( key );
        if( it != m_creatorMap.end() )
        {
            return false;
        }
        else
        {
            m_creatorMap[key] = &CreateObject<TSpecificType>;
            return true;
        }
    }

    //! Create object using key
    template< typename TBaseType, typename TKeyType, typename TConstructorParam >
    TBaseType* Factory<TBaseType,TKeyType,TConstructorParam>::Create( TKeyType key, TConstructorParam param ) const
    {
        FactoryMapIt it = m_creatorMap.find( key );
        if( it == m_creatorMap.end() )
            return NULL;
        else 
            return (*it).second(param);
    }
    
    //! Create object of given class
    template< typename TBaseType, typename TKeyType, typename TConstructorParam >
    template< typename TClass >
    TBaseType* Factory<TBaseType,TKeyType,TConstructorParam>::CreateObject(TConstructorParam param)
    {
        return new TClass(param);
    }

    //! Register type
    template< typename TBaseType, typename TKeyType>
    template< typename TSpecificType >
    bool Factory<TBaseType,TKeyType>::Register( TKeyType key )
    {
        FactoryMapIt it = m_creatorMap.find( key );
        if( it != m_creatorMap.end() )
        {
            return false;
        }
        else
        {
            m_creatorMap[key] = &CreateObject<TSpecificType>;
            return true;
        }
    }

    //! Create object using key
    template< typename TBaseType, typename TKeyType>
    TBaseType* Factory<TBaseType,TKeyType>::Create( TKeyType key ) const
    {
        FactoryMapIt it = m_creatorMap.find( key );
        if( it == m_creatorMap.end() )
            return NULL;
        else 
            return (*it).second();
    }
    
    //! Create object of given class
    template< typename TBaseType, typename TKeyType>
    template< typename TClass >
    TBaseType* Factory<TBaseType,TKeyType>::CreateObject()
    {
        return new TClass();
    }
} // namespace Patterns
} // namespace Jungle

#endif // __INCLUDED_PATTERNS__FACTORY_HPP__
// EOF
//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/patterns/StackSM.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED_PATTERNS_STACK_SM_HPP__
#define __INCLUDED_PATTERNS_STACK_SM_HPP__

// Internal includes
#include <jungle/patterns/State.hpp>
#include <jungle/patterns/StackAdapter.hpp>

#include <jungle/base/Errors.hpp>
#include <jungle/base/Types.hpp>

// External includes
#include <map>
#include <vector>
#include <algorithm>
#include <list>

namespace Jungle
{
namespace Patterns
{
    //! Stack based state machine implementation.
    /*!
    * State machine class template for easy managing immediate and delayed states changes and stacking.
    * It allows for collecting processed states on the stack such as some state may cover other one
    * for some time (imagine GameState being stacked under PauseState during pause menu display),
    * poping top state from the stack automatically returns the underaying one to the normal processing.
    * Moreover if someone requires bottom states processing (i.e. GameState during PauseState)
    * he may implement State::OnSleep method. State return to the stack top as bottom push are immediatelly
    * reported via State::OnResume, State::OnSuspend methods.
    * \note States are registered via pointers such as ownership is given to the state machine, this way there is
    * no need to continous memory allocation/deallocation during state changes and this way each State may "remember" its
    * own data giving architect more flexibility to implement sophisticated logics without additional storage class overhead.
    * Please note that this way all variables that would be reset in constructor should be setup in State::OnEnter method.
    * \note There is no problem with registering the same State classes object with different ids, this way single state class may
    * even handle different (slightly modified) logics such as: StateMove may cover both walking and running character if initialized
    * with different seed settings.
    * \see State class interface.
    */
    template < class TActor, class TStateId = int >
    class StackSM
    {
    public:
        typedef             TActor                                          Actor;
        typedef             TStateId                                        StateId;
        typedef             State< Actor, StateId >                         StateType;
        typedef typename    std::map< StateId, StateType* >                 StatesRegister;
        typedef             StackAdapter< StateType* >                      StatesStack;

    private:
        // Forward declarations
        class PendingTask;

        // Internal types definitions
        typedef enum _TaskId
        {
            TASK_CHANGE_STATE = 0,
            TASK_PUSH_STATE,
            TASK_POP_STATE,
            TASK_POP_ALL
        } TaskId;

        typedef typename    StatesStack::reverse_iterator                   StatesStackReverseIterator;

        typedef typename    StatesRegister::iterator                        StatesRegisterIterator;
        typedef typename    StatesRegister::const_iterator                  StatesRegisterConstIterator;
        typedef typename    StatesRegister::reverse_iterator                StatesRegisterReverseIterator;

        typedef             PendingTask                                     Task;
        typedef typename    std::list< Task >                               TasksQueue;
        typedef typename    std::list< Task >::iterator                     TasksQueueIterator;

    public:
        //! Construct state machine with an idle state identifier for easier "idling" identifcation.
                            StackSM( const StateId& idleStateId );

        //! Update states
        void                Update( Actor& owner, uint32 tick );

        //! Register new state instance.
        bool                RegisterState( const StateId& id, StateType* pState );

        //! Un-register new state instance.
        bool                UnregisterState( const StateId& id, bool autoRelease = false );

        //! Get state instance already registered.
        const StateType*    GetState( const StateId& id ) const;

        //! Get registered state id.
        const StateId*      GetStateId( const StateType* state ) const;

        //! Acquire currently active state - the one on top of stack.
        const StateType*    GetCurrentState() const;

        //! Get active state id - state on top of stack.
        const StateId*      GetCurrentStateId() const;

        //! Access curent states stack to allow all (active and suspended) states iteration.
        const StatesStack&  GetStatesStack() const;

        //! Access container with all registered states.
        const
        StatesRegister&     GetStatesRegister() const;

        //! Change active state.
        void                ChangeState( const StateId& id, uint32 delay = 0, bool bFlushQueue = false );

        //! Push new state on top of stack.
        void                PushState( const StateId& id, uint32 delay = 0, bool bFlushQueue = false );

        //! Pop currently active state - from top of stack.
        void                PopState( uint32 statesToPop = 1, uint32 delay = 0, bool bFlushQueue = false );

        //! Pop all states - newly active state have no instance but only id registered on StackSM creation.
        void                PopAllStates( uint32 delay = 0, bool bFlushWholeQueue = false );

        //! Get number of stacked states.
        inline uint32       GetStackDepth();

        //! Get number of states waiting in the queue
        inline uint32       GetQueueLength() const;

        //! Clear states changes queue - this involves canceling all pending states operations.
        void                ClearQueue();

        //! Clear states changes queue, removing only operations concerning state with passed id.
        void                ClearQueue( const StateId& stateId );

    private:
        void                AddTask( TaskId task, const StateId& id, uint32 delay = 0, bool bFlushQueue = false );

        void                ClearQueue( TaskId tasksToClear );

        void                ProcessTasksQueue( Actor& owner, uint32 tick );

        void                ProcessStatesUpdate( Actor& owner, uint32 tick );

        void                PopStateImm( Actor& owner, StateType* statePtr, const StateId& stateId );

        void                PushStateImm( Actor& owner, StateType* statePtr, const StateId& stateId );

        void                ChangeStateImm( Actor& owner, StateType* statePtr, const StateId& stateId );

    private:
        //! Helper internal class for storing pending states changes and handling delayed state switches.
        class PendingTask
        {
        public:
            // Types definitions
            typedef TActor                      Actor;
            typedef TStateId                    StateId;
            typedef State< Actor, StateId >     StateType;

            PendingTask( TaskId taskType, StateType* statePtr, const StateId& id, uint32 delay )
                : m_type( taskType )
                , m_pState( statePtr )
                , m_delay( delay )
                , m_id( id )
            {
            }

            inline TaskId               GetType() const                 { return m_type;    }

            inline StateType*           GetStatePtr() const             { return m_pState;  }

            inline const StateId&       GetStateId() const              { return m_id;      }

            inline uint32               GetDelay() const                { return m_delay;   }

            inline void                 SetDelay( const uint32& delay ) { m_delay = delay;  }

        private:
            TaskId                      m_type;
            StateType*                  m_pState;
            StateId                     m_id;
            uint32                      m_delay;
        }; // class PendingTask

        //! Unary predicate template for fast tasks list traversal
        template < class PendingTask, TaskId type >
        class IsType : public std::unary_function< PendingTask, bool >
        {
        public:
            bool operator()( PendingTask& task )
            {
                return task.GetType() == type;
            }
        }; // class IsType

        //! Unary predicate template for fast checking tasks' states
        template < class PendingTask, typename PendingTask::StateId stateId >
        class HasState : public std::unary_function< PendingTask, bool >
        {
        public:
            bool operator()( PendingTask& task )
            {
                return task.GetStateId() == stateId;
            }
        }; // class HasState

        StatesRegister      m_register;
        StatesStack         m_stack;

        StateId             m_currentId;
        StateId             m_idleStateId;

        TasksQueue          m_queue;
    };

    template < class TActor, class TStateId >
    StackSM< TActor, TStateId >::StackSM( const StateId& idleStateId )
        : m_idleStateId( idleStateId )
        , m_currentId( idleStateId )
    {
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::Update( Actor& owner, uint32 tick )
    {
        ProcessTasksQueue( owner, tick );
        ProcessStatesUpdate( owner, tick );
    }

    template < class TActor, class TStateId >
    bool StackSM< TActor, TStateId >::RegisterState( const StateId& id, StateType* pState )
    {
        std::pair< StatesRegisterIterator, bool > pr;
        pr = m_register.insert( std::pair< StateId, StateType* >( id, pState ) );
        return pr.second;
    }

    template < class TActor, class TStateId >
    bool StackSM< TActor, TStateId >::UnregisterState( const StateId& id, bool autoRelease /* = false */ )
    {
        StatesRegisterIterator it = m_register.find( id );
        if( it == m_register.end() )
        {
            JUNGLE_WARNING_MSG( "[StackSM::UnRegisterState()] States register doesn't contain State with specified id" );
            return 0;
        }
        else
        {
            // Clear all pending state changes concerning this state id.
            ClearQueue( id );

            if( autoRelease )
                delete it->second;

            m_register.erase( it );
            return true;
        }
    }

    template < class TActor, class TStateId >
    const typename StackSM< TActor, TStateId >::StateType* StackSM< TActor, TStateId >::GetState( const StateId& id ) const
    {
        StatesRegisterConstIterator it = m_register.find( id );
        if( it == m_register.end() )
        {
            JUNGLE_WARNING_MSG( "[StackSM::GetState()] States register doesn't contain State with specified id" );
            return 0;
        }
        else
        {
            return it->second;
        }
    }

    template < class TActor, class TStateId >
    const TStateId* StackSM< TActor, TStateId >::GetStateId( const StateType* state ) const
    {
        StatesRegisterConstIterator it = m_register.begin();
        StatesRegisterConstIterator end = m_register.end();
        for( ; it != end ; ++it )
        {
            if( it->second == state )
                return &it->first;
        }
        JUNGLE_WARNING_MSG( "[StackSM::GetStateId()] State on specified address, has not been registered to this SSM" );
        return 0;
    }

    template < class TActor, class TStateId >
    const typename StackSM< TActor, TStateId >::StateType* StackSM< TActor, TStateId >::GetCurrentState() const
    {
        if( m_stack.empty() )
        {
            JUNGLE_WARNING_MSG( "[StackSM::GetCurrentState()] There is no active state on stack -StackSM is in FREEZE/IDLE state" );
            return 0;
        }
        else
        {
            return m_stack.top();
        }
    }

    template < class TActor, class TStateId >
    const TStateId* StackSM< TActor, TStateId >::GetCurrentStateId() const
    {
        return &m_currentId;
    }

    template < class TActor, class TStateId >
    const typename StackSM< TActor, TStateId >::StatesStack& StackSM< TActor, TStateId >::GetStatesStack() const
    {
        return m_stack;
    }

    template < class TActor, class TStateId >
    const typename StackSM< TActor, TStateId >::StatesRegister& StackSM< TActor, TStateId >::GetStatesRegister() const
    {
        return m_register;
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ChangeState( const StateId& id, uint32 delay /* = 0 */, bool bFlushQueue /* = false */ )
    {
        AddTask( TASK_CHANGE_STATE, id, delay, bFlushQueue );
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::PushState( const StateId& id, uint32 delay /* = 0 */, bool bFlushQueue /* = false */ )
    {
        AddTask( TASK_PUSH_STATE, id, delay, bFlushQueue );
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::PopState( uint32 statesToPop /* = 1 */, uint32 delay /* = 0 */, bool bFlushQueue /* = false */ )
    {
        while( statesToPop-- )
        {
            AddTask( TASK_POP_STATE, m_currentId, delay, bFlushQueue );
        }
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::PopAllStates( uint32 delay /* = 0 */, bool bFlushWholeQueue /* = false */ )
    {
        // Doesn't flush only specified task queue - flush all previous commands
        if( bFlushWholeQueue )
        {
            ClearQueue();
        }
        AddTask( TASK_POP_ALL, m_currentId, delay, false );
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::AddTask( TaskId task, const StateId& id, uint32 delay /* = 0 */, bool bFlushQueue /* = false */ )
    {
        if( bFlushQueue )
        {
            ClearQueue( task );
        }

        if( task == TASK_POP_STATE || task == TASK_POP_ALL )
        {
            m_queue.push_back( Task( task, NULL, id, delay ) );
        }
        else
        {
            StatesRegisterConstIterator stateIt = m_register.find( id );
            if( stateIt == m_register.end() )
            {
                JUNGLE_ASSERT( !"[StackSM::AddTask()] Required state is not registered in SSM. You can not add task with this state to queue");
            }
            else
            {
                m_queue.push_back( Task( task, stateIt->second, id, delay ) );
            }
        }
    }

    template < class TActor, class TStateId >
    inline uint32 StackSM< TActor, TStateId >::GetStackDepth()
    {
        return m_stack.size();
    }

    template < class TActor, class TStateId >
    inline uint32 StackSM< TActor, TStateId >::GetQueueLength() const
    {
        return m_queue.size();
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ClearQueue()
    {
        m_queue.clear();
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ClearQueue( const StateId& stateId )
    {
        TasksQueueIterator taskIt = m_queue.begin();
        // To support both vector and list container end iterator can not cached.
        // In vector container iterators are no longer valid after erase
        while( taskIt != m_queue.end() )
        {
            const Task& pendingTask = *taskIt;
            if( pendingTask.GetStateId() == stateId )
            {
                taskIt = m_queue.erase( taskIt );
            }
            else
            {
                ++taskIt;
            }
        }
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ClearQueue( TaskId tasksToClear )
    {
        switch( tasksToClear )
        {
        case TASK_CHANGE_STATE:
            m_queue.remove_if( IsType< Task, TASK_CHANGE_STATE >() );
            break;
        case TASK_PUSH_STATE:
            m_queue.remove_if( IsType< Task, TASK_PUSH_STATE >() );
            break;
        case TASK_POP_STATE:
            m_queue.remove_if( IsType< Task, TASK_POP_STATE >() );
            break;
        case TASK_POP_ALL:
            m_queue.remove_if( IsType< Task, TASK_POP_ALL >() );
            break;
        default:
            JUNGLE_ASSERT( !"[StackSM::ClearQueue()] Unsupported task type passed." );
        }
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ProcessStatesUpdate( Actor& owner, uint32 tick )
    {
        JUNGLE_ASSERT_MSG( tick > 0, "Zero time progression is dangerous for underalying logic and may cause unpredicatble errors (physics, math)" );

        if( m_stack.size() )
        {
            m_stack.top()->OnUpdate( owner, *this, tick );

            StatesStackReverseIterator it = m_stack.rbegin();
            StatesStackReverseIterator end = m_stack.rend();
            for( ;++it != end; )
            {
                (*it)->OnSleep( owner, *this, tick );
            }
        }
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ProcessTasksQueue( Actor& owner, uint32 tick )
    {
        JUNGLE_ASSERT_MSG( tick > 0, "Zero time progression is dangerous for underalying logic and may cause unpredicatble errors (physics, math)" );

        TasksQueueIterator taskIt = m_queue.begin();
        // To support both vector and list container end iterator can not cached.
        // In vector container iterators are no longer valid after erase
        while( taskIt != m_queue.end() )
        {
            Task& pendingTask = *taskIt;
            if( pendingTask.GetDelay() <= tick )    // In case of zero tick use equal-smaller
            {
                switch( pendingTask.GetType() )
                {
                case TASK_CHANGE_STATE:
                    ChangeStateImm( owner, pendingTask.GetStatePtr(), pendingTask.GetStateId() );
                    break;
                case TASK_PUSH_STATE:
                    PushStateImm( owner, pendingTask.GetStatePtr(), pendingTask.GetStateId() );
                    break;
                case TASK_POP_STATE:
                    PopStateImm( owner, pendingTask.GetStatePtr(), pendingTask.GetStateId() );
                    break;
                case TASK_POP_ALL:
                    while( m_stack.size() )
                    {
                        PopStateImm( owner, pendingTask.GetStatePtr(), pendingTask.GetStateId() );
                    }
                    break;
                default:
                    JUNGLE_ASSERT( !"[StackSM::ProcessTaskQueue()] Unrecognized task in queue" );
                }
                taskIt = m_queue.erase( taskIt );
            }
            else
            {
                pendingTask.SetDelay( pendingTask.GetDelay() - tick );
                ++taskIt;
            }
        }
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::PopStateImm( Actor& owner, StateType* /*statePtr*/, const StateId& /*stateId*/ )
    {
        // Allow for state pop only when there is still some state available
        // If there is not more states available set current state to IDLE
        // NOTE: Another approach is to check m_stack.size() > 1 and allow for stack 
        // pop only if there is more then one state available this will never come back
        // to IDLE/FREEZE state
        if( m_stack.size() )
        {
            StateType* popState = m_stack.top();
            m_stack.pop();

            // Create temporary variable for storing next state to be processed
            StateId nextStateId;
            if( m_stack.size() )
            {
                // Actualize currently active id
                const StateId* newStateId = GetStateId( m_stack.top() );
                JUNGLE_ASSERT( newStateId && "[StateSM::PopStateIm()] Newly active state is invalid or not registered" );
                nextStateId = *newStateId;
            }
            else
            {
                // Set current state to IDLE, nothing will be processed in update
                nextStateId = m_idleStateId;
            }
            
            // Invoke OnExit event on old active state
            popState->OnExit( owner, *this, nextStateId );

            // Store current state as previous one
            StateId prevStateId( m_currentId );
            
            // Set new state as a current
            m_currentId = nextStateId;

            // Once again check the stack size and invoke OnResume on the top state
            // This code block needs to be here to preserve the proper events order:
            // Firstly remove state (send OnExit event) and then activate state below (OnResume)
            if( m_stack.size() )
            {
                // Invoke OnResume event for the new active state
                m_stack.top()->OnResume( owner, *this, prevStateId );
            }
        }
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::PushStateImm( Actor& owner, StateType* statePtr, const StateId& stateId )
    {
        if( m_stack.size() )
        {
            // Invoke OnSuspend event on the old active state
            m_stack.top()->OnSuspend( owner, *this, stateId );
        }
        // For proper current state representation in OnEnter method, we should change 
        // the current state before method call, save previous state in temporary variable
        // to pass it to the method
        StateId prevStateId( m_currentId );

        // Set new id for active state
        m_currentId = stateId;

        // Invoke OnEnter event on the newly active state
        statePtr->OnEnter( owner, *this, prevStateId );

        // Push new state on the stack
        m_stack.push( statePtr );
    }

    template < class TActor, class TStateId >
    void StackSM< TActor, TStateId >::ChangeStateImm( Actor& owner, StateType* statePtr, const StateId& stateId )
    {
        if( m_stack.size() )
        {
            // Invoke OnExit event on the old active state
            m_stack.top()->OnExit( owner, *this, stateId );
            // Remove old state from the stack
            m_stack.pop();
        }
        // To properly read current state in OnEnter state event we need to save the new
        // state id before calling it, to store old state id and pass it to method 
        // save it in temporary variable
        StateId prevStateId( m_currentId );

        // Set new id for active state
        m_currentId = stateId;

        // Invoke OnEnter event on the newly active state
        statePtr->OnEnter( owner, *this, prevStateId );

        // Add new active state
        m_stack.push( statePtr );
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __INCLUDED_PATTERNS_STACK_SM_HPP__
// EOF

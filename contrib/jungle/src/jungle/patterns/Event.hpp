//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/Event.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2011, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation and/or other materials
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors
//   may be used to endorse or promote products derived from this software without specific
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__EVENT_HPP__
#define __PATTERNS__EVENT_HPP__

// Internal includes

// External includes

namespace Jungle
{
namespace Patterns
{
    // Forward declarations
    template< class TEvent >
    class EventHandler;

    template< class TEvent >
    class EventDispatcher;

    template< class TEvent >
    class EventQueue;

    //! Basic Event class template definition.
    /*!
    * Class may be used as a base for any type of events. Derived class should contain case
    * specifc members and provide access interface. This class simply exposes sub-type (id)
    * info, and allows usage with EventHandler, EventDispatcher classes.
    * Template parameter TDerived is supplied to allow easy types definitions for EventHandler,
    * EventDispatcher, as also to automatically generate their templates with class extending Event
    * template which allows to retreive derived members without pointer casting
    * \see EventHandler::HandleEvent().
    */
    template < class TDerived, class TEventId = int >
    class Event
    {
    public:
        typedef TDerived                        self_type;

        //! TypeId shorter notation.
        typedef TEventId                        Id;
        //! Event type already predefines handler type.
        typedef EventHandler< self_type >       Handler;
        //! Predefine dispatcher type.
        typedef EventDispatcher< self_type >    Dispatcher;
        //! Fast EventQueue type access.
        typedef EventQueue< self_type >         Queue;

        //! Construct event with a given sub-type id.
        inline          Event( const TEventId& id );

        //! Get event id.
        inline
        const TEventId& GetId() const;

    protected:
        TEventId        m_id;

    }; // class Event

    template< class TDerived, class TEventId >
    inline Event< TDerived, TEventId >::Event( const TEventId& id )
        : m_id( id )
    {}

    template< class TDerived, class TEventId >
    inline const TEventId& Event< TDerived, TEventId >::GetId() const
    {
        return m_id;
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__EVENT_HPP__
// EOF

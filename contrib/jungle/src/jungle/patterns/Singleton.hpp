//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/patterns/Singleton.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2008, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __PATTERNS__SINGLETON_HPP__
#define __PATTERNS__SINGLETON_HPP__

// Internal includes
#include <jungle/patterns/SingletonPolices.hpp>
#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Patterns
{
    /**
    * Basic singleton class with different creation polices.
    * Extend this class to implement custom singletons. Use new class name as the
    * template argument.
    */
    template < class Type, template <class> class Policy = LazyCreation >
    class Singleton : public Policy< Type >
    {
    public:
        /** Get singleton instance. */
        static const Type&  GetRef();

    protected:
        /** Protected constructor to disallow manual allocation. */
                            Singleton();

        /** Protected coping constructor - to prevent coping. */
                            Singleton( const Singleton& rInstance );

        /** Protected copy operator to prevent "accidental" copy. */
        Singleton&          operator =( const Singleton& src );

        /** Protected destructor to prevent users that holds the pointer from deleting it. */
        virtual             ~Singleton();
    };

    /** Object access. */
    template< class Type, template < class > class Policy >
    const Type& Singleton< Type, Policy >::GetRef()
    {
        return *Type::GetInstance();
    }

    /** Constructor. Offsets object pointer from base implementation. */
    template< class Type, template < class > class Policy >
    Singleton< Type, Policy >::Singleton()
    {
        // Assert if some instance has been already created
        if( CreationPolicy< Type >::s_pInstance )
        {
            // Assert here
            JUNGLE_ASSERT_MSG( false, "Singleton instance was already created" );
        }
        long long ptrOffset = ((long long)(Type*)0x1) - ((long long)(Singleton< Type >*)0x1);
        CreationPolicy< Type >::s_pInstance = (Type*)(this + ptrOffset);
    }

    /** Empty destructor. */
    template< class Type, template < class > class Policy >
    Singleton< Type, Policy >::~Singleton()
    {
    }

} // namespace Patterns

} // namespace Jungle

#endif // !defined __PATTERNS__SINGLETON_HPP__
// EOF

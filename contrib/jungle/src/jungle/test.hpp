#ifndef __JUNGLE__TEST_HPP__
#define __JUNGLE__TEST_HPP__

namespace Jungle
{
    class Test
    {
    public:
                Test();

        void    TestBase();
        void    TestPatterns();
        void    TestUtils();
        void    TestMath();
        void    TestMathGeo();
        void    TestMathApprox();
        void    TestMathStats();
        void    TestScene();
        void    TestRtti();

    }; // class Test

} // namespace Jungle

#endif // !defined __JUNGLE__TEST_HPP__
// EOF

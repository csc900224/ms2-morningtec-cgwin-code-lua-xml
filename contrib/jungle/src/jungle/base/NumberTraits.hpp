//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/base/NumberTraits.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2009, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_NUMBER_TRAITS_HPP__
#define __BASE_NUMBER_TRAITS_HPP__

// Internal includes
#include <jungle/base/Types.hpp>
#include <jungle/base/Errors.hpp>

// External includes
#include <limits>

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

namespace Jungle
{
    //! Template for various numerical types definitions.
    template < class T >
    class NumberTraits
    {
    public:
        typedef T                           value_type;
        typedef std::numeric_limits< T >    TLimits;

        static value_type const     ZERO            = (value_type)0;
        static value_type const     ZERO_TOLERANCE  = (value_type)1e-06f;
        static value_type const     ONE             = (value_type)1;
        static value_type const     TWO             = (value_type)2;
        static value_type const     PI              = (value_type)3.14159265358979323846;
        static value_type const     TWO_PI          = TWO * PI;
        static value_type const     HALF_PI         = PI / TWO;
        static value_type const     INV_PI          = ONE / PI;
        static value_type const     INV_TWO_PI      = ONE / TWO_PI;
        static value_type const     INV_HALF_PI     = ONE / HALF_PI;
        static uint8 const          BITS_NUM        = sizeof(value_type)*8;
        static uint8 const          USIGN_BITS_NUM  = TLimits::is_signed ? BITS_NUM - 1 : BITS_NUM;

        inline static value_type    Zero()          { return (value_type)0; }
        inline static value_type    One()           { return (value_type)1; }
        inline static value_type    Half()          { JUNGLE_STATIC_CHECK_MSG( (value_type)(ONE/2) != 0, NumberTraits_Half_value_can_not_be_specified_for_this_type ); return ONE/2; }
        inline static value_type    Pi()            { return (value_type)3.14159265358979323846f; }
        inline static value_type    Maximum()       { return TLimits::max(); }
        inline static value_type    Epsilon()       { return TLimits::epsilon() == 0 ? 1 : TLimits::epsilon(); }
        inline static value_type    Minimum()       { return TLimits::is_signed? (-TLimits::max()) : TLimits::min(); }
    }; // class NumberTraits

    template<>
    class NumberTraits< float >
    {
    public:
        typedef float                           value_type;
        typedef std::numeric_limits< float >    TLimits;

        static value_type const     ZERO;
        static value_type const     ZERO_TOLERANCE;
        static value_type const     ONE;
        static value_type const     TWO;
        static value_type const     PI;
        static value_type const     TWO_PI;
        static value_type const     HALF_PI;
        static value_type const     INV_PI;
        static value_type const     INV_TWO_PI;
        static value_type const     INV_HALF_PI;
        static uint8 const          BITS_NUM;
        static uint8 const          USIGN_BITS_NUM;

        inline static value_type    Zero()          { return NumberTraits< float >::ZERO; }
        inline static value_type    One()           { return NumberTraits< float >::ONE; }
        inline static value_type    Half()          { return 0.5f; }
        inline static value_type    Pi()            { return NumberTraits< float >::PI; }
        inline static value_type    Epsilon()       { return TLimits::epsilon(); }
        inline static value_type    Maximum()       { return TLimits::max() - 1; }      // On MSVC: 3.402823466e+38F
        inline static value_type    Minimum()       { return -TLimits::max(); }         // On MSVC: -3.402823466e+38F
    }; // class NumberTraits< float >

    template<>
    class NumberTraits< double >
    {
    public:
        typedef double                          value_type;
        typedef std::numeric_limits< double >   TLimits;

        static value_type const     ZERO;
        static value_type const     ZERO_TOLERANCE;
        static value_type const     ONE;
        static value_type const     TWO;
        static value_type const     PI;
        static value_type const     TWO_PI;
        static value_type const     HALF_PI;
        static value_type const     INV_PI;
        static value_type const     INV_TWO_PI;
        static value_type const     INV_HALF_PI;
        static uint8 const          BITS_NUM;
        static uint8 const          USIGN_BITS_NUM;

        inline static value_type    Zero()         { return NumberTraits< double >::ZERO; }
        inline static value_type    One()          { return NumberTraits< double >::ONE; }
        inline static value_type    Half()         { return 0.5; }
        inline static value_type    Pi()           { return NumberTraits< double >::PI; }
        inline static value_type    Epsilon()      { return TLimits::epsilon(); }
        inline static value_type    Maximum()      { return TLimits::max(); }       // On MSVC: 3.402823466e+38F
        inline static value_type    Minimum()      { return TLimits::is_signed? (-TLimits::max()) : TLimits::min(); }
    }; // class NumberTraits< double >

} // namespace Jungle

#endif // !defined __BASE_NUMBER_TRAITS_HPP__
// EOF

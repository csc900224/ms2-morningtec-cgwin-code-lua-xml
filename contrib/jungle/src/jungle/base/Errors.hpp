//
//  FILE NAME:
//      jungle/base/Errors.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
// Copyright (c) 2008, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __BASE_ERRORS_HPP__
#define __BASE_ERRORS_HPP__

#include <jungle/base/Platform.hpp>

#if defined JUNGLE_CLAW
#include <jungle/base/hal/claw/ErrorsClaw.hpp>
#elif defined JUNGLE_WIN32
#include <jungle/base/hal/win32/ErrorsWin32.hpp>
#elif defined JUNGLE_WII
#include <jungle/base/hal/wii/ErrorsWii.hpp>
#else
#error "Platform not supported"
#endif

#if defined _DEBUG
    #ifndef JUNGLE_ASSERT
    #define JUNGLE_ASSERT( exp )               JUNGLE_ASSERT_IMPL( (exp) )
    #endif
    #ifndef JUNGLE_ASSERT_MSG
    #define JUNGLE_ASSERT_MSG( exp, ...)       JUNGLE_ASSERT_MSG_IMPL( (exp), __VA_ARGS__ )
    #endif
    #ifndef JUNGLE_WARNING_MSG
    #define JUNGLE_WARNING_MSG( ... )          JUNGLE_WARNING_MSG_IMPL( __VA_ARGS__ )
    #endif
    #ifndef JUNGLE_WARNING_EXP_MSG
    #define JUNGLE_WARNING_EXP_MSG( exp, ... ) JUNGLE_WARNING_EXP_MSG_IMPL( (exp), __VA_ARGS__ )
    #endif
#else
    #ifndef JUNGLE_ASSERT
    #define JUNGLE_ASSERT( exp )               {}
    #endif
    #ifndef JUNGLE_ASSERT_MSG
    #define JUNGLE_ASSERT_MSG( exp, ... )      {}
    #endif
    #ifndef JUNGLE_WARNING_MSG
    #define JUNGLE_WARNING_MSG( ... )          {}
    #endif
    #ifndef JUNGLE_WARNING_EXP_MSG
    #define JUNGLE_WARNING_EXP_MSG( exp, ... ) {}
    #endif
#endif // !defined _DEBUG

    namespace Jungle
    {
        template<bool>  struct CompileTimeError;
        template<>      struct CompileTimeError<true> {};

        template<bool>  struct CompileTimeAssert
        {
            CompileTimeAssert(...) {};
        };
        template<>      struct CompileTimeAssert<false>;

    } // namespace Jungle

    #define JUNGLE_STATIC_CHECK_MSG(expr, msg)                          \
            {                                                           \
            char msg[ (expr) ? 1 : 0 ];                                 \
            msg[0] = '\0';                                              \
            }

    #define JUNGLE_STATIC_CHECK(expr)                                   \
            (Jungle::CompileTimeError<(expr) != 0>())

    #define JUNGLE_STATIC_ASSERT_MSG(expr, msg)                         \
            {                                                           \
            class ERROR_##msg {};                                       \
            Jungle::CompileTimeAssert<(expr) != 0> ( ERROR_##msg() );     \
            }

    #define JUNGLE_STATIC_ASSERT(x)                                     \
            JUNGLE_STATIC_ASSERT_MSG( (x), Static_Assertion_Failed )

#endif //! defined __BASE_ERRORS_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/base/NumberTraits.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2009, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/NumberTraits.hpp>

namespace Jungle
{

    float const     NumberTraits< float >::ZERO             = 0.0f;
    float const     NumberTraits< float >::ZERO_TOLERANCE   = 1e-06f; //0.0001f;
    float const     NumberTraits< float >::ONE              = 1.0f;
    float const     NumberTraits< float >::TWO              = 2.0f;
    float const     NumberTraits< float >::PI               = 3.14159265358979323846f;
    float const     NumberTraits< float >::TWO_PI           = 2 * NumberTraits< float >::PI;
    float const     NumberTraits< float >::HALF_PI          = NumberTraits< float >::PI / 2;
    float const     NumberTraits< float >::INV_PI           = 1.0f / NumberTraits< float >::PI;
    float const     NumberTraits< float >::INV_TWO_PI       = 1.0f / NumberTraits< float >::TWO_PI;
    float const     NumberTraits< float >::INV_HALF_PI      = 1.0f / NumberTraits< float >::HALF_PI;
    uint8 const     NumberTraits< float >::BITS_NUM         = sizeof( float ) * 8;
    uint8 const     NumberTraits< float >::USIGN_BITS_NUM   = NumberTraits< float >::BITS_NUM - 1;

    double const    NumberTraits< double >::ZERO            = 0.0;
    double const    NumberTraits< double >::ZERO_TOLERANCE  = 1e-06; //0.0000001f;
    double const    NumberTraits< double >::ONE             = 1.0;
    double const    NumberTraits< double >::TWO             = 2.0;
    double const    NumberTraits< double >::PI              = 3.14159265358979323846;
    double const    NumberTraits< double >::TWO_PI          = 2 * NumberTraits< double >::PI;
    double const    NumberTraits< double >::HALF_PI         = NumberTraits< double >::PI / 2;
    double const    NumberTraits< double >::INV_PI          = 1.0 / NumberTraits< double >::PI;
    double const    NumberTraits< double >::INV_TWO_PI      = 1.0 / NumberTraits< double >::TWO_PI;
    double const    NumberTraits< double >::INV_HALF_PI     = 1.0 / NumberTraits< double >::HALF_PI;
    uint8 const     NumberTraits< double >::BITS_NUM        = sizeof( double ) * 8;
    uint8 const     NumberTraits< double >::USIGN_BITS_NUM  = NumberTraits< double >::BITS_NUM - 1;

} // namespace Jungle
// EOF

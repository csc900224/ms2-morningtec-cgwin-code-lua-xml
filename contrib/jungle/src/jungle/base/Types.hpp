/* /////////////////////////////////////////////////////////////////////////////
*  FILE:
*       jungle/base/Types.hpp
*
*  AUTHOR(S):
*      Krystian Kostecki <krystian.kostecki@game-lion.com>
*
*  Copyright (c) 2009, Gamelion. All rights reserved.
* ////////////////////////////////////////////////////////////////////////// */

#ifndef __BASE_TYPES_HPP__
#define __BASE_TYPES_HPP__

#include <jungle/base/Platform.hpp>

//! Base namespace for application startup, exit and update handling.
namespace Jungle
{

}

#if defined JUNGLE_CLAW
#include <jungle/base/hal/claw/TypesClaw.hpp>
#elif defined JUNGLE_WIN32
#include <jungle/base/hal/win32/TypesWin32.hpp>
#elif defined JUNGLE_WII
#include <jungle/base/hal/wii/TypesWii.hpp>
#else
#error "Platform not supported"
#endif

#endif // !defined __BASE_TYPES_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/Rand.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/Errors.hpp>

namespace Jungle
{
    inline f64 Rand( uint32 seed )
    {
        RandSeed( seed );

        return Rand();
    }

    inline f32 Randf( uint32 seed )
    {
        RandSeed( seed );

        return Randf();
    }

    inline uint32 Randi( uint32 seed )
    {
        RandSeed( seed );

        return Randi();
    }

    inline f64 RandSymmetric()
    {
        f64 frac = Rand();
        return static_cast< f64 >( 2.0 * frac - 1.0 );
    }

    inline f32 RandSymmetricf()
    {
        f32 frac = Randf();
        return ( 2.0f * frac - 1.0f );
    }

    inline f64 RandSymmetric( uint32 seed )
    {
        RandSeed( seed );

        return RandSymmetric();
    }

    inline f32 RandSymmetricf( uint32 seed )
    {
        RandSeed( seed );

        return RandSymmetricf();
    }

    inline f64 RandSign()
    {
        return Rand() > 0.5 ? 1. : -1.;
    }

    inline f32 RandSignf()
    {
        return Rand() > 0.5 ? 1.f : -1.f;
    }

    inline f64 RandSign( uint32 seed )
    {
        RandSeed( seed );

        return RandSymmetric();
    }

    inline f32 RandSignf( uint32 seed )
    {
        RandSeed( seed );

        return RandSignf();
    }

    template < typename T >
    inline T RandRange( T min, T max )
    {
        JUNGLE_ASSERT( max >= min );
        return static_cast< T >( min + (f64)(Rand()) * ( max - min ) );
    }

    template < class T >
    inline T RandRange( T min, T max, uint32 seed )
    {
        JUNGLE_ASSERT( max >= min );

        RandSeed( seed );

        f64 frac = Rand();
        return min + ( max - min ) * static_cast< T >( frac );
    }

} // namespace Jungle
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/Time.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_TIME_HPP__
#define __BASE_TIME_HPP__

// Internal includes
#include <jungle/base/Types.hpp>
#include <jungle/base/Platform.hpp>

namespace Jungle
{
    inline uint32 GetTimeSec();

    inline uint32 GetTimeMiliSec();

    inline uint32 GetTimeMicroSec();

} // namespace Jungle

#if defined JUNGLE_CLAW
#include <jungle/base/hal/claw/TimeClaw.hpp>
#elif defined JUNGLE_WIN32
#include <jungle/base/hal/win32/TimeWin32.hpp>
#elif defined JUNGLE_WII
#include <jungle/base/hal/wii/TimeWii.hpp>
#else
#error "Platform not supported"
#endif

#endif // !defined __BASE_TIME_HPP__
// EOF

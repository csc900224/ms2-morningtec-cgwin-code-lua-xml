//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/hal/wii/RandWii.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_RAND_WII_INL__
#define __BASE_RAND_WII_INL__

// Internal includes
#include <jungle/base/Types.hpp>

namespace Jungle
{
    static uint32           s_seed;
    static const uint32     RAND_MAX = 65536;
    static const uint32     RAND_MASK = RAND_MAX - 1;

    inline void MixSeed()
    {
        s_seed = s_seed * 214013UL + 2531011UL;
    }

    uint32 RandInt(void)
    {
        MixSeed();
        return static_cast<int>((s_seed >> 16) & RAND_MASK);
    }

    inline f64 Rand()
    {
        return static_cast<f64>(RandInt()) / RAND_MAX;
    }

    inline f32 Randf()
    {
        return static_cast<f32>(RandInt()) / RAND_MAX;
    }

    inline uint32 Randi()
    {
        return RandInt();
    }

    inline uint32 RandMaxi()
    {
        return Jungle::RAND_MAX;
    }

    inline void RandSeed( uint32 seed /* = 0 */ )
    {
        s_seed = seed;
    }

} // namespace Jungle

#endif // !defined __BASE_RAND_WII_INL__
// EOF

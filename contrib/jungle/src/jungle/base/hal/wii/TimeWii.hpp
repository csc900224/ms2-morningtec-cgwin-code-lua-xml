//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/hal/wii/TimeWii.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_TIME_WII_HPP__
#define __BASE_TIME_WII_HPP__

// Internal includes
#include <jungle/base/Types.hpp>

// External includes
#include <revolution/os.h>

namespace Jungle
{
    inline uint32 GetTimeSec()
    {
        return static_cast<uint32>( OSTicksToSeconds( OSGetTime() ) );
    }

    inline uint32 GetTimeMiliSec()
    {
        return static_cast<uint32>( OSTicksToMilliseconds( OSGetTime() ) );
    }

    inline uint32 GetTimeMicroSec()
    {
        return static_cast<uint32> ( OSTicksToMicroseconds( OSGetTime() ) );
    }

} // namespace Jungle

#endif // !defined __BASE_TIME_WII_HPP__
// EOF

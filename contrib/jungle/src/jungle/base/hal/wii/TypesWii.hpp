/* /////////////////////////////////////////////////////////////////////////////
*  FILE:
*       jungle/base/hal/wii/TypesWii.hpp
*
*  AUTHOR(S):
*      Krystian Kostecki <krystian.kostecki@game-lion.com>
*
*  Copyright (c) 2009, Gamelion. All rights reserved.
* ////////////////////////////////////////////////////////////////////////// */

#ifndef __BASE_TYPES_WII_HPP__
#define __BASE_TYPES_WII_HPP__

// External includes
#include <revolution/types.h>

namespace Jungle
{

} // namespace Jungle

#endif // !defined __BASE_TYPES_WII_HPP__

// EOF

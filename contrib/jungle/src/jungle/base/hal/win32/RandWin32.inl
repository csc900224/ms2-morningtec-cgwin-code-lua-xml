//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/hal/win32/RandWin32.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_RAND_WIN32_INL__
#define __BASE_RAND_WIN32_INL__

// Internal includes
#include <jungle/base/Types.hpp>

// External includes
#include <stdlib.h>

namespace Jungle
{
    inline f64 Rand()
    {
        return ( (f64)rand() ) / ( (f64)(RAND_MAX) );
    }

    inline f32 Randf()
    {
        return ((f32)rand() / RAND_MAX );
    }

    inline uint32 Randi()
    {
        return static_cast< uint32 >( rand() );
    }

    inline uint32 RandMaxi()
    {
        return RAND_MAX;
    }

    inline void RandSeed( uint32 seed /* = 0 */ )
    {
        srand( seed );
    }

} // namespace Jungle

#endif // !defined __BASE_RAND_WIN32_INL__
// EOF

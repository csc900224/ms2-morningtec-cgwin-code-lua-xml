//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/hal/win32/TimeWin32.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_TIME_WIN32_HPP__
#define __BASE_TIME_WIN32_HPP__

// Internal includes
#include <jungle/base/Time.hpp>

// External includes
#include <time.h>

namespace Jungle
{
    // TODO: Insert proper timing functions - the best approach is to start counting from
    // application loop - less overflow risk.
    inline uint32 GetTimeSec()
    {
        static time_t timeBuf;
        time( ( time_t *)&timeBuf );

        return static_cast< uint32 >( timeBuf );
    }

    inline uint32 GetTimeMiliSec()
    {
        return static_cast< uint32 >( clock() );
    }

    inline uint32 GetTimeMicroSec()
    {
        return static_cast<uint32>( GetTimeMiliSec() * 1000 );
    }

} // namespace Jungle

#endif // !defined __BASE_TIME_WIN32_HPP__
// EOF

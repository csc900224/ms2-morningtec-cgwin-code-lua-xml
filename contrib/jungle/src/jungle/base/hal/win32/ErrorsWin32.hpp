//
//  FILE NAME:
//      jungle/base/hal/win32/ErrorsWin32.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  NOTES:
//
//
// Copyright (c) 2008, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __BASE_ERRORS_WIN32_HPP__
#define __BASE_ERRORS_WIN32_HPP__

// External includes
#include <assert.h>  // Assertions

// TODO: Add all assertions implementations and maybe move them to functions instead of macros.

#if defined _DEBUG
    #ifndef JUNGLE_ASSERT_IMPL
    #define JUNGLE_ASSERT_IMPL( exp )                  assert( (exp) )
    #endif
    #ifndef JUNGLE_ASSERT_MSG_IMPL
    #define JUNGLE_ASSERT_MSG_IMPL( exp, ...)          assert( (exp) )
    #endif
    #ifndef JUNGLE_WARNING_MSG_IMPL
    #define JUNGLE_WARNING_MSG_IMPL( ... )             {}
    #endif
    #ifndef JUNGLE_WARNING_EXP_MSG_IMPL
    #define JUNGLE_WARNING_EXP_MSG_IMPL( exp, ... )    {}
    #endif
#else
    #ifndef JUNGLE_ASSERT_IMPL
    #define JUNGLE_ASSERT_IMPL( exp )                  {}
    #endif
    #ifndef JUNGLE_ASSERT_MSG_IMPL
    #define JUNGLE_ASSERT_MSG_IMPL( exp, ... )         {}
    #endif
    #ifndef JUNGLE_WARNING_MSG_IMPL
    #define JUNGLE_WARNING_MSG_IMPL( ... )             {}
    #endif
    #ifndef JUNGLE_WARNING_EXP_MSG_IMPL
    #define JUNGLE_WARNING_EXP_MSG_IMPL( exp, ... )    {}
    #endif
#endif // !defined _DEBUG

#endif //! defined __BASE_ERRORS_WIN32_HPP__
// EOF

/* /////////////////////////////////////////////////////////////////////////////
*  FILE:
*       jungle/base/hal/win32/TypesWin32.hpp
*
*  AUTHOR(S):
*      Krystian Kostecki <krystian.kostecki@game-lion.com>
*
*  Copyright (c) 2009, Gamelion. All rights reserved.
* ////////////////////////////////////////////////////////////////////////// */

#ifndef __BASE_TYPES_WIN32_HPP__
#define __BASE_TYPES_WIN32_HPP__

namespace Jungle
{
    typedef unsigned long long  uint64;
    typedef long long           int64;

    typedef unsigned int        uint32;
    typedef int                 int32;

    typedef unsigned short      uint16;
    typedef short               int16;

    typedef unsigned char       uint8;
    typedef char                int8;

    typedef float               f32;
    typedef double              f64;

} // namespace Jungle

#endif // !defined __BASE_TYPES_WIN32_HPP__
// EOF

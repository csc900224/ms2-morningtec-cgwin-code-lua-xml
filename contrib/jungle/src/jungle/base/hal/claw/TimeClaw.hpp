//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/hal/claw/TimeClaw.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_TIME_CLAW_HPP__
#define __BASE_TIME_CLAW_HPP__

// Internal includes
#include <jungle/base/Time.hpp>

// External includes
#include <claw/application/Time.hpp>

namespace Jungle
{
    // NOTE: Notice that Claw::Time::GetTime() and GetTimeMs methods may start counting from different
    // startup point, that is way to maintain comparability we need to use one method in both places.
    inline uint32 GetTimeSec()
    {
        return static_cast< uint32 >( GetTimeMiliSec() / 1000 );
    }

    inline uint32 GetTimeMiliSec()
    {
        return static_cast< uint32 >( Claw::Time::GetTimeMs() );
    }

    inline uint32 GetTimeMicroSec()
    {
        return static_cast<uint32>( GetTimeMiliSec() * 1000 );
    }

} // namespace Jungle

#endif // !defined __BASE_TIME_CLAW_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/hal/claw/RandClaw.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_RAND_CLAW_INL__
#define __BASE_RAND_CLAW_INL__

// Internal includes
#include <jungle/base/Types.hpp>
#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>

// External includes
#include <claw/base/RNG.hpp>

namespace Jungle
{
    static Claw::RNG s_rng;

    inline f64 Rand()
    {
        return f64( s_rng.GetDouble() );
    }

    inline f32 Randf()
    {
        return f32( s_rng.GetDouble() );
    }

    inline uint32 Randi()
    {
        return s_rng.GetInt();
    }

    inline uint32 RandMaxi()
    {
        // Claw returns random integer up to maximum 32-bit unsigned int value
        return NumberTraits< uint32 >::Maximum();
    }

    inline void RandSeed( uint32 seed /* = 0 */ )
    {
        s_rng.SetSeed( seed );
    }

} // namespace Jungle

#endif // !defined __BASE_RAND_CLAW_INL__
// EOF

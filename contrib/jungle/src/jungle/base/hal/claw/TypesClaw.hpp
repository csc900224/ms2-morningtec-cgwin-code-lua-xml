/* /////////////////////////////////////////////////////////////////////////////
*  FILE:
*       jungle/base/hal/claw/TypesClaw.hpp
*
*  AUTHOR(S):
*      Krystian Kostecki <krystian.kostecki@game-lion.com>
*
*  Copyright (c) 2009, Gamelion. All rights reserved.
* ////////////////////////////////////////////////////////////////////////// */

#ifndef __BASE_TYPES_CLAW_HPP__
#define __BASE_TYPES_CLAW_HPP__

// External includes
#include <claw/compat/ClawTypes.hpp>

namespace Jungle
{
    typedef Claw::Int64         int64;
    typedef Claw::UInt64        uint64;

    typedef Claw::Int32         int32;
    typedef Claw::UInt32        uint32;

    typedef Claw::Int16         int16;
    typedef Claw::UInt16        uint16;

    typedef Claw::Int8          int8;
    typedef Claw::UInt8         uint8;

    typedef float               f32;
    typedef double              f64;

} // namespace Jungle

#endif // !defined __BASE_TYPES_CLAW_HPP__
// EOF

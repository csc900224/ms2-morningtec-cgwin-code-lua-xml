#ifndef __BASE_PLATFORM_HPP__
#define __BASE_PLATFORM_HPP__

#if defined CLAW_HW
#  define JUNGLE_CLAW
#  define JUNGLE_PLATFORM Claw
#elif defined WIN32
#  define JUNGLE_WIN32
#  define JUNGLE_PLATFORM Win32
#elif defined RVL
#  define JUNGLE_WII
#  define JUNGLE_PLATFORM Wii
#else
#  error "Unsupported platform"
#endif

#endif // !defined __BASE_PLATFORM_HPP__
// EOF

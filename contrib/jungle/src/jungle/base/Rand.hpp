//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/base/Rand.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@game-lion.com>
//                        <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2010, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __BASE_RAND_HPP__
#define __BASE_RAND_HPP__

// Internal includes
#include <jungle/base/Types.hpp>
#include <jungle/base/Platform.hpp>

//! Collection of utility methods and classes for easy accessing system information.
namespace Jungle
{
    //! Return semi-random double precision floating point number in 0 - 1 range.
    inline f64      Rand();

    //! Return semi-random floating point number in 0 - 1 range.
    inline f32      Randf();

    //! Return semi-random integer point number in 0 - RandMaxi() range.
    inline uint32   Randi();

    //! Acquire maximum possible random integer value.
    inline uint32   RandMaxi();

    //! Reset random seed.
    inline void     RandSeed( uint32 seed = 0 );

    //! Return double precision random number with an given seed.
    inline f64      Rand( uint32 seed );

    //! Return random number of single precision with an given seed.
    inline f32      Randf( uint32 seed );

    //! Return integer random number with an seed predefined.
    inline uint32   Randi( uint32 seed );

    //! Return random value of double precision between -1 and 1.
    inline f64      RandSymmetric();

    //! Return single precision semi-random value within range -1 and 1.
    inline f32      RandSymmetricf();

    //! Return random value of double precision between -1 and 1 with a given random seed.
    inline f64      RandSymmetric( uint32 seed );

    //! Return single precision semi-random value within range -1 and 1 with a given seed.
    inline f32      RandSymmetricf( uint32 seed );

    //! Returns semi-random sign multiplier: 1 or -1.
    inline f64      RandSign();

    //! Return single precision sign multiplier: 1.f or -1.f.
    inline f32      RandSignf();

    //! Double precision sign value: 1 or -1, with a given random seed.
    inline f64      RandSign( uint32 seed );

    //! Single precision semi-random sign value: 1 or -1, with a given seed.
    inline f32      RandSignf( uint32 seed );

    //! Return random number within given values range.
    template < typename T >
    inline T        RandRange( T min, T max );

    //! Return random number within given range and with random seed specified.
    template < typename T >
    inline T        RandRange( T min, T max, uint32 seed );

} // namespace Jungle

// Include inlined functions
#include <jungle/base/Rand.inl>

#if defined JUNGLE_CLAW
#include <jungle/base/hal/claw/RandClaw.inl>
#elif defined JUNGLE_WIN32
#include <jungle/base/hal/win32/RandWin32.inl>
#elif defined JUNGLE_WII
#include <jungle/base/hal/wii/RandWii.inl>
#else
#error "Platform not supported"
#endif

#endif // !defined __BASE_RAND_HPP__
// EOF

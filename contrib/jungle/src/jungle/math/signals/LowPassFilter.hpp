//////////////////////////////////////////////////////////////////////////
// FILE:
//      jungle/math/signals/LowPassFilter.hpp
//
//  AUTHOR(S):
//      Jacek Nijaki <jacek@nijaki.pl>
//
//  Copyright (c) 2008-2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__SIGNALS__LOW_PASS_FILTER_HPP__
#define __INCLUDED__MATH__SIGNALS__LOW_PASS_FILTER_HPP__

// Internal includes
#include <jungle/base/NumberTraits.hpp>

namespace Jungle
{
namespace Math
{
    //! Simple and generic implementation of low pass filter.
    /*!
    * Works both on scalars and vectors.
    * Input method: AddSample()
    * Output method: GetValue()
    */
    template< typename TValue, typename TFactor = float >
    class LowPassFilter
    {
    public:
        //! Constructor taking damping factor as parameter. Factor should be normalized (0-1)
                            LowPassFilter( const TFactor& factor );

        //! Constructor taking initial filter value and damping factor as parameters.
                            LowPassFilter( const TValue& value, const TFactor& factor );

        //! Put new sample to the filter.
        TValue              AddSample( const TValue& sample );

        //! Get filtered (output) value.
        inline TValue       GetValue() const;

        //! Set current damping factor.
        /*!
        * Value must be normalized to 0-1 range.
        * 0 - means permanent damping (no changes).
        * 1 - means no damping (filter does not work, just propagates vales from input).
        */
        inline void         SetFactor( const TFactor& factor );

        //! Get current damping factor.
        inline TFactor      GetFactor() const;

        //! Reset filter with given value.
        void                Reset( const TValue& value );

    private:
        bool                m_initialized;
        TValue              m_value;
        TFactor             m_factor;
    };

    template< typename TValue, typename TFactor >
    LowPassFilter<TValue,TFactor>::LowPassFilter( const TFactor& factor ) :
        m_initialized( false ),
        m_factor( factor ),
        m_value( TValue() )
    {}

    template< typename TValue, typename TFactor >
    LowPassFilter<TValue,TFactor>::LowPassFilter( const TValue& value, const TFactor& factor ) :
        m_initialized( true ),
        m_factor( factor ),
        m_value( value )
    {}

    template< typename TValue, typename TFactor >
    TValue LowPassFilter<TValue,TFactor>::AddSample( const TValue& value )
    {
        if( !m_initialized )
        {
            m_value = value;
            m_initialized = true;
        }
        else
        {
            m_value = value * m_factor + m_value * ( Claw::NumberTraits<TFactor>::ONE - m_factor );
        }
        return m_value;
    }

    template< typename TValue, typename TFactor >
    inline TValue LowPassFilter<TValue,TFactor>::GetValue() const
    {
        return m_value;
    }

    template< typename TValue, typename TFactor >
    inline void LowPassFilter<TValue,TFactor>::SetFactor( const TFactor& factor )
    {
        m_factor = factor;
    }

    template< typename TValue, typename TFactor >
    inline TFactor LowPassFilter<TValue,TFactor>::GetFactor() const
    {
        return m_factor;
    }

    template< typename TValue, typename TFactor >
    void LowPassFilter<TValue,TFactor>::Reset( const TValue& value )
    {
        m_value = value;
        if( !m_initialized )
            m_initialized = true;
    }

} // namespace Math

} // namespace Jungle

#endif // #define __INCLUDED__MATH__SIGNALS__LOW_PASS_FILTER_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/graphs/GraphsApi.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__GRAPHS__GRAPHS_API_HPP__
#define __INCLUDED__MATH__GRAPHS__GRAPHS_API_HPP__

// Internal includes
#include <jungle/math/Vector2.hpp>

namespace Jungle
{
namespace Math
{
    class GraphsApi
    {
    public:
        typedef Vector2f        Vector2;
        typedef float           Real;
        typedef unsigned int    Color;

        //! Renderer abstract class exposes interface for on platform rendering.
        /*!
        * Provide Renderer implementation and register it via Renderer::Register to allow
        * scene primitives debug drawing.
        */
        class Renderer
        {
        public:
            virtual         ~Renderer() {}

            virtual void    DrawSegment( const Vector2& v, const Vector2& extent, unsigned int color, void* context /*= 0*/ ) = 0;

            virtual void    DrawRect( const Vector2& min, const Vector2& extent, unsigned int color, void* context /*= 0*/ ) = 0;

            virtual void    DrawCircle( const Vector2& pos, Real rad, unsigned int color, void* context /*= 0*/ ) = 0;

        }; // class Renderer

        //! Register renderer instance.
        /*!
        * Takes ownership over passed object, so renderer should not be deallocated before Release() call.
        * \return previosly registered renderer or NULL if nothing yet registered.
        */
        static Renderer* RegisterRenderer( Renderer* renderer );

        //! Release previously registered debug Renderer from beeing used for rendering.
        /*!
        * \return previously registered renderer pointer (for manual deallocation) or NULL
        * if nothing was registered yet.
        */
        static Renderer* ReleaseRenderer();

        //! Create color type object based on the color channels.
        static Color    MakeRgb( unsigned char r, unsigned char g, unsigned char b );

        static void     DrawSegment( const Vector2& v, const Vector2& extent, unsigned int color, void* user /*= 0*/ );

        static void     DrawRect( const Vector2& min, const Vector2& extent, unsigned int color, void* user /*= 0*/ );

        static void     DrawCircle( const Vector2& pos, Real rad, unsigned int color, void* user /*= 0*/ );

    private:
        static
        Renderer*       s_renderer;

    }; // class GraphsApi

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH__GRAPHS__GRAPHS_API_HPP__

// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      slot_machine/graph/Histogram.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__GRAPHS__HISTOGRAM_HPP__
#define __INCLUDED__MATH__GRAPHS__HISTOGRAM_HPP__

// Internal includes
#include <jungle/math/graphs/Graph.hpp>
#include <jungle/math/Vector2.hpp>

// External includes
#include <vector>

namespace Jungle
{
namespace Math
{
    template < class TVar >
    class Histogram : public Graph
    {
    public:
        typedef TVar                SampleType;
        typedef unsigned long long  Freq;
        typedef unsigned long       Count;

        //! Define histogram ranges and the number of intervals the whole range will be divided.
                                Histogram( SampleType rangeMin, SampleType rangeMax, Count intervalsNum );

        //! Graph interface method implementation.
        virtual void            Render( void* user = NULL, unsigned int color = 0x00FF00 ) const;

        //! Set size of the single distribution interval beeing rendered as single rectangle.
        /*!
        * \note seting up new interval requires clearing all gathered data, so samples need to be added again.
        * \note this function will change the intervals (bins) number in order to fit them in the probed
        * distribution range.
        */
        void                    SetIntervalSize( SampleType intervalSize );

        //! Setup number of bins that divides the whole considered variable distribution range.
        /*
        * Note that it changes the bin interval size to adopt to the new number of bins.
        * \note this method clears already gathered data, so samples need to be added again.
        * \see SetRange() method.
        */
        void                    SetIntervalsNum( Count num );

        //! Setup distribution range to be considered.
        /*!
        * Samples outside the ranges will be simply ignored with warrning.
        * \note this method clears already gathered data, so samples need to be added again.
        */
        void                    SetRange( SampleType min, SampleType max );

        //! Clear all gathered data, leaving histogram settings unchanged.
        void                    Clear();

        //! Add new obervation.
        void                    AddSample( SampleType observation );

        //! Get number (total) of samples gathered.
        inline Freq             GetSamplesNum() const;

        //! Enable storing samples so if you change intervals setting the stored data will not be lost.
        /*!
        * If you change samples caching note that historgram data will need to be reset.
        */
        inline void             SetCachingSamples( bool enable );

        //! Check if samples caching is enabled.
        inline bool             IsCachingSamples() const;

        //! Setup size of the arrows showing diretions of the X and Y axes.
        inline void             SetArrowSize( const Vector2f& scale );

        //! Get currently set X and Y axis arrows size.
        inline const Vector2f&  GetArrowSize() const;

        //! Change drawing scheme, render real frequencies (enable = false) or normalized/relative values from 0 to 1.
        inline void             SetNormalized( bool enable );

        //! Check if histogram will plot relative frequencies/
        /*
        * Normalized histogram shows the proportion of cases that fall into each of several categories,
        * with the total area equaling 1.
        */
        inline bool             IsNormalized() const;

    private:
        void                    UpdateFrequency( SampleType observation );

        void                    ResetIntervals();

        typedef std::vector< Freq >                 Frequencies;
        typedef Frequencies::iterator               FrequenciesIt;
        typedef Frequencies::const_iterator         FrequenciesConstIt;

        typedef std::vector< SampleType >           Samples;
        typedef typename Samples::iterator          SamplesIt;
        typedef typename Samples::const_iterator    SamplesConstIt;

        SampleType              m_rangeMin;
        SampleType              m_rangeMax;

        SampleType              m_intervalSize;
        Count                   m_intervalsNum;

        Frequencies             m_frequencies;
        Samples                 m_samples;
        Freq                    m_samplesNum;

        bool                    m_cacheSamples;
        bool                    m_normalize;

        Vector2f                m_arrowSize;

    }; // class Histogram

    template < class TVar >
    inline typename Histogram< TVar >::Freq Histogram< TVar >::GetSamplesNum() const
    {
        return m_samplesNum;
    }

    template < class TVar >
    inline void Histogram< TVar >::SetCachingSamples( bool enable )
    {
        Clear();

        m_cacheSamples = enable;
    }

    template < class TVar >
    inline bool Histogram< TVar >::IsCachingSamples() const
    {
        return m_cacheSamples;
    }

    template < class TVar >
    inline void Histogram< TVar >::SetArrowSize( const Vector2f& size )
    {
        m_arrowSize = size;
    }

    template < class TVar >
    inline const Vector2f& Histogram< TVar >::GetArrowSize() const
    {
        return m_arrowSize;
    }

    template < class TVar >
    inline void Histogram< TVar >::SetNormalized( bool enable )
    {
        m_normalize = enable;
    }

    template < class TVar >
    inline bool Histogram< TVar >::IsNormalized() const
    {
        return m_normalize;
    }

} // namespace Math

} // namespace Jungle

#include <jungle/math/graphs/Histogram.inl>

#endif // !defined __INCLUDED__MATH__GRAPHS__HISTOGRAM_HPP__
// EOF

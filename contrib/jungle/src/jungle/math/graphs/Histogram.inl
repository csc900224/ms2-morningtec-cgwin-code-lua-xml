//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      slot_machine/graph/Histogram.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/math/Arithmetic.hpp>   // Ceil, Floor
#include <jungle/math/graphs/GraphsApi.hpp>

// External includes

namespace Jungle
{
namespace Math
{
    template < class TVar >
    Histogram< TVar >::Histogram( SampleType rangeMin, SampleType rangeMax, Count intervalsNum )
        : m_rangeMin( rangeMin )
        , m_rangeMax( rangeMax )
        , m_intervalsNum( intervalsNum )
        , m_samplesNum( 0 )
        , m_cacheSamples( false )
        , m_normalize( true )
        , m_arrowSize( 3.f, 3.f )
    {
        m_intervalSize = (SampleType)Ceil( (double)( m_rangeMax - m_rangeMin ) / m_intervalsNum );

        m_frequencies.resize( m_intervalsNum, Count( 0 ) );
    }

    template < class TVar >
    void Histogram< TVar >::Render( void* user /*= NULL*/, unsigned int color /*= 0x00FF00*/ ) const
    {
        FrequenciesConstIt it = m_frequencies.begin();
        FrequenciesConstIt end = m_frequencies.end();

        float maxFreq = 0.f;
        //float freqScalling;
        for( ; it != end; ++it )
        {
            maxFreq = Max( (float)(*it), maxFreq );
        }
        it = m_frequencies.begin();
        end = m_frequencies.end();

        Vector2f scale;
        if( m_sizing == GS_ABSOLUTE )
        {
            scale.m_x = m_size.m_x / (m_rangeMax - m_rangeMin);
            scale.m_y = m_normalize ? m_size.m_y / m_samplesNum : m_size.m_y / maxFreq;
        }
        else
        {
            scale.m_x = m_scale.m_x;
            scale.m_y = m_normalize ? m_scale.m_y / m_samplesNum : m_scale.m_y;
        }

        Vector2f intervalStart( m_origin.m_x + m_rangeMin * scale.m_x, m_origin.m_y );
        Vector2f intervalExtent( m_intervalSize * scale.m_x, 0.f );

        for( ; it != end; ++it )
        {
            intervalExtent.m_y = (*it) * scale.m_y;
            GraphsApi::DrawRect( intervalStart, intervalExtent, color, user );
            intervalStart.m_x += intervalExtent.m_x;
        }
        // Draw histogram X axis
        intervalStart.Set( m_origin.m_x, m_origin.m_y );
        intervalExtent.Set( (m_rangeMax - m_rangeMin) * scale.m_x + m_arrowSize.m_x * 2.f, 0.f );
        GraphsApi::DrawSegment( intervalStart, intervalExtent, color, user );
        // Draw X arrow
        intervalStart += intervalExtent;
        GraphsApi::DrawSegment( intervalStart, Vector2f( -m_arrowSize.m_x, m_arrowSize.m_y ), color, user );
        GraphsApi::DrawSegment( intervalStart, Vector2f( -m_arrowSize.m_x, -m_arrowSize.m_y ), color, user );

        // Draw histogram Y axis
        intervalStart.Set( m_origin.m_x, m_origin.m_y );
        intervalExtent.Set( 0.f, maxFreq * scale.m_y + m_arrowSize.m_y * 2.f );
        GraphsApi::DrawSegment( intervalStart, intervalExtent, color, user );
        // Draw Y arrow
        intervalStart -= intervalExtent;
        GraphsApi::DrawSegment( intervalStart, Vector2f( -m_arrowSize.m_x, -m_arrowSize.m_y ), color, user );
        GraphsApi::DrawSegment( intervalStart, Vector2f( m_arrowSize.m_x, -m_arrowSize.m_y ), color, user );

    }

    template < class TVar >
    void Histogram< TVar >::SetIntervalSize( SampleType intervalSize )
    {
        m_intervalSize = intervalSize;
        // Make sure we will cover whole range
        m_intervalsNum = (Count)Ceil( (double)( m_rangeMax - m_rangeMin ) / m_intervalSize );

        ResetIntervals();
    }

    template < class TVar >
    void Histogram< TVar >::SetIntervalsNum( Count num )
    {
        m_intervalsNum = num;
        // Make sure whole range will be covered with bins
        m_intervalSize = (SampleType)Ceil( (double)( m_rangeMax - m_rangeMin ) / m_intervalsNum );

        ResetIntervals();
    }

    template < class TVar >
    void Histogram< TVar >::SetRange( SampleType min, SampleType max )
    {
        m_rangeMin = min;
        m_rangeMax = max;

        m_intervalSize = (SampleType)Ceil( (double)( m_rangeMax - m_rangeMin ) / m_intervalsNum );

        ResetIntervals();
    }

    template < class TVar >
    void Histogram< TVar >::Clear()
    {
        m_frequencies.clear();

        m_samples.clear();
        m_samplesNum = 0;
    }

    template < class TVar >
    void Histogram< TVar >::AddSample( SampleType observation )
    {
        if( observation < m_rangeMin )
            return;
        if( observation >= m_rangeMax )
            return;

        ++m_samplesNum;
        if( IsCachingSamples() )
        {
            m_samples.push_back( observation );
        }

        UpdateFrequency( observation );
    }

    template < class TVar >
    void Histogram< TVar >::UpdateFrequency( SampleType observation )
    {
        observation -= m_rangeMin;
        Count idx = (Count)Floor( (double)(observation) / m_intervalSize );
        ++m_frequencies[idx];
    }

    template < class TVar >
    void Histogram< TVar >::ResetIntervals()
    {
        m_frequencies.clear();
        m_frequencies.resize( m_intervalsNum, Count( 0 ) );

        if( IsCachingSamples() )
        {
            SamplesConstIt it = m_samples.begin();
            SamplesConstIt end = m_samples.end();
            for( ; it != end; ++it )
            {
                if( *it < m_rangeMin )
                    continue;
                if( *it > m_rangeMax )
                    continue;

                UpdateFrequency( *it );
            }
        }
    }
} // namespace Math

} // namespace Jungle

// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/graph/GraphsApi.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/math/graphs/GraphsApi.hpp>

namespace Jungle
{
namespace Math
{
    GraphsApi::Renderer* GraphsApi::s_renderer = 0;

    /* static */
    void GraphsApi::DrawSegment( const Vector2& v, const Vector2& extent, unsigned int color, void* user /*= 0*/ )
    {
        if( s_renderer )
            s_renderer->DrawSegment( v, extent, color, user );
    }

    /* static */
    void GraphsApi::DrawRect( const Vector2& min, const Vector2& extent, unsigned int color, void* user /*= 0*/ )
    {
        if( s_renderer )
            s_renderer->DrawRect( min, extent, color, user );
    }

    /* static */
    void GraphsApi::DrawCircle( const Vector2& pos, Real radius, unsigned int color, void* user /*= 0*/ )
    {
        if( s_renderer )
            s_renderer->DrawCircle( pos, radius, color, user );
    }

    /* static */
    GraphsApi::Renderer* GraphsApi::RegisterRenderer( Renderer* instance )
    {
        Renderer* ret = s_renderer;
        s_renderer = instance;
        return ret;
    }

    /* static */
    GraphsApi::Renderer* GraphsApi::ReleaseRenderer()
    {
        Renderer* ret = s_renderer;
        s_renderer = NULL;
        return ret;
    }

    /* static */
    GraphsApi::Color GraphsApi::MakeRgb( unsigned char r, unsigned char g, unsigned char b )
    {
        Color rgb = (Color(r) << 16) | (Color(g) << 8) | (Color(b));
        return rgb;
    }

} // namespace Scene

} // namespace Jungle

// EOF

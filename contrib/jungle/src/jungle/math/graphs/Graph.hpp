//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/graphs/Graph.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__GRAPHS__GRAPH_HPP__
#define __INCLUDED__MATH__GRAPHS__GRAPH_HPP__

// Internal includes
#include <jungle/math/Vector2.hpp>

namespace Jungle
{
namespace Math
{
    //! Basic interface for various graphs rendering classes.
    class Graph
    {
    public:
        enum GraphSizing
        {
            GS_ABSOLUTE,    //!< Indicates that absolute dimensions will be used to represent graph space (\see SetSize()).
            GS_RELATIVE,    //!< Graph values will be scaled by scalling factor before rendering (\see SetScale()).

            GS_NUM
        }; // enum GraphSizing

        //! Setup graph origin to (0, 0) and scalling to (1, 1)
        inline                  Graph();

        //! Virtual destructor to allow polymorphic usage.
        virtual                 ~Graph()    {}

        //! Define simple rendering inteface which allows passing any case specific data pointer.
        /*!
        * Pure virtual base rendering function receiving user controller data pointer which may
        * hold rendering context, rendering target, etc., depending from the platfrom and framework used.
        * This approach allows for mutli-platform and multi-context implementation, internally graph
        * should call GraphsApi rendering functions (such as DrawLine, DrawRect) which will provide
        * drawing functionality via currently registered renderer (\see GraphsApi::SetRenderer() method).
        */
        virtual void            Render( void* user = NULL, unsigned int color = 0x00FF00 ) const = 0;

        //! Setup graph coordinates system origin position.
        inline void             SetOrigin( const Vector2f& pos );

        //! Get currently used origin.
        inline const Vector2f&  GetOrigin() const;

        //! Setup graph scalling factor, used to easier visualise smaller values.
        inline void             SetScale( const Vector2f& scale );

        //! Get current graph scalling (may be non-uniform).
        inline const Vector2f&  GetScale() const;

        //! Setup graph absolute size, when used scalling is ignored.
        inline void             SetSize( const Vector2f& scale );

        //! Get currently set graph size.
        inline const Vector2f&  GetSize() const;

    protected:
        Vector2f                m_origin;
        Vector2f                m_scale;
        Vector2f                m_size;
        GraphSizing             m_sizing;

    }; // class Graph

    Graph::Graph()
        : m_origin( 0.f, 0.f )
        , m_scale( 1.f, 1.f )
        , m_sizing( GS_RELATIVE )   // should use scale for drawing
    {
    }

    inline void Graph::SetOrigin( const Vector2f& pos )
    {
        m_origin = pos;
    }

    inline const Vector2f& Graph::GetOrigin() const
    {
        return m_origin;
    }

    inline void Graph::SetScale( const Vector2f& scale )
    {
        m_scale = scale;
        m_sizing = GS_RELATIVE;
    }

    inline const Vector2f& Graph::GetScale() const
    {
        return m_scale;
    }

    inline void Graph::SetSize( const Vector2f& size )
    {
        m_size = size;
        m_sizing = GS_ABSOLUTE;
    }

    inline const Vector2f& Graph::GetSize() const
    {
        return m_size;
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH__GRAPHS__GRAPH_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/HarmonicSines.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_HARMONIC_SINES_HPP__
#define __INCLUDED__MATH_HARMONIC_SINES_HPP__

// Internal includes
#include <jungle/math/Harmonic.hpp>
#include <jungle/math/Trigonometry.hpp>
#include <jungle/base/NumberTraits.hpp>
#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Math
{
    //! Utility class for harmonics evaluation based on sines lookup table.
    /*!
    * Class allows for easy signal evaluation in real space. Output signal values
    * differs from -amplitude to +max amplitude. You may change harmonics phase offset ( SetPhaseOffset() )
    * within time and get current signal value depending on harmonics frequency (or period) and current phase ( GetValue() ).
    */
    template < class TValue, class TPhase >
    class HarmonicSines : public Harmonic< TValue, TPhase >
    {
    public:
        typedef TPhase      PhaseType;
        typedef TValue      ValueType;

        virtual void        SetFrequency( float freq );
        virtual float       GetFrequency() const            { return 1.0f / m_period; }

        virtual void        SetPeriod( PhaseType period );
        virtual PhaseType   GetPeriod() const               { return m_period; }

        virtual ValueType   GetValue() const;

    private:
        PhaseType           m_period;
        PhaseType           m_angVel;

    }; // class HarmonicSines

    // Common types definition
    typedef HarmonicSines< float, float >    HarmonicSinesf;
    typedef HarmonicSines< double, double >  HarmonicSinesd;

    template < class TValue, class TPhase >
    void HarmonicSines< TValue, TPhase >::SetFrequency( float freq )
    {
        m_period = PhaseType( 1.0f / freq );
        JUNGLE_ASSERT( m_period );

        m_angVel = 2 * NumberTraits< TPhase >::PI * freq;
        JUNGLE_ASSERT( m_angVel );
    }

    template < class TValue, class TPhase >
    void HarmonicSines< TValue, TPhase >::SetPeriod( TPhase period )
    {
        m_period = period;
        JUNGLE_ASSERT( m_period );
        m_angVel = 2 * NumberTraits< TPhase >::PI / period;
        JUNGLE_ASSERT( m_angVel );
    }

    template < class TValue, class TPhase >
    typename HarmonicSines< TValue, TPhase >::ValueType HarmonicSines< TValue, TPhase >::GetValue() const
    {
        if( Harmonic< TValue, TPhase >::GetAmplitude() == NumberTraits< TValue >::ZERO )
            return NumberTraits< TValue >::ZERO;

        // f(t) = A * sin( angVel * phase )
        return Harmonic< TValue, TPhase >::GetAmplitude() * Math::SinRad( m_angVel * Harmonic< TValue, TPhase >::m_phase );
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH_HARMONIC_SINES_HPP__
// EOF

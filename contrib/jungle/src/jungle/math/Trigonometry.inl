//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Trigonometry.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/math/Arithmetic.hpp>

// External includes
#include <math.h>                   // sin, cos, acos, atan, atan2

#define USE_STD_ATAN                // prefer math.h implementation for atan
#define USE_STD_ATAN2               // prefer atan2 for ATan2 computation

namespace Jungle
{
namespace Math
{
    template < class TReal >
    inline TReal Deg2Rad( TReal deg )
    {
        return ( deg * NumberTraits<TReal>::PI ) / TReal(180);
    }

    template < class TReal >
    inline TReal Rad2Deg( TReal rad )
    {
        return ( rad * TReal(180) ) / NumberTraits<TReal>::PI;
    }

    template < class TReal >
    inline TReal Sin( TReal rad )
    {
        return SinRad( rad );
    }

    template < class TReal >
    inline TReal SinRad( TReal rad )
    {
        return static_cast< TReal > ( sin( static_cast< double > ( rad ) ) );
    }

    template < class TReal >
    inline TReal SinDeg( TReal deg )
    {
        return SinRad( Deg2Rad( deg ) );
    }

    template < typename TReal >
    inline TReal FastSin0( TReal rad )
    {
        rad -= ( (int)( rad / NumberTraits<TReal>::TWO_PI ) ) * NumberTraits<TReal>::TWO_PI;

        if( rad > NumberTraits<TReal>::HALF_PI )
            return FastSin0( NumberTraits<TReal>::PI - rad );
        else if( rad < -NumberTraits<TReal>::HALF_PI )
            return FastSin0( -NumberTraits<TReal>::PI - rad );

        TReal radSqr = rad * rad;
        TReal ret = static_cast< TReal >( -2.39e-08f );
        ret = ( ret * radSqr ); 
        ret += static_cast< TReal >( 2.7526e-06f );
        ret = ( ret * radSqr );
        ret -= static_cast< TReal >( 1.98409e-04f );
        ret = ( ret * radSqr );
        ret += static_cast< TReal >( 8.3333315e-03f );
        ret = ( ret * radSqr );
        ret -= static_cast< TReal >( 1.666666664e-01f );
        ret = ( ret * radSqr );
        ret += static_cast< TReal >( 1.0f );
        ret = ( ret * rad );
        return ret;
    }

    template < class TReal >
    inline TReal FastSin1( TReal rad )
    {
        rad -= ( (int)( rad / NumberTraits<TReal>::TWO_PI ) ) * NumberTraits<TReal>::TWO_PI;

        if( rad > NumberTraits<TReal>::HALF_PI )
            return FastSin1( NumberTraits<TReal>::PI - rad );
        else if( rad < -NumberTraits<TReal>::HALF_PI )
            return FastSin1( -NumberTraits<TReal>::PI - rad );

        TReal radSqr = rad*rad;
        TReal ret = static_cast< TReal >( 0.61e-03f );
        ret *= radSqr;
        ret -= static_cast< TReal >( 1.6605e-01f );
        ret *= radSqr;
        ret += static_cast< TReal >( 1.0f );
        ret *= rad;
        return ret;
    }

    template < class TReal >
    inline TReal Cos( TReal rad )
    {
        return CosRad( rad );
    }

    template < class TReal >
    inline TReal CosRad( TReal rad )
    {
        // NOTE: This will increase accuracy
        // rad = Mod( rad, NumberTraits< TReal >::TWO_PI );
        return static_cast< TReal > ( cos( static_cast< double > ( rad ) ) );
    }

    template < class TReal >
    inline TReal CosDeg( TReal deg )
    {
        return CosRad( Deg2Rad( deg ) );
    }

    template < class TReal >
    inline TReal FastCos0( TReal rad )
    {
        // rad %= ( 2 * PI )
        rad -= ( (int)( rad / NumberTraits<TReal>::TWO_PI ) ) * NumberTraits<TReal>::TWO_PI;

        if( rad > NumberTraits<TReal>::HALF_PI )
            return FastSin0( NumberTraits<TReal>::HALF_PI - rad );
        else if( rad < -NumberTraits<TReal>::HALF_PI )
            return -FastSin0( -NumberTraits<TReal>::HALF_PI + rad );

        register TReal radSqr = rad*rad;
        register TReal ret = -2.605e-07f;
        ret *= radSqr;
        ret += 2.47609e-05f;
        ret *= radSqr;
        ret -= 1.3888397e-03f;
        ret *= radSqr;
        ret += 4.16666418e-02f;
        ret *= radSqr;
        ret -= 4.999999963e-01f;
        ret *= radSqr;
        ret += 1.0f;
        return ret;
    }

    template < class TReal >
    inline TReal FastCos1( TReal rad )
    {
        // rad %= (2 * PI)
        rad -= ( (int)( rad / NumberTraits<TReal>::TWO_PI ) ) * NumberTraits<TReal>::TWO_PI;

        if( rad > NumberTraits<TReal>::HALF_PI )
            return FastSin1( NumberTraits<TReal>::HALF_PI - rad );
        else if( rad < -NumberTraits<TReal>::HALF_PI )
            return -FastSin1( -NumberTraits<TReal>::HALF_PI + rad );

        TReal radSqr = rad*rad;
        TReal ret = 3.705e-02f;
        ret *= radSqr;
        ret -= 4.967e-01f;
        ret *= radSqr;
        ret += 1.0f;
        return ret;
    }

    template < class TReal >
    inline void SinCosRad( TReal* sin, TReal* cos, TReal rad )
    {
        // TODO: Will be optimized in the future
        *sin = SinRad( rad );
        *cos = CosRad( rad );
    }

    template < class TReal >
    inline void SinCosDeg( TReal* sin, TReal* cos, TReal deg )
    {
        SinCosRad( sin, cos, Deg2Rad( deg ) );
    }

    template < class TReal >
    inline TReal ACos( TReal val )
    {
        if( -(TReal)1.0 < val )
        {
            if( val < (TReal)1.0 )
            {
                return (TReal)acos( static_cast< double >( val ) );
            }
            else
            {
                return (TReal)0.0;
            }
        }
        else
        {
            return NumberTraits< TReal >::PI;
        }
    }

    template < class TReal >
    inline TReal FastACos( TReal val )
    {
        // Function very unaccurate near the domain margins <-1, 1> clamp results to real values
        if( val <= -NumberTraits<TReal>::ONE )
        {
            return NumberTraits< TReal >::PI;
        }
        else if( val >= NumberTraits<TReal>::ONE )
        {
            return NumberTraits< TReal >::ZERO;
        }

        static const TReal AS1 = static_cast< TReal >(0.0187293f);
        static const TReal AS2 = static_cast< TReal >(0.0742610f);
        static const TReal AS3 = static_cast< TReal >(0.2121144f);
        static const TReal AS4 = static_cast< TReal >(1.5707288f);

        TReal fRoot = Sqrt< TReal >( NumberTraits< TReal >::ONE - val );
        TReal result = AS1;
        result = (result * val);
        result += AS2;
        result = (result * val);
        result -= AS3;
        result = (result * val);
        result += AS4;
        result = (fRoot * result);

        // Return result in radians
        return result;
    }

    template < class TReal >
    inline TReal ATan( TReal f )
    {
#ifdef USE_STD_ATAN
        return static_cast<TReal>( atan( (double)f ) );
#else
        static const TReal AT1 = static_cast< TReal >(0.0208282470703125);
        static const TReal AT2 = static_cast< TReal >(0.0851287841796875);
        static const TReal AT3 = static_cast< TReal >(0.1801300048828125);
        static const TReal AT4 = static_cast< TReal >(0.330291748046875);
        static const TReal AT5 = static_cast< TReal >(0.9998626708984375);

        if( f > NumberTraits< TReal >::ONE )
            return NumberTraits< TReal >::HALF_PI - ATan( NumberTraits < TReal >::ONE / f);
        else if( f < -NumberTraits< TReal >::ONE )
            return -NumberTraits< TReal >::HALF_PI - ATan( NumberTraits< TReal >::ONE / f);

        TReal sqr = ( f * f );
        TReal result = AT1;
        result = ( result * sqr );
        result -= AT2;
        result = ( result * sqr );
        result += AT3;
        result = ( result * sqr );
        result -= AT4;
        result = ( result * sqr );
        result += AT5;
        result = result * f;
        // Return result in radians
        return result;
#endif
    }

    template < class TReal >
    inline TReal ATan2( TReal sin, TReal cos )
    {
#ifdef USE_STD_ATAN2
        return static_cast<TReal>( atan2( (double)sin, (double)cos ) );
#else
        if(cos == 0)
            return NumberTraits< TReal >::HALF_PI * Sign( sin );
        if(sin == 0)
        {
            if(cos >= 0)
                return 0;
            else
                return NumberTraits< TReal >::PI;
        }
        // We want to spread domain range from ATan <-PI/2, PI/2> to
        // (-PI, PI> using additional information from sin and cos values
        // This gives us hint about quarters:
        // - I and II quarter -> <-PI/2, PI/2> are ignored since they are 
        // accurately calculated using ATan function
        // - III and IV quarter are recognized by sin and cos sign using following rules:

        // Trigonometric functions signs:
        // - I quarter      <0,    PI/2>  => sin > 0 ^ cos > 0
        // - II quarter     <PI/2,   PI>  => sin > 0 ^ cos < 0
        // - IV' quarter    <-PI/2,   0>  => sin < 0 ^ cos > 0
        // - III' quarter   <-PI, -PI/2>  => sin < 0 ^ cos < 0
        // ' - we shift phase 2PI into left to have x < 0 in domain using 
        // III and IV quarter properties. We need to solve II and III' situation
        // which all have cos < 0 and differ only by sinus value

        // If we are in II or III quarter move in phase using equation
        // atan2(sin, cos) =  (PI - |angle|)*sign(sin)
        if(cos < 0)
            return ( NumberTraits< TReal >::PI - Abs( ATan( sin / cos ) ) ) * Sign( sin );
        else
            return ATan( sin / cos );
#endif
    }

    template < class TReal >
    inline TReal FastATan2( TReal y, TReal x )
    {
        // Compute a self-normalizing ratio depending on the quadrant that the complex number resides in.
        // For a complex number z, let x=Re(z) and y=Im(z).
        TReal piOver4 = NumberTraits< TReal >::PI / 4;
        TReal yAbs = Abs( y ) + NumberTraits< TReal >::ZERO_TOLERANCE; //add epsilon to prevent 1e-10 division by 0, actually 0/0 condition
        TReal r, angle;
        if( x >=0 )
        {
            // For complex number in quadrant I, the ratio is:
            //      x-y
            // r =  ---
            //      x+y
            r = ( x - yAbs ) / ( x + yAbs );
#ifndef FAST_ATAN2_PRECISE
            // Angle (phase) will approximate 1st-order polynomial:
            // angle = pi/4 - pi/4 * r
            angle = piOver4 - ( piOver4 * r );
#else
            // To increase precision use higher degree polynomial:
            // angle = 0.1963 * r^3 - 0.9817 * r + pi/4
            angle = TReal(0.1963) * r * r * r - TReal(0.9817) * r + piOver4;
#endif
        }
        else
        {
            // If the complex number resides in quadrant II, compute the ratio using:
            //      x+y
            // r =  ---
            //      y-x
            r = ( x + yAbs ) / ( yAbs - x );
#ifndef FAST_ATAN2_PRECISE
            // Phase angle in II quadrant will use:
            // angle = 3 * pi/4 - pi/4 * r
            angle = 3 * piOver4 - ( piOver4 * r );
#else
            // Better precision in II quadrant can be achieved using polynomial:
            // angle = 0.1963 * r^3 - 0.9817 * r + 3*pi/4
            angle = TReal(0.1963) * r * r * r - TReal(0.9817) * r + 3 * piOver4;
#endif
        }
        // If complex number was really in quad IV instead of I or in III quad instead of II,
        // just negate the angle.
        if( y < 0 )
            return -angle;
        else
            return angle;
    }

} // namespace Math

} // namespace Jungle
// EOF

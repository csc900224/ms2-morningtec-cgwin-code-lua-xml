//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Harmonic.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_HARMONIC_HPP__
#define __INCLUDED__MATH_HARMONIC_HPP__

namespace Jungle
{
namespace Math
{
    //! Base harmonics class which can be used to describe different types of cyclic functions (i.e. sines, cosines).
    template < class TValue, class TPhase >
    class Harmonic
    {
    public:
        typedef             TValue  ValueType;
        typedef             TPhase  PhaseType;

        void                SetPhase( const TPhase& phase )     { m_phase = phase; }
        const TPhase&       GetPhase() const                    { return m_phase; }

        void                SetAmplitude( const TValue& amp )   { m_amplitude = amp; }
        const TValue&       GetAmplitude() const                { return m_amplitude; }

        virtual void        SetFrequency( float freq )          = 0;
        virtual float       GetFrequency() const                = 0;

        virtual void        SetPeriod( TPhase period )          = 0;
        virtual TPhase      GetPeriod() const                   = 0;

        virtual TValue      GetValue() const                    = 0;

    protected:
        TPhase              m_phase;
        TValue              m_amplitude;

    }; // class Harmonic

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH_HARMONIC_HPP__
// EOF

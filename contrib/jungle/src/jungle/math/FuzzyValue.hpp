//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/FuzzyValue.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_FUZZY_VALUE_HPP__
#define __INCLUDED__MATH_FUZZY_VALUE_HPP__

// Internal includes
#include <jungle/base/Rand.hpp>
#include <jungle/base/Time.hpp>

namespace Jungle
{
namespace Math
{
    template < class T >
    class FuzzyValue
    {
    public:
        typedef T               value_type;
        typedef FuzzyValue< T > self_type;

        //! Contruction always require setting some "fuzziness" ranges.
                            FuzzyValue( T min, T max );

        //! Get randomized/fuzzy value of type T.
        inline T            GetValue() const;

        //! Set "fuzziness" range.
        inline void         SetRange( T min, T max );

        //! Get minimal value that may be acquired during randomization.
        inline const T&     GetMin() const;

        //! Get maximum value range.
        inline const T&     GetMax() const;

        //! Operator that allows to use FuzzyValue as an normal T type value.
        inline              operator T() const;

        //! Expand or squeeze fuzzy value range leaving its median untouched.
        inline self_type&   operator^=( const T& scaling );

        //! Operator for multiplying whole fuzzy value range.
        inline self_type&   operator*=( const T& mult );

        //! Fuzzy value ranges divider.
        inline self_type&   operator/=( const T& div );

        //! Addition operator - moves whole fuzzy range at about certain offset.
        inline self_type&   operator+=( const T& add );

        //! Substraction operator, offset fuzzy range to the left about certain value.
        inline self_type&   operator-=( const T& sub );

    private:
        T                   m_min;
        T                   m_max;

    }; // class FuzzyValue

    // Common types definitions
    typedef FuzzyValue< double >        FuzzyValued;
    typedef FuzzyValue< float >         FuzzyValuef;
    typedef FuzzyValue< int >           FuzzyValuei;
    typedef FuzzyValue< unsigned int >  FuzzyValueui;
    typedef FuzzyValue< char >          FuzzyValuec;
    typedef FuzzyValue< unsigned char > FuzzyValueuc;

    template < class T >
    FuzzyValue< T >::FuzzyValue( T min, T max )
        : m_min ( min )
        , m_max ( max )
    {
        // It is enough that each FuzzyValue instance will change the seed
        RandSeed( GetTimeMiliSec() );
    }

    template < class T >
    inline T FuzzyValue< T >::GetValue() const
    {
        return RandRange( m_min, m_max );
    }

    template < class T >
    inline void FuzzyValue< T >::SetRange( T min, T max )
    {
        m_min = min;
        m_max = max;
    }

    template < class T >
    inline const T& FuzzyValue< T >::GetMin() const
    {
        return m_min;
    }

    template < class T >
    inline const T& FuzzyValue< T >::GetMax() const
    {
        return m_max;
    }

    template < class T >
    inline FuzzyValue< T >::operator T() const
    {
        return GetValue();
    }

    template < class T >
    inline FuzzyValue< T >& FuzzyValue< T >::operator ^=( const T& scaling )
    {
        const T center = ( m_max + m_min ) / 2;
        const T range = ( m_max - m_min ) / 2;
        range *= scaling;
        m_min = center - range;
        m_max = center + range;
        return *this;
    }

    template < class T >
    inline FuzzyValue< T >& FuzzyValue< T >::operator *=( const T& mult )
    {
        m_min *= mult;
        m_max *= mult;
        return *this;
    }

    template < class T >
    inline FuzzyValue< T >& FuzzyValue< T >::operator /=( const T& div )
    {
        m_min /= div;
        m_max /= div;
        return *this;
    }

    template < class T >
    inline FuzzyValue< T >& FuzzyValue< T >::operator +=( const T& add )
    {
        m_min += add;
        m_max += add;
        return *this;
    }

    template < class T >
    inline FuzzyValue< T >& FuzzyValue< T >::operator -=( const T& sub )
    {
        m_min -= sub;
        m_max -= sub;
        return *this;
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH_FUZZY_VALUE_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Vector3.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__VECTOR3_HPP__
#define __INCLUDED__MATH__VECTOR3_HPP__

// Internal includes
#include <jungle/base/NumberTraits.hpp>

namespace Jungle
{
namespace Math
{
    //! Three dimensional vector template.
    template < class T >
    class Vector3
    {
    public:
        //! Array access operator index type.
        typedef unsigned int    index_type;
        //! Self type definition.
        typedef Vector3<T>      self_type;
        //! Inner type definition.
        typedef T               value_type;

        //! Default constructor sets elements to zero.
                        Vector3();
        //! Initialize all vector coordinates with the same value.
        explicit        Vector3( T v );
        //! Initialization constructor.
                        Vector3( T x, T y, T z );
        //! Array based construction.
                        Vector3( const T* coords );
        //! Copying constructor.
                        Vector3( const Vector3& vec );

        //! Set coordinates values.
        inline void     Set( T x, T y, T z );
        //! Zero coordinates values.
        inline void     Zero();

        // Below functions confuse compiler with such call v[0], it doesn't know if should call:
        // pointer access operator and native array access on it => v.operator const T*()[0]
        // or should he call array access operator directly v.operator[](0)

        //! Read only coordinates access via array pointer.
        //inline          operator const T* () const;
        //! Coordinates write access via array pointer.
        //inline          operator T* ();

        //! Array access operator - read only.
        inline T        operator [] ( index_type i ) const;
        //! Array access operator - write and read.
        inline T&       operator [] ( index_type i );

        //! Equality operator.
        inline bool     operator == ( const Vector3& v ) const;
        //! Inequality operator.
        inline bool     operator != ( const Vector3& v ) const;
        //! Comparison operator - greater.
        inline bool     operator >  ( const Vector3& v ) const;
        //! Comparison operator - greater or equal.
        inline bool     operator >= ( const Vector3& v ) const;
        //! Comparison operator - smaller.
        inline bool     operator <  ( const Vector3& v ) const;
        //! Comparison operator - smaller or equal.
        inline bool     operator <= ( const Vector3& v ) const;

        //! Add a vector to this vector.
        inline Vector3& operator += (const Vector3& v);
        //! Subtract a vector from this vector.
        inline Vector3& operator -= (const Vector3& v);
        //! Arithmetic update - multiply vector.
        inline Vector3& operator *= (const Vector3& v);
        //! Arithmetic update - divide vector.
        inline Vector3& operator /= (const Vector3& v);

        //! Divide coordinates by given scalar
        inline Vector3& operator /= (T a);
        //! Add a scalar to this vector coordinates.
        inline Vector3& operator += (T a);
        //! Multiply this vector by a scalar.
        inline Vector3& operator *= (T a);
        //! Multiply this vector by a scalar.
        inline Vector3  operator * (T a) const;
        //! Divide this vector by a scalar.
        inline Vector3  operator / (T a) const;

        //! Vector negation.
        inline Vector3  operator - () const;

        //! Calculate dot product - scalar vector multiplication.
        inline T        Dot( const Vector3& v ) const;

        //! Calculate cross product with other vector - vector multiplication.
        /*!
        * The cross product v0 � v1 is defined as a vector that is perpendicular to both v0 and v1,
        * with direction given by the right-hand rule and a magnitude equal to the area of the parallelogram
        * that the vectors span. This formula may be also expressed as:
        * v0 x v1 = |v0| * |v1| * sinA * N  ^ N - vector perpendicular to v0 and v1.
        */
        inline Vector3  Cross( const Vector3& v ) const;

        //! Get length.
        inline T        Length() const;
        //! Get length squared.
        inline T        LengthSqr() const;
        //! Normalize current vector and returns its unnormalized length.
        inline T        Normalize();
        //! Returns normalized vector.
        inline Vector3  Normalized() const;

        //! Standard vectors definition.
        static const Vector3    ZERO;
        static const Vector3    ONE;
        static const Vector3    ONE_X;
        static const Vector3    ONE_Y;
        static const Vector3    ONE_Z;

    public:
        //! Vector coordinates
        union
        {
            // Member conventions
            struct
            {
                T       m_x;
                T       m_y;
                T       m_z;
            };
            T   m_coords[3];

            // Shorter easy access conventions
            struct
            {
                T       x;
                T       y;
                T       z;
            };
            T           v[3];
        };

    }; // class Vector3

    // Common types definitions
    typedef Vector3< int >      Vector3i;
    typedef Vector3< float >    Vector3f;
    typedef Vector3< double >   Vector3d;

    // Common vectors definitions
    template < class TReal >
    const Vector3< TReal > Vector3< TReal >::ZERO( 0, 0, 0 );

    template < class TReal >
    const Vector3< TReal > Vector3< TReal >::ONE( 1, 1, 1 );

    template < class TReal >
    const Vector3< TReal > Vector3< TReal >::ONE_X( 1, 0, 0 );

    template < class TReal >
    const Vector3< TReal > Vector3< TReal >::ONE_Y( 0, 1, 0 );

    template < class TReal >
    const Vector3< TReal > Vector3< TReal >::ONE_Z( 0, 0, 1 );

    //! Vector addition operator.
    template < class T >
    const inline Vector3< T > operator + ( const Vector3< T >& v1, const Vector3< T >& v2 );

    //! Two vectors subtraction - subtracts v2 from v1 (v1 - v2).
    template < class T >
    const inline Vector3< T > operator - ( const Vector3< T >& v1, const Vector3< T >& v2 );

    //! Two vectors coordinate multiplication - multiply vectors coordinates.
    template < class T >
    const inline Vector3< T > operator * ( const Vector3< T >& v1, const Vector3< T >& v2 );

    //! Vectors multiplication - vectors cross product.
    template < class T >
    const inline Vector3< T > operator ^ ( const Vector3< T >& v1, const Vector3< T >& v2 );

    //! Two vectors division - divide vectors coordinates.
    template < class T >
    const inline Vector3< T > operator / ( const Vector3< T >& v1, const Vector3< T >& v2 );

    //! Vector multiply by scalar operator.
    template< class T >
    const inline Vector3< T > operator * ( const T& scalar, const Vector3<T>& v );

    //! Specialization for arithmetic Min function (\see jungle/math/Arithmetic.hpp)
    template < class T >
    inline Vector3<T> Min( const Vector3<T>& a, const Vector3<T>& b );

    //! Specialization for arithmetic Min function (\see jungle/math/Arithmetic.hpp)
    template < class T >
    inline Vector3<T> Max( const Vector3<T>& a, const Vector3<T>& b );


    // Implementation
    template< class T >
    Vector3< T >::Vector3() :
        m_x( NumberTraits< T >::ZERO ),
        m_y( NumberTraits< T >::ZERO ),
        m_z( NumberTraits< T >::ZERO )
    {}

    template< class T >
    Vector3< T >::Vector3( T v ) :
        m_x( v ),
        m_y( v ),
        m_z( v )
    {}

    template< class T >
    Vector3< T >::Vector3( T x, T y, T z ) :
        m_x( x ),
        m_y( y ),
        m_z( z )
    {}

    template< class T >
    Vector3< T >::Vector3( const T* coords ) :
        m_x( coords[0] ),
        m_y( coords[1] ),
        m_z( coords[2] )
    {}

    template< class T >
    Vector3< T >::Vector3( const Vector3& vec ) :
        m_x( vec.m_x ),
        m_y( vec.m_y ),
        m_z( vec.m_z )
    {}

} // namespace Math

} // namespace Jungle

#include <jungle/math/Vector3.inl>

#endif // !defined __INCLUDED__MATH__VECTOR3_HPP__
// EOF

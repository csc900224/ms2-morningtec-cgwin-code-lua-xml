//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/approx/PolynomialHarmonic1.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>
#include <jungle/math/Trigonometry.hpp>

namespace Jungle
{
namespace Math
{
namespace Approx
{
    template< class TReal >
    PolynomialHarmonic1< TReal >::PolynomialHarmonic1( unsigned int numHarmonics /* = 1 */ ) :
        m_numHarmonics( numHarmonics )
    {
        JUNGLE_ASSERT( numHarmonics );

        m_coeff[COEF_SIN] = ( new Coefficient[numHarmonics] );
        m_coeff[COEF_COS] = ( new Coefficient[numHarmonics] );

        while( numHarmonics > 0 )
        {
            --numHarmonics;
            m_coeff[COEF_SIN][numHarmonics] = TReal(0.0f);
            m_coeff[COEF_COS][numHarmonics] = TReal(0.0f);
        }
    }

    template< class TReal >
    PolynomialHarmonic1< TReal >::PolynomialHarmonic1( unsigned int numHarmonics, const TReal* coeff[2] ) :
        m_numHarmonics( numHarmonics )
    {
        JUNGLE_ASSERT( numHarmonics );

        m_coeff[COEF_SIN] = ( new Coefficient[numHarmonics] );
        m_coeff[COEF_COS] = ( new Coefficient[numHarmonics] );

        while( numHarmonics > 0)
        {
            --numHarmonics;
            m_coeff[COEF_SIN][numHarmonics] = coeff[COEF_SIN][numHarmonics];
            m_coeff[COEF_COS][numHarmonics] = coeff[COEF_COS][numHarmonics];
        }
    }

    template< class TReal >
    PolynomialHarmonic1< TReal >::PolynomialHarmonic1( const PolynomialHarmonic1& poly ) :
        m_numHarmonics( poly.GetNumHarmonics() ),
    {
        ASSERT( m_numHarmonics );

        m_coeff[COEF_SIN] = ( new Coefficient[numHarmonics] );
        m_coeff[COEF_COS] = ( new Coefficient[numHarmonics] );

        unsigned int harmonicIdx = m_numHarmonics;
        while( harmonicIdx > 0)
        {
            --harmonicIdx;
            m_coeff[COEF_SIN][harmonicIdx] = poly.m_coeff[COEF_SIN][harmonicIdx];
            m_coeff[COEF_COS][harmonicIdx] = poly.m_coeff[COEF_COS][harmonicIdx];
        }
    }

    template< class TReal >
    PolynomialHarmonic1< TReal >::~PolynomialHarmonic1()
    {
        delete [] m_coeff[COEF_SIN];
        delete [] m_coeff[COEF_COS];
    }

    template < class TReal >
    template < class TSerie >
    void PolynomialHarmonic1< TReal >::Estimate( const TSerie& samples, unsigned int count )
    {
        TReal alpha = NumberTraits< TReal >::TWO_PI / count;
        TReal invCountTwo = NumberTraits< TReal >::TWO / count;
        for( unsigned int h = 0; h < m_numHarmonics; ++h )
        {
            TReal sumSin = 0;
            TReal sumCos = 0;
            TReal term;
            for( unsigned int s = 0; s < count; ++s)
            {
                term = h * alpha * s;
                sumSin += Math::Sin( term ) * samples[s];
                sumCos += Math::Cos( term ) * samples[s];
            }
            m_coeff[COEF_SIN][h] = sumSin * invCountTwo;
            m_coeff[COEF_COS][h] = sumCos * invCountTwo;
        }
    }

    template < class TReal >
    template < class TSerie >
    void PolynomialHarmonic1< TReal >::Evaluate( TSerie& outSamples, unsigned int count ) const
    {
        TReal alpha = NumberTraits< TReal >::TWO_PI / count;

        for( unsigned int i = 0; i < count; ++i )
        {
            TReal harmonicsSum = 0;
            TReal sinC, cosC, term;
            for (int j = 1; j < m_numHarmonics; ++j)
            {
                sinC = m_coeff[COEF_SIN][j];
                cosC = m_coeff[COEF_COS][j];
                term = j * alpha * i;
                harmonicsSum += sinC * Math::Sin(term) + cosC * Math::Cos(term);
            }
            outSamples[ i ] = harmonicsSum + m_coeff[COEF_COS][0] * NumberTraits< TReal >::Half();
        }
    }

    template< class TReal >
    inline TReal PolynomialHarmonic1< TReal >::GetSinCoeff( unsigned int index ) const
    {
        ASSERT( m_numHarmonics > index );
        return m_coeff[COEF_SIN][index];
    }

    template< class TReal >
    inline TReal PolynomialHarmonic1< TReal >::GetCosCoeff( unsigned int index ) const
    {
        ASSERT( m_numHarmonics > index );
        return m_coeff[COEF_COS][index];
    }

    template< class TReal >
    inline unsigned int PolynomialHarmonic1< TReal >::GetNumHarmonics() const
    {
        return m_numHarmonics;
    }
/*
    template< class TReal >
    inline unsigned int PolynomialHarmonic1< TReal >::GetDegree() const
    {
        return m_degree;
    }

    template< class TReal >
    inline void PolynomialHarmonic1< TReal >::SetDegree( unsigned int degree )
    {
        if( degree != GetDegree() )
        {
            TReal* newCoeff = new TReal[degree+1];
            unsigned int i;
            for( i = 0; i <= degree && i <= GetDegree(); ++i )
            {
                newCoeff[i] = m_coeff[i];
            }
            if( degree > GetDegree() )
            {
                //for( i = )
            }
        }
    }
*/

} // namespace Approx

} // namespace Math

} // namespace Jungle
// EOF

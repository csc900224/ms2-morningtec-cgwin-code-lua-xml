//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/approx/Polynomial1.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>

namespace Jungle
{
namespace Math
{

    template< class TReal >
    Polynomial1< TReal >::Polynomial1( unsigned int degree /* = 0 */ )
        : m_degree( degree )
    {
        JUNGLE_ASSERT( degree >= 0 );

        // The polynomial of the 0 (zero) degree it is just constant value f(x) = C - thus requires single coefficient.
        m_coeff = new Coefficient[degree + 1];

        for( unsigned int i = 0; i <= degree; ++i )
        {
            m_coeff[i] = NumberTraits< TReal >::ZERO;
        }
    }

    template< class TReal >
    Polynomial1< TReal >::Polynomial1( unsigned int degree, const TReal* coeff )
        : m_degree( degree )
    {
        JUNGLE_ASSERT( degree >= 0 );

        // Zero degree polynomial also requires single coefficient - constant value: f(x) = C
        m_coeff = new Coefficient[degree + 1];

        for( unsigned int i = 0; i <= degree; ++i )
        {
            m_coeff[i] = coeff[i];
        }
    }

    template< class TReal >
    Polynomial1< TReal >::Polynomial1( const Polynomial1& poly )
        : m_degree( poly.m_degree )
    {
        m_coeff = new Coefficient[m_degree + 1];

        unsigned int degree = m_degree;
        for( unsigned int i = 0; i <= degree; ++i )
        {
            m_coeff[i] = poly.m_coeff[i];
        }
    }

    template< class TReal >
    Polynomial1< TReal >::~Polynomial1()
    {
        delete [] m_coeff;
    }

    template < class TReal >
    template < class TSerie >
    void Polynomial1< TReal >::Estimate( const TSerie& samples, unsigned int count )
    {
        // TODO: Implement me
        JUNGLE_ASSERT( !"To be implemented" );
    }

    template < class TReal >
    void Polynomial1< TReal >::Compress( const TReal& epsilon )
    {
        JUNGLE_ASSERT_MSG( false, "This function is to be checked" );

        // TODO: To be checked
        // Estimate new poly degree starting from the highest degree
        int i;
        for( i = m_degree; i >= 0; --i )
        {
            if( Math::Abs( m_coeff[i] ) <= epsilon )
            {
                --m_degree;
            }
            else
            {
                break;
            }
        }

        if( m_degree >= 0 )
        {
            TReal invLeading = NumberTraits< TReal >::ONE / m_coeff[m_degree];
            m_coeff[m_degree] = NumberTraits< TReal >::ONE;
            for( i = 0; i < (int)m_degree; ++i )
            {
                m_coeff[i] *= invLeading;
            }
        }
    }

    template < class TReal >
    TReal Polynomial1< TReal >::Evaluate( const TReal& var ) const
    {
        // This uses iterative formula that simplifies polynomial calculation:
        // r(0) = c(n)
        // r(i+1) = r(i) * var + c(n-i)
        int deg = m_degree;
        TReal result = m_coeff[deg];
        for(--deg; deg >= 0; --deg)
        {
            result *= var;
            result += m_coeff[deg];
        }
        return result;
    }

    template< class TReal >
    Polynomial1< TReal > Polynomial1< TReal >::GetDerivative() const
    {
        if( m_degree > 0 )
        {
            Polynomial1 polyD( m_degree - 1 );
            for( unsigned int di = 0, i = 1; di < m_degree; ++di, ++i )
            {
                // f'( c(i)*x^i ) = c(i) * i
                polyD.m_coeff[di] = m_coeff[i] * i;
            }
            return polyD;
        }
        else
        {
            // Simply return zero degree polynomial
            return Polynomial1( 0 );
        }
    }

    template< class TReal >
    inline void Polynomial1< TReal >::SetCoeff( unsigned int degree, const TReal& coeff )
    {
        JUNGLE_ASSERT( degree <= m_degree );
        return m_coeff[degree] = coeff;
    }

    template< class TReal >
    inline TReal Polynomial1< TReal >::GetCoeff( unsigned int degree ) const
    {
        JUNGLE_ASSERT( degree <= m_degree );
        return m_coeff[degree];
    }

    template< class TReal >
    inline unsigned int Polynomial1< TReal >::GetCoeffNum() const
    {
        return m_degree + 1;
    }

    template< class TReal >
    inline unsigned int Polynomial1< TReal >::GetDegree() const
    {
        return m_degree;
    }

    template< class TReal >
    inline void Polynomial1< TReal >::SetDegree( unsigned int degree )
    {
        if( degree != GetDegree() )
        {
            unsigned int i;
            Coefficient* newCoeff = new Coefficient[degree+1];
            for( i = 0; i <= degree && i <= GetDegree(); ++i )
            {
                newCoeff[i] = m_coeff[i];
            }
            if( degree > GetDegree() )
            {
                for( ; i <= degree; ++i )
                {
                    newCoeff[i] = NumberTraits< TReal >::ZERO;
                }
            }
            delete [] m_coeff;
            m_coeff = newCoeff;
        }
    }

    template< class TReal >
    Polynomial1< TReal >& Polynomial1< TReal >::operator=( const Polynomial1< TReal >& poly )
    {
        if( m_degree != poly.m_degree )
        {
            m_degree = poly.m_degree;

            delete m_coeff;
            m_coeff = new Coefficient[m_degree];
        }

        for( unsigned int i = 0; i <= m_degree; ++i )
        {
            m_coeff[i] = poly.m_coeff[i];
        }
        return *this;
    }

    template< class TReal >
    inline const TReal& Polynomial1< TReal >::operator[]( unsigned int degree ) const
    {
        JUNGLE_ASSERT( degree <= m_degree );
        return m_coeff[degree];
    }

    template< class TReal >
    inline TReal& Polynomial1< TReal >::operator[]( unsigned int degree )
    {
        JUNGLE_ASSERT( degree <= m_degree );
        return m_coeff[degree];
    }

    template< class TReal >
    TReal Polynomial1< TReal >::operator()( const TReal& var ) const
    {
        return Evaluate( var );
    }

    template< class TReal >
    Polynomial1< TReal >& Polynomial1< TReal >::operator*=( const TReal& scalar )
    {
        for (unsigned int i = 0; i <= m_degree; ++i)
        {
            m_coeff[i] *= scalar;
        }
        return this;
    }

    template< class TReal >
    Polynomial1< TReal >& Polynomial1< TReal >::operator/=( const TReal& scalar )
    {
        JUNGLE_ASSERT_MSG( scalar != 0, "Can not divide by zero." );
        TReal invScalar = NumberTraits< TReal >::ONE / scalar;
        for (unsigned int i = 0; i <= m_degree; ++i)
        {
            m_coeff[i] *= invScalar;
        }
        return this;
    }

} // namespace Math

} // namespace Jungle
// EOF

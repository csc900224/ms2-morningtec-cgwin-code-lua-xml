//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/approx/Polynomial1.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__APPROX__POLYNOMIAL1_HPP__
#define __INCLUDED__MATH__APPROX__POLYNOMIAL1_HPP__

namespace Jungle
{
namespace Math
{

    template < class TReal >
    class Polynomial1
    {
    public:
        //! Create polynomial of specified degree, all coefficient and const value (degree + 1) will be set to zero.
                                Polynomial1( unsigned int degree = 0 );

        //! Create polynomial based on coefficients table (need to have size equal degree + 1).
                                Polynomial1( unsigned int degree, const TReal* coeff );

        //! Copying constructor.
                                Polynomial1( const Polynomial1& poly );

        //! Destructor frees coefficients' table.
                                ~Polynomial1();

        //! Estimate harmonics based on observations serie
        template < class TSerie >
        void                    Estimate( const TSerie& inSamples, unsigned int count );

        //! Eliminates insignifficant polynomial coefficients - smaller then epsilon.
        void                    Compress( const TReal& epsilon );

        //! Polynomial evaluation - calculates polynomial value based on input data.
        TReal                   Evaluate( const TReal& var ) const;

        //! Calculate and return derivative polynomial.
        Polynomial1             GetDerivative() const;

        //! Change polynomial coefficient assigned to specified degree level.
        /*!
        * It will modify parameter c(i=d) in formula:
        * F(x) = c(0) + c(1)x + c(2)x^2 + ... + c(degree)x^degree
        */
        inline void             SetCoeff( unsigned int degree, const TReal& coeff );

        //! Return polynomial coefficient for specified degree.
        inline TReal            GetCoeff( unsigned int degree ) const;

        //! Return number of coefficients - always equal to degree + 1, where one coefficient is used as constant value.
        inline unsigned int     GetCoeffNum() const;

        //! Return polynomial degree.
        /*!
        * Degree equal to 0 (zero) means that polynomial represents single constant value.
        */
        inline unsigned int     GetDegree() const;

        //! Set new polynomial degree.
        /*!
        * Function will copy previous polynomial coefficients up to the previous degree level.
        * All coefficients with degree higher then old one will be set to 0. This way when increasing
        * polynomial degree we will preserve its mathematic equivalence (will provide same results) at least
        * before we change some coefficients. Decreasing polynomial degree will always change the results
        * unless higher old coefficient were insigniffiant (very small) or equal to zero.
        */
        void                    SetDegree( unsigned int degree );

        //! Copying/assignment operator
        Polynomial1&            operator=( const Polynomial1& poly );

        //! Coefficient access operator
        inline const TReal&     operator[]( unsigned int degree ) const;

        //! Write access to polynomial coefficient
        inline TReal&           operator[]( unsigned int degree );

        //! Polynomial evaluation
        TReal                   operator()( const TReal& var ) const;

        //! Scalar coefficient multiplication operator with update
        Polynomial1&            operator*=( const TReal& scalar );

        //! Scalar divide operator with update
        Polynomial1&            operator/=( const TReal& scalar );

    protected:
        typedef TReal           Coefficient;
        typedef TReal*          Coefficients;

        unsigned int            m_degree;
        Coefficients            m_coeff;

    }; // class Polynomial1

    // Common types definitions
    typedef Polynomial1< float >    Polynomial1f;
    typedef Polynomial1< double >   Polynomial1d;

} // namespace Math

} // namespace Jungle

#include <jungle/math/approx/Polynomial1.inl>

#endif // !defined __INCLUDED__MATH__APPROX__POLYNOMIAL1_HPP__
// EOF

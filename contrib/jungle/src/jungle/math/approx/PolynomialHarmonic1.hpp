//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/approx/PolynomialHarmonic1.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__APPROX__POLYNOMIAL_HARMONIC1_HPP__
#define __INCLUDED__MATH__APPROX__POLYNOMIAL_HARMONIC1_HPP__

namespace Jungle
{
namespace Math
{
    //! Harmonic polynomial class for approximating cyclic processes and variable changes.
    /*!
    * The class approximates cyclic processes by using finite sum of sines and
    * cosines harmonic functions.
    */
    template < class TReal >
    class PolynomialHarmonic1
    {
    public:
        //! Create polynomial with specified number of sin and cos harmonics, all coefficient will be set to zero.
                                PolynomialHarmonic1( unsigned int numHarmonics = 2 );

        //! Create polynomial based on coefficients table (need to have size equal numPeriods + 1).
                                PolynomialHarmonic1( unsigned int numHarmonics, const TReal* coeff[2] );

        //! Copying constructor.
                                PolynomialHarmonic1( const PolynomialHarmonic1& poly );

        //! Destructor frees coefficient table.
                                ~PolynomialHarmonic1();

        //! Estimate harmonics based on observations serie
        template < class TSerie >
        void                    Estimate( const TSerie& inSamples, unsigned int count );

        //! Polynomial evaluation - calculates samples based on polynomial harmonics.
        template < class TSerie >
        void                    Evaluate( TSerie& outSamples, unsigned int count ) const;

        //! Return sinusoid harmonic coefficient (for harmonics with index < 0, numHarmonics-1 >
        inline TReal            GetSinCoeff( unsigned int index ) const;

        //! Return cosinesoid coefficient (for harmonics with index < 0, numHarmonics-1 >
        inline TReal            GetCosCoeff( unsigned int index ) const;

        //! Return the number of sines-cosines harmonic pairs.
        inline unsigned int     GetNumHarmonics() const;

    protected:
        enum
        {
            COEF_SIN = 0,
            COEF_COS = 1,
            COEF_NUM
        };

        typedef TReal           Coefficient;
        typedef TReal*          Coefficients;

        unsigned int            m_numHarmonics;
        Coefficients            m_coeff[COEF_NUM];

    }; // class PolynomialHarmonic1

} // namespace Math

} // namespace Jungle

#include <jungle/math/approx/PolynomialHarmonic1.inl>

#endif // !defined __INCLUDED__MATH__APPROX__POLYNOMIAL_HARMONIC1_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Factorial.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_FACTORIAL_HPP__
#define __INCLUDED__MATH_FACTORIAL_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    //! Template for various factorial numerical limits definitions.
    template < class T >
    class FactorialTraits
    {
    public:
        typedef T                   value_type;

        static value_type const     MAX_N;
        static value_type const     MAX_VALUE;

    }; // class FactorialTraits

    //! Factorial function, ussually expressed as the n!.
    /*!
    * This implements simple recursion:
    * n! = 1,               if n = 0,
    * n! = (n - 1)! * n,    if n > 0.
    * Note this is implementation only for "integer"/discreet values. It will even not compile if
    * defined for floating point types: Factorial<float>, the compile time check will be performed when
    * FactorialTraits<float> symbol will be (tried to be) linked.
    * Altrough the factorial function can also be defined for non-integer values, this requires more
    * advanced tools from mathematical analysis. One function that "fills in" the values of the factorial
    * is called the Gamma function (Wikipedia).
    */
    template < class T >
    class Factorial
    {
    public:
        typedef T              value_type;
        typedef Factorial< T > self_type;

        //! Get value of factorial for given n.
        /*!
        * \note this function will assert if supplied with bigger n! then
        * the resulting value type can hold. In such case try to use Factorial template
        * with a bigger base type etc. Factorial64i.
        */
        static inline T     GetValue( T n );

        //! Get maximum input (n) value for this Factorial instance.
        static inline T     GetMaxN();

        //! Get maximum value that this Factorial instance may return.
        /*!
        * This value would be acquired as a result of Factorial::GetValue( Factorial::GetMaxN() ) call.
        */
        static inline T     GetMaxValue();

    }; // class Factorial

    // Common types definitions
    typedef Factorial< unsigned long long > Factorial64i;
    typedef Factorial< unsigned int >       Factorial32i;
    typedef Factorial< unsigned short >     Factorial16i;
    typedef Factorial< unsigned char >      Factorial8i;

    template < class T >
    inline T Factorial< T >::GetValue( T n )
    {
        JUNGLE_ASSERT_MSG( n >= 0, "You can not define factorial for numbers smaller then zero." );
        JUNGLE_ASSERT_MSG( n <= FactorialTraits< T >::MAX_N, "Resulting factorial will be greater then capability of the type used." );
        T ret = 1;
        for( ; n > 1; --n )
            ret *= n;
        return ret;
    }

    template < class T >
    inline T Factorial< T >::GetMaxN()
    {
        return FactorialTraits< T >::MAX_N;
    }

    template < class T >
    inline T Factorial< T >::GetMaxValue()
    {
        return FactorialTraits< T >::MAX_VALUE;
    }

    char const                  FactorialTraits< char >::MAX_N                      = 5;
    char const                  FactorialTraits< char >::MAX_VALUE                  = 120;
    unsigned char const         FactorialTraits< unsigned char >::MAX_N             = 5;
    unsigned char const         FactorialTraits< unsigned char >::MAX_VALUE         = 120;

    short const                 FactorialTraits< short >::MAX_N                     = 7;
    short const                 FactorialTraits< short >::MAX_VALUE                 = 5040;
    unsigned short const        FactorialTraits< unsigned short >::MAX_N            = 8;
    unsigned short const        FactorialTraits< unsigned short >::MAX_VALUE        = 40320;

    int const                   FactorialTraits< int >::MAX_N                       = 12;
    int const                   FactorialTraits< int >::MAX_VALUE                   = 479001600;
    unsigned int const          FactorialTraits< unsigned int >::MAX_N              = 12;
    unsigned int const          FactorialTraits< unsigned int >::MAX_VALUE          = 479001600;

    long const                  FactorialTraits< long >::MAX_N                      = 12;
    long const                  FactorialTraits< long >::MAX_VALUE                  = 479001600;
    unsigned long const         FactorialTraits< unsigned long >::MAX_N             = 12;
    unsigned long const         FactorialTraits< unsigned long >::MAX_VALUE         = 479001600;

    unsigned long long const    FactorialTraits< unsigned long long >::MAX_N        = 20UL;
    unsigned long long const    FactorialTraits< unsigned long long >::MAX_VALUE    = 2432902008176640000UL;

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH_FUZZY_VALUE_HPP__
// EOF

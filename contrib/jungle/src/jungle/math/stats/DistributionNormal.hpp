//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/stats/DistributionNormal.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__STATS__DISTRIBUTION_NORMAL_HPP__
#define __INCLUDED__MATH__STATS__DISTRIBUTION_NORMAL_HPP__

// Internal includes
#include <jungle/math/stats/Distribution.hpp>

namespace Jungle
{
namespace Math
{
    //! Class providing normalized Gaussian distribution model.
    template < class TReal >
    class DistributionNormal : public Distribution< TReal >
    {
    public:
        virtual TReal       Pdf( const TReal& x ) const;

        virtual TReal       Cdf( const TReal& x ) const;

        static TReal        Density( TReal x );

        static TReal        CumulativeDensity( TReal x );

    }; // class DistributionNormal

    template < class TReal >
    TReal DistributionNormal< TReal >::Pdf( const TReal& x ) const
    {
        return DistributionNormal< TReal >::Density( x );
    }

    template < class TReal >
    TReal DistributionNormal< TReal >::Cdf( const TReal& x ) const
    {
        return DistributionNormal< TReal >::CumulativeDensity( x );
    }

    template < class TReal >
    TReal DistributionNormal< TReal >::Density( TReal x )
    {
        // General Gaussian distribution function takes form
        // n(x) = exp(-(x-m)^2/(2*s^2) ) / (s * sqrt(2*pi))
        // where:
        // m - mean,
        // s - standard deviation.
        // For normalized (m = 0, s = 1) Gaussian distribution it takes simplest form:
        // n(x) = exp( -x^2/2 ) / sqrt(2*pi)
        const TReal invSqrt2Pi = TReal(0.39894228);
        return exp( -x * x / TReal(2) ) * invSqrt2Pi;
    }

    template < class TReal >
    TReal DistributionNormal< TReal >::CumulativeDensity( TReal x )
    {
        // Abromowitz and Stegun approximation. From from Abromowitz and Stegun, Handbook of Mathematical Functions.
        // It has a maximum absolute error of 7.5e^-8.
        // N(x) = integral[ -inf, x ] ( n(t)dt )
        // N(x) = 1 - n(x)( b1 * t + b2 * t^2 + b3 * t^3 + b4 * t^4 + b5 * t^5 ) + err,
        // where:
        // t = 1 / ( 1 + p * x )
        const TReal b1 = TReal(0.319381530);
        const TReal b2 = TReal(-0.356563782);
        const TReal b3 = TReal(1.781477937);
        const TReal b4 = TReal(-1.821255978);
        const TReal b5 = TReal(1.330274429);
        const TReal p  = TReal(0.2316419);

        if(x >= 0 )
        {
            TReal t = TReal(1.0) / ( TReal(1.0) + p * x );
            return  ( TReal(1.0) - Density( x ) * t *
                    ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
        }
        else
        {
            TReal t = TReal(1.0) / ( TReal(1.0) - p * x );
            return  ( Density( x ) * t *
                    ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
        }
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH__STATS__DISTRIBUTION_NORMAL_HPP__
// EOF

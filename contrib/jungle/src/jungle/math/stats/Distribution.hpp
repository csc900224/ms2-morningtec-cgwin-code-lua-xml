//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/stats/Distribution.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__STATS__DISTRIBUTION_HPP__
#define __INCLUDED__MATH__STATS__DISTRIBUTION_HPP__

namespace Jungle
{
namespace Math
{
    //! Probability distribution model - abstract class.
    template < class TReal >
    class Distribution
    {
    public:
        //! Calculate value of Probability Density Function.
        /*!
        * For a continuous function, the probability density function (pdf) returns the probability
        * that the variate has the value x. Since for continuous distributions the probability at a single point
        * is actually zero, the probability is better expressed as the integral of the pdf between two points.
        * \see Cdf() (Cumulative Distribution Function).
        * For a discrete distribution, the pdf is the probability that the variate takes the value x.
        */
        virtual TReal       Pdf( const TReal& x ) const = 0;

        //! Calculate Cumulative Distribution Function.
        /*!
        * Cumulative Distribution Function is the probability that the variable takes a value
        * less than or equal to x. It is equivalent to the integral from -(minus)infinity to x of
        * the Probability Density Function.
        */
        virtual TReal       Cdf( const TReal& x ) const = 0;

    }; // class Distribution

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH__STATS__DISTRIBUTION_HPP__
// EOF

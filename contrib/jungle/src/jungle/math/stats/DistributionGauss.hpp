//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/stats/DistributionGauss.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__STATS__DISTRIBUTION_GAUSS_HPP__
#define __INCLUDED__MATH__STATS__DISTRIBUTION_GAUSS_HPP__

// Internal includes
#include <jungle/math/stats/DistributionNormal.hpp>

namespace Jungle
{
namespace Math
{
    //! Class providing normalized Gaussian distribution model.
    template < class TReal >
    class DistributionGauss : public DistributionNormal< TReal >
    {
    public:
        //! By default gauss distribution will use exaclty normal distribution with mean=0 and standard deviation=1.
                            DistributionGauss();

        //! Create gaussian distribution with and given mean and standard deviation values.
                            DistributionGauss( TReal mean, TReal stdDev );

        //! Gaussian (normal) density function.
        virtual TReal       Pdf( const TReal& x ) const;

        //! Gaussian cumulated density.
        virtual TReal       Cdf( const TReal& x ) const;

        static TReal        Density( TReal x, TReal mu, TReal stdDev );

        static TReal        CumulativeDensity( TReal x, TReal mu, TReal stdDev );

    protected:
        TReal               m_mean;             //!< mu
        TReal               m_standardDev;      //!< standard deviation - sigma

    }; // class DistributionGauss

    template < class TReal >
    DistributionGauss< TReal >::DistributionGauss()
        : m_mean( NumberTraits< TReal >::ZERO )
        , m_standardDev( NumberTraits< TReal >::ONE )
    {
    }

    template < class TReal >
    DistributionGauss< TReal >::DistributionGauss( TReal mean, TReal stdDev )
        : m_mean( mean )
        , m_standardDev( stdDev )
    {
    }

    template < class TReal >
    TReal DistributionGauss< TReal >::Pdf( const TReal& x ) const
    {
        return Density( x, m_mean, m_standardDev );
    }

    template < class TReal >
    TReal DistributionGauss< TReal >::Cdf( const TReal& x ) const
    {
        return CumulativeDensity( x, m_mean, m_standardDev );
    }

    template < class TReal >
    TReal DistributionGauss< TReal >::Density( TReal x, TReal mean, TReal standardDev )
    {
        // Could be also:
        // TReal xn = (x - mean) / standardDev;
        // return DistributionNormal< TReal >::Density( xn ) / standardDev;

        // Use general Gaussian distribution function that takes form
        // n(x) = exp(-(x-m)^2 / ( 2 * s^2 ) ) / ( s * sqrt( 2*pi ) )
        // where:
        // m - mean,
        // s - standard deviation.
        // After replacing (x - m) / s, with:
        TReal xn = (x - mean) / standardDev;
        // we have:
        // exp( -xp * xp / 2 ) / ( s * sqrt( 2*pi ) )
        // then we replace:
        // invSqrt2Pi = 1 / sqrt( 2 * pi ), with:
        const TReal invSqrt2Pi = TReal(0.39894228);
        return exp( -xn * xn / TReal(2) ) * invSqrt2Pi / standardDev;
    }

    template < class TReal >
    TReal DistributionGauss< TReal >::CumulativeDensity( TReal x, TReal mean, TReal standardDev )
    {
        // Use distribution conversion to normalized one N( 0, 1 )
        TReal xn = (x - mean ) / standardDev;

        return DistributionNormal< TReal >::CumulativeDensity( xn );
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH__STATS__DISTRIBUTION_GAUSS_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/stats/TestRuns.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__STATS__TEST_RUNS_HPP__
#define __INCLUDED__MATH__STATS__TEST_RUNS_HPP__

// Internal includes

// External includes
#include <vector>

namespace Jungle
{
namespace Math
{
    //! Runs test, also called Wald�Wolfowitz or Stevens' test.
    /*!
    * Non-parametric statistical test that checks a randomness hypothesis for a data sequence.
    * More precisely, test can be used to test the hypothesis that the elements of the sequence are mutually
    * independent and identically distributed
    * Runs tests can be used to test:
    * 1) The randomness of a distribution, by taking the data in the given order and marking
    * with A the data greater than the median, and with B the data less than the median, numbers
    * equalling the median are omitted.
    * 2) To test whether a function fits well to a data set, by marking the data exceeding the function
    * valuef with A and the other data with B. For this use, the runs test, which takes into account the signs
    * but not the distances, is complementary to the chi square test, which takes into account the distances
    * but not the signs.
    */
    template < class TReal >
    class TestRuns
    {
    public:
        typedef unsigned int    Count;

        //! Add single observation to tested samples.
        void                    AddSample( const TReal& sample );

        //! Add collection of observations to the testing population.
        /*!
        * Method will accept any type of stl random access iterators or even raw pointers from the same
        * memory region (array).
        */
        template< class TIter >
        void                    AddSamples( const TIter& fistSample, const TIter& endSample );

        //! Perform hypothesis check for the population (samples) stored in object and given alpha value.
        bool                    Test( float alpha );

        //! Check the hypothesis for a given samples range.
        /*!
        * Method will accept any type of forward iterator or event simple pointers from the same
        * memory region.
        */
        template< class TIter >
        static bool             Test( float alpha, const TIter& fistSample, const TIter& endSample );

        //! Check the hypothesis for given observations serie with a precalculated median for that range.
        /*!
        * Method will accept any type of forward iterator or event simple pointers from the same
        * memory region.
        */
        template< class TIter >
        static bool             Test( float alpha, TReal median, const TIter& firstSample, const TIter& endSample );

    private:
        template< class TIter >
        static Count            CountSeries( const TIter& fistSample, const TIter& endSample, const TReal& median, Count& above, Count& below );

        typedef std::vector< TReal >                Samples;
        typedef typename Samples::iterator          SamplesIt;
        typedef typename Samples::const_iterator    SamplesConstIt;

        Median< TReal >         m_median;
        Samples                 m_samples;
        float                   m_alpha;    //!< Probability of making the mistake of second type.

    }; // class TestRuns

} // namespace Math

} // namespace Jungle

#include <jungle/math/stats/TestRuns.inl>

#endif // !defined __INCLUDED__MATH__STATS__TEST_RUNS_HPP__
// EOF

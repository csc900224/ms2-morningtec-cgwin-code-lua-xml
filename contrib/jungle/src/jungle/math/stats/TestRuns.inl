//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/stats/TestRuns.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes

// External includes

namespace Jungle
{
namespace Math
{
    template < class TReal >
    void TestRuns< TReal >::AddSample( const TReal& sample )
    {
        m_samples.push_back( sample );
        m_median.AddSample( sample );
    }

    template < class TReal >
    template < class TIter >
    void TestRuns< TReal >::AddSamples( const TIter& firstSample, const TIter& endSample )
    {
        m_samples.insert( m_samples.end(), firstSample, endSample );
        m_median.AddSamples( firstSample, endSample );
    }

    template < class TReal >
    bool TestRuns< TReal >::Test( float alpha )
    {
        return Test( alpha, m_median.GetValue(), m_samples.begin(), m_samples.end() );
    }

    template < class TReal >
    template< class TIter >
    bool TestRuns< TReal >::Test( float alpha, const TIter& firstSample, const TIter& endSample )
    {
        TReal median = Median< TReal >::Calculate( firstSample, endSample );
        return Test( alpha, median, firstSample, endSample );
    }

    template < class TReal >
    template< class TIter >
    bool TestRuns< TReal >::Test( float alpha, TReal median, const TIter& firstSample, const TIter& endSample )
    {
        float Z = 0;
        float k1 = 0, k2 = 0;
        Count nA, nB;
        Count k = CountSeries( firstSample, endSample, median, nA, nB );
        // We will test null hypothesis that stands about randomness of the samples.
        // H0: Series == Random
        // H1: Series != Random
        // For that case we need to setup critical area which is double sided/tailed:
        // K = ( -inf, k1 > v < k2, +inf )
        // If Z C ( -inf, k1 > v < k2, +inf ) we need to null (H0) hypothesis
        // If Z C ( k1, k2 ) we have no reasons to refuse H0
        // For small nA, nB we read critical values from the tables: series number distribution
        if( false && nA <= 20 && nB <= 20 )
        {
            // k1 ( alpha / 2, nA, nB )
            // k2 ( 1 - alpha / 2, nA, nB )
            // k (the number of series) will be our test statistic
            Z = (float)k;
        }
        else
        {
            // For bigger nA, nB values k variable leads asymptotically to the normal distribution with
            // mean E(k) and variance D(k) defined below.
            DistributionNormal<float> dNorm;
            k1 = dNorm.Cdf( alpha / 2 );
            k2 = dNorm.Cdf( 1 - alpha / 2 );

            // For nA && nB > 20, k variable leads asymptotically to the normal distribution with mean E(k) and variance D(k)
            // N(E(K),D(K)) defined below:
            if( k < 1000 )
            {
                float Ek = (float)( 2 * nA * nB ) / (float)k + 1;
                float Dk = (float)( 2 * nA * nB ) * ( 2 * nA * nB - k ) / (float)( k - 1 ) * k * k;
                Dk = Sqrt( Dk );
                if( Dk < NumberTraits< float >::Epsilon() )
                    Dk = NumberTraits< float >::Epsilon();
                // Using above mean and deviation we may calculate statistic Z which assuming H0 has normal N(0,1) distribution
                Z = ( k - Ek ) / ( Dk );
            }
            // For equal distribution and very big series number we may use N( k / 2; sqrt(n) / 2 )
            else
            {
                float Ek = (float)k / 2;
                float Dk = Sqrt( (float)k ) / 2;
                // Using above mean and deviation we may calculate statistic Z which assuming H0 has normal N(0,1) distribution
                if( Dk < NumberTraits< float >::Epsilon() )
                    Dk = NumberTraits< float >::Epsilon();
                Z = ( k - Ek ) / ( Dk );
            }
        }
        // compare observed number of series (k) or Z statistic with critical test area:
        if( Z <= k1 || Z >= k2 )
            // we need refuse H0 hypothesis (observations' serie is random / model is linear) which means,
            // (H1) that serie is not random or model is not linear
            return false;
        else
            return true;
    }

    template < class TReal >
    template< class TIter >
    typename TestRuns< TReal >::Count TestRuns< TReal >::CountSeries( const TIter& firstSample, const TIter& endSample, const TReal& median, Count& above, Count& below )
    {
        TIter it = firstSample;
        bool belowMedian = *it > median;    // forces first switch
        above = 0;
        below = 0;
        for( ; it != endSample; ++it )
        {
            if( belowMedian && *it > median )
            {
                belowMedian = false;
                ++above;
            }
            else if( !belowMedian && *it < median )
            {
                belowMedian = true;
                ++below;
            }
            // Observations equal to median are ignored
        }
        return above + below;
    }

} // namespace Math

} // namespace Jungle

// EOF

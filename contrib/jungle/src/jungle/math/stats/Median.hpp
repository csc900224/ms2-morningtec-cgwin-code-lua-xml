//////////////////////////////////////////////////////////////////////////
//
//  FILE NAME:
//      jungle/math/stats/Median.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@gmail.com>
//  NOTES:
//
//
// Copyright (c) 2009, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__STATS__MEDIAN_HPP__
#define __INCLUDED__MATH__STATS__MEDIAN_HPP__

// Internal includes
#include <jungle/base/NumberTraits.hpp>

// External includes
#include <vector>

namespace Jungle
{
namespace Math
{
    //! Simple statistic describing the value that splits population in two halfs.
    template < class TReal >
    class Median
    {
    public:
        //! Default constructor initializes internal container for sorted variable range.
                                Median();

        //! Add sample to the population from which the middle value will be searched.
        void                    AddSample( const TReal& sample );

        //! Add samples serie at once.
        /*!
        * Method will accept any type of forward iterator or event simple pointers from the same
        * memory region.
        */
        template < class TFwdIter >
        void                    AddSamples( const TFwdIter& itBegin, const TFwdIter& itEnd );

        //! Get middle value of gathered samples population.
        const TReal&            GetValue() const;

        //! Static implementation that allows to acquire Median from any observations set.
        /*!
        * Method will accept any type of stl random access iterators or even raw pointers from the same
        * memory region (array).
        */
        template < class TRanIter >
        static TReal            Calculate( const TRanIter& itBegin, const TRanIter& itEnd );

    protected:
        typedef std::vector< TReal >                Samples;
        typedef typename Samples::const_iterator    SamplesConstIt;

        void                    Precalculate();

        Samples                 m_samples;
        TReal                   m_median;

    }; // class Median

    // Common types definitions.
    typedef Median< float >     Medianf;
    typedef Median< double >    Mediand;

    template < class TReal >
    Median< TReal >::Median()
        : m_median( NumberTraits< TReal >::ZERO )
    {
    }

    template < class TReal >
    void Median< TReal >::AddSample( const TReal& sample )
    {
        SamplesConstIt lowIt = std::lower_bound( m_samples.begin(), m_samples.end(), sample );
        m_samples.insert( lowIt, sample );

        Precalculate();
    }

    template < class TReal >
    template < class TFwdIter >
    void Median< TReal >::AddSamples( const TFwdIter& itBegin, const TFwdIter& itEnd )
    {
        SamplesConstIt lowIt;
        TFwdIter it = itBegin;
        for( ; it != itEnd ; ++it )
        {
            lowIt = std::lower_bound( m_samples.begin(), m_samples.end(), *it );
            m_samples.insert( lowIt, *it );
        }

        Precalculate();
    }

    template < class TReal >
    void Median< TReal >::Precalculate()
    {
        SamplesConstIt var0 = m_samples.begin();
        size_t n = m_samples.size();
        JUNGLE_WARNING_MSG(  n % 2 ||
                            ((TReal)(NumberTraits< TReal >::ONE / 2)) != 0,
                            "May not provide accurate median value for discreet observations." );
        // Median may take two variants, depending from the number of observations (n),
        // if n % 2 != 0:
        // M = var[(n + 1) / 2]
        // if n % 2 == 0
        // M = ( var[n/2] + var[n/2 + 1] ) / 2
        // Because the first index in container is zero we need to substract -1 from above indexes.
        if( n % 2 )
            m_median =  *( var0 + ( (n + 1) / 2 - 1 ) );
        else
            m_median = (*( var0 + ( (n / 2) - 1 ) ) +
                        *( var0 + ( (n / 2) ) ) ) / 2;
    }

    template < class TReal >
    const TReal& Median< TReal >::GetValue() const
    {
        JUNGLE_ASSERT( !m_samples.empty() );

        return m_median;
    }

    template < class TReal >
    template < class TRanIter >
    TReal Median< TReal >::Calculate( const TRanIter& itBegin, const TRanIter& itEnd )
    {
        if( itBegin == itEnd )
            return *itBegin;
        std::vector< typename std::iterator_traits<TRanIter>::value_type > tmp( itBegin, itEnd );
        std::sort( tmp.begin(), tmp.end() );

        std::vector< typename std::iterator_traits<TRanIter>::value_type >::const_iterator var0 = tmp.begin();
        size_t n = tmp.size();
        if( n % 2 )
            return  *( var0 + ( (n + 1) / 2 - 1 ) );
        else
            return  *( var0 + ( (n / 2) - 1 ) ) +
                    *( var0 + ( (n / 2) ) );
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH__STATS__MEDIAN_HPP__
// EOF

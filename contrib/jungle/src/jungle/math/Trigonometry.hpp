//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Trigonometry.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_TRIGONOMETRY_HPP__
#define __INCLUDED__MATH_TRIGONOMETRY_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    template < class TReal >
    inline TReal        Deg2Rad( TReal deg );

    template < class TReal >
    inline TReal        Rad2Deg( TReal rad );

    template < class TReal >
    inline TReal        Sin( TReal rad );

    template < class TReal >
    inline TReal        SinRad( TReal deg );

    template < class TReal >
    inline TReal        SinDeg( TReal deg );

    template < class TReal >
    inline TReal        FastSin0( TReal rad );

    template < class TReal >
    inline TReal        FastSin1( TReal rad );

    template < class TReal >
    inline TReal        Cos( TReal rad );

    template < class TReal >
    inline TReal        CosRad( TReal rad );

    template < class TReal >
    inline TReal        CosDeg( TReal deg );

    template < class TReal >
    inline TReal        FastCos0( TReal rad );

    template < class TReal >
    inline TReal        FastCos1( TReal rad );

    template < class TReal >
    inline void         SinCosRad( TReal* sin, TReal* cos, TReal rad );

    template < class TReal >
    inline void         SinCosDeg( TReal* sin, TReal* cos, TReal deg );

    template < class TReal >
    inline TReal        ACos( TReal val );

    /*!
    * Compute ArcCos(f) function, in value domain -1 <= f <= 1.
    * Since ArcCos is 1->n function returns correct values
    * only for angle range <0, PI> (first two quarters).
    * \note             Function is less precise in second quarter especially when
    *                   using not standardized (tabelarized) cosines values.
    *                   Use it rather for estimating angles only from < 0, PI/2 > range.
    * \param val        cosines value which angle will be evaluated
    * \return           angle measured in radians.
    **/
    template < class TReal >
    inline TReal        FastACos( TReal val );

    /*!
    * Computes ArcTan(f) function.
    * Resulting angles may differ from (-PI/2, PI/2).
    * Function is more accurate in domain -1 <= f <= 1 (|f| <= 1).
    * \note             All approximations are accurate for |t| <= 1, other values are calculated
    *                   using formulas:
    *                   ArcTan(t) for t > 1, will use ArcTan(t) = PI/2 - ArcTan(1/t).
    *                   For t < -1, ArcTan(t) = -PI/2 - ATAN(1/t) will be used
    * \param            Tangent value for which angle will be approximated.
    * \return           Resulting angle in radians.
    **/
    template < class TReal >
    inline TReal        ATan( TReal f );

    /*!
    * Computes ArcTan(f) function, returning angles in range (-PI, PI>.
    * Function calculates proper results for cosines in range (-1, 1>, thus angles (-PI, PI>, can be properly approximated.
    * \note             Function uses internally ArcTan so its accuracy is based on this function.
    * \param sin        Sines of angle being approximated.
    * \param sin        Cosines of angle being approximated.  
    * \return           Resulting angle in radians range (-PI, PI>.
    **/
    template < class TReal >
    inline TReal        ATan2( TReal sin, TReal cos );

    /*!
    * Fast ArcTan2(sin, cos) function computing a 4-quadrant arctangent based on article from DSP tricks:
    * http://www.dspguru.com/comp.dsp/tricks/alg/fxdatan2.htm.
    * \note             Estimator's accuracy is quite good even using 1st-order polynomial to estimate the phase angle.
    *                   The maximum estimation error should be little less than 0.07 rads (only at a few angles though).
    * \param y          Sines of angle being approximated.
    * \param x          Cosines of angle being approximated.
    * \return           Resulting angle in radians range (-PI, PI>.
    **/
    template < class TReal >
    inline TReal        FastATan2( TReal y, TReal x );

} // namespace Math

} // namespace Jungle

#include <jungle/math/Trigonometry.inl>

#endif // !defined __INCLUDED__MATH_TRIGONOMETRY_HPP__
// EOF

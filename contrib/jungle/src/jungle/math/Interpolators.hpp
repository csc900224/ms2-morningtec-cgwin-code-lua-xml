//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Interpolators.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
// Copyright (c) 2006, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_INTERPOLATORS_HPP__
#define __INCLUDED__MATH_INTERPOLATORS_HPP__

// Internal includes
#include <jungle/base/Types.hpp>

namespace Jungle
{
namespace Math
{
    // For easier time conversion
    static const float MILLSEC    = 0.001f;

    //! Utility class with few static methods for mid value interpolation.
    class Interpolation
    {
    public:
        /*!
        * Simple linear interpolation between two fixed point values
        * \param mu             interpolation factor <0, 1>
        * \param y0             interpolation range start
        * \param y1             end of interpolation range
        */
        template< class T >
        static inline T         Linear( float mu, const T& start, const T& end );

        /*!
        * Simple cosines interpolation between two fixed point values
        * Cosines interpolation offer smoother results then linear, 
        * cosine function provides close to continuous transition between adjacent segments.
        * \param mu             interpolation factor <0, 1>
        * \param y0             interpolation range start
        * \param y1             end of interpolation range
        * \sa Linear()
        */
        template< class T >
        static inline T         Cosine( float mu, const T& start, const T& end );

        /*!
        * Cubic interpolation function between two fixed point values.
        * This kind of interpolation provides true continuity between the segments. 
        * It requires two additional end point on either side of interpolated segment.
        * \param mu             interpolation factor <0, 1>, 
        *                       fraction of distance between y1 and y2 segment points
        * \param y0             additional left side endpoint
        * \param y1             first point in the interpolated range
        * \param y2             second point in the interpolated segment
        * \param y3             additional right side endpoint
        */
        template< class T >
        static T                Cubic( float mu, const T& y0, const T& y1, const T& y2, const T& y3);

        /*!
        * Hermite interpolation between two fixed point values.
        * Similarly to cubic interpolation requires 4 points to achieve desired degree of continuity. 
        * Allows controlling interpolated curvature using tension and biasing controls. 
        * Tension parameter can be used to tighten up the curvature at the known points. 
        * Bias is used to twist the curve about the known points. 
        * \param mu             interpolation factor <0, 1>, 
        *                       fraction of distance between y1 and y2 segment points
        * \param y0             additional left side endpoint
        * \param y1             first point in the interpolated range
        * \param y2             second point in the interpolated segment
        * \param y3             additional right side endpoint
        * \param tension        controls curvature relaxation 
        *                       (1 - tight curvature, 0 - neutral, -1 - relaxed curve)
        * \param bias           negative bias "moves" interpolation towards second segment, positive
        *                       twist curve around fist segment
        */
        template < class T >
        static T                Hermite( float mu, const T& y0, const T& y1, const T& y2, const T& y3, float tension, float bias);

    }; // class Interpolation

    template < class T >
    class Interpolator
    {
    public:
        virtual bool    Setup(T aFrom, T aTo, float aTime) = 0;
        virtual bool    Interpolate(float aTick) = 0;

        const T&        GetValue() const    { return m_val; }
        const T&        GetTarget() const   { return m_target; }
        const T&        GetVelocity() const { return m_vel; }
        float           GetTimeLeft() const { return m_timeRemain; }

        // Minimal time step to perform interpolation
        static
        const float     TIME_EPSILON;

    protected:
        T               m_val;
        T               m_vel;
        T               m_target;
        T               m_from;
        float           m_timeRemain;
        float           m_timeElapsed;

    }; // class template Interpolator

    template< class T >
    const float Interpolator< T >::TIME_EPSILON = (MILLSEC * 10);

    // Common types definitions
    typedef Interpolator<int32>     Interpolatori;
    typedef Interpolator<f32>       Interpolatorf;
    typedef Interpolator<f64>       Interpolatord;

    template < class T >
    class LinearInterpolator: public Interpolator < T >
    {
    public:
        virtual bool    Setup(T aFrom, T aTo, float aTime);

        virtual bool    Interpolate(float aTick);

    }; // class template LinearInterpolator

    template<class T>
    class EaseInOutInterpolator : public Interpolator<T>
    {
    public:
        virtual bool    Setup(T aFrom, T aTo, float aTime);

        virtual bool    Interpolate(float aTick);

    protected:
        T               m_acc;
        T               m_halfWay;
        float           m_halfTime;

    }; // class template EaseInOutInterpolator 

    template<class T>
    class EaseInInterpolator : public Interpolator<T>
    {
    public:
        virtual bool    Setup(T aFrom, T aTo, float aTime);

        virtual bool    Interpolate(float aTick);

    protected:
        T               m_acc;
        float           m_timeTotal;

    }; // class template EaseInInterpolator 

    template<class T>
    class EaseOutInterpolator : public Interpolator<T>
    {
    public:
        virtual bool    Setup(T aFrom, T aTo, float aTime);

        virtual bool    Interpolate(float aTick);

    protected:
        T               m_acc;

    }; // class template EaseOutInterpolator

} // namespace Math

} // namespace Jungle

#include <jungle/math/Interpolators.inl>

#endif // !defined __INCLUDED__MATH_INTERPOLATORS_HPP__
// EOF

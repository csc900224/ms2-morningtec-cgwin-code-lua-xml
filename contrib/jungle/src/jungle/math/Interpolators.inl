//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Interpolators.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
// Copyright (c) 2006, Krystian Kostecki
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
//   this list of conditions and the following disclaimer in the documentation and/or other materials 
//   provided with the distribution.
// * Neither the name of the Krystian Kostecki nor the names of its contributors 
//   may be used to endorse or promote products derived from this software without specific 
//   prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////

#include <jungle/base/NumberTraits.hpp>
#include <jungle/math/Trigonometry.hpp>

namespace Jungle
{
namespace Math
{
    template< class T >
    inline T Interpolation::Linear( float mu, const T& start, const T& end )
    {
        return( start * ( 1.0f - mu ) + end * mu );
    }

    template< class T >
    inline T Interpolation::Cosine(float mu, const T& start, const T& end )
    {
        float mu2 = ( 1.0f - Math::Cos( mu * NumberTraits<float>::PI ) ) / 2;
        return ( start * ( 1.0f - mu2 ) + end * mu2 );
    }

    template < class T >
    inline T Interpolation::Cubic( float mu, const T& y0, const T& y1, const T& y2, const T& y3 )
    {
        float mu2 = (mu * mu);
        T a0,a1,a2,a3;
        a0 = y3 - y2 - y0 + y1;
        a1 = y0 - y1 - a0;
        a2 = y2 - y0;
        a3 = y1;

        return ( ( (a0 * mu) * mu2) + (a1 * mu2) + (a2 * mu) +a3 );
    }

    template < class T >
    inline T Interpolation::Hermite( float mu, const T& y0, const T& y1, const T& y2, const T& y3, float tension, float bias)
    {
        register float mu2, mu3;
        float a0, a1, a2, a3;
        T m0, m1;

        mu2 = (mu * mu);
        mu3 = (mu2 * mu);

        a0 = ( 2 * mu3 ) - ( 3 * mu2 ) + 1.0f;
        a1 = mu3 - (2 * mu2) + mu;
        a2 = mu3 -   mu2;
        a3 = -(2 * mu3) + (3 * mu2);

        m0  = ( ( (y1 - y0) * (1.0f + bias) ) * (1.0f - tension) / 2 );
        m0 += ( ( (y2 - y1) * (1.0f - bias) ) * (1.0f - tension) / 2 );
        m1  = ( ( (y2 - y1) * (1.0f + bias) ) * (1.0f - tension) / 2 );
        m1 += ( ( (y3 - y2) * (1.0f - bias) ) * (1.0f - tension) / 2 );

        return ( (a0 * y1) + (a1 * m0) + (a2 * m1)+ (a3 * y2) );
    }


    template < class T >
    bool LinearInterpolator< T >::Setup(T aFrom, T aTo, float aTime)
    {
        Interpolator<T>::m_target = aTo;
        if(aTime == 0)
        {
            Interpolator<T>::m_val = aTo;
            Interpolator<T>::m_timeRemain = 0;
            Interpolator<T>::m_vel = Interpolator<T>::m_val - Interpolator<T>::m_val;
            return false;
        }
        else
        {
            Interpolator<T>::m_timeRemain = aTime;
            Interpolator<T>::m_val = aFrom;
            Interpolator<T>::m_vel = (aTo - aFrom)/aTime;
            return true;
        }
    }

    template < class T >
    bool LinearInterpolator< T >::Interpolate(float aTick)
    {
        if(Interpolator<T>::m_timeRemain == 0)
        {
            return false;
        }
        else if(Interpolator<T>::m_timeRemain <= aTick )
        {
            Interpolator<T>::m_timeRemain = 0;
            Interpolator<T>::m_val = Interpolator<T>::m_target;
            return true;
        }
        else
        {
            Interpolator<T>::m_timeRemain -= aTick;
        }

        Interpolator<T>::m_val += Interpolator<T>::m_vel * aTick;
        return true;
    }

    template < class T >
    bool EaseInOutInterpolator< T >::Setup(T aFrom, T aTo, float aTime)
    {
        Interpolator<T>::m_target = aTo;
        Interpolator<T>::m_from   = aFrom;

        // When time is smaller then time epsilon or too small to find half time - finnish interpolation
        if( (aTime < Interpolator<T>::TIME_EPSILON) || (aTime * 0.5f) == 0 )
        {
            Interpolator<T>::m_val = aTo;
            Interpolator<T>::m_timeRemain = m_halfTime = float(0);
            return false;
        }

        Interpolator<T>::m_val = aFrom;
        Interpolator<T>::m_timeRemain = aTime;
        Interpolator<T>::m_timeElapsed = 0;

        // From equation x = x0 + v0*t + a*t*t/2 ^ v0 = 0
        // Find half way (time) acceleration/deceleration
        m_acc = (aTo - aFrom) * 4 / (aTime * aTime);
        Interpolator<T>::m_vel = m_acc * aTime / 2;

        m_halfWay = Interpolator<T>::m_from + (Interpolator<T>::m_target - Interpolator<T>::m_from) * 0.5f;
        m_halfTime = aTime * 0.5f;
        return true;
    }

    template < class T >
    bool EaseInOutInterpolator< T >::Interpolate(float aTick)
    {
        if( Interpolator<T>::m_timeRemain == 0 )
        {
            return false;
        }
        else if( Interpolator<T>::m_timeRemain <= aTick )
        {
            Interpolator<T>::m_timeRemain = 0;
            Interpolator<T>::m_val = Interpolator<T>::m_target;
            return true;
        }
        else
        {
            Interpolator<T>::m_timeRemain  -= aTick;
            Interpolator<T>::m_timeElapsed += aTick;
        }

        if( Interpolator<T>::m_timeElapsed < m_halfTime )
        {
            Interpolator<T>::m_val = Interpolator<T>::m_from + (m_acc * Interpolator<T>::m_timeElapsed * Interpolator<T>::m_timeElapsed) * 0.5f;
        }
        else
        {
            float afterHalfTime = Interpolator<T>::m_timeElapsed - m_halfTime;

            Interpolator<T>::m_val =    m_halfWay + \
                                        Interpolator<T>::m_vel * \
                                        afterHalfTime - \
                                        (m_acc * afterHalfTime * afterHalfTime) * \
                                        0.5f;
        }
        return true;
    }

    template < class T >
    bool EaseInInterpolator< T >::Setup(T aFrom, T aTo, float aTime)
    {
        Interpolator<T>::m_target = aTo;
        Interpolator<T>::m_from   = aFrom;

        // When time is smaller then time epsilon end interpolation
        if(aTime < Interpolator<T>::TIME_EPSILON)
        {
            Interpolator<T>::m_val = aTo;
            Interpolator<T>::m_timeRemain = 0;
            return false;
        }

        Interpolator<T>::m_val = aFrom;
        Interpolator<T>::m_timeRemain = aTime;
        Interpolator<T>::m_timeElapsed = 0;

        // From equation x = x0 + v0*t + a*t*t/2 ^ v0 = 0
        // We have: (x - x0) = a*t*t/2
        // Find acceleration needed to interpolate distance during required time
        m_acc = (aTo - aFrom) * 2 / (aTime * aTime);
        // Trick to get zero (vector, scalar)
        Interpolator<T>::m_vel = aFrom - aFrom;

        return true;
    }

    template < class T >
    bool EaseInInterpolator< T >::Interpolate(float aTick)
    {
        if( Interpolator<T>::m_timeRemain == 0 )
        {
            return false;
        }
        else if( Interpolator<T>::m_timeRemain <= aTick )
        {
            Interpolator<T>::m_timeRemain = 0;
            Interpolator<T>::m_val = Interpolator<T>::m_target;
            return true;
        }
        else
        {
            Interpolator<T>::m_timeRemain  -= aTick;
            Interpolator<T>::m_timeElapsed += aTick;

            Interpolator<T>::m_val = Interpolator<T>::m_from + (m_acc * Interpolator<T>::m_timeElapsed * Interpolator<T>::m_timeElapsed) * 0.5f;
            return true;
        }
    }

    template < class T >
    bool EaseOutInterpolator< T >::Setup(T aFrom, T aTo, float aTime)
    {
        Interpolator<T>::m_target = aTo;
        Interpolator<T>::m_from   = aFrom;

        // When time is smaller then time epsilon end interpolation
        if( aTime < Interpolator<T>::TIME_EPSILON )
        {
            Interpolator<T>::m_val = aTo;
            Interpolator<T>::m_timeRemain = 0;
            return false;
        }

        Interpolator<T>::m_val = aFrom;
        Interpolator<T>::m_timeRemain = aTime;
        Interpolator<T>::m_timeElapsed = 0;

        // From equation x = x0 + v0*t + a*t*t/2
        // Find deceleration needed to interpolate distance during required time
        m_acc = (aTo - aFrom) * 2 / (aTime * aTime);
        // Compute start velocity assuming that end velocity should be equal 0
        // v1 = v0 + a * t =>  0 = v0 + a * t
        Interpolator<T>::m_vel = m_acc * aTime;

        return true;
    }

    template < class T >
    bool EaseOutInterpolator< T >::Interpolate(float aTick)
    {
        if( Interpolator<T>::m_timeRemain == 0 )
        {
            return false;
        }
        else if( Interpolator<T>::m_timeRemain <= aTick )
        {
            Interpolator<T>::m_timeRemain = 0;
            Interpolator<T>::m_val = Interpolator<T>::m_target;
            return true;
        }
        else
        {
            Interpolator<T>::m_timeRemain  -= aTick;
            Interpolator<T>::m_timeElapsed += aTick;

            Interpolator<T>::m_val =    Interpolator<T>::m_from + \
                                        Interpolator<T>::m_vel * \
                                        Interpolator<T>::m_timeElapsed - \
                                        (m_acc * Interpolator<T>::m_timeElapsed * Interpolator<T>::m_timeElapsed) * \
                                        0.5f;
            return true;
        }
    }

} // namespace Math

} // namespace Jungle
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/HarmonicDigital.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_HARMONIC_DIGITAL_HPP__
#define __INCLUDED__MATH_HARMONIC_DIGITAL_HPP__

// Internal includes
#include <jungle/math/Harmonic.hpp>

namespace Jungle
{
namespace Math
{
    //! Utility class for harmonics evaluation in discreet (digital) space.
    /*!
    * Class allows for easy signal evaluation in digital space - allowable values for output signal
    * are only 0 (zero) and max amplitude. Change harmonics phase ( SetPhase() ) withing time and get current signal value
    * depending on harmonics frequency (or period) - SetFrequency(), SetPeriod().
    */
    template < class TValue, class TPhase >
    class HarmonicDigital : public Harmonic< TValue, TPhase >
    {
    public:
        typedef TPhase      PhaseType;
        typedef TValue      ValueType;

        virtual void        SetFrequency( float freq )      { m_period = PhaseType(1.0f / freq); }
        virtual float       GetFrequency() const            { return 1.0f / m_period; }

        virtual void        SetPeriod( PhaseType period )   { m_period = period; }
        virtual PhaseType   GetPeriod() const               { return m_period; }

        virtual ValueType   GetValue() const
        {
            if (GetPhase() - ( ( (long long)( GetPhase() / GetPeriod() ) ) * GetPeriod() ) >= GetPeriod() / 2 )
                return m_amplitude;
            else
                return ValueType(m_amplitude - m_amplitude);
        }
    private:
        PhaseType           m_period;

    }; // class HarmonicDigital

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH_HARMONIC_DIGITAL_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Vector3.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/Errors.hpp>           // JUNGLE_ASSERT
#include <jungle/math/Arithmetic.hpp>       // Sqrt, Abs

namespace Jungle
{
namespace Math
{
    template < class T >
    inline void Vector3< T >::Set( T x, T y, T z )
    {
        m_x = x;
        m_y = y;
        m_z = z;
    }

    template < class T >
    inline void Vector3< T >::Zero()
    {
        m_x = m_y = m_z = NumberTraits< T >::ZERO;
    }

    /*
    template < class T >
    inline Vector3< T >::operator const T* () const
    {
        return m_coords;
    }

    template < class T >
    inline Vector3< T >::operator T* ()
    {
        return m_coords;
    }
    */

    template < class T >
    inline T Vector3< T >::operator [] ( index_type i ) const
    {
        JUNGLE_ASSERT( i < 3 );
        return m_coords[i];
    }

    template < class T >
    inline T& Vector3< T >::operator [] ( index_type i )
    {
        JUNGLE_ASSERT( i < 3 );
        return m_coords[i];
    }

    template < class T >
    inline bool Vector3< T >::operator == ( const Vector3< T >& v ) const
    {
        return  Math::Abs( m_x - v.m_x ) < NumberTraits<T>::ZERO_TOLERANCE &&
                Math::Abs( m_y - v.m_y ) < NumberTraits<T>::ZERO_TOLERANCE &&
                Math::Abs( m_z - v.m_z ) < NumberTraits<T>::ZERO_TOLERANCE;
    }

    template < class T >
    inline bool Vector3< T >::operator != ( const Vector3< T >& v ) const
    {
        return !operator==( v );
    }

    template < class T >
    inline bool Vector3< T >::operator > ( const Vector3< T >& v ) const
    {
        return ( m_x > v.m_x ) && ( m_y > v.m_y ) && ( m_z > v.m_z );
    }

    template < class T >
    inline bool Vector3< T >::operator >= ( const Vector3< T >& v ) const
    {
        return ( m_x >= v.m_x ) && ( m_y >= v.m_y ) && ( m_z >= v.m_z );
    }

    template < class T >
    inline bool Vector3< T >::operator < ( const Vector3< T >& v ) const
    {
        return ( m_x < v.m_x ) && ( m_y < v.m_y ) && ( m_z < v.m_z );
    }

    template < class T >
    inline bool Vector3< T >::operator <= ( const Vector3< T >& v ) const
    {
        return ( m_x <= v.m_x ) && ( m_y <= v.m_y ) && ( m_z > v.m_z );
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator += (const Vector3< T >& v)
    {
        m_x += v.m_x; m_y += v.m_y; m_z += v.m_z; 
        return *this;
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator += (T v)
    {
        m_x += v; m_y += v; m_z += v; 
        return *this;
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator /= (T v)
    {
        if( v != NumberTraits< T >::ZERO )
        {
            m_x = ( m_x / v );
            m_y = ( m_y / v );
            m_z = ( m_z / v );
        }
        else
        {
            JUNGLE_WARNING_MSG( "Division by scalar equal to ZERO." );
            m_x = m_y = m_z = NumberTraits< T >::Maximum();
        }
        return *this;
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator -= (const Vector3< T >& v)
    {
        m_x -= v.m_x; m_y -= v.m_y; m_z -= v.m_z; 
        return *this;
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator *= (const Vector3< T >& v)
    {
        m_x *= v.m_x; m_y *= v.m_y; m_z *= v.m_z; 
        return *this;
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator /= (const Vector3< T >& v)
    {
        m_x /= v.m_x; m_y /= v.m_y; m_z /= v.m_z;
        return *this;
    }

    template < class T >
    inline Vector3< T >& Vector3< T >::operator *= (T a)
    {
        m_x *= a; m_y *= a; m_z *= a;
        return *this;
    }

    template < class T >
    inline Vector3<T> Vector3< T >::operator * (T a) const
    {
        Vector3<T> ret( *this );
        ret *= a;
        return ret;
    }

    template < class T >
    inline Vector3<T> Vector3< T >::operator / (T a) const
    {
        Vector3<T> ret( *this );
        ret /= a;
        return ret;
    }

    template < class T >
    inline Vector3<T> Vector3< T >::operator - () const
    {
        Vector3< T > ret = Vector3< T >( -m_x, -m_y, -m_z );
        return ret;
    }

    template < class T >
    inline T Vector3< T >::Dot( const Vector3< T >& v ) const
    {
        return (m_x * v.m_x) + (m_y * v.m_y) + (m_z * v.m_z);
    }

    template < class T >
    inline Vector3< T > Vector3< T >::Cross( const Vector3< T >& v ) const
    {
        return Vector3< T > (
            (m_y * v.m_z) - (m_z * v.m_y),
            (m_z * v.m_x) - (m_x * v.m_z),
            (m_x * v.m_y) - (m_y * v.m_x)
        );
    }

    template < class T >
    inline T Vector3< T >::Length() const
    {
        return static_cast< T >( Math::Sqrt( LengthSqr() ) );
    }

    template < class T >
    inline T Vector3< T >::LengthSqr() const
    {
        return (m_x * m_x) + (m_y * m_y) + (m_z * m_z);
    }

    template < class T >
    inline T Vector3< T >::Normalize()
    {
        T length = Length();
        if( length > NumberTraits< T >::Epsilon() )
        {
            T lenInv = NumberTraits< T >::ONE / length;
            m_x = ( m_x * lenInv );
            m_y = ( m_y * lenInv );
            m_z = ( m_z * lenInv );
        }
        else
        {
            m_x = m_y = m_z = NumberTraits< T >::ZERO;
            length = NumberTraits< T >::ZERO;
        }
        return length;
    }

    template < class T >
    inline Vector3<T> Vector3< T >::Normalized() const
    {
        Vector3< T > normalized( *this );
        normalized.Normalize();
        return normalized;
    }

    template < class T >
    const inline Vector3< T > operator + ( const Vector3< T >& v1, const Vector3< T >& v2 )
    {
        Vector3< T > ret( v1 );
        ret += v2;
        return ret;
    }

    template < class T >
    const inline Vector3< T > operator - ( const Vector3< T >& v1, const Vector3< T >& v2 )
    {
        Vector3< T > ret( v1 );
        ret -= v2;
        return ret;
    }

    template < class T >
    const inline Vector3< T > operator * ( const Vector3< T >& v1, const Vector3< T >& v2 )
    {
        Vector3< T > ret( v1 );
        ret *= v2;
        return ret;
    }

    template < class T >
    const inline Vector3< T > operator ^ ( const Vector3< T >& v1, const Vector3< T >& v2 )
    {
        return v1.Cross( v2 );
    }

    template < class T >
    const inline Vector3< T > operator / ( const Vector3< T >& v1, const Vector3< T >& v2 )
    {
        Vector3< T > ret( v1 );
        ret /= v2;
        return ret;
    }

    template < class T >
    const inline Vector3<T> operator * ( const T& scalar, const Vector3<T>& v )
    {
        return v * scalar;
    }

    template < class T >
    inline Vector3<T> Min( const Vector3<T>& a, const Vector3<T>& b )
    {
        Vector3<T> res( a );
        if( res.m_x > b.m_x )   res.m_x = b.m_x;
        if( res.m_y > b.m_y )   res.m_y = b.m_y;
        if( res.m_z > b.m_z )   res.m_z = b.m_z;
        return res;
    }

    template < class T >
    inline Vector3<T> Max( const Vector3<T>& a, const Vector3<T>& b )
    {
        Vector3<T> res( a );
        if( res.m_x < b.m_x )   res.m_x = b.m_x;
        if( res.m_y < b.m_y )   res.m_y = b.m_y;
        if( res.m_z < b.m_z )   res.m_z = b.m_z;
        return res;
    }

} // namespace Math

} // namespace Jungle
// EOF

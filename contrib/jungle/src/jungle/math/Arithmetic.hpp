//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Arithmetic.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_ARITHMETIC_HPP__
#define __INCLUDED__MATH_ARITHMETIC_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    template < class T >
    inline T            Mul( T a, T b );

    template < class T >
    inline T            Div( T a, T b );

    template < class T >
    inline T            Div2( T a );

    template < class T >
    inline T            Min( T a, T b );

    template < class T >
    inline T            Max( T a, T b );

    template < class T >
    inline T            MinMax( T val, T min, T max );

    template < class T >
    inline T            Abs( T a );

    //! Find the division reminder
    template < class T >
    inline T            Mod( T base, T divisor );

    //! Find the smallest integer value greater or equal to argument.
    template < class T >
    inline T            Ceil( T a );

    //! Find the largest integer number less then or equal to argument.
    template < class T >
    inline T            Floor( T a );

    template < class T >
    inline T            Clamp( T val, T low, T high );

    //! Check values equivalence taking NumberTraits::Epsilon() as the maximum variance allowed between values.
    template < class T >
    inline bool         Equal( T val0, T val1 );

    //! Check equivalence using precision value as an maximum allowable variation.
    template < class T >
    inline bool         Equal( T val0, T val1, T precision );

    //! Compare two values.
    /*!
    * Firstly check values equivalence with NumberTraits::Epsilon() given as the maximum allowable variance.
    * If values are not equal in terms of Equal function then return 1 if val0 > val1 or -1 otherwise.
    */
    template < class T >
    inline int          Compare( T val0, T val1 );

    //! Compare two values with a given precision level.
    /*!
    * Same as Compare function, but use predefined precision level for checking equality.
    * If values differs more then given precision return 1 if val0 > val1 or -1 otherwise.
    */
    template < class T >
    inline int          Compare( T val0, T val1, T precision );

    template < class T >
    inline T            Scale( T val, T lowIn, T highIn, T lowOut, T highOut );

    template < class T >
    inline T            BiasAndScale( const T& val, const T& bias, const T& scale );

    template < class T >
    inline T            ScaleAndBias( const T& val, const T& scale, const T& bias );

    template < class TReal >
    inline TReal        Sign( TReal fValue );

    template < class TReal >
    inline TReal        Sqr( TReal val );

    template < class TReal >
    inline TReal        Sqrt( TReal val );

    template < class TReal >
    inline TReal        FastSqrt( TReal val );

    template < class TReal >
    inline TReal        InvSqrt( TReal val );

    template < class TReal >
    inline TReal        FastInvSqrt( TReal val );

} // namespace Math

} // namespace Jungle

#include <jungle/math/Arithmetic.inl>

#endif // !defined __INCLUDED__MATH_ARITHMETIC_HPP__
// EOF

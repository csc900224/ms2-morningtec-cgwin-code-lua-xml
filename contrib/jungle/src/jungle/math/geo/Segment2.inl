////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Segment2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes

namespace Jungle
{
namespace Math
{
    template< class TReal, class TVector >
    inline void Segment2< TReal, TVector >::SetStartXY(TReal x, TReal y)
    {
        SetStart( Vector2( x, y ) );
    }

    template< class TReal, class TVector >
    inline void Segment2< TReal, TVector >::SetEndXY(TReal x, TReal y)
    {
        SetEnd( Vector2( x, y ) );
    }

    template< class TReal, class TVector >
    inline void Segment2< TReal, TVector >::SetDirXY(TReal x, TReal y)
    {
        SetDir( Vector2( x, y ) );
    }

} // namespace Math

} // namespace Jungle

// EOF

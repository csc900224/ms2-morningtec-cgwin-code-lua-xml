////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/AABB.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/NumberTraits.hpp>
#include <jungle/math/geo/CoordSystem.hpp>

#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Math
{
    template < class TReal, class TVector >
    AABB< TReal, TVector >::AABB()
    {
        SetPoint( TVector( NumberTraits< TReal >::ZERO ) );
    }

    template < class TReal, class TVector >
    AABB< TReal, TVector >::AABB(const AABB* pAABB)
#ifdef AABB_USE_MIN_MAX
        : m_min( pAABB->m_min )
        , m_max( pAABB->m_max )
#else
        : m_center( pAABB->rCenter )
        , m_extents( pAABB->rExtents )
#endif
    {
    }

    template < class TReal, class TVector >
    AABB< TReal, TVector >::AABB(const TVector& rCenter, const TVector& rExtents)
#ifdef AABB_USE_MIN_MAX
        : m_min( rCenter - rExtents )
        , m_max( rCenter + rExtents )
#else
        : m_center( rCenter )
        , m_extents( rExtents )
#endif
    {
    }

    template < class TReal, class TVector >
    AABB< TReal, TVector >::AABB(const TVector& rExtents)
    {
        SetCenter( TVector( NumberTraits< TReal >::ZERO ) );
        SetExtents( rExtents );
    }

    template < class TReal, class TVector >
    void AABB< TReal, TVector >::SetEmpty()
    {
#ifdef AABB_USE_MIN_MAX
        m_min = TVector( NumberTraits< TReal >::Maximum() );
        m_max = TVector( NumberTraits< TReal >::Minimum() );
#else
        m_center = TVector( NumberTraits< TReal >::ZERO );
        m_extents = TVector( NumberTraits< TReal >::Minimum() );
#endif
    }

    template < class TReal, class TVector >
    bool AABB< TReal, TVector >::IsEmpty() const
    {
        return GetMax() == TVector( NumberTraits< TReal >::Minimum() );
    }

    template < class TReal, class TVector >
    void AABB< TReal, TVector >::SetPoint(const TVector& rPos )
    {
#ifdef AABB_USE_MIN_MAX
        m_min = rPos;
        m_max = rPos;
#else
        m_center = rPos;
        m_extents = TVector( NumberTraits< Real >::ZERO );
#endif
    }

    template < class TReal, class TVector >
    bool AABB< TReal, TVector >::IsPoint() const
    {
        return GetMin() == GetMax();
    }

#ifdef AABB_USE_MIN_MAX
    template < class TReal, class TVector >
    inline TVector AABB< TReal, TVector >::GetCenter() const
    {
        return (m_min + m_max) * NumberTraits< TReal >::Half();
    }
#else
    template < class TReal, class TVector >
    inline const TVector& AABB< TReal, TVector >::GetCenter() const
    {
        return m_center;
    }
#endif

#ifdef AABB_USE_MIN_MAX
    template < class TReal, class TVector >
    inline TVector AABB< TReal, TVector >::GetExtents() const
    {
        return (m_max - m_min) * NumberTraits< TReal >::Half();
    }
#else
    template < class TReal, class TVector >
    inline const TVector& AABB< TReal, TVector >::GetExtents() const
    {
        return m_extents;
    }
#endif

#ifdef AABB_USE_MIN_MAX
    template < class TReal, class TVector >
    inline const TVector& AABB< TReal, TVector >::GetMin() const
    {
        return m_min;
    }
#else
    template < class TReal, class TVector >
    inline TVector AABB< TReal, TVector >::GetMin() const
    {
        return (m_center - m_extents);
    }
#endif

#ifdef AABB_USE_MIN_MAX
    template < class TReal, class TVector >
    inline const TVector& AABB< TReal, TVector >::GetMax() const
    {
        return m_max;
    }
#else
    template < class TReal, class TVector >
    inline TVector AABB< TReal, TVector >::GetMax() const
    {
        return (m_center + m_extents);
    }
#endif

    template < class TReal, class TVector >
    const TVector& AABB< TReal, TVector >::GetAxis(unsigned char axis) const
    {
        JUNGLE_ASSERT_MSG( axis < AXIS_NUM, "Axis index out of allowable range, specify index below AXIS_NUM.\n" );

        return CoordSystem< TReal, TVector >::GetUnitAxis(axis);
    }

    template < class TReal, class TVector >
    void AABB< TReal, TVector >::SetMinMax(const TVector& rMin, const TVector& rMax)
    {
#ifdef AABB_USE_MIN_MAX
        m_min = rMin;
        m_max = rMax;
#else
        m_center = ( rMax + rMin ) * NumberTraits< TReal >::Half();
        m_extents = ( rMax - rMin ) * NumberTraits< TReal >::Half();
#endif
    }

    template < class TReal, class TVector >
    void AABB< TReal, TVector >::SetCenter(const TVector& rCenter)
    {
#ifdef AABB_USE_MIN_MAX
        const TVector& extents( GetExtents() );

        m_min = rCenter - extents;
        m_max = rCenter + extents;
#else
        m_center = rCenter;
#endif
    }

    template < class TReal, class TVector >
    void AABB< TReal, TVector >::SetExtents(const TVector& rExtents)
    {
#ifdef AABB_USE_MIN_MAX
        const TVector& center( GetCenter() );

        m_min = center - rExtents;
        m_max = center + rExtents;
#else
        m_extents = rExtents;
#endif
    }

    template < class TReal, class TVector >
    void AABB< TReal, TVector >::SetCenterExtents(const TVector& rCenter, const TVector& rExtents)
    {
#ifdef AABB_USE_MIN_MAX
        m_min = rCenter - rExtents;
        m_max = rCenter + rExtents;
#else
        m_center = rCenter;
        m_extents = rExtents;
#endif
    }

} // namespace Math

} // namespace Jungle
// EOF

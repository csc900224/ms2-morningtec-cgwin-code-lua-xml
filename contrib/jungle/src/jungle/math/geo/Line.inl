////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Line.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////


// Internal includes
#include <jungle/math/geo/Sphere.hpp>
#include <jungle/math/geo/AABB.hpp>

#include <jungle/math/Arithmetic.hpp>

#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>

// External includes

namespace Jungle
{
namespace Math
{
    template< class TReal, class TVector >
    inline const TVector& Line< TReal, TVector >::GetOrigin() const
    {
        return m_origin;
    }

    template< class TReal, class TVector >
    inline const TVector& Line< TReal, TVector >::GetDir() const
    {
        return m_dir;
    }

    template< class TReal, class TVector >
    inline void Line< TReal, TVector >::SetOrigin(const TVector& rOrigin)
    {
        m_origin = rOrigin;
    }

    template< class TReal, class TVector >
    inline void Line< TReal, TVector >::SetDir(const TVector& rDirection)
    {
        m_dir = rDirection;
    }

    // Based on: 3D Game Engine Architecture: Engineering Real-Time Applications with Wild Magic
    template< class TReal, class TVector >
    bool Line< TReal, TVector >::Intersect( const Vector& origin, const Vector& dir, const Sphere* pSphere, unsigned int& pointsNum, Real pointsDist[2] )
    {
        // Lets use line and circle equations:
        // P+t*Dir (line)
        // R^2 = |x-Center|^2 (circle)
        // Assuming the line direction (Dir vecor) is normalized we can derive quadric equation for root t
        // which is our distance on the line to the line-circle instersection:
        //   | X - C |^2 - R^2 = 0, where X is any point on the line, thus : X = (t*Dir+P), where P is the line origin
        //   | ( t * Dir+P ) - C |^2 - R^2 = 0
        //   t^2 + 2*Dot(Dir,P-C)*t + |P-C|^2-R^2 = 0
        //   t^2 + 2*a1*t + a0 = 0

        // Solve equation for two roots, the order is T[0] < T[1].
        Vector vOff = origin - pSphere->GetCenter();
        Real a0 = vOff.LengthSqr() - Sqr( pSphere->GetRadius() );
        Real a1 = dir.Dot( vOff );
        Real delta = Mul(a1, a1) - a0;      // Calculate delta
        // Check Delta > 0 (two roots)
        if (delta > NumberTraits< Real >::ZERO ) // > EPSILON
        {
            pointsNum = 2;
            delta = Sqrt(delta);
            pointsDist[0] = -a1 - delta;
            pointsDist[1] = -a1 + delta;
        }
        // Delta < 0 (no solution)
        else if (delta < 0 ) // < -EPSILON
        {
            pointsNum = 0;
        }
        // Delta == 0 (one root)
        else
        {
            pointsNum = 1;
            pointsDist[0] = -a1;
        }
        return pointsNum != 0;
    }

    template< class TReal, class TVector >
    bool Line< TReal, TVector >::Intersect( const Vector& origin, const Vector& dir, const Sphere* pSphere, Real tMin, Real tMax, unsigned int& pointsNum, Real pointsDist[2] )
    {
        // Solve equation for two roots, the order is T[0] < T[1].
        Vector vOff = origin - pSphere->GetCenter();
        Real a0 = vOff.LengthSqr() - Sqr( pSphere->GetRadius() );
        Real a1 = dir.Dot( vOff );
        Real delta = Mul(a1, a1) - a0;      // Calculate delta
        // Check Delta > 0 (two roots)
        if (delta > NumberTraits< Real >::ZERO_TOLERANCE) // > EPSILON
        {
            pointsNum = 2;
            delta = Sqrt(delta);
            pointsDist[0] = -a1 - delta;
            pointsDist[1] = -a1 + delta;

            // Reduce number of solution to segment (ray) range
            if( pointsDist[0] > tMax || pointsDist[1] < tMin )
            {
                pointsNum = 0;
            }
            else
            {
                if( pointsDist[1] <= tMax )
                {
                    if( pointsDist[0] < tMin )
                    {
                        pointsNum = 1;
                        pointsDist[0] = pointsDist[1];
                    }
                }
                else if( pointsDist[0] >= tMin )
                {
                    pointsNum = 1;
                }
                else
                {
                    pointsNum = 0;
                }
            }
        }
        // Delta < 0 (no solution)
        else if (delta < -NumberTraits< Real >::ZERO_TOLERANCE ) // < -EPSILON
        {
            pointsNum = 0;
        }
        // Delta == 0 (one root)
        else
        {
            pointsNum = 1;
            pointsDist[0] = -a1;

            // Reduce number of solutions if point is outside of segment/ray range
            if( pointsDist[0] > tMax )
            {
                pointsNum = 0;
            }
        }
        return pointsNum != 0;
    }

    template< class TReal, class TVector >
    bool Line< TReal, TVector >::Intersect( const Vector& origin, const Vector& dir, const AABB* pAABB, Real tMin, Real tMax, unsigned int& pointsNum, Real pointsDist[2], Vector pointsNorm[2] /* = NULL */ )
    {
        const bool bSolid = false;

        // Smiths' / Slabs method
        Real& Tnear = pointsDist[0];
        Real& Tfar = pointsDist[1];

        const Vector& slabL = pAABB->GetMin();
        const Vector& slabH = pAABB->GetMax();

        Real T0, T1;

        Tnear = tMin;   // for ray should be 0, while for line -infinity
        Tfar = tMax;    // should be +infinity both for line and ray collisions
        pointsNum = 0;

        // Here assume that vector may contain only coordinates components
        const int axesNum = sizeof( Vector ) / sizeof( Real );
        for( int i = 0; i < axesNum; ++i )
        {
            // Line is parallel to i-plane ( nth axis )
            if( dir[i] == NumberTraits< Real >::ZERO )
            {
                // If origin Xi is not between the slabs ( Xo < Xl or Xo > Xh) then return false
                if( origin[i] < slabL[i] || origin[i] > slabH[i] )
                {
                    return false;
                }
                // Origin between slabls its position will be derived in the next step
                // during opposite slab distance calculus.
                else
                {
                    if( pointsNorm )
                    {
                        const int oppositeAxis = (i + 1) % axesNum;
                        // setup both axis since one of them may be ommited in segment intersection calculus
                        pointsNorm[0] = dir[oppositeAxis] > 0 ? -pAABB->GetAxis( oppositeAxis ) : pAABB->GetAxis( oppositeAxis );
                        pointsNorm[1] = -pointsNorm[0];
                    }
                    continue;
                }
            }
            else
            {
                // Compute intersection DISTANCE of the slabs' planes along line direction
                // T0 = (Xl - Xo) / Xd
                // T1 = (Xh - Xo) / Xd
                Real dirInv = Div( NumberTraits< Real >::ONE, dir[i] );
                T0 = Mul( (slabL[i] - origin[i] ), dirInv );
                T1 = Mul( (slabH[i] - origin[i] ), dirInv );

                if( T0 > T1 ) std::swap( T0, T1 );  // T0 must be intersection with near plane
                if( T0 > Tnear )
                {
                    Tnear = T0;        // want largest Tnear
                    if( pointsNorm )    pointsNorm[0] = origin[i] < slabL[i] ? -pAABB->GetAxis( i ) : pAABB->GetAxis( i );
                }
                if( T1 < Tfar )
                {
                    Tfar= T1;           // want smallest Tfar
                    if( pointsNorm )    pointsNorm[1] = origin[i] < slabH[i] ? -pAABB->GetAxis( i ) : pAABB->GetAxis( i );
                }
                if( Tnear > Tfar ) return false;    // box is missed
                //if( Tfar < 0 ) return false;      // box is behind ray
                if( Tfar < tMin ) return false;     // box is behind segment/ray, Tfar < 0 specifies less generic ray intersection condition
                if( Tnear > tMax ) return false;    // generic condition for segment intersection
            }
        }
        // If AABB survived all tests, there must be intersection point at Tnear and exit point Tfar.
        // Check tMin/tMax boundaries
        pointsNum = 0;
        // Tnear bigger then min or in the segment start in case of inside volume collisions
        if( Tnear > tMin || (bSolid && Tnear == tMin) )
        {
            ++pointsNum;
        }
        // Check if before tMax and if both intersections aren't in the same point (corner)
        if( Tfar != Tnear && (Tfar < tMax || (bSolid && Tfar == tMax) ) )
        {
            if( pointsNum == 0 )
            {
                pointsDist[pointsNum] = pointsDist[pointsNum+1];
                if( pointsNorm )        pointsNorm[pointsNum] = pointsNorm[pointsNum+1];
            }
            ++pointsNum;
        }
        return pointsNum > 0;
    }

    template< class TReal, class TVector >
    bool Line< TReal, TVector >::Intersect( const Vector& origin, const Vector& dir, const OBB* pOBB, Real tMin, Real tMax, unsigned int& pointsNum, Real pointsDist[2], Vector pointsNorm[2] /* = NULL */)
    {
        JUNGLE_ASSERT( tMin < tMax);

        const bool bSolid = false;

        // convert linear component to box coordinates
        const Vector vOff = origin - pOBB->GetCenter();
        const Vector vOBBAxes[] = { pOBB->GetAxis(0), pOBB->GetAxis(1) };

        Vector projOff( vOff.Dot( vOBBAxes[0] ), vOff.Dot( vOBBAxes[1] ) );
        Vector projDir( dir.Dot( vOBBAxes[0] ), dir.Dot( vOBBAxes[1] ) );

        Real tMinOld = tMin, tMaxOld = tMax;
        bool bNotAllClipped = true;
        bool minMax[2] = { false, false };
        unsigned int axis = 0;
        unsigned int axisNum = sizeof( Vector ) / sizeof( origin[0] );
        while( bNotAllClipped && axis < axisNum )
        {
            if( pointsNorm )
            {
                minMax[0] = minMax[1] = false;
                bNotAllClipped = Clip( +projDir[axis], -projOff[axis] - pOBB->GetExtentsSize()[axis], tMin, tMax, minMax );

                if( bNotAllClipped )
                {
                    if( minMax[0] ) pointsNorm[0] = -vOBBAxes[axis];
                    if( minMax[1] ) pointsNorm[1] = -vOBBAxes[axis];
                }

                minMax[0] = minMax[1] = false;
                bNotAllClipped = bNotAllClipped && Clip( -projDir[axis], +projOff[axis] - pOBB->GetExtentsSize()[axis], tMin, tMax, minMax );

                if( bNotAllClipped )
                {
                    if( minMax[0] ) pointsNorm[0] = vOBBAxes[axis];
                    if( minMax[1] ) pointsNorm[1] = vOBBAxes[axis];
                }
            }
            else
            {
                bNotAllClipped =    Clip( +projDir[axis], -projOff[axis] - pOBB->GetExtentsSize()[axis], tMin, tMax ) &&
                                    Clip( -projDir[axis], +projOff[axis] - pOBB->GetExtentsSize()[axis], tMin, tMax );
            }
            ++axis;
        }

        // Store the results in output arguments
        bool bBorderIntersect = (tMin != tMinOld) || (tMax != tMaxOld);
        pointsNum = 0;
        if (bNotAllClipped && (bSolid || bBorderIntersect) )
        {
            if( !bSolid )
            {
                if( tMin != tMinOld )
                {
                    pointsDist[0] = tMin;
                    // pointsNorm[0] should be already calculated - see above
                    ++pointsNum;
                }
                if( tMax != tMaxOld )
                {
                    pointsDist[pointsNum] = tMax;
                    if( pointsNorm && pointsNum == 0)   pointsNorm[pointsNum] = pointsNorm[pointsNum+1];
                    ++pointsNum;
                }
            }
            else    // solid intersections inside the volume
            {
                ++pointsNum;
                pointsDist[0] = tMin;
                // Solid "intersection" inside the volume - assign reversed direction normal
                if( pointsNorm && tMin == tMinOld )     pointsNorm[0] = -dir;

                if (tMax > tMin)
                {
                    ++pointsNum;
                    pointsDist[1] = tMax;
                    if( pointsNorm && tMax == tMaxOld ) pointsNorm[1] = -dir;
                }
            }
        }
        return pointsNum > 0;
    }

    template< class TReal, class TVector >
    int Line< TReal, TVector >::Clip( const Vector& origin, const Vector& dir, const Sphere* pSphere, Real& tMin, Real& tMax )
    {
        JUNGLE_ASSERT( tMin < tMax);
        JUNGLE_ASSERT_MSG( false, "Method not implemented and not tested yet." );

        // TODO: CHECK IT !!!!!
        // TODO: Fix line clipping when origin outside the sphere
        // convert linear component to box coordinates
        const Vector vOff = origin - pSphere->GetCenter();
        Real projLen = vOff.Dot( dir );
        Vector projOff( dir.x * projLen, dir.y * projLen );

        Real tMinOld = tMin, tMaxOld = tMax;

        bool bNotAllClipped = true;
        unsigned int axis = 0;
        unsigned int axisNum = sizeof( Vector ) / sizeof( origin[0] );
        while( bNotAllClipped && axis < axisNum )
        {
            bNotAllClipped =    Line::Clip( +dir[axis], -projOff[axis] - pSphere->GetRadius(), tMin, tMax ) &&
                                Line::Clip( -dir[axis], +projOff[axis] - pSphere->GetRadius(), tMin, tMax );
            ++axis;
        }

        if( bNotAllClipped && (tMin != tMinOld || tMax != tMaxOld) )
        {
            if (tMax > tMin)
            {
                // Both points clipped
                return 2;
            }
            else
            {
                // One point clipped tMin
                return 1;
            }
        }
        return 0;
    }

    template< class TReal, class TVector >
    int Line< TReal, TVector >::Clip( const Vector& origin, const Vector& dir, const AABB* pAABB, Real& tMin, Real& tMax )
    {
        // TODO: Fix line clipping when origin outside the box
        JUNGLE_ASSERT( tMin < tMax);

        const Vector vOff = origin - pAABB->GetCenter();
        Real tMinOld = tMin, tMaxOld = tMax;

        bool bNotAllClipped = true;
        unsigned int axis = 0;
        unsigned int axisNum = sizeof( Vector ) / sizeof( origin[0] );
        while( bNotAllClipped && axis < axisNum )
        {
            bNotAllClipped =    Clip( +dir[axis], -vOff[axis] - pAABB->GetExtents()[axis], tMin, tMax ) &&
                                Clip( -dir[axis], +vOff[axis] - pAABB->GetExtents()[axis], tMin, tMax );
            ++axis;
        }

        if (bNotAllClipped && (tMin != tMinOld || tMax != tMaxOld))
        {
            if (tMax > tMin)
            {
                // Both points clipped
                return 2;
            }
            else
            {
                // One point clipped tMin
                return 1;
            }
        }
        return 0;
    }

    template< class TReal, class TVector >
    int Line< TReal, TVector >::Clip( const Vector& origin, const Vector& dir, const OBB* pOBB, Real& tMin, Real& tMax )
    {
        JUNGLE_ASSERT( tMin < tMax);

        // TODO: Fix line clipping when origin outside the box
        // convert linear component to box coordinates
        const Vector vOff = origin - pOBB->GetCenter();
        const Vector vOBBAxes[] = { pOBB->GetAxis(0), pOBB->GetAxis(1) };

        Vector projOff( vOff.Dot( vOBBAxes[0] ), vOff.Dot( vOBBAxes[1] ) );
        Vector projDir( dir.Dot( vOBBAxes[0] ), dir.Dot( vOBBAxes[1] ) );

        Real tMinOld = tMin, tMaxOld = tMax;

        bool bNotAllClipped = true;
        unsigned int axis = 0;
        unsigned int axisNum = sizeof( Vector ) / sizeof( origin[0] );
        while( bNotAllClipped && axis < axisNum )
        {
            bNotAllClipped =    Clip( +projDir[axis], -projOff[axis] - pOBB->GetExtentsSize()[axis], tMin, tMax ) &&
                                Clip( -projDir[axis], +projOff[axis] - pOBB->GetExtentsSize()[axis], tMin, tMax );
            ++axis;
        }

        if( bNotAllClipped && (tMin != tMinOld || tMax != tMaxOld) )
        {
            if (tMax > tMin)
            {
                // Both points clipped
                return 2;
            }
            else
            {
                // One point clipped tMin
                return 1;
            }
        }
        return 0;
    }

    template< class TReal, class TVector >
    bool Line< TReal, TVector >::Clip( Real denom, Real numer, Real& rTMin, Real& rTMax )
    {
        // Return value is 'true' if line segment intersects the current test
        // plane.  Otherwise 'false' is returned in which case the line segment
        // is entirely clipped.

        if( denom > NumberTraits< Real >::ZERO )
        {
            if( numer > Mul( denom, rTMax) )
            {
                return false;
            }
            if( numer > Mul( denom, rTMin) )
            {
                // Clip minimum
                rTMin = Div( numer, denom );
            }
            return true;
        }
        else if( denom < NumberTraits< Real >::ZERO )
        {
            if( numer > Mul( denom, rTMin) )
            {
                return false;
            }
            if( numer > Mul( denom, rTMax) )
            {
                // Clip maximum
                rTMax = Div( numer, denom );
            }
            return true;
        }
        else
        {
            return numer <= NumberTraits< Real >::ZERO;
        }
    }

    template< class TReal, class TVector >
    bool Line< TReal, TVector >::Clip( Real denom, Real numer, Real& rTMin, Real& rTMax, bool minMax[2] )
    {
        // Extended version of above method, simply add information about clipped ranges.
        if( denom > NumberTraits< Real >::ZERO )
        {
            if( numer > Mul( denom, rTMax) )
            {
                return false;
            }
            if( numer > Mul( denom, rTMin) )
            {
                rTMin = Div( numer, denom );
                minMax[0] = true;
                minMax[1] = false;
            }
            return true;
        }
        else if( denom < NumberTraits< Real >::ZERO )
        {
            if( numer > Mul( denom, rTMin) )
            {
                return false;
            }
            if( numer > Mul( denom, rTMax) )
            {
                rTMax = Div( numer, denom );
                minMax[0] = false;
                minMax[1] = true;
            }
            return true;
        }
        else
        {
            return numer <= NumberTraits< Real >::ZERO;
        }
    }

} // namespace Math

} // namespace Jungle
// EOF

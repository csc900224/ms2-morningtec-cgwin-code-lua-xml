////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/OBB.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__OBB_HPP__
#define __MATH__GEO__OBB_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    //! Generic oriented box class template, handles only methods allowable both for 2d and 3d space.
    /*!
    * \note TVector template class should define basic arithmetics operators (+, -, operator*( TReal ) ),
    * comparison operator == as also should define contructor based on one input parameter TVector( TReal ),
    * filling all vector coordinates with the same value.
    */
    template < class TReal, class TVector >
    class OBB
    {
    public:
        typedef TVector Vector;
        typedef TReal   Real;

        static const int AXIS_NUM = sizeof( TVector ) / sizeof( TReal );

        /*!
        * Default constructor, sets geometry position to (0, 0) and makes it size minima (point size)
        */
        inline          OBB();

        /*!
        * Coping constructor
        * \param pOBB pointer to another OBB
        */
        inline          OBB(const OBB* pOBB);

        /*!
        * Create centered, axis aligned geomatry based on the extents size.
        * \param size of the box in each dimensions.
        */
        explicit inline OBB(const Vector& extentsSize);

        /*!
        * Create box based on the center position and its extents sizes in neutral orientation (axis aligned).
        * \param center box center
        * \param extentsSize box length in each cordinates stored in vector
        */
        inline          OBB(const Vector& center, const Vector& extentsSize);

        /*!
        * Create oriented box based on the major/minor axis, center point and extents lenght.
        * \param center geometry center
        * \param extentsSize extents length stored in vector
        * \param axes x,y,... normalized local orientation axes - vectors table
        */
        inline          OBB(const Vector& center, const Vector& extentsSize, const Vector* axes);

        /*!
        * Sets bounding volume to infinitelly small, reset rotations to world coordinate system orientation.
        */
        inline void     SetEmpty();

        /*!
        * Check if volume/area is totally empty and can not contain anything
        * Scaling and rotating shape doesn't affect it's empty status.
        */
        inline bool     IsEmpty() const;

        /*!
        * Set bounding geometry to contain single point with coordinates passed to method
        * \param rPos   new bounding area coordinates center
        */
        inline void     SetPoint(const Vector& pos );

        /*!
        * Check if bounding area forms a single point volume (very small).
        */
        inline bool     IsPoint() const;

        /*!
        * Move box center.
        * \param pos new center position.
        */
        inline void     SetCenter(const Vector& pos);

        /*!
        * Change box size in local coordinates (not rotated).
        * \param extentSize new size of the box extents stored in vector.
        */
        inline void     SetExtentsSize(const Vector& extentSize);

        /*!
        * Setup box based on the major/minor axis (orientation), center point and extents lenght.
        * \param center rectangle center
        * \param extent lenght in each dimensions (after rotation) stored in vector
        * \param axes pointer to the table with box orientation axes (X, Y, ... )
        */
        inline void     SetCenterExtentsAndAxes(const Vector& center, const Vector& extent, const Vector* axes);

        /*!
        * Set minimum and maximum points in the bounding volume.
        * \param rMin   minimum point still in bounding area.
        * \param rMax   maximum point that may be contained inside volume.
        */
        inline void     SetMinMax(const Vector& rMin, const Vector& rMax);

        //! Return box center.
        inline
        const Vector&   GetCenter() const;

        //! Return box dimensions stored in the vector coordinates.
        inline
        const Vector&   GetExtentsSize() const;

        //! Get one of the axes vector.
        /*!
        * Returns minor or major axis vector with appropriate rotation aplied to it.
        * \param 0, 1 or 2 (for 3d) to specify X or Y, Z axis respectivelly.
        */
        inline
        const Vector&   GetAxis( unsigned char axis ) const;

        //! Get smallest coordinates that may be overlaped on every axis.
        /*!
        * This gives us closest overlapping AABB parameters.
        */
        inline Vector   GetMin() const;

        //! Get biggest coordinates that can be covered on every axis.
        /*!
        * This gives us closest overlapping AABB parameters.
        */
        inline Vector   GetMax() const;

        //! Return axis aligned box (AABB) extent based on this oriented box.
        /*
        * This extent defines maxium (always positive) area that oriented box may overlap, think about it like
        * radius of the cicle which fully overlaps current rectangle.
        */
        inline Vector   GetAABBExtent() const;

        //! Turns OBB into AABB with the same dimensions.
        inline void     ResetRotation();

        //! Calculate the closest path point distance from the point to box boundaries.
        inline Real     DistanceSqr( const Vector* point ) const;

        //! Check if point lays inside the bounding volume.
        inline bool     IsInside( const Vector* point ) const;

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for OBB *= float. Scales the extents, keeps same center.
        inline OBB&     operator*=(Real s)
        {
            SetExtentsSize( GetExtentsSize() * s );
            return *this;
        }

        //! Operator for OBB /= float. Scales the extents, keeps same center.
        inline OBB&     operator/=(Real s)
        {
            SetExtentsSize( GetExtentsSize() / s );
            return *this;
        }

        //! Operator for OBB += rTrans. Translates the rectangle keeping the extents.
        inline OBB&     operator+=(const Vector& rTrans)
        {
            SetCenter( GetCenter() + rTrans );
            return *this;
        }

        //! Operator for OBB -= rTrans. Translates the rectangle keeping it's extents.
        inline OBB&     operator-=(const Vector& rTrans)
        {
            SetCenter( GetCenter() - rTrans );
            return *this;
        }

    public:
        Vector          m_center;               //!<- Box geometric and rotation center
        Vector          m_extents;              //!<- Object space box extents
        Vector          m_axes[AXIS_NUM];       //!<- Oriented box major/minor axes (X, Y, ...)

    }; // class OBB

} // namespace Math

} // namespace Jungle


#include <jungle/math/geo/OBB.inl>

#endif // !defined __MATH__GEO__OBB_HPP__
// EOF

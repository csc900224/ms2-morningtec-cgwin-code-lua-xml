////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/CoordSystem.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__COORD_SYSTEM_HPP__
#define __MATH__GEO__COORD_SYSTEM_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    //! Coordinate system template class, may be used with 2d and 3d space vectors.
    template < class TReal, class TVector, int Dim = sizeof( TVector ) / sizeof( TReal ) >
    class CoordSystem
    {
    public:
        static const TVector    UNIT_X;                 //!<- Helper vector defining unit length X axis
        static const TVector    UNIT_Y;                 //!<- Helper vector defining unit length Y axis
        static const TVector    UNIT_Z;                 //!<- Helper vector defining unit length Z axis
        static const TVector    ORIGIN;                 //!<- World origin point

        static const int        AXIS_NUM = Dim;

        static const TVector&   GetUnitAxis( int i );

    }; // class CoordSystem

    // CoordSystem class specialization for 2d space
    template < class TReal, class TVector >
    class CoordSystem< TReal, TVector, 2 >
    {
    public:
        static const TVector    UNIT_X;
        static const TVector    UNIT_Y;
        static const TVector    ORIGIN;

        static const int        AXIS_NUM = 2;

        static const TVector&   GetUnitAxis( int i );

    }; // class CoordSystem

    // CoordSystem specialization for 3 dimensional space
    template < class TReal, class TVector >
    class CoordSystem< TReal, TVector, 3 >
    {
    public:
        static const TVector    UNIT_X;
        static const TVector    UNIT_Y;
        static const TVector    UNIT_Z;
        static const TVector    ORIGIN;

        static const int        AXIS_NUM = 3;

        static const TVector&   GetUnitAxis( int i );

    }; // class CoordSystem

    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 2 >::UNIT_X( NumberTraits< TReal >::ONE, NumberTraits< TReal >::ZERO );
    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 2 >::UNIT_Y( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ONE );
    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 2 >::ORIGIN( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO );

    template < class TReal, class TVector >
    const TVector& CoordSystem< TReal, TVector, 2 >::GetUnitAxis( int i )
    {
        JUNGLE_ASSERT( i < AXIS_NUM );
        static const TVector UNIT_AXIS[2] =
        {
            TVector( NumberTraits< TReal >::ONE, NumberTraits< TReal >::ZERO ),
            TVector( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ONE )
        };
        return UNIT_AXIS[ i ];
    }

    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 3 >::UNIT_X( NumberTraits< TReal >::ONE, NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO );
    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 3 >::UNIT_Y( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ONE, NumberTraits< TReal >::ZERO );
    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 3 >::UNIT_Z( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ONE );
    template < class TReal, class TVector >
    const TVector CoordSystem< TReal, TVector, 3 >::ORIGIN( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO );

    template < class TReal, class TVector >
    const TVector& CoordSystem< TReal, TVector, 3 >::GetUnitAxis( int i )
    {
        JUNGLE_ASSERT( i < AXIS_NUM );
        static const TVector UNIT_AXIS[3] =
        {
            TVector( NumberTraits< TReal >::ONE, NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO ),
            TVector( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ONE, NumberTraits< TReal >::ZERO ),
            TVector( NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ZERO, NumberTraits< TReal >::ONE )
        };
        return UNIT_AXIS[ i ];
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __MATH__GEO__COORD_SYSTEM_HPP__
// EOF

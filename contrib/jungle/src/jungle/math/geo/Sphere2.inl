////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Sphere2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/NumberTraits.hpp>
#include <jungle/base/Errors.hpp>
#include <jungle/math/Arithmetic.hpp>       // Math::Sqr

namespace Jungle
{
namespace Math
{
    template < class TReal, class TVector >
    bool Sphere2< TReal, TVector >::Intersect(const Sphere2* pSphere2) const
    {
        TReal distSqr = Math::Sqr( base::GetCenter().x - pSphere2->GetCenter().x ) +
                        Math::Sqr( base::GetCenter().y - pSphere2->GetCenter().y );
        TReal radiusSqr = Math::Sqr( base::GetRadius() + pSphere2->GetRadius() );

        return ( distSqr < radiusSqr );
    }

    template < class TReal, class TVector >
    bool Sphere2< TReal, TVector >::Overlaps(const Sphere2* pSphere2) const
    {
        // TODO: To be tested
        if( base::GetRadius() < pSphere2->GetRadius() )
            return false;

        TReal distSqr = Math::Sqr( base::GetCenter().x - pSphere2->GetCenter().x ) +
                        Math::Sqr( base::GetCenter().y - pSphere2->GetCenter().y );

        return ( distSqr < Math::Sqr( base::GetRadius() -  pSphere2->GetRadius() ) );
    }

    template < class TReal, class TVector >
    bool Sphere2< TReal, TVector >::Overlaps(const TVector* pPoint) const
    {
        TReal diffSqr = Math::Sqr( base::GetCenter().x - pPoint->x ) +
                        Math::Sqr( base::GetCenter().y - pPoint->y );

        return ( diffSqr < Math::Sqr( base::GetRadius() ) );
    }

} // namespace Math

} // namespace Jungle

// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Ray2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__RAY2_HPP__
#define __MATH__GEO__RAY2_HPP__

// Internal includes
#include <jungle/math/geo/Ray.hpp>

// External includes

namespace Jungle
{
namespace Math
{
    // Forward declarations
    template < class TReal >
    class Vector2;

    template < class TReal, class TVector = Vector2< TReal > >
    class Ray2 : public Ray< TReal, TVector >
    {
        typedef Ray< TReal, TVector >   base;

    public:
        typedef TReal           Real;
        typedef TVector         Vector2;

        /*!
        * Default constructor, doesn't reset ray origin neigher direction
        */
        inline                  Ray2()
                                : base()
        {}

        /*!
        * Initializing constructor, sets ray direction and position
        * \param origin         ray start position
        * \param dir            ray direction
        */
        inline                  Ray2(const Vector2& rOrigin, const Vector2& rDir)
                                : base( rOrigin, rDir )
        {}

        /*!
        * Initializing constructor, setup ray direction and origin in the world center (0, 0) coordinates
        * \param dir            ray direction
        */
        explicit inline         Ray2(const Vector2& rDir)
                                : base( rDir )
        {}

        /*!
        * Initializing constructor, sets ray direction using x and y coordinates
        * \param x              ray x direction
        * \param y              y direction coordinate
        */
        inline                  Ray2(Real xDir, Real yDir)
                                : base( Vector2( xDir, yDir ) )
        {}

        /*!
        * Set ray starting point using x, y coordinates
        * \param x              ray start x coordinate
        * \param y              ray origin y value
        */
        inline void             SetOriginXY(Real x, Real y);

        /*!
        * Set ray x and y direction
        * \param x              ray x direction
        * \param y              ray y direction
        */
        inline void             SetDirXY(Real x, Real y);

    }; // class Ray2

} // namespace Scene

} // namespace Jungle

#include <jungle/math/geo/Ray2.inl>

#endif // !defined __MATH__GEO__RAY2_HPP__
// EOF

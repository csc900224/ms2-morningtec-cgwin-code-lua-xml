////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Line.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__LINE_HPP__
#define __MATH__GEO__LINE_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    // Forward declarations
    template < class TReal, class TVector >
    class AABB;
    template < class TReal, class TVector >
    class Sphere;
    template < class TReal, class TVector >
    class OBB;

    template < class TReal, class TVector >
    class Line
    {
    public:
        typedef TReal                       Real;
        typedef TVector                     Vector;
        typedef Jungle::Math::AABB< TReal, TVector > AABB;
        typedef Jungle::Math::Sphere< TReal, TVector > Sphere;
        typedef Jungle::Math::OBB< TReal, TVector > OBB;

        /*!
        * Default constructor, doesn't reset line origin neigher direction
        */
        inline              Line()
        {}

        /*!
        * Initializing constructor, sets line direction and origin (any point laying on the line)
        * \param origin     line point
        * \param dir        line normalized direction
        */
        inline              Line(const Vector& rOrigin, const Vector& rDir)
                            : m_origin(rOrigin)
                            , m_dir(rDir)
        {}

        /*!
        * Initializing constructor, setups line direction and passes it thru the world center (zero point)
        * After this construction line will have form o y = D * x
        * \param dir        ray direction
        */
        explicit inline     Line(const Vector& rDir)
                            : m_origin( NumberTraits< Real >::ZERO )
                            , m_dir(rDir)
        {}

        /*!
        * Get line origin point.
        * This gives any point that lays on the line path.
        * \return           ray origin
        */
        inline const Vector& GetOrigin() const;

        /*!
        * Get line direction vector
        * \return            line normalized direction
        */
        inline const Vector& GetDir() const;

        /*!
        * Setup point that lays on the line (origin)
        * \param             line origin
        */
        inline void          SetOrigin(const Vector& rOrigin);

        /*!
        * Set line direction
        * \param rDir       new line direction
        */
        inline void         SetDir(const Vector& rDir);

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for Line += rTrans. Translates the line origin while keeping its direction.
        inline Line&            operator+=(const Vector& rTrans)
        {
            SetOrigin( GetOrigin() + rTrans );
            return *this;
        }

        //! Operator for Line -= rTrans. Negative translation of the line origin.
        inline Line&            operator-=(const Vector& rTrans)
        {
            SetOrigin( GetOrigin() - rTrans );
            return *this;
        }


        //! Sphere - line intersection test.
        static bool         Intersect( const Vector& origin, const Vector& dir, const Sphere* pSphere, unsigned int& pointsNum, Real pointsDist[2] );

        //! Sphere limited line intersection test.
        /*
        * This is generic test for line limited with tMin and tMax parameters, that gives use possibility to test not
        * only infinite lines (tMin= -inf, tMax= +inf) but also Ray (0, +inf), and Segment (0, segLenght).
        */
        static bool         Intersect( const Vector& origin, const Vector& dir, const Sphere* pSphere, Real tMin, Real tMax, unsigned int& pointsNum, Real pointsDist[2] );

        //! AABB - Line parametrized intersection test.
        /*
        * \see See Line - Sphere intersection for detailed description of tMin and tMax parameters.
        */
        static bool         Intersect( const Vector& origin, const Vector& dir, const AABB* pAABB, Real tMin, Real tMax, unsigned int& pointsNum, Real pointsDist[2], Vector pointsNorm[2] = NULL );

        //! OBB - Line parametrized intersection algorithm.
        static bool         Intersect( const Vector& origin, const Vector& dir, const OBB* pOBB, Real tMin, Real tMax, unsigned int& pointsNum, Real pointsDist[2], Vector pointsNorm[2] = NULL );

        static int          Clip( const Vector& origin, const Vector& dir, const Sphere* pSphere, Real& tMin, Real& tMax );

        static int          Clip( const Vector& origin, const Vector& dir, const AABB* pAABB, Real& tMin, Real& tMax );

        static int          Clip( const Vector& origin, const Vector& dir, const OBB* pOBB, Real& tMin, Real& tMax );

    protected:
        //! Used only internally.
        static bool         Clip( Real denom, Real numer, Real& rTMin, Real& rTMax );

        //! Used internaly - clip segment in plane giving info about clipped ranges (minMax array).
        static bool         Clip( Real denom, Real numer, Real& rTMin, Real& rTMax, bool minMax[2] );

        //! Line origin - some point laying on the line.
        Vector              m_origin;
        //! Line direction - normalized direction.
        Vector              m_dir;

    }; // class Line

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/Line.inl>

#endif // !defined __MATH__GEO__LINE_HPP__
// EOF

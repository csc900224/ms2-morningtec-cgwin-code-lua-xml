////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/AABB2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__AABB2_HPP__
#define __MATH__GEO__AABB2_HPP__

// Internal includes
#include <jungle/math/geo/AABB.hpp>

namespace Jungle
{
namespace Math
{
    // Forward declare Vector2 template
    template < class TReal >
    class Vector2;

    //! Axis aligned bounding volume class template designed to use 2d space.
    /*!
    * This class extends functionality of AABB class allowing to check for volumes intersections, overlapping
    * and point containement.
    * \note Vector template parameter class should define basic arithmetics operators (+, -, operator*( TReal ) ),
    * as also comparison operator == and static helper vector TVector::ZERO.
    */
    template < class TReal, class TVector = Vector2< TReal > >
    class AABB2 : public AABB< TReal, TVector >
    {
        typedef AABB< TReal, TVector >  base;

    public:
        typedef TVector Vector;
        typedef TReal   Real;

        //! Constructor wrappers for base class contructors.
                        AABB2()
                            : base()
                        {}

                        AABB2(const AABB2* pAABB2)
                            : base( pAABB2 )
                        {}

                        AABB2(const TVector& rCenter, const TVector& rExtents)
                            : base( rCenter, rExtents )
                        {}

        explicit        AABB2(const TVector& rExtents)
                            : base( rExtents )
                        {}

        /*!
        * Intersection test with another axis aligned area.
        * \param pAABB2 Tested area
        */
        inline bool     Intersect(const AABB2* pAABB2) const;

        /*!
        * Containement test with another axis aligned area.
        * Checks if the passed volume may be overlapped by the current one.
        * \param pAABB2 Tested area pointer
        */
        inline bool     Overlaps(const AABB2* pAABB2) const;

        /*!
        * Check if point lays inside the bounding area
        * \param pPoint Tested point position
        */
        inline bool     Overlaps(const Vector* pPoint) const;

    }; // class AABB2

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/AABB2.inl>

#endif // !defined __MATH__GEO__AABB2_HPP__
// EOF

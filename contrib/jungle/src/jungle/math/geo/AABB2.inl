////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/AABB2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/NumberTraits.hpp>
#include <jungle/base/Errors.hpp>
#include <jungle/math/Arithmetic.hpp>

namespace Jungle
{
namespace Math
{
    template < class TReal, class TVector >
    bool AABB2< TReal, TVector >::Intersect(const AABB2* pAABB2) const
    {
        TReal distX = base::GetCenter().x - pAABB2->GetCenter().x;
        TReal distY = base::GetCenter().y - pAABB2->GetCenter().y;
        return  ( Abs(distX) < base::GetExtents().x + pAABB2->GetExtents().x ) &&
                ( Abs(distY) < base::GetExtents().y + pAABB2->GetExtents().y );
    }

    template < class TReal, class TVector >
    bool AABB2< TReal, TVector >::Overlaps(const AABB2* pAABB2) const
    {
        const TVector& min = base::GetMin();
        const TVector& rMin = pAABB2->GetMin();
        if( rMin.x < min.x )    return false;
        if( rMin.y < min.y )    return false;

        const TVector& max = base::GetMax();
        const TVector& rMax = pAABB2->GetMax();
        if( rMax.x > max.x )    return false;
        if( rMax.y > max.y )    return false;
        return true;
    }

    template < class TReal, class TVector >
    bool AABB2< TReal, TVector >::Overlaps(const TVector* pPoint) const
    {
        const TVector& min = base::GetMin();
        if( pPoint->x < min.x )    return false;
        if( pPoint->y < min.y )    return false;

        const TVector& max = base::GetMax();
        if( pPoint->x > max.x )    return false;
        if( pPoint->y > max.y )    return false;
        return true;
    }

} // namespace Math

} // namespace Jungle
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Sphere2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__SPHERE2_HPP__
#define __MATH__GEO__SPHERE2_HPP__

// Internal includes
#include <jungle/math/geo/Sphere.hpp>

namespace Jungle
{
namespace Math
{
    // Forward declare Vector2 template
    template < class TReal >
    class Vector2;

    //! Sphere bounding volume class template designed to use 2d space.
    /*!
    * Class extends interface of Sphere class allowing to check for volumes intersections, overlapping
    * and point containement.
    * \note Vector template parameter class should define basic arithmetics operators (+, -, operator*( TReal ) ),
    * as also comparison operator == and static helper vector TVector::ZERO, and provide Length() method.
    */
    template < class TReal, class TVector = Math::Vector2< TReal > >
    class Sphere2 : public Sphere< TReal, TVector >
    {
        typedef Sphere< TReal, TVector >    base;

    public:
        typedef TVector Vector;
        typedef TReal   Real;

        //! Constructors wrappers for base class constructors.
                        Sphere2()
                            : base()
                        {}

                        Sphere2(const Sphere2* pSphere2)
                            : base( pSphere2 )
                        {}

                        Sphere2(const Vector& rCenter, Real radius)
                            : base( rCenter, radius )
                        {}

        explicit        Sphere2(Real radius)
                            : base( radius )
                        {}

        /*!
        * Intersection test with 2d sphere area.
        * \param pSphere2 Tested area
        */
        inline bool     Intersect(const Sphere2* pSphere2) const;

        /*!
        * Containement test with another sphere area.
        * Checks if the passed volume may be overlapped by the current one.
        * \param pSphere2 Tested area pointer
        */
        inline bool     Overlaps(const Sphere2* pSphere2) const;

        /*!
        * Check if point lays inside the bounding sphere
        * \param pPoint Tested point position
        */
        inline bool     Overlaps(const Vector* pPoint) const;

    }; // class Sphere2

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/Sphere2.inl>

#endif // !defined __MATH__GEO__SPHERE2_HPP__
// EOF

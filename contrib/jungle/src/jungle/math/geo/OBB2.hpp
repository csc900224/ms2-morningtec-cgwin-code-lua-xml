////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/OBB2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__OBB2_HPP__
#define __MATH__GEO__OBB2_HPP__

// Internal includes
#include <jungle/math/geo/OBB.hpp>

namespace Jungle
{
namespace Math
{
    // Forward declare Vector2 template
    template < class TReal >
    class Vector2;

    //! Oriented box class template for 2d space, extends generic OBB with methods allowable for 2d space.
    /*!
    * This class allows for testing intersection and overlapping in 2d space.
    * \note TVector template class should define basic arithmetics operators (+, -, operator*( TReal ) ),
    * comparison operator == as also should define contructor based on one input parameter TVector( TReal ),
    * filling all vector coordinates with the same value.
    */
    template < class TReal, class TVector = Math::Vector2< TReal > >
    class OBB2 : public OBB< TReal, TVector >
    {
        typedef OBB< TReal, TVector >    base;

    public:
        typedef TVector Vector;
        typedef TReal   Real;

        /*!
        * Default constructor, sets rectangle position to (0, 0) and makes it point size (minimal/zero size)
        */
        inline          OBB2();

        /*!
        * Coping constructor
        * \param pOBB2 pointer to another OBB2
        */
        inline          OBB2(const OBB2* pOBB2);

        /*!
        * Create centered, axis aligned rectangle based on the extents size.
        * \param size of the rectangle in each dimensions.
        */
        explicit inline OBB2(const Vector& extentsSize);

        /*!
        * Create rectangle based on the center position and its extent in neutral orientation (axis aligned).
        * \param center rectangle center
        * \param extent coordinates lenghts stored in vector
        */
        inline          OBB2(const Vector& center, const Vector& extentsSize);

        /*!
        * Just more generic constructor wrapper.
        * Creates oriented rectangle based center point, extents lenght and major/minor axis orientations.
        * \param center rectangle center
        * \param extentsSize lengths stored in vector
        * \param axes x,y major/minor axes vectors table (normalized)
        */
        inline          OBB2(const Vector& center, const Vector& extentsSize, const Vector* axes);

        /*!
        * Create oriented rectangle based on the major/minor axis, center point and extents lenght.
        * \param center rectangle center
        * \param extentsSize lenght stored in vector
        * \param axisX major axis vector
        * \param axisY minor axis vector
        */
        inline          OBB2(const Vector& center, const Vector& extentsSize, const Vector& axisX, const Vector& axisY);

        /*!
        * Setup rectangle based on the major/minor axis (orientation), center point and extents lenght.
        * \param center rectangle center
        * \param extent lenght in each dimensions (after rotation) stored in vector
        * \param axisX major axis vector (orientation)
        * \param axisY minor axis vector
        */
        inline void     SetCenterExtentsAndAxes(const Vector& center, const Vector& extent, const Vector& axisX, const Vector& axisY);

        //! Setup new box rotation.
        /*
        * \param radAngle rotation angle specified in radians,.
        */
        inline void     SetRotation( Real radAngle );

        //! Apply rotation
        /*!
        * Apply additional box rotation.
        * \param radAngle aditional rotation angle in radians.
        */
        inline void     Rotate( Real radAngle );

        //! Intersection test with other OBB2.
        /*!
        * Intersection test with another box area.
        * \param pOBB2 Tested area pointer.
        */
        bool            Intersect(const OBB2* pOBB2) const;

        //! Containement test with another oriented box.
        /*!
        * Checks if the passed volume may be overlapped by the current one.
        * \param pOBB2 Tested geometry pointer
        */
        bool            Overlaps(const OBB2* pOBB2) const;

        //! Check if box overlaps specified point.
        /*!
        * Check if point lays inside the bounding area
        * \param pPoint Tested point position
        */
        bool            Overlaps( const Vector* point ) const;

    private:
        /*!
        * Rotate vector clockwise. Method can be used with the same vector references as an parameters
        * \param rSrcVec[in]    vector to be rotated
        * \param rDstVec[out]   reference to resulting vector
        */
        static void     RotateVector(Real angle, const Vector& rSrcVec, Vector& rDstVec);

    }; // class OBB2

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/OBB2.inl>

#endif // !defined __MATH__GEO__OBB2_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/OBB2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// External modules includes
#include <jungle/math/Arithmetic.hpp>
#include <jungle/math/Trigonometry.hpp>
#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Math
{
    template < class TReal, class TVector >
    inline OBB2< TReal, TVector >::OBB2()
        : base()
    {
    }

    template < class TReal, class TVector >
    inline OBB2< TReal, TVector >::OBB2(const OBB2* pOBB2)
        : base( pOBB2 )
    {
    }

    template < class TReal, class TVector >
    inline OBB2< TReal, TVector >::OBB2(const TVector& extentSize)
        : base( extentSize )
    {
    }

    template < class TReal, class TVector >
    inline OBB2< TReal, TVector >::OBB2(const TVector& center, const TVector& extentSize)
        : base( center, extentSize )
    {
    }

    template < class TReal, class TVector >
    inline OBB2< TReal, TVector >::OBB2(const TVector& center, const TVector& extentsSize, const TVector* axes)
        : base( center, extentsSize, axes )
    {
    }

    template < class TReal, class TVector >
    inline OBB2< TReal, TVector >::OBB2(const TVector& center, const TVector& extent, const TVector& axisX, const TVector& axisY)
    {
        SetCenterExtentsAndAxes( center, extent, axisX, axisY );
    }

    template < class TReal, class TVector >
    inline void OBB2< TReal, TVector >::SetCenterExtentsAndAxes(const TVector& center, const TVector& extentsSize, const TVector& axisX, const TVector& axisY)
    {
        base::m_center = ( center );
        base::m_extents = ( extentsSize );

        base::m_axes[0] = axisX;
        base::m_axes[1] = axisY;
    }

    template < class TReal, class TVector >
    void OBB2< TReal, TVector >::SetRotation( TReal radAngle )
    {
        RotateVector( radAngle, CoordSystem< TReal, TVector >::UNIT_X, base::m_axes[0] );
        // Find perpendicular
        base::m_axes[1].x =  base::m_axes[0].y;
        base::m_axes[1].y = -base::m_axes[0].x;
    }

    template < class TReal, class TVector >
    void OBB2< TReal, TVector >::Rotate( TReal radAngle )
    {
        RotateVector( radAngle, base::m_axes[0], base::m_axes[0] );

        // Find perpendicular
        base::m_axes[1].x =  base::m_axes[0].y;
        base::m_axes[1].y = -base::m_axes[0].x;
    }

    template < class TReal, class TVector >
    bool OBB2< TReal, TVector >::Intersect( const OBB2* pOBB2 ) const
    {
        JUNGLE_ASSERT_MSG( pOBB2, "NULL passed to function OBB2::Intersect" );

        // convenience variables
        const TVector vAxesA[] = { base::GetAxis(0), base::GetAxis(1) };
        const TVector vAxesB[] = { pOBB2->GetAxis(0), pOBB2->GetAxis(1) };
        const TReal vExtentsA[] = { base::GetExtentsSize().x, base::GetExtentsSize().y };
        const TReal vExtentsB[] = { pOBB2->GetExtentsSize().x, pOBB2->GetExtentsSize().y };

        // compute difference of box centers, D = C1-C0
        TVector vDiff = pOBB2->GetCenter() - base::GetCenter();

        TReal absAxesAdotB[2][2];
        TReal absAxesDotDiff, rangesSum;

        // axis C0+t*A0
        absAxesAdotB[0][0] = Math::Abs( vAxesA[0].Dot(vAxesB[0]) );
        absAxesAdotB[0][1] = Math::Abs( vAxesA[0].Dot(vAxesB[1]) );
        absAxesDotDiff = Math::Abs( vAxesA[0].Dot(vDiff) );
        rangesSum = vExtentsA[0] + vExtentsB[0] * absAxesAdotB[0][0] + vExtentsB[1] * absAxesAdotB[0][1];
        if (absAxesDotDiff > rangesSum)
        {
            return false;
        }

        // axis C0+t*A1
        absAxesAdotB[1][0] = Math::Abs( vAxesA[1].Dot(vAxesB[0]) );
        absAxesAdotB[1][1] = Math::Abs( vAxesA[1].Dot(vAxesB[1]) );
        absAxesDotDiff = Math::Abs(vAxesA[1].Dot(vDiff));
        rangesSum = vExtentsA[1] + vExtentsB[0] * absAxesAdotB[1][0] + vExtentsB[1] * absAxesAdotB[1][1];
        if (absAxesDotDiff > rangesSum)
        {
            return false;
        }

        // axis C0+t*B0
        absAxesDotDiff = Math::Abs( vAxesB[0].Dot(vDiff) );
        rangesSum = vExtentsB[0] + vExtentsA[0]*absAxesAdotB[0][0] + vExtentsA[1]*absAxesAdotB[1][0];
        if (absAxesDotDiff > rangesSum)
        {
            return false;
        }

        // axis C0+t*B1
        absAxesDotDiff = Math::Abs( vAxesB[1].Dot(vDiff) );
        rangesSum = vExtentsB[1] + vExtentsA[0]*absAxesAdotB[0][1] + vExtentsA[1]*absAxesAdotB[1][1];
        if (absAxesDotDiff > rangesSum)
        {
            return false;
        }

        return true;
    }

    template < class TReal, class TVector >
    bool OBB2< TReal, TVector >::Overlaps( const OBB2* pOBB2 ) const
    {
        JUNGLE_ASSERT_MSG( pOBB2, "NULL passed to function OBB2::Overlaps." );
        JUNGLE_STATIC_CHECK_MSG( false, Not_implemented_yet );
        return false;
    }

    template < class TReal, class TVector >
    bool OBB2< TReal, TVector >::Overlaps( const TVector* point ) const
    {
        // Find the offset vector between point and box center - work in the OBB coordinate space
        TVector vDiff = *point - base::GetCenter();
        TVector vAxis = base::GetAxis(0);             // oriented extents including rotation

        TReal projDist = vAxis.Dot( vDiff );
        TReal extSqr = Math::Sqr( base::GetExtentsSize().x );

        if( projDist < -extSqr || projDist > extSqr )
            return false;

        // Check other axis (minor)
        vAxis.Set( -vAxis.y, vAxis.x );

        projDist = vAxis.Dot( vDiff );
        extSqr = Math::Sqr( base::GetExtentsSize().y );

        if( projDist < -extSqr || projDist > extSqr )
            return false;

        return true;
    }

    /* static */
    template < class TReal, class TVector >
    void OBB2< TReal, TVector >::RotateVector( TReal radAngle, const TVector& inVec, TVector& outVec )
    {
        register TReal sin, cos;
        sin = Math::Sin( radAngle );
        cos = Math::Cos( radAngle );

        // For safety make temp variable
        // this secures situation when source vector is the same as destination
        TReal temp = inVec.x;
        outVec.x = (cos * temp)    - (sin * inVec.y);
        outVec.y = (cos * inVec.y) + (sin * temp);
    }

} // namespace Math

} // namespace Jungle

// EOF

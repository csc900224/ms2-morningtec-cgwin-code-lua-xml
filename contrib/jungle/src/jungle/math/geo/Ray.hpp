////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Ray.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__RAY_HPP__
#define __MATH__GEO__RAY_HPP__

// Internal includes
#include <jungle/math/geo/Line.hpp>

// External includes

namespace Jungle
{
namespace Math
{
    // Forward declarations
    template < class TReal, class TVector >
    class AABB;
    template < class TReal, class TVector >
    class OBB;
    template < class TReal, class TVector >
    class Segment;

    //! Most generic Ray template working both in 2d and 3d space.
    /*!
    * \note Please note that common getters and setters are duplicated here from Line implementation
    * just to notice their different interpretation and generate appropriate documentation.
    */
    template < class TReal, class TVector >
    class Ray : public Line< TReal, TVector >
    {
        typedef Line< TReal, TVector >      base;

    public:
        typedef TReal                       Real;
        typedef TVector                     Vector;

        /*!
        * Default constructor, doesn't reset ray origin neigher direction
        */
        inline                  Ray()
        {}

        /*!
        * Initializing constructor, sets ray direction and position
        * \param origin         ray start position
        * \param dir            ray direction
        */
        inline                  Ray(const Vector& rOrigin, const Vector& rDir)
                                : base( rOrigin, rDir )
        {}

        /*!
        * Initializing constructor, setup ray direction and origin in the world center (zero point)
        * \param dir            ray direction
        */
        explicit inline         Ray(const Vector& rDir)
                                : base( rDir )
        {}

        /*!
        * Get ray starting point
        * \return               ray origin
        */
        inline const Vector&    GetOrigin() const;

        /*!
        * Get ray direction vector
        * \return               ray direction
        */
        inline const Vector&    GetDir() const;

        /*!
        * Set ray starting point
        * \param                ray origin
        */
        inline void             SetOrigin(const Vector& rOrigin);

        /*!
        * Set ray direction
        * \param rDirection     new ray direction
        */
        inline void             SetDir(const Vector& rDirection);

    protected:
        //! Ray origin - starting point
        using base::m_origin;
        //! Ray direction
        using base::m_dir;

    }; // class Ray

} // namespace Scene

} // namespace Jungle

#include <jungle/math/geo/Ray.inl>

#endif // !defined __MATH__GEO__RAY_HPP__
// EOF

////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Sphere.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

namespace Jungle
{
namespace Math
{
    template < class TReal, class TVector >
    Sphere< TReal, TVector >::Sphere()
    {
        SetPoint( TVector( NumberTraits< TReal >::ZERO ) );
    }

    template < class TReal, class TVector >
    Sphere< TReal, TVector >::Sphere(const Sphere* pSphere)
        : m_center( pSphere->m_center )
        , m_radius( pSphere->m_radius )
    {
    }

    template < class TReal, class TVector >
    Sphere< TReal, TVector >::Sphere(const TVector& rCenter, TReal radius)
        : m_center( rCenter )
        , m_radius( radius )
    {
    }

    template < class TReal, class TVector >
    Sphere< TReal, TVector >::Sphere(TReal radius)
        : m_center( TVector( NumberTraits< TReal >::ZERO ) )
        , m_radius( radius )
    {
    }

    template < class TReal, class TVector >
    void Sphere< TReal, TVector >::SetEmpty()
    {
        m_center = TVector( NumberTraits< TReal >::ZERO );
        m_radius = NumberTraits< Real >::Minimum();
    }

    template < class TReal, class TVector >
    bool Sphere< TReal, TVector >::IsEmpty() const
    {
        // We are checking only local size (scaling, rotating an empty shape
        // will not affect its status) 
        return ( m_radius == NumberTraits< TReal >::Minimum() );
    }

    template < class TReal, class TVector >
    void Sphere< TReal, TVector >::SetPoint(const TVector& rPos)
    {
        m_center = rPos;
        m_radius = NumberTraits< TReal >::ZERO;
    }

    template < class TReal, class TVector >
    bool Sphere< TReal, TVector >::IsPoint() const
    {
        return m_radius == NumberTraits< TReal >::ZERO;
    }

    template < class TReal, class TVector >
    void Sphere< TReal, TVector >::SetCenter(const TVector& rCenter)
    {
        m_center = rCenter;
    }

    template < class TReal, class TVector >
    void Sphere< TReal, TVector >::SetRadius(TReal radius)
    {
        m_radius = radius;
    }

    template < class TReal, class TVector >
    void Sphere< TReal, TVector >::SetCenterRadius(const TVector& rCenter, TReal radius)
    {
        m_center = rCenter;
        m_radius = radius;
    }

    template < class TReal, class TVector >
    void Sphere< TReal, TVector >::SetMinMax(const TVector& rMin, const TVector& rMax)
    {
        JUNGLE_ASSERT( rMin != rMax );

        m_center = ( rMax + rMin ) * NumberTraits< TReal >::Half();
        m_radius = ( rMax - rMin ).Length() * NumberTraits< TReal >::Half();
    }


    template < class TReal, class TVector >
    const TVector& Sphere< TReal, TVector >::GetCenter() const
    {
        return m_center;
    }

    template < class TReal, class TVector >
    const TReal& Sphere< TReal, TVector >::GetRadius() const
    {
        return m_radius;
    }

    template < class TReal, class TVector >
    TVector Sphere< TReal, TVector >::GetMin() const
    {
        return m_center - TVector( m_radius );
    }

    template < class TReal, class TVector >
    TVector Sphere< TReal, TVector >::GetMax() const
    {
        return m_center + TVector( m_radius );
    }

} // namespace Math

} // namespace Jungle

// EOF

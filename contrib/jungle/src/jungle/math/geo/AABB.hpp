////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/AABB.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__AABB_HPP__
#define __MATH__GEO__AABB_HPP__

// Internal includes

#define AABB_USE_MIN_MAX  // use precalculated boundaries (min/max) with a of center, extents calculation.

namespace Jungle
{
namespace Math
{
    //! Axis aligned bounding volume class template.
    /*!
    * \note TVector template class should define basic arithmetics operators (+, -, operator*( TReal ) ),
    * comparison operator == as also should define contructor based on one input parameter TVector( TReal ),
    * filling all vector coordinates with the same value.
    */
    template < class TReal, class TVector >
    class AABB
    {
    public:
        typedef TVector Vector;
        typedef TReal   Real;

        static const int AXIS_NUM = sizeof( TVector ) / sizeof( TReal );

        /*!
        * Default constructor, sets rectangle position to (0, 0) and makes it point size (extents equal zero).
        */
                        AABB();

        /*!
        * Coping constructor.
        * \param pAABB pointer to another AABB
        */
                        AABB(const AABB* pAABB);

        /*!
        * Construct bounding rectangle based on the center position and dimensions vector.
        * \param rCenter rectangle center position
        * \param rExtents rectangle extents its dimensions will be equal 2*extents
        */
                        AABB(const Vector& rCenter, const Vector& rExtents);

        /*!
        * Construct bounding area starting from a world (0, 0) point to the point given by
        * parameter - dimensions vector.
        * \param rExtents box half dimensions (extents equal to the half size)
        */
        explicit        AABB(const Vector& rExtents);

        /*!
        * Set bounding area to smallest possible size - do not contain anything.
        */
        inline void     SetEmpty();

        /*!
        * Check if area is totally empty and can not contain anything.
        * Scaling and rotating shape doesn't affect it's empty status.
        */
        inline bool     IsEmpty() const;

        /*!
        * Set bounding area to contain single point with coordinates passed to method.
        * \param rPos   new bounding area coordinates center 
        */
        inline void     SetPoint(const Vector& rPos);

        /*!
        * Check if bounding area forms a single point.
        */
        inline bool     IsPoint() const;

        /*!
        * Set minimum and maximum points in the bounding area.
        * \param rMin   minimum point in bounding rectangle
        * \param rMax   maximum point
        */
        inline void     SetMinMax(const Vector& rMin, const Vector& rMax);

        /*!
        * Change center of a bounding area without changing its extents.
        * \param rCenter vector pointing to the new area center
        */
        inline void     SetCenter(const Vector& rCenter);

        /*!
        * Change extents of a bounding area without changing its center position.
        * \param rExtents vector from a center to the farthest pixel in the bounded area
        */
        inline void     SetExtents(const Vector& rExtents);

        /*!
        * Set center and extend of a bounding rectangle.
        * \param rCenter geometric rectangle center
        * \param rExtents vector from a center to the farthest pixel in the bounded area
        */
        inline void     SetCenterExtents(const Vector& rCenter, const Vector& rExtents);

        /*!
        * Get minimum point of a rectangle.
        * \return minimum point
        */
#ifdef AABB_USE_MIN_MAX
        inline const Vector&    GetMin() const;
#else
        inline Vector           GetMin() const;
#endif
        /*!
        * Get rectangle maximum point.
        * \return rectangle max
        */
#ifdef AABB_USE_MIN_MAX
        inline const Vector&    GetMax() const;
#else
        inline Vector           GetMax() const;
#endif
        /*!
        * Get rectangle center point.
        * \return bounding surface center
        */
#ifdef AABB_USE_MIN_MAX
        inline Vector           GetCenter() const;
#else
        inline const Vector&    GetCenter() const;
#endif

        /*!
        * Get rectangle extents.
        * \return bounding area extents
        */
#ifdef AABB_USE_MIN_MAX
        inline Vector           GetExtents() const;
#else
        inline const Vector&    GetExtents() const;
#endif

        /*!
        * Get rectangle major/minor axis vector
        * \param  axis index, acceptable values are: 0 for X axis and 1 for Y, 2 for Z (if exist).
        * \return unit length axis vector
        */
        inline const Vector&    GetAxis(unsigned char axis) const;

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for AABB *= float. Scales the extents, keeps same center.
        inline AABB&            operator*=(Real s)
        {
            SetExtents( GetExtents() * s );
            return *this;
        }

        //! Operator for AABB /= float. Scales the extents, keeps same center.
        inline AABB&            operator/=(Real s)
        {
            SetExtents( GetExtents() / s );
            return *this;
        }

        //! Operator for AABB += rTrans. Translates the rectangle keeping the extents.
        inline AABB&            operator+=(const Vector& rTrans)
        {
            SetCenter( GetCenter() + rTrans );
            return *this;
        }

        //! Operator for AABB -= rTrans. Translates the rectangle keeping it's extents.
        inline AABB&            operator-=(const Vector& rTrans)
        {
            SetCenter( GetCenter() - rTrans );
            return *this;
        }

    private:
#ifdef AABB_USE_MIN_MAX
        Vector          m_min;
        Vector          m_max;
#else
        Vector          m_center;
        Vector          m_extents;
#endif

    }; // class AABB

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/AABB.inl>

#endif // !defined __MATH__GEO__AABB_HPP__
// EOF

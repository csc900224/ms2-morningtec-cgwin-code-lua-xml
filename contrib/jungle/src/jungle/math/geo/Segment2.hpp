////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Segment2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__SEGMENT2_HPP__
#define __MATH__GEO__SEGMENT2_HPP__

// Internal includes
#include <jungle/math/geo/Segment.hpp>

// External includes

namespace Jungle
{
namespace Math
{
    // Forward declare Vector2 template for default parameters list.
    // NOTE: In this case Vector2 should be explicitly included.
    template < class TReal >
    class Vector2;

    template< class TReal, class TVector = Vector2< TReal > >
    class Segment2 : public Segment< TReal, TVector >
    {
        typedef Segment< TReal, TVector >   base;

    public:
        typedef TReal           Real;
        typedef TVector         Vector2;

        /*!
        * Default constructor, doesn't reset anything, assume undefined.
        */
        inline                  Segment2()
        {}

        /*!
        * Initializing constructor, sets segment direction, position and size
        * \param origin         ray start position
        * \param dir            ray direction
        * \param length         segment length
        */
        inline                  Segment2(const Vector2& rOrigin, const Vector2& rDir, const Real& length)
                                : base( rOrigin, rDir, length )
        {}

        /*!
        * Initialize segment with direction and length, simple base constructor wrapper.
        * \param dir            ray direction
        * \param extent         segment lenght
        */
        inline                  Segment2(const Vector2& rDir, const Real& length)
                                : base( rDir, length )
        {}

        /*!
        * Initializing constructor, sets segment origin in (0,0), its' direction using x and y coordinates and length.
        * \param x              x direction
        * \param y              y direction coordinate
        * \param lenght         segment lenght
        */
        inline                  Segment2(Real xDir, Real yDir, Real length)
                                : base( Vector2( xDir, yDir ), length )
        {}

        /*!
        * Set segment starting point using x, y coordinates.
        * \see SetStart(const Vector2&)
        * \param x              segment start x coordinate
        * \param y              segment start y coordinate
        */
        inline void             SetStartXY(Real x, Real y);

        /*!
        * Set segment end point using x, y coordinates.
        * \see SetEnd(const Vector2&)
        * \param x              segment end point x value
        * \param y              segment end point y value
        */
        inline void             SetEndXY(Real x, Real y);

        /*!
        * Sets segment direction using its X and Y coordinates.
        * \note Coordinates should be already normalized.
        * \see SetDir(const Vector2&)
        * \param x              segment end point x value
        * \param y              segment end point y value
        */
        inline void             SetDirXY(Real x, Real y);

    }; // class Segment2

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/Segment2.inl>

#endif // !defined __MATH__GEO__SEGMENT2_HPP__
// EOF

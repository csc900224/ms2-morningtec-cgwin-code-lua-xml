////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Ray.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes

namespace Jungle
{
namespace Math
{
    template< class TReal, class TVector >
    inline const TVector& Ray< TReal, TVector >::GetOrigin() const
    {
        return base::GetOrigin();
    }

    template< class TReal, class TVector >
    inline const TVector& Ray< TReal, TVector >::GetDir() const
    {
        return base::GetDir();
    }

    template< class TReal, class TVector >
    inline void Ray< TReal, TVector >::SetOrigin(const TVector& rOrigin)
    {
        base::SetOrigin( rOrigin );
    }

    template< class TReal, class TVector >
    inline void Ray< TReal, TVector >::SetDir(const TVector& rDirection)
    {
        base::SetDir( rDirection );
    }

} // namespace Math

} // namespace Jungle

// EOF

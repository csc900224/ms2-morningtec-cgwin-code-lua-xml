////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      Segment.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/math/Arithmetic.hpp>

namespace Jungle
{
namespace Math
{
    template< class TReal, class TVector >
    inline const TVector& Segment< TReal, TVector >::GetStart() const
    {
        return base::m_origin;
    }

    template< class TReal, class TVector >
    inline TVector Segment< TReal, TVector >::GetCenter() const
    {
        return GetStart() + GetHalfLength() * base::m_dir;
    }

    template< class TReal, class TVector >
    inline TVector Segment< TReal, TVector >::GetEnd() const
    {
        return base::m_origin + m_extent * base::m_dir;
    }

    template< class TReal, class TVector >
    inline void Segment< TReal, TVector >::SetStart(const TVector& rStart)
    {
        base::m_origin = rStart;
    }

    template< class TReal, class TVector >
    inline void Segment< TReal, TVector >::SetEnd(const TVector& rEnd)
    {
        base::m_dir = rEnd - base::m_origin;
        m_extent = base::m_dir.Length();
        base::m_dir /= m_extent;
    }

    template< class TReal, class TVector >
    inline TVector Segment< TReal, TVector >::GetExtent() const
    {
        return m_extent * base::m_dir;
    }

    template< class TReal, class TVector >
    inline void Segment< TReal, TVector >::SetExtent(const TVector& rExtent)
    {
        m_extent = rExtent.Length();
        base::m_dir = rExtent / m_extent;
    }

    template< class TReal, class TVector >
    inline TReal Segment< TReal, TVector >::GetLength() const
    {
        return m_extent;
    }

    template< class TReal, class TVector >
    inline TReal Segment< TReal, TVector >::GetHalfLength() const
    {
        return Div2( m_extent );
    }

    template< class TReal, class TVector >
    inline void Segment< TReal, TVector >::SetLength(const TReal& rLength)
    {
        m_extent = rLength;
    }

} // namespace Math

} // namespace Jungle

// EOF

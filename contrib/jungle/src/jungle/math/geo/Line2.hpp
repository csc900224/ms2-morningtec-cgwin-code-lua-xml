////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Line2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__LINE2_HPP__
#define __MATH__GEO__LINE2_HPP__

// Internal includes
#include <jungle/math/geo/Line.hpp>

namespace Jungle
{
namespace Math
{
    // Forward declare Vector2 template for default parameters list
    template < class TReal >
    class Vector2;

    template < class TReal, class TVector = Vector2< TReal > >
    class Line2 : public Line< TReal, TVector >
    {
    public:
        typedef TReal                       Real;
        typedef TVector                     Vector2;

    }; // class Line2

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/Line2.inl>

#endif // !defined __MATH__GEO__LINE2_HPP__
// EOF

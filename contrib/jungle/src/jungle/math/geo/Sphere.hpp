////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Sphere.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__SPHERE_HPP__
#define __MATH__GEO__SPHERE_HPP__

// Internal includes

namespace Jungle
{
namespace Math
{
    //! Sphere volume class template.
    /*!
    * \note TVector template class should define basic arithmetics operators (+, -, operator*( TReal ) ),
    * comparison operator == as also should define contructor based on one input parameter TVector( TReal ),
    * filling all vector coordinates with the same value, there should be Length() method defined in TVector class.
    */
    template < class TReal, class TVector >
    class Sphere
    {
    public:
        typedef TVector Vector;
        typedef TReal   Real;

        /*!
        * Default constructor, sets sphere position to world center and radius to zero.
        */
                        Sphere();

        /*!
        * Coping constructor
        * \param pSphere pointer to another boudning sphere
        */
                        Sphere(const Sphere* pSphere);

        /*!
        * Construct bounding sphere based on a center position and radius passed to it.
        * \param rCenter rectangle center position
        * \param radius  sphere size
        */
                        Sphere(const Vector& rCenter, Real radius);

        /*!
        * Construct bounding sphere centered in (0, 0) point with specified radius
        * given by constructor parameter
        * \param radius  sphere size
        */
        explicit        Sphere(Real radius);

        /*!
        * Set bounding area to smallest possible size - do not contain anything
        */
        inline void     SetEmpty();

        /*!
        * Check if volume is totally empty and can not contain anything.
        */
        inline bool     IsEmpty() const;

        /*!
        * Set bounding area to contain single point with coordinates passed to method
        * \param rPos   new bounding area coordinates center
        */
        inline void     SetPoint(const Vector& rPos);

        /*!
        * Check if bounding area forms a single point.
        */
        inline bool     IsPoint() const;

        /*!
        * Change sphere geomatric center
        * \param rCenter geometric center
        */
        inline void     SetCenter(const Vector& rCenter);

        /*!
        * Change radius of a bounding sphere
        * \param radius distance from a center to the farthest pixel in the bounded volume
        */
        inline void     SetRadius(Real radius);

        /*!
        * Set center and radius of a bounding sphere
        * \param rCenter geometric center
        * \param radius distance from a center to the farthest pixel in the bounded volume
        */
        inline void     SetCenterRadius(const Vector& rCenter, Real radius);

        /*!
        * Set minimum and maximum points in the bounding area.
        * \param rMin   minimum point of the smallest possible overlapping bounding box
        * \param rMax   maximum point of the overalapping bounding box.
        */
        inline void     SetMinMax(const Vector& rMin, const Vector& rMax);

        /*!
        * Get sphere geometric center.
        * \return bounding volume center
        */
        inline
        const Vector&   GetCenter() const;

        /*!
        * Get radius
        * \return bounding sphere radius
        */
        inline
        const Real&     GetRadius() const;

        /*!
        * Get minimum point of a the closest box fully overlapping sphere.
        * \return minimum point on every world coordinates axis.
        */
        inline Vector   GetMin() const;

        /*!
        * Get sphere maximum point assuming city metrics as the distance meassure.
        * \return maximum point on every coordinates axis.
        */
        inline Vector   GetMax() const;

        /*****************************************************************************************************************************************************************************************************/
        /* Operators
        /*****************************************************************************************************************************************************************************************************/

        //! Operator for Sphere *= float. Scales the radius, keeps same center.
        inline Sphere&  operator*=(Real s)
        {
            m_radius *= s;
            return *this;
        }

        //! Operator for Sphere /= float. Scales the radius, keeps same center.
        inline Sphere&  operator/=(Real s)
        {
            m_radius /= s;
            return *this;
        }

        //! Operator for Sphere += rTrans. Translates shape keeping it's size.
        inline Sphere&  operator+=(const Vector& rTrans)
        { 
            m_center += rTrans;
            return *this; 
        }

        //! Operator for Sphere -= rTrans. Translates shape keeping it's radius.
        inline Sphere&  operator-=(const Vector& rTrans)
        {
            m_center -= rTrans;
            return *this;
        }

    protected:
        TVector         m_center;
        TReal           m_radius;

    }; // class Sphere

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/Sphere.inl>

#endif // !defined __MATH__GEO__SPHERE_HPP__
// EOF

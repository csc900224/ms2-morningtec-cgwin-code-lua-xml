////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Segment.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

#ifndef __MATH__GEO__SEGMENT_HPP__
#define __MATH__GEO__SEGMENT_HPP__

// Internal includes
#include <jungle/math/geo/Ray.hpp>

// External includes

namespace Jungle
{
namespace Math
{
    template< class TReal, class TVector >
    class Segment : public Ray< TReal, TVector >
    {
        typedef Ray< TReal, TVector >   base;

    public:
        typedef TReal           Real;
        typedef TVector         Vector;

        /*!
        * Default constructor, doesn't reset anything so assue unexpected values.
        */
        inline                  Segment()
                                : base()
        {}

        /*!
        * Initializing constructor, sets segment direction, position and size
        * \param origin         ray start position
        * \param dir            ray direction
        * \param extent         segment length
        */
        inline                  Segment(const Vector& rOrigin, const Vector& rDir, const Real& length)
                                : base( rOrigin, rDir )
                                , m_extent( length )
        {}

        /*!
        * Initialize segment with direction and length, sets segment starting point in the world origin
        * \param dir            ray direction
        * \param extent         segment lenght
        */
        inline                  Segment(const Vector& rDir, const Real& length)
                                : base( rDir )
                                , m_extent( length )
        {}

        /*!
        * Get segment starting position.
        * In current implementation segment origin is assumed to be the segment start.
        * \return               point at the begining of the segment
        */
        inline const Vector&    GetStart() const;

        /*!
        * Get segment center point.
        * \return               point in the middle between segmend start and end position.
        */
        inline Vector           GetCenter() const;

        /*!
        * Get ending point
        * \return               segment end point
        */
        inline Vector           GetEnd() const;

        /*!
        * Setup segment start point, this will not change current direction and segment lenght,
        * but simply translates whole segment to the new origin.
        * \see SetOrigin()
        * \param                segment start point
        */
        inline void             SetStart(const Vector& rStart);

        /*!
        * Set segment end point, this will involve calculating new direction and segment lenght based
        * on current origin.
        * \param                segment end point
        */
        inline void             SetEnd(const Vector& rEnd);

        /*!
        * Get segment extent vector - vector pointing from origin to the segment end.
        * \return               segment end point
        */
        inline Vector           GetExtent() const;

        /*!
        * Set segment extent (or end point), this will involve calculating new direction and segment lenght based
        * on current origin and extent vector expressed as an offset from origin to the end point.
        * \param                segment origin
        */
        inline void             SetExtent(const Vector& rExtent);

        /*!
        * Get segment lenght as a distance between segment start and end point.
        * \return               segment length
        */
        inline Real             GetLength() const;

        /*!
        * Get segment half of the segment lenght.
        * Method added for performance reasons, such as returned value may be precalculated.
        * \return               segment length
        */
        inline Real             GetHalfLength() const;

        /*!
        * Set new segment length, this will spread or shorten segment in the current direction,
        * starting from its origin (start point).
        * \param                new length
        */
        inline void             SetLength(const Real& rLength);

    private:
        Real                    m_extent;

    }; // class Segment

} // namespace Math

} // namespace Jungle

#include <jungle/math/geo/Segment.inl>

#endif // !defined __MATH__GEO__SEGMENT_HPP__
// EOF

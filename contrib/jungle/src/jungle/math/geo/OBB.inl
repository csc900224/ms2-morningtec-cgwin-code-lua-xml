////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/OBB.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// External modules includes
#include <jungle/math/Arithmetic.hpp>
#include <jungle/math/geo/CoordSystem.hpp>

#include <jungle/base/Errors.hpp>

namespace Jungle
{
namespace Math
{

    template < class TReal, class TVector >
    inline OBB< TReal, TVector >::OBB()
    {
        SetPoint( TVector( NumberTraits< TReal >::ZERO ) );

        // Reset major/minor axes
        ResetRotation();
    }

    template < class TReal, class TVector >
    inline OBB< TReal, TVector >::OBB(const OBB* pOBB)
        : m_center( pOBB->m_center )
        , m_extents( pOBB->m_extents )
    {
        for( unsigned int i = 0; i < AXIS_NUM; ++i )
        {
            m_axes[i] = ( pOBB->m_axes[i] );
        }
    }

    template < class TReal, class TVector >
    inline OBB< TReal, TVector >::OBB(const TVector& extentSize)
        : m_center( TVector( NumberTraits< TReal >::ZERO ) )
        , m_extents( extentSize )
    {
        // Reset major/minor axes
        ResetRotation();
    }

    template < class TReal, class TVector >
    inline OBB< TReal, TVector >::OBB(const TVector& center, const TVector& extentSize)
    {
        m_center = center;
        m_extents = extentSize;

        ResetRotation();
    }

    template < class TReal, class TVector >
    inline OBB< TReal, TVector >::OBB(const TVector& center, const TVector& extent, const TVector* axes)
    {
        SetCenterExtentsAndAxes( center, extent, axes );
    }

    template < class TReal, class TVector >
    inline void OBB< TReal, TVector >::SetEmpty()
    {
        m_center = TVector( NumberTraits< TReal >::ZERO );
        m_extents = TVector( NumberTraits< TReal >::Minimum() );
    }

    template < class TReal, class TVector >
    inline bool OBB< TReal, TVector >::IsEmpty() const
    {
        // Check only extent size (rotating doesn't affect empty shape)
        return m_extents == TVector( NumberTraits< TReal >::Minimum() );
    }

    template < class TReal, class TVector >
    inline void OBB< TReal, TVector >::SetPoint(const TVector& rPos )
    {
        m_center = rPos;
        m_extents = TVector( NumberTraits< Real >::ZERO );
    }

    template < class TReal, class TVector >
    inline bool OBB< TReal, TVector >::IsPoint() const
    {
        // Check only extent size (rotating doesn't affect empty volume)
        return m_extents == TVector( NumberTraits< TReal >::ZERO );
    }

    template < class TReal, class TVector >
    inline void OBB< TReal, TVector >::SetCenter(const Vector& pos)
    {
        m_center = pos;
    }

    template < class TReal, class TVector >
    inline void OBB< TReal, TVector >::SetExtentsSize(const Vector& extentSize)
    {
        m_extents = extentSize;
    }

    template < class TReal, class TVector >
    inline void OBB< TReal, TVector >::SetCenterExtentsAndAxes(const TVector& center, const TVector& extentsSize, const TVector* axes)
    {
        m_center = ( center );
        m_extents = ( extentsSize );

        for( unsigned int i = 0; i < AXIS_NUM; ++i )
        {
            m_axes[i] = ( axes[i] );
        }
    }

    template < class TReal, class TVector >
    void OBB< TReal, TVector >::SetMinMax(const TVector& rMin, const TVector& rMax)
    {
        m_center = ( rMax + rMin ) * NumberTraits< TReal >::Half();
        m_extents = ( rMax - rMin ) * NumberTraits< TReal >::Half();

        ResetRotation();
    }

    template < class TReal, class TVector >
    const TVector& OBB< TReal, TVector >::GetCenter() const
    {
        return m_center;
    }

    template < class TReal, class TVector >
    const TVector& OBB< TReal, TVector >::GetExtentsSize() const
    {
        return m_extents;
    }

    template < class TReal, class TVector >
    const TVector& OBB< TReal, TVector >::GetAxis(unsigned char axis) const
    {
        JUNGLE_ASSERT_MSG( axis < AXIS_NUM, "Axis index out of allowable range, specify index below AXIS_NUM.\n" );

        return m_axes[axis];
    }

    template < class TReal, class TVector >
    TVector OBB< TReal, TVector >::GetMin() const
    {
        return GetCenter() - GetAABBExtent();
    }

    template < class TReal, class TVector >
    TVector OBB< TReal, TVector >::GetMax() const
    {
        return GetCenter() + GetAABBExtent();
    }

    template < class TReal, class TVector >
    TVector OBB< TReal, TVector >::GetAABBExtent() const
    {
        // NOTE: World extent does not direct to the farthest point in north/west
        // but simply represents rotated extent and may be even negative
        // Project world extent on world axes and take positive result
        TVector aabbExtents;
        int i = 0, j = 0, k;
        for(; i < AXIS_NUM; ++i )
        {
            aabbExtents[i] = Math::Abs( m_axes[i].Dot( CoordSystem< TReal, TVector >::GetUnitAxis(i) ) * m_extents[i] );
            for(j = 1; j < AXIS_NUM; ++j )
            {
                k = (i + j) % AXIS_NUM;
                aabbExtents[i] += Math::Abs( m_axes[k].Dot( CoordSystem< TReal, TVector >::GetUnitAxis(i) ) * m_extents[k] );
            }
        }
        return aabbExtents;
    }

    template < class TReal, class TVector >
    TReal OBB< TReal, TVector >::DistanceSqr( const TVector* point ) const
    {
        // Find the offset vector between point and box center - work in the OBB coordinate space
        TVector vDiff = *point - GetCenter();

        // compute squared distance and closest point on box
        TReal sqrDistance = NumberTraits< Real >::ZERO;
        TReal delta;
        TVector vClosest;

        for(int i = 0; i < AXIS_NUM; ++i )
        {
            // Project offset vector on each OBB unit length axis to get OBB coordinate system distance
            vClosest[i] = vDiff.Dot( m_axes[i] );
            if( vClosest[i] < -GetExtentsSize()[i] )
            {
                delta = vClosest[i] + GetExtentsSize()[i];
                vClosest[i] = -GetExtentsSize()[i];
                sqrDistance += Math::Sqr( delta );
            }
            else if( vClosest[i] > GetExtentsSize()[i] )
            {
                delta = vClosest[i] - GetExtentsSize()[i];
                vClosest[i] = GetExtentsSize()[i];
                sqrDistance += Math::Sqr( delta );
            }
        }
        return sqrDistance;
    }

    template < class TReal, class TVector >
    bool OBB< TReal, TVector >::IsInside( const TVector* point ) const
    {
        // Find the offset vector between point and box center - work in the OBB coordinate space
        Vector vDiff = *point - GetCenter();

        for(int i = 0; i < AXIS_NUM; ++i )
        {
            Vector vAxis = GetAxis(i);              // oriented axis
            Real projDistSqr = vAxis.Dot( vDiff );
            Real extSqr = Sqr( GetExtentsSize()[i] );

            if( Abs( projDistSqr ) > extSqr )
                return false;
        }
        return true;
    }

    template < class TReal, class TVector >
    void OBB< TReal, TVector >::ResetRotation()
    {
        for( unsigned int i = 0; i < AXIS_NUM; ++i )
        {
            m_axes[i] = ( CoordSystem< TReal, TVector >::GetUnitAxis(i) );
        }
    }

} // namespace Math

} // namespace Jungle

// EOF

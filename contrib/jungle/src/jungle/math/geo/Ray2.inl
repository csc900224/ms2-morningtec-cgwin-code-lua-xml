////////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/geo/Ray2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
////////////////////////////////////////////////////////////////////////////

// Internal includes

namespace Jungle
{
namespace Math
{
    template< class TReal, class TVector >
    inline void Ray2< TReal, TVector >::SetOriginXY(TReal x, TReal y)
    {
        base::m_origin.Set(x, y);
    }

    template< class TReal, class TVector >
    inline void Ray2< TReal, TVector >::SetDirXY(TReal x, TReal y)
    {
        base::m_dir.Set(x , y);
    }

} // namespace Math

} // namespace Jungle

// EOF

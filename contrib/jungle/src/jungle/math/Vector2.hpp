//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Vector2.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH__VECTOR2_HPP__
#define __INCLUDED__MATH__VECTOR2_HPP__

// Internal includes
#include <jungle/base/NumberTraits.hpp>

namespace Jungle
{
namespace Math
{
    //! Two dimensional vector template class.
    template < class T >
    class Vector2
    {
    public:
        //! Array access operator index type.
        typedef unsigned int    index_type;
        //! Self type definition.
        typedef Vector2<T>      self_type;
        //! Inner type definition.
        typedef T               value_type;

        //! Default constructor sets elements to zero.
                        Vector2();
        //! Initialize vector coordinates with the same value.
        explicit        Vector2( T v );
        //! Initialization constructor.
                        Vector2( T x, T y );
        //! Array based construction.
                        Vector2( const T* coords );
        //! Copying constructor.
                        Vector2( const Vector2& vec );

        //! Set coordinates values.
        inline void     Set( T x, T y );
        //! Zero coordinates values.
        inline void     Zero();

        // Below functions confuse compiler with such call v[0], it doesn't know if should call:
        // pointer access operator and native array access on it => v.operator const T*()[0]
        // or should he call array access operator directly v.operator[](0)

        //! Read only coordinates access via array pointer.
        //inline          operator const T* () const;
        //! Coordinates write access via array pointer.
        //inline          operator T* ();

        //! Array access operator - read only.
        inline T        operator [] ( index_type i ) const;
        //! Array access operator - write and read.
        inline T&       operator [] ( index_type i );

        //! Equality operator.
        inline bool     operator == ( const Vector2& v ) const;
        //! Inequality operator.
        inline bool     operator != ( const Vector2& v ) const;
        //! Comparison operator - greater.
        inline bool     operator >  ( const Vector2& v ) const;
        //! Comparison operator - greater or equal.
        inline bool     operator >= ( const Vector2& v ) const;
        //! Comparison operator - smaller.
        inline bool     operator <  ( const Vector2& v ) const;
        //! Comparison operator - smaller or equal.
        inline bool     operator <= ( const Vector2& v ) const;

        //! Arithmetic update - add vector.
        inline Vector2& operator += ( const Vector2& v );
        //! Arithmetic update - subtract vector.
        inline Vector2& operator -= ( const Vector2& v );
        //! Arithmetic update - multiply vector.
        inline Vector2& operator *= ( const Vector2& v );
        //! Arithmetic update - divide vector.
        inline Vector2& operator /= ( const Vector2& v );

        //! Arithmetic update - multiply by scalar.
        inline Vector2& operator *= ( T f );
        //! Arithmetic update - divide by scalar.
        inline Vector2& operator /= ( T f );

        //! Vector negation.
        inline Vector2  operator - () const;

        //! Multiply by scalar.
        inline Vector2  operator * ( T f ) const;
        //! Divide by scalar.
        inline Vector2  operator / ( T f ) const;

        //! Calculate dot product - scalar vector multiplication.
        inline T        Dot( const Vector2& v ) const;

        //! Calculate cross product.
        inline T        Cross( const Vector2& v ) const;

        //! Get length.
        inline T        Length() const;
        //! Get length squared.
        inline T        LengthSqr() const;

        //! Normalize current vector and returns its unnormalized length.
        inline T        Normalize();

        //! Returns normalized vector.
        inline Vector2  Normalized() const;

        //! Get vector perpendicular to this, it just returns Vector2(y,-x).
        inline Vector2  Perpendicular() const;

        //! Get unit length perpendicular vector, it returns Vector2(y,-x)/Length()
        inline Vector2  PerpendicularNorm () const;

        //! Calculate vector magnitude.
        static T        Mag( const Vector2& v );
        //! Calculate point distance in Cartesian coordinates (simple Cauchy metrics).
        static T        Distance( const Vector2& a, const Vector2& b );
        //! Get squared distance between two points in 2d space.
        static T        DistanceSqr( const Vector2& a, const Vector2& b );

        //! Rotate vector clockwise by specified angle (in radians).
        /*!
        * Method can be used with the same vector references as an parameters.
        * \param radAngle rotation angle defined in radians.
        * \param inVec  vector to be rotated.
        * \param outVec reference to resulting vector.
        */
        static void     RotateVector( T radAngle, const Vector2& inVec, Vector2& outVec );

        //! Standard vectors definition.
        static const Vector2    ZERO;
        static const Vector2    ONE;
        static const Vector2    ONE_X;
        static const Vector2    ONE_Y;

    public:
        //! Vector coordinates
        union
        {
            // Member convention
            struct
            {
                T       m_x;
                T       m_y;
            };
            T           m_coords[2];

            // Shorter easy access convention
            struct
            {
                T       x;
                T       y;
            };
            T           v[2];
        };

    }; // class Vector2

    // Common types definitions
    typedef Vector2< int >      Vector2i;
    typedef Vector2< float >    Vector2f;
    typedef Vector2< double >   Vector2d;

    // Common vectors definitions
    template < class TReal >
    const Vector2< TReal > Vector2< TReal >::ZERO( 0, 0 );

    template < class TReal >
    const Vector2< TReal > Vector2< TReal >::ONE( 1, 1 );

    template < class TReal >
    const Vector2< TReal > Vector2< TReal >::ONE_X( 1, 0 );

    template < class TReal >
    const Vector2< TReal > Vector2< TReal >::ONE_Y( 0, 1 );

    //! Vector addition operator.
    template < class T >
    const inline Vector2< T > operator + ( const Vector2< T >& v1, const Vector2< T >& v2 );

    //! Two vectors subtraction - subtracts v2 from v1 (v1 - v2).
    template < class T >
    const inline Vector2< T > operator - ( const Vector2< T >& v1, const Vector2< T >& v2 );

    //! Vectors coordinates multiplication - multiply vectors coordinates.
    template < class T >
    const inline Vector2< T > operator * ( const Vector2< T >& v1, const Vector2< T >& v2 );

    //! Vectors multiplication - compute vectors cross product.
    template < class T >
    const inline T            operator ^ ( const Vector2< T >& v1, const Vector2< T >& v2 );

    //! Two vectors division - multiply vectors coordinates.
    template < class T >
    const inline Vector2< T > operator / ( const Vector2< T >& v1, const Vector2< T >& v2 );

    //! Scalar vector multiplication.
    template < class T >
    const inline Vector2< T > operator * ( T scalar, const Vector2< T >& v );

    //! Specialization for arithmetic Min function (\see jungle/math/Arithmetic.hpp)
    template < class T >
    inline Vector2<T> Min( const Vector2<T>& a, const Vector2<T>& b );

    //! Specialization for arithmetic Max function (\see jungle/math/Arithmetic.hpp)
    template < class T >
    inline Vector2<T> Max( const Vector2<T>& a, const Vector2<T>& b );


    // Implementation
    template< class T >
    Vector2< T >::Vector2() :
        m_x( NumberTraits< T >::ZERO ),
        m_y( NumberTraits< T >::ZERO )
    {}

    template< class T >
    Vector2< T >::Vector2( T v ) :
        m_x( v ),
        m_y( v )
    {}

    template< class T >
    Vector2< T >::Vector2( T x, T y) :
        m_x( x ),
        m_y( y )
    {}

    template< class T >
    Vector2< T >::Vector2( const T* coords ) :
        m_x( coords[0] ),
        m_y( coords[1] )
    {}

    template< class T >
    Vector2< T >::Vector2( const Vector2& vec ) :
        m_x( vec.m_x ),
        m_y( vec.m_y )
    {}

} // namespace Math

} // namespace Jungle

#include <jungle/math/Vector2.inl>

#endif // !defined __INCLUDED__MATH__VECTOR2_HPP__
// EOF

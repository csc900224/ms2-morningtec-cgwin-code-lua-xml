//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/BinomialCoeff.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki   <krystian.kostecki@gmail.com>
//                          <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2012, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDED__MATH_BINOMIAL_COEFF_HPP__
#define __INCLUDED__MATH_BINOMIAL_COEFF_HPP__

// Internal includes
#include <jungle/math/Factorial.hpp>

namespace Jungle
{
namespace Math
{
    //! BinomialCoefficient represents family of positive integers that occur as coefficients in the binomial theorem.
    /*!
    * The coefficient of binomial theorem: X^k in (1 + X)^n. Under some circumstances the value of
    * coefficient is given by expression n! / k! (n-k)! and known as the Newton Symbol.
    * Ussually indexed with n and k wich is written as ( n / k) - "n choose k", binormal coefficients
    * when arranged into rows of succesive n values, with k ranging from 0 to n gives special triangular
    * array called Pascal's triangle.
    * This family of numbers is widelly used in combinatorics:
    * For any set of n elements the number of distinct k-element subsets may be expressed with binomial coefficient,
    * that is why it is ofen called "n choose k".
    * Their recursive formula is defined as:
    * ( n / 0 ) = 1,    for n >= 0,
    * ( 0 / k ) = 0,    for k > 0,
    * ( n / k ) = ( n - 1 / k - 1 ) + ( n - 1 / k ),    for all n,k > 0
    * Note that equal factorial formula is often used:
    * ( n / k ) = n! / k! (n-k)!
    * \note Binormal coefficient are implemented here only for "integer"/discreet values. The template definition,
    * may even not compile if defined for floating point types: BinomialCoeff<float>.
    */
    template < class T >
    class BinomialCoeff
    {
    public:
        typedef T                   value_type;
        typedef BinomialCoeff< T >  self_type;

        //! Get value of coefficient ( n choose k).
        /*!
        * This value gives the number of ways (disregarding order) that k objects can be chosen
        * from among n objects.
        * \note this function will assert if supplied with bigger n! then
        * the resulting value type can hold. In such case try to use BinomialCoeff template
        * with a bigger base type etc. BinomialCoeff64i.
        */
        static inline T     GetValue( T n, T k );

        //! Get maximum input n value for a given k this BinomialCoeff instance may handle.
        static inline T     GetMaxN( T k = 1 );

        //! Get maximum input k value for a given n - it simply equals n, cause n >= k.
        /*
        * For all k > n the binomial coefficient returns equals 0 and it will 
        * return 0 for k == n -> (n / n) case.
        */
        static inline T     GetMaxK( T n );

    }; // class BinomialCoeff

    // Common types definitions
    typedef BinomialCoeff< unsigned long long > BinomialCoeff64i;
    typedef BinomialCoeff< unsigned int >       BinomialCoeff32i;
    typedef BinomialCoeff< unsigned short >     BinomialCoeff16i;
    typedef BinomialCoeff< unsigned char >      BinomialCoeff8i;

    template < class T >
    inline T BinomialCoeff< T >::GetValue( T n, T k )
    {
        JUNGLE_ASSERT_MSG( n >= 0, "You can not define binomial coeff for n smaller then zero." );
        JUNGLE_ASSERT_MSG( k >= 0, "The k element must be always greater then zero." );
        JUNGLE_ASSERT_MSG( n <= FactorialTraits< T >::MAX_N, "Can not handle such big n, the formula will be greater then capability of the type used." );
        // ( n / 0 ) == 1, for all n >= 0 (note also (0 / 0) == 1)
        if( k == 0 )
            return 1;
        // ( 0 / k ) == 0, for all k > 0 (note: for k == 0 we previous scenario)
        // ( n / k ) == 0, for all k > n
        if( k > n ) // this solves also ( n == 0 ), because here k > 0
            return 0;
        // ( n / n ) == 1, for all n
        if( k == n )
            return 1;
        // ( n / 1 ) == n, this may also be solved by iteration, simply fast exit
        if( k == 1 )
            return n;

        // Here we use more efficient multiplicative formula:
        // ( n - ( k - i ) ) / i for each( i=1 to k )
        // after expansion:
        // ( n / k ) = ( n * (n-1) *...* (n-k+1) ) / 1 * 2 * ... *k
        // cause we may only use discreet numbers we can use some properties:
        // - from every 2 successive natural numbers at least on is odd (dividable by 2),
        // - from every 3 successive natural numbers at least on may be divided by 3 (with discreet result),
        // - from every 4 successive natural numbers at least on may be divided by 4, etc.
        // This assures us that: n * (n - 1) may be divided by 2, so the result of n * (n-1) / 2 is integer value,
        // moreover expression: n * (n - 1)*(n - 2) (3 consecutive numbers) may be divided by 3 (as also by 2), so
        // previously calculated value (n * (n-1) / (1 + 1)) multiplied with (n-2) is dividable by 3 so
        // n*(n-1)*(n-2) / (1 * 2 * 3) will certainly be integer number.
        T ret = 1;
        for( T i = 1; i <= k; ++i )
        {
            ret = ret * ( n - i + 1 ) / i;
        }
        JUNGLE_ASSERT( ret == Factorial< T >::GetValue( n ) / ( Factorial< T >::GetValue( k ) * Factorial< T >::GetValue(n - k) ) );
        return ret;
    }

    template < class T >
    inline T BinomialCoeff< T >::GetMaxN( T k )
    {
        // TODO: This is true for factorial formula: n! / ( k!(n-k)! )
        // Find some other max n fomulation.
        return FactorialTraits< T >::MAX_N;
    }

    template < class T >
    inline T BinomialCoeff< T >::GetMaxK( T n )
    {
        return n;
    }

} // namespace Math

} // namespace Jungle

#endif // !defined __INCLUDED__MATH_FUZZY_VALUE_HPP__
// EOF

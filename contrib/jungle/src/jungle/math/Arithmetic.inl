//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Arithmetic.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/Errors.hpp>

// External includes
#include <math.h>                   // fabs, modf, floor, ceil, sqrt

namespace Jungle
{
namespace Math
{
    template < class T >
    inline T Mul( T a, T b )
    {
        return a * b;
    }

    template < class T >
    inline T Div( T a, T b )
    {
        JUNGLE_ASSERT( b != 0 );
        return a / b;
    }

    template < class T >
    inline T Div2( T a )
    {
        return a / NumberTraits< T >::TWO;
    }

    template <>
    inline float Div2( float a )
    {
        return a * 0.5f;
    }

    template <>
    inline double Div2( double a )
    {
        return a * 0.5;
    }

    template < class T >
    inline T Min( T a, T b )
    {
        return a < b ? a : b;
    }

    template < class T >
    inline T Max( T a, T b )
    {
        return a > b ? a : b;
    }

    template < class T >
    inline T MinMax( T val, T min, T max )
    {
        JUNGLE_ASSERT( min <= max );

        return Min( Max( val, min ), max );
    }

    template < class T >
    inline T Abs( T a )
    {
        return a < 0 ? -a : a;
    }

    template <>
    inline float Abs( float a )
    {
        return fabsf( a );
    }

    template <>
    inline double Abs( double a )
    {
        return fabs( a );
    }

    template < class T >
    inline T Mod( T numerator, T denominator )
    {
        return numerator % denominator;
    }

    template <>
    inline float Mod( float numerator, float denominator )
    {
        return fmodf( numerator, denominator );
    }

    template <>
    inline double Mod( double numerator, double denominator )
    {
        return fmod( numerator, denominator );
    }

    template < class T >
    inline T Round( T a )
    {
        return static_cast< T >( static_cast< long long >( a + ( a > 0 ? NumberTraits<T>::Half() : -NumberTraits<T>::Half() ) ) );
    }

    template < class T >
    inline T Floor( T a )
    {
        return static_cast< T >( floor( (double)a ) );
    }

    template < class T >
    inline T Ceil( T a )
    {
#if 0
        return static_cast< T > ( static_cast< int >( a + ( a > 0 ? NumberTraits<T>::Half() : -NumberTraits<T>::Half() ) ) );
#else
        return static_cast< T >( ceil( (double) a ) );
#endif
    }

    template < class T >
    inline T Clamp( T val, T low, T high )
    {
        JUNGLE_ASSERT( low <= high );
        return ( val > high ) ? high : ( ( val < low ) ? low : val );
    }

    template < class T >
    inline bool Equal( T val0, T val1 )
    {
        return Abs( val0 - val1 ) < NumberTraits< T >::Epsilon();
    }

    template < class T >
    inline bool Equal( T val0, T val1, T precision )
    {
        return Abs( val0 - val1 ) < precision;
    }

    template < class T >
    inline int Compare( T val0, T val1 )
    {
        if( Equal( val0, val1 ) )
            return 0;
        else
            return val0 > val1 ? 1 : -1;
    }

    template < class T >
    inline int Compare( T val0, T val1, T precision )
    {
        if( Equal( val0, val1, precision ) )
            return 0;
        else
            return val0 > val1 ? 1 : -1;
    }

    template < class T >
    inline T Scale( T val, T lowIn, T highIn, T lowOut, T highOut )
    {
        return ( Clamp( val, lowIn, highIn ) - lowIn ) / ( highIn - lowIn ) * ( highOut - lowOut ) + lowOut;
    }

    template < class T >
    inline T BiasAndScale( const T& val, const T& bias, const T& scale )
    {
        return ( val + bias ) * scale;
    }

    template < class T >
    inline T ScaleAndBias( const T& val, const T& scale, const T& bias )
    {
        return ( val * scale ) + bias;
    }

    template < class T >
    inline T Sign( T val )
    {
        if( val < NumberTraits< T >::ZERO )
        {
            return -NumberTraits< T >::ONE;
        }
        return NumberTraits< T >::ONE;
    }

    template < class TReal >
    inline TReal Sqr( TReal val )
    {
        return val * val;
    }

    template < class TReal >
    inline TReal SqrSum2( TReal val0, TReal val1 )
    {
        return val0 * val0 + val1 * val1;
    }

    template < class TReal >
    inline TReal Sqrt( TReal val )
    {
        JUNGLE_ASSERT( val >= NumberTraits< TReal >::ZERO );

        return static_cast< TReal >( sqrt( val ) );
    }

    template <>
    inline float Sqrt( float val )
    {
        JUNGLE_ASSERT( val >= NumberTraits< float >::ZERO );

        // TODO: Provide float specific faster implementation
        return sqrtf( val );
    }

    template < class TReal >
    inline TReal FastSqrt( TReal val )
    {
        // TODO: Check the accuracy
        return static_cast< TReal >( FastSqrt( static_cast< float >( val ) ) );
    }

    template <>
    inline float FastSqrt( float val )
    {
        long i;
        float x, y;
        const float f = 1.5f;

        x = val * 0.5f;
        y = val;
        i = * ( long * ) &y;
        i = 0x5f3759df - ( i >> 1 );
        y = * ( float * ) &i;
        y = y * ( f - ( x * y * y ) );
        y = y * ( f - ( x * y * y ) );
        return val * y;
    }

    template < class TReal >
    inline TReal InvSqrt( TReal val )
    {
        return NumberTraits< TReal >::ONE / Sqrt( val );
    }

    template < class TReal >
    inline TReal FastInvSqrt( TReal val )
    {
        // TODO: Check accuracy of both approaches
        // also: NumberTraits< TReal >::ONE / FastSqrt( val );
        return static_cast< TReal >( FastInvSqrt( static_cast< float >( val ) ) );
    }

    template <>
    inline float FastInvSqrt( float val )
    {
        // Lemont inverted square root implementation
        float half = 0.5f * val;
        int i = *(int*)&val;                        // get bits for floating value
        i = 0x5f375a86- (i>>1);                     // gives initial guess y0
        val = *(float*)&i;                          // convert bits back to float
        val = val * ( 1.5f - half * val * val );    // Newton step, repeating increases accuracy
        return val;
    }

} // namespace Math

} // namespace Jungle
// EOF

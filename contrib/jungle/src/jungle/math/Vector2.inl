//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      jungle/math/Vector2.inl
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//
//  Copyright (c) 2009, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include <jungle/base/Errors.hpp>           // JUNGLE_ASSERT, JUNGLE_WARNING_MSG
#include <jungle/math/Arithmetic.hpp>       // Sqrt
#include <jungle/math/Trigonometry.hpp>     // SinRad, CosRad

namespace Jungle
{
namespace Math
{
    template < class T >
    inline void Vector2< T >::Set( T x, T y )
    {
        m_x = x;
        m_y = y;
    }

    template < class T >
    inline void Vector2< T >::Zero()
    {
        m_x = m_y = NumberTraits< T >::ZERO;
    }

    /*
    template < class T >
    inline Vector2< T >::operator const T* () const
    {
        return m_coords;
    }

    template < class T >
    inline Vector2< T >::operator T* ()
    {
        return m_coords;
    }
    */

    template < class T >
    inline T Vector2< T >::operator [] ( index_type i ) const
    {
        JUNGLE_ASSERT( i < 2 );
        return m_coords[i];
    }

    template < class T >
    inline T& Vector2< T >::operator [] ( index_type i )
    {
        JUNGLE_ASSERT( i < 2 );
        return m_coords[i];
    }

    template < class T >
    inline bool Vector2< T >::operator == ( const Vector2< T >& v ) const
    {
        return  Math::Abs( m_x - v.m_x ) < NumberTraits<T>::ZERO_TOLERANCE &&
                Math::Abs( m_y - v.m_y ) < NumberTraits<T>::ZERO_TOLERANCE;
    }

    template < class T >
    inline bool Vector2< T >::operator != ( const Vector2< T >& v ) const
    {
        return !operator==( v );
    }

    template < class T >
    inline bool Vector2< T >::operator > ( const Vector2< T >& v ) const
    {
        return ( m_x > v.m_x ) && ( m_y > v.m_y );
    }

    template < class T >
    inline bool Vector2< T >::operator >= ( const Vector2< T >& v ) const
    {
        return ( m_x >= v.m_x ) && ( m_y >= v.m_y );
    }

    template < class T >
    inline bool Vector2< T >::operator < ( const Vector2< T >& v ) const
    {
        return ( m_x < v.m_x ) && ( m_y < v.m_y );
    }

    template < class T >
    inline bool Vector2< T >::operator <= ( const Vector2< T >& v ) const
    {
        return ( m_x <= v.m_x ) && ( m_y <= v.m_y );
    }

    template < class T >
    inline Vector2< T >& Vector2< T >::operator += ( const Vector2< T >& v )
    {
        m_x += v.m_x;
        m_y += v.m_y;

        return *this;
    }

    template < class T >
    inline Vector2< T >& Vector2< T >::operator -= ( const Vector2< T >& v )
    {
        m_x -= v.m_x;
        m_y -= v.m_y;

        return *this;
    }

    template < class T >
    inline Vector2< T >& Vector2< T >::operator *= ( const Vector2< T >& v )
    {
        m_x = ( m_x * v.m_x );
        m_y = ( m_y * v.m_y );

        return *this;
    }

    template < class T >
    inline Vector2< T >& Vector2< T >::operator /= ( const Vector2< T >& v )
    {
        m_x = ( m_x / v.m_x );
        m_y = ( m_y / v.m_y );

        return *this;
    }

    template < class T >
    inline Vector2< T >& Vector2< T >::operator *= ( T f )
    {
        m_x = ( m_x * f );
        m_y = ( m_y * f );

        return *this;
    }

    template < class T >
    inline Vector2< T >& Vector2< T >::operator /= ( T f )
    {
        if( f != NumberTraits< T >::ZERO )
        {
            m_x = ( m_x / f );
            m_y = ( m_y / f );
        }
        else
        {
            JUNGLE_WARNING_MSG( "Division by scalar equal to ZERO." );
            m_x = m_y = NumberTraits< T >::Maximum();
        }
        return *this;
    }

    template < class T >
    inline Vector2< T > Vector2< T >::operator - () const
    {
        Vector2< T > ret = Vector2< T >( -m_x, -m_y );

        return ret;
    }

    template < class T >
    inline Vector2< T > Vector2< T >::operator * ( T f ) const
    {
        Vector2< T > ret = Vector2< T >( (m_x * f), (m_y * f) );

        return ret;
    }

    template < class T >
    inline Vector2< T > Vector2< T >::operator / ( T f ) const
    {
        Vector2<T> ret( *this );
        ret /= f;
        return ret;
    }

    template < class T >
    inline T Vector2< T >::Dot(const Vector2< T >& v ) const
    {
        return (m_x * v.m_x) + (m_y * v.m_y);
    }

    template < class T >
    inline T Vector2< T >::Cross(const Vector2< T >& v ) const
    {
        return (m_x * v.m_y) - (m_y * v.m_x);
    }

    template < class T >
    inline T Vector2< T >::Length() const
    {
        return static_cast< T >( Math::Sqrt( LengthSqr() ) );
    }

    template < class T >
    inline T Vector2< T >::LengthSqr() const
    {
        return (m_x * m_x) + (m_y * m_y);
    }

    template < class T >
    inline T Vector2< T >::Normalize()
    {
        T length = Length();
        if( length > NumberTraits< T >::Epsilon() )
        {
            m_x = ( m_x / length );
            m_y = ( m_y / length );
        }
        else
        {
            m_x = m_y = NumberTraits< T >::ZERO;
            length = NumberTraits< T >::ZERO;
        }
        return length;
    }

    template < class T >
    inline Vector2< T > Vector2< T >::Normalized() const
    {
        Vector2< T > normalized( *this );
        normalized.Normalize();
        return normalized;
    }

    template < class T >
    inline Vector2< T > Vector2< T >::Perpendicular() const
    {
        return Vector2( -m_y, m_x );
    }

    template < class T >
    inline Vector2< T > Vector2< T >::PerpendicularNorm() const
    {
        Vector2 perp( -m_y, m_x );
        perp.Normalize();
        return perp;
    }

    template < class T >
    inline T Vector2< T >::Mag( const Vector2< T >& v )
    {
        return static_cast< T >( Math::Sqrt( (v.m_x * v.m_x) + (v.m_y * v.m_y) ) );
    }

    template < class T >
    inline T Vector2< T >::Distance(const Vector2< T >& a, const Vector2< T >& b )
    {
        return static_cast< T >( Math::Sqrt( DistanceSqr( a, b ) ) );
    }

    template < class T >
    inline T Vector2< T >::DistanceSqr(const Vector2< T >& a, const Vector2< T >& b )
    {
        T dx = a.m_x - b.m_x;
        T dy = a.m_y - b.m_y;

        return (dx * dx) + (dy * dy);
    }

    template < class T >
    void Vector2< T >::RotateVector( T radAngle, const Vector2< T >& inVec, Vector2< T >& outVec )
    {
        register T sin, cos;
        sin = Math::SinRad( radAngle );
        cos = Math::CosRad( radAngle );

        // For safety make temp variable
        // this secures situation when source vector is the same as destination
        T temp = inVec.x;
        outVec.x = (cos * temp)    - (sin * inVec.y);
        outVec.y = (cos * inVec.y) + (sin * temp);
    }

    template < class T >
    const inline Vector2< T > operator + ( const Vector2< T >& v1, const Vector2< T >& v2 )
    {
        Vector2< T > ret( v1 );
        ret += v2;

        return ret;
    }

    template < class T >
    const inline Vector2< T > operator - ( const Vector2< T >& v1, const Vector2< T >& v2 )
    {
        Vector2< T > ret( v1 );
        ret -= v2;

        return ret;
    }

    template < class T >
    const inline Vector2< T > operator * ( const Vector2< T >& v1, const Vector2< T >& v2 )
    {
        Vector2< T > ret( v1 );
        ret *= v2;

        return ret;
    }

    template < class T >
    const inline T operator ^ ( const Vector2< T >& v1, const Vector2< T >& v2 )
    {
        return v1.Cross( v2 );
    }

    template < class T >
    const inline Vector2< T > operator / ( const Vector2< T >& v1, const Vector2< T >& v2 )
    {
        Vector2< T > ret( v1 );
        ret /= v2;

        return ret;
    }

    template < class T >
    const inline Vector2< T > operator * ( T scalar, const Vector2< T >& v )
    {
        return Vector2< T >( v.m_x * scalar, v.m_y * scalar );
    }

    template < class T >
    inline Vector2<T> Min( const Vector2<T>& a, const Vector2<T>& b )
    {
        Vector2<T> res( a );
        if( res.m_x > b.m_x )   res.m_x = b.m_x;
        if( res.m_y > b.m_y )   res.m_y = b.m_y;
        return res;
    }

    template < class T >
    inline Vector2<T> Max( const Vector2<T>& a, const Vector2<T>& b )
    {
        Vector2<T> res( a );
        if( res.m_x < b.m_x )   res.m_x = b.m_x;
        if( res.m_y < b.m_y )   res.m_y = b.m_y;
        return res;
    }

} // namespace Math

} // namespace Jungle
// EOF

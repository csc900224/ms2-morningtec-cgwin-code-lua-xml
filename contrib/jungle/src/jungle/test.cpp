// Internal includes
#include <jungle/test.hpp>

// Base
#include <jungle/base/Types.hpp>
#include <jungle/base/Time.hpp>
#include <jungle/base/Rand.hpp>
#include <jungle/base/Errors.hpp>
#include <jungle/base/NumberTraits.hpp>

// Math
#include <jungle/math/Arithmetic.hpp>
#include <jungle/math/Trigonometry.hpp>
#include <jungle/math/Vector2.hpp>
#include <jungle/math/Vector3.hpp>
#include <jungle/math/Factorial.hpp>
#include <jungle/math/BinomialCoeff.hpp>

// Math/Geo
#include <jungle/math/geo/AABB.hpp>
#include <jungle/math/geo/AABB2.hpp>
#include <jungle/math/geo/Sphere.hpp>
#include <jungle/math/geo/Sphere2.hpp>
#include <jungle/math/geo/OBB2.hpp>
#include <jungle/math/geo/Line.hpp>
#include <jungle/math/geo/Line2.hpp>
#include <jungle/math/geo/Ray.hpp>
#include <jungle/math/geo/Ray2.hpp>
#include <jungle/math/geo/Segment.hpp>
#include <jungle/math/geo/Segment2.hpp>

// Math/Approx
#include <jungle/math/approx/Polynomial1.hpp>

// Math/Stats
#include <jungle/math/stats/DistributionNormal.hpp>
#include <jungle/math/stats/DistributionGauss.hpp>
#include <jungle/math/stats/Median.hpp>
#include <jungle/math/stats/TestRuns.hpp>

// Scene
#include <jungle/scene/SceneDebug.hpp>

// Patterns
#include <jungle/patterns/Singleton.hpp>

// Rtti
#include <jungle/rtti/TypeHierarchy.hpp>

// Scene
#include <jungle/scene/SceneObject.hpp>
#include <jungle/scene/ContactPoint.hpp>
#include <jungle/scene/ContactSet.hpp>
#include <jungle/scene/CollisionQuery.hpp>

// Utils
#include <jungle/utils/Round.hpp>

// External includes
#include <list>

namespace Jungle
{
    class SampleClass : Patterns::Singleton<SampleClass>
    {
    };

    class SampleBv;

    class SampleSceneObj : Scene::SceneObject< SampleBv >
    {
        virtual CollisionObj*           GetCollisionObj()       { return NULL; }
        virtual RenderObj*              GetRenderObj()          { return NULL; }

        virtual const SampleBv*         GetRenderWorldBV() const    { return NULL; }
        virtual const SampleBv*         GetCollisionWorldBV() const { return NULL; }
    };

    class DebugRenderer : public Scene::Debug::Renderer
    {
    public:
        virtual void    DrawSegment( const Scene::Vector2& x0, const Scene::Vector2& v0, unsigned int color, void* user /*= 0*/ ) {}

        virtual void    DrawRect( const Scene::Vector2& center, const Scene::Vector2& extentX, const Scene::Vector2& extentY, unsigned int color, void* user /*= 0*/ ) {}

        virtual void    DrawCircle( const Scene::Vector2& pos, const Scene::Real& rad, unsigned int color, void* user /*= 0*/ ) {}
    };

    class TypeA
    {
        JUNGLE_DECLARE_TYPE_HIERARCHY;
    };

    JUNGLE_DEFINE_TYPE_HIERARCHY_0( Jungle, TypeA );

    class TypeB
    {
        JUNGLE_DECLARE_TYPE_HIERARCHY;
    };
    JUNGLE_DEFINE_TYPE_HIERARCHY_BASE( Jungle, TypeB );

    class TypeDerivedA : public TypeA
    {
        JUNGLE_DECLARE_TYPE_HIERARCHY;
    };

    JUNGLE_DEFINE_TYPE_HIERARCHY( NULL, TypeDerivedA, TypeA );

    class TypeDerivedAB : public TypeA, public TypeB
    {
        JUNGLE_DECLARE_TYPE_HIERARCHY;
    };

    //JUNGLE_DEFINE_TYPE_HIERARCHY_2( Jungle, TypeDerivedAB, TypeA, TypeB );
    JUNGLE_DEFINE_TYPE_HIERARCHY_LIST( Jungle, TypeDerivedAB, &TypeA::TYPE, &TypeB::TYPE, NULL );

    Test::Test()
    {
        TestBase();
        TestPatterns();
        TestUtils();
        TestMath();
        TestMathGeo();
        TestMathApprox();
        TestMathStats();
        TestScene();
        TestRtti();
    }

    void Test::TestBase()
    {
        uint8   ui8(0);
        uint16  ui16(0);
        uint32  ui32(0);
        uint64  ui64(0);

        int8    i8(0);
        int16   i16(0);
        int32   i32(0);
        int64   i64(0);

        f32     fl32(0.0f);
        f64     fl64(0.0);

        GetTimeSec();
        GetTimeMiliSec();
        GetTimeMicroSec();

        Rand();
        RandRange( 'a', 'z' );
        RandRange( 2.0f, 3.0f );

        JUNGLE_ASSERT( true );
        //JUNGLE_ASSERT( false );
        JUNGLE_ASSERT_MSG( true, "Assertion of true");
        //JUNGLE_ASSERT_MSG( false, "Assertion of false");

        ////////////////////////////////////////////////////////////////////////////////
        // NumberTraits tests
        ////////////////////////////////////////////////////////////////////////////////

        // JUNGLE_ASSERT( NumberTraits< uint8 >::Half() == 0 );     // This shouldn't compile
        JUNGLE_ASSERT( NumberTraits< uint8 >::Minimum() == 0 );
        JUNGLE_ASSERT( NumberTraits< uint8 >::Maximum() == 255 );
        JUNGLE_ASSERT( NumberTraits< uint8 >::ONE == 1 );
        JUNGLE_ASSERT( NumberTraits< uint8 >::PI == 3 );
        JUNGLE_ASSERT( NumberTraits< uint8 >::BITS_NUM == 8 );
        JUNGLE_ASSERT( NumberTraits< uint8 >::USIGN_BITS_NUM == 8 );
        JUNGLE_ASSERT( NumberTraits< uint8 >::ZERO == 0 );

        // JUNGLE_ASSERT( NumberTraits< int16 >::Half() == 0 );     // This shouldn't compile
        JUNGLE_ASSERT( NumberTraits< int16 >::Minimum() == -32767 );
        JUNGLE_ASSERT( NumberTraits< int16 >::Maximum() == 32767 );
        JUNGLE_ASSERT( NumberTraits< int16 >::Epsilon() == 1 );
        JUNGLE_ASSERT( NumberTraits< int16 >::ONE == 1 );
        JUNGLE_ASSERT( NumberTraits< int16 >::PI == 3 );
        JUNGLE_ASSERT( NumberTraits< int16 >::BITS_NUM == 16 );
        JUNGLE_ASSERT( NumberTraits< int16 >::USIGN_BITS_NUM == 15 );
        JUNGLE_ASSERT( NumberTraits< int16 >::ZERO == 0 );

        JUNGLE_ASSERT( NumberTraits< f32 >::Half() == 0.5f );
        JUNGLE_ASSERT( NumberTraits< f32 >::Minimum() == NumberTraits< f32 >::Minimum() - 1.0f );
        JUNGLE_ASSERT( NumberTraits< f32 >::Maximum() == NumberTraits< f32 >::Maximum() + 1.0f );
        JUNGLE_ASSERT( NumberTraits< f32 >::Epsilon() > 0.0f && NumberTraits< f32 >::Epsilon() < 1.0f );
        JUNGLE_ASSERT( NumberTraits< f32 >::ONE == 1.0f );
        JUNGLE_ASSERT( NumberTraits< f32 >::BITS_NUM == 32 );
        JUNGLE_ASSERT( NumberTraits< f32 >::USIGN_BITS_NUM == 31 );
        JUNGLE_ASSERT( NumberTraits< f32 >::ZERO == 0.0f );

    }

    void Test::TestPatterns()
    {
        using namespace Patterns;

        Singleton<SampleClass>::CreateInstance();
        SampleClass* ptr = Singleton<SampleClass>::GetInstance();
        Singleton<SampleClass>::Release();
    }

    void Test::TestUtils()
    {
        using namespace Utils;

        // Bits aligned round functions
        JUNGLE_ASSERT( RoundUp( 11, 2 ) == 12 );
        JUNGLE_ASSERT( RoundDown( 31, 1 ) == 31 );
    }

    void Test::TestMath()
    {
        using namespace Math;

        ////////////////////////////////////////////////////////////////////////////////
        // Arithmetic tests
        ////////////////////////////////////////////////////////////////////////////////

        JUNGLE_ASSERT( Min( 1., 1.5 ) == 1. );
        JUNGLE_ASSERT( Min( -0.5, 1. ) == -0.5 );
        JUNGLE_ASSERT( Max( 0.5, 1. ) == 1. );
        JUNGLE_ASSERT( Max( -1.0, 0.5 ) == 0.5 );
        JUNGLE_ASSERT( Abs( -1 ) == 1 );
        JUNGLE_ASSERT( Abs( -1.000001 ) == 1.000001 );
        JUNGLE_ASSERT( Abs( 1.0f ) == 1.0f );
        JUNGLE_ASSERT( Abs( Mod( 1.00001, 1. ) - 0.00001 ) < NumberTraits<f64>::Epsilon() );
        JUNGLE_ASSERT( Abs( Mod( -1.00001, 1. ) + 0.00001 ) < NumberTraits<f64>::Epsilon() );
        JUNGLE_ASSERT( Mod( 4.5f, 1.5f ) == 0.0f );
        JUNGLE_ASSERT( Mod( -5.0f, 2.f ) == -1.0f );
        JUNGLE_ASSERT( Mod( 5.0f, -2.f ) == 1.0f );
        JUNGLE_ASSERT( Round( 1.0f ) == 1.f );
        JUNGLE_ASSERT( Round( 1.1f ) == 1.f );
        JUNGLE_ASSERT( Round( 1.5 ) == 2. );
        JUNGLE_ASSERT( Round( -1.4f ) == -1.f );
        JUNGLE_ASSERT( Round( -1.5f ) == -2.f );
        //JUNGLE_ASSERT( Round( -1 ) == -1 );       // This won't compile - can not get half value for integers
        JUNGLE_ASSERT( Ceil( -1 ) == -1 );
        JUNGLE_ASSERT( Ceil( -1.1 ) == -1. );
        JUNGLE_ASSERT( Ceil( -1.9 ) == -1. );
        JUNGLE_ASSERT( Ceil( 1.9 ) == 2. );
        JUNGLE_ASSERT( Ceil( 0.1f ) == 1.f );
        JUNGLE_ASSERT( Ceil( 1 ) == 1 );
        JUNGLE_ASSERT( Ceil( -1 ) == -1 );

        JUNGLE_ASSERT( Floor( -1.1 ) == -2. );
        JUNGLE_ASSERT( Floor( -1.9 ) == -2. );
        JUNGLE_ASSERT( Floor( 1.9 ) == 1. );
        JUNGLE_ASSERT( Floor( 0.1f ) == 0.f );
        JUNGLE_ASSERT( Floor( 1 ) == 1 );

        JUNGLE_ASSERT( Sqr( 2 ) == 4 );
        JUNGLE_ASSERT( Sqr( -2. ) == 4. );
        JUNGLE_ASSERT( Sqr( -1.f ) == 1.f );
        JUNGLE_ASSERT( Sqrt( 4. ) == 2. );
        JUNGLE_ASSERT( Sqrt( 9.f ) == 3.f );
        JUNGLE_ASSERT( Abs( FastSqrt( 4. ) - 2. ) < 0.00001 );
        JUNGLE_ASSERT( Abs( FastSqrt( 9.f ) - 3.f ) < 0.00001f );
        JUNGLE_ASSERT( InvSqrt( 4. ) == 1. / 2. );
        JUNGLE_ASSERT( InvSqrt( 9.f ) == 1.f / 3.f );
        JUNGLE_ASSERT( Abs( FastInvSqrt( 4. ) - 1. / 2. ) < 0.001 );
        JUNGLE_ASSERT( Abs( FastInvSqrt( 9.f ) - 1.f / 3.f ) < 0.001f );

        ////////////////////////////////////////////////////////////////////////////////
        // Trigonometry tests
        ////////////////////////////////////////////////////////////////////////////////

        JUNGLE_ASSERT( Deg2Rad( 90.f ) == NumberTraits<float>::HALF_PI );
        JUNGLE_ASSERT( Deg2Rad( -360.f - 90.f ) == -NumberTraits<float>::TWO_PI - NumberTraits<float>::HALF_PI );
        JUNGLE_ASSERT( Deg2Rad( 180 ) == NumberTraits<int>::PI );
        JUNGLE_ASSERT( Rad2Deg( NumberTraits<double>::PI ) == 180. );
        JUNGLE_ASSERT( Rad2Deg( -NumberTraits<float>::HALF_PI ) == -90.f );
        JUNGLE_ASSERT( Rad2Deg( -NumberTraits<int>::HALF_PI ) == -60 );     // Note HALF_PI truncation to 1 gives (1 * 180 / 3 )

        const float TRIG_TOLERANCE = 0.00001f;
        const float TRIG_TOLERANCE_2 = 0.1f;

        JUNGLE_ASSERT( Equal( SinRad( NumberTraits<float>::PI ), 0.f ) );
        JUNGLE_ASSERT( Equal( SinRad( -NumberTraits<float>::HALF_PI ), -1.f ) );
        JUNGLE_ASSERT( Equal( SinRad( NumberTraits<double>::TWO_PI + NumberTraits<double>::HALF_PI ), 1. ) );
        JUNGLE_ASSERT( Equal( SinDeg( -30.f ), -0.5f ) );
        JUNGLE_ASSERT( Equal( SinDeg( 180.f ), 0.0f ) );
        JUNGLE_ASSERT( Equal( SinDeg( 360.f + 30.f ), 0.5f ) );
        JUNGLE_ASSERT( Equal( SinDeg( -720.f - 45.f ), -Sqrt( 2.0f ) / 2.0f, TRIG_TOLERANCE ) );

        JUNGLE_ASSERT( Equal( FastSin0( NumberTraits<float>::PI ), 0.f, TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( FastSin0( -NumberTraits<float>::HALF_PI ), -1.f, TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( FastSin0( NumberTraits<double>::TWO_PI + NumberTraits<double>::HALF_PI ), 1. , (double)TRIG_TOLERANCE ) );

        JUNGLE_ASSERT( Equal( FastSin1( NumberTraits<float>::PI ), 0.f, TRIG_TOLERANCE_2 ) );
        JUNGLE_ASSERT( Equal( FastSin1( -NumberTraits<float>::HALF_PI ), -1.f, TRIG_TOLERANCE_2 ) );
        JUNGLE_ASSERT( Equal( FastSin1( NumberTraits<double>::TWO_PI + NumberTraits<double>::HALF_PI ), 1. , (double)TRIG_TOLERANCE_2 ) );

        JUNGLE_ASSERT( Equal( CosRad( NumberTraits<float>::PI ), -1.f ) );
        JUNGLE_ASSERT( Equal( CosRad( -NumberTraits<float>::HALF_PI ), 0.f ) );
        JUNGLE_ASSERT( Equal( CosRad( NumberTraits<double>::TWO_PI + NumberTraits<double>::HALF_PI ), 0., (double)TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( CosDeg( -60.f ), 0.5f ) );
        JUNGLE_ASSERT( Equal( CosDeg( 180.f ), -1.0f ) );
        JUNGLE_ASSERT( Equal( CosDeg( 360.f + 60.f ), 0.5f, TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( CosDeg( -720.f - 45.f ), Sqrt( 2.0f ) / 2.0f, TRIG_TOLERANCE ) );

        JUNGLE_ASSERT( Equal( FastCos0( -NumberTraits<float>::PI ), -1.f, TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( FastCos0( -NumberTraits<float>::HALF_PI ), 0.f, TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( FastCos0( NumberTraits<double>::TWO_PI + NumberTraits<double>::HALF_PI ), 0. , (double)TRIG_TOLERANCE ) );

        JUNGLE_ASSERT( Equal( FastCos1( -NumberTraits<float>::PI ), -1.f, TRIG_TOLERANCE_2 ) );
        JUNGLE_ASSERT( Equal( FastCos1( -NumberTraits<float>::HALF_PI ), 0.f, TRIG_TOLERANCE_2 ) );
        JUNGLE_ASSERT( Equal( FastCos1( NumberTraits<double>::TWO_PI + NumberTraits<double>::HALF_PI ), 0. , (double)TRIG_TOLERANCE_2 ) );

        JUNGLE_ASSERT( Equal( ACos( -1.f ), NumberTraits<float>::PI ) );
        JUNGLE_ASSERT( Equal( ACos( 0.f ), NumberTraits<float>::HALF_PI ) );
        JUNGLE_ASSERT( Equal( ACos( 0.5 ), Deg2Rad( 60. ), (double)TRIG_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( ACos( Sqrt( 2.0f ) / 2.0f ), Deg2Rad( 45.f ) ) );

        JUNGLE_ASSERT( Equal( FastACos( -1.f ), NumberTraits<float>::PI ) );        // Value is clamped so maybe checked accuratelly, otherwise would not fit
        JUNGLE_ASSERT( Equal( FastACos( 0.f ), NumberTraits<float>::HALF_PI, TRIG_TOLERANCE_2 ) );
        JUNGLE_ASSERT( Equal( FastACos( 0.5 ), Deg2Rad( 60. ), (double)TRIG_TOLERANCE_2 ) );
        JUNGLE_ASSERT( Equal( FastACos( Sqrt( 2.0f ) / 2.0f ), Deg2Rad( 45.f ), TRIG_TOLERANCE_2 ) );

        ////////////////////////////////////////////////////////////////////////////////
        // Vector2 tests
        ////////////////////////////////////////////////////////////////////////////////

        Vector2f v2a( 1.0f, 0.0f );
        Vector2f v2b( 2.0f, 1.0f );
        float s;

        JUNGLE_ASSERT( 2.f * v2b == v2b * 2 );
        JUNGLE_ASSERT( 2.f * v2b != v2b * 1.9f );

        v2a = v2b * 2;
        v2a = v2b / 2;

        v2a = v2b + v2b;                            JUNGLE_ASSERT( v2a == 2.f * v2b );
        v2a = v2a - v2b;                            JUNGLE_ASSERT( v2a == v2b );
        v2a = v2b * Vector2f::ONE;                  JUNGLE_ASSERT( v2a == v2b );
        s = v2b ^ v2b;                              JUNGLE_ASSERT( s == 0.0f );         // |a x b| = |a||b|sin(0)
        s = Vector2f::ONE_X ^ Vector2f::ONE_Y;      JUNGLE_ASSERT( s == SinDeg(90.f) ); // |a x b| = |a||b|sin(90)
        v2a = v2b / Vector2f::ONE;                  JUNGLE_ASSERT( v2a == v2b );

        v2a.Set( 3.0f, 4.0f );
        v2b.Set( 1.0f, -2.0f );

        // Lagrange identity
        JUNGLE_ASSERT( Sqr( ( v2a ^ v2b ) ) == v2a.LengthSqr() * v2b.LengthSqr() - Sqr( v2a.Dot( v2b ) ) );

        v2a += v2b;
        v2a -= v2b;
        v2a *= v2b;
        v2a /= v2b;

        v2a == v2b;
        v2a != v2b;
        v2a > v2b;
        v2a >= v2b;
        v2a < v2b;
        v2a <= v2b;

        ////////////////////////////////////////////////////////////////////////////////
        // Vector3 tests
        ////////////////////////////////////////////////////////////////////////////////

        Vector3f v3a( 1.0f, 0.0f, 1.0f );
        Vector3f v3b( 2.0f, 1.0f, 0.0f );

        JUNGLE_ASSERT( 2.f * v3b == v3b * 2 );
        JUNGLE_ASSERT( 2.f * v3b != v3b * 1.9f );

        v3a = v3b * 2;
        v3a = v3b / 2;

        v3a = v3b + v3b;                            JUNGLE_ASSERT( v3a == 2.f * v3b );
        v3a = v3a - v3b;                            JUNGLE_ASSERT( v3a == v3b );
        v3a = v3b * Vector3f::ONE;                  JUNGLE_ASSERT( v3a == v3b );
        v3a = v3b ^ v3b;                            JUNGLE_ASSERT( v3a.LengthSqr() == 0.0f );           // |a x b| = |a||b|sin(0)
        v3a = Vector3f::ONE_X ^ Vector3f::ONE_Y;    JUNGLE_ASSERT( v3a.Length() == SinDeg(90.f) );      // |a x b| = |a||b|sin(90)
        v3a = v3b / Vector3f::ONE;                  JUNGLE_ASSERT( v3a == v3b );

        v3a.Set( 3.0f, 4.0f, 2.0f );
        v3b.Set( 1.0f, -2.0f, 0.5f );

        // Lagrange identity
        JUNGLE_ASSERT( ( v3a ^ v3b ).LengthSqr() == v3a.LengthSqr() * v3b.LengthSqr() - Sqr( v3a.Dot( v3b ) ) );

        v3a += v3b;
        v3a -= v3b;
        v3a *= v3b;
        v3a /= v3b;

        v3a == v3b;
        v3a != v3b;
        v3a > v3b;
        v3a >= v3b;
        v3a < v3b;
        v3a <= v3b;

        ////////////////////////////////////////////////////////////////////////////////
        // Factorial tests
        ////////////////////////////////////////////////////////////////////////////////

        // Test (0)!
        JUNGLE_ASSERT( Factorial<char>::GetValue( 0 ) == 1 );
        JUNGLE_ASSERT( Factorial32i::GetValue( 0 ) == 1 );
        JUNGLE_ASSERT( Factorial64i::GetValue( 0 ) == 1UL );
        // Test (1)!
        JUNGLE_ASSERT( Factorial64i::GetValue( 1 ) == 1UL );
        JUNGLE_ASSERT( Factorial<int>::GetValue( 1 ) == 1 );
        // Test max
        JUNGLE_ASSERT( Factorial32i::GetMaxN() == 12 );
        JUNGLE_ASSERT( Factorial32i::GetMaxValue() == 479001600 );
        JUNGLE_ASSERT( Factorial64i::GetMaxN() == 20UL );
        JUNGLE_ASSERT( Factorial64i::GetMaxValue() == 2432902008176640000UL );
        // Test common values
        JUNGLE_ASSERT( Factorial32i::GetValue( 2 ) == 2 );
        JUNGLE_ASSERT( Factorial32i::GetValue( 3 ) == 6 );
        JUNGLE_ASSERT( Factorial32i::GetValue( 4 ) == 24 );

        ////////////////////////////////////////////////////////////////////////////////
        // Binomial coefficient tests
        ////////////////////////////////////////////////////////////////////////////////
        // Test (0 / 0) == 1
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 0, 0 ) == 1 );
        JUNGLE_ASSERT( BinomialCoeff32i::GetValue( 0, 0 ) == 1 );
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 0, 0 ) == 1UL );
        // Test (1 / 0) == 1
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 1, 0 ) == 1 );
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 1, 0 ) == 1 );
        // Test (n / 0) == 1
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 20, 0 ) == 1 );
        JUNGLE_ASSERT( BinomialCoeff32i::GetValue( 10, 0 ) == 1 );
        // Test (0 / 1) == 1
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 1, 0 ) == 1 );
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 1, 0 ) == 1 );
        // Test (1 / 1) == 1
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 1, 1 ) == 1UL );
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 1, 1 ) == 1 );
        // Test (n / n) == 1
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 20, 20 ) == 1UL );
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 10, 10 ) == 1 );
        // Test (n / 1) == n
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 20, 1 ) == 20 );
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 12, 1 ) == 12 );
        // Test (n / k)
        JUNGLE_ASSERT( BinomialCoeff64i::GetValue( 7, 2 ) == 21 );
        JUNGLE_ASSERT( BinomialCoeff<int>::GetValue( 3, 2 ) == 3 );
        BinomialCoeff<int>::GetValue( 10, 2 );
    }

    void Test::TestMathGeo()
    {
        using namespace Math;

        ////////////////////////////////////////////////////////////////////////////////
        // AABB tests
        ////////////////////////////////////////////////////////////////////////////////

        AABB< float, Vector2f > aabb2f;
        AABB< float, Vector3f > aabb3f;

        aabb2f.SetCenter( Vector2f() );
        aabb2f.SetExtents( Vector2f() );
        aabb2f.SetMinMax( Vector2f(), Vector2f() );

        aabb2f.GetCenter();
        aabb2f.GetExtents();
        aabb2f.GetMin();
        aabb2f.GetMax();

        aabb2f.SetPoint( Vector2f( 0.0f ) );
        JUNGLE_ASSERT( aabb2f.IsPoint() );
        aabb2f.SetEmpty();
        JUNGLE_ASSERT( aabb2f.IsEmpty() );

        ////////////////////////////////////////////////////////////////////////////////
        // AABB2 tests
        ////////////////////////////////////////////////////////////////////////////////

        AABB2< float > aabb2( Vector2f( 2.f, 2.f ) );
        JUNGLE_ASSERT( aabb2.GetAxis( 0 ) == Vector2f( 1.f, 0.f ) );
        JUNGLE_ASSERT( aabb2.GetAxis( 1 ) == Vector2f( 0.f, 1.f ) );

        Vector2f pointInside( 1.5f, 1.5f );
        JUNGLE_ASSERT( aabb2.Overlaps( &pointInside ) );
        AABB2< float > aabb2Small( Vector2f( 1.0f, 1.0f ) );
        JUNGLE_ASSERT( aabb2.Overlaps( &aabb2Small ) );
        aabb2Small += Vector2f( 1.5f, 1.5f );
        JUNGLE_ASSERT( aabb2.Intersect( &aabb2Small ) );

        ////////////////////////////////////////////////////////////////////////////////
        // Sphere tests
        ////////////////////////////////////////////////////////////////////////////////

        Sphere2< float > sphere2f;
        Sphere< float, Vector3f > sphere3f;

        sphere2f.SetCenter( Vector2f() );
        sphere2f.SetRadius( 0.0f );
        JUNGLE_ASSERT( sphere2f.IsPoint() );
        sphere2f.SetMinMax( Vector2f(), Vector2f(0.1f, 0.1f) );
        JUNGLE_ASSERT( !sphere2f.IsPoint() );

        sphere2f.GetCenter();
        sphere2f.GetRadius();
        sphere2f.GetMin();
        sphere2f.GetMax();

        sphere2f.SetPoint( Vector2f( 0.0f ) );
        JUNGLE_ASSERT( sphere2f.IsPoint() );
        sphere2f.SetEmpty();
        JUNGLE_ASSERT( sphere2f.IsEmpty() );

        ////////////////////////////////////////////////////////////////////////////////
        // Sphere2 tests
        ////////////////////////////////////////////////////////////////////////////////

        // Sphere equation R^2 >= (x - a)^2 + (y - b)^2
        // For centered spehere R^2 >= x^2 + y^2
        // For same coordinates R^2 >= 2 * c ^ 2
        // c <= sqrt( R^2 ) / sqrt( 2 )
        // c <= R / sqrt( 2 )
        float R = 2.0f;
        Sphere2< float > sphere2( R );
        float C = R / Math::Sqrt( 2.f ) - NumberTraits< float >::Epsilon();
        pointInside.Set( C, C );
        JUNGLE_ASSERT( sphere2.Overlaps( &pointInside ) );
        Sphere2< float > sphere2Small( 1.0f );
        JUNGLE_ASSERT( sphere2.Overlaps( &sphere2Small ) );
        sphere2Small += Vector2f( 1.5f, 1.5f );
        JUNGLE_ASSERT( sphere2.Intersect( &sphere2Small ) );

        ////////////////////////////////////////////////////////////////////////////////
        // OBB tests
        ////////////////////////////////////////////////////////////////////////////////

        OBB< float, Vector2f > obb2f;
        JUNGLE_ASSERT( obb2f.GetAxis( 0 ) == Vector2f( 1.f, 0.f ) );
        JUNGLE_ASSERT( obb2f.GetAxis( 1 ) == Vector2f( 0.f, 1.f ) );

        obb2f.SetCenter( Vector2f() );
        obb2f.SetExtentsSize( Vector2f() );
        obb2f.SetMinMax( Vector2f(), Vector2f() );
        JUNGLE_ASSERT( obb2f.IsPoint() );

        obb2f.GetCenter();
        obb2f.GetExtentsSize();
        obb2f.GetMin();
        obb2f.GetMax();

        obb2f.SetPoint( Vector2f( 0.0f ) );
        JUNGLE_ASSERT( obb2f.IsPoint() );
        obb2f.SetEmpty();
        JUNGLE_ASSERT( obb2f.IsEmpty() );

        Vector2f axes[2] = { Vector2f( 0.f, 1.f ), Vector2f( 1.f, 0.f ) };
        obb2f.SetCenterExtentsAndAxes( Vector2f( 0.f, 0.f ), Vector2f( 2.f, 3.f ), axes );
        JUNGLE_ASSERT( obb2f.GetMax() == Vector2f( 3.f, 2.f ) );
        JUNGLE_ASSERT( obb2f.GetMin() == Vector2f( -3.f, -2.f ) );

        ////////////////////////////////////////////////////////////////////////////////
        // OBB2 tests
        ////////////////////////////////////////////////////////////////////////////////
        OBB2< float > obb2( Vector2f( 2.f, 2.f ) );
        JUNGLE_ASSERT( obb2.GetAxis( 0 ) == Vector2f( 1.f, 0.f ) );
        JUNGLE_ASSERT( obb2.GetAxis( 1 ) == Vector2f( 0.f, 1.f ) );

        obb2.SetRotation( NumberTraits< float >::HALF_PI );
        JUNGLE_ASSERT( obb2.GetAxis( 0 ) == Vector2f( 0.f, 1.f ) );
        JUNGLE_ASSERT( obb2.GetAxis( 1 ) == Vector2f( 1.f, 0.f ) );

        pointInside.Set( 1.5f, 1.5f );
        JUNGLE_ASSERT( obb2.Overlaps( &pointInside ) );
        OBB2< float > obb2Small( Vector2f( 1.0f, 1.0f ) );
        //JUNGLE_ASSERT( obb2.Overlaps( &obb2Small ) );     // Not implemented yet
        obb2Small += Vector2f( 1.5f, 1.5f );
        JUNGLE_ASSERT( obb2.Intersect( &obb2Small ) );

        ////////////////////////////////////////////////////////////////////////////////
        // Line tests
        ////////////////////////////////////////////////////////////////////////////////
        Line< float, Vector2f > linef( Vector2f( 1.0f, 0.f ) );
        linef.SetDir( Vector2f( 0.f, 0.f ) );
        linef.SetOrigin( Vector2f( 1.f, 1.f ) );
        linef.GetDir();
        linef.GetOrigin();

        linef += Vector2f( 1.f, 0.f );
        linef -= Vector2f( 0.f, 1.f );

        ////////////////////////////////////////////////////////////////////////////////
        // Line tests - AABB2 intersections
        ////////////////////////////////////////////////////////////////////////////////

        Vector2f origin2f( -2.f, 0.f );
        Vector2f dir2f( 1.f, 0.f );
        AABB<float, Vector2f> aabb2fLine( Vector2f(0.f, 0.f), Vector2f(1.f, 1.f) );
        float tMin( 0.0f );
        float tMax( NumberTraits< float >::Maximum() );
        unsigned int pointsNum;
        float pointsDist[2];
        Vector2f pointsNorm[2];

        Line< float, Vector2f >::Intersect( origin2f, dir2f, &aabb2fLine, tMin, tMax, pointsNum, pointsDist, pointsNorm );
        JUNGLE_ASSERT( pointsNum == 2 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], 1.0f ) );
        JUNGLE_ASSERT( Math::Equal( pointsDist[1], 3.0f ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm[0], Vector2f(-1.f, 0.f) ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm[1], Vector2f(1.f, 0.f) ) );

        origin2f.Set( 0.f, -2.f );
        dir2f.Set( 0.f, 1.f );
        Line< float, Vector2f >::Intersect( origin2f, dir2f, &aabb2fLine, tMin, tMax, pointsNum, pointsDist, pointsNorm );
        JUNGLE_ASSERT( pointsNum == 2 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], 1.0f ) );
        JUNGLE_ASSERT( Math::Equal( pointsDist[1], 3.0f ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm[0], Vector2f(0.f, 1.f) ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm[1], Vector2f(0.f, -1.f) ) );

        origin2f.Set( -2.0f, 0.f );
        dir2f.Set( 1.f, 1.f );
        Line< float, Vector2f >::Intersect( origin2f, dir2f, &aabb2fLine, tMin, tMax, pointsNum, pointsDist, pointsNorm );
        JUNGLE_ASSERT( pointsNum == 1 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], Sqrt( 1.0f ) ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm[0], Vector2f(-1.f, 0.f) ) );

        pointsNum = Line< float, Vector2f >::Clip( origin2f, dir2f, &aabb2fLine, tMin, tMax );
        JUNGLE_ASSERT( pointsNum == 1 );

        ////////////////////////////////////////////////////////////////////////////////
        // Line tests - AABB3 intersections
        ////////////////////////////////////////////////////////////////////////////////

        Vector3f origin3f( -2.f, 0.f, 0.f );
        Vector3f dir3f( 1.f, 0.f, 0.f );
        Vector3f pointsNorm3[2];
        AABB<float, Vector3f> aabb3fLine( Vector3f(0.f, 0.f, 0.f), Vector3f(1.f, 1.f, 1.f) );
        tMin = ( 0.0f );
        tMax = ( NumberTraits< float >::Maximum() );

        Line< float, Vector3f >::Intersect( origin3f, dir3f, &aabb3fLine, tMin, tMax, pointsNum, pointsDist, pointsNorm3 );
        JUNGLE_ASSERT( pointsNum == 2 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], 1.0f ) );
        JUNGLE_ASSERT( Math::Equal( pointsDist[1], 3.0f ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm3[0], (unsigned char)0 ) );
        //JUNGLE_ASSERT( Math::Equal( pointsNorm3[1], (unsigned char)0 ) );

        origin3f.Set( 0.f, 2.f, 0.f );
        dir3f.Set( 0.f, -1.f, 0.f );
        Line< float, Vector3f >::Intersect( origin3f, dir3f, &aabb3fLine, tMin, tMax, pointsNum, pointsDist, pointsNorm3 );
        JUNGLE_ASSERT( pointsNum == 2 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], 1.0f ) );
        JUNGLE_ASSERT( Math::Equal( pointsDist[1], 3.0f ) );
        //JUNGLE_ASSERT( Math::Equal( pointsAxes[0], (unsigned char)1 ) );  // TODO: Fix axes/normals calculation
        //JUNGLE_ASSERT( Math::Equal( pointsAxes[1], (unsigned char)1 ) );

        pointsNum = Line< float, Vector3f >::Clip( origin3f, dir3f, &aabb3fLine, tMin, tMax );
        JUNGLE_ASSERT( pointsNum == 2 );

        ////////////////////////////////////////////////////////////////////////////////
        // Line tests - Sphere3 intersections
        ////////////////////////////////////////////////////////////////////////////////
        Sphere<float, Vector2f> sphere2fLine( 1.f );

        origin2f.Set( 2.f, 0.f );
        dir2f.Set( -1.f, 0.f );
        dir2f.Normalize();
        tMin = ( 0.0f );
        tMax = ( NumberTraits< float >::Maximum() );

        Line< float, Vector2f >::Intersect( origin2f, dir2f, &sphere2fLine, pointsNum, pointsDist );
        JUNGLE_ASSERT( pointsNum == 2 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], 1.0f ) );
        JUNGLE_ASSERT( Math::Equal( pointsDist[1], 3.0f ) );

        // TODO: Provide Clip method and tests
        //pointsNum = Line< float, Vector2f >::Clip( origin2f, dir2f, &sphere2fLine, tMin, tMax );
        //JUNGLE_ASSERT( pointsNum == 2 );

        origin2f.Set( -1.f, -1.f );
        dir2f.Set( 1.f, 0.f );
        //pointsNum = Line< float, Vector2f >::Clip( origin2f, dir2f, &sphere2fLine, tMin, tMax );
        //JUNGLE_ASSERT( pointsNum == 1 );

        ////////////////////////////////////////////////////////////////////////////////
        // Line tests - OBB2 intersections
        ////////////////////////////////////////////////////////////////////////////////
        OBB2<float, Vector2f> obb2fLine( Vector2f( 1.f, 1.f ) );
        obb2fLine.SetRotation( NumberTraits< float >::HALF_PI );

        origin2f.Set( 0.f, -2.f );
        dir2f.Set( 0.f, 1.f );
        Line2< float, Vector2f >::Intersect( origin2f, dir2f, &obb2fLine, tMin, tMax, pointsNum, pointsDist );
        JUNGLE_ASSERT( pointsNum == 2 );
        JUNGLE_ASSERT( Math::Equal( pointsDist[0], 1.0f ) );
        JUNGLE_ASSERT( Math::Equal( pointsDist[1], 3.0f ) );
        pointsNum = Line2< float, Vector2f >::Clip( origin2f, dir2f, &obb2fLine, tMin, tMax );
        JUNGLE_ASSERT( pointsNum == 2 );

        ////////////////////////////////////////////////////////////////////////////////
        // Ray tests
        ////////////////////////////////////////////////////////////////////////////////
        dir2f.Set( 1.0f, 0.f );
        origin2f.Set( 1.0f, 1.f );
        Ray< float, Vector2f > rayf( dir2f );
        rayf = Ray< float, Vector2f >( origin2f, dir2f );
        rayf.SetDir( dir2f );
        rayf.SetOrigin( origin2f );
        rayf.GetDir();
        rayf.GetOrigin();

        rayf += Vector2f( 1.f, 0.f );
        rayf -= Vector2f( 0.f, 1.f );

        ////////////////////////////////////////////////////////////////////////////////
        // Ray2 tests
        ////////////////////////////////////////////////////////////////////////////////
        Ray2< float, Vector2f > ray2f( dir2f );
        ray2f = Ray2< float >( origin2f, dir2f );
        ray2f.SetDir( dir2f );
        ray2f.SetOrigin( origin2f );
        ray2f.GetDir();
        ray2f.GetOrigin();

        ray2f += Vector2f( 1.f, 0.f );
        ray2f -= Vector2f( 0.f, 1.f );

        ////////////////////////////////////////////////////////////////////////////////
        // Segment tests
        ////////////////////////////////////////////////////////////////////////////////
        float segLen = 2.f;
        Segment< float, Vector2f > segmentf( dir2f, segLen );
        segmentf = Segment< float, Vector2f >( origin2f, dir2f, segLen );
        segmentf.SetDir( dir2f );
        segmentf.GetDir();
        segmentf.GetOrigin();
        segmentf.SetStart( origin2f );
        segmentf.SetEnd( origin2f + dir2f );
        segmentf.GetStart();
        segmentf.GetEnd();
        segmentf.GetCenter();
        segmentf.SetOrigin( origin2f );
        segmentf.SetExtent( dir2f * segLen );
        bool equal = segmentf.GetLength() == Segment< float, Vector2f >( origin2f, dir2f, segLen ).GetLength();
        JUNGLE_ASSERT( equal );

        segmentf += Vector2f( 1.f, 0.f );
        segmentf -= Vector2f( 0.f, 1.f );

        ////////////////////////////////////////////////////////////////////////////////
        // Segment2 tests
        ////////////////////////////////////////////////////////////////////////////////
        Segment2< float > segment2f( dir2f, segLen );
        segment2f = Segment2< float >( 1.0f, 1.0f, segLen );
        segment2f.SetDir( dir2f );
        segment2f.GetDir();
        segment2f.GetOrigin();
        segment2f.SetStart( origin2f );
        segment2f.SetEnd( origin2f + dir2f );
        segment2f.GetStart();
        segment2f.GetEnd();
        segment2f.GetCenter();
        segment2f.SetOrigin( origin2f );
        segment2f.SetExtent( dir2f * segLen );

    }

    void Test::TestMathApprox()
    {
        using namespace Math;

        ////////////////////////////////////////////////////////////////////////////////
        // Polynomials tests
        ////////////////////////////////////////////////////////////////////////////////

        // Construction

        Polynomial1f polyDefault;
        Polynomial1f polyDegree( 3 );
        float coeff[] = { 1.f, 2.f, 3.f, 4.f };
        Polynomial1f polyDegreeDef( 3, coeff );

        // Evaluation

        JUNGLE_ASSERT( polyDefault( 0.f ) == polyDefault( 1.f ) && polyDefault( 2.f ) == 0.f );
        JUNGLE_ASSERT( polyDegree( 0.f ) == polyDegree( 1.f ) && polyDegree( 2.f ) == 0.f );
        // Polynomial for zero variable always equals its const coefficient
        JUNGLE_ASSERT( polyDegreeDef( 0.f ) == polyDegreeDef[0] );
        // Polynomial for 1.f eqals the sum of its coefficients
        float coeffSum = polyDegreeDef[ 0 ];
        unsigned int degree = polyDegreeDef.GetDegree();
        while( degree > 0 )
            coeffSum += polyDegreeDef[ degree-- ];
        JUNGLE_ASSERT( polyDegreeDef( 1.f ) == coeffSum );

        // Derivative
        Polynomial1f polyDefaultDer = polyDefault.GetDerivative();
        JUNGLE_ASSERT( polyDefaultDer( 100.f ) == 0.f );

        Polynomial1f poly1( 1 );
        poly1[0] = 10.f;
        poly1[1] = 5.f;
        Polynomial1f poly1Der = poly1.GetDerivative();
        JUNGLE_ASSERT( poly1Der( 100.f ) == poly1Der[0] && poly1Der[0] == poly1[1] );
    }

    void Test::TestMathStats()
    {
        using namespace Math;

        const float MU_NORM = 0.f;
        const float SD_NORM = 1.f;
        const float MU_GAUSS = 1.f;
        const float SD_GAUSS = 2.f;

        DistributionNormal<float> dnorm;
        DistributionGauss<float> dgauss( MU_GAUSS, SD_GAUSS );

        static const float DISTRIBUTION_TOLERANCE = 0.0000001f;

        // Half of population lies before the distribution average value.
        JUNGLE_ASSERT( Equal( dnorm.Cdf( MU_NORM ), 0.5f, DISTRIBUTION_TOLERANCE ) );
        JUNGLE_ASSERT( Equal( dgauss.Cdf( MU_GAUSS ), 0.5f, DISTRIBUTION_TOLERANCE ) );

        // 68.26% of the population is contained within 1 standard deviations from the mean.
        JUNGLE_ASSERT( dnorm.Cdf( 1 * SD_NORM ) - dnorm.Cdf( -1 * SD_NORM ) > 0.6825 );
        JUNGLE_ASSERT( dnorm.Cdf( 1 * SD_GAUSS ) - dnorm.Cdf( -1 * SD_GAUSS ) > 0.6825 );
        // 95.44% of the population is contained within 2 standard deviations from the mean.
        JUNGLE_ASSERT( dnorm.Cdf( 2 * SD_NORM ) - dnorm.Cdf( -2 * SD_NORM ) > 0.9543 );
        JUNGLE_ASSERT( dnorm.Cdf( 2 * SD_GAUSS ) - dnorm.Cdf( -2 * SD_GAUSS ) > 0.9543 );
        // 99.73% of the population is contained within 3 standard deviations from the mean.
        JUNGLE_ASSERT( dnorm.Cdf( 3 * SD_NORM ) - dnorm.Cdf( -3 * SD_NORM ) > 0.9973 );
        JUNGLE_ASSERT( dnorm.Cdf( 3 * SD_GAUSS ) - dnorm.Cdf( -3 * SD_GAUSS ) > 0.9973 );

        ////////////////////////////////////////////////////////////////////////////////
        // Median tests
        ////////////////////////////////////////////////////////////////////////////////
        Median<int> mediani;
        mediani.AddSample( 0 );
        mediani.AddSample( 1 );
        mediani.AddSample( 2 );
        JUNGLE_ASSERT( mediani.GetValue() == 1 );
        mediani.AddSample( 3 );
        JUNGLE_ASSERT( mediani.GetValue() == 1 );

        Medianf medianf;
        medianf.AddSample( 0.f );
        medianf.AddSample( 1.f );
        medianf.AddSample( 2.f );
        JUNGLE_ASSERT( medianf.GetValue() == 1.f );
        medianf.AddSample( 3.f );
        JUNGLE_ASSERT( medianf.GetValue() == 1.5f );

        std::list< char > tmp;
        tmp.push_back( 'J' );
        tmp.push_back( 'U' );
        tmp.push_back( 'N' );
        tmp.push_back( 'G' );
        tmp.push_back( 'L' );
        tmp.push_back( 'E' );
        tmp.push_back( 'S' );
        JUNGLE_ASSERT( Median<char>::Calculate( tmp.begin(), tmp.end() ) == 'L' );
        const char* tmp2 = "ABCDEFG";
        JUNGLE_ASSERT( Median<char>::Calculate( tmp2, tmp2 + 7 ) == 'D' );
        Median< char > medianc;
        medianc.AddSamples( tmp.begin(), tmp.end() );
        JUNGLE_ASSERT( medianc.GetValue() == 'L' );

        ////////////////////////////////////////////////////////////////////////////////
        // Unit test for TestRuns
        ////////////////////////////////////////////////////////////////////////////////
        const float ALPHA = 0.05f;

        TestRuns< char > testSeriesC;
        testSeriesC.AddSamples( tmp.begin(), tmp.end() );
        TestRuns< char >::Test( ALPHA, tmp.begin(), tmp.end() );

        TestRuns< int > testSeriesI;
        for( int i = 0; i < 100; ++i )
            testSeriesI.AddSample( Randi() % 1000 );
        testSeriesI.Test( ALPHA );
    }

    void Test::TestScene()
    {
        using namespace Scene;

        SampleSceneObj sceneObj;

        ////////////////////////////////////////////////////////////////////////////////
        // ContactPoint tests
        ////////////////////////////////////////////////////////////////////////////////

        typedef ContactPoint< Math::Vector2f >  ContactPoint2;
        typedef ContactSet< Math::Vector2f >    ContactSet2;

        ContactPoint2 contactPoint;
        contactPoint.m_pos = Math::Vector2f::ZERO;
        JUNGLE_ASSERT( contactPoint.m_pos == Math::Vector2f::ZERO );

        contactPoint.m_normal = Math::Vector2f::ZERO;
        JUNGLE_ASSERT( contactPoint.m_normal == Math::Vector2f::ZERO );

        ////////////////////////////////////////////////////////////////////////////////
        // ContactSet tests
        ////////////////////////////////////////////////////////////////////////////////

        ContactSet2 contactSet;
        JUNGLE_ASSERT( contactSet.GetSize() == 0 );
        JUNGLE_ASSERT( contactSet.IsEmpty() );

        contactSet.Add( contactPoint );
        JUNGLE_ASSERT( contactSet.Get( 0 ).m_pos == contactPoint.m_pos );
        JUNGLE_ASSERT( contactSet.GetSize() == 1 );
        JUNGLE_ASSERT( !contactSet.IsEmpty() );

        // Test gathering few contact points
        contactSet.Add( contactPoint );
        JUNGLE_ASSERT( contactSet.GetSize() == 2 );

        contactSet = ContactSet2( ContactSet2::CG_FIRST_POINT );
        contactSet.Add( contactPoint );
        contactSet.Add( contactPoint );

        JUNGLE_ASSERT( contactSet.GetSize() == 1 );

        ////////////////////////////////////////////////////////////////////////////////
        // CollisionQuery tests
        ////////////////////////////////////////////////////////////////////////////////

        class BoundingVolume
        {
        };
        BoundingVolume bv;

        CollisionCache< SampleSceneObj, BoundingVolume > collisionCache;
        collisionCache.Add( &sceneObj );
        JUNGLE_ASSERT( *collisionCache.GetColliders()->begin() = &sceneObj );
        JUNGLE_ASSERT( collisionCache.GetColliders()->size() == 1 );
        collisionCache.Clear();

        CollisionQuery< SampleSceneObj, BoundingVolume > collisionQuery;
        collisionQuery.InitQuery();
        collisionQuery.SetQueryBV( &bv );
        JUNGLE_ASSERT( collisionQuery.GetQueryBV() == &bv );
        collisionQuery.GetColliders();
        collisionQuery.GetCache();
        collisionQuery.GetQueryBV();

        Debug::Renderer::Register( new DebugRenderer() );
        delete Debug::Renderer::Release();
    }

    void Test::TestRtti()
    {
        TypeA           objA;
        TypeB           objB;
        TypeDerivedA    objDerA;
        TypeDerivedAB   objDerAB;

        JUNGLE_ASSERT( objA.GetType().IsExactly( TypeA().GetType() ) );
        JUNGLE_ASSERT( objDerA.GetType().IsExactly( TypeDerivedA().GetType() ) );

        JUNGLE_ASSERT( !objA.GetType().IsExactly( objDerA.GetType() ) );
        JUNGLE_ASSERT( !objDerA.GetType().IsExactly( objA.GetType() ) );

        JUNGLE_ASSERT( objA.GetType().IsDerived( TypeA().GetType() ) );
        JUNGLE_ASSERT( objDerA.GetType().IsDerived( TypeDerivedA().GetType() ) );

        JUNGLE_ASSERT( !objA.GetType().IsDerived( objDerA.GetType() ) );
        JUNGLE_ASSERT( objDerA.GetType().IsDerived( objA.GetType() ) );

        JUNGLE_ASSERT( !objA.GetType().IsDerived( objDerAB.GetType() ) );
        JUNGLE_ASSERT( !objB.GetType().IsDerived( objDerAB.GetType() ) );
        JUNGLE_ASSERT( objDerAB.GetType().IsDerived( objA.GetType() ) );
        JUNGLE_ASSERT( objDerAB.GetType().IsDerived( objB.GetType() ) );
        JUNGLE_ASSERT( !objDerAB.GetType().IsDerived( objDerA.GetType() ) );

        const char* derAName = objDerA.GetType().GetName();
        const char* derABName = objDerAB.GetType().GetName();
    }

}

// EOF

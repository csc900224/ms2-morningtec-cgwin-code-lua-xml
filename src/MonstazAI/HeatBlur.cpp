#include "claw/compat/Platform.h"
#include "claw/graphics/pixeldata/PixelDataGL.hpp"
#include "claw/graphics/Batcher.hpp"

#include "MonstazAI/HeatBlur.hpp"


static const char* vertex =
    "uniform mediump vec2 dist;\n"
    "varying mediump vec2 vTex0;\n"
    "varying mediump vec2 vTex1;\n"
    "varying mediump vec2 vTex2;\n"
    "varying mediump vec2 vTex3;\n"
    "void main(void)\n"
    "{\n"
    "vTex0 = inUV + vec2( -dist.x, -dist.y );\n"
    "vTex1 = inUV + vec2( dist.x, -dist.y );\n"
    "vTex2 = inUV + vec2( dist.x, dist.y );\n"
    "vTex3 = inUV + vec2( -dist.x, dist.y );\n"
    "GLPOSITION"
    "}";

static const char* fragment =
    "varying mediump vec2 vTex0;\n"
    "varying mediump vec2 vTex1;\n"
    "varying mediump vec2 vTex2;\n"
    "varying mediump vec2 vTex3;\n"
    "void main(void)\n"
    "{\n"
    "mediump vec2 t0 = texture2D( clawTex, vTex0 ).rg;\n"
    "mediump vec2 t1 = texture2D( clawTex, vTex1 ).rg;\n"
    "mediump vec2 t2 = texture2D( clawTex, vTex2 ).rg;\n"
    "mediump vec2 t3 = texture2D( clawTex, vTex3 ).rg;\n"
    "highp vec2 to = ( ( t0 + t1 + t2 + t3 ) - 2.0 );\n"
    "to = to * 0.21;\n"
    "if( abs( to.r ) < 0.05 ) to.r = 0.0;\n"
    "if( abs( to.g ) < 0.05 ) to.g = 0.0;\n"
    "to = to + 0.5;\n"
    "gl_FragColor = vec4( to.rg, 1.0, 1.0 );\n"
    "}";


HeatBlur::HeatBlur()
{
    m_shader.Load( vertex, fragment );
}

void HeatBlur::Render( Claw::Surface* src, Claw::Surface* dst, float x, float y, const Claw::Rect& clipRect )
{
    Claw::PixelDataGL* pd = (Claw::PixelDataGL*)src->GetPixelData();
    Claw::g_batcher->SetShader( &m_shader );
    m_shader.Uniform( "dist", 1.f / ( src->GetWidth() * 2 ), 1.f / ( src->GetWidth() * 2 ) );
    Claw::g_batcher->SetTexturing( true );
    Claw::g_batcher->SetDestination( (Claw::PixelDataGL*)dst->GetPixelData(), dst->QueryFlag() );
    Claw::g_batcher->SetSource( pd, src->QueryFlag(), src->GetAlpha() );
    Claw::g_batcher->SetClipping( dst->GetClipRect() );
    Claw::g_batcher->SetDrawingMode( Claw::Batcher::Blending );
    Claw::g_batcher->SetColorKey( false, 0, 0, 0 );
    Claw::g_batcher->SetPrimitiveType( Claw::Batcher::Triangle );

    float x0 = x + src->GetXOffset();
    float y0 = y + src->GetYOffset();

    float x1, x2, y1, y2;

    x1 = (float)( clipRect.m_x + src->GetStartX() ) * pd->m_scaleX;
    x2 = x1 + (float)clipRect.m_w * pd->m_scaleX;
    y1 = (float)( clipRect.m_y + src->GetStartY() ) * pd->m_scaleY;
    y2 = y1 + (float)clipRect.m_h * pd->m_scaleY;

    Claw::g_batcher->Queue(
        Claw::BatcherVertex( x0,                y0,                x1, y1, 255, 255, 255, 255 ),
        Claw::BatcherVertex( x0 + clipRect.m_w, y0,                x2, y1, 255, 255, 255, 255 ),
        Claw::BatcherVertex( x0 + clipRect.m_w, y0 + clipRect.m_h, x2, y2, 255, 255, 255, 255 ),
        Claw::BatcherVertex( x0,                y0 + clipRect.m_h, x1, y2, 255, 255, 255, 255 ) );

    Claw::g_batcher->SetShader( NULL );
}

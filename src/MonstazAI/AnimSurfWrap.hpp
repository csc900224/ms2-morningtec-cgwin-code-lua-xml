#ifndef __MONSTAZ_ANIMSURFWRAP_HPP__
#define __MONSTAZ_ANIMSURFWRAP_HPP__

#include "claw/graphics/AnimatedSurface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/Renderable.hpp"

class AnimSurfWrap : public Claw::RefCounter, public Renderable
{
public:
    enum Transform
    {
        NoTransform,
        Rotate
    };

    AnimSurfWrap( Claw::AnimatedSurface* surface, const Vectorf& pos );
    AnimSurfWrap( Claw::AnimatedSurface* surface, const Vectorf& pos, const Vectorf& vec );
    AnimSurfWrap( Claw::AnimatedSurface* surface, const Vectorf& pos, float angle );

    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    bool Update( float dt );

    const Vectorf& GetPos() const { return m_pos; }
    const Vectorf& GetRot() const { return m_rot; }
    Claw::Surface* GetRawSurface( int& x, int& y );
    Transform GetTransform() const { return m_transform; }
    bool IsPlaying() const { return m_time > 0; }

private:
    Claw::AnimatedSurface* m_surface;
    int m_frame;
    float m_time;
    Transform m_transform;
    Vectorf m_rot;
};

typedef Claw::SmartPtr<AnimSurfWrap> AnimSurfWrapPtr;

#endif

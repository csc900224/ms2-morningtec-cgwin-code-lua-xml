#include "claw/base/Registry.hpp"
#include "claw/compat/Platform.h"

#include "MonstazAI/RNG.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/ApplicationConfig.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/db/UserDataManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"

#include <sstream>

Shop* Shop::s_instance = NULL;

static const char* REG_UPGRAGE_PATH     = "/monstaz/weaponupgrade/";
static const char* REG_OWNERSHIP_PATH   = "/monstaz/shop/";

const Shop::ItemId Shop::Items::Smg         ( "smg" );
const Shop::ItemId Shop::Items::Nuke        ( "nuke" );
const Shop::ItemId Shop::Items::Grenade     ( "grenade" );
const Shop::ItemId Shop::Items::HealthKit   ( "healthkit" );
const Shop::ItemId Shop::Items::Shield      ( "shield" );
const Shop::ItemId Shop::Items::Mine        ( "mine" );
const Shop::ItemId Shop::Items::Airstrike   ( "airstrike" );
const Shop::ItemId Shop::Items::FreeIap     ( "free" );
const Shop::ItemId Shop::Items::MechFlamer  ( "mechflamer" );
const Shop::ItemId Shop::Items::MechRocket  ( "mechrocket" );

LUA_DECLARATION( Shop )
{
    METHOD( Shop, IsBought ),
    METHOD( Shop, GetUpgrades ),
    METHOD( Shop, Buy ),
    METHOD( Shop, UnlockBuy ),
    METHOD( Shop, Upgrade ),
    METHOD( Shop, BuyVirtualCash ),
    METHOD( Shop, BuySubscription ),
    METHOD( Shop, CheckSubscription ),
    METHOD( Shop, Use ),
    METHOD( Shop, GetCash ),
    METHOD( Shop, SetCash ),
    METHOD( Shop, OnEnter ),
    METHOD( Shop, OnExit ),
    METHOD( Shop, FreeGold ),
    METHOD( Shop, Preload ),
    {0,0}
};

Shop::Shop()
    : m_lua( NULL )
    , m_shopActive( false )
    , m_transactionInProgress( false )
    , m_iapsSupported( true )
    , m_iabInitialized( false )
    , m_gameSession( false )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;

    MonetizationManager::GetInstance()->GetIapStore()->RegisterTransObserver( this );
    MonetizationManager::GetInstance()->GetIapStore()->RegisterSubscriptionObserver( this );
    MonetizationManager::GetInstance()->GetAdColony()->RegisterObserver( this );
    MonetizationManager::GetInstance()->GetPlayhaven()->RegisterObserver( this );
}

Shop::~Shop()
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;

    MonetizationManager::GetInstance()->GetIapStore()->UnregisterTransObserver( this );
    MonetizationManager::GetInstance()->GetIapStore()->UnregisterSubscriptionObserver( this );
    MonetizationManager::GetInstance()->GetAdColony()->UnregisterObserver( this );
    MonetizationManager::GetInstance()->GetPlayhaven()->UnregisterObserver( this );
}

int Shop::CheckOwnership( const ItemId& item )
{
    ItemToIntMap::iterator it = m_ownership.find( item );
    if( it != m_ownership.end() )
    {
        return it->second;
    }
    else
    {
        int ownership = 0;
        Claw::NarrowString key( REG_OWNERSHIP_PATH );
        key.append( item );
        Claw::Registry::Get()->Get( key.c_str(), ownership );
        m_ownership[item] = ownership;
        return ownership;
    }
}

int Shop::GetUpgrades( const ItemId& item )
{
    ItemToIntMap::iterator it = m_upgrades.find( item );
    if( it != m_upgrades.end() )
    {
        return it->second;
    }
    else
    {
        int upgrade = 0;
        Claw::NarrowString key( REG_UPGRAGE_PATH );
        key.append( item );
        Claw::Registry::Get()->Get( key.c_str(), upgrade );
        m_upgrades[item] = upgrade;
        return upgrade;
    }
}

bool Shop::CanUpgradeItem( const ItemId& item )
{
    m_lua->PushString( item );
    m_lua->Call( "ItemDbCanUpgrade", 1, 1 );
    bool canUpgrade = m_lua->CheckBool( -1 );
    m_lua->Pop( 1 );
    return canUpgrade;
}

bool Shop::CanUpgradeAnyItem()
{
    m_lua->Call( "ItemDbCanUpgradeAnything", 0, 1 );
    bool canUpgrade = m_lua->CheckBool( -1 );
    m_lua->Pop( 1 );
    return canUpgrade;
}

void Shop::Init( Claw::Lua* lua )
{
    Claw::Lunar<Shop>::Register( *lua );
    Claw::Lunar<Shop>::push( *lua, this );
    lua->RegisterGlobal( "Shop" );

    m_lua = lua;
}

void Shop::OnGameSessionsStart()
{
    m_gameSession = true;
}

void Shop::OnGameSessionsStop()
{
    if( m_gameSession )
    {
        m_sessionOwnership.clear();
        m_gameSession = false;
    }
}

bool Shop::Upgrade( const ItemId& item, bool save )
{
    // Do not touch cash if not saving. The only case for not saving, currently, is during
    // second mech weapon purchase/upgrade. If not for that check, cash would be drained twice.
    if( save )
    {
        // Update cash
        int priceCash = 0, priceGold = 0;
        GetItemUpgradePrice( item, priceCash, priceGold );
        UpdateCash( -priceCash, -priceGold );
    }

    // Update upgrade
    int upgrade = GetUpgrades(item) + 1;
    m_upgrades[item] = upgrade;
    Claw::NarrowString ownKey = REG_UPGRAGE_PATH;
    ownKey.append( item ); 
    Claw::Registry::Get()->Set( ownKey.c_str(), upgrade );

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_ITEM_UPGRADED, upgrade, item );
    //mech disabled for friend backup
    if ( item.c_str() != Shop::Items::MechFlamer && item.c_str() != Shop::Items::MechRocket )
    {
        UserDataManager::GetInstance()->SendSetWeaponRequest( item.c_str(), upgrade );
    }

    if( save )
    {
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    }
    return true;
}

bool Shop::Buy( const ItemId& item, bool unlock, bool save )
{
    // Update ownership
    int onwership = CheckOwnership( item ) + 1;
    m_ownership[item] = onwership;
    Claw::NarrowString ownKey = REG_OWNERSHIP_PATH;
    ownKey.append( item ); 
    Claw::Registry::Get()->Set( ownKey.c_str(), onwership );

    if( m_gameSession )
    {
        ++m_sessionOwnership[item];
    }

    // Do not touch cash if not saving. The only case for not saving, currently, is during
    // second mech weapon purchase/upgrade. If not for that check, cash would be drained twice.
    if( save )
    {
        // Update cash
        if( unlock )
        {
            UpdateCash( 0, -GetItemUnlockPrice( item ) );
        }
        else
        {
            int priceCash = 0, priceGold = 0;
            GetItemPrice( item, priceCash, priceGold );
            UpdateCash( -priceCash, -priceGold );
        }
    }

    GameEventDispatcher::GetInstance()->HandleGameEvent( unlock ? GEI_ITEM_UNLOCKED : GEI_ITEM_BOUGHT, 1.0f, item );

    //mech disabled for friend backup
    if ( item.c_str() != Shop::Items::MechFlamer && item.c_str() != Shop::Items::MechRocket )
    {
        UserDataManager::GetInstance()->SendSetWeaponRequest( item.c_str() );
    }

    if( save )
    {
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    }
    return true;
}

bool Shop::BuyVirtualCash( const ItemId& id )
{
    CLAW_MSG( "Shop::BuyVirtualCash():" << id );
    if( id == Items::FreeIap )
    {
        FreeGold();
    }
    else
    {
        if( !m_iapsSupported )
        {
            m_lua->Call( "OnTransactionNotSupported", 0, 0 );
            return false;
        }
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_STARTED, 1, id );

        CLAW_MSG_WARNING( !m_transactionInProgress, "Shop::BuyVirtualCash() - another transaction already in progress!" );
        m_transactionInProgress = true;
        m_lua->Call( "OnTransactionStart", 0, 0 );
        MonetizationManager::GetInstance()->GetIapStore()->BuyConsumableProduct( id );
        MonetizationManager::GetInstance()->SkipPlayhavenResume();
    }
    return true;
}

bool Shop::BuySubscription( const ItemId& id )
{
    CLAW_MSG( "Shop::BuySubscription():" << id );
    if( !m_iapsSupported )
    {
        m_lua->Call( "OnTransactionNotSupported", 0, 0 );
        return false;
    }
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_STARTED, 1, id );

    CLAW_MSG_WARNING( !m_transactionInProgress, "Shop::BuySubscription() - another transaction already in progress!" );
    m_transactionInProgress = true;
    m_lua->Call( "OnTransactionStart", 0, 0 );
    MonetizationManager::GetInstance()->GetIapStore()->BuySubscription( id );
    MonetizationManager::GetInstance()->SkipPlayhavenResume();
    return true;
}

void Shop::CheckSubscriptions()
{
    if( m_iabInitialized )
    {
        CLAW_MSG( "Shop::CheckSubscriptions()" );
        m_lua->Call( "ItemDbCheckSubscriptions", 0, 0 );
    }
}

void Shop::FreeGold()
{
    if( MonetizationManager::GetInstance()->GetAdColony()->AreVideoAdsAvailable( MonetizationManager::ACZ_1 ) )
    {
        MonetizationManager::GetInstance()->GetAdColony()->PlayVideo( MonetizationManager::ACZ_1, true, true );
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_ACTION_STARTED, 1, "AdColony" );
    }
    else if( MonetizationManager::GetInstance()->IsTapjoySupported() )
    {
        MonetizationManager::GetInstance()->GetTapjoy()->ShowOffers();
        MonetizationManager::GetInstance()->GetTapjoy()->CheckPoints();
        MonetizationManager::GetInstance()->SkipPlayhavenResume();
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_ACTION_STARTED, 1, "Tapjoy" );
    }
    else if( MonetizationManager::GetInstance()->IsMetapsSupported() )
    {
        MonetizationManager::GetInstance()->GetMetaps()->ShowOfferWall( "user", "default" );
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_ACTION_STARTED, 1, "Metaps" );
    }
}

Shop::ItemCategory Shop::GetItemCategory( const ItemId& itemId ) const
{
    m_lua->PushString( itemId );
    m_lua->Call( "ItemDbGetItemCategory", 1, 1 );
    ItemCategory category = (ItemCategory)(int)m_lua->CheckNumber( -1 );
    m_lua->Pop( 1 );
    return category;
}

void Shop::GetItemPrice( const ItemId& itemId, int& cash, int& gold ) const
{
    m_lua->PushString( itemId );
    m_lua->Call( "ItemDbGetItemPrice", 1, 2 );
    cash = m_lua->CheckNumber( -2 );
    gold = m_lua->CheckNumber( -1 );
    m_lua->Pop( 2 );
}

int Shop::GetItemUnlockPrice( const ItemId& itemId ) const
{
    m_lua->PushString( itemId );
    m_lua->Call( "ItemDbGetItemUnlockPrice", 1, 1 );
    int price = m_lua->CheckNumber( -1 );
    m_lua->Pop( 1 );
    return price;
}

void Shop::GetItemUpgradePrice( const ItemId& itemId, int& cash, int& gold ) const
{
    m_lua->PushString( itemId );
    m_lua->Call( "ItemDbGetItemUpgradePrice", 1, 2 );
    cash = m_lua->IsNil( -2 ) ? 0 : m_lua->CheckNumber( -2 );
    gold = m_lua->IsNil( -1 ) ? 0 : m_lua->CheckNumber( -1 );
    m_lua->Pop( 2 );
}

int Shop::GetCashItemBonus( const ItemId& itemId ) const
{
    m_lua->PushString( itemId );
    m_lua->Call( "ItemDbGetCashItemBonus", 1, 1 );
    int bonus = m_lua->CheckNumber( -1 );
    m_lua->Pop( 1 );
    return bonus;
}

float Shop::GetRealItemPriceInUSD( const ItemId& itemid ) const
{
    m_lua->PushString( itemid );
    m_lua->Call( "ItemDbGetUsdPrice", 1, 1 );
    float realPrice = m_lua->CheckNumber( -1 );
    m_lua->Pop( 1 );
    return realPrice;
}

float Shop::GetTapjoyConversionRate() const
{
    m_lua->Call( "ItemDbGetTapjoyConversionRate", 0, 1 );
    float convRate = m_lua->CheckNumber( -1 );
    m_lua->Pop( 1 );
    return convRate;
}

bool Shop::Use( const ItemId& item, bool saveChange /*= false*/ )
{
    int onwership = CheckOwnership( item );
    if( onwership == 0 )
    {
        return false;
    }

    m_ownership[item] = --onwership;
    Claw::NarrowString ownKey = REG_OWNERSHIP_PATH;
    ownKey.append( item );
    Claw::Registry::Get()->Set( ownKey.c_str(), onwership );

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_ITEM_USED, 1.0f, item );

    if( saveChange )
    {
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    }

    return true;
}

static int Crypt( int v )
{
    Claw::Int32 r = g_rng.GetInt();

    int b = ( ( r & 0xF ) << 4 ) | 0x1;
    int c = (~r) & 0xF;
    v &= 0x00FFFFFF;
    v ^= 0x0046A1B9;
    v <<= 4;
    v = ( ( v & ( b << 16 ) ) >> 12 ) |
        ( ( v & ( b << 4 ) ) << 12 ) |
        ( v & ( ( c << 8 ) | ( c << 20 ) | 0x0F0EF0E0 ) ) |
        ( r & 0x0000000F );
    int s = 0;
    for( int i=0; i<7; i++ )
    {
        s += ( v >> ( i * 4 ) ) & 0xF;
    }
    return v | 0x80000000 | ( ( s & 0x7 ) << 28 );
}

static int Decrypt( int v )
{
    if( ( v & 0x80000000 ) == 0 ) return v;
    int s = 0;
    for( int i=0; i<7; i++ )
    {
        s += ( v >> ( i*4 ) ) & 0xF;
    }
    if( ( ( v >> 28 ) & 0x7 ) != ( s & 0x7 ) ) return 0;
    int b = ( ( v & 0xF ) << 4 ) | 0x1;
    int c = (~v) & 0xF;
    v &= 0x0FFFFFFF;
    v >>= 4;
    v = ( v & ( ( c << 4 ) | ( c << 16 ) | 0x00F0EF0E ) ) |
        ( ( v & ( b << 12 ) ) >> 12 ) |
        ( ( v & b ) << 12 );
    return v ^ 0x0046A1B9;
}

void Shop::SetCash( int cash, int gold )
{
    Claw::Registry::Get()->Set( "/monstaz/cash/soft", Crypt( cash ) );
    Claw::Registry::Get()->Set( "/monstaz/cash/hard", Crypt( gold ) );

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_CHANGED );
}

void Shop::GetCash( int& cash, int& gold ) const
{
    Claw::Registry::Get()->Get( "/monstaz/cash/soft", cash );
    Claw::Registry::Get()->Get( "/monstaz/cash/hard", gold );
    cash = Decrypt( cash );
    gold = Decrypt( gold );
}

void Shop::UpdateCashEncryption()
{
    int cash = 0;
    int gold = 0;
    Claw::Registry::Get()->Get( "/monstaz/cash/soft", cash );
    Claw::Registry::Get()->Get( "/monstaz/cash/hard", gold );
    Claw::Registry::Get()->Set( "/monstaz/cash/soft", Crypt( Decrypt( cash ) ) );
    Claw::Registry::Get()->Set( "/monstaz/cash/hard", Crypt( Decrypt( gold ) ) );
}

void Shop::UpdateCash( int cashDelta, int goldDelta )
{
    int cash = 0;
    int gold = 0;

    GetCash( cash, gold );
    cash += cashDelta;
    gold += goldDelta;

    CLAW_ASSERT( cash >= 0 && gold >= 0 );
    cash = std::max( cash, 0 );
    gold = std::max( gold, 0 );

    SetCash( cash, gold );
}

int Shop::l_IsBought( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString item = lua.CheckString( 1 );
    lua.PushNumber( CheckOwnership( item ) );
    lua.PushNumber( m_sessionOwnership[item] );
    return 2;
}

int Shop::l_GetUpgrades( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString item = lua.CheckString( 1 );
    lua.PushNumber( GetUpgrades( item ) );
    return 1;
}

int Shop::l_Buy( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString item = lua.CheckString( 1 );
    bool save = true;
    if( lua.IsBool( 2 ) )
    {
        save = lua.CheckBool( 2 );
    }
    lua.PushBool( Buy( item, false, save ) );
    return 1;
}

int Shop::l_UnlockBuy( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString item = lua.CheckString( 1 );
    bool save = true;
    if( lua.IsBool( 2 ) )
    {
        save = lua.CheckBool( 2 );
    }
    lua.PushBool( Buy( item, true, save ) );
    return 1;
}

int Shop::l_Upgrade( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString item = lua.CheckString( 1 );
    bool save = true;
    if( lua.IsBool( 2 ) )
    {
        save = lua.CheckBool( 2 );
    }
    lua.PushBool( Upgrade( item, save ) );
    return 1;
}

int Shop::l_BuyVirtualCash( lua_State* L )
{
    Claw::Lua lua( L );
    const ItemId& id = lua.CheckString( 1 );
    lua.PushBool( BuyVirtualCash( id ) );
    return 1;
}

int Shop::l_BuySubscription( lua_State* L )
{
    Claw::Lua lua( L );
    const ItemId& id = lua.CheckString( 1 );
    lua.PushBool( BuySubscription( id ) );
    return 1;
}

int Shop::l_CheckSubscription( lua_State* L )
{
    Claw::Lua lua( L );
    const ItemId& id = lua.CheckString( 1 );
    MonetizationManager::GetInstance()->GetIapStore()->CheckSubscription( id );
    return 1;
}

int Shop::l_Use( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString item = lua.CheckString( 1 );
    lua.PushBool( Use( item ) );
    return 1;
}

int Shop::l_GetCash( lua_State* L )
{
    Claw::Lua lua( L );
    int cash = 0;
    int gold = 0;
    GetCash( cash, gold );
    lua.PushNumber( cash );
    lua.PushNumber( gold );
    return 2;
}

int Shop::l_SetCash( lua_State* L )
{
    Claw::Lua lua( L );
    SetCash( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) );
    return 0;
}

int Shop::l_OnEnter( lua_State* L )
{
    m_shopActive = true;

    return 0;
}

int Shop::l_OnExit( lua_State* L )
{
    m_shopActive = false;

    return 0;
}

int Shop::l_FreeGold( lua_State* L )
{
    FreeGold();
    return 0;
}

int Shop::l_Preload( lua_State* L )
{
    Claw::Lua lua( L );
    AtlasManager::GetInstance()->Preload( lua.CheckEnum<AtlasSet::Type>( 1 ) );
    return 0;
}

void Shop::Focus( bool focus )
{
}

void Shop::Update( float dt )
{
    if( g_rng.GetDouble() < 0.2f )
    {
        UpdateCashEncryption();
    }

    if( MonetizationManager::GetInstance()->GetFreeGoldEarned() > 0 )
    {
        int gold = MonetizationManager::GetInstance()->GetFreeGoldEarned();
        UpdateCash( 0, gold );
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();

        m_lua->PushNumber( gold );
        m_lua->Call( "CountersCashEarned", 1, 0 );
        MonetizationManager::GetInstance()->FlushFreeGold();

        const char* freeGoldSystem = "";
        if( MonetizationManager::GetInstance()->IsTapjoySupported() )
        {
            freeGoldSystem = "Tapjoy";
        }
        else if( MonetizationManager::GetInstance()->IsMetapsSupported() )
        {
            freeGoldSystem = "Metaps";
        }
        else
        {
            CLAW_ASSERT( !"Cash from unknown system" );
        }
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_ACTION_FINISHED, gold, freeGoldSystem );
    }

    if( !TutorialManager::GetInstance()->IsActive() && !m_iabInitialized )
    {
        m_iabInitialized = true;
        MonetizationManager::GetInstance()->CheckBillingSupport();
        MonetizationManager::GetInstance()->GetIapStore()->CheckPendingTransactions();
    }
}

void Shop::UpdateIapCash( const ItemId& itemId )
{
    int cash = 0, gold = 0;
    GetItemPrice( itemId, cash, gold );

    int bonus = GetCashItemBonus( itemId );
    cash += (int)(cash * (bonus / 100.f) + 0.5);
    gold += (int)(gold * (bonus / 100.f) + 0.5);

    UpdateCash( cash, gold );

    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
}

void Shop::UpdateSubscription( const ItemId& itemId, bool active )
{
    Claw::Registry::Get()->Set( "/monstaz/subscription", active );
    UserDataManager::GetInstance()->SendSetVipStatus( active );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
}

bool Shop::IsSubscriptionActive()
{
    bool subscriptionActive = false;
    Claw::Registry::Get()->Get( "/monstaz/subscription", subscriptionActive );
    return subscriptionActive;
}

void Shop::TransactionFailed( const ClawExt::InAppTransaction& trans )
{
    CLAW_MSG( "Shop::TransactionFailed():" << trans.GetProductId() );
    CLAW_MSG_WARNING( m_transactionInProgress, "StoreManager::TransactionFailed() for item: " << trans.GetProductId() << " called when no transaction was running" );
    if( !m_transactionInProgress ) return;
    MonetizationManager::GetInstance()->GetPlayhaven()->NotifyPurchaseResult( ClawExt::Playhaven::PR_FAILED );
    m_lua->PushString( trans.GetProductId() );
    m_lua->Call( "OnTransactionFailed", 1, 0 );
    m_transactionInProgress = false;

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_FAILED, 1, "", (void*)&trans );
}

void Shop::TransactionCancel( const ClawExt::InAppTransaction& trans )
{
    CLAW_MSG( "Shop::TransactionCancel():" << trans.GetProductId() );
    CLAW_MSG_WARNING( m_transactionInProgress, "Shop::TransactionCancel() for item: " << trans.GetProductId() << " called when no transaction was running" );
    if( !m_transactionInProgress ) return;
    MonetizationManager::GetInstance()->GetPlayhaven()->NotifyPurchaseResult( ClawExt::Playhaven::PR_CANCELED );
    m_lua->PushString( trans.GetProductId() );
    m_lua->Call( "OnTransactionCancel", 1, 0 );
    m_transactionInProgress = false;

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_CANCELED, 1, "", (void*)&trans );
}

void Shop::TransactionComplete( const ClawExt::InAppTransaction& trans )
{
    CLAW_MSG( "Shop::TransactionComplete():" << trans.GetProductId() );
    CLAW_MSG_WARNING( m_transactionInProgress, "Shop::TransactionComplete() for item: " << trans.GetProductId() << " called when no transaction was running" );
    // Always process transaction complete
    // This is a political decision - if something goes wrong assume that item was bought 
    // so user won't lose his money.
    MonetizationManager::GetInstance()->GetPlayhaven()->NotifyPurchaseResult( ClawExt::Playhaven::PR_COMPLETED );

    UpdateIapCash( trans.GetProductId() );

    m_lua->PushString( trans.GetProductId() );
    m_lua->Call( "OnTransactionComplete", 1, 0 );
    m_transactionInProgress = false;

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_COMPLETED, 1, "", (void*)&trans );
}

void Shop::TransactionRestore( const ClawExt::InAppTransaction& trans )
{
    CLAW_MSG( "Shop::TransactionRestore():" << trans.GetProductId() );
    UpdateIapCash( trans.GetProductId() );

    m_lua->PushString( trans.GetProductId() );
    m_lua->Call( "OnTransactionRestore", 1, 0 );
}

void Shop::TransactionRefund( const ClawExt::InAppTransaction& trans )
{
    CLAW_MSG( "Shop::TransactionRefund():" << trans.GetProductId() );
}

void Shop::TransactionSupport( bool supported )
{
    CLAW_MSG( "Shop::TransactionSupport():" << supported );
    if( m_iapsSupported != supported )
    {
        m_iapsSupported = supported;

        if( !m_iapsSupported )
        {
            m_lua->Call( "OnTransactionNotSupported", 0, 0 );
        }
    }

    if( supported )
    {
        CheckSubscriptions();
    }
}

void Shop::OnVirtualCurrencyAward( const Claw::NarrowString& zondeId, const Claw::NarrowString& currencyName, int amount )
{
    UpdateCash( 0, amount );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();

    m_lua->PushNumber( amount );
    m_lua->Call( "CountersCashEarned", 1, 0 );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_ACTION_FINISHED, amount, "AdColony" );
}

void Shop::SubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active )
{
    CLAW_MSG( "Shop::SubscriptionStatus():" << subscriptionId << " status: " << active );
    if( active != IsSubscriptionActive() )
    {
        UpdateSubscription( subscriptionId, active );
        if( !active )
        {
            GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_CASH_ACTION_SUBSCRIPTION_CANELED, 1, subscriptionId );
        }
    }
}

void Shop::SubscriptionCancel( const Claw::NarrowString&  subscriptionId )
{
    CLAW_MSG( "Shop::SubscriptionCancel():" << subscriptionId );
    CLAW_MSG_WARNING( m_transactionInProgress, "Shop::SubscriptionCancel() for item: " << subscriptionId << " called when no transaction was running" );
    if( !m_transactionInProgress ) return;
    MonetizationManager::GetInstance()->GetPlayhaven()->NotifyPurchaseResult( ClawExt::Playhaven::PR_CANCELED );

    m_lua->PushString( subscriptionId );
    m_lua->Call( "OnTransactionCancel", 1, 0 );
    m_transactionInProgress = false;

    ClawExt::InAppTransaction trans( subscriptionId, 1 );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_CANCELED, 1, "", (void*)&trans );
}

void Shop::SubscriptionBought( const Claw::NarrowString&  subscriptionId )
{
    CLAW_MSG( "Shop::SubscriptionBought():" << subscriptionId );
    CLAW_MSG_WARNING( m_transactionInProgress, "Shop::SubscriptionBought() for item: " << subscriptionId << " called when no transaction was running" );
    // Always process transaction complete
    // This is a political decision - if something goes wrong assume that item was bought 
    // so user won't lose his money.
    MonetizationManager::GetInstance()->GetPlayhaven()->NotifyPurchaseResult( ClawExt::Playhaven::PR_COMPLETED );

    UpdateSubscription( subscriptionId, true );

    m_lua->PushString( subscriptionId );
    m_lua->Call( "OnTransactionComplete", 1, 0 );
    m_transactionInProgress = false;

    ClawExt::InAppTransaction trans( subscriptionId, 1 );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_COMPLETED, 1, "", (void*)&trans );
}

void Shop::SubscriptionAlreadyOwned( const Claw::NarrowString&  subscriptionId )
{
    CLAW_MSG( "Shop::SubscriptionAlreadyOwned():" << subscriptionId );

    UpdateSubscription( subscriptionId, true );

    m_lua->PushString( subscriptionId );
    m_lua->Call( "OnTransactionComplete", 1, 0 );
}

void Shop::SubscriptionFailed( const Claw::NarrowString&  subscriptionId )
{
    CLAW_MSG( "Shop::SubscriptionFailed():" << subscriptionId );
    CLAW_MSG_WARNING( m_transactionInProgress, "Shop::SubscriptionFailed() for item: " << subscriptionId << " called when no transaction was running" );
    if( !m_transactionInProgress ) return;
    MonetizationManager::GetInstance()->GetPlayhaven()->NotifyPurchaseResult( ClawExt::Playhaven::PR_FAILED );

    m_lua->PushString( subscriptionId );
    m_lua->Call( "OnTransactionFailed", 1, 0 );
    m_transactionInProgress = false;

    ClawExt::InAppTransaction trans( subscriptionId, 1 );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TRANSACTION_FAILED, 1, "", (void*)&trans );
}

void Shop::OnMakePurchase( const Claw::NarrowString& iapId )
{
    BuyVirtualCash( iapId );
}

void Shop::OnUnlockReward( const Claw::NarrowString& rewardName, int rewardQuantity )
{
    CLAW_ASSERT( !"Playhaven rewards not supported!" );
}

// EOF

#include "claw/base/Errors.hpp"

#include "MonstazAI/GameCenterPopup.hpp"

#ifndef CLAW_IPHONE

void GameCenterPopup::Initialize()
{
    CLAW_MSG( "Initialize GameCenter selection popup" );
}

void GameCenterPopup::Release()
{
    CLAW_MSG( "Release GameCenter selection popup" );
}

void GameCenterPopup::Show( bool authenticate )
{
    CLAW_MSG( "Show GameCenter selection popup" );
}

#endif

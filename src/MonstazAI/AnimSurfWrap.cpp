#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/AnimSurfWrap.hpp"

AnimSurfWrap::AnimSurfWrap( Claw::AnimatedSurface* surface, const Vectorf& pos )
    : Renderable( pos )
    , m_surface( surface )
    , m_frame( 0 )
    , m_time( surface->GetTime( 0 ) )
    , m_transform( NoTransform )
{
}

AnimSurfWrap::AnimSurfWrap( Claw::AnimatedSurface* surface, const Vectorf& pos, const Vectorf& vec )
    : Renderable( pos )
    , m_surface( surface )
    , m_frame( 0 )
    , m_time( surface->GetTime( 0 ) )
    , m_transform( Rotate )
    , m_rot( vec )
{
}

AnimSurfWrap::AnimSurfWrap( Claw::AnimatedSurface* surface, const Vectorf& pos, float angle )
    : Renderable( pos )
    , m_surface( surface )
    , m_frame( 0 )
    , m_time( surface->GetTime( 0 ) )
    , m_transform( Rotate )
    , m_rot( sin( angle ), cos( angle ) )
{
}

void AnimSurfWrap::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_surface->SetFrame( m_frame );

    if( scale != 1.0f )
    {
        switch( m_transform )
        {
        case NoTransform:
            target->Blit( m_pos.x * scale - offset.x, m_pos.y * scale - offset.y, m_surface );
            break;
        case Rotate:
            {
            float m[4] = { m_rot.y, -m_rot.x, -m_rot.x, -m_rot.y };
            Claw::TriangleEngine::Blit( target, m_surface, m_pos.x * scale - offset.x, m_pos.y * scale - offset.y, m, Vectorf( 0, 0 ) );
            }
            break;
        default:
            CLAW_ASSERT( false );
            break;
        }
    }
    else
    {
        switch( m_transform )
        {
        case NoTransform:
            target->Blit( m_pos.x - offset.x, m_pos.y - offset.y, m_surface );
            break;
        case Rotate:
            {
            float m[4] = { m_rot.y, -m_rot.x, -m_rot.x, -m_rot.y };
            Claw::TriangleEngine::Blit( target, m_surface, m_pos.x - offset.x, m_pos.y - offset.y, m, Vectorf( 0, 0 ) );
            }
            break;
        default:
            CLAW_ASSERT( false );
            break;
        }
    }
}

bool AnimSurfWrap::Update( float dt )
{
    m_time -= dt;

    while( m_time < 0 )
    {
        m_surface->SetFrame( m_frame );
        if( !m_surface->NextFrame() )
        {
            return false;
        }
        m_frame = m_surface->GetFrame();
        m_time += m_surface->GetTime( m_frame );
    }

    return true;
}

Claw::Surface* AnimSurfWrap::GetRawSurface( int& x, int& y )
{
    const Claw::AnimatedSurface::Frame& frame = m_surface->GetRawFrame( m_frame );
    x = frame.x;
    y = frame.y;
    return frame.surface;
}

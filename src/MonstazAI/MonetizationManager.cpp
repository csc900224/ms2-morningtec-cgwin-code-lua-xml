// Internal includes
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/Application.hpp"

// Monetization engines 
#include "claw_ext/monetization/chartboost/Chartboost.hpp"
#include "claw_ext/monetization/playhaven/Playhaven.hpp"
#include "claw_ext/monetization/appsflyer/AppsFlyer.hpp"
#include "claw_ext/monetization/adcolony/AdColony.hpp"

// External includes
#include "claw/application/AbstractApp.hpp"
#include "claw/base/Registry.hpp"

#include <sstream>

// Chartboost ids
#ifdef CLAW_ANDROID
#ifdef AMAZON_BUILD
static const char* CHARTBOOST_APP_ID            = "0";
static const char* CHARTBOOST_APP_SGINATURE     = "0";
#else
static const char* CHARTBOOST_APP_ID            = "0";
static const char* CHARTBOOST_APP_SGINATURE     = "0";
#endif
#else
static const char* CHARTBOOST_APP_ID            = "0";
static const char* CHARTBOOST_APP_SGINATURE     = "0";
#endif

// Playhaven ids
#ifdef CLAW_ANDROID
#ifdef AMAZON_BUILD
static const char* PLAYHAVEN_TOKEN              = "0";
static const char* PLAYHAVEN_SECRET             = "0";
#else
static const char* PLAYHAVEN_TOKEN              = "0";
static const char* PLAYHAVEN_SECRET             = "0";
#endif
#else
static const char* PLAYHAVEN_TOKEN              = "0";
static const char* PLAYHAVEN_SECRET             = "0";
#endif

// Tapjoy ids
#ifdef CLAW_ANDROID
#ifdef AMAZON_BUILD
static const char* TAPJOY_APP_ID                = "0";
static const char* TAPJOY_SECRET                = "0";
static const char* TAPJOY_ACTION_IDS[MonetizationManager::TA_NUM] = 
{
    "0"
};
#else
static const char* TAPJOY_APP_ID                = "0";
static const char* TAPJOY_SECRET                = "0";
static const char* TAPJOY_ACTION_IDS[MonetizationManager::TA_NUM] = 
{
    "0"
};
#endif
#else
static const char* TAPJOY_APP_ID                = "0";
static const char* TAPJOY_SECRET                = "0";
static const char* TAPJOY_ACTION_IDS[MonetizationManager::TA_NUM] = 
{
    "0"
};
#endif

// Metaps ids
static const char* METAPS_USER_ID               = "0";
static const char* METAPS_APP_ID                = "0";
static const char* METAPS_APP_KEY               = "0";

// AppsFlyer ids
#ifdef CLAW_ANDROID
static const char* APPSFLYER_ID                 = "0";
#else
static const char* APPSFLYER_ID                 = "0";
#endif

// AdColony ids
static const char* ADCOLONY_APP_ID              = "0";
static const char* ADCOLONY_ZONE_1_ID           = "0";

static const char* ADS_ENABLED_PATH             = "/monstaz/monetization/ads-enbaled";
static const char* METAPS_ENABLED_PATH          = "/monstaz/monetization/metaps-enbaled";

static const char* IAP_PUBLIC_KEY               = "0";

#if defined IAP_SERVER_VERIFICATION
#ifdef CLAW_IPHONE
IAP_SERVER_ADDR("http://127.0.0.1:1043//");
#else
IAP_SERVER_ADDR("http://127.0.0.1:1043//");
#endif
#endif

MonetizationManager::MonetizationManager()
    : m_chartboost( NULL )
    , m_playhaven( NULL )
    , m_tapjoy( NULL )
    , m_metaps( NULL )
    , m_appsFlyer( NULL )
    , m_freeGoldCache( 0 )
    , m_progressLoaded( false )
    , m_skipPhResume( false )
{
    Initialize();
}

MonetizationManager::~MonetizationManager()
{
    ClawExt::Chartboost::Release();
    ClawExt::Playhaven::Release();
    ClawExt::AppsFlyer::Release();
    ClawExt::AdColony::Release();
    if( m_tapjoy ) ClawExt::Tapjoy::Release();
    if( m_metaps ) ClawExt::Metaps::Release();
}

void MonetizationManager::OnFocusChange( bool focus )
{
    if( focus )
    {
        if( m_chartboost )
            m_chartboost->OnResume();

        if( m_playhaven )
        {
            m_playhaven->ReportOpen();
            if( !m_skipPhResume )
            {
                if( m_progressLoaded && !TutorialManager::GetInstance()->IsActive() )
                {
                    m_playhaven->ContentPlacement( "resume" );
                }
            }
            else
            {
                m_skipPhResume = false;
            }
        }

        if( m_progressLoaded )
        {
            if( m_tapjoy ) m_tapjoy->CheckPoints();
            if( m_metaps ) m_metaps->RunInstallReport();
        }
    }

    if( m_tapjoy )
    {
        m_tapjoy->OnFocusChange( focus );
    }
}

void MonetizationManager::Initialize()
{
    // Chartboost
    m_chartboost = ClawExt::Chartboost::QueryInterface();
    m_chartboost->Initialize( CHARTBOOST_APP_ID, CHARTBOOST_APP_SGINATURE );

    // Playhaven
    m_playhaven = ClawExt::Playhaven::QueryInterface();
    m_playhaven->Initialize( PLAYHAVEN_TOKEN, PLAYHAVEN_SECRET );
    m_playhaven->RegisterPlacementObserver( this );

    // AppsFlyer
    m_appsFlyer = ClawExt::AppsFlyer::QueryInterface();
    m_appsFlyer->Initialize( APPSFLYER_ID );
    m_appsFlyer->ReportInstall();

    // AdColony
    m_adColony = ClawExt::AdColony::QueryInterface();
    m_adColony->Initialize( ADCOLONY_APP_ID );
    m_adColony->AddZoneDefinition( ACZ_1, ADCOLONY_ZONE_1_ID );

     // IAPS
#if defined IAP_SERVER_VERIFICATION
#ifdef AMAZON_BUILD
    m_iaps = ClawExt::InAppStore::QueryInterface( NULL, "save/.iap", ClawExt::InAppStore::ST_AMAZON );
#else
    m_iaps = ClawExt::InAppStore::QueryInterface( NULL, "save/.iap" );
#endif
#else
#ifdef AMAZON_BUILD
    m_iaps = ClawExt::InAppStore::QueryInterface( ClawExt::InAppStore::ST_AMAZON );
#else
    m_iaps = ClawExt::InAppStore::QueryInterface();
#endif
#endif
    m_iaps->RegisterTransObserver( this );
}

void MonetizationManager::Update( float dt )
{
#if defined IAP_SERVER_VERIFICATION
    m_iaps->Update();
#endif
}

void MonetizationManager::SkipPlayhavenResume()
{
    m_skipPhResume = true;
}

void MonetizationManager::OnProgressLoaded()
{
    // Make sure Tapjoy is intialized after progress is loaded.
    // Otherwise you can recive callback with poinst update before that.
    if( ShouldUseMetaps() )
    {
        // Metaps
        m_metaps = ClawExt::Metaps::QueryInterface();
        m_metaps->RegisterObserver( this );
        m_metaps->Initialize( METAPS_USER_ID, METAPS_APP_ID, METAPS_APP_KEY );
        m_metaps->RunInstallReport();
    }
    else
    {
        // Tapjoy
        m_tapjoy = ClawExt::Tapjoy::QueryInterface();
        m_tapjoy->RegisterObserver( this );
        m_tapjoy->Initialize( TAPJOY_APP_ID, TAPJOY_SECRET );
    }
    m_progressLoaded = true;
}

void MonetizationManager::CheckBillingSupport()
{
    ClawExt::InAppStore::Message publicKey( IAP_PUBLIC_KEY );
    m_iaps->CheckBillingSupport( &publicKey );
}

bool MonetizationManager::AreAdsEnabled() const
{
    bool adsEnabled = true;
    Claw::Registry::Get()->Get( ADS_ENABLED_PATH, adsEnabled );
    return adsEnabled;
}

void MonetizationManager::SetAdsEnabled( bool enabled )
{
    if( AreAdsEnabled() != enabled )
    {
        Claw::Registry::Get()->Set( ADS_ENABLED_PATH, enabled );
        ((MonstazApp*)MonstazApp::GetInstance())->Save();
    }
}

void MonetizationManager::ReportTapjoyAction( TapjoyAction action )
{
    if( m_tapjoy )
    {
        m_tapjoy->ReportActionComplete( TAPJOY_ACTION_IDS[action] );
    }
}

void MonetizationManager::FlushFreeGold()
{
    m_freeGoldCache = 0;
}

void MonetizationManager::TapjoyPointsEarned( int points )
{
    m_freeGoldCache += points;
    m_tapjoy->FlushCash( points );
}

void MonetizationManager::TapjoyClosed()
{
    m_tapjoy->CheckPoints();
}

void MonetizationManager::MetapsCurrencyEarned( int currencyAmount )
{
    m_freeGoldCache += currencyAmount;
}

void MonetizationManager::OnPlacementShown( const Claw::NarrowString& placement )
{
    m_skipPhResume = true;
}

void MonetizationManager::TransactionComplete( const ClawExt::InAppTransaction& trans )
{
    ReportAppsFlyerTransaction( trans );
}

void MonetizationManager::TransactionRestore( const ClawExt::InAppTransaction& trans )
{
    ReportAppsFlyerTransaction( trans );
}

void MonetizationManager::ReportAppsFlyerTransaction( const ClawExt::InAppTransaction& trans )
{
    float usdPrice = Shop::GetInstance()->GetRealItemPriceInUSD( trans.GetProductId() ) / 100.0f;
    std::ostringstream valueStream;
    valueStream.setf( std::ios::fixed, std::ios::floatfield );
    valueStream.precision(2);
    valueStream << usdPrice;
    m_appsFlyer->ReportConversionEvent( trans.GetProductId().c_str(), valueStream.str().c_str() );
}

bool MonetizationManager::ShouldUseMetaps()
{
    // Metaps
    bool metapsEnabled = false;
#ifdef USE_METAPS
    if ( !Claw::Registry::Get()->Get( METAPS_ENABLED_PATH, metapsEnabled ) )
    {
        //Determine if we should enable metaps by phone country code
        Claw::NarrowString countryCode = MonstazApp::GetNetworkCountryCode();
        if (countryCode.length() == 0)
        {
            countryCode = MonstazApp::GetOsCountryCode();
        }
        metapsEnabled = IsMetapsCountryCode( countryCode );

        Claw::Registry::Get()->Set( METAPS_ENABLED_PATH, metapsEnabled );
        ((MonstazApp*)MonstazApp::GetInstance())->Save();
    }
#endif
    return metapsEnabled;
}

 bool MonetizationManager::IsMetapsCountryCode(const Claw::NarrowString& countryCode)
{
    static const char* MetapsCountryCodes[] = 
    {
        "JP", "JPN", "KP", "PRK", "KR", "KOR", "TW", "TWN", "HK", "HKG", "TH", "THA", "MY", "MYS", "SG", "SGP", "PH", "PHL", "VN", "VNM", "ID", "IDN"
    };
    static const int CountryCodesAmount = sizeof(MetapsCountryCodes) / sizeof(MetapsCountryCodes[0]);

    Claw::NarrowString ccUpperCase;
    ccUpperCase.resize( countryCode.size() );
    std::transform(countryCode.begin(), countryCode.end(), ccUpperCase.begin(), ::toupper);

    for (int i = 0; i < CountryCodesAmount; ++i) 
    {
        if (ccUpperCase == MetapsCountryCodes[i]) 
        {
            return true;
        }
    }
    return false;
}
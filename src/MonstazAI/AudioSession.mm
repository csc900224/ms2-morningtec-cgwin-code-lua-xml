/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      MonstazAI/AudioSession.mm
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2011, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

// Internal includes
#include "MonstazAI/AudioSession.hpp"

// External includes
#import <AVFoundation/AVAudioSession.h>
#import <AudioToolbox/AudioServices.h>
#import <Foundation/NSNotification.h>
#import <MediaPlayer/MPMusicPlayerController.h>

static void (*s_callback)(void);

@interface CallbackWrapper : NSObject
{
}
+(id) sharedInstance;
-(void) onIPodPlaybackStateChanged:(NSNotification*) notification;
@end

@implementation CallbackWrapper

static CallbackWrapper* s_instance = NULL;

+(id) sharedInstance
{
    @synchronized(self)
    {
		if(s_instance == nil)
        {
			s_instance = [[CallbackWrapper alloc] init];
		}
	}
    return s_instance;
}

-(void) onIPodPlaybackStateChanged:(NSNotification*) notification
{
    if( s_callback )
    {
        s_callback();
    }
}
@end


void AudioSession::SetCategory( AudioSession::Category category )
{
    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    NSError* setCategoryError = nil;

    switch( category )
    {
    case C_AMBIENT:
        [audioSession setCategory:AVAudioSessionCategoryAmbient error:&setCategoryError];
        break;
    case C_SOLO_AMBIENT:
        [audioSession setCategory:AVAudioSessionCategorySoloAmbient error:&setCategoryError];
        break;
    case C_RECORD:
        [audioSession setCategory:AVAudioSessionCategoryRecord error:&setCategoryError];
        break;
    case C_PLAYBACK:
        [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
        break;
    case C_PLAY_AND_RECORD:
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&setCategoryError];
        break;
    case C_AUDIO_PROCESSING:
        [audioSession setCategory:AVAudioSessionCategoryAudioProcessing error:&setCategoryError];
        break;
    default:
        NSLog(@"Unknown AVAudioSessionCategory");
        abort();
        break;
    }
}

bool AudioSession::IsOtherAudioPlaying()
{
    UInt32 otherAudioIsPlaying;
    UInt32 propertySize = sizeof(otherAudioIsPlaying);

    AudioSessionGetProperty( kAudioSessionProperty_OtherAudioIsPlaying, &propertySize, &otherAudioIsPlaying );
    return otherAudioIsPlaying;
}

void AudioSession::RegisterIPodPlaybackStateChangedEvent( void (*callback)(void) )
{
    if( !s_callback )
    {
        [[NSNotificationCenter defaultCenter] addObserver:[CallbackWrapper sharedInstance] selector:@selector(onIPodPlaybackStateChanged:) name:MPMusicPlayerControllerPlaybackStateDidChangeNotification object:nil];
        [[MPMusicPlayerController iPodMusicPlayer] beginGeneratingPlaybackNotifications];
    }
    s_callback = callback;
}

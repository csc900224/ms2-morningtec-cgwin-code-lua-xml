#include "claw/base/Registry.hpp"
#include "claw/math/Math.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/graphics/Batcher.hpp"
#include "claw/graphics/OpenGLShader.hpp"

#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/TouchControls.hpp"
#include "MonstazAI/entity/effect/EffectHealth.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/Application.hpp"

enum { _TouchActiveRadius = 125 };
enum { _TouchRadius = 25 };
enum { _TouchSlideRadius = 35 };
enum { _TouchShotRadius = 10 };
enum { _TouchNipple = 20 };
enum { _TouchBorder = 40 };
enum { _LimitY = 200 };

enum { SlideMin = 39 };
enum { SlideMax = 74 };
enum { SlideMargin = 10 };

TouchControls::TouchControls()
    : m_scale( GameManager::GetGameScale() )
    , m_leftBtnTime( 0 )
    , m_rightBtnTime( 0 )
    , m_mineBtnTime( 0 )
    , m_shieldBtnTime( 0 )
    , m_switchBtnTime( 0 )
    , TouchActiveRadius( _TouchActiveRadius * m_scale )
    , TouchRadius( _TouchRadius * m_scale )
    , TouchShotRadius( _TouchShotRadius * m_scale )
    , TouchNipple( _TouchNipple * m_scale )
    , TouchBorder( _TouchBorder * m_scale )
    , LimitY( _LimitY * m_scale )
    , TouchOffset( TouchRadius + TouchBorder )
    , TouchSquare( TouchRadius * TouchRadius )
    , TouchSlideSquare( _TouchSlideRadius * _TouchSlideRadius * m_scale * m_scale )
    , TouchShotSquare( TouchShotRadius * TouchShotRadius )
    , TouchActiveSquare( TouchActiveRadius * TouchActiveRadius )
    , m_bg( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/controller_bg.png@linear" ) )
    , m_decorL( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/controller_l_decoration.png@linear" ) )
    , m_decorR( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/controller_r_decoration.png@linear" ) )
    , m_left( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/controller_move.png@linear" ) )
    , m_font( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_small.xml@linear" ) )
    , m_leftNum( -1 )
    , m_rightNum( -1 )
    , m_mineNum( -1 )
    , m_shieldNum( -1 )
    , m_fixed( false )
    , m_deadTimer( 5 )
    , m_autoaim( GameManager::GetInstance()->IsAutoaim() )
    , m_slideButton( InactiveTouch )
    , m_slidePos( SlideMin )
    , m_slideTimer( 0 )
    , m_fingerVisible( false )
    , m_fingerTimer( 0 )
{
    m_right.Reset( Claw::AssetDict::Get<Claw::Surface>( m_autoaim ? "gfx/hud/controller_shot_auto.png@linear" : "gfx/hud/controller_shot.png@linear" ) );

    m_bg->SetAlpha( 255 );
    m_decorL->SetAlpha( 255 );
    m_decorR->SetAlpha( 255 );
    m_left->SetAlpha( 255 );
    m_right->SetAlpha( 255 );

    GameManager::GetInstance()->TouchEnable( true );

    Claw::Registry::Get()->RegisterCallback( "/monstaz/settings/fixedvpad", FixedVPadSwitchCallback, this );

    FixedVPadSwitch();

    m_grenade[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_grenade_button.png@linear" ) );
    m_grenade[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_grenade_button_push.png@linear" ) );
    m_grenade[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_grenade_ico.png@linear" ) );
    m_health[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_health_button.png@linear" ) );
    m_health[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_health_button_push.png@linear" ) );
    m_health[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_health_ico.png@linear" ) );
    m_mine[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_mine_button.png@linear" ) );
    m_mine[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_mine_button_push.png@linear" ) );
    m_mine[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_mine_ico.png@linear" ) );
    m_shield[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_shield_button.png@linear" ) );
    m_shield[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_shield_button_push.png@linear" ) );
    m_shield[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_shield_ico.png@linear" ) );
    m_switch[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_weapon_swap_button.png@linear" ) );
    m_switch[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_weapon_swap_button_push.png@linear" ) );
    m_finger.Reset( Claw::AssetDict::Get<Claw::Surface>( "menu2/finger.png@linear" ) );
    m_arrow.Reset( Claw::AssetDict::Get<Claw::Surface>( "menu2/arrow_small.png@linear" ) );

    std::fill( m_touchHandled, m_touchHandled+10, false );
}

TouchControls::~TouchControls()
{
    Claw::Registry::Get()->RemoveCallback( "/monstaz/settings/fixedvpad", FixedVPadSwitchCallback, this );
}

void TouchControls::Render( Claw::Surface* target )
{
    if( GameManager::GetInstance()->GetMenu() ) return;

    if( m_autoaim )
    {
        m_data[1].pos = Vectorf( 0, 0 );
    }

    if( m_data[0].visible )
    {
        m_bg->SetAlpha( m_data[0].time * 255 );
        m_left->SetAlpha( m_data[0].time * 255 );
        m_decorL->SetAlpha( m_data[0].time * 255 );
        m_decorR->SetAlpha( m_data[0].time * 255 );
        target->Blit( m_data[0].center.x - m_bg->GetWidth() / 2, m_data[0].center.y - m_bg->GetWidth() / 2, m_bg );
        target->Blit( m_data[0].center.x - m_bg->GetWidth() / 2 + 5 * m_scale, m_data[0].center.y - m_bg->GetWidth() / 2 + 12 * m_scale, m_decorL );
        target->Blit( m_data[0].center.x - m_bg->GetWidth() / 2 + 88 * m_scale, m_data[0].center.y - m_bg->GetWidth() / 2 + 74 * m_scale, m_decorR );
        target->Blit( m_data[0].center.x + m_data[0].pos.x - m_left->GetWidth() / 2, m_data[0].center.y + m_data[0].pos.y - m_left->GetHeight() / 2, m_left );

        if( Shop::GetInstance()->CheckOwnership( Shop::Items::HealthKit ) > 0 )
        {
            Vectorf pos( GetMedkitPosition() );

            m_health[0]->SetAlpha( m_data[0].time * 255 );
            target->Blit( pos, m_health[0] );

            if( m_leftBtnTime > 0 )
            {
                m_health[1]->SetAlpha( m_data[0].time * 255 );
                target->Blit( pos + Vectorf( 16, 3 ) * m_scale, m_health[1] );
            }

            m_health[2]->SetAlpha( m_data[0].time * 255 );
            target->Blit( pos + Vectorf( 22, 14 ) * m_scale, m_health[2] );

            if( m_healthText )
            {
                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Font );
                Claw::OpenGLShader* s = (Claw::OpenGLShader*)Claw::g_batcher->GetTexturingShader();
                s->Uniform( "color1", 161/255.f, 202/255.f, 243/255.f );
                s->Uniform( "color2", 6/255.f, 19/255.f, 59/255.f );
                s->Uniform( "color3", 1, 1, 1 );

                m_font->GetSurface()->SetAlpha( m_data[0].time * 128 );
                pos += Vectorf( 8, 47 ) * m_scale;
                m_healthText->Draw( target, pos.x - m_healthText->GetWidth() / 2, pos.y );

                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Normal );
            }
        }
        if( Shop::GetInstance()->CheckOwnership( Shop::Items::Shield ) > 0 )
        {
            Vectorf pos( GetShieldPosition() );

            m_shield[0]->SetAlpha( m_data[0].time * 255 );
            target->Blit( pos, m_shield[0] );

            if( m_shieldBtnTime > 0 )
            {
                m_shield[1]->SetAlpha( m_data[0].time * 255 );
                target->Blit( pos + Vectorf( 16, 3 ) * m_scale, m_shield[1] );
            }

            m_shield[2]->SetAlpha( m_data[0].time * 255 );
            target->Blit( pos + Vectorf( 21, 4 ) * m_scale, m_shield[2] );

            if( m_shieldText )
            {
                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Font );
                Claw::OpenGLShader* s = (Claw::OpenGLShader*)Claw::g_batcher->GetTexturingShader();
                s->Uniform( "color1", 161/255.f, 202/255.f, 243/255.f );
                s->Uniform( "color2", 6/255.f, 19/255.f, 59/255.f );
                s->Uniform( "color3", 1, 1, 1 );

                m_font->GetSurface()->SetAlpha( m_data[0].time * 128 );
                pos += Vectorf( 8, 2 ) * m_scale;
                m_shieldText->Draw( target, pos.x - m_shieldText->GetWidth() / 2, pos.y );

                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Normal );
            }
        }
    }
    if( m_data[1].visible )
    {
        m_right->SetAlpha( m_data[1].time * 255 );
        if( !m_autoaim )
        {
            m_bg->SetAlpha( m_data[1].time * 255 );
            m_decorL->SetAlpha( m_data[1].time * 255 );
            m_decorR->SetAlpha( m_data[1].time * 255 );
            target->Blit( m_data[1].center.x - m_bg->GetWidth() / 2, m_data[1].center.y - m_bg->GetWidth() / 2, m_bg );
            target->Blit( m_data[1].center.x - m_bg->GetWidth() / 2 + 5 * m_scale, m_data[1].center.y - m_bg->GetWidth() / 2 + 12 * m_scale, m_decorL );
            target->Blit( m_data[1].center.x - m_bg->GetWidth() / 2 + 88 * m_scale, m_data[1].center.y - m_bg->GetWidth() / 2 + 74 * m_scale, m_decorR );
        }
        target->Blit( m_data[1].center.x + m_data[1].pos.x - m_right->GetWidth() / 2, m_data[1].center.y + m_data[1].pos.y - m_right->GetHeight() / 2, m_right );

        if( Shop::GetInstance()->CheckOwnership( Shop::Items::Grenade ) > 0 )
        {
            Vectorf pos( GetGrenadePosition() );

            m_grenade[0]->SetAlpha( m_data[1].time * 255 );
            target->Blit( pos, m_grenade[0] );

            if( m_rightBtnTime > 0 )
            {
                m_grenade[1]->SetAlpha( m_data[1].time * 255 );
                target->Blit( pos + Vectorf( 3, 3 ) * m_scale, m_grenade[1] );
            }

            m_grenade[2]->SetAlpha( m_data[1].time * 255 );

            TutorialManager* tm = TutorialManager::GetInstance();
            if ( tm->IsActive() && tm->GetCurrentChapter() == TutorialChapter::Items && tm->IsTaskActive( TutorialTask::ItemsUse ) )
            {
                float s = sin( Claw::Time::GetTimeMs()/1000.0f * 8 );
                s = s * s;
                s = s * s;

                const float scale = 1 + s * 0.17f;
                const float angle = sin( Claw::Time::GetTimeMs()/1000.0f * 8.41f ) * M_PI * 0.025f;
                Claw::TriangleEngine::Blit( target, m_grenade[2], pos.x + 4*m_scale + m_grenade[2]->GetWidth()/2, pos.y + 5*m_scale + m_grenade[2]->GetHeight()/2, angle, scale, Claw::Vectorf( m_grenade[2]->GetSize()/2 ) );
            }
            else
            {
                target->Blit( pos + Vectorf( 4, 5 ) * m_scale, m_grenade[2] );
            }

            if( m_grenadeText )
            {
                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Font );
                Claw::OpenGLShader* s = (Claw::OpenGLShader*)Claw::g_batcher->GetTexturingShader();
                s->Uniform( "color1", 161/255.f, 202/255.f, 243/255.f );
                s->Uniform( "color2", 6/255.f, 19/255.f, 59/255.f );
                s->Uniform( "color3", 1, 1, 1 );

                m_font->GetSurface()->SetAlpha( m_data[1].time * 128 );
                pos += Vectorf( 41, 47 ) * m_scale;
                m_grenadeText->Draw( target, pos.x - m_grenadeText->GetWidth() / 2, pos.y );

                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Normal );
            }
        }
        if( Shop::GetInstance()->CheckOwnership( Shop::Items::Mine ) > 0 )
        {
            Vectorf pos( GetMinePosition() );

            m_mine[0]->SetAlpha( m_data[1].time * 255 );
            target->Blit( pos, m_mine[0] );

            if( m_mineBtnTime > 0 )
            {
                m_mine[1]->SetAlpha( m_data[1].time * 255 );
                target->Blit( pos + Vectorf( 3, 3 ) * m_scale, m_mine[1] );
            }

            m_mine[2]->SetAlpha( m_data[1].time * 255 );

            TutorialManager* tm = TutorialManager::GetInstance();
            if ( tm->IsActive() && tm->GetCurrentChapter() == TutorialChapter::Items && tm->IsTaskActive( TutorialTask::ItemsUse ) )
            {
                float s = sin( Claw::Time::GetTimeMs()/1000.0f * 8 );
                s = s * s;
                s = s * s;

                const float scale = 1 + s * 0.17f;
                const float angle = sin( Claw::Time::GetTimeMs()/1000.0f * 8.41f ) * M_PI * 0.025f;
                Claw::TriangleEngine::Blit( target, m_mine[2], pos.x + 4*m_scale + m_mine[2]->GetWidth()/2, pos.y + 5*m_scale + m_mine[2]->GetHeight()/2, angle, scale, Claw::Vectorf( m_mine[2]->GetSize()/2 ) );
            }
            else
            {
                target->Blit( pos + Vectorf( 4, 5 ) * m_scale, m_mine[2] );
            }

            if( m_mineText )
            {
                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Font );
                Claw::OpenGLShader* s = (Claw::OpenGLShader*)Claw::g_batcher->GetTexturingShader();
                s->Uniform( "color1", 161/255.f, 202/255.f, 243/255.f );
                s->Uniform( "color2", 6/255.f, 19/255.f, 59/255.f );
                s->Uniform( "color3", 1, 1, 1 );

                m_font->GetSurface()->SetAlpha( m_data[1].time * 128 );
                pos += Vectorf( 41, 2 ) * m_scale;
                m_mineText->Draw( target, pos.x - m_mineText->GetWidth() / 2, pos.y );

                Claw::g_batcher->SetTexturingShader( Claw::Batcher::Normal );
            }
        }

        if( m_weaponsNum > 1 && TutorialManager::GetInstance()->IsWeaponChangeAllowed() )
        {
            Vectorf pos( GetSwapPosition() );

            m_switch[0]->SetAlpha( m_data[1].time * 255 );
            m_switch[1]->SetAlpha( m_data[1].time * 255 );
            m_weapons[0]->SetAlpha( m_data[1].time * 255 );
            m_weapons[1]->SetAlpha( m_data[1].time * 255 );

            target->Blit( pos, m_switch[0] );

            if ( m_fingerVisible )
            {
                float slide = SlideMin;
                for ( int i = 0; slide <= SlideMax; ++i, slide += ( SlideMax - SlideMin )/5 )
                {
                    m_arrow->SetAlpha( 64 + ( cos( m_fingerTimer*8.0f - i*0.9f ) + 1 )*96 );
                    Claw::TriangleEngine::Blit( target, m_arrow, m_data[1].center.x, m_data[1].center.y, slide/75.0f - M_PI*0.21f, 1, Claw::Vectorf( 8, 80 ) * m_scale );
                }
            }

            Claw::TriangleEngine::Blit( target, m_switch[1], m_data[1].center.x, m_data[1].center.y, m_slidePos / 75 - M_PI * 0.21f, 1, Claw::Vectorf( 8, 80 ) * m_scale );
            Claw::TriangleEngine::Blit( target, m_weapons[0], pos.x + 10 * m_scale, pos.y + 17 * m_scale, 0, 0.35f, Vectorf( m_weapons[0]->GetSize() / 2 ) );
            Claw::TriangleEngine::Blit( target, m_weapons[1], pos.x + 83 * m_scale, pos.y + 27 * m_scale, 0, 0.5f, Vectorf( m_weapons[1]->GetSize() / 2 ) );

            if ( m_fingerVisible )
            {
                const float slide = fmod( m_fingerTimer, 1.0f )*( SlideMax - SlideMin ) + SlideMin;

                m_switch[1]->SetAlpha( m_data[1].time * 128 );
                Claw::TriangleEngine::Blit( target, m_switch[1], m_data[1].center.x, m_data[1].center.y, slide/75.0f - M_PI*0.21f, 1, Claw::Vectorf( 8, 80 ) * m_scale );

                m_finger->SetAlpha( 255*( Claw::SmoothStep<float>( 0, 0.2, fmod( m_fingerTimer, 1.0f ) ) - Claw::SmoothStep<float>( 0.8, 1.0, fmod( m_fingerTimer, 1.0f ) ) ) );
                Claw::TriangleEngine::Blit( target, m_finger, m_data[1].center.x, m_data[1].center.y, slide/75.0f - M_PI*0.21f, 1, Claw::Vectorf( 8, 80 ) * m_scale );
            }
        }
    }
}

void TouchControls::Update( float dt )
{
    m_leftBtnTime -= dt;
    m_rightBtnTime -= dt;
    m_mineBtnTime -= dt;
    m_shieldBtnTime -= dt;
    m_switchBtnTime -= dt;

    if ( m_fingerVisible )
    {
        m_fingerTimer += dt;
    }

    m_deadTimer -= dt;
    if( m_data[0].button != InactiveTouch || m_data[1].button != InactiveTouch || m_slideButton != InactiveTouch)
    {
        ResetDeadTimer( 5 );
    }

    for( int i=0; i<2; i++ )
    {
        if( m_data[i].button != InactiveTouch )
        {
#ifndef CLAW_ANDROID
            Vectorf d = m_data[i].target - m_data[i].pos;
            //float l = Claw::Min( d.Length(), dt * 10 );
            //m_data[i].pos += d * l;
            m_data[i].pos = m_data[i].target;
#endif
        }
        else
        {
            if( m_data[i].visible && !m_fixed )
            {
                m_data[i].time -= dt;
                if( m_data[i].time <= 0 )
                {
                    m_data[i].visible = false;
                }
            }
        }
    }

    if( m_slideButton == InactiveTouch && m_slidePos != SlideMin )
    {
        m_slideTimer += dt;
        while( m_slideTimer > 1/60.f )
        {
            m_slideTimer -= 1/60.f;
            m_slidePos = std::max<float>( SlideMin, m_slidePos - std::max( 0.2f, ( m_slidePos - SlideMin ) / 7.5f ) );
        }
    }

    static const float div = 1.0f / TouchRadius;

    Vectorf off0( m_data[0].pos * div );
    Vectorf off1( m_data[1].pos * div );

    if( off0.LengthSqr() > 0.01f )
    {
        off0.Normalize();
    }

    if( m_autoaim )
    {
        if( m_data[1].button != InactiveTouch )
        {
            GameManager::GetInstance()->TouchUpdate( off0, Vectorf( 1, 1 ) );
        }
        else
        {
            GameManager::GetInstance()->TouchUpdate( off0, Vectorf( 0, 0 ) );
        }
    }
    else
    {
        GameManager::GetInstance()->TouchUpdate( off0, off1 );
    }

    int ln = Shop::GetInstance()->CheckOwnership( Shop::Items::HealthKit );
    int rn = Shop::GetInstance()->CheckOwnership( Shop::Items::Grenade );
    int mn = Shop::GetInstance()->CheckOwnership( Shop::Items::Mine );
    int sn = Shop::GetInstance()->CheckOwnership( Shop::Items::Shield );

    if( ln != m_leftNum )
    {
        char tmp[16];
        sprintf( tmp, "%ix", ln );
        m_healthText.Reset( new Claw::ScreenText( m_font, Claw::String( tmp ) ) );
        m_leftNum = ln;
    }
    if( rn != m_rightNum )
    {
        char tmp[16];
        sprintf( tmp, "x%i", rn );
        m_grenadeText.Reset( new Claw::ScreenText( m_font, Claw::String( tmp ) ) );
        m_rightNum = rn;
    }
    if( mn != m_mineNum )
    {
        char tmp[16];
        sprintf( tmp, "x%i", mn );
        m_mineText.Reset( new Claw::ScreenText( m_font, Claw::String( tmp ) ) );
        m_mineNum = mn;
    }
    if( sn != m_shieldNum )
    {
        char tmp[16];
        sprintf( tmp, "%ix", sn );
        m_shieldText.Reset( new Claw::ScreenText( m_font, Claw::String( tmp ) ) );
        m_shieldNum = sn;
    }
}

void TouchControls::Reset()
{
    for( int i = 0; i < 2; ++i )
    {
        m_data[i].button = InactiveTouch;
        m_data[i].pos.x = 0;
        m_data[i].pos.y = 0;
    }
}

void TouchControls::OnTouchUp( int x, int y, int button )
{
    if( button >= 0 && button < 10 )
    {
        m_touchHandled[button] = false;
    }

    if( m_slideButton == button )
    {
        m_slideButton = InactiveTouch;
        if( m_slidePos > SlideMax - SlideMargin )
        {
            AudioManager::GetInstance()->Play( SFX_WEAPON_SWIPE );
            GameManager::GetInstance()->GetLua()->Call( "NextBoughtWeapon", 0, 0 );
            TutorialManager::GetInstance()->OnWeaponChange();
        }
        return;
    }
    for( int i=0; i<2; i++ )
    {
        if( m_data[i].button == button )
        {
            m_data[i].button = InactiveTouch;
            m_data[i].pos.x = 0;
            m_data[i].pos.y = 0;
            return;
        }
    }
}

void TouchControls::OnTouchDown( int x, int y, int button )
{
    Claw::TouchDeviceId devId = Claw::TouchDevice::GetId( button );
    if( devId == Claw::TDT_TOUCHPAD_XPERIA_PLAY )
        OnXperiaTouchDown( x, y, button );
    else
        OnDisplayTouchDown( x, y, button );
}

void TouchControls::OnTouchMove( int x, int y, int button )
{
    Claw::TouchDeviceId devId = Claw::TouchDevice::GetId( button );
    if( devId == Claw::TDT_TOUCHPAD_XPERIA_PLAY )
        OnXperiaTouchMove( x, y, button );
    else
        OnDisplayTouchMove( x, y, button );
}

void TouchControls::AddWeapon( const Claw::SurfacePtr& weapon )
{
    CLAW_ASSERT( m_weapons.size() < 2 );
    m_weapons.push_back( weapon );
}

void TouchControls::WeaponSwitch()
{
    CLAW_ASSERT( m_weapons.size() == 2 );
    std::swap( m_weapons[0], m_weapons[1] );
}

void TouchControls::OnDisplayTouchDown( int x, int y, int button )
{
    Vectorf res( ((MonstazApp*)MonstazApp::GetInstance())->GetResolution() );
    Vectorf touch( x, y );

    if( m_data[0].button == InactiveTouch )
    {
        if( x < res.x * 0.5f - 20 * m_scale )
        {
            if( Shop::GetInstance()->CheckOwnership( Shop::Items::HealthKit ) > 0 &&
                GameManager::GetInstance()->GetPlayer()->GetHitPoints() < GameManager::GetInstance()->GetStats()->GetMaxPlayerHP() &&
                m_data[0].visible &&
                ( GetMedkitPosition() + Vectorf( m_health[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
            {
                if( m_leftBtnTime <= 0 )
                {
                    m_leftBtnTime = 2.25f;
                    m_data[0].time = 1;
                    ResetDeadTimer( 0.75f );
                    GameManager::GetInstance()->UseHealthKit();
                    TouchHandled( button );
                }
            }
            else if( Shop::GetInstance()->CheckOwnership( Shop::Items::Shield ) > 0 &&
                m_data[0].visible &&
                ( GetShieldPosition() + Vectorf( m_shield[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
            {
                if( m_shieldBtnTime <= 0 )
                {
                    m_shieldBtnTime = 0.5f;
                    m_data[0].time = 1;
                    ResetDeadTimer( 0.75f );
                    GameManager::GetInstance()->EnableShield();
                    TouchHandled( button );
                }
            }
            else if( m_fixed && Vectorf( touch - m_data[0].center ).LengthSqr() < TouchActiveSquare )
            {
                m_data[0].button = button;

                Vectorf e( touch - m_data[0].center );
                float l = DotProduct( e, e );
                if( l > TouchSquare )
                {
                    l = TouchRadius / sqrt( l );
                    e *= l;
                }
                m_data[0].target = e;
                m_data[0].time = 1;
                TouchHandled( button );
            }
            else if( !m_fixed )
            {
                m_data[0].button = button;

                const Vectori& res = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
                int cx = Claw::MinMax<int>( x, TouchOffset, ( res.x ) - TouchOffset );
                int cy = Claw::MinMax<int>( y, LimitY, ( res.y ) - TouchOffset );
                m_data[0].center = Vectorf( cx, cy );
                m_data[0].visible = true;
                m_data[0].time = 1;

                Vectorf d( Vectorf( x, y ) - m_data[0].center );
                float len = DotProduct( d, d );
                if( len > TouchSquare )
                {
                    len = TouchRadius / sqrt( len );
                    d *= len;
                }

                m_data[0].pos = m_data[0].target = d;
                TouchHandled( button );
            }

            return;
        }
    }
    else if( Shop::GetInstance()->CheckOwnership( Shop::Items::HealthKit ) > 0 &&
        GameManager::GetInstance()->GetPlayer()->GetHitPoints() < GameManager::GetInstance()->GetStats()->GetMaxPlayerHP() &&
        ( GetMedkitPosition() + Vectorf( m_health[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
    {
        if( m_leftBtnTime <= 0 )
        {
            m_leftBtnTime = 2.25f;
            GameManager::GetInstance()->UseHealthKit();
            TouchHandled( button );
        }
        return;
    }
    else if( Shop::GetInstance()->CheckOwnership( Shop::Items::Shield ) > 0 &&
        ( GetShieldPosition() + Vectorf( m_shield[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
    {
        if( m_shieldBtnTime <= 0 )
        {
            m_shieldBtnTime = 0.5f;
            GameManager::GetInstance()->EnableShield();
            TouchHandled( button );
        }
        return;
    }
    if( m_data[1].button == InactiveTouch )
    {
        if( x > res.x * 0.5f + 20 * m_scale )
        {
            if( Shop::GetInstance()->CheckOwnership( Shop::Items::Grenade ) > 0 &&
                m_data[1].visible &&
                ( GetGrenadePosition() + Vectorf( m_grenade[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
            {
                if( m_rightBtnTime <= 0 )
                {
                    m_rightBtnTime = 0.5f;
                    m_data[1].time = 1;
                    ResetDeadTimer( 0.75f );
                    GameManager::GetInstance()->FireGrenade();
                    TouchHandled( button );
                }
            }
            else if( Shop::GetInstance()->CheckOwnership( Shop::Items::Mine ) > 0 &&
                m_data[1].visible &&
                ( GetMinePosition() + Vectorf( m_mine[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
            {
                if( m_mineBtnTime <= 0 )
                {
                    m_mineBtnTime = 0.5f;
                    m_data[1].time = 1;
                    ResetDeadTimer( 0.75f );
                    GameManager::GetInstance()->PlaceMine();
                    TouchHandled( button );
                }
            }
            else if( m_slideButton == InactiveTouch &&
                m_weaponsNum > 1 &&
                m_data[1].visible &&
                ( GetSwapPosition() + Vectorf( 22, 15 ) - touch ).LengthSqr() < TouchSlideSquare &&
                TutorialManager::GetInstance()->IsWeaponChangeAllowed() )
            {
                m_slideButton = button;
                TouchHandled( button );
            }
            else if( m_fixed && Vectorf( touch - m_data[1].center ).LengthSqr() < TouchActiveSquare )
            {
                m_data[1].button = button;

                Vectorf e( Vectorf( x, y ) - m_data[1].center );
                float l = DotProduct( e, e );
                if( l > TouchSquare )
                {
                    l = TouchRadius / sqrt( l );
                    e *= l;
                }
                m_data[1].target = e;
                m_data[1].time = 1;
                TouchHandled( button );
            }
            else if( !m_fixed )
            {
                m_data[1].button = button;

                const Vectori& res = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
                int cx = Claw::MinMax<int>( x, TouchOffset, ( res.x ) - TouchOffset );
                int cy = Claw::MinMax<int>( y, LimitY, ( res.y ) - TouchOffset );
                m_data[1].center = Vectorf( cx, cy );

                m_data[1].visible = true;
                m_data[1].time = 1;

                Vectorf d( Vectorf( x, y ) - m_data[1].center );
                float len = DotProduct( d, d );

                if( len > TouchSquare )
                {
                    len = TouchRadius / sqrt( len );
                    d *= len;
                }

                m_data[1].pos = m_data[1].target = d;
                TouchHandled( button );
            }
            return;
        }
    }
    else if( m_data[1].visible &&
        ( GetGrenadePosition() + Vectorf( m_grenade[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
    {
        if(  m_rightBtnTime <= 0 ) 
        {
            m_rightBtnTime = 0.5f;
            GameManager::GetInstance()->FireGrenade();
            TouchHandled( button );
        }
        return;
    }
    else if( m_data[1].visible &&
        ( GetMinePosition() + Vectorf( m_mine[0]->GetSize() / 2 ) - touch ).LengthSqr() < TouchSquare )
    {
        if( m_mineBtnTime <= 0 )
        {
            m_mineBtnTime = 0.5f;
            GameManager::GetInstance()->PlaceMine();
            TouchHandled( button );
        }
        return;
    }
    else if( m_slideButton == InactiveTouch &&
        m_weaponsNum > 1 &&
        m_data[1].visible &&
        ( GetSwapPosition() + Vectorf( 22, 15 ) - touch ).LengthSqr() < TouchSlideSquare )
    {
        m_slideButton = button;
        TouchHandled( button );
    }
}

void TouchControls::OnDisplayTouchMove( int x, int y, int button )
{
    if( m_slideButton == button )
    {
        m_slidePos = ( x - GetSwapPosition().x + 22 ) / m_scale;
        m_slidePos = Claw::MinMax<float>( m_slidePos, SlideMin, SlideMax );
        return;
    }
    for( int i=0; i<2; i++ )
    {
        if( m_data[i].button == button )
        {
            Vectorf d( Vectorf( x, y ) - m_data[i].center );
            float len = DotProduct( d, d );

#ifdef CLAW_ANDROID
            // If finger moves out of active area consider it as touch up
            // (on some devices touch screen messes touch ids) - this helps
            // prevent tocuh controlls locking inssue.
            if( len > TouchActiveSquare )
            {
                OnTouchUp( x, y, button );
                return;
            }
#endif

            if( len > TouchSquare )
            {
                len = TouchRadius / sqrt( len );
                d *= len;
            }

#ifndef CLAW_ANDROID
            m_data[i].target = d;
#else
            m_data[i].pos = d;
#endif

            return;
        }
    }

    if( button >= 0 && button < 10 && !m_touchHandled[button] )
    {
        OnTouchDown( x, y, button );
    }
}

void TouchControls::OnXperiaTouchDown( int x, int y, int button )
{
    const Vectori& touchpadSize = ((MonstazApp*)MonstazApp::GetInstance())->GetXperiaPlayTouchpadSize();
    Vectorf touch( x, touchpadSize.y - y );

    if( m_data[0].button == InactiveTouch )
    {
        if( x < touchpadSize.y )
        {
            m_data[0].button = button;

            m_data[0].xperia_center = Vectorf( touchpadSize.y * 0.5f, touchpadSize.y * 0.5f );
            const float radius =  m_data[0].xperia_center.y * 0.75;

            Vectorf e( touch - m_data[0].xperia_center );
            e = e / radius * TouchRadius;

            float l = DotProduct( e, e );
            if( l > TouchSquare )
            {
                l = TouchRadius / sqrt( l );
                e *= l;
            }
            m_data[0].target = e;
            m_data[0].time = 1;
            return;
        }
    }
    if( m_data[1].button == InactiveTouch )
    {
        if( x > (touchpadSize.x - touchpadSize. y) )
        {
            m_data[1].button = button;

            m_data[1].xperia_center = Vectorf( touchpadSize.x - touchpadSize.y * 0.5f, touchpadSize.y * 0.5f );
            const float radius =  m_data[1].xperia_center.y * 0.75;

            Vectorf e( touch - m_data[1].xperia_center );
            e = e / radius * TouchRadius;

            float l = DotProduct( e, e );
            if( l > TouchSquare )
            {
                l = TouchRadius / sqrt( l );
                e *= l;
            }
            m_data[1].target = e;
            m_data[1].time = 1;
            return;
        }
    }
}

void TouchControls::OnXperiaTouchMove( int x, int y, int button )
{
    const Vectori& touchpadSize = ((MonstazApp*)MonstazApp::GetInstance())->GetXperiaPlayTouchpadSize();
    Vectorf touch( x, touchpadSize.y - y );

    for( int i=0; i<2; i++ )
    {
        if( m_data[i].button == button )
        {
            const float radius =  m_data[i].xperia_center.y * 0.75;
            Vectorf d( touch - m_data[i].xperia_center );
            d = d / radius * TouchRadius;

            float len = DotProduct( d, d );

            if( len > TouchSquare )
            {
                len = TouchRadius / sqrt( len );
                d *= len;
            }

            m_data[i].pos = m_data[i].target = d;
            return;
        }
    }

    if( button != -1 )
    {
        OnTouchDown( x, y, button );
    }
}

void TouchControls::ModeSwitched()
{
    if( m_fixed )
    {
        Vectorf res( ((MonstazApp*)MonstazApp::GetInstance())->GetResolution() );

        m_data[0].visible = true;
        m_data[1].visible = true;

        m_data[0].time = 1;
        m_data[1].time = 1;

        m_data[0].center = Vectorf( TouchOffset, res.y - TouchOffset );
        m_data[1].center = Vectorf( res.x - TouchOffset, res.y - TouchOffset );
    }
    else
    {
        m_data[0].visible = false;
        m_data[1].visible = false;
    }
}

void TouchControls::FixedVPadSwitch()
{
    Claw::Registry::Get()->Get( "/monstaz/settings/fixedvpad", m_fixed );
    ModeSwitched();
}

Vectorf TouchControls::GetMedkitPosition() const
{
    return m_data[0].center + Vectorf( 40, -3 ) * m_scale;
}

Vectorf TouchControls::GetShieldPosition() const
{
    return m_data[0].center + Vectorf( 36, -60 ) * m_scale;
}

Vectorf TouchControls::GetMinePosition() const
{
    return m_data[1].center + Vectorf( -84, -60 ) * m_scale;
}

Vectorf TouchControls::GetGrenadePosition() const
{
    return m_data[1].center + Vectorf( -87, -3 ) * m_scale;
}

Vectorf TouchControls::GetSwapPosition() const
{
    return m_data[1].center + Vectorf( -40, -85 ) * m_scale;
}

Vectori TouchControls::GetSwapSize() const
{
    const int idx = m_switchBtnTime > 0 ? 1 : 0;
    return m_switch[idx]->GetSize();
}

void TouchControls::ShowFinger( bool show )
{
    if ( m_fingerVisible != show )
    {
        m_fingerVisible = show; m_fingerTimer = 0;
    }
}

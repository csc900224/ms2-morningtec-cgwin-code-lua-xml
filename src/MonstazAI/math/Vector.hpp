#ifndef __MONSTAZ_VECTOR_HPP__
#define __MONSTAZ_VECTOR_HPP__

#include "claw/math/Vector.hpp"

typedef Claw::Vectorf Vectorf;
typedef Claw::Vectori Vectori;

#endif

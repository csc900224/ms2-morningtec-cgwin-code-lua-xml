#include "claw/math/Math.hpp"

#include "MonstazAI/math/LuaMath.hpp"
#include "MonstazAI/math/Vector.hpp"

void RegisterMath( Claw::Lua* lua )
{
    lua->RegisterFunction( "Rotate", LuaRotate );
    lua->RegisterFunction( "Normalize", LuaNormalize );
    lua->RegisterFunction( "GetLength", LuaGetLength );
    lua->RegisterFunction( "DotProduct", LuaDotProduct );
    lua->RegisterFunction( "CrossProduct", LuaCrossProduct );
}

int LuaRotate( lua_State* L )
{
    Claw::Lua lua( L );

    float x = lua.CheckNumber( 1 );
    float y = lua.CheckNumber( 2 );
    float a = lua.CheckNumber( 3 );
    float c = cos( a );
    float s = sin( a );

    lua.PushNumber( x * c - y * s );
    lua.PushNumber( x * s + y * c );

    return 2;
}

int LuaNormalize( lua_State* L )
{
    Claw::Lua lua( L );

    Vectorf v( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) );
    float len = v.Normalize();

    lua.PushNumber( v.x );
    lua.PushNumber( v.y );
    lua.PushNumber( len );

    return 3;
}

int LuaGetLength( lua_State* L )
{
    Claw::Lua lua( L );

    Vectorf v( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) );
    lua.PushNumber( v.Length() );

    return 1;
}

int LuaDotProduct( lua_State* L )
{
    Claw::Lua lua( L );

    Vectorf v1( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) );
    Vectorf v2( lua.CheckNumber( 3 ), lua.CheckNumber( 4 ) );

    lua.PushNumber( DotProduct( v1, v2 ) );

    return 1;
}

int LuaCrossProduct( lua_State* L )
{
    Claw::Lua lua( L );

    Vectorf v1( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) );
    Vectorf v2( lua.CheckNumber( 3 ), lua.CheckNumber( 4 ) );

    lua.PushNumber( CrossProduct( v1, v2 ) );

    return 1;
}

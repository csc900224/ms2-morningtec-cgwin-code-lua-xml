#ifndef __MONSTAZ_LUAMATH_HPP__
#define __MONSTAZ_LUAMATH_HPP__

#include "claw/base/Lua.hpp"

void RegisterMath( Claw::Lua* lua );

int LuaRotate( lua_State* L );
int LuaNormalize( lua_State* L );
int LuaGetLength( lua_State* L );
int LuaDotProduct( lua_State* L );
int LuaCrossProduct( lua_State* L );

#endif

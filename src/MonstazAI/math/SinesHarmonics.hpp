#ifndef __MATH_SINES_HARMONICS_HPP__
#define __MATH_SINES_HARMONICS_HPP__

#include "claw/base/Errors.hpp"

class SinesHarmonics
{
public:
    void SetPhaseOffset( float phase )  { m_phaseOff = phase; }
    void SetAmplitude( float amp )      { m_amplitude = amp; }
    void SetPeriod( float period )      { m_angVel = 2 * M_PI / period; }

    float GetValue( float phase ) const { return m_amplitude == 0 ? 0 : m_amplitude * sinf( m_angVel * phase + m_phaseOff ); }

private:
    float m_phaseOff;
    float m_amplitude;
    float m_angVel;
};

#endif

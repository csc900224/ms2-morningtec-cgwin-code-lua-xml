#include "claw/application/DebugOverlay.hpp"
#include "claw/application/Time.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/base/TextDict.hpp"
#include "claw/compat/Platform.h"
#include "claw/graphics/Batcher.hpp"
#include "claw/network/Network.hpp"
#include "claw/math/Math.hpp"
#include "claw/system/Alloc.hpp"
#include "claw/system/AllocPool.hpp"
#include "claw/system/Memory.hpp"
#include "claw/vfs/Vfs.hpp"
#include "claw/sound/mixer/EffectWideStereo.hpp"
#include "claw/math/Matrix.hpp"
#include "guif/Control.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/job/InitJob.hpp"
#include "MonstazAI/job/UnsupportedJob.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/VibraController.hpp"
#include "MonstazAI/GameCenterManager.hpp"
#include "MonstazAI/AudioSession.hpp"
#include "MonstazAI/ReminderPopup.hpp"
#include "MonstazAI/Achievements.hpp"
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/network/facebook/Facebook.hpp"
#include "MonstazAI/network/googleservices/GoogleServices.hpp"
#include "MonstazAI/network/testflight/TestFlightService.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/AtlasManager.hpp"
#include "MonstazAI/ConnectionMonitor.hpp"

#ifdef CLAW_WIN32
#  include <Windows.h>
#endif

#ifndef _DEBUG
#define PUBLIC_BUILD
#endif

//#define FILTER_BICUBIC

namespace MonstazAI
{
    static const int   BASE_HEIGHT          = 290;
    static const int   BASE_LOWSPACE_OFSSET = 20;

    static const float MULTIPLIER_HD        = 4.0f;
    static const float MULTIPLIER_FULLHD    = 3.0f;
    static const float MULTIPLIER_HIGH      = 2.0f;
    static const float MULTIPLIER_MEDIUM    = 1.5f;
    static const float MULTIPLIER_NORMAL    = 1.0f;

    const char* MonstazAIApplication::ENCRYPTION_KEY    = "0";
    const int   MonstazAIApplication::BACKUP_SAVE_NUM   = 3;

    static const Vectorf NORMAL_SPLASH   ( 1280, 800 );
    static const Vectorf MEDIUM_SPLASH   ( 1280, 800 );
    static const Vectorf HIGH_SPLASH     ( 1280, 800 );
    static const Vectorf HD_SPLASH       ( 1280, 800 );

    static const char* FACEBOOK_APP_ID = "0";
    static const char* FACEBOOK_URL_SCHEME_SUFFIX = "0";

    static int nextMap = 0;

    MonstazAIApplication::MonstazAIApplication()
        : Claw::Application( Claw::AF_DISPLAY_32BPP | Claw::AF_ENABLE_EXTENDED_TOUCH )
        , m_mallocCount( 0 )
        , m_freeCount( 0 )
        , m_job( NULL )
        , m_shop( NULL )
        , m_cashTimer( new CashTimer() )
        , m_time( new char[32] )
        , m_backgroundMusicCheckTimer( 0 )
        , m_lowVerticalSpace( false )
        , m_tutorialManager( new TutorialManager() )
        , m_facebook( NULL )
        , m_googleServices( NULL )
        , m_twitter( new TwitterService() )
        , m_sms( new SmsService() )
        , m_email( new EmailService() )
        , m_scaleX( 1 )
        , m_scaleY( 1 )
    {
#ifndef CLAW_IPHONE
        Claw::AllocPool::AddPool( 8, 4 * 1024 );
        Claw::AllocPool::AddPool( 12, 24 * 1024 );
        Claw::AllocPool::AddPool( 16, 16 * 1024 );
        Claw::AllocPool::AddPool( 32, 3 * 1024 );
        Claw::AllocPool::AddPool( 64, 32 * 1024 );
        Claw::AllocPool::AddPool( 96, 8 * 1024 );
        Claw::AllocPool::AddPool( 128, 1024 );
        Claw::AllocPool::AddPool( 192, 256 );
        Claw::AllocPool::AddPool( 256, 32 );
        Claw::AllocPool::AddPool( 384, 72 );
        Claw::AllocPool::AddPool( 512, 28 );
#endif

#ifdef CLAW_WIN32
        Claw::UInt64 freq;
        QueryPerformanceFrequency( (LARGE_INTEGER*)&freq );
        m_freq = 1.0 / freq;
#endif

        EnableFlag( Claw::AF_IPHONE_ENABLE_RETINA_DISPLAY );
        EnableFlag( Claw::AF_IPAD_ENABLE_RETINA_DISPLAY );
        EnableFlag( Claw::AF_ENABLE_EXTENDED_TOUCH );
#ifndef AMAZON_BUILD
        EnableFlag( Claw::AF_ANDROID_HIDE_STATUS_BAR );
#endif
        //EnableFlag( Claw::AF_SDL_HANDLE_FOCUS_CHANGE );

        // Allow iPod music playback
        AudioSession::SetCategory( AudioSession::C_AMBIENT );
        AudioSession::RegisterIPodPlaybackStateChangedEvent( MonstazAIApplication::BackgroundMusicStateCallback );

        // Register myself as server-sync observer
        ClawExt::ServerSync::GetInstance()->RegisterObserver( this );
    }

    MonstazAIApplication::~MonstazAIApplication()
    {
        // Save progress
        if( m_job )
        {
            m_job.Release();
            Save();
        }

        UserDataManager::Release();

        // Release GameCenter
        GameCenterManager::Release();

        ReminderPopup::Release();

        // Release server synchronization
        ClawExt::ServerSync::Release();

        // Release Network monitor
        ClawExt::NetworkMonitor::Release();

        delete[] m_time;
    }

    static const char* vert =
        "void main(void)\n"
        "{\n"
        "TEXTURE;\n"
        "GLPOSITION;\n"
        "}";

    static const char* frag =
#ifdef CLAW_GLES
"precision mediump float;"
"precision mediump int;"
#endif
"uniform mediump vec2 ts;"
"float func2( float x )"
"{"
"   float f = x * 0.75;"
"   if( f > -1.5 && f < -0.5 )"
"   {"
"       f = f + 1.5;"
"       return( 0.5 * f*f );"
"   }"
"   else if( f > -0.5 && f < 0.5 )"
"   {"
"       return 3.0 / 4.0 - ( f * f );"
"   }"
"   else if( ( f > 0.5 && f < 1.5 ) )"
"   {"
"       f = f - 1.5;"
"       return( 0.5 * f*f );"
"   }"
"   return 0.0;"
"}"
"vec3 BiCubic( sampler2D textureSampler, vec2 TexCoord, vec2 texsize )"
"{"
"    float texelSizeX = 1.0 / texsize.x;"
"    float texelSizeY = 1.0 / texsize.y;"
"    vec3 nSum = vec3( 0.0 );"
"    float nDenom = 0.0;"
"    float a = fract( TexCoord.x * texsize.x );"
"    float b = fract( TexCoord.y * texsize.y );"
"    for( int m = -1; m <=2; m++ )"
"    {"
"        for( int n =-1; n<= 2; n++)"
"        {"
"            vec3 vecData = texture2D( textureSampler,"
"                               TexCoord + vec2( texelSizeX * float( m ), texelSizeY * float( n ) ) ).rgb;"
"            float cm = func2( float( m ) - a ) * func2 ( b - float( n ) );"
"            nSum = nSum + ( vecData * cm );"
"            nDenom = nDenom + cm;"
"        }"
"    }"
"    return nSum / nDenom;"
"}"
"void main(void)\n"
"{\n"
"gl_FragColor = vec4( BiCubic( clawTex, vTex, ts ), 1.0 );"
"}";

    void MonstazAIApplication::CheckScreenMode()
    {
        CLAW_MSG( ">> Screen Resolution: " << m_resolution.x << "x" << m_resolution.y );

        m_screenMode = SM_NORMAL;
        m_lowVerticalSpace = false;

        if( m_resolution.y >= BASE_HEIGHT * MULTIPLIER_MEDIUM )
        {
            float ty = 320;
            Vectori newres( ty / m_resolution.y * m_resolution.x, ty );
            CLAW_MSG( "Draw buffer: " << newres.x << "x" << newres.y );
            m_scaleX = newres.x / (float)m_resolution.x;
            m_scaleY = newres.y / (float)m_resolution.y;
            m_drawBuffer.Reset( new Claw::Surface( newres.x, newres.y, Claw::PF_RGB_565 ) );
#ifndef FILTER_BICUBIC
            m_drawBuffer->SetFlag( Claw::Surface::SF_LINEAR_FILTERING, true );
#endif
            m_resolution = newres;
            m_bicubic.Load( vert, frag );
        }
        else if( m_resolution.y < BASE_HEIGHT * MULTIPLIER_NORMAL )
        {
            m_screenMode = SM_UNSUPPORTED;
        }
    }

    void MonstazAIApplication::OnStartup()
    {
#if defined NOOK_BUILD
        PreventDimming( false );
#endif
        Claw::Network::Open();

        GameEventDispatcher::CreateInstance();
        GameCenterManager::GetInstance();
        MonetizationManager::CreateInstance();
        ReminderPopup::Initialize();
        AnalyticsManager::CreateInstance();
        Missions::MissionManager::CreateInstance();
        ConnectionMonitor::CreateInstance();

        m_shop.Reset( new Shop() );

        m_resolution.x = GetDisplay()->GetWidth();
        m_resolution.y = GetDisplay()->GetHeight();
        
        if( m_resolution.x < m_resolution.y ) 
            std::swap( m_resolution.x, m_resolution.y );

        CheckScreenMode();

        int budget = 16 * 1024 * 1024;
        if( GetDeviceId() == Claw::DEVID_APPLE_IPAD1G )
        {
            budget /= 2;
        }
        budget *= GetGameScale() * GetGameScale();
#ifdef CLAW_SDL
        budget *= 4;
#endif
        m_atlasManager.Reset( new AtlasManager( budget ) );
        m_pakManager.Reset( new PakManager() );

        Claw::Vfs::Initialize();
#if !defined CLAW_SDL
        Claw::MountPak( "data.pak", "", Claw::MF_TRANSPARENT | Claw::MF_DISABLE_MMAP );
#else
        Claw::MountNative( "./", "" );

        if( m_screenMode == SM_HD )
        {
            Claw::MountNative( "../res-hd/", "", Claw::MF_TRANSPARENT );
#ifdef BUILD_JP
            Claw::MountNative( "../override-japan/res-hd/", "", Claw::MF_TRANSPARENT );
#endif
        }
        else if( m_screenMode == SM_FULLHD )
        {
            Claw::MountNative( "../res-fullhd/", "", Claw::MF_TRANSPARENT );
#ifdef BUILD_JP
            Claw::MountNative( "../override-japan/res-fullhd/", "", Claw::MF_TRANSPARENT );
#endif
        }
        else if( m_screenMode == SM_HIGH )
        {
            Claw::MountNative( "../res-retina/", "", Claw::MF_TRANSPARENT );
#ifdef BUILD_JP
            Claw::MountNative( "../override-japan/res-retina/", "", Claw::MF_TRANSPARENT );
#endif
        }
        else if( m_screenMode == SM_MEDIUM )
        {
            Claw::MountNative( "../res-android/", "", Claw::MF_TRANSPARENT );
#ifdef BUILD_JP
            Claw::MountNative( "../override-japan/res-android/", "", Claw::MF_TRANSPARENT );
#endif
        }
        else
        {
            Claw::MountNative( "../res-normal/", "", Claw::MF_TRANSPARENT );
#ifdef BUILD_JP
            Claw::MountNative( "../override-japan/res-normal/", "", Claw::MF_TRANSPARENT );
#endif
        }
#endif

        Claw::MountNative( "save/", "save/", Claw::MF_TRANSPARENT | Claw::MF_SAVEGAME );
        Claw::MountNative( "assetcache/", "assetcache/", Claw::MF_SAVEGAME | Claw::MF_EXTERNAL_STORAGE | Claw::MF_TRANSPARENT );

        Claw::DebugOverlay::Create();

#ifndef PUBLIC_BUILD
        Claw::DebugOverlay::Get()->EnableNetworkAccess();
        Claw::DebugOverlay::Get()->EnableFpsCounter( true );
#endif

        Claw::DebugOverlay::Get()->AddFunction( "lua", LuaCallbackEntry, this, "execute lua command" );
        Claw::DebugOverlay::Get()->AddFunction( "luastack", LuaStackEntry, this, "dump lua stack" );
        Claw::DebugOverlay::Get()->AddFunction( "obstacles", SwitchObstacleRendering, this, "switch obstacle rendering" );
        Claw::DebugOverlay::Get()->AddFunction( "dump", DumpMenuState, this, "dump menu state" );
        Claw::DebugOverlay::Get()->AddFunction( "map", SwitchMap, this, "load map" );
        Claw::DebugOverlay::Get()->AddFunction( "addcash", AddCash, this, "add cash [amount]" );
        Claw::DebugOverlay::Get()->AddFunction( "addgold", AddGold, this, "add gold [amount]" );
        Claw::DebugOverlay::Get()->AddFunction( "zerocash", ZeroCash, this, "reset cash" );
        Claw::DebugOverlay::Get()->AddFunction( "win", Win, this, "win level" );
        Claw::DebugOverlay::Get()->AddFunction( "addpoints", AddPoints, this, "add poinst [amount]" );
        Claw::DebugOverlay::Get()->AddFunction( "worldmap", ShowWorldMap, this, "show world map [index]" );
        Claw::DebugOverlay::Get()->AddFunction( "skiptutorial", SkipTutorial, this, "skip tutorial" );
        Claw::DebugOverlay::Get()->AddFunction( "addbackup", AddBackup, this, "addbackup" );
        Claw::DebugOverlay::Get()->AddFunction( "preloadatlas", PreloadAtlas, this, "preload atlas" );

#ifdef _DEBUG
        //Claw::DebugOverlay::Get()->EnablePoolStats( true );
#endif

        CreateTextDict();
        CreateAssetDict();
        CreateRegistry();

        CreateMixer( Claw::AudioFormat( 2, 44100 ), Claw::MixerParams( 4096 ) );
        Claw::Mixer::Get()->AddEffect( new Claw::EffectWideStereo( Claw::Mixer::Get()->GetFormat(), 0.25f, 0.09f ) );

        Claw::DeviceId did = GetDeviceId();
        Claw::Registry::Get()->Set( "/internal/vibra",
            did == Claw::DEVID_APPLE_IPHONE2G ||
            did == Claw::DEVID_APPLE_IPHONE3G ||
            did == Claw::DEVID_APPLE_IPHONE3GS ||
            did == Claw::DEVID_APPLE_IPHONE4 ||
            did == Claw::DEVID_APPLE_IPHONE4S );

        if( m_screenMode == SM_NORMAL )
        {
            Claw::Registry::Get()->Set( "/guif/textsnap", true );
        }

        DetectXpreriaPlayKeyboard();

        m_audioManager.Reset( new AudioManager() );
        m_vibraController.Reset( new VibraController() );

        // update available
        Claw::Registry* registry = Claw::Registry::Get();
        registry->Set( "/internal/updatechecked", false );
        registry->Set( "/internal/amazon", false );
        registry->Set( "/internal/metaflow", false );
#if defined AMAZON_BUILD
        registry->Set( "/internal/amazon", true );
#endif
#if defined METAFLOW_BUILD
        registry->Set( "/internal/metaflow", true );
#endif

        if( m_screenMode == SM_UNSUPPORTED )
            m_job.Reset( new UnsupportedJob() );
        else
            m_job.Reset( new InitJob() );

        m_job->Initialize();
        m_audioManager->MuteMusic( AudioSession::IsOtherAudioPlaying() );

#ifdef ADHOC_BUILD
        TestFlightService::TakeOff( "0" );
#endif

        m_cashTimer->Initialize();

        m_facebook = Network::Facebook::QueryInterface( FACEBOOK_APP_ID, FACEBOOK_URL_SCHEME_SUFFIX );
        m_facebook->Authenticate( false );

        m_googleServices = Network::GoogleServices::QueryInterface();
        m_googleServices->Authenticate( false );
    }

    void MonstazAIApplication::OnShutdown()
    {
        AnalyticsManager::Release();

        Missions::MissionManager::Release();
        m_shop.Release();
        m_atlasManager.Release();
        m_pakManager.Release();
        MonetizationManager::Release();
        ConnectionMonitor::Release();

        Network::Facebook::Release( m_facebook );
        m_facebook = NULL;

        GameEventDispatcher::Release();
    }

    void MonstazAIApplication::OnUpdate( float _dt )
    {
        const float tick = 1/60.f;
        if( fabs( _dt - tick ) < 1/360.f )
        {
            _dt = tick;
        }

        if ( m_backgroundMusicCheckTimer > 0 )
        {
            m_backgroundMusicCheckTimer -= _dt;
            if ( m_backgroundMusicCheckTimer <= 0 )
            {
                m_audioManager->MuteMusic( AudioSession::IsOtherAudioPlaying() );
            }
        }

        if( m_nextJob )
        {
            // Try to sync server data periodically
            // We can check e.g. every job change.
            ClawExt::ServerSync::GetInstance()->SyncData();

            CLAW_ASSERT( m_job->GetReferenceCount() == 1 );
            m_job.Reset( m_nextJob );
            m_nextJob.Release();

            // Perform initialization after old job was destroyed 
            // to prevent race conditions.
            m_job->Initialize();
        }

        if( !m_scheduledMapSwitch.empty() )
        {
            ((GameplayJob*)m_job.GetPtr())->LoadLevel( m_scheduledMapSwitch.c_str() );
            m_scheduledMapSwitch.clear();
        }

        float dt = std::min( 0.1f, _dt );
        m_cashTimer->Update( dt );

        double start = GetTimeMs();

        while( !m_luaQueue.empty() )
        {
            GameManager::GetInstance()->GetLua()->Execute( m_luaQueue.front() );
            m_luaQueue.pop();
        }

        m_job->Update( dt );

        if( m_refill )
        {
            m_refill->Tick();
        }

        Missions::MissionManager::GetInstance()->Update( dt );
        MonetizationManager::GetInstance()->Update( dt );
        ConnectionMonitor::GetInstance()->Update( dt );

        double end = GetTimeMs();
        sprintf( m_time, "%c %f ms", 168, float( end - start ) );

        if ( nextMap > 0 )
        {
            Claw::Lua* lua = ((MainMenuJob*)(m_job.GetPtr()))->GetLua();
            lua->PushNumber( nextMap );
            lua->Call( "MapShowView", 1, 0 );

            nextMap = 0;
        }
    }

    void MonstazAIApplication::OnRender( Claw::Surface* target )
    {
        target->Clear( 0 );

        if( m_drawBuffer )
        {
            m_drawBuffer->Clear( 0 );
            m_job->Render( m_drawBuffer );
#ifdef FILTER_BICUBIC
            Claw::g_batcher->SetShader( &m_bicubic );
            m_bicubic.Uniform( "ts", Claw::AlignPOT( m_resolution.x ), Claw::AlignPOT( m_resolution.y ) );
#endif
            target->BlitScale( m_drawBuffer );
            Claw::g_batcher->SetShader( NULL );
        }
        else
        {
            m_job->Render( target );
        }

        char buf[64];

#ifdef _DEBUG
        Claw::DebugOverlay::Get()->Draw( target, 350, 0, m_time );
        Claw::UInt64 mallocCount = Claw::GetMallocCount();
        Claw::UInt64 freeCount = Claw::GetFreeCount();
        sprintf( buf, "m: %i", mallocCount - m_mallocCount );
        Claw::DebugOverlay::Get()->Draw( target, 0, 8, buf );
        sprintf( buf, "f: %i", freeCount - m_freeCount );
        Claw::DebugOverlay::Get()->Draw( target, 0, 16, buf );
        m_mallocCount = mallocCount;
        m_freeCount = freeCount;
#endif

#ifndef PUBLIC_BUILD
        static const char* ok = "";
        static const char overbudget[] = { 1, (char)255, 96, 96, 0 };
        int size = m_atlasManager->GetSize();
        int budget = m_atlasManager->GetBudget();
        sprintf( buf, "%sbudget: %i/%iMB", size <= budget ? ok : overbudget, size / (1024*1024), budget / (1024*1024) );
        Claw::DebugOverlay::Get()->Draw( target, 450, 0, buf );
#endif
    }

    void MonstazAIApplication::OnKeyPress( Claw::KeyCode code )
    {
#ifdef CLAW_ANDROID
        if( code == Claw::KEY_BACK )
        {
            code = Claw::KEY_ESCAPE;
        }
#elif defined CLAW_WIN32
        if( code == Claw::KEY_N )
        {
            Achievements::GetInstance()->Show( "test achievements system", 0 );
        }
#ifdef XPERIA_PLAY_BUILD
        else if( code == Claw::KEY_X )
        {
            m_xperiaPlayTouchpadAvailable = !m_xperiaPlayTouchpadAvailable;
            if( m_job )
            {
                m_job->OnTouchDeviceChange();
            }
        }
#endif
#endif
        m_job->KeyPress( code );
    }

    void MonstazAIApplication::OnText( const char* text )
    {
        m_job->OnText( text );
    }

    void MonstazAIApplication::OnTextNewLine()
    {
        m_job->OnTextNewLine();
    }

    void MonstazAIApplication::OnKeyRelease( Claw::KeyCode code )
    {
        m_job->KeyRelease( code );
    }

    void MonstazAIApplication::OnTouchDown( int x, int y, int button )
    {
        m_job->TouchDown( x * m_scaleX, y * m_scaleY, button );
    }

    void MonstazAIApplication::OnTouchUp( int x, int y, int button )
    {
        m_job->TouchUp( x * m_scaleX, y * m_scaleY, button );
    }

    void MonstazAIApplication::OnTouchMove( int x, int y, int button )
    {
        if( ( button & Claw::TD_DEVICE_MASK ) == Claw::TDT_CURSOR ) button = -1;

        m_job->TouchMove( x * m_scaleX, y * m_scaleY, button );
    }

    void MonstazAIApplication::OnTouchDeviceChange()
    {
        DetectXpreriaPlayKeyboard();
        if( m_job )
        {
            m_job->OnTouchDeviceChange();
        }
    }

    void MonstazAIApplication::DetectXpreriaPlayKeyboard()
    {
        m_xperiaPlayTouchpadAvailable = false;
        Claw::TouchDevice** dev = GetTouchDevices();
        while( dev && *dev )
        {
            if( Claw::TDT_TOUCHPAD_XPERIA_PLAY == (*dev)->m_type )
            {
                m_xperiaPlayTouchpadAvailable = true;
                m_xperiaPlayTouchSize.x = (*dev)->m_width;
                m_xperiaPlayTouchSize.y = (*dev)->m_height;
                break;
            }
            ++dev;
        }
    }

    void MonstazAIApplication::OnResize( int w, int h )
    {
        if( !m_drawBuffer )
        {
            m_resolution.x = w;
            m_resolution.y = h;
        }
        if( m_job )
        {
            m_job->Resize( w, h );
        }
    }

    void MonstazAIApplication::OnFocusChange( bool focus )
    {
        MonetizationManager::GetInstance()->OnFocusChange( focus );
        AnalyticsManager::GetInstance()->OnFocusChange( focus );

        if( !m_nextJob && m_job )
        {
            m_job->Focus( focus );
        }

        if( focus && m_cashTimer )
        {
            m_cashTimer->Initialize();
        }

        if( focus && m_audioManager )
        {
            m_audioManager->MuteMusic( AudioSession::IsOtherAudioPlaying() );
        }
    }

    void MonstazAIApplication::SwitchJob( Job* job )
    {
        m_nextJob.Reset( job );
    }

    void MonstazAIApplication::SaveLoaded()
    {
    }

    void MonstazAIApplication::Save( bool forced )
    {
        if ( !forced && TutorialManager::GetInstance()->IsActive() )
        {
            return;
        }

        bool loaded = false;
        Claw::Registry::Get()->Get( "/internal/dataloaded", loaded );

        if( loaded )
        {
            CLAW_MSG( "Saving progres... " );

#if defined CLAW_SDL
            bool success = Claw::Registry::Get()->Save( "save/config.xml", "/monstaz" );
            success &= Claw::Registry::Get()->Save( "save/maps.xml", "/maps" );
            success &= Claw::Registry::Get()->Save( "save/levels.xml", "/levels" );
            success &= Claw::Registry::Get()->Save( "save/missions.xml", "/missions" );
#else
           bool success = SaveWithBackup( "config", "/monstaz" );
           success &= SaveWithBackup( "maps",  "/maps" );
           success &= SaveWithBackup( "levels",  "/levels" );
           success &= SaveWithBackup( "missions",  "/missions" );
#endif

#ifdef CLAW_SDL
            if( !success ) CLAW_MSG( "Save failed!!!" );
#else
            CLAW_MSG_ASSERT( success, "Save failed!!!" );
#endif
            if( success ) CLAW_MSG( "Save succeed." );
        }
    }

    bool MonstazAIApplication::SaveWithBackup( const char* fileName, const char* branchName )
    {
        if( m_saveIdx.find( fileName ) == m_saveIdx.end() )
        {
            m_saveIdx[fileName] = 0;
        }

        // Generate file name
        Claw::StdOStringStream filename;
        filename << "save/" << fileName << "_" << m_saveIdx[fileName] << ".xml";

        // Add timestamp 
        Claw::NarrowString timestamp = branchName;
        timestamp += "/save-timestamp";
        Claw::Registry::Get()->Set( timestamp.c_str(), int(Claw::Time::GetTime()) );

        // Save to file
        bool success = Claw::Registry::Get()->SaveEncrypted( filename.m_str.c_str(), ENCRYPTION_KEY, branchName, true );
        m_saveIdx[fileName] = (m_saveIdx[fileName] + 1) % BACKUP_SAVE_NUM;
        return success;
    }

    const Vectori& MonstazAIApplication::GetResolution() const
    {
        return m_resolution;
    }

    void MonstazAIApplication::LuaCallback( const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn )
    {
        m_luaQueue.push( args );
    }

    void MonstazAIApplication::LuaDumpStack( Claw::DebugOverlay::Connection* conn )
    {
        if( conn )
        {
            Claw::DebugOverlay::Get()->AddLine( "Lua stack dump not reliable on remote debug sessions due to race condition.", conn );
        }
        else
        {
            GameManager::GetInstance()->GetLua()->DumpStack();
        }
    }

    MonstazAIApplication::ScreenMode MonstazAIApplication::GetScreenMode()
    {
        return ((MonstazApp*)MonstazApp::GetInstance())->m_screenMode;
    }

    void MonstazAIApplication::PushScreenModes( Claw::Lua* lua )
    {
        MonstazApp* app = ((MonstazApp*)MonstazApp::GetInstance());

        lua->PushNumber( app->GetGameScale() );
        lua->RegisterGlobal( "GAME_SCALE" );

        lua->PushNumber( app->m_resolution.x );
        lua->RegisterGlobal( "SCREEN_WIDTH" );

        lua->PushNumber( app->m_resolution.y );
        lua->RegisterGlobal( "SCREEN_HEIGHT" );

        lua->PushBool( app->m_lowVerticalSpace );
        lua->RegisterGlobal( "LOW_VERTICAL_SPACE" );

        lua->PushNumber( GetBgImgSize().x );
        lua->RegisterGlobal( "BG_WIDTH" );

        lua->PushNumber( GetBgImgSize().y );
        lua->RegisterGlobal( "BG_HEIGHT" );

#ifdef ANDROID_BUILD
        lua->PushBool( true );
#else
        lua->PushBool( false );
#endif
        lua->RegisterGlobal( "IS_ANDROID" );

#ifdef CLAW_IPHONE
        lua->PushBool( true );
#else
        lua->PushBool( false );
#endif
        lua->RegisterGlobal( "IS_IPHONE" );

#ifdef AMAZON_BUILD
        lua->PushBool( true );
#else
        lua->PushBool( false );
#endif
        lua->RegisterGlobal( "IS_AMAZON" );

#ifdef _DEBUG
        lua->PushBool( true );
#else
        lua->PushBool( false );
#endif
        lua->RegisterGlobal( "IS_DEBUG" );

#ifdef IAPS_DISABLED
        lua->PushBool( false );
#else
        lua->PushBool( true );
#endif
        lua->RegisterGlobal( "IAPS_ENABLED" );

#ifdef SUBSCRIPTION_DISABLED
        lua->PushBool( false );
#else
        lua->PushBool( true );
#endif
        lua->RegisterGlobal( "SUBSCRIPTION_ENABLED" );
    }

    double MonstazAIApplication::GetTimeMs()
    {
#ifdef CLAW_WIN32
        Claw::UInt64 t;
        QueryPerformanceCounter( (LARGE_INTEGER*)&t );
        return t * m_freq * 1000;
#else
        return Claw::Time::GetTimeMs();
#endif
    }

    const char* MonstazAIApplication::GetFacebookAppId()
    {
        return FACEBOOK_APP_ID;
    }

    void MonstazAIApplication::BackgroundMusicStateCallback()
    {
        ((MonstazAIApplication*)Claw::Application::GetInstance())->m_backgroundMusicCheckTimer = 0.25f;
    }

    void _GetBV( Guif::Control* control, Claw::Rect& bv )
    {
        if( control )
        {
            bv = bv.Extend( control->GetBoundingBoxAbsolute() );
            for( Guif::Node<Guif::Control>::child_iterator it = control->GetNode()->child(); it; ++it )
            {
                _GetBV( &*it, bv );
            }
        }
    }

    int MonstazAIApplication::GetControlSize( Guif::Screen* screen, lua_State* L )
    {
        Claw::Lua lua( L );
        const char* name = lua.CheckCString( 1 );

        Guif::Control* control = NULL;
        
        if( screen )
        {
            control = screen->FindControl( name );
        }

        Claw::Rect bb;
        if( control )
        {
            bb = control->GetBoundingBoxAbsolute();
            _GetBV( control, bb );
        }

        lua.PushNumber( bb.m_w );
        lua.PushNumber( bb.m_h );
        return 2;
    }

    float MonstazAIApplication::GetGameScale() const
    {
        switch( m_screenMode )
        {
        case SM_HD:
            return MULTIPLIER_HD;
        case SM_FULLHD:
            return MULTIPLIER_FULLHD;
        case SM_HIGH:
            return MULTIPLIER_HIGH;
        case SM_MEDIUM:
            return MULTIPLIER_MEDIUM;
        default:
            return MULTIPLIER_NORMAL;
        }
    }

    Vectorf MonstazAIApplication::GetBgImgSize()
    {
        switch( GetScreenMode() )
        {
        case SM_HD:
            return HD_SPLASH;
        case SM_FULLHD:
            return HD_SPLASH;
        case SM_HIGH:
            return HIGH_SPLASH;
        case SM_MEDIUM:
            return MEDIUM_SPLASH;
        default:
            return NORMAL_SPLASH;
        }
    }

    void MonstazAIApplication::OnSynchronisationStart()
    {
        CLAW_MSG( "MonstazAIApplication::OnSynchronisationStart()" );
    }

    void MonstazAIApplication::OnSynchronisationEnd( bool success )
    {
        CLAW_MSG( "MonstazAIApplication::OnSynchronisationEnd(): " << success );
    }

    void MonstazAIApplication::OnSynchronisationSkip()
    {
        CLAW_MSG( "MonstazAIApplication::OnSynchronisationSkip()" );
    }

    void MonstazAIApplication::OnTaskFinished( const ClawExt::ServerSync::TaskId& taskId, bool success )
    {
        CLAW_MSG( "MonstazAIApplication::OnTaskFinished(): " << taskId << " : " << success );
    }

    void MonstazAIApplication::OnGroupChanged( const ClawExt::ServerSync::GroupName& newGroupName, bool initial )
    {
        CLAW_MSG( "MonstazAIApplication::OnGroupChanged: " << newGroupName << " initial: " << initial );
    }

    void MonstazAIApplication::ShowWorldMap( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn )
    {
        nextMap = atoi( args.c_str() );
    }

    void MonstazAIApplication::PreloadAtlas( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn )
    {
        AtlasSet::Type* set = new AtlasSet::Type[AtlasSet::NUMBER_OF_SETS + 1];
        for( int i=0; i<AtlasSet::NUMBER_OF_SETS; i++ )
        {
            set[i] = (AtlasSet::Type)i;
        }
        set[AtlasSet::NUMBER_OF_SETS] = AtlasSet::Terminator;
        AtlasManager::GetInstance()->Request( set );
        delete[] set;
    }

    void MonstazAIApplication::StartRefill()
    {
        m_refill.Reset( new FuelRefill() );
    }
}

CLAW_DEFINE_APPLICATION( new MonstazAI::MonstazAIApplication, "Monster Shooter 2" );

#include "MonstazAI/ConnectionMonitor.hpp"
#include "claw/application/Application.hpp"

static const unsigned int RECHECK_INTERVAL = 15;

LUA_DECLARATION( ConnectionMonitor )
{
    METHOD( ConnectionMonitor, NoNetworkRetry ),
    METHOD( ConnectionMonitor, NoNetworkClose ),
    METHOD( ConnectionMonitor, NoNetworkContinue ),
    {0,0}
};

ConnectionMonitor::ConnectionMonitor()
    : m_enabled( false )
    , m_lua( NULL )
    , m_hasConnection( true )
    , m_luaCall( NULL )
{}

ConnectionMonitor::~ConnectionMonitor()
{
    if( m_enabled )
    {
        SetEnabled( false );
    }
}

void ConnectionMonitor::SetLua( Claw::Lua* lua ) 
{ 
    Claw::Lunar<ConnectionMonitor>::Register( *lua );
    Claw::Lunar<ConnectionMonitor>::push( *lua, this );
    lua->RegisterGlobal( "NoNetworkCallback" );
    m_lua = lua;
}

void ConnectionMonitor::Update( float dt )
{
    if( m_enabled )
    {
        ClawExt::NetworkMonitor::GetInstance()->Update( dt );
        if( m_luaCall )
        {
            m_lua->Call( m_luaCall, 0, 0 );
            m_luaCall = NULL;
        }
    }
}

void ConnectionMonitor::SetEnabled( bool enabled )
{
    bool networkRequired = false;
    Claw::Registry::Get()->Get( "/app-config/interstitials/network-required", networkRequired );
    if( !networkRequired && enabled )
    {
        return;
    }

    if( m_enabled != enabled )
    {
        if( enabled )
        {
            ClawExt::NetworkMonitor::GetInstance()->RegisterObserver( this );
            ClawExt::NetworkMonitor::GetInstance()->SetConnectionCheckInterval( RECHECK_INTERVAL );
            ClawExt::NetworkMonitor::GetInstance()->ConnectionCheck();
        }
        else
        {
            ClawExt::NetworkMonitor::GetInstance()->UnregisterObserver( this );
        }
        m_enabled = enabled;
    }
}

int ConnectionMonitor::l_NoNetworkRetry( lua_State* L )
{
    ClawExt::NetworkMonitor::GetInstance()->ConnectionCheck();
    return 0;
}

int ConnectionMonitor::l_NoNetworkClose( lua_State* L )
{
    Claw::Application::GetInstance()->Exit();
    return 0;
}

int ConnectionMonitor::l_NoNetworkContinue( lua_State* L )
{
    return 0;
}

void ConnectionMonitor::OnNetworkCheckResult( ClawExt::NetworkMonitor::NetworkStatus status )
{
    if( m_lua == NULL || !m_enabled ) return;
    int tutorial = 0;
    Claw::Registry::Get()->Get( "/monstaz/tutorial/chapter", tutorial );
    if( tutorial != 0 ) return;

    m_hasConnection = status == ClawExt::NetworkMonitor::NS_CONNECTED;

    if( m_hasConnection )
    {
        m_luaCall = "NoNetworkPopupConnected";
    }
    else
    {
        m_luaCall = "NoNetworkPopupShowWithLock";
    }
}
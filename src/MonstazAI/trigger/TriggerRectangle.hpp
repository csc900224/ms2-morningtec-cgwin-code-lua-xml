#ifndef __MONSTAZ_TRIGGERRECTANGLE_HPP__
#define __MONSTAZ_TRIGGERRECTANGLE_HPP__

#include "MonstazAI/trigger/Trigger.hpp"
#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/ObjectRectangle.hpp"

class TriggerRectangle : public Trigger, public ObjectRectangle
{
public:
    LUA_DEFINITION( TriggerRectangle );
    TriggerRectangle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    TriggerRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular );
};

#endif

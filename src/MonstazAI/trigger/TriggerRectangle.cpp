#include "claw/math/Math.hpp"

#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/trigger/TriggerRectangle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

LUA_DECLARATION( TriggerRectangle )
{
    METHOD( TriggerRectangle, GetPos ),
    METHOD( TriggerRectangle, GetType ),
    METHOD( TriggerRectangle, GetEdge ),
    METHOD( TriggerRectangle, GetPerpendicular ),
    {0,0}
};

void TriggerRectangle::Init( Claw::Lua* lua )
{
    Claw::Lunar<TriggerRectangle>::Register( *lua );
}

TriggerRectangle::TriggerRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular )
    : Trigger( id, x, y, Trigger::Rectangle )
    , ObjectRectangle( edge, perpendicular )
{
    Vectorf center( x, y );
    center += m_edge * 0.5f;
    center += m_perp * 0.5f;
    Vectorf extents( m_edgeLen, m_perpLen );
    extents *= 0.5f;
    static_cast<Scene::OBB2*>(m_bv)->SetCenterExtentsAxes( center, extents, m_edge );
}

#ifndef __MONSTAZ_TRIGGER_HPP__
#define __MONSTAZ_TRIGGER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"

namespace Scene
{
    class BoundingArea;
};

class Entity;

class Trigger
{
public:
    enum Type
    {
        Circle,
        Rectangle,
        Custom
    };

    LUA_DEFINITION( Trigger );
    Trigger( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    virtual ~Trigger();

    const Vectorf& GetPos() const { return m_pos; }
    Type GetType() const { return m_type; }

    bool Execute( Claw::Lua* lua, Entity* e );

    Scene::BoundingArea* GetBA() const { return m_bv; }

    int l_GetPos( lua_State* L );
    int l_GetType( lua_State* L );

protected:
    Trigger() { CLAW_ASSERT( false ); }
    Trigger( const Claw::NarrowString& id, float x, float y, Type type );

    Vectorf m_pos;
    Type m_type;
    Claw::NarrowString m_id;

    Scene::BoundingArea* m_bv;
};

#endif

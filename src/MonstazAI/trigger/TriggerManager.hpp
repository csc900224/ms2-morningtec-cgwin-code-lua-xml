#ifndef __MONSTAZ_TRIGGERMANAGER_HPP__
#define __MONSTAZ_TRIGGERMANAGER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/trigger/Trigger.hpp"
#include "MonstazAI/trigger/TriggerCircle.hpp"
#include "MonstazAI/trigger/TriggerRectangle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"

#include <vector>


class TriggerManager : public Claw::RefCounter
{
public:
    LUA_DEFINITION( TriggerManager );
    TriggerManager( lua_State* L ) { CLAW_ASSERT( false ); }

    TriggerManager( Claw::Lua* lua );
    ~TriggerManager();

    TriggerCircle* AddTriggerCircle( const Claw::NarrowString& id, float x, float y, float r );
    TriggerRectangle* AddTriggerRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular );

    const std::vector<Trigger*>& GetTriggers() const { return m_ents; }

    void CheckTriggers();

    int l_Add( lua_State* L );

private:
    std::vector<Trigger*> m_ents;
    Claw::LuaPtr m_lua;
    Scene::Circle m_collisionBv;
};

typedef Claw::SmartPtr<TriggerManager> TriggerManagerPtr;

#endif

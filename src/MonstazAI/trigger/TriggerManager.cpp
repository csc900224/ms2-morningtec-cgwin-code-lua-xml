#include "MonstazAI/trigger/TriggerManager.hpp"
#include "MonstazAI/GameManager.hpp"


LUA_DECLARATION( TriggerManager )
{
    METHOD( TriggerManager, Add ),
    {0,0}
};

TriggerManager::TriggerManager( Claw::Lua* lua )
    : m_lua( lua )
    , m_collisionBv( Entity::AVERAGE_RADIUS )
{
    Trigger::Init( lua );

    Claw::Lunar<TriggerManager>::Register( *lua );
    Claw::Lunar<TriggerManager>::push( *lua, this );
    lua->RegisterGlobal( "TriggerManager" );

    lua->CreateEnumTable();
    lua->AddEnum( Trigger::Circle );
    lua->AddEnum( Trigger::Rectangle );
    lua->AddEnum( Trigger::Custom );
    lua->RegisterEnumTable( "TriggerType" );
}

TriggerManager::~TriggerManager()
{
    for( std::vector<Trigger*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        delete *it;
    }
}

TriggerCircle* TriggerManager::AddTriggerCircle( const Claw::NarrowString& id, float x, float y, float r )
{
    TriggerCircle* e = new TriggerCircle( id, x, y, r );
    m_ents.push_back( e );
    return e;
}

TriggerRectangle* TriggerManager::AddTriggerRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular )
{
    TriggerRectangle* e = new TriggerRectangle( id, x, y, edge, perpendicular );
    m_ents.push_back( e );
    return e;
}

void TriggerManager::CheckTriggers()
{
    if( m_ents.empty() ) return;

    const std::vector<Entity*>& e = GameManager::GetInstance()->GetEntityManager()->GetEntities();

    for( std::vector<Entity*>::const_iterator eit = e.begin(); eit != e.end(); ++eit )
    {
        m_collisionBv.SetTranslation( (*eit)->GetPos() );

        std::vector<Trigger*>::iterator it = m_ents.begin();
        while( it != m_ents.end() )
        {
            if( (*it)->GetBA()->Intersect( &m_collisionBv ) && !(*it)->Execute( m_lua, *eit ) )
            {
                it = m_ents.erase( it );
            }
            else
            {
                ++it;
            }
        }
    }
}

int TriggerManager::l_Add( lua_State* L )
{
    Claw::Lua lua( L );

    Trigger::Type type = lua.CheckEnum<Trigger::Type>( 1 );

    if( type == Trigger::Circle )
    {
        TriggerCircle* e = AddTriggerCircle( lua.CheckString( 2 ), lua.CheckNumber( 3 ), lua.CheckNumber( 4 ), lua.CheckNumber( 5 ) );
        Claw::Lunar<TriggerCircle>::push( L, e );
    }
    else if( type == Trigger::Rectangle )
    {
        TriggerRectangle* e = AddTriggerRectangle( lua.CheckString( 2 ), lua.CheckNumber( 3 ), lua.CheckNumber( 4 ),
            Vectorf( lua.CheckNumber( 5 ), lua.CheckNumber( 6 ) ),
            lua.CheckNumber( 7 ) );
        Claw::Lunar<TriggerRectangle>::push( L, e );
    }
    else
    {
        CLAW_ASSERT( false );
    }

    return 1;
}

#include "claw/math/Math.hpp"

#include "MonstazAI/trigger/TriggerCircle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"

LUA_DECLARATION( TriggerCircle )
{
    METHOD( TriggerCircle, GetPos ),
    METHOD( TriggerCircle, GetType ),
    METHOD( TriggerCircle, GetRadius ),
    {0,0}
};

void TriggerCircle::Init( Claw::Lua* lua )
{
    Claw::Lunar<TriggerCircle>::Register( *lua );
}

TriggerCircle::TriggerCircle( const Claw::NarrowString& id, float x, float y, float r )
    : Trigger( id, x, y, Trigger::Circle )
    , ObjectCircle( r )
{
    static_cast<Scene::Circle*>(m_bv)->SetCenterRadius( Vectorf( x, y ), r );
}

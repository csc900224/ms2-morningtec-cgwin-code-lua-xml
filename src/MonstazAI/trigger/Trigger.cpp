#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/trigger/Trigger.hpp"
#include "MonstazAI/trigger/TriggerCircle.hpp"
#include "MonstazAI/trigger/TriggerRectangle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

LUA_DECLARATION( Trigger )
{
    METHOD( Trigger, GetPos ),
    METHOD( Trigger, GetType ),
    {0,0}
};

void Trigger::Init( Claw::Lua* lua )
{
    Claw::Lunar<Trigger>::Register( *lua );

    TriggerCircle::Init( lua );
    TriggerRectangle::Init( lua );
}

Trigger::Trigger( const Claw::NarrowString& id, float x, float y, Type type )
    : m_pos( x, y )
    , m_type( type )
    , m_id( id )
{
    // Create trigger bounding area
    if( m_type == Circle )
        m_bv = new Scene::Circle( m_pos, 0 );
    else
        m_bv = new Scene::OBB2( m_pos );
}

Trigger::~Trigger()
{
    delete m_bv;
}

bool Trigger::Execute( Claw::Lua* lua, Entity* e )
{
    Claw::Lunar<Entity>::push( *lua, e );
    if( !lua->Call( m_id, 1, 1 ) )
    {
        CLAW_CONSOLE_ASSERT( false, Claw::NarrowString( "No trigger named " ) + m_id );
    }
    bool res = lua->CheckBool( -1 );
    lua->Pop( 1 );
    return res;
}

int Trigger::l_GetPos( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_pos.x );
    lua.PushNumber( m_pos.y );
    return 2;
}

int Trigger::l_GetType( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_type );
    return 1;
}

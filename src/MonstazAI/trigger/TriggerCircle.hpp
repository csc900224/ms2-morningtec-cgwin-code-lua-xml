#ifndef __MONSTAZ_TRIGGERCIRCLE_HPP__
#define __MONSTAZ_TRIGGERCIRCLE_HPP__

#include "MonstazAI/ObjectCircle.hpp"
#include "MonstazAI/trigger/Trigger.hpp"

class TriggerCircle : public Trigger, public ObjectCircle
{
public:
    LUA_DEFINITION( TriggerCircle );
    TriggerCircle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    TriggerCircle( const Claw::NarrowString& id, float x, float y, float r );
};

#endif

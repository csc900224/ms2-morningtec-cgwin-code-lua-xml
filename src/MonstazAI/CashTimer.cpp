#include "claw/base/Registry.hpp"
#include "claw/network/NtpRequest.hpp"

#include "MonstazAI/CashTimer.hpp"

CashTimer::CashTimer()
    : m_timeCorrect( false )
    , m_thread( NULL )
    , m_subsecond( 0 )
    , m_epoch( 0 )
    , m_connectionError( false )
{}

CashTimer::~CashTimer()
{
    if( m_ntp )
    {
        m_ntp->Abort();
    }
    delete m_thread;
}

void CashTimer::Initialize()
{
    if( m_thread )
    {
        if( m_ntp )
        {
            m_ntp->Abort();
        }
        delete m_thread;
        m_ntp.Release();
    }
    m_connectionError = false;
    m_timeCorrect = false;
    m_epoch = 0;
    m_subsecond = 0;
    m_ntp.Reset( new Claw::NtpRequest() );
    m_thread = new Claw::Thread( NetworkEntry, this );
}

void CashTimer::Update( float dt )
{
    if( !m_timeCorrect ) return;
    if( m_thread ) { delete m_thread; m_thread = NULL; }

    m_subsecond += dt;
    while( m_subsecond > 1 )
    {
        m_subsecond -= 1;
        m_epoch++;
    }
}

int CashTimer::Network()
{
    m_ntp->Connect();
    if( !m_ntp->CheckError() )
    {
        m_epoch = m_ntp->GetEpoch();
        m_timeCorrect = true;
    }
    else
    {
        m_connectionError = true;
    }
    return 0;
}

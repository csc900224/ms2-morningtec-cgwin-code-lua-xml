#ifndef __INCLUDED__CASHTIMER_HPP__
#define __INCLUDED__CASHTIMER_HPP__

#include "claw/base/SmartPtr.hpp"
#include "claw/base/Thread.hpp"
#include "claw/network/NtpRequest.hpp"

class CashTimer : public Claw::RefCounter
{
public:
    CashTimer();
    ~CashTimer();
    void Initialize();
    void Update( float dt );

    bool IsCorrect() const { return m_timeCorrect; }
    bool IsConnectionError() const { return m_connectionError; }
    Claw::UInt32 Epoch() const { return m_timeCorrect ? m_epoch : 0; }

private:
    static int NetworkEntry( void* ptr ) { return ((CashTimer*)ptr)->Network(); }
    int Network();

    bool m_connectionError;
    bool m_timeCorrect;
    Claw::UInt32 m_epoch;
    float m_subsecond;

    Claw::Thread* m_thread;
    Claw::NtpRequestPtr m_ntp;
};

typedef Claw::SmartPtr<CashTimer> CashTimerPtr;

#endif

#ifndef __MONSTAZ_SHOP_HPP__
#define __MONSTAZ_SHOP_HPP__

#include <map>
#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"

#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/adcolony/AdColony.hpp"
#include "claw_ext/monetization/playhaven/Playhaven.hpp"

// Forward declarations
class InAppProductsDb;

class Shop 
    : public Claw::RefCounter
    , public ClawExt::InAppStore::TransactionsObserver
    , public ClawExt::InAppStore::SubscriptionObserver
    , public ClawExt::AdColony::AdColonyObserver
    , public ClawExt::Playhaven::PlayhavenObserver
{
public:
    typedef Claw::NarrowString ItemId;

    struct Items
    {
        static const ItemId Smg;
        static const ItemId Nuke;
        static const ItemId Grenade;
        static const ItemId HealthKit;
        static const ItemId Shield;
        static const ItemId Mine;
        static const ItemId Airstrike;
        static const ItemId FreeIap;
        static const ItemId MechFlamer;
        static const ItemId MechRocket;
    };

    enum ItemCategory
    {
        IC_WEAPON = 1,
        IC_PERK,
        IC_ITEM,
        IC_CASH
    };

    LUA_DEFINITION( Shop );
    Shop( lua_State* L ) { CLAW_ASSERT( false ); }
    Shop();
    ~Shop();

    void Init( Claw::Lua* lua );

    int CheckOwnership( const ItemId& item );
    int GetUpgrades( const ItemId& item );
    bool CanUpgradeItem( const ItemId& item );
    bool CanUpgradeAnyItem();
    bool Buy( const ItemId& item, bool unlock, bool save );
    bool BuyVirtualCash( const ItemId& productId );
    bool BuySubscription( const ItemId& productId );
    void CheckSubscriptions();
    bool IsSubscriptionActive();
    bool Upgrade( const ItemId& item, bool save );
    bool Use( const ItemId& item, bool saveChange = false );
    void UpdateCash( int cashDelta, int goldDelta );
    void SetCash( int cash, int gold );
    void GetCash( int& cash, int& gold ) const;
    void FreeGold();

    ItemCategory GetItemCategory( const ItemId& itemId ) const;
    void GetItemPrice( const ItemId& itemId, int& cash, int& gold ) const;
    void GetItemUpgradePrice( const ItemId& itemId, int& cash, int& gold ) const;
    int GetItemUnlockPrice( const ItemId& itemId ) const;
    int GetCashItemBonus( const ItemId& itemId ) const;
    float GetRealItemPriceInUSD( const ItemId& itemid ) const;
    float GetTapjoyConversionRate() const;

    void OnGameSessionsStart();
    void OnGameSessionsStop();

    int l_IsBought( lua_State* L );
    int l_GetUpgrades( lua_State* L );
    int l_Buy( lua_State* L );
    int l_UnlockBuy( lua_State* L );
    int l_Upgrade( lua_State* L );
    int l_BuyVirtualCash( lua_State* L );
    int l_BuySubscription( lua_State* L );
    int l_CheckSubscription( lua_State* L );
    int l_Use( lua_State* L );
    int l_GetCash( lua_State* L );
    int l_SetCash( lua_State* L );
    int l_OpenTapjoy( lua_State* L );
    int l_OnEnter( lua_State* L );
    int l_OnExit( lua_State* L );
    int l_PopupClosed( lua_State* L );
    int l_FreeGold( lua_State* L );
    int l_Preload( lua_State* L );

    static Shop* GetInstance() { return s_instance; }
    void Focus( bool focus );
    void Update( float dt );

    // InAppStore transaction observer interface
    virtual void TransactionFailed( const ClawExt::InAppTransaction& trans );
    virtual void TransactionCancel( const ClawExt::InAppTransaction& trans );
    virtual void TransactionComplete( const ClawExt::InAppTransaction& trans );
    virtual void TransactionRestore( const ClawExt::InAppTransaction& trans );
    virtual void TransactionRefund( const ClawExt::InAppTransaction& trans );
    virtual void TransactionSupport( bool supported );

    // InAppStore subscription observer interface
    virtual void SubscriptionStatus( const Claw::NarrowString& subscriptionId, bool active );
    virtual void SubscriptionCancel( const Claw::NarrowString&  subscriptionId );
    virtual void SubscriptionBought( const Claw::NarrowString&  subscriptionId );
    virtual void SubscriptionAlreadyOwned( const Claw::NarrowString&  subscriptionId );
    virtual void SubscriptionFailed( const Claw::NarrowString&  subscriptionId );

    // AddColony observer iterface
    virtual void OnVirtualCurrencyAward( const Claw::NarrowString& zondeId, const Claw::NarrowString& currencyName, int amount );
    virtual void OnVideoBegin( const Claw::NarrowString& zondeId ) {}
    virtual void OnVideoEnd( const Claw::NarrowString& zondeId ) {}
    virtual void OnVideoError( const Claw::NarrowString& zondeId ) {}
    virtual void OnVideoAdsReady( const Claw::NarrowString& zondeId ) {}
    virtual void OnVideoAdsNotReady( const Claw::NarrowString& zondeId ) {}

    // Playhaven observer interface
    virtual void OnMakePurchase( const Claw::NarrowString& iapId );
    virtual void OnUnlockReward( const Claw::NarrowString& rewardName, int rewardQuantity );

private:
    typedef std::map<ItemId, int> ItemToIntMap;

    void UpdateIapCash( const ItemId& itemId );
    void UpdateSubscription( const ItemId& itemId, bool active );
    void UpdateCash();
    void UpdateCashEncryption();

    ItemToIntMap m_ownership;
    ItemToIntMap m_sessionOwnership;
    ItemToIntMap m_upgrades;

    bool m_shopActive;
    bool m_transactionInProgress;
    bool m_iapsSupported;
    bool m_iabInitialized;
    bool m_gameSession;

    static Shop* s_instance;

    Claw::Lua* m_lua;
};

typedef Claw::SmartPtr<Shop> ShopPtr;

#endif

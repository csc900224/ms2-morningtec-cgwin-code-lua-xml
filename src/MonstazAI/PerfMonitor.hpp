#ifndef __MONSTAZ__PERFMONITOR_HPP__
#define __MONSTAZ__PERFMONITOR_HPP__

#include "claw/base/SmartPtr.hpp"

class PerfMonitor : public Claw::RefCounter
{
public:
    PerfMonitor();
    ~PerfMonitor();

    void ReportFPS( unsigned int fps );
    void Reset();

    unsigned int GetMedian() const;

private:
    unsigned int m_buckets[61];
    unsigned int m_samples;
};

typedef Claw::SmartPtr<PerfMonitor> PerfMonitorPtr;

#endif

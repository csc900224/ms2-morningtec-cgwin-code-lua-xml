#include <algorithm>

#include "claw/base/AssetDict.hpp"

#include "MonstazAI/AtlasManager.hpp"
#include "MonstazAI/PakManager.hpp"


const char* AtlasBackground[] = { "atlas-bg.xml@linear", "atlas-bg1.xml@linear", "atlas-bg2.xml@linear", NULL };
const char* AtlasBase[] = { "atlas-base1.xml@linear", "atlas-base2.xml@linear", "atlas-base3.xml@linear", "atlas-base4.xml@linear", "atlas-base5.xml@linear", NULL };
const char* AtlasOverrides[] = { "atlas-override-japan1.xml@linear", "atlas-override-japan2.xml@linear" , NULL };
const char* AtlasBoss1[] = { "atlas-boss1a.xml@linear", "atlas-boss1b.xml@linear", "atlas-boss1d.xml@linear", NULL };
const char* AtlasBoss1Shared[] = { "atlas-boss1c.xml@linear", NULL };
const char* AtlasBoss2[] = { "atlas-boss2a.xml@linear", "atlas-boss2b.xml@linear", "atlas-boss2c.xml@linear", "atlas-boss2d.xml@linear", "atlas-boss2e.xml@linear", NULL };
const char* AtlasBoss3[] = { "atlas-boss3a.xml@linear", "atlas-boss3b.xml@linear", "atlas-boss3c.xml@linear", NULL };
const char* AtlasEnemy1[] = { "atlas-enemy1.xml@linear", "atlas-enemy1a.xml@linear", "atlas-enemy1b.xml@linear", "atlas-enemy1c.xml@linear", NULL };
const char* AtlasEnemy2[] = { "atlas-enemy2.xml@linear", "atlas-enemy2a.xml@linear", "atlas-enemy2b.xml@linear", "atlas-enemy2c.xml@linear", NULL };
const char* AtlasEnemy3[] = { "atlas-enemy3.xml@linear", "atlas-enemy3a.xml@linear", "atlas-enemy3b.xml@linear", "atlas-enemy3c.xml@linear", NULL };
const char* AtlasEnemy4[] = { "atlas-enemy4.xml@linear", "atlas-enemy4a.xml@linear", "atlas-enemy4b.xml@linear", NULL };
const char* AtlasEnemy5[] = { "atlas-enemy5.xml@linear", "atlas-enemy5a.xml@linear", "atlas-enemy5b.xml@linear", NULL };
const char* AtlasEnemy6[] = { "atlas-enemy6.xml@linear", "atlas-enemy6a.xml@linear", "atlas-enemy6b.xml@linear", NULL };
const char* AtlasEnemy7[] = { "atlas-enemy7.xml@linear", NULL };
const char* AtlasEnemy8[] = { "atlas-enemy8.xml@linear", "atlas-enemy8a.xml@linear", "atlas-enemy8b.xml@linear", NULL };
const char* AtlasMenu[] = { "atlas-menu1.xml@linear", "atlas-menu2.xml@linear", "atlas-menu3.xml@linear", "atlas-menu4.xml@linear", "atlas-menu5.xml@linear", NULL };
const char* AtlasMap01[] = { "atlas-map1.xml@linear", "atlas-map1a.xml@linear", "atlas-map1b.xml@linear", NULL };
const char* AtlasMap02[] = { "atlas-map2.xml@linear", "atlas-map2a.xml@linear", "atlas-map2b.xml@linear", "atlas-map2c.xml@linear", NULL };
const char* AtlasMap03[] = { "atlas-map3.xml@linear", "atlas-map3a.xml@linear", "atlas-map3b.xml@linear", NULL };
const char* AtlasMap04[] = { "atlas-map4.xml@linear", "atlas-map4a.xml@linear", "atlas-map4b.xml@linear", "atlas-map4c.xml@linear", NULL };
const char* AtlasMap05[] = { "atlas-map5.xml@linear", "atlas-map5a.xml@linear", "atlas-map5b.xml@linear", NULL };
const char* AtlasMap06[] = { "atlas-map6a.xml@linear", "atlas-map6b.xml@linear", "atlas-map6c.xml@linear", NULL };
const char* AtlasEnvCommon[] = { "atlas-env-common.xml@linear", NULL };
const char* AtlasEnvUrban[] = { "atlas-env-urban.xml@linear", NULL };
const char* AtlasEnvSuburbs[] = { "atlas-env-suburbs.xml@linear", NULL };
const char* AtlasEnvCity[] = { "atlas-env-city.xml@linear", "atlas-env-city2.xml@linear", "atlas-env-city3.xml@linear", NULL };
const char* AtlasEnvDesert[] = { "atlas-env-desert.xml@linear", "atlas-env-desert-tiles.xml@linear", NULL };
const char* AtlasEnvIce[] = { "atlas-env-ice.xml@linear", NULL };
const char* AtlasPlayer01[] = { "atlas-player01.xml@linear", NULL };
const char* AtlasPlayer02[] = { "atlas-player02.xml@linear", NULL };
const char* AtlasPlayer03[] = { "atlas-player03.xml@linear", NULL };
const char* AtlasPlayer04[] = { "atlas-player04.xml@linear", NULL };
const char* AtlasPlayer05[] = { "atlas-player05.xml@linear", NULL };
const char* AtlasPlayer06[] = { "atlas-player06.xml@linear", NULL };
const char* AtlasPlayer07[] = { "atlas-player07.xml@linear", NULL };
const char* AtlasPlayer08[] = { "atlas-player08.xml@linear", NULL };
const char* AtlasPlayer09[] = { "atlas-player09.xml@linear", NULL };
const char* AtlasPlayer10[] = { "atlas-player10.xml@linear", NULL };
const char* AtlasPlayer11[] = { "atlas-player11.xml@linear", NULL };
const char* AtlasPlayer12[] = { "atlas-player12.xml@linear", NULL };
const char* AtlasPlayer13[] = { "atlas-player13.xml@linear", NULL };
const char* AtlasPlayer14[] = { "atlas-player14.xml@linear", NULL };
const char* AtlasPlayer15[] = { "atlas-player15.xml@linear", NULL };
const char* AtlasPlayer16[] = { "atlas-player16.xml@linear", NULL };
const char* AtlasPlayer17[] = { "atlas-player17.xml@linear", NULL };
const char* AtlasPlayer18[] = { "atlas-player18.xml@linear", NULL };
const char* AtlasPlayerMech[] = { "atlas-playermech1.xml@linear", "atlas-playermech2.xml@linear", "atlas-playermech3.xml@linear", NULL };

struct AtlasData_t
{
    const char** atlas;
    Claw::UInt32 set;
};

AtlasData_t AtlasData[AtlasSet::NUMBER_OF_SETS] = {
    { AtlasBackground, 0 },
    { AtlasBase, 0 },
    { AtlasOverrides, 0 },
    { AtlasBoss1, PakManager::SetBoss1 },
    { AtlasBoss1Shared, 0 },
    { AtlasBoss2, PakManager::SetBoss2 },
    { AtlasBoss3, PakManager::SetBoss3 },
    { AtlasEnemy1, PakManager::SetEnemy1 },
    { AtlasEnemy2, PakManager::SetEnemy2 },
    { AtlasEnemy3, PakManager::SetEnemy3 },
    { AtlasEnemy4, PakManager::SetEnemy4 },
    { AtlasEnemy5, PakManager::SetEnemy5 },
    { AtlasEnemy6, PakManager::SetEnemy6 },
    { AtlasEnemy7, PakManager::SetEnemy7 },
    { AtlasEnemy8, PakManager::SetEnemy8 },
    { AtlasMenu, 0 },
    { AtlasMap01, 0 },
    { AtlasMap02, 0 },
    { AtlasMap03, 0 },
    { AtlasMap04, 0 },
    { AtlasMap05, 0 },
    { AtlasMap06, 0 },
    { AtlasEnvCommon, 0 },
    { AtlasEnvUrban, 0 },
    { AtlasEnvSuburbs, 0 },
    { AtlasEnvCity, 0 },
    { AtlasEnvDesert, PakManager::SetEnviroIceDesert },
    { AtlasEnvIce, PakManager::SetEnviroIceDesert },
    { AtlasPlayer01, PakManager::SetPlayerA },
    { AtlasPlayer02, PakManager::SetPlayerA },
    { AtlasPlayer03, PakManager::SetPlayerA },
    { AtlasPlayer04, PakManager::SetPlayerB },
    { AtlasPlayer05, PakManager::SetPlayerB },
    { AtlasPlayer06, PakManager::SetPlayerB },
    { AtlasPlayer07, PakManager::SetPlayerC },
    { AtlasPlayer08, PakManager::SetPlayerC },
    { AtlasPlayer09, PakManager::SetPlayerC },
    { AtlasPlayer10, PakManager::SetPlayerD },
    { AtlasPlayer11, PakManager::SetPlayerD },
    { AtlasPlayer12, PakManager::SetPlayerD },
    { AtlasPlayer13, PakManager::SetPlayerE },
    { AtlasPlayer14, 0 },
    { AtlasPlayer15, PakManager::SetPlayerE },
    { AtlasPlayer16, 0 },
    { AtlasPlayer17, PakManager::SetPlayerE },
    { AtlasPlayer18, PakManager::SetPlayerE },
    { AtlasPlayerMech, PakManager::SetPlayerMech }
};


AtlasManager* AtlasManager::s_instance = NULL;


AtlasManager::AtlasManager( int budget )
    : m_workerWait( 128 )
    , m_abort( false )
    , m_current( AtlasSet::Terminator )
    , m_size( 0 )
    , m_budget( budget )
{
    CLAW_MSG( "Atlas manager init" );
    for( int i=0; i<128; i++ ) m_workerWait.Enter();

    CLAW_ASSERT( !s_instance );
    s_instance = this;

    std::fill( m_hold, m_hold + AtlasSet::NUMBER_OF_SETS, false );
    m_thread = new Claw::Thread( ThreadEntry, this );
}

AtlasManager::~AtlasManager()
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;

    m_abort = true;
    m_workerWait.Leave();
    delete m_thread;
}

void AtlasManager::InitEnum( Claw::Lua* lua )
{
    lua->CreateEnumTable();
    lua->AddEnum( AtlasSet::Background );
    lua->AddEnum( AtlasSet::Base );
    lua->AddEnum( AtlasSet::Overrides );
    lua->AddEnum( AtlasSet::Boss1 );
    lua->AddEnum( AtlasSet::Boss1Shared );
    lua->AddEnum( AtlasSet::Boss2 );
    lua->AddEnum( AtlasSet::Boss3 );
    lua->AddEnum( AtlasSet::Enemy1 );
    lua->AddEnum( AtlasSet::Enemy2 );
    lua->AddEnum( AtlasSet::Enemy3 );
    lua->AddEnum( AtlasSet::Enemy4 );
    lua->AddEnum( AtlasSet::Enemy5 );
    lua->AddEnum( AtlasSet::Enemy6 );
    lua->AddEnum( AtlasSet::Enemy7 );
    lua->AddEnum( AtlasSet::Enemy8 );
    lua->AddEnum( AtlasSet::Menu );
    lua->AddEnum( AtlasSet::Map01 );
    lua->AddEnum( AtlasSet::Map02 );
    lua->AddEnum( AtlasSet::Map03 );
    lua->AddEnum( AtlasSet::Map04 );
    lua->AddEnum( AtlasSet::Map05 );
    lua->AddEnum( AtlasSet::Map06 );
    lua->AddEnum( AtlasSet::EnvCommon );
    lua->AddEnum( AtlasSet::EnvUrban );
    lua->AddEnum( AtlasSet::EnvSuburbs );
    lua->AddEnum( AtlasSet::EnvCity );
    lua->AddEnum( AtlasSet::EnvDesert );
    lua->AddEnum( AtlasSet::EnvIce );
    lua->AddEnum( AtlasSet::Player01 );
    lua->AddEnum( AtlasSet::Player02 );
    lua->AddEnum( AtlasSet::Player03 );
    lua->AddEnum( AtlasSet::Player04 );
    lua->AddEnum( AtlasSet::Player05 );
    lua->AddEnum( AtlasSet::Player06 );
    lua->AddEnum( AtlasSet::Player07 );
    lua->AddEnum( AtlasSet::Player08 );
    lua->AddEnum( AtlasSet::Player09 );
    lua->AddEnum( AtlasSet::Player10 );
    lua->AddEnum( AtlasSet::Player11 );
    lua->AddEnum( AtlasSet::Player12 );
    lua->AddEnum( AtlasSet::Player13 );
    lua->AddEnum( AtlasSet::Player14 );
    lua->AddEnum( AtlasSet::Player15 );
    lua->AddEnum( AtlasSet::Player16 );
    lua->AddEnum( AtlasSet::Player17 );
    lua->AddEnum( AtlasSet::Player18 );
    lua->AddEnum( AtlasSet::PlayerMech );
    lua->RegisterEnumTable( "AtlasSet" );
}

void AtlasManager::Preload( AtlasSet::Type req )
{
    PakManager::GetInstance()->Require( AtlasData[req].set );
}

void AtlasManager::Request( const AtlasSet::Type* req )
{
    Claw::LockGuard<Claw::Mutex> lock( m_queueLock );

    CLAW_MSG( "Atlas manager got request" );

    while( *req != AtlasSet::Terminator )
    {
        CLAW_MSG( "  requested set " << *req );
        if( m_current != *req &&
            std::find( m_loaded.begin(), m_loaded.end(), *req ) == m_loaded.end() &&
            std::find( m_wanted.begin(), m_wanted.end(), *req ) == m_wanted.end() )
        {
            m_wanted.push_back( *req );
            PakManager::GetInstance()->Require( AtlasData[*req].set );
            m_workerWait.Leave();
        }
        else
        {
            m_hold[*req] = true;
        }
        req++;
    }
}

bool AtlasManager::AreLoaded( const AtlasSet::Type* req )
{
    Claw::LockGuard<Claw::Mutex> lock( m_queueLock );

    bool loaded = true;
    const AtlasSet::Type* ptr = req;
    while( *ptr != AtlasSet::Terminator )
    {
        if( std::find( m_loaded.begin(), m_loaded.end(), *ptr ) == m_loaded.end() )
        {
            loaded = false;
            break;
        }
        ptr++;
    }
    if( loaded ) return true;

    int idx = 0;
    while( *req != AtlasSet::Terminator )
    {
        std::vector<AtlasSet::Type>::iterator it = std::find( m_wanted.begin(), m_wanted.end(), *req );
        if( it != m_wanted.end() )
        {
            if( m_wanted[idx] != *it )
            {
                m_wanted.erase( it );
                m_wanted.insert( m_wanted.begin()+idx, *req );
            }
            idx++;
        }
        req++;
    }

    return false;
}

bool AtlasManager::AreLoaded()
{
    Claw::LockGuard<Claw::Mutex> lock( m_queueLock );
    return m_current == AtlasSet::Terminator && m_wanted.empty();
}

void AtlasManager::Release( const AtlasSet::Type* req )
{
    Claw::LockGuard<Claw::Mutex> lock( m_queueLock );

    CLAW_MSG( "Atlas manager release request" );

    while( *req != AtlasSet::Terminator )
    {
        CLAW_MSG( "  releasing set " << *req );

        std::vector<AtlasSet::Type>::iterator it = std::find( m_wanted.begin(), m_wanted.end(), *req );
        if( it != m_wanted.end() )
        {
            m_wanted.erase( it );
        }
        m_hold[*req] = false;
        req++;
    }

    if( m_size > m_budget )
    {
        MakeSpace();
    }
}

int AtlasManager::Worker()
{
    CLAW_MSG( "Atlas manager worker thread" );
    for(;;)
    {
        m_workerWait.Enter();
        if( m_abort )
        {
            CLAW_MSG( "Atlas manager exiting worker thread" );
            return 0;
        }

        bool goback = false;

        {
            Claw::LockGuard<Claw::Mutex> lock( m_queueLock );
            // Queue might be empty due to Release(). Just eat work orders and do nothing.
            if( m_wanted.empty() ) continue;
            m_current = *m_wanted.begin();
            m_wanted.erase( m_wanted.begin() );
            if( std::find( m_loaded.begin(), m_loaded.end(), m_current ) != m_loaded.end() )
            {
                m_current = AtlasSet::Terminator;
                continue;
            }
            m_hold[m_current] = true;
        }

        CLAW_MSG( "Atlas manager loading set " << m_current );

        while( !PakManager::GetInstance()->CheckDone( AtlasData[m_current].set ) ) { Claw::Time::Sleep( 1/60.f ); }
        PakManager::GetInstance()->Reset( AtlasData[m_current].set );

        m_imgSize[m_current] = 0;
        const char** ptr = AtlasData[m_current].atlas;
        while( *ptr )
        {
            if( !AtlasExists( *ptr ) )
            {
                ++ptr;
                continue;
            }

            int size = ImageSize( *ptr );
            m_size += size;
            m_imgSize[m_current] += size;
            if( m_size > m_budget )
            {
                Claw::LockGuard<Claw::Mutex> lock( m_queueLock );
                MakeSpace();
            }
            Claw::AssetDict::AddAtlas( *ptr++, true );
            Claw::LockGuard<Claw::Mutex> lock( m_queueLock );
            if( m_abort || !m_hold[m_current] )
            {
                const char** r = AtlasData[m_current].atlas;
                while( r != ptr )
                {
                    Claw::AssetDict::RemoveAtlas( *r++ );
                }
                m_size -= m_imgSize[m_current];
                if( m_abort )
                {
                    CLAW_MSG( "Atlas manager exiting worker thread" );
                    return 0;
                }
                else
                {
                    goback = true;
                }
            }
            if( goback ) break;
        }

        Claw::LockGuard<Claw::Mutex> lock( m_queueLock );
        if( !goback )
        {
            m_loaded.push_back( m_current );
        }
        m_current = AtlasSet::Terminator;
    }
}

int AtlasManager::ImageSize( const char* atlas )
{
    Claw::NarrowString name( atlas );

    size_t at = name.rfind( '@' );
    if( at != Claw::NarrowString::npos )
    {
        name = name.substr( 0, at - 3 );
    }
    else
    {
        name = name.substr( 0, name.length() - 3 );
    }
    Claw::NarrowString image = Claw::FindOptimalImageFormat( name );
    Claw::FilePtr f( Claw::OpenFile( image ) );
    Claw::ImageLoaderPtr il( Claw::ImageLoader::Create( f ) );
    il->Initialize();
    int ret = il->GetWidth() * il->GetHeight() * Claw::GetPixelFormatInfo( il->GetPixelFormat() ).m_Bits / 8;
    il.Release();
    return ret;
}

void AtlasManager::MakeSpace()
{
    for( int i=0; i<AtlasSet::NUMBER_OF_SETS; i++ )
    {
        if( !m_hold[i] )
        {
            std::vector<AtlasSet::Type>::iterator it = std::find( m_loaded.begin(), m_loaded.end(), i );
            if( it != m_loaded.end() )
            {
                CLAW_MSG( "Atlas manager unloading set " << i );
                const char** ptr = AtlasData[i].atlas;
                while( *ptr )
                {
                    Claw::AssetDict::RemoveAtlas( *ptr++ );
                }
                m_loaded.erase( it );
                m_size -= m_imgSize[i];
            }
        }
        if( m_size <= m_budget ) break;
    }
}

bool AtlasManager::AtlasExists( const char* atlas )
{
    Claw::NarrowString name( atlas );

    size_t at = name.rfind( '@' );
    if( at != Claw::NarrowString::npos )
    {
        name = name.substr( 0, at );
    }

    Claw::FilePtr f( Claw::OpenFile( name ) );
    return f;
}

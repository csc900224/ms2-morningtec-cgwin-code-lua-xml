#ifndef __INCLUDED_MONSTAZ_STACK_SM_HPP__
#define __INCLUDED_MONSTAZ_STACK_SM_HPP__

#include "MonstazAI/State.hpp"

#include "claw/base/Errors.hpp"
#include "claw/compat/ClawTypes.hpp"

#include <map>
#include <vector>
#include <algorithm>
#include <list>

class Entity;

class StackSM
{
public:
    StackSM( StateName idleStateId );

    void Update( Entity& owner, float tick );
    void RegisterState( StateName id, State* pState );
    void ReleaseStates();

    //! Get state instance already registered
    State* GetState( StateName id );

    //! Get registered state id
    StateName GetStateId( const State* state ) const;

    //! Acquire currently active state - the one on top of stack.
    State* GetCurrentState();

    //! Get active state id - state on top of stack.
    StateName GetCurrentStateId() const;

    //! Change active state.
    void ChangeState( Entity& owner, StateName id );

private:
    State* m_register[NUMBER_OF_STATES];
    std::vector<State*> m_stack;

    StateName m_currentId;
    StateName m_idleStateId;
};

#endif

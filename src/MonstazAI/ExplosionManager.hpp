#ifndef __MONSTAZ__EXPLOSIONMANAGER_HPP__
#define __MONSTAZ__EXPLOSIONMANAGER_HPP__

#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/shot/Shot.hpp"

class Explosion
{
public:
    struct Params
    {
        Params( float radius, float power, float dradius, float dpower ) : radius( radius ), power( power ), dradius( dradius ), dpower( dpower ) {}

        float radius;
        float power;
        float dradius;
        float dpower;
    };

    LUA_DEFINITION( Explosion );
    Explosion( lua_State* L );
    Explosion( const Vectorf& pos, const Params& params, Shot::Type source );
    ~Explosion();
    static void Init( Claw::Lua* lua );

    bool Update( float dt );

    int l_GetPos( lua_State* L );
    int l_SetPos( lua_State* L );

    Vectorf m_pos;
    float m_radius;
    float m_power;
    float m_dradius;
    float m_dpower;
    Shot::Type m_source;
};

class ExplosionManager : public Claw::RefCounter
{
public:
    LUA_DEFINITION( ExplosionManager );
    ExplosionManager( lua_State* L ) { CLAW_ASSERT( false ); }

    ExplosionManager( Claw::Lua* lua );
    ~ExplosionManager();

    void Render( Claw::Surface* target, const Vectorf& offset );
    void RenderPost( Claw::Surface* target, const Vectorf& offset );
    void RenderHeat( Claw::Surface* target, const Vectorf& offset );
    void Update( float dt );

    Explosion* Add( const Vectorf& pos, const Explosion::Params& params, bool gib, Shot::Type source );
    void AddEmitter( const Vectorf& pos, float vel, float time, float particlesPerSec, bool gib = false );
    void ApplyDamages( const Explosion* e );

    std::vector<Explosion*>& GetExplosions() { return m_ents; }

    ParticleFunctor* GetGibs() { return m_gibFunctor; }

private:
    std::vector<Explosion*> m_ents;

    GfxAssetPtr m_heatExplosion;
    Claw::SurfacePtr m_glow;
    Claw::SurfacePtr m_dark;

    ParticleFunctorPtr m_explosionFunctor;
    ParticleFunctorPtr m_explosionFunctorDark;
    ParticleFunctorPtr m_gibFunctor;

    Claw::SurfacePtr m_gibs[11];
};

typedef Claw::SmartPtr<ExplosionManager> ExplosionManagerPtr;

#endif

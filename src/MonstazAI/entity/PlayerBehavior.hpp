#ifndef __INCLUDED_MONSTAZ_PLAYER_BEHAVIOR_HPP__
#define __INCLUDED_MONSTAZ_PLAYER_BEHAVIOR_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/entity/EntityBehavior.hpp"
#include "MonstazAI/math/Vector.hpp"

class PlayerBehavior : public EntityBehavior
{
protected:
    void ApplyPotential( Entity& entity, float dt, const Vectorf& vec );
};

#endif

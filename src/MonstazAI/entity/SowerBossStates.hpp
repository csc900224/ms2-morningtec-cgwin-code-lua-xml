#ifndef __INCLUDED_MONSTAZ_SOWERBOSS_STATES_HPP__
#define __INCLUDED_MONSTAZ_SOWERBOSS_STATES_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/StackSM.hpp"

namespace SowerBossStates
{
    class Move : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_cooldownTime;
    };

    class AttackState : public State
    {
    public:
        AttackState() : m_target( NULL ) {}
        Entity* m_target;
    };

    class Run : public AttackState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_time;
    };

    class Attack : public AttackState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        bool m_wasShot;
    };

    class AttackEggSpit : public AttackState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_time;
        int m_shotsNum;
    };

    class AttackTongue : public AttackState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_time;
        float m_origRadius;
    };
}

#endif

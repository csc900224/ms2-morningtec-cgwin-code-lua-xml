#ifndef __INCLUDED_MONSTAZ_SECTOID_STATES_HPP__
#define __INCLUDED_MONSTAZ_SECTOID_STATES_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/StackSM.hpp"

namespace SectoidStates
{

    class Move : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Attack : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class MoveShooting : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
    };

    class AttackShooting : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

        Entity* m_target;
        float m_time;
    };

    class Targetting : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

        Entity* m_target;
        float m_time;
    };

}

#endif

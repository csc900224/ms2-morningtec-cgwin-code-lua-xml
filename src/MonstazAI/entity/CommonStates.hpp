#ifndef __INCLUDED_MONSTAZ_COMMON_STATES_HPP__
#define __INCLUDED_MONSTAZ_COMMON_STATES_HPP__

#include "claw/compat/ClawTypes.hpp"

#include "MonstazAI/State.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/waypoint/Waypoint.hpp"

class Entity;

namespace CommonStates
{
    static const char* STATE_ID_SPAWN   = "CommonSpawn";

    class Spawn : public State
    {
    public:
        Spawn( StateName nextState );

        void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        void OnUpdate( Entity& entity, StackSM& sm, float tick );

    protected:
        float m_timer;
        StateName m_nextState;
    };

    class FollowWaypoints : public State
    {
    public:
        FollowWaypoints( Entity::Behavior behavior );

        void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        void OnUpdate( Entity& entity, StackSM& sm, float tick );

        void AddWaypoint( Waypoint* waypoint, float time = 0 );
        void SetLoop( bool loop ) { m_loop = loop; }
        Waypoint* GetWaypoint();

    private:
        Entity::Behavior m_behavior;

        struct Node
        {
            Node( Waypoint* w, float t ) : w( w ), t( t ) {}

            Waypoint* w;
            float t;
        };

        std::vector<Node> m_nodes;
        int m_idx;
        bool m_loop;
        Scene::Circle m_collisionBv;
    };

    class RunAway : public State
    {
    public:
        void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class IdleAnim : public State
    {
    public:
        void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        void OnUpdate( Entity& entity, StackSM& sm, float tick );

        void SetWait( float time ) { m_time = time; }

    private:
        float m_time;
    };

}

#endif

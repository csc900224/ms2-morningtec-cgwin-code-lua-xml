#ifndef __INCLUDED_MONSTAZ_BEHAVIOR_FUNCTORS_HPP__
#define __INCLUDED_MONSTAZ_BEHAVIOR_FUNCTORS_HPP__

#include "MonstazAI/entity/EntityBehavior.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/math/Vector.hpp"

struct AvoidFriends : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct AvoidAll : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

class AvoidLineOfSight : public EntityBehavior::PotentialFunctor
{
public:
    Vectorf Process( Entity& entity, float dt );

    static void SetLimitAngle( float angle )            { s_angleLimit = angle; }
    static void SetAvoidanceForce( float force )        { s_avoidanceForce = force; }

private:
    static float s_angleLimit;
    static float s_avoidanceForce;
};

class AvoidMapBorders : public EntityBehavior::PotentialFunctor
{
public:
    Vectorf Process( Entity& entity, float dt );

    static void SetMapSize( const Vectorf& size )       { s_mapSize = size; }
    static void SetRepulsionArea( float area )          { s_repulsionArea = area; }
    static void SetRepulsionForce( float force )        { s_repulsionForce = force; }

private:
    static Vectorf s_mapSize;
    static float s_repulsionArea;
    static float s_repulsionForce;
};

struct AvoidObstacles : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct AvoidShots : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct InterceptPlayer : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct TargetAim : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct TargetPlayer : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct TargetEnemies : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct UserControl : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct KeepDistanceToPlayer : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct GoToWaypoint : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct KeepCloseToPlayer : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

struct FireAtEnemies : public EntityBehavior::PotentialFunctor
{
    Vectorf Process( Entity& entity, float dt );
};

#endif

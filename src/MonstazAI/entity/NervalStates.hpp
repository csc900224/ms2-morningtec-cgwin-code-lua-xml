#ifndef __INCLUDED_MONSTAZ_NERVAL_STATES_HPP__
#define __INCLUDED_MONSTAZ_NERVAL_STATES_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/StackSM.hpp"
#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/particle/Emitter.hpp"
#include "MonstazAI/AudioManager.hpp"

namespace NervalStates
{

    class Move : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_timer;
    };

    class Attack : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Seek : public State
    {
    public:
        Seek();
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        void RemoveFX();

        float m_timer;
        Emitter* m_emitter;
        ParticleFunctorPtr m_particle;
        AudioManager::SoundHandle3D m_engineSound;
    };

    class Charge : public State
    {
    public:
        Charge();
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        void RemoveFX();

        Entity* m_target;
        Emitter* m_emitter;
        AudioManager::SoundHandle3D m_engineSound;
        float m_time;
    };
}

#endif

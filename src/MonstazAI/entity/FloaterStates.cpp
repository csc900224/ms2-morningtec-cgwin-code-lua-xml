#include "claw/math/Math.hpp"
#include "claw/application/Time.hpp"

#include "MonstazAI/entity/FloaterStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

namespace FloaterStates
{
    static const int ATTACK_DISTANCE_SQR    = 40 * 40;
    static const int FLOATER_EATTACK_DISTANCE_SQR   = 160 * 160;
    static const int FLOATER_EATTACK_DISTANCE_SQR_MIN = 20 * 20;
    static const int FLOATER_ATTACK_SFX_DELAY       = 300;  // [ms]
    static const int FLOATER_ATTACK_SFX_DELAY_RAND  = 150;  // [ms]
    static Claw::UInt64 s_octopusLastHit            = 0;

    Move::Move( Entity::Behavior behavior )
        : m_behavior( behavior )
    {
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( m_behavior );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        if( DotProduct( dst, dst ) < ATTACK_DISTANCE_SQR )
        {
            if( g_rng.GetDouble() < 0.5f )
            {
                sm.ChangeState( entity, SFloaterAttack );
            }
            else
            {
                sm.ChangeState( entity, SFloaterAttack2 );
            }
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        if( DotProduct( dst, dst ) > ATTACK_DISTANCE_SQR )
        {
            sm.ChangeState( entity, SFloaterMove );
        }
        else
        {
            Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
            if( s_octopusLastHit < timeMs )
            {
                GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
                s_octopusLastHit = timeMs + FLOATER_ATTACK_SFX_DELAY + g_rng.GetInt() % FLOATER_ATTACK_SFX_DELAY_RAND;
            }
        }
    }

    void Attack2::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
    }

    EMove::EMove( Entity::Behavior behavior )
        : m_behavior( behavior )
    {
    }

    void EMove::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( m_behavior );
    }

    void EMove::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        Vectorf dist( GameManager::GetInstance()->GetPlayer()->GetPos() - entity.GetPos() );
        dist.Normalize();
        float dsqr = DotProduct( dst, dst );
        if( dsqr < FLOATER_EATTACK_DISTANCE_SQR && dsqr > FLOATER_EATTACK_DISTANCE_SQR_MIN && DotProduct( dist, entity.GetLook() ) > 0 )
        {
            sm.ChangeState( entity, SElectricAttack );
        }
    }

    void EAttack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
        m_firing = false;
    }

    void EAttack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        Vectorf dist( GameManager::GetInstance()->GetPlayer()->GetPos() - entity.GetPos() );
        dist.Normalize();
        float dsqr = DotProduct( dst, dst );
        if( dsqr > FLOATER_EATTACK_DISTANCE_SQR || dsqr < FLOATER_EATTACK_DISTANCE_SQR_MIN || DotProduct( dist, entity.GetLook() ) < 0 )
        {
            sm.ChangeState( entity, SElectricMove );
        }
        else
        {
            entity.SetLook( dist );

            if( entity.GetFrame() >= 4 && entity.GetFrame() <= 7 )
            {
                float dmg = GameManager::GetInstance()->GetEntityManager()->GetData( Entity::FloaterElectric ).damage;
                GameManager::GetInstance()->GetShotManager()->Add( &entity, entity.GetPos(), entity.GetLook(), 1, dmg, Shot::Electricity, 1, 1, 0, false );
                if( !m_firing )
                {
                    m_firing = true;
                    AudioManager::GetInstance()->Play3D( SFX_ELECTRICITY_START, entity.GetPos() );
                }
            }
            else
            {
                m_firing = false;
            }
        }
    }

}

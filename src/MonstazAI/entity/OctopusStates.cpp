#include "claw/math/Math.hpp"
#include "claw/application/Time.hpp"

#include "MonstazAI/entity/OctopusStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

namespace OctopusStates
{
    static const int OCTOPUS_ATTACK_DISTANCE_SQR    = 40 * 40;
    static const int OCTOPUS_ATTACK_SFX_DELAY       = 300;  // [ms]
    static const int OCTOPUS_ATTACK_SFX_DELAY_RAND  = 150;  // [ms]
    static Claw::UInt64 s_octopusLastHit            = 0;

    Move::Move( Entity::Behavior behavior )
        : m_behavior( behavior )
    {
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( m_behavior );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < OCTOPUS_ATTACK_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SOctopusAttack );
                return;
            }
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        bool change = true;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < OCTOPUS_ATTACK_DISTANCE_SQR )
            {
                change = false;
                break;
            }
        }

        if( change )
        {
            sm.ChangeState( entity, SOctopusMove );
            return;
        }

        Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
        if( s_octopusLastHit < timeMs )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
            s_octopusLastHit = timeMs + OCTOPUS_ATTACK_SFX_DELAY + g_rng.GetInt() % OCTOPUS_ATTACK_SFX_DELAY_RAND;
        }
    }

}

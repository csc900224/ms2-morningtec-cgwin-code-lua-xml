#ifndef __INCLUDED_MONSTAZ_FISH_STATES_HPP__
#define __INCLUDED_MONSTAZ_FISH_STATES_HPP__

#include "MonstazAI/State.hpp"

#include "claw/compat/ClawTypes.hpp"

class Entity;

namespace FishStates
{

    class Move : public State
    {
    public:
        Move();

        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

    protected:
        float m_timer;
        float m_walkMin, m_walkMax;
        float m_rageDistSqr;
    };

    class MoveAndThrow : public Move
    {
    public:
        MoveAndThrow();
    };

    class Attack : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Rage : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    private:
        float m_timer;
    };

    class RageAndThrow : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    private:
        bool            Throw( Entity* entity, Entity* target );

        float m_seekTimer;
        Claw::UInt8 m_seekCounter;
        bool m_throwing;
    };

}

#endif

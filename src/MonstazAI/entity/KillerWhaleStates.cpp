#include "claw/math/Math.hpp"

#include "MonstazAI/entity/KillerWhaleStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/State.hpp"

namespace KillerWhaleStates
{

    static const int SECTOID_ATTACK_DISTANCE_SQR    = 30 * 30;
    static const int SECTOID_ATTACK_RELEASE_DISTANCE_SQR    = 35 * 35;
    static const int OCTOPUS_ATTACK_SFX_DELAY       = 300;
    static const int OCTOPUS_ATTACK_SFX_DELAY_RAND  = 150;
    static Claw::UInt64 s_lastHit                   = 0;

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::Chaser );
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_MOVE );

        m_timer = g_rng.GetDouble() * 2 + 4;
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SKillerWhaleAttack );
                return;
            }
        }

        m_timer -= tick;
        if( m_timer < 0 )
        {
            sm.ChangeState( entity, SKillerWhaleSeek );
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/30.f );
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        bool change = true;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_RELEASE_DISTANCE_SQR )
            {
                change = false;
                break;
            }
        }

        if( change )
        {
            sm.ChangeState( entity, SKillerWhaleMove );
            return;
        }

        Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
        if( s_lastHit < timeMs )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
            s_lastHit = timeMs + OCTOPUS_ATTACK_SFX_DELAY + g_rng.GetInt() % OCTOPUS_ATTACK_SFX_DELAY_RAND;
        }
    }

    void Seek::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SwitchAnimSet( Entity::AS_IDLE );
        m_timer = 0;
    }

    void Seek::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_timer += tick;

        if( m_timer < 1 ) return;
        if( m_timer > 3 )
        {
            sm.ChangeState( entity, SKillerWhaleMove );
            return;
        }

        float dist = std::numeric_limits<float>::max();
        Entity* target = NULL;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( (*it)->GetClass() != Entity::Friendly ) continue;
            Vectorf dir( (*it)->GetPos() - entity.GetPos() );
            float dd = DotProduct( dir, dir );
            if( dd < dist )
            {
                float d = sqrt( dd );
                dir /= d;

                bool collide = false;
                Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, d );
                const std::vector<Obstacle*>& v = query->GetColliders();
                for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
                {
                    if( (*oit)->GetKind() == Obstacle::Regular )
                    {
                        collide = true;
                        break;
                    }
                }
                if( !collide )
                {
                    dist = dd;
                    target = *it;
                }
            }
        }

        if( target )
        {
            sm.ChangeState( entity, SKillerWhaleCharge );
            ((Charge*)sm.GetCurrentState())->m_target = target;
        }
    }

    void Charge::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/30.f );
        entity.m_speedMul = 3;
        entity.SetBehavior( Entity::Roller );
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        AudioManager::GetInstance()->Play3D( SFX_WHALE_CHARGE, entity.GetPos() );
    }

    void Charge::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        entity.LookAt( (int)m_target->GetPos().x, (int)m_target->GetPos().y );

        Vectorf dir = m_target->GetPos() - entity.GetPos();
        if( DotProduct( dir, dir ) < 25*25 )
        {
            entity.m_speedMul = 1;
            sm.ChangeState( entity, SKillerWhaleAttack );
        }

        dir.Normalize();
        Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, 25 );
        const std::vector<Obstacle*>& v = query->GetColliders();
        for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
        {
            if( (*oit)->GetKind() == Obstacle::Regular )
            {
                entity.m_speedMul = 1;
                sm.ChangeState( entity, SKillerWhaleMove );
                return;
            }
        }
    }

    void SimpleMove::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_MOVE );
    }

    void SimpleMove::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SKillerWhaleSimpleAttack );
                return;
            }
        }
    }

    void SimpleAttack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void SimpleAttack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        bool change = true;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_DISTANCE_SQR )
            {
                change = false;
                break;
            }
        }

        if( change )
        {
            sm.ChangeState( entity, SKillerWhaleSimpleMove );
            return;
        }

        Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
        if( s_lastHit < timeMs )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
            s_lastHit = timeMs + OCTOPUS_ATTACK_SFX_DELAY + g_rng.GetInt() % OCTOPUS_ATTACK_SFX_DELAY_RAND;
        }
    }

}

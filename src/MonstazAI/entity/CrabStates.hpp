#ifndef __INCLUDED_MONSTAZ_CRAB_STATES_HPP__
#define __INCLUDED_MONSTAZ_CRAB_STATES_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/StackSM.hpp"

namespace CrabStates
{

    class Move : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
    };

    class Attack : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
    };

    class DigIn : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
        bool m_ground;
    };

    class DigOut : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
    };

    class Hidden : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
    };

    class Seek : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
        bool m_wait;
    };

}

#endif

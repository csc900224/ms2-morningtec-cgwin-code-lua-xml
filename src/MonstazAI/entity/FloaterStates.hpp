#ifndef __INCLUDED_MONSTAZ_FLOATER_STATES_HPP__
#define __INCLUDED_MONSTAZ_FLOATER_STATES_HPP__

#include "MonstazAI/State.hpp"
#include "MonstazAI/entity/Entity.hpp"

#include "claw/compat/ClawTypes.hpp"

namespace FloaterStates
{

    class Move : public State
    {
    public:
        Move( Entity::Behavior behavior );

        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        Entity::Behavior m_behavior;
    };

    class Attack : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Attack2 : public Attack
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
    };

    class EMove : public State
    {
    public:
        EMove( Entity::Behavior behavior );

        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        Entity::Behavior m_behavior;
    };

    class EAttack : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        bool m_firing;
    };

}

#endif

#ifndef __INCLUDED_MONSTAZ_AIFRIEND_STATES_HPP__
#define __INCLUDED_MONSTAZ_AIFRIEND_STATES_HPP__

#include "MonstazAI/State.hpp"

class Entity;

namespace AIFriendStates
{

    class Move : public State
    {
    public:
        void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Fire : public State
    {
    public:
        void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

}

#endif

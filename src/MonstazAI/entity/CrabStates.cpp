#include "MonstazAI/entity/CrabStates.hpp"
#include "MonstazAI/GameManager.hpp"

namespace CrabStates
{

    enum { DigInDist = 100*100 };
    enum { DigOutDist = 125*125 };

    static Claw::UInt64 s_lastHit           = 0;

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.m_shadow = 255;
        entity.SetBehavior( Entity::HoundBig );
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_MOVE );

        m_time = g_rng.GetDouble( 2, 7 );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf d( entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos() );
        if( DotProduct( d, d ) < DigInDist && g_rng.GetDouble() < 0.1f )
        {
            sm.ChangeState( entity, SCrabDigIn );
        }

        m_time -= tick;
        if( m_time <= 0 )
        {
            sm.ChangeState( entity, SCrabSeek );
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_time = 7/15.f;
        entity.SwitchAnimSet( Entity::AS_ATTACK );

        Claw::Lua* lua = GameManager::GetInstance()->GetLua();

        lua->PushNumber( 17 );   // Weapon type
        Claw::Lunar<Entity>::push( *lua, &entity );  // shot owner
        lua->PushNumber( entity.GetLook().x );
        lua->PushNumber( entity.GetLook().y );
        lua->Call( "EntityFireWeapon", 4, 0 );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_time -= tick;
        if( m_time <= 0 )
        {
            sm.ChangeState( entity, SCrabMove );
        }
    }

    static const float DigInTime = 11/15.f;

    void DigIn::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetBehavior( Entity::None );
        m_time = DigInTime;
        m_ground = true;
        AudioManager::GetInstance()->Play3D( SFX_CRAB_DIGIN, entity.GetPos() );
    }

    void DigIn::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_time -= tick;
        entity.m_shadow = (Claw::UInt8)Claw::MinMax( (int)(m_time / DigInTime * 255), 0, 255 );
        if( m_ground && m_time < 0.3f )
        {
            m_ground = false;
            GameManager::GetInstance()->CrabDigIn( entity.GetPos() );
        }
        if( m_time <= 0 )
        {
            sm.ChangeState( entity, SCrabHidden );
        }
    }

    static const float DigOutTime = 10/15.f;

    void DigOut::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.m_draw = true;
        entity.SwitchAnimSet( Entity::AS_IDLE2 );
        m_time = DigOutTime;
        AudioManager::GetInstance()->Play3D( SFX_CRAB_DIGOUT, entity.GetPos() );
    }

    void DigOut::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_time -= tick;
        entity.m_shadow = 255 - (Claw::UInt8)Claw::MinMax( (int)(m_time / DigOutTime * 255), 0, 255 );
        if( m_time <= 0 )
        {
            sm.ChangeState( entity, SCrabMove );
        }
    }

    void Hidden::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.m_draw = false;
        m_time = 5;
    }

    void Hidden::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        m_time -= tick;

        Vectorf d( entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos() );
        if( m_time <= 0 || DotProduct( d, d ) > DigOutDist && g_rng.GetDouble() < 0.1f )
        {
            sm.ChangeState( entity, SCrabDigOut );
        }
    }


    void Seek::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetBehavior( Entity::None );
        m_time = g_rng.GetDouble( 1, 2 );
        m_wait = false;
    }

    void Seek::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_wait )
        {
            m_time -= tick;
            if( m_time <= 0 )
            {
                sm.ChangeState( entity, SCrabAttack );
            }
            return;
        }

        float dist = std::numeric_limits<float>::max();
        Entity* target = NULL;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( (*it)->GetClass() != Entity::Friendly ) continue;
            Vectorf dir( (*it)->GetPos() - entity.GetPos() );
            float dd = DotProduct( dir, dir );
            if( dd < dist )
            {
                float d = sqrt( dd );
                dir /= d;

                bool collide = false;
                Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, d );
                const std::vector<Obstacle*>& v = query->GetColliders();
                for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
                {
                    if( (*oit)->GetKind() == Obstacle::Regular )
                    {
                        collide = true;
                        break;
                    }
                }
                if( !collide )
                {
                    dist = dd;
                    target = *it;
                }
            }
        }

        if( target )
        {
            entity.LookAt( (int)target->GetPos().x, (int)target->GetPos().y );
        }

        m_time -= tick;
        if( m_time <= 0 )
        {
            if( target )
            {
                ParticleSystem* p = GameManager::GetInstance()->GetParticleSystem();
                const Vectorf& pos = target->GetPos();
                p->Add( GameManager::GetInstance()->GetTargetIndicator()->operator()( pos.x, pos.y, 0, 0 ) );
            }
            m_wait = true;
            m_time = 1;
        }
    }

}

#ifndef __INCLUDED_MONSTAZ_NAUTIL_STATES_HPP__
#define __INCLUDED_MONSTAZ_NAUTIL_STATES_HPP__

#include "MonstazAI/State.hpp"
#include "MonstazAI/math/Vector.hpp"

#include "claw/compat/ClawTypes.hpp"

class Entity;

namespace NautilStates
{

    class MoveStraight : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class MoveAndTurn : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    private:
        float m_dt;
    };

    class AttackRolling : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class AttackWalking : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Bump : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    private:
        Claw::UInt32    m_lastFrame;
    };

    class Shocked : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    private:
        float   m_timer;
        bool    m_outAnim;
    };

    class Seek : public State
    {
    public:
                        Seek();

        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

    protected:
        virtual Vectorf GetTargetPos( const Entity& entity ) const;

        bool            CheckPath( const Vectorf& origin, const Vectorf& dir, float dist );

        bool            BoundTargetToMap( Vectorf& targetPos, const Vectorf& entityPos );

        float m_timer;
        float m_seekDistMin, m_seekDistMax;
        float m_seekTimeMin, m_seekTimeMax;
    };

    class SeekAndBypass : public Seek
    {
    public:
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

    protected:
        bool            SeekBypass( Vectorf& bypassPos, const Vectorf& origin, const Vectorf& dir, unsigned char tries = 3 );
    };

    class SeekIdle : public Seek
    {
    public:
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class SeekAway : public SeekAndBypass
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

}

#endif

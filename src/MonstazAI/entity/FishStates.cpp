#include "claw/math/Math.hpp"

#include "MonstazAI/entity/FishStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

namespace FishStates
{

    static const int FISH_ATTACK_DISTANCE_SQR               = 40 * 40;
    static const int FISH_THROWING_RAGE_DISTANCE_SQR        = 50 * 50;
    static const float FISH_SEEK_MIN_TIME                   = 0.25f;
    static const float FISH_SEEK_MAX_TIME                   = 0.75f;


    Move::Move()
        : m_walkMin( 8 )
        , m_walkMax( 15 )
        , m_rageDistSqr( 150*150 )
    {}

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_timer = g_rng.GetDouble( m_walkMin, m_walkMax );

        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::Interceptor );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player && player->GetHitPoints() )
        {
            Vectorf dst = entity.GetPos() - player->GetPos();
            float dstSqr = DotProduct( dst, dst );
            if( dstSqr < FISH_ATTACK_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SFishAttack );
            }
            // If outside the rage distance count the time till the next rage.
            // Walk time is reset only after Rage state (by purpose),
            // this will secure situation when player evades near the rage distance border
            // what would cause starving rage timer.
            else if( dstSqr > m_rageDistSqr )
            {
                if( m_timer > tick )
                {
                    m_timer -= tick;
                }
                else
                {
                    m_timer = 0;
                    sm.ChangeState( entity, SFishRage );
                    return;
                }
            }
        }
    }

    MoveAndThrow::MoveAndThrow()
        : Move()
    {
        m_walkMin = 2;
        m_walkMax = 5;
        m_rageDistSqr = FISH_THROWING_RAGE_DISTANCE_SQR;
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player )
        {
            Vectorf dst = entity.GetPos() - player->GetPos();
            // Player out of distance or already died
            if( DotProduct( dst, dst ) > FISH_ATTACK_DISTANCE_SQR || !player->GetHitPoints() )
            {
                sm.ChangeState( entity, SFishMove );
            }
            else
            {
                if( g_rng.GetDouble() < 0.01f )
                {
                    const unsigned int SOUNDS_NUM = 6;
                    const unsigned int SOUND_ID = SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % SOUNDS_NUM;
                    GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SOUND_ID ), entity.GetPos() );
                }
            }
        }
        // Player died or has been removed from scene
        else
        {
            sm.ChangeState( entity, SFishMove );
        }
    }

    void Rage::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetBehavior( Entity::None );

        m_timer = g_rng.GetDouble( 0.25f, 2 );
    }

    void Rage::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer > tick )
        {
            m_timer -= tick;

            // Rage on the player
            Entity* player = GameManager::GetInstance()->GetPlayer();
            if( player )
            {
                entity.LookAt( player->GetPos().x, player->GetPos().y );
            }
        }
        else
        {
            sm.ChangeState( entity, SFishMove );
        }
    }

    void RageAndThrow::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetBehavior( Entity::None );

        m_seekTimer = g_rng.GetDouble( FISH_SEEK_MIN_TIME, FISH_SEEK_MAX_TIME );
        m_seekCounter = g_rng.GetInt( 1, 3 );
        m_throwing = false;

        AudioManager::GetInstance()->Play( SFX_SPIT_PREP );
    }

    void RageAndThrow::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_seekTimer > tick )
        {
            m_seekTimer -= tick;

            // Rage on the player
            Entity* player = GameManager::GetInstance()->GetPlayer();
            if( player )
            {
                entity.LookAt( player->GetPos().x, player->GetPos().y );
            }
        }
        else if( !m_throwing )
        {
            m_seekTimer = 0;

            Entity* player = GameManager::GetInstance()->GetPlayer();
            if( player && m_seekCounter-- )
            {
                m_throwing = Throw( &entity, player );

                // Give me more time to check again
                if( m_seekCounter && !m_throwing )
                {
                    m_seekTimer = g_rng.GetInt( FISH_SEEK_MIN_TIME, FISH_SEEK_MAX_TIME );
                }
            }
            else
            {
                sm.ChangeState( entity, SFishMove );
            }
        }
        else
        {
            if( entity.GetFrame() == entity.GetAnimSet()->GetFrames() - 1 )
            {
                // Revert back animation looping
                entity.SetAnimLoop( true );
                sm.ChangeState( entity, SFishMove );
            }
        }
    }

    bool RageAndThrow::Throw( Entity* entity, Entity* target )
    {
        const Vectorf& targetPos = target->GetPos();
        const Vectorf& entityPos = entity->GetPos();
        Vectorf targetDir = targetPos - entityPos;
        float targetDist = DotProduct( targetDir, targetDir );

        if( targetDist > FISH_THROWING_RAGE_DISTANCE_SQR )
        {
            targetDist = sqrt( targetDist );
            targetDir /= targetDist;

            Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entityPos, targetDir, targetDist );
            const std::vector<Obstacle*>& v = query->GetColliders();
            for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
            {
                if( (*oit)->GetKind() == Obstacle::Regular )
                {
                    return false;
                }
            }
            entity->LookAt( static_cast<int>(targetPos.x), static_cast<int>(targetPos.y) );

            Claw::Lua* lua = GameManager::GetInstance()->GetLua();

            lua->PushNumber( 12 );
            Claw::Lunar<Entity>::push( *lua, entity );
            lua->PushNumber( targetDir.x );
            lua->PushNumber( targetDir.y );
            lua->Call( "EntityFireWeapon", 4, 0 );

            entity->SwitchAnimSet( Entity::AS_SPECIAL );
            entity->SetAnimLoop( false );

            return true;
        }
        return false;
    }

}

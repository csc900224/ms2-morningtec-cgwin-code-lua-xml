#include "claw/math/Math.hpp"
#include "claw/application/Time.hpp"

#include "MonstazAI/entity/LobsterStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

namespace LobsterStates
{
    static const int LOBSTER_ATTACK_DISTANCE_MIN    = 140 * 140;
    static const int LOBSTER_ATTACK_DISTANCE_MAX    = 200 * 160;

    void HitAwareState::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_lastHitPoints = entity.GetHitPoints();
    }

    void HitAwareState::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_lastHitPoints != entity.GetHitPoints() )
        {
            OnHit( entity, sm, m_lastHitPoints - entity.GetHitPoints() );
            m_lastHitPoints = entity.GetHitPoints();
            return;
        }
    }

    Move::Move( Entity::Behavior behavior )
        : m_behavior( behavior )
    {
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        HitAwareState::OnEnter( entity, sm, previousState );

        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( m_behavior );
        entity.SetAnimLoop( true );
        entity.SetFrameTime( 1/20.0f );

        m_timer = 0;
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        HitAwareState::OnUpdate( entity, sm, tick );

        m_timer += tick;

        if( m_timer < 1 ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        float dist = DotProduct( dst, dst );
        if( dist < LOBSTER_ATTACK_DISTANCE_MAX && dist > LOBSTER_ATTACK_DISTANCE_MIN )
        {
            sm.ChangeState( entity, SLobsterIdleBefore );
            return;
        }
    }

    void Move::OnHit( Entity& entity, StackSM& sm, float pointsDelta )
    {
        sm.ChangeState( entity, SLobsterHit );
    }

    void Hit::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetAnimLoop( false );
        entity.SetFrameTime( 1/15.0f );
        entity.SetSlowedDown( true, 10 );

        m_prevState = previousState;
    }

    void Hit::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            sm.ChangeState( entity, m_prevState );
            entity.SetSlowedDown( false );
        }
    }

    void Shoot::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        HitAwareState::OnEnter( entity, sm, previousState );

        entity.SwitchAnimSet( Entity::AS_ATTACK );
        entity.SetAnimLoop( true );
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/20.0f );

        m_timer = 0;
        m_shotsFired = 0;
        m_shoot = previousState != SLobsterHit;
    }

    void Shoot::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        HitAwareState::OnUpdate( entity, sm, tick );

        m_timer += tick;

        if( m_timer > 1 )
        {
            Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
            float dist = DotProduct( dst, dst );
            if( dist > LOBSTER_ATTACK_DISTANCE_MAX || dist < LOBSTER_ATTACK_DISTANCE_MIN )
            {
                sm.ChangeState( entity, SLobsterIdleAfter );
            }
        }

        if( m_shoot && entity.GetFrame() < (entity.GetAnimSet()->GetFrames() >> 1) )
        {
            m_shoot = false;
            entity.LookAt( player->GetPos().x, player->GetPos().y );

            m_shotsFired++;
            GameManager::GetInstance()->LobsterFire();
            GameManager::GetInstance()->LobsterAnim( &entity, m_shotsFired % 2 == 0 );
        }
        else if( !m_shoot && entity.GetFrame() >= (entity.GetAnimSet()->GetFrames() >> 1) )
        {
            m_shoot = true;
        }
    }

    void Shoot::OnHit( Entity& entity, StackSM& sm, float pointsDelta )
    {
        sm.ChangeState( entity, SLobsterHit );
    }

    void IdleBefore::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetAnimLoop( false );
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/15.0f );

        entity.GetHitPoints();
    }

    void IdleBefore::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        entity.LookAt( player->GetPos().x, player->GetPos().y );

        if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            sm.ChangeState( entity, SLobsterShoot );
        }
    }

    void IdleAfter::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE2 );
        entity.SetAnimLoop( false );
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/15.0f );
    }

    void IdleAfter::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        entity.LookAt( player->GetPos().x, player->GetPos().y );

        if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            sm.ChangeState( entity, SLobsterMove );
        }
    }
}

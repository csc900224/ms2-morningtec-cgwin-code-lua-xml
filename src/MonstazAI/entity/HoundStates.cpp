#include "claw/application/Time.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/entity/HoundStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

namespace HoundStates
{
    static const int HOUND_ATTACK_DISTANCE_SQR    = 40 * 40;
    static const int HOUND_ATTACK_SFX_DELAY       = 300;  // [ms]
    static const int HOUND_ATTACK_SFX_DELAY_RAND  = 150;  // [ms]
    static Claw::UInt64 s_octopusLastHit            = 0;

    Move::Move( Entity::Behavior behavior )
        : m_behavior( behavior )
    {
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( m_behavior );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        if( DotProduct( dst, dst ) < HOUND_ATTACK_DISTANCE_SQR )
        {
            sm.ChangeState( entity, SHoundAttack );
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        if( DotProduct( dst, dst ) > HOUND_ATTACK_DISTANCE_SQR )
        {
            sm.ChangeState( entity, SHoundMove );
        }
        else
        {
            Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
            if( s_octopusLastHit < timeMs )
            {
                GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
                s_octopusLastHit = timeMs + HOUND_ATTACK_SFX_DELAY + g_rng.GetInt() % HOUND_ATTACK_SFX_DELAY_RAND;
            }
        }
    }

    void MoveAvoid::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::HoundBig );
        entity.SetFrameTime( 1/15.f );

        m_timer = 0;
    }

    void MoveAvoid::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        m_timer += tick;

        if( m_timer < 1 ) return;

        static const float DistMin = 135 * 140;
        static const float DistMax = 165 * 160;

        Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
        float dist = DotProduct( dst, dst );
        if( dist < DistMax && dist > DistMin )
        {
            sm.ChangeState( entity, SHoundIdleBefore );
        }
    }

    void Shoot::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetFrameTime( 1/30.f );

        m_timer = 0;
        m_shotsFired = 0;
    }

    void Shoot::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        entity.LookAt( player->GetPos().x, player->GetPos().y );

        m_timer += tick;

        while( m_timer > ( m_shotsFired * 9 + 5 ) * 0.033f )
        {
            m_shotsFired++;
            GameManager::GetInstance()->HoundFire();
            GameManager::GetInstance()->HoundAnim( &entity, m_shotsFired % 2 == 0 );
        }
        if( m_timer > 5 * 9 * 0.033f )
        {
            sm.ChangeState( entity, SHoundIdleAfter );
        }
    }

    void IdleBefore::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetBehavior( Entity::None );
        m_timer = 0;
    }

    void IdleBefore::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        entity.LookAt( player->GetPos().x, player->GetPos().y );

        m_timer += tick;
        if( m_timer > 0.5f )
        {
            sm.ChangeState( entity, SHoundShoot );
        }
    }

    void IdleAfter::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetBehavior( Entity::None );
        m_timer = 0;
    }

    void IdleAfter::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_timer += tick;
        if( m_timer > 1 )
        {
            sm.ChangeState( entity, SHoundMoveAvoid );
        }
    }

}

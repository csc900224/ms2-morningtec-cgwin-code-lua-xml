#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/entity/CommonStates.hpp"
#include "MonstazAI/entity/Entity.hpp"

namespace CommonStates
{

    Spawn::Spawn( StateName nextState )
        : m_nextState( nextState )
    {
    }

    void Spawn::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        m_timer = 0.15f;
    }

    void Spawn::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer > tick )
        {
            m_timer -= tick;
        }
        else
        {
            sm.ChangeState( entity, m_nextState );
        }
    }

    FollowWaypoints::FollowWaypoints( Entity::Behavior behavior )
        : m_behavior( behavior )
        , m_loop( false )
        , m_idx( 0 )
        , m_collisionBv( Entity::AVERAGE_RADIUS )
    {
    }

    void FollowWaypoints::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::FollowWaypoints );
        entity.SetBehavior( m_behavior );
        entity.SetFrameTime( 1/15.f );
    }

    void FollowWaypoints::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_idx >= m_nodes.size() ) return;

        const Node& n = m_nodes[m_idx];
        m_collisionBv.SetTranslation( entity.GetPos() );
        if( n.w->Check( &m_collisionBv ) )
        {
            m_idx++;
            if( m_idx == m_nodes.size() && m_loop )
            {
                m_idx = 0;
            }

            if( n.t > 0 )
            {
                sm.ChangeState( entity, SIdleAnim );
                ((IdleAnim*)sm.GetState( SIdleAnim ))->SetWait( n.t );
            }
        }
    }

    void FollowWaypoints::AddWaypoint( Waypoint* waypoint, float time )
    {
        CLAW_ASSERT( waypoint );
        m_nodes.push_back( Node( waypoint, time ) );
    }

    Waypoint* FollowWaypoints::GetWaypoint()
    {
        if( m_idx >= m_nodes.size() ) return NULL;
        return m_nodes[m_idx].w;
    }

    void RunAway::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
        entity.SetBehavior( Entity::RunAway );
        entity.SetFrameTime( 1/15.f );
        entity.SetSlowedDown( true, 100000 );
        entity.GetHarmonics().SetPeriod( 8.0f );
        entity.GetHarmonics().SetAmplitude( 0.5f );
        AudioManager::GetInstance()->Play3D( SFX_CAT_ANGRY, entity.GetPos() );
    }

    void RunAway::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
    }

    void IdleAnim::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        int id = Entity::AS_IDLE + ( g_rng.GetInt() % 5 );
        entity.SwitchAnimSet( id );
        if( id == Entity::AS_IDLE2 )
        {
            AudioManager::GetInstance()->Play3D( SFX_CAT_VOMIT, entity.GetPos() );
        }
    }

    void IdleAnim::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_time > 0 )
        {
            m_time -= tick;
        }
        else
        {
            sm.ChangeState( entity, SFollowWaypoints );
        }
    }

}

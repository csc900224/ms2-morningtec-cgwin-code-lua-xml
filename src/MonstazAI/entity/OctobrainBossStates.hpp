#ifndef __INCLUDED_MONSTAZ_OCTOBRAINBOSS_STATES_HPP__
#define __INCLUDED_MONSTAZ_OCTOBRAINBOSS_STATES_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/StackSM.hpp"

namespace OctobrainBossStates
{
    class Move : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        bool Attack( Entity& entity, StackSM& sm );

        float m_cooldownTimer;
        float m_minHp;
        bool m_rescue;
    };

    class Charge : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    private:
        void SetTarget( Entity& entity, const Entity& target );

        float m_origSpeed;
        float m_timer;
    };

    class Shoot : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    protected:
        bool ShootingTick( Entity& entity, float tick, int weapon, float delay );

    private:
        float m_delayTimer;
        float m_timer;
        float m_minHp;
        Entity* m_target;
    };

    class Teleport : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    private:
        enum TeleportState
        {
            TS_UNKNOWN = -1,
            TS_OUT,
            TS_TRAVEL,
            TS_IN,
        };

        void         ChangeLocataion( Entity& entity );
        void         ChangeState( Entity& entity, TeleportState newState );

        TeleportState m_state;
        float m_timer;
        float m_blinkTimer;
        bool  m_blinker;
        float m_animTime;
        float m_origShadowScale;
        StateName m_prevState;
    };

    class TeleportPlayer : public State
    {
    public:
        TeleportPlayer();
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    private:
        enum TeleportState
        {
            TS_UNKNOWN = -1,
            TS_OUT,
            TS_TRAVEL,
            TS_IN,
        };

        void         ChangeLocataion( Entity& entity );
        void         ChangeState( Entity& entity, TeleportState newState );

        TeleportState m_state;
        float m_timer;
        int m_lastLocation;
        StateName m_prevState;
    };

    class Clone : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    private:
        typedef std::vector<Vectorf > PosVector;

        void CalculateSpawnPos( Entity& entity );
        void CalculateSpawnPos( Entity& entity, int numClones );
        void SpawnClone( const Vectorf& pos );
        void SpawnTick( float tick );

        float m_startHp;
        float m_startSpeed;
        float m_timer;
        float m_spawnTimer;
        PosVector m_positions;
    };

    class CloneBaseState : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    protected:
        Claw::UInt8 m_flickerAlpha;
        Claw::UInt8 m_baseAlpha;
        float m_flickerTimer;
    };

    class CloneSapawn : public CloneBaseState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    private:
        float m_animTime;
        float m_origShadowScale;
        float m_timer;
    };

    class CloneShoot : public Shoot, public CloneBaseState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState );

    private:
        float m_delayTimer;
        float m_timer;
    };

    class CloneMove : public CloneBaseState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_timer;
    };
}

#endif

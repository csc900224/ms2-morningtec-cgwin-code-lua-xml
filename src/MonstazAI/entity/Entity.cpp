#include "claw/base/AssetDict.hpp"
#include "claw/math/Math.hpp"
#include "claw/math/Matrix.hpp"

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/entity/OctopusStates.hpp"
#include "MonstazAI/entity/SqueezerStates.hpp"
#include "MonstazAI/entity/FishStates.hpp"
#include "MonstazAI/entity/CommonStates.hpp"
#include "MonstazAI/entity/FloaterStates.hpp"
#include "MonstazAI/entity/HoundStates.hpp"
#include "MonstazAI/entity/SectoidStates.hpp"
#include "MonstazAI/entity/KillerWhaleStates.hpp"
#include "MonstazAI/entity/CrabStates.hpp"
#include "MonstazAI/entity/MechaBossStates.hpp"
#include "MonstazAI/entity/SowerBossStates.hpp"
#include "MonstazAI/entity/OctobrainBossStates.hpp"
#include "MonstazAI/entity/LobsterStates.hpp"
#include "MonstazAI/entity/NautilStates.hpp"
#include "MonstazAI/entity/NervalStates.hpp"
#include "MonstazAI/entity/AIFriendStates.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/shot/RipperShot.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"
#include "MonstazAI/TutorialManager.hpp"

const float Entity::AVERAGE_RADIUS = 10.f;

const float RAM_INVIOLABLE_TIME     = 1.0f;    // ram distance / rammer speed
const float GLOP_INVIOLABLE_TIME    = 0.5f;    // (entity size- glop size) / glop speed
const float GLOP_SLOW_DOWN_TIME     = 4.0f;
const float SPIT_INVIOLABLE_TIME    = 0.4f;
const float SPIT_SLOW_DOWN_TIME     = 5.0f;

LUA_DECLARATION( Entity )
{
    METHOD( Entity, GetType ),
    METHOD( Entity, GetBehavior ),
    METHOD( Entity, GetPos ),
    METHOD( Entity, SetPos ),
    METHOD( Entity, GetLook ),
    METHOD( Entity, SetLook ),
    METHOD( Entity, GetTarget ),
    METHOD( Entity, SetTarget ),
    METHOD( Entity, GetDir ),
    METHOD( Entity, GetHitPoints ),
    METHOD( Entity, SetHitPoints ),
    METHOD( Entity, GetMaxHitPoints ),
    METHOD( Entity, SetMaxHitPoints ),
    METHOD( Entity, SetFrame ),
    METHOD( Entity, SetClass ),
    METHOD( Entity, AddWaypoint ),
    METHOD( Entity, SetLoop ),
    {0,0}
};

Entity::Entity( lua_State* L )
    : m_sm( SEntityIdle )
    , m_animUpdate( true )
    , m_animLoop( true )
    , m_glopTimer( 0 )
    , m_slowDownTimer( 0 )
    , m_avoidanceRotSpeed( 0.0f )
    , m_class( Enemy )
{
    CLAW_ASSERT( false );
}

void Entity::Init( Claw::Lua* lua )
{
    Claw::Lunar<Entity>::Register( *lua );
}

Entity::Entity( Type type, float x, float y, Pickup::Type item, const Vectorf& look, int element )
    : Renderable( x, y )
    , m_type( type )
    , m_look( look )
    , m_frame( 0 )
    , m_frameTime( 0 )
    , m_frameNext( 1/30.f )
    , m_lastDir( 0 )
    , m_currentAnim( 0 )
    , m_animUpdate( true )
    , m_animLoop( true )
    , m_hitPoints( GameManager::GetInstance()->GetEntityManager()->GetData( type ).hp )
    , m_maxHitPoints( m_hitPoints )
    , m_orbsCounter( GameManager::GetInstance()->GetEntityManager()->GetData( type ).orbsDrop )
    , m_lastHit( Shot::Bullet )
    , m_glopTimer( 0 )
    , m_slowDownTimer( 0 )
    , m_slowDownFactor( 1.0f )
    , m_sm( SEntityIdle )
    , m_target( 1.0f, 0.0f )
    , m_dir( 1.0f, 0.0f )
    , m_behavior( None )
    , m_avoidanceRotSpeed( 0.0f )
    , m_class( Enemy )
    , m_item( item )
    , m_speedMul( 1 )
    , m_draw( true )
    , m_shadow( 255 )
    , m_shadowScale( 1 )
    , m_entityRadius( 12*12 )
    , m_bossState( 0 )
    , m_invulnerable( false )
    , m_frameDir( 1 )
    , m_alpha( 255 )
    , m_inviolable( false )
    , m_harmless( false )
    , m_canBleed( true )
    , m_hologram( 0.0f )
    , m_elements( NULL )
    , m_underAttack( false )
{
    m_moveHarmonics.SetPhaseOffset( 0.0f );
    m_moveHarmonics.SetAmplitude( 0.0f );

    float ss = GameManager::GetInstance()->GetEntityManager()->GetData( type ).shieldScale;
    switch( element )
    {
    case 0:
        break;
    case 1:
        m_elements = new EffectElements( this, Claw::AssetDict::Get<GfxAsset>( "gfx/elements/shield_electric_huge.png.pivot@linear" ), ss );
        m_effects.push_back( EntityEffectPtr( m_elements ) );
        break;
    case 2:
        m_elements = new EffectElements( this, Claw::AssetDict::Get<GfxAsset>( "gfx/elements/shield_fire_huge.png.pivot@linear" ), ss );
        m_effects.push_back( EntityEffectPtr( m_elements ) );
        break;
    case 4:
        m_elements = new EffectElements( this, Claw::AssetDict::Get<GfxAsset>( "gfx/elements/shield_ice_huge.png.pivot@linear" ), ss );
        m_effects.push_back( EntityEffectPtr( m_elements ) );
        break;
    default:
        CLAW_ASSERT( false );
        break;
    }

    switch( type )
    {
    case Player:
        m_hitPoints = GameManager::GetInstance()->GetStats()->GetMaxPlayerHP();
        SetAnimUpdate( false );
        SetBehavior( Human );
        m_slowDownFactor = 0.7f;    // Player slow down after beeing hit by glop
        break;
    case OctopusSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 0 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 12 ) );
        m_sm.RegisterState( SOctopusMove, new OctopusStates::Move( type == OctopusShotAware ? ShotAvoider : Following ) );
        m_sm.RegisterState( SOctopusAttack, new OctopusStates::Attack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SOctopusMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Set harmonic period equal to number of walk animation loop
        m_moveHarmonics.SetPeriod( 12.0f );
        m_moveHarmonics.SetPhaseOffset( 0.5f );
        m_moveHarmonics.SetAmplitude( 0.55f );
        break;
     case OctopusShotAware:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 6 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 17 ) );
        m_sm.RegisterState( SOctopusMove, new OctopusStates::Move( type == OctopusShotAware ? ShotAvoider : Following ) );
        m_sm.RegisterState( SOctopusAttack, new OctopusStates::Attack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SOctopusMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Set harmonic period equal to number of walk animation loop
        m_moveHarmonics.SetPeriod( 12.0f );
        m_moveHarmonics.SetPhaseOffset( 0.5f );
        m_moveHarmonics.SetAmplitude( 0.55f );
        break;
     case OctopusChaser:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 1 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 13 ) );
        m_sm.RegisterState( SOctopusMove, new OctopusStates::Move( Chaser ) );
        m_sm.RegisterState( SOctopusAttack, new OctopusStates::Attack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SOctopusMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Set harmonic period equal to number of walk animation loop
        m_moveHarmonics.SetPeriod( 12.0f );
        m_moveHarmonics.SetPhaseOffset( 2.0f );
        m_moveHarmonics.SetAmplitude( 0.75f );
        break;
    case OctopusFriend:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 11 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 21 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 22 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 23 ) );
        m_animSet[AS_IDLE3].Reset( GameManager::GetInstance()->GetMonsterSet( 28 ) );
        m_animSet[AS_IDLE4].Reset( GameManager::GetInstance()->GetMonsterSet( 29 ) );
        m_animSet[AS_IDLE5].Reset( GameManager::GetInstance()->GetMonsterSet( 30 ) );
        m_sm.RegisterState( SFollowWaypoints, new CommonStates::FollowWaypoints( FollowWaypoints ) );
        m_sm.RegisterState( SRunAway, new CommonStates::RunAway() );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SFollowWaypoints ) );
        m_sm.RegisterState( SIdleAnim, new CommonStates::IdleAnim() );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_slowDownFactor = 4;
        break;
    case AIFriend:
        {
            Claw::Lua* lua = GameManager::GetInstance()->GetLua();
            lua_getglobal( *lua, "Weapons" );
            lua_getglobal( *lua, "WeaponsIdxById" );
            lua_pushstring( *lua, Claw::Registry::Get()->CheckString( "/internal/friendWeapon" ) );
            lua_gettable( *lua, -2 );
            lua_gettable( *lua, -3 );
            lua_pushstring( *lua, "Atlas" );
            lua_gettable( *lua, -2 );
            int set = luaL_checknumber( *lua, -1 );
            lua_pop( *lua, 4 );

            m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetPlayerSet( 20 + set ) );
            SetAnimUpdate( false );
            m_maxHitPoints = m_hitPoints = GameManager::GetInstance()->GetStats()->GetMaxPlayerHP();
            m_sm.RegisterState( SAIFriendMove, new AIFriendStates::Move() );
            m_sm.RegisterState( SAIFriendFire, new AIFriendStates::Fire() );
            m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SAIFriendMove ) );
            m_sm.ChangeState( *this, SCommonSpawn );
            m_slowDownFactor = 0.7f;
        }
        break;
    case SqueezerSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 2 ) );
        m_animSet[AS_ATTACK].Release();
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 24 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 32 ) );
        m_sm.RegisterState( SSqueezerMove, new SqueezerStates::MoveStraight );
        m_sm.RegisterState( SSqueezerAttack, new SqueezerStates::AttackRolling );
        m_sm.RegisterState( SSqueezerBump, new SqueezerStates::Bump );
        m_sm.RegisterState( SSqueezerShocked, new SqueezerStates::Shocked );
        m_sm.RegisterState( SSqueezerSeek, new SqueezerStates::SeekAndBypass );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SSqueezerSeek ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Set harmonic period to the half of animation loop - this nicely simulates rolling
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( -1.0f );
        m_moveHarmonics.SetAmplitude( 0.35f );
        break;
    case SqueezerTurning:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 3 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 14 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 25 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 33 ) );
        m_sm.RegisterState( SSqueezerMove, new SqueezerStates::MoveAndTurn );
        m_sm.RegisterState( SSqueezerMoveStraight, new SqueezerStates::MoveStraight );
        m_sm.RegisterState( SSqueezerAttack, new SqueezerStates::AttackWalking );
        m_sm.RegisterState( SSqueezerBump, new SqueezerStates::Bump );
        m_sm.RegisterState( SSqueezerShocked, new SqueezerStates::Shocked );
        m_sm.RegisterState( SSqueezerSeek, new SqueezerStates::SeekIdle );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SSqueezerSeek ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_slowDownFactor = 0.4f;        // Velocity slow down during player attack
        // Set harmonic period to the half of animation loop - this nicely simulates and bitting
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( 1.0f );
        m_moveHarmonics.SetAmplitude( 0.2f );
        break;
    case FishSimple:
    case FishSimpleNonExploding:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 4 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 15) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 26 ) );
        m_sm.RegisterState( SFishMove, new FishStates::Move );
        m_sm.RegisterState( SFishAttack, new FishStates::Attack );
        m_sm.RegisterState( SFishRage, new FishStates::Rage );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SFishMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Half of animation frames is just enough harmonic for walking
        m_moveHarmonics.SetPeriod( 7.0f );
        m_moveHarmonics.SetAmplitude( 0.0f );
        break;
    case FishThrowing:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 5 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 16 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 27 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 34 ) );
        m_sm.RegisterState( SFishMove, new FishStates::MoveAndThrow );
        m_sm.RegisterState( SFishAttack, new FishStates::Attack );
        m_sm.RegisterState( SFishRage, new FishStates::RageAndThrow );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SFishMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Same as above
        m_moveHarmonics.SetPeriod( 7.0f );
        m_moveHarmonics.SetAmplitude( 0.0f );
        break;
    case FloaterSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 7 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 18 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 29 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 35 ) );
        m_sm.RegisterState( SFloaterMove, new FloaterStates::Move( Flying ) );
        m_sm.RegisterState( SFloaterAttack, new FloaterStates::Attack );
        m_sm.RegisterState( SFloaterAttack2, new FloaterStates::Attack2 );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SFloaterMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        break;
    case FloaterElectric:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 8 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 19 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 30 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 36 ) );
        m_sm.RegisterState( SElectricMove, new FloaterStates::EMove( FlyingAggressive ) );
        m_sm.RegisterState( SElectricAttack, new FloaterStates::EAttack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SElectricMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        break;
    case FloaterSower:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 97 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 95 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 96 ) );
        m_sm.RegisterState( SFloaterMove, new FloaterStates::Move( Flying ) );
        m_sm.RegisterState( SFloaterAttack, new FloaterStates::Attack );
        m_sm.RegisterState( SFloaterAttack2, new FloaterStates::Attack2 );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SFloaterMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        break;
    case HoundSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 9 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 20 ) );
        m_sm.RegisterState( SHoundMove, new HoundStates::Move( Chaser ) );
        m_sm.RegisterState( SHoundAttack, new HoundStates::Attack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SHoundMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_shadowScale = 1.75f;
        break;
    case HoundShooting:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 10 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 31 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 37 ) );
        m_sm.RegisterState( SHoundMoveAvoid, new HoundStates::MoveAvoid() );
        m_sm.RegisterState( SHoundShoot, new HoundStates::Shoot() );
        m_sm.RegisterState( SHoundIdleBefore, new HoundStates::IdleBefore() );
        m_sm.RegisterState( SHoundIdleAfter, new HoundStates::IdleAfter() );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SHoundMoveAvoid ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_moveHarmonics.SetPeriod( 11 );
        m_moveHarmonics.SetPhaseOffset( 0.3f );
        m_moveHarmonics.SetAmplitude( 1 );
        break;
    case SectoidSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 40 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 38 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 39 ) );
        m_sm.RegisterState( SSectoidMove, new SectoidStates::Move() );
        m_sm.RegisterState( SSectoidAttack, new SectoidStates::Attack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SSectoidMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        SetBehavior( Chaser );
        break;
    case KillerWhale:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 41 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 42 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 43 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 44 ) );
        m_sm.RegisterState( SKillerWhaleMove, new KillerWhaleStates::Move() );
        m_sm.RegisterState( SKillerWhaleAttack, new KillerWhaleStates::Attack );
        m_sm.RegisterState( SKillerWhaleSeek, new KillerWhaleStates::Seek );
        m_sm.RegisterState( SKillerWhaleCharge, new KillerWhaleStates::Charge );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SKillerWhaleMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        break;
    case KillerWhaleSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 45 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 46 ) );
        m_sm.RegisterState( SKillerWhaleSimpleMove, new KillerWhaleStates::SimpleMove() );
        m_sm.RegisterState( SKillerWhaleSimpleAttack, new KillerWhaleStates::SimpleAttack );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SKillerWhaleSimpleMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        SetBehavior( Chaser );
        break;
    case SectoidShooting:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 48 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 47 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 49 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 50 ) );
        m_sm.RegisterState( SSectoidShootingMove, new SectoidStates::MoveShooting() );
        m_sm.RegisterState( SSectoidShootingAttack, new SectoidStates::AttackShooting );
        m_sm.RegisterState( SSectoidShootingTargetting, new SectoidStates::Targetting );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SSectoidShootingMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        break;
    case Crab:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 55 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 52 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 53 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 54 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 51 ) );
        m_sm.RegisterState( SCrabMove, new CrabStates::Move() );
        m_sm.RegisterState( SCrabDigIn, new CrabStates::DigIn );
        m_sm.RegisterState( SCrabDigOut, new CrabStates::DigOut );
        m_sm.RegisterState( SCrabHidden, new CrabStates::Hidden );
        m_sm.RegisterState( SCrabAttack, new CrabStates::Attack );
        m_sm.RegisterState( SCrabSeek, new CrabStates::Seek );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SCrabMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_moveHarmonics.SetPeriod( 3.5f );
        m_moveHarmonics.SetPhaseOffset( 0 );
        m_moveHarmonics.SetAmplitude( 0.65f );
        break;
    case MechaBoss:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 61 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 60 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 56 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 57 ) );
        m_animSet[AS_IDLE3].Reset( GameManager::GetInstance()->GetMonsterSet( 58 ) );
        m_animSet[AS_IDLE4].Reset( GameManager::GetInstance()->GetMonsterSet( 59 ) );
        m_sm.RegisterState( SMechaBossMove, new MechaBossStates::Move() );
        m_sm.RegisterState( SMechaBossAttackMace, new MechaBossStates::Attack() );
        m_sm.RegisterState( SMechaBossFly, new MechaBossStates::Fly() );
        m_sm.RegisterState( SMechaBossAttackFlamer, new MechaBossStates::AttackFlamer() );
        m_sm.RegisterState( SMechaBossAttackRocket, new MechaBossStates::AttackRocket() );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SMechaBossMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_moveHarmonics.SetPeriod( 5 );
        m_moveHarmonics.SetPhaseOffset( 0 );
        m_moveHarmonics.SetAmplitude( 0.3f );
        m_shadowScale = 2;
        m_entityRadius = 30*30;
        break;
    case SowerBoss:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 86 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 83 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 84 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 85 ) );
        m_sm.RegisterState( SSowerBossMove, new SowerBossStates::Move() );
        m_sm.RegisterState( SSowerBossRun, new SowerBossStates::Run() );
        m_sm.RegisterState( SSowerBossAttackEggShot, new SowerBossStates::Attack() );
        m_sm.RegisterState( SSowerBossAttackEggSpit, new SowerBossStates::AttackEggSpit() );
        m_sm.RegisterState( SSowerBossAttackTongue, new SowerBossStates::AttackTongue() );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SSowerBossMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( 0 );
        m_moveHarmonics.SetAmplitude( 1.0f );
        m_shadowScale = 3.5f;
        m_entityRadius = 30*30;
        break;
    case OctobrainBoss:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 94 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 93 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 92 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 87 ) );
        m_animSet[AS_IDLE3].Reset( GameManager::GetInstance()->GetMonsterSet( 88 ) );
        m_animSet[AS_IDLE4].Reset( GameManager::GetInstance()->GetMonsterSet( 89 ) );
        m_sm.RegisterState( SOctobrainBossMove, new OctobrainBossStates::Move() );
        m_sm.RegisterState( SOctobrainBossClone, new OctobrainBossStates::Clone() );
        m_sm.RegisterState( SOctobrainBossTeleport, new OctobrainBossStates::Teleport() );
        m_sm.RegisterState( SOctobrainBossTeleportPlayer, new OctobrainBossStates::TeleportPlayer() );
        m_sm.RegisterState( SOctobrainBossCharge, new OctobrainBossStates::Charge() );
        m_sm.RegisterState( SOctobrainBossShoot, new OctobrainBossStates::Shoot() );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SOctobrainBossMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( 0 );
        m_moveHarmonics.SetAmplitude( 0 );
        m_shadowScale = 2;
        m_entityRadius = 20*20;
        break;
    case OctobrainBossClone:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 94 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 93 ) );
        m_animSet[AS_IDLE4].Reset( GameManager::GetInstance()->GetMonsterSet( 89 ) );
        m_sm.RegisterState( SOctobrainBossMove, new OctobrainBossStates::CloneMove() );
        m_sm.RegisterState( SOctobrainBossCloneSpawn, new OctobrainBossStates::CloneSapawn() );
        m_sm.RegisterState( SOctobrainBossShoot, (OctobrainBossStates::Shoot*)new OctobrainBossStates::CloneShoot() );
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( 0 );
        m_moveHarmonics.SetAmplitude( 0 );
        m_shadowScale = 2;
        m_entityRadius = 20*20;
        m_canBleed = false;
        m_sm.ChangeState( *this, SOctobrainBossCloneSpawn );
        break;
    case Lobster:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 66 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 62 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 63 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 64 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 65 ) );
        m_sm.RegisterState( SLobsterHit, new LobsterStates::Hit() );
        m_sm.RegisterState( SLobsterIdleAfter, new LobsterStates::IdleAfter() );
        m_sm.RegisterState( SLobsterIdleBefore, new LobsterStates::IdleBefore() );
        m_sm.RegisterState( SLobsterMove, new LobsterStates::Move( HoundBig ) );
        m_sm.RegisterState( SLobsterShoot, new LobsterStates::Shoot() );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SLobsterMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_moveHarmonics.SetPeriod( 5.5f );
        m_moveHarmonics.SetPhaseOffset( 0 );
        m_moveHarmonics.SetAmplitude( 0.5f );
        m_slowDownFactor = 0.5f;
        break;
    case NautilSimple:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 71 ) );
        m_animSet[AS_ATTACK].Release();
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 70 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 67 ) );
        m_animSet[AS_IDLE3].Reset( GameManager::GetInstance()->GetMonsterSet( 69 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 68 ) );
        m_sm.RegisterState( SNautilMove, new NautilStates::MoveStraight );
        m_sm.RegisterState( SNautilAttack, new NautilStates::AttackRolling );
        m_sm.RegisterState( SNautilBump, new NautilStates::Bump );
        m_sm.RegisterState( SNautilShocked, new NautilStates::Shocked );
        m_sm.RegisterState( SNautilSeek, new NautilStates::SeekAndBypass );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SNautilSeek ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        // Set harmonic period to the half of animation loop - this nicely simulates rolling
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( -1.0f );
        m_moveHarmonics.SetAmplitude( 0.35f );
        break;
    case NautilTurning:
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 76 ) );
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 77 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 75 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 72 ) );
        m_animSet[AS_IDLE3].Reset( GameManager::GetInstance()->GetMonsterSet( 73 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 73 ) );
        m_sm.RegisterState( SNautilMove, new NautilStates::MoveAndTurn );
        m_sm.RegisterState( SNautilMoveStraight, new NautilStates::MoveStraight );
        m_sm.RegisterState( SNautilAttack, new NautilStates::AttackWalking );
        m_sm.RegisterState( SNautilBump, new NautilStates::Bump );
        m_sm.RegisterState( SNautilShocked, new NautilStates::Shocked );
        m_sm.RegisterState( SNautilSeek, new NautilStates::SeekIdle );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SNautilSeek ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        m_slowDownFactor = 0.4f;        // Velocity slow down during player attack
        // Set harmonic period to the half of animation loop - this nicely simulates and bitting
        m_moveHarmonics.SetPeriod( 4.0f );
        m_moveHarmonics.SetPhaseOffset( 1.0f );
        m_moveHarmonics.SetAmplitude( 0.2f );
        break;
    case Nerval:
        m_animSet[AS_ATTACK].Reset( GameManager::GetInstance()->GetMonsterSet( 78 ) );
        m_animSet[AS_SPECIAL].Reset( GameManager::GetInstance()->GetMonsterSet( 79 ) );
        m_animSet[AS_IDLE].Reset( GameManager::GetInstance()->GetMonsterSet( 80 ) );
        m_animSet[AS_IDLE2].Reset( GameManager::GetInstance()->GetMonsterSet( 81 ) );
        m_animSet[AS_MOVE].Reset( GameManager::GetInstance()->GetMonsterSet( 82 ) );
        m_sm.RegisterState( SNervalMove, new NervalStates::Move() );
        m_sm.RegisterState( SNervalAttack, new NervalStates::Attack );
        m_sm.RegisterState( SNervalSeek, new NervalStates::Seek );
        m_sm.RegisterState( SNervalCharge, new NervalStates::Charge );
        m_sm.RegisterState( SCommonSpawn, new CommonStates::Spawn( SNervalMove ) );
        m_sm.ChangeState( *this, SCommonSpawn );
        break;
    default:
        CLAW_MSG_ASSERT( false, "Unrecognized entity type." );
    }
}

Entity::~Entity()
{
    m_sm.ReleaseStates();
}

void Entity::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    float x, y;
    x = m_pos.x * scale - offset.x;
    y = m_pos.y * scale - offset.y;

    for( std::vector<EntityEffectPtr>::const_iterator it = m_effects.begin(); it != m_effects.end(); ++it )
    {
        (*it)->RenderBefore( target, x, y );
    }

    GfxAsset* gfx = m_animSet[m_currentAnim]->GetAsset( m_look, m_frame, m_lastDir );
    gfx->GetSurface()->SetAlpha( m_alpha );
    
    if( m_hologram > 0 )
    {
        Hologram* h = GameManager::GetInstance()->GetHologram();
        h->SetIntensity( m_hologram );
        h->Render( gfx->GetSurface(), target, x - gfx->GetPivot().x, y - gfx->GetPivot().y, gfx->GetSurface()->GetClipRect() );
    }
    else
    {
        gfx->Blit( target, x, y );
    }

    for( std::vector<EntityEffectPtr>::const_iterator it = m_effects.begin(); it != m_effects.end(); ++it )
    {
        (*it)->RenderAfter( target, x, y );
    }
}

void Entity::RenderPost( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    float x, y;
    x = m_pos.x * scale - offset.x;
    y = m_pos.y * scale - offset.y;

    for( std::vector<EntityEffectPtr>::const_iterator it = m_effects.begin(); it != m_effects.end(); ++it )
    {
        (*it)->RenderAfterEverything( target, x, y );
    }
}

void Entity::Update( float dt )
{
    if( IsAnimUpdated() )
    {
        m_frameTime += dt;
        while( m_frameTime > m_frameNext )
        {
            m_frameTime -= m_frameNext;
            if( IsAnimLooped() )
            {
                if( m_frameDir > 0 )
                {
                    m_frame = ( m_frame + 1 ) % m_animSet[m_currentAnim]->GetFrames();
                }
                else if( m_frameDir < 0 )
                {
                    if( --m_frame < 0 ) m_frame = m_animSet[m_currentAnim]->GetFrames() + m_frame;
                }
            }
            else
            {
                if( m_frameDir > 0 && m_frame < m_animSet[m_currentAnim]->GetFrames() - 1 ) ++m_frame;
                else if( m_frameDir < 0 && m_frame > 0 ) --m_frame;
                else break;
            }
        }
    }

    std::vector<EntityEffectPtr>::iterator it = m_effects.begin();
    while( it != m_effects.end() )
    {
        if( (*it)->Update( dt ) )
        {
            ++it;
        }
        else
        {
            it = m_effects.erase( it );
        }
    }

    if( m_glopTimer > 0 )       m_glopTimer -= dt;
    if( m_slowDownTimer > 0 )   m_slowDownTimer -= dt;

    m_sm.Update( *this, dt );
}

void Entity::LookAt( int x, int y )
{
    Vectorf l( x - m_pos[0], y - m_pos[1] );
    l.Normalize();
    m_look = l;
}

void Entity::AddEffect( EntityEffect* entity )
{
    m_effects.push_back( EntityEffectPtr( entity ) );
}

void Entity::Hit( Shot::Type type, Claw::UInt8 elements, float val )
{
    m_lastHitElements = elements;
    m_lastHit = type;
    m_underAttack = true;

    if( type == Shot::FishGlop )
    {
        m_glopTimer = GLOP_INVIOLABLE_TIME;
        m_slowDownTimer = GLOP_SLOW_DOWN_TIME;
        m_slowDownHit = type;
    }
    else if( type == Shot::SowerSpit )
    {
        m_glopTimer = SPIT_INVIOLABLE_TIME;
        m_slowDownTimer = SPIT_SLOW_DOWN_TIME;
        m_slowDownHit = type;
    }

    if( m_invulnerable || val == 0 ) return;

    // Designer request - rolling entities gets 1/3 of injuries and play rock sound on beeing hit
    if( IsSheathed( type ) )
    {
        val *= GameManager::GetInstance()->GetSheathedHitMultiplier();

        if( GameManager::GetInstance()->GetEntityManager()->CheckLastHit() )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_RICOCHET1 + g_rng.GetInt() % 7 ), GetPos() );
        }
    }
    else
    {
        val = GameManager::GetInstance()->ApplyShotHitMultiplier( type, GetType(), val );

        if( !( m_type == Player && type == Shot::Electricity ) && GameManager::GetInstance()->GetEntityManager()->CheckLastHit() )
        {
            switch( m_type )
            {
            case FishSimple:
            case FishSimpleNonExploding:
            case FishThrowing:
                GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_FISH_HIT1 + g_rng.GetInt() % 2 ), GetPos() );
                break;
            default:
                GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_HIT1 + g_rng.GetInt() % 3 ), GetPos() );
                break;
            }
        }
    }

    m_hitPoints -= val;
    m_orbsCounter -= val;

    int orbtime = GameManager::GetInstance()->GetStats()->CheckPerk( Perks::OrbExtender ) ? 4 : 3;
    int orbs = GameManager::GetInstance()->GetEntityManager()->GetData( m_type ).orbs;
    float drop = GameManager::GetInstance()->GetEntityManager()->GetData( m_type ).orbsDrop;
    while( m_orbsCounter < 0 )
    {
        m_orbsCounter += drop;

        for( int i=0; i<orbs; i++ )
        {
            GameManager::GetInstance()->GetPickupManager()->Add( m_pos + Vectorf( ( g_rng.GetDouble() * 2 - 1 ) * 100.f, ( g_rng.GetDouble() * 2 - 1 ) * 100.f ), Pickup::MagicOrb, orbtime );
        }
    }
}

void Entity::CollideObstacle()
{
    switch( GetType() )
    {
    case Entity::SqueezerSimple:
    case Entity::SqueezerTurning:
        m_sm.ChangeState( *this, SSqueezerBump );
        break;
    case Entity::NautilSimple:
    case Entity::NautilTurning:
        m_sm.ChangeState( *this, SNautilBump );
        break;
    case Entity::OctobrainBoss:
        m_sm.ChangeState( *this, SOctobrainBossTeleport );
        break;
    case Entity::KillerWhale:
        m_speedMul = 1;
        m_sm.ChangeState( *this, SKillerWhaleMove );
        break;
    case Entity::Nerval:
        m_sm.ChangeState( *this, SNervalMove );
        break;
    default:
        CLAW_ASSERT( false );
        break;
    }
}

void Entity::Die()
{
    if( m_type == Entity::FishSimple )
    {
        Explosion::Params params( 0.2f, 1.5f, 400, 4 );

        GameManager::GetInstance()->GetExplosionManager()->Add( GetPos(), params, true, Shot::FishGlop );
        GameManager::GetInstance()->AddExplosionHole( GetPos() );
    }
    else if( m_type == Entity::HoundShooting )
    {
        ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
        ps->Add( new ExplosionEmitter( GameManager::GetInstance()->GetExplosionManager()->GetGibs(), ps, m_pos.x, m_pos.y, 150, 300, 0.25f, 25 ) );
        ps->Add( new ExplosionEmitter( GameManager::GetInstance()->GetExplosionManager()->GetGibs(), ps, m_pos.x, m_pos.y, 300, 300, 0.25f, 50 ) );
        ps->Add( new ExplosionEmitter( GameManager::GetInstance()->GetExplosionManager()->GetGibs(), ps, m_pos.x, m_pos.y, 400, 400, 0.25f, 50 ) );
        ps->Add( new ExplosionEmitter( GameManager::GetInstance()->GetExplosionManager()->GetGibs(), ps, m_pos.x, m_pos.y, 500, 500, 0.25f, 50 ) );
    }

    m_sm.ChangeState( *this, SEntityIdle );
}

Waypoint* Entity::GetWaypoint()
{
    CommonStates::FollowWaypoints* s = (CommonStates::FollowWaypoints*)m_sm.GetState( SFollowWaypoints );
    CLAW_ASSERT( s );
    return s->GetWaypoint();
}

void Entity::ElementHit()
{
    if( m_elements )
    {
        TutorialManager::GetInstance()->OnElementHit();
        m_elements->Hit();
    }
}

int Entity::l_GetType( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( GetType() );
    return 1;
}

int Entity::l_GetBehavior( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( GetBehavior() );
    return 1;
}

int Entity::l_GetPos( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_pos[0] );
    lua.PushNumber( m_pos[1] );
    return 2;
}

int Entity::l_SetPos( lua_State* L )
{
    Claw::Lua lua( L );
    m_pos[0] = lua.CheckNumber( 1 );
    m_pos[1] = lua.CheckNumber( 2 );
    return 0;
}

int Entity::l_GetLook( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_look[0] );
    lua.PushNumber( m_look[1] );
    return 2;
}

int Entity::l_SetLook( lua_State* L )
{
    Claw::Lua lua( L );
    m_look[0] = lua.CheckNumber( 1 );
    m_look[1] = lua.CheckNumber( 2 );
    m_look.Normalize();
    return 0;
}

int Entity::l_GetTarget( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_target[0] );
    lua.PushNumber( m_target[1] );
    return 2;
}

int Entity::l_SetTarget( lua_State* L )
{
    Claw::Lua lua( L );
    m_target[0] = lua.CheckNumber( 1 );
    m_target[1] = lua.CheckNumber( 2 );
    m_target.Normalize();
    return 0;
}

int Entity::l_GetDir( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_dir[0] );
    lua.PushNumber( m_dir[1] );
    return 2;
}

int Entity::l_GetHitPoints( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_hitPoints );
    return 1;
}

int Entity::l_SetHitPoints( lua_State* L )
{
    Claw::Lua lua( L );
    m_hitPoints = lua.CheckNumber( 1 );
    return 1;
}

int Entity::l_GetMaxHitPoints( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_maxHitPoints );
    return 1;
}

int Entity::l_SetMaxHitPoints( lua_State* L )
{
    Claw::Lua lua( L );
    m_maxHitPoints = lua.CheckNumber( 1 );
    return 1;
}

int Entity::l_SetFrame( lua_State* L )
{
    Claw::Lua lua( L );
    m_frame = (int)lua.CheckNumber( 1 );
    return 0;
}

int Entity::l_SetClass( lua_State* L )
{
    Claw::Lua lua( L );
    m_class = lua.CheckEnum<Class>( 1 );
    return 0;
}

int Entity::l_AddWaypoint( lua_State* L )
{
    Claw::Lua lua( L );
    
    const char* str = lua.CheckCString( 1 );
    float time = lua.IsNumber( 2 ) ? (float)lua.CheckNumber( 2 ) : 0;

    CommonStates::FollowWaypoints* s = (CommonStates::FollowWaypoints*)m_sm.GetState( SFollowWaypoints );
    CLAW_ASSERT( s );
    s->AddWaypoint( GameManager::GetInstance()->GetWaypointManager()->GetWaypoint( str ), time );

    return 0;
}

int Entity::l_SetLoop( lua_State* L )
{
    Claw::Lua lua( L );
    CommonStates::FollowWaypoints* s = (CommonStates::FollowWaypoints*)m_sm.GetState( SFollowWaypoints );
    CLAW_ASSERT( s );
    s->SetLoop( lua.CheckBool( 1 ) );
    return 0;
}

bool Entity::IsInviolable( const Shot* shot ) const
{
    if( m_inviolable ) return true;

    // Saw should not hit enity multiple times in a row
    // Railgun also can cause multiple hits in a single frame
    Shot::Type shotType = shot->GetType();
    if( shotType == Shot::Vortex || shotType == Shot::Lurker ) return false;
    if( shot->GetLastHit() == this )
    {
        return true;
    }
    // FishGlop is pretty slow and would hurt entities multiple times
    if( shotType == Shot::FishGlop || shotType == Shot::SowerSpit )
    {
        if( m_glopTimer > 0 )
            return true;
    }
    return false;
}

bool Entity::SwitchAnimSet( int idx )
{
    if( m_currentAnim != idx )
    {
        m_currentAnim = idx;
        m_frame = 0;
        m_frameTime = 0;
        return true;
    }
    return false;
}

const AnimationSetPtr Entity::GetAnimSet() const
{
    return m_animSet[m_currentAnim];
}

void Entity::SetAvoidanceAbility( float factor /*= 0.0f*/ )
{
    factor = Claw::MinMax( factor, 0.0f, 1.0f );
    m_avoidanceRotSpeed = factor * M_PI / 4;

    // If full obstacle avoidance is required reset other potential functors
    if( factor == 1.0f )
    {
        m_avoidance = Vectorf( 0, 0 );
        m_shotAvoidance = Vectorf( 0, 0 );
    }
}

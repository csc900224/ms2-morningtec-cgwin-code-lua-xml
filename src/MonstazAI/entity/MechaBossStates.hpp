#ifndef __INCLUDED_MONSTAZ_MECHABOSS_STATES_HPP__
#define __INCLUDED_MONSTAZ_MECHABOSS_STATES_HPP__

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/StackSM.hpp"
#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/particle/Emitter.hpp"

namespace MechaBossStates
{

    class Move : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Attack : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

    class Fly : public State
    {
    public:
        Fly();

        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        ParticleFunctorPtr m_particle;
        Emitter* m_emitter;

        float m_time;
    };

    class AttackFlamer : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        void OnExit( Entity& entity, StackSM& sm, StateName nextState );

        float m_time;
    };

    class AttackRocket : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

        float m_time;
    };

}

#endif

#include "MonstazAI/entity/BehaviorFunctors.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/TutorialManager.hpp"

#include <math.h>

static const float TARGET_PLAYER_MIN_DIST               = 35.0f;

Vectorf AvoidMapBorders::s_mapSize          ( 10.f, 10.f );

float   AvoidMapBorders::s_repulsionArea    = 20.0f;
float   AvoidMapBorders::s_repulsionForce   = 50.0f;

float   AvoidLineOfSight::s_angleLimit      = cosf( 45 * ( M_PI / 180 ) );
float   AvoidLineOfSight::s_avoidanceForce  = 100.0f;

Vectorf AvoidFriends::Process( Entity& entity, float dt )
{  
    return entity.GetAvoidance() * 0.5f;
}

Vectorf AvoidAll::Process( Entity& entity, float dt )
{
    Vectorf v;
    const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        Vectorf dist( entity.GetPos() - (*it)->GetPos() );
        v += dist;
    }
    v.Normalize();
    return v;
}

Vectorf AvoidLineOfSight::Process( Entity& entity, float dt )
{
    return GameManager::GetInstance()->AvoidLineOfSight( entity, s_angleLimit, s_avoidanceForce );
}

Vectorf AvoidMapBorders::Process( Entity& entity, float dt )
{
    Vectorf impact( 0, 0 );

    const Vectorf pos = entity.GetPos();

    if( pos.x < s_repulsionArea )
        impact.x = ( s_repulsionArea - pos.x ) / s_repulsionArea * s_repulsionForce;
    else if( pos.x > s_mapSize.x - s_repulsionArea )
        impact.x = ( s_mapSize.x - s_repulsionArea - pos.x ) / s_repulsionArea * s_repulsionForce;

    if( pos.y < s_repulsionArea )
        impact.y = ( s_repulsionArea - pos.y ) / s_repulsionArea * s_repulsionForce;
    else if( pos.y > s_mapSize.y - s_repulsionArea )
        impact.y = ( s_mapSize.y - s_repulsionArea - pos.y ) / s_repulsionArea * s_repulsionForce;

    return impact;
}

Vectorf AvoidObstacles::Process( Entity& entity, float dt )
{
    return entity.GetObstacleAvoidance();
}

Vectorf AvoidShots::Process( Entity& entity, float dt )
{
    return entity.GetShotAvoidance();
}

Vectorf InterceptPlayer::Process( Entity& entity, float dt )
{
    Entity* player = GameManager::GetInstance()->GetPlayer();
    if( !player )
        return Vectorf( 0, 0 );

    Vectorf ePos = entity.GetPos();
    Vectorf pPos = player->GetPos();

    // Acquire entity and player directions (always normalized)
    Vectorf eVel = entity.GetDir();
    Vectorf pVel = player->GetDir();

    float eSpeed = GameManager::GetInstance()->GetEntityManager()->GetData(entity.GetType()).moveSpeed;
    float pSpeed = GameManager::GetInstance()->GetEntityManager()->GetData(Entity::Player).moveSpeed;
    // Calculate velocities
    eVel = eVel * eSpeed;
    pVel = pVel * pSpeed;
    // Get relative positions and velocities
    Vectorf s( pPos - ePos );
    Vectorf v( pVel - eVel );
    float sl = s.Length();
    float vl = v.Length();
    float t = 0;

    if( (sl > 0) && (vl > 0) )
        t = sl / vl;
    else if( sl > 0 )
        t = 0;      // simply chase scenario if player is not moving
    else
        t = 1;      // move towards player direction if exactly following player - same velocities

    // Generic interception point equation:
    // iPos = pPos + pVel * t;
    // Thus normalized interception point direction would be:
    // iDir = Normalize( iPos - ePos )
    // Replacing iPos we get:
    // iDir = Normalize( (pPos - pVel * t) - ePos )
    // because pPos - ePos gives s value, we can simplify to:
    Vectorf interceptionForce( s.x + pVel.x * t, s.y + pVel.y * t );
    interceptionForce.Normalize();

    return interceptionForce;
}

Vectorf TargetAim::Process( Entity& entity, float dt )
{
    Vectorf impact( entity.GetTarget() - entity.GetPos() );
    impact.Normalize();
    return impact;
}

Vectorf TargetPlayer::Process( Entity& entity, float dt )
{
    const Entity* player = GameManager::GetInstance()->GetPlayer();
    if( player )
    {
        Vectorf dst( player->GetPos() - entity.GetPos() );
        float len = dst.Length();
        if( len < TARGET_PLAYER_MIN_DIST )
        {
            return -dst / len;
        }
        else
        {
            return dst / len;
        }
    }
    return Vectorf( 0, 0 );
}

Vectorf TargetEnemies::Process( Entity& entity, float dt )
{
    const Entity* closest = NULL;
    float dist = 1000000000;

    const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        if( (*it)->GetClass() == entity.GetClass() )
        {
            continue;
        }

        Vectorf dst( (*it)->GetPos() - entity.GetPos() );
        float len = dst.LengthSqr();
        if( len < dist )
        {
            dist = len;
            closest = *it;
        }
    }

    if( closest )
    {
        Vectorf dst( closest->GetPos() - entity.GetPos() );
        float len = dst.Length();
        if( len < TARGET_PLAYER_MIN_DIST )
        {
            return -dst / len;
        }
        else
        {
            return dst / len;
        }
    }
    return Vectorf( 0, 0 );
}

Vectorf UserControl::Process( Entity& entity, float dt )
{
    GameManager* gm = GameManager::GetInstance();
    Vectorf moveVec = gm->GetTouchControl().m_offset0;
    const Vectorf& shotVec = gm->GetTouchControl().m_offset1;

    if( shotVec.x != 0 || shotVec.y != 0 )
    {
        bool shot = gm->IsAutoaim();
        if( !shot )
        {
            float len = shotVec.Length();
            entity.SetTarget( shotVec / len );
            gm->GetEntityManager()->PlayerLookDirectionChanged();

            shot = len > 0.3;
        }
        gm->GetLua()->PushBool( shot );
        gm->GetLua()->Call( "Shot", 1, 0 );

        if ( shot )
        {
            TutorialManager::GetInstance()->OnPlayerShot();
        }
    }
    else if( gm->GetTouchControl().m_enabled )
    {
        gm->GetLua()->PushBool( false );
        gm->GetLua()->Call( "Shot", 1, 0 );
    }

    const GameManager::KeysControl& keys = gm->GetKeysControl();
    if( keys.IsActive( GameManager::KeysControl::KEY_UP ) )     moveVec.y -= 1;
    if( keys.IsActive( GameManager::KeysControl::KEY_DOWN ) )   moveVec.y += 1;
    if( keys.IsActive( GameManager::KeysControl::KEY_LEFT ) )   moveVec.x -= 1;
    if( keys.IsActive( GameManager::KeysControl::KEY_RIGHT ) )  moveVec.x += 1;

    if( moveVec.x != 0 || moveVec.y != 0 )
    {
        float len = moveVec.Length();
        if( len > 1.0f )
        {
            moveVec /= len;
        }

        TutorialManager::GetInstance()->OnPlayerMove();
    }

    return moveVec;
}

Vectorf KeepDistanceToPlayer::Process( Entity& entity, float dt )
{
    static const float TargetDist = 150;

    const Entity* player = GameManager::GetInstance()->GetPlayer();
    if( player )
    {
        Vectorf dst( player->GetPos() - entity.GetPos() );
        float len = dst.Length();
        if( len < TargetDist )
        {
            return -dst / len;
        }
        else
        {
            return dst / len;
        }
    }
    return Vectorf( 0, 0 );
}

Vectorf GoToWaypoint::Process( Entity& entity, float dt )
{
    Waypoint* w = entity.GetWaypoint();
    if( !w ) return Vectorf( 0, 0 );

    Vectorf dst( w->GetPos() - entity.GetPos() );
    float len = std::max( 0.01f, dst.Length() );
    return dst / len;
}

Vectorf KeepCloseToPlayer::Process( Entity& entity, float dt )
{
    static const float TargetDist = 100;
    static const float RelaxDist = 40;
    static const float StillDist = 5;

    const Entity* player = GameManager::GetInstance()->GetPlayer();
    if( player )
    {
        Vectorf dst( player->GetPos() - entity.GetPos() );
        float len = dst.Length();

        dst /= len;

        float fd = fabs( TargetDist - len );

        if( fd < StillDist )
        {
            return Vectorf( 0, 0 );
        }

        if( fd < RelaxDist )
        {
            dst *= fd / RelaxDist;
        }

        if( len < TargetDist )
        {
            return -dst;
        }
        else
        {
            return dst;
        }
    }
    return Vectorf( 0, 0 );
}

Vectorf FireAtEnemies::Process( Entity& entity, float dt )
{
    static const float ViewDist = 250;
    static const float MaxSpeedDist = 200;
    static const float TargetDist = 125;

    const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        if( (*it)->GetClass() == entity.GetClass() )
        {
            continue;
        }

        Vectorf dst( (*it)->GetPos() - entity.GetPos() );
        float len = dst.LengthSqr();

        if( len > ViewDist * ViewDist )
        {
            continue;
        }

        len = sqrt( len );
        dst /= len;

        bool cont = false;
        Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dst, len );
        const std::vector<Obstacle*>& v = query->GetColliders();
        for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
        {
            if( (*oit)->GetKind() != Obstacle::Holo )
            {
                cont = true;
            }
        }
        if( cont ) continue;

        if( len > MaxSpeedDist )
        {
            dst *= ( len - MaxSpeedDist ) / ( ViewDist - MaxSpeedDist );
        }

        if( len < TargetDist )
        {
            return -dst;
        }
        else
        {
            return dst;
        }
    }
    return Vectorf( 0, 0 );
}

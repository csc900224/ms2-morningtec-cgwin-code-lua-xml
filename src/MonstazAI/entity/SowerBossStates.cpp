#include "claw/math/Math.hpp"

#include "MonstazAI/entity/SowerBossStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/State.hpp"

namespace SowerBossStates
{
    static const float DISTANT_ATTACK               = 300*300;
    static const float INITIAL_ATTACK               = 250*250;
    static const float NEAR_ATTACK                  = 170*170;
    static const float TONGUE_ATTACK                = 100*100;
    static const float TONGUE_RADIUS                = 120*120;
    static const int   TOUNGE_FRAME_START           = 3;
    static const int   TOUNGE_FRAME_END             = 6;
    static const int   EGG_SPIT_FRAME               = 3;
    static const int   EGG_SPIT_SHOT_FRAMES_NUM     = 2;
    static const int   EGG_SHOT_FRAME               = 6;
    static const int   EGG_SHOT_MIN_NUM             = 3;
    static const int   EGG_SHOT_MAX_NUM             = 15;

    static const float MAX_RUN_TIME                 = 3.0f;
    static const float COOLDOWN_TIME_MIN            = 1.0f;
    static const float COOLDOWN_TIME_MAX            = 3.0f;

    static const float EGG_SHOT_PROB_INCREASE       = 2.0f;

    enum BossState 
    {
        BS_INIT = 0,
        BS_NORMAL,
        BS_DYING
    };

    StateName SelectAttack( Entity& self, Entity& target )
    {
        Vectorf dst = self.GetPos() - target.GetPos();
        float dp = DotProduct( dst, dst );
        
        if( self.m_bossState == BS_INIT )
        {
            if( dp < INITIAL_ATTACK )
            {
                // Start game with egg attack
                self.m_bossState = BS_NORMAL;
                return SSowerBossAttackEggShot;
            }
            return SSowerBossMove;
        }
        else
        {
            float lifePercent = self.GetHitPoints() / self.GetMaxHitPoints();
            if( self.m_bossState == BS_NORMAL && lifePercent < 0.3f )
                self.m_bossState = BS_DYING;

            if( dp < NEAR_ATTACK * lifePercent )
            {
                return SSowerBossRun;
            }
            else if( dp < DISTANT_ATTACK )
            {
                float prob = g_rng.GetDouble();
                return lifePercent < prob * EGG_SHOT_PROB_INCREASE ? SSowerBossAttackEggShot : SSowerBossAttackEggSpit;
            }
            return SSowerBossMove;
        }
    }

    int GetNumEggs( Entity& self, Entity& target )
    {
        float lifePercent = self.GetHitPoints() / self.GetMaxHitPoints();
        return (int)(EGG_SHOT_MIN_NUM + (1.0f-lifePercent) * (EGG_SHOT_MAX_NUM - EGG_SHOT_MIN_NUM) + 0.5f);
    }

    Entity* FinNearestOpponent( Entity& self )
    {
        Entity* nearestEnt = NULL;
        float minDist = std::numeric_limits<float>::max();

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( self.GetClass() == (*it)->GetClass() ) continue;
            
            Vectorf entDir = (*it)->GetPos() - self.GetPos();
            float entDistSqr = DotProduct(entDir, entDir);

            if( entDistSqr < minDist )
            {
                minDist = entDistSqr;
                nearestEnt = (*it);
            }
        }
        return nearestEnt;
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/15.0f );
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::Chaser );
        entity.m_speedMul = 1;
        entity.m_invulnerable = false;
        entity.SetAnimLoop( true );

        if( previousState != SCommonSpawn )
        {
            if( entity.m_bossState != BS_DYING )
                m_cooldownTime = COOLDOWN_TIME_MIN + g_rng.GetDouble() * (COOLDOWN_TIME_MAX - COOLDOWN_TIME_MIN);
            else
                m_cooldownTime = COOLDOWN_TIME_MIN;
        }
        else
        {
            m_cooldownTime = 0;
        }
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        if( m_cooldownTime <= 0 )
        {
            // Randomize opponents order
            const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
            std::vector<Entity*> opponents;
            for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
            {
                if( entity.GetClass() == (*it)->GetClass() ) continue;
                opponents.push_back( *it );
            }
            std::random_shuffle(opponents.begin(), opponents.end());

            for( std::vector<Entity*>::const_iterator it = opponents.begin(); it != opponents.end(); ++it )
            {
                StateName attackState = SelectAttack( entity, **it );
                if( attackState != SSowerBossMove )
                {
                    ((AttackState*)sm.GetState( attackState ))->m_target = *it;
                    sm.ChangeState( entity, attackState );
                    return;
                }
            }
        }
        else
        {
            m_cooldownTime -= tick;
        }
    }

    void Run::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/35.0f );
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::Chaser );
        entity.m_speedMul = 3;
        entity.m_invulnerable = false;
        entity.SetAnimLoop( true );
        m_time = MAX_RUN_TIME;

        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_SOWER_CHARGE, entity.GetPos() );
    }

    void Run::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !m_target || m_target->GetHitPoints() == 0 )
        {
            sm.ChangeState( entity, SSowerBossMove );
            return;
        }

        Vectorf dst = entity.GetPos() - m_target->GetPos();
        float dp = DotProduct( dst, dst );
        if( dp < TONGUE_ATTACK )
        {
            ((AttackState*)sm.GetState( SSowerBossAttackTongue ))->m_target = m_target;
            sm.ChangeState( entity, SSowerBossAttackTongue );
        }

        m_time -= tick;
        if( m_time <= 0 )
        {
            sm.ChangeState( entity, SSowerBossMove );
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/10.f );
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetAnimLoop( false );
        m_wasShot = false;
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            sm.ChangeState( entity, SSowerBossMove );
        }
        else if( entity.GetFrame() >= EGG_SHOT_FRAME && !m_wasShot )
        {
            m_wasShot = true;

            const int eggsToShoot = GetNumEggs( entity, *m_target );
            Claw::Lua* lua = GameManager::GetInstance()->GetLua();
            for( int i = 0; i < eggsToShoot; ++i )
            {
                Claw::Lunar<Entity>::push( *lua, &entity );
                lua->PushNumber( entity.GetLook().x * g_rng.GetDouble( 0.5f, 0.8f ) );
                lua->PushNumber( entity.GetLook().y * g_rng.GetDouble( 0.5f, 0.8f ) );
                lua->Call( "SowerEggShot", 3, 0 );
            }
        }
    }

    void AttackEggSpit::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/10.f );
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetAnimLoop( false );
        entity.SetFrame( 0 );
        m_shotsNum = 0;
    }

    void AttackEggSpit::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !m_target || m_target->GetHitPoints() == 0 )
        {
            sm.ChangeState( entity, SSowerBossMove );
            return;
        }

        if( entity.GetFrame() >= EGG_SPIT_FRAME + m_shotsNum * EGG_SPIT_SHOT_FRAMES_NUM )
        {
            ++m_shotsNum;

            // Try to target ahead player movement dir
            Vectorf playerDir = m_target->GetPos() - entity.GetPos();
            float playerDist = sqrt(DotProduct(playerDir, playerDir));
            float speed = GameManager::GetInstance()->GetEntityManager()->GetData(Entity::Player).moveSpeed;
            Vectorf change = m_target->GetDir() * speed * playerDist * 0.1f;

            const Vectorf& targetPos = m_target->GetPos() + change;
            const Vectorf& entityPos = entity.GetPos();
            Vectorf targetDir = targetPos - entityPos;
            float targetDist = DotProduct( targetDir, targetDir );

            entity.LookAt( (int)targetPos.x, (int)targetPos.y );

            targetDist = sqrt( targetDist );
            targetDir /= targetDist;

            // If obstacle appeared on shot line
            Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entityPos, targetDir, targetDist );
            const std::vector<Obstacle*>& v = query->GetColliders();
            bool shootingObstacle = false;
            for( std::vector<Obstacle*>::const_iterator it = v.begin(); it != v.end(); ++it )
            {
                if( (*it)->GetKind() == Obstacle::Regular )
                {
                    shootingObstacle = true;
                    break;
                }
            }
            if( shootingObstacle )
            {
                sm.ChangeState( entity, SSowerBossMove );
                return;
            }

            Claw::Lua* lua = GameManager::GetInstance()->GetLua();
            lua->PushNumber( 22 );
            Claw::Lunar<Entity>::push( *lua, &entity );
            lua->PushNumber( targetDir.x );
            lua->PushNumber( targetDir.y );
            lua->Call( "EntityFireWeapon", 4, 0 );
        }

        if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            ((AttackState*)sm.GetState( SSowerBossRun ))->m_target = m_target;
            sm.ChangeState( entity, SSowerBossRun );
        }
    }

    void AttackTongue::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_IDLE2 );
        entity.SetAnimLoop( false );
        m_origRadius = entity.m_entityRadius;

        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_SOWER_LICK, entity.GetPos() );
    }

    void AttackTongue::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !m_target || m_target->GetHitPoints() == 0 )
        {
            sm.ChangeState( entity, SSowerBossMove );
            return;
        }

        entity.LookAt( (int)m_target->GetPos().x, (int)m_target->GetPos().y );

        if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            entity.m_entityRadius = m_origRadius;
            sm.ChangeState( entity, SSowerBossMove );
        }
        else if( entity.GetFrame() >= TOUNGE_FRAME_END )
            entity.m_entityRadius = m_origRadius;
        else if( entity.GetFrame() >= TOUNGE_FRAME_START )
            entity.m_entityRadius = TONGUE_RADIUS / 8;
    }
}

#ifndef __INCLUDED_MONSTAZ_LOBSTER_STATES_HPP__
#define __INCLUDED_MONSTAZ_LOBSTER_STATES_HPP__

#include "MonstazAI/State.hpp"
#include "MonstazAI/entity/Entity.hpp"

#include "claw/compat/ClawTypes.hpp"

namespace LobsterStates
{
    class HitAwareState : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

        virtual void OnHit( Entity& entity, StackSM& sm, float pointsDelta ) = 0;

    private:
        float m_lastHitPoints;
    };

    class Move : public HitAwareState
    {
    public:
        Move( Entity::Behavior behavior );

        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnHit( Entity& entity, StackSM& sm, float pointsDelta );

    private:
        Entity::Behavior m_behavior;
        float m_timer;
    };

    class Hit : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_timer;
        StateName m_prevState;
    };

    class Shoot : public HitAwareState
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );
        virtual void OnHit( Entity& entity, StackSM& sm, float pointsDelta );

    private:
        float m_timer;
        int m_shotsFired;
        bool m_shoot;
    };

    class IdleBefore : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_timer;
    };

    class IdleAfter : public State
    {
    public:
        virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        float m_timer;
    };

}

#endif

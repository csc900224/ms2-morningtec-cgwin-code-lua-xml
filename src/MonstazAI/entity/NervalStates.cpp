#include "claw/math/Math.hpp"

#include "MonstazAI/entity/NervalStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/particle/ExplosionParticle.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"

namespace NervalStates
{

    static const int SECTOID_ATTACK_DISTANCE_SQR    = 30 * 30;
    static const int SECTOID_ATTACK_RELEASE_DISTANCE_SQR    = 35 * 35;
    static const int OCTOPUS_ATTACK_SFX_DELAY       = 300;
    static const int OCTOPUS_ATTACK_SFX_DELAY_RAND  = 150;
    static Claw::UInt64 s_lastHit                   = 0;

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::Chaser );
        entity.SetFrameTime( 1/30.f );
        entity.SwitchAnimSet( Entity::AS_MOVE );

        m_timer = g_rng.GetDouble() * 2 + 4;
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SNervalAttack );
                return;
            }
        }

        m_timer -= tick;
        if( m_timer < 0 )
        {
            sm.ChangeState( entity, SNervalSeek );
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/30.f );
        entity.SwitchAnimSet( g_rng.GetDouble() < 0.5f ? Entity::AS_ATTACK : Entity::AS_SPECIAL );
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        bool change = true;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_RELEASE_DISTANCE_SQR )
            {
                change = false;
                break;
            }
        }

        if( change )
        {
            sm.ChangeState( entity, SNervalMove );
            return;
        }
        else if( entity.GetFrame() >= entity.GetAnimSet()->GetFrames() - 1 )
        {
            entity.SwitchAnimSet( g_rng.GetDouble() < 0.5f ? Entity::AS_ATTACK : Entity::AS_SPECIAL );
        }

        Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
        if( s_lastHit < timeMs )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
            s_lastHit = timeMs + OCTOPUS_ATTACK_SFX_DELAY + g_rng.GetInt() % OCTOPUS_ATTACK_SFX_DELAY_RAND;
        }
    }

    Seek::Seek()
        : m_particle( new ExplosionParticleFunctor( 512, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/geiser.png@linear" ) ) )
        , m_emitter( NULL )
    {}

    void Seek::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SwitchAnimSet( Entity::AS_IDLE );

        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_ROCKET, entity.GetPos() );
        Claw::AudioChannelWPtr acw = GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_FLAMER, entity.GetPos(), &m_engineSound );
        if( Claw::AudioChannelPtr channel = acw.Lock() )
        {
            channel->SetLoop( true );
        }

        m_timer = 0;

        ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
        m_emitter = new ExplosionEmitter( m_particle, ps, entity.GetPos().x, entity.GetPos().y, 150, 150, 31536000, 20 );
        if( !ps->Add( m_emitter ) )
        {
            m_emitter = NULL;
        }
    }

    void Seek::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        if( nextState != SNervalCharge )
        {
            RemoveFX();
        }
    }

    void Seek::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_timer += tick;

        if( m_timer < 1 ) return;
        if( m_timer > 3 )
        {
            sm.ChangeState( entity, SNervalMove );
            return;
        }

        float dist = std::numeric_limits<float>::max();
        Entity* target = NULL;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( (*it)->GetClass() != Entity::Friendly ) continue;
            Vectorf dir( (*it)->GetPos() - entity.GetPos() );
            float dd = DotProduct( dir, dir );
            if( dd < dist )
            {
                float d = sqrt( dd );
                dir /= d;

                bool collide = false;
                Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, d );
                const std::vector<Obstacle*>& v = query->GetColliders();
                for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
                {
                    if( (*oit)->GetKind() == Obstacle::Regular )
                    {
                        collide = true;
                        break;
                    }
                }
                if( !collide )
                {
                    dist = dd;
                    target = *it;
                }
            }
        }

        if( target )
        {
            sm.ChangeState( entity, SNervalCharge );
            ((Charge*)sm.GetCurrentState())->m_target = target;
            ((Charge*)sm.GetCurrentState())->m_emitter = m_emitter;
            ((Charge*)sm.GetCurrentState())->m_engineSound = m_engineSound;
            m_emitter = NULL;
        }
    }

    void Seek::RemoveFX()
    {
        GameManager::GetInstance()->GetAudioManager()->Stop3D( m_engineSound );
        if( m_emitter )
        {
            GameManager::GetInstance()->GetParticleSystem()->Remove( m_emitter );
            m_emitter = NULL;
        }
    }

    Charge::Charge()
        : m_emitter( NULL )
    {}

    void Charge::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/30.f );
        entity.m_speedMul = 4;
        entity.SetBehavior( Entity::Roller );
        entity.SwitchAnimSet( Entity::AS_IDLE2 );
        m_time = 0;
    }

    void Charge::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        entity.m_speedMul = 1;
        RemoveFX();
    }

    void Charge::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_time += tick;
        if( m_time > 0.6f )
        {
            sm.ChangeState( entity, SNervalMove );
            return;
        }

        entity.LookAt( (int)m_target->GetPos().x, (int)m_target->GetPos().y );
        GameManager::GetInstance()->GetAudioManager()->UpdatePos3D( entity.GetPos(), m_engineSound );
        if( m_emitter )
        {
            m_emitter->SetPosition( entity.GetPos().x - entity.GetDir().x * entity.m_speedMul * 3 , entity.GetPos().y - entity.GetDir().y * entity.m_speedMul * 3 );
        }

        Vectorf dir = m_target->GetPos() - entity.GetPos();
        if( DotProduct( dir, dir ) < 25*25 )
        {
            sm.ChangeState( entity, SNervalAttack );
        }

        dir.Normalize();
        Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, 25 );
        const std::vector<Obstacle*>& v = query->GetColliders();
        for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
        {
            if( (*oit)->GetKind() == Obstacle::Regular )
            {
                sm.ChangeState( entity, SNervalMove );
                return;
            }
        }
    }

    void Charge::RemoveFX()
    {
        GameManager::GetInstance()->GetAudioManager()->Stop3D( m_engineSound );
        if( m_emitter )
        {
            GameManager::GetInstance()->GetParticleSystem()->Remove( m_emitter );
            m_emitter = NULL;
        }
    }
}

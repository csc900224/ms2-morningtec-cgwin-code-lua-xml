#include "claw/math/Math.hpp"

#include "MonstazAI/entity/OctobrainBossStates.hpp"
#include "MonstazAI/entity/effect/EffectTeleport.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/Application.hpp"

#include <cmath>

namespace OctobrainBossStates
{
    static const float  TELEPORT_TIME_NORMAL        = 1.0f;
    static const float  TELEPORT_MIN_DISTANCE       = 100*100;
    static const int    TELEPORT_TRACE_LENGTH_OUT   = 150;
    static const int    TELEPORT_TRACE_LENGTH_IN    = 100;
    static const float  TELEPORT_TBLINK_TIME        = 0.02;
    static const char*  TELEPORT_SPAWN_NAME         = "tele";

    static const int    CLONE_ENTS_MIN              = 2;
    static const int    CLONE_ENTS_MAX              = 5;
    static const float  CLONE_LOW_HP_RATIO          = 0.15f;
    static const float  CLONE_MAX_HP_DORP_RATIO     = 0.1f;
    static const float  CLONE_MIN_RADIUS            = 100;
    static const float  CLONE_MAX_RADIUS            = 200;
    static const float  CLONE_FLICKER_TIME          = 0.02;
    static const float  CLONE_MIN_DIST              = 100;
    static const int    CLONE_MAX_TRIES             = 20;
    static const float  CLONE_SPAWN_DELAY           = 0.3f;

    static const float  CHARGE_TARGET_MIN_DIST      = 50*50;
    static const float  CHARGE_MAX_TIME             = 3.0f;
    static const float  CHARGE_SPEED                = 4.0f;

    static const float  SHOOT_MAX_TIME              = 6.0f;
    static const float  SHOOT_DELAY_MIN             = 0.1f;
    static const float  SHOOT_DELAY_MAX             = 0.5f;
    static const float  SHOOT_DELAY_CLONE           = 0.15f;
    static const float  SHOOT_MAX_DIST              = 300;
    static const float  SHOOT_MAX_HP_DROP_RATIO     = 0.1f;
    static const int    SHOOT_WEAPON_ID             = 24;
    static const int    SHOOT_WEAPON_CLONE_ID       = 25;
    static const float  SHOOT_CLONE_COOLDOWN_MIN    = 0.5f;
    static const float  SHOOT_CLONE_COOLDOWN_MAX    = 2.0f;

    static const float  COOLDOWN_TIME_MIN           = 1.0f;
    static const float  COOLDOWN_TIME_MAX           = 3.0f;
    static const float  COOLDOWN_MAX_HP_DROP        = 0.05f;
    static const float  COOLDOWN_CHARGE_MIN_DIST    = 200*200;
    static const float  COOLDOWN_NEAR_ATTACK_DIST   = 300*300;
    static const float  COOLDOWN_ATTACK_MAX_DIST    = 400*400;

    static const float  ATTACK_SHOOT_PROB           = 2.0f;
    static const float  ATTACK_CHARGE_PROB          = 1.5f;
    static const float  ATTACK_CLONE_PROB_BASE      = 3.0f;
    static const float  ATTACK_TELEPORT_PROB_BASE   = 2.5f;

    Entity* FinNearestOpponent( Entity& self )
    {
        Entity* nearestEnt = NULL;
        float minDist = std::numeric_limits<float>::max();

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( self.GetClass() == (*it)->GetClass() ) continue;
            
            Vectorf entDir = (*it)->GetPos() - self.GetPos();
            float entDistSqr = entDir.LengthSqr();

            if( entDistSqr < minDist )
            {
                minDist = entDistSqr;
                nearestEnt = (*it);
            }
        }
        return nearestEnt;
    }

    bool TestSpawnLocation( Entity& entity, const Vectorf& newPos )
    {
        float entRadius = sqrt( entity.m_entityRadius );

        // Check player/AIFriend colisions
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf toEnemyDir = (*it)->GetPos() - newPos;
            if( toEnemyDir.LengthSqr() <= entity.m_entityRadius * 8 )
            {
                return false;
            }
        }

        // Check obstacle colisions
        Scene::CollisionQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryCollision( newPos, entRadius );
        return query->GetColliders().empty();
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/15.0f );
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::FlyingAggressive );
        entity.SetAnimLoop( true );

        m_minHp = entity.GetHitPoints() - entity.GetMaxHitPoints() * COOLDOWN_MAX_HP_DROP;

        if( previousState == SCommonSpawn )
        {
            m_cooldownTimer = COOLDOWN_TIME_MAX;
        }
        else if( previousState == SOctobrainBossTeleport && m_rescue )
        {
            // NOOP
        }
        else
        {
            m_cooldownTimer = COOLDOWN_TIME_MIN + g_rng.GetDouble() * (COOLDOWN_TIME_MAX - COOLDOWN_TIME_MIN);
        }
        m_rescue = false;
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        if( m_cooldownTimer <= 0 )
        {
            if( !Attack( entity, sm ) )
            {
                m_cooldownTimer = COOLDOWN_TIME_MIN / 2;
            }
            m_rescue = false;
        }
        else
        {
            if( m_minHp > entity.GetHitPoints() )
            {
                sm.ChangeState( entity, SOctobrainBossTeleport );
                m_rescue = true;
            }
            m_cooldownTimer -= tick;
        }
    }

    bool Move::Attack( Entity& entity, StackSM& sm )
    {
        typedef std::pair<StateName, float> StateProb;
        typedef std::vector<StateProb> StatesProb;

        Entity* nearest = FinNearestOpponent( entity );
        if( !nearest ) return false;

        float lifeLeft = entity.GetHitPoints() / entity.GetMaxHitPoints();

        // Slect attacks allowed and set its probabilites
        StatesProb allowedStates;
        Vectorf nearestDir = nearest->GetPos() - entity.GetPos();
        if( nearestDir.LengthSqr() > COOLDOWN_ATTACK_MAX_DIST )
        {
            return false;
        }
        if( nearestDir.LengthSqr() < COOLDOWN_NEAR_ATTACK_DIST )
        {
            allowedStates.push_back( StatesProb::value_type( SOctobrainBossShoot, ATTACK_SHOOT_PROB ) );
        }
        if( nearestDir.LengthSqr() > COOLDOWN_CHARGE_MIN_DIST )
        {
            allowedStates.push_back( StatesProb::value_type( SOctobrainBossCharge, ATTACK_CHARGE_PROB ) );
        }
        allowedStates.push_back( StatesProb::value_type( SOctobrainBossTeleportPlayer, ATTACK_TELEPORT_PROB_BASE * (1-lifeLeft) ) );
        allowedStates.push_back( StatesProb::value_type( SOctobrainBossClone, ATTACK_CLONE_PROB_BASE * (1-lifeLeft) ) );

        // Normalize probabilities
        float normSum = 0;
        for( StatesProb::iterator it = allowedStates.begin(); it != allowedStates.end(); ++it )
        {
            normSum += it->second;
        }
        float prev = 0;
        for( StatesProb::iterator it = allowedStates.begin(); it != allowedStates.end(); ++it )
        {
            it->second = prev + it->second / normSum;
            prev = it->second;
        }

        // Randmize atack
        float rand = g_rng.GetDouble();
        for( StatesProb::iterator it = allowedStates.begin(); it != allowedStates.end(); ++it )
        {
            if( rand < it->second )
            {
                sm.ChangeState( entity, it->first );
                return true;
            }
        }

        // Fallback if slection failed
        sm.ChangeState( entity, allowedStates.rbegin()->first );
        return true;
    }

    void Charge::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_origSpeed = entity.m_speedMul;

        Entity* nearest = FinNearestOpponent( entity );
        if( nearest == NULL ) return;

        entity.SetFrameTime( 1/15.0f );
        entity.SwitchAnimSet( Entity::AS_IDLE3 );
        entity.SetBehavior( Entity::Roller );
        entity.SetAnimLoop( true );
        entity.m_speedMul = CHARGE_SPEED;

        if( previousState != SOctobrainBossTeleport )
        {
            m_timer = CHARGE_MAX_TIME;
        }

        SetTarget( entity, *nearest );
    }

    void Charge::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer <=  0)
        {
            sm.ChangeState( entity, SOctobrainBossMove );
        }
        else
        {
            Vectorf toTarget = entity.GetTarget() - entity.GetPos();
            if( toTarget.LengthSqr() < CHARGE_TARGET_MIN_DIST )
            {
                sm.ChangeState( entity, SOctobrainBossTeleport );
            }

            m_timer -= tick;
        }
    }

    void Charge::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        entity.m_speedMul = m_origSpeed;
    }

    void Shoot::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/15.0f );
        entity.SwitchAnimSet( Entity::AS_IDLE4 );
        entity.SetBehavior( Entity::None );
        entity.SetAnimLoop( true );

        m_target = FinNearestOpponent( entity );

        m_delayTimer = 0;

        if( previousState != SOctobrainBossTeleport )
        {
            m_timer = SHOOT_MAX_TIME;
        }

        m_minHp = entity.GetHitPoints() - entity.GetMaxHitPoints() * SHOOT_MAX_HP_DROP_RATIO;
    }

    void Shoot::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        float lifeLeft = entity.GetHitPoints() / entity.GetMaxHitPoints();
        if( m_minHp > entity.GetHitPoints() )
        {
            sm.ChangeState( entity, SOctobrainBossTeleport );
        }
        else if( !ShootingTick( entity, tick, SHOOT_WEAPON_ID, SHOOT_DELAY_MIN + lifeLeft * (SHOOT_DELAY_MAX - SHOOT_DELAY_MIN) ) || m_timer <= 0 )
        {
            sm.ChangeState( entity, SOctobrainBossMove );
        }
        m_timer -= tick;
    }

    bool Shoot::ShootingTick( Entity& entity, float tick, int weapon, float delay )
    {
        if( m_delayTimer <= 0 )
        {
            m_target = FinNearestOpponent( entity );
            if( !m_target ) return false;

            const Vectorf& targetPos = m_target->GetPos();
            const Vectorf& entityPos = entity.GetPos();
            Vectorf targetDir = targetPos - entityPos;
            float targetDist = DotProduct( targetDir, targetDir );
            entity.LookAt( (int)targetPos.x, (int)targetPos.y );

            targetDist = sqrt( targetDist );
            targetDir /= targetDist;

            // If obstacle appeared on shot line
            bool shootingObstacle = false;
            Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entityPos, targetDir, targetDist );
            const std::vector<Obstacle*>& v = query->GetColliders();
            for( std::vector<Obstacle*>::const_iterator it = v.begin(); it != v.end(); ++it )
            {
                if( (*it)->GetKind() == Obstacle::Regular )
                {
                    shootingObstacle = true;
                    break;
                }
            }

            if( shootingObstacle || targetDist > SHOOT_MAX_DIST )
            {
                return false;
            }

            Claw::Lua* lua = GameManager::GetInstance()->GetLua();
            lua->PushNumber( weapon );
            Claw::Lunar<Entity>::push( *lua, &entity );
            lua->PushNumber( targetDir.x );
            lua->PushNumber( targetDir.y );
            lua->Call( "EntityFireWeapon", 4, 0 );

            m_delayTimer = delay;
        }
        m_delayTimer -= tick;
        return true;
    }

    void Shoot::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        Claw::Lua* lua = GameManager::GetInstance()->GetLua();
        lua->PushNumber( SHOOT_WEAPON_ID );
        Claw::Lunar<Entity>::push( *lua, &entity );
        lua->Call( "EntityStopFiring", 2, 0 );
    }

    void Charge::SetTarget( Entity& entity, const Entity& target )
    {
        Vectorf dir = target.GetPos() - entity.GetPos();
        dir *= 2;
        entity.SetTarget( entity.GetPos() + dir );
    }

    void Teleport::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        float frameTime = 1/15.0f;
        entity.SetFrameTime( frameTime );
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetBehavior( Entity::None );
        entity.SetAnimLoop( false );

        m_state = TS_UNKNOWN;
        m_origShadowScale = entity.m_shadowScale;
        m_animTime = frameTime * entity.GetAnimSet()->GetFrames();
        m_timer = 0;
        m_blinkTimer = TELEPORT_TBLINK_TIME;
        m_blinker = true;
        m_prevState = previousState;

        ChangeState( entity, TS_OUT );
    }

    void Teleport::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* nearest = FinNearestOpponent( entity );
        if( nearest == NULL ) return;

        const Vectorf& nearestPos = nearest->GetPos();
        entity.LookAt( (int)nearestPos.x, (int)nearestPos.y );

        if( m_blinkTimer <= 0 )
        {
            m_blinkTimer = TELEPORT_TBLINK_TIME;
            m_blinker = !m_blinker;
        }

        if( m_state == TS_OUT )
        {
            float progress = std::min( m_timer / m_animTime, 1.0f );
            Claw::UInt8 alpha = (Claw::UInt8)((255 - progress * 255) * (int)m_blinker);

            entity.SetAlpha( alpha );
            entity.m_shadow = alpha;
            entity.m_shadowScale = (1 - progress) * m_origShadowScale;

            if( progress >= 1.0f )
            {
                ChangeState( entity, TS_TRAVEL );
            }
        }
        else if( m_state == TS_TRAVEL && m_timer > TELEPORT_TIME_NORMAL )
        {
            ChangeState( entity, TS_IN );
        }
        else if( m_state == TS_IN )
        {
            float progress = std::min( m_timer / m_animTime, 1.0f );
            Claw::UInt8 alpha = (Claw::UInt8)(progress * 255 * (int)m_blinker);

            entity.SetAlpha( alpha );
            entity.m_shadow = alpha;
            entity.m_shadowScale = progress * m_origShadowScale;

            if( progress >= 1.0f )
            {
                sm.ChangeState( entity, m_prevState );
            }
        }

        m_timer += tick;
        m_blinkTimer -= tick;
    }

    void Teleport::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        entity.SetInviolable( false );
        entity.m_shadowScale = m_origShadowScale;
        entity.SetAlpha( 255 );
        entity.m_shadow = 255 ;
        entity.SetHarmless( false );
    }

    void Teleport::ChangeState( Entity& entity, TeleportState newState )
    {
        if( newState != m_state )
        {
            switch( newState )
            {
            case TS_IN:
                ChangeLocataion( entity );
                entity.AddEffect( new EffectTeleport( &entity, Vectorf(0, -TELEPORT_TRACE_LENGTH_IN ), Vectorf(0, 0), m_animTime * 0.75f ) );
                entity.SetAlpha( 0 );
                entity.SetAnimDirection( -1 );
                entity.SetHarmless( false );
                break;

            case TS_TRAVEL:
                break;

            case TS_OUT:
                entity.AddEffect( new EffectTeleport( &entity, Vectorf(0, 0), Vectorf(0, -TELEPORT_TRACE_LENGTH_OUT), m_animTime ) );
                entity.SetAlpha( 255 );
                entity.SetAnimDirection( 1 );
                entity.SetInviolable( true );
                entity.SetHarmless( true );
                break;
            }

            m_timer = 0;
            m_state = newState;
        }
    }

    void Teleport::ChangeLocataion( Entity& entity )
    {
        const Vectorf& mapOffset =  GameManager::GetInstance()->GetMap()->GetOffset();
        const Vectori& resolution = ((MonstazApp*)MonstazApp::GetInstance())->GetResolution();
        float entRadius = sqrt( entity.m_entityRadius );
        float gameSacle = ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale();

        Vectorf newPos;
        bool validPos = false;
        do
        {
            // Rand new pos
            newPos.x = (mapOffset.x + entRadius + g_rng.GetDouble() * (resolution.x - entRadius * 2)) / gameSacle;
            newPos.y = (mapOffset.y + entRadius + g_rng.GetDouble() * (resolution.y - entRadius * 2)) / gameSacle;

            // Chcek minimal distance
            Vectorf moveDir = entity.GetPos() - newPos;
            float distSqr = moveDir.LengthSqr();
            if( distSqr > TELEPORT_MIN_DISTANCE )
            {
                // Test for colisions
                validPos = TestSpawnLocation( entity, newPos );
            }
        }
        while( !validPos );

        entity.SetPos( newPos );
    }

    TeleportPlayer::TeleportPlayer() :
        m_lastLocation( 0 )
    {}

    void TeleportPlayer::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/30.0f );
        entity.SwitchAnimSet( Entity::AS_IDLE2 );
        entity.SetBehavior( Entity::None );
        entity.SetAnimLoop( true );

        m_state = TS_UNKNOWN;
        m_timer = 0;
        m_prevState = previousState;

        GameManager::GetInstance()->StartDistortionEffect();
    }

    void TeleportPlayer::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* nearest = FinNearestOpponent( entity );
        if( nearest == NULL ) return;

        const Vectorf& nearestPos = nearest->GetPos();
        entity.LookAt( (int)nearestPos.x, (int)nearestPos.y );

        if( m_state == TS_UNKNOWN && m_timer > 1.0f )
        {
            ChangeState( entity, TS_OUT );
        }
        else if( m_state == TS_OUT && m_timer > 0.1f )
        {
            ChangeState( entity, TS_TRAVEL );
        }
        else if( m_state == TS_TRAVEL && m_timer > 0 )
        {
            ChangeState( entity, TS_IN );
        }
        else if( m_state == TS_IN && m_timer > 0.5f )
        {
            sm.ChangeState( entity, m_prevState );
        }

        m_timer += tick;
    }

    void TeleportPlayer::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        entity.SetInviolable( false );
        entity.SetHarmless( false );
    }

    void TeleportPlayer::ChangeState( Entity& entity, TeleportState newState )
    {
        if( newState != m_state )
        {
            switch( newState )
            {
            case TS_IN:
                ChangeLocataion( entity );
                entity.SetHarmless( false );
                GameManager::GetInstance()->StopFlashEffect();
                break;

            case TS_TRAVEL:
                entity.SetFrameTime( 1/15.0f );
                entity.SwitchAnimSet( Entity::AS_MOVE );
                GameManager::GetInstance()->StopDistortionEffect(true);
                break;

            case TS_OUT:
                entity.SetInviolable( true );
                entity.SetHarmless( true );
                GameManager::GetInstance()->StartFlashEffect();
                break;
            }

            m_timer = 0;
            m_state = newState;
        }
    }

    void TeleportPlayer::ChangeLocataion( Entity& entity )
    {
        typedef std::map<Claw::NarrowString, SpawnCircle> SpawnsMap;
        typedef SpawnsMap::const_iterator SpawnsMapConstIt;
        typedef std::vector<const SpawnCircle*> TelportCircles;
        typedef std::vector<Entity*> Entities;

        // Select random teleport point
        const SpawnsMap& spawns = GameManager::GetInstance()->GetMap()->GetSpawnData();

        SpawnsMapConstIt it = spawns.begin();
        SpawnsMapConstIt end = spawns.end();

        TelportCircles teleportCircles;
        for( ; it != end; ++it )
        {
            if( it->first.find( TELEPORT_SPAWN_NAME ) == 0 )
            {
                teleportCircles.push_back( &it->second );
            }
        }

        CLAW_MSG_ASSERT( !teleportCircles.empty(), "Could not find any teleport point!" );
        if( teleportCircles.empty() ) return;

        // Select next teleportlocation
        int locationIdx = 0;
        if( teleportCircles.size() > 1 )
        {
            do
            {
                locationIdx = g_rng.GetInt( 0, teleportCircles.size() - 1 );
            }
            while( locationIdx == m_lastLocation );
            m_lastLocation = locationIdx;
        }
        const SpawnCircle* circleToTeleport = teleportCircles[locationIdx];

        // Create list of entities to teleport
        Entities entsToTeleport;
        entsToTeleport.push_back( &entity );

        const Entities& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        Entities::const_iterator eIt = ents.begin();
        Entities::const_iterator eEnd = ents.end();
        for( ; eIt != eEnd; ++eIt )
        {
            if( entity.GetClass() != (*eIt)->GetClass() )
            {
                entsToTeleport.push_back( *eIt );
            }
        }

        // Move entities
        Entities::iterator ettIt = entsToTeleport.begin();
        Entities::iterator ettEnd = entsToTeleport.end();
        float angle = M_PI * 2 * g_rng.GetDouble();
        float angleStep = (M_PI * 2)/entsToTeleport.size();
        for( ; ettIt != ettEnd; ++ettIt )
        {
            float r = circleToTeleport->r;
            angle += angleStep;
            Vectorf newPos;
            newPos.x = circleToTeleport->x + std::sin(angle) * r;
            newPos.y = circleToTeleport->y + std::cos(angle) * r;

            (*ettIt)->SetPos( newPos );
        }
    }

    void Clone::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_startSpeed = entity.m_speedMul;
        m_startHp = entity.GetHitPoints();
        m_timer = 0;
        m_positions.clear();
        m_positions.push_back( entity.GetPos() );
        m_spawnTimer = CLONE_SPAWN_DELAY;

        entity.SetFrameTime( 1/10.0f );
        entity.SwitchAnimSet( Entity::AS_IDLE2 );
        entity.SetBehavior( Entity::HoundBig );
        entity.SetAnimLoop( true );
        entity.m_speedMul = 0.5f;

        GameManager::GetInstance()->StartDistortionEffect();

        CalculateSpawnPos( entity );
    }

    void Clone::SpawnTick( float tick )
    {
        // leave last element - this is master boss position
        if( m_positions.size() > 1 )
        {
            if( m_spawnTimer >= CLONE_SPAWN_DELAY )
            {
                m_spawnTimer = 0;
                SpawnClone( m_positions.back() );
                m_positions.pop_back();
            }
            else
            {
                m_spawnTimer += tick;
            }
        }
    }

    void Clone::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;
        
        bool finihsed = false;

        SpawnTick( tick );

        if( entity.GetHitPoints() < m_startHp - entity.GetMaxHitPoints() * CLONE_MAX_HP_DORP_RATIO )
        {
            finihsed = true;
        }
        else if( m_timer > 3.0f )
        {
            bool clonesAlive = false;
            const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
            for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
            {
                if( (*it)->GetType() == Entity::OctobrainBossClone )
                {
                    clonesAlive = true;
                    break;
                }
            }
            finihsed = !clonesAlive;
        }

        if( finihsed )
        {
            sm.ChangeState( entity, SOctobrainBossMove );
        }

        m_timer += tick;
    }

    void Clone::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        GameManager::GetInstance()->StopDistortionEffect();
        entity.m_speedMul = m_startSpeed;

        // Kill all reamaining clone entities
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( (*it)->GetType() == Entity::OctobrainBossClone )
            {
                (*it)->SetHitPoints( 0 );
            }
        }
    }

    void Clone::CalculateSpawnPos( Entity& entity )
    {
        float maxHp = entity.GetMaxHitPoints();
        float lowHp = maxHp * CLONE_LOW_HP_RATIO;
        float lifeLeft = std::max(0.0f, entity.GetHitPoints() - lowHp) / (entity.GetMaxHitPoints() - lowHp);
        int clonesNum = CLONE_ENTS_MIN + (1 - lifeLeft) * (CLONE_ENTS_MAX - CLONE_ENTS_MIN);
        CalculateSpawnPos( entity, clonesNum );
    }

    void Clone::CalculateSpawnPos( Entity& entity, int numClones )
    {
        if( GameManager::GetInstance()->GetPlayer() == NULL ) return;
        const Vectorf& playerPos = GameManager::GetInstance()->GetPlayer()->GetPos();

        const Vectorf toPlayerDir = playerPos -  entity.GetPos();
        float playerToEntAngle = atan2( toPlayerDir.y, toPlayerDir.x ) + M_PI;
        float playerToEntDist = std::max( toPlayerDir.Length(), CLONE_MIN_DIST );

        for( int i = 0; i < numClones; ++i )
        {
            bool validPos = false;
            Vectorf newPos;
            for( int iTry = 0; iTry < CLONE_MAX_TRIES && !validPos; ++iTry )
            {
                validPos = true;
                float angle = playerToEntAngle + g_rng.GetDouble() * M_PI - M_PI/2;

                // Modify radius slightly with each try, to help avoid infinite searches
                float radius = (1 + 0.05f * iTry) * playerToEntDist;

                Vectorf offset( cos( angle ) * radius,
                                      sin( angle ) * radius );
                newPos = playerPos + offset;

                // Check if position overlaps previously selected.
                PosVector::iterator it = m_positions.begin();
                PosVector::iterator end = m_positions.end();
                for( ; it != end && validPos; ++it )
                {
                    Vectorf dir = newPos - *it;
                    validPos = dir.LengthSqr() > entity.m_entityRadius * 8;
                }

                // Test for colisions
                validPos = validPos && TestSpawnLocation( entity, newPos );
            }

            if( validPos )
            {
                m_positions.push_back( newPos );
            }
        }
    }

    void Clone::SpawnClone( const Vectorf& pos )
    {
        Claw::Lua* lua = GameManager::GetInstance()->GetLua();
        lua->PushNumber( Entity::OctobrainBossClone );
        lua->PushNumber( pos.x );
        lua->PushNumber( pos.y );
        lua->Call( "SpawnEntityDelayed", 3, 0 );
    }

    void CloneBaseState::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_flickerTimer = 0;
        m_flickerAlpha = 0;
        m_baseAlpha = 255;

        bool postprocess = false;
        Claw::Registry::Get()->Get( "/monstaz/settings/postprocess", postprocess );
        if( postprocess )
        {
            entity.SetHologram( 1 );
            entity.SetAlpha( 128 );
        }
    }

    void CloneBaseState::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        // Backfill for no-shader devices
        if( entity.GetHologram() <= 0 )
        {
            if( m_flickerTimer <= 0 )
            {
                m_flickerAlpha = g_rng.GetInt( 100, 255 );
                m_flickerTimer = CLONE_FLICKER_TIME;
            }

            float base = m_baseAlpha / 255.0f;
            float flicker = m_flickerAlpha / 255.0f;

            entity.SetAlpha( (Claw::UInt8)(base * flicker * 255) );
            entity.m_shadow = entity.GetAlpha();
            m_flickerTimer -= tick;
        }
    }

    void CloneSapawn::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        CloneBaseState::OnEnter( entity, sm, previousState );

        float frameTime = 1/10.0f;
        m_origShadowScale = entity.m_shadowScale;
        m_animTime = frameTime * entity.GetAnimSet()->GetFrames();
        m_baseAlpha = 0;
        m_timer = 0;

        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetBehavior( Entity::None );
        entity.SetAlpha( 0 );
        entity.SetAnimDirection( -1 );
        entity.SetFrameTime( frameTime );
        entity.SetAnimLoop( false );
        entity.SetFrame( entity.GetAnimSet()->GetFrames() - 1 );
        entity.SetHarmless( true );
        entity.SetInviolable( true );
        entity.m_shadowScale = 0;

        Entity* nearest = FinNearestOpponent( entity );
        if( nearest == NULL ) return;
        const Vectorf& nearestPos = nearest->GetPos();
        entity.LookAt( (int)nearestPos.x, (int)nearestPos.y );
    }

    void CloneSapawn::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        entity.SetAnimDirection( 1 );
        entity.SetHarmless( false );
        entity.SetInviolable( false );
        entity.m_shadowScale = m_origShadowScale;
    }

    void CloneSapawn::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        float progress = std::min( m_timer / m_animTime, 1.0f );

        m_baseAlpha = (Claw::UInt8)( std::min(progress * 2 * 255, 255.0f) );
        entity.m_shadowScale = m_origShadowScale * progress;
        m_timer += tick;

        if( progress >= 1.0f )
        {
            sm.ChangeState( entity, SOctobrainBossMove );
        }
        else if( entity.GetBehavior() == Entity::None && progress >= 0.75f )
        {
            entity.SetBehavior( Entity::FlyingAggressive );
        }

        CloneBaseState::OnUpdate( entity, sm, tick );
    }

    void CloneShoot::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        Shoot::OnEnter( entity, sm, previousState );
        CloneBaseState::OnEnter( entity, sm, previousState );

        m_timer = SHOOT_MAX_TIME * g_rng.GetDouble();
    }

    void CloneShoot::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        CloneBaseState::OnUpdate( entity, sm, tick );

        if( !ShootingTick( entity, tick, SHOOT_WEAPON_CLONE_ID, SHOOT_DELAY_CLONE ) || m_timer <= 0 )
        {
            sm.ChangeState( entity, SOctobrainBossMove );
        }
        m_timer -= tick;
    }

    void CloneShoot::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        Claw::Lua* lua = GameManager::GetInstance()->GetLua();
        lua->PushNumber( SHOOT_WEAPON_CLONE_ID );
        Claw::Lunar<Entity>::push( *lua, &entity );
        lua->Call( "EntityStopFiring", 2, 0 );
    }

    void CloneMove::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        CloneBaseState::OnEnter( entity, sm, previousState );

        entity.SetFrameTime( 1/15.0f );
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::FlyingAggressive );
        entity.SetAnimLoop( true );
        entity.SetFrame( g_rng.GetInt( 0, entity.GetAnimSet()->GetFrames() - 1 ) );

        m_timer = SHOOT_CLONE_COOLDOWN_MIN + g_rng.GetDouble() * ( SHOOT_CLONE_COOLDOWN_MAX - SHOOT_CLONE_COOLDOWN_MIN );
    }

    void CloneMove::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        CloneBaseState::OnUpdate( entity, sm, tick );

        if( m_timer <= 0 )
        {
            sm.ChangeState( entity, SOctobrainBossShoot );
        }
        m_timer -= tick;
    }
}

#include "MonstazAI/entity/EnemyBehavior.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/entity/EntityManager.hpp"

void EnemyBehavior::ApplyPotential( Entity& entity, float dt, const Vectorf& vec )
{
    EntityManager* mgr = GameManager::GetInstance()->GetEntityManager();
    const EntityData& data = mgr->GetData( entity.GetType() );
    mgr->Move( &entity, vec * dt, data.rotSpeed, data.moveSpeed );

    entity.GetPos() += entity.m_forced;
    entity.m_forced = Vectorf( 0, 0 );
}

#ifndef __INCLUDED_MONSTAZ_ENTITY_BEHAVIOR_HPP__
#define __INCLUDED_MONSTAZ_ENTITY_BEHAVIOR_HPP__

#include "MonstazAI/math/Vector.hpp"
#include "claw/compat/ClawTypes.hpp"

#include <vector>

class Entity;

class EntityBehavior
{
public:
    struct PotentialFunctor
    {
        virtual ~PotentialFunctor() {}
        virtual Vectorf Process( Entity& entity, float dt ) = 0;
    };

    virtual ~EntityBehavior() { Reset(); }

    bool AddPotential( PotentialFunctor* func, float weight = 1 );
    void Reset();
    void Update( Entity& entity, float dt );

protected:
    virtual void ApplyPotential( Entity& entity, float dt, const Vectorf& vec ) {}

private:
    std::vector<std::pair<PotentialFunctor*, float> > m_potentials;
};

#endif

#ifndef __MONSTAZ__ENTITYMANAGER_HPP__
#define __MONSTAZ__ENTITYMANAGER_HPP__

#include <algorithm>
#include <map>
#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/PickupManager.hpp"
#include "MonstazAI/particle/BloodParticle.hpp"

class EntityBehavior;

struct EntityData
{
    float rotSpeed;
    float moveSpeed;
    int orbs;
    float orbsDrop;
    int xp;
    float damage;
    float hp;
    float moneyChance;
    int moneyAmount;
    int score;
    int shield;
    float shieldScale;
};


class EntityManager : public Claw::RefCounter
{
    struct PlayerMovementData
    {
        PlayerMovementData() : accum( 0 ), frame( 0 ) {}

        Vectorf pos;
        float accum;
        int frame;
    };
    struct DelayedSpawn
    {
        DelayedSpawn( const Vectorf& _pos, Entity::Type _type, Pickup::Type _item ) : pos( _pos ), type( _type ), item( _item ) {}

        Vectorf pos;
        Entity::Type type;
        Pickup::Type item;
    };
    struct DelayedSpawnGroup
    {
        DelayedSpawnGroup() { Reset(); }
        void Reset() { ds.clear(); time = 0; pos.x = 0; pos.y = 0; spawned = false; animPlayed = false; }

        std::vector<DelayedSpawn> ds;
        float time;
        Vectorf pos;
        bool spawned;
        bool animPlayed;
    };

public:
    LUA_DEFINITION( EntityManager );
    EntityManager( lua_State* L ) { CLAW_ASSERT( false ); }

    EntityManager( Claw::Lua* lua );
    ~EntityManager();

    static void InitEnum( Claw::Lua* lua );

    void Render( Claw::Surface* target, const Vectorf& offset );
    void RenderPost( Claw::Surface* target, const Vectorf& offset );
    void Update( float dt );
    void UpdateBehaviors( float dt );

    Entity* Add( float x, float y, Entity::Type type, Pickup::Type item = Pickup::NumPickups );
    std::vector<Entity*>::iterator Remove( Entity* e );
    std::vector<Entity*>::iterator Remove( std::vector<Entity*>::iterator e );
    int Count() const { return m_ents.size(); }

    Entity* FindPlayerEntity();
    std::vector<Entity*>& GetEntities() { return m_ents; }
    const EntityData& GetData( Entity::Type type ) const { return m_data[type]; }

    void BecomeUnstoppable() { m_unstoppable = true; }
    bool CheckLastHit();

    void PlayerLookDirectionChanged() { m_playerDirectionalTicks = 60; }
    void SetPlayerWeaponSlowDown( float slowDown ) { m_playerWeaponSlowDown = slowDown; }
    void SetPlayerAnimationSpeed( float speed ) { m_playerAnimSpeed[0] = speed; }

    int l_Add( lua_State* L );
    int l_AddDelayed( lua_State* L );
    int l_Remove( lua_State* L );
    int l_Count( lua_State* L );
    int l_GrimReaper( lua_State* L );
    int l_SetEntityData( lua_State* L );
    int l_SetupEntityBehaviors( lua_State* L );
    int l_PlayerLookDirectionChanged( lua_State* L );
    int l_UpdateBehaviors( lua_State* L );
    int l_SpawnTickFinished( lua_State* L );
    int l_GetCat( lua_State* L );

    void Move( Entity* entity, const Vectorf& dir, float angSpeed, float linSpeed );

    bool IsValid( const Entity* entity ) const { return std::find( m_ents.begin(), m_ents.end(), entity ) != m_ents.end(); }

    bool m_playerUnderAttack;
    float m_attackModifier;

private:
    void SetupEntityBehaviors();

    void MoveNormal( Entity* entity, const Vectorf& dir, float angSpeed, float linSpeed );
    void MoveFloater( Entity* entity, const Vectorf& dir, float angSpeed, float linSpeed );
    void MovePlayer( Entity* entity, const Vectorf& dir, float linSpeed );
    void MoveAIFriend( Entity* entity, const Vectorf& dir, float linSpeed );

    std::vector<Entity*> m_ents;
    Claw::SurfacePtr m_shadow;
    Claw::SurfacePtr m_spawnGfx;
    float m_scale;
    std::map<Entity*, PlayerMovementData> m_pmd;
    int m_playerDirectionalTicks;
    float m_playerWeaponSlowDown;
    float m_playerAnimSpeed[2];
    ParticleFunctorPtr m_bloodSplat;

    bool m_unstoppable;
    float m_entityLastHit;

    Claw::SurfacePtr m_spawnAnim;

    EntityData m_data[Entity::TypesCount];
    EntityBehavior* m_behaviors[Entity::BehaviorsCount];

    DelayedSpawnGroup m_delayedSpawnGroup;
    std::vector<DelayedSpawnGroup> m_dsgList;
};

typedef Claw::SmartPtr<EntityManager> EntityManagerPtr;

#endif

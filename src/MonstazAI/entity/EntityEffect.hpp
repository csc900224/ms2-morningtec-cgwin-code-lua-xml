#ifndef __MONSTAZ_ENTITYEFFECT_HPP__
#define __MONSTAZ_ENTITYEFFECT_HPP__

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

class Entity;

class EntityEffect : public Claw::RefCounter
{
public:
    EntityEffect( Entity* e );

    virtual bool Update( float dt ) = 0;
    virtual void RenderBefore( Claw::Surface* target, int x, int y ) {}
    virtual void RenderAfter( Claw::Surface* target, int x, int y ) {}
    virtual void RenderAfterEverything( Claw::Surface* target, int x, int y ) {}

protected:
    Entity* m_entity;
};

typedef Claw::SmartPtr<EntityEffect> EntityEffectPtr;

#endif

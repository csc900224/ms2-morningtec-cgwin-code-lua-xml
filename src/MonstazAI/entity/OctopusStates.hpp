#ifndef __INCLUDED_MONSTAZ_OCTOPUS_STATES_HPP__
#define __INCLUDED_MONSTAZ_OCTOPUS_STATES_HPP__

#include "MonstazAI/State.hpp"
#include "MonstazAI/entity/Entity.hpp"

#include "claw/compat/ClawTypes.hpp"

namespace OctopusStates
{

    class Move : public State
    {
    public:
        Move( Entity::Behavior behavior );

        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );

    private:
        Entity::Behavior m_behavior;
    };

    class Attack : public State
    {
    public:
        virtual void    OnEnter( Entity& entity, StackSM& sm, StateName previousState );
        virtual void    OnUpdate( Entity& entity, StackSM& sm, float tick );
    };

}

#endif

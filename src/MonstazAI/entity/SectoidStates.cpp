#include "claw/math/Math.hpp"

#include "MonstazAI/entity/SectoidStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/State.hpp"

namespace SectoidStates
{

    static const int SECTOID_ATTACK_DISTANCE_SQR    = 40 * 40;
    static const int OCTOPUS_ATTACK_SFX_DELAY       = 300;
    static const int OCTOPUS_ATTACK_SFX_DELAY_RAND  = 150;
    static Claw::UInt64 s_lastHit                   = 0;

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SSectoidAttack );
                return;
            }
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/15.f );

        if( g_rng.GetDouble() < 0.5f )
        {
            entity.SwitchAnimSet( Entity::AS_ATTACK );
        }
        else
        {
            entity.SwitchAnimSet( Entity::AS_SPECIAL );
        }
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        bool change = true;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < SECTOID_ATTACK_DISTANCE_SQR )
            {
                change = false;
                break;
            }
        }

        if( change )
        {
            sm.ChangeState( entity, SSectoidMove );
            return;
        }

        Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
        if( s_lastHit < timeMs )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
            s_lastHit = timeMs + OCTOPUS_ATTACK_SFX_DELAY + g_rng.GetInt() % OCTOPUS_ATTACK_SFX_DELAY_RAND;
        }
    }

    void MoveShooting::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_time = g_rng.GetDouble() + 1;

        entity.SetBehavior( Entity::HoundBig );

        if( g_rng.GetDouble() < 0.5f )
        {
            entity.SwitchAnimSet( Entity::AS_MOVE );
        }
        else
        {
            entity.SwitchAnimSet( Entity::AS_SPECIAL );
        }
    }

    void MoveShooting::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        m_time -= tick;

        if( m_time <= 0 )
        {
            float dist = std::numeric_limits<float>::max();
            Entity* target = NULL;
            const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
            for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
            {
                if( (*it)->GetClass() != Entity::Friendly ) continue;
                Vectorf dir( (*it)->GetPos() - entity.GetPos() );
                float dd = DotProduct( dir, dir );
                if( dd < dist )
                {
                    float d = sqrt( dd );
                    dir /= d;

                    bool collide = false;
                    Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, d );
                    const std::vector<Obstacle*>& v = query->GetColliders();
                    for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
                    {
                        if( (*oit)->GetKind() == Obstacle::Regular )
                        {
                            collide = true;
                            break;
                        }
                    }
                    if( !collide )
                    {
                        dist = dd;
                        target = *it;
                    }
                }
            }

            if( target )
            {
                sm.ChangeState( entity, SSectoidShootingTargetting );
                ((Targetting*)sm.GetCurrentState())->m_target = target;
            }
        }
    }

    void AttackShooting::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        m_time = 16/30.f;
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void AttackShooting::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_target )
        {
            if( !GameManager::GetInstance()->GetEntityManager()->IsValid( m_target ) )
            {
                sm.ChangeState( entity, SSectoidShootingMove );
                return;
            }

            Claw::Lua* lua = GameManager::GetInstance()->GetLua();

            Vectorf targetDir = m_target->GetPos() - entity.GetPos();
            targetDir.Normalize();

            lua->PushNumber( 21 );   // Weapon type
            Claw::Lunar<Entity>::push( *lua, &entity );  // shot owner
            lua->PushNumber( targetDir.x );
            lua->PushNumber( targetDir.y );
            lua->Call( "EntityFireWeapon", 4, 0 );

            m_target = NULL;
        }

        m_time -= tick;

        if( m_time <= 0 )
        {
            sm.ChangeState( entity, SSectoidShootingMove );
        }
    }

    void Targetting::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SwitchAnimSet( Entity::AS_IDLE );

        m_time = g_rng.GetDouble() + 1;
    }

    void Targetting::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetEntityManager()->IsValid( m_target ) )
        {
            sm.ChangeState( entity, SSectoidShootingMove );
            return;
        }

        entity.LookAt( (int)m_target->GetPos().x, (int)m_target->GetPos().y );

        m_time -= tick;
        if( m_time <= 0 )
        {
            Vectorf dir( m_target->GetPos() - entity.GetPos() );
            float d = dir.Normalize();
            Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, d );
            const std::vector<Obstacle*>& v = query->GetColliders();
            for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
            {
                if( (*oit)->GetKind() == Obstacle::Regular )
                {
                    sm.ChangeState( entity, SSectoidShootingMove );
                    return;
                }
            }

            sm.ChangeState( entity, SSectoidShootingAttack );
            ((AttackShooting*)sm.GetCurrentState())->m_target = m_target;
        }
    }

}

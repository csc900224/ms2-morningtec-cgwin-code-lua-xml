#include "MonstazAI/entity/EntityBehavior.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/entity/EntityManager.hpp"

#include <algorithm>

bool EntityBehavior::AddPotential( EntityBehavior::PotentialFunctor* func, float weight )
{
    std::vector<std::pair<PotentialFunctor*, float> >::const_iterator it = std::find( m_potentials.begin(), m_potentials.end(), std::make_pair( func, weight ) );
    if( it != m_potentials.end() )
        return false;
    m_potentials.push_back( std::make_pair( func, weight ) );
    return true;
}

void EntityBehavior::Update( Entity& entity, float dt )
{
    Vectorf v( 0, 0 );
    for( std::vector<std::pair<PotentialFunctor*, float> >::iterator it = m_potentials.begin(); it != m_potentials.end(); ++it )
    {
        v += it->first->Process( entity, dt ) * it->second;
    }

    ApplyPotential( entity, dt, v );
}

void EntityBehavior::Reset()
{
    for( std::vector<std::pair<PotentialFunctor*, float> >::iterator it = m_potentials.begin(); it != m_potentials.end(); ++it )
    {
        delete it->first;
    }
    m_potentials.clear();
}

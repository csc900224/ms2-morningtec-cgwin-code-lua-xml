#ifndef __MONSTAZ_EFFECTHIGHLIGHT_HPP__
#define __MONSTAZ_EFFECTHIGHLIGHT_HPP__

#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/entity/EntityEffect.hpp"

class EffectHighlight : public EntityEffect
{
public:
    EffectHighlight( Entity* e, float scale, bool arrow );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

private:
    GfxAssetPtr m_ground[12];
    Claw::SurfacePtr m_arrow;

    float m_frameTime;
    int m_frame;
    float m_sinTime;
    float m_scale;
    bool m_showArrow;
};

#endif

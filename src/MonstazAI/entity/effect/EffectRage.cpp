#include "MonstazAI/entity/effect/EffectRage.hpp"

EffectRage::EffectRage( Entity* e, Claw::Surface* anim, Claw::Surface* anim2, float scale )
    : EntityEffect( e )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_anim2( (Claw::AnimatedSurface*)anim2 )
    , m_scale( scale )
    , m_active( false )
{
}

bool EffectRage::Update( float dt )
{
    if( m_active )
    {
        m_anim->Update( dt );
        m_anim2->Update( dt );
    }
    return true;
}

void EffectRage::RenderBefore( Claw::Surface* target, int x, int y )
{
    if( m_active )
    {
        target->Blit( x, y, m_anim2 );
    }
}

void EffectRage::RenderAfter( Claw::Surface* target, int x, int y )
{
    if( m_active )
    {
        target->Blit( x, y - 30 * m_scale, m_anim );
    }
}

void EffectRage::SetActive( bool active )
{
    if( active != m_active )
    {
        m_active = active;

        if( active )
        {
            m_anim->SetFrame( 0 );
        }
    }
}

#ifndef __MONSTAZ_EFFECTSTUN_HPP__
#define __MONSTAZ_EFFECTSTUN_HPP__

#include "claw/graphics/AnimatedSurface.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectStun : public EntityEffect
{
public:
    EffectStun( Entity* e, Claw::Surface* anim, float scale );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

private:
    Claw::AnimatedSurfacePtr m_anim;
    float m_scale;
    float m_time;
};

#endif

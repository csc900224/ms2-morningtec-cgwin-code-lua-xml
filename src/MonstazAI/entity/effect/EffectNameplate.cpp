#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/entity/effect/EffectNameplate.hpp"

EffectNameplate::EffectNameplate( Entity* e, float scale, const Claw::NarrowString& name )
    : EntityEffect( e )
    , m_scale( scale )
    , m_font( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_normal.xml@linear" ) )
{
    m_text.Reset( new Claw::ScreenText( m_font, Claw::String( name ) ) );
}

bool EffectNameplate::Update( float dt )
{
    return true;
}

void EffectNameplate::RenderAfterEverything( Claw::Surface* target, int x, int y )
{
    m_font->GetSurface()->SetAlpha( 255 );
    m_text->Draw( target, x - m_text->GetWidth() / 2, y - 50 * m_scale );
}

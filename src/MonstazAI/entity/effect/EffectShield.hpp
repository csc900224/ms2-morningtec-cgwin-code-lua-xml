#ifndef __MONSTAZ_EFFECTSHIELD_HPP__
#define __MONSTAZ_EFFECTSHIELD_HPP__

#include "claw/graphics/AnimatedSurface.hpp"
#include "claw/sound/mixer/AudioChannel.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectShield : public EntityEffect
{
public:
    EffectShield( Entity* e, Claw::Surface* anim, float scale );

    bool Update( float dt );
    void RenderAfter( Claw::Surface* target, int x, int y );

    void SetActive( bool active );
    void SetLocalScale( float scale ) { m_localScale = scale; }

private:
    Claw::AudioChannelWPtr m_acw;
    Claw::AnimatedSurfacePtr m_anim;
    float m_scale;

    bool m_active;
    float m_time;
    bool m_decay;
    float m_localScale;
};

#endif

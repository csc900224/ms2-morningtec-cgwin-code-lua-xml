#include <stdio.h>

#include "MonstazAI/entity/effect/EffectHighlight.hpp"

EffectHighlight::EffectHighlight( Entity* e, float scale, bool arrow )
    : EntityEffect( e )
    , m_arrow( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/arrow_blue.png@linear" ) )
    , m_frameTime( 0 )
    , m_frame( 0 )
    , m_sinTime( 0 )
    , m_scale( scale )
    , m_showArrow( arrow )
{
    for( int i=0; i<12; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/items/glow/guns_glow-%02i.png.pivot@linear", i+1 );
        m_ground[i].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
    }
}

bool EffectHighlight::Update( float dt )
{
    m_frameTime += dt;
    while( m_frameTime > 1/30.f )
    {
        m_frameTime -= 1/30.f;
        m_frame = ( m_frame + 1 ) % 12;
    }

    m_sinTime += dt * 4;
    while( m_sinTime >= 2*M_PI )
    {
        m_sinTime -= 2*M_PI;
    }

    return true;
}

void EffectHighlight::RenderBefore( Claw::Surface* target, int x, int y )
{
    m_ground[m_frame]->Blit( target, x, y );
}

void EffectHighlight::RenderAfter( Claw::Surface* target, int x, int y )
{
    if( m_showArrow )
    {
        target->Blit( x - m_arrow->GetWidth() / 2, y - m_arrow->GetHeight() - ( 50 + 20 * sin( m_sinTime ) ) * m_scale, m_arrow );
    }
}

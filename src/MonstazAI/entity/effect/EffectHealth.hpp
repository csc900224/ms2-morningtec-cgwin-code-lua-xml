#ifndef __MONSTAZ_EFFECTHEALTH_HPP__
#define __MONSTAZ_EFFECTHEALTH_HPP__

#include <vector>

#include "claw/graphics/AnimatedSurface.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectHealth : public EntityEffect
{
    struct Particle
    {
        Particle( float _x, float _y, float _t, int _id ) : x( _x ), y( _y ), t( _t ), id( _id ) {}

        float x;
        float y;
        float t;
        int id;
    };

public:
    EffectHealth( Entity* e, Claw::Surface* anim, Claw::Surface* glow, Claw::SurfacePtr* particles, float scale, float maxhp, float gain );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

private:
    Claw::AnimatedSurfacePtr m_anim;
    Claw::SurfacePtr m_glow;
    Claw::SurfacePtr* m_particles;
    int m_frame;
    float m_time;
    float m_scale;
    float m_glowTime;
    float m_particleTime;
    float m_maxhp;
    float m_hp;

    std::vector<Particle> m_list;
};

#endif

#include "MonstazAI/entity/effect/EffectCash.hpp"

EffectCash::EffectCash( Entity* e, Claw::Surface* anim, float scale )
    : EntityEffect( e )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_frame( 0 )
    , m_time( m_anim->GetTime( 0 ) )
    , m_scale( scale )
{
}

bool EffectCash::Update( float dt )
{
    m_time -= dt;

    while( m_time < 0 )
    {
        m_anim->SetFrame( m_frame );
        if( !m_anim->NextFrame() )
        {
            return false;
        }
        else
        {
            m_frame = m_anim->GetFrame();
            m_time += m_anim->GetTime( m_frame );
        }
    }

    return true;
}

void EffectCash::RenderBefore( Claw::Surface* target, int x, int y )
{
}

void EffectCash::RenderAfter( Claw::Surface* target, int x, int y )
{
    m_anim->SetFrame( m_frame );
    target->Blit( x, y - 20 * m_scale, m_anim );
}

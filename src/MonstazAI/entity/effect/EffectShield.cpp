#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/entity/effect/EffectShield.hpp"
#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/Shop.hpp"

EffectShield::EffectShield( Entity* e, Claw::Surface* anim, float scale )
    : EntityEffect( e )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_scale( scale )
    , m_active( false )
    , m_time( 0 )
    , m_decay( false )
    , m_localScale( 1 )
{
}

bool EffectShield::Update( float dt )
{
    m_time += dt;

    if( m_active || m_decay )
    {
        m_anim->Update( dt );
    }
    if( m_decay && m_time > 1 )
    {
        m_decay = false;
    }
    return true;
}

void EffectShield::RenderAfter( Claw::Surface* target, int x, int y )
{
    if( m_active )
    {
        m_anim->SetAlpha( 255 );
    }
    else if( m_decay )
    {
        m_anim->SetAlpha( 255 * std::min( 1.0f, float( cos( m_time * M_PI / 2 ) - sin( m_time * 50 ) * 0.08f ) ) );
    }
    if( m_active || m_decay )
    {
        if( m_localScale == 1 )
        {
            Claw::TriangleEngine::Blit( target, m_anim, x, y, 0, m_localScale, Claw::Vectorf( m_anim->GetWidth() / 2, m_anim->GetHeight() * 9 / 10 ) - Claw::Vectorf( 0, 10 * m_scale * m_localScale ) );
        }
        else
        {
            Claw::TriangleEngine::Blit( target, m_anim, x, y, 0, m_localScale, Claw::Vectorf( m_anim->GetWidth() / 2, m_anim->GetHeight() ) - Claw::Vectorf( 0, 10 * m_scale * m_localScale ) );
        }
    }
}

void EffectShield::SetActive( bool active )
{
    if( active != m_active )
    {
        m_active = active;

        int id = Shop::GetInstance()->CheckOwnership( Shop::Items::Shield ) % 4;

        if( active )
        {
            m_anim->SetFrame( 0 );
            m_acw = AudioManager::GetInstance()->PlayLooped( AudioSfx( SFX_SHIELD1 + id ) );
        }
        else
        {
            AudioManager::GetInstance()->StopLooped( m_acw );
            m_decay = true;
            m_time = 0;
        }
    }
}

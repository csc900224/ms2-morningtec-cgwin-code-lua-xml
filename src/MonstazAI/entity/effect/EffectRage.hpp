#ifndef __MONSTAZ_EFFECTRAGE_HPP__
#define __MONSTAZ_EFFECTRAGE_HPP__

#include "claw/graphics/AnimatedSurface.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectRage : public EntityEffect
{
public:
    EffectRage( Entity* e, Claw::Surface* anim, Claw::Surface* anim2, float scale );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

    void SetActive( bool active );

private:
    Claw::AnimatedSurfacePtr m_anim;
    Claw::AnimatedSurfacePtr m_anim2;
    float m_scale;

    bool m_active;
};

#endif

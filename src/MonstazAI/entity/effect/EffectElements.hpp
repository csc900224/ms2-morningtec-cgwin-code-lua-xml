#ifndef __MONSTAZ_EFFECTELEMENTS_HPP__
#define __MONSTAZ_EFFECTELEMENTS_HPP__

#include "MonstazAI/entity/EntityEffect.hpp"
#include "MonstazAI/GfxAsset.hpp"

class EffectElements : public EntityEffect
{
public:
    EffectElements( Entity* e, GfxAsset* gfx, float scale );

    bool Update( float dt );
    void RenderAfter( Claw::Surface* target, int x, int y );

    void Hit() { m_alpha = 1; }

private:
    GfxAssetPtr m_gfx;
    float m_scale;
    float m_alpha;
};

#endif

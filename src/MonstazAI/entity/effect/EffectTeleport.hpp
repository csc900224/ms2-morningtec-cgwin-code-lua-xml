#ifndef __MONSTAZ_EFFECTTELEPORT_HPP__
#define __MONSTAZ_EFFECTTELEPORT_HPP__

#include "MonstazAI/entity/EntityEffect.hpp"
#include "MonstazAI/particle/GlowParticle.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"
#include "MonstazAI/particle/ParticleSystem.hpp"

class EffectTeleport : public EntityEffect
{
public:
    EffectTeleport( Entity* e, const Vectorf& startPoint, const Vectorf& endPoint, float duration );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

private:
    ParticleSystemPtr m_ps;
    ParticleFunctorPtr m_particle;
    Emitter* m_emitter;

    Vectorf m_start;
    Vectorf m_end;

    const float m_duration;
    float m_timer;
};

#endif

#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/entity/effect/EffectBoost.hpp"

EffectBoost::EffectBoost( Entity* e, Claw::Surface* anim, Claw::Surface* loop, float time, float scale, float localScale )
    : EntityEffect( e )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_loop( (Claw::AnimatedSurface*)loop )
    , m_frame( 0 )
    , m_time( m_anim->GetTime( 0 ) )
    , m_loopFrame( 0 )
    , m_loopTime( m_loop->GetTime( 0 ) )
    , m_life( time )
    , m_scale( scale )
    , m_localScale( localScale )
{
}

bool EffectBoost::Update( float dt )
{
    if( m_anim )
    {
        m_time -= dt;

        while( m_time < 0 )
        {
            m_anim->SetFrame( m_frame );
            if( !m_anim->NextFrame() )
            {
                m_anim.Release();
                m_time = 0;
            }
            else
            {
                m_frame = m_anim->GetFrame();
                m_time += m_anim->GetTime( m_frame );
            }
        }
    }

    m_loopTime -= dt;

    while( m_loopTime < 0 )
    {
        m_loop->SetFrame( m_loopFrame );
        if( m_loop->NextFrame() )
        {
            m_loopFrame = m_loop->GetFrame();
            m_loopTime += m_loop->GetTime( m_loopFrame );
        }
    }

    m_life -= dt;

    return m_life > 0;
}

void EffectBoost::RenderBefore( Claw::Surface* target, int x, int y )
{
}

void EffectBoost::RenderAfter( Claw::Surface* target, int x, int y )
{
    if( m_life < 0.25f )
    {
        m_loop->SetAlpha( m_life * 4 * 255 );
    }
    else
    {
        m_loop->SetAlpha( 255 );
    }
    m_loop->SetFrame( m_loopFrame );
    Claw::TriangleEngine::Blit( target, m_loop, x, y, 0, m_localScale, Claw::Vectorf() );

    if( m_anim )
    {
        m_anim->SetFrame( m_frame );
        Claw::TriangleEngine::Blit( target, m_anim, x, y - 20 * m_scale * m_localScale, 0, m_localScale, Claw::Vectorf() );
    }
}

#ifndef __MONSTAZ_EFFECTBOOST_HPP__
#define __MONSTAZ_EFFECTBOOST_HPP__

#include "claw/graphics/AnimatedSurface.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectBoost : public EntityEffect
{
public:
    EffectBoost( Entity* e, Claw::Surface* anim, Claw::Surface* loop, float time, float scale, float localScale );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

private:
    Claw::AnimatedSurfacePtr m_anim;
    Claw::AnimatedSurfacePtr m_loop;
    int m_frame;
    float m_time;
    float m_scale;
    int m_loopFrame;
    float m_loopTime;
    float m_life;
    float m_localScale;
};

#endif

#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/entity/effect/EffectHealth.hpp"
#include "MonstazAI/RNG.hpp"

EffectHealth::EffectHealth( Entity* e, Claw::Surface* anim, Claw::Surface* glow, Claw::SurfacePtr* particles, float scale, float maxhp, float gain )
    : EntityEffect( e )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_glow( glow )
    , m_particles( particles )
    , m_frame( 0 )
    , m_time( m_anim->GetTime( 0 ) )
    , m_scale( scale )
    , m_glowTime( 0 )
    , m_particleTime( 0.1f + 0.05f * g_rng.GetDouble() )
    , m_maxhp( maxhp )
    , m_hp( maxhp * gain / 2 )
{
}

bool EffectHealth::Update( float dt )
{
    if( m_anim )
    {
        m_time -= dt;

        while( m_time < 0 )
        {
            m_anim->SetFrame( m_frame );
            if( !m_anim->NextFrame() )
            {
                m_anim.Release();
                m_time = 0;
            }
            else
            {
                m_frame = m_anim->GetFrame();
                m_time += m_anim->GetTime( m_frame );
            }
        }
    }

    m_glowTime += dt;

    if( m_glowTime < 2 )
    {
        m_particleTime -= dt;
        while( m_particleTime < 0 )
        {
            m_particleTime += 0.1f + 0.05f * g_rng.GetDouble();
            m_list.push_back( Particle( 0, -30 - 20 * g_rng.GetDouble(), 0.25f, g_rng.GetInt() % 3 ) );
        }

        float hp = m_entity->GetHitPoints();
        m_entity->SetHitPoints( std::min( m_maxhp, hp + m_hp * dt ) );
    }

    std::vector<Particle>::iterator it = m_list.begin();
    while( it != m_list.end() )
    {
        it->x -= dt * 120;
        it->t -= dt;
        if( it->t < 0 )
        {
            it = m_list.erase( it );
        }
        else
        {
            ++it;
        }
    }

    return m_glowTime < 2.25f;
}

void EffectHealth::RenderBefore( Claw::Surface* target, int x, int y )
{
    if( m_glowTime < 0.25f )
    {
        m_glow->SetAlpha( m_glowTime * 255 * 4 );
    }
    else if( m_glowTime > 1.85f )
    {
        m_glow->SetAlpha( ( 2.1f - m_glowTime ) * 255 * 4 );
    }
    else
    {
        m_glow->SetAlpha( 255 );
    }
    Claw::TriangleEngine::Blit( target, m_glow, x , y - 9 * m_scale,
        0, 0.9f + sin( m_glowTime * 20 ) * 0.1f, Vectorf( m_glow->GetSize()/2 ) );
}

void EffectHealth::RenderAfter( Claw::Surface* target, int x, int y )
{
    if( m_anim )
    {
        m_anim->SetFrame( m_frame );
        target->Blit( x, y - 20 * m_scale, m_anim );
    }

    for( std::vector<Particle>::iterator it = m_list.begin(); it != m_list.end(); ++it )
    {
        m_particles[it->id]->SetAlpha( sin( it->t * 4 * M_PI ) * 255 );
        target->Blit( x + ( it->x * m_scale ), y + ( it->y * m_scale ), m_particles[it->id] );
    }
}

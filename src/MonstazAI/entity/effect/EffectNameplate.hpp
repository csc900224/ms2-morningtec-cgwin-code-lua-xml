#ifndef __MONSTAZ_EFFECTNAMEPLATE_HPP__
#define __MONSTAZ_EFFECTNAMEPLATE_HPP__

#include "claw/base/String.hpp"
#include "claw/graphics/ScreenText.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectNameplate : public EntityEffect
{
public:
    EffectNameplate( Entity* e, float scale, const Claw::NarrowString& name );

    bool Update( float dt );
    void RenderAfterEverything( Claw::Surface* target, int x, int y );

private:
    float m_scale;
    Claw::ScreenTextPtr m_text;
    Claw::FontExPtr m_font;
};

#endif

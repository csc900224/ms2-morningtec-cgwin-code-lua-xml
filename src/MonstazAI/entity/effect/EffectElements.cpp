#include "MonstazAI/entity/effect/EffectElements.hpp"

EffectElements::EffectElements( Entity* e, GfxAsset* gfx, float scale )
    : EntityEffect( e )
    , m_gfx( gfx )
    , m_scale( scale )
    , m_alpha( 0 )
{
}

bool EffectElements::Update( float dt )
{
    if( m_alpha > 0 )
    {
        m_alpha -= dt;
    }
    return true;
}

void EffectElements::RenderAfter( Claw::Surface* target, int x, int y )
{
    if( m_alpha > 0 )
    {
        m_gfx->GetSurface()->SetAlpha( m_alpha * 255 );
        m_gfx->Blit( target, x, y, m_scale );
    }
}

#include "MonstazAI/entity/effect/EffectStun.hpp"

EffectStun::EffectStun( Entity* e, Claw::Surface* anim, float scale )
    : EntityEffect( e )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_scale( scale )
    , m_time( 2 )
{
}

bool EffectStun::Update( float dt )
{
    m_time -= dt;
    return m_time > 0;
}

void EffectStun::RenderBefore( Claw::Surface* target, int x, int y )
{
}

void EffectStun::RenderAfter( Claw::Surface* target, int x, int y )
{
    target->Blit( x, y - 30 * m_scale, m_anim );
}

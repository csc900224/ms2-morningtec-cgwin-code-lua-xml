#ifndef __MONSTAZ_EFFECTCASH_HPP__
#define __MONSTAZ_EFFECTCASH_HPP__

#include "claw/graphics/AnimatedSurface.hpp"

#include "MonstazAI/entity/EntityEffect.hpp"

class EffectCash : public EntityEffect
{
public:
    EffectCash( Entity* e, Claw::Surface* anim, float scale );

    bool Update( float dt );
    void RenderBefore( Claw::Surface* target, int x, int y );
    void RenderAfter( Claw::Surface* target, int x, int y );

private:
    Claw::AnimatedSurfacePtr m_anim;
    int m_frame;
    float m_time;
    float m_scale;
};

#endif

#include "MonstazAI/entity/effect/EffectTeleport.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/Application.hpp"
#include "claw/base/AssetDict.hpp"

EffectTeleport::EffectTeleport( Entity* e, const Vectorf& startPoint, const Vectorf& endPoint, float duration )
    : EntityEffect( e )
    , m_start( startPoint )
    , m_end( endPoint )
    , m_timer( 0 )
    , m_duration( duration )
    , m_ps( new ParticleSystem(true) )
    , m_particle( new GlowParticleFunctor(512, Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/flare.png@linear" )) )
{
    m_emitter = new ExplosionEmitter( m_particle, m_ps, 0, 0, 100, 100, 31536000, 200 );
    m_ps->Add( m_emitter );
}

bool EffectTeleport::Update( float dt )
{
    if( m_emitter )
    {
        float progress = std::min( m_timer / m_duration, 1.0f );
        float x = m_start.x + progress * (m_end.x - m_start.x) + m_entity->GetPos().x;
        float y = m_start.y + progress * (m_end.y - m_start.y) + m_entity->GetPos().y;

        m_emitter->SetPosition( x, y );

        m_timer += dt;
        bool done = m_timer >= m_duration;
        if( done )
        {
            m_ps->Remove( m_emitter );
            m_emitter = NULL;
        }
    }
    m_ps->Update( dt );
    return m_ps->GetNumParticles() > 0;
}

void EffectTeleport::RenderBefore( Claw::Surface* target, int x, int y )
{
}

void EffectTeleport::RenderAfter( Claw::Surface* target, int x, int y )
{
    Vectorf pos = m_entity->GetPos();
    pos *= ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale();
    Vectorf offset = pos - Vectorf( x, y );

    m_ps->RenderDirect( target, offset );
}

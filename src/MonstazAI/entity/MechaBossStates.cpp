#include "claw/math/Math.hpp"

#include "MonstazAI/entity/MechaBossStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/particle/ExplosionParticle.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"

namespace MechaBossStates
{

    static const int OCTOPUS_ATTACK_SFX_DELAY       = 300;
    static const int OCTOPUS_ATTACK_SFX_DELAY_RAND  = 150;
    static Claw::UInt64 s_lastHit                   = 0;

    static bool CheckStateChange( Entity& e )
    {
        switch( e.m_bossState )
        {
        case 0:
            if( e.GetHitPoints() * 3 < e.GetMaxHitPoints() * 2 )
            {
                e.m_bossState = 1;
                return true;
            }
            break;
        case 1:
            if( e.GetHitPoints() * 3 < e.GetMaxHitPoints() )
            {
                e.m_bossState = 2;
                return true;
            }
            break;
        default:
            break;
        }
        return false;
    }

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/(5.f*(1+entity.m_bossState)) );
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::Chaser );
        entity.m_speedMul = 1+entity.m_bossState;
        entity.m_invulnerable = false;
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        if( CheckStateChange( entity ) )
        {
            Claw::Lua* lua = GameManager::GetInstance()->GetLua();
            lua->PushNumber( entity.m_bossState );
            lua->Call( "BossStage", 1, 0 );

            sm.ChangeState( entity, SMechaBossFly );
            return;
        }

        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            float dp = DotProduct( dst, dst );
            if( dp < 60*60 )
            {
                sm.ChangeState( entity, SMechaBossAttackMace );
                return;
            }
            if( *it == GameManager::GetInstance()->GetPlayer() )
            {
                if( entity.m_bossState == 1 && dp < 150*150 )
                {
                    sm.ChangeState( entity, SMechaBossAttackFlamer );
                }
                else if( entity.m_bossState == 2 && dp < 200*200 )
                {
                    sm.ChangeState( entity, SMechaBossAttackRocket );
                }
            }
        }
    }

    void Attack::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/30.f );
        if( g_rng.GetDouble() < 0.5f )
        {
            entity.SwitchAnimSet( Entity::AS_IDLE2 );
        }
        else
        {
            entity.SwitchAnimSet( Entity::AS_IDLE3 );
        }
    }

    void Attack::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( !GameManager::GetInstance()->GetPlayer() ) return;

        if( CheckStateChange( entity ) )
        {
            sm.ChangeState( entity, SMechaBossFly );
            return;
        }

        bool change = true;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < 75*75 )
            {
                change = false;
                break;
            }
        }

        if( change )
        {
            sm.ChangeState( entity, SMechaBossMove );
            return;
        }

        Claw::UInt64 timeMs = Claw::Time::GetTimeMs();
        if( s_lastHit < timeMs )
        {
            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_OCTOPUS_ATTACK1 + g_rng.GetInt() % 6 ), entity.GetPos() );
            s_lastHit = timeMs + OCTOPUS_ATTACK_SFX_DELAY + g_rng.GetInt() % OCTOPUS_ATTACK_SFX_DELAY_RAND;
        }
    }

    Fly::Fly()
        : m_particle( new ExplosionParticleFunctor( 512, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/geiser.png@linear" ) ) )
        , m_emitter( NULL )
    {
    }

    void Fly::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        entity.SetBehavior( Entity::HoundBig );
        entity.m_speedMul = 4;
        entity.m_invulnerable = true;

        ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
        m_emitter = new ExplosionEmitter( m_particle, ps, entity.GetPos().x, entity.GetPos().y, 150, 150, 31536000, 20 );
        ps->Add( m_emitter );

        m_time = g_rng.GetDouble( 4, 6 );
    }

    void Fly::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        m_emitter->SetPosition( entity.GetPos().x, entity.GetPos().y );

        m_time -= tick;
        if( m_time <= 0 )
        {
            GameManager::GetInstance()->GetParticleSystem()->Remove( m_emitter );
            m_emitter = NULL;
            sm.ChangeState( entity, SMechaBossMove );
        }
    }

    void AttackFlamer::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_IDLE );
        m_time = g_rng.GetDouble( 3, 4 );
    }

    void AttackFlamer::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        if( CheckStateChange( entity ) )
        {
            sm.ChangeState( entity, SMechaBossFly );
            return;
        }

        Claw::Lua* lua = GameManager::GetInstance()->GetLua();

        m_time -= tick;
        if( m_time <= 0 )
        {
            lua->PushNumber( 18 );   // Weapon type
            Claw::Lunar<Entity>::push( *lua, &entity );  // shot owner
            lua->Call( "EntityStopFiring", 2, 0 );

            sm.ChangeState( entity, SMechaBossMove );
            return;
        }

        entity.LookAt( (int)player->GetPos().x, (int)player->GetPos().y );

        Vectorf targetDir = player->GetPos() - entity.GetPos();
        targetDir.Normalize();

        lua->PushNumber( 18 );   // Weapon type
        Claw::Lunar<Entity>::push( *lua, &entity );  // shot owner
        lua->PushNumber( targetDir.x );
        lua->PushNumber( targetDir.y );
        lua->Call( "EntityFireWeapon", 4, 0 );
    }

    void AttackFlamer::OnExit( Entity& entity, StackSM& sm, StateName nextState )
    {
        if( m_time > 0 )
        {
            Claw::Lua* lua = GameManager::GetInstance()->GetLua();
            lua->PushNumber( 18 );   // Weapon type
            Claw::Lunar<Entity>::push( *lua, &entity );  // shot owner
            lua->Call( "EntityStopFiring", 2, 0 );
        }
    }

    void AttackRocket::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SetBehavior( Entity::None );
        entity.SetFrameTime( 1/15.f );
        entity.SwitchAnimSet( Entity::AS_IDLE4 );
        m_time = 4/15.f;
    }

    void AttackRocket::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( !player ) return;

        if( CheckStateChange( entity ) )
        {
            sm.ChangeState( entity, SMechaBossFly );
            return;
        }

        entity.LookAt( (int)player->GetPos().x, (int)player->GetPos().y );

        m_time -= tick;
        if( m_time <= 0 )
        {
            Claw::Lua* lua = GameManager::GetInstance()->GetLua();

            Vectorf targetDir = player->GetPos() - entity.GetPos();
            targetDir.Normalize();

            lua->PushNumber( 19 );   // Weapon type
            Claw::Lunar<Entity>::push( *lua, &entity );  // shot owner
            lua->PushNumber( targetDir.x );
            lua->PushNumber( targetDir.y );
            lua->Call( "EntityFireWeapon", 4, 0 );

            m_time = 10/15.f;
        }
    }

}

#include "claw/math/Math.hpp"

#include "MonstazAI/entity/effect/EffectStun.hpp"
#include "MonstazAI/entity/SqueezerStates.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

namespace SqueezerStates
{
    static const Claw::UInt32 SQUEEZER_HIT_DISTANCE_SQR     = 15 * 15;  // square units
    static const Claw::UInt32 SQUEEZER_SHOCK_FRAME_BEGIN    = 11;       // frame index
    static const Claw::UInt32 SQUEEZER_SHOCK_FRAME_END      = 16;       // frame index
    static const float SQUEEZER_MOVE_RAND_MIN_DIST          = 100.f;    // game units
    static const float SQUEEZER_MOVE_RAND_MAX_DIST          = 200.f;    // game units
    static const Claw::UInt32 SQUEEZER_ATTACK_TURN_DISTANCE_SQR = 30 * 30;  // square units
    static const Claw::UInt32 SQUEEZER_ATTACK_RESIGNATION_DISTANCE_SQR = 50 * 50;  // square units

    void MoveStraight::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::Roller );
    }

    void MoveStraight::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        // Check if reached the player
        Entity* player = GameManager::GetInstance()->GetPlayer();
        Vectorf dst;
        // If player exist and its still alive
        if( player && player->GetHitPoints() )
        {
            dst = entity.GetPos() - player->GetPos();
            if( DotProduct( dst, dst ) < SQUEEZER_HIT_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SSqueezerAttack );
                return;
            }
        }
        // Check if aim has been reached
        dst = entity.GetPos() - entity.GetTarget();
        if( DotProduct( dst, dst ) < SQUEEZER_HIT_DISTANCE_SQR )
        {
            sm.ChangeState( entity, SSqueezerSeek );
            return;
        }
        // TODO: Check obstacles collisions
    }

    void MoveAndTurn::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::RollerTurning );

        m_dt = g_rng.GetDouble( 2.5f, 4.f );
    }

    void MoveAndTurn::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        // Check if player is still alive
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player && player->GetHitPoints() )
        {
            // Check if reached the player
            Vectorf dst = entity.GetPos() - player->GetPos();
            if( DotProduct( dst, dst ) < SQUEEZER_ATTACK_TURN_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SSqueezerAttack );
                return;
            }
        }
        // Check if aim has been reached
        if( m_dt < tick )
        {
            sm.ChangeState( entity, SSqueezerSeek );
            return;
        }
        else
        {
            m_dt -= tick;
        }
    }

    void AttackRolling::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player && player->GetHitPoints() )
        {
            const int SOUNDS_NUM = SFX_PLAYER_HIT7 - SFX_PLAYER_HIT1;
            const int SOUND_ID = SFX_PLAYER_HIT1 + g_rng.GetInt() % SOUNDS_NUM;

            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SOUND_ID ), entity.GetPos() );
        }
    }

    void AttackRolling::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        // Check if player is alive
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player && player->GetHitPoints() )
        {
            Vectorf dst = entity.GetPos() - GameManager::GetInstance()->GetPlayer()->GetPos();
            if( DotProduct( dst, dst ) > SQUEEZER_HIT_DISTANCE_SQR )
            {
                sm.ChangeState( entity, SSqueezerMove );
            }
        }
        else
        {
            sm.ChangeState( entity, SSqueezerSeek );
        }
    }

    void AttackWalking::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_ATTACK );
    }

    void AttackWalking::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player && player->GetHitPoints() )
        {
            Vectorf dst = entity.GetPos() - player->GetPos();
            if( DotProduct( dst, dst ) > SQUEEZER_ATTACK_RESIGNATION_DISTANCE_SQR )
            {
                entity.SetSlowedDown( false );
                sm.ChangeState( entity, SSqueezerSeek );
            }
            else
            {
                // Slow down during attack
                entity.SetSlowedDown( true );
                if( g_rng.GetDouble() < 0.01f )
                {
                    const int SOUND_ID = SFX_BITE1 + g_rng.GetInt() % 2;

                    GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SOUND_ID ), entity.GetPos() );
                }
            }
        }
        else
        {
            entity.SetSlowedDown( false );
            sm.ChangeState( entity, SSqueezerSeek );
        }
    }

    void Bump::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        // Ignore multiple bumps
        if( previousState == SSqueezerBump )
        {
            // Just reset the animation
            entity.SetFrame( 0 );
            return;
        }
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        // Stop looping the animation
        entity.SetAnimLoop( false );
        // Play until 11 frame - enemy collapse
        m_lastFrame = SQUEEZER_SHOCK_FRAME_BEGIN;

        // TODO: Experimental
        entity.SetBehavior( Entity::None );
        entity.SetPos( entity.GetPos() - 10.f * entity.GetDir() );
        //entity.ApplyImpulse( entity.GetDir() * (-200.f * entity.GetMass() ) ); // apply force about (-200000.f)
    }

    void Bump::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        // Wait until animation end and then change the state
        if( entity.GetFrame() >= static_cast<int>(m_lastFrame) )
        {
            // Revert back animation looping
            entity.SetAnimLoop( true );
            sm.ChangeState( entity, SSqueezerShocked );
        }
    }

    void Shocked::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        // In the shock loop animation between 11 and 16th frame
        entity.SwitchAnimSet( Entity::AS_SPECIAL );
        // Stop automatic animation loop
        entity.SetAnimLoop( false );
        // Set to the begining of the shocked sequence
        entity.SetFrame( SQUEEZER_SHOCK_FRAME_BEGIN );
        entity.SetBehavior( Entity::None );

        m_lastFrame = entity.GetAnimSet()->GetFrames() - 1;
        m_timer = g_rng.GetDouble( 0.5f, 1.5f );

        entity.AddEffect( new EffectStun( &entity, GameManager::GetInstance()->GetShockedStars(), GameManager::GetGameScale() ) );
    }

    void Shocked::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer > tick )
        {
            m_timer -= tick;
            // Manually loop animation between 11-16 frames
            if( entity.GetFrame() == static_cast<int>( SQUEEZER_SHOCK_FRAME_END ) )
            {
                entity.SetFrame( SQUEEZER_SHOCK_FRAME_BEGIN );
            }
        }
        // Wait until whole animation ends and then change the state
        else if( entity.GetFrame() == static_cast<int>(m_lastFrame) )
        {
            // Revert back animation looping
            entity.SetAnimLoop( true );
            sm.ChangeState( entity, SSqueezerSeek );
        }
    }

    Seek::Seek()
        : m_seekDistMin( 100 )
        , m_seekDistMax( 350 )
        , m_seekTimeMin( 0.5f )
        , m_seekTimeMax( 2.f )
    {}

    void Seek::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        // Change to idle animation
        entity.SwitchAnimSet( Entity::AS_IDLE );
        entity.SetBehavior( Entity::None );
        m_timer = g_rng.GetDouble( m_seekTimeMin, m_seekTimeMax );
    }

    void Seek::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer > tick )
        {
            m_timer -= tick;
            return;
        }

        const Vectorf& entityPos = entity.GetPos();
        Vectorf targetPos = GetTargetPos( entity );
        Vectorf targetDir = targetPos - entityPos;

        // TODO: Distance limitation may be removed
        //if( DistanceSqr( targetDir, targetDir ) > SQUEEZER_HIT_DISTANCE_SQR )
        {
            float targetDist = targetDir.Length();
            targetDir /= targetDist;

            // Check for obstacles within target range
            // (targetDist -> seekDist range is not checked by purpose)
            float seekDist = g_rng.GetDouble( m_seekDistMin, m_seekDistMax );
            float seekClosest = std::min( seekDist, targetDist );

            Vectorf norm( -entity.GetLook().y, entity.GetLook().x );
            float r = sqrt( entity.m_entityRadius );

            bool pathClear =
                CheckPath( entityPos, targetDir, seekClosest ) &&
                CheckPath( entityPos + norm * r, targetDir, seekClosest ) &&
                CheckPath( entityPos - norm * r, targetDir, seekClosest );

            if( !pathClear )
            {
                // Give me some time until situation will change
                m_timer = g_rng.GetDouble( m_seekTimeMin, m_seekTimeMax );
                return;
            }
            // Move along full path, this will introduce funny effect of missing the player
            targetDir *= seekDist;
            targetPos = entityPos + targetDir;
            // Do not allow to move outside the map boundaries
            BoundTargetToMap( targetPos, entityPos );
            // Setup movement target and look direction
            entity.SetTarget( targetPos );
            entity.LookAt( static_cast<int>(entity.GetTarget().x), static_cast<int>(entity.GetTarget().y) );
            sm.ChangeState( entity, SSqueezerMove );
        }
    }

    Vectorf Seek::GetTargetPos( const Entity& entity ) const
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        if( player && player->GetHitPoints() )
        {
            return player->GetPos();
        }
        else
        {
            const float angle = g_rng.GetDouble( -M_PI, M_PI );
            const float dist = g_rng.GetDouble( SQUEEZER_MOVE_RAND_MIN_DIST, SQUEEZER_MOVE_RAND_MAX_DIST );
            Vectorf dir( dist, 0.0f );
            dir.Rotate( angle );
            return entity.GetPos() + dir;
        }
    }

    bool Seek::CheckPath( const Vectorf& origin, const Vectorf& dir, float dist )
    {
        using namespace Scene;

        RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( origin, dir, dist );
        const std::vector<Obstacle*>& v = query->GetColliders();
        for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
        {
            if( (*oit)->GetKind() != Obstacle::Holo )
            {
                return false;
            }
        }

        return true;
    }

    bool Seek::BoundTargetToMap( Vectorf& targetPos, const Vectorf& entityPos )
    {
        // Do not allow to move outside the map boundaries
        const Scene::AARect& mapBv = GameManager::GetInstance()->GetMap()->GetBv();
        if( mapBv.Overlaps( entityPos ) )
        {
            return mapBv.BoundToArea( targetPos );
        }
        return false;
    }

    void SeekAndBypass::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer > tick )
        {
            m_timer -= tick;
            return;
        }

        const Vectorf& entityPos = entity.GetPos();
        Vectorf targetPos = GetTargetPos( entity );
        Vectorf seekDir = targetPos - entityPos;

        // TODO: Distance limitation may be removed
        if( DotProduct( seekDir, seekDir ) > SQUEEZER_HIT_DISTANCE_SQR )
        {
            float targetDist = seekDir.Length();
            seekDir /= targetDist;

            // Check for obstacles within target range (not full seek range)
            // distance between target point and seek range is not checked by purpose)
            float seekDist = g_rng.GetDouble( m_seekDistMin, m_seekDistMax );
            float seekClosest = std::min( seekDist, targetDist );

            bool pathClear = CheckPath( entityPos, seekDir, seekClosest );
            if( !pathClear )
            {
                targetPos = Vectorf( 0, 0 );
                if( SeekBypass( targetPos, entityPos, seekDir ) )
                {
                    // Clamp movement target within map area boundary
                    BoundTargetToMap( targetPos, entityPos );
                    entity.SetTarget( targetPos );
                }
                else
                {
                    // Give me some time until situation will change
                    m_timer = g_rng.GetDouble( m_seekTimeMin, m_seekTimeMax );
                    return;
                }
            }
            else
            {
                seekDir *= seekDist;
                targetPos = entityPos + seekDir;
                // Do not allow to move outside the map boundaries
                BoundTargetToMap( targetPos, entityPos );
                entity.SetTarget( targetPos );
            }
            entity.LookAt( static_cast<int>(entity.GetTarget().x), static_cast<int>(entity.GetTarget().y) );
            sm.ChangeState( entity, SSqueezerMove );
        }
        else
        {
            // Wait for a while until player goes away
            m_timer = g_rng.GetDouble( m_seekTimeMin, m_seekTimeMax );
        }
    }

    bool SeekAndBypass::SeekBypass( Vectorf& bypassPos, const Vectorf& origin, const Vectorf& dir, unsigned char tries /* = 3 */ )
    {
        if( tries == 0 )
            return false;

        float fovMin = M_PI / 4;
        float fovMax = M_PI / 6;
        const float fovLimit = M_PI;
        const float fovInc = M_PI / 6;
        Vectorf randDir;
        float randDist = 0;
        bool pathClear = false;
        do
        {
            fovMax = Claw::MinMax( fovMax + fovInc, fovMax, fovLimit );
            float angle = g_rng.GetDouble( fovMin, fovMax );
            angle *= Claw::Sgn( g_rng.GetInt( -100, 100 ) );

            randDir = dir;
            randDir.Rotate( angle );
            randDist = g_rng.GetDouble( m_seekDistMin, m_seekDistMax );

            pathClear = CheckPath( origin, randDir, randDist );
        } while( --tries && !pathClear );

        if( !pathClear )
        {
            // bypass position not found
            return false;
        }
        else
        {
            // bypass found save it and exit with true
            bypassPos = randDir;
            bypassPos *= randDist;
            bypassPos += origin;
            return true;
        }
    }

    void SeekIdle::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        if( m_timer > tick )
        {
            m_timer -= tick;
            return;
        }
        sm.ChangeState( entity, SSqueezerMove );
    }


    void SeekAway::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        // Just do nothing but overide base state implementation
    }

    void SeekAway::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        const Vectorf& entityPos = entity.GetPos();
        Vectorf targetPos = GetTargetPos( entity );
        Vectorf seekDir = entityPos - targetPos;

        float targetDist = seekDir.Length();
        seekDir /= targetDist;

        // Check for obstacles within seek range
        float seekDist = g_rng.GetDouble( m_seekDistMin, m_seekDistMax );

        bool pathClear = CheckPath( entityPos, seekDir, seekDist );
        if( !pathClear )
        {
            targetPos = Vectorf( 0, 0 );
            if( SeekBypass( targetPos, entityPos, seekDir ) )
            {
                // Clamp movement target to the map boundaries
                BoundTargetToMap( targetPos, entityPos );
                entity.SetTarget( targetPos );
            }
            else
            {
                // Give me some time until situation will change
                m_timer = g_rng.GetDouble( m_seekTimeMin, m_seekTimeMax );
                return;
            }
        }
        else
        {
            seekDir *= seekDist;
            targetPos = entityPos + seekDir;
            BoundTargetToMap( targetPos, entityPos );
            entity.SetTarget( targetPos );
        }
        entity.LookAt( static_cast<int>(entity.GetTarget().x), static_cast<int>(entity.GetTarget().y) );
        sm.ChangeState( entity, SSqueezerMoveStraight );
    }

}

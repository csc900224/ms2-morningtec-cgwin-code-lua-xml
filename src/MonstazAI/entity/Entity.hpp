#ifndef __MONSTAZ_ENTITY_HPP__
#define __MONSTAZ_ENTITY_HPP__

#include <string.h>
#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/math/Matrix.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/AnimationSet.hpp"
#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/Renderable.hpp"
#include "MonstazAI/entity/EntityEffect.hpp"
#include "MonstazAI/entity/effect/EffectElements.hpp"
#include "MonstazAI/StackSM.hpp"
#include "MonstazAI/State.hpp"
#include "MonstazAI/math/SinesHarmonics.hpp"
#include "MonstazAI/entity/SqueezerStates.hpp"
#include "MonstazAI/PickupManager.hpp"

class Waypoint;

class Entity : public Renderable
{
public:
    enum Type
    {
        Player = 1,     // Must start from 1 to be indexed from lua

        OctopusSimple,
        OctopusShotAware,
        OctopusChaser,

        SqueezerSimple,
        SqueezerTurning,

        FishSimple,
        FishSimpleNonExploding,
        FishThrowing,

        FloaterSimple,
        FloaterElectric,
        FloaterSower,

        HoundSimple,
        HoundShooting,

        SectoidSimple,
        SectoidShooting,

        KillerWhale,
        KillerWhaleSimple,

        Crab,

        MechaBoss,
        SowerBoss,

        OctobrainBoss,
        OctobrainBossClone,

        OctopusFriend,
        AIFriend,

        Lobster,

        NautilSimple,
        NautilTurning,

        Nerval,

        TypesCount
    };

    enum Behavior
    {
        None,
        Human,
        Following,
        ShotAvoider,
        Chaser,
        Interceptor,
        Roller,
        RollerTurning,
        Flying,
        FlyingAggressive,
        HoundBig,
        FollowWaypoints,
        RunAway,
        HelpPlayer,

        BehaviorsCount
    };

    enum AnimSet
    {
        AS_MOVE = 0,
        AS_ATTACK,
        AS_IDLE,
        AS_IDLE2,
        AS_IDLE3,
        AS_IDLE4,
        AS_IDLE5,
        AS_SPECIAL,

        AS_NUM
    };

    enum Class
    {
        Friendly,
        Enemy
    };

    LUA_DEFINITION( Entity );
    Entity( lua_State* L );
    static void Init( Claw::Lua* lua );

    Entity( Type type, float x, float y, Pickup::Type item, const Vectorf& look, int element );
    ~Entity();

    void Render( Claw::Surface* target, const Vectorf& offset, float scale = 1.0f ) const;
    void RenderPost( Claw::Surface* target, const Vectorf& offset, float scale = 1.0f ) const;
    void Update( float dt );

    Vectorf& GetLook() { return m_look; }
    Vectorf& GetAvoidance() { return m_avoidance; }
    Vectorf& GetObstacleAvoidance() { return m_obstacleAvoidance; }
    Vectorf& GetShotAvoidance() { return m_shotAvoidance; }

    void SetAvoidanceAbility( float factor = 0.0f );
    float GetAvoidanceRotSpeed() const { return m_avoidanceRotSpeed; }

    float GetHitPoints() const { return m_hitPoints; }
    float GetMaxHitPoints() const { return m_maxHitPoints; }
    void SetHitPoints( float hp ) { m_hitPoints = hp; }
    void SetFrame( int frame ) { m_frame = frame; }
    int GetFrame() const { return m_frame; }
    bool SwitchAnimSet( int idx );
    const AnimationSetPtr GetAnimSet() const;
    void SetFrameTime( float frameTime ) { m_frameNext = frameTime; }
    Class GetClass() const { return m_class; }
    void SetClass( Class cls ) { m_class = cls; }
    int GetLastDir() const { return m_lastDir; }

    void SetAnimUpdate( bool enable )   { m_animUpdate = enable; }
    bool IsAnimUpdated() const          { return m_animUpdate; }

    void SetAnimLoop( bool enable )     { m_animLoop = enable; }
    bool IsAnimLooped() const           { return m_animLoop; }

    void SetAnimDirection( int dir )    { m_frameDir = dir; }
    int  GetAnimDirection() const       { return m_frameDir; }

    void SetVisibility( int visible )   { m_draw = visible; }
    bool IsVisible() const              { return m_draw; }

    void SetAlpha( Claw::UInt8 alpha )  { m_alpha = alpha; }
    Claw::UInt8 GetAlpha() const        { return m_alpha; }

    void SetHologram( float intensity ) { m_hologram = intensity; }
    float GetHologram() const           { return m_hologram; }

    void SetTarget( const Vectorf& target ) { m_target = target; }
    const Vectorf& GetTarget() const { return m_target; }

    void SetDir( const Vectorf& dir ) { m_dir = dir; }
    const Vectorf& GetDir() const { return m_dir; }

    void SetLook( const Vectorf& dir ) { m_look = dir; m_look.Normalize(); }
    const Vectorf& GetLook() const { return m_look; }

    void SetPos( const Vectorf& pos ) { m_pos = pos; }

    void LookAt( int x, int y );

    void AddEffect( EntityEffect* effect );

    void Hit( Shot::Type type, Claw::UInt8 elements, float val );
    Shot::Type GetLastHit() const { return m_lastHit; }
    Claw::UInt8 GetLastHitElements() const { return m_lastHitElements; }
    Shot::Type GetSlowDownHit() const { return m_slowDownHit; }

    void CollideObstacle();

    inline bool MayCollideObstacles() const { return GetBehavior() == Roller; }
    bool IsStateSet( StateName state ) const { return state == m_sm.GetCurrentStateId(); }
    void SetState( StateName state ) { m_sm.ChangeState( *this, state ); }

    inline bool IsSheathed( Shot::Type shotType ) const { return ( (GetBehavior() == Roller) || (GetBehavior() == RollerTurning) ) && (shotType != Shot::Flamer && shotType != Shot::Electricity); }
    inline bool CanBleed() const { return m_canBleed; }

    void Die();

    Type GetType() const { return m_type; }
    void SetType( Type type ) { m_type = type; }
    int l_GetType( lua_State* L );

    void SetBehavior( Behavior bh ) { m_behavior = bh; }
    Behavior GetBehavior() const { return m_behavior; }
    int l_GetBehavior( lua_State* L );

    bool IsInviolable( const Shot* shot ) const;
    void SetInviolable( bool inviolable ) { m_inviolable = inviolable; }

    inline bool IsHarmless() const { return m_harmless; }
    void SetHarmless( bool harmless ) { m_harmless = harmless; }

    bool IsSlowedDown() const { return m_slowDownTimer > 0; }

    void SetSlowedDown( bool enable, float time = 2.0f )    { if( enable ) m_slowDownTimer = time; else m_slowDownTimer = 0; }
    float GetSlowDownFactor() const                         { return m_slowDownFactor; }
    float GetHarmonicSpeedFactor() const                    { return m_moveHarmonics.GetValue( GetFrame() ); }
    SinesHarmonics& GetHarmonics()                          { return m_moveHarmonics; }

    void SetAnimSet( AnimSet set, AnimationSet* animset )   { m_animSet[set].Reset( animset ); }

    Waypoint* GetWaypoint();
    Pickup::Type GetItem() { return m_item; }

    void ElementHit();

    int l_GetPos( lua_State* L );
    int l_SetPos( lua_State* L );
    int l_GetLook( lua_State* L );
    int l_SetLook( lua_State* L );
    int l_GetTarget( lua_State* L );
    int l_SetTarget( lua_State* L );
    int l_GetDir( lua_State* L );
    int l_GetHitPoints( lua_State* L );
    int l_SetHitPoints( lua_State* L );
    int l_GetMaxHitPoints( lua_State* L );
    int l_SetMaxHitPoints( lua_State* L );
    int l_SetFrame( lua_State* L );
    int l_SetClass( lua_State* L );
    int l_AddWaypoint( lua_State* L );
    int l_SetLoop( lua_State* L );

    Entity* m_nextSeg;

    float m_speedMul;
    bool m_draw;
    Claw::UInt8 m_shadow;
    float m_shadowScale;
    float m_entityRadius;
    int m_bossState;
    bool m_invulnerable;
    Vectorf m_forced;
    bool m_underAttack;

    static const float  AVERAGE_RADIUS;

protected:
    Type                m_type;
    Behavior            m_behavior;
    Class               m_class;
    Vectorf       m_look;
    Vectorf       m_target;
    Vectorf       m_dir;
    int                 m_frame;
    float               m_frameTime;
    float               m_frameNext;
    int                 m_frameDir;
    mutable int         m_lastDir;

    Vectorf       m_avoidance;
    Vectorf       m_obstacleAvoidance;
    Vectorf       m_shotAvoidance;

    float               m_avoidanceRotSpeed;

    AnimationSetPtr     m_animSet[AS_NUM];
    bool                m_animUpdate;
    bool                m_animLoop;
    int                 m_currentAnim;

    Claw::UInt8         m_alpha;
    float               m_hologram;

    float               m_hitPoints;
    float               m_maxHitPoints;
    float               m_orbsCounter;
    Shot::Type          m_lastHit;
    Shot::Type          m_slowDownHit;
    Claw::UInt8         m_lastHitElements;
    bool                m_inviolable;
    bool                m_harmless;
    bool                m_canBleed;

    float               m_glopTimer;
    float               m_slowDownTimer;

    float               m_slowDownFactor;
    SinesHarmonics      m_moveHarmonics;

    Pickup::Type        m_item;

    StackSM m_sm;

    std::vector<EntityEffectPtr> m_effects;
    EffectElements*     m_elements;
};

#endif

#include "MonstazAI/entity/AIFriendStates.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/GameManager.hpp"

namespace AIFriendStates
{

    void Move::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
        entity.SwitchAnimSet( Entity::AS_MOVE );
        entity.SetBehavior( Entity::HelpPlayer );
    }

    void Move::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( entity.GetClass() == (*it)->GetClass() ) continue;

            Vectorf dst = entity.GetPos() - (*it)->GetPos();
            if( DotProduct( dst, dst ) < 250*250 )
            {
                sm.ChangeState( entity, SAIFriendFire );
                return;
            }
        }
    }

    void Fire::OnEnter( Entity& entity, StackSM& sm, StateName previousState )
    {
    }

    void Fire::OnUpdate( Entity& entity, StackSM& sm, float tick )
    {
        float dist = 250*250;
        Entity* target = NULL;
        const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( (*it)->GetClass() != Entity::Enemy ) continue;
            Vectorf dir( (*it)->GetPos() - entity.GetPos() );
            float dd = DotProduct( dir, dir );
            if( dd < dist )
            {
                float d = sqrt( dd );
                dir /= d;

                bool collide = false;
                Scene::RayTraceQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryRayTrace( entity.GetPos(), dir, d );
                const std::vector<Obstacle*>& v = query->GetColliders();
                for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
                {
                    if( (*oit)->GetKind() == Obstacle::Regular )
                    {
                        collide = true;
                        break;
                    }
                }
                if( !collide )
                {
                    dist = dd;
                    target = *it;
                }
            }
        }

        Claw::Lua* lua = GameManager::GetInstance()->GetLua();

        if( target )
        {
            Vectorf targetDir = target->GetPos() - entity.GetPos();
            targetDir.Normalize();

            entity.SetLook( targetDir );

            lua->PushBool( true );
            lua->Call( "AIShot", 1, 0 );
        }
        else
        {
            lua->PushBool( false );
            lua->Call( "AIShot", 1, 0 );
            sm.ChangeState( entity, SAIFriendMove );
            return;
        }
    }

}

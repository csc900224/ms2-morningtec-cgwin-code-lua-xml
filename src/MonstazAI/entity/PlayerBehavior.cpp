#include "MonstazAI/entity/PlayerBehavior.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/entity/EntityManager.hpp"

void PlayerBehavior::ApplyPotential( Entity& entity, float dt, const Vectorf& vec )
{
    GameManager::GetInstance()->GetEntityManager()->Move( &entity, vec * dt, 0, GameManager::GetInstance()->GetEntityManager()->GetData( Entity::Player ).moveSpeed );
}

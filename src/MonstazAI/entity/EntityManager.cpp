#include <limits>

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/system/Alloc.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/entity/EntityManager.hpp"
#include "MonstazAI/math/LuaMath.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/entity/EnemyBehavior.hpp"
#include "MonstazAI/entity/PlayerBehavior.hpp"
#include "MonstazAI/entity/BehaviorFunctors.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/entity/effect/EffectHighlight.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"


LUA_DECLARATION( EntityManager )
{
    METHOD( EntityManager, Add ),
    METHOD( EntityManager, AddDelayed ),
    METHOD( EntityManager, Remove ),
    METHOD( EntityManager, Count ),
    METHOD( EntityManager, GrimReaper ),
    METHOD( EntityManager, SetEntityData ),
    METHOD( EntityManager, SetupEntityBehaviors ),
    METHOD( EntityManager, PlayerLookDirectionChanged ),
    METHOD( EntityManager, UpdateBehaviors ),
    METHOD( EntityManager, SpawnTickFinished ),
    METHOD( EntityManager, GetCat ),
    {0,0}
};

EntityManager::EntityManager( Claw::Lua* lua )
    : m_shadow( Claw::AssetDict::Get<Claw::Surface>( "gfx/shadow.png@linear" ) )
    , m_spawnGfx( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/spawn_bg.ani" ) )
    , m_scale( GameManager::GetGameScale() )
    , m_playerUnderAttack( false )
    , m_attackModifier( 1 )
    , m_unstoppable( false )
    , m_entityLastHit( 0 )
    , m_playerDirectionalTicks( 0 )
    , m_bloodSplat( new BloodParticleFunctor( 0.65f ) )
    , m_spawnAnim( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/spawn_flash.ani" ) )
    , m_playerWeaponSlowDown( 0.3f )
{
    lua->RegisterLibrary( Claw::Lua::L_BASE );
    lua->RegisterLibrary( Claw::Lua::L_MATH );
    lua->RegisterLibrary( Claw::Lua::L_TABLE );

    RegisterMath( lua );

    Entity::Init( lua );

    Claw::Lunar<EntityManager>::Register( *lua );
    Claw::Lunar<EntityManager>::push( *lua, this );
    lua->RegisterGlobal( "EntityManager" );

    InitEnum( lua );

    for( int i = 0; i < Entity::BehaviorsCount; ++i )
    {
        m_behaviors[i] = NULL;
    }

    m_playerAnimSpeed[0] = 6;
    m_playerAnimSpeed[1] = 6;
}

void EntityManager::InitEnum( Claw::Lua* lua )
{
    lua->CreateEnumTable();
    lua->AddEnum( Entity::Player );
    lua->AddEnum( Entity::OctopusSimple );
    lua->AddEnum( Entity::OctopusShotAware );
    lua->AddEnum( Entity::OctopusChaser );
    lua->AddEnum( Entity::OctopusFriend );
    lua->AddEnum( Entity::AIFriend );
    lua->AddEnum( Entity::SqueezerSimple );
    lua->AddEnum( Entity::SqueezerTurning );
    lua->AddEnum( Entity::FishSimple );
    lua->AddEnum( Entity::FishSimpleNonExploding );
    lua->AddEnum( Entity::FishThrowing );
    lua->AddEnum( Entity::FloaterSimple );
    lua->AddEnum( Entity::FloaterElectric );
    lua->AddEnum( Entity::FloaterSower );
    lua->AddEnum( Entity::HoundSimple );
    lua->AddEnum( Entity::HoundShooting );
    lua->AddEnum( Entity::SectoidSimple );
    lua->AddEnum( Entity::SectoidShooting );
    lua->AddEnum( Entity::KillerWhale );
    lua->AddEnum( Entity::KillerWhaleSimple );
    lua->AddEnum( Entity::Crab );
    lua->AddEnum( Entity::MechaBoss );
    lua->AddEnum( Entity::SowerBoss );
    lua->AddEnum( Entity::OctobrainBoss );
    lua->AddEnum( Entity::OctobrainBossClone );
    lua->AddEnum( Entity::Lobster );
    lua->AddEnum( Entity::NautilSimple );
    lua->AddEnum( Entity::NautilTurning );
    lua->AddEnum( Entity::Nerval );
    lua->RegisterEnumTable( "EntityType" );

    lua->CreateEnumTable();
    lua->AddEnum( Entity::Friendly );
    lua->AddEnum( Entity::Enemy );
    lua->RegisterEnumTable( "EntityClass" );
}

EntityManager::~EntityManager()
{
    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        delete *it;
    }
    for( int i = 0; i < Entity::BehaviorsCount; ++i )
    {
        delete m_behaviors[i];
    }
}

void EntityManager::Render( Claw::Surface* target, const Vectorf& offset )
{
    const Vectorf pivot( m_shadow->GetWidth() / 2, m_shadow->GetHeight() / 2 );
    Claw::Rect viewRect = target->GetClipRect();
    Claw::Rect shadowRect = m_shadow->GetClipRect();
    viewRect.m_x += (int)pivot.x;
    viewRect.m_y += (int)pivot.y;

    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( !(*it)->m_draw ) continue;
        const Vectorf& pos = (*it)->GetPos();
        shadowRect.m_x = m_scale * pos.x - offset.x;
        shadowRect.m_y = m_scale * pos.y - offset.y;
        if( viewRect.IsIntersect( shadowRect ) )
        {
            m_shadow->SetAlpha( (*it)->m_shadow );
            Claw::TriangleEngine::Blit( target, m_shadow, shadowRect.m_x, shadowRect.m_y, 0, 2 * (*it)->m_shadowScale, pivot );
        }
    }

    for( std::vector<DelayedSpawnGroup>::iterator it = m_dsgList.begin(); it != m_dsgList.end(); ++it )
    {
        // Dont't render spawn area for Octobrain Clone
        if( !it->animPlayed && it->ds.begin()->type != Entity::OctobrainBossClone )
        {
            it->animPlayed = true;
            GameManager::GetInstance()->AddAnimation( m_spawnGfx, it->pos );
        }
    }

    RenderableManager* rm = GameManager::GetInstance()->GetRenderableManager();
    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( (*it)->m_draw )
        {
            rm->Add( *it );
        }
    }
}

void EntityManager::RenderPost( Claw::Surface* target, const Vectorf& offset )
{
    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( !(*it)->m_draw ) continue;
        (*it)->RenderPost( target, offset, m_scale );
    }
}

void EntityManager::Update( float dt )
{
    if( m_entityLastHit > 0 )
    {
        m_entityLastHit -= dt;
    }

    ObstacleManager* om = GameManager::GetInstance()->GetObstacleManager();
    Map* map = GameManager::GetInstance()->GetMap();

    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        (*it)->Update( dt );

        Obstacle* o = om->QueryCollision( (*it)->GetPos() );
        if( o && o->GetKind() == Obstacle::Holo )
        {
            CLAW_ASSERT( o->GetName() );
            const std::vector<Map::StaticObject*>& v = map->GetNamedObject( o->GetName() );

            for( std::vector<Map::StaticObject*>::const_iterator vit = v.begin(); vit != v.end(); ++vit )
            {
                (*vit)->m_holo = std::min( 1.f, (*vit)->m_holo + 0.1f );
            }
        }
    }

    std::vector<DelayedSpawnGroup>::iterator it = m_dsgList.begin();
    while( it != m_dsgList.end() )
    {
        it->time += dt;
        if( it->time >= 0.8f && !it->spawned )
        {
            for( std::vector<DelayedSpawn>::const_iterator sit = it->ds.begin(); sit != it->ds.end(); ++sit )
            {
                Add( sit->pos.x, sit->pos.y, sit->type, sit->item );
            }
            it->spawned = true;
        }
        if( it->time >= 1 )
        {
            it = m_dsgList.erase( it );
        }
        else
        {
            ++it;
        }
    }
}

void EntityManager::UpdateBehaviors( float dt )
{
    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        Entity* entity = *it;
        if ( m_behaviors[entity->GetBehavior()] )
        {
            m_behaviors[entity->GetBehavior()]->Update( *entity, dt );
        }
    }
}

Entity* EntityManager::Add( float x, float y, Entity::Type type, Pickup::Type item )
{
    bool ok = true;
    do
    {
        ok = true;
        for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
        {
            const Vectorf& pos = (*it)->GetPos();
            if( abs( pos.x - x ) < 1 && abs( pos.y - y ) < 1 )
            {
                x += g_rng.GetDouble() * 2 - 1;
                y += g_rng.GetDouble() * 2 - 1;
                ok = false;
                break;
            }
        }
    }
    while( !ok );

    if( type == Entity::OctopusFriend )
    {
        Claw::Registry::Get()->Set( "/internal/catlevel", true );
    }

    if( type != Entity::OctobrainBossClone )
    {
        GameManager::GetInstance()->AddAnimation( m_spawnAnim, Vectorf( x, y + 5 ) );
    }
    Entity* e = new Entity( type, x, y, item, Vectorf( 0, 1 ), m_data[type].shield );
    if( type == Entity::OctopusFriend || type == Entity::AIFriend )
    {
        e->AddEffect( new EffectHighlight( e, GameManager::GetGameScale(), type == Entity::OctopusFriend ) );
    }
    m_ents.push_back( e );
    return e;
}

std::vector<Entity*>::iterator EntityManager::Remove( Entity* e )
{
    for( std::vector<Entity*>::iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( *it == e )
        {
            return Remove( it );
        }
    }
    return m_ents.end();
}

std::vector<Entity*>::iterator EntityManager::Remove( std::vector<Entity*>::iterator it )
{
    Entity* e = *it;
    switch( e->GetType() )
    {
    case Entity::SqueezerSimple:
    case Entity::SqueezerTurning:
    case Entity::NautilSimple:
    case Entity::NautilTurning:
        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_ROLLER_DEATH, e->GetPos() );
        break;
    case Entity::FishSimple:
        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_FISH_EXPLOSION, e->GetPos() );
        // fallthrough
    case Entity::FishSimpleNonExploding:
    case Entity::FishThrowing:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_FISH_DEATH1 + g_rng.GetInt() % 3 ), e->GetPos() );
        break;
    case Entity::FloaterSimple:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_FLOATER_DIE1 + g_rng.GetInt() % 2 ), e->GetPos() );
        break;
    case Entity::FloaterElectric:
        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_FLOATER_ELECTRIC_DIE, e->GetPos() );
        break;
    case Entity::FloaterSower:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_FLOATER_DIE1 + g_rng.GetInt() % 2 ), e->GetPos() );
        break;
    case Entity::HoundSimple:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_HOUND_DIE1 + g_rng.GetInt() % 2 ), e->GetPos() );
        break;
    case Entity::HoundShooting:
        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_HOUND_SHOOTING_DIE, e->GetPos() );
        break;
    case Entity::SowerBoss:
        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_SOWER_DEATH, e->GetPos() );
        break;
    case Entity::KillerWhale:
    case Entity::KillerWhaleSimple:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_GIANT_BLOT_DMG1 + g_rng.GetInt() % 3 ), e->GetPos() );
        break;
    case Entity::SectoidShooting:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_SECTOID2_DEATH1 + g_rng.GetInt() % 4 ), e->GetPos() );
        break;
    case Entity::SectoidSimple:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_SECTOID_DEATH1 + g_rng.GetInt() % 2 ), e->GetPos() );
        break;
    default:
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_DEATH1 + g_rng.GetInt() % 3 ), e->GetPos() );
        break;
    }
    it = m_ents.erase( it );
    delete e;
    return it;
}

Entity* EntityManager::FindPlayerEntity()
{
    for( std::vector<Entity*>::iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( (*it)->GetType() == Entity::Player )
        {
            return *it;
        }
    }
    return NULL;
}

bool EntityManager::CheckLastHit()
{
    if( m_entityLastHit <= 0 )
    {
        m_entityLastHit = 0.1f + g_rng.GetDouble() * 0.05f;
        return true;
    }
    else
    {
        return false;
    }
}

int EntityManager::l_Add( lua_State* L )
{
    Claw::Lua lua( L );
    Pickup::Type p = Pickup::NumPickups;
    int top = lua.GetTop();
    if( top > 3 )
    {
        p = lua.CheckEnum<Pickup::Type>( 4 );
    }
    Entity* e = Add( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ), lua.CheckEnum<Entity::Type>( 3 ), p );
    if( e->GetType() == Entity::Player || e->GetType() == Entity::AIFriend )
    {
        e->SetClass( Entity::Friendly );
    }
    Claw::Lunar<Entity>::push( L, e );
    return 1;
}

int EntityManager::l_AddDelayed( lua_State* L )
{
    Claw::Lua lua( L );
    Pickup::Type p = Pickup::NumPickups;
    if( !lua.IsNil( 4 ) )
    {
        p = lua.CheckEnum<Pickup::Type>( 4 );
    }
    Vectorf pos( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) );
    m_delayedSpawnGroup.ds.push_back( DelayedSpawn( pos, lua.CheckEnum<Entity::Type>( 3 ), p ) );
    AudioManager::GetInstance()->Play3D( AudioSfx( SFX_TELEPORT1 + g_rng.GetInt( 2 ) ), pos );
    return 0;
}

int EntityManager::l_Remove( lua_State* L )
{
    Claw::Lua lua( L );
    Entity* e = Claw::Lunar<Entity>::check( L, 1 );
    Remove( e );
    return 0;
}

int EntityManager::l_Count( lua_State* L )
{
    Claw::Lua lua( L );
    int cnt = 0;
    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( (*it)->GetClass() == Entity::Friendly ) continue;
        cnt++;
    }
    for( std::vector<DelayedSpawnGroup>::const_iterator it = m_dsgList.begin(); it != m_dsgList.end(); ++it )
    {
        cnt += it->ds.size();
    }
    lua.PushNumber( cnt );
    return 1;
}

void EntityManager::Move( Entity* e, const Vectorf& dir, float rs, float mms )
{
    switch( e->GetType() )
    {
    case Entity::Player:
        MovePlayer( e, dir, mms );
        break;
    case Entity::AIFriend:
        MoveAIFriend( e, dir, mms );
        break;
    case Entity::FloaterElectric:
        MoveFloater( e, dir, rs, mms );
        break;
    default:
        MoveNormal( e, dir, rs, mms );
        break;
    }
}

void EntityManager::MoveNormal( Entity* e, const Vectorf& dir, float rs, float mms )
{
    // No direction fast exit
    if( dir.x == 0 && dir.y == 0 )
        return;

    // Override angular speed to allow entities fast reaction on surrounding
    // obstacles. Entity surrounding modifies avoidance its ability via
    // rotation speed attenuation in the obstacle proximity (see SetAvoidanceAbility() )
    rs += e->GetAvoidanceRotSpeed();

    mms *= ( 1.0f + e->GetHarmonicSpeedFactor() ) * e->m_speedMul;

    if( e->m_underAttack )
    {
        e->m_underAttack = false;
        mms *= 0.5f;
        rs *= 0.5f;
    }

    Vectorf v(dir);
    Vectorf& pos = e->GetPos();
    Vectorf& l = e->GetLook();
    float len = v.Normalize();

    Vectorf vel( 0.0f, 0.0f );

    float s = CrossProduct( l, v );
    float c = DotProduct( l, v );

    if( c < -1 + 0.001 )
    {
        if( g_rng.GetDouble() > 0.5 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }
    }
    else if( c < 1 - 0.001 )
    {
        if( s > 0 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }

        if( CrossProduct( l, v ) * s < 0 )
        {
            l = v;
        }
    }

    if( e->IsSlowedDown() )
    {
        mms *= e->GetSlowDownFactor();
    }

    // Limit movement to desired speed factor
    if( len < 1 )
    {
        vel += l * len * mms;
    }
    else
    {
        vel += l * mms;
    }

    if( e->MayCollideObstacles() )
    {
        float speed = vel.LengthSqr();
        if( speed > 0.0f )
        {
            Scene::CollisionQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryCollision( pos, sqrt(e->m_entityRadius) );
            if( !query->GetColliders().empty() )
            {
                std::vector<Obstacle*>::iterator it = query->GetColliders().begin();

                while( it != query->GetColliders().end() )
                {
                    if( ( e->GetType() == Entity::OctobrainBoss && (*it)->GetKind() != Obstacle::Regular ) || (*it)->GetKind() == Obstacle::Holo )
                    {
                        it = query->GetColliders().erase(it);
                    }
                    else
                    {
                        ++it;
                    }
                }
            }

            if( !query->GetColliders().empty() )
            {
                vel = Vectorf( 0, 0 );
                e->CollideObstacle();
            }
        }
    }

    pos += vel;

    const Vectorf& mapMin = GameManager::GetInstance()->GetSegmentableMapMin();
    const Vectorf& mapMax = GameManager::GetInstance()->GetSegmentableMapMax();

    // Clip position to the map size
    if( pos.x < mapMin.x ) { pos.x = mapMin.x; e->CollideObstacle(); }
    if( pos.x > mapMax.x ) { pos.x = mapMax.x; e->CollideObstacle(); }
    if( pos.y < mapMin.y ) { pos.y = mapMin.y; e->CollideObstacle(); }
    if( pos.y > mapMax.y ) { pos.y = mapMax.y; e->CollideObstacle(); }

    // Ignore forced moves in direction settings, just set look vector
    e->SetDir( l );
}

void EntityManager::MoveFloater( Entity* e, const Vectorf& dir, float rs, float mms )
{
    // No direction fast exit
    if( dir.x == 0 && dir.y == 0 )
        return;

    // Override angular speed to allow entities fast reaction on surrounding
    // obstacles. Entity surrounding modifies avoidance its ability via
    // rotation speed attenuation in the obstacle proximity (see SetAvoidanceAbility() )
    rs += e->GetAvoidanceRotSpeed();

    mms *= ( 1.0f + e->GetHarmonicSpeedFactor() ) * e->m_speedMul;

    bool attacking = e->IsStateSet( SElectricAttack );

    Vectorf v(dir);
    Vectorf& pos = e->GetPos();
    Vectorf l = attacking ? e->GetDir() : e->GetLook();
    float len = v.Normalize();

    Vectorf vel( 0.0f, 0.0f );

    float s = CrossProduct( l, v );
    float c = DotProduct( l, v );

    if( c < -1 + 0.001 )
    {
        if( g_rng.GetDouble() > 0.5 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }
    }
    else if( c < 1 - 0.001 )
    {
        if( s > 0 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }

        if( CrossProduct( l, v ) * s < 0 )
        {
            l = v;
        }
    }

    if( e->IsSlowedDown() )
    {
        mms *= e->GetSlowDownFactor();
    }

    // Limit movement to desired speed factor
    if( len < 1 )
    {
        vel += l * len * mms;
    }
    else
    {
        vel += l * mms;
    }

    pos += vel;

    const Vectorf& mapMin = GameManager::GetInstance()->GetSegmentableMapMin();
    const Vectorf& mapMax = GameManager::GetInstance()->GetSegmentableMapMax();

    // Clip position to the map size
    if( pos.x < mapMin.x ) { pos.x = mapMin.x; e->CollideObstacle(); }
    if( pos.x > mapMax.x ) { pos.x = mapMax.x; e->CollideObstacle(); }
    if( pos.y < mapMin.y ) { pos.y = mapMin.y; e->CollideObstacle(); }
    if( pos.y > mapMax.y ) { pos.y = mapMax.y; e->CollideObstacle(); }

    // Ignore forced moves in direction settings, just set look vector
    e->SetDir( l );

    if( !attacking )
    {
        e->SetLook( l );
    }
}

void EntityManager::MovePlayer( Entity* e, const Vectorf& dir, float pms )
{
    Vectorf d( dir );
    float pmc = m_playerWeaponSlowDown;
    float pas = m_playerAnimSpeed[0];
    if( m_playerUnderAttack && !m_unstoppable )
    {
        m_attackModifier = 0.79f;
    }
    m_attackModifier = std::min( m_attackModifier + 0.01f, 1.0f );
    if( m_attackModifier != 1.0f )
    {
        pmc *= m_attackModifier;
        pms *= m_attackModifier;
    }

    pmc *= e->m_speedMul;
    pms *= e->m_speedMul;

    if( e->IsSlowedDown() && !GameManager::GetInstance()->GetStats()->CheckPerk( Perks::MrClean ) )
    {
        pmc *= e->GetSlowDownFactor();
        pms *= e->GetSlowDownFactor();
    }

    float len = d.Normalize();
    if( len < 1 )
    {
        d *= len * pms;
    }
    else
    {
        d *= pms;
    }

    PlayerMovementData& pmd = m_pmd[e];

    Vectorf v = d - pmd.pos;
    len = v.Normalize();
    if( len < pmc )
    {
        pmd.pos = d;
    }
    else
    {
        pmd.pos += v * pmc;
    }

    pmd.accum += pmd.pos.Length() * pms;

    while( pmd.accum > pas )
    {
        pmd.frame++;
        if( pmd.frame > 9 )
        {
            if( GameManager::GetInstance()->IsMech() )
            {
                AudioManager::GetInstance()->Play( SFX_MECH_SERVO );
            }
            pmd.frame = 0;
        }
        pmd.accum -= pas;
    }


    Vectorf vel( pmd.pos );

    Vectorf oldpos( e->GetPos() );

    Obstacle::Kind o = GameManager::GetInstance()->CheckObstacleCollision( e->GetPos() + vel );
    if( o == Obstacle::Regular || o == Obstacle::Ground )
    {
        e->SetDir( Vectorf( 0, 0 ) );
    }
    else
    {
        e->GetPos() += vel;
        vel.Normalize();
        e->SetDir( vel );
    }

    // Clip position to the map size
    Vectorf mapMin( Entity::AVERAGE_RADIUS, Entity::AVERAGE_RADIUS );
    Vectorf mapMax( GameManager::GetInstance()->GetMap()->GetSize() );
    mapMax -= mapMin;
    e->GetPos().x = Claw::MinMax( e->GetPos().x, mapMin.x, mapMax.x );
    e->GetPos().y = Claw::MinMax( e->GetPos().y, mapMin.y, mapMax.y );

    e->SetFrame( pmd.frame );

    if( e->GetDir().Length() != 0 && m_playerDirectionalTicks-- < 0 )
    {
        e->SetTarget( e->GetDir() );
    }


    Vectorf t = e->GetTarget();
    Vectorf& l = e->GetLook();
    t.Normalize();

    float s = CrossProduct( l, t );
    float c = DotProduct( l, t );

    float rs = GetData( Entity::Player ).rotSpeed;

    if( c < -1 + 0.001 )
    {
        if( g_rng.GetDouble() > 0.5 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }
    }
    else if( c < 1 - 0.001 )
    {
        if( s > 0 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }

        if( CrossProduct( l, t ) * s < 0 )
        {
            l = t;
        }
    }
}

void EntityManager::MoveAIFriend( Entity* e, const Vectorf& dir, float pms )
{
    Vectorf d( dir );
    float pmc = m_playerWeaponSlowDown;
    float pas = m_playerAnimSpeed[1];

    pmc *= e->m_speedMul;
    pms *= e->m_speedMul;

    if( e->IsSlowedDown() )
    {
        pmc *= e->GetSlowDownFactor();
        pms *= e->GetSlowDownFactor();
    }

    float len = d.Normalize();
    if( len < 1 )
    {
        d *= len * pms;
    }
    else
    {
        d *= pms;
    }

    PlayerMovementData& pmd = m_pmd[e];

    Vectorf v = d - pmd.pos;
    len = v.Normalize();
    if( len < pmc )
    {
        pmd.pos = d;
    }
    else
    {
        pmd.pos += v * pmc;
    }

    pmd.accum += pmd.pos.Length() * pms;

    while( pmd.accum > pas )
    {
        pmd.frame++;
        if( pmd.frame > 9 )
        {
            pmd.frame = 0;
        }
        pmd.accum -= pas;
    }


    Vectorf vel( pmd.pos );

    Vectorf oldpos( e->GetPos() );

    Obstacle::Kind o = GameManager::GetInstance()->CheckObstacleCollision( e->GetPos() + vel );
    if( o == Obstacle::Regular || o == Obstacle::Ground )
    {
        e->SetDir( Vectorf( 0, 0 ) );
    }
    else
    {
        e->GetPos() += vel;
        vel.Normalize();
        e->SetDir( vel );
    }

    // Clip position to the map size
    Vectorf mapMin( Entity::AVERAGE_RADIUS, Entity::AVERAGE_RADIUS );
    Vectorf mapMax( GameManager::GetInstance()->GetMap()->GetSize() );
    mapMax -= mapMin;
    e->GetPos().x = Claw::MinMax( e->GetPos().x, mapMin.x, mapMax.x );
    e->GetPos().y = Claw::MinMax( e->GetPos().y, mapMin.y, mapMax.y );

    e->SetFrame( pmd.frame );

    if( e->GetDir().Length() != 0 && m_playerDirectionalTicks-- < 0 )
    {
        e->SetTarget( e->GetDir() );
    }


    Vectorf t = e->GetTarget();
    Vectorf& l = e->GetLook();
    t.Normalize();

    float s = CrossProduct( l, t );
    float c = DotProduct( l, t );

    float rs = GetData( Entity::Player ).rotSpeed;

    if( c < -1 + 0.001 )
    {
        if( g_rng.GetDouble() > 0.5 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }
    }
    else if( c < 1 - 0.001 )
    {
        if( s > 0 )
        {
            l.Rotate( rs );
        }
        else
        {
            l.Rotate( -rs );
        }

        if( CrossProduct( l, t ) * s < 0 )
        {
            l = t;
        }
    }
}

int EntityManager::l_GrimReaper( struct lua_State* L )
{
    std::vector<Entity*>::iterator it = m_ents.begin();
    while( it != m_ents.end() )
    {
        if( (*it)->GetHitPoints() <= 0 )
        {
            if( (*it)->GetType() == Entity::Player )
            {
                GameManager::GetInstance()->GenerateSplatter( (*it)->GetPos(), 10 + ( g_rng.GetInt() % 20 ) );
                (*it)->SetHitPoints( 1 );
                ++it;
                continue;
                //GameManager::GetInstance()->KilledPlayer( *it );
            }
            else
            {
                const EntityData& ed = GetData( (*it)->GetType() );

                GameManager::GetInstance()->KilledEnemy( *it );
                GameManager::GetInstance()->GetStats()->AddPoints( ed.score );

                if( (*it)->GetType() == Entity::OctopusFriend )
                {
                    (*it)->SetHitPoints( 1 );
                    Claw::Registry::Get()->Set( "/internal/revivecat", true );
                    Claw::Lua* l = GameManager::GetInstance()->GetLua();
                    l->PushBool( true );
                    l->RegisterGlobal( "reviveLock" );
                    GameManager::GetInstance()->l_StartRevive( NULL );
                    ++it;
                    continue;
                }
                else if( (*it)->GetType() == Entity::AIFriend )
                {
                    Claw::Lunar<Entity>::push( *GameManager::GetInstance()->GetLua(), *it );
                    GameManager::GetInstance()->GetLua()->Call( "AIFriendDied", 1, 0 );
                }

                int xp = ed.xp;
                if( GameManager::GetInstance()->GetStats()->CheckPerk( Perks::Wrath ) )
                {
                    xp = (int)ceil( xp * 1.3f );
                }

                if( (*it)->GetHitPoints() <= -5 )
                {
                    // Brutal Kill
                    if( (*it)->CanBleed() )
                    {
                        GameManager::GetInstance()->GenerateSplatter( (*it)->GetPos(), 4 + ( g_rng.GetInt() % 6 ) );
                    }
                    //GameManager::GetInstance()->GetStats()->AddXp( xp * 1.5f );

                    ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
                    ps->Add( new ExplosionEmitter( m_bloodSplat, ps, (*it)->GetPos().x, (*it)->GetPos().y, 300, 300, 0.25f, 30 ) );
                }
                else
                {
                    if( (*it)->CanBleed() )
                    {
                        GameManager::GetInstance()->GenerateSplatter( (*it)->GetPos(), 2 + ( g_rng.GetInt() % 3 ) );
                    }
                    //GameManager::GetInstance()->GetStats()->AddXp( xp );
                }

                int orbtime = GameManager::GetInstance()->GetStats()->CheckPerk( Perks::OrbExtender ) ? 4 : 3;
                int orbs = ed.orbs;
                if( orbs == 1 )
                {
                    GameManager::GetInstance()->GetPickupManager()->Add( (*it)->GetPos() + Vectorf( 0, 0.1f ), Pickup::MagicOrb, orbtime );
                }
                else
                {
                    for( int i=0; i<orbs; i++ )
                    {
                        GameManager::GetInstance()->GetPickupManager()->Add( (*it)->GetPos() + Vectorf( g_rng.GetDouble() * 15.f, g_rng.GetDouble() * 15.f ), Pickup::MagicOrb, orbtime );
                    }
                }

                if( (*it)->GetItem() != Pickup::NumPickups )
                {
                    Vectorf p = (*it)->GetPos();
                    p.y += 0.1f;
                    GameManager::GetInstance()->GetPickupManager()->Add( p, (*it)->GetItem(), 31536000, NULL );
                }
                else if( 
                    ( GameManager::GetInstance()->m_withdrawPerk > 0 || g_rng.GetDouble() < m_data[(*it)->GetType()].moneyChance ) &&
                    GameManager::GetInstance()->GetObstacleManager()->QueryCollision( (*it)->GetPos() ) == NULL )
                {
                    int cash = m_data[(*it)->GetType()].moneyAmount;
                    if( Shop::GetInstance()->CheckOwnership( "cashbonus" ) )
                    {
                        cash *= 110;
                        cash /= 100;
                    }
                    int* ptr = (int*)_malloc( sizeof( int ) );
                    *ptr = cash;
                    Vectorf p = (*it)->GetPos();
                    p.y += 0.1f;
                    if( GameManager::GetInstance()->GetStats()->CheckPerk( Perks::Greed ) )
                    {
                        GameManager::GetInstance()->GetPickupManager()->Add( p, Pickup::Cash, 20, ptr );
                    }
                    else
                    {
                        GameManager::GetInstance()->GetPickupManager()->Add( p, Pickup::Cash, 15, ptr );
                    }
                }
            }

            it = Remove( it );
        }
        else
        {
            ++it;
        }
    }

    return 0;
}

int EntityManager::l_SetEntityData( lua_State* L )
{
    Claw::Lua lua( L );

    Entity::Type idx = lua.CheckEnum<Entity::Type>( 1 );

    m_data[idx].rotSpeed = lua.CheckNumber( 2 );
    m_data[idx].moveSpeed = lua.CheckNumber( 3 );
    m_data[idx].orbs = lua.CheckNumber( 4 );
    m_data[idx].xp = lua.CheckNumber( 5 );
    m_data[idx].damage = lua.CheckNumber( 6 );
    m_data[idx].hp = lua.CheckNumber( 7 );
    m_data[idx].moneyChance = lua.CheckNumber( 8 );
    m_data[idx].moneyAmount = lua.CheckNumber( 9 );
    m_data[idx].score = lua.CheckNumber( 10 );
    m_data[idx].shield = lua.CheckNumber( 11 );
    m_data[idx].shieldScale = lua.CheckNumber( 12 );
    if( lua.IsNumber( 13 ) )
    {
        m_data[idx].orbsDrop = lua.CheckNumber( 13 );
    }
    else
    {
        m_data[idx].orbsDrop = m_data[idx].hp + 100000000;
    }

    return 0;
}

int EntityManager::l_PlayerLookDirectionChanged( lua_State* L )
{
    PlayerLookDirectionChanged();
    return 0;
}

int EntityManager::l_SetupEntityBehaviors( lua_State* L )
{
    SetupEntityBehaviors();
    return 0;
}

int EntityManager::l_UpdateBehaviors( lua_State* L )
{
    UpdateBehaviors( 1.0f );
    return 0;
}

int EntityManager::l_SpawnTickFinished( lua_State* L )
{
    if( m_delayedSpawnGroup.ds.size() != 0 )
    {
        for( std::vector<DelayedSpawn>::iterator it = m_delayedSpawnGroup.ds.begin(); it != m_delayedSpawnGroup.ds.end(); ++it )
        {
            m_delayedSpawnGroup.pos += it->pos;
        }
        m_delayedSpawnGroup.pos /= m_delayedSpawnGroup.ds.size();

        m_dsgList.push_back( m_delayedSpawnGroup );
        m_delayedSpawnGroup.Reset();
    }
    return 0;
}

int EntityManager::l_GetCat( lua_State* L )
{
    for( std::vector<Entity*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( (*it)->GetType() == Entity::OctopusFriend )
        {
            Claw::Lunar<Entity>::push( L, *it );
            return 1;
        }
    }

    CLAW_ASSERT( false );
    return 0;
}

void EntityManager::SetupEntityBehaviors()
{
    m_behaviors[Entity::None] = new EnemyBehavior();
    m_behaviors[Entity::Human] = new PlayerBehavior();

    for( int i = Entity::Human + 1; i < Entity::BehaviorsCount; ++i )
    {
        m_behaviors[i] = new EnemyBehavior();
    }

    m_behaviors[Entity::Human]->AddPotential( new UserControl );
    m_behaviors[Entity::Human]->AddPotential( new AvoidObstacles, 0.1f );
    m_behaviors[Entity::Human]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::Following]->AddPotential( new TargetEnemies );
    m_behaviors[Entity::Following]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::Following]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::Following]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::ShotAvoider]->AddPotential( new TargetEnemies );
    m_behaviors[Entity::ShotAvoider]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::ShotAvoider]->AddPotential( new AvoidShots );
    m_behaviors[Entity::ShotAvoider]->AddPotential( new AvoidLineOfSight );
    m_behaviors[Entity::ShotAvoider]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::ShotAvoider]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::Chaser]->AddPotential( new TargetEnemies );
    m_behaviors[Entity::Chaser]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::Chaser]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::Chaser]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::Interceptor]->AddPotential( new InterceptPlayer );
    m_behaviors[Entity::Interceptor]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::Interceptor]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::Interceptor]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::Roller]->AddPotential( new TargetAim );

    m_behaviors[Entity::RollerTurning]->AddPotential( new InterceptPlayer );
    m_behaviors[Entity::RollerTurning]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::RollerTurning]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::RollerTurning]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::Flying]->AddPotential( new TargetEnemies );
    m_behaviors[Entity::Flying]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::Flying]->AddPotential( new AvoidShots );
    m_behaviors[Entity::Flying]->AddPotential( new AvoidLineOfSight );
    m_behaviors[Entity::Flying]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::Flying]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::FlyingAggressive]->AddPotential( new InterceptPlayer );
    m_behaviors[Entity::FlyingAggressive]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::FlyingAggressive]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::FlyingAggressive]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::HoundBig]->AddPotential( new KeepDistanceToPlayer );
    m_behaviors[Entity::HoundBig]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::HoundBig]->AddPotential( new AvoidShots );
    m_behaviors[Entity::HoundBig]->AddPotential( new AvoidLineOfSight );
    m_behaviors[Entity::HoundBig]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::HoundBig]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::FollowWaypoints]->AddPotential( new GoToWaypoint );
    m_behaviors[Entity::FollowWaypoints]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::FollowWaypoints]->AddPotential( new AvoidObstacles );
    m_behaviors[Entity::FollowWaypoints]->AddPotential( new AvoidMapBorders );

    m_behaviors[Entity::RunAway]->AddPotential( new AvoidAll );
    m_behaviors[Entity::RunAway]->AddPotential( new AvoidObstacles );

    m_behaviors[Entity::HelpPlayer]->AddPotential( new KeepCloseToPlayer );
    m_behaviors[Entity::HelpPlayer]->AddPotential( new FireAtEnemies );
    m_behaviors[Entity::HelpPlayer]->AddPotential( new AvoidObstacles, 0.1f );
    m_behaviors[Entity::HelpPlayer]->AddPotential( new AvoidFriends );
    m_behaviors[Entity::HelpPlayer]->AddPotential( new AvoidMapBorders );
    m_behaviors[Entity::HelpPlayer]->AddPotential( new AvoidShots );
}

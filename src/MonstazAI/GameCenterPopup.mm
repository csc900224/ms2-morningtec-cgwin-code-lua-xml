#include "claw/base/Errors.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/CashTimer.hpp"
#include "MonstazAI/GameCenterPopup.hpp"
#include "MonstazAI/GameCenterManager.hpp"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define GAME_CENTER_POPUP_TAG   837

static bool s_authenticate = false;
extern bool g_popupSraka;

@interface Popup : NSObject
{
}

- (void) show;
- (void) alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex;

@end

@implementation Popup

- (void) show
{
    UIAlertView* popup = [[[UIAlertView alloc]
                          initWithTitle:@"Select Default View"
                          message:@"Please select your preferred service for viewing Leaderboards. Data is still uploaded to both services."
                          delegate:self
                          cancelButtonTitle:@"OpenFeint"
                          otherButtonTitles:@"Game Center", nil] autorelease];

    [popup setTag:GAME_CENTER_POPUP_TAG];
    [popup show];
}

- (void) alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if ( alertView.tag != GAME_CENTER_POPUP_TAG || g_popupSraka )
    {
        return;
    }

    NSString* title = [alertView buttonTitleAtIndex:buttonIndex];
    if ( [title isEqualToString:@"OpenFeint"] )
    {
        GameCenterManager::GetInstance()->SetDefualtGameCenter( GameCenter::OpenFeint );
    }
    else if ( [title isEqualToString:@"Game Center"] )
    {
        GameCenterManager::GetInstance()->SetDefualtGameCenter( GameCenter::GameKit );
    }

    if ( s_authenticate )
    {
        GameCenterManager::GetInstance()->Authenticate();
        s_authenticate = false;
    }
}

@end

static Popup* s_popup = NULL;

void GameCenterPopup::Initialize()
{
    if ( !s_popup )
    {
        s_popup = [[Popup alloc] init];
    }
}

void GameCenterPopup::Release()
{
    if ( s_popup )
    {
        [s_popup release];
        s_popup = NULL;
    }
}

void GameCenterPopup::Show( bool authenticate )
{
    s_authenticate = authenticate;

    CLAW_ASSERT( s_popup );
    [s_popup show];
}

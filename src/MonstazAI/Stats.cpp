#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/Stats.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"

Stats::Stats()
    : m_xp( 0 )
    , m_level( 1 )
    , m_perks( 0 )
    , m_points( 0 )
    , m_multi( 1 )
    , m_fallSpeed( 0 )
    , m_multiKill( 1 )
    , m_multiKillTimer( 0 )
    , m_maxMulti( 1 )
    , m_kills( 0 )
    , m_maxPlayerHP( 1 )
{
    for( int i=0; i<Perks::PerkCount; i++ )
    {
        m_perkState[i] = false;
    }

    m_levels.push_back( 0 );
    m_levels.push_back( 1000000000 );
}

Stats::~Stats()
{
}

void Stats::Update( float dt )
{
    int count = GameManager::GetInstance()->GetEntityManager()->Count();
    float speed = 0.0006f;
    if( count > 30 )
    {
        count = std::min( count - 30, 100 );
        speed += ( 0.002f - 0.0006f ) * count / 100;
    }
    m_fallSpeed += dt * speed;
    m_multi = std::max( 1.f, m_multi - m_fallSpeed );

    if( m_multiKillTimer > 0 )
    {
        m_multiKillTimer -= dt;
        if( m_multiKillTimer <= 0 )
        {
            m_multiKill = 1;
        }
    }
}

void Stats::AddXp( int xp )
{
    m_xp += xp;

    while( m_xp > m_levels[m_level] )
    {
        m_level++;
        m_perks++;

        if( CheckPerk( Perks::Ragenerator ) )
        {
            Entity* e = GameManager::GetInstance()->GetPlayer();
            e->SetHitPoints( std::min( e->GetMaxHitPoints(), e->GetHitPoints() + 25 ) );
        }
    }
}

void Stats::AddPoints( int amount )
{
    CLAW_MSG( "MultiKill: " << m_multiKill );

    //m_points += amount * GetMultiplier() * m_multiKill;
    const float pointsDelta = amount * GetMultiplier();
    m_points += pointsDelta;
    m_multiKill++;
    m_multiKillTimer = 2;

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_GAME_SCORE_CHANGED, pointsDelta );
}

float Stats::GetXpProgress() const
{
    return float( m_levels[m_level] - m_xp ) / ( m_levels[m_level] - m_levels[m_level-1] );
}

void Stats::EnablePerk( Perks::Type perk )
{
    CLAW_ASSERT( !m_perkState[perk] );
    m_perkState[perk] = true;
    m_perks--;

    switch( perk )
    {
    case Perks::Runner:
        GameManager::GetInstance()->GetLua()->Call( "PerkRunner", 0, 0 );
        break;
    case Perks::AmmoManiac:
        GameManager::GetInstance()->GetLua()->Call( "PerkAmmoManiac", 0, 0 );
        break;
    case Perks::FastHands:
        GameManager::GetInstance()->GetLua()->Call( "PerkFastHands", 0, 0 );
        break;
    case Perks::ComeGetSome:
        GameManager::GetInstance()->GetLua()->Call( "PerkComeGetSome", 0, 0 );
        break;
    case Perks::Slaughter:
        GameManager::GetInstance()->SlaughterMode();
        break;
    case Perks::Regeneration:
        GameManager::GetInstance()->GetLua()->Call( "PerkRegeneration", 0, 0 );
        break;
    case Perks::Unstoppable:
        GameManager::GetInstance()->GetEntityManager()->BecomeUnstoppable();
        break;
    case Perks::Rage:
        GameManager::GetInstance()->GetLua()->Call( "PerkRage", 0, 0 );
        break;
    case Perks::Endoskeleton:
        GameManager::GetInstance()->GetLua()->Call( "PerkEndoskeleton", 0, 0 );
        break;
    case Perks::ColdVengeance:
        GameManager::GetInstance()->ColdVengeance();
        GameManager::GetInstance()->GetPickupManager()->WeaponBoost();
        break;
    case Perks::FirstAid:
        GameManager::GetInstance()->GetPickupManager()->FirstAidCourse();
        break;
    case Perks::TeamPlayer:
        GameManager::GetInstance()->GetLua()->Call( "PerkTeamPlayer", 0, 0 );
        break;
    case Perks::MuscleFever:
        GameManager::GetInstance()->GetLua()->Call( "PerkMuscleFever", 0, 0 );
        break;
    case Perks::Withdraw:
        GameManager::GetInstance()->m_withdrawPerk = 20;
        break;
    case Perks::PurfectCat:
        GameManager::GetInstance()->GetLua()->Call( "PerkCat", 0, 0 );
        break;
    case Perks::RussianRoulette:
        if( g_rng.GetDouble() < 0.5f )
        {
            AudioManager::GetInstance()->Play( SFX_MENU_RUSSIAN_WIN );
            GameManager::GetInstance()->Nuke( true );
        }
        else
        {
            AudioManager::GetInstance()->Play( SFX_MENU_RUSSIAN_LOSE );
            GameManager::GetInstance()->GetPlayer()->SetHitPoints( -100 );
        }
        break;
    case Perks::ToughSkin:
    case Perks::OrbExtender:
    case Perks::Greed:
    case Perks::Wrath:
    case Perks::MrClean:
    case Perks::BigBang:
    case Perks::Ragenerator:
    case Perks::ExtraExplosives:
        break;
    default:
        CLAW_ASSERT( false );
        break;
    };

    Claw::Lua* menuLua = GameManager::GetInstance()->GetMenu( true )->GetLua();
    menuLua->PushNumber( perk );
    menuLua->Call( "PerkGetHeader", 1, 1 );
    Claw::NarrowString perkName = menuLua->CheckCString( -1 );
    menuLua->Pop( 1 );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_PERK_USED, 1, perkName, &perk );
}

void Stats::IncreaseMultiplier()
{
    m_multi = GetMultiplier() + 1;
    m_fallSpeed = 0;
    m_maxMulti = std::max( m_maxMulti, (int)m_multi );
}

void Stats::IncreaseKills()
{
    m_kills++;

    int idx = -1;
    switch( m_kills )
    {
    case 10:
        idx = 0;
        break;
    case 25:
        idx = 1;
        break;
    case 50:
        idx = 2;
        break;
    case 75:
        idx = 3;
        break;
    case 100:
        idx = 4;
        break;
    case 150:
        idx = 5;
        break;
    case 200:
        idx = 6;
        break;
    case 250:
        idx = 7;
        break;
    default:
        break;
    }

    if( idx != -1 )
    {
        CLAW_MSG( "Announcer " << idx );

        if( Claw::AudioChannelPtr ac = m_announcer.Lock() )
        {
            AudioManager::GetInstance()->GetMixer()->Remove( ac.GetPtr() );
        }
        m_announcer = AudioManager::GetInstance()->Play( (AudioSfx)(SFX_MULTIKILL1 + idx) );
    }
}

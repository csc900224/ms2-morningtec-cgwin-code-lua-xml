// Internal includes
#include "MonstazAI/LanguageSettings.hpp"


#if defined BUILD_JP
    LanguageSettings::Language LanguageSettings::s_gameLanguage = LanguageSettings::L_JAPAN;
#else
    LanguageSettings::Language LanguageSettings::s_gameLanguage = LanguageSettings::L_ENGLISH;
#endif

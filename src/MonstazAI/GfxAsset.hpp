#ifndef __MONSTAZ_GFXASSET_HPP__
#define __MONSTAZ_GFXASSET_HPP__

#include <map>

#include "claw/base/AssetDict.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/base/WeakRefCounter.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"

class GfxAsset : public Claw::RefCounter, public Claw::WeakRefCounter
{
public:
    GfxAsset( const Claw::NarrowString& path );
    ~GfxAsset();

    void Blit( Claw::Surface* target, float x, float y );
    void Blit( Claw::Surface* target, float x, float y, float scale );
    void BlitAdditive( Claw::Surface* target, float x, float y );
    void Update( float dt ) { m_surface->Update( dt ); }

    Claw::Surface* GetSurface() { return m_surface; }
    const Vectorf& GetPivot() { return m_pivot; }

    static void LoadPivotDB( const char* fn );

private:
    Claw::SurfacePtr m_surface;
    Vectorf m_pivot;

    static std::map<Claw::NarrowString, Vectorf > s_pivotDB;
};

typedef Claw::SmartPtr<GfxAsset> GfxAssetPtr;

ASSETDICT_LOADER( GfxAsset )

#endif

#include "claw/base/AssetDict.hpp"
#include "claw/base/Registry.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/ExplosionManager.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/particle/DustParticle.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"
#include "MonstazAI/particle/ExplosionParticle.hpp"
#include "MonstazAI/particle/GibParticle.hpp"
#include "MonstazAI/VibraController.hpp"

LUA_DECLARATION( Explosion )
{
    METHOD( Explosion, GetPos ),
    METHOD( Explosion, SetPos ),
    {0,0}
};

Explosion::Explosion( lua_State* L )
    : m_radius( 0.0f )
    , m_power( 0.0f )
    , m_dradius( 0.0f )
    , m_dpower( 0.0f )
{
    CLAW_ASSERT( false );
}

Explosion::Explosion( const Vectorf& pos, const Explosion::Params& params, Shot::Type source )
    : m_pos( pos )
    , m_radius( params.radius )
    , m_power( params.power )
    , m_dradius( params.dradius )
    , m_dpower( params.dpower )
    , m_source( source )
{
}

Explosion::~Explosion()
{
}

void Explosion::Init( Claw::Lua* lua )
{
    Claw::Lunar<Explosion>::Register( *lua );
}

bool Explosion::Update( float dt )
{
    m_radius += dt * m_dradius;
    m_power -= dt * m_dpower;

    return m_power > 0;
}

int Explosion::l_GetPos( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_pos[0] );
    lua.PushNumber( m_pos[1] );
    return 2;
}

int Explosion::l_SetPos( lua_State* L )
{
    Claw::Lua lua( L );
    m_pos[0] = lua.CheckNumber( 1 );
    m_pos[1] = lua.CheckNumber( 2 );
    return 0;
}


LUA_DECLARATION( ExplosionManager )
{
    {0,0}
};

ExplosionManager::ExplosionManager( Claw::Lua* lua )
    : m_heatExplosion( Claw::AssetDict::Get<GfxAsset>( "gfx/heat/explo.png.pivot@linear" ) )
    , m_glow( Claw::AssetDict::Get<Claw::Surface>( "gfx/rocket_shadow.png@linear" ) )
    , m_dark( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/dust_dark.png@linear" ) )
    , m_explosionFunctor( new ExplosionParticleFunctor( 192, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/geiser.png@linear" ) ) )
    , m_explosionFunctorDark( new ExplosionParticleFunctor( 256, m_dark, false ) )
{
    Explosion::Init( lua );

    Claw::Lunar<ExplosionManager>::Register( *lua );
    Claw::Lunar<ExplosionManager>::push( *lua, this );
    lua->RegisterGlobal( "ExplosionManager" );

    for( int i=0; i<11; i++ )
    {
        char buf[32];
        sprintf( buf, "gfx/fx/gib_%02i.png@linear", i+1 );
        m_gibs[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }

    m_gibFunctor.Reset( new GibParticleFunctor( m_gibs, 11, false, true ) );
}

ExplosionManager::~ExplosionManager()
{
    for( std::vector<Explosion*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        delete *it;
    }
}

void ExplosionManager::Render( Claw::Surface* target, const Vectorf& offset )
{
}

void ExplosionManager::RenderPost( Claw::Surface* target, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();

    for( std::vector<Explosion*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        Explosion* e = (*it);
        int alpha = (int)( std::min( 1.f, e->m_power ) * 255 );
        m_glow->SetAlpha( alpha );
        Claw::TriangleEngine::BlitAdditive( target, m_glow, e->m_pos.x * scale - offset.x, e->m_pos.y * scale - offset.y, 0, e->m_radius * 0.25f, Vectorf( m_glow->GetSize() / 2 ) );
    }
}

void ExplosionManager::RenderHeat( Claw::Surface* target, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();

    for( std::vector<Explosion*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        Explosion* e = (*it);
        int alpha = (int)( std::min( 1.f, e->m_power ) * 255 );
        m_heatExplosion->GetSurface()->SetAlpha( alpha );
        Claw::TriangleEngine::Blit( target, m_heatExplosion->GetSurface(), ( (*it)->m_pos.x * scale - offset.x ) / 4, ( (*it)->m_pos.y * scale - offset.y ) / 4,
            0, (*it)->m_radius / m_heatExplosion->GetSurface()->GetWidth(), m_heatExplosion->GetPivot() );
    }
}

void ExplosionManager::Update( float dt )
{
    std::vector<Explosion*>::iterator it = m_ents.begin();
    while( it != m_ents.end() )
    {
        if( (*it)->Update( dt ) )
        {
            ApplyDamages( *it );
            ++it;
        }
        else
        {
            delete *it;
            it = m_ents.erase( it );
        }
    }
}

Explosion* ExplosionManager::Add( const Vectorf& pos, const Explosion::Params& params, bool gib, Shot::Type source )
{
    ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
    if( gib )
    {
        ps->Add( new ExplosionEmitter( m_gibFunctor, ps, pos.x, pos.y, 300, 300, 0.25f, 75 ) );
    }
    else
    {
        ps->Add( new ExplosionEmitter( m_explosionFunctor, ps, pos.x, pos.y, 300, 250, 0.25f, 50 ) );
        ps->Add( new ExplosionEmitter( m_explosionFunctorDark, ps, pos.x, pos.y, 120, 100, 0.25f, 10 ) );
        VibraController::GetInstance()->Start( 0.2f, params.power );

        for( int i=0; i<5; i++ )
        {
            ps->Add( new DustParticle( pos.x, pos.y, g_rng.GetDouble() * 60 - 30, g_rng.GetDouble() * 40 - 20,
                    m_dark, NULL, 2 ) );
        }
    }
    Explosion* e = new Explosion( pos, params, source );
    m_ents.push_back( e );
    return e;
}

void ExplosionManager::AddEmitter( const Vectorf& pos, float vel, float time, float particlesPerSec, bool gib /* = false */ )
{
    ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
    if( gib )
    {
        ps->Add( new ExplosionEmitter( m_gibFunctor, ps, pos.x, pos.y, vel, vel, time, particlesPerSec ) );
    }
    else
    {
        ps->Add( new ExplosionEmitter( m_explosionFunctor, ps, pos.x, pos.y, vel, vel, time, particlesPerSec ) );
    }
}

void ExplosionManager::ApplyDamages( const Explosion* e )
{
    GameManager* gm = GameManager::GetInstance();
    Stats* st = gm->GetStats();
    const std::vector<Entity*>& ents = gm->GetEntityManager()->GetEntities();

    float rinv = 1.f / e->m_radius;

    float r2 = e->m_radius * e->m_radius;
    for( std::vector<Entity*>::const_iterator eit = ents.begin(); eit != ents.end(); ++eit )
    {
        if( (*eit)->GetType() == Entity::OctopusFriend ) continue;

        const Vectorf& p = (*eit)->GetPos();
        Vectorf off = p - e->m_pos;
        float distSqr = DotProduct( off, off );
        if( distSqr < r2 )
        {
            float dist = sqrt( distSqr );
            float attenuation = ( e->m_radius - dist ) * rinv;
            float dmg = attenuation * e->m_power;
            if( (*eit)->GetType() == Entity::Player )
            {
                if( GameManager::GetInstance()->GetShieldTime() != 0 ) continue;

                if( e->m_source == Shot::HoundShot )
                {
                    dmg *= 5;
                }
                else if( st->CheckPerk( Perks::ToughSkin ) )
                {
                    dmg *= 0.33;
                }

                (*eit)->Hit( e->m_source, 0, dmg );
                GameManager::GetInstance()->GetHud()->PlayerDamaged( dmg );
            }
            else if( e->m_source != Shot::HoundShot )
            {
                (*eit)->Hit( e->m_source, 0, dmg );
            }
        }
    }
}

#ifndef __MONSTAZ_ACHIEVEMENTS_HPP__
#define __MONSTAZ_ACHIEVEMENTS_HPP__

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/FontEx.hpp"
#include "claw/graphics/ScreenText.hpp"

class Achievements : public Claw::RefCounter
{
public:
    Achievements();
    ~Achievements();

    void Render( Claw::Surface* target );
    void Update( float dt );

    void Show( const Claw::NarrowString& str, int id );

    static Achievements* GetInstance();

private:
    float m_scale;

    Claw::SurfacePtr m_icon[5];
    Claw::FontExPtr m_font1;
    Claw::FontExPtr m_font2;

    Claw::ScreenTextPtr m_text1;
    Claw::ScreenTextPtr m_text2;

    float m_time;
    int m_state;
    float m_pos;
    int m_iconId;
};

typedef Claw::SmartPtr<Achievements> AchievementsPtr;

#endif

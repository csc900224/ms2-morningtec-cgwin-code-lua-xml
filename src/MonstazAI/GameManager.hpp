#ifndef __MONSTAZ_GAMEMANAGER_HPP__
#define __MONSTAZ_GAMEMANAGER_HPP__

#include <vector>

#include "claw/base/SmartPtr.hpp"
#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/menu/MenuInGame.hpp"
#include "MonstazAI/entity/EntityManager.hpp"
#include "MonstazAI/obstacle/ObstacleManager.hpp"
#include "MonstazAI/trigger/TriggerManager.hpp"
#include "MonstazAI/shot/ShotManager.hpp"
#include "MonstazAI/waypoint/WaypointManager.hpp"
#include "MonstazAI/AnimationSet.hpp"
#include "MonstazAI/AnimSurfWrap.hpp"
#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/ExplosionManager.hpp"
#include "MonstazAI/Renderable.hpp"
#include "MonstazAI/Hologram.hpp"
#include "MonstazAI/HUD.hpp"
#include "MonstazAI/PickupManager.hpp"
#include "MonstazAI/Map.hpp"
#include "MonstazAI/Stats.hpp"
#include "MonstazAI/TimeController.hpp"
#include "MonstazAI/particle/ParticleSystem.hpp"
#include "MonstazAI/particle/GeiserParticle.hpp"
#include "MonstazAI/particle/TargetParticle.hpp"
#include "MonstazAI/particle/BomberParticle.hpp"
#include "MonstazAI/particle/GlowParticle.hpp"
#include "MonstazAI/particle/BloodParticle.hpp"
#include "MonstazAI/particle/FlareParticle.hpp"
#include "MonstazAI/TouchControls.hpp"
#include "MonstazAI/entity/effect/EffectRage.hpp"
#include "MonstazAI/entity/effect/EffectShield.hpp"
#include "MonstazAI/PostProcess.hpp"

#define RNDSIZE 1024

class GameManager : public Claw::RefCounter
{
    enum { SplatterSize = 512 };     // Keep this power of two!
    struct SplatterData
    {
        SplatterData() : t( 1 ), invlt( 1 ) {}

        Claw::SurfacePtr g;
        Vectorf p;
        float s;
        float a;
        int f;
        float t;
        float invlt;
        float d;
    };

    struct AnimData
    {
        AnimData( AnimSurfWrapPtr _s, bool leave = false ) : s( _s ), l( leave ) {}

        AnimSurfWrapPtr s;
        bool l;
    };

public:
    enum { STS = 64 };
    enum { SEG_BORDER = 10 };

    struct TouchControl
    {
        TouchControl() : m_enabled( false ) {}
        void Reset() { m_offset0 = m_offset1 = Vectorf( 0, 0 ); }

        Vectorf m_offset0;
        Vectorf m_offset1;
        bool m_enabled;
    };

    struct KeysControl
    {
        enum Key
        {
            KEY_LEFT    = 0,
            KEY_RIGHT,
            KEY_UP,
            KEY_DOWN,

            KEY_NUM
        };

        KeysControl()
        {
            int i = 0;
            while( i < KEY_NUM )
            {
                m_key[i++] = false;
            }
        }

        inline bool IsActive( Key key ) const
        {
            CLAW_ASSERT( key < KEY_NUM );
            return m_key[ key ];
        }

        bool m_key[KEY_NUM];
    };

    LUA_DEFINITION( GameManager );
    GameManager( lua_State* L ) : SCREEN_WIDTH( 0 ), SCREEN_HEIGHT( 0 ), SEGMENT_SEARCH_RADIUS( 0 ) { CLAW_ASSERT( false ); }

    GameManager( const char* filename, Stats* stats );
    ~GameManager();

    void Update( float dt );
    void Render( Claw::Surface* target );
    void RenderHeat( Claw::Surface* target );
    void RenderHud( Claw::Surface* target );
    void RenderDistortion( Claw::Surface* target );
    void RenderFlash( Claw::Surface* target );
    void Focus( bool focus );

    void TouchDown( int x, int y, int button );
    void TouchUp( int x, int y, int button );
    void TouchMove( int x, int y, int button );

    void SetResolution( int w, int h );
    void Load( const char* fn );
    void FinishSetup();
    void MapSetup( Map::Type mapType );

    void ShowPerkMenu( bool show );
    void ShowPauseMenu( bool show );

    void TouchUpdate( const Vectorf& touch0, const Vectorf& touch1 );
    void TouchEnable( bool enable );
    const TouchControl& GetTouchControl() const;

    void DisplayControlsEnable( bool enable );

    void KeyPressed( Claw::KeyCode code );

    void KeysUpdate( Claw::KeyCode code, bool pressed );
    const KeysControl& GetKeysControl() const;
    void KeysClear();

    Claw::Lua* GetLua() { return m_lua; }
    EntityManager* GetEntityManager() { return m_entityManager; }
    ShotManager* GetShotManager() { return m_shotManager; }
    ObstacleManager* GetObstacleManager() { return m_obstacleManager; }
    TriggerManager* GetTriggerManager() { return m_triggerManager; }
    PickupManager* GetPickupManager() { return m_pickupManager; }
    ExplosionManager* GetExplosionManager() { return m_explosionManager; }
    RenderableManager* GetRenderableManager() { return m_renderableManager; }
    WaypointManager* GetWaypointManager() { return m_waypointManager; }
    Map* GetMap() { return m_map; }
    TimeController* GetTimeController() { return m_timeController; }
    AudioManager* GetAudioManager() { return m_audioManager; }
    const Vectori& GetResolution() const { return m_resolution; }
    const Vectorf& GetCamOffset() const { return m_map->GetOffset(); }
    Hud* GetHud() { return m_hud; }
    Stats* GetStats() { return m_stats; }
    MenuInGame* GetMenu( bool force = false ) { return m_menuActive || force ? m_menu.GetPtr() : NULL; }  /// \todo do this properly
    ParticleSystem* GetParticleSystem() { return m_particleSystem; }
    ParticleFunctor* GetLaserGlow( int idx ) { return m_laserGlow[idx]; }
    ParticleFunctor* GetElectricityGlow() { return m_electricityGlow; }
    TouchControls* GetTouchControls() { return m_touch; }

    AnimationSet* GetPlayerSet( int idx );
    AnimationSet* GetMonsterSet( int idx );
    Claw::SurfacePtr GetMonsterDeathFire( Entity::Type eType );
    Claw::SurfacePtr GetMonsterDeathShoot( Entity::Type eType, Vectorf look );

    void GenerateSplatter( const Vectorf& pos, int num );
    void AddExplosionHole( const Vectorf& pos );
    void AddGlopRemains( const Vectorf& pos, Shot::Type shotType );
    void AddSplatter( Claw::Surface* g, Vectorf p, float s, float a );
    void DrawSplatter( Claw::Surface* target, const Vectorf& offset );
    void SlaughterMode() { m_slaughter = true; }
    void ColdVengeance() { m_weaponBoostTime *= 2; }
    float WeaponBoost();
    bool IsWeaponBoostActive() const { return m_weaponBoost > 0; }
    bool NukeAvailable() const { return m_nukeAvailable; }
    bool NukeLaunched() const { return m_nuke == 1; }
    void Nuke( bool free = false );
    void NukeAnim();
    bool AirstrikeAvailable() const { return m_airstrikeAvailable && m_airstrikeTimer <= 0; }
    bool AirstrikeLaunched() const { return m_airstrikeTimer > 0; }
    void Airstrike();
    void HoundAnim( Entity* e, bool even );
    void LobsterAnim( Entity* e, bool even );
    void FireGrenade();
    void PlaceMine();
    void EnableShield();
    void HealthKit( float gain );
    void UseHealthKit();
    bool IsPauseMenu() const { return m_pauseMenu; }
    bool IsGameEnd() const { return m_gameEnd; }
    bool IsAutoaim() const { return m_autoaim; }
    bool IsPlayerMaxHP() const;
    void CrabDigIn( const Vectorf& pos );
    void ShotFlash() { m_shotFlash = 1; }
    void DamageObject( Obstacle* o, float damage );
    void GodMode( float time ) { m_godmode = time; }

    void StartDistortionEffect();
    void StopDistortionEffect( bool force = false );
    void StartFlashEffect();
    void StopFlashEffect();

    float GetSheathedHitMultiplier() const { return m_sheathedHitMultiplier; }
    float ApplyShotHitMultiplier( Shot::Type shotType, Entity::Type entType, float val ) const;

    AnimSurfWrapPtr AddAnimation( Claw::Surface* anim, const Vectorf& pos, bool leave = false );
    AnimSurfWrapPtr AddAnimation( Claw::Surface* anim, const Vectorf& pos, const Vectorf& look, bool leave = false );

    AnimSurfWrapPtr AddHitAnimation( const Vectorf& pos, const Vectorf& vel, bool useBlood, bool leave = false );

    Entity* GetPlayer() { return m_player; }
    void KilledEnemy( Entity* entity );
    void KilledPlayer( Entity* entity );

    Obstacle::Kind CheckObstacleCollision( const Vectorf& pos ) { Obstacle* o = m_obstacleManager->QueryCollision( pos ); return o ? o->GetKind() : Obstacle::None; }

    bool CheckRicochetLastHit();
    void HoundFire();
    void LobsterFire();
    void PlayerUnderAttack( bool val = true ) { m_entityManager->m_playerUnderAttack = val; }
    float GetShieldTime() const { return m_playerShield; }
    int GetWeaponSet() const { return m_weaponSet; }

    int l_CalculateShotAvoidance( lua_State* L );
    int l_CalculateAvoidance( lua_State* L );
    int l_CalculateObstacles( lua_State* L );
    int l_ProcessExplosions( lua_State* L );
    int l_AvoidLineOfSight( lua_State* L );
    int l_ProcessShots( lua_State* L );
    int l_FillSegmentTable( lua_State* L );
    int l_MonstersEatPlayer( lua_State* L );
    int l_MonstersEatFriends( lua_State* L );
    int l_CheckObstacleCollision( lua_State* L );
    int l_SummaryScreen( lua_State* L );
    int l_SetPlayerHP( lua_State* L );
    int l_NextPerkLevel( lua_State* L );
    int l_StartStoryTutorial( lua_State* L );
    int l_Analytics( lua_State* L );
    int l_GetClosestEnemy( lua_State* L );
    int l_GetClosestTarget( lua_State* L );
    int l_SetShieldTime( lua_State* L );
    int l_PlayerChangedWeapon( lua_State* L );
    int l_AddVPadWeapon( lua_State* L );
    int l_VPadWeaponSwitch( lua_State* L );
    int l_StartRevive( lua_State* L );

    Vectorf AvoidLineOfSight( const Entity& e, float angleLimit, float avoidanceForce ) const;

    static GameManager* GetInstance() { return s_instance; }

    Entity** GetSegmentTable() { return m_segmentTable; }
    static int Segmentize( float val ) { return int(val) / STS + SEG_BORDER; }

    const Vectorf& GetSegmentableMapMin() const { return m_mapMin; }
    const Vectorf& GetSegmentableMapMax() const { return m_mapMax; }

    Claw::AnimatedSurface* GetShockedStars() { return (Claw::AnimatedSurface*)m_shockedStars.GetPtr(); }

    void OrbCollected() { m_orbCollected = true; }

    static inline float GetGameScale() { return s_gameScale; }
    static void SetGameScale( float scale ) { s_gameScale = scale; } 

    static Claw::SurfacePtr GetBloodAsset( int idx );
    Hologram* GetHologram() { return m_hologram; }
    ParticleFunctor* GetTargetIndicator() { return m_targetIndicator; }

    Claw::Surface* GetWeaponHUDAsset( const Claw::NarrowString& weapon );
    bool IsMech() const { return m_mech; }

    void DrawBufferSize( int x, int y );
    void SetMenuActive( bool active );

    bool m_renderObstacles;
    float m_withdrawPerk;

    float m_mechTime;
    bool m_mechLoop;

private:
    // Just helper methods for simplify code reading
    void UpdateSplatters( float dt );
    void UpdateAnims( float dt );
    void UpdateHud( float dt );
    void UpdateNuke( float dt );
    void UpdateAirstrike( float dt );
    void UpdateDimming( float dt );
    void UpdateFlash( float dt );
    void UpdateGlopTrace( float dt );


    Claw::Surface* GetAnim( const Claw::NarrowString& base );

    void ShotHit( Shot* shot, bool bloodyHit, bool leaveImpact = true );
    inline bool IsSplatterOnScreen( const SplatterData& splatter )
    {
        Vectori pos( splatter.p.x - m_screenRect.m_x, splatter.p.y - m_screenRect.m_y );
        return !( pos.x < 0 || pos.y < 0 || pos.x > m_screenRect.m_w || pos.y > m_screenRect.m_h );
    }

    Claw::Rect m_screenRect;

    TouchControl m_touchControl;
    KeysControl m_keysControl;

    Claw::LuaPtr m_lua;

    EntityManagerPtr m_entityManager;
    ShotManagerPtr m_shotManager;
    ObstacleManagerPtr m_obstacleManager;
    TriggerManagerPtr m_triggerManager;
    PickupManagerPtr m_pickupManager;
    ExplosionManagerPtr m_explosionManager;
    RenderableManagerPtr m_renderableManager;
    WaypointManagerPtr m_waypointManager;
    MapPtr m_map;
    TimeControllerPtr m_timeController;
    AudioManagerPtr m_audioManager;
    Entity* m_player;
    HudPtr m_hud;
    StatsPtr m_stats;
    ParticleSystemPtr m_particleSystem;
    Entity* m_tracking;
    float m_trackingTime;

    std::vector<SplatterData> m_fadingSplatter;
    SplatterData m_splatter[SplatterSize];
    int m_splatterCnt;
    bool m_slaughter;
    float m_weaponBoost;
    float m_weaponBoostTime;
    bool m_nukeAvailable;
    bool m_airstrikeAvailable;
    int m_nuke;
    Vectorf m_nukePos;
    float m_nukeTimer;
    float m_airstrikeTimer;
    bool m_pauseMenu;
    bool m_survival;
    bool m_bossFight;
    bool m_pauseNotification;
    bool m_menuActive;

    Vectori m_resolution;
    bool m_autoaim;
    bool m_orbCollected;
    const char* m_mapPath;

    Vectorf m_mapMin;
    Vectorf m_mapMax;

    const int SEGMENT_SEARCH_RADIUS;

    AnimationSetPtr m_playerSet[40];
    AnimationSetPtr m_monsterSet[98];
    Claw::SurfacePtr m_crabDigIn[3];
    Claw::SurfacePtr m_blood[6];
    Claw::SurfacePtr m_glop[2];
    Claw::SurfacePtr m_spit[2];
    Claw::SurfacePtr m_explosionHole;
    Claw::SurfacePtr m_explosion;
    Claw::SurfacePtr m_impactShot[2];
    Claw::SurfacePtr m_impactBlood[4];
    Claw::SurfacePtr m_impactPlasma;
    Claw::SurfacePtr m_impactFishGlop;
    Claw::SurfacePtr m_impactSowerSpit;
    Claw::SurfacePtr m_impactSpiral;
    Claw::SurfacePtr m_enemyDeathFire[25];
    Claw::SurfacePtr m_enemyDeathShot[27][8];
    Claw::SurfacePtr m_enemyDeathShotSecond[3];
    Claw::SurfacePtr m_death[2];
    Claw::SurfacePtr m_deathLastFrame[2];
    Claw::SurfacePtr m_shadow;
    Claw::SurfacePtr m_heatNoise;
    Claw::SurfacePtr m_nukeLaunch;
    Claw::SurfacePtr m_nukeShadow;
    Claw::SurfacePtr m_nukePayload;
    Claw::SurfacePtr m_healthAnim;
    Claw::SurfacePtr m_healthGlow;
    Claw::SurfacePtr m_healthParticle[3];
    Claw::SurfacePtr m_shockedStars;
    Claw::SurfacePtr m_shield[4];
    Claw::SurfacePtr m_energyBar;
    Claw::SurfacePtr m_energyBarRed;
    Claw::SurfacePtr m_dimmingGradient;
    Claw::SurfacePtr m_bushThrash[5];
    Claw::SurfacePtr m_bushRat;
    Claw::SurfacePtr m_dustThrash[8];
    Claw::SurfacePtr m_stoneThrash[13];
    Claw::SurfacePtr m_skullThrash[13];
    Claw::SurfacePtr m_cactusThrash[6];
    Claw::SurfacePtr m_iceDust[7];
    Claw::SurfacePtr m_iceThrash[12];
    Claw::SurfacePtr m_clothThrash[5];
    Claw::SurfacePtr m_shotFlashGfx;
    Claw::Color m_dimmingColor;
    Claw::Color m_flashColor;
    ParticleFunctorPtr m_spiralTrail;
    BomberParticleFunctorPtr m_bomber;
    ParticleFunctorPtr m_targetIndicator;
    ParticleFunctorPtr m_laserGlow[2];
    ParticleFunctorPtr m_electricityGlow;
    ParticleFunctorPtr m_bloodSplat;
    ParticleFunctorPtr m_exploFlare;
    ParticleFunctorPtr m_cloneDieFlare;
    HologramPtr m_hologram;

    std::vector<AnimData> m_anims;

    Entity** m_segmentTable;

    MenuInGamePtr m_menu;

    struct Effect
    {
        enum Sate
        {
            ES_OFF,
            ES_IN,
            ES_ON,
            ES_OUT
        };
        Effect() : timer( 0 ), state( ES_OFF ) {}
        Effect::Sate state;
        float timer;
    };

    float m_ticker;
    float m_time;
    bool  m_gameEnd;
    bool  m_gameEndFail;
    float m_gameEndTimer;
    float m_playerLastHit;
    float m_ricochetLastHit;
    float m_sheathedHitMultiplier;
    float m_deathTimer;
    Effect m_distortion;
    Effect m_flash;
    AnimSurfWrapPtr m_deathBody;
    AnimSurfWrapPtr m_deathBodyWhite;
    int m_weaponSet;
    float m_shotFlash;
    float m_godmode;

    float m_playerNextGlopTraceDist;
    Vectorf m_playerLastGlopTracePos;

    const int SCREEN_WIDTH;
    const int SCREEN_HEIGHT;

    ParticleFunctorPtr m_geiserFunctor;

    EffectRage* m_rage;
    EffectShield* m_shieldFx;

    PostProcessPtr m_vengeanceFx;

    TouchControlsPtr m_touch;
    bool m_displayControlsEnabled;

    static float s_gameScale;

    float m_playerShield;
    float m_shieldTime;
    bool m_mech;

    std::map<Claw::NarrowString, Claw::SurfacePtr> m_weaponMap;

    Claw::Int8 m_rndBuf[RNDSIZE];

    static GameManager* s_instance;
};

typedef Claw::SmartPtr<GameManager> GameManagerPtr;

#endif

#ifndef __INCLUDED__GAMEEVENTDISPATCHER__HPP__
#define __INCLUDED__GAMEEVENTDISPATCHER__HPP__

#include "MonstazAI/GameEvent.hpp"

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include "jungle/patterns/Singleton.hpp"

#include <set>

class GameEventDispatcher 
    : public Jungle::Patterns::Singleton< GameEventDispatcher, Jungle::Patterns::ExplicitCreation >
    , public GameEventHandler
{
    friend class Jungle::Patterns::ExplicitCreation<GameEventDispatcher>;

public:
    LUA_DEFINITION( GameEventDispatcher );

    void InitLua( Claw::Lua* lua );

    void RegisterGameEventHandler( GameEventHandler* handler );
    void UnregisterGameEventHandler( GameEventHandler* handler );

    bool HandleGameEvent( const GameEvent& ev );
    bool HandleGameEvent( const GameEvent::Id& id, float value = 0, const Claw::NarrowString& text = "", void* userData = NULL );

    int l_HandleGameEvent( lua_State* L );

protected:
    GameEventDispatcher();

private:
    typedef std::set<GameEventHandler*> GameEventHandlers;
    GameEventHandlers m_handlers;
};

#endif
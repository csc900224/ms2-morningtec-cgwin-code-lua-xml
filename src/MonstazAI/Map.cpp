#include <string.h>

#include "claw/base/AssetDict.hpp"
#include "claw/vfs/Vfs.hpp"

#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/Map.hpp"
#include "MonstazAI/IsoSet.hpp"
#include "MonstazAI/particle/GibParticle.hpp"
#include "MonstazAI/particle/GeiserEmitter.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"
#include "MonstazAI/particle/DustParticle.hpp"

Map::StaticObject::StaticObject( const Vectorf& pos, const char* name, float life )
    : Renderable( pos )
    , m_name( NULL )
    , m_life( life )
    , m_maxLife( life )
    , m_rmaxLife( life == 0 ? 0 : 1.f / life )
    , m_repl( NULL )
    , m_holo( 0 )
    , m_thrash( T_None )
{
    if( name )
    {
        m_name = new char[strlen(name)+1];
        strcpy( m_name, name );
    }
}

Map::StaticObject::~StaticObject()
{
    delete[] m_name;
}

Map::StaticObjectSurface::StaticObjectSurface( const Vectorf& pos, Claw::Surface* gfx, float scale, const char* name, float life )
    : StaticObject( pos, name, life )
    , m_gfx( gfx )
{
    m_worldBV = gfx->GetClipRect();
    m_worldBV.m_x += scale * pos.x;
    m_worldBV.m_y += scale * pos.y;
}

void Map::StaticObjectSurface::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    target->Blit( m_pos.x * scale - offset.x, m_pos.y * scale - offset.y, m_gfx );
    if( m_anim )
    {
        target->Blit( m_pos.x * scale - offset.x, m_pos.y * scale - offset.y, m_anim );
    }
}

Map::StaticObjectIsoSet::StaticObjectIsoSet( const Vectorf& pos, IsoSet* isoset, const char* name, float life )
    : StaticObject( pos, name, life )
    , m_isoset( isoset )
{
    m_isoset->m_obj = this;
}

void Map::StaticObjectIsoSet::Update( float dt )
{
    m_isoset->Update( dt );
    if( m_marker )
    {
        m_marker->Update( dt );
    }
}


LUA_DECLARATION( Map )
{
    METHOD( Map, MoveCamera ),
    METHOD( Map, GetSpawnData ),
    METHOD( Map, SetType ),
    METHOD( Map, KillObject ),
    METHOD( Map, ObjectToDestroy ),
    METHOD( Map, AddMarker ),
    METHOD( Map, RemoveMarker ),
    {0,0}
};

Map::Map( Claw::Lua* lua )
    : m_lua( lua )
    , m_seed( 0 )
    , m_layout( NULL )
    , m_shadow( Claw::AssetDict::Get<Claw::Surface>( "gfx/shadow.png@linear" ) )
    , m_hydrantGeiser( new HydrantParticleFunctor( 192, Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/hydrant_water.png@linear" ) ) )
    , m_dustSpawn( false )
    , m_dustTimer( 0 )
{
    Claw::Lunar<Map>::Register( *lua );
    Claw::Lunar<Map>::push( *lua, this );
    lua->RegisterGlobal( "Map" );

    InitEnum( lua );

    m_hydrantThrash[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_hydrant1.png@linear" ) );
    m_hydrantThrash[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_hydrant2.png@linear" ) );

    char buf[64];
    for( int i=0; i<14; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_bank%02i.png@linear", i+1 );
        m_atmThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<32; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_vario%02i.png@linear", i+1 );
        m_miscThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<19; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_glass%02i.png@linear", i+1 );
        m_glassThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<10; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_trashcan%i.png@linear", i );
        m_garbageThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<15; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_post%02i.png@linear", i+1 );
        m_postThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<15; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_papers%02i.png@linear", i+1 );
        m_newspaperThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<5; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_belka%02i.png@linear", i+1 );
        m_belkaThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<2; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_chain%02i.png@linear", i+1 );
        m_chainThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<3; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_clasp%02i.png@linear", i+1 );
        m_claspThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<5; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_cloth%02i.png@linear", i+1 );
        m_clothThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<11; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_dirtyglass%02i.png@linear", i+1 );
        m_dirtyGlassThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<4; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_dish%02i.png@linear", i+1 );
        m_dishThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<7; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_handle%02i.png@linear", i+1 );
        m_handleThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<2; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_light%02i.png@linear", i+1 );
        m_lightThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<5; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_pipe%02i.png@linear", i+1 );
        m_pipeThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<8; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_rails%02i.png@linear", i+1 );
        m_railsThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<38; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_scrap%02i.png@linear", i+1 );
        m_scrapThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<7; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_screw%02i.png@linear", i+1 );
        m_screwThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<3; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_seal%02i.png@linear", i+1 );
        m_sealThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    m_tireThrash[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_tire.png@linear" ) );
    for( int i=1; i<3; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_tire%02i.png@linear", i+1 );
        m_tireThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<4; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_window%02i.png@linear", i+1 );
        m_windowThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<7; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_wire%02i.png@linear", i+1 );
        m_wireThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<6; i++ )
    {
        sprintf( buf, "gfx/thrash/trash_plank%02i.png@linear", i+1 );
        m_plankThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }

    m_doorThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_door01.png@linear" ) );
    m_engineThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_engine01.png@linear" ) );
    m_gaugeThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_gauge01.png@linear" ) );
    m_radioThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_radio.png@linear" ) );
    m_spoolThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_spool01.png@linear" ) );
    m_stuffThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_stuff01.png@linear" ) );
    m_tubeThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_tube01.png@linear" ) );
    m_turbineThrash.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_turbine01.png@linear" ) );

    m_atmFunctor.Reset( new GibParticleFunctor( m_atmThrash, 14, true, false ) );
    m_miscFunctor.Reset( new GibParticleFunctor( m_miscThrash, 32, true, false ) );
    m_glassFunctor.Reset( new GibParticleFunctor( m_glassThrash, 19, true, false ) );
    m_garbageFunctor.Reset( new GibParticleFunctor( m_garbageThrash, 10, true, false ) );
    m_postFunctor.Reset( new GibParticleFunctor( m_postThrash, 15, true, false ) );
    m_newspaperFunctor.Reset( new GibParticleFunctor( m_newspaperThrash, 15, true, false ) );

    m_belkaFunctor.Reset( new GibParticleFunctor( m_belkaThrash, 5, true, false ) );
    m_chainFunctor.Reset( new GibParticleFunctor( m_chainThrash, 2, true, false ) );
    m_claspFunctor.Reset( new GibParticleFunctor( m_claspThrash, 3, true, false ) );
    m_clothFunctor.Reset( new GibParticleFunctor( m_clothThrash, 5, true, false ) );
    m_dirtyGlassFunctor.Reset( new GibParticleFunctor( m_dirtyGlassThrash, 11, true, false ) );
    m_dishFunctor.Reset( new GibParticleFunctor( m_dishThrash, 4, true, false ) );
    m_handleFunctor.Reset( new GibParticleFunctor( m_handleThrash, 7, true, false ) );
    m_lightFunctor.Reset( new GibParticleFunctor( m_lightThrash, 2, true, false ) );
    m_pipeFunctor.Reset( new GibParticleFunctor( m_pipeThrash, 5, true, false ) );
    m_railsFunctor.Reset( new GibParticleFunctor( m_railsThrash, 8, true, false ) );
    m_scrapFunctor.Reset( new GibParticleFunctor( m_scrapThrash, 38, true, false ) );
    m_screwFunctor.Reset( new GibParticleFunctor( m_screwThrash, 7, true, false ) );
    m_sealFunctor.Reset( new GibParticleFunctor( m_sealThrash, 3, true, false ) );
    m_tireFunctor.Reset( new GibParticleFunctor( m_tireThrash, 3, true, false ) );
    m_windowFunctor.Reset( new GibParticleFunctor( m_windowThrash, 4, true, false ) );
    m_wireFunctor.Reset( new GibParticleFunctor( m_wireThrash, 7, true, false ) );
    m_doorFunctor.Reset( new GibParticleFunctor( &m_doorThrash, 1, true, false ) );
    m_engineFunctor.Reset( new GibParticleFunctor( &m_engineThrash, 1, true, false ) );
    m_gaugeFunctor.Reset( new GibParticleFunctor( &m_gaugeThrash, 1, true, false ) );
    m_radioFunctor.Reset( new GibParticleFunctor( &m_radioThrash, 1, true, false ) );
    m_spoolFunctor.Reset( new GibParticleFunctor( &m_spoolThrash, 1, true, false ) );
    m_stuffFunctor.Reset( new GibParticleFunctor( &m_stuffThrash, 1, true, false ) );
    m_tubeFunctor.Reset( new GibParticleFunctor( &m_tubeThrash, 1, true, false ) );
    m_turbineFunctor.Reset( new GibParticleFunctor( &m_turbineThrash, 1, true, false ) );
    m_plankFunctor.Reset( new GibParticleFunctor( m_plankThrash, 6, true, false ) );
}

void Map::InitEnum( Claw::Lua* lua )
{
    lua->CreateEnumTable();
    lua->AddEnum( Grass );
    lua->AddEnum( City );
    lua->AddEnum( Desert );
    lua->AddEnum( Ice );
    lua->RegisterEnumTable( "MapType" );
}

Map::~Map()
{
    for( std::vector<StaticObject*>::iterator it = m_background.begin(); it != m_background.end(); ++it )
    {
        delete *it;
    }
    for( std::vector<StaticObject*>::iterator it = m_ground.begin(); it != m_ground.end(); ++it )
    {
        delete *it;
    }
    for( std::vector<StaticObject*>::iterator it = m_list.begin(); it != m_list.end(); ++it )
    {
        delete (*it)->m_repl;
        delete *it;
    }

    delete[] m_layout;
}

void Map::Render( Claw::Surface* target )
{
    const float scale = GameManager::GetGameScale();

    Vectori xy = m_size;
    xy.x *= scale;
    xy.y *= scale;

    Vectori tileSize( m_bg[0]->GetWidth(), m_bg[0]->GetHeight() );
    Vectori min = Vectori( GetOffset() ) - tileSize;
    Vectori max = Vectori( GetOffset() ) + m_resolution;

    char* ptr = m_layout;
    for( int i = 0; i < xy.x; i += tileSize.x )
    {
        for( int j = 0; j < xy.y; j += tileSize.y )
        {
            char idx = *ptr++;

            // After that may clip to screen dimensions
            if( i < min.x || i > max.x || j < min.y || j > max.y ) continue;

            target->Blit( i - m_offset.x, j - m_offset.y, m_bg[idx] );
        }
    }

    Claw::Rect screenRect = target->GetClipRect();
    screenRect.m_x += m_offset.x;
    screenRect.m_y += m_offset.y;

    for( std::vector<StaticObject*>::iterator it = m_background.begin(); it != m_background.end(); ++it )
    {
        CLAW_ASSERT( !(*it)->IsIsoSet() );
        StaticObjectSurface* so = (StaticObjectSurface*)*it;
        if( screenRect.IsIntersect( so->GetRenderWorldBV() ) )
        {
            so->Render( target, m_offset, scale );
        }
    }

    for( std::vector<StaticObject*>::iterator it = m_ground.begin(); it != m_ground.end(); ++it )
    {
        CLAW_ASSERT( !(*it)->IsIsoSet() );
        StaticObjectSurface* so = (StaticObjectSurface*)*it;
        if( screenRect.IsIntersect( so->GetRenderWorldBV() ) )
        {
            so->Render( target, m_offset, scale );
        }
    }

    GameManager::GetInstance()->DrawSplatter( target, m_offset );

    RenderableManager* rm = GameManager::GetInstance()->GetRenderableManager();
    for( std::vector<StaticObject*>::iterator it = m_list.begin(); it != m_list.end(); ++it )
    {
        if( (*it)->IsIsoSet() )
        {
            StaticObjectIsoSet* so = (StaticObjectIsoSet*)*it;
            // Culled inside render method
            so->m_isoset->Render( target, m_offset, scale );
        }
        else
        {
            StaticObjectSurface* so = (StaticObjectSurface*)*it;
            if( screenRect.IsIntersect( so->GetRenderWorldBV() ) )
            {
                rm->Add( *it );
            }
        }
        if( (*it)->m_marker )
        {
            rm->Add( (*it)->m_marker );
        }
    }

    for( std::map<const char*, MarkerArrowPtr, Comparator>::iterator it = m_markers.begin(); it != m_markers.end(); ++it )
    {
        rm->Add( it->second );
    }
}

void Map::Update( float dt )
{
    for( std::vector<StaticObject*>::iterator it = m_list.begin(); it != m_list.end(); ++it )
    {
        (*it)->m_holo = std::max( 0.f, (*it)->m_holo - 0.08f );
        (*it)->Update( dt );
    }
    for( std::map<const char*, MarkerArrowPtr, Comparator>::iterator it = m_markers.begin(); it != m_markers.end(); ++it )
    {
        it->second->Update( dt );
    }
    for( std::set<Claw::SurfacePtr>::iterator it = m_animList.begin(); it != m_animList.end(); ++it )
    {
        (*it)->Update( dt );
    }

    if( m_dustSpawn )
    {
        const float scale = GameManager::GetGameScale();

        m_dustTimer += dt;
        while( m_dustTimer > m_dustWait )
        {
            m_dustTimer -= m_dustWait;
            int x = ( m_offset.x + g_rng.GetInt( m_resolution.x ) ) / scale - m_dustVector.x;
            int y = ( m_offset.y + g_rng.GetInt( m_resolution.y ) ) / scale - m_dustVector.y;
            GameManager::GetInstance()->GetParticleSystem()->Add( new DustParticle( x, y,
                m_dustVector.x * ( 0.9f + g_rng.GetDouble() * 0.2f ),
                m_dustVector.y * ( 0.9f + g_rng.GetDouble() * 0.2f ),
                m_dustThrash[g_rng.GetInt( m_dustNum )], m_dustShadow ? m_shadow : (Claw::Surface*)NULL ) );
        }
    }
}

void Map::SetSize( int w, int h )
{
    m_size.x = w;
    m_size.y = h;

    m_bv.SetMinMax( Vectorf( 0, 0 ), Vectorf( w, h ) );

    m_lua->PushNumber( w );
    m_lua->PushNumber( h );
    m_lua->Call( "SetMapSize", 2, 0 );
}

void Map::SetResolution( int w, int h )
{
    m_resolution.x = w;
    m_resolution.y = h;
}

Map::StaticObject* Map::AddBackgroundObject( const Claw::NarrowString& fn, int x, int y )
{
    StaticObject* so = NULL;
    const float scale = GameManager::GetGameScale();
    CLAW_CHECK( Claw::AssetDict::Get<Claw::Surface>( fn ) && Claw::AssetDict::Get<Claw::Surface>( fn )->IsInAtlas(), "Surface " << fn << " not in atlas." );
    so = new StaticObjectSurface( Vectorf( x, y ), Claw::AssetDict::Get<Claw::Surface>( fn ), scale, NULL, 0 );
    m_background.push_back( so );
    return so;
}

Map::StaticObject* Map::AddGroundObject( const Claw::NarrowString& fn, int x, int y )
{
    StaticObject* so = NULL;
    const float scale = GameManager::GetGameScale();
    CLAW_CHECK( Claw::AssetDict::Get<Claw::Surface>( fn ) && Claw::AssetDict::Get<Claw::Surface>( fn )->IsInAtlas(), "Surface " << fn << " not in atlas." );
    so = new StaticObjectSurface( Vectorf( x, y ), Claw::AssetDict::Get<Claw::Surface>( fn ), scale, NULL, 0 );
    m_ground.push_back( so );
    return so;
}

Map::StaticObject* Map::AddStaticObject( const Claw::NarrowString& fn, int x, int y, const char* msname, float mslife, int offset )
{
    StaticObject* so = NULL;
    const float scale = GameManager::GetGameScale();

    if( fn.substr( fn.size() - 3 ) == "ani" )
    {
        so = new StaticObjectIsoSet( Vectorf( x, y ), new IsoSetAnim( fn, Vectorf( x, y ), scale, offset ), msname, mslife );
    }
    else
    {
        Claw::NarrowString isosetfn( Claw::NarrowString( "gfx/assets/" ) + fn.substr( 0, fn.size() - 3 ) + "isoset" );
        Claw::FilePtr f( Claw::OpenFile( isosetfn ) );
        if( f )
        {
            f.Release();
            so = new StaticObjectIsoSet( Vectorf( x, y ), new IsoSet( isosetfn, Vectorf( x, y ), scale, offset ), msname, mslife );

            Claw::NarrowString destrfn( Claw::NarrowString( "gfx/assets/" ) + fn.substr( 0, fn.size() - 4 ) + "_destr.isoset" );
            Claw::FilePtr f( Claw::OpenFile( destrfn ) );
            if( f )
            {
                so->m_repl = new StaticObjectIsoSet( Vectorf( x, y ), new IsoSet( destrfn, Vectorf( x, y ), scale, offset ), 0, 0 );
            }
        }
        else
        {
            CLAW_CHECK( Claw::AssetDict::Get<Claw::Surface>( Claw::NarrowString( "gfx/assets/" ) + fn + "@linear" ) && Claw::AssetDict::Get<Claw::Surface>( Claw::NarrowString( "gfx/assets/" ) + fn + "@linear" )->IsInAtlas(), "Surface " << fn << " not in atlas." );
            so = new StaticObjectSurface( Vectorf( x, y ), Claw::AssetDict::Get<Claw::Surface>( Claw::NarrowString( "gfx/assets/" ) + fn + "@linear" ), scale, msname, mslife );
        }
    }

    m_list.push_back( so );

    return so;
}

void Map::AddSpawnCircle( const Claw::NarrowString& id, float x, float y, float r )
{
    m_spawn.insert( std::make_pair( id, SpawnCircle( x, y, r ) ) );
}

void Map::BuildNamedObjectsMap()
{
    for( std::vector<StaticObject*>::iterator it = m_list.begin(); it != m_list.end(); ++it )
    {
        if( (*it)->m_name )
        {
            m_namedObjs[(*it)->m_name].push_back( *it );
        }
    }
}

const std::vector<Map::StaticObject*>& Map::GetNamedObject( const char* name )
{
    static const std::vector<Map::StaticObject*> empty;

    std::map<const char*, std::vector<StaticObject*>, Comparator>::iterator it = m_namedObjs.find( name );
    if( it != m_namedObjs.end() )
    {
        return it->second;
    }
    return empty;
}

void Map::KillObject( Map::StaticObject* so )
{
    bool iterate = true;
    std::map<const char*, std::vector<StaticObject*>, Comparator>::iterator it = m_namedObjs.begin();
    while( iterate && it != m_namedObjs.end() )
    {
        for( std::vector<StaticObject*>::iterator oit = it->second.begin(); oit != it->second.end(); ++oit )
        {
            if( *oit == so )
            {
                iterate = false;
                it->second.erase( oit );
                break;
            }
        }
        if( it->second.empty() )
        {
            std::map<const char*, std::vector<StaticObject*>, Comparator>::iterator temp = it;
            ++it;
            m_namedObjs.erase( temp );
        }
        else
        {
            ++it;
        }
    }

    const Vectorf& pos = so->GetPos();
    ParticleSystem* ps = GameManager::GetInstance()->GetParticleSystem();
    switch( so->m_thrash )
    {
    case T_None:
        break;
    case T_Hydrant:
        ps->Add( new GibParticle( pos.x + 9, pos.y + 9, g_rng.GetDouble() * 200.f - 100.f, g_rng.GetDouble() * 200.f - 100.f, g_rng.GetDouble() * 75.f + 200.f, m_hydrantThrash[0], m_shadow, 0.5f, true, false ) );
        ps->Add( new GibParticle( pos.x + 9, pos.y + 9, g_rng.GetDouble() * 200.f - 100.f, g_rng.GetDouble() * 200.f - 100.f, g_rng.GetDouble() * 75.f + 200.f, m_hydrantThrash[1], m_shadow, 1, true, false ) );
        ps->Add( new GeiserEmitter( m_hydrantGeiser, ps, pos.x + 9, pos.y + 18, 5, 5, 10, 5 ) );
        break;
    case T_ATM:
        ps->Add( new ExplosionEmitter( m_atmFunctor, ps, pos.x + 12, pos.y + 18, 200, 200, 0.5f, 25 ) );
        ps->Add( new ExplosionEmitter( m_miscFunctor, ps, pos.x + 12, pos.y + 18, 200, 200, 0.5f, 15 ) );
        ps->Add( new ExplosionEmitter( m_glassFunctor, ps, pos.x + 12, pos.y + 18, 200, 200, 0.5f, 15 ) );
        break;
    case T_Garbage:
        ps->Add( new ExplosionEmitter( m_garbageFunctor, ps, pos.x + 20, pos.y + 18, 200, 200, 0.5f, 30 ) );
        ps->Add( new ExplosionEmitter( m_miscFunctor, ps, pos.x + 20, pos.y + 18, 200, 200, 0.5f, 15 ) );
        break;
    case T_Glass:
        ps->Add( new ExplosionEmitter( m_miscFunctor, ps, pos.x + 26, pos.y + 32, 200, 200, 0.5f, 10 ) );
        ps->Add( new ExplosionEmitter( m_glassFunctor, ps, pos.x + 26, pos.y + 32, 200, 200, 0.5f, 30 ) );
        break;
    case T_Mailbox:
        ps->Add( new ExplosionEmitter( m_miscFunctor, ps, pos.x + 24, pos.y + 24, 200, 200, 0.5f, 10 ) );
        ps->Add( new ExplosionEmitter( m_postFunctor, ps, pos.x + 24, pos.y + 24, 200, 200, 0.5f, 30 ) );
        break;
    case T_Newspaper:
        ps->Add( new ExplosionEmitter( m_miscFunctor, ps, pos.x + 15, pos.y + 30, 200, 200, 0.5f, 10 ) );
        ps->Add( new ExplosionEmitter( m_newspaperFunctor, ps, pos.x + 15, pos.y + 30, 200, 200, 0.5f, 30 ) );
        break;
    case T_UFO:
        ps->Add( new ExplosionEmitter( m_dirtyGlassFunctor, ps, pos.x + 120, pos.y + 60, 600, 600, 0.5f, 20 ) );
        ps->Add( new ExplosionEmitter( m_garbageFunctor, ps, pos.x + 120, pos.y + 60, 600, 600, 0.5f, 30 ) );
        ps->Add( new ExplosionEmitter( m_wireFunctor, ps, pos.x + 120, pos.y + 60, 600, 600, 0.5f, 15 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 120, pos.y + 60, 600, 600, 0.5f, 40 ) );
        ps->Add( new ExplosionEmitter( m_screwFunctor, ps, pos.x + 120, pos.y + 60, 600, 600, 0.5f, 15 ) );
        ps->Add( new ExplosionEmitter( m_dishFunctor, ps, pos.x + 120, pos.y + 60, 600, 600, 0.5f, 10 ) );
        for( int i=0; i<40; i++ )
        {
            ps->Add( new DustParticle( pos.x + 120 + g_rng.GetDouble() * 80 - 40, pos.y + 120 + g_rng.GetDouble() * 40 - 20, g_rng.GetDouble() * 80 - 40, g_rng.GetDouble() * 60 - 30,
                    m_dustThrash[g_rng.GetInt( m_dustNum )], NULL ) );
        }
        break;
    case T_Tractor:
        ps->Add( new GibParticle( pos.x + 100, pos.y + 100, g_rng.GetDouble() * 200.f + 1000, g_rng.GetDouble() * 50.f - 25.f, g_rng.GetDouble() * 50.f + 50.f, m_doorThrash, m_shadow, 2, true, false ) );
        ps->Add( new ExplosionEmitter( m_handleFunctor, ps, pos.x + 100, pos.y + 50, 600, 600, 0.5f, 20 ) );
        ps->Add( new ExplosionEmitter( m_pipeFunctor, ps, pos.x + 100, pos.y + 50, 600, 600, 0.5f, 10 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 100, pos.y + 50, 600, 600, 0.5f, 25 ) );
        ps->Add( new ExplosionEmitter( m_screwFunctor, ps, pos.x + 100, pos.y + 50, 600, 600, 0.5f, 10 ) );
        ps->Add( new ExplosionEmitter( m_lightFunctor, ps, pos.x + 100, pos.y + 50, 600, 600, 0.5f, 6 ) );
        for( int i=0; i<30; i++ )
        {
            ps->Add( new DustParticle( pos.x + 100 + g_rng.GetDouble() * 80 - 40, pos.y + 120 + g_rng.GetDouble() * 40 - 20, g_rng.GetDouble() * 60 - 30, g_rng.GetDouble() * 40 - 20,
                    m_dustThrash[g_rng.GetInt( m_dustNum )], NULL ) );
        }
        break;
    case T_Boiler:
        ps->Add( new ExplosionEmitter( m_screwFunctor, ps, pos.x + 80, pos.y + 50, 600, 600, 0.5f, 25 ) );
        ps->Add( new ExplosionEmitter( m_belkaFunctor, ps, pos.x + 80, pos.y + 50, 600, 600, 0.5f, 10 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 80, pos.y + 50, 600, 600, 0.5f, 15 ) );
        ps->Add( new ExplosionEmitter( m_pipeFunctor, ps, pos.x + 80, pos.y + 50, 600, 600, 0.5f, 15 ) );
        for( int i=0; i<25; i++ )
        {
            ps->Add( new DustParticle( pos.x + 80 + g_rng.GetDouble() * 80 - 40, pos.y + 60 + g_rng.GetDouble() * 40 - 20, g_rng.GetDouble() * 60 - 30, g_rng.GetDouble() * 40 - 20,
                    m_dustThrash[g_rng.GetInt( m_dustNum )], NULL ) );
        }
        break;
    case T_Puszka:
        ps->Add( new GibParticle( pos.x + 50, pos.y + 50, g_rng.GetDouble() * 200.f - 100, g_rng.GetDouble() * 50.f - 25.f, g_rng.GetDouble() * 50.f + 150.f, m_sealThrash[1], m_shadow, 2, true, false ) );
        ps->Add( new ExplosionEmitter( m_belkaFunctor, ps, pos.x + 50, pos.y + 50, 600, 600, 0.5f, 15 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 50, pos.y + 50, 600, 600, 0.5f, 10 ) );
        for( int i=0; i<20; i++ )
        {
            ps->Add( new DustParticle( pos.x + 80 + g_rng.GetDouble() * 80 - 40, pos.y + 60 + g_rng.GetDouble() * 40 - 20, g_rng.GetDouble() * 60 - 30, g_rng.GetDouble() * 40 - 20,
                    m_dustThrash[g_rng.GetInt( m_dustNum )], NULL ) );
        }
        break;
    case T_Wall:
        ps->Add( new ExplosionEmitter( m_handleFunctor, ps, pos.x + 50, pos.y + 125, 600, 600, 0.25f, 15 ) );
        ps->Add( new ExplosionEmitter( m_pipeFunctor, ps, pos.x + 50, pos.y + 125, 600, 600, 0.25f, 10 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 50, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_screwFunctor, ps, pos.x + 50, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_wireFunctor, ps, pos.x + 50, pos.y + 125, 600, 600, 0.25f, 20 ) );
        for( int i=0; i<25; i++ )
        {
            ps->Add( new DustParticle( pos.x + 60 + g_rng.GetDouble() * 80 - 40, pos.y + 125 + g_rng.GetDouble() * 40 - 20, g_rng.GetDouble() * 60 - 30, g_rng.GetDouble() * 40 - 20,
                    m_dustThrash[g_rng.GetInt( m_dustNum )], NULL ) );
        }
        break;
    case T_Jet:
        ps->Add( new GibParticle( pos.x + 35, pos.y + 50, g_rng.GetDouble() * 200.f - 100, g_rng.GetDouble() * 50.f - 25.f, g_rng.GetDouble() * 50.f + 250.f, m_turbineThrash, m_shadow, 2, true, false ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 35, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_screwFunctor, ps, pos.x + 35, pos.y + 125, 600, 600, 0.25f, 20 ) );
        for( int i=0; i<25; i++ )
        {
            ps->Add( new DustParticle( pos.x + 35 + g_rng.GetDouble() * 80 - 40, pos.y + 100 + g_rng.GetDouble() * 40 - 20, g_rng.GetDouble() * 60 - 30, g_rng.GetDouble() * 40 - 20,
                    m_dustThrash[g_rng.GetInt( m_dustNum )], NULL ) );
        }
        break;
    case T_Tower:
        ps->Add( new ExplosionEmitter( m_dishFunctor, ps, pos.x + 35, pos.y + 70, 600, 600, 0.25f, 8 ) );
        ps->Add( new ExplosionEmitter( m_pipeFunctor, ps, pos.x + 35, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_wireFunctor, ps, pos.x + 35, pos.y + 125, 600, 600, 0.25f, 20 ) );
        break;
    case T_Titanic:
        ps->Add( new GibParticle( pos.x + 175, pos.y + 170, g_rng.GetDouble() * 200.f - 100, g_rng.GetDouble() * 50.f + 10.f, g_rng.GetDouble() * 50.f + 150.f, m_spoolThrash, m_shadow, 2, true, false ) );
        ps->Add( new ExplosionEmitter( m_chainFunctor, ps, pos.x + 130, pos.y + 140, 900, 800, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_claspFunctor, ps, pos.x + 215, pos.y + 135, 900, 800, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_handleFunctor, ps, pos.x + 190, pos.y + 135, 900, 700, 0.25f, 15 ) );
        ps->Add( new ExplosionEmitter( m_railsFunctor, ps, pos.x + 175, pos.y + 125, 600, 700, 0.25f, 30 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 150, pos.y + 175, 800, 600, 0.25f, 30 ) );
        ps->Add( new ExplosionEmitter( m_windowFunctor, ps, pos.x + 200, pos.y + 150, 800, 700, 0.25f, 15 ) );
        break;
    case T_Tank:
        ps->Add( new ExplosionEmitter( m_windowFunctor, ps, pos.x + 75, pos.y + 75, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 75, pos.y + 75, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_claspFunctor, ps, pos.x + 75, pos.y + 75, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_handleFunctor, ps, pos.x + 75, pos.y + 75, 600, 600, 0.25f, 15 ) );
        break;
    case T_Base:
        ps->Add( new GibParticle( pos.x + 140, pos.y + 30, g_rng.GetDouble() * 200.f - 100, g_rng.GetDouble() * 50.f - 25.f, g_rng.GetDouble() * 50.f + 250.f, m_sealThrash[2], m_shadow, 2, true, false ) );
        ps->Add( new ExplosionEmitter( m_windowFunctor, ps, pos.x + 75, pos.y + 75, 600, 600, 0.25f, 10 ) );
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_claspFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_handleFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_wireFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        break;
    case T_Crates:
        ps->Add( new ExplosionEmitter( m_plankFunctor, ps, pos.x + 65, pos.y + 60, 500, 450, 0.25f, 50 ) );
        ps->Add( new ExplosionEmitter( m_belkaFunctor, ps, pos.x + 75, pos.y + 60, 500, 450, 0.25f, 15 ) );
        break;
    case T_Wychodek:
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 75, pos.y + 60, 600, 600, 0.25f, 30 ) );
        break;
    case T_SiloFull:
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 75, pos.y + 120, 600, 600, 0.25f, 20 ) );
        // fallthrough
    case T_SiloHalf:
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 75, pos.y + 90, 600, 600, 0.25f, 20 ) );
        // fallthrough
    case T_Silo:
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 75, pos.y + 60, 600, 600, 0.25f, 20 ) );
        break;
    case T_Periscope:
        ps->Add( new ExplosionEmitter( m_scrapFunctor, ps, pos.x + 100, pos.y + 125, 600, 600, 0.25f, 30 ) );
        ps->Add( new ExplosionEmitter( m_windowFunctor, ps, pos.x + 100, pos.y + 125, 600, 600, 0.25f, 10 ) );
        break;
    case T_Antenna:
        ps->Add( new ExplosionEmitter( m_claspFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_handleFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        ps->Add( new ExplosionEmitter( m_wireFunctor, ps, pos.x + 125, pos.y + 125, 600, 600, 0.25f, 20 ) );
        break;
    default:
        CLAW_ASSERT( false );
        break;
    }

    Explosion::Params ep( 0.2f, 1.5f, 400, 4 );
    if( GameManager::GetInstance()->GetStats()->CheckPerk( Perks::ExtraExplosives ) )
    {
        ep.dpower *= 0.75f;
    }

    if( so->IsIsoSet() )
    {
        StaticObjectIsoSet* si = (StaticObjectIsoSet*)so;
        const Vectorf& expl = si->GetPos();
        Vectorf size( si->m_isoset->GetWidth(), si->m_isoset->GetHeight() );
        size /= GameManager::GetInstance()->GetGameScale();

        ExplosionManager* em = GameManager::GetInstance()->GetExplosionManager();
        for( int i=0; i<size.x/30; i++ )
        {
            Vectorf pos( expl + Vectorf( ( g_rng.GetDouble() + i ) * 30, ( g_rng.GetDouble() * 0.4f + 0.4f ) * size.y ) );
            em->Add( pos, ep, false, Shot::EnemyHit );
        }
        GameManager::GetInstance()->AddExplosionHole( expl + size * 0.5f );
    }

    if( so->m_repl )
    {
        for( std::vector<StaticObject*>::iterator it = m_list.begin(); it != m_list.end(); ++it )
        {
            if( *it == so )
            {
                *it = so->m_repl;
                delete so;
                break;
            }
        }
    }
}

int Map::l_MoveCamera( lua_State* L )
{
    const float scale = GameManager::GetGameScale();

    Claw::Lua lua( L );

    float x = lua.CheckNumber( 1 );
    float y = lua.CheckNumber( 2 );

    m_offset.x = Claw::MinMax( x * scale, m_resolution.x * 0.5f, m_size.x * scale - m_resolution.x / 2 ) - m_resolution.x / 2;
    m_offset.y = Claw::MinMax( y * scale, m_resolution.y * 0.5f, m_size.y * scale - m_resolution.y / 2 ) - m_resolution.y / 2;

    return 0;
}

int Map::l_GetSpawnData( lua_State* L )
{
    Claw::Lua lua( L );

    Claw::NarrowString id = lua.CheckString( 1 );
    std::map<Claw::NarrowString, SpawnCircle>::iterator it = m_spawn.find( id );
    if( it == m_spawn.end() )
    {
        CLAW_MSG_ASSERT( false, Claw::NarrowString( "Requested nonexistant spawn point " ) + id );
        return 0;
    }

    lua.PushNumber( it->second.x );
    lua.PushNumber( it->second.y );
    lua.PushNumber( it->second.r );

    return 3;
}

int Map::l_SetType( lua_State* L )
{
    const float scale = GameManager::GetGameScale();
    Claw::Lua lua( L );

    m_type = lua.CheckEnum<Type>( 1 );

    GameManager::GetInstance()->MapSetup( m_type );

    int storylevel = Claw::Registry::Get()->CheckInt( "/internal/storylevel" );

    switch( m_type )
    {
    case Grass:
        m_bg[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/background_01.png@linear" ) );
        m_bg[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/background_02.png@linear" ) );
        m_bg[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/background_03.png@linear" ) );
        break;
    case City:
        m_bg[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/city_background.png@linear" ) );
        break;
    case Desert:
        m_bg[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/bck1.png@linear" ) );
        m_bg[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/bck2.png@linear" ) );
        m_bg[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/bck3.png@linear" ) );
        m_dustSpawn = true;
        m_dustVector = Vectorf( 0, 250 );
        m_dustVector.Rotate( g_rng.GetDouble() * M_PI * 2 );
        m_dustVector.y *= 0.8;
        m_dustVector *= g_rng.GetDouble() + 1;
        m_dustWait = g_rng.GetDouble() * 0.1f + 0.05f;
        m_dustShadow = true;
        m_dustNum = 8;
        for( int i=0; i<9; i++ )
        {
            char buf[64];
            sprintf( buf, "gfx/thrash/trash_dust%02i.png@linear", i+1 );
            m_dustThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        }
        break;
    case Ice:
        m_bg[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/bcksnow1.png@linear" ) );
        m_bg[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/bcksnow2.png@linear" ) );
        m_bg[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/bcksnow3.png@linear" ) );
        m_dustSpawn = true;
        m_dustVector = Vectorf( 0, 200 );
        m_dustVector.Rotate( g_rng.GetDouble() * M_PI * 2 );
        m_dustVector.y *= 0.8;
        m_dustVector *= g_rng.GetDouble() + 1;
        m_dustWait = g_rng.GetDouble() * 0.02f + 0.02f;
        m_dustShadow = false;
        m_dustNum = 0;
        m_dustThrash[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/snowflakes.png@linear" ) );
        break;
    default:
        CLAW_ASSERT( false );
        break;
    }

    bool boss = false;
    Claw::Registry::Get()->Get( "/internal/boss", boss );
    if( boss )
    {
        GameManager::GetInstance()->GetAudioManager()->PlayMusic( "boss.ogg" );
    }
    else
    {
        float rnd = g_rng.GetDouble();
        switch( m_type )
        {
        case Grass:
        case City:
            if( rnd < 0.25f )
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "urban.ogg" );
            }
            else if( rnd < 0.5f )
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "urban2.ogg" );
            }
            else if( rnd < 0.75f )
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "urban3.ogg" );
            }
            else
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "urban4.ogg" );
            }
            break;
        case Desert:
            if( rnd < 0.5f )
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "desert.ogg" );
            }
            else
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "desert2.ogg" );
            }
            break;
        case Ice:
            if( rnd < 0.5f )
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "ice.ogg" );
            }
            else
            {
                GameManager::GetInstance()->GetAudioManager()->PlayMusic( "ice2.ogg" );
            }
            break;
        default:
            break;
        }
    }

    // Calculate map layout.
    m_rng.SetSeed( m_seed );

    Vectori xy = m_size;
    xy.x *= scale;
    xy.y *= scale;

    Vectori tileSize( m_bg[0]->GetWidth(), m_bg[0]->GetHeight() );

    int cnt = 0;
    for( int i = 0; i < xy.x; i += tileSize.x )
    {
        for( int j = 0; j < xy.y; j += tileSize.y )
        {
            cnt++;
        }
    }

    delete[] m_layout;
    m_layout = new char[cnt];
    char* ptr = m_layout;

    for( int i = 0; i < xy.x; i += tileSize.x )
    {
        for( int j = 0; j < xy.y; j += tileSize.y )
        {
            int idx = 0;
            int r;

            switch( m_type )
            {
            case Grass:
            case Desert:
            case Ice:
                idx = m_rng.GetInt() % 3;
                break;
            case City:
                idx = 0;
                break;
            default:
                CLAW_ASSERT( false );
                break;
            }

            *ptr++ = idx;
        }
    }

    return 0;
}

int Map::l_KillObject( lua_State* L )
{
    Claw::Lua lua( L );

    std::vector<StaticObject*> v = GetNamedObject( lua.CheckCString( 1 ) );
    for( std::vector<StaticObject*>::const_iterator it = v.begin(); it != v.end(); ++it )
    {
        KillObject( *it );
    }

    return 0;
}

int Map::l_ObjectToDestroy( lua_State* L )
{
    Claw::Lua lua( L );

    std::vector<StaticObject*> v = GetNamedObject( lua.CheckCString( 1 ) );
    CLAW_ASSERT( !v.empty() );

    for( std::vector<StaticObject*>::const_iterator it = v.begin(); it != v.end(); ++it )
    {
        CLAW_ASSERT( (*it)->IsIsoSet() );
        StaticObjectIsoSet* so = (StaticObjectIsoSet*)*it;

        so->m_marker.Reset( new MarkerArrow( so->GetPos() + Vectorf( so->m_isoset->GetWidth() / ( 2 * GameManager::GetGameScale() ), 0 ),
            so->m_isoset->GetHeight() / GameManager::GetGameScale(), MarkerArrow::Target ) );
    }

    return 0;
}

void Map::AddMarker( const char* key, const Vectorf& pos, bool exit )
{
    CLAW_ASSERT( m_markers.find( key ) == m_markers.end() );
    char* buf = new char[strlen(key)+1];
    strcpy( buf, key );
    m_markers.insert( std::make_pair( buf, MarkerArrowPtr( new MarkerArrow( Vectorf( pos.x, pos.y - 60 ), 80, exit ? MarkerArrow::Exit : MarkerArrow::AccessCard ) ) ) );
}

void Map::RemoveMarker( const char* key )
{
    std::map<const char*, MarkerArrowPtr, Comparator>::iterator it = m_markers.find( key );
    CLAW_ASSERT( it != m_markers.end() );
    const char* ptr = it->first;
    m_markers.erase( it );
    delete[] ptr;
}

int Map::l_AddMarker( lua_State* L )
{
    Claw::Lua lua( L );
    AddMarker( lua.CheckCString( 1 ), Vectorf( lua.CheckNumber( 2 ), lua.CheckNumber( 3 ) ), true );
    return 0;
}

int Map::l_RemoveMarker( lua_State* L )
{
    Claw::Lua lua( L );
    RemoveMarker( lua.CheckCString( 1 ) );
    return 0;
}

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/system/Alloc.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/Shop.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/PickupManager.hpp"
#include "MonstazAI/entity/effect/EffectBoost.hpp"
#include "MonstazAI/entity/effect/EffectCash.hpp"
#include "MonstazAI/VibraController.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"

LUA_DECLARATION( Pickup )
{
    METHOD( Pickup, GetPos ),
    METHOD( Pickup, SetPos ),
    METHOD( Pickup, GetType ),
    {0,0}
};

Pickup::Pickup( const Vectorf& pos, Type type, float life, Claw::Surface* anim, void* data )
    : Renderable( pos )
    , m_type( type )
    , m_life( life )
    , m_anim( (Claw::AnimatedSurface*)anim )
    , m_frame( 0 )
    , m_time( m_anim ? m_anim->GetTime( 0 ) : 0 )
    , m_zoom( m_anim ? 0.01f : 1 )
    , m_data( data )
{
    if( type == Potato )
    {
        char buf[32];
        sprintf( buf, "0x%x", this );
        GameManager::GetInstance()->GetMap()->AddMarker( buf, Vectorf( pos.x, pos.y ), false );
    }
}

Pickup::~Pickup()
{
    if( m_type == Potato )
    {
        char buf[32];
        sprintf( buf, "0x%x", this );
        GameManager::GetInstance()->GetMap()->RemoveMarker( buf );
    }

    _free( m_data );
}

void Pickup::Init( Claw::Lua* lua )
{
    Claw::Lunar<Pickup>::Register( *lua );
}

bool Pickup::Update( float dt )
{
    if( m_anim )
    {
        m_time -= dt;

        while( m_time < 0 )
        {
            m_anim->SetFrame( m_frame );
            if( !m_anim->NextFrame() )
            {
                m_anim.Release();
                m_time = 0;
            }
            else
            {
                m_frame = m_anim->GetFrame();
                m_time += m_anim->GetTime( m_frame );
            }
        }
    }

    if( m_life < 0.25 )
    {
        m_zoom = m_life * 4;
    }
    else
    {
        m_zoom += dt * 2;
    }
    m_life -= dt;
    return m_life > 0;
}

void Pickup::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    PickupManager* pm = GameManager::GetInstance()->GetPickupManager();
    float time = pm->GetTime();

    int idx = ( (int)( time * 20 ) ) % 12;
    float mult = scale;

    Claw::Rect viewRect = target->GetClipRect();
    Claw::Rect clipRect;
    Vectorf pos;
    clipRect.m_x = pos.x = m_pos.x * mult - offset.x;
    clipRect.m_y = pos.y = m_pos.y * mult - offset.y;

    if( m_anim )
    {
        clipRect.m_x -= m_anim->GetClipRect().m_x;
        clipRect.m_y -= m_anim->GetClipRect().m_y;
        clipRect.m_w = m_anim->GetClipRect().m_w;
        clipRect.m_h = m_anim->GetClipRect().m_h;
        if( viewRect.IsIntersect( clipRect ) )
        {
            target->Blit( pos.x, pos.y, m_anim );
        }
    }
    else
    {
        GfxAsset* glow = pm->GetGlow( m_type > static_cast<Type>(LastWeapon), idx );
        clipRect.m_x -= (int)glow->GetPivot().x;
        clipRect.m_y -= (int)glow->GetPivot().y;
        clipRect.m_w = glow->GetSurface()->GetClipRect().m_w;
        clipRect.m_h = glow->GetSurface()->GetClipRect().m_h;
        if( viewRect.IsIntersect( clipRect ) )
        {
            glow->Blit( target, pos.x, pos.y );
        }
    }
    pos.y -= mult * (10 - 5 * sin( time * 5 ));

    GfxAsset* pickup = pm->GetPickup( m_type );
    clipRect.m_x = pos.x - (int)pickup->GetPivot().x;
    clipRect.m_y = pos.y - (int)pickup->GetPivot().y;
    clipRect.m_w = pickup->GetSurface()->GetClipRect().m_w;
    clipRect.m_h = pickup->GetSurface()->GetClipRect().m_h;
    if( m_zoom < 1 )
    {
        Claw::TriangleEngine::Blit( target, pickup->GetSurface(), pos.x, pos.y,
            0, m_zoom, pickup->GetPivot() );
    }
    else
    {
        if( viewRect.IsIntersect( clipRect ) )
        {
            pickup->Blit( target, pos.x, pos.y );
        }
    }
}

int Pickup::l_GetPos( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_pos[0] );
    lua.PushNumber( m_pos[1] );
    return 2;
}

int Pickup::l_SetPos( lua_State* L )
{
    Claw::Lua lua( L );
    m_pos[0] = lua.CheckNumber( 1 );
    m_pos[1] = lua.CheckNumber( 2 );
    return 0;
}

int Pickup::l_GetType( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_type );
    return 1;
}


PickupOrb::PickupOrb( const Vectorf& pos, Type type, float life, void* data )
    : Pickup( pos, type, life, NULL, data )
{
}

void PickupOrb::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    PickupManager* pm = GameManager::GetInstance()->GetPickupManager();

    pm->GetMagicOrb()->SetAlpha( std::min( 1.f, m_life * 4.f ) * 255 );

    target->Blit( m_pos.x * scale - offset.x, m_pos.y * scale - offset.y, pm->GetMagicOrb() );
}


LUA_DECLARATION( PickupManager )
{
    METHOD( PickupManager, Add ),
    METHOD( PickupManager, Remove ),
    METHOD( PickupManager, CollectCash ),
    {0,0}
};

PickupManager::PickupManager( Claw::Lua* lua )
    : m_time( 0 )
    , m_magicOrb( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/orb.ani" ) )
    , m_itemPickup( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/glow/item_collect.ani" ) )
    , m_weaponPickup( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/glow/weapon_collect.ani" ) )
    , m_itemAppear( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/glow/item_appear.ani" ) )
    , m_weaponAppear( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/glow/weapon_appear.ani" ) )
    , m_itemDisappear( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/glow/item_disappear.ani" ) )
    , m_weaponDisappear( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/glow/weapon_disappear.ani" ) )
    , m_cashAnim( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/money.ani" ) )
    , m_boostAnim( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/quad_skull.ani" ) )
    , m_boostLoop( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/quad.ani" ) )
    , m_textFunctor( new TextParticleFunctor )
    , m_hpGain( 0.3f )
{
    if( Shop::GetInstance()->CheckOwnership( "medkitbonus" ) )
    {
        m_hpGain = 0.5f;
    }

    Pickup::Init( lua );

    Claw::Lunar<PickupManager>::Register( *lua );
    Claw::Lunar<PickupManager>::push( *lua, this );
    lua->RegisterGlobal( "PickupManager" );

    InitEnum( lua );

    for( int i=0; i<12; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/items/glow/items_glow-%02i.png.pivot@linear", i+1 );
        m_glowItem[i].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
        sprintf( buf, "gfx/items/glow/guns_glow-%02i.png.pivot@linear", i+1 );
        m_glowWeapon[i].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
    }

    m_pickup[Pickup::ElectricityGun].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_electricity_gun.png.pivot@linear" ) );
    m_pickup[Pickup::Flamer].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_flamer.png.pivot@linear" ) );
    m_pickup[Pickup::Minigun].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_minigun.png.pivot@linear" ) );
    m_pickup[Pickup::PlasmaGun].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_plasma.png.pivot@linear" ) );
    m_pickup[Pickup::Railgun].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_rail.png.pivot@linear" ) );
    m_pickup[Pickup::RocketLauncher].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_rocket.png.pivot@linear" ) );
    m_pickup[Pickup::Shotgun].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_shotgun.png.pivot@linear" ) );
    m_pickup[Pickup::CombatShotgun].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_grenade.png.pivot@linear" ) );
    m_pickup[Pickup::SMG].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_SMG.png.pivot@linear" ) );
    m_pickup[Pickup::Ripper].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_saw.png.pivot@linear" ) );
    m_pickup[Pickup::Spiral].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_saw.png.pivot@linear" ) );
    m_pickup[Pickup::Health].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_aid_kit.png.pivot@linear" ) );
    m_pickup[Pickup::WeaponBoost].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_weapon_boost.png.pivot@linear" ) );
    m_pickup[Pickup::Cash].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/pickup_cash.png.pivot@linear" ) );
    m_pickup[Pickup::Potato].Reset( Claw::AssetDict::Get<GfxAsset>( "gfx/items/access_card.png.pivot@linear" ) );
    m_pickup[Pickup::Token].Reset( Claw::AssetDict::Get<GfxAsset>( "menu2/token_small.png.pivot@linear" ) );
}

PickupManager::~PickupManager()
{
    for( std::vector<Pickup*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        delete *it;
    }
}

void PickupManager::InitEnum( Claw::Lua* lua )
{
    lua->CreateEnumTable();
    lua->AddEnum( Pickup::ElectricityGun );
    lua->AddEnum( Pickup::Flamer );
    lua->AddEnum( Pickup::Minigun );
    lua->AddEnum( Pickup::PlasmaGun );
    lua->AddEnum( Pickup::Railgun );
    lua->AddEnum( Pickup::RocketLauncher );
    lua->AddEnum( Pickup::Shotgun );
    lua->AddEnum( Pickup::CombatShotgun );
    lua->AddEnum( Pickup::SMG );
    lua->AddEnum( Pickup::Ripper );
    lua->AddEnum( Pickup::Spiral );
    lua->AddEnum( Pickup::Health );
    lua->AddEnum( Pickup::WeaponBoost );
    lua->AddEnum( Pickup::Cash );
    lua->AddEnum( Pickup::Potato );
    lua->AddEnum( Pickup::MagicOrb );
    lua->AddEnum( Pickup::Vortex );
    lua->AddEnum( Pickup::Chainsaw );
    lua->AddEnum( Pickup::LineGun );
    lua->AddEnum( Pickup::Magnum );
    lua->AddEnum( Pickup::Nailer );
    lua->AddEnum( Pickup::MechFlamer );
    lua->AddEnum( Pickup::MechRocket );
    lua->AddEnum( Pickup::Token );
    lua->RegisterEnumTable( "PickupType" );
}

void PickupManager::Render( Claw::Surface* target, const Vectorf& offset )
{
    RenderableManager* rm = GameManager::GetInstance()->GetRenderableManager();
    for( std::vector<Pickup*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        rm->Add( *it );
    }
}

void PickupManager::Update( float dt )
{
    Entity* player = GameManager::GetInstance()->GetPlayer();
    if( !player ) return;

    m_time += dt;

    m_magicOrb->Update( dt );

    std::vector<Pickup*>::iterator it = m_ents.begin();
    while( it != m_ents.end() )
    {
        bool remove = false;

        if( (*it)->Update( dt ) )
        {
            // magic vacuum
            if( (*it)->m_type == Pickup::MagicOrb )
            {
                const Vectorf& pp = GameManager::GetInstance()->GetPlayer()->GetPos();
                Vectorf d( pp - (*it)->GetPos() );
                float l = DotProduct( d, d );
                float v = GameManager::GetInstance()->GetStats()->CheckPerk( Perks::OrbExtender ) ? 95 : 85;
                if( l < v * v )
                {
                    l = sqrt( l );
                    d /= l;
                    l = ( v - l );
                    d *= l * dt * 4.5f;
                    (*it)->GetPos() += d;
                }
            }

            Vectorf d = player->GetPos() - (*it)->GetPos();
            float l = DotProduct( d, d );
            if( (*it)->m_type == Pickup::MagicOrb )
            {
                if( l < 10*10 )
                {
                    remove = true;
                    int xp = 5;
                    if( GameManager::GetInstance()->GetStats()->CheckPerk( Perks::Wrath ) )
                    {
                        xp = 8;
                    }
                    GameManager::GetInstance()->GetStats()->IncreaseMultiplier();
                    GameManager::GetInstance()->GetStats()->AddXp( xp );
                    GameManager::GetInstance()->GetAudioManager()->Play( SFX_ORB );
                    GameManager::GetInstance()->OrbCollected();
                }
            }
            else
            {
                if( l < 30*30 )
                {
                    remove = true;
                    switch( (*it)->m_type )
                    {
                    case Pickup::Health:
                        {
                        float maxhp = GameManager::GetInstance()->GetStats()->GetMaxPlayerHP();
                        if( Claw::Registry::Get()->CheckBool( "/internal/catlevel" ) || player->GetHitPoints() < maxhp * 0.99f )
                        {
                            GameManager::GetInstance()->HealthKit( m_hpGain );
                        }
                        else
                        {
                            remove = false;
                        }
                        break;
                        }
                    case Pickup::WeaponBoost:
                        WeaponBoost();
                        break;
                    case Pickup::Cash:
                        {
                        int cash = 10;
                        if( (*it)->m_data )
                        {
                            cash = *((int*)(*it)->m_data);
                        }
                        Claw::Registry::Get()->Set( "/internal/money", Claw::Registry::Get()->CheckInt( "/internal/money" ) + cash );
                        Shop::GetInstance()->UpdateCash( cash, 0 );
                        player->AddEffect( new EffectCash( player, m_cashAnim, GameManager::GetGameScale() ) );
                        GameManager::GetInstance()->GetAudioManager()->Play( SFX_CASH );

                        char buf[32];
                        sprintf( buf, "+ %i%c%c ", cash, 194, 156 );
                        GameManager::GetInstance()->GetParticleSystem()->Add( (*m_textFunctor)( player->GetPos().x, player->GetPos().y, buf ) );
                        break;
                        }
                    case Pickup::Potato:
                        GameManager::GetInstance()->GetAudioManager()->Play( SFX_CASH );
                        GameManager::GetInstance()->GetLua()->Call( "PotatoPickup", 0, 0 );
                        break;
                    case Pickup::Token:
                        Claw::Registry::Get()->Set( "/internal/tokens", Claw::Registry::Get()->CheckInt( "/internal/tokens" ) );
                        break;
                    default:
                        GameManager::GetInstance()->GetLua()->PushNumber( (*it)->m_type );
                        GameManager::GetInstance()->GetLua()->Call( "PickupWeapon", 1, 0 );
                        GameManager::GetInstance()->GetAudioManager()->Play( SFX_WEAPON_PICKUP );
                        break;
                    }

                    if( remove )
                    {
                        if( (*it)->m_type > static_cast<Pickup::Type>(Pickup::LastWeapon) )
                        {
                            GameManager::GetInstance()->AddAnimation( m_itemPickup, (*it)->GetPos() );
                        }
                        else
                        {
                            GameManager::GetInstance()->AddAnimation( m_weaponPickup, (*it)->GetPos() );
                        }
                        VibraController::GetInstance()->StartVfx( VibraController::VFX_PLAYER_COLLECT_ITEM );
                    }
                }
            }
        }
        else
        {
            remove = true;

            if( (*it)->m_type != Pickup::MagicOrb )
            {
                GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_PICKUP_DISAPPEAR, (*it)->GetPos() );

                if( (*it)->m_type > static_cast<Pickup::Type>(Pickup::LastWeapon) )
                {
                    GameManager::GetInstance()->AddAnimation( m_itemDisappear, (*it)->GetPos() );
                }
                else
                {
                    GameManager::GetInstance()->AddAnimation( m_weaponDisappear, (*it)->GetPos() );
                }
            }
        }

        if( remove )
        {
            float value = 1.0f;
            if( (*it)->m_type == Pickup::Cash )
            {
                value = (float)*((int*)(*it)->m_data);
            }
            GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_OBJECT_PICKUP, value, "", *it );
            delete *it;
            it = m_ents.erase( it );
        }
        else
        {
            ++it;
        }
    }
}

Pickup* PickupManager::Add( const Vectorf& pos, Pickup::Type type, float life, void* data )
{
    Pickup* pickup;
    if( type == Pickup::MagicOrb )
    {
        pickup = new PickupOrb( pos, type, life, data );
    }
    else
    {
        GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_PICKUP_APPEAR, pos );

        if( type > static_cast<Pickup::Type>(Pickup::LastWeapon) )
        {
            pickup = new Pickup( pos, type, life, m_itemAppear, data );
        }
        else
        {
            pickup = new Pickup( pos, type, life, m_weaponAppear, data );
        }
    }

    m_ents.push_back( pickup );

    return pickup;
}

int PickupManager::l_Add( lua_State* L )
{
    Claw::Lua lua( L );

    float px = lua.CheckNumber( 1 );
    float py = lua.CheckNumber( 2 );
    Pickup::Type type = lua.CheckEnum<Pickup::Type>( 3 );
    float life = lua.CheckNumber( 4 );

    Claw::Lunar<Pickup>::push( L, Add( Vectorf( px, py ), type, life ) );

    return 1;
}

int PickupManager::l_Remove( lua_State* L )
{
    Claw::Lua lua( L );
    Pickup* pickup = Claw::Lunar<Pickup>::check( L, 1 );

    if( pickup->m_type != Pickup::MagicOrb )
    {
        if( pickup->m_type > static_cast<Pickup::Type>(Pickup::LastWeapon) )
        {
            GameManager::GetInstance()->AddAnimation( m_itemPickup, pickup->GetPos() );
        }
        else
        {
            GameManager::GetInstance()->AddAnimation( m_weaponPickup, pickup->GetPos() );
        }
    }

    for( std::vector<Pickup*>::iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( *it == pickup )
        {
            m_ents.erase( it );
            break;
        }
    }
    delete pickup;
    return 0;
}

int PickupManager::l_CollectCash( lua_State* L )
{
    bool playSound = false;

    std::vector<Pickup*>::iterator it = m_ents.begin();
    while( it != m_ents.end() )
    {
        Pickup* p = *it;
        if( p->m_type == Pickup::Cash )
        {
            int cash = 10;
            if( p->m_data )
            {
                cash = *((int*)p->m_data);
            }
            Shop::GetInstance()->UpdateCash( cash, 0 );

            char buf[32];
            sprintf( buf, "+ %i", cash );
            GameManager::GetInstance()->GetParticleSystem()->Add( (*m_textFunctor)( p->GetPos().x, p->GetPos().y, buf ) );
            GameManager::GetInstance()->AddAnimation( m_itemPickup, p->GetPos() );

            Claw::Registry::Get()->Set( "/internal/money", Claw::Registry::Get()->CheckInt( "/internal/money" ) + cash );

            it = m_ents.erase( it );
            playSound = true;
        }
        else
        {
            ++it;
        }
    }

    if( playSound )
    {
        GameManager::GetInstance()->GetAudioManager()->Play( SFX_CASH );
    }

    return 0;
}

void PickupManager::WeaponBoost()
{
    Entity* player = GameManager::GetInstance()->GetPlayer();
    if( !player ) return;

    float time = GameManager::GetInstance()->WeaponBoost();
    player->AddEffect( new EffectBoost( player, m_boostAnim, m_boostLoop, time, GameManager::GetGameScale(), GameManager::GetInstance()->IsMech() ? 2 : 1 ) );
    GameManager::GetInstance()->GetAudioManager()->Play( SFX_QUAD );
    GameManager::GetInstance()->GetLua()->Call( "StopReload", 0, 0 );
}

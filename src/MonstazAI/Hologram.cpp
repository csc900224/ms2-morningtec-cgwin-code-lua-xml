#include "claw/compat/Platform.h"
#include "claw/graphics/pixeldata/PixelDataGL.hpp"
#include "claw/graphics/Batcher.hpp"

#include "MonstazAI/Hologram.hpp"
#include "MonstazAI/GameManager.hpp"

static const char* vertex =
    "uniform highp float offset;\n"
    "uniform highp float screenMul;\n"
    "varying highp float vPos;\n"
    "void main(void)\n"
    "{\n"
    "TEXTURE;\n"
    "vPos = ( inPos.y + offset ) / screenMul;\n"
    "GLPOSITION;\n"
    "}";

static const char* fragment =
    "uniform mediump float t;\n"
    "uniform lowp float intensity;\n"
    "uniform highp float texSize;\n"
    "uniform highp float screenMul;\n"
    "varying highp float vPos;\n"
    "void main(void)\n"
    "{\n"
    "mediump float s = screenMul * 0.33 * ( sin( ( t + vPos * 0.025 ) * 10.0 ) + sin( ( t + vPos * 0.0082 ) * 20.0 ) + sin( ( t + vPos * 0.002 ) * 6.0 ) );\n"
    "mediump float a = ( sin( ( t + vPos * 0.002 ) * 18.0 ) + 1.0 * 0.5 ) * 0.3 + 0.5;\n"
    "lowp float r = GetTexture( vec2( vTex.x - intensity * s * 3.0 * texSize, vTex.y ) ).r;\n"
    "lowp vec2 ga = GetTexture( vec2( vTex.x + intensity * s * 6.3 * texSize, vTex.y ) ).ga;\n"
    "lowp float b = GetTexture( vec2( vTex.x - intensity * s * 5.0 * texSize, vTex.y ) ).b;\n"
    "gl_FragColor = vec4( r, ga.x, b, ga.y * mix( 1.0, a, intensity ) );\n"
    "}";

Hologram::Hologram()
    : m_time( 2 )
    , m_intensity( 0 )
    , m_enabled( Claw::Registry::Get()->CheckBool( "/monstaz/settings/postprocess" ) )
{
    m_shader.Load( vertex, fragment );
}

void Hologram::Render( Claw::Surface* src, Claw::Surface* dst, float x, float y, const Claw::Rect& clipRect )
{
    if( m_enabled )
    {
        Claw::g_batcher->SetShader( &m_shader );
        m_shader.Uniform( "t", m_time );
        m_shader.Uniform( "intensity", m_intensity );
        m_shader.Uniform( "offset", GameManager::GetInstance()->GetCamOffset().y );
        m_shader.Uniform( "texSize", 1.f / src->GetPixelData()->m_width );
        m_shader.Uniform( "screenMul", GameManager::GetInstance()->GetGameScale() / 2.f );
        dst->Blit( x, y, src, clipRect );
        Claw::g_batcher->SetShader( NULL );
    }
    else
    {
        dst->Blit( x, y, src, clipRect );
    }
}

void Hologram::Update( float dt )
{
    m_time += dt;
    while( m_time > 22.f )
    {
        m_time -= 20.f;
    }
}

#include "MonstazAI/ObjectRectangle.hpp"

ObjectRectangle::ObjectRectangle( const Vectorf& edge, float perpendicular )
    : m_edge( edge )
    , m_perp( -m_edge[1], m_edge[0] )
    , m_edgeLen( edge.Length() )
    , m_perpLen( perpendicular )
{
    m_perp.Normalize();
    m_perp *= perpendicular;
}

int ObjectRectangle::l_GetEdge( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_edge[0] );
    lua.PushNumber( m_edge[1] );
    lua.PushNumber( m_edgeLen );
    return 3;
}

int ObjectRectangle::l_GetPerpendicular( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_perp[0] );
    lua.PushNumber( m_perp[1] );
    lua.PushNumber( m_perpLen );
    return 3;
}

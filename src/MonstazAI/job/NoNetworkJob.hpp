#ifndef __MONSTAZ_NONETWORKJOB_HPP__
#define __MONSTAZ_NONETWORKJOB_HPP__

#include "MonstazAI/job/Job.hpp"

#include "guif/Screen.hpp"

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/Mutex.hpp"

#include "claw_ext/network/net_monitor/NetworkMonitor.hpp"

class NoNetworkJob : public Job, public ClawExt::NetworkMonitor::Observer
{
public:
    LUA_DEFINITION( NoNetworkJob );
    NoNetworkJob( lua_State* L ) { CLAW_ASSERT( false ); }

    NoNetworkJob();
    ~NoNetworkJob();

    void Initialize();

    void Render( Claw::Surface* target );
    void Update( float dt );
    void TouchUp( int x, int y, int button );
    void TouchDown( int x, int y, int button );
    void TouchMove( int x, int y, int button );

    int l_NoNetworkRetry( lua_State* L );
    int l_NoNetworkClose( lua_State* L );
    int l_NoNetworkContinue( lua_State* L );

    virtual void OnNetworkCheckResult( ClawExt::NetworkMonitor::NetworkStatus status );

private:
    typedef Claw::LockGuard<Claw::Mutex> LockGuard;
    void LoadLayout();

    Guif::ScreenPtr m_screen;
    Claw::Mutex m_mutex;
    bool m_active;
};

#endif

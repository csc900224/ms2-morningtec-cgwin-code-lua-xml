#ifndef __MONSTAZ_INTERMEDIATEJOB_HPP__
#define __MONSTAZ_INTERMEDIATEJOB_HPP__

#include "claw/base/String.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/job/Job.hpp"
#include "MonstazAI/job/GameplayJob.hpp"

// This class is here to alleviate problems with race on GameManager creation and destruction.
// We need to have old GameManager destroyed before a new one is created, which this class
// allows.

class IntermediateJob : public Job
{
public:
    IntermediateJob( const Claw::NarrowString& fn ) : m_fn( fn ) {}
    ~IntermediateJob() {}

    virtual void Initialize() {}
    virtual void Render( Claw::Surface* target ) { target->Clear( 0 ); }
    virtual void Update( float dt ) { ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new GameplayJob( m_fn ) ); }

private:
    Claw::NarrowString m_fn;
};

#endif

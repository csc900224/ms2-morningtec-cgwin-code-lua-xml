#include "claw/base/AssetDict.hpp"
#include "claw/graphics/TriangleEngine.hpp"

#include "claw_ext/monetization/chartboost/Chartboost.hpp"
#include "claw_ext/monetization/playhaven/Playhaven.hpp"

#include <math.h>

#include "AlertBox.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/job/InitJob.hpp"
#include "MonstazAI/job/VersionCheckJob.hpp"
#include "MonstazAI/job/NoNetworkJob.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/Achievements.hpp"
#include "MonstazAI/ServerConstants.hpp"
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/FileConstants.hpp"

static const Claw::Color    COLOR_DOT                   = Claw::MakeRGB( 100, 100, 100 );
static const float          DOT_SIZE_RATIO              = 0.0075f;
static const char*          PATH_LOGO                   = "plate/logo.@linear";

static const float          CHARTBOOST_WAIT_TIME        = 10.0f;
static const float          PLAYHAVEN_WAIT_TIME         = 10.0f;

static const int            VERSION_APP_CONFIG          = 1;

#if defined CLAW_WIN32 && defined _DEBUG
static const bool           SERVER_SYNC_ENABLED         = false;
#else
static const bool           SERVER_SYNC_ENABLED         = false;
#endif

InitJob::InitJob( bool adsOnly )
    : m_logo( NULL )
    , m_preload( NULL )
    , m_preloadDone( false )
    , m_animationTimer( 0 )
    , m_playhavenFirst( false )
    , m_networkRequired( false )
    , m_timer( 0 )
    , m_state( S_IDLE )
    , m_networkState( ClawExt::NetworkMonitor::NS_UNKNOWN )
    , m_adsOnly( adsOnly )
    , m_syncLock( 1 )
    , m_initAnalytics( false )
    , m_notifyMonetization( false )
    , m_afterDL( false )
{}

InitJob::~InitJob()
{
    Release();
    ((MonstazApp*)MonstazApp::GetInstance())->Save( true );
}

void InitJob::Initialize()
{
    m_logo = Claw::AssetDict::Get<Claw::Surface>( PATH_LOGO );
    CLAW_ASSERT( m_logo );

    if( m_adsOnly )
    {
        InitNetworkCheck();
        SetupAds();
        m_preloadDone = true;
    }
    else
    {
        InitServerSyncTasks();

        CLAW_ASSERT( !m_preload );
        m_preload = new Claw::Thread( LoadEntry, this );
    }

    ClawExt::NetworkMonitor::GetInstance()->RegisterObserver( this );
    ClawExt::ServerSync::GetInstance()->RegisterObserver( this );
}

void InitJob::OnNetworkCheckResult( ClawExt::NetworkMonitor::NetworkStatus status )
{
    m_networkState = status;
}

void InitJob::Render( Claw::Surface* target )
{
    target->Clear(0);

    if( m_afterDL )
    {
        // Draw logo
        target->Blit( static_cast<float>((target->GetWidth() - m_logo->GetWidth()) >> 1),
                      static_cast<float>((target->GetHeight() - m_logo->GetHeight()) >> 1),
                      m_logo );

        // Draw dots
        const int dotSize = static_cast<int>(m_logo->GetWidth() * DOT_SIZE_RATIO + 0.5f);
        const float splashBottom = static_cast<float>((target->GetHeight() + m_logo->GetHeight()) >> 1);
        const float yPos = std::min( target->GetHeight() - (target->GetHeight() - splashBottom) * 0.5f - dotSize * 4, target->GetHeight() - dotSize * 14.0f );
        const float xPos = static_cast<float>(target->GetWidth() >> 1);

        target->DrawFilledCircle(
            xPos - dotSize * 10,
            yPos,
            ( 1 + std::max( 0.f, (float)sin( m_animationTimer * 8 ) ) ) * dotSize,
            COLOR_DOT
        );

        target->DrawFilledCircle(
            xPos,
            yPos,
            ( 1 + std::max( 0.f, (float)sin( ( m_animationTimer - 0.1f ) * 8 ) ) ) * dotSize,
            COLOR_DOT
        );

        target->DrawFilledCircle(
            xPos + dotSize * 10,
            yPos,
            ( 1 + std::max( 0.f, (float)sin( ( m_animationTimer - 0.2f ) * 8 ) ) ) * dotSize,
            COLOR_DOT
        );
    }

    if( !PakManager::GetInstance()->CheckDone() && m_dl )
    {
        m_dl->SetProgress( PakManager::GetInstance()->GetProgress() );
        m_dl->Render( target );
    }
}

void InitJob::Update( float dt )
{
    // Update timers
    if( m_timer > 0 )
    {
        m_timer -= dt;
    }
    m_animationTimer += dt; 

    ClawExt::NetworkMonitor::GetInstance()->Update( dt );

    if( m_initAnalytics )
    {
        AnalyticsManager::GetInstance()->Initialize();
        m_initAnalytics = false;
    }

    if( m_notifyMonetization )
    {
        MonetizationManager::GetInstance()->OnProgressLoaded();
        m_notifyMonetization = false;
    }

    switch( m_state )
    {
    case S_WAITING_CHARTBOOST:
        {
            ClawExt::Chartboost::CharboostState state = MonetizationManager::GetInstance()->GetChartboost()->GetState();
            if( state == ClawExt::Chartboost::CS_SHOWN || state == ClawExt::Chartboost::CS_CLOSED )
            {
                // Add was shown
                CLAW_MSG( "Chartboost shown!" );
                ChangeState( S_CHARTBOOST_VISIBLE );
            }
            else if( state == ClawExt::Chartboost::CS_FAILED )
            {
                // An error occured - try Playhaven or end
                CLAW_MSG( "Chartboost errr!" );
                ChangeState( m_playhavenFirst ? S_COUNTRY_CHECK : S_WAITING_PLAYHAVEN );
            }
            else if( m_timer <= 0 )
            {
                // Timeout - cancel and move to playhaven or end
                CLAW_MSG( "Chartboost timeout!" );
                MonetizationManager::GetInstance()->GetChartboost()->CancelRequest();
                ChangeState( m_playhavenFirst ? S_COUNTRY_CHECK : S_WAITING_PLAYHAVEN );
            }
            break;
        }

    case S_CHARTBOOST_VISIBLE:
        {
            ClawExt::Chartboost::CharboostState state = MonetizationManager::GetInstance()->GetChartboost()->GetState();
            if( state == ClawExt::Chartboost::CS_CLOSED )
            {
                // Visible add was closed - we can now end the sequence
                ChangeState( S_COUNTRY_CHECK );
            }
            break;
        }

    case S_WAITING_PLAYHAVEN:
        {
            ClawExt::Playhaven::PlayhavenState state = MonetizationManager::GetInstance()->GetPlayhaven()->GetState();
            if( state == ClawExt::Playhaven::PS_SHOWN || state == ClawExt::Playhaven::PS_CLOSED )
            {
                // Add was shown
                CLAW_MSG( "Playhaven shown!" );
                ChangeState( S_PLAYHAVEN_VISIBLE );
            }
            else if( state == ClawExt::Playhaven::PS_FAILED )
            {
                // An error occured - end or to chartboost 
                CLAW_MSG( "Playhaven error!" );
                ChangeState( m_playhavenFirst ? S_WAITING_CHARTBOOST : S_COUNTRY_CHECK );
            }
            else if( m_timer < 0 )
            {
                // Timeout - sequence finished - move on...
                CLAW_MSG( "Playhaven timeout!" );
                MonetizationManager::GetInstance()->GetPlayhaven()->CancelRequest();
                ChangeState( m_playhavenFirst ? S_WAITING_CHARTBOOST : S_COUNTRY_CHECK );
            }
            break;
        }

    case S_PLAYHAVEN_VISIBLE:
        {
            ClawExt::Playhaven::PlayhavenState state = MonetizationManager::GetInstance()->GetPlayhaven()->GetState();
            if( state == ClawExt::Playhaven::PS_CLOSED )
            {
                // Visible add was closed - we can now end the sequence
                ChangeState( S_COUNTRY_CHECK );
            }
            break;
        }

    case S_COUNTRY_CHECK:
        {
            CheckCountry();
            break;
        }

    case S_AGE_CHECK:
        {
            CheckAge();
            break;
        }

    case S_END:
        {
            // Sequence finished - if loading finished also, we can proceed
            if( m_preloadDone )
            {
                if( m_networkState == ClawExt::NetworkMonitor::NS_CONNECTED || !m_networkRequired )
                {
                    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new VersionCheckJob() );
                }
                else if( m_networkState == ClawExt::NetworkMonitor::NS_DISCONNECTED )
                {
                    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new NoNetworkJob() );
                }
            }
            break;
        }
    }
}

void InitJob::ChangeState( State newState )
{
    if( m_state != newState )
    {
        m_state = newState;

        switch( newState )
        {
        case S_WAITING_CHARTBOOST:
            m_timer = CHARTBOOST_WAIT_TIME;
            MonetizationManager::GetInstance()->GetChartboost()->RequestAd();
            break;

        case S_WAITING_PLAYHAVEN:
            m_timer = PLAYHAVEN_WAIT_TIME;
            MonetizationManager::GetInstance()->GetPlayhaven()->ContentPlacement( "launch" );
            break;
        }
    }
}

void InitJob::Release()
{
    ClawExt::NetworkMonitor::GetInstance()->UnregisterObserver( this );
    ClawExt::ServerSync::GetInstance()->UnregisterObserver( this );

    if( m_preload )
    {
        m_preload->Wait();
        delete m_preload;
        m_preload = NULL;
    }
    m_logo.Release();
}

#ifdef CLAW_ANDROID
    static const char* PATH_LEVEL       = "startmission_169.mp4";
    static const char* PATH_LEVEL_LOW   = "startmission_low_169.mp4";
#else
    static const char* PATH_LEVEL       = "startmission_43.mp4";
    static const char* PATH_LEVEL_LOW   = "startmission_low_43.mp4";
#endif

int InitJob::Load()
{
    PakManager::GetInstance()->Download( Claw::Uri( Claw::NarrowString( "http://cdn.game-lion.com/files/ms2/movies/1/" ) +
        ( ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale() < 2 ? PATH_LEVEL_LOW : PATH_LEVEL ) ), "assetcache/startmission.mp4" );
    PakManager::GetInstance()->Require( PakManager::SetFirstTutorial );

    if( !PakManager::GetInstance()->CheckDone() )
    {
        m_dl.Reset( new DownloadScreen() );
    }

    while( !PakManager::GetInstance()->CheckDone() ) { Claw::Time::Sleep( 1/60.f ); }
    PakManager::GetInstance()->Reset();
    m_afterDL = true;

    Claw::TextDict::Get()->LoadStrings( "l10n/cn.xml" );

    // Sync settings with server
    ClawExt::ServerSync::GetInstance()->SyncData();

    AudioManager::GetInstance()->Load();
    
    AtlasSet::Type set[] = {
        AtlasSet::Overrides,
        AtlasSet::Menu,
        AtlasSet::Background,
        AtlasSet::Base,
        AtlasSet::EnvCommon,
        AtlasSet::Terminator
    };

    AtlasManager::GetInstance()->Request( set );

    // Load default config
    Claw::Registry::Get()->Load( "data/defaultconfig.xml", true );

    // Load default maps data
    Claw::Registry::Get()->Load( "data/maps.xml", true );

    // Sync default config - wait until download is finished
    m_syncLock.Enter();
    Claw::FilePtr file = ClawExt::ServerSync::GetInstance()->LockTaskFile( ServerConstants::DEFAULT_CONFIG_DATA_SYNC_TASK );
    if( file )
    {
        CLAW_VERIFY( Claw::Registry::Get()->Load( file ) );
        ClawExt::ServerSync::GetInstance()->ReleaseTaskFile( ServerConstants::DEFAULT_CONFIG_DATA_SYNC_TASK, file );
    }
    m_syncLock.Leave();

    // Load progress
    LoadProgress();

    // Notify monetization manager in the main thread that progres was loaded
    m_notifyMonetization = true;

    // Load missions data
    Missions::MissionManager::GetInstance()->LoadMissions( "MissionDB.lua" );

    int runcount = 0;
    Claw::Registry::Get()->Get( "/monstaz/runcount", runcount );
    Claw::Registry::Get()->Set( "/internal/firstrun", runcount == 0 );
    Claw::Registry::Get()->Set( "/monstaz/runcount", ++runcount );

    // Workaround for Android GA crash when starting session on loading thread.
    m_initAnalytics = true;

    // Load app config (contains interstitials order)
    LoadAppConfig();

    InitNetworkCheck();
    TutorialManager::GetInstance()->Init();

    SetupAds();

    bool autoaim;
    if( !Claw::Registry::Get()->Get( "/monstaz/settings/autoaim", autoaim ) )
    {
        Claw::Registry::Get()->Set( "/monstaz/settings/autoaim", false );
        Claw::Registry::Get()->Set( "/internal/autoaimask", true );
    }

    Claw::Registry::Get()->Set( "/internal/dataloaded", true );

    GfxAsset::LoadPivotDB( "pivot.db" );

    AtlasSet::Type reqset[] = { AtlasSet::Menu, AtlasSet::Terminator };
    while( !AtlasManager::GetInstance()->AreLoaded( reqset ) ) { Claw::Time::Sleep( 1/60.f ); }

    new Achievements();

    ((MonstazApp*)MonstazApp::GetInstance())->StartRefill();

    m_preloadDone = true;

    return 0;
}

void InitJob::InitNetworkCheck()
{
    // Check if internet connection is required
    Claw::Registry::Get()->Get( "/app-config/interstitials/network-required", m_networkRequired );

    // Check network status
    if( m_networkRequired )
    {
        ClawExt::NetworkMonitor::GetInstance()->ConnectionCheck();
    }
}

void InitJob::SetupAds()
{
    int runcount = 0;
    Claw::Registry::Get()->Get( "/monstaz/runcount", runcount );

    // Check loaded interstitials order
    Claw::Registry::Get()->Get( "/app-config/interstitials/playhaven-first", m_playhavenFirst );

    // Check how many app runs are needed to start interstitials
    int minRunCount = 0;
    Claw::Registry::Get()->Get( "/app-config/interstitials/show-from-run", minRunCount );

    // Show chartboost or playhaven at start - only if this is not a first launch and ads are enabled
    if( runcount >= minRunCount && MonetizationManager::GetInstance()->AreAdsEnabled() && !TutorialManager::GetInstance()->IsActive() )
    {
#ifdef CHARTBOOST_SPLASH_DISABLED
        ChangeState( S_WAITING_PLAYHAVEN );
#else
        ChangeState( m_playhavenFirst ? S_WAITING_PLAYHAVEN : S_WAITING_CHARTBOOST );
#endif
    }
    else
    {
        ChangeState( S_COUNTRY_CHECK );
    }
}

void InitJob::CheckAge()
{
#if defined CLAW_IPHONE || defined CLAW_ANDROID
    if ( Claw::Registry::Get()->CheckBool( "/internal/firstrun" ) )
    {
        const Claw::String title = Claw::TextDict::Get()->GetText( "TEXT_BLOOD_ALERT_TITLE" );
        const Claw::String message = Claw::TextDict::Get()->GetText( "TEXT_BLOOD_ALERT_MESSAGE" );
        const Claw::String button1 = Claw::TextDict::Get()->GetText( "TEXT_BLOOD_ALERT_BUTTON_1" );
        const Claw::String button2 = Claw::TextDict::Get()->GetText( "TEXT_BLOOD_ALERT_BUTTON_2" );

        AlertBox::Show(
            Claw::NarrowString( title ),
            Claw::NarrowString( message ),
            Claw::NarrowString( button1 ),
            Claw::NarrowString( button2 ),
            OnBloodPopupResult,
            this
        );

        ChangeState( S_AGE_CHECK_ALERT );
    }
    else
#endif
    {
        ChangeState( S_END );
    };
}

void InitJob::CheckCountry()
{
#if defined CHECK_COUNTRY_CODE && defined CLAW_ANDROID
    Claw::NarrowString cc = Claw::Application::GetInstance()->GetNetworkCountryCode();
    std::transform( cc.begin(), cc.end(), cc.begin(), toupper );

    if ( cc.empty() || ( cc != "CN" && cc != "CHN" ) )
    {
        ChangeState( S_AGE_CHECK );
    }
    else
    {
        const Claw::String title = Claw::TextDict::Get()->GetText( "TEXT_COUNTRY_CHECK_ALERT_TITLE" );
        const Claw::String message = Claw::TextDict::Get()->GetText( "TEXT_COUNTRY_CHECK_ALERT_MESSAGE" );
        const Claw::String button = Claw::TextDict::Get()->GetText( "TEXT_COUNTRY_CHECK_ALERT_BUTTON" );
        AlertBox::Show( Claw::NarrowString( title ), Claw::NarrowString( message ), Claw::NarrowString( button ), Claw::NarrowString(), OnCountryCheckPopupResult );

        ChangeState( S_COUNTRY_CHECK_ALERT );
    }
#else
    ChangeState( S_AGE_CHECK );
#endif
}

void InitJob::LoadProgress()
{
    CLAW_MSG( "Loading progress..." );
#if defined CLAW_SDL
    bool success = Claw::Registry::Get()->Load( "save/config.xml", true );
    success &= Claw::Registry::Get()->Load( "save/maps.xml", true );
    success &= Claw::Registry::Get()->Load( "save/levels.xml", true );
    success &= Claw::Registry::Get()->Load( "save/missions.xml", true );
#else
    bool success = LoadProgressWithBackup( "config", "/monstaz" );
    success &= LoadProgressWithBackup( "maps",  "/maps" );
    success &= LoadProgressWithBackup( "levels",  "/levels" );
    success &= LoadProgressWithBackup( "missions",  "/missions" );
#endif
    CLAW_MSG( (success ? "Loading succeed." : "Loading failed!!!") );
}

bool InitJob::LoadProgressWithBackup( const char* fileName,  const char* branchName )
{
    // Find newest correct save
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->m_saveIdx[fileName] = -1;
    int lastLaodedFile = -1;
    int maxTimestamp = 0;

    for( int i = 0; i < MonstazAI::MonstazAIApplication::BACKUP_SAVE_NUM; ++i )
    {
        // Generate file name
        Claw::StdOStringStream filename;
        filename << "save/" << fileName << "_" << i << ".xml";

        if( Claw::Registry::Get()->LoadEncrypted( filename.m_str.c_str(), MonstazAI::MonstazAIApplication::ENCRYPTION_KEY, true, NULL, true ) )
        {
            // Look for timestamp
            Claw::NarrowString timestampKey = branchName;
            timestampKey += "/save-timestamp";
            int timestamp = 0;
            if( Claw::Registry::Get()->Get( timestampKey.c_str(), timestamp ) )
            {
                lastLaodedFile = i;

                // Newer file found
                if( timestamp > maxTimestamp )
                {
                    maxTimestamp = timestamp;
                    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->m_saveIdx[fileName] = lastLaodedFile;
                }
            }
        }
    }

    bool loaded = lastLaodedFile >= 0 ;

    // Load newest file
    int currentSaveIdx = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->m_saveIdx[fileName];
    if( currentSaveIdx >= 0 && currentSaveIdx != lastLaodedFile )
    {
        Claw::StdOStringStream filename;
        filename << "save/" << fileName << "_" << currentSaveIdx << ".xml";

        loaded = Claw::Registry::Get()->LoadEncrypted( filename.m_str.c_str(), MonstazAI::MonstazAIApplication::ENCRYPTION_KEY, true, NULL, true );
        CLAW_MSG_ASSERT( loaded, "Something went wrong with loading prorgess!!!" );
    }

    // Increase save idx to point next file
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->m_saveIdx[fileName] = (currentSaveIdx + 1) % MonstazAI::MonstazAIApplication::BACKUP_SAVE_NUM;

    // If successfully loaded - always try to delete unencrytped file
    if( loaded )
    {
        Claw::NarrowString oldSave = "save/";
        oldSave += fileName;
        oldSave += ".xml";
        Claw::UnlinkFile( oldSave.c_str() );
    }

    // Loading finished
    return loaded;
}

void InitJob::LoadAppConfig()
{
    Claw::Registry::Get()->Load( "data/app-config.xml", true );
    Claw::FilePtr file = ClawExt::ServerSync::GetInstance()->LockTaskFile( ServerConstants::CONFIGURATION_DATA_SYNC_TASK );
    if( file )
    {
        CLAW_VERIFY( Claw::Registry::Get()->Load( file, true ) );
        ClawExt::ServerSync::GetInstance()->ReleaseTaskFile( ServerConstants::CONFIGURATION_DATA_SYNC_TASK, file );
    }

    int version = 0;
    Claw::Registry::Get()->Get( "/app-config/version", version );

    if ( version < VERSION_APP_CONFIG )
    {
        // Last version downloaded from server is older than configuration shipped with application
        CLAW_VERIFY( Claw::Registry::Get()->Load( ServerConstants::CONFIGURATION_DATA_XML ) );
    }
}

void InitJob::InitServerSyncTasks()
{
    // Initialize and configure Server Sync
    ClawExt::ServerSync::Config config;
    config.applicationName  = ServerConstants::SYNC_APP_NAME;
    config.platformName     = ServerConstants::SYNC_DATA_PLATFORM;
    config.hostURL          = ServerConstants::SYNC_REMOTE_URL;
    config.saveDirectory    = ServerConstants::SYNC_SAVE_DIR;
    config.updatePertiod    = ServerConstants::SYNC_UPDATE_PERIOD;
    config.updateEveryStart = ServerConstants::SYNC_UPDATE_EVERY_START;
    config.encryptionKey    = ServerConstants::SYNC_ENC_KEY;
    config.enableABTesting  = ServerConstants::SYNC_AB_TESTS_ENABLED;

    ClawExt::ServerSync::GetInstance()->Initialize( config );
    ClawExt::ServerSync::GetInstance()->SetEnabled( SERVER_SYNC_ENABLED );

    // Application version
    ClawExt::ServerSync::GetInstance()->AddTask( ServerConstants::APP_VERSION_SYNC_TASK,
                                                 ServerConstants::APP_VERSION_REMOTE_FILE,
                                                 ServerConstants::APP_VERSION_XML );

    // Default config
    ClawExt::ServerSync::GetInstance()->AddTask( ServerConstants::DEFAULT_CONFIG_DATA_SYNC_TASK,
                                                 ServerConstants::DEFAULT_CONFIG_DATA_REMOTE_FILE,
                                                 ServerConstants::DEFAULT_CONFIG_DATA_XML );

    // Configuration
    ClawExt::ServerSync::GetInstance()->AddTask( ServerConstants::CONFIGURATION_DATA_SYNC_TASK,
                                                 ServerConstants::CONFIGURATION_DATA_REMOTE_FILE,
                                                 ServerConstants::CONFIGURATION_DATA_XML );

    // LUA - menu
    ClawExt::ServerSync::GetInstance()->AddTask( ServerConstants::LUA_MENU_SYNC_TASK,
                                                 ServerConstants::LUA_MENU_REMOTE_FILE,
                                                 ServerConstants::LUA_MENU_LOCAL_FILE );

    // LUA - ingame
    ClawExt::ServerSync::GetInstance()->AddTask( ServerConstants::LUA_INGAME_SYNC_TASK,
                                                 ServerConstants::LUA_INGAME_REMOTE_FILE,
                                                 ServerConstants::LUA_INGAME_LOCAL_FILE );
}

void InitJob::OnBloodPopupResult( int button, void* ptr )
{
    Claw::Registry::Get()->Set( "/monstaz/settings/blood", button == 0 ? 2 : 1 );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_AGE_CHECKED, button == 0 ? GEP_AGE_LT_8 : GEP_AGE_GTE_8 );

    InitJob* job = (InitJob*)ptr;
    job->ChangeState( S_END );
}

void InitJob::OnCountryCheckPopupResult( int button, void* ptr )
{
    Claw::Application::GetInstance()->Exit();
}

void InitJob::OnSynchronisationStart()
{
    m_syncLock.Enter();
}

void InitJob::OnSynchronisationEnd( bool success )
{
    m_syncLock.Leave();
}

void InitJob::OnSynchronisationSkip()
{
    m_syncLock.Leave();
}

void InitJob::OnTaskFinished( const ClawExt::ServerSync::TaskId& taskId, bool success )
{
}

void InitJob::OnGroupChanged( const ClawExt::ServerSync::GroupName& newGroupName, bool initial )
{
}

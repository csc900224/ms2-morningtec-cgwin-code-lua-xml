#ifndef __MONSTAZ_INITJOB_HPP__
#define __MONSTAZ_INITJOB_HPP__

#include "claw/base/Thread.hpp"
#include "claw/base/Semaphore.hpp"
#include "claw/network/PakDownloader.hpp"
#include "claw_ext/network/net_monitor/NetworkMonitor.hpp"
#include "claw_ext/network/server_sync/ServerSync.hpp"

#include "MonstazAI/job/Job.hpp"
#include "MonstazAI/DownloadScreen.hpp"

class InitJob : public Job, public ClawExt::NetworkMonitor::Observer, public ClawExt::ServerSync::Observer
{
public:
    InitJob( bool adsOnly = false );
    ~InitJob();

    void Initialize();

    void Render( Claw::Surface* target );
    void Update( float dt );

    // NetworkMonitor::Observer
    virtual void OnNetworkCheckResult( ClawExt::NetworkMonitor::NetworkStatus status );

    // ServerSync::Observer
    virtual void OnSynchronisationStart();
    virtual void OnSynchronisationEnd( bool success );
    virtual void OnSynchronisationSkip();
    virtual void OnTaskFinished( const ClawExt::ServerSync::TaskId& taskId, bool success );
    virtual void OnGroupChanged( const ClawExt::ServerSync::GroupName& newGroupName, bool initial );

private:
    enum State
    {
        S_IDLE,
        S_WAITING_CHARTBOOST,
        S_CHARTBOOST_VISIBLE,
        S_WAITING_PLAYHAVEN,
        S_PLAYHAVEN_VISIBLE,
        S_COUNTRY_CHECK,
        S_COUNTRY_CHECK_ALERT,
        S_AGE_CHECK,
        S_AGE_CHECK_ALERT,
        S_END,
    };

    void ChangeState( State newState );
    int Load();
    void InitNetworkCheck();
    void SetupAds();
    void CheckAge();
    void CheckCountry();
    void LoadProgress();
    bool LoadProgressWithBackup( const char* fileName, const char* branchName );
    void LoadAppConfig();
    void Release();
    void InitServerSyncTasks();
    static int LoadEntry( void* ptr ) { return ((InitJob*)ptr)->Load(); }

    static void OnBloodPopupResult( int button, void* ptr );
    static void OnCountryCheckPopupResult( int button, void* ptr );

    Claw::Thread* m_preload;
    volatile bool m_preloadDone;
    volatile bool m_initAnalytics;
    volatile bool m_notifyMonetization;
    float m_animationTimer;
    float m_timer;
    volatile State m_state;
    bool m_playhavenFirst;
    bool m_networkRequired;
    Claw::SurfacePtr m_logo;
    bool m_adsOnly;
    DownloadScreenPtr m_dl;
    bool m_afterDL;

    ClawExt::NetworkMonitor::NetworkStatus m_networkState;
    Claw::Semaphore m_syncLock;

    Claw::PakDownloaderPtr m_downloader;
};

#endif

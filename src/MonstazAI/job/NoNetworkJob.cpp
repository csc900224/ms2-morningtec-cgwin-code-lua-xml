#include "MonstazAI/job/NoNetworkJob.hpp"
#include "MonstazAI/job/InitJob.hpp"
#include "MonstazAI/ServerConstants.hpp"
#include "MonstazAI/Application.hpp"
#include "AppVersion.hpp"

#include "claw/base/Registry.hpp"
#include "claw_ext/network/server_sync/ServerSync.hpp"

#include <sstream>

static const unsigned int RECHECK_INTERVAL = 15;

LUA_DECLARATION( NoNetworkJob )
{
    METHOD( NoNetworkJob, NoNetworkRetry ),
    METHOD( NoNetworkJob, NoNetworkClose ),
    METHOD( NoNetworkJob, NoNetworkContinue ),
    {0,0}
};

NoNetworkJob::NoNetworkJob()
{}

NoNetworkJob::~NoNetworkJob()
{
    ClawExt::NetworkMonitor::GetInstance()->UnregisterObserver( this );
}

void NoNetworkJob::Initialize()
{
    LoadLayout();
    ClawExt::NetworkMonitor::GetInstance()->RegisterObserver( this );
    ClawExt::NetworkMonitor::GetInstance()->SetConnectionCheckInterval( RECHECK_INTERVAL );
}

void NoNetworkJob::Render( Claw::Surface* target )
{
    target->Clear(0);
    if( m_screen )
    {
        LockGuard guard(m_mutex);
        m_screen->Render( target );
    }
}

void NoNetworkJob::Update( float dt )
{
    ClawExt::NetworkMonitor::GetInstance()->Update( dt );
    if( m_screen )
    {
        LockGuard guard(m_mutex);
        m_screen->Update( dt );
    }
}

void NoNetworkJob::LoadLayout()
{
    m_screen.Reset( new Guif::Screen() );
    Claw::LuaPtr lua = m_screen->GetLuaState();

    lua->RegisterLibrary( Claw::Lua::L_MATH );
    lua->RegisterLibrary( Claw::Lua::L_STRING );

    Claw::Lunar<NoNetworkJob>::Register( *lua );
    Claw::Lunar<NoNetworkJob>::push( *lua, this );
    lua->RegisterGlobal( "NoNetworkCallback" );

    MonstazApp::PushScreenModes( lua );

    lua->Load( "menu2/no_network.lua" );
    lua->Call( "NoNetworkPopupShow", 0, 0 );
}

void NoNetworkJob::OnNetworkCheckResult( ClawExt::NetworkMonitor::NetworkStatus status )
{
    Claw::LuaPtr lua = m_screen->GetLuaState();
    LockGuard guard(m_mutex);

    if( status == ClawExt::NetworkMonitor::NS_CONNECTED )
    {
        lua->Call( "NoNetworkPopupConnected", 0, 0 );
    }
    else
    {
        lua->Call( "NoNetworkPopupShow", 0, 0 );
    }
}

void NoNetworkJob::TouchUp( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    m_screen->OnTouchUp( x, y, button );
}

void NoNetworkJob::TouchDown( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    m_screen->OnTouchDown( x, y, button );
}

void NoNetworkJob::TouchMove( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    m_screen->OnTouchMove( x, y, button );
}

int NoNetworkJob::l_NoNetworkRetry( lua_State* L )
{
    ClawExt::NetworkMonitor::GetInstance()->ConnectionCheck();
    return 0;
}

int NoNetworkJob::l_NoNetworkClose( lua_State* L )
{
    Claw::Application::GetInstance()->Exit();
    return 0;
}

int NoNetworkJob::l_NoNetworkContinue( lua_State* L )
{
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new InitJob(true) );
    return 0;
}


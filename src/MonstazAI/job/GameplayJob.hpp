#ifndef __MONSTAZ_GAMEPLAYJOB_HPP__
#define __MONSTAZ_GAMEPLAYJOB_HPP__

#include <vector>

#include "claw/base/Thread.hpp"
#include "claw/sound/mixer/AudioChannel.hpp"

#include "MonstazAI/job/Job.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/Heat.hpp"
#include "MonstazAI/HeatBlur.hpp"
#include "MonstazAI/Loading.hpp"
#include "MonstazAI/PerfMonitor.hpp"
#include "MonstazAI/PostProcess.hpp"

class GameplayJob : public Job
{
public:
    GameplayJob( const Claw::NarrowString& fn );
    ~GameplayJob();

    void Initialize();

    void Render( Claw::Surface* target );
    void Update( float dt );
    void KeyPress( Claw::KeyCode code );
    void KeyRelease( Claw::KeyCode code );
    void TouchUp( int x, int y, int button );
    void TouchDown( int x, int y, int button );
    void TouchMove( int x, int y, int button );
    void Resize( int x, int y );
    void Focus( bool focus );
    void OnTouchDeviceChange();

    void MenuDump() { GameManager::GetInstance()->GetMenu( true )->Dump(); }

    void LoadLevel( const char* fn );
    void AddPostProcess( PostProcessPtr pp ) { m_pp.push_back( pp ); }

private:
    static int PreloadEntry( void* ptr ) { return ((GameplayJob*)ptr)->Preload(); }
    float CalculateGameScale() const;
    void PauseGame();
    int Preload();
    void SetupDrawbuffer( int x, int y );

    Claw::SurfacePtr m_drawBuffer[2];
    Claw::SurfacePtr m_heatBuffer[2];
    HeatPtr m_heat;
    HeatBlurPtr m_heatBlur;
    std::vector<PostProcessPtr> m_pp;
    float m_heatTime;
    bool m_heatStage;
    bool m_heatClear;
    bool m_movieWait;

    Claw::Int8** m_heatBuf;
    float* m_heatDiff;
    Vectori m_heatSize;

    Claw::Thread* m_preload;
    bool m_preloadDone;
    Claw::NarrowString m_fn;
    LoadingPtr m_loading;

    int m_dlList;

    GameManagerPtr m_gameManager;
    PerfMonitorPtr m_perfMonitor;

    Claw::AudioChannelWPtr m_ambient;
};

#endif

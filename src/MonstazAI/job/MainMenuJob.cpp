#include "claw/application/AbstractApp.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/application/HardwareKey.hpp"

#include "MonstazAI/job/GameplayJob.hpp"
#include "MonstazAI/job/MainMenuJob.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/VibraController.hpp"
#include "MonstazAI/GameCenterManager.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/Achievements.hpp"
#include "MonstazAI/entity/EntityManager.hpp"
#include "MonstazAI/ServerConstants.hpp"
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/db/UserDataInput.hpp"
#include "MonstazAI/math/LuaMath.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/ConnectionMonitor.hpp"

#include "claw_ext/network/UrlEncode.hpp"

#include <sstream>
#include <ctime>

static const char* PH_MORE_GAMES_PLACEMENT      = "more_games";
#ifdef CLAW_ANDROID
#ifdef AMAZON_BUILD
static const char* RATE_US_URL                  = "amzn://apps/android?p=com.gamelion.ms2.amazon";
static const char* MORE_GAMES_URL               = "amzn://apps/android?p=com.gamelion.ms2.amazon&showAll=1";
#else
static const char* RATE_US_URL                  = "market://details?id=com.gamelion.ms2";
static const char* MORE_GAMES_URL               = "market://search?q=pub:Gamelion+Studios";
#endif
#else
static const char* RATE_US_URL                  = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=623241721";
static const char* MORE_GAMES_URL               = "itms-apps://itunes.apple.com/artist/id314395447";
#endif
static const char* TWITTER_URL                  = "https://twitter.com/gamelion";
static const char* FACEBOOK_URL                 = "https://www.facebook.com/MonsterShooter";
static const char* OST_URL                      = "itms://itunes.apple.com/album/id669228994";

#ifdef AMAZON_BUILD
static const char* TEXT_DOWNLOAD_URL            = "TEXT_DOWNLOAD_URL_AMAZON";
#else
static const char* TEXT_DOWNLOAD_URL            = "TEXT_DOWNLOAD_URL";
#endif

static const char* SHARE_MSG_PREFIX[]         = {
    "TEXT_TWEETER_WEAPON_MSG_",
    "TEXT_TWEETER_PERK_MSG_"
};
static const int SHARE_MSG_NUM[]              = { 4, 1 };

LUA_DECLARATION( MainMenuJob )
{
    METHOD( MainMenuJob, StartGame ),
    METHOD( MainMenuJob, ResetData ),
    METHOD( MainMenuJob, Url ),
    METHOD( MainMenuJob, Save ),
    METHOD( MainMenuJob, LogEvent ),
    METHOD( MainMenuJob, ExitApplication ),
    METHOD( MainMenuJob, GetControlSize ),
    METHOD( MainMenuJob, Playhaven ),
    METHOD( MainMenuJob, RequestAtlas ),
    METHOD( MainMenuJob, ReleaseAtlas ),
    METHOD( MainMenuJob, IsAtlasLoaded ),
    METHOD( MainMenuJob, MoreGames ),
    METHOD( MainMenuJob, OnFacebookButton ),
    METHOD( MainMenuJob, OnTwitterButton ),
    METHOD( MainMenuJob, OnRateUsButton ),
    METHOD( MainMenuJob, CanSendTweet ),
    METHOD( MainMenuJob, SendTweet ),
    METHOD( MainMenuJob, PublishOnFacebook ),
    METHOD( MainMenuJob, GetTime ),
    METHOD( MainMenuJob, GetNetworkTime ),
    METHOD( MainMenuJob, BuyOST ),
    METHOD( MainMenuJob, ResistanceLogin ),
    METHOD( MainMenuJob, ResistanceCreateUser ),
    METHOD( MainMenuJob, ResistanceRecoverPassword ),
    METHOD( MainMenuJob, ResistanceOnAcceptedInvite ),
    METHOD( MainMenuJob, ResistanceOnRejectInvite ),
    METHOD( MainMenuJob, ResistanceOnSendGift ),
    METHOD( MainMenuJob, ResistanceOnAcceptedGift ),
    METHOD( MainMenuJob, ResistanceShowTermsOfUse ),
    METHOD( MainMenuJob, ResistanceSaveAccountSettings ),
    METHOD( MainMenuJob, OpenVkb ),
    METHOD( MainMenuJob, ResistanceOnSendInvite ),
    METHOD( MainMenuJob, ResistanceOnGetFriendInfo ),
    METHOD( MainMenuJob, ResistanceSetFriendAsBackup ),
    METHOD( MainMenuJob, ResistanceOnRewardNewFriendsAccepted ),
    METHOD( MainMenuJob, ResistanceGetUsernamePath ),
    METHOD( MainMenuJob, ResistanceRefreshInputs ),
    METHOD( MainMenuJob, ResistanceForceRequestSend ),
    METHOD( MainMenuJob, ResistanceLoginLastUser ),
    METHOD( MainMenuJob, ResistanceLoggedOut ),
    METHOD( MainMenuJob, ResistanceFB ),
    METHOD( MainMenuJob, ResistanceCreateUserViaFb ),
    METHOD( MainMenuJob, ResistanceLoginUserViaFb ),
    METHOD( MainMenuJob, ResistanceIsFbAvatarAvaible ),
    METHOD( MainMenuJob, ResistanceGetFbId ),
    METHOD( MainMenuJob, SetClipboard ),
    METHOD( MainMenuJob, ResistanceRemoveFriend ),
    METHOD( MainMenuJob, ResistanceUpdateAll ),
    METHOD( MainMenuJob, ResistanceSetUsernameImg ),
    METHOD( MainMenuJob, ResistanceSpeedUpFriendBackup ),
    METHOD( MainMenuJob, ResistanceConfirmGifts ),
    METHOD( MainMenuJob, GetInputLenght ),
    METHOD( MainMenuJob, IsSMSSupported ),
    METHOD( MainMenuJob, IsEmailSupported ),
    METHOD( MainMenuJob, SendSmsInvitation ),
    METHOD( MainMenuJob, SendEmailInvitation ),
    METHOD( MainMenuJob, SendFacebookInvitation ),
    METHOD( MainMenuJob, GetCurrentDateString ),
    METHOD( MainMenuJob, GetVersionString ),
    METHOD( MainMenuJob, ResistanceSetVipStatus ),
    METHOD( MainMenuJob, ResistanceGooglePlus ),
    METHOD( MainMenuJob, ResistanceGetGpId ),
    METHOD( MainMenuJob, OpenAchievementsUI ),
   {0,0}
};

MainMenuJob::MainMenuJob( StartScreen start )
    : m_preload( NULL )
    , m_preloadDone( false )
    , m_loading( new Loading( false ) )
    , m_audioManager( AudioManager::GetInstance() )
    , m_startScreen( start )
    , m_syncResult ( false )
    , m_facebookAction( FA_NONE )
    , m_moreGamesShown( false )
    , m_thread( NULL )
    , m_threadGs( NULL )
    , m_userAvatarRequest( false )
    , m_wasVkbOpened( false )
    , m_gpAchievements( false )
    , m_dlList( PakManager::SetFirstTutorial | PakManager::SetTutorialMenu )
{
    PakManager::GetInstance()->Require( m_dlList );
}

MainMenuJob::~MainMenuJob()
{
    if( ConnectionMonitor::IsCreated() )
    {
        ConnectionMonitor::GetInstance()->SetEnabled( false );
    }

    m_screen->GetLuaState()->Call( "MapStorePosition", 0, 0 );

    if ( AtlasManager::GetInstance() )
    {
        AtlasSet::Type unload[] = {
            AtlasSet::Map01,
            AtlasSet::Map02,
            AtlasSet::Map03,
            AtlasSet::Map04,
            AtlasSet::Map05,
            AtlasSet::Map06,
            AtlasSet::Terminator
        };
        AtlasManager::GetInstance()->Release( unload );
    }

    if ( TutorialManager::GetInstance() )
    {
        TutorialManager::GetInstance()->SetMainMenuLua( NULL );
    }

    UserDataManager::GetInstance()->UnregisterObserver( this );

    if( MonetizationManager::IsCreated() )
    {
        MonetizationManager::GetInstance()->GetPlayhaven()->UnregisterPlacementObserver( this );
    }

    MonstazAI::MonstazAIApplication* app = (MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance();
    if( app->GetTwitter() )
    {
        app->GetTwitter()->SetListener( NULL );
    }
    if( app->GetFacebook() )
    {
        app->GetFacebook()->UnregisterObserver( this );
    }
    if( app->GetGoogleServices() )
    {
        app->GetGoogleServices()->UnregisterObserver( this );
    }
}

void MainMenuJob::Initialize()
{
    Missions::MissionManager::GetInstance()->SetPaused( true );

    m_audioManager->StopMusic();
    m_audioManager->PlayMusic( "menu.ogg" );

    m_preload = new Claw::Thread( PreloadEntry, this );

    m_cLink[0] = Vectorf( -65, 85 );
    m_cLink[1] = Vectorf( -14, 64 );

    // Register myself as server-sync observer
    UserDataManager::GetInstance()->RegisterObserver( this );
    UserDataManager::GetInstance()->UpdateResistanceDB();

    if( MonetizationManager::GetInstance()->IsTapjoySupported() )
    {
        MonetizationManager::GetInstance()->GetTapjoy()->CheckPoints();
    }

    MonetizationManager::GetInstance()->GetPlayhaven()->RegisterPlacementObserver( this );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetTwitter()->SetListener( this );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetSmsService()->SetListener( this );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetEmailService()->SetListener( this );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook()->RegisterObserver( this );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices()->RegisterObserver( this );
}

void MainMenuJob::Render( Claw::Surface* target )
{
    if( m_preload )
    {
        m_loading->Render( target, m_dlList );
    }
    else
    {
        m_screen->Render( target );

        UserDataInput::GetInstance()->Render( target );
        UserDataInput::GetInstance()->RenderInputFields( target , m_screen );
    }

    Achievements::GetInstance()->Render( target );
}

void MainMenuJob::Update( float dt )
{
    Achievements::GetInstance()->Update( dt );

    if( m_preload )
    {
        m_loading->Update( dt );

        if( m_preloadDone && m_loading->CanLeave() )
        {
            delete m_preload;
            m_preload = NULL;
            m_loading.Release();

            static bool firstTime = true;
            if( firstTime )
            {
                // Authenticate to the game center just at the start of main menu
                GameCenterManager::GetInstance()->Authenticate();
                firstTime = false;
            }

            Missions::MissionManager::GetInstance()->SetPaused( false );
            ConnectionMonitor::GetInstance()->SetEnabled( true );
        }
        else
        {
            return;
        }
    }

    m_screen->Update( dt );
    Shop::GetInstance()->Update( dt );

    UserDataManager::GetInstance()->SyncData();
    UserDataManager::GetInstance()->Update(dt);

    if ( m_syncResult ) 
    {
       m_syncResult = false;
       UserDataManager::GetInstance()->HandleRequests();
       UserDataManager::GetInstance()->FillLuaFromDb();
    }

    if( m_facebookResponse.action != FA_NONE )
    {
        Claw::LuaPtr lua = m_screen->GetLuaState();
        if( m_facebookResponse.action == FA_PUBLISH_TIEM )
        {
            lua->Call( m_facebookResponse.success ? "ShopShareFacebookSuccess" : "ShopShareFacebookFail", 0 , 0 );
        }
        else if( m_facebookResponse.action == FA_SEND_INVITATION )
        {
            lua->PushBool( m_facebookResponse.success );
            lua->Call( "OutOfFuelInviteOnFacebookSent", 1, 0 );
        }
        m_facebookResponse.action = FA_NONE;
    }

    CheckVkbStatus();
}

void MainMenuJob::KeyPress( Claw::KeyCode code )
{
    if( m_preload ) return;
    m_screen->OnKeyDown( code );

    if( Claw::Application::GetInstance()->IsVkbOpened() && code == Claw::KEY_BACK )
    {
        Claw::Application::GetInstance()->CloseVkb();
        Claw::LuaPtr lua = m_screen->GetLuaState();
        lua->Call( "OnCloseVkb", 0 , 0 );
        m_wasVkbOpened = false;
    }
}

void MainMenuJob::TouchUp( int x, int y, int button )
{
    if( m_preload || Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    if( Claw::Application::GetInstance()->IsVkbOpened() )
    {
        Claw::Application::GetInstance()->CloseVkb();
        Claw::LuaPtr lua = m_screen->GetLuaState();
        lua->Call( "OnCloseVkb", 0 , 0 );
        m_wasVkbOpened = false;
        return;
    }
    m_screen->OnTouchUp( x, y, button );
}

void MainMenuJob::TouchDown( int x, int y, int button )
{
    if( m_preload || Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH || Claw::Application::GetInstance()->IsVkbOpened() ) return;
    m_screen->OnTouchDown( x, y, button );
}

void MainMenuJob::TouchMove( int x, int y, int button )
{
    if( m_preload || Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH || Claw::Application::GetInstance()->IsVkbOpened() ) return;
    m_screen->OnTouchMove( x, y, button );
}

void MainMenuJob::Focus( bool focus )
{
    if( m_preload ) return;
    Shop::GetInstance()->Focus( focus );
}

void MainMenuJob::OnText( const char* text )
{
    Claw::LuaPtr lua = m_screen->GetLuaState();
    lua->PushString( text );
    lua->Call( "UpdateInput", 1 , 0 );
}

void MainMenuJob::OnTextNewLine()
{
    Claw::Application::GetInstance()->CloseVkb();
    Claw::LuaPtr lua = m_screen->GetLuaState();
    m_wasVkbOpened = false;
    lua->Call( "OnCloseVkb", 0 , 0 );
}

int MainMenuJob::l_StartGame( lua_State* L )
{
    Claw::Lua lua( L );

    Claw::Registry::Get()->Set( "/internal/storylevel", 0 );
    Claw::Registry::Get()->Set( "/internal/survivalplanet", 1 );
    Claw::Registry::Get()->Set( "/internal/survivalworld", 1 );

    Claw::NarrowString level = lua.CheckString( 1 );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new GameplayJob( level ) );

    return 0;
}

int MainMenuJob::l_ResetData( lua_State* L )
{
    CLAW_MSG( "RESETING SAVE DATA!!!" );
    Claw::Registry::Get()->Load( "defaultconfig.xml", true );
    return 0;
}

int MainMenuJob::l_Url( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::AbstractApp::GetInstance()->OpenWebBrowser( Claw::String( lua.CheckString( 1 ) ) );
    return 0;
}

int MainMenuJob::l_Save( lua_State* L )
{
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    return 0;
}

int MainMenuJob::l_LogEvent( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int MainMenuJob::l_ExitApplication( lua_State* L )
{
    Claw::AbstractApp::GetInstance()->Exit();
    return 0;
}

int MainMenuJob::l_GetControlSize( lua_State* L )
{
    return MonstazAI::MonstazAIApplication::GetControlSize( m_screen, L );
}

int MainMenuJob::l_Playhaven( lua_State* L )
{
    Claw::Lua lua( L );
    if( !TutorialManager::GetInstance()->IsActive() )
    {
        MonetizationManager::GetInstance()->GetPlayhaven()->ContentPlacement( lua.CheckString( 1 ).c_str() );
    }
    return 0;
}

int MainMenuJob::l_RequestAtlas( lua_State* L )
{
    Claw::Lua lua( L );
    AtlasSet::Type set[] = { lua.CheckEnum<AtlasSet::Type>( 1 ), AtlasSet::Terminator };
    AtlasManager::GetInstance()->Request( set );
    return 0;
}

int MainMenuJob::l_ReleaseAtlas( lua_State* L )
{
    Claw::Lua lua( L );
    AtlasSet::Type set[] = { lua.CheckEnum<AtlasSet::Type>( 1 ), AtlasSet::Terminator };
    AtlasManager::GetInstance()->Release( set );
    return 0;
}

int MainMenuJob::l_IsAtlasLoaded( lua_State* L )
{
    Claw::Lua lua( L );
    AtlasSet::Type set[] = { lua.CheckEnum<AtlasSet::Type>( 1 ), AtlasSet::Terminator };
    lua.PushBool( AtlasManager::GetInstance()->AreLoaded( set ) );
    return 1;
}

int MainMenuJob::l_MoreGames( lua_State* L )
{
    m_moreGamesShown = false;
    MonetizationManager::GetInstance()->GetPlayhaven()->ContentPlacement( PH_MORE_GAMES_PLACEMENT );
    return 0;
}

int MainMenuJob::l_OnFacebookButton( lua_State* L )
{
    GiveAward( L );
    Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( FACEBOOK_URL ) );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FREE_STUFF_ACTION, GEP_FREE_STUFF_FACEBOOK );
    return 0;
}

int MainMenuJob::l_OnTwitterButton( lua_State* L )
{
    GiveAward( L );
    Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( TWITTER_URL ) );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FREE_STUFF_ACTION, GEP_FREE_STUFF_TWITTER );
    return 0;
}

int MainMenuJob::l_OnRateUsButton( lua_State* L )
{
    GiveAward( L );
    Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( RATE_US_URL ) );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FREE_STUFF_ACTION, GEP_FREE_STUFF_RATE );
    return 0;
}

void MainMenuJob::GiveAward( lua_State* L )
{
    Claw::Lua lua( L );
    int cash = lua.CheckNumber( -2 );
    int gold = lua.CheckNumber( -1 );
    Shop::GetInstance()->UpdateCash( cash, gold );
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
}

void MainMenuJob::GenerateShareMessage( Claw::String& outMessage, const Claw::NarrowString& itemName, int itemCat )
{
    CLAW_ASSERT( itemCat <= sizeof(SHARE_MSG_PREFIX)/sizeof(SHARE_MSG_NUM[0]) );
    std::ostringstream msgId;
    msgId << SHARE_MSG_PREFIX[itemCat-1] << g_rng.GetInt( 1, SHARE_MSG_NUM[itemCat-1] );

    outMessage = Claw::TextDict::Get()->GetText( msgId.str() );
    Claw::String item = Claw::TextDict::Get()->GetText( itemName );
    Claw::String url = Claw::TextDict::Get()->GetText( TEXT_DOWNLOAD_URL );

    CLAW_VERIFY( Claw::TextDict::ReplaceVar( outMessage, "ITEM", item ) );
    CLAW_VERIFY( Claw::TextDict::ReplaceVar( outMessage, "URL", url ) );
}

void MainMenuJob::GenerateInvitationMessage( Claw::String& outMessage, bool email )
{
    outMessage = Claw::TextDict::Get()->GetText( email ? "TEXT_INVITATION_MAIL_CONTENTS" : "TEXT_INVITATION_CONTENTS" );
    Claw::String downloadUrl = Claw::TextDict::Get()->GetText( TEXT_DOWNLOAD_URL );
    const char* userID = NULL;
    Claw::Registry::Get()->Get( "/monstaz/resistance/uniqueId", userID );
    if( !userID )
    {
        userID = "";
    }

    CLAW_VERIFY( Claw::TextDict::ReplaceVar( outMessage, "URL", downloadUrl ) );
    CLAW_VERIFY( Claw::TextDict::ReplaceVar( outMessage, "USER_ID", userID ) );
}

int MainMenuJob::l_CanSendTweet( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushBool( ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetTwitter()->CanSendTweet() );
    return 1;
}

int MainMenuJob::l_SendTweet( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString itemName = lua.CheckString( -2 );
    int itemCat = lua.CheckNumber( -1 );

    Claw::String message;
    GenerateShareMessage( message, itemName, itemCat );

    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetTwitter()->SendTweet( Claw::NarrowString( message ) );
    return 0;
}

void MainMenuJob::OnTweetSent( bool success )
{
    Claw::LuaPtr lua = m_screen->GetLuaState();
    lua->Call( success ? "ShopShareTwitterSuccess" : "ShopShareTwitterFail", 0 , 0 );
    Claw::Application::GetInstance()->CloseVkb();
}

void MainMenuJob::OnSmsSent( bool success )
{
    Claw::LuaPtr lua = m_screen->GetLuaState();
    lua->PushBool( success );
    lua->Call( "OutOfFuelInviteOnSmsSent", 1, 0 );
    Claw::Application::GetInstance()->CloseVkb();
}

void MainMenuJob::OnEmailSent( bool success )
{
    Claw::LuaPtr lua = m_screen->GetLuaState();
    lua->PushBool( success );
    lua->Call( "OutOfFuelInviteOnEmailSent", 1, 0 );
    Claw::Application::GetInstance()->CloseVkb();
}

int MainMenuJob::l_PublishOnFacebook( lua_State* L )
{
    CLAW_MSG( "MainMenuJob::l_PublishOnFacebook()" );
    m_facebookAction = FA_PUBLISH_TIEM;

    Claw::Lua lua( L );
    m_itemData.id = lua.CheckString( -3 );
    m_itemData.name = lua.CheckString( -2 );
    m_itemData.category = lua.CheckNumber( -1 );

    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
    if( fb->IsAuthenticated() )
    {
        CLAW_MSG( "MainMenuJob::l_PublishOnFacebook() - publishing" );
        PublishOnFacebook();
    }
    else
    {
        CLAW_MSG( "MainMenuJob::l_PublishOnFacebook() - authenticating" );
        fb->Authenticate( true );
    }
    return 0;
}

void MainMenuJob::PublishOnFacebook()
{
    if( m_facebookAction == FA_PUBLISH_TIEM )
    {
        CLAW_MSG( "MainMenuJob::PublishOnFacebook(): FA_PUBLISH_TIEM" );
        Claw::String imageUrl = Claw::TextDict::Get()->GetText( "TEXT_FACEBOOK_ACTION_IMAGE" );
        CLAW_VERIFY( Claw::TextDict::ReplaceVar( imageUrl, "IMAGE", m_itemData.id ) );

        Claw::String message;
        GenerateShareMessage( message, m_itemData.name, m_itemData.category );

        Claw::String object = Claw::TextDict::Get()->GetText( "TEXT_FACEBOOK_ACTION_URL" );

        CLAW_VERIFY( Claw::TextDict::ReplaceVar( object, "APP_ID", MonstazAI::MonstazAIApplication::GetFacebookAppId() ) );
        CLAW_VERIFY( Claw::TextDict::ReplaceVar( object, "TYPE", "gl-ms-bte:item" ) );
        CLAW_VERIFY( Claw::TextDict::ReplaceVar( object, "TITLE", UrlEncode( Claw::NarrowString( Claw::TextDict::Get()->GetText( m_itemData.name ) ) ) ) );
        CLAW_VERIFY( Claw::TextDict::ReplaceVar( object, "DESC", UrlEncode( Claw::NarrowString( message ) ) ) );
        CLAW_VERIFY( Claw::TextDict::ReplaceVar( object, "IMAGE", UrlEncode( Claw::NarrowString( imageUrl ) ) ) );

        Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
        fb->PublishAction( "me/gl-ms-bte:buy", "item", Claw::NarrowString( object ).c_str() );
    }
    else if( m_facebookAction == FA_SEND_INVITATION )
    {
        CLAW_MSG( "MainMenuJob::PublishOnFacebook(): FA_SEND_INVITATION" );
        Claw::String invitationText;
        GenerateInvitationMessage( invitationText, false );

        Network::Facebook::RequestData request;
        request.message = Claw::NarrowString( invitationText );
        request.filter = Network::Facebook::RF_APP_NON_USERS;

        Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
        fb->SendRequest( request );
    }
}

void MainMenuJob::OnAuthenticationChange( bool authenticated )
{
    CLAW_MSG( "MainMenuJob::OnAuthenticationChange(): " << authenticated << " m_facebookAction = " << m_facebookAction );
    if( m_facebookAction != FA_NONE )
    {
        if( authenticated )
        {
            PublishOnFacebook();
        }
        else
        {
            // Authentication failed - give up
            m_facebookResponse.action = m_facebookAction;
            m_facebookResponse.success = false;
            m_facebookAction = FA_NONE;
        }
    }
    else
    {
        Claw::LuaPtr lua = m_screen->GetLuaState();

        if( authenticated )
        {
            UserDataManager::GetInstance()->SendFbUserAvatarRequest();
            m_userAvatarRequest = true;
        }
        else
        {
            lua->Call( "LoginViaSocialFail", 0 , 0 );
        }
    }
}

void MainMenuJob::OnGSAuthenticationChange( bool authenticated )
{
    Claw::LuaPtr lua = m_screen->GetLuaState();
    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();

    if ( m_gpAchievements )
    {
        if( authenticated )
        {
            Missions::MissionManager::GetInstance()->UnlockCachedAchievements();
            if ( !UserDataManager::GetInstance()->IsLogin() )
            {
                Claw::NarrowString gsUserId = gs->GetUserId();
                gs->GetAvatar( gsUserId.c_str() , UserDataManager::GetInstance()->GetAvatarsSize() );
                UserDataManager::GetInstance()->SendGetHasFbIdAccountRequest( UserDataManager::GetInstance()->ConstructGpUserId( gsUserId.c_str() ).c_str());
            }
            else
            {
                gs->OpenAchievementsUI();
            }
        }
        m_gpAchievements = false;
    }
    else
    {
        if( authenticated )
        {
            Claw::NarrowString gsUserId = gs->GetUserId();
            gs->GetAvatar( gsUserId.c_str() , UserDataManager::GetInstance()->GetAvatarsSize() );
            if ( !UserDataManager::GetInstance()->IsLogin() )
                UserDataManager::GetInstance()->SendGetHasFbIdAccountRequest( UserDataManager::GetInstance()->ConstructGpUserId( gsUserId.c_str() ).c_str());
            Missions::MissionManager::GetInstance()->UnlockCachedAchievements();
        }
        else
        {
            lua->Call( "LoginViaSocialFail", 0 , 0 );
        }
    }
}

void MainMenuJob::OnActionPublished( const char* id )
{
    CLAW_MSG( "MainMenuJob::OnActionPublished: " << id );
    if( m_facebookAction == FA_PUBLISH_TIEM )
    {
        m_facebookResponse.action = m_facebookAction;
        m_facebookResponse.success = id != NULL;
    }
    // Publishing is finished
    m_facebookAction = FA_NONE;
}

void MainMenuJob::OnRequestSent( const char* id )
{
    CLAW_MSG( "MainMenuJob::OnRequestSent: " << id );
    if( m_facebookAction == FA_SEND_INVITATION )
    {
        m_facebookResponse.action = m_facebookAction;
        m_facebookResponse.success = id != NULL;
    }
    m_facebookAction = FA_NONE;
}

void MainMenuJob::OnPlacementClosed( const Claw::NarrowString& placement )
{
    CLAW_MSG( "MainMenuJob::OnPlacementClosed(): " << placement );
    if( placement == PH_MORE_GAMES_PLACEMENT && !m_moreGamesShown )
    {
        // Show more games URL if playhaven can not display anything
        Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( MORE_GAMES_URL ) );
    }
}

void MainMenuJob::OnPlacementShown( const Claw::NarrowString& placement )
{
    CLAW_MSG( "MainMenuJob::OnPlacementShown(): " << placement );
    if( placement == PH_MORE_GAMES_PLACEMENT )
    {
        m_moreGamesShown = true;
    }
}

void MainMenuJob::OnPlacementFailed( const Claw::NarrowString& placement )
{
    CLAW_MSG( "MainMenuJob::OnPlacementFailed(): " << placement );
    if( placement == PH_MORE_GAMES_PLACEMENT )
    {
        Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( MORE_GAMES_URL ) );
    }
}

void MainMenuJob::OnAvatarsReceived( Network::Facebook::AvatarData& avatar )
{
    CLAW_MSG( "MainMenuJob::OnAvatarsReceived()" );

    m_avatar = avatar;

    if ( m_userAvatarRequest )
    {
        m_userAvatarRequest = false;
        UserDataManager::GetInstance()->SetFbUserId( m_avatar.m_uid.c_str() );
        UserDataManager::GetInstance()->SendGetHasFbIdAccountRequest( m_avatar.m_uid.c_str() );
    }

    if ( m_thread )
    {
        delete m_thread;
    }

    m_thread = new Claw::Thread( DoAvatars, this );
}

void MainMenuJob::OnGSAvatarReceived( const Network::GoogleServices::AvatarData& avatar )
{
    CLAW_MSG( "MainMenuJob::OnGSAvatarReceived()" );
    m_avatarGs = avatar;

    if ( m_threadGs )
    {
        delete m_threadGs;
    }

    m_threadGs = new Claw::Thread( DoGsAvatars, this );
}

int MainMenuJob::DoAvatars( void* data )
{
    MainMenuJob* state = static_cast<MainMenuJob*>( data );
    state->DownloadProfileImages( state->m_avatar.m_uid.c_str() , state->m_avatar.m_picture.c_str());
    return 0;
}

int MainMenuJob::DoGsAvatars( void* data )
{
    MainMenuJob* state = static_cast<MainMenuJob*>( data );
    state->DownloadProfileImages(  UserDataManager::GetInstance()->ConstructGpUserId( state->m_avatarGs.m_uid.c_str() ).c_str() , state->m_avatarGs.m_picture.c_str());
    return 0;
}

void MainMenuJob::DownloadProfileImages( const char* userId , const char* url )
{
    Claw::Network::Open();

    CLAW_MSG( "Avatars start dowload userID: " << userId << " pictureUrl " << url );


    Claw::NarrowString filename = UserDataInput::GetInstance()->GetTextImageFilename( userId );
    Claw::FilePtr file( Claw::OpenFile( filename ) );
    if ( !file )
    {
        Claw::Uri uri( url );
        Claw::HttpRequest request( uri );

        request.Connect();
        if ( !request.CheckError() )
        {
            request.Download();
            if ( !request.CheckError() )
            {
                file.Reset( Claw::VfsCreateFile( filename ) );
                if ( file )
                {
                    file->Write( request.GetData(), request.GetLength() );
                }
            }
        }
    }

    CLAW_MSG_WARNING( file, "Error during gp avatar download !!!" );

    Claw::Network::Close();
    CLAW_MSG("Avatar dowloaded");
}

int MainMenuJob::Preload()
{
    AtlasSet::Type unload[] = {
        AtlasSet::Boss1,
        AtlasSet::Boss1Shared,
        AtlasSet::Enemy1,
        AtlasSet::Enemy2,
        AtlasSet::Enemy3,
        AtlasSet::Enemy4,
        AtlasSet::Enemy5,
        AtlasSet::Enemy6,
        AtlasSet::Enemy7,
        AtlasSet::Enemy8,
        AtlasSet::EnvSuburbs,
        AtlasSet::EnvCity,
        AtlasSet::EnvDesert,
        AtlasSet::EnvIce,
        AtlasSet::Player01,
        AtlasSet::Player02,
        AtlasSet::Player03,
        AtlasSet::Player04,
        AtlasSet::Player05,
        AtlasSet::Player06,
        AtlasSet::Player07,
        AtlasSet::Player08,
        AtlasSet::Player09,
        AtlasSet::Player10,
        AtlasSet::Player10,
        AtlasSet::Player11,
        AtlasSet::Player12,
        AtlasSet::Player13,
        AtlasSet::Player14,
        AtlasSet::Player15,
        AtlasSet::Player16,
        AtlasSet::Player17,
        AtlasSet::Player18,
        AtlasSet::Terminator
    };
    AtlasManager::GetInstance()->Release( unload );

    while( !PakManager::GetInstance()->CheckDone( m_dlList ) ) { Claw::Time::Sleep( 1/60.f ); }
    PakManager::GetInstance()->Reset( m_dlList );
    if( TutorialManager::GetInstance()->GetCurrentChapter() == TutorialChapter::Shop )
    {
        PakManager::GetInstance()->Require( PakManager::SetEnemy2 | PakManager::SetEnemy7 );
    }

    const int map = Claw::Registry::Get()->CheckInt( "/maps/current" );
    AtlasSet::Type set[] = { (AtlasSet::Type)( AtlasSet::Map01 + map - 1 ), AtlasSet::Terminator };
    AtlasManager::GetInstance()->Request( set );
    while( !AtlasManager::GetInstance()->AreLoaded( set ) ) { Claw::Time::Sleep( 1/60.f ); }

    m_screen.Reset( new Guif::Screen() );
    Claw::LuaPtr lua = m_screen->GetLuaState();

    lua->RegisterLibrary( Claw::Lua::L_MATH );
    lua->RegisterLibrary( Claw::Lua::L_STRING );

    RegisterMath( lua );

    char tmp[48];
    sprintf( tmp, "math.randomseed(%i)", g_rng.GetInt() );
    lua->Execute( tmp );

    m_audioManager->Init( lua );
    VibraController::GetInstance()->Init( lua );

    MonstazApp::PushScreenModes( lua );

    Claw::Lunar<MainMenuJob>::Register( *lua );
    Claw::Lunar<MainMenuJob>::push( *lua, this );
    lua->RegisterGlobal( "callback" );

    Claw::Lunar<Claw::Registry>::Register( *lua );
    Claw::Lunar<Claw::Registry>::push( *lua, Claw::Registry::Get() );
    lua->RegisterGlobal( "registry" );

    Claw::Lunar<Claw::TextDict>::Register( *lua );
    Claw::Lunar<Claw::TextDict>::push( *lua, Claw::TextDict::Get() );
    lua->RegisterGlobal( "TextDict" );

    Shop::GetInstance()->Init( lua );
    GameCenterManager::GetInstance()->Init( lua );
    GameEventDispatcher::GetInstance()->InitLua( lua );
    Missions::MissionManager::GetInstance()->InitLua( lua, true );

    AnalyticsManager::GetInstance()->SetLua( lua );
    ConnectionMonitor::GetInstance()->SetLua( lua );

    EntityManager::InitEnum( lua );
    AtlasManager::InitEnum( lua );
    PickupManager::InitEnum( lua );
    ShotManager::InitEnum( lua );
    Map::InitEnum( lua );

    TutorialManager* tm = TutorialManager::GetInstance();
    tm->Init( lua );
    tm->SetMainMenuLua( lua );

    lua->Load( "menu2/mainmenu.lua" );

    UserDataManager::GetInstance()->InitLua( lua );
    UserDataInput::GetInstance()->Initialize();
    UserDataInput::GetInstance()->InitLua( lua );

    // Apply synchronized data
    Claw::FilePtr luaScript = ClawExt::ServerSync::GetInstance()->LockTaskFile( ServerConstants::LUA_MENU_SYNC_TASK );
    if( luaScript )
    {
        lua->Load( luaScript, ServerConstants::LUA_MENU_REMOTE_FILE );
        lua->Call( "Synchronize", 0 , 0 );
        ClawExt::ServerSync::GetInstance()->ReleaseTaskFile( ServerConstants::LUA_MENU_SYNC_TASK, luaScript );
    }
    lua->Call( "MainMenuInit", 0 , 0 );

    tm->OnMainMenu();

    // Valuidate subscriptions on entering menu
    Shop::GetInstance()->CheckSubscriptions();

    if( m_startScreen == SS_SHOP )
    {
        const char* tab = "";
        Claw::Registry::Get()->Get( "/internal/shop-startup-tab", tab );
        if ( strlen( tab ) > 0 )
        {
            lua->PushString( tab );
            lua->PushBool( true );
            lua->PushNil();

            const char* item = "";
            Claw::Registry::Get()->Get( "/internal/shop-startup-item", item );
            if ( strlen( item ) > 0 )
            {
                lua->PushString( item );
            }
            else
            {
                lua->PushNil();
            }

            Claw::Registry::Get()->Set( "/internal/shop-startup-tab", "" );
            Claw::Registry::Get()->Set( "/internal/shop-startup-item", "" );

            lua->Call( "ShopShow", 4, 0 );
        }
        else
        {
            lua->PushNil();
            lua->PushBool( true );
            lua->Call( "ShopShow", 2, 0 );
        }
    }

    m_preloadDone = true;
    return 0;
}

int MainMenuJob::l_ResistanceLogin( lua_State* L )
{
    Claw::Lua lua( L );

    const char* login = lua.CheckCString( 1 );
    const char* pass = lua.CheckCString( 2 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendLoginRequest( login, pass);

    return 0;
}

int MainMenuJob::l_ResistanceCreateUser( lua_State* L )
{
    Claw::Lua lua( L );

    const char* login = lua.CheckCString( 1 );
    const char* email = lua.CheckCString( 2 );
    const char* pass = lua.CheckCString( 3 );
    const char* lang = "cn";
    Claw::NarrowString ref = Claw::HardwareKey::Get();
    const char* hw_key = ref.c_str();
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendCreateUserRequest( login,email,pass,lang , hw_key );
    return 0;
}

int MainMenuJob::l_ResistanceRecoverPassword( lua_State* L )
{
    Claw::Lua lua( L );
    const char* email = lua.CheckCString( 1 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendRemindPasswordRequest( email );
    return 0;
}

int MainMenuJob::l_ResistanceOnAcceptedInvite( lua_State* L )
{
    Claw::Lua lua( L );
    const char* friendId = lua.CheckCString( 1 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendFriendRequestResponse( friendId , true );
    return 0;
}

int MainMenuJob::l_ResistanceOnAcceptedGift( lua_State* L )
{
    Claw::Lua lua( L );  
    UserDataManager::GetInstance()->SendSetGiftAccepted( lua.CheckCString( 1 ) );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    return 0;
}

int MainMenuJob::l_ResistanceOnSendGift( lua_State* L )
{
    Claw::Lua lua( L );

    const char* itemId = lua.CheckCString( 1 );
    const char* name = lua.CheckCString( 2 );
    const char* quantity = lua.CheckCString( 3 );

    if ( UserDataManager::GetInstance()->IsBot ( lua.CheckCString(2) ))
    {
        lua.PushString( name );
        lua.Call( "SendGiftSucces", 1, 0 );

        Claw::UInt32 now = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime();
        if( now == 0 ) now = Claw::Time::GetTime();
        Claw::Registry::Get()->Set( "/monstaz/aifriend/sendGift" , int ( now ) );

        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FRIEND_GIFT_SENT );
        return 0;
    }
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendSendGiftRequest( itemId , name , quantity );
    return 0;
}

int MainMenuJob::l_ResistanceOnRejectInvite( lua_State* L )
{
    Claw::Lua lua( L );
    const char* friendId = lua.CheckCString( 1 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendFriendRequestResponse( friendId , false );
    return 0;
}

int MainMenuJob::l_ResistanceShowTermsOfUse(  lua_State* L )
{
    Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( "http://www.game-lion.com/about/about.php?go=terms_of_use" ) );
    return 0;
}

int MainMenuJob::l_ResistanceSaveAccountSettings( lua_State* L )
{
    Claw::Lua lua( L );
    const char* email = lua.CheckCString( 1 );
    bool emailNotChanged = lua.CheckBool( 2 );
    const char* newPass = lua.CheckCString( 3 );
    bool passNotChanged = lua.CheckBool( 4 );

    if ( !emailNotChanged && !passNotChanged )
    {
         lua.Call( "SyncIndicatorShow", 0 , 0 );
        UserDataManager::GetInstance()->SendSetEmailPasswordChange( email, newPass );
    }
    else
    {
        if ( !passNotChanged )
        {
            UserDataManager::GetInstance()->SendChangePasswordRequest( newPass );
            lua.Call( "SyncIndicatorShow", 0 , 0 );
        }

        if ( !emailNotChanged )
        {
            UserDataManager::GetInstance()->SendChangeEmailRequest( email );
            lua.Call( "SyncIndicatorShow", 0 , 0 );
        }
    }

    return 0;
}

int MainMenuJob::l_OpenVkb( lua_State* L )
{
    Claw::Lua lua( L );
    const char* startString = lua.CheckCString( 1 );
    int maxLength = lua.CheckNumber( 2 );
    Claw::Application::GetInstance()->OpenVkb( startString , maxLength );
    lua.Call( "StartCursorAnimation", 0 , 0 );

    return 0;
}

int MainMenuJob::l_ResistanceOnSendInvite( lua_State* L )
{
    Claw::Lua lua( L );
    const char* friendUniqueId = lua.CheckCString( 1 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendAddFriendRequest( friendUniqueId );
    return 0;
}

int MainMenuJob::l_ResistanceOnGetFriendInfo( lua_State* L )
{
    Claw::Lua lua( L );
    const char* friendUniqueId = lua.CheckCString( 1 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendGetFriendInfoRequest( friendUniqueId );
    UserDataManager::GetInstance()->ForceSendRequests();
    return 0;
}

int MainMenuJob::l_ResistanceGetUsernamePath( lua_State* L )
{
    Claw::Lua lua( L );
    const char* friendName = lua.CheckCString( 1 );
    lua.PushString( UserDataInput::GetInstance()->GetTextImageFilename( friendName ) );
    return 1;
}

int MainMenuJob::l_ResistanceSetFriendAsBackup( lua_State* L )
{
    Claw::Lua lua( L );
    const char* friendUniqueId = lua.CheckCString( 1 );
    
    if ( UserDataManager::GetInstance()->IsBot ( friendUniqueId ) )
    {
        Claw::UInt32 now = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime();
        if( now == 0 ) now = Claw::Time::GetTime();
        Claw::Registry::Get()->Set( "/monstaz/aifriend/canHelp" , int (now) );
        return 0;
    }

    UserDataManager::GetInstance()->SendSetFriendHelpRequest( friendUniqueId );
    UserDataManager::GetInstance()->SyncData();
    return 0;
}

int MainMenuJob::l_ResistanceOnRewardNewFriendsAccepted( lua_State* L )
{
    Claw::Lua lua( L );
    lua.Call( "PopupLock", 0 , 0 );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    UserDataManager::GetInstance()->SendSetApproveNewFriends();
    return 0;
}

int MainMenuJob::l_ResistanceRefreshInputs( lua_State* L )
{
    UserDataInput::GetInstance()->RefreshInputs();
    return 0;
}

int MainMenuJob::l_ResistanceForceRequestSend( lua_State* L )
{
    Claw::Lua lua( L );

    return 0;
}

void MainMenuJob::OnSynchronisationStart()
{
    m_syncResult = false;
    CLAW_MSG( "MainMenuJob::OnSynchronisationStart()" );
}

void MainMenuJob::OnSynchronisationEnd( bool success )
{
    CLAW_MSG( "MainMenuJob::OnSynchronisationEnd(): " << success );
   // UserDataManager::GetInstance()->HandleRequests();
    m_syncResult = true;
}

void MainMenuJob::OnSynchronisationSkip()
{
   // CLAW_MSG( "MainMenuJob::OnSynchronisationSkip()" );
}

void MainMenuJob::OnRequestFinished( bool success )
{
    CLAW_MSG( "MainMenuJob::OnTaskFinished(): " << success );
}

int MainMenuJob::l_ResistanceLoginLastUser( lua_State* L )
{
    Claw::Lua lua( L );
    const char* uniqueId = lua.CheckCString( 1 );
    UserDataManager::GetInstance()->UserLoggedIn( uniqueId );
    UserDataManager::GetInstance()->UpdateResistanceDB();
    return 0;
}

int MainMenuJob::l_ResistanceLoggedOut( lua_State* L )
{
    Claw::Lua lua( L );
    UserDataManager::GetInstance()->UserLoggedOut();
    return 0;
}

int MainMenuJob::l_ResistanceCreateUserViaFb( lua_State* L )
{
    Claw::Lua lua( L );

    const char* name  = lua.CheckCString( 1 );
    const char* fbId = lua.CheckCString( 2 );
    const char* lang ="cn";
    const char* hw_key = Claw::HardwareKey::Get().c_str();
    UserDataManager::GetInstance()->SendCreateUserFBRequest( name, fbId , lang, hw_key );
    lua.Call( "SyncIndicatorShow", 0 , 0 ); 
    return 0;
}

int MainMenuJob::l_ResistanceLoginUserViaFb( lua_State* L )
{
    Claw::Lua lua( L );

    const char* name  = lua.CheckCString( 1 );
    const char* fbId = lua.CheckCString( 2 );

    UserDataManager::GetInstance()->SendLoginWithFBRequest( name, fbId );
    lua.Call( "SyncIndicatorShow", 0 , 0 );
    return 0;
}

int MainMenuJob::l_ResistanceFB( lua_State* L )
{
    m_facebookAction = FA_NONE;
    Claw::Lua lua( L );
    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
    if( fb->IsAuthenticated())
    {
        if ( UserDataManager::GetInstance()->IsFbUserSet() )
        {
            UserDataManager::GetInstance()->SendGetHasFbIdAccountRequest( UserDataManager::GetInstance()->GetFbUserId() );
            lua.Call( "SyncIndicatorShow", 0 , 0 ); 
        }
        else
        {
            m_userAvatarRequest = true;
            UserDataManager::GetInstance()->SendFbUserAvatarRequest();
        }
    }
    else
    {
        lua.Call( "PopupUnlock", 0 , 0 ); 
        fb->Authenticate( true );
    }
    return 0;
}

int MainMenuJob::l_ResistanceIsFbAvatarAvaible( lua_State* L )
{
    Claw::Lua lua( L );
    const char* path = lua.CheckCString( 1 );
    
    Claw::FilePtr file( Claw::OpenFile( path ) );

    if ( !file )
        lua.PushBool( false );
    else
        lua.PushBool ( Claw::ImageLoader::Compatible( file ));
    return 1;
}

int MainMenuJob::l_ResistanceGetFbId( lua_State* L )
{
    m_facebookAction = FA_NONE;

    Claw::Lua lua( L );
    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();

    if( fb->IsAuthenticated() )
    {
        if ( UserDataManager::GetInstance()->IsFbUserSet() )
        {
            lua.PushString ( UserDataManager::GetInstance()->GetFbUserId() );
        }
        else
        {
            UserDataManager::GetInstance()->SendFbUserAvatarRequest();
            lua.PushString( "unknown" );
        }
    }
    else
    {
        fb->Authenticate( true );
        lua.PushString( "unknown" );
    }

    return 1;
}

int MainMenuJob::l_GetTime( lua_State* L )
{
    lua_pushnumber( L, Claw::Time::GetTime() );
    return 1;
}

int MainMenuJob::l_GetNetworkTime( lua_State* L )
{
    lua_pushnumber( L, ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime() );
    return 1;
}


int MainMenuJob::l_SetClipboard(  lua_State* L )
{
    Claw::Lua lua( L );
    const char* text = lua.CheckCString( 1 );

    Claw::Application::GetInstance()->SetClipboard( text );
    return 0;
}

int MainMenuJob::l_ResistanceRemoveFriend(  lua_State* L )
{
    Claw::Lua lua( L );
    UserDataManager::GetInstance()->SendDeleteFriend( lua.CheckCString( 1 ) );
    return 0;
}

int MainMenuJob::l_ResistanceUpdateAll(  lua_State* L )
{
    UserDataManager::GetInstance()->UpdateAll();
    return 0;
}

int MainMenuJob::l_ResistanceSetUsernameImg( lua_State* L )
{
    Claw::Lua lua( L );
    const char* path = lua.CheckCString( 1 );
    const char* userId = lua.CheckCString( 2 );

    Guif::Control* control = m_screen->FindControl( path );

    UserDataInput::GetInstance()->SetSurfaceForControl( control ,UserDataInput::GetInstance()->GetSurfaceForUser( userId ));
    return 0;
}

int MainMenuJob::l_ResistanceSpeedUpFriendBackup( lua_State* L )
{
    Claw::Lua lua( L );
    const char* userId = lua.CheckCString( 1 );
    UserDataManager::GetInstance()->SendResetFriendHelpRequest( userId );
    return 0;
}

int MainMenuJob::l_ResistanceConfirmGifts( lua_State* L )
{
    UserDataManager::GetInstance()->SendSetGiftAcceptedAll();
    return 0;
}

void MainMenuJob::CheckVkbStatus()
{
    if ( !m_wasVkbOpened )
    {
        if ( Claw::Application::GetInstance()->IsVkbOpened() ) m_wasVkbOpened = true;
    }
    else 
    {
        if (!Claw::Application::GetInstance()->IsVkbOpened())
        {
            Claw::LuaPtr lua = m_screen->GetLuaState();
            lua->Call( "OnCloseVkb", 0 , 0 );
            m_wasVkbOpened = false;
        }
    }
}

int MainMenuJob::l_GetInputLenght( lua_State* L )
{
    Claw::Lua lua( L );
    int number = wcslen( Claw::WideString(lua.CheckCString( 1 )).c_str() );
    lua.PushNumber( number );
    return 1;
}

int MainMenuJob::l_IsSMSSupported( lua_State* L )
{
    SmsService* sms = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetSmsService();
    Claw::Lua lua(L);
    lua.PushBool( sms->CanSendSms() );
    return 1;
}

int MainMenuJob::l_IsEmailSupported( lua_State* L )
{
    EmailService* email = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetEmailService();
    Claw::Lua lua(L);
    lua.PushBool( email->CanSendEmail() );
    return 1;
}

int MainMenuJob::l_SendSmsInvitation( lua_State* L )
{
    Claw::String invitationText;
    GenerateInvitationMessage( invitationText, false );
    SmsService* sms = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetSmsService();
    sms->SendSms( Claw::NarrowString( invitationText ) );
    return 0;
}

int MainMenuJob::l_SendEmailInvitation( lua_State* L )
{
    Claw::String invitationText;
    GenerateInvitationMessage( invitationText, true );
    Claw::String title = Claw::TextDict::Get()->GetText( "TEXT_INVITATION_MAIL_TITLE" );

    EmailService* email = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetEmailService();
    email->SendEmail( Claw::NarrowString( title ), Claw::NarrowString( invitationText ), true );
    return 0;
}

int MainMenuJob::l_SendFacebookInvitation( lua_State* L )
{
    m_facebookAction = FA_SEND_INVITATION;
    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
    if( fb->IsAuthenticated() )
    {
        PublishOnFacebook();
    }
    else
    {
        fb->Authenticate( true );
    }
    return 0;
}

int MainMenuJob::l_GetCurrentDateString( lua_State* L )
{
    time_t rawtime = time( NULL );
    tm* timeinfo = localtime( &rawtime );

    std::ostringstream ss;
    ss << timeinfo->tm_year << "-" << timeinfo->tm_mon << "-" << timeinfo->tm_mday;

    Claw::Lua lua(L);
    lua.PushString( ss.str() );
    return 1;
}

int MainMenuJob::l_OpenAchievementsUI( lua_State* L )
{
    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
    if( gs->IsAuthenticated() )
    {
        gs->OpenAchievementsUI();
    }
    else
    {
        m_gpAchievements = true;
        gs->Authenticate( true );
    }
    return 0;
}

int MainMenuJob::l_GetVersionString( lua_State* L )
{
    Claw::Lua lua(L);
    lua.PushString( AnalyticsManager::GetInstance()->GenerateBuildName( true ) );
    return 1;
}

int MainMenuJob::l_BuyOST( lua_State* L )
{
    Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( OST_URL ) );
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FREE_STUFF_ACTION, GEP_FREE_STUFF_SOUNDTRACK );
    return 0;
}

int MainMenuJob::l_ResistanceSetVipStatus( lua_State* L )
{
    Claw::Lua lua( L );
    bool enable = lua.CheckBool( 1 );
    UserDataManager::GetInstance()->SendSetVipStatus( enable );
    return 0;
}

int MainMenuJob::l_ResistanceGooglePlus( lua_State* L )
{
    Claw::Lua lua( L );

    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
    if( gs->IsAuthenticated())
    {
        gs->GetAvatar( gs->GetUserId().c_str() , UserDataManager::GetInstance()->GetAvatarsSize() );
        UserDataManager::GetInstance()->SendGetHasFbIdAccountRequest( UserDataManager::GetInstance()->ConstructGpUserId( gs->GetUserId().c_str() ).c_str() );
    }
    else
    {
        gs->Authenticate( true );
    }
    return 0;
}

int MainMenuJob::l_ResistanceGetGpId( lua_State* L )
{
    Claw::Lua lua( L );
    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
    if( gs->IsAuthenticated() )
    {
        lua.PushString( UserDataManager::GetInstance()->ConstructGpUserId( gs->GetUserId().c_str() ).c_str() );
    }
    else
    {
        lua.PushString( "unknown" );
    }

    return 1;
}

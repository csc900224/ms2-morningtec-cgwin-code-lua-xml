#include "MonstazAI/job/VersionCheckJob.hpp"
#include "MonstazAI/job/IntroJob.hpp"
#include "MonstazAI/ServerConstants.hpp"
#include "MonstazAI/Application.hpp"
#include "AppVersion.hpp"

#include "claw/base/Registry.hpp"
#include "claw_ext/network/server_sync/ServerSync.hpp"

#include <sstream>

#if defined AMAZON_BUILD
    static const char* GET_IT_URL      = "amzn://apps/android?p=com.gamelion.ms2.amazon";
#elif defined CLAW_ANDROID
    static const char* GET_IT_URL      = "market://details?id=com.gamelion.ms2";
#else
    static const char* GET_IT_URL      = "https://itunes.apple.com/app/id623241721?ls=1&mt=8";
#endif

LUA_DECLARATION( VersionCheckJob )
{
    METHOD( VersionCheckJob, IsUpdateMandatory ),
    METHOD( VersionCheckJob, Update ),
    METHOD( VersionCheckJob, Continue ),
    METHOD( VersionCheckJob, Exit ),
    {0,0}
};

VersionCheckJob::VersionCheckJob()
    : m_active( false )
{
}

VersionCheckJob::~VersionCheckJob()
{
}

void VersionCheckJob::Initialize()
{
    //LoadVersionData();

    //if( IsNewVersionAvailable() )
    //{
    //    // Activate state
    //    m_active = true;
    //    LoadLayout();
    //}
}

void VersionCheckJob::Render( Claw::Surface* target )
{
    target->Clear(0);
    if( m_screen )
    {
        m_screen->Render( target );
    }
}

void VersionCheckJob::Update( float dt )
{
    if( !m_active )
    {
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new IntroJob() );
    }
    else if( m_screen )
    {
        m_screen->Update( dt );
    }
}

void VersionCheckJob::LoadVersionData()
{
    Claw::FilePtr dataFile = ClawExt::ServerSync::GetInstance()->LockTaskFile( ServerConstants::APP_VERSION_SYNC_TASK );
    if( dataFile )
    {
        Claw::Registry::Get()->Load( dataFile, true );
        ClawExt::ServerSync::GetInstance()->ReleaseTaskFile( ServerConstants::APP_VERSION_SYNC_TASK, dataFile );
    }
}

bool VersionCheckJob::IsNewVersionAvailable()
{
    const char* appVersion = NULL;
    Claw::Registry::Get()->Get( "/app-version/value", appVersion );

    if( appVersion )
    {
        Claw::NarrowString currentVersion( Claw::g_appVersion );
        Claw::NarrowString syncVesrion( appVersion );

        int currentMajor, currentMinor, currentBuild;
        ExtractVersion( currentVersion, currentMajor, currentMinor, currentBuild );

        int syncMajor, syncMinor, syncBuild;
        ExtractVersion( syncVesrion, syncMajor, syncMinor, syncBuild );

        return syncMajor > currentMajor 
            || syncMajor == currentMajor && syncMinor > currentMinor 
            || syncMajor == currentMajor && syncMinor == currentMinor && syncBuild > currentBuild;
    }
    else
    {
        return false;
    }
}

bool VersionCheckJob::IsUpdateMandatory()
{
    bool mandatory = false;
    Claw::Registry::Get()->Get( "/app-version/mandatory", mandatory );
    return mandatory;
}

void VersionCheckJob::ExtractVersion( const Claw::NarrowString& versionString, int& major, int& minor, int& build ) const
{
    major = minor = build = 0;
    size_t startPos = 0;

    std::string tmpString = versionString;
    while( (startPos = tmpString.find( ".", startPos )) != std::string::npos )
        tmpString.replace( startPos++, 1, " " );

    std::stringstream versionStream( tmpString );
    versionStream >> major >> minor >> build;
}

void VersionCheckJob::LoadLayout()
{
    m_screen.Reset( new Guif::Screen() );
    Claw::LuaPtr lua = m_screen->GetLuaState();

    lua->RegisterLibrary( Claw::Lua::L_MATH );
    lua->RegisterLibrary( Claw::Lua::L_STRING );

    Claw::Lunar<VersionCheckJob>::Register( *lua );
    Claw::Lunar<VersionCheckJob>::push( *lua, this );
    lua->RegisterGlobal( "callback" );

    MonstazApp::PushScreenModes( lua );

    lua->Load( "menu2/version_check.lua" );
    lua->Call( "VersionPopupShow", 0, 0 );
}

void VersionCheckJob::TouchUp( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    m_screen->OnTouchUp( x, y, button );
}

void VersionCheckJob::TouchDown( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    m_screen->OnTouchDown( x, y, button );
}

void VersionCheckJob::TouchMove( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH ) return;
    m_screen->OnTouchMove( x, y, button );
}

int VersionCheckJob::l_IsUpdateMandatory( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushBool( IsUpdateMandatory() );
    return 1;
}

int VersionCheckJob::l_Update( lua_State* L )
{
    Claw::Application::GetInstance()->OpenWebBrowser( Claw::String( GET_IT_URL ) );
    return 0;
}

int VersionCheckJob::l_Exit( lua_State* L )
{
    Claw::Application::GetInstance()->Exit();
    return 0;
}

int VersionCheckJob::l_Continue( lua_State* L )
{
    m_active = false;
    return 0;
}


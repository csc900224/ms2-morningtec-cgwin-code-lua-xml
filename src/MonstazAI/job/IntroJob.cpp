#include "MonstazAI/job/IntroJob.hpp"
#include "MonstazAI/job/MainMenuJob.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/MonetizationManager.hpp"

IntroJob::IntroJob()
{}

void IntroJob::Initialize()
{
}

void IntroJob::Render( Claw::Surface* target )
{
    target->Clear( 0 );
}

void IntroJob::Update( float dt )
{
    if ( TutorialManager::GetInstance()->GetCurrentChapter() == TutorialChapter::Intro )
    {
        StartIntroLevel();
    }
    else
    {
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new MainMenuJob() );
    }
}

void IntroJob::StartIntroLevel()
{
    Claw::Registry::Get()->Set( "/internal/story", true );
    Claw::Registry::Get()->Set( "/internal/storylevel", 0 );
    Claw::Registry::Get()->Set( "/internal/survival", false );
    Claw::Registry::Get()->Set( "/internal/survivalplanet", 1 );
    Claw::Registry::Get()->Set( "/internal/survivalworld", 1 );

    Claw::Registry::Get()->Set( "/internal/boss", false );
    Claw::Registry::Get()->Set( "/internal/map", 0 );
    Claw::Registry::Get()->Set( "/internal/area", 1 );
    Claw::Registry::Get()->Set( "/internal/friend", false );
    Claw::Registry::Get()->Set( "/internal/monsters/1", Entity::SectoidSimple );
    Claw::Registry::Get()->Set( "/internal/monsters/2", Entity::SectoidShooting );
    Claw::Registry::Get()->Set( "/internal/monsters/3", Entity::NautilSimple );
    Claw::Registry::Get()->Set( "/internal/monsters/4", -1 );

    Claw::Registry::Get()->Set( "/internal/reward/soft", Claw::Registry::Get()->CheckInt( "/maps/0/reward/soft" ) );
    Claw::Registry::Get()->Set( "/internal/reward/xp", Claw::Registry::Get()->CheckInt( "/maps/0/reward/xp" ) );

    Claw::Registry::Get()->Set( "/internal/levelname", "TEXT_LEVEL_TUTORIAL_01_NAME" );

    Claw::LuaPtr lua( new Claw::Lua() );
    AtlasManager::InitEnum( lua );
    EntityManager::InitEnum( lua );
    lua->RegisterLibrary( Claw::Lua::L_BASE );
    lua->RegisterLibrary( Claw::Lua::L_TABLE );
    lua->Load( "LevelDB.lua" );

    lua_getglobal( *lua, "Tutorial01" );
    CLAW_ASSERT( lua_istable( *lua, -1 ) );

    lua_pushstring( *lua, "f" );
    lua_gettable( *lua, -2 );
    CLAW_ASSERT( lua_isstring( *lua, -1 ) );

    Claw::NarrowString level = lua->CheckString( -1 );
    lua->Pop( 1 );

    AtlasSet::Type set[] = {
        AtlasSet::Player16,
        AtlasSet::EnvSuburbs,
        AtlasSet::EnvUrban,
        AtlasSet::Boss1Shared,
        AtlasSet::Terminator
    };

    AtlasManager::GetInstance()->Request( set );

    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new GameplayJob( level ) );
}

#ifndef __MONSTAZ_JOB_HPP__
#define __MONSTAZ_JOB_HPP__

#include "claw/application/KeyCodes.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

class Job : public Claw::RefCounter
{
public:
    Job();
    virtual ~Job();

    virtual void Initialize() = 0;
    virtual void Render( Claw::Surface* target ) = 0;
    virtual void Update( float dt ) = 0;
    virtual void KeyPress( Claw::KeyCode code ) {}
    virtual void KeyRelease( Claw::KeyCode code ) {}
    virtual void TouchUp( int x, int y, int button ) {}
    virtual void TouchDown( int x, int y, int button ) {}
    virtual void TouchMove( int x, int y, int button ) {}
    virtual void Resize( int x, int y ) {}
    virtual void Focus( bool focus ) {}
    virtual void OnTouchDeviceChange() {}

    virtual void OnText( const char* text  ){}
    virtual void OnTextNewLine(){}

    virtual void MenuDump() {}
};

typedef Claw::SmartPtr<Job> JobPtr;

#endif

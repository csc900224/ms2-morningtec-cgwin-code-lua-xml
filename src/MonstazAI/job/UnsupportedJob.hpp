#ifndef __MONSTAZ_UNSUPPORTEDJOB_HPP__
#define __MONSTAZ_UNSUPPORTEDJOB_HPP__

#include "MonstazAI/job/Job.hpp"
#include "claw/graphics/FontEx.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/graphics/text/Format.hpp"
#include "claw/graphics/text/FontSet.hpp"

class UnsupportedJob : public Job
{
public:
    UnsupportedJob();
    ~UnsupportedJob();

    void Initialize();

    void Render( Claw::Surface* target );
    void Update( float dt );
    void TouchDown( int x, int y, int button );

private:
    enum { NUM_PARAGRAPHS = 2 };

    Claw::FontExPtr         m_font;
    Claw::ScreenTextPtr     m_text[NUM_PARAGRAPHS];
    Claw::Text::Format      m_format;
    Claw::Text::FontSetPtr  m_fontset;
    float m_timer;
    bool m_tapVisible;
};

#endif

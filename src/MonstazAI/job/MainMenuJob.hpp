#ifndef __MONSTAZ_MAINMENUJOB_HPP__
#define __MONSTAZ_MAINMENUJOB_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/Thread.hpp"

#include "guif/Screen.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/job/Job.hpp"
#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/Loading.hpp"
#include "MonstazAI/db/UserDataManager.hpp"
#include "MonstazAI/network/facebook/Facebook.hpp"
#include "MonstazAI/network/googleservices/GoogleServices.hpp"

#include "claw_ext/monetization/playhaven/Playhaven.hpp"
#include "claw_ext/network/twitter/TwitterService.hpp"
#include "claw_ext/network/sms/SmsService.hpp"
#include "claw_ext/network/email/EmailService.hpp"

class MainMenuJob 
    : public Job
    , public UserDataManager::Observer
    , public ClawExt::Playhaven::PlacementObserver
    , public TwitterServiceListener
    , public SmsServiceListener
    , public EmailServiceListener
    , public Network::Facebook::Observer
    , public Network::GoogleServices::Observer
{
public:
    enum StartScreen
    {
        SS_MAP,
        SS_SHOP
    };

    LUA_DEFINITION( MainMenuJob );
    MainMenuJob( lua_State* L ) : m_startScreen( SS_MAP ) { CLAW_ASSERT( false ); }

    MainMenuJob( StartScreen start = SS_MAP );
    ~MainMenuJob();

    void Initialize();

    void Render( Claw::Surface* target );
    void Update( float dt );
    void KeyPress( Claw::KeyCode code );
    void TouchUp( int x, int y, int button );
    void TouchDown( int x, int y, int button );
    void TouchMove( int x, int y, int button );
    void Focus( bool focus );

    void OnText( const char* text );
    void OnTextNewLine();

    void MenuDump() { m_screen->DumpState(); }

    Claw::Lua* GetLua() { return m_screen->GetLuaState(); }

    int l_StartGame( lua_State* L );
    int l_ResetData( lua_State* L );
    int l_Url( lua_State* L );
    int l_Save( lua_State* L );
    int l_LogEvent( lua_State* L );
    int l_ExitApplication( lua_State* L );
    int l_GetControlSize( lua_State* L );
    int l_Playhaven( lua_State* L );
    int l_RequestAtlas( lua_State* L );
    int l_ReleaseAtlas( lua_State* L );
    int l_IsAtlasLoaded( lua_State* L );
    int l_MoreGames( lua_State* L );
    int l_OnFacebookButton( lua_State* L );
    int l_OnTwitterButton( lua_State* L );
    int l_OnRateUsButton( lua_State* L );
    int l_CanSendTweet( lua_State* L );
    int l_SendTweet( lua_State* L );
    int l_PublishOnFacebook( lua_State* L );
    int l_GetTime( lua_State* L );
    int l_GetNetworkTime( lua_State* L );
    int l_GetVersionString( lua_State* L );
    int l_BuyOST( lua_State* L );

    int l_ResistanceLogin( lua_State* L );
    int l_ResistanceCreateUser( lua_State* L );
    int l_ResistanceRecoverPassword( lua_State* L );
    int l_ResistanceOnAcceptedInvite( lua_State* L );
    int l_ResistanceOnSendGift( lua_State* L );
    int l_ResistanceOnAcceptedGift( lua_State* L );
    int l_ResistanceOnRejectInvite( lua_State* L );
    int l_ResistanceShowTermsOfUse( lua_State* L );
    int l_ResistanceSaveAccountSettings( lua_State* L );
    int l_OpenVkb( lua_State* L );
    int l_ResistanceOnSendInvite( lua_State* L );
    int l_ResistanceOnGetFriendInfo( lua_State* L );
    int l_ResistanceSetFriendAsBackup( lua_State* L );
    int l_ResistanceOnRewardNewFriendsAccepted( lua_State* L );
    int l_ResistanceGetUsernamePath( lua_State* L );
    int l_ResistanceRefreshInputs( lua_State* L );
    int l_ResistanceForceRequestSend( lua_State* L );
    int l_ResistanceLoginLastUser( lua_State* L );
    int l_ResistanceLoggedOut( lua_State* L );
    int l_ResistanceRemoveFriend( lua_State* L );
    //force update 
    int l_ResistanceUpdateAll( lua_State* L );

    int l_ResistanceFB( lua_State* L );
    int l_ResistanceCreateUserViaFb( lua_State* L );
    int l_ResistanceLoginUserViaFb( lua_State* L );
    int l_ResistanceIsFbAvatarAvaible( lua_State* L );
    int l_ResistanceGetFbId( lua_State* L );
    int l_ResistanceGetGpId( lua_State* L );

    int l_SetClipboard( lua_State* L );
    int l_ResistanceSetUsernameImg( lua_State* L );
    int l_ResistanceSpeedUpFriendBackup( lua_State* L );
    int l_ResistanceConfirmGifts( lua_State* L );
    int l_ResistanceSetVipStatus( lua_State* L );
    int l_GetInputLenght( lua_State* L );

    int l_ResistanceGooglePlus( lua_State* L );

    int l_IsSMSSupported( lua_State* L );
    int l_IsEmailSupported( lua_State* L );
    int l_SendSmsInvitation( lua_State* L );
    int l_SendEmailInvitation( lua_State* L );
    int l_SendFacebookInvitation( lua_State* L );
    int l_GetCurrentDateString( lua_State* L );

    int l_OpenAchievementsUI( lua_State* L );

    // UserDataManager::Observer
    virtual void OnSynchronisationStart();
    virtual void OnSynchronisationEnd( bool success );
    virtual void OnSynchronisationSkip();
    virtual void OnRequestFinished( bool success );

    // ClawExt::Playhaven::PlacementObserver interface
    virtual void OnPlacementClosed( const Claw::NarrowString& placement );
    virtual void OnPlacementShown( const Claw::NarrowString& placement );
    virtual void OnPlacementFailed( const Claw::NarrowString& placement );

    // TwitterServiceListener interface
    virtual void OnTweetSent( bool success );

    // SmsServiceListener interface
    virtual void OnSmsSent( bool success );

    // EmailServiceListener interface
    virtual void OnEmailSent( bool success );

    // Network::Facebook::Observer interface 
    virtual void OnAuthenticationChange( bool authenticated );
    virtual void OnActionPublished( const char* id );
    virtual void OnRequestSent( const char* id );
    virtual void OnAvatarsReceived( Network::Facebook::AvatarData& avatar );
    Network::Facebook::AvatarData m_avatar;

    // Network::GoogleService::Observer interface
    virtual void OnGSAuthenticationChange( bool authenticated );
    virtual void OnGSAvatarReceived( const Network::GoogleServices::AvatarData& avatar );
    Network::GoogleServices::AvatarData m_avatarGs;

private:
    struct ItemPublishData
    {
        Claw::NarrowString id;
        Claw::NarrowString name;
        int category;
    };

    enum FacebookAction
    {
        FA_NONE,
        FA_PUBLISH_TIEM,
        FA_SEND_INVITATION
    };

    void GiveAward( lua_State* L );
    void GenerateShareMessage( Claw::String& outMessage, const Claw::NarrowString& itemName, int itemCat );
    void GenerateInvitationMessage( Claw::String& outMessage, bool email );
    void PublishOnFacebook();
    void CheckVkbStatus();

    static int PreloadEntry( void* ptr ) { return ((MainMenuJob*)ptr)->Preload(); }
    int Preload();

    void DownloadProfileImages( const char* userId , const char* url );
    static int DoAvatars( void* data );
    Claw::Thread* m_thread;

    static int DoGsAvatars( void* data );
    Claw::Thread* m_threadGs;

    Claw::Thread* m_preload;
    bool m_preloadDone;
    LoadingPtr m_loading;

    Guif::ScreenPtr m_screen;
    AudioManagerPtr m_audioManager;

    Vectorf m_cLink[2];
    Vectorf m_cPos[3];

    const StartScreen m_startScreen;

    bool m_syncResult;

    ItemPublishData m_itemData;
    FacebookAction m_facebookAction;
    
    struct FacebookResonse
    {
        FacebookResonse() : action( FA_NONE ), success( false ) {}
        FacebookAction action;
        bool success;
    };
    FacebookResonse m_facebookResponse;

    bool m_moreGamesShown;
    bool m_userAvatarRequest;
    bool m_wasVkbOpened;
    bool m_gpAchievements;

    Claw::UInt32 m_dlList;
};

#endif

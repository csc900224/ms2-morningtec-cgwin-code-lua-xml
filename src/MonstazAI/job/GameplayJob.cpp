#include "claw/application/AbstractApp.hpp"
#include "claw/application/DebugOverlay.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/base/Registry.hpp"
#include "claw/compat/Platform.h"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/job/GameplayJob.hpp"
#include "MonstazAI/job/MainMenuJob.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/Achievements.hpp"
#include "MonstazAI/network/testflight/TestFlightService.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/ConnectionMonitor.hpp"

//#define ENABLE_AUTOAIMING

GameplayJob::GameplayJob( const Claw::NarrowString& fn )
    : m_heatTime( 0 )
    , m_heatStage( false )
    , m_heatClear( false )
    , m_movieWait( false )
    , m_preload( NULL )
    , m_preloadDone( false )
    , m_fn( fn )
#ifdef ENABLE_AUTOAIMING
    , m_loading( new Loading( true ) )
#else
    , m_loading( new Loading( false ) )
#endif
    , m_dlList( -1 )
    , m_perfMonitor( new PerfMonitor() )
{
    m_ambient = AudioManager::GetInstance()->Play( SFX_AMBIENT );
}

GameplayJob::~GameplayJob()
{
    if( ConnectionMonitor::IsCreated() )
    {
        ConnectionMonitor::GetInstance()->SetEnabled( false );
    }

    if( Shop::GetInstance() )
    {
        Shop::GetInstance()->OnGameSessionsStop();
    }
    m_gameManager.Release();

    unsigned int fps = m_perfMonitor->GetMedian();
    CLAW_MSG( "Median FPS: " << fps );
    if( !Claw::Registry::Get()->CheckBool( "/monstaz/benchmarkdone" ) )
    {
        Claw::Registry::Get()->Set( "/monstaz/benchmarkdone", true );
        if( fps < 30 )
        {
            CLAW_MSG( "Reducing quality" );
            Claw::Registry::Get()->Set( "/monstaz/settings/lowquality", true );
        }
    }
}

void GameplayJob::Initialize()
{
    Missions::MissionManager::GetInstance()->SetPaused( true );

    Claw::Registry::Get()->Set( "/internal/levelfile", m_fn.c_str() );

    if( !m_movieWait )
    {
        m_preload = new Claw::Thread( PreloadEntry, this );
    }
}

void GameplayJob::Render( Claw::Surface* target )
{
    if( m_movieWait )
    {
        target->Clear( 0 );
        return;
    }

    if( m_preload )
    {
        m_loading->Render( target, m_dlList );
        Achievements::GetInstance()->Render( target );
        return;
    }

    if( m_drawBuffer[0] )
    {
        if( m_heatClear )
        {
            for( int i=0; i<2; i++ )
            {
                m_heatBuffer[i]->Clear( Claw::MakeRGB( 128, 128, 0 ) );
            }
            m_heatClear = false;
        }

        m_drawBuffer[0]->Clear( 0 );
        m_gameManager->Render( m_drawBuffer[0] );

        for( std::vector<PostProcessPtr>::const_iterator it = m_pp.begin(); it != m_pp.end(); ++it )
        {
            PostProcess* pp = *it;
            if( pp->m_active )
            {
                m_drawBuffer[1]->Clear( 0 );
                pp->Render( m_drawBuffer[0], m_drawBuffer[1] );
                std::swap( m_drawBuffer[0], m_drawBuffer[1] );
            }
        }

        while( m_heatTime > (1/60.f) )
        {
            m_heatTime -= (1/60.f);
            m_heatStage = !m_heatStage;
            if( m_heatStage )
            {
                m_gameManager->RenderHeat( m_heatBuffer[0] );
            }
            else
            {
                m_heatBlur->Render( m_heatBuffer[0], m_heatBuffer[1], 0, 0, m_heatBuffer[0]->GetClipRect() );
                std::swap( m_heatBuffer[0], m_heatBuffer[1] );
            }
        }

        m_heat->SetHeatTexture( m_heatBuffer[1] );
        m_heat->Render( m_drawBuffer[0], target, 0, 0, m_drawBuffer[0]->GetClipRect() );
    }
    else
    {
        m_gameManager->Render( target );
    }

    m_gameManager->RenderHud( target );

    Achievements::GetInstance()->Render( target );

#ifdef _DEBUG
    char buf[32];
    sprintf( buf, "%c %iKB", 185, m_gameManager->GetLua()->GetMemUsage() );
    Claw::DebugOverlay::Get()->Draw( target, 88, 16, buf );
    sprintf( buf, "%c %i", 171, m_gameManager->GetEntityManager()->Count() );
    Claw::DebugOverlay::Get()->Draw( target, 440, 0, buf );
#endif
}

void GameplayJob::Update( float dt )
{
    if( m_movieWait )
    {
        if( Claw::AbstractApp::GetInstance()->MovieFinished() )
        {
            m_movieWait = false;
            AudioManager::GetInstance()->Pause( false );
            m_preload = new Claw::Thread( PreloadEntry, this );
        }
        return;
    }

    Achievements::GetInstance()->Update( dt );

    if( m_preload )
    {
        m_loading->Update( dt );

        if( m_preloadDone && m_loading->CanLeave() )
        {
            delete m_preload;
            m_preload = NULL;
            m_loading.Release();
            AudioManager::GetInstance()->Stop( m_ambient );
            Missions::MissionManager::GetInstance()->SetPaused( false );
            ConnectionMonitor::GetInstance()->SetEnabled( true );
        }
        else
        {
            return;
        }
    }

    m_perfMonitor->ReportFPS( (unsigned int)( 1.f/dt ) );

    m_gameManager->Update( dt );
    m_heatTime += dt;

    m_gameManager->GetLua()->Call( "CheckReload", 0, 1 );
    float reload = m_gameManager->GetLua()->CheckNumber( -1 );
    m_gameManager->GetLua()->Pop( 1 );
    m_gameManager->GetLua()->Call( "CheckAmmo", 0, 1 );
    int a1 = m_gameManager->GetLua()->CheckNumber( -1 );
    m_gameManager->GetLua()->Pop( 1 );
    m_gameManager->GetLua()->Call( "CheckWeapon", 0, 2 );
    const char* w = m_gameManager->GetLua()->CheckCString( -2 );
    int elements = m_gameManager->GetLua()->CheckNumber( -1 );
    m_gameManager->GetLua()->Pop( 2 );

    m_gameManager->GetLua()->Call( "CheckWeaponSlowDown", 0, 1 );
    float slowDown = m_gameManager->GetLua()->CheckNumber( -1 );
    m_gameManager->GetLua()->Pop( 1 );

    m_gameManager->GetLua()->Call( "CheckNumBoughtWeapons", 0, 1 );
    float nw = m_gameManager->GetLua()->CheckNumber( -1 );
    m_gameManager->GetLua()->Pop( 1 );

    m_gameManager->GetEntityManager()->SetPlayerWeaponSlowDown( slowDown );
    m_gameManager->GetHud()->SetWeapon( w, elements );
    TouchControls* tc = m_gameManager->GetTouchControls();
    if( tc )
    {
        tc->SetNumWeapons( nw );
    }
    m_gameManager->GetHud()->SetReload( reload );
    m_gameManager->GetHud()->SetBullets( a1 );
}

void GameplayJob::PauseGame()
{
    if( !m_gameManager->GetMenu() )
    {
        Claw::Registry::Get()->Set( "/monstaz/settings/pausedone", true );
        m_gameManager->ShowPauseMenu( true );
    }
}

void GameplayJob::KeyPress( Claw::KeyCode code )
{
    if( m_preload )
    {
        m_loading->KeyPress( code );
        return;
    }
    m_gameManager->KeyPressed( code );

#ifdef XPERIA_PLAY_BUILD
    if( ((MonstazApp*)MonstazApp::GetInstance())->IsXperiaPlayKeyboarbAvailable() )
    {
        switch( code )
        {
        case Claw::KEY_PS_START:
            PauseGame();
            break;
        case Claw::KEY_PS_LT:
            if( !m_gameManager->GetMenu() )
                m_gameManager->PlaceMine();
            break;
        case Claw::KEY_PS_RT:
            if( !m_gameManager->GetMenu() )
                m_gameManager->FireGrenade();
            break;
        case Claw::KEY_SELECT: // Cross
            if( !m_gameManager->GetMenu() )
            {
                m_gameManager->GetLua()->Call( "DoReload", 0, 0 );
            }
            break;
        case Claw::KEY_ESCAPE: // Circle
            if( !m_gameManager->GetMenu() && m_gameManager->GetMenu(true)->IsPerkIconVisible() )
                m_gameManager->ShowPerkMenu( true );
            break;
        case Claw::KEY_PS_TRIANGLE:
            if( !m_gameManager->GetMenu() && m_gameManager->NukeAvailable() )
                m_gameManager->Nuke();
            break;
        case Claw::KEY_PS_SQUARE:
            if( !m_gameManager->GetMenu() )
                m_gameManager->EnableShield();
            break;
        case Claw::KEY_DOWN:
            if( !m_gameManager->GetMenu() )
                m_gameManager->UseHealthKit();
            break;
        case Claw::KEY_LEFT:
            m_gameManager->GetLua()->Call( "PrevBoughtWeapon", 0, 0 );
            TutorialManager::GetInstance()->OnWeaponChange();
            break;
        case Claw::KEY_RIGHT:
            m_gameManager->GetLua()->Call( "NextBoughtWeapon", 0, 0 );
            TutorialManager::GetInstance()->OnWeaponChange();
            break;
        }
        return;
    }
#endif

#ifdef ANDROID_BUILD
    switch( code )
    {
    case Claw::KEY_ESCAPE:
        PauseGame();
        break;
    }
#endif

#ifdef CLAW_SDL
    switch( code )
    {
    case Claw::KEY_LEFT:
        code = Claw::KEY_A;
        break;
    case Claw::KEY_RIGHT:
        code = Claw::KEY_D;
        break;
    case Claw::KEY_UP:
        code = Claw::KEY_W;
        break;
    case Claw::KEY_DOWN:
        code = Claw::KEY_S;
        break;
    default:
        break;
    };

    switch( code )
    {
    case Claw::KEY_W:
    case Claw::KEY_S:
    case Claw::KEY_A:
    case Claw::KEY_D:
        m_gameManager->KeysUpdate( code, true );
        break;
    case Claw::KEY_SPACE:
        m_gameManager->GetLua()->PushBool( true );
        m_gameManager->GetLua()->Call( "Shot", 1, 0 );
        break;
    case Claw::KEY_F:
        //m_gameManager->GetTimeController()->Switch( 0.5f );
        m_gameManager->GetLua()->Call( "NextWeapon", 0, 0 );
        TutorialManager::GetInstance()->OnWeaponChange();
        break;
    case Claw::KEY_G:
        m_gameManager->GetLua()->Call( "NextBoughtWeapon", 0, 0 );
        TutorialManager::GetInstance()->OnWeaponChange();
        break;
    case Claw::KEY_ESCAPE:
    case Claw::KEY_P:
        PauseGame();
        break;
    case Claw::KEY_R:
        m_gameManager->GetLua()->Call( "DoReload", 0, 0 );
        break;
    case Claw::KEY_1:
    case Claw::KEY_2:
    case Claw::KEY_3:
        m_gameManager->EnableShield();
        break;
    case Claw::KEY_4:
        m_gameManager->GetStats()->EnablePerk( Perks::Runner );
        break;
    case Claw::KEY_B:
        m_gameManager->Nuke();
        break;
    case Claw::KEY_Q:
        m_gameManager->Airstrike();
        break;
    default:
        break;
    }
#endif
}

void GameplayJob::KeyRelease( Claw::KeyCode code )
{
    if( m_preload ) return;

#ifdef CLAW_SDL
    switch( code )
    {
    case Claw::KEY_LEFT:
        code = Claw::KEY_A;
        break;
    case Claw::KEY_RIGHT:
        code = Claw::KEY_D;
        break;
    case Claw::KEY_UP:
        code = Claw::KEY_W;
        break;
    case Claw::KEY_DOWN:
        code = Claw::KEY_S;
        break;
    default:
        break;
    };

    switch( code )
    {
    case Claw::KEY_W:
    case Claw::KEY_S:
    case Claw::KEY_A:
    case Claw::KEY_D:
        m_gameManager->KeysUpdate( code, false );
        break;
    case Claw::KEY_SPACE:
        break;
    default:
        break;
    }
#endif
}

void GameplayJob::TouchUp( int x, int y, int button )
{
    if( m_preload )
    {
        if( m_loading )
        {
            if( Claw::TouchDevice::GetId( button ) == Claw::TDT_MAIN_TOUCH )
            {
                if( m_preloadDone )
                {
                    m_loading->WaitForTap( false );
                }
                else
                {
                    m_loading->TouchUp( x, y );
                }
            }
        }
        return;
    }

    if( m_gameManager )
    {
        m_gameManager->TouchUp( x, y, button );
    }
}

void GameplayJob::TouchDown( int x, int y, int button )
{
    if( m_preload )
    {
        if( Claw::TouchDevice::GetId( button ) == Claw::TDT_MAIN_TOUCH )
        {
            m_loading->TouchDown( x, y );
        }
        return;
    }

    if( m_gameManager )
    {
        m_gameManager->TouchDown( x, y, button );
    }
}

void GameplayJob::TouchMove( int x, int y, int button )
{
    if( m_preload ) return;

    if( m_gameManager )
    {
        m_gameManager->TouchMove( x, y, button );
    }
}

void GameplayJob::Resize( int x, int y )
{
    const Vectori resolution = ((MonstazApp*)MonstazApp::GetInstance())->GetResolution();
    int w = resolution.x;
    int h = resolution.y;

    if( m_gameManager )
    {
        m_gameManager->SetResolution( w, h );
    }
    if( m_drawBuffer[0] )
    {
        SetupDrawbuffer( w, h );
    }
}

void GameplayJob::Focus( bool focus )
{
    if( m_preload ) return;

    if( m_gameManager )
    {
        m_gameManager->Focus( focus );
    }
}

void GameplayJob::OnTouchDeviceChange()
{
    if( m_gameManager )
    {
        m_gameManager->DisplayControlsEnable( !((MonstazApp*)MonstazApp::GetInstance())->IsXperiaPlayKeyboarbAvailable() );
    }
}

void GameplayJob::LoadLevel( const char* fn )
{
#ifdef ADHOC_BUILD
    TestFlightService::PassCheckpoint( fn );
#endif

    Vectori res = ((MonstazApp*)MonstazApp::GetInstance())->GetResolution();

    m_gameManager.Release();
    GameManager::SetGameScale( CalculateGameScale() );
    m_gameManager.Reset( new GameManager( "GameLogic.lua", new Stats() ) );

    m_gameManager->SetResolution( res.x, res.y );
    m_gameManager->DisplayControlsEnable( !((MonstazApp*)MonstazApp::GetInstance())->IsXperiaPlayKeyboarbAvailable() );
    m_gameManager->Load( fn );
    m_gameManager->FinishSetup();
}

int GameplayJob::Preload()
{
#ifdef ENABLE_AUTOAIMING
    m_loading->ThreadLoadPopup();
    m_loading->BeforeLoadWait();
#endif

    switch( TutorialManager::GetInstance()->GetCurrentChapter() )
    {
    case TutorialChapter::Gameplay:
        PakManager::GetInstance()->Require( PakManager::SetPlayerMech | PakManager::SetEnemy1 | PakManager::SetEnemy5 | PakManager::SetEnemy8 );
        m_dlList = PakManager::SetEnemy2 | PakManager::SetEnemy7;
        break;
    case TutorialChapter::Items:
        m_dlList = 0;
        break;
    default:
        break;
    }
    while( !PakManager::GetInstance()->CheckDone( m_dlList ) ) { Claw::Time::Sleep( 1/60.f ); }
    PakManager::GetInstance()->Reset( m_dlList );
    while( !AtlasManager::GetInstance()->AreLoaded() ) { Claw::Time::Sleep( 1/60.f ); }
    m_dlList = 0;

    LoadLevel( m_fn.c_str() );

    Shop::GetInstance()->OnGameSessionsStart();
    TutorialManager::GetInstance()->OnLevelLoaded();

    m_preloadDone = true;
    m_loading->WaitForTap( true );

    bool postprocess = false;
    Claw::Registry::Get()->Get( "/monstaz/settings/postprocess", postprocess );
    if( postprocess )
    {
        const Vectori resolution = ((MonstazApp*)MonstazApp::GetInstance())->GetResolution();
        int w = resolution.x;
        int h = resolution.y;
        SetupDrawbuffer( w, h );

        m_heat.Reset( new Heat() );
        m_heatBlur.Reset( new HeatBlur() );
    }

    return 0;
}

float GameplayJob::CalculateGameScale() const
{
    return ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale();
}

void GameplayJob::SetupDrawbuffer( int x, int y )
{
    x += 64;
    y += 64;

    for( int i=0; i<2; i++ )
    {
        m_drawBuffer[i].Reset( new Claw::Surface( x, y, Claw::PF_RGB_565 ) );
        m_drawBuffer[i]->SetFlag( Claw::Surface::SF_LINEAR_FILTERING, true );

        m_heatBuffer[i].Reset( new Claw::Surface( Claw::AlignPOT( x / 4 ), Claw::AlignPOT( y / 4 ), Claw::PF_RGBA_8888 ) );
        m_heatBuffer[i]->SetFlag( Claw::Surface::SF_LINEAR_FILTERING, true );
        m_heatClear = true;
    }

    m_gameManager->DrawBufferSize( x, y );
}

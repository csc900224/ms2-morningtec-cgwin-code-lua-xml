#include "claw/base/AssetDict.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/job/UnsupportedJob.hpp"

static const char* TEXT[] = {
    "Your device is not supported.",
    "Tap the screen to exit..."
};

static const float TEXT_SPACE = 0.85f;
static const float BLINK_RATE1 = 0.7f;
static const float BLINK_RATE2 = 0.3f;

UnsupportedJob::UnsupportedJob()
    : m_timer( 0 )
    , m_tapVisible( false )
{}

UnsupportedJob::~UnsupportedJob()
{
}

void UnsupportedJob::Initialize()
{
    Claw::AssetDict::AddAtlas( "atlas-menu.xml@linear" );

    m_font = Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_big.xml@linear" );
    m_fontset.Reset( new Claw::Text::FontSet() );
    m_fontset->AddFont( "default", m_font );

    m_format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );
    m_format.SetVerticalAlign( Claw::Text::Format::VA_CENTER );
    m_format.SetFontSet( m_fontset );
    m_format.SetFontId( "default" );

    for( int i = 0; i < NUM_PARAGRAPHS; ++i )
    {
        Claw::String text(TEXT[i]);
        m_text[i].Reset( new Claw::ScreenText( m_format,
                                               text,
                                               Claw::Extent( MonstazApp::GetInstance()->GetDisplay()->GetWidth() * TEXT_SPACE, 0 ) ) );
    }
}

void UnsupportedJob::Render( Claw::Surface* target )
{
    target->Clear( 0 );

    int parHeight = (MonstazApp::GetInstance()->GetDisplay()->GetHeight() - m_font->GetHeight()) * TEXT_SPACE / (NUM_PARAGRAPHS - 1) ;

    for( int i = 0; i < NUM_PARAGRAPHS; ++i )
    {
        int y = 0;
        if( NUM_PARAGRAPHS < 3 ) {
            y= (MonstazApp::GetInstance()->GetDisplay()->GetHeight() - m_font->GetHeight()*2) * 0.5f;
        } else {
            y= MonstazApp::GetInstance()->GetDisplay()->GetHeight() * (1 - TEXT_SPACE) + i * parHeight;
        }
        if( i == NUM_PARAGRAPHS - 1 )
        {
            if( !m_tapVisible ) return;
            y =  MonstazApp::GetInstance()->GetDisplay()->GetHeight() - m_text[i]->GetHeight();
        }

        m_text[i]->Draw( target, MonstazApp::GetInstance()->GetDisplay()->GetWidth() * (1 - TEXT_SPACE) / 2, y );
    }
}

void UnsupportedJob::Update( float dt )
{
    m_timer += dt;
    if( m_tapVisible && m_timer > BLINK_RATE1 || !m_tapVisible && m_timer > BLINK_RATE2 )
    {
        m_timer = 0;
        m_tapVisible = !m_tapVisible;
    }
}

void UnsupportedJob::TouchDown( int x, int y, int button )
{
    MonstazApp::GetInstance()->Exit();
}

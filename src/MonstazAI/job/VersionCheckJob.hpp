#ifndef __MONSTAZ_VERSIONCHECKJOB_HPP__
#define __MONSTAZ_VERSIONCHECKJOB_HPP__

#include "MonstazAI/job/Job.hpp"

#include "guif/Screen.hpp"

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

class VersionCheckJob : public Job
{
public:
    LUA_DEFINITION( VersionCheckJob );
    VersionCheckJob( lua_State* L ) { CLAW_ASSERT( false ); }

    VersionCheckJob();
    ~VersionCheckJob();

    void Initialize();

    void Render( Claw::Surface* target );
    void Update( float dt );
    void TouchUp( int x, int y, int button );
    void TouchDown( int x, int y, int button );
    void TouchMove( int x, int y, int button );

    int l_IsUpdateMandatory( lua_State* L );
    int l_Update( lua_State* L );
    int l_Exit( lua_State* L );
    int l_Continue( lua_State* L );

private:
    void LoadVersionData();
    bool IsNewVersionAvailable();
    bool IsUpdateMandatory();
    void ExtractVersion( const Claw::NarrowString& versionString, int& major, int& minor, int& build ) const;
    void LoadLayout();

    Guif::ScreenPtr m_screen;
    bool m_active;
};

#endif

#ifndef __MONSTAZ_INTROJOB_HPP__
#define __MONSTAZ_INTROJOB_HPP__

#include "MonstazAI/job/Job.hpp"

class IntroJob : public Job
{
public:
    IntroJob();

    void Initialize();
    void Render( Claw::Surface* target );
    void Update( float dt );

private:
    void StartIntroLevel();

};

#endif

#include <new>

#include "claw/base/AssetDict.hpp"
#include "claw/system/Alloc.hpp"
#include "claw/vfs/Vfs.hpp"

#include "MonstazAI/IsoSet.hpp"
#include "MonstazAI/GameManager.hpp"

IsoSetElement::IsoSetElement()
{
}

IsoSetElement::IsoSetElement( Claw::Surface* asset, const Claw::Rect& rect, int height, const Vectorf& pos, float scale, IsoSet* parent )
    : Renderable( Vectorf( pos.x, pos.y + height + rect.m_y / 2 ) )
    , m_asset( asset )
    , m_rect( rect )
    , m_height( height + rect.m_y / 2 )
    , m_parent( parent )
{
    m_rect.m_x = Claw::Round( m_rect.m_x / 2 * scale );
    m_rect.m_y = Claw::Round( m_rect.m_y / 2 * scale );
    m_rect.m_w = Claw::Round( m_rect.m_w / 2 * scale );
    m_rect.m_h = Claw::Round( m_rect.m_h / 2 * scale );

    m_worldBV = m_asset->GetClipRect();
    m_worldBV.m_x += Claw::Round( scale * pos.x );
    m_worldBV.m_y += Claw::Round( scale * pos.y );
}

void IsoSetElement::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    if( m_parent->m_obj && m_parent->m_obj->m_holo > 0 )
    {
        Hologram* h = GameManager::GetInstance()->GetHologram();
        h->SetIntensity( m_parent->m_obj->m_holo );
        h->Render( m_asset, target, m_pos.x * scale - offset.x + m_rect.m_x, ( m_pos.y - m_height ) * scale - offset.y + m_rect.m_y, m_rect );
        if( m_parent->m_obj && m_parent->m_obj->m_anim )
        {
            h->Render( m_parent->m_obj->m_anim, target, m_pos.x * scale - offset.x + m_rect.m_x, ( m_pos.y - m_height ) * scale - offset.y + m_rect.m_y, m_rect );
        }
    }
    else
    {
        target->Blit( m_pos.x * scale - offset.x + m_rect.m_x, ( m_pos.y - m_height ) * scale - offset.y + m_rect.m_y, m_asset, m_rect );
        if( m_parent->m_obj && m_parent->m_obj->m_anim )
        {
            target->Blit( m_pos.x * scale - offset.x + m_rect.m_x, ( m_pos.y - m_height ) * scale - offset.y + m_rect.m_y, m_parent->m_obj->m_anim, m_rect );
        }
    }
}

const Claw::Rect& IsoSetElement::GetRenderLocalBV() const
{
    return m_asset->GetClipRect();
}

const Claw::Rect& IsoSetElement::GetRenderWorldBV() const
{
    return m_worldBV;
}

IsoSet::IsoSet( const Vectorf& pos, float scale, int offset )
    : m_pos( pos * scale )
    , m_rects( NULL )
    , m_rectNum( 0 )
{
}

IsoSet::IsoSet( const Claw::NarrowString& fn, const Vectorf& pos, float scale, int offset )
    : m_pos( pos * scale )
{
    Claw::FilePtr f( Claw::OpenFile( fn ) );

    Claw::NarrowString tmp;
    f->ReadLine( tmp );
    Claw::NarrowString tmp2;
    tmp2 = Claw::NarrowString( "gfx/assets/" ) + tmp + "@linear";
    m_asset.Reset( Claw::AssetDict::Get<Claw::Surface>( tmp2 ) );
    CLAW_ASSERT( m_asset );
    CLAW_CHECK( m_asset->IsInAtlas(), "Surface " << tmp << " not in atlas." );

    tmp.clear();
    f->ReadLine( tmp );
    if( !tmp.empty() )
    {
        tmp2 = Claw::NarrowString( "gfx/assets/" ) + tmp + "@linear";
        m_shadow.Reset( Claw::AssetDict::Get<Claw::Surface>( tmp2 ) );
        CLAW_ASSERT( m_shadow );
        CLAW_CHECK( m_asset->IsInAtlas(), "Surface " << tmp << " not in atlas." );
    }

    int x, y;
    f->Read( &x, 4 );
    f->Read( &y, 4 );
    m_shadowPos = Vectorf( x / (2 / scale), y / (2 / scale) );

    m_shadowRect = m_shadow->GetClipRect();
    m_shadowRect.m_x += m_pos.x + m_shadowPos.x;
    m_shadowRect.m_y += m_pos.y + m_shadowPos.y;

    f->Read( &m_rectNum, 4 );
    m_rects = (IsoSetElement*)_malloc( m_rectNum * sizeof(IsoSetElement) );
    for( int i=0; i<m_rectNum; i++ )
    {
        int data[5];
        f->Read( data, 5*4 );
        new (m_rects+i) IsoSetElement( m_asset, Claw::Rect( data[0], data[1], data[2], data[3] ), data[4] / 2 + offset, pos, scale, this );
    }
}

IsoSet::~IsoSet()
{
    for( int i=0; i<m_rectNum; i++ )
    {
        m_rects[i].~IsoSetElement();
    }
    _free( m_rects );
}

void IsoSet::Render( Claw::Surface* target, const Vectorf& offset, float scale )
{
    RenderableManager* rm = GameManager::GetInstance()->GetRenderableManager();
    Claw::Rect clipRect = target->GetClipRect();

    clipRect.m_x += offset.x;
    clipRect.m_y += offset.y;

    if( m_shadow && clipRect.IsIntersect( m_shadowRect ) )
    {
        RenderShadow( target, Vectorf( m_pos.x - offset.x, m_pos.y - offset.y ) );
    }

    for( int i=0; i<m_rectNum; i++ )
    {
        if( clipRect.IsIntersect( m_rects[i].GetRenderWorldBV() ) )
        {
            rm->Add( m_rects+i );
        }
    }
}

void IsoSet::RenderShadow( Claw::Surface* target, const Vectorf& pos )
{
    target->Blit( pos.x + m_shadowPos.x, pos.y + m_shadowPos.y, m_shadow );
}

IsoSetAnim::IsoSetAnim( const Claw::NarrowString& fn, const Vectorf& pos, float scale, int offset )
    : IsoSet( pos, scale, offset )
    , m_count( 0 )
    , m_currentFrame( 0 )
    , m_timeRemaining( 0 )
{
    Claw::XmlPtr xmlTree( Claw::Xml::LoadFromFile( "gfx/assets/" + fn ) );
    Claw::XmlIt  itRoot( *xmlTree );

    for( Claw::XmlIt it = itRoot.Child( "frame" ); it; ++it )
    {
        m_count++;
    }

    m_frame = new IsoSetPtr[m_count];
    m_time = new float[m_count];

    int idx = 0;
    for( Claw::XmlIt it = itRoot.Child( "frame" ); it; ++it )
    {
        Claw::NarrowString image;
        it.GetAttribute( "image", image );
        int time;
        it.GetAttribute( "time", time );

        Claw::NarrowString isosetfn( Claw::NarrowString( "gfx/assets/" ) + image.substr( 0, image.size() - 3 ) + "isoset" );

        m_frame[idx].Reset( new IsoSet( isosetfn, pos, scale, offset ) );
        m_time[idx] = time / 1000.f;

        idx++;
    }

    m_timeRemaining = m_time[0];
}

IsoSetAnim::~IsoSetAnim()
{
    delete[] m_frame;
    delete[] m_time;
}

void IsoSetAnim::Render( Claw::Surface* target, const Vectorf& offset, float scale )
{
    m_frame[m_currentFrame]->Render( target, offset, scale );
}

void IsoSetAnim::RenderShadow( Claw::Surface* target, const Vectorf& pos )
{
    m_frame[m_currentFrame]->RenderShadow( target, pos );
}

void IsoSetAnim::Update( float dt )
{
    m_timeRemaining -= dt;
    if( m_timeRemaining < 0 )
    {
        m_currentFrame = ( m_currentFrame + 1 ) % m_count;
        m_timeRemaining = m_time[m_currentFrame];
    }
}

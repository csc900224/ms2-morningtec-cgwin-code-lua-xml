#include "claw/base/AssetDict.hpp"
#include "claw/math/Math.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/graphics/Batcher.hpp"
#include "claw/base/Registry.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/HUD.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/IsoSet.hpp"


static const char* vertex =
    "void main(void)\n"
    "{\n"
    "TEXTURE;\n"
    "GLPOSITION;\n"
    "}";

static const char* fragment =
    "uniform lowp float intensity;\n"
    "uniform lowp sampler2D mask;\n"
    "void main(void)\n"
    "{\n"
    "mediump float m = texture2D( mask, vTex ).r;\n"
    "m = ( intensity - m ) * 10.0;\n"
    "m = smoothstep( 0.0, 1.0, m );\n"
    "lowp vec4 c = GetTexture( vTex );\n"
    "gl_FragColor = vec4( c.rgb, c.a * m );\n"
    "}";


static const char* hpvert =
    "void main(void)\n"
    "{\n"
    "TEXTURE;\n"
    "GLPOSITION;\n"
    "}";

static const char* hpfrag =
    "uniform mediump float hp;\n"
    "void main(void)\n"
    "{\n"
    "lowp vec4 c = GetTexture( vTex );\n"
    "lowp float m = smoothstep( 0.0, 1.0, ( hp - 0.25 ) * 5.0 );\n"
    "gl_FragColor = mix( vec4( c.g, 0.0, 0.0, c.a ), c, m );\n"
    "}";


#define RIGHT_BG_X 146
#define RIGHT_BG_Y 5
#define GUN_X 42
#define GUN_Y 5
#define LEVELUP_X 15
#define LEVELUP_Y 60

Hud* Hud::s_instance = NULL;

LUA_DECLARATION( Hud )
{
    METHOD( Hud, ShowXp ),
    METHOD( Hud, ShowScore ),
    METHOD( Hud, ShowNuke ),
    METHOD( Hud, ShowAirstrike ),
    {0,0}
};

Hud::Hud( lua_State* L )
{
    CLAW_ASSERT( false );
}

void Hud::Init( Claw::Lua* lua )
{
    // Must be already initialized
    CLAW_ASSERT( s_instance );

    Claw::Lunar<Hud>::Register( *lua );
    Claw::Lunar<Hud>::push( *lua, this );
    lua->RegisterGlobal( "Hud" );
}

int Hud::l_ShowXp( lua_State* L )
{
    Claw::Lua lua( L );
    CLAW_ASSERT( lua.IsBool( 1 ) );
    {
        ShowXp( lua.CheckBool( 1 ) );
    }
    return 0;
}

int Hud::l_ShowScore( lua_State* L )
{
    Claw::Lua lua( L );
    CLAW_ASSERT( lua.IsBool( 1 ) );
    {
        ShowScore( lua.CheckBool( 1 ) );
    }
    return 0;
}

int Hud::l_ShowNuke( lua_State* L )
{
    Claw::Lua lua( L );
    CLAW_ASSERT( lua.IsBool( 1 ) );
    {
        ShowNuke( lua.CheckBool( 1 ) );
    }
    return 0;
}

int Hud::l_ShowAirstrike( lua_State* L )
{
    Claw::Lua lua( L );
    CLAW_ASSERT( lua.IsBool( 1 ) );
    {
        ShowAirstrike( lua.CheckBool( 1 ) );
    }
    return 0;
}

Hud::Hud()
    : m_scale( GameManager::GetGameScale() )
    , m_rscale( 1.f / m_scale )
    , m_time( 0 )
    , m_life( 1 )
    , m_xp( 0 )
    , m_reloadState( 1 )
    , m_reloadFinish( false )
    , m_current( -2 )
    , m_refreshBullets( true )
    , m_isLevelup( false )
    , m_points( -1 )
    , m_refreshPoints( true )
    , m_multi( -1 )
    , m_refreshMulti( true )
    , m_cash( -1 )
    , m_grenades( 0 )
    , m_stimpaks( 0 )
    , m_mines( 0 )
    , m_shields( 0 )
    , m_refreshGrenades( true )
    , m_refreshStimpaks( true )
    , m_refreshMines( true )
    , m_refreshShield( true )
    , m_playerDamage( 0 )
    , m_playerDead( false )
    , m_showScore( Claw::Registry::Get()->CheckBool( "/internal/survival" ) )
    , m_showXp( true )
    , m_showNuke( true )
    , m_showAirstrike( true )
    , m_showInventory( true )
    , m_health( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_health.png@linear" ) )
    , m_healthBar( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_health_bar.png@linear" ) )
    , m_healthBarBg( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_health_bar_bg.png@linear" ) )
    , m_rageBar( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_rage_bar.png@linear" ) )
    , m_rageBarBg( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_rage_bar_bg.png@linear" ) )
    , m_gunInfo( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_gun_info.png@linear" ) )
    , m_gunIce( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_gun_ice.png@linear" ) )
    , m_gunFire( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_gun_fire.png@linear" ) )
    , m_gunElectric( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_gun_electricity.png@linear" ) )
    , m_reload( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/reload.png@linear" ) )
    , m_reloadFill( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/reload_fill.ani" ) )
    , m_reloadEnd( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/reload_end.ani" ) )
    , m_font( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_big.xml@linear" ) )
    , m_fontSmall( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_normal.xml@linear" ) )
    , m_scoreBar( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_score.png@linear" ) )
    , m_scoreBar2( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hud_score_2.png@linear" ) )
    , m_cashBg( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/cash_bg.png@linear" ) )
    , m_cashIcon( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/pickup_cash.png@linear" ) )
    , m_hit( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/hit.png@linear" ) )
    , m_tick( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/complete.png@linear" ) )
    , m_pause( Claw::AssetDict::Get<Claw::Surface>( "plate/pause.@linear" ) )
    , m_grenadesIcon( Claw::AssetDict::Get<Claw::Surface>( "menu2/item_grenades.png@linear" ) )
    , m_stimpakIcon( Claw::AssetDict::Get<Claw::Surface>( "menu2/item_stim_pack.png@linear" ) )
    , m_minesIcon( Claw::AssetDict::Get<Claw::Surface>( "menu2/item_mines.png@linear" ) )
    , m_shieldIcon( Claw::AssetDict::Get<Claw::Surface>( "menu2/item_shield.png@linear" ) )
    , m_pauseTime( -1 )
    , m_pauseSpeed( 0.75f )
    , m_cashState( CPS_DORMANT )
    , m_cashSlider( 0 )
    , m_cashPosition( 0 )
    , m_cpt( CPT_INVALID )
    , m_elements( 0 )
    , m_lowquality( Claw::Registry::Get()->CheckBool( "/monstaz/settings/lowquality" ) )
{
    if( !Claw::Registry::Get()->CheckBool( "/monstaz/settings/pausedone" ) &&
         Claw::Registry::Get()->CheckInt( "/internal/storylevel" ) != 1 &&
        !TutorialManager::GetInstance()->IsActive() )
    {
        ShowPauseNotification();
    }

    m_pickupArrow[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/item_arrow.png@linear" ) );
    m_pickupArrow[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/weapon_arrow.png@linear" ) );
    m_pickupArrow[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/arrow_red_small.png@linear" ) );
    m_pickupArrow[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/arrow_blue_small.png@linear" ) );
    m_pickupArrow[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/arrow_red_small.png@linear" ) );
    m_pickupArrow[5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/arrow_blue_small.png@linear" ) );
    m_pickupArrow[6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/arrow_blue_small.png@linear" ) );
    m_pickupArrow[7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/arrow_blue_small.png@linear" ) );
    m_pickupExclamation[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/exclamation_green_icon.png@linear" ) );
    m_pickupExclamation[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/weapon_arrow_exclamation.png@linear" ) );
    m_pickupExclamation[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/skull_icon.png@linear" ) );
    m_pickupExclamation[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/friend_icon.png@linear" ) );
    m_pickupExclamation[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/target_icon.png@linear" ) );
    m_pickupExclamation[5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/acces_card_icon.png@linear" ) );
    m_pickupExclamation[6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/dum_dum_icon.png@linear" ) );
    m_pickupExclamation[7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/exclamation_blue_icon.png@linear" ) );

    m_nuke[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/nuke_buton.png@linear" ) );
    m_nuke[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/nuke_buton_push.png@linear" ) );

    m_airstrike[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/airstrike_buton.png@linear" ) );
    m_airstrike[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/airstrike_buton_push.png@linear" ) );

    m_missionIco[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/ico_armory.png@linear" ) );
    m_missionIco[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/ico_cash.png@linear" ) );
    m_missionIco[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/ico_orb.png@linear" ) );

    if( Claw::Registry::Get()->CheckInt( "/monstaz/settings/blood" ) == 1 )
    {
        m_bokeh[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "plate/bokeh_texture2.@linear" ) );
    }
    else
    {
        m_bokeh[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "plate/bokeh_texture2_green.@linear" ) );
    }
    m_bokeh[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "plate/bokeh_mask.@linear" ) );

    m_shader.Load( vertex, fragment );
    m_hpShader.Load( hpvert, hpfrag );

    int tmp;
    Shop::GetInstance()->GetCash( m_cash, tmp );

    CLAW_ASSERT( !s_instance );
    s_instance = this;
}

Hud::~Hud()
{
    s_instance = NULL;
}

Hud* Hud::GetInstance()
{
    CLAW_ASSERT( s_instance );
    return s_instance;
}

void Hud::SetWeapon( const Claw::NarrowString& weapon, int elements )
{
    SetWeapon( weapon.c_str(), elements );
}

void Hud::SetWeapon( const char* weapon, int elements )
{
    m_elements = elements;
    m_weaponSel = weapon;
}

void Hud::SetReload( float reload )
{
    if( m_reloadState != 1 && reload == 1 )
    {
        m_reloadFinish = true;
        ((Claw::AnimatedSurface*)m_reloadEnd.GetPtr())->SetFrame( 0 );
    }

    m_reloadState = reload;
}

void Hud::SetLevelUp( bool levelup )
{
    if( levelup && !m_isLevelup )
    {
        GameManager::GetInstance()->GetMenu( true )->GetLua()->Call( "PerksMenuShowNotification", 0, 0 );
        GameManager::GetInstance()->GetAudioManager()->Play( SFX_LEVELUP );
        TutorialManager::GetInstance()->OnRage();
    }

    m_isLevelup = levelup;
}

void Hud::SetCash( int cash )
{
    if( m_cash != cash )
    {
        if( m_cpt == CPT_CASH && m_cashState != CPS_DORMANT )
        {
            m_cash = cash;
            RefreshPopup();

            if( m_cashState == CPS_ACTIVE )
            {
                m_cashSlider = 0;
            }
            else
            {
                m_cashState = CPS_SLIDEIN;
            }
        }
        else
        {
            m_cash = cash;
            for( std::deque<CashPopupType>::const_iterator it = m_cashQueue.begin(); it != m_cashQueue.end(); ++it )
            {
                if( *it == CPT_CASH )
                {
                    return;
                }
            }
            m_cashQueue.push_back( CPT_CASH );
        }
    }
}

void Hud::SetMissionDone( int num )
{
    m_cashQueue.push_back( CashPopupType( CPT_MISSION1 + num ) );
}

// Infinity sign
static const wchar_t INF[] = { 0x221E, 0x0 };

void Hud::Render( Claw::Surface* target )
{
    if( m_refreshBullets || m_refreshPoints || m_refreshMulti || m_refreshStimpaks || m_refreshGrenades || m_refreshMines || m_refreshShield )
    {
        Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
        fontSet->AddFont( "f", m_font );

        Claw::Text::Format format;
        format.SetFontSet( fontSet );
        format.SetFontId( "f" );
        format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );

        Claw::Text::FontSetPtr fontSetSmall( new Claw::Text::FontSet() );
        fontSetSmall->AddFont( "f", m_fontSmall );

        Claw::Text::Format formatSmall;
        formatSmall.SetFontSet( fontSetSmall );
        formatSmall.SetFontId( "f" );
        formatSmall.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_LEFT );


        if( m_refreshBullets )
        {
            m_refreshBullets = false;

            char tmp[32];
            if( m_current == -1 )
            {
                m_bullets.Reset( new Claw::ScreenText( format, Claw::String( INF ), Claw::Extent( 45 * m_scale, 0 ) ) );
            }
            else
            {
                sprintf( tmp, "%i", m_current );
                m_bullets.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( 45 * m_scale, 0 ) ) );
            }
        }

        format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_LEFT );

        if( m_refreshPoints )
        {
            m_refreshPoints = false;

            char tmp[32];
            sprintf( tmp, "%i", m_points );

            m_pointsTxt.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( m_scoreBar->GetWidth() - ( 20 * m_scale ), 0 ) ) );
        }

        if( m_refreshMulti )
        {
            m_refreshMulti = false;

            char tmp[32];
            sprintf( tmp, "x%i", m_multi );

            m_multiTxt.Reset( new Claw::ScreenText( formatSmall, Claw::String( tmp ), Claw::Extent( 85 * m_scale, 0 ) ) );
        }

        if( m_refreshGrenades )
        {
            format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_RIGHT );
            m_refreshGrenades = false;

            char tmp[32];
            sprintf( tmp, "%ix", m_grenades );
            m_grenadesTxt.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( 85 * m_scale, 0 ) ) );
        }

        if( m_refreshStimpaks )
        {
            format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_LEFT );
            m_refreshStimpaks = false;

            char tmp[32];
            sprintf( tmp, "x%i", m_stimpaks );
            m_stimpaksTxt.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( 85 * m_scale, 0 ) ) );
        }

        if( m_refreshMines )
        {
            format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_LEFT );
            m_refreshMines = false;

            char tmp[32];
            sprintf( tmp, "x%i", m_mines );
            m_minesTxt.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( 85 * m_scale, 0 ) ) );
        }

        if( m_refreshShield )
        {
            format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_RIGHT );
            m_refreshShield = false;

            char tmp[32];
            sprintf( tmp, "%ix", m_shields );
            m_shieldTxt.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( 85 * m_scale, 0 ) ) );
        }
    }
    if( m_showXp )
    {
        RenderXp( target );
    }
    RenderWeapon( target );
    if( m_showScore )
    {
        RenderScore( target );
    }
    RenderCash( target );
    if( m_showNuke )
    {
        RenderNuke( target );
    }
    if( m_showAirstrike )
    {
        RenderAirstrike( target );
    }
    RenderTexts( target );
    RenderPickupHints( target );

    if( m_pauseTime > 0 )
    {
        m_pause->SetAlpha( 255 * ( 1 - cos( m_pauseTime * m_pauseSpeed * M_PI * 2 ) ) / 2 );
        Claw::TriangleEngine::Blit( target, m_pause, target->GetWidth() / 2, target->GetHeight() / 2, 0, Vectorf( 1.453125, 0.765625 ), Vectorf( m_pause->GetSize() / 2 ) );
    }

    if( m_showInventory )
    {
        RenderInventory( target );
    }
}

static float powhack( float val )
{
    for( int i=0; i<5; i++ )
    {
        val *= val;
    }
    return val;
}

void Hud::RenderXp( Claw::Surface* target )
{
    if( m_playerDead )
    {
        RenderHit( target, 0.75f );
    }
    else if( m_playerDamage > 0 )
    {
        float alphaorig = std::min( m_playerDamage, 0.8f );
        float hp = GameManager::GetInstance()->GetPlayer()->GetHitPoints();
        float maxhp = GameManager::GetInstance()->GetPlayer()->GetMaxHitPoints();
        alphaorig *= 1.f - hp / maxhp;
        float alpha = alphaorig * ( ( powhack( sin( m_time * 3 ) ) + powhack( sin( m_time * 3 + 0.7f ) ) ) * 0.25f + 0.75f );

        RenderHit( target, alpha );
    }


    float lifeWidthf = m_healthBar->GetWidth() * m_life;
    int lifeWidth = int( lifeWidthf );
    int xpWidth = m_rageBar->GetWidth() * m_xp;

    target->Blit( (4+32) * m_scale, (4+9) * m_scale, m_healthBarBg );
    target->Blit( (4+40) * m_scale, (4+29) * m_scale, m_rageBarBg );
    if( lifeWidth > 0 )
    {
        m_hpShader.Uniform( "hp", m_life );
        Claw::g_batcher->SetShader( &m_hpShader );
        target->Blit( (4+32) * m_scale + lifeWidthf - lifeWidth, (4+9) * m_scale, m_healthBar, Claw::Rect( m_healthBar->GetWidth() - lifeWidth, 0, lifeWidth, m_healthBar->GetHeight() ) );
        Claw::g_batcher->SetShader( NULL );
    }
    target->Blit( (4+40) * m_scale, (4+29) * m_scale, m_rageBar, Claw::Rect( xpWidth, 0, m_rageBar->GetWidth() - xpWidth, m_rageBar->GetHeight() ) );
    target->Blit( 4 * m_scale, 4 * m_scale, m_health );
}

void Hud::RenderWeapon( Claw::Surface* target )
{
    target->Blit( target->GetWidth() - ( RIGHT_BG_X * m_scale ), RIGHT_BG_Y * m_scale, m_gunInfo );

    Claw::SurfacePtr w( GameManager::GetInstance()->GetWeaponHUDAsset( m_weaponSel ) );
    if( w )
    {
        w->SetAlpha( 255 );
        target->Blit( target->GetWidth() - ( GUN_X * m_scale ) - w->GetWidth() / 2, ( GUN_Y * m_scale ), w );
    }

    if( m_elements & 0x1 )
    {
        target->Blit( target->GetWidth() - ( ( RIGHT_BG_X - 15 ) * m_scale ), ( RIGHT_BG_Y + 25 ) * m_scale, m_gunElectric );
    }
    if( m_elements & 0x2 )
    {
        target->Blit( target->GetWidth() - ( ( RIGHT_BG_X - 30 ) * m_scale ), ( RIGHT_BG_Y + 25 ) * m_scale, m_gunFire );
    }
    if( m_elements & 0x4 )
    {
        target->Blit( target->GetWidth() - ( ( RIGHT_BG_X - 45 ) * m_scale ), ( RIGHT_BG_Y + 25 ) * m_scale, m_gunIce );
    }

    if( m_reloadState != 1 )
    {
        target->Blit( target->GetWidth() - ( GUN_X * m_scale ) - m_reloadFill->GetWidth() / 2, ( GUN_Y * m_scale ), m_reloadFill );
        target->Blit( target->GetWidth() - ( GUN_X * m_scale ) - m_reload->GetWidth() / 2, ( GUN_Y * m_scale ), m_reload, Claw::Rect( m_reload->GetWidth() * m_reloadState, m_reload->GetHeight() ) );
    }
    if( m_reloadFinish )
    {
        target->Blit( target->GetWidth() - ( GUN_X * m_scale ) - m_reloadEnd->GetWidth() / 2, ( GUN_Y * m_scale ), m_reloadEnd );
    }
}

void Hud::RenderScore( Claw::Surface* target )
{
    target->Blit( ( target->GetWidth() - m_scoreBar->GetWidth() ) / 2, 8 * m_scale, m_scoreBar );
    target->Blit( ( target->GetWidth() - m_scoreBar->GetWidth() ) / 2 + 32 * m_scale, m_scale + m_scoreBar->GetHeight() - 1 * m_scale, m_scoreBar2 );
}

void Hud::RenderCash( Claw::Surface* target )
{
    if( m_cashState != CPS_DORMANT )
    {
        if( m_cpt == CPT_CASH )
        {
            target->Blit( ( target->GetWidth() - m_cashBg->GetWidth() ) / 2, target->GetHeight() - m_cashBg->GetHeight() * m_cashPosition, m_cashBg );
            target->Blit( ( target->GetWidth() - m_cashBg->GetWidth() ) / 2 + (5 * m_scale), target->GetHeight() - m_cashBg->GetHeight() * m_cashPosition + (3 * m_scale), m_cashIcon );
        }
        else
        {
            target->DrawFilledRectangle( Claw::Rect( ( target->GetWidth() - ( 270 * m_scale ) ) / 2, target->GetHeight() - ( 35 * m_scale ) * m_cashPosition, 270 * m_scale, 35 * m_scale ), Claw::MakeRGBA( 0, 0, 0, 165 ) );
            target->Blit( ( target->GetWidth() - ( 270 * m_scale ) ) / 2 + (5 * m_scale), target->GetHeight() - ( 35 * m_scale ) * m_cashPosition + (3 * m_scale), m_missionIco[m_cashMissionIco] );
            target->Blit( ( target->GetWidth() - ( 270 * m_scale ) ) / 2 + (20 * m_scale), target->GetHeight() - ( 35 * m_scale ) * m_cashPosition + (17 * m_scale), m_tick );
        }
    }
}

void Hud::RenderTexts( Claw::Surface* target )
{
    m_font->GetSurface()->SetAlpha( 255 );

    m_bullets->Draw( target, target->GetWidth() - ( ( RIGHT_BG_X - 15 ) * m_scale ), RIGHT_BG_Y * m_scale );

    if( m_showScore )
    {
        m_pointsTxt->Draw( target, ( target->GetWidth() - m_scoreBar->GetWidth() ) / 2 + 38 * m_scale, ( 8 + 3 ) * m_scale );
        m_multiTxt->Draw( target, ( target->GetWidth() - m_scoreBar->GetWidth() ) / 2 + 38 * m_scale, ( 8 + 3 + 24 ) * m_scale );
    }

    if( m_cashState != CPS_DORMANT )
    {
        if( m_cpt == CPT_CASH )
        {
            m_cashTxt->Draw( target, ( target->GetWidth() - m_cashBg->GetWidth() ) / 2, target->GetHeight() - m_cashBg->GetHeight() * m_cashPosition + (4 * m_scale) );
        }
        else
        {
            m_fontSmall->GetSurface()->SetAlpha( 255 );
            int offset = 0;
            if( m_cashTxt->GetHeight() == m_fontSmall->GetHeight() )
            {
                offset = m_fontSmall->GetHeight() / 2;
            }
            m_cashTxt->Draw( target, ( target->GetWidth() - ( 270 * m_scale ) ) / 2 + ( 45 * m_scale ), target->GetHeight() - ( 35 * m_scale ) * m_cashPosition + (4 * m_scale) + offset );
        }
    }
}

void Hud::RenderNuke( Claw::Surface* target )
{
    if( GameManager::GetInstance()->NukeAvailable() )
    {
        target->Blit( target->GetWidth() - m_nuke[0]->GetWidth() - ( 10 * m_scale ), ( target->GetHeight() - m_nuke[0]->GetHeight() ) / 2 - (70 * m_scale ), m_nuke[0] );
    }
    else if( GameManager::GetInstance()->NukeLaunched() )
    {
        target->Blit( target->GetWidth() - m_nuke[1]->GetWidth() - ( 10 * m_scale ), ( target->GetHeight() - m_nuke[1]->GetHeight() ) / 2 - (70 * m_scale ), m_nuke[1] );
    }
}

void Hud::RenderAirstrike( Claw::Surface* target )
{
    if( GameManager::GetInstance()->AirstrikeLaunched() )
    {
        target->Blit( target->GetWidth() - m_airstrike[1]->GetWidth() - ( 10 * m_scale ), ( target->GetHeight() - m_airstrike[1]->GetHeight() ) / 2 - (20 * m_scale ), m_airstrike[1] );
    }
    else if( GameManager::GetInstance()->AirstrikeAvailable() )
    {
        target->Blit( target->GetWidth() - m_airstrike[0]->GetWidth() - ( 10 * m_scale ), ( target->GetHeight() - m_airstrike[0]->GetHeight() ) / 2 - (20 * m_scale ), m_airstrike[0] );
    }
}

void Hud::RenderInventory( Claw::Surface* target )
{
    const int margin = m_stimpakIcon->GetHeight() * 0.25;
    const int textPadding = margin * 0.25f;
    int yOffset = 0;

    if( m_mines > 0 )
    {
        const Vectorf minesPos( margin, target->GetHeight() - m_minesIcon->GetHeight() - margin );
        yOffset = target->GetHeight() - minesPos.y;

        int prevAlpha = m_minesIcon->GetAlpha();
        m_minesIcon->SetAlpha( 255 );
        target->Blit( minesPos.x, minesPos.y, m_minesIcon );
        m_minesIcon->SetAlpha( prevAlpha );

        m_minesTxt->Draw( target, 
                          minesPos.x + m_minesIcon->GetWidth() + textPadding,
                          minesPos.y + ( m_minesIcon->GetHeight() - m_minesTxt->GetHeight() ) * 0.5f );
    }

    if( m_stimpaks > 0 )
    {
        const Vectorf stimpakPos( margin, target->GetHeight() - m_stimpakIcon->GetHeight() - margin - yOffset );

        int prevAlpha = m_stimpakIcon->GetAlpha();
        m_stimpakIcon->SetAlpha( 255 );
        target->Blit( stimpakPos.x, stimpakPos.y, m_stimpakIcon );
        m_stimpakIcon->SetAlpha( prevAlpha );

        m_stimpaksTxt->Draw( target, 
                             stimpakPos.x + m_stimpakIcon->GetWidth() + textPadding,
                             stimpakPos.y + ( m_stimpakIcon->GetHeight() - m_stimpaksTxt->GetHeight() ) * 0.5f );
    }

    if( m_grenades > 0 )
    {
        const Vectorf grenadesPos( target->GetWidth() - m_grenadesIcon->GetWidth() - margin,
                                          target->GetHeight() - m_grenadesIcon->GetHeight() - margin );
        yOffset = target->GetHeight() - grenadesPos.y;

        int prevAlpha = m_grenadesIcon->GetAlpha();
        m_grenadesIcon->SetAlpha( 255 );
        target->Blit( grenadesPos.x, grenadesPos.y, m_grenadesIcon );
        m_grenadesIcon->SetAlpha( prevAlpha );

        m_grenadesTxt->Draw( target, 
                             grenadesPos.x - m_grenadesTxt->GetWidth() - textPadding,
                             grenadesPos.y + ( m_grenadesIcon->GetHeight() - m_grenadesTxt->GetHeight() ) * 0.5f );
    }

    if( m_shields > 0 )
    {
        const Vectorf shieldPos( target->GetWidth() - m_shieldIcon->GetWidth() - margin,
                                        target->GetHeight() - m_shieldIcon->GetHeight() - margin - yOffset );

        int prevAlpha = m_shieldIcon->GetAlpha();
        m_shieldIcon->SetAlpha( 255 );
        target->Blit( shieldPos.x, shieldPos.y, m_shieldIcon );
        m_shieldIcon->SetAlpha( prevAlpha );

        m_shieldTxt->Draw( target, 
                            shieldPos.x - m_shieldTxt->GetWidth() - textPadding,
                            shieldPos.y + ( m_shieldIcon->GetHeight() - m_shieldTxt->GetHeight() ) * 0.5f );
    }
}

void Hud::Update( float dt )
{
    m_time += dt;
    m_pauseTime -= dt;

    m_playerDamage = Claw::MinMax( m_playerDamage - dt, 0.f, 2.f );

    if( m_reloadState != 1 )
    {
        m_reloadFill->Update( dt );
    }
    if( m_reloadFinish )
    {
        if( !m_reloadEnd->Update( dt ) )
        {
            m_reloadFinish = false;
        }
    }

    switch( m_cashState )
    {
    case CPS_DORMANT:
        if( GameManager::GetInstance()->IsPauseMenu() )
        {
            m_cashState = CPS_SLIDEIN;
            if( m_cashQueue.empty() )
            {
                m_cpt = CPT_CASH;
            }
            else
            {
                m_cpt = m_cashQueue.front();
                m_cashQueue.pop_front();
            }
            RefreshPopup();
        }
        else
        {
            if( !m_cashQueue.empty() )
            {
                m_cpt = m_cashQueue.front();
                m_cashQueue.pop_front();
                RefreshPopup();
                m_cashState = CPS_SLIDEIN;
            }
        }
        break;
    case CPS_SLIDEIN:
        m_cashSlider += dt * 2;
        if( m_cashSlider > 1 )
        {
            m_cashSlider = 0;
            m_cashPosition = 1;
            m_cashState = CPS_ACTIVE;
        }
        else
        {
            m_cashPosition = Claw::SmoothStep( 0.f, 1.f, m_cashSlider );
        }
        break;
    case CPS_ACTIVE:
        if( GameManager::GetInstance()->IsPauseMenu() && m_cpt == CPT_CASH )
        {
            m_cashSlider = 0;
        }
        else
        {
            m_cashSlider += dt;
            if( m_cashSlider > 1 )
            {
                m_cashSlider = 1;
                m_cashState = CPS_SLIDEOUT;
            }
        }
        break;
    case CPS_SLIDEOUT:
        m_cashSlider -= dt * 2;
        if( m_cashPosition <= 0 )
        {
            m_cashSlider = 0;
            m_cashPosition = 0;
            m_cashState = CPS_DORMANT;
        }
        else
        {
            m_cashPosition = Claw::SmoothStep( 0.f, 1.f, m_cashSlider );
        }
        break;
    default:
        CLAW_ASSERT( false );
        break;
    };
}

void Hud::RenderPickupHints( Claw::Surface* target )
{
    const std::vector<Pickup*>& pickups = GameManager::GetInstance()->GetPickupManager()->GetPickups();
    const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
    const Vectorf& offset = GameManager::GetInstance()->GetMap()->GetOffset();
    Claw::Rect rect( offset.x, offset.y, resolution.x, resolution.y );

    rect.m_h *= m_rscale;
    rect.m_w *= m_rscale;
    rect.m_x *= m_rscale;
    rect.m_y *= m_rscale;

    for( std::vector<Pickup*>::const_iterator it = pickups.begin(); it != pickups.end(); ++it )
    {
        Pickup* p = *it;
        if( p->m_type != Pickup::MagicOrb && p->m_type != Pickup::Potato && !rect.IsInRect( p->GetPos().x, p->GetPos().y ) )
        {
            int idx = p->m_type <= static_cast<Pickup::Type>(Pickup::LastWeapon) ? 1 : 0;
            RenderHint( target, idx, p->GetPos() );
        }
    }

    const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
    Entity* player = GameManager::GetInstance()->GetPlayer();
    if( !player )
    {
        return;
    }

    Vectori center( rect.m_x + rect.m_w / 2, rect.m_y + rect.m_h / 2 );
    Vectorf fcenter( center.x, center.y );

    Entity* closest = NULL;
    float dist = 1000000000;
    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        Entity* e = *it;

        if( e->GetClass() == Entity::Friendly || !e->m_draw )     // player is also friendly
        {
            continue;
        }
        if( rect.IsInRect( e->GetPos().x, e->GetPos().y ) )
        {
            closest = NULL;
            break;
        }

        Vectorf v = e->GetPos() - fcenter;
        float l = DotProduct( v, v );
        if( l < dist )
        {
            dist = l;
            closest = e;
        }
    }

    if( closest )
    {
        RenderHint( target, 2, closest->GetPos() );
    }

    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        Entity* e = *it;

        if( e->GetClass() != Entity::Friendly || e->GetType() == Entity::Player )
        {
            continue;
        }
        if( !rect.IsInRect( e->GetPos().x, e->GetPos().y ) )
        {
            Vectorf v = e->GetPos() - fcenter;
            float l = DotProduct( v, v );
            if( l < dist )
            {
                dist = l;
                closest = e;
            }
            if( e->GetType() == Entity::AIFriend )
            {
                RenderHint( target, 6, e->GetPos() );
            }
            else
            {
                RenderHint( target, 3, e->GetPos() );
            }
        }
    }

    const std::vector<Map::StaticObject*>& so = GameManager::GetInstance()->GetMap()->GetStaticObjects();
    for( std::vector<Map::StaticObject*>::const_iterator it = so.begin(); it != so.end(); ++it )
    {
        if( (*it)->m_marker )
        {
            Map::StaticObjectIsoSet* si = (Map::StaticObjectIsoSet*)*it;
            const Vectorf& pos = si->GetPos();
            const Vectorf size( si->m_isoset->GetWidth() / 2, si->m_isoset->GetHeight() / 2 );
            const Vectorf mid = pos + size * 0.5f;

            if( !rect.IsInRect( mid.x, mid.y ) )
            {
                RenderHint( target, 4, mid );
            }
        }
    }

    const std::map<const char*, MarkerArrowPtr, Map::Comparator>& markers = GameManager::GetInstance()->GetMap()->GetMarkers();
    for( std::map<const char*, MarkerArrowPtr, Map::Comparator>::const_iterator it = markers.begin(); it != markers.end(); ++it )
    {
        const Vectorf& pos = it->second->GetPos();
        if( !rect.IsInRect( pos.x, pos.y ) )
        {
            switch( it->second->GetType() )
            {
            case MarkerArrow::Target:
                RenderHint( target, 4, pos );
                break;
            case MarkerArrow::AccessCard:
                RenderHint( target, 5, pos );
                break;
            case MarkerArrow::Exit:
                RenderHint( target, 7, pos );
                break;
            default:
                CLAW_ASSERT( false );
                break;
            }
        }
    }
}

void Hud::RenderHint( Claw::Surface* target, int idx, Vectorf epos )
{
    const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
    const Vectorf& offset = GameManager::GetInstance()->GetMap()->GetOffset();
    Claw::Rect rect( offset.x, offset.y, resolution.x, resolution.y );

    rect.m_h *= m_rscale;
    rect.m_w *= m_rscale;
    rect.m_x *= m_rscale;
    rect.m_y *= m_rscale;

    Vectorf center( rect.m_x + rect.m_w / 2, rect.m_y + rect.m_h / 2 );

    epos.x += sin( m_time ) * 10;
    epos.y += cos( m_time ) * 10;

    Vectorf v = epos - center;
    Vectorf n( v );
    float l = n.Normalize();

    float gfxoff = m_pickupArrow[idx]->GetHeight() * 1.5f;

    float x = ( rect.m_h - gfxoff * 2 * m_rscale ) / ( 2 * fabs( v.y ) );
    float y = ( rect.m_w - gfxoff * 2 * m_rscale ) / ( 2 * fabs( v.x ) );
    float s = std::min( x, y );

    float off = 30 * m_scale;

    Vectorf pos( center + v * s );
    float l2 = ( l * s ) + off;

    float m[4] = { n.y, -n.x, -n.x, -n.y };

    if( l - l2 < off )
    {
        m_pickupArrow[idx]->SetAlpha( std::max( 0.f, ( l - l2 ) / off * 255 ) );
        m_pickupExclamation[idx]->SetAlpha( std::max( 0.f, ( l - l2 ) / off * 192 ) );
    }
    else
    {
        m_pickupArrow[idx]->SetAlpha( 255 );
        m_pickupExclamation[idx]->SetAlpha( 192 );
    }

    Claw::TriangleEngine::Blit( target, m_pickupArrow[idx],
        pos.x * m_scale - offset.x, pos.y * m_scale - offset.y, m, Vectorf( m_pickupArrow[idx]->GetWidth() * 0.5f, gfxoff ) );
    Claw::TriangleEngine::Blit( target, m_pickupExclamation[idx],
        pos.x * m_scale - offset.x, pos.y * m_scale - offset.y, 0, 0.7f * ( 1.5f + 0.5f * sin( m_time * 4 ) ),
        Vectorf( m_pickupExclamation[idx]->GetSize() / 2 ) );
}

void Hud::RefreshPopup()
{
    Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
    Claw::Text::Format format;

    if( m_cpt == CPT_CASH )
    {
        fontSet->AddFont( "f", m_font );

        format.SetFontSet( fontSet );
        format.SetFontId( "f" );
        format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_RIGHT );

        char tmp[32];
        sprintf( tmp, "%i", m_cash );

        m_cashTxt.Reset( new Claw::ScreenText( format, Claw::String( tmp ), Claw::Extent( m_cashBg->GetWidth() - ( 15 * m_scale ), 0 ) ) );
    }
}

void Hud::SetInventory( int grenades, int stimpaks, int landmines, int shield )
{
    if( m_grenades != grenades )
    {
        m_grenades = grenades;
        m_refreshGrenades = true;
    }

    if( m_stimpaks != stimpaks )
    {
        m_stimpaks = stimpaks;
        m_refreshStimpaks = true;
    }

    if( m_mines != landmines )
    {
        m_mines = landmines;
        m_refreshMines = true;
    }

    if( m_shields != shield )
    {
        m_shields = shield;
        m_refreshShield = true;
    }
}

Vectorf Hud::GetWeaponPosition() const
{
    Claw::SurfacePtr w( GameManager::GetInstance()->GetWeaponHUDAsset( m_weaponSel ) );
    CLAW_ASSERT( w );

    const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
    return Vectorf( resolution.x - GUN_X*m_scale, GUN_Y*m_scale + w->GetHeight() );
}

void Hud::RenderHit( Claw::Surface* target, float intensity )
{
    if( m_lowquality )
    {
        m_hit->SetAlpha( intensity * 255 );

        float scale = 0.6f + 0.4f * intensity;

        Claw::TriangleEngine::Blit( target, m_hit, 0, 0, 0, scale, Vectorf( 0, 0 ) );
        Claw::TriangleEngine::Blit( target, m_hit, target->GetWidth(), 0, M_PI/2, scale, Vectorf( 0, 0 ) );
        Claw::TriangleEngine::Blit( target, m_hit, target->GetWidth(), target->GetHeight(), M_PI, scale, Vectorf( 0, 0 ) );
        Claw::TriangleEngine::Blit( target, m_hit, 0, target->GetHeight(), -M_PI/2, scale, Vectorf( 0, 0 ) );
    }
    else
    {
        Claw::g_batcher->SetSource( m_bokeh[0]->GetPixelData(), m_bokeh[0]->QueryFlag(), m_bokeh[0]->GetAlpha() );
        Claw::g_batcher->SetShader( &m_shader );
        Claw::g_batcher->SetTexture( 2, m_bokeh[1]->GetPixelData(), m_bokeh[1]->QueryFlag() );
        m_shader.Uniform( "intensity", intensity );
        m_shader.Uniform( "mask", 2 );
        target->BlitScale( m_bokeh[0] );
        Claw::g_batcher->SetShader( NULL );
    }
}

#include <sstream>

#include "MonstazAI/GameCenterManager.hpp"
#include "MonstazAI/GameCenterPopup.hpp"
#include "MonstazAI/network/gamecenter/GameCenter.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/Achievements.hpp"

#include "claw/base/Errors.hpp"

#include "AnalyticsManager.hpp"

static const char* GC_SCORES_CATEGORIES[GameCenterManager::SC_ALL] =
{
#ifdef CLAW_ANDROID
    "1176017",  // Moon
    "1176027",  // Mars
    "1176037",  // Spaceship
    "1199007"   // Ice
#else
    "com.gamelion.mstll.survival.moon",
    "com.gamelion.mstll.survival.mars",
    "com.gamelion.mstll.survival.spaceship",
    "com.gamelion.mstll.survival.ice"
#endif
};

static const char* OF_SCORES_CATEGORIES[GameCenterManager::SC_ALL] =
{
    "1176017", // Moon
    "1176027", // Mars
    "1176037", // Spaceship
    "1199007"  // Ice
};

//static const char* OF_ACHIEVEMENTS_MASTERY[mastery][stage]
static const char* OF_ACHIEVEMENTS_MASTERY[][4] = 
{
    // x - stage
    // y - mastery
    {"1681692", "1681952", "1682252", "1710602"},
    {"1681702", "1681962", "1682262", "1710612"},
    {"1681712", "1682002", "1682272", "1710622"},
    {"1681722", "1682022", "1682282", "1710632"},
    {"1681732", "1682032", "1682302", "1710642"},
    {"1681742", "1682042", "1682312", "1710652"},
    {"1681762", "1682062", "1682332", "1710662"},
    {"1681772", "1682072", "1682342", "1710672"},
    {"1681782", "1682122", "1682352", "1710682"},
    {"1681792", "1682132", "1682362", "1710702"},
    {"1681802", "1682142", "1682382", "1710692"},
    {"1681812", "1682152", "1682372", "1710712"},
    {"1681832", "1682162", "1682392", "1710722"},
    {"1681842", "1682182", "1682402", "1710732"},
    {"1681852", "1682192", "1682412", "1710742"},
    {"1681872", "1682202", "1682422", "1710752"},
    {"1681892", "1682212", "1682432", "1710762"},
    {"1681912", "1682222", "1682442", "1710772"},
    {"1681922", "1682232", "1682452", "1710782"},
    {"1681932", "1682242", "1682462", "1710792"}
};

static const char* OF_ACHIEVEMENTS_RANK[] = 
{
    "1682472",
    "1682482",
    "1682492",
    "1682502",
    "1682512",
    "1682522",
    "1682532",
    "1682542",
    "1682552",
    "1682562",
    "1682572",
    "1682582",
    "1682592",
    "1682602",
    "1682612",
    "1682622",
    "1682642",
    "1682632",
    "1682652"
};


#ifdef CLAW_ANDROID
static const char* GC_CREDITERIALS_APP_NAME = "Monster Shooter - Lost Levels";
static const char* GC_CREDITERIALS_APP_ID   = "0";
static const char* GC_CREDITERIALS_SECRET   = "0";
static const char* GC_CREDITERIALS_KEY      = "0";
#else
static const char* GC_CREDITERIALS_APP_NAME = "Monster Shooter";
static const char* GC_CREDITERIALS_APP_ID   = "";
static const char* GC_CREDITERIALS_SECRET   = "0";
static const char* GC_CREDITERIALS_KEY      = "0";
#endif

GameCenterManager* GameCenterManager::s_instance = NULL;

GameCenterManager* GameCenterManager::GetInstance()
{
    if( !s_instance )
    {
        s_instance = new GameCenterManager;
    }
    return s_instance;
}

void GameCenterManager::Release()
{
    if( s_instance )
    {
        delete s_instance;
        s_instance = NULL;
    }
}

GameCenterManager::GameCenterManager()
    : m_gameCenter( NULL )
    , m_openFeint( NULL )
    , m_authenticationTry( false )
{
    GameCenterPopup::Initialize();
    m_gameCenter = GameCenter::QueryInterface();
    m_gameCenter->RegisterObserver( this );
}

GameCenterManager::~GameCenterManager()
{
    if( m_gameCenter )
    {
        GameCenter::Release( m_gameCenter );
    }

    if( m_openFeint )
    {
        GameCenter::Release( m_openFeint );
    }
}

bool GameCenterManager::Authenticate()
{
    if( !m_gameCenter )
    {
        return false;
    }

    if( !m_authenticationTry )
    {
        GameCenter::Crediterials crediterials;
        crediterials[GameCenter::AC_APP_ID]           = GC_CREDITERIALS_APP_ID;
        crediterials[GameCenter::AC_APP_NAME]         = GC_CREDITERIALS_APP_NAME;
        crediterials[GameCenter::AC_PRODUCT_SECRET]   = GC_CREDITERIALS_SECRET;
        crediterials[GameCenter::AC_PRODUCT_KEY]      = GC_CREDITERIALS_KEY;

        if ( m_openFeint )
        {
            m_openFeint->Authenticate( &crediterials );
        }

        return m_gameCenter->Authenticate( &crediterials );
    }
    return false;
}

bool GameCenterManager::SubmitScore( GameCenterManager::ScoreCategory sc, int score )
{
    CLAW_ASSERT( sc < SC_ALL );

    if( !m_gameCenter )
    {
        return false;
    }

    if ( m_openFeint )
    {
        m_openFeint->SubmitScore( OF_SCORES_CATEGORIES[ sc ], score );
    }

    return m_gameCenter->SubmitScore( GC_SCORES_CATEGORIES[ sc ], score );
}

bool GameCenterManager::ShowLeaderboard( GameCenterManager::ScoreCategory sc )
{
    if( !m_gameCenter )
    {
        return false;
    }

    GameCenter* instance = NULL;
    const char* leaderboard = NULL;

    GameCenter::Type type = GetDefaultGameCenter();

#ifdef CLAW_IPHONE
    if ( type == GameCenter::OpenFeint )
    {
        CLAW_ASSERT( m_openFeint );
        instance = m_openFeint;
        leaderboard = sc < SC_ALL ? OF_SCORES_CATEGORIES[ sc ] : "";
    }
    else
#endif
    {
        if ( type != GameCenter::Default && type != GameCenter::GameKit )
        {
            CLAW_MSG( "Requested GameCenter implementation not available, using default." );
        }

        instance = m_gameCenter;
        leaderboard = sc < SC_ALL ? GC_SCORES_CATEGORIES[ sc ] : "";
    }

    return instance->ShowLeaderboard( leaderboard );
}

GameCenter::Type GameCenterManager::GetDefaultGameCenter() const
{
    int id = GameCenter::Default;
    Claw::Registry::Get()->Get( "/monstaz/settings/gamecenter", id );
    return (GameCenter::Type)id;
}

void GameCenterManager::SetDefualtGameCenter( GameCenter::Type type )
{
    Claw::Registry::Get()->Set( "/monstaz/settings/gamecenter", type );

    MonstazAI::MonstazAIApplication* application = (MonstazAI::MonstazAIApplication*)Claw::Application::GetInstance();
    application->Save();
}

/////////////////////////////////////////////////////////////////////
// Lua interface
/////////////////////////////////////////////////////////////////////

LUA_DECLARATION( GameCenterManager )
{
    METHOD( GameCenterManager, SubmitScore ),
    METHOD( GameCenterManager, SubmitScoreKiip ),
    METHOD( GameCenterManager, ShowLeaderboard ),
    METHOD( GameCenterManager, SetDefaultGameCenter ),
    METHOD( GameCenterManager, GetDefaultGameCenter ),
    METHOD( GameCenterManager, Achievement ),
    METHOD( GameCenterManager, ShowAchievements ),
    METHOD( GameCenterManager, SubmitAllAchievements ),
    {0,0}
};

GameCenterManager::GameCenterManager( lua_State* L )
{
    CLAW_ASSERT( false );
}

void GameCenterManager::Init( Claw::Lua* lua )
{
    Claw::Lunar<GameCenterManager>::Register( *lua );
    Claw::Lunar<GameCenterManager>::push( *lua, this );
    lua->RegisterGlobal( "GameCenterManager" );

    lua->CreateEnumTable();
    lua->AddEnum( SC_MOON );
    lua->AddEnum( SC_MARS );
    lua->AddEnum( SC_SPACESHIP );
    lua->AddEnum( SC_ICE );
    lua->RegisterEnumTable( "ScoreCategory" );

    lua->CreateEnumTable();
    lua->AddEnum( GameCenter::Default );
    lua->AddEnum( GameCenter::GameKit );
    lua->AddEnum( GameCenter::OpenFeint );
    lua->RegisterEnumTable( "GameCenterType" );
}

int GameCenterManager::l_SubmitScore( lua_State* L )
{
    Claw::Lua lua( L );
    SubmitScore( lua.CheckEnum<ScoreCategory>( 1 ), lua.CheckNumber( 2 ) );
    return 0;
}

int GameCenterManager::l_SubmitScoreKiip( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int GameCenterManager::l_ShowLeaderboard( lua_State* L )
{
    Claw::Lua lua( L );
    if( lua.IsNil( 1 ) )
    {
        ShowLeaderboard( SC_ALL );
    }
    else
    {
        ShowLeaderboard( lua.CheckEnum<ScoreCategory>( 1 ) );
    }
    return 0;
}

int GameCenterManager::l_SetDefaultGameCenter( lua_State* L )
{
    Claw::Lua lua( L );
    SetDefualtGameCenter( (GameCenter::Type)lua.CheckEnum<GameCenter::Type>( 1 ) );
    return 0;
}

int GameCenterManager::l_GetDefaultGameCenter( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( GetDefaultGameCenter() );
    return 1;
}

int GameCenterManager::l_Achievement( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::NarrowString id( lua.CheckString( 1 ) );
    Claw::NarrowString reportId = id;
    float percent = 100;
    int world = lua.CheckNumber( 3 );
    int level = lua.CheckNumber( 4 );
    if( lua.IsNumber( 5 ) )
    {
        percent = lua.CheckNumber( 5 );
    }

#ifdef ANDROID_BUILD
    if( world > 0 )
    {
        reportId = OF_ACHIEVEMENTS_MASTERY[level-1][world-1];
    }
    else
    {
        reportId = OF_ACHIEVEMENTS_RANK[level-2];
    }
#endif
    CLAW_ASSERT( m_gameCenter );

    if( m_openFeint )
    {
        m_openFeint->SubmitAchievement( reportId, percent );
    }

    if( m_gameCenter )
    {
        m_gameCenter->SubmitAchievement( reportId, percent );
    }

    if( percent >= 100 )
    {
        std::stringstream ss;
        ss << "/monstaz/achievements/" << id;
        bool doneBefore = false;
        Claw::Registry::Get()->Get( ss.str().c_str(), doneBefore );
        Claw::Registry::Get()->Set( ss.str().c_str(), true );
        if( !doneBefore )
        {
            Achievements::GetInstance()->Show( lua.CheckString( 2 ), lua.CheckNumber( 3 ) );
            CLAW_MSG( "Achievement " << id << " first time!" );
        }
    }

    return 0;
}

int GameCenterManager::l_ShowAchievements( lua_State* L )
{
    if( m_gameCenter )
    {
        m_gameCenter->ShowAchievements();
    }
    return 0;
}

int GameCenterManager::l_SubmitAllAchievements( lua_State* L )
{
    SubmitAllAchievements();
    return 0;
}

void GameCenterManager::SubmitAllAchievements()
{
    if( !m_gameCenter || !m_gameCenter->IsAuthenticated() )
    {
        return;
    }

    for( int i=1; i<=4; i++ )
    {
        for( int j=1; j<=20; ++j )
        {
            bool done = false;
            std::stringstream s;
            s << "/monstaz/achievements/com.gamelion.mstll." << i << "_" << j << "_mastery";
            const std::string& temp = s.str();
            const char* str = temp.c_str();

            Claw::Registry::Get()->Get( str, done );
            if( done )
            {
#ifdef CLAW_IPHONE
                std::stringstream p;
                p << "com.gamelion.mstll." << i << "_" << j << "_mastery";
                str = p.str().c_str();
#else
                str = OF_ACHIEVEMENTS_MASTERY[j-1][i-1];
#endif
                if( m_openFeint )
                {
                    m_openFeint->SubmitAchievement( str, 100 );
                }

                m_gameCenter->SubmitAchievement( str, 100 );
            }
        }
    }
    for( int i=2; i<=20; ++i )
    {
        bool done = false;
        std::stringstream s;
        s << "/monstaz/achievements/com.gamelion.mstll.rank_" << i;
        const std::string& temp = s.str();
        const char* str = temp.c_str();

        Claw::Registry::Get()->Get( str, done );
        if( done )
        {
#ifdef CLAW_IPHONE
            std::stringstream p;
            p << "com.gamelion.mstll.rank_" << i;
            str = p.str().c_str();
#else
            str = OF_ACHIEVEMENTS_RANK[i-2];
#endif
            if( m_openFeint )
            {
                m_openFeint->SubmitAchievement( str, 100 );
            }

            m_gameCenter->SubmitAchievement( str, 100 );
        }
    }
}

void GameCenterManager::OnAchievementsLoad( const GameCenter::Achievements& achievements )
{}

void GameCenterManager::OnAuthenticationChange( bool authenticated )
{
    if( authenticated )
    {
        SubmitAllAchievements();
    }
}

void GameCenterManager::OnLeaderboardView( bool opened )
{}

void GameCenterManager::OnAchievementsView( bool opened )
{}

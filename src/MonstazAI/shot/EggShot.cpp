#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Matrix.hpp"

#include "MonstazAI/shot/EggShot.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

EggShot::EggShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Shot::Type type, int life, float rotation, char elements )
    : Shot( owner, pos, look, speed, damage, gfx, type, life, 1, elements, false )
    , m_rotation( rotation )
    , m_angle( 0 )
    , m_time( 0 )
    , m_z( 40 + g_rng.GetDouble() * 20.0f )
    , m_vz( 3.75f + g_rng.GetDouble() * 2.5f )
{
}

EggShot::~EggShot()
{
}

void EggShot::Explode()
{
    Explosion::Params p( 0, 0, 0, 0 );
    GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_MENU_GWIAZDKI, GetPos() );
    GameManager::GetInstance()->GetExplosionManager()->Add( GetPos(), p, false, Shot::SowerEgg );
    GameManager::GetInstance()->GetEntityManager()->Add( GetPos().x, GetPos().y, Entity::FloaterSower );
}

void EggShot::Update( float dt )
{
    m_angle += m_rotation * dt;

    m_time += dt;
    while( m_time > 1/60.f )
    {
        m_time -= 1/60.f;

        m_z += m_vz;
        m_vz -= 0.135f;

        if( m_z < 0 )
        {
            // Fall in hole
            if( IsInHole() )
            {
                m_life = 1;
            }
            // Bounce
            else
            {
                m_z *= -1;
                m_vz *= -0.5f;
                m_speed *= 0.5f;
                m_rotation *= 0.5f;
            }
        }
        if( m_speed < 0.1f )
        {
            Explode();
            m_life = 1;
        }
    }
}

void EggShot::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    Vectorf look = GetDir();
    look.Rotate( m_angle );
    float m[4] = { look.y, -look.x, -look.x, -look.y };

    if( scale != 1.0f )
    {
        Claw::TriangleEngine::Blit( target, m_gfx->GetSurface(), m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - m_z * scale, m, m_gfx->GetPivot() );
    }
    else
    {
        Claw::TriangleEngine::Blit( target, m_gfx->GetSurface(), m_pos[0] - offset.x, m_pos[1] - offset.y - m_z, m, m_gfx->GetPivot() );
    }
}

bool EggShot::IsInHole()
{
    if( m_z > 0 )
        return false;

    Obstacle* o = GameManager::GetInstance()->GetObstacleManager()->QueryCollision( GetPos() );

    return o && o->GetKind() == Obstacle::Ground;
}

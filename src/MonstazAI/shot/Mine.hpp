#ifndef __MONSTAZ__MINE_HPP__
#define __MONSTAZ__MINE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/AnimSurfWrap.hpp"

class Mine : public Shot
{
public:
    enum State
    {
        S_ARMING,
        S_LIVE,
        S_COUNTDOWN,
        S_DEAD
    };

    Mine( const Entity* owner, const Vectorf& pos, float damage, Shot::Type type, Claw::Surface* arm, GfxAsset* green, GfxAsset* red, char elements );
    ~Mine();

    void Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    void Countdown();

    State GetState() const { return m_state; }

private:
    void Explode();

    State m_state;
    AnimSurfWrapPtr m_arm;
    GfxAssetPtr m_green;
    GfxAssetPtr m_red;

    float m_time;
};

#endif

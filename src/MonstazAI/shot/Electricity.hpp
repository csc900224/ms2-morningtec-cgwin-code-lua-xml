#ifndef __MONSTAZ__ELECTRICITY_HPP__
#define __MONSTAZ__ELECTRICITY_HPP__

#include <vector>

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/Renderable.hpp"

class Electricity;
class Entity;

class ElectricityArc : public Renderable
{
public:
    ElectricityArc() {}
    ElectricityArc( Electricity* e, const Vectori& nodes, float power, int gfx );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    int GetNode( int idx ) const { return m_nodes[idx]; }

private:
    Electricity* m_e;
    Vectori m_nodes;
    float m_power;
    int m_gfx;
};

class Electricity : public Claw::RefCounter
{
public:
    Electricity( const Entity* owner, const Vectorf& pos, const Vectorf& look, Claw::SurfacePtr* gfx, float damage, bool rage, bool laser, char elements );

    void Render( Claw::Surface* target, const Vectorf& offset, float scale );
    void RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale );

    const Vectorf& GetNode( int idx ) const { return m_nodes[idx]; }
    Claw::SurfacePtr* GetGfx() { return m_gfx; }
    const Entity* GetOwner() const { return m_owner; }

    int m_power;
    int m_powerDecay;

private:
    void Generate( int idx, int num, const Vectorf& look, const Entity* owner, char elements );
    void GenerateLaser( int idx, int num, const Vectorf& look, const Entity* owner, char elements );

    Vectorf m_nodes[384];
    int m_nodesIdx;
    int m_links[384];
    int m_linksIdx;

    Claw::SurfacePtr* m_gfx;

    float m_damage;
    const Entity* m_owner;

    ElectricityArc m_arc[384];
};

typedef Claw::SmartPtr<Electricity> ElectricityPtr;

#endif

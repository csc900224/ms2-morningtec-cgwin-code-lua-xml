#ifndef __MONSTAZ__VORTEXSHOT_HPP__
#define __MONSTAZ__VORTEXSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/obstacle/Obstacle.hpp"
#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"

class VortexShot : public Shot
{
public:
    VortexShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Claw::Surface* heat, Shot::Type type, int life, char elements );
    ~VortexShot();

    void Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    bool ShotHit( Obstacle* o );

    float m_time;
    float m_wait;

private:
    void ShotHit( ObstacleCircle* o );
    void ShotHit( ObstacleRectangle* o );

    Claw::SurfacePtr m_heat;

    float m_angle;
};

#endif

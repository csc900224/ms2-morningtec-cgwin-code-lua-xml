#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Matrix.hpp"

#include "MonstazAI/shot/FlamerShot.hpp"
#include "MonstazAI/RNG.hpp"

FlamerShot::FlamerShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Claw::Surface* heat, Shot::Type type, int life, float rotation, char elements )
    : Shot( owner, pos, look, speed, damage, gfx, type, life, 1, elements, false )
    , m_scale( 0.5f )
    , m_rotation( rotation )
    , m_angle( 0 )
    , m_heat( heat )
{
}

FlamerShot::~FlamerShot()
{
}

void FlamerShot::Update( float dt )
{
    m_scale += dt * 4.f;
    m_angle += m_rotation * dt;
}

void FlamerShot::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    Vectorf look = GetDir() * m_scale;
    look.Rotate( m_angle );
    float m[4] = { look.y, -look.x, -look.x, -look.y };

    if( m_life < 16 )
    {
        m_gfx->GetSurface()->SetAlpha( 255 * m_life / 16 );
    }
    else
    {
        m_gfx->GetSurface()->SetAlpha( 255 );
    }

    if( scale != 1.0f )
    {
        Claw::TriangleEngine::BlitAdditive( target, m_gfx->GetSurface(), m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - 10 * scale, m, m_gfx->GetPivot() );
    }
    else
    {
        Claw::TriangleEngine::BlitAdditive( target, m_gfx->GetSurface(), m_pos[0] - offset.x, m_pos[1] - offset.y - 10, m, m_gfx->GetPivot() );
    }
}

void FlamerShot::RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    Vectorf look = GetDir() * m_scale;
    look.Rotate( -m_angle * 8 );
    look /= 2.25f;
    float m[4] = { look.y, -look.x, -look.x, -look.y };

    Claw::TriangleEngine::Blit( target, m_heat, ( m_pos.x * scale - offset.x ) / 4, ( m_pos.y * scale - offset.y - 20 ) / 4, m, Vectorf( m_heat->GetSize() / 2 ) );
}

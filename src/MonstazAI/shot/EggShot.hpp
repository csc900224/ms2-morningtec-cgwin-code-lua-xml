#ifndef __MONSTAZ__EGGSHOT_HPP__
#define __MONSTAZ__EGGSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"

class EggShot : public Shot
{
public:
    EggShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Shot::Type type, int life, float rotation, char elements );
    ~EggShot();

    void Explode();

    void Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    float m_rotation;
    float m_angle;

    float m_z;
    float m_vz;

private:
    bool  IsInHole();

    float m_time;
};

#endif

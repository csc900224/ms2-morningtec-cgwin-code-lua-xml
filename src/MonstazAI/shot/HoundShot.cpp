#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Matrix.hpp"

#include "MonstazAI/shot/HoundShot.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

static const float TargetTime = 1.5f;
static const float InitHeight = 300;

HoundShot::HoundShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Type type, int life, int steps, char elements, bool rage )
    : Shot( owner, pos, look, speed, damage, gfx, type, life, steps, elements, rage )
    , m_time( 0 )
{
}

HoundShot::~HoundShot()
{
}

void HoundShot::Explode()
{
    // \todo Set sane values.
    Explosion::Params p( 0.2f, 1.5f, 400, 4 );

    GameManager::GetInstance()->AddExplosionHole( GetPos() );
    GameManager::GetInstance()->GetExplosionManager()->Add( GetPos(), p, false, (Shot::Type)m_type );
}

void HoundShot::Update( float dt )
{
    m_time += dt;

    if( m_time > TargetTime )
    {
        Explode();
        m_life = 1;
    }
}

void HoundShot::Render( Claw::Surface* target, const Vectorf& offset, float s ) const
{
    m_gfx->Blit( target, m_pos.x * s - offset.x, ( m_pos.y - InitHeight * ( TargetTime - m_time ) ) * s - offset.y );
}

float HoundShot::TimeLeft()
{
    return TargetTime - m_time;
}

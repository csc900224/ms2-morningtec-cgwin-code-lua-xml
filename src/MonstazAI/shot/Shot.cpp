#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

Shot::Shot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Type type, int life, int steps, char elements, bool rage )
    : Renderable( pos )
    , m_owner( owner )
    , m_dir( look )
    , m_speed( speed )
    , m_damage( damage )
    , m_gfx( gfx )
    , m_lastHit( NULL )
    , m_len( 51 )   // 0.2
    , m_kills( 0 )
    , m_type( (Claw::UInt8)type )
    , m_life( life )
    , m_steps( steps )
    , m_elements( elements )
{
    CLAW_ASSERT( life >= -128 && life <= 127 );
    CLAW_ASSERT( steps > 0 && steps <= 127 );

    if( rage )
    {
        m_steps |= 0x80;
    }
}

Shot::~Shot()
{
}

void Shot::Update( float dt )
{
    if( m_len < 255 )
    {
        m_len = std::min( 255, m_len + 42 );
    }
}

void Shot::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    float len = m_len == 255 ? 1 : m_len / 255.f;

    float m[4] = { m_dir.y, -m_dir.x * len, -m_dir.x, -m_dir.y * len };
    Claw::TriangleEngine::Blit( target, m_gfx->GetSurface(), m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - 10 * scale, m, m_gfx->GetPivot() );

    switch( m_type )
    {
    case Bullet:
    case CombatShell:
    case Shell:
    case Minigun:
    case Magnum:
        {
        Claw::Surface* glow = GameManager::GetInstance()->GetShotManager()->GetShotGlow();
        target->BlitAdditive( m_pos.x * scale - offset.x - glow->GetWidth() / 2, m_pos.y * scale - offset.y - 10 * scale - glow->GetHeight() / 2, glow );
        }
        break;
    default:
        break;
    }
}

void Shot::RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
}

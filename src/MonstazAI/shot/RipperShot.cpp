#include "claw/math/Math.hpp"

#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/shot/RipperShot.hpp"
#include "MonstazAI/RNG.hpp"

RipperShot::RipperShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, Shot::Type type, int life, Claw::SurfacePtr* gfx, char elements )
    : Shot( owner, pos, look, speed, damage, NULL, type, life, 1, elements, false )
    , m_gfx( gfx )
    , m_frame( 0 )
    , m_blood( 0 )
    , m_time( 0 )
    , m_durability( 5 )
{
}

RipperShot::~RipperShot()
{
}

void RipperShot::Update( float dt )
{
    m_time += dt;
    while( m_time > 0.033f )
    {
        m_time -= 0.033f;
        m_frame = ( m_frame + 1 ) % 3;
    }
}

void RipperShot::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    Claw::Surface* s = m_gfx[m_blood*3+m_frame];
    s->SetAlpha( 255 );

    if( scale != 1.0f )
    {
        target->Blit( m_pos.x * scale - offset.x - s->GetWidth()/2, m_pos.y * scale - offset.y - 10 * scale - s->GetHeight()/2, s );
    }
    else
    {
        target->Blit( m_pos.x - offset.x - s->GetWidth()/2, m_pos.y - offset.y - 10 - s->GetHeight()/2, s );
    }
}

bool RipperShot::ShotHit( Entity* e )
{
    Vectorf n( e->GetPos() - GetPos() );
    n.Normalize();
    m_pos -= m_dir * m_speed;
    m_dir = 2.f * DotProduct( m_dir, n ) * n - m_dir;
    m_dir.Normalize();
    m_pos += m_dir * m_speed;

    m_blood = std::min( m_blood + 1, 2 );

    return HitCommon();
}

bool RipperShot::ShotHit( Obstacle* o )
{
    if( o->GetType() == Obstacle::Circle )
    {
        return ShotHit( static_cast<ObstacleCircle*>( o ) );
    }
    else
    {
        CLAW_ASSERT( o->GetType() == Obstacle::Rectangle );
        return ShotHit( static_cast<ObstacleRectangle*>( o ) );
    }
}

bool RipperShot::ShotHit( ObstacleCircle* o )
{
    if( GameManager::GetInstance()->CheckRicochetLastHit() )
    {
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_RICOCHET1 + g_rng.GetInt() % 7 ), GetPos() );
    }

    m_lastHit = NULL;

    Vectorf n( o->GetPos() - GetPos() );
    n.Normalize();
    m_pos -= m_dir * m_speed;
    m_dir = 2.f * DotProduct( m_dir, n ) * n - m_dir;
    m_dir.Normalize();
    m_pos += m_dir * m_speed;

    return HitCommon();
}

bool RipperShot::ShotHit( ObstacleRectangle* o )
{
    if( GameManager::GetInstance()->CheckRicochetLastHit() )
    {
        GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_RICOCHET1 + g_rng.GetInt() % 7 ), GetPos() );
    }

    m_lastHit = NULL;

    m_pos -= m_dir * m_speed;
    Vectorf n;
    Vectorf l = GetPos() - o->GetPos();
    float dp = DotProduct( l, o->GetEdge() );
    if( dp >= 0 && dp <= DotProduct( o->GetEdge(), o->GetEdge() ) )
    {
        if( DotProduct( l, o->GetPerp() ) < 0 )
        {
            n = 0.f - o->GetEdge();
        }
        else
        {
            n = o->GetEdge();
        }
    }
    else
    {
        if( dp < 0 )
        {
            n = 0.f - o->GetPerp();
        }
        else
        {
            n = o->GetPerp();
        }
    }
    n.Normalize();

    m_dir = 2.f * DotProduct( m_dir, n ) * n - m_dir;
    m_dir.Normalize();
    m_pos += m_dir * m_speed;

    return HitCommon();
}

bool RipperShot::HitCommon()
{
    m_durability--;
    m_speed *= 0.9;
    m_damage *= 0.9;

    bool desintegrate = m_durability <= 0;
    if( desintegrate )
    {
        GameManager::GetInstance()->AddSplatter( m_gfx[m_blood*3+m_frame], m_pos, 1, 0 );
    }
    return !desintegrate;
}

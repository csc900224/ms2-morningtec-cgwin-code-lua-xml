#include "MonstazAI/shot/Mine.hpp"
#include "MonstazAI/GameManager.hpp"

Mine::Mine( const Entity* owner, const Vectorf& pos, float damage, Shot::Type type, Claw::Surface* arm, GfxAsset* green, GfxAsset* red, char elements )
    : Shot( owner, pos, Vectorf( 1, 0 ), 0, damage, green, type, -1, 1, elements, false )
    , m_state( S_ARMING )
    , m_arm( new AnimSurfWrap( (Claw::AnimatedSurface*)arm, pos ) )
    , m_green( green )
    , m_red( red )
    , m_time( 0 )
{
}

Mine::~Mine()
{
}

void Mine::Update( float dt )
{
    m_time += dt;

    switch( m_state )
    {
    case S_ARMING:
        m_arm->Update( dt );
        if( m_time > 0.5f )
        {
            m_time = 0;
            m_state = S_LIVE;
            AudioManager::GetInstance()->Play3D( SFX_MINE_ARM2, m_pos );
        }
        break;
    case S_LIVE:
        break;
    case S_COUNTDOWN:
        if( m_time > 0.4f )
        {
            Explode();
            m_state = S_DEAD;
            m_life = 1;
        }
        break;
    case S_DEAD:
        break;
    default:
        break;
    }
}

void Mine::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    if( m_state == S_DEAD ) return;

    if( m_state == S_ARMING )
    {
        m_arm->Render( target, offset, scale );
    }
    else
    {
        GfxAsset* asset = m_red;
        if( m_state == S_LIVE )
        {
            if( m_time - (int)m_time > 0.9f )
            {
                asset = m_green;
            }
        }
        else
        {
            float t = m_time * 7.5f;
            if( t - (int)t > 0.5f )
            {
                asset = m_green;
            }
        }

        asset->Blit( target, m_pos.x * scale - offset.x, m_pos.y * scale - offset.y );
    }
}

void Mine::Countdown()
{
    CLAW_ASSERT( m_state == S_LIVE );

    m_state = S_COUNTDOWN;
    m_time = 0;

    AudioManager::GetInstance()->Play3D( SFX_MINE_DETECT, m_pos );
}

void Mine::Explode()
{
    Explosion::Params p( 0.2f, 1.5f, 400, 4 );
    if( GameManager::GetInstance()->GetStats()->CheckPerk( Perks::BigBang ) )
    {
        p.dpower *= 0.75f;
    }

    GameManager::GetInstance()->AddExplosionHole( GetPos() );
    GameManager::GetInstance()->GetExplosionManager()->Add( GetPos(), p, false, Shot::Mine );
}

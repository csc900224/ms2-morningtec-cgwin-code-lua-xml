#ifndef __MONSTAZ__SHOTMANAGER_HPP__
#define __MONSTAZ__SHOTMANAGER_HPP__

#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/shot/Electricity.hpp"
#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/particle/SmokeParticle.hpp"
#include "MonstazAI/particle/GlowParticle.hpp"
#include "MonstazAI/particle/FlareParticle.hpp"

struct ShotTrail
{
    ShotTrail( const Vectorf& _v1, const Vectorf& _v2, float _power, const Claw::Color& _color ) : v1( _v1 ), v2( _v2 ), d( v2 - v1 ), power( _power ), color( _color ) { d.Normalize(); }

    Vectorf v1;
    Vectorf v2;
    Vectorf d;
    float power;
    Claw::Color color;
};

struct ElectricDelay
{
    ElectricDelay( Entity* _owner, const Vectorf& _p, const Vectorf& _l, float _speed, float _damage, int _life, int _steps, bool _rage, Shot::Type _type, char _elements )
        : owner( _owner )
        , p( _p )
        , l( _l )
        , speed( _speed )
        , damage( _damage )
        , life( _life )
        , steps( _steps )
        , rage( _rage )
        , type( _type )
        , elements( _elements )
    {
    }

    Entity* owner;
    Vectorf p;
    Vectorf l;
    float speed;
    float damage;
    int life;
    int steps;
    bool rage;
    Shot::Type type;
    char elements;
};

class ShotManager : public Claw::RefCounter
{
public:
    typedef std::vector<Shot*>          Shots;
    typedef Shots::iterator             ShotsIt;
    typedef Shots::const_iterator       ShotsConstIt;

    LUA_DEFINITION( ShotManager );
    ShotManager( lua_State* L ) : m_particle( 0, NULL ) { CLAW_ASSERT( false ); }

    ShotManager( Claw::Lua* lua );
    ~ShotManager();

    static void InitEnum( Claw::Lua* lua );

    void Render( Claw::Surface* target, const Vectorf& offset );
    void RenderHeat( Claw::Surface* target, const Vectorf& offset );
    void Update( float dt );

    Shots& GetShots() { return m_ents; }

    void AddTrail( Shot::Type type, const Vectorf& v1, const Vectorf& v2, bool rage );

    void Add( Entity* owner, Vectorf p, const Vectorf l, float speed, float damage, Shot::Type type, int life, int steps, bool rage, char elements, bool delayElectricity = true );
    ShotsIt RemoveShot( ShotsIt it );

    Claw::Surface* GetShotGlow() const { return m_shotGlow; }

    int l_Add( lua_State* L );
    int l_ElectricityStep( lua_State* L );
    int l_ElectricitySpawn( lua_State* L );
    int l_GetShotPos( lua_State* L );

private:
    int GetWeaponId();

    Shots m_ents;
    std::vector<ElectricityPtr> m_electricity;
    std::vector<ElectricDelay> m_electricDelay;

    GfxAssetPtr m_shot[Shot::SHOT_COUNT][4];
    GfxAssetPtr m_shotRed[Shot::SHOT_COUNT];

    Claw::SurfacePtr m_electricityGfx[5];
    Claw::SurfacePtr m_electricityRageGfx[5];
    Claw::SurfacePtr m_flamerHeat[8];
    Claw::SurfacePtr m_vortexHeat;
    Claw::SurfacePtr m_shotGases;
    Claw::SurfacePtr m_shotGasesPlasma;
    Claw::SurfacePtr m_shotGasesChainsaw;
    Claw::SurfacePtr m_rocketShadow;
    Claw::SurfacePtr m_shadow;
    Claw::SurfacePtr m_saw[4][9];
    Claw::SurfacePtr m_sawRed[9];
    Claw::SurfacePtr m_lurkerGround;
    Claw::SurfacePtr m_lurker[5];
    Claw::SurfacePtr m_mineArm;
    Claw::SurfacePtr m_shotGlow;
    GfxAssetPtr m_mineGreen;
    GfxAssetPtr m_mineRed;
    ParticleFunctorPtr m_flareElectricity;
    ParticleFunctorPtr m_flareLaser;
    ParticleFunctorPtr m_shotFlare;
    bool m_dualPhase;
    bool m_shotFlareOk;

    std::vector<ShotTrail> m_trails;

    SmokeParticleFunctor m_particle;
};

typedef Claw::SmartPtr<ShotManager> ShotManagerPtr;

#endif

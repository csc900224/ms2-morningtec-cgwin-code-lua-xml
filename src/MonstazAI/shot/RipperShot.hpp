#ifndef __MONSTAZ__RIPPERSHOT_HPP__
#define __MONSTAZ__RIPPERSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"

class RipperShot : public Shot
{
public:
    RipperShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, Shot::Type type, int life, Claw::SurfacePtr* gfx, char elements );
    ~RipperShot();

    void Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    bool ShotHit( Entity* e );
    bool ShotHit( Obstacle* o );
    bool ShotHit( ObstacleCircle* o );
    bool ShotHit( ObstacleRectangle* o );

    int GetDurability() const { return m_durability; }

private:
    bool HitCommon();

    Claw::SurfacePtr* m_gfx;
    int m_frame;
    int m_blood;
    float m_time;
    int m_durability;
};

#endif

#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Matrix.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/shot/VortexShot.hpp"
#include "MonstazAI/RNG.hpp"

VortexShot::VortexShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Claw::Surface* heat, Shot::Type type, int life, char elements )
    : Shot( owner, pos, look, speed, damage, gfx, type, life, 1, elements, false )
    , m_time( 0 )
    , m_heat( heat )
    , m_angle( 0 )
    , m_wait( 0 )
{
}

VortexShot::~VortexShot()
{
}

void VortexShot::Update( float dt )
{
    Shot::Update( dt );

    m_angle += dt * 4 * M_PI;
    m_wait += dt;

    m_time += dt;
    while( m_time > 0.25f )
    {
        m_time -= 0.25f;
    }

    m_speed *= 0.975f;
}

void VortexShot::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    float len = m_len == 255 ? 1 : m_len / 255.f;

    Vectorf dir = m_dir;
    dir.Rotate( m_angle );
    float m[4] = { dir.y, -dir.x * len, -dir.x, -dir.y * len };
    m_gfx->GetSurface()->SetAlpha( 255 );
    Claw::TriangleEngine::Blit( target, m_gfx->GetSurface(), m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - 10 * scale, m, m_gfx->GetPivot() );

    float s = 3 - m_time * 8;
    dir = m_dir;
    dir.Rotate( -m_angle * 2 );
    m[0] = dir.y * s;
    m[1] = -dir.x * len * s;
    m[2] = -dir.x * s;
    m[3] = -dir.y * len * s;

    m_gfx->GetSurface()->SetAlpha( m_time * 1024 );
    Claw::TriangleEngine::Blit( target, m_gfx->GetSurface(), m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - 10 * scale, m, m_gfx->GetPivot() );
}

void VortexShot::RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_heat->SetAlpha( Claw::MinMax( (int)( ( m_wait - 0.15f ) * 256  ), 0, 64 ) );
    Claw::TriangleEngine::Blit( target, m_heat, ( m_pos.x * scale - offset.x ) / 4, ( m_pos.y * scale - offset.y - 20 ) / 4, 0, 1 + sin( m_time ) * 0.75f, Vectorf( m_heat->GetSize() / 2 ) );
}

bool VortexShot::ShotHit( Obstacle* o )
{
    if( o->GetType() == Obstacle::Circle )
    {
        ShotHit( static_cast<ObstacleCircle*>( o ) );
    }
    else
    {
        CLAW_ASSERT( o->GetType() == Obstacle::Rectangle );
        ShotHit( static_cast<ObstacleRectangle*>( o ) );
    }

    return true;
}

void VortexShot::ShotHit( ObstacleCircle* o )
{
    Vectorf n( o->GetPos() - GetPos() );
    n.Normalize();
    m_pos -= m_dir * m_speed;
    m_dir = 2.f * DotProduct( m_dir, n ) * n - m_dir;
    m_dir.Normalize();
    m_pos += m_dir * m_speed;
}

void VortexShot::ShotHit( ObstacleRectangle* o )
{
    m_pos -= m_dir * m_speed;
    Vectorf n;
    Vectorf l = GetPos() - o->GetPos();
    float dp = DotProduct( l, o->GetEdge() );
    if( dp >= 0 && dp <= DotProduct( o->GetEdge(), o->GetEdge() ) )
    {
        if( DotProduct( l, o->GetPerp() ) < 0 )
        {
            n = 0.f - o->GetEdge();
        }
        else
        {
            n = o->GetEdge();
        }
    }
    else
    {
        if( dp < 0 )
        {
            n = 0.f - o->GetPerp();
        }
        else
        {
            n = o->GetPerp();
        }
    }
    n.Normalize();

    m_dir = 2.f * DotProduct( m_dir, n ) * n - m_dir;
    m_dir.Normalize();
    m_pos += m_dir * m_speed;
}

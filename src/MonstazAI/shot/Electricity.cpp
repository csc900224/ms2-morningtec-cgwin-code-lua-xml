#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/shot/Electricity.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/missions/MissionManager.hpp"

ElectricityArc::ElectricityArc( Electricity* e, const Vectori& nodes, float power, int gfx )
    : Renderable( e->GetNode( nodes[0] ) )
    , m_e( e )
    , m_nodes( nodes )
    , m_power( power )
    , m_gfx( gfx )
{
}

void ElectricityArc::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    Claw::SurfacePtr s( m_e->GetGfx()[m_gfx] );
    s->SetAlpha( m_e->m_power );

    const Vectorf& v1 = m_e->GetNode( m_nodes[0] );
    const Vectorf& v2 = m_e->GetNode( m_nodes[1] );
    Vectorf v = ( v1 - v2 ) * 0.2f;

    float m[4] = { v.y * m_power, -v.x, -v.x * m_power, -v.y };
    Claw::TriangleEngine::BlitAdditive( target, s, v1.x * scale - offset.x, v1.y * scale - offset.y - 10 * scale, m, Vectorf( 4 * scale, 0 ) );
}


Electricity::Electricity( const Entity* owner, const Vectorf& pos, const Vectorf& look, Claw::SurfacePtr* gfx, float damage, bool rage, bool laser, char elements )
    : m_gfx( gfx )
    , m_damage( damage )
    , m_owner( owner )
    , m_power( 255 )
    , m_powerDecay( laser ? 12 : 128 )
    , m_nodesIdx( 0 )
    , m_linksIdx( 0 )
{
    int nodes;
    if( m_owner->GetType() == Entity::FloaterElectric )
    {
        nodes = 12 + g_rng.GetInt() % 8;
    }
    else
    {
        nodes = 40 + g_rng.GetInt() % 10;
        if( rage )
        {
            nodes += 10;
        }
    }

    m_nodes[m_nodesIdx++] = pos - look * 10.f;

    if( laser )
    {
        GenerateLaser( 0, nodes*4, look, m_owner, elements );
    }
    else
    {
        Generate( 0, nodes, look, m_owner, elements );
    }
}

void Electricity::Render( Claw::Surface* target, const Vectorf& offset, float scale )
{
    RenderableManager* rm = GameManager::GetInstance()->GetRenderableManager();

    for( int i=0; i<m_linksIdx; i++ )
    {
        rm->Add( m_arc + m_links[i] );
    }
}

void Electricity::RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale )
{
    for( int i=0; i<m_linksIdx; i++ )
    {
        const Vectorf& v1 = m_nodes[(m_arc+m_links[i])->GetNode( 0 )];
        const Vectorf& v2 = m_nodes[(m_arc+m_links[i])->GetNode( 1 )];

        target->DrawLine( ( v1.x * scale - offset.x ) / 4, ( v1.y * scale - offset.y - 10 * scale ) / 4, ( v2.x * scale - offset.x ) / 4, ( v2.y * scale - offset.y - 10 * scale ) / 4, Claw::MakeRGBA( 96 + (int)g_rng.GetInt() % 64, 96 + (int)g_rng.GetInt() % 64, 255, 48 ) );
    }
}

void Electricity::Generate( int idx, int num, const Vectorf& look, const Entity* owner, char elements )
{
    Entity** seg = GameManager::GetInstance()->GetSegmentTable();
    Entity* player = GameManager::GetInstance()->GetPlayer();

    while( num-- )
    {
        Entity* e = NULL;
        float dist = 70*70;
        const Vectorf& p = m_nodes[idx];

        Vectorf l( look );

        Obstacle* o = GameManager::GetInstance()->GetObstacleManager()->QueryCollision( p + l * 5.f );
        if( o && o->GetKind() == Obstacle::Regular )
        {
            l.Rotate( M_PI * 0.55f * ( g_rng.GetInt( 1 ) == 0 ? 1 : -1 ) );

            const char* oname = o->GetName();
            if( oname != NULL )
            {
                GameManager::GetInstance()->DamageObject( o, m_damage * 0.05f );
            }
        }
        else
        {
            l.Rotate( g_rng.GetDouble() - 0.5f );
        }

        int sx = GameManager::Segmentize( p.x );
        int sy = GameManager::Segmentize( p.y );
        int ex = sx;
        int ey = sy;

        if( l.x > 0 )
        {
            ex++;
        }
        else
        {
            sx--;
        }
        if( l.y > 0 )
        {
            ey++;
        }
        else
        {
            sy--;
        }

        for( int i=sx; i<=ex; i++ )
        {
            for( int j=sy; j<=ey; j++ )
            {
                Entity* s = *( seg + i + j*GameManager::STS );
                for( ; s ; s = s->m_nextSeg )
                {
                    if( s->GetClass() == GetOwner()->GetClass() || s->GetType() == Entity::FloaterElectric )
                        continue;
                    Vectorf d( p - s->GetPos() );
                    float l = DotProduct( d, d );
                    if( l < dist )
                    {
                        dist = l;
                        e = s;
                    }
                }
            }
        }

        if( e )
        {
            Vectorf d( p - e->GetPos() );
            d.Normalize();
            if( DotProduct( l, d ) < 0 )
            {
                float s = CrossProduct( l, d );
                if( s < 0 )
                {
                    l.Rotate( g_rng.GetDouble() * 0.5f + 0.6f );
                }
                else
                {
                    l.Rotate( -g_rng.GetDouble() * 0.5f - 0.6f );
                }
            }

            if( dist < 12*12 )
            {
                float damage = 0.05f * m_damage;
                if( e == player )
                {
                    damage *= 3;
                    GameManager::GetInstance()->PlayerUnderAttack();
                    if( GameManager::GetInstance()->GetShieldTime() > 0 )
                    {
                        damage = 0;
                    }
                    else
                    {
                        GameManager::GetInstance()->GetHud()->PlayerDamaged( damage );
                    }
                }
                if( damage != 0 )
                {
                    bool shield = ( GameManager::GetInstance()->GetEntityManager()->GetData( e->GetType() ).shield & ~elements ) != 0;
                    if( shield )
                    {
                        damage *= 0.1f;
                        e->ElementHit();
                    }

                    e->Hit( Shot::Electricity, elements, damage );
                    if( g_rng.GetDouble() < 0.1f )
                    {
                        GameManager::GetInstance()->GetParticleSystem()->Add( GameManager::GetInstance()->GetElectricityGlow()->operator()( p.x, p.y, 0, 0 ) );
                    }
                    if( g_rng.GetDouble() < 0.01f )
                    {
                        GameManager::GetInstance()->GenerateSplatter( e->GetPos(), 1 );
                    }
                }
            }
        }

        Vectorf v( p + l * 5.f );

        int newidx = m_nodesIdx;
        m_nodes[m_nodesIdx++] = v;

        new(m_arc+newidx-1) ElectricityArc( this, Vectori( idx, newidx ), 0.5f + num / 10.f, g_rng.GetInt()%4 );
        m_links[m_linksIdx++] = newidx - 1;
        idx = newidx;

        if( num%10 == 0 )
        {
            l.Rotate( g_rng.GetDouble() - 0.5f );
            Generate( newidx, num * 0.66f, l, owner, elements );
        }
    }
}

void Electricity::GenerateLaser( int idx, int num, const Vectorf& look, const Entity* owner, char elements )
{
    Entity** seg = GameManager::GetInstance()->GetSegmentTable();
    Entity* player = GameManager::GetInstance()->GetPlayer();

    float numMaxRecip = 2.f / num;

    GameManager::GetInstance()->GetParticleSystem()->Add( GameManager::GetInstance()->GetLaserGlow( 1 )->operator()( m_nodes[0].x, m_nodes[0].y - 5 * GameManager::GetInstance()->GetGameScale(), 0, 0 ) );

    while( num-- )
    {
        Entity* e = NULL;
        float dist = 12*12;
        const Vectorf& p = m_nodes[idx];
        Vectorf v( p + look * 5.f );

        if( GameManager::GetInstance()->CheckObstacleCollision( v ) == Obstacle::Regular )
        {
            GameManager::GetInstance()->GetParticleSystem()->Add( GameManager::GetInstance()->GetLaserGlow( 0 )->operator()( p.x, p.y - 5 * GameManager::GetInstance()->GetGameScale(), 0, 0 ) );
            return;
        }

        int sx = GameManager::Segmentize( p.x );
        int sy = GameManager::Segmentize( p.y );
        int ex = sx;
        int ey = sy;

        if( look.x > 0 )
        {
            ex++;
        }
        else
        {
            sx--;
        }
        if( look.y > 0 )
        {
            ey++;
        }
        else
        {
            sy--;
        }

        for( int i=sx; i<=ex; i++ )
        {
            for( int j=sy; j<=ey; j++ )
            {
                Entity* s = *( seg + i + j*GameManager::STS );
                for( ; s ; s = s->m_nextSeg )
                {
                    if( s->GetClass() == GetOwner()->GetClass() || s->GetType() == Entity::FloaterElectric )
                        continue;
                    Vectorf d( p - s->GetPos() );
                    float l = DotProduct( d, d );
                    if( l < dist )
                    {
                        dist = l;
                        e = s;
                    }
                }
            }
        }

        int newidx = m_nodesIdx;
        m_nodes[m_nodesIdx++] = v;

        new(m_arc+newidx-1) ElectricityArc( this, Vectori( idx, newidx ), 0.5f + num * numMaxRecip, 4 );
        m_links[m_linksIdx++] = newidx - 1;
        idx = newidx;

        if( e )
        {
            Vectorf d( p - e->GetPos() );
            d.Normalize();

            if( dist < 12*12 && ( e == player || owner == player ) )
            {
                float damage = m_damage;
                if( e == player )
                {
                    damage *= 3;
                    GameManager::GetInstance()->PlayerUnderAttack();
                    if( GameManager::GetInstance()->GetShieldTime() > 0 )
                    {
                        damage = 0;
                    }
                    else
                    {
                        GameManager::GetInstance()->GetHud()->PlayerDamaged( damage );
                    }
                }
                if( damage != 0 )
                {
                    e->Hit( Shot::Electricity, elements, damage );
                    GameManager::GetInstance()->GenerateSplatter( e->GetPos(), 1 );
                }
                GameManager::GetInstance()->GetParticleSystem()->Add( GameManager::GetInstance()->GetLaserGlow( 0 )->operator()( v.x, v.y - 5 * GameManager::GetInstance()->GetGameScale(), 0, 0 ) );
                return;
            }
        }
    }
}

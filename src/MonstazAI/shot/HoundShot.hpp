#ifndef __MONSTAZ__HOUNDSHOT_HPP__
#define __MONSTAZ__HOUNDSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"

class HoundShot : public Shot
{
public:
    HoundShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Type type, int life, int steps, char elements, bool rage );
    ~HoundShot();

    void Explode();

    void Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    float TimeLeft();

private:
    float m_time;
};

#endif

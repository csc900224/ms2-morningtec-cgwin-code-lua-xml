#ifndef __MONSTAZ__FLAMERSHOT_HPP__
#define __MONSTAZ__FLAMERSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"

class FlamerShot : public Shot
{
public:
    FlamerShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Claw::Surface* heat, Shot::Type type, int life, float rotation, char elements );
    ~FlamerShot();

    void Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    float m_scale;
    float m_rotation;
    float m_angle;

    Claw::SurfacePtr m_heat;
};

#endif

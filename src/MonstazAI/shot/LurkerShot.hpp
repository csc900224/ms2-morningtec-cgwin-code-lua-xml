#ifndef __MONSTAZ__LURKERSHOT_HPP__
#define __MONSTAZ__LURKERSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/shot/Shot.hpp"

class LurkerShot : public Shot
{
public:
    LurkerShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Type type, int life, int steps, char elements, bool rage, Claw::Surface* ground, Claw::SurfacePtr* spike );

    void Update( float dt );

private:
    Claw::SurfacePtr m_ground;
    Claw::SurfacePtr* m_spike;
    float m_time;
};

#endif

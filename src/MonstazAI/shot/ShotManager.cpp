#include "claw/base/AssetDict.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Vector.hpp"

#include "MonstazAI/shot/ShotManager.hpp"
#include "MonstazAI/shot/FlamerShot.hpp"
#include "MonstazAI/shot/GrenadeShot.hpp"
#include "MonstazAI/shot/EggShot.hpp"
#include "MonstazAI/shot/RipperShot.hpp"
#include "MonstazAI/shot/Mine.hpp"
#include "MonstazAI/shot/SpiralShot.hpp"
#include "MonstazAI/shot/HoundShot.hpp"
#include "MonstazAI/shot/VortexShot.hpp"
#include "MonstazAI/shot/LurkerShot.hpp"
#include "MonstazAI/AnimationSet.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"

#include <math.h>   // atan2

static const char* const s_shotAssets[] = {
    "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet_e.png.pivot@linear", "gfx/projectiles/bullet_f.png.pivot@linear", "gfx/projectiles/bullet_i.png.pivot@linear",
    "gfx/projectiles/shotgun.png.pivot@linear", "gfx/projectiles/shotgun_electric.png.pivot@linear", "gfx/projectiles/shotgun_fire.png.pivot@linear", "gfx/projectiles/shotgun_ice.png.pivot@linear",
    "gfx/projectiles/shotgun.png.pivot@linear", "gfx/projectiles/shotgun_electric.png.pivot@linear", "gfx/projectiles/shotgun_fire.png.pivot@linear", "gfx/projectiles/shotgun_ice.png.pivot@linear",
    "gfx/projectiles/minigun.png.pivot@linear", "gfx/projectiles/minigun.png.pivot@linear", "gfx/projectiles/minigun.png.pivot@linear", "gfx/projectiles/minigun.png.pivot@linear",
    "gfx/projectiles/plasma.ani.pivot", "gfx/projectiles/plasma.ani.pivot", "gfx/projectiles/plasma.ani.pivot", "gfx/projectiles/plasma.ani.pivot",
    "gfx/projectiles/rocket.ani.pivot", "gfx/projectiles/rocket_electric.ani.pivot", "gfx/projectiles/rocket_fire.ani.pivot", "gfx/projectiles/rocket_ice.ani.pivot",
    "gfx/projectiles/flamer.png.pivot@linear", "gfx/projectiles/flamer_electric.png.pivot@linear", "gfx/projectiles/flamer_fire.png.pivot@linear", "gfx/projectiles/flamer_ice.png.pivot@linear",
    "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet.png.pivot@linear",
    "gfx/projectiles/railgun.png.pivot@linear", "gfx/projectiles/railgun_electric.png.pivot@linear", "gfx/projectiles/railgun_fire.png.pivot@linear", "gfx/projectiles/railgun_ice.png.pivot@linear",
    "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet.png.pivot@linear", "gfx/projectiles/bullet.png.pivot@linear",
    "gfx/projectiles/dualizer_projectile.png.pivot@linear", "gfx/projectiles/dualizer_projectile_electric.png.pivot@linear", "gfx/projectiles/dualizer_projectile_fire.png.pivot@linear", "gfx/projectiles/dualizer_projectile_ice.png.pivot@linear",
    "gfx/projectiles/fish_projectile.ani.pivot", "gfx/projectiles/fish_projectile.ani.pivot", "gfx/projectiles/fish_projectile.ani.pivot", "gfx/projectiles/fish_projectile.ani.pivot",
    "gfx/projectiles/grenade.png.pivot@linear", "gfx/projectiles/grenade.png.pivot@linear", "gfx/projectiles/grenade.png.pivot@linear", "gfx/projectiles/grenade.png.pivot@linear",
    "gfx/projectiles/grenade.png.pivot@linear", "gfx/projectiles/grenade.png.pivot@linear", "gfx/projectiles/grenade.png.pivot@linear", "gfx/projectiles/grenade.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/projectiles/vortex.png.pivot@linear", "gfx/projectiles/vortex_electric.png.pivot@linear", "gfx/projectiles/vortex_fire.png.pivot@linear", "gfx/projectiles/vortex_ice.png.pivot@linear",
    "gfx/projectiles/fish_projectile_yellow.ani.pivot@linear", "gfx/projectiles/fish_projectile_yellow.ani.pivot@linear", "gfx/projectiles/fish_projectile_yellow.ani.pivot@linear", "gfx/projectiles/fish_projectile_yellow.ani.pivot@linear",
    "gfx/projectiles/sower_egg.png.pivot@linear", "gfx/projectiles/sower_egg.png.pivot@linear", "gfx/projectiles/sower_egg.png.pivot@linear", "gfx/projectiles/sower_egg.png.pivot@linear",
    "gfx/assets/trans.png.pivot@linear", "gfx/assets/trans.png.pivot@linear", "gfx/assets/trans.png.pivot@linear", "gfx/assets/trans.png.pivot@linear",
    "gfx/projectiles/deadspace.png.pivot@linear", "gfx/projectiles/deadspace.png.pivot@linear", "gfx/projectiles/deadspace.png.pivot@linear", "gfx/projectiles/deadspace.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear", "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/assets/trans.png.pivot@linear", "gfx/assets/trans.png.pivot@linear", "gfx/assets/trans.png.pivot@linear", "gfx/assets/trans.png.pivot@linear",
    "gfx/projectiles/magnum.png.pivot@linear", "gfx/projectiles/magnum_electric.png.pivot@linear", "gfx/projectiles/magnum_fire.png.pivot@linear", "gfx/projectiles/magnum_ice.png.pivot@linear",
};

static const char* const s_shotAssetsRed[] = {
    "gfx/projectiles/bullet_red.png.pivot@linear",
    "gfx/projectiles/shotgun_red.png.pivot@linear",
    "gfx/projectiles/shotgun_red.png.pivot@linear",
    "gfx/projectiles/minigun_red.png.pivot@linear",
    "gfx/projectiles/plasma_blue.ani.pivot",
    "gfx/projectiles/rocket_red.ani.pivot",
    "gfx/projectiles/flamer_red.png.pivot@linear",
    "gfx/projectiles/bullet.png.pivot@linear",
    "gfx/projectiles/railgun_red.png.pivot@linear",
    "gfx/projectiles/bullet.png.pivot@linear",
    "gfx/projectiles/dualizer_projectile_red.png.pivot@linear",
    "gfx/projectiles/grenade.png.pivot@linear",
    "gfx/projectiles/grenade.png.pivot@linear",
    "gfx/projectiles/grenade.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/projectiles/vortex_red.png.pivot@linear",
    "gfx/projectiles/fish_projectile_yellow.ani.pivot@linear",
    "gfx/projectiles/sower_egg.png.pivot@linear",
    "gfx/assets/trans.png.pivot@linear",
    "gfx/projectiles/deadspace.png.pivot@linear",
    "gfx/projectiles/hound_shot.png.pivot@linear",
    "gfx/assets/trans.png.pivot@linear",
    "gfx/projectiles/magnum_red.png.pivot@linear",
};

typedef int static_assert_shotSet[ sizeof( s_shotAssets ) / sizeof( const char* ) == Shot::SHOT_COUNT * 4 ? 1 : -1 ];
typedef int static_assert_shotSet[ sizeof( s_shotAssetsRed ) / sizeof( const char* ) == Shot::SHOT_COUNT ? 1 : -1 ];

const static Vectorf s_sowerSpitPos[8] = {
    Vectorf( 3, 15 ),
    Vectorf( 49, 0 ),
    Vectorf( 67, -30 ),
    Vectorf( 47, -69 ),
    Vectorf( -5, -75 ),
    Vectorf( -54, -54 ),
    Vectorf( -67, -22 ),
    Vectorf( -45, 5 )
};

const static Vectorf s_octobrainShootPos[8] = {
    Vectorf( -25, 3 ),
    Vectorf( 26, 5 ),
    Vectorf( 32, -5 ),
    Vectorf( 41, -24 ),
    Vectorf( 25, -43 ),
    Vectorf( -14, -41 ),
    Vectorf( -30, -35 ),
    Vectorf( -40, -17 ),
};

LUA_DECLARATION( ShotManager )
{
    METHOD( ShotManager, Add ),
    METHOD( ShotManager, ElectricityStep ),
    METHOD( ShotManager, ElectricitySpawn ),
    METHOD( ShotManager, GetShotPos ),
    {0,0}
};

ShotManager::ShotManager( Claw::Lua* lua )
    : m_mineArm( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/mine.ani" ) )
    , m_mineGreen( Claw::AssetDict::Get<GfxAsset>( "gfx/projectiles/mine_07.png.pivot@linear" ) )
    , m_mineRed( Claw::AssetDict::Get<GfxAsset>( "gfx/projectiles/mine_08.png.pivot@linear" ) )
    , m_particle( 256, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/geiser.png@linear" ) )
    , m_lurkerGround( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/spikegun_crack.ani" ) )
    , m_shotGlow( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/bullet_glow.png@linear" ) )
    , m_flareElectricity( new GlowParticleFunctor( 4096, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/flara5.png@linear" ) ) )
    , m_flareLaser( new FlareParticleFunctor( 880, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/flara16.png@linear" ) ) )
    , m_shotFlare( new FlareParticleFunctor( 2000, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/flareshot.png@linear" ), 0, 0.25f ) )
    , m_dualPhase( false )
    , m_shotFlareOk( true )
{
    Claw::Lunar<ShotManager>::Register( *lua );
    Claw::Lunar<ShotManager>::push( *lua, this );
    lua->RegisterGlobal( "ShotManager" );

    InitEnum( lua );

    for( int i=0; i<Shot::SHOT_COUNT; i++ )
    {
        for( int j=0; j<4; j++ )
        {
            m_shot[i][j].Reset( Claw::AssetDict::Get<GfxAsset>( s_shotAssets[i*4+j] ) );
        }
        m_shotRed[i].Reset( Claw::AssetDict::Get<GfxAsset>( s_shotAssetsRed[i] ) );
    }

    m_electricityGfx[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_01.png@linear" ) );
    m_electricityGfx[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_02.png@linear" ) );
    m_electricityGfx[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_03.png@linear" ) );
    m_electricityGfx[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_04.png@linear" ) );
    m_electricityGfx[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/laser.png@linear" ) );

    m_electricityRageGfx[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_2_01.png@linear" ) );
    m_electricityRageGfx[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_2_02.png@linear" ) );
    m_electricityRageGfx[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_2_03.png@linear" ) );
    m_electricityRageGfx[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_2_04.png@linear" ) );
    m_electricityRageGfx[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/laser.png@linear" ) );

    m_flamerHeat[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/explosion.png@linear" ) );
    m_flamerHeat[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/n_flamer01.png@linear" ) );
    m_flamerHeat[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/n_flamer03.png@linear" ) );
    m_flamerHeat[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/n_flamer04.png@linear" ) );
    m_flamerHeat[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/n_flamer07.png@linear" ) );
    m_flamerHeat[5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/n_flamer09.png@linear" ) );
    m_flamerHeat[6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/n_flamer14.png@linear" ) );
    m_flamerHeat[7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/special.png@linear" ) );
    for( int i=0; i<8; i++ )
    {
        m_flamerHeat[i]->SetAlpha( 96 );
    }

    m_vortexHeat.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/vortex.png@linear" ) );
    m_vortexHeat->SetAlpha( 64 );

    m_shotGases.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/shot/shot.ani" ) );
    m_shotGasesPlasma.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/shot/plasma_shot.ani" ) );
    m_rocketShadow.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/rocket_shadow.png@linear" ) );
    m_shadow.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/shadow.png@linear" ) );

    m_shotGasesChainsaw.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/chainsawspark.ani" ) );

    int blood = Claw::Registry::Get()->CheckInt( "/monstaz/settings/blood" );

    m_saw[0][0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_no_blood_01.png@linear" ) );
    m_saw[0][1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_no_blood_02.png@linear" ) );
    m_saw[0][2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_no_blood_03.png@linear" ) );
    if( blood == 1 )
    {
        m_saw[0][3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_blood_01.png@linear" ) );
        m_saw[0][4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_blood_02.png@linear" ) );
        m_saw[0][5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_blood_03.png@linear" ) );
        m_saw[0][6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_blood_01.png@linear" ) );
        m_saw[0][7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_blood_02.png@linear" ) );
        m_saw[0][8].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_blood_03.png@linear" ) );
    }
    else
    {
        m_saw[0][3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_greenblood_01.png@linear" ) );
        m_saw[0][4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_greenblood_02.png@linear" ) );
        m_saw[0][5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_greenblood_03.png@linear" ) );
        m_saw[0][6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_greenblood_01.png@linear" ) );
        m_saw[0][7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_greenblood_02.png@linear" ) );
        m_saw[0][8].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_greenblood_03.png@linear" ) );
    }

    for( int i=0; i<3; i++ )
    {
        char buf[128];
        sprintf( buf, "gfx/projectiles/saw_electric_0%i.png@linear", i+1 );
        m_saw[1][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        m_saw[1][i+3].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        m_saw[1][i+6].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        sprintf( buf, "gfx/projectiles/saw_fire_0%i.png@linear", i+1 );
        m_saw[2][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        m_saw[2][i+3].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        m_saw[2][i+6].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        sprintf( buf, "gfx/projectiles/saw_ice_0%i.png@linear", i+1 );
        m_saw[3][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        m_saw[3][i+3].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        m_saw[3][i+6].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }

    m_sawRed[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_no_blood_01_blue.png@linear" ) );
    m_sawRed[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_no_blood_02_blue.png@linear" ) );
    m_sawRed[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_no_blood_03_blue.png@linear" ) );
    if( blood == 1 )
    {
        m_sawRed[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_blood_01_blue.png@linear" ) );
        m_sawRed[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_blood_02_blue.png@linear" ) );
        m_sawRed[5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_blood_03_blue.png@linear" ) );
        m_sawRed[6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_blood_01_blue.png@linear" ) );
        m_sawRed[7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_blood_02_blue.png@linear" ) );
        m_sawRed[8].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_blood_03_blue.png@linear" ) );
    }
    else
    {
        m_sawRed[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_greenblood_01_blue.png@linear" ) );
        m_sawRed[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_greenblood_02_blue.png@linear" ) );
        m_sawRed[5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_greenblood_03_blue.png@linear" ) );
        m_sawRed[6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_greenblood_01_blue.png@linear" ) );
        m_sawRed[7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_greenblood_02_blue.png@linear" ) );
        m_sawRed[8].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/saw_more_greenblood_03_blue.png@linear" ) );
    }

    m_lurker[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/spike_1.ani" ) );
    m_lurker[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/spike_2.ani" ) );
    m_lurker[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/spike_3.ani" ) );
    m_lurker[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/spike_4.ani" ) );
    m_lurker[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/spike_5.ani" ) );
}

ShotManager::~ShotManager()
{
    ShotsIt it = m_ents.begin();
    while( it != m_ents.end() ) { it = RemoveShot( it ); }
}

void ShotManager::InitEnum( Claw::Lua* lua )
{
    lua->CreateEnumTable();
    lua->AddEnum( Shot::Bullet );
    lua->AddEnum( Shot::CombatShell );
    lua->AddEnum( Shot::Shell );
    lua->AddEnum( Shot::Minigun );
    lua->AddEnum( Shot::Plasma );
    lua->AddEnum( Shot::Rocket );
    lua->AddEnum( Shot::Flamer );
    lua->AddEnum( Shot::Electricity );
    lua->AddEnum( Shot::Railgun );
    lua->AddEnum( Shot::Saw );
    lua->AddEnum( Shot::Spiral );
    lua->AddEnum( Shot::FishGlop );
    lua->AddEnum( Shot::Grenade );
    lua->AddEnum( Shot::Mine );
    lua->AddEnum( Shot::HoundShot );
    lua->AddEnum( Shot::EnemyHit );
    lua->AddEnum( Shot::Laser );
    lua->AddEnum( Shot::Vortex );
    lua->AddEnum( Shot::SowerSpit );
    lua->AddEnum( Shot::SowerEgg );
    lua->AddEnum( Shot::Chainsaw );
    lua->AddEnum( Shot::DeadSpace );
    lua->AddEnum( Shot::Airstrike );
    lua->AddEnum( Shot::Lurker );
    lua->AddEnum( Shot::Magnum );
    lua->RegisterEnumTable( "ShotType" );
}

void ShotManager::Render( Claw::Surface* target, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();

    for( ShotsConstIt it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        Shot* s = *it;

        GameManager::GetInstance()->GetRenderableManager()->Add( s );

        if( s->GetType() == Shot::Rocket )
        {
            m_rocketShadow->SetAlpha( 64 );
            Vectorf p( s->GetPos() - s->GetDir() * 18.f );
            p *= scale;

            Claw::TriangleEngine::BlitAdditive( target, m_rocketShadow, p.x - offset.x, p.y - offset.y, 0, ( 2.5f + g_rng.GetDouble() ) * 3.f, Vectorf( m_rocketShadow->GetSize() / 2 ) );
        }
        else if( s->GetType() == Shot::Grenade || s->GetType() == Shot::SowerEgg )
        {
            GrenadeShot* g = (GrenadeShot*)s;
            float z = ( 64 - g->m_z ) / 64.f;
            if( z > 0 )
            {
                m_shadow->SetAlpha( z * 255 );
                if( scale != 1.0f )
                {
                    Claw::TriangleEngine::Blit( target, m_shadow, s->GetPos().x * scale - offset.x, s->GetPos().y * scale - offset.y, 0, 0.5f, Vectorf( m_shadow->GetSize() / 2 ) );
                }
            }
        }
    }

    if( scale != 1.0f )
    {
        for( std::vector<ShotTrail>::const_iterator it = m_trails.begin(); it != m_trails.end(); ++it )
        {
            Claw::Color c = it->color;
            c.SetA( int( it->power * 96 ) );
            target->DrawLine( it->v1.x * scale - offset.x, it->v1.y * scale - offset.y - 10 * scale, it->v2.x * scale - offset.x, it->v2.y * scale - offset.y - 10 * scale, c );
        }
    }
    else
    {
        for( std::vector<ShotTrail>::const_iterator it = m_trails.begin(); it != m_trails.end(); ++it )
        {
            Claw::Color c = it->color;
            c.SetA( int( it->power * 96 ) );
            target->DrawLine( it->v1.x - offset.x, it->v1.y - offset.y - 10, it->v2.x - offset.x, it->v2.y - offset.y - 10, c );
        }
    }

    if( !m_electricity.empty() )
    {
        for( std::vector<ElectricityPtr>::const_iterator it = m_electricity.begin(); it != m_electricity.end(); ++it )
        {
            (*it)->Render( target, offset, scale );
        }
    }
}

void ShotManager::RenderHeat( Claw::Surface* target, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();

    for( ShotsConstIt it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        (*it)->RenderHeat( target, offset, scale );
    }

    for( std::vector<ElectricityPtr>::iterator it = m_electricity.begin(); it != m_electricity.end(); ++it )
    {
        (*it)->RenderHeat( target, offset, scale );
    }

    for( std::vector<ShotTrail>::const_iterator it = m_trails.begin(); it != m_trails.end(); ++it )
    {
        target->DrawLine( ( it->v1.x * scale - offset.x ) / 4, ( it->v1.y * scale - offset.y - 10 * scale ) / 4, ( it->v2.x * scale - offset.x ) / 4, ( it->v2.y * scale - offset.y - 10 * scale ) / 4, Claw::MakeRGBA( 128 - int( it->d.x * 32 ), 128 - int( it->d.y * 32 ), 255, int( it->power * 192 )  ) );
    }
}

void ShotManager::Update( float dt )
{
    m_shotFlareOk = true;

    for( ShotsConstIt it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        (*it)->Update( dt );
    }

    for( int i=0; i<Shot::SHOT_COUNT; i++ )
    {
        for( int j=0; j<4; j++ )
        {
            m_shot[i][j]->Update( dt );
        }

        m_shotRed[i]->Update( dt );
    }

    std::vector<ShotTrail>::iterator it = m_trails.begin();
    while( it != m_trails.end() )
    {
        it->power -= dt * 5;
        if( it->power < 0 )
        {
            it = m_trails.erase( it );
        }
        else
        {
            ++it;
        }
    }
}

void ShotManager::AddTrail( Shot::Type type, const Vectorf& v1, const Vectorf& v2, bool rage )
{
    if( type == Shot::Railgun )
    {
        Claw::Color spiral;
        if( rage || GameManager::GetInstance()->IsWeaponBoostActive() )
        {
            spiral = Claw::MakeRGB( 192, 0, 0 );
        }
        else
        {
            spiral = Claw::MakeRGB( 0, 255, 255 );
        }

        Vectorf l = v2 - v1;
        float len = l.Normalize() / 7;
        Vectorf n( l.y, -l.x );

        Vectorf prev = v1;

        for( int i=1; i<=7; i++ )
        {
            Vectorf next = v1 + l * len * float( i );
            m_trails.push_back( ShotTrail( prev + n * (float)sin( (i-1) * M_PI / 3.5f ) * 4.f, next + n * (float)sin( i * M_PI / 3.5f ) * 4.f, 2, spiral ) );
            m_trails.push_back( ShotTrail( prev - n * (float)cos( (i-1) * M_PI / 3.5f ) * 4.f, next - n * (float)cos( i * M_PI / 3.5f ) * 4.f, 2, spiral ) );
            m_trails.push_back( ShotTrail( prev - n * (float)sin( ( ( M_PI / 2 ) + (i-1) * M_PI ) / 3.5f ) * 4.f, next - n * (float)sin( ( ( M_PI / 2 ) + i * M_PI ) / 3.5f ) * 4.f, 2, spiral ) );
            prev = next;
        }

        m_trails.push_back( ShotTrail( v1, v2, 2.5f, Claw::MakeRGB( 255, 255, 255 ) ) );
    }
    else if( type == Shot::Rocket )
    {
        GameManager::GetInstance()->GetParticleSystem()->Add( m_particle( v1.x, v1.y, 0, 0 ) );
    }
    else if( type != Shot::Chainsaw && type != Shot::DeadSpace && type != Shot::Vortex )
    {
        if( rage || GameManager::GetInstance()->IsWeaponBoostActive() )
        {
            m_trails.push_back( ShotTrail( v1, v2, 1, Claw::MakeRGB( 192, 0, 0 ) ) );
        }
        else
        {
            m_trails.push_back( ShotTrail( v1, v2, 1, Claw::MakeRGB( 255, 255, 255 ) ) );
        }
    }
}

ShotManager::ShotsIt ShotManager::RemoveShot( ShotManager::ShotsIt it )
{
    delete *it;
    return m_ents.erase( it );
}

void ShotManager::Add( Entity* owner, Vectorf p, const Vectorf l, float speed, float damage, Shot::Type type, int life, int steps, bool rage, char elements, bool delayElectricity )
{
    if( owner == GameManager::GetInstance()->GetPlayer() && type != Shot::Airstrike )
    {
        GameManager::GetInstance()->ShotFlash();
    }

    if( ( type == Shot::Electricity || type == Shot::Laser ) && delayElectricity )
    {
        m_electricDelay.push_back( ElectricDelay( owner, p, l, speed, damage, life, steps, rage, type, elements ) );
        return;
    }

    if( type != Shot::HoundShot )
    {
        if( type == Shot::Mine )
        {
            if( owner )
            {
                p -= owner->GetLook() * Vectorf( 30.f, 20.f );
            }
            else
            {
                p -= GameManager::GetInstance()->GetPlayer()->GetLook() * Vectorf( 30.f, 20.f );
            }
        }
        else if( type == Shot::SowerSpit )
        {
            if( owner )
            {
                p += s_sowerSpitPos[ owner->GetLastDir() ];
                CLAW_ASSERT( owner->GetLastDir() < 8 );
            }
        }
        else if( type == Shot::Spiral && owner && (owner->GetType() == Entity::OctobrainBoss || owner->GetType() == Entity::OctobrainBossClone) )
        {
            p += s_octobrainShootPos[ owner->GetLastDir() ];
            CLAW_ASSERT( owner->GetLastDir() < 8 );
        }
        else if( type == Shot::SowerEgg )
        {
            Vectorf offset( g_rng.GetDouble() * 2 - 1, g_rng.GetDouble() * 2 - 1 );
            offset *= 20;
            p += offset - AnimationSet::GetShotPos32( AnimationSet::TranslateFor32( owner->GetLook() ), 0 );
        }
        else if( type == Shot::Laser && owner->GetType() == Entity::Crab )
        {
            static const Vectorf cp[8] = {
                Vectorf( 0, -8 ),
                Vectorf( 11, -11 ),
                Vectorf( 16, -18 ),
                Vectorf( 12, -15 ),
                Vectorf( 0, -29 ),
                Vectorf( -11, -26 ),
                Vectorf( -16, -17 ),
                Vectorf( -11, -11 ),
            };
            p += cp[AnimationSet::TranslateFor8( owner->GetLook() )];
        }
        else if( type != Shot::Grenade )
        {
            // Move shot a little bit in looking direction
            if( owner )
            {
                p += AnimationSet::GetShotPos32( AnimationSet::TranslateFor32( owner->GetLook() ), GetWeaponId() );
            }
            // Assume player - shots without owner are used to kill everybody and may be only by player
            else
            {
                p += AnimationSet::GetShotPos32( AnimationSet::TranslateFor32( GameManager::GetInstance()->GetPlayer()->GetLook() ), GetWeaponId() );
            }
        }
    }

    switch( type )
    {
    case Shot::Chainsaw:
        if( g_rng.GetDouble() < 0.15f )
        {
            GameManager::GetInstance()->AddAnimation( m_shotGasesChainsaw, p, l );
        }
        break;
    case Shot::Electricity:
    case Shot::Laser:
    case Shot::Flamer:
    case Shot::Mine:
    case Shot::FishGlop:
    case Shot::SowerSpit:
    case Shot::SowerEgg:
    case Shot::Airstrike:
        break;
    case Shot::Plasma:
        GameManager::GetInstance()->AddAnimation( m_shotGasesPlasma, p, l );
        break;
    case Shot::Grenade:
        GameManager::GetInstance()->NukeAnim();
        break;
    default:
        GameManager::GetInstance()->AddAnimation( m_shotGases, p, l );
        if( m_shotFlareOk )
        {
            GameManager::GetInstance()->GetParticleSystem()->Add( (*m_shotFlare)( p.x, p.y, 0, 0 ) );
            m_shotFlareOk = false;
        }
        break;
    }

    if( type != Shot::Grenade && type != Shot::Mine && type != Shot::SowerEgg )
    {
        p += l * speed * ( (float)g_rng.GetDouble() - 0.5f ) + Vectorf( 0, 10 );
    }

    int e = 0;
    if( elements & 0x1 ) { if( g_rng.GetDouble() < 0.075f ) e = 1; }
    if( elements & 0x2 ) { if( g_rng.GetDouble() < 0.075f ) e = 2; }
    if( elements & 0x4 ) { if( g_rng.GetDouble() < 0.075f ) e = 3; }

    Shot* shot;
    switch( type )
    {
    case Shot::Flamer:
        if( rage )
        {
            shot = new FlamerShot( owner, p, l, speed, damage, m_shotRed[type], m_flamerHeat[g_rng.GetInt()%8], type, life, g_rng.GetDouble() * M_PI * 2 - M_PI, elements );
        }
        else
        {
            shot = new FlamerShot( owner, p, l, speed, damage, m_shot[type][e], m_flamerHeat[g_rng.GetInt()%8], type, life, g_rng.GetDouble() * M_PI * 2 - M_PI, elements );
        }
        break;
    case Shot::Electricity:
        GameManager::GetInstance()->GetParticleSystem()->Add( (*m_flareElectricity)( p.x, p.y - 5 * GameManager::GetInstance()->GetGameScale(), 0, 0 ) );
        if( rage )
        {
            m_electricity.push_back( ElectricityPtr( new Electricity( owner, p, l, m_electricityRageGfx, damage, rage, false, elements ) ) );
        }
        else
        {
            m_electricity.push_back( ElectricityPtr( new Electricity( owner, p, l, m_electricityGfx, damage, rage, false, elements ) ) );
        }
        return;
    case Shot::Laser:
        GameManager::GetInstance()->GetParticleSystem()->Add( (*m_flareLaser)( p.x, p.y - 5 * GameManager::GetInstance()->GetGameScale(), 0, 0 ) );
        m_electricity.push_back( ElectricityPtr( new Electricity( owner, p, l, m_electricityGfx, damage, rage, true, elements ) ) );
        return;
    case Shot::Saw:
        if( rage )
        {
            shot = new RipperShot( owner, p, l, speed, damage, type, life, m_sawRed, elements );
        }
        else
        {
            shot = new RipperShot( owner, p, l, speed, damage, type, life, m_saw[e], elements );
        }
        break;
    case Shot::Grenade:
        shot = new GrenadeShot( owner, p, l, speed, damage, m_shot[type][e], type, life, g_rng.GetDouble() * M_PI * 16 - M_PI * 8, elements );
        break;
    case Shot::SowerEgg:
        shot = new EggShot( owner, p, l, speed, damage, m_shot[type][e], type, life, g_rng.GetDouble() * M_PI * 16 - M_PI * 8, elements );
        break;
    case Shot::Mine:
        shot = new Mine( owner, p, damage, type, m_mineArm, m_mineGreen, m_mineRed, elements );
        break;
    case Shot::Spiral:
        if( rage )
        {
            shot = new SpiralShot( owner, p, l, speed, damage, m_shotRed[type], type, life, false, false, elements );
            m_ents.push_back( shot );
            shot = new SpiralShot( owner, p, l, speed, damage, m_shotRed[type], type, life, true, false, elements );
            m_ents.push_back( shot );
            shot = new SpiralShot( owner, p, l, speed, damage, m_shotRed[type], type, life, false, true, elements );
        }
        else
        {
            shot = new SpiralShot( owner, p, l, speed, damage, m_shot[type][e], type, life, false, false, elements );
            m_ents.push_back( shot );
            shot = new SpiralShot( owner, p, l, speed, damage, m_shot[type][e], type, life, true, false, elements );
            m_ents.push_back( shot );
            shot = new SpiralShot( owner, p, l, speed, damage, m_shot[type][e], type, life, false, true, elements );
        }
        break;
    case Shot::HoundShot:
    case Shot::Airstrike:
        shot = new HoundShot( owner, p, l, speed, damage, m_shotRed[type], type, life, steps, rage, elements );
        break;
    case Shot::Vortex:
        if( rage )
        {
            shot = new VortexShot( owner, p, l, speed, damage, m_shotRed[type], m_vortexHeat, type, life, elements );
        }
        else
        {
            shot = new VortexShot( owner, p, l, speed, damage, m_shot[type][e], m_vortexHeat, type, life, elements );
        }
        break;
    case Shot::DeadSpace:
        {
        int target = rage ? 15 : 10;
        for( int i=1; i<target; i++ )
        {
            Vectorf pp = p + Vectorf( -l.y, l.x ) * (float)i * 2.f;
            shot = new Shot( owner, pp, l, speed, damage, m_shot[type][e], type, life, steps, elements, rage );
            m_ents.push_back( shot );
            pp = p - Vectorf( -l.y, l.x ) * (float)i * 2.f;
            shot = new Shot( owner, pp, l, speed, damage, m_shot[type][e], type, life, steps, elements, rage );
            m_ents.push_back( shot );
        }
        shot = new Shot( owner, p, l, speed, damage, m_shot[type][e], type, life, steps, elements, rage );
        }
        break;
    case Shot::Lurker:
        shot = new LurkerShot( owner, p, l, speed, damage, m_shot[type][e], type, life, steps, elements, rage, m_lurkerGround, m_lurker );
        break;
    default:
        if( rage )
        {
            shot = new Shot( owner, p, l, speed, damage, m_shotRed[type], type, life, steps, elements, rage );
        }
        else
        {
            shot = new Shot( owner, p, l, speed, damage, m_shot[type][e], type, life, steps, elements, rage );
        }
        break;
    }
    m_ents.push_back( shot );
}

int ShotManager::l_Add( lua_State* L )
{
    Claw::Lua lua( L );

    Entity* owner = Claw::Lunar<Entity>::check( L, 1 );
    float px = lua.CheckNumber( 2 );
    float py = lua.CheckNumber( 3 );
    float lx = lua.CheckNumber( 4 );
    float ly = lua.CheckNumber( 5 );
    float speed = lua.CheckNumber( 6 );
    float damage = lua.CheckNumber( 7 );
    Shot::Type type = lua.CheckEnum<Shot::Type>( 8 );
    int life = lua.CheckNumber( 9 );
    int steps = lua.CheckNumber( 10 );
    bool rage = lua.CheckBool( 11 );
    char elements = lua.CheckNumber( 12 );

    Vectorf p( px, py );
    Vectorf l( lx, ly );

    Add( owner, p, l, speed, damage, type, life, steps, rage, elements );

    return 0;
}

int ShotManager::l_ElectricityStep( lua_State* L )
{
    GameManager::GetInstance()->PlayerUnderAttack( false );
    std::vector<ElectricityPtr>::iterator it = m_electricity.begin();
    while( it != m_electricity.end() )
    {
        (*it)->m_power -= (*it)->m_powerDecay;
        if( (*it)->m_power <= 0 )
        {
            it = m_electricity.erase( it );
        }
        else
        {
            ++it;
        }
    }
    return 0;
}

int ShotManager::l_ElectricitySpawn( lua_State* L )
{
    for( std::vector<ElectricDelay>::const_iterator it = m_electricDelay.begin(); it != m_electricDelay.end(); ++it )
    {
        Add( it->owner, it->p, it->l, it->speed, it->damage, it->type, it->life, it->steps, it->rage, it->elements, false );
    }
    m_electricDelay.clear();
    return 0;
}

int ShotManager::l_GetShotPos( lua_State* L )
{
    Claw::Lua lua( L );

    const Vectorf& v = AnimationSet::GetShotPos32( AnimationSet::TranslateFor32( GameManager::GetInstance()->GetPlayer()->GetLook() ), GetWeaponId() );
    lua.PushNumber( v.x );
    lua.PushNumber( v.y );

    return 2;
}

int ShotManager::GetWeaponId()
{
    int weapon = GameManager::GetInstance()->GetWeaponSet();
    if( weapon == 2 )
    {
        if( m_dualPhase )
        {
            weapon = -1;
        }
        m_dualPhase = !m_dualPhase;
    }
    return weapon;
}

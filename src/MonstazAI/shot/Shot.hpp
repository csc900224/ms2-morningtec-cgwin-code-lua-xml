#ifndef __MONSTAZ__SHOT_HPP__
#define __MONSTAZ__SHOT_HPP__

#include "claw/base/Errors.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/Renderable.hpp"

// Forward declarations
class Entity;

class Shot : public Renderable
{
public:
    enum Type
    {
        Bullet,
        CombatShell,
        Shell,
        Minigun,
        Plasma,
        Rocket,
        Flamer,
        Electricity,
        Railgun,
        Saw,
        Spiral,
        FishGlop,
        Grenade,
        Mine,
        HoundShot,
        EnemyHit,
        Laser,
        Vortex,
        SowerSpit,
        SowerEgg,
        Chainsaw,
        DeadSpace,
        Airstrike,
        Lurker,
        Magnum,

        SHOT_COUNT
    };

    Shot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Type type, int life, int steps, char elements, bool rage );
    virtual ~Shot();

    virtual void Update( float dt );
    virtual void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    virtual void RenderHeat( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    const Vectorf& GetDir() const         { return m_dir; }
    Vectorf GetTangent() const            { return Vectorf( m_dir.y, -m_dir.x ); }

    void SetDir( const Vectorf dir )      { m_dir = dir; }

    float GetSpeed() const                      { return m_speed; }
    float GetDamage() const                     { return m_damage; }
    Type GetType() const                        { return (Type)m_type; }
    void SetLife( int life )                    { CLAW_ASSERT( life >= -128 && life <= 127 ); m_life = life; }
    int GetLife() const                         { return m_life; }
    int GetSteps() const                        { return m_steps & 0x7F; }
    bool IsRage() const                         { return ( m_steps & 0x80 ) != 0; }
    const Entity* GetOwner() const              { return m_owner; }
    Claw::UInt8 GetElements() const             { return m_elements; }

    int GetKills() const                        { return m_kills; }
    void IncKills()                             { m_kills = std::min( 255, m_kills + 1 ); }

    Entity* GetLastHit() const                  { return m_lastHit; }
    void SetLastHit( Entity* e )                { m_lastHit = e; }

protected:
    Vectorf m_dir;

    float m_speed;
    float m_damage;

    Entity* m_lastHit;

    GfxAssetPtr m_gfx;
    const Entity* m_owner;

    Claw::UInt8 m_len;
    Claw::UInt8 m_kills;
    Claw::UInt8 m_type;
    Claw::Int8 m_life;
    Claw::UInt8 m_steps;    // high bit is rage
    Claw::UInt8 m_elements;
};

#endif

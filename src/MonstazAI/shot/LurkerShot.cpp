#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/shot/LurkerShot.hpp"

LurkerShot::LurkerShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Type type, int life, int steps, char elements, bool rage, Claw::Surface* ground, Claw::SurfacePtr* spike )
    : Shot( owner, pos, look, speed, damage, gfx, type, life, steps, elements, rage )
    , m_ground( ground )
    , m_spike( spike )
    , m_time( 0 )
{
}

void LurkerShot::Update( float dt )
{
    Shot::Update( dt );
    m_time -= dt;

    if( m_time < 0 )
    {
        m_time = 0.065f;

        Vectorf tangent = GetTangent();

        static const float dmul[4] = { 25, 17.5f, 17.5f, 25 };
        static const float tmul[4] = { 7.5f, 22.5f, -22.5f, -7.5f };

        for( int i=0; i<4; i++ )
        {
            Vectorf pos = m_pos + Vectorf( ( g_rng.GetDouble() - 0.5f ) * 5, ( g_rng.GetDouble() - 0.5f ) * 5 ) + tangent * tmul[i] + m_dir * dmul[i];
            GameManager::GetInstance()->AddAnimation( m_ground, pos, m_dir, true );
            GameManager::GetInstance()->AddAnimation( m_spike[g_rng.GetInt()%5], pos, false );
        }

        AudioManager::GetInstance()->Play3D( AudioSfx( SFX_SPIKES_1 + ( g_rng.GetInt() % 3 ) ), m_pos );
    }
}

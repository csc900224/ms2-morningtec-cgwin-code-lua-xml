#include "MonstazAI/shot/SpiralShot.hpp"
#include "MonstazAI/GameManager.hpp"

SpiralShot::SpiralShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Shot::Type type, int life, bool revPhase, bool center, char elements )
    : Shot( owner, pos, look, speed, damage, gfx, type, life, 1, elements, false )
    , m_perp( look.y, -look.x )
    , m_time( revPhase ? M_PI : 0 )
    , m_vec( 0 )
    , m_center( center )
{
    m_time += 1/120.f;
}

SpiralShot::~SpiralShot()
{
}

void SpiralShot::Step( float dt )
{
    if( !m_center )
    {
        m_time += dt * 20;
        m_vec = sin( m_time ) * 4;
        m_pos += m_perp * m_vec;
    }
}

void SpiralShot::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_gfx->BlitAdditive( target, m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - 10 * scale );
}

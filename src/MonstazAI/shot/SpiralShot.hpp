#ifndef __MONSTAZ__SPIRALSHOT_HPP__
#define __MONSTAZ__SPIRALSHOT_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/shot/Shot.hpp"

class SpiralShot : public Shot
{
public:
    SpiralShot( const Entity* owner, const Vectorf& pos, const Vectorf& look, float speed, float damage, GfxAsset* gfx, Shot::Type type, int life, bool revPhase, bool center, char elements );
    ~SpiralShot();

    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    void Step( float dt );

private:
    Vectorf m_perp;
    float m_time;
    float m_vec;
    float m_stepTime;
    bool m_center;
};

#endif

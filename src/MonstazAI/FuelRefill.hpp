#ifndef __MONSTAZ__FUELREFILL_HPP__
#define __MONSTAZ__FUELREFILL_HPP__

#include "claw/base/SmartPtr.hpp"

class FuelRefill : public Claw::RefCounter
{
public:
    FuelRefill();
    ~FuelRefill();

    void Tick();

private:
    int GetFuel() const;
    void SetFuel( int val ) const;
    int GetMaxFuel() const;

    bool m_full;
    Claw::UInt32 m_last;
};

typedef Claw::SmartPtr<FuelRefill> FuelRefillPtr;

#endif

#ifndef __MONSTAZ_HEAT_HPP__
#define __MONSTAZ_HEAT_HPP__

#include "claw/graphics/Surface.hpp"
#include "claw/graphics/OpenGLShader.hpp"

class Heat : public Claw::RefCounter
{
public:
    Heat();
    void Render( Claw::Surface* src, Claw::Surface* dst, float x, float y, const Claw::Rect& clipRect );

    void SetHeatTexture( Claw::Surface* heat ) { m_heat = heat; }

private:
    Claw::Surface* m_heat;
    Claw::OpenGLShader m_shader;
};

typedef Claw::SmartPtr<Heat> HeatPtr;

#endif

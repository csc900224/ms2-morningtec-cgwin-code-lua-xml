#include "MonstazAI/TutorialManager.hpp"

#include "guif/Screen.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"

LUA_DECLARATION( TutorialManager )
{
    METHOD( TutorialManager, GetCurrentChapter ),
    METHOD( TutorialManager, IsTaskActive ),
    METHOD( TutorialManager, IsTaskCompleted ),
    METHOD( TutorialManager, SetTaskActive ),
    METHOD( TutorialManager, SetTaskCompleted ),
    METHOD( TutorialManager, OnEnemyWaveKilled ),
    {0,0}
};

TutorialManager* TutorialManager::s_instance = NULL;

TutorialManager::TutorialManager()
    : m_chapter( TutorialChapter::None )
    , m_activeTasks( 0 )
    , m_completedTasks( 0 )
    , m_gameLua( NULL )
    , m_gameMenuLua( NULL )
    , m_mainMenuLua( NULL )
    , m_weaponChangeAllowed( true )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;
}

TutorialManager::~TutorialManager()
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;
}

void TutorialManager::Init()
{
    int chapter = -1;
    if ( Claw::Registry::Get()->Get( "/monstaz/tutorial/chapter", chapter ) )
    {
        m_chapter = static_cast<TutorialChapter::Id>( chapter );
    }
}

void TutorialManager::Skip()
{
    if ( !IsActive() )
    {
        return;
    }

    m_chapter = TutorialChapter::None;

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TUTORIAL_COMPLETED );

    Claw::Registry::Get()->Set( "/monstaz/tutorial/chapter", m_chapter );

    m_activeTasks = 0;
    m_completedTasks = 0;

    if ( m_gameMenuLua )
    {
        Claw::Registry::Get()->Set( "/maps/current", 2 );

        m_gameMenuLua->PushBool( false );
        m_gameMenuLua->RegisterGlobal( "TutorialActive" );

        m_gameMenuLua->Call( "TutorialTooltipRemoveAll", 0, 0 );

        m_gameLua->Call( "GameWon", 0, 0 );
    }
    else if ( m_mainMenuLua )
    {
        m_mainMenuLua->PushBool( false );
        m_mainMenuLua->RegisterGlobal( "TutorialActive" );

        m_mainMenuLua->Call( "TutorialTooltipRemoveAll", 0, 0 );

        m_mainMenuLua->PushNumber( 2 );
        m_mainMenuLua->Call( "MapShow", 1, 0 );
    }

    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
}

void TutorialManager::SetTaskActive( TutorialTask::Id task )
{
    if( (m_activeTasks & task) != 0 ) return;

    m_activeTasks |= task;
    m_completedTasks &= ~task;

    if ( m_chapter == TutorialChapter::Intro )
    {
        if ( task == TutorialTask::IntroPlayerMove )
        {
            TouchControls* tc = GameManager::GetInstance()->GetTouchControls();
            const TouchControls::TouchData& p = tc->GetTouchData( 0 );
            const float offset = 55*GameManager::GetGameScale();

            ShowTooltip( "/Tutorial", "IntroPlayerMove", "TEXT_TUTORIAL_01", p.center.x, p.center.y - offset, TutorialAnchor::Bottom, 1 );
        }
        else if ( task == TutorialTask::IntroPlayerShot )
        {
            TouchControls* tc = GameManager::GetInstance()->GetTouchControls();
            const TouchControls::TouchData& p = tc->GetTouchData( 1 );
            const float offset = 55*GameManager::GetGameScale();

            ShowTooltip( "/Tutorial", "IntroPlayerShot", "TEXT_TUTORIAL_02", p.center.x, p.center.y - offset, TutorialAnchor::Bottom, 1 );
        }
        else if ( task == TutorialTask::IntroKillFirstWave )
        {
            PakManager::GetInstance()->Require( PakManager::SetTutorialMenu );

            // Spawn enemy
            m_gameLua->Call( "Tutorial01", 0, 0 );

            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "IntroKillFirstWave", "TEXT_TUTORIAL_04", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 0.5f, false, 5.0f );
        }
        else if ( task == TutorialTask::IntroSummary )
        {
            m_gameMenuLua->Call( "TutorialIntroSummary", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Shop )
    {
        if ( task == TutorialTask::ShopEnter )
        {
            m_mainMenuLua->Call( "TutorialShopEnter", 0, 0 );
        }
        else if ( task == TutorialTask::ShopBuyShotgun )
        {
            m_mainMenuLua->Call( "TutorialShopBuyShotgun", 0, 0 );
        }
        else if ( task == TutorialTask::ShopBuyShotgunPopup )
        {
            m_mainMenuLua->Call( "TutorialShopBuyShotgunPopup", 0, 0 );
        }
        else if ( task == TutorialTask::ShopExit )
        {
            m_mainMenuLua->Call( "TutorialShopExit", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Premission )
    {
        if ( task == TutorialTask::PremissionSelect )
        {
            m_mainMenuLua->Call( "TutorialPremissionSelect", 0, 0 );
        }
        else if ( task == TutorialTask::PremissionSecondWeaponSlot )
        {
            m_mainMenuLua->Call( "TutorialPremissionSecondWeaponSlot", 0, 0 );
        }
        else if ( task == TutorialTask::PremissionEquipShotgun )
        {
            m_mainMenuLua->Call( "TutorialPremissionEquipShotgun", 0, 0 );
        }
        else if ( task == TutorialTask::PremissionFirstWeaponSlot )
        {
            m_mainMenuLua->Call( "TutorialPremissionFirstWeaponSlot", 0, 0 );
        }
        else if ( task == TutorialTask::PremissionUpgradeSmg )
        {
            m_mainMenuLua->Call( "TutorialPremissionUpgradeSmg", 0, 0 );
        }
        else if ( task == TutorialTask::PremissionUpgradeSmgBack )
        {
            m_mainMenuLua->Call( "TutorialPremissionUpgradeSmgBack", 0, 0 );
        }
        else if ( task == TutorialTask::PremissionPlay )
        {
            m_mainMenuLua->Call( "TutorialPremissionPlay", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Gameplay )
    {
        if ( task == TutorialTask::GameplayChangeWeapon )
        {
            TouchControls* tc = GameManager::GetInstance()->GetTouchControls();
            if ( tc )
            {
                tc->ShowFinger( true );

                Vectorf p = tc->GetSwapPosition();
                Vectori s = tc->GetSwapSize();
                ShowTooltip( "/Tutorial", "GameplayChangeWeapon", "TEXT_TUTORIAL_06", p.x, p.y + s.y/2, TutorialAnchor::Right, 1, true );
            }
            else
            {
                const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
                ShowTooltip( "/Tutorial", "GameplayChangeWeapon", "TEXT_TUTORIAL_06", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 1, false );
            }
        }
        else if ( task == TutorialTask::GameplayKillEnemies )
        {
            // Spawn enemies
            m_gameLua->Call( "Tutorial01", 0, 0 );

            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "GameplayKillEnemies", "TEXT_TUTORIAL_07", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 1, false, 5.0f );
        }
        else if ( task == TutorialTask::GameplayRage )
        {
            GameManager::GetInstance()->SetMenuActive( true );
            m_gameMenuLua->Call( "TutorialGameplayRage", 0, 0 );
        }
        else if ( task == TutorialTask::GameplaySelectPerk )
        {
            m_gameMenuLua->Call( "TutorialGameplaySelectPerk", 0, 0 );
        }
        else if ( task == TutorialTask::GameplayKillMoreEnemies )
        {
            // Spawn more enemies
            m_gameLua->Call( "Tutorial02", 0, 0 );
        }
        else if ( task == TutorialTask::GameplayKillElectricEnemies )
        {
            m_weaponChangeAllowed = true;

            // Spawn electric enemies
            m_gameLua->Call( "Tutorial03", 0, 0 );
        }
        else if ( task == TutorialTask::GameplayUseSmg )
        {
            TouchControls* tc = GameManager::GetInstance()->GetTouchControls();
            if ( tc )
            {
                tc->ShowFinger( true );

                Vectorf p = tc->GetSwapPosition();
                Vectori s = tc->GetSwapSize();
                ShowTooltip( "/Tutorial", "GameplayUseSmg", "TEXT_TUTORIAL_50", p.x, p.y + s.y/2, TutorialAnchor::Right, 3.0f, true );
            }
            else
            {
                const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
                ShowTooltip( "/Tutorial", "GameplayUseSmg", "TEXT_TUTORIAL_50", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 3.0f, false );
            }
        }
        else if ( task == TutorialTask::GameplayLevelUp )
        {
            m_gameMenuLua->Call( "TutorialGameplayLevelUp", 0, 0 );
        }
        else if ( task == TutorialTask::GameplaySummary )
        {
            m_gameMenuLua->Call( "TutorialGameplaySummary", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Challenges )
    {
        if ( task == TutorialTask::ChallengesSelectArea )
        {
            m_mainMenuLua->Call( "TutorialChallengesSelectArea", 0, 0 );
        }
        else if ( task == TutorialTask::ChallengesPlay )
        {
            m_mainMenuLua->Call( "TutorialChallengesPlay", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Items )
    {
        if ( task == TutorialTask::ItemsPause )
        {
            // Show pause notification for a little bit longer
            GameManager::GetInstance()->GetHud()->ShowPauseNotification( 10000 );

            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "ItemsPause", "TEXT_TUTORIAL_09", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 1, false );

            m_gameMenuLua->Call( "TutorialItemsPause", 0, 0 );
        }
        else if ( task == TutorialTask::ItemsBuy )
        {
            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "ItemsBuy", "TEXT_TUTORIAL_10", resolution.x/2, 0, TutorialAnchor::Top, 1, false );
        }
        else if ( task == TutorialTask::ItemsBuyGrenade )
        {
            m_gameMenuLua->Call( "TutorialItemsBuyGrenade", 0, 0 );
        }
        else if ( task == TutorialTask::ItemsBuyMine )
        {
            m_gameMenuLua->Call( "TutorialItemsBuyMine", 0, 0 );
        }
        else if ( task == TutorialTask::ItemsResume )
        {
            m_gameMenuLua->Call( "TutorialItemsResume", 0, 0 );
        }
        else if ( task == TutorialTask::ItemsUse )
        {
            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "ItemsUse", "TEXT_TUTORIAL_11", resolution.x/2, 0, TutorialAnchor::Top, 1, false );

            SetTaskActive( TutorialTask::ItemsUseGrenade );
            SetTaskActive( TutorialTask::ItemsUseMine );
        }
        else if ( task == TutorialTask::ItemsUseGrenade )
        {
            const float scale = GameManager::GetGameScale();
            Vectorf p = GameManager::GetInstance()->GetTouchControls()->GetGrenadePosition();
            ShowTooltip( "/Tutorial", "ItemsUseGrenade", "TEXT_TUTORIAL_12", p.x, p.y + scale*25.0f, TutorialAnchor::Right, 1.5, true );
        }
        else if ( task == TutorialTask::ItemsUseMine )
        {
            const float scale = GameManager::GetGameScale();
            Vectorf p = GameManager::GetInstance()->GetTouchControls()->GetMinePosition();
            ShowTooltip( "/Tutorial", "ItemsUseMine", "TEXT_TUTORIAL_13", p.x, p.y + scale*15.0f, TutorialAnchor::Right, 1.6, true );
        }
        else if ( task == TutorialTask::ItemsKillEnemies )
        {
            // Spawn enemies
            m_gameLua->Call( "Tutorial01", 0, 0 );

            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "ItemsKillEnemies", "TEXT_TUTORIAL_15", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 1, false, 5.0f );
        }
        else if ( task == TutorialTask::ItemsSummary )
        {
            m_gameMenuLua->Call( "TutorialItemsSummary", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Mech )
    {
        if ( task == TutorialTask::MechSelectArea )
        {
            Claw::Registry::Get()->Set( "/monstaz/weaponselection/1", "mechflamer" );
            Claw::Registry::Get()->Set( "/monstaz/weaponselection/2", "" );

            m_mainMenuLua->Call( "TutorialMechSelectArea", 0, 0 );
        }
        else if ( task == TutorialTask::MechPlay )
        {
            m_mainMenuLua->Call( "TutorialMechPlay", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Final )
    {
        if ( task == TutorialTask::FinalKillEnemies )
        {
            const Vectori& resolution = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution();
            ShowTooltip( "/Tutorial", "FinalKillEnemies", "TEXT_TUTORIAL_47", resolution.x/2, resolution.y, TutorialAnchor::Bottom, 1, false, 5.0f );
        }
        else if ( task == TutorialTask::FinalSummary )
        {
            m_gameMenuLua->Call( "TutorialFinalSummary", 0, 0 );
        }
    }
    else if ( m_chapter == TutorialChapter::Outro )
    {
        if ( task == TutorialTask::OutroGoodbye )
        {
            Claw::Registry::Get()->Set( "/monstaz/weaponselection/1", "smg" );
            Claw::Registry::Get()->Set( "/monstaz/weaponselection/2", "shotgun" );

            m_mainMenuLua->Call( "TutorialOutroGoodbye", 0, 0 );
        }
    }
}

void TutorialManager::SetTaskCompleted( TutorialTask::Id task )
{
    if( (m_completedTasks & task) != 0 ) return;

    m_activeTasks &= ~task;
    m_completedTasks |= task;

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TUTORIAL_TASK_COMPLETED, task );

    if ( m_chapter == TutorialChapter::Intro )
    {
        if ( task == TutorialTask::IntroPlayerMove )
        {
            HideTooltip( "/Tutorial/IntroPlayerMove" );
            if ( IsTaskCompleted( TutorialTask::IntroPlayerShot ) )
            {
                SetTaskActive( TutorialTask::IntroKillFirstWave );
            }
        }
        else if ( task == TutorialTask::IntroPlayerShot )
        {
            HideTooltip( "/Tutorial/IntroPlayerShot" );
            if ( IsTaskCompleted( TutorialTask::IntroPlayerMove ) )
            {
                SetTaskActive( TutorialTask::IntroKillFirstWave );
            }
        }
        else if ( task == TutorialTask::IntroKillFirstWave )
        {
            HideTooltip( "/Tutorial/IntroKillFirstWave" );
            SetTaskActive( TutorialTask::IntroSummary );
        }
        else if ( task == TutorialTask::IntroSummary )
        {
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Shop )
    {
        if ( task == TutorialTask::ShopEnter )
        {
            m_mainMenuLua->Call( "TutorialShopEnterCompleted", 0, 0 );
            SetTaskActive( TutorialTask::ShopBuyShotgun );
        }
        else if ( task == TutorialTask::ShopBuyShotgun )
        {
            m_mainMenuLua->Call( "TutorialShopBuyShotgunCompleted", 0, 0 );
            SetTaskActive( TutorialTask::ShopBuyShotgunPopup );
        }
        else if ( task == TutorialTask::ShopBuyShotgunPopup )
        {
            m_mainMenuLua->Call( "TutorialShopBuyShotgunPopupCompleted", 0, 0 );
            SetTaskActive( TutorialTask::ShopExit );
        }
        else if ( task == TutorialTask::ShopExit )
        {
            m_mainMenuLua->Call( "TutorialShopExitCompleted", 0, 0 );
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Premission )
    {
        if ( task == TutorialTask::PremissionSelect )
        {
            m_mainMenuLua->Call( "TutorialPremissionSelectCompleted", 0, 0 );
            SetTaskActive( TutorialTask::PremissionSecondWeaponSlot );
        }
        else if ( task == TutorialTask::PremissionSecondWeaponSlot )
        {
            m_mainMenuLua->Call( "TutorialPremissionSecondWeaponSlotCompleted", 0, 0 );
            SetTaskActive( TutorialTask::PremissionEquipShotgun );
        }
        else if ( task == TutorialTask::PremissionEquipShotgun )
        {
            m_mainMenuLua->Call( "TutorialPremissionEquipShotgunCompleted", 0, 0 );
            SetTaskActive( TutorialTask::PremissionFirstWeaponSlot );
        }
        else if ( task == TutorialTask::PremissionFirstWeaponSlot )
        {
            m_mainMenuLua->Call( "TutorialPremissionFirstWeaponSlotCompleted", 0, 0 );
            SetTaskActive( TutorialTask::PremissionUpgradeSmg );
        }
        else if ( task == TutorialTask::PremissionUpgradeSmg )
        {
            m_mainMenuLua->Call( "TutorialPremissionUpgradeSmgCompleted", 0, 0 );
            SetTaskActive( TutorialTask::PremissionUpgradeSmgBack );
        }
        else if ( task == TutorialTask::PremissionUpgradeSmgBack )
        {
            m_mainMenuLua->Call( "TutorialPremissionUpgradeSmgBackCompleted", 0, 0 );
            SetTaskActive( TutorialTask::PremissionPlay );
        }
        else if ( task == TutorialTask::PremissionPlay )
        {
            m_mainMenuLua->Call( "TutorialPremissionPlayCompleted", 0, 0 );
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Gameplay )
    {
        if ( task == TutorialTask::GameplayChangeWeapon )
        {
            m_weaponChangeAllowed = false;

            TouchControls* tc = GameManager::GetInstance()->GetTouchControls();
            if ( tc )
            {
                tc->ShowFinger( false );
            }

            HideTooltip( "/Tutorial/GameplayChangeWeapon" );
            SetTaskActive( TutorialTask::GameplayKillEnemies );
        }
        else if ( task == TutorialTask::GameplayKillEnemies )
        {
            HideTooltip( "/Tutorial/GameplayKillEnemies" );
            SetTaskActive( TutorialTask::GameplayRage );
        }
        else if ( task == TutorialTask::GameplayRage )
        {
            m_gameMenuLua->Call( "TutorialGameplayRageCompleted", 0, 0 );
            SetTaskActive( TutorialTask::GameplaySelectPerk );
        }
        else if ( task == TutorialTask::GameplaySelectPerk )
        {
            m_gameMenuLua->Call( "TutorialGameplaySelectPerkCompleted", 0, 0 );
            SetTaskActive( TutorialTask::GameplayKillMoreEnemies );
        }
        else if ( task == TutorialTask::GameplayKillMoreEnemies )
        {
            SetTaskActive( TutorialTask::GameplayKillElectricEnemies );
        }
        else if ( task == TutorialTask::GameplayKillElectricEnemies )
        {
        }
        else if ( task == TutorialTask::GameplayUseSmg )
        {
            TouchControls* tc = GameManager::GetInstance()->GetTouchControls();
            if ( tc )
            {
                tc->ShowFinger( false );
            }

            HideTooltip( "/Tutorial/GameplayUseSmg" );
        }
        else if ( task == TutorialTask::GameplayLevelUp )
        {
            m_gameMenuLua->Call( "TutorialGameplayLevelUpCompleted", 0, 0 );
            SetTaskActive( TutorialTask::GameplaySummary );
        }
        else if ( task == TutorialTask::GameplaySummary )
        {
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Challenges )
    {
        if ( task == TutorialTask::ChallengesSelectArea )
        {
            m_mainMenuLua->Call( "TutorialChallengesSelectAreaCompleted", 0, 0 );
            SetTaskActive( TutorialTask::ChallengesPlay );
        }
        else if ( task == TutorialTask::ChallengesPlay )
        {
            m_mainMenuLua->Call( "TutorialChallengesPlayCompleted", 0, 0 );
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Items )
    {
        if ( task == TutorialTask::ItemsPause )
        {
            // Hide pause notification
            GameManager::GetInstance()->GetHud()->ShowPauseNotification( 0 );

            HideTooltip( "/Tutorial/ItemsPause" );
            m_gameMenuLua->Call( "TutorialItemsPauseCompleted", 0, 0 );

            SetTaskActive( TutorialTask::ItemsBuy );
        }
        else if ( task == TutorialTask::ItemsBuyGrenade )
        {
            m_gameMenuLua->Call( "TutorialItemsBuyGrenadeCompleted", 0, 0 );
            SetTaskActive( TutorialTask::ItemsBuyMine );
        }
        else if ( task == TutorialTask::ItemsBuyMine )
        {
            m_gameMenuLua->Call( "TutorialItemsBuyMineCompleted", 0, 0 );
            SetTaskCompleted( TutorialTask::ItemsBuy );
        }
        else if ( task == TutorialTask::ItemsBuy )
        {
            HideTooltip( "/Tutorial/ItemsBuy" );
            SetTaskActive( TutorialTask::ItemsResume );
        }
        else if ( task == TutorialTask::ItemsResume )
        {
            m_gameMenuLua->Call( "TutorialItemsResumeCompleted", 0, 0 );

            if ( GameManager::GetInstance()->GetTouchControls() )
            {
                SetTaskActive( TutorialTask::ItemsUse );
            }
            else
            {
                SetTaskActive( TutorialTask::ItemsKillEnemies );
            }
        }
        else if ( task == TutorialTask::ItemsUse )
        {
            HideTooltip( "/Tutorial/ItemsUse" );

            SetTaskCompleted( TutorialTask::ItemsUseGrenade );
            SetTaskCompleted( TutorialTask::ItemsUseMine );

            SetTaskActive( TutorialTask::ItemsKillEnemies );
        }
        else if ( task == TutorialTask::ItemsUseGrenade )
        {
            HideTooltip( "/Tutorial/ItemsUseGrenade" );
        }
        else if ( task == TutorialTask::ItemsUseMine )
        {
            HideTooltip( "/Tutorial/ItemsUseMine" );
        }
        else if ( task == TutorialTask::ItemsKillEnemies )
        {
            HideTooltip( "/Tutorial/ItemsKillEnemies" );
            SetTaskActive( TutorialTask::ItemsSummary );
        }
        else if ( task == TutorialTask::ItemsSummary )
        {
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Mech )
    {
        if ( task == TutorialTask::MechSelectArea )
        {
            m_mainMenuLua->Call( "TutorialMechSelectAreaCompleted", 0, 0 );
            SetTaskActive( TutorialTask::MechPlay );
        }
        else if ( task == TutorialTask::MechPlay )
        {
            m_mainMenuLua->Call( "TutorialMechPlayCompleted", 0, 0 );
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Final )
    {
        if ( task == TutorialTask::FinalKillEnemies )
        {
            HideTooltip( "/Tutorial/FinalKillEnemies" );
            SetTaskActive( TutorialTask::FinalSummary );
        }
        else if ( task == TutorialTask::FinalSummary )
        {
            Claw::Registry::Get()->Set( "/internal/socialsplash", true );
            SetChapterCompleted();
        }
    }
    else if ( m_chapter == TutorialChapter::Outro )
    {
        if ( task == TutorialTask::OutroGoodbye )
        {
            SetChapterCompleted();
        }
    }
}

void TutorialManager::SetChapterCompleted()
{
    if ( m_chapter == TutorialChapter::None )
    {
        return;
    }

    m_activeTasks = 0;
    m_completedTasks = 0;

    bool save = false;
    if ( m_chapter == TutorialChapter::Intro )
    {
        m_chapter = TutorialChapter::Shop;
    }
    else if ( m_chapter == TutorialChapter::Shop )
    {
        m_chapter = TutorialChapter::Premission;
        SetTaskActive( TutorialTask::PremissionSelect );
    }
    else if ( m_chapter == TutorialChapter::Premission )
    {
        m_chapter = TutorialChapter::Gameplay;
    }
    else if ( m_chapter == TutorialChapter::Gameplay )
    {
        m_chapter = TutorialChapter::Challenges;
    }
    else if ( m_chapter == TutorialChapter::Challenges )
    {
        m_chapter = TutorialChapter::Items;
    }
    else if ( m_chapter == TutorialChapter::Items )
    {
        m_chapter = TutorialChapter::Mech;
    }
    else if ( m_chapter == TutorialChapter::Mech )
    {
        m_chapter = TutorialChapter::Final;
    }
    else if ( m_chapter == TutorialChapter::Final )
    {
        m_chapter = TutorialChapter::Outro;
    }
    else if ( m_chapter == TutorialChapter::Outro )
    {
        m_chapter = TutorialChapter::None;

        m_mainMenuLua->PushBool( IsActive() );
        m_mainMenuLua->RegisterGlobal( "TutorialActive" );

        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TUTORIAL_COMPLETED );
        MonetizationManager::GetInstance()->ReportTapjoyAction( MonetizationManager::TA_TUTORIAL_COMPLETED );
        save = true;
    }

    Claw::Registry::Get()->Set( "/monstaz/tutorial/chapter", m_chapter );

    if ( save )
    {
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    }
}

void TutorialManager::OnPause()
{
    if ( !IsActive() )
    {
        return;
    }

    if ( m_chapter == TutorialChapter::Items && IsTaskActive( TutorialTask::ItemsPause ) )
    {
        SetTaskCompleted( TutorialTask::ItemsPause );
    }
    else if ( m_chapter != TutorialChapter::Items || IsTaskCompleted( TutorialTask::ItemsResume ) )
    {
        CLAW_ASSERT( m_gameMenuLua );
        m_gameMenuLua->Call( "TutorialPause", 0, 0 );
    }
}

void TutorialManager::OnLevelLoaded()
{
    if ( m_chapter == TutorialChapter::Intro )
    {
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_TUTORIAL_STARTED );

        GameManager::GetInstance()->GetHud()->SetWeapon( Shop::Items::Smg, 0 );

        if ( GameManager::GetInstance()->GetTouchControls() )
        {
            SetTaskActive( TutorialTask::IntroPlayerMove );
            SetTaskActive( TutorialTask::IntroPlayerShot );
        }
        else
        {
            SetTaskActive( TutorialTask::IntroKillFirstWave );
        }
    }
    else if ( m_chapter == TutorialChapter::Gameplay )
    {
        // Make sure that default weapon is selected
        GameManager::GetInstance()->GetHud()->SetWeapon( Shop::Items::Smg, 0 );

        SetTaskActive( TutorialTask::GameplayChangeWeapon );
    }
    else if ( m_chapter == TutorialChapter::Items )
    {
        SetTaskActive( TutorialTask::ItemsPause );
    }
    else if ( m_chapter == TutorialChapter::Final )
    {
        SetTaskActive( TutorialTask::FinalKillEnemies );
    }
}

void TutorialManager::OnLevelSummary( bool fail )
{
    if ( m_chapter == TutorialChapter::Intro && IsTaskActive( TutorialTask::IntroKillFirstWave ) )
    {
        SetTaskCompleted( TutorialTask::IntroKillFirstWave );
    }
    else if ( m_chapter == TutorialChapter::Gameplay && IsTaskActive( TutorialTask::GameplayKillElectricEnemies ) )
    {
        SetTaskCompleted( TutorialTask::GameplayKillElectricEnemies );
        SetTaskCompleted( TutorialTask::GameplayUseSmg );
    }
    else if ( m_chapter == TutorialChapter::Items && IsTaskActive( TutorialTask::ItemsKillEnemies ) )
    {
        SetTaskCompleted( TutorialTask::ItemsKillEnemies );
    }
    else if ( m_chapter == TutorialChapter::Final && IsTaskActive( TutorialTask::FinalKillEnemies ) )
    {
        SetTaskCompleted( TutorialTask::FinalKillEnemies );
    }
}

void TutorialManager::OnPlayerMove()
{
    if ( m_chapter == TutorialChapter::Intro && IsTaskActive( TutorialTask::IntroPlayerMove ) )
    {
        SetTaskCompleted( TutorialTask::IntroPlayerMove );
    }
}

void TutorialManager::OnPlayerShot()
{
    if ( m_chapter == TutorialChapter::Intro && IsTaskActive( TutorialTask::IntroPlayerShot ) )
    {
        SetTaskCompleted( TutorialTask::IntroPlayerShot );
    }
}

void TutorialManager::OnWeaponChange()
{
    if ( m_chapter == TutorialChapter::Gameplay )
    {
        if ( IsTaskActive( TutorialTask::GameplayChangeWeapon ) )
        {
            SetTaskCompleted( TutorialTask::GameplayChangeWeapon );
        }
        else if ( IsTaskActive( TutorialTask::GameplayUseSmg ) )
        {
            SetTaskCompleted( TutorialTask::GameplayUseSmg );
        }
    }
}

void TutorialManager::OnEnemyKilled( Entity* entity )
{
    if ( m_chapter == TutorialChapter::Gameplay && IsTaskActive( TutorialTask::GameplayKillEnemies ) )
    {
        const EntityData& ed = GameManager::GetInstance()->GetEntityManager()->GetData( entity->GetType() );

        int xp = ed.xp;
        if ( entity->GetHitPoints() <= -5 )
        {
            xp *= 1.5f;
        }

        GameManager::GetInstance()->GetStats()->AddXp( xp );
    }
}

void TutorialManager::OnEnemyWaveKilled( int wave )
{
    if ( m_chapter == TutorialChapter::Gameplay && IsTaskActive( TutorialTask::GameplayKillMoreEnemies ) && wave == 2 )
    {
        SetTaskCompleted( TutorialTask::GameplayKillMoreEnemies );
    }
}

void TutorialManager::OnRage()
{
    if ( m_chapter == TutorialChapter::Gameplay && IsTaskActive( TutorialTask::GameplayKillEnemies ) )
    {
        SetTaskCompleted( TutorialTask::GameplayKillEnemies );
    }
}

void TutorialManager::OnElementHit()
{
    if ( IsTaskActive( TutorialTask::GameplayKillElectricEnemies ) && !IsTaskActive( TutorialTask::GameplayUseSmg ) )
    {
        SetTaskActive ( TutorialTask::GameplayUseSmg );
    }
}

void TutorialManager::OnMainMenu()
{
    if ( m_chapter == TutorialChapter::Shop && !IsTaskCompleted( TutorialTask::ShopEnter ) )
    {
        SetTaskActive( TutorialTask::ShopEnter );
    }
    else if ( m_chapter == TutorialChapter::Premission )
    {
        SetTaskActive( TutorialTask::PremissionSelect );
    }
    else if ( m_chapter == TutorialChapter::Challenges )
    {
        SetTaskActive( TutorialTask::ChallengesSelectArea );
    }
    else if ( m_chapter == TutorialChapter::Mech )
    {
        SetTaskActive( TutorialTask::MechSelectArea );
    }
    else if ( m_chapter == TutorialChapter::Outro )
    {
        SetTaskActive( TutorialTask::OutroGoodbye );
    }
}

void TutorialManager::OnFireGrenade()
{
    if ( m_chapter == TutorialChapter::Items && IsTaskActive( TutorialTask::ItemsUseGrenade ) )
    {
        SetTaskCompleted( TutorialTask::ItemsUseGrenade );
        SetTaskCompleted( TutorialTask::ItemsUse );
    }
}

void TutorialManager::OnPlaceMine()
{
    if ( m_chapter == TutorialChapter::Items && IsTaskActive( TutorialTask::ItemsUseMine ) )
    {
        SetTaskCompleted( TutorialTask::ItemsUseMine );
        SetTaskCompleted( TutorialTask::ItemsUse );
    }
}

void TutorialManager::ShowTooltip( const char* parent, const char* name, const char* message, int x, int y, TutorialAnchor::Pos anchor, float delay, bool showArrow, float hideDelay )
{    Claw::Lua* lua = m_gameMenuLua ? m_gameMenuLua : m_mainMenuLua;
    CLAW_ASSERT( lua );

    lua->PushString( parent );
    lua->PushString( name );
    lua->PushString( message );
    lua->PushNumber( x );
    lua->PushNumber( y );
    lua->PushNumber( anchor );
    lua->PushNumber( delay );
    lua->PushBool( showArrow );
    lua->PushNumber( hideDelay );
    lua->Call( "TutorialTooltipShow", 9, 0 );
}

void TutorialManager::HideTooltip( const char* path )
{
    Claw::Lua* lua = m_gameMenuLua ? m_gameMenuLua : m_mainMenuLua;
    CLAW_ASSERT( lua );

    lua->PushString( path );
    lua->Call( "TutorialTooltipHide", 1, 0 );
}

void TutorialManager::Init( Claw::Lua* lua )
{
    Claw::Lunar<TutorialManager>::Register( *lua );
    Claw::Lunar<TutorialManager>::push( *lua, this );
    lua->RegisterGlobal( "TutorialManager" );

    lua->PushBool( IsActive() );
    lua->RegisterGlobal( "TutorialActive" );

    lua->CreateEnumTable();
    lua->AddEnum( TutorialChapter::None );
    lua->AddEnum( TutorialChapter::Intro );
    lua->AddEnum( TutorialChapter::Shop );
    lua->AddEnum( TutorialChapter::Premission );
    lua->AddEnum( TutorialChapter::Gameplay );
    lua->AddEnum( TutorialChapter::Challenges );
    lua->AddEnum( TutorialChapter::Items );
    lua->AddEnum( TutorialChapter::Mech );
    lua->AddEnum( TutorialChapter::Final );
    lua->AddEnum( TutorialChapter::Outro );
    lua->RegisterEnumTable( "TutorialChapter" );

    lua->CreateEnumTable();
    lua->AddEnum( TutorialTask::IntroPlayerMove );
    lua->AddEnum( TutorialTask::IntroPlayerShot );
    lua->AddEnum( TutorialTask::IntroKillFirstWave );
    lua->AddEnum( TutorialTask::IntroSummary );
    lua->AddEnum( TutorialTask::ShopEnter );
    lua->AddEnum( TutorialTask::ShopBuyShotgun );
    lua->AddEnum( TutorialTask::ShopBuyShotgunPopup );
    lua->AddEnum( TutorialTask::ShopExit );
    lua->AddEnum( TutorialTask::PremissionSelect );
    lua->AddEnum( TutorialTask::PremissionSecondWeaponSlot );
    lua->AddEnum( TutorialTask::PremissionEquipShotgun );
    lua->AddEnum( TutorialTask::PremissionFirstWeaponSlot );
    lua->AddEnum( TutorialTask::PremissionUpgradeSmg );
    lua->AddEnum( TutorialTask::PremissionUpgradeSmgBack );
    lua->AddEnum( TutorialTask::PremissionPlay );
    lua->AddEnum( TutorialTask::GameplayChangeWeapon );
    lua->AddEnum( TutorialTask::GameplayKillEnemies );
    lua->AddEnum( TutorialTask::GameplayRage );
    lua->AddEnum( TutorialTask::GameplaySelectPerk );
    lua->AddEnum( TutorialTask::GameplayKillMoreEnemies );
    lua->AddEnum( TutorialTask::GameplayKillElectricEnemies );
    lua->AddEnum( TutorialTask::GameplayUseSmg );
    lua->AddEnum( TutorialTask::GameplayLevelUp );
    lua->AddEnum( TutorialTask::GameplaySummary );
    lua->AddEnum( TutorialTask::ChallengesSelectArea );
    lua->AddEnum( TutorialTask::ChallengesPlay );
    lua->AddEnum( TutorialTask::ItemsPause );
    lua->AddEnum( TutorialTask::ItemsBuy );
    lua->AddEnum( TutorialTask::ItemsBuyGrenade );
    lua->AddEnum( TutorialTask::ItemsBuyMine );
    lua->AddEnum( TutorialTask::ItemsResume );
    lua->AddEnum( TutorialTask::ItemsUse );
    lua->AddEnum( TutorialTask::ItemsUseGrenade );
    lua->AddEnum( TutorialTask::ItemsUseMine );
    lua->AddEnum( TutorialTask::ItemsKillEnemies );
    lua->AddEnum( TutorialTask::ItemsSummary );
    lua->AddEnum( TutorialTask::MechSelectArea );
    lua->AddEnum( TutorialTask::MechPlay );
    lua->AddEnum( TutorialTask::FinalKillEnemies );
    lua->AddEnum( TutorialTask::FinalSummary );
    lua->AddEnum( TutorialTask::OutroGoodbye );
    lua->RegisterEnumTable( "TutorialTask" );

    lua->CreateEnumTable();
    lua->AddEnum( TutorialAnchor::Bottom );
    lua->AddEnum( TutorialAnchor::Top );
    lua->AddEnum( TutorialAnchor::Left );
    lua->AddEnum( TutorialAnchor::Right );
    lua->RegisterEnumTable( "TutorialAnchor" );
}

int TutorialManager::l_GetCurrentChapter( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_chapter );
    return 1;
}

int TutorialManager::l_IsTaskActive( lua_State* L )
{
    Claw::Lua lua( L );

    const int task = static_cast<int>( lua.CheckNumber( -1 ) );
    lua.PushBool( IsTaskActive( static_cast<TutorialTask::Id>( task ) ) );

    return 1;
}

int TutorialManager::l_IsTaskCompleted( lua_State* L )
{
    Claw::Lua lua( L );

    const int task = static_cast<int>( lua.CheckNumber( -1 ) );
    lua.PushBool( IsTaskCompleted( static_cast<TutorialTask::Id>( task ) ) );

    return 1;
}

int TutorialManager::l_SetTaskActive( lua_State* L )
{
    Claw::Lua lua( L );

    const int task = static_cast<int>( lua.CheckNumber( -1 ) );
    SetTaskActive( static_cast<TutorialTask::Id>( task ) );

    return 0;
}

int TutorialManager::l_SetTaskCompleted( lua_State* L )
{
    Claw::Lua lua( L );

    const int task = static_cast<int>( lua.CheckNumber( -1 ) );
    SetTaskCompleted( static_cast<TutorialTask::Id>( task ) );

    return 0;
}

int TutorialManager::l_OnEnemyWaveKilled( lua_State* L )
{
    Claw::Lua lua( L );

    const int wave = static_cast<int>( lua.CheckNumber( -1 ) );
    OnEnemyWaveKilled( wave );

    return 0;
}

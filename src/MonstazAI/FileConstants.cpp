#include "MonstazAI/FileConstants.hpp"

namespace FileConstants
{
    const char* LOCALE[LanguageSettings::L_NUM] = 
    {
        "l10n/en.xml",
        "l10n/jp.xml",
    };
} // namespace FileConstants

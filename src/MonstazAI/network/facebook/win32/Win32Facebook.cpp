#include "MonstazAI/network/facebook/win32/Win32Facebook.hpp"

#include "claw/application/Time.hpp"
#include "claw/base/Errors.hpp"
#include "claw/vfs/Vfs.hpp"

static Win32Facebook* s_instance = NULL;

namespace Network
{
    Facebook* Facebook::QueryInterface( const char* fbAppId, const char* fbUrlSchemeSuffix )
    {
        // Force singleton pattern
        CLAW_ASSERT( s_instance == NULL );
        s_instance = new Win32Facebook;
        return s_instance;
    }

    void Facebook::Release( Facebook* instance )
    {
        CLAW_ASSERT( s_instance );
        s_instance = NULL;
        delete instance;
    }
}

bool Win32Facebook::Authenticate( bool allowLoginUi, Claw::NarrowString* err /* = NULL */ )
{
    if ( !m_authenticated )
    {
        m_authenticated = true;
        NotifyAuthenticationChange( m_authenticated );
    }

    return true;
}

bool Win32Facebook::PublishFeed()
{
    NotifyFeedPublished( "12345" );
    return true;
}

bool Win32Facebook::PublishFeed( const FeedData& feed )
{
    NotifyFeedPublished( "23456" );
    return true;
}

bool Win32Facebook::PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message )
{
    PngCreate( "save/fb-publish-image.png", pixels, width, height );
    NotifyImagePublished( "12345" );

    return true;
}

void Win32Facebook::PngCreate( const char* filename, const char* pixels, int width, int height )
{
    Claw::FilePtr file( Claw::VfsCreateFile( filename ) );
    CLAW_ASSERT( file );

    png_structp png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    png_infop info_ptr = png_create_info_struct( png_ptr );
    setjmp( png_jmpbuf( png_ptr ) );

    png_set_write_fn( png_ptr, file.GetPtr(), &Win32Facebook::PngWrite, NULL );
    png_set_IHDR( png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE );
    png_write_info( png_ptr, info_ptr );

    const unsigned char* ptr = (const unsigned char*)pixels;
    for( int i=0; i<height; i++ )
    {
        png_write_rows( png_ptr, (png_bytepp)(&ptr), 1 );
        ptr += width*4;
    }

    png_write_end( png_ptr, info_ptr );
    png_destroy_write_struct( &png_ptr, &info_ptr );
}

void Win32Facebook::PngWrite( png_structp png_ptr, png_bytep data, png_size_t length )
{
    Claw::File* f = (Claw::File*)png_ptr->io_ptr;
    f->Write( data, length );
}

bool Win32Facebook::PublishAction( const char* graphPath, const char* objectName, const char* objectUrl )
{
    NotifyActionPublished( "98765" );
    return true;
}

bool Win32Facebook::SendRequest( const RequestData& appRequest )
{
    NotifyRequestSent( "34567" );
    return true;
}

bool Win32Facebook::SendRequest( const RequestData& appRequest, const RequestCallbackData& callback )
{
    callback.function( "34567", callback.ptr );
    return true;
}

bool Win32Facebook::GetRequests()
{
    RequestData request;
    request.id = "12345";
    request.uid = "100004032715787";
    request.username = "Andrzej Deweloperski";
    request.message = "Dummy request from dummy user";

    NotifyRequestReceived( request );

    return true;
}

bool Win32Facebook::RemoveRequest( const char* requestId )
{
    NotifyRequestRemoved();
    return true;
}

bool Win32Facebook::SendScore( int score )
{
    NotifyScoreSent();
    return true;
}

bool Win32Facebook::GetScores( int limit )
{
    if ( limit == 0 )
    {
        limit = 15;
    }

    int uid = 123;
    char buffer[32];

    std::vector<Facebook::ScoreData> scores;
    for ( int i = 0; i < limit; ++i )
    {
        ScoreData score;
        score.m_username = "Andrzej Deweloperski";
        score.m_picture = "http://profile.ak.fbcdn.net/static-ak/rsrc.php/v2/yo/r/UlIqmHJn-SK.gif";
        score.m_score = 100 - i;

        sprintf( buffer, "%d", uid + i );
        score.m_uid = buffer;

        scores.push_back( score );
    }

    NotifyScoresReceived( scores );

    return true;
}

Claw::NarrowString Win32Facebook::GetUserId()
{
    Claw::NarrowString uid;
    if ( IsAuthenticated() )
    {
        uid = "1234567890";
    }

    return uid;
}

Claw::NarrowString Win32Facebook::GetUserName()
{
    Claw::NarrowString username;
    if ( IsAuthenticated() )
    {
        username = "Andrzej Deweloperski";
    }

    return username;
}

bool Win32Facebook::GetAvatar( const char* fbId )
{
    AvatarData avatar;
    avatar.m_picture = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-frc3/c191.46.579.579/s200x200/972276_277489895728765_1266147947_n.jpg";
   
    if (fbId == "me")
        fbId = "100004032715787";

    avatar.m_uid = fbId;
    NotifyAvatarsReceived( avatar );
    return true;
}

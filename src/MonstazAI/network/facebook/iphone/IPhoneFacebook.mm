#import "IPhoneFacebookImpl.h"

#include "claw/base/Errors.hpp"

#include "MonstazAI/network/facebook/Facebook.hpp"
#include "MonstazAI/network/facebook/iphone/IPhoneFacebook.hpp"

static IPhoneFacebookImpl* m_impl = NULL;

namespace Network
{
    Facebook* Facebook::QueryInterface( const char* fbAppId, const char* fbUrlSchemeSuffix )
    {
        return new IPhoneFacebook( fbAppId, fbUrlSchemeSuffix );
    }

    void Facebook::Release( Facebook* instance )
    {
        delete instance;
    }
}

IPhoneFacebook::IPhoneFacebook( const char* fbAppId, const char* fbUrlSchemeSuffix )
    :Facebook()
{
    CLAW_MSG_ASSERT( !m_impl, "Using Objective-C PIMPL doesn't allow for multiple object instances (impl static)." );

    NSString* appId = [NSString stringWithCString:fbAppId encoding:NSASCIIStringEncoding];

    if ( fbUrlSchemeSuffix )
    {
        NSString* urlSchemeSuffix = [NSString stringWithCString:fbUrlSchemeSuffix encoding:NSASCIIStringEncoding];
        m_impl = [[IPhoneFacebookImpl alloc] initWithAppId:appId andUrlSchemeSuffix:urlSchemeSuffix andObserver:this];
    }
    else
    {
        m_impl = [[IPhoneFacebookImpl alloc] initWithAppId:appId andUrlSchemeSuffix:nil andObserver:this];
    }
}

IPhoneFacebook::~IPhoneFacebook()
{
    [m_impl dealloc];
    m_impl = NULL;
}

bool IPhoneFacebook::Authenticate( bool allowLoginUi, Claw::NarrowString* err )
{
    return [m_impl authenticateWithLoginUI:allowLoginUi];
}

bool IPhoneFacebook::Logout()
{
    return [m_impl logout];
}

bool IPhoneFacebook::PublishFeed()
{
    return [m_impl publishFeed];
}

bool IPhoneFacebook::PublishFeed( const FeedData& feed )
{
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];

    if ( !feed.link.empty() )
    {
        NSString* link = [NSString stringWithUTF8String:feed.link.c_str()];
        [params setObject:link forKey:@"link"];
    }

    if ( !feed.picture.empty() )
    {
        NSString* picture = [NSString stringWithUTF8String:feed.picture.c_str()];
        [params setObject:picture forKey:@"picture"];
    }

    if ( !feed.name.empty() )
    {
        NSString* name = [NSString stringWithUTF8String:feed.name.c_str()];
        [params setObject:name forKey:@"name"];
    }

    if ( !feed.caption.empty() )
    {
        NSString* caption = [NSString stringWithUTF8String:feed.caption.c_str()];
        [params setObject:caption forKey:@"caption"];
    }

    if ( !feed.description.empty() )
    {
        NSString* description = [NSString stringWithUTF8String:feed.description.c_str()];
        [params setObject:description forKey:@"description"];
    }

    FBSBJSON* jsonWriter = [[FBSBJSON new] autorelease];

    if ( !feed.properties.empty() )
    {
        NSMutableDictionary* properties = [[NSMutableDictionary alloc] init];

        FeedPropertyList::const_iterator it = feed.properties.begin();
        FeedPropertyList::const_iterator end = feed.properties.end();
        for ( ; it != end; ++it )
        {
            NSString* key = [NSString stringWithUTF8String:it->key.c_str()];
            NSString* text = [NSString stringWithUTF8String:it->text.c_str()];
            NSString* href = [NSString stringWithUTF8String:it->href.c_str()];

            NSDictionary* property = [NSDictionary dictionaryWithObjectsAndKeys:
                                      text, @"text",
                                      href, @"href",
                                      nil];

            [properties setObject:property forKey:key];
        }

        [params setObject:[jsonWriter stringWithObject:properties] forKey:@"properties"];
    }

    if ( !feed.action.name.empty() && !feed.action.link.empty() )
    {
        NSString* name = [NSString stringWithUTF8String:feed.action.name.c_str()];
        NSString* link = [NSString stringWithUTF8String:feed.action.link.c_str()];

        NSDictionary* actions = [NSDictionary dictionaryWithObjectsAndKeys:
                                 name, @"name",
                                 link, @"link",
                                 nil];

        [params setObject:[jsonWriter stringWithObject:actions] forKey:@"actions"];
    }

    return [m_impl publishFeedWithParams:params];
}

bool IPhoneFacebook::PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message )
{
    CGDataProviderRef dataProviderRef = CGDataProviderCreateWithData( NULL, pixels, width*height*4, NULL );
    CGImageRef imageRef = CGImageCreate( width, height, 8, 32, width*4, CGColorSpaceCreateDeviceRGB(), kCGBitmapByteOrderDefault, dataProviderRef, NULL, NO, kCGRenderingIntentDefault );

    UIImage* image = [UIImage imageWithCGImage:imageRef];
    if ( !message.empty() )
    {
        NSString* msg = [NSString stringWithCString:message.c_str() encoding:NSASCIIStringEncoding];
        [m_impl publishImage:image withMessage:msg];
    }
    else
    {
        [m_impl publishImage:image withMessage:nil];
    }

    CGImageRelease( imageRef );
    CGDataProviderRelease( dataProviderRef );

    return true;
}

bool IPhoneFacebook::PublishAction( const char* graphPath, const char* objectName, const char* objectUrl )
{
    return [m_impl publishAction:[NSString stringWithUTF8String:graphPath]
                  withObjectName:[NSString stringWithUTF8String:objectName]
                    andObjectUrl:[NSString stringWithUTF8String:objectUrl]];
}

bool IPhoneFacebook::SendRequest( const RequestData& request )
{
    CLAW_ASSERT( !request.message.empty() );

    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];

    NSString* message = [NSString stringWithUTF8String:request.message.c_str()];
    [params setObject:message forKey:@"message"];

    if ( !request.uid.empty() )
    {
        NSString* to = [NSString stringWithUTF8String:request.uid.c_str()];
        [params setObject:to forKey:@"to"];
    }

    if ( !request.properties.empty() )
    {
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init];

        RequestPropertyList::const_iterator it = request.properties.begin();
        RequestPropertyList::const_iterator end = request.properties.end();
        for ( ; it != end; ++it )
        {
            NSString* key = [NSString stringWithUTF8String:it->key.c_str()];
            NSString* value = [NSString stringWithUTF8String:it->value.c_str()];

            [data setObject:value forKey:key];
        }

        FBSBJSON* jsonWriter = [[FBSBJSON new] autorelease];
        [params setObject:[jsonWriter stringWithObject:data] forKey:@"data"];
    }

    return [m_impl sendRequestWithParams:params];
}

bool IPhoneFacebook::SendRequest( const RequestData& request, const RequestCallbackData& callback )
{
    CLAW_ASSERT( !request.message.empty() );

    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];

    NSString* message = [NSString stringWithUTF8String:request.message.c_str()];
    [params setObject:message forKey:@"message"];

    if ( !request.uid.empty() )
    {
        NSString* to = [NSString stringWithUTF8String:request.uid.c_str()];
        [params setObject:to forKey:@"to"];
    }

    if ( !request.properties.empty() )
    {
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init];

        RequestPropertyList::const_iterator it = request.properties.begin();
        RequestPropertyList::const_iterator end = request.properties.end();
        for ( ; it != end; ++it )
        {
            NSString* key = [NSString stringWithUTF8String:it->key.c_str()];
            NSString* value = [NSString stringWithUTF8String:it->value.c_str()];

            [data setObject:value forKey:key];
        }

        FBSBJSON* jsonWriter = [[FBSBJSON new] autorelease];
        [params setObject:[jsonWriter stringWithObject:data] forKey:@"data"];
    }

    return [m_impl sendRequestWithParams:params andCallback:callback];
}

bool IPhoneFacebook::GetRequests()
{
    return [m_impl getRequests];
}

bool IPhoneFacebook::RemoveRequest( const char* requestId )
{
    NSString* rid = [NSString stringWithCString:requestId encoding:NSASCIIStringEncoding];
    return [m_impl removeRequest:rid];
}

bool IPhoneFacebook::SendScore( int score )
{
    return [m_impl sendScore:score];
}

bool IPhoneFacebook::GetScores( int limit )
{
    return [m_impl getScoresWithLimit:limit];
}

bool IPhoneFacebook::GetAvatar( const char* fbId )
{
    NSString* idFB = [NSString stringWithCString:fbId encoding:NSASCIIStringEncoding];
    return [m_impl getAvatar:idFB];
}

Claw::NarrowString IPhoneFacebook::GetUserId()
{
    Claw::NarrowString uid;
    if ( m_impl.uid )
    {
        return [m_impl.uid cStringUsingEncoding:NSUTF8StringEncoding];
    }

    return uid;
}

Claw::NarrowString IPhoneFacebook::GetUserName()
{
    Claw::NarrowString username;
    if ( m_impl.username )
    {
        return [m_impl.username cStringUsingEncoding:NSUTF8StringEncoding];
    }

    return username;
}

void IPhoneFacebook::NotifyAuthenticationChange( bool authenticated )
{
    Facebook::NotifyAuthenticationChange( authenticated );
}

void IPhoneFacebook::NotifyFeedPublished( const char* identifier )
{
    Facebook::NotifyFeedPublished( identifier );
}

void IPhoneFacebook::NotifyImagePublished( const char* identifier )
{
    Facebook::NotifyImagePublished( identifier );
}

void IPhoneFacebook::NotifyActionPublished( const char* identifier )
{
    Facebook::NotifyActionPublished( identifier );
}

void IPhoneFacebook::NotifyRequestSent( const char* identifier )
{
    Facebook::NotifyRequestSent( identifier );
}

void IPhoneFacebook::NotifyRequestReceived( const RequestData& appRequest )
{
    Facebook::NotifyRequestReceived( appRequest );
}

void IPhoneFacebook::NotifyRequestRemoved()
{
    Facebook::NotifyRequestRemoved();
}

void IPhoneFacebook::NotifyScoreSent()
{
    Facebook::NotifyScoreSent();
}

void IPhoneFacebook::NotifyScoresReceived( std::vector<ScoreData>& scores )
{
    Facebook::NotifyScoresReceived( scores );
}

void IPhoneFacebook::NotifyAvatarsReceived(AvatarData &avatar)
{
    Facebook::NotifyAvatarsReceived( avatar );
}

#import "RequestCallbackDelegate.h"

#include "claw/base/Errors.hpp"

#include "MonstazAI/network/facebook/iphone/IPhoneFacebook.hpp"

@interface RequestCallbackDelegate ()

@property Network::Facebook::RequestCallbackData callback;

@end

@implementation RequestCallbackDelegate

@synthesize callback = _callback;

- (id) initWithCallback:(Network::Facebook::RequestCallbackData) callback
{
	if ( ( self = [super init] ) )
    {
        self.callback = callback;
    }

    return self;
}

- (void)dialogDidComplete:(FBDialog *)dialog
{
    CLAW_MSG( "[RequestCallbackDelegate] Facebook dialog completed successfully." );
}

- (void)dialogCompleteWithUrl:(NSURL *)url
{
    CLAW_MSG( "[RequestCallbackDelegate] Facebook dialog completed successfully with returning URL." );

    NSArray *pairs = [[url query] componentsSeparatedByString:@"&"];
	NSMutableDictionary *params = [[[NSMutableDictionary alloc] init] autorelease];
	for ( NSString *pair in pairs )
    {
		NSArray *kv = [pair componentsSeparatedByString:@"="];
		NSString *val = [[kv objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];

		[params setObject:val forKey:[kv objectAtIndex:0]];
	}

    for ( NSString *paramKey in params )
    {
        if ( [paramKey hasPrefix:@"request"] )
        {
            NSString* requestId = [params objectForKey:paramKey];
            self.callback.function( [requestId cStringUsingEncoding:NSASCIIStringEncoding], self.callback.ptr );
        }
    }
}

- (void)dialogDidNotCompleteWithUrl:(NSURL *)url
{
    CLAW_MSG( "[RequestCallbackDelegate] Facebook dialog canceled by the user." );
}

- (void)dialogDidNotComplete:(FBDialog *)dialog
{
    CLAW_MSG( "[RequestCallbackDelegate] Facebook dialog is cancelled and is about to be dismissed." );
}

- (void) dialog:(FBDialog*) dialog didFailWithError:(NSError*) error
{
    CLAW_MSG( "[RequestCallbackDelegate] Facebook dialog failed to load due to an error." );
}

@end

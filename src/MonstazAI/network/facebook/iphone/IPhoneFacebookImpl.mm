#import "Facebook.h"
#import "IPhoneFacebookImpl.h"
#import "delegates/RequestCallbackDelegate.h"

#include "claw/base/Errors.hpp"
#include "MonstazAI/network/facebook/iphone/IPhoneFacebook.hpp"

@interface IPhoneFacebookImpl()

@property BOOL authenticated;
@property (retain, nonatomic) Facebook *facebook;

@property (readwrite, nonatomic, retain) NSString* urlSchemeSuffix;

@property (readwrite, nonatomic, retain) NSString* uid;
@property (readwrite, nonatomic, retain) NSString* username;

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error;

@end

@implementation IPhoneFacebookImpl

@synthesize authenticated = _authenticated;
@synthesize facebook = _facebook;

@synthesize urlSchemeSuffix = _urlSchemeSuffix;

@synthesize uid = _uid;
@synthesize username = _username;

- (id) initWithAppId:(NSString*)appId andUrlSchemeSuffix:(NSString*)urlSchemeSuffix andObserver:(IPhoneFacebook*) observer
{
    if( ( self = [super init] ) )
    {
        self.authenticated = NO;
        self.urlSchemeSuffix = urlSchemeSuffix;

        m_observer = observer;

        [FBSession setDefaultAppID:appId];
    }
    return self;
}

- (void) dealloc
{
    self.facebook = nil;

    [super dealloc];
}

- (bool) authenticateWithLoginUI:(BOOL)allowLoginUI
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"publish_actions", // Required for sending scores
                            nil];

    FBSession *session =
        [[FBSession alloc] initWithAppID:nil
                             permissions:permissions
                         urlSchemeSuffix:self.urlSchemeSuffix
                      tokenCacheStrategy:nil];

    if ( allowLoginUI || session.state == FBSessionStateCreatedTokenLoaded )
    {
        [FBSession setActiveSession:session];
        [session openWithCompletionHandler:^(FBSession *session, FBSessionState state, NSError *error)
         {
             [self sessionStateChanged:session
                            state:state
                            error:error];
         }];
    }
    [permissions release];
    [session release];

    return true;
}

- (bool) logout
{
    [FBSession.activeSession closeAndClearTokenInformation];
    return true;
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    switch ( state )
    {
        case FBSessionStateOpen:
            break;

        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            NSLog( @"%@", error.description );
            [FBSession.activeSession closeAndClearTokenInformation];
            break;

        default:
            break;
    }

    if ( FBSession.activeSession.isOpen )
    {
        self.facebook = [[Facebook alloc]
                         initWithAppId:FBSession.activeSession.appID
                         andDelegate:nil];

        self.facebook.accessToken = FBSession.activeSession.accessToken;
        self.facebook.expirationDate = FBSession.activeSession.expirationDate;

        [self.facebook enableFrictionlessRequests];

        // Try to fetch user data before notifying about authentication change
        FBRequest* request = [FBRequest requestForMe];
        [request startWithCompletionHandler:^(FBRequestConnection *connection, id<FBGraphUser> user, NSError *error)
         {
             if ( !error )
             {
                 self.uid = user.id;
                 self.username = user.name;
             }

             self.authenticated = YES;
             m_observer->NotifyAuthenticationChange( self.authenticated );
         }];
    }
    else
    {
        self.facebook = nil;
        self.authenticated = NO;
        self.uid = nil;
        self.username = nil;
        m_observer->NotifyAuthenticationChange( self.authenticated );
    }
}

- (bool) sendRequestWithParams:(NSMutableDictionary*) params
{
    if( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User not authenticated to Facebook App, cannot send request" );
        return false;
    }

    [self.facebook dialog:@"apprequests" andParams:params andDelegate:self];
    return true;
}

- (bool) sendRequestWithParams:(NSMutableDictionary*) params andCallback:(Network::Facebook::RequestCallbackData) callback
{
    if( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User not authenticated to Facebook App, cannot send request" );
        return false;
    }

    RequestCallbackDelegate* delegate = [[RequestCallbackDelegate alloc] initWithCallback:callback];
    [self.facebook dialog:@"apprequests" andParams:params andDelegate:delegate];

    return true;
}

- (bool) getRequests
{
    if ( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User is not authenticated, cannot get scores" );
        return false;
    }

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   FBSession.activeSession.accessToken, @"access_token",
                                   nil];

    FBRequest* request =
        [FBRequest requestWithGraphPath:@"me/apprequests"
                             parameters:params
                             HTTPMethod:@"GET"];

    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if ( error )
         {
             NSLog( @"%@", [error description] );
             return;
         }

         NSArray *data = [result objectForKey:@"data"];
         if ( data )
         {
             for ( NSDictionary *info in data )
             {
                 NSString* requestId = [info objectForKey:@"id"];
                 NSString* message = [info objectForKey:@"message"];

                 NSDictionary *from = [info objectForKey:@"from"];
                 NSString* uid = [NSString stringWithFormat:@"%lld", [[from objectForKey:@"id"] longLongValue]];
                 NSString* username = [from objectForKey:@"name"];

                 Network::Facebook::RequestData requestData;
                 requestData.id = [requestId cStringUsingEncoding:NSUTF8StringEncoding];
                 requestData.uid = [uid cStringUsingEncoding:NSUTF8StringEncoding];
                 requestData.username = [username cStringUsingEncoding:NSUTF8StringEncoding];
                 requestData.message = [message cStringUsingEncoding:NSUTF8StringEncoding];

                 if ( [info objectForKey:@"data"] )
                 {
                     FBSBJSON* jsonParser = [[FBSBJSON new] autorelease];
                     NSDictionary* properties = [jsonParser objectWithString:[info objectForKey:@"data"]];

                     NSArray* keys = [properties allKeys];
                     for ( NSString* key in keys )
                     {
                         NSString* value = [NSString stringWithFormat:@"%@", [properties objectForKey:key]];

                         Network::Facebook::RequestProperty property;
                         property.key = [key cStringUsingEncoding:NSUTF8StringEncoding];
                         property.value = [value cStringUsingEncoding:NSUTF8StringEncoding];

                         requestData.properties.push_back( property );
                     }
                 }

                 m_observer->NotifyRequestReceived( requestData );
             }
         }
     }];

    return true;
}

- (bool) removeRequest:(NSString*)requestId
{
    FBRequest* request =
        [FBRequest requestWithGraphPath:requestId
                             parameters:nil
                             HTTPMethod:@"DELETE"];

    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
        if ( !error )
        {
            m_observer->NotifyRequestRemoved();
        }
     }];

    return true;
}

- (bool) publishFeed
{
    if( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User not authenticated to Facebook App, can not pubishFeed" );
        return false;
    }

    [self.facebook dialog:@"feed" andDelegate:self];
    return true;
}

- (bool) publishFeedWithParams:(NSMutableDictionary*) params
{
    if( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User not authenticated to Facebook App, can not pubishFeed" );
        return false;
    }

    [self.facebook dialog:@"feed" andParams:params andDelegate:self];
    return true;
}

- (bool) publishImage:(UIImage*)image withMessage:(NSString*)message
{
    if( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User not authenticated to Facebook App, can not pubishFeed" );
        return false;
    }

    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   image, @"picture",
                                   nil];

    if ( message )
    {
        [params setObject:message forKey:@"message"];
    }

    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if ( !error )
                              {
                                  NSString* postId = [result objectForKey:@"post_id"];
                                  m_observer->NotifyImagePublished( [postId cStringUsingEncoding:NSUTF8StringEncoding] );
                              }
                              else
                              {
                                  m_observer->NotifyImagePublished( NULL );
                              }
                          }];

    return true;
}

- (bool) publishAction:(NSString*)graphPath withObjectName:(NSString*)objectName andObjectUrl:(NSString*)objectUrl
{
    if( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User not authenticated to Facebook App, can not pubishFeed" );
        return false;
    }

    id<FBGraphObject> graphObject = [FBGraphObject graphObject];
    [graphObject setObject:objectUrl forKey:@"url"];

    id<FBOpenGraphAction> graphAction = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
    [graphAction setObject:graphObject forKey:objectName];

    [FBRequestConnection startForPostWithGraphPath:graphPath
                                       graphObject:graphAction
                                 completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                     if ( !error ) {
                                         NSString* actionId = [result objectForKey:@"id"];
                                         m_observer->NotifyActionPublished( [actionId cStringUsingEncoding:NSUTF8StringEncoding] );
                                     }
                                     else {
                                         m_observer->NotifyActionPublished( NULL );
                                     }
                                 }];

    return true;
}

- (bool) getScoresWithLimit:(int)limit
{
    if ( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User is not authenticated, cannot get scores" );
        return false;
    }

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   FBSession.activeSession.accessToken, @"access_token",
                                   nil];

    NSMutableString* graphPath = [NSString stringWithFormat:@"%@/scores?fields=score,user.id,user.name,user.picture", [FBSession defaultAppID]];
    if ( limit > 0 )
    {
        [graphPath appendFormat:@"&limit=%d", limit];
    }

    FBRequest* request =
        [FBRequest requestWithGraphPath:graphPath
                             parameters:params
                             HTTPMethod:@"GET"];

    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if ( error )
         {
             NSLog( @"%@", [error description] );
             return;
         }

         std::vector<Network::Facebook::ScoreData> scores;

         NSArray *data = [result objectForKey:@"data"];
         for ( NSDictionary *info in data )
         {
             int score = [[info objectForKey:@"score"] integerValue];

             NSDictionary *user = [info objectForKey:@"user"];
             NSString* uid = [NSString stringWithFormat:@"%lld", [[user objectForKey:@"id"] longLongValue]];
             NSString* username = [user objectForKey:@"name"];
             NSString* picture = [[[user objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];

             Network::Facebook::ScoreData scoreData;
             scoreData.m_score = score;
             scoreData.m_uid = [uid cStringUsingEncoding:NSUTF8StringEncoding];
             scoreData.m_username = [username cStringUsingEncoding:NSUTF8StringEncoding];
             scoreData.m_picture = [picture cStringUsingEncoding:NSUTF8StringEncoding];

             scores.push_back( scoreData );
         }

         m_observer->NotifyScoresReceived( scores );
     }];

    return true;
}

- (bool) sendScore:(int)score
{
    if ( !self.authenticated )
    {
        CLAW_MSG( "[IPhoneFacebookImpl] User is not authenticated, cannot send score" );
        return false;
    }

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   FBSession.activeSession.accessToken, @"access_token",
                                   [NSString stringWithFormat:@"%d", score], @"score",
                                   nil];

    FBRequest* request =
        [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"me/scores"]
                             parameters:params
                             HTTPMethod:@"POST"];

    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if ( !error )
         {
             m_observer->NotifyScoreSent();
         }
     }];

    return true;
}

- (bool) getAvatar:(NSString*)fbId
{
    NSMutableString* graphPath = [NSString stringWithFormat:@"%@/", fbId];

    NSString* pars = @"id,picture.width(200).height(200)";

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   pars, @"fields",
                                      nil];

    FBRequest* request =
    [FBRequest requestWithGraphPath:graphPath
                         parameters:params
                         HTTPMethod:@"GET"];

    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if ( error )
         {
             NSLog( @"%@", [error description] );
             return;
         }

         NSString* uid = [NSString stringWithFormat:@"%lld", [[result objectForKey:@"id"] longLongValue]];
         NSString* picture = [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
             
         Network::Facebook::AvatarData avatarData;
         avatarData.m_uid = [uid cStringUsingEncoding:NSUTF8StringEncoding];
         avatarData.m_picture = [picture cStringUsingEncoding:NSUTF8StringEncoding];

         m_observer->NotifyAvatarsReceived( avatarData );
     }];

    return true;
}

- (void)dialogDidComplete:(FBDialog *)dialog
{
    CLAW_MSG( "[IPhoneFacebookImpl] Facebook dialog completed successfully." );
}

- (void)dialogCompleteWithUrl:(NSURL *)url
{
    CLAW_MSG( "[IPhoneFacebookImpl] Facebook dialog completed successfully with returning URL." );

    NSArray *pairs = [[url query] componentsSeparatedByString:@"&"];
	NSMutableDictionary *params = [[[NSMutableDictionary alloc] init] autorelease];
	for ( NSString *pair in pairs )
    {
		NSArray *kv = [pair componentsSeparatedByString:@"="];
		NSString *val = [[kv objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];

		[params setObject:val forKey:[kv objectAtIndex:0]];
	}

    for ( NSString *paramKey in params )
    {
        if ( [paramKey hasPrefix:@"request"] )
        {
            NSString* requestId = [params objectForKey:paramKey];
            m_observer->NotifyRequestSent( [requestId cStringUsingEncoding:NSASCIIStringEncoding] );
        }
        else if ( [paramKey hasPrefix:@"post_id"] )
        {
            NSString* postId = [params valueForKey:@"post_id"];
            m_observer->NotifyFeedPublished( [postId cStringUsingEncoding:NSASCIIStringEncoding] );
        }
    }
}

- (void)dialogDidNotCompleteWithUrl:(NSURL *)url
{
    CLAW_MSG( "[IPhoneFacebookImpl] Facebook dialog canceled by the user." );
}

- (void)dialogDidNotComplete:(FBDialog *)dialog
{
    CLAW_MSG( "[IPhoneFacebookImpl] Facebook dialog is cancelled and is about to be dismissed." );
}

- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error
{
    CLAW_MSG( "[IPhoneFacebookImpl] Facebook dialog failed to load due to an error." );
}

@end

@implementation IPhoneAppDelegate(FacebookSupport)

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

@end

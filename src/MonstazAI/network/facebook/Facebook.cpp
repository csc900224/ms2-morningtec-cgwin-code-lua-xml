#include <algorithm>

#include "MonstazAI/network/facebook/Facebook.hpp"

namespace Network
{
    bool Facebook::RegisterObserver( Facebook::Observer* observer )
    {
        // Already registered
        if( std::find( m_observers.begin(), m_observers.end(), observer ) != m_observers.end() )
            return false;
        m_observers.push_back( observer );
        return true;
    }

    bool Facebook::UnregisterObserver( Facebook::Observer* observer )
    {
        // Not yet registered
        ObserversIt it = std::find( m_observers.begin(), m_observers.end(), observer );
        if( it == m_observers.end() )
            return false;
        m_observers.erase( it );
        return true;
    }

    bool Facebook::IsAuthenticated() const
    {
        return m_authenticated;
    }

    Facebook::Facebook()
        : m_authenticated( false )
    {
    }

    void Facebook::NotifyAuthenticationChange( bool authenticated )
    {
        m_authenticated = authenticated;

        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnAuthenticationChange( authenticated );
        }
    }

    void Facebook::NotifyFeedPublished( const char* id )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnFeedPublished( id );
        }
    }

    void Facebook::NotifyImagePublished( const char* id )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnImagePublished( id );
        }
    }

    void Facebook::NotifyActionPublished( const char* id )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnActionPublished( id );
        }
    }

    void Facebook::NotifyRequestSent( const char* id )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnRequestSent( id );
        }
    }

    void Facebook::NotifyRequestReceived( const RequestData& appRequest )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnRequestReceived( appRequest );
        }
    }

    void Facebook::NotifyRequestRemoved()
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnRequestRemoved();
        }
    }

    void Facebook::NotifyScoreSent()
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnScoreSent();
        }
    }

    void Facebook::NotifyScoresReceived( std::vector<ScoreData>& scores )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnScoresReceived( scores );
        }
    }

    void Facebook::NotifyAvatarsReceived( AvatarData& avatar )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnAvatarsReceived( avatar );
        }
    }
}

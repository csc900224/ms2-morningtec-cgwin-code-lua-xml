#ifndef __NETWORK_ANDROID_FACEBOOK_HPP__
#define __NETWORK_ANDROID_FACEBOOK_HPP__

#include <jni.h>

#include "libpng/png.h"
#include "libpng/pngstruct.h"

#include "MonstazAI/network/facebook/Facebook.hpp"

class AndroidFacebook : public Network::Facebook
{
public:
    AndroidFacebook( const char* appId );
    ~AndroidFacebook();

    virtual bool Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL );

    virtual bool PublishFeed();
    virtual bool PublishFeed( const FeedData& feed );
    virtual bool PublishImage( const char* pixels, int width, int height, const Claw::NarrowString& message );
    virtual bool PublishAction( const char* graphPath, const char* objectName, const char* objectUrl );

    virtual bool SendRequest( const RequestData& appRequest );
    virtual bool SendRequest( const RequestData& appRequest, const RequestCallbackData& callback );
    virtual bool GetRequests();
    virtual bool RemoveRequest( const char* identifier );

    virtual bool SendScore( int score );
    virtual bool GetScores( int limit );

    virtual Claw::NarrowString GetUserId();
    virtual Claw::NarrowString GetUserName();

    virtual bool GetAvatar( const char* fbId );

    void NotifyAuthenticationChange( bool authenticated );
    void NotifyFeedPublished( const char* identifier );
    void NotifyImagePublished( const char* identifier );
    void NotifyActionPublished( const char* identifier );
    void NotifyRequestSent( const char* identifier );
    void NotifyRequestReceived( const RequestData& appRequest );
    void NotifyRequestRemoved();
    void NotifyScoreSent();
    void NotifyScoresReceived( std::vector<ScoreData>& scores );
    void NotifyAvatarsReceived( AvatarData& avatar );

private:
    jobject m_facebook;

    struct PngBuffer
    {
        char* data;
        int size;
    };

    static void PngCompress( PngBuffer* buffer, const char* image, int width, int height );
    static void PngWrite( png_structp png_ptr, png_bytep data, png_size_t length );

}; // class AndroidFacebook

#endif // !defined __NETWORK_ANDROID_FACEBOOK_HPP__

package com.gamelion;

public class AndroidScoreData
{
    private String mUserId;
    private String mUserName;
    private String mPicture;
    private int mScore;

    public AndroidScoreData( String userId, String userName, String picture, int score )
    {
        mUserId = userId;
        mUserName = userName;
        mPicture = picture;
        mScore = score;
    }

    public String GetUserId()
    {
        return mUserId;
    }

    public String GetUserName()
    {
        return mUserName;
    }

    public String GetPicture()
    {
        return mPicture;
    }

    public int GetScore()
    {
        return mScore;
    }
}
package com.gamelion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.Claw.Android.ClawActivity;
import com.Claw.Android.ClawActivityCommon;
import com.Claw.Android.ClawActivityListener;

import com.facebook.android.*;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.Facebook.ServiceListener;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

public class AndroidFacebook implements ClawActivityListener
{
    public static AndroidFacebook sInstance;

    Facebook mFacebook;
    AsyncFacebookRunner mAsyncRunner;

    private String mUserId = "";
    private String mUserName = "";

    private SharedPreferences mPreferences;

    public AndroidFacebook( final String appId ) {
        sInstance = this;

        mFacebook = new Facebook( appId );
        mAsyncRunner = new AsyncFacebookRunner( mFacebook );

        mPreferences = ClawActivityCommon.mActivity.getPreferences( Activity.MODE_PRIVATE );

        String accessToken = mPreferences.getString( "fb_access_token", null );
        if ( accessToken != null) {
            mFacebook.setAccessToken( accessToken );
        }

        long accessExpires = mPreferences.getLong( "fb_access_expires", 0 );
        if ( accessExpires != 0 ) {
            mFacebook.setAccessExpires( accessExpires );
        }

        ClawActivityCommon.AddListener( this );
    }

    public void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        mFacebook.authorizeCallback( requestCode, resultCode, data );
    }

    public void authenticate( boolean allowLoginUi ) {
        if ( mFacebook.isSessionValid() )
        {
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    onSessionStateChanged();

                    final Runnable extendTokenRunnable = new Runnable() {
                        @Override
                        public void run() {
                            ServiceListener listener = new ServiceListener() {
                                public void onComplete(Bundle values)
                                {
                                    onSessionStateChanged();
                                }

                                public void onFacebookError(FacebookError e)
                                {
                                    onSessionStateChanged();
                                }

                                public void onError(Error e)
                                {
                                    onSessionStateChanged();
                                }
                            };

                            mFacebook.extendAccessTokenIfNeeded( ClawActivityCommon.mActivity, listener );
                        }
                    };

                    ClawActivityCommon.mActivity.runOnUiThread( extendTokenRunnable );
                }
            };

            Thread thread = new Thread( runnable );
            thread.start();
        }
        else if ( allowLoginUi )
        {
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    DialogListener listener = new DialogListener()
                    {
                        @Override
                        public void onComplete( Bundle values ) {
                            onSessionStateChanged();
                        }

                        @Override
                        public void onFacebookError( FacebookError error ) {
                            onSessionStateChanged();
                            sInstance.showError( error.getMessage() );
                        }

                        @Override
                        public void onError( DialogError error ) {
                            onSessionStateChanged();
                            sInstance.showError( error.getMessage() );
                        }

                        @Override
                        public void onCancel() {
                            onSessionStateChanged();
                        }
                    };

                    mFacebook.authorize( ClawActivityCommon.mActivity, new String[] { "publish_actions" }, listener );
                }
            };

            ClawActivityCommon.mActivity.runOnUiThread( runnable );
        }
        else
        {
            onAuthenticationChanged( false );
        }
    }

    public void sendRequest( final AndroidRequestData request )
    {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Bundle params = new Bundle();
                params.putString( "message", request.message );

                if ( request.userId != null )
                {
                    params.putString( "to", request.userId );
                }

                if ( request.properties != null )
                {
                    try {
                        JSONObject properties = new JSONObject();
                        for ( int i = 0; i < request.properties.length; ++i )
                        {
                            properties.putOpt( request.properties[i].key, request.properties[i].value );
                        }

                        params.putString( "data", properties.toString() );
                    }
                    catch ( JSONException e ) {
                        Log.d( "AndroidFacebook", e.toString() );
                    }
                }

                DialogListener listener = new DialogListener()
                {
                    @Override
                    public void onComplete( Bundle values ) {
                        String request = values.getString( "request" );
                        if ( request != null )
                        {
                            onRequestSent( request );
                        }
                    }

                    @Override
                    public void onFacebookError( FacebookError error ) {}

                    @Override
                    public void onError( DialogError error ) {}

                    @Override
                    public void onCancel() {}
                };

                mFacebook.dialog( ClawActivityCommon.mActivity, "apprequests", params, listener );
            }
        };

        ClawActivityCommon.mActivity.runOnUiThread( runnable );
    }

    public void sendRequest( final AndroidRequestData request, final AndroidRequestCallback callback )
    {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Bundle params = new Bundle();
                params.putString( "message", request.message );

                if ( request.userId != null )
                {
                    params.putString( "to", request.userId );
                }

                if ( request.properties != null )
                {
                    try {
                        JSONObject properties = new JSONObject();
                        for ( int i = 0; i < request.properties.length; ++i )
                        {
                            properties.putOpt( request.properties[i].key, request.properties[i].value );
                        }

                        params.putString( "data", properties.toString() );
                    }
                    catch ( JSONException e ) {
                        Log.d( "AndroidFacebook", e.toString() );
                    }
                }

                DialogListener listener = new DialogListener()
                {
                    @Override
                    public void onComplete( Bundle values ) {
                        String request = values.getString( "request" );
                        if ( request != null )
                        {
                            onRequestSentCallback( request, callback );
                        }
                    }

                    @Override
                    public void onFacebookError( FacebookError error ) {}

                    @Override
                    public void onError( DialogError error ) {}

                    @Override
                    public void onCancel() {}
                };

                mFacebook.dialog( ClawActivityCommon.mActivity, "apprequests", params, listener );
            }
        };

        ClawActivityCommon.mActivity.runOnUiThread( runnable );
    }

    public void getRequests()
    {
        Bundle params = new Bundle();
        params.putString( "access_token", mFacebook.getAccessToken() );

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state )
            {
                try {
                    JSONObject json = new JSONObject( response );
                    JSONArray data = json.getJSONArray( "data" );

                    for ( int i = 0; i < data.length(); ++i ) {
                        JSONObject request = data.getJSONObject( i );

                        final String id = request.getString( "id" );
                        final String message = request.getString( "message" );

                        JSONObject from = request.getJSONObject( "from" );
                        final String userId = from.getString( "id" );
                        final String userName = from.getString( "name" );

                        AndroidRequestData requestData = new AndroidRequestData();
                        requestData.id = id;
                        requestData.userId = userId;
                        requestData.userName = userName;
                        requestData.message = message;

                        if ( request.has( "data" ) )
                        {
                            JSONObject dataObject = new JSONObject( request.getString( "data" ) );
                            requestData.properties = new AndroidRequestProperty[dataObject.length()];

                            Iterator it = dataObject.keys();
                            for ( int j = 0; it.hasNext(); ++j )
                            {
                                AndroidRequestProperty property = new AndroidRequestProperty();
                                property.key = (String)it.next();
                                property.value = dataObject.getString( property.key );

                                requestData.properties[j] = property;
                            }
                        }

                        onRequestReceived( requestData );
                    }
                }
                catch ( JSONException e ) {
                    Log.d( "AndroidFacebook", e.toString() );
                }
            }

            @Override
            public void onIOException( IOException e, Object state ) {}

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {}

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {}

            @Override
            public void onFacebookError( FacebookError e, Object state ) {}
        };

        mAsyncRunner.request( "me/apprequests", params, "GET", listener, null );
    }

    public void removeRequest( String requestId )
    {
        Bundle params = new Bundle();
        params.putString( "access_token", mFacebook.getAccessToken() );

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state ) {
                onRequestRemoved();
            }

            @Override
            public void onIOException( IOException e, Object state ) {}

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {}

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {}

            @Override
            public void onFacebookError( FacebookError e, Object state ) {}
        };

        mAsyncRunner.request( requestId, params, "DELETE", listener, null );
    }

    public void getScores( int limit )
    {
        Bundle params = new Bundle();
        params.putString( "access_token", mFacebook.getAccessToken() );
        params.putString( "fields", "score,user.id,user.name,user.picture" );

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state )
            {
                try {
                    JSONObject json = new JSONObject( response );
                    JSONArray data = json.getJSONArray( "data" );

                    AndroidScoreData[] scores = new AndroidScoreData[data.length()];
                    for ( int i = 0; i < data.length(); ++i ) {
                        JSONObject entry = data.getJSONObject( i );

                        JSONObject user = entry.getJSONObject( "user" );
                        final String userId = user.getString( "id" );
                        final String userName = user.getString( "name" );
                        final String picture = user.getJSONObject( "picture" ).getJSONObject( "data" ).getString( "url" );

                        final int score = entry.getInt( "score" );

                        scores[i] = new AndroidScoreData( userId, userName, picture, score );
                    }

                    onScoresReceived( scores );
                }
                catch ( JSONException e ) {
                    Log.d( "AndroidFacebook", e.toString() );
                }
            }

            @Override
            public void onIOException( IOException e, Object state ) {}

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {}

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {}

            @Override
            public void onFacebookError( FacebookError e, Object state ) {}
        };

        String path = mFacebook.getAppId() + "/scores";
        mAsyncRunner.request( path, params, "GET", listener, null );
    }

    public void sendScore( int score )
    {
        Bundle params = new Bundle();
        params.putString( "score", Integer.toString( score ) );
        params.putString( "access_token", mFacebook.getAccessToken() );

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state ) {
                onScoreSent();
            }

            @Override
            public void onIOException( IOException e, Object state ) {}

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {}

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {}

            @Override
            public void onFacebookError( FacebookError e, Object state ) {}
        };

        mAsyncRunner.request( "me/scores", params, "POST", listener, null );
    }

    public void publishImage( final byte[] pixels, final String message )
    {
        Bundle params = new Bundle();
        params.putByteArray( "source", pixels );
        params.putString( "message", message );

        // Bundle params = new Bundle();
        // params.putString("name", "Pick-A-Gem");
        // params.putString("link", "http://www.game-lion.com/pag/index.php");
        // params.putString("picture", "http://game-lion.net/pag/icon.png");
        // params.putString("caption", "New highscore");
        // params.putString("description", message);

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state )
            {
                try {
                    JSONObject json = new JSONObject( response );
                    onImagePublished( json.getString( "post_id" ) );
                }
                catch ( JSONException e ) {
                    onImagePublished( "" );
                }
            }

            @Override
            public void onIOException( IOException e, Object state ) {
                onImagePublished( "" );
            }

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {
                onImagePublished( "" );
            }

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {
                onImagePublished( "" );
            }

            @Override
            public void onFacebookError( FacebookError e, Object state ) {
                onImagePublished( "" );
            }
        };

        mAsyncRunner.request( "me/photos", params, "POST", listener, null );
    }

    public void publishAction( final String graphPath, final String objectName, final String objectUrl )
    {
        Bundle params = new Bundle();
        params.putString( objectName, objectUrl );

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state )
            {
                try {
                    JSONObject json = new JSONObject( response );
                    Log.d( "AndroidFacebookAction", response );
                    onActionPublished( json.getString( "id" ) );
                }
                catch ( JSONException e ) {
                    Log.d( "AndroidFacebookAction", e.toString() );
                    onActionPublished( "" );
                }
            }

            @Override
            public void onIOException( IOException e, Object state ) {
                onActionPublished( "" );
            }

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {
                onActionPublished( "" );
            }

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {
                onActionPublished( "" );
            }

            @Override
            public void onFacebookError( FacebookError e, Object state ) {
                onActionPublished( "" );
            }
        };

        mAsyncRunner.request( graphPath, params, "POST", listener, null );
    }

    public String getUserId()
    {
        return mUserId;
    }

    public String getUserName()
    {
        return mUserName;
    }

    public void onSessionStateChanged()
    {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString( "fb_access_token", mFacebook.getAccessToken() );
        editor.putLong( "fb_access_expires", mFacebook.getAccessExpires() );
        editor.commit();

        // Get current user data
        if ( mFacebook.isSessionValid() )
        {
            try {
                JSONObject me = new JSONObject( mFacebook.request( "me" ) );
                mUserId = me.getString( "id" );
                mUserName = me.getString( "name" );
            }
            catch ( Exception e ) {
                Log.d( "AndroidFacebook", e.toString() );
            }
        }
        else
        {
            mUserId = "";
            mUserName = "";
        }

        Runnable runnable = new Runnable() {
            public void run() {
                onAuthenticationChanged( mFacebook.isSessionValid() );
            }
        };

        ClawActivityCommon.mActivity.runOnUiThread( runnable );
    }

    void showError( final String message )
    {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder( ClawActivityCommon.mActivity );
                builder.setTitle( "Error" );
                builder.setMessage( message );
                builder.setCancelable( false );
                builder.setPositiveButton( "OK", null );
                builder.create().show();
            }
        };

        ClawActivityCommon.mActivity.runOnUiThread( runnable );
    }

    public void getAvatar( String fbId )
    {
        Bundle params = new Bundle();
        params.putString( "fields", "id,picture.width(200).height(200)" );

        RequestListener listener = new RequestListener()
        {
            @Override
            public void onComplete( String response, Object state )
            {
                try {
                    JSONObject avData = new JSONObject( response );

                    final String userId = avData.getString( "id" );
                    final String picture = avData.getJSONObject( "picture" ).getJSONObject( "data" ).getString( "url" );

                    AndroidAvatarData avatar  = new AndroidAvatarData( userId, picture );

                    onAvatarsReceived( avatar );
                }
                catch ( JSONException e ) {
                    Log.d( "AndroidFacebook", e.toString() );
                }
            }

            @Override
            public void onIOException( IOException e, Object state ) {}

            @Override
            public void onFileNotFoundException( FileNotFoundException e, Object state ) {}

            @Override
            public void onMalformedURLException( MalformedURLException e, Object state ) {}

            @Override
            public void onFacebookError( FacebookError e, Object state ) {}
        };

        String path = mFacebook.getAppId();
        mAsyncRunner.request( fbId, params, "GET", listener, null );
    }

    private static native void onAuthenticationChanged( boolean authenticated );
    private static native void onImagePublished( String identifier );
    private static native void onActionPublished( String identifier );
    private static native void onRequestSent( String identifier );
    private static native void onRequestSentCallback( String identifier, AndroidRequestCallback callback );
    private static native void onRequestReceived( AndroidRequestData request );
    private static native void onRequestRemoved();
    private static native void onScoresReceived( AndroidScoreData[] scores );
    private static native void onScoreSent();
    private static native void onAvatarsReceived( AndroidAvatarData avatar );
}

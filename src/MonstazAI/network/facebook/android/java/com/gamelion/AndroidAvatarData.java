package com.gamelion;

public class AndroidAvatarData
{
    public String mUserId;
    public String mPicture;

    public AndroidAvatarData( String userId, String picture )
    {
        mUserId = userId;
        mPicture = picture;
    }

    public String GetUserId()
    {
        return mUserId;
    }

    public String GetPicture()
    {
        return mPicture;
    }
}
#include <algorithm>

#include "MonstazAI/network/googleservices/GoogleServices.hpp"
#include "claw/base/String.hpp"

namespace Network
{
    bool GoogleServices::RegisterObserver( GoogleServices::Observer* observer )
    {
        if( std::find( m_observers.begin(), m_observers.end(), observer ) != m_observers.end() )
            return false;
        m_observers.push_back( observer );
        return true;
    }

    bool GoogleServices::UnregisterObserver( GoogleServices::Observer* observer )
    {
        ObserversIt it = std::find( m_observers.begin(), m_observers.end(), observer );
        if( it == m_observers.end() )
            return false;
        m_observers.erase( it );
        return true;
    }

    bool GoogleServices::IsAuthenticated() const
    {
        return m_authenticated;
    }

    GoogleServices::GoogleServices()
        : m_authenticated( false )
    {
    }

    void GoogleServices::NotifyAuthenticationChange( bool authenticated )
    {
        m_authenticated = authenticated;

        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnGSAuthenticationChange( authenticated );
        }
    }

    void GoogleServices::NotifyAvatarsReceived( const AvatarData& avatar )
    {
        AvatarData notify( avatar );
        if( m_imageSizes.find( avatar.m_uid ) != m_imageSizes.end() )
        {
            Claw::StdOStringStream ostr;
            ostr << notify.m_picture << "?sz=" << m_imageSizes[ avatar.m_uid ];
            notify.m_picture = ostr.m_str;
            m_imageSizes.erase( avatar.m_uid );
        }

        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnGSAvatarReceived( notify );
        }
    }

    bool GoogleServices::GetAvatar( const char* googleId, int size )
    {
        if( size > 0 )
        {
            Claw::NarrowString gId( googleId );
            if( gId == "me" )
            {
                gId = GetUserId();
            }
            m_imageSizes[ gId ] = size;
        }
        return GetAvatarImpl( googleId );
    }
}

#ifndef __NETWORK_ANDROID_GOOGLESERVICES_HPP__
#define __NETWORK_ANDROID_GOOGLESERVICES_HPP__

#include "MonstazAI/network/googleservices/GoogleServices.hpp"

class DummyGoogleServices : public Network::GoogleServices
{
public:
    DummyGoogleServices();
    ~DummyGoogleServices();

    virtual bool Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL );

    virtual Claw::NarrowString GetUserId();
    virtual Claw::NarrowString GetUserName();

    virtual bool GetAvatarImpl( const char* fbId );
    virtual void UnlockAchievement( const char* achievementId );
    virtual void OpenAchievementsUI();

    void NotifyAuthenticationChange( bool authenticated );
    void NotifyAvatarsReceived( const AvatarData& avatar );

}; // class AndroidGoogleServices

#endif // !defined __NETWORK_ANDROID_FACEBOOK_HPP__

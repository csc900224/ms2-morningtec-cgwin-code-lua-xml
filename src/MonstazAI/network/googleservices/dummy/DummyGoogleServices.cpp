#include "MonstazAI/network/googleservices/dummy/DummyGoogleServices.hpp"

#include "claw/base/Errors.hpp"

static DummyGoogleServices* s_instance = NULL;

namespace Network
{
    GoogleServices* GoogleServices::QueryInterface()
    {
        // Force singleton pattern
        CLAW_ASSERT( s_instance == NULL );
        s_instance = new DummyGoogleServices();
        return s_instance;
    }

    void GoogleServices::Release( GoogleServices* instance )
    {
        CLAW_ASSERT( s_instance );
        s_instance = NULL;
        delete instance;
    }
}

DummyGoogleServices::DummyGoogleServices()
{
}

DummyGoogleServices::~DummyGoogleServices()
{
}

bool DummyGoogleServices::Authenticate( bool allowLoginUi, Claw::NarrowString* err )
{
    if( !m_authenticated )
    {
        m_authenticated = true;
        NotifyAuthenticationChange( true );
    }
    return true;
}

Claw::NarrowString DummyGoogleServices::GetUserId()
{
    if( IsAuthenticated() )
    {
        return "105261834817106912888";
    }
    return "";
}

Claw::NarrowString DummyGoogleServices::GetUserName()
{
    if( IsAuthenticated() )
    {
        return "Dziki kalosz";
    }
    return "";
}

bool DummyGoogleServices::GetAvatarImpl( const char* googleId )
{
    AvatarData avatar;
    avatar.m_picture = "http://profiles.google.com/s2/photos/profile/";
    avatar.m_picture += googleId;

    avatar.m_uid = googleId;
    NotifyAvatarsReceived( avatar );

    return true;
}

void DummyGoogleServices::UnlockAchievement( const char* achievementId )
{
    CLAW_MSG( "Achievement " << achievementId << " unlocked!" );
}

void DummyGoogleServices::OpenAchievementsUI()
{
}

void DummyGoogleServices::NotifyAuthenticationChange( bool authenticated )
{
    GoogleServices::NotifyAuthenticationChange( authenticated );
}

void DummyGoogleServices::NotifyAvatarsReceived( const AvatarData& avatar )
{
    GoogleServices::NotifyAvatarsReceived( avatar );
}

#ifndef __NETWORK_GOOLGESERVICES_HPP__
#define __NETWORK_GOOLGESERVICES_HPP__

#include "claw/base/String.hpp"

#include <map>
#include <vector>

namespace Network
{
    class GoogleServices
    {
    public:
        class Observer;

        struct AvatarData
        {
            Claw::NarrowString m_uid;
            Claw::NarrowString m_picture;
        };

        virtual ~GoogleServices() {}

        //! Register GoogleServices observer.
        /**
         * \return false if observer was already registered, true if successfully registered.
         */
        bool RegisterObserver( Observer* observer );

        //! Unregister GoogleServices observer.
        /**
         * \return false if observer was not registered.
         */
        bool UnregisterObserver( Observer* observer );

        //! Check authentication status
        bool IsAuthenticated() const;

        //! Try to authenticate and login to Google+
        /**
        * Log in and grant application all neccessary permissions.
        * The result of authentication is send via OnGSAuthenticationChange() method to Observer
        * objects registered in.
        */
        virtual bool Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL ) = 0;

        //! Get Avatar information. Size may be specified.
        /**
         * Resonse returned via observer callback.
         */
        bool GetAvatar( const char* fbId, int size = -1 );

        virtual void UnlockAchievement( const char* achievementId ) = 0;
        virtual void OpenAchievementsUI() = 0;

        virtual Claw::NarrowString GetUserId() = 0;
        virtual Claw::NarrowString GetUserName() = 0;

        //! Retreive platfrom dependend implementation.
        /**
         * This method should be implemented and linked once in platform dependend object,
         * returning appropriate GoogleServices object implementation.
         */
        static GoogleServices* QueryInterface();

        static void Release( GoogleServices* fb );

        class Observer
        {
        public:
            //! Called in response to change in player authentication.
            virtual void OnGSAuthenticationChange( bool authenticated ) {}

            //! Called after successfully received list of avatars
            virtual void OnGSAvatarReceived( const AvatarData& avatar ) {}
        };

    protected:
        GoogleServices();

        virtual bool GetAvatarImpl( const char* fbId ) = 0;

        void NotifyAuthenticationChange( bool authenticated );
        void NotifyAvatarsReceived( const AvatarData& avatar );

        typedef std::vector<Observer*> Observers;
        typedef Observers::iterator ObserversIt;
        typedef Observers::const_iterator ObserversConstIt;

        typedef std::map<Claw::NarrowString, int> RequestedImageSize;
        RequestedImageSize m_imageSizes;

        Observers m_observers;
        bool m_authenticated;
    };
}

#endif

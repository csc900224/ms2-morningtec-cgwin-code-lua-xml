#include "MonstazAI/network/googleservices/android/AndroidGoogleServices.hpp"

#include "claw/base/Errors.hpp"
#include "claw/system/android/JniAttach.hpp"

static AndroidGoogleServices* s_instance = NULL;

namespace Network
{

    GoogleServices* GoogleServices::QueryInterface()
    {
        // Force singleton pattern
        CLAW_ASSERT( s_instance == NULL );
        s_instance = new AndroidGoogleServices();
        return s_instance;
    }

    void GoogleServices::Release( GoogleServices* instance )
    {
        CLAW_ASSERT( s_instance );
        s_instance = NULL;
        delete instance;
    }

}

AndroidGoogleServices::AndroidGoogleServices()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "<init>", "()V" );
    CLAW_ASSERT( mid != NULL );

    jobject jgooglesrv = env->NewObject( cls, mid );
    CLAW_ASSERT( jgooglesrv != NULL );

    m_googleServices = env->NewGlobalRef( jgooglesrv );
    CLAW_ASSERT( m_googleServices != NULL );

    env->DeleteLocalRef( jgooglesrv );

    Claw::JniAttach::Detach( isAttached );
}

AndroidGoogleServices::~AndroidGoogleServices()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    env->DeleteGlobalRef( m_googleServices );

    Claw::JniAttach::Detach( isAttached );
}

bool AndroidGoogleServices::Authenticate( bool allowLoginUi, Claw::NarrowString* err )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "authenticate", "(Z)V" );
    CLAW_ASSERT( mid != NULL );

    env->CallVoidMethod( m_googleServices, mid, allowLoginUi );

    Claw::JniAttach::Detach( isAttached );

    return true;
}

void AndroidGoogleServices::PngCompress( PngBuffer* buffer, const char* image, int width, int height )
{
    CLAW_ASSERT( buffer );

    png_structp png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    png_infop info_ptr = png_create_info_struct( png_ptr );
    setjmp( png_jmpbuf( png_ptr ) );

    png_set_write_fn( png_ptr, buffer, &AndroidGoogleServices::PngWrite, NULL );
    png_set_IHDR( png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE );
    png_write_info( png_ptr, info_ptr );

    const char* ptr = image;
    for( int i=0; i<height; i++ )
    {
        png_write_rows( png_ptr, (png_bytepp)(&ptr), 1 );
        ptr += width*4;
    }

    png_write_end( png_ptr, info_ptr );
    png_destroy_write_struct( &png_ptr, &info_ptr );
}

void AndroidGoogleServices::PngWrite( png_structp png_ptr, png_bytep data, png_size_t length )
{
    PngBuffer* buffer = static_cast<PngBuffer*>( png_ptr->io_ptr );
    if ( !buffer->data )
    {
        buffer->data = (char*)malloc( length );
    }
    else
    {
        buffer->data = (char*)realloc( buffer->data, buffer->size + length );
    }

    memcpy( buffer->data + buffer->size, data, length );
    buffer->size += length;
}

Claw::NarrowString AndroidGoogleServices::GetUserId()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getUserId", "()Ljava/lang/String;" );
    CLAW_ASSERT( mid != NULL );

    jobject result = env->CallObjectMethod( m_googleServices, mid );
    CLAW_ASSERT( result != NULL );

    const char* uid = env->GetStringUTFChars( (jstring)result, 0 );
    Claw::NarrowString ret( uid );
    env->ReleaseStringUTFChars( (jstring)result, uid );

    return ret;
}

Claw::NarrowString AndroidGoogleServices::GetUserName()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getUserName", "()Ljava/lang/String;" );
    CLAW_ASSERT( mid != NULL );

    jobject result = env->CallObjectMethod( m_googleServices, mid );
    CLAW_ASSERT( result != NULL );

    const char* uid = env->GetStringUTFChars( (jstring)result, 0 );
    Claw::NarrowString ret( uid );
    env->ReleaseStringUTFChars( (jstring)result, uid );

    return ret;
}

bool AndroidGoogleServices::GetAvatarImpl( const char* googleId )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "getAvatar", "(Ljava/lang/String;)V" );
    CLAW_ASSERT( mid != NULL );
    jstring gId = Claw::JniAttach::GetStringFromChars( env, googleId );
    env->CallVoidMethod( m_googleServices, mid, gId );
    Claw::JniAttach::ReleaseString( env, gId );

    Claw::JniAttach::Detach( isAttached );
    return true;
}

void AndroidGoogleServices::UnlockAchievement( const char* achievementId )
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "unlockAchievement", "(Ljava/lang/String;)V" );
    CLAW_ASSERT( mid != NULL );
    jstring aId = Claw::JniAttach::GetStringFromChars( env, achievementId );
    env->CallVoidMethod( m_googleServices, mid, aId );
    Claw::JniAttach::ReleaseString( env, aId );

    Claw::JniAttach::Detach( isAttached );
}

void AndroidGoogleServices::OpenAchievementsUI()
{
    JNIEnv* env;
    bool isAttached = Claw::JniAttach::Attach( &env );

    jclass cls = env->FindClass( "com/gamelion/AndroidGoogleServices" );
    CLAW_ASSERT( cls != NULL );

    jmethodID mid = env->GetMethodID( cls, "openAchievementsUI", "()V" );
    CLAW_ASSERT( mid != NULL );
    env->CallVoidMethod( m_googleServices, mid );

    Claw::JniAttach::Detach( isAttached );
}

void AndroidGoogleServices::NotifyAuthenticationChange( bool authenticated )
{
    GoogleServices::NotifyAuthenticationChange( authenticated );
}

void AndroidGoogleServices::NotifyAvatarsReceived( const AvatarData& avatar )
{
    GoogleServices::NotifyAvatarsReceived( avatar );
}

extern "C"
{
    JNIEXPORT void JNICALL Java_com_gamelion_AndroidGoogleServices_onAuthenticationChanged( JNIEnv* env, jclass clazz, jboolean authenticated )
    {
        CLAW_ASSERT( s_instance );
        CLAW_MSG( "AuthenticationChanged: " << authenticated );
        s_instance->NotifyAuthenticationChange( authenticated );
    }

    JNIEXPORT void JNICALL Java_com_gamelion_AndroidGoogleServices_onAvatarsReceived( JNIEnv* env, jclass clazz, jstring personId, jstring avatarUrl )
    {
        Network::GoogleServices::AvatarData avatarData;
        // Get user ID
        {
            const char* uid = env->GetStringUTFChars( personId, 0 );
            avatarData.m_uid = uid;
            env->ReleaseStringUTFChars( personId, uid );
        }

        // Get profile picture
        {
            const char* picture = env->GetStringUTFChars( avatarUrl, 0 );
            avatarData.m_picture = picture;
            env->ReleaseStringUTFChars( avatarUrl, picture );
        }

        CLAW_ASSERT( s_instance );
        s_instance->NotifyAvatarsReceived( avatarData );
    }

}

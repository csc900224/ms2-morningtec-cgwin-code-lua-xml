#ifndef __NETWORK_ANDROID_GOOGLESERVICES_HPP__
#define __NETWORK_ANDROID_GOOGLESERVICES_HPP__

#include <jni.h>

#include "libpng/png.h"
#include "libpng/pngstruct.h"

#include "MonstazAI/network/googleservices/GoogleServices.hpp"

class AndroidGoogleServices : public Network::GoogleServices
{
public:
    AndroidGoogleServices();
    ~AndroidGoogleServices();

    virtual bool Authenticate( bool allowLoginUi, Claw::NarrowString* err = NULL );

    virtual Claw::NarrowString GetUserId();
    virtual Claw::NarrowString GetUserName();

    virtual bool GetAvatarImpl( const char* fbId );
    virtual void UnlockAchievement( const char* achievementId );
    virtual void OpenAchievementsUI();

    void NotifyAuthenticationChange( bool authenticated );
    void NotifyAvatarsReceived( const AvatarData& avatar );

private:
    jobject m_googleServices;

    struct PngBuffer
    {
        char* data;
        int size;
    };

    static void PngCompress( PngBuffer* buffer, const char* image, int width, int height );
    static void PngWrite( png_structp png_ptr, png_bytep data, png_size_t length );

}; // class AndroidGoogleServices

#endif

package com.gamelion;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.Claw.Android.ClawActivityCommon;
import com.Claw.Android.ClawActivityListener;

import com.google.android.gms.appstate.AppStateClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.games.OnSignOutCompleteListener;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.people.Person;

import com.gamelion.GameHelper;
import com.gamelion.GameHelper.GameHelperListener;

import java.net.URL;

public class AndroidGoogleServices implements ClawActivityListener, GameHelperListener, PlusClient.OnPersonLoadedListener
{
    public static AndroidGoogleServices sInstance;

    private static final int REQUEST_ACHIEVEMENTS_UI = 10001;

    GameHelper mGoogleServices;

    private String mUserId = "";
    private String mUserName = "";

    public AndroidGoogleServices()
    {
        sInstance = this;

        mGoogleServices = new GameHelper( ClawActivityCommon.mActivity );
        mGoogleServices.setup( this, GameHelper.CLIENT_GAMES | GameHelper.CLIENT_PLUS );

        ClawActivityCommon.AddListener( this );
    }

    public void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if( requestCode != REQUEST_ACHIEVEMENTS_UI )
        {
            mGoogleServices.onActivityResult( requestCode, resultCode, data );
        }
    }

    public void authenticate( boolean allowLoginUi )
    {
        if ( mGoogleServices.isSignedIn() )
        {
            onSessionStateChanged();
        }
        else if ( allowLoginUi )
        {
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    mGoogleServices.beginUserInitiatedSignIn();
                }
            };

            ClawActivityCommon.mActivity.runOnUiThread( runnable );
        }
        else if ( !allowLoginUi )
        {
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    mGoogleServices.onStart( ClawActivityCommon.mActivity );
                }
            };

            ClawActivityCommon.mActivity.runOnUiThread( runnable );
        }
        else
        {
            onSessionStateChanged();
        }
    }


    public void onSignInFailed()
    {
        onSessionStateChanged();
    }

    public void onSignInSucceeded()
    {
        onSessionStateChanged();
    }

    public String getUserId()
    {
        return mUserId;
    }

    public String getUserName()
    {
        return mUserName;
    }

    public void onSessionStateChanged()
    {
        if ( mGoogleServices.isSignedIn() && mGoogleServices.getPlusClient().getCurrentPerson() != null )
        {
            mUserId = mGoogleServices.getPlusClient().getCurrentPerson().getId();
            mUserName = mGoogleServices.getPlusClient().getCurrentPerson().getDisplayName();
        }
        else
        {
            mUserId = "";
            mUserName = "";
        }

        Runnable runnable = new Runnable() {
            public void run() {
                onAuthenticationChanged( mGoogleServices.isSignedIn() );
            }
        };

        ClawActivityCommon.mActivity.runOnUiThread( runnable );
    }

    public void getAvatar( String userId )
    {
        if( mGoogleServices.isSignedIn() )
        {
            mGoogleServices.getPlusClient().loadPerson( this, userId );
        }
        else
        {
            onAvatarsReceived( userId, "http://profiles.google.com/s2/photos/profile/" + userId );
        }
    }

    public void unlockAchievement( String aid )
    {
        mGoogleServices.getGamesClient().unlockAchievement( aid );
    }

    public void openAchievementsUI()
    {
        ClawActivityCommon.mActivity.startActivityForResult( mGoogleServices.getGamesClient().getAchievementsIntent(), REQUEST_ACHIEVEMENTS_UI );
    }

    @Override
    public void onPersonLoaded(ConnectionResult status, Person person)
    {
        String url = person.getImage().getUrl();
        try
        {
            URL u = new URL( person.getImage().getUrl() );
            url = "http://" + u.getAuthority() + u.getPath();
        }
        catch( java.net.MalformedURLException e )
        {
        }
        String id = person.getId();
        onAvatarsReceived( id, url );
    }

    private static native void onAuthenticationChanged( boolean authenticated );
    private static native void onAvatarsReceived( String avatarId, String avatarUrl );
}

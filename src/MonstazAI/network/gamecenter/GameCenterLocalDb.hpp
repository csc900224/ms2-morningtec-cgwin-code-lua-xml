//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/GameCenterLocalDb.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_GAME_CENTER_LOCAL_DB_HPP__
#define __NETWORK_GAME_CENTER_LOCAL_DB_HPP__

// Internal includes
#include "MonstazAI/network/gamecenter/GameCenter.hpp"

// External includes
#include "claw/base/String.hpp"

#include <map>

class GameCenterLocalDb
{
public:
    bool            StashAchivement( const GameCenter::UserId& playerId, const GameCenter::Achievement& ach );

    bool            StashScore( const GameCenter::UserId& playerId, const GameCenter::Score& sc );

    bool            ContainsAchievement( const GameCenter::UserId& playerId, const GameCenter::Achievement& ach ) const;

    bool            ContainsScore( const GameCenter::UserId& playerId, const GameCenter::Score& sc ) const;

    bool            SyncRequest( GameCenter* gc );

    virtual bool    Serialize() const = 0;

    virtual bool    Deserialize() = 0;

private:
    typedef std::map< GameCenter::Achievement::Id, GameCenter::Achievement::Value > PlayerAchievements;
    typedef std::map< GameCenter::UserId, PlayerAchievements > Achievements;

    typedef std::map< GameCenter::Score::Id, GameCenter::Score::Value > PlayerScores;
    typedef std::map< GameCenter::UserId, PlayerScores > Scores;

    bool            SyncAchievements( GameCenter* gc );

    bool            SyncScores( GameCenter* gc );

    Achievements    m_achievements;
    Scores          m_scores;
}; // GameCenterDb

#endif // !defined __NETWORK_GAME_CENTER_LOCAL_DB_HPP__
// EOF

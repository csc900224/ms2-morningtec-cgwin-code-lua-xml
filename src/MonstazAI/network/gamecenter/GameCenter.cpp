//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/GameCenter.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////


// Internal includes
#include "MonstazAI/network/gamecenter/GameCenter.hpp"

// External includes
#include <algorithm>    // std::find

GameCenter::GameCenter()
    : m_authenticated( false )
{
}

bool GameCenter::RegisterObserver( GameCenter::Observer* observer )
{
    // Already registered
    if( std::find( m_observers.begin(), m_observers.end(), observer ) != m_observers.end() )
        return false;
    m_observers.push_back( observer );
    return true;
}

bool GameCenter::UnregisterObserver( GameCenter::Observer* observer )
{
    // Not yet registered
    ObserversIt it = std::find( m_observers.begin(), m_observers.end(), observer );
    if( it == m_observers.end() )
        return false;
    m_observers.erase( it );
    return true;
}

void GameCenter::NotifyAchievementsLoad( const Achievements& achievements )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnAchievementsLoad( achievements );
    }
}

void GameCenter::NotifyAuthenticationChange( bool authenticated )
{
    m_authenticated = authenticated;

    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnAuthenticationChange( authenticated );
    }
}

void GameCenter::NotifyLeaderboardView( bool opened )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnLeaderboardView( opened );
    }
}

void GameCenter::NotifyAchievementsView( bool opened )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();
    for( ; it != end; ++it )
    {
        (*it)->OnAchievementsView( opened );
    }
}

// EOF

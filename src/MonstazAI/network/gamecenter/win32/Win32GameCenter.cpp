//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/win32/Win32GameCenter.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "MonstazAI/network/gamecenter/win32/Win32GameCenter.hpp"
#include "claw/base/Errors.hpp"

static Win32GameCenter* s_instance = NULL;

GameCenter* GameCenter::QueryInterface( GameCenter::Type type )
{
    // Force singleton pattern
    CLAW_ASSERT( s_instance == NULL );

    switch ( type )
    {
        case GameCenter::Default:
            s_instance = new Win32GameCenter;
            break;
    }

    return s_instance;
}

void GameCenter::Release( GameCenter* instance )
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;
    delete instance;
}

bool Win32GameCenter::Authenticate( const Crediterials* /*crediterials*/ )
{
    return true;
}

bool Win32GameCenter::ShowLeaderboard( const char* category /* = NULL */ )
{
    return false;
}

bool Win32GameCenter::ShowAchievements()
{
    return false;
}

bool Win32GameCenter::SubmitScore( const char* category, int score /* Message* reason = 0 */ )
{
    return false;
}

bool Win32GameCenter::SubmitAchievement( const char* achievement, float progress /* = 100.0f */ )
{
    CLAW_MSG( "Achievement " << achievement << " progress " << progress << "%" );
    return false;
}

bool Win32GameCenter::GetUserId( UserId& outUserId ) const
{
    outUserId = "Win32GameCenterUser";
    return true;
}
// EOF

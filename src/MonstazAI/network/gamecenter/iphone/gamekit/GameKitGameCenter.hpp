//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/iphone/GameKitGameCenter.hpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#ifndef __NETWORK_IPHONE_GAME_CENTER_HPP__
#define __NETWORK_IPHONE_GAME_CENTER_HPP__

// Internal includes
#include "MonstazAI/network/gamecenter/GameCenter.hpp"

// External includes

// Forward declaration for PIMPL
//class GameKitGameCenterImpl;

class GameKitGameCenter : public GameCenter
{
public:
                        GameKitGameCenter();

    virtual             ~GameKitGameCenter();

    virtual bool        Authenticate( const Crediterials* crediterials = NULL );

    virtual bool        SubmitScore( const char* category, int score );

    virtual bool        SubmitAchievement( const char* achievement, float progress = 100.0f );

    virtual bool        ShowLeaderboard( const char* category = NULL );

    virtual bool        ShowAchievements();

    //static bool IsGameCenterAvailable();
    //static bool IsLocalPlayerAuthenticated();

    // Make them public for PIMPL
    void                NotifyAchievementsLoad( const Achievements& achievements );

    void                NotifyAuthenticationChange( bool authenticated );

    void                NotifyLeaderboardView( bool opened );

    void                NotifyAchievementsView( bool opened );

protected:
    virtual bool        GetUserId( UserId& outUserId ) const;

private:
    //GameKitGameCenterImpl* m_impl;

}; // class GameKitGameCenter

#endif // __NETWORK_IPHONE_GAME_CENTER_HPP__
// EOF

//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/iphone/GameKitGameCenterImpl.h
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

class GameKitGameCenter;

@interface GameKitGameCenterImpl : NSObject <GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate>
{
    BOOL                            m_available;
    BOOL                            m_authenticated;
    BOOL                            m_authentication;
    BOOL                            m_supportOrientationChange;
    GameKitGameCenter*               m_observer;
    GKAchievementViewController*    m_achievementsView;
    GKLeaderboardViewController*    m_leaderboardsView;
}

// Constructor
- (id) initWithObserver:(GameKitGameCenter*) observer;

// Destructor
- (void) dealloc;

- (void) orientationDidChange:(NSNotification*) notification;

- (bool) authenticate;
- (void) onAuthenticationChange;

- (void) loadAchievements;
- (void) onLoadAchievements:(NSArray*) achievements;

- (bool) submitScore:(int64_t) score forCategory:(NSString*) category;
- (bool) submitAchievement:(NSString*) identifier withProgress:(float) progress;

- (bool) showLeaderboard:(NSString*) category;
- (void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController*) viewController;

- (bool) showAchievements;
- (void) achievementViewControllerDidFinish:(GKAchievementViewController*) viewController;

- (void) showErrorNotification:(NSError*) error;
- (void) showDisabledAlert;
- (void) showUnavailableAlert;

- (NSString*) getUserId;
- (bool) isAvailable;
- (bool) isAuthenticated;

@end

/////////////////////////////////////////////////////////////////////////////////
// Standard GameCenter ViewControllers extensions to support orientation change
/////////////////////////////////////////////////////////////////////////////////

@interface OrientedAchievementViewController : GKAchievementViewController
{
}
@end

@interface OrientedLeaderboardViewController : GKLeaderboardViewController
{
}
@end

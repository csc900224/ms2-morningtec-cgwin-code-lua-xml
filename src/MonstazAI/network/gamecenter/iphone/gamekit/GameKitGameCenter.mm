//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/iphone/GameKitGameCenter.mm
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#import "GameKitGameCenterImpl.h"

#include "MonstazAI/network/gamecenter/GameCenter.hpp"
#include "MonstazAI/network/gamecenter/iphone/gamekit/GameKitGameCenter.hpp"

#include "claw/base/Errors.hpp" // CLAW_MSG
#include "claw/math/Math.hpp"   // Claw::MinMax

static GameKitGameCenterImpl* m_impl = NULL;

GameKitGameCenter::GameKitGameCenter()
    :GameCenter()
{
    CLAW_MSG_ASSERT( !m_impl, "Using Objective-C PIMPL doesn't allow for multiple object instances (impl static)." );

    m_impl = [[GameKitGameCenterImpl alloc] initWithObserver: this];
}

GameKitGameCenter::~GameKitGameCenter()
{
    [m_impl dealloc];
    m_impl = NULL;
}

bool GameKitGameCenter::Authenticate( const Crediterials* crediterials )
{
    if( !m_impl )
        return false;

    CLAW_MSG( "[GameKitGameCenter] Authenticate()" );

    return [m_impl authenticate];
}

bool GameKitGameCenter::SubmitScore( const char* category, int score )
{
    if( !m_impl )
        return false;

    CLAW_MSG( "[GameKitGameCenter] SubmitScore(): " << category << " : " << score );

    NSString* tmp = [NSString stringWithCString:category encoding:NSASCIIStringEncoding];
    return [m_impl submitScore:score forCategory:tmp];
}

bool GameKitGameCenter::SubmitAchievement( const char* achievement, float progress /* = 100.0f */ )
{
    if( !m_impl )
        return false;

    CLAW_MSG( "[GameKitGameCenter] SubmitAchievement(): " << achievement << " : " << progress );

    progress = Claw::MinMax( progress, 0.0f, 100.0f );

    NSString* tmp = [NSString stringWithCString:achievement encoding:NSASCIIStringEncoding];
    return [m_impl submitAchievement:tmp withProgress:progress];
}

bool GameKitGameCenter::ShowLeaderboard( const char* category /* = NULL */ )
{
    if( !m_impl )
        return false;

    NSString* tmp = nil;
    // Send category if not not null, otherwise use default
    if( category != NULL )
    {
        tmp = [NSString stringWithCString:category encoding:NSASCIIStringEncoding];
    }
    return [m_impl showLeaderboard:tmp];
}

bool GameKitGameCenter::ShowAchievements()
{
    if( !m_impl )
        return false;

    [m_impl showAchievements];
    return true;
}

void GameKitGameCenter::NotifyAchievementsLoad( const Achievements& achievements )
{
    GameCenter::NotifyAchievementsLoad( achievements );
}

void GameKitGameCenter::NotifyAuthenticationChange( bool authenticated )
{
    GameCenter::NotifyAuthenticationChange( authenticated );
}

void GameKitGameCenter::NotifyLeaderboardView( bool opened )
{
    GameCenter::NotifyLeaderboardView( opened );
}

void GameKitGameCenter::NotifyAchievementsView( bool opened )
{
    GameCenter::NotifyAchievementsView( opened );
}

bool GameKitGameCenter::GetUserId( UserId& outUserId ) const
{
    if( !m_impl )
        return false;

    // User doesn't need to be authenticated to get his identifier, he neither need
    // to be connected to the game center
    NSString* userId = [m_impl getUserId];
    NSUInteger userIdLen = userId.length;
    char* buff = (char*)alloca( ( userIdLen + 1 ) * sizeof(char) );
    bool ret = [userId getCString:buff maxLength:userIdLen + 1 encoding:NSASCIIStringEncoding];
    if( ret )
    {
        outUserId = buff;
    }
    return ret;
}

// EOF

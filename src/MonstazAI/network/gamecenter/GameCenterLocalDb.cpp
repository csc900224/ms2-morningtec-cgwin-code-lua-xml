//////////////////////////////////////////////////////////////////////////
//  FILE NAME:
//      MonstazAI/network/gamecenter/GameCenterLocalDb.cpp
//
//  AUTHOR(S):
//      Krystian Kostecki <krystian.kostecki@gmail.com>
//                        <krystian.kostecki@game-lion.com>
//
//  Copyright (c) 2011, Gamelion. All rights reserved.
//////////////////////////////////////////////////////////////////////////

// Internal includes
#include "MonstazAI/network/gamecenter/GameCenterLocalDb.hpp"
#include "MonstazAI/network/gamecenter/GameCenter.hpp"

// External includes

bool GameCenterLocalDb::StashAchivement( const GameCenter::UserId& playerId, const GameCenter::Achievement& ach )
{
    // Checks if specified user achievements store already exist and return it or inserts new
    PlayerAchievements& playerAchievements = m_achievements[playerId];
    // Check if supplied achievement is already stored
    PlayerAchievements::iterator achIt = playerAchievements.find( ach.GetId() );
    if( achIt == playerAchievements.end() )
    {
        playerAchievements.insert( std::pair< GameCenter::Achievement::Id, GameCenter::Achievement::Value >( ach.GetId(), ach.GetValue() ) );
    }
    // Achievement record exists check which is most up to date
    else
    {
        // Update record
        if( (*achIt).second < ach.GetValue() )
        {
            (*achIt).second = ach.GetValue();
        }
        // Database record is the most up to date
        else
        {
            return false;
        }
    }
    // Achievement record updated
    return true;
}

bool GameCenterLocalDb::StashScore( const GameCenter::UserId& playerId, const GameCenter::Score& sc )
{
    // Checks if specified user records already exist in database or create new store if required
    PlayerScores& playerScores = m_scores[playerId];
    // Check if supplied score is already stored in database
    PlayerScores::iterator scoreIt = playerScores.find( sc.GetId() );
    if( scoreIt == playerScores.end() )
    {
        playerScores.insert( std::pair< GameCenter::Score::Id, GameCenter::Score::Value >( sc.GetId(), sc.GetValue() ) );
    }
    // Score in this category exists check which is most up to date
    else
    {
        // Update record
        if( (*scoreIt).second < sc.GetValue() )
        {
            (*scoreIt).second = sc.GetValue();
        }
        // Database record is the most up to date
        else
        {
            return false;
        }
    }
    // Achievement record updated
    return true;
}

bool GameCenterLocalDb::ContainsAchievement( const GameCenter::UserId& playerId, const GameCenter::Achievement& ach ) const
{
    // Check if player record exist
    Achievements::const_iterator playerAchIt = m_achievements.find( playerId );
    if( playerAchIt != m_achievements.end() )
    {
        // Check if achievemnt with specified id is stored
        PlayerAchievements::const_iterator achIt = (*playerAchIt).second.find( ach.GetId() );
        if( achIt != (*playerAchIt).second.end() )
        {
            // Check if achievement value is bigger then stashed
            if( (*achIt).second >= ach.GetValue() )
            {
                // Specified or higher achievement exist in database for this user, assume up to date
                return true;
            }
        }
    }
    return false;
}

bool GameCenterLocalDb::ContainsScore( const GameCenter::UserId& playerId, const GameCenter::Score& sc ) const
{
    // Check if any player record exist at all
    Scores::const_iterator playerScoresIt = m_scores.find( playerId );
    if( playerScoresIt != m_scores.end() )
    {
        // Check if score with specified id is stored for such player
        PlayerScores::const_iterator scoresIt = (*playerScoresIt).second.find( sc.GetId() );
        if( scoresIt != (*playerScoresIt).second.end() )
        {
            // Check if supplied score value is smaller then stashed one
            if( (*scoresIt).second >= sc.GetValue() )
            {
                // Specified value is smaller then stashed one assume database entry is most up to date
                return true;
            }
        }
    }
    return false;
}

bool GameCenterLocalDb::SyncRequest( GameCenter* gc )
{
    // Synchronize achievements
    bool ret = SyncAchievements( gc );
    // and Scores
    ret = ret || SyncScores( gc );
    return ret;
}

bool GameCenterLocalDb::SyncAchievements( GameCenter* gc )
{
    Achievements::const_iterator playerAchIt = m_achievements.begin();
    Achievements::const_iterator playerAchEnd = m_achievements.end();
    for( ; playerAchIt != playerAchEnd; ++playerAchIt )
    {
        const GameCenter::UserId& playerId = playerAchIt->first;
        const PlayerAchievements& playerAchievements = playerAchIt->second;
        PlayerAchievements::const_iterator achIt = playerAchievements.begin();
        PlayerAchievements::const_iterator achEnd = playerAchievements.end();
        for( ; achIt != achEnd; ++achIt )
        {
            if( !gc->SubmitAchievement( achIt->first, achIt->second ) )
                return false;
        }
    }
    return true;
}

bool GameCenterLocalDb::SyncScores( GameCenter* gc )
{
    Scores::const_iterator playerScoresIt = m_scores.begin();
    Scores::const_iterator playerScoresEnd = m_scores.end();
    for( ; playerScoresIt != playerScoresEnd; ++playerScoresIt )
    {
        const GameCenter::UserId& playerId = playerScoresIt->first;
        const PlayerScores& playerScores = playerScoresIt->second;
        PlayerScores::const_iterator scoreIt = playerScores.begin();
        PlayerScores::const_iterator scoreEnd = playerScores.end();
        for( ; scoreIt != scoreEnd; ++scoreIt )
        {
            if ( !gc->SubmitScore( scoreIt->first, scoreIt->second ) )
                return false;
        }
    }
    return true;
}

// EOF

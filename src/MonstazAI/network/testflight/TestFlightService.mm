#include "MonstazAI/network/testflight/TestFlightService.hpp"

#import <UIKit/UIDevice.h>

#import "TestFlight.h"

namespace TestFlightService
{
    void TakeOff( const char* token )
    {
        [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
        [TestFlight takeOff:[NSString stringWithCString:token encoding:NSASCIIStringEncoding]];
    }

    void PassCheckpoint( const char* name )
    {
        [TestFlight passCheckpoint:[NSString stringWithCString:name encoding:NSASCIIStringEncoding]];
    }
}

#ifndef __MONSTAZ_TESTFLIGHTSERVICE_HPP__
#define __MONSTAZ_TESTFLIGHTSERVICE_HPP__

namespace TestFlightService
{
    void TakeOff( const char* token );
    void PassCheckpoint( const char* name );
}

#endif

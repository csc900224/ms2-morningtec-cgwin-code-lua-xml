#ifndef __INCLUDED__CONNECTIONMONITOR_HPP__
#define __INCLUDED__CONNECTIONMONITOR_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include "claw_ext/network/net_monitor/NetworkMonitor.hpp"
#include "jungle/patterns/Singleton.hpp"

class ConnectionMonitor 
    : public Jungle::Patterns::Singleton< ConnectionMonitor, Jungle::Patterns::ExplicitCreation >
    , ClawExt::NetworkMonitor::Observer
{
    friend class Jungle::Patterns::ExplicitCreation<ConnectionMonitor>;
public:
    LUA_DEFINITION( ConnectionMonitor );
    ConnectionMonitor( lua_State* L ) { CLAW_ASSERT( false ); }

    ~ConnectionMonitor();

    bool HasNetworkConnection() const { return m_hasConnection; }

    void SetLua( Claw::Lua* lua );
    void Update( float dt );
    void SetEnabled( bool enabled );

    int l_NoNetworkRetry( lua_State* L );
    int l_NoNetworkClose( lua_State* L );
    int l_NoNetworkContinue( lua_State* L );

    // ClawExt::NetworkMonitor::Observer interface
    virtual void OnNetworkCheckResult( ClawExt::NetworkMonitor::NetworkStatus status );

protected:
    ConnectionMonitor();

private:
     Claw::Lua* m_lua;
     bool m_enabled;
     bool m_hasConnection;
     const char* m_luaCall;
};

#endif /// __INCLUDED__CONNECTIONMONITOR_HPP__
/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      MonstazAI/AudioSession.hpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2011, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

#ifndef __INCLUDED__AUDIOSESSION_HPP__
#define __INCLUDED__AUDIOSESSION_HPP__

namespace AudioSession
{
    enum Category
    {
        C_AMBIENT,
        C_SOLO_AMBIENT,
        C_PLAYBACK,
        C_RECORD,
        C_PLAY_AND_RECORD,
        C_AUDIO_PROCESSING
    };

    void SetCategory( Category category );
    bool IsOtherAudioPlaying();
    void RegisterIPodPlaybackStateChangedEvent( void (*callback)(void) );

} // AudioSession

#endif // __INCLUDED__AUDIOSESSION_HPP__

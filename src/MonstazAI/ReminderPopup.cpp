#include "claw/base/Errors.hpp"

#include "MonstazAI/ReminderPopup.hpp"

#ifndef CLAW_IPHONE

void ReminderPopup::Initialize()
{
    CLAW_MSG( "Initialize ReminderPopup" );
}

void ReminderPopup::Release()
{
    CLAW_MSG( "Release ReminderPopup" );
}

void ReminderPopup::Show()
{
    CLAW_MSG( "Show ReminderPopup" );
}

#endif

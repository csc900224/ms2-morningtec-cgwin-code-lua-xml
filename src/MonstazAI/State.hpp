#ifndef __INCLUDED_MONSTAZ_STATE_HPP__
#define __INCLUDED_MONSTAZ_STATE_HPP__

#include "claw/compat/ClawTypes.hpp"

enum StateName
{
    SEntityIdle,
    SCommonSpawn,
    SOctopusMove,
    SOctopusAttack,
    SSqueezerMove,
    SSqueezerAttack,
    SSqueezerBump,
    SSqueezerShocked,
    SSqueezerSeek,
    SSqueezerMoveAndTurn,
    SSqueezerMoveStraight,
    SSqueezerSeekAway,
    SSqueezerSeekRandom,
    SFishMove,
    SFishAttack,
    SFishRage,
    SFishRageAndThrow,
    SFloaterMove,
    SFloaterAttack,
    SFloaterAttack2,
    SElectricMove,
    SElectricAttack,
    SHoundMove,
    SHoundAttack,
    SHoundMoveAvoid,
    SHoundShoot,
    SHoundIdleBefore,
    SHoundIdleAfter,
    SFollowWaypoints,
    SRunAway,
    SIdleAnim,
    SSectoidMove,
    SSectoidAttack,
    SSectoidShootingMove,
    SSectoidShootingAttack,
    SSectoidShootingTargetting,
    SKillerWhaleMove,
    SKillerWhaleAttack,
    SKillerWhaleSeek,
    SKillerWhaleCharge,
    SKillerWhaleSimpleMove,
    SKillerWhaleSimpleAttack,
    SCrabMove,
    SCrabDigIn,
    SCrabDigOut,
    SCrabHidden,
    SCrabSeek,
    SCrabAttack,
    SMechaBossMove,
    SMechaBossAttackMace,
    SMechaBossAttackFlamer,
    SMechaBossAttackRocket,
    SMechaBossFly,
    SSowerBossMove,
    SSowerBossRun,
    SSowerBossAttackTongue,
    SSowerBossAttackEggShot,
    SSowerBossAttackEggSpit,
    SOctobrainBossMove,
    SOctobrainBossTeleport,
    SOctobrainBossTeleportPlayer,
    SOctobrainBossClone,
    SOctobrainBossCloneSpawn,
    SOctobrainBossCloneDie,
    SOctobrainBossCharge,
    SOctobrainBossShoot,
    SLobsterMove,
    SLobsterHit,
    SLobsterShoot,
    SLobsterIdleBefore,
    SLobsterIdleAfter,
    SNautilMove,
    SNautilAttack,
    SNautilBump,
    SNautilShocked,
    SNautilSeek,
    SNautilMoveAndTurn,
    SNautilMoveStraight,
    SNautilSeekAway,
    SNautilSeekRandom,
    SNervalMove,
    SNervalAttack,
    SNervalSeek,
    SNervalCharge,
    SAIFriendMove,
    SAIFriendFire,

    NUMBER_OF_STATES
};

class Entity;
class StackSM;

struct State
{
    virtual void OnEnter( Entity& entity, StackSM& sm, StateName previousState ) {}
    virtual void OnExit( Entity& entity, StackSM& sm, StateName nextState ) {}
    virtual void OnUpdate( Entity& entity, StackSM& sm, float tick ) {}
};

#endif

#include "claw/base/AssetDict.hpp"
#include "claw/base/Registry.hpp"
#include "claw/application/AbstractApp.hpp"
#include "claw/network/HttpRequest.hpp"
#include "claw/vfs/Vfs.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Math.hpp"

#include "guif/TextLine.hpp"
#include "guif/Sprite.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/Loading.hpp"

#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/MonetizationManager.hpp"

static const char* s_images[] = {
    "plate/loading_1.@linear",
    "plate/loading_2.@linear",
    "plate/loading_3.@linear",
    "plate/loading_4.@linear",
    "plate/loading_5.@linear",
    "plate/loading_6.@linear",
    "plate/loading_7.@linear"
};

static const int TIP_COUNT = 44;

static const char* s_tapInfo            = "tap screen to continue";
static const char* TMP_DIR              = "save/";

static const int    TEXT_EXTENT         = 200;
static const float  TITLE_OFFSET_Y      = 0;
static const float  BAR_HEIGHT_RATIO    = 0.15625f;
static const float  BLIP_SPACING        = 0;
static const float  TEXT_TO_BLIP_SPACE  = 0.03f;
static const float  TEXT_EXTENT_RATIO   = 0.4f;
static const float  BLIP_START_X_RATIO  = 0.729f;

LUA_DECLARATION( Loading )
{
    METHOD( Loading, OnContinue ),
    METHOD( Loading, OnDownload ),
    METHOD( Loading, Release ),
    METHOD( Loading, Analytics ),
    METHOD( Loading, GetControlSize ),
    METHOD( Loading, Playhaven ),
    {0,0}
};

Loading::Loading( bool autoaimquery )
    : m_bg( NULL )
    , m_blip( Claw::AssetDict::Get<Claw::Surface>( "menu2/dot_medium_03.png@linear" ) )
    , m_blipSpace( Claw::AssetDict::Get<Claw::Surface>( "menu2/dot_medium_01.png@linear" ) )
    , m_font( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_normal.xml@linear" ) )
    , m_time( 0 )
    , m_state( 0 )
    , m_tapWait( false )
    , m_tapText( NULL )
    , m_tapTextAlpha( 0 )
    , m_firstLoading( true )
    , m_socialSplash( false )
    , m_screenOk( false )
    , m_textExtent( TEXT_EXTENT_RATIO * ((MonstazApp*)MonstazApp::GetInstance())->GetResolution().x )
    , m_scale( ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale() )
    , m_movie( false )
    , m_currentTime( 0 )
    , m_targetTime( 0 )
{
    for( int i = 0; i < BLIP_NUM; ++i )
        m_a[i] = 0;

    Claw::StdOStringStream tip;
    tip << "TEXT_LOADING_TIP_" << ( g_rng.GetInt() % TIP_COUNT + 1 );

    Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
    fontSet->AddFont( "f", m_font );
    Claw::Text::Format format;
    format.SetFontSet( fontSet );
    format.SetFontId( "f" );
    format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );
    m_text.Reset( new Claw::ScreenText( format, Claw::TextDict::Get()->GetText( tip.str() ), Claw::Extent( m_textExtent, 0 ) ) );
    format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_RIGHT );
    m_tapText.Reset( new Claw::ScreenText( format, Claw::String( s_tapInfo ), Claw::Extent( m_textExtent, 0 ) ) );

    Claw::Registry::Get()->Get( "/internal/firstloading", m_firstLoading );
    Claw::Registry::Get()->Set( "/internal/firstloading", false );

    Claw::Registry::Get()->Get( "/internal/socialsplash", m_socialSplash );
    Claw::Registry::Get()->Set( "/internal/socialsplash", false );

    if( m_firstLoading )
    {
        m_bg = Claw::AssetDict::Get<Claw::Surface>( "plate/splash.@linear" );
    }
    else
    {
        if( m_socialSplash )
        {
            m_bg = Claw::AssetDict::Get<Claw::Surface>( "plate/splash_social.@linear" );
            m_targetTime = 5;
            m_text.Release();
        }
        else
        {
            m_bg = Claw::AssetDict::Get<Claw::Surface>( s_images[g_rng.GetInt() % ( sizeof(s_images)/sizeof(char*) )] );
        }
    }

    if( autoaimquery )
    {
        m_mutex.Enter();
    }

    const char* movie = "";
    Claw::Registry::Get()->Get( "/internal/loadingmovie", movie );
    if( *movie )
    {
        m_movie = true;
        if( Claw::AbstractApp::GetInstance()->PlayMovie( movie ) )
        {
            MonetizationManager::GetInstance()->SkipPlayhavenResume();
        }
        Claw::Registry::Get()->Set( "/internal/loadingmovie", "" );
    }
}

Loading::~Loading()
{
}

void Loading::Update( float dt )
{
    if( m_movie )
    {
        if( !Claw::AbstractApp::GetInstance()->MovieFinished() )
        {
            return;
        }
        m_movie = false;
    }

    m_currentTime += dt;
    m_time += dt * 4;
    static const float target = 0.5f;

    if( m_state < BLIP_NUM )
    {
        m_a[m_state] = Claw::SmoothStep( 0.f, target, m_time ) * 255;
        if( m_time > target )
        {
            m_time = 0;
            m_state++;
        }
    }
    else
    {
        float val = Claw::SmoothStep( 0.f, target, target - m_time ) * 255;
        for( int i=0; i<BLIP_NUM; i++ )
        {
            m_a[i] = val;
        }
        if( m_time > target )
        {
            m_time = 0;
            m_state = 0;
        }
    }

    if( m_screenOk && m_autoAimScreen )
    {
        m_autoAimScreen->Update( dt );
    }
}

void Loading::Render( Claw::Surface* target, Claw::UInt32 dlList )
{
    if( m_movie )
    {
        target->Clear( 0 );
        return;
    }

    Vectorf BG_SIZE;

    float IMG_SCALE = 1.0f;
    float x = 0;
    float y = 0;

    if( m_firstLoading || !m_text )
    {
        BG_SIZE = Vectorf( 1920, 1280 );
        IMG_SCALE = std::max( target->GetHeight() / BG_SIZE.y, target->GetWidth() / BG_SIZE.x);
        x = (target->GetWidth() - BG_SIZE.x * IMG_SCALE) * 0.5f;
        if( !m_socialSplash )
        {
            y = (target->GetHeight() - BG_SIZE.y * IMG_SCALE) * 0.5f;
        }
    }
    else
    {
        BG_SIZE = Vectorf( 1280, 800 );
        IMG_SCALE = target->GetHeight() / BG_SIZE.y;
    }

    Vectorf drawScale;
    drawScale.x = BG_SIZE.x / m_bg->GetWidth() * IMG_SCALE;
    drawScale.y = BG_SIZE.y / m_bg->GetHeight() * IMG_SCALE;

    // Draw backround
    Claw::TriangleEngine::Blit( target, 
                                     m_bg, 
                                     x,
                                     y,
                                     0,
                                     drawScale,
                                     Vectorf( 0, 0 ),
                                     Claw::TriangleEngine::FM_NONE,
                                     m_bg->GetClipRect());

    if( !PakManager::GetInstance()->CheckDone( dlList ) )
    {
        if( !m_dl )
        {
            m_dl.Reset( new DownloadScreen() );
        }

        m_dl->SetProgress( PakManager::GetInstance()->GetProgress() );
        m_dl->Render( target );
        return;
    }

    if( m_firstLoading )
    {
        // Draw only splash screen on first loading
        return;
    }

    if( m_text )
    {
        m_font->GetSurface()->SetAlpha( 255 );
        m_text->Draw( target,
                        target->GetWidth() * 0.75f - m_textExtent * 0.5f,
                        (target->GetHeight() - m_text->GetHeight()) * 0.5f );
    }

    const float bottomBarMidle = target->GetHeight() - target->GetHeight() * BAR_HEIGHT_RATIO * 0.5f;
    const int blipY = bottomBarMidle - m_blipSpace->GetHeight() * 0.5f;

    const float blipSpacing = target->GetWidth() * BLIP_SPACING;
    const float blipRealWidth = m_blip->GetWidth() + blipSpacing;
    const float blipStartX = BLIP_START_X_RATIO * target->GetWidth();

    for( int i = 0; i < BLIP_NUM; ++i )
    {
        m_blipSpace->SetAlpha( 255 );
        m_blip->SetAlpha( m_a[i] );
        target->Blit( blipStartX + blipRealWidth * i , blipY, m_blipSpace );
        target->Blit( blipStartX + blipRealWidth * i , blipY, m_blip );
    }

    if( m_tapWait )
    {
        m_font->GetSurface()->SetAlpha( m_tapTextAlpha );
        m_tapText->Draw( target,
                            blipStartX - m_tapText->GetWidth() - target->GetWidth() * TEXT_TO_BLIP_SPACE, 
                            bottomBarMidle - m_tapText->GetHeight() * 0.5f );
    }

    if( m_screenOk && m_autoAimScreen )
    {
        m_autoAimScreen->Render( target );
    }
}

void Loading::TouchUp( int x, int y )
{
    if( m_screenOk && m_autoAimScreen )
    {
        m_autoAimScreen->OnTouchUp( x, y, 1 );
    }
}

void Loading::TouchDown( int x, int y )
{
    if( m_screenOk && m_autoAimScreen )
    {
        m_autoAimScreen->OnTouchDown( x, y, 1 );
    }
}

void Loading::WaitForTap( bool wait )
{
    m_tapWait = wait;
    if( wait )
        m_state = 4;
}

bool Loading::CanLeave() const
{
    return m_state >= 3 && m_currentTime > m_targetTime;
}

void Loading::KeyPress( Claw::KeyCode code )
{
}

int Loading::l_OnContinue( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int Loading::l_OnDownload( lua_State* L )
{
    // TODO: Remove
    return 0;
}

void Loading::ThreadLoadPopup()
{
    m_autoAimScreen.Reset( new Guif::Screen() );
    Claw::LuaPtr lua = m_autoAimScreen->GetLuaState();

    lua->RegisterLibrary( Claw::Lua::L_MATH );

    AudioManager::GetInstance()->Init( lua );

    MonstazApp::PushScreenModes( lua );

    Claw::Lunar<Claw::Registry>::Register( *lua );
    Claw::Lunar<Claw::Registry>::push( *lua, Claw::Registry::Get() );
    lua->RegisterGlobal( "registry" );

    Claw::Lunar<Loading>::Register( *lua );
    Claw::Lunar<Loading>::push( *lua, this );
    lua->RegisterGlobal( "callback" );

    lua->Load( "menu/autoaim.lua" );

    m_screenOk = true;
}

int Loading::l_Release( lua_State* L )
{
    m_screenOk = false;
    m_mutex.Leave();
    return 0;
}

int Loading::l_Analytics( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int Loading::l_GetControlSize( lua_State* L )
{
    return MonstazAI::MonstazAIApplication::GetControlSize( NULL, L );
}

int Loading::l_Playhaven( lua_State* L )
{
    // TODO: Remove
    return 0;
}

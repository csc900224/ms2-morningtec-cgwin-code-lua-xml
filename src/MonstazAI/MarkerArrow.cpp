#include "claw/base/AssetDict.hpp"

#include "MarkerArrow.hpp"

MarkerArrow::MarkerArrow( const Vectorf& pos, int offset, Type type )
    : Renderable( pos + Vectorf( 0, offset ) )
    , m_time( 0 )
    , m_arrow( Claw::AssetDict::Get<Claw::Surface>( type == Target ? "gfx/items/arrow_red.png@linear" : "gfx/items/arrow_blue.png@linear" ) )
    , m_offset( offset )
    , m_type( type )
{
}

MarkerArrow::~MarkerArrow()
{
}

void MarkerArrow::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    target->Blit( m_pos.x * scale - offset.x - m_arrow->GetWidth() / 2, ( m_pos.y - m_offset ) * scale - offset.y - m_arrow->GetHeight() - 20 * sin( m_time ) * scale, m_arrow );
}

void MarkerArrow::Update( float dt )
{
    m_time += dt * 4;
    while( m_time > 2*M_PI )
    {
        m_time -= 2*M_PI;
    }
}

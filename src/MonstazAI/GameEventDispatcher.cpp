#include "MonstazAI/GameEventDispatcher.hpp"

LUA_DECLARATION( GameEventDispatcher )
{
    METHOD( GameEventDispatcher, HandleGameEvent ),
    {0,0}
};

GameEventDispatcher::GameEventDispatcher()
{}

void GameEventDispatcher::InitLua( Claw::Lua* lua )
{
    Claw::Lunar<GameEventDispatcher>::Register( *lua );
    Claw::Lunar<GameEventDispatcher>::push( *lua, this );
    lua->RegisterGlobal( "GameEventDispatcher" );
}

void GameEventDispatcher::RegisterGameEventHandler( GameEventHandler* handler )
{
    m_handlers.insert( handler );
}

void GameEventDispatcher::UnregisterGameEventHandler( GameEventHandler* handler )
{
    m_handlers.erase( handler );
}

bool GameEventDispatcher::HandleGameEvent( const GameEvent& ev )
{
    GameEventHandlers::iterator it = m_handlers.begin();
    GameEventHandlers::iterator end = m_handlers.end();

    bool ret = false;
    for( ; it != end; ++it )
    {
        ret |= (*it)->HandleGameEvent( ev );
    }
    return ret;
}

bool GameEventDispatcher::HandleGameEvent( const GameEvent::Id& id, float value, const Claw::NarrowString& text, void* userData )
{
    return GameEventHandler::HandleGameEvent( id, value, text, userData );
}

int GameEventDispatcher::l_HandleGameEvent( lua_State* L )
{
    Claw::Lua lua( L );
    const int top = lua.GetTop();
    GameEvent::Id id = lua.CheckEnum<GamEventId>( 1 );
    float value = (float)((top < 2 || lua.IsNil( 2 )) ? 0 : lua.CheckNumber( 2 ));
    Claw::NarrowString text = (top < 3 || lua.IsNil( 3 )) ? "" : lua.CheckString( 3 );
    HandleGameEvent( id, value, text );
    return 0;
}
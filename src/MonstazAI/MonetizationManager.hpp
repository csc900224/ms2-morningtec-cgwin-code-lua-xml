#ifndef __INCLUDED__MONETIZATION_MANAGER__HPP__
#define __INCLUDED__MONETIZATION_MANAGER__HPP__

// External includes
#include "claw/compat/Platform.h"
#include "jungle/patterns/Singleton.hpp"
#include "claw_ext/monetization/tapjoy/Tapjoy.hpp"
#include "claw_ext/monetization/playhaven/Playhaven.hpp"
#include "claw_ext/monetization/inappstore/InAppStore.hpp"
#include "claw_ext/monetization/metaps/Metaps.hpp"

// Forward declarations
namespace ClawExt
{
    class Chartboost;
    class AppsFlyer;
    class AdColony;
}

class MonetizationManager 
    : public Jungle::Patterns::Singleton< MonetizationManager, Jungle::Patterns::ExplicitCreation >
    , public ClawExt::Tapjoy::TapjoyObserver
    , public ClawExt::Playhaven::PlacementObserver
    , public ClawExt::InAppStore::TransactionsObserver
    , public ClawExt::Metaps::MetapsObserver
{
    // Make friendship with creation policy to call protected constructor
    friend class Jungle::Patterns::ExplicitCreation< MonetizationManager >;

public:
    enum TapjoyAction
    {
        TA_TUTORIAL_COMPLETED,

        TA_NUM
    };

    enum AdColonyZone
    {
        ACZ_1
    };

    ~MonetizationManager();

    void OnFocusChange( bool focus );

    ClawExt::Chartboost* GetChartboost() const { return m_chartboost; }
    ClawExt::Playhaven* GetPlayhaven() const { return m_playhaven; }
    ClawExt::InAppStore* GetIapStore() const { return m_iaps; }
    ClawExt::Tapjoy* GetTapjoy() const { return m_tapjoy; }
    ClawExt::AppsFlyer* GetAppsFlyer() const { return m_appsFlyer; }
    ClawExt::AdColony* GetAdColony() const { return m_adColony; }
    ClawExt::Metaps* GetMetaps() const { return m_metaps; }

    void OnProgressLoaded();
    void SkipPlayhavenResume();

    void CheckBillingSupport();

    bool AreAdsEnabled() const;
    void SetAdsEnabled( bool enabled );

    bool IsTapjoySupported() const { return m_tapjoy != NULL; }
    bool IsMetapsSupported() const { return m_metaps != NULL; }
    void ReportTapjoyAction( TapjoyAction action );

    int GetFreeGoldEarned() const { return m_freeGoldCache; }
    void FlushFreeGold();

    void Update( float dt );

    // Tapjoy observer interface
    virtual void TapjoyPointsEarned( int points );
    virtual void TapjoyPointsUpdate( const Claw::NarrowString& currencyName, int pointsTotal ) {}
    virtual void TapjoyFullScreenAdReceived() {}
    virtual void TapjoyFullScreenAdFailed(int error) {}
    virtual void TapjoyClosed();

    // Playhaven placement observer interface
    virtual void OnPlacementClosed( const Claw::NarrowString& placement ) {};
    virtual void OnPlacementShown( const Claw::NarrowString& placement );
    virtual void OnPlacementFailed( const Claw::NarrowString& placement ) {};

    // InAppStore TransactionsObserver interface
    virtual void TransactionFailed( const ClawExt::InAppTransaction& trans ) {}
    virtual void TransactionCancel( const ClawExt::InAppTransaction& trans ) {}
    virtual void TransactionComplete( const ClawExt::InAppTransaction& trans );
    virtual void TransactionRestore( const ClawExt::InAppTransaction& trans );
    virtual void TransactionRefund( const ClawExt::InAppTransaction& trans ) {}
    virtual void TransactionSupport( bool supported ) {}

    // Metaps observer interface
    virtual void MetapsCurrencyEarned( int currencyAmount );
    virtual void MetapsOnError( const Claw::NarrowString& errorCode ) {}

protected:
    MonetizationManager();
    void Initialize();
    void ReportAppsFlyerTransaction( const ClawExt::InAppTransaction& trans );

    bool ShouldUseMetaps();
    bool IsMetapsCountryCode(const Claw::NarrowString& countryCode);

private:
    ClawExt::Chartboost* m_chartboost;
    ClawExt::Playhaven* m_playhaven;
    ClawExt::InAppStore* m_iaps;
    ClawExt::Tapjoy* m_tapjoy;
    ClawExt::AppsFlyer* m_appsFlyer;
    ClawExt::AdColony* m_adColony;
    ClawExt::Metaps* m_metaps;

    int m_freeGoldCache;
    bool m_progressLoaded;
    bool m_skipPhResume;
}; // class ProgressManager

#endif
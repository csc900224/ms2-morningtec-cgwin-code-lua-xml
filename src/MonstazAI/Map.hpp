#ifndef __MONSTAZ_MAP_HPP__
#define __MONSTAZ_MAP_HPP__

#include <map>
#include <set>
#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/RNG.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/MarkerArrow.hpp"
#include "MonstazAI/scene/collisions/AARect.hpp"
#include "MonstazAI/particle/GeiserParticle.hpp"

class IsoSet;

struct SpawnCircle
{
    SpawnCircle( float _x, float _y, float _r ) : x( _x ), y( _y ), r( _r ) {}
    float x, y, r;
};

class Map : public Claw::RefCounter
{
public:
    struct Comparator
    {
        bool operator()( const char* lhs, const char* rhs ) const
        {
            return strcmp( lhs, rhs ) < 0;
        }
    };

    enum Type
    {
        Grass,
        City,
        Desert,
        Ice
    };

    enum Thrash
    {
        T_None,
        T_Hydrant,
        T_ATM,
        T_Garbage,
        T_Glass,
        T_Mailbox,
        T_Newspaper,
        T_UFO,
        T_Tractor,
        T_Boiler,
        T_Puszka,
        T_Wall,
        T_Jet,
        T_Tower,
        T_Titanic,
        T_Tank,
        T_Base,
        T_Crates,
        T_Wychodek,
        T_Silo,
        T_SiloHalf,
        T_SiloFull,
        T_Periscope,
        T_Antenna
    };

    struct StaticObject : public Renderable
    {
        StaticObject( const Vectorf& pos, const char* name, float life );
        virtual ~StaticObject();
        virtual bool IsIsoSet() const { return false; }
        virtual void Update( float dt ) {}

        char* m_name;
        float m_life, m_maxLife, m_rmaxLife;
        StaticObject* m_repl;
        MarkerArrowPtr m_marker;
        float m_holo;
        Thrash m_thrash;
        Claw::SurfacePtr m_anim;
    };
    struct StaticObjectSurface : public StaticObject
    {
        StaticObjectSurface( const Vectorf& pos, Claw::Surface* gfx, float scale, const char* name, float life );
        Claw::SurfacePtr m_gfx;

        void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
        inline const Claw::Rect& GetRenderWorldBV() const   { return m_worldBV; }

    private:
        Claw::Rect m_worldBV;
    };
    struct StaticObjectIsoSet : public StaticObject
    {
        StaticObjectIsoSet( const Vectorf& pos, IsoSet* isoset, const char* name, float life );
        Claw::SmartPtr<IsoSet> m_isoset;
        bool IsIsoSet() const { return true; }

        void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const { CLAW_ASSERT( false ); }
        void Update( float dt );
    };

    LUA_DEFINITION( Map );
    Map( lua_State* L ) { CLAW_ASSERT( false ); }

    Map( Claw::Lua* lua );
    ~Map();

    static void InitEnum( Claw::Lua* lua );

    void Render( Claw::Surface* target );
    void Update( float dt );

    void SetSize( int w, int h );
    void SetResolution( int w, int h );
    void SetTwoFingerMove( int x, int y );
    const Vectori& GetSize() const { return m_size; }
    const Vectorf& GetOffset() const { return m_offset; }
    std::vector<StaticObject*>& GetStaticObjects() { return m_list; }
    const std::map<Claw::NarrowString, SpawnCircle>& GetSpawnData() { return m_spawn; }
    Type GetType() const { return m_type; }
    const Scene::AARect& GetBv() { return m_bv; }
    const std::map<const char*, MarkerArrowPtr, Comparator>& GetMarkers() const { return m_markers; }

    StaticObject* AddBackgroundObject( const Claw::NarrowString& fn, int x, int y );
    StaticObject* AddGroundObject( const Claw::NarrowString& fn, int x, int y );
    StaticObject* AddStaticObject( const Claw::NarrowString& fn, int x, int y, const char* msname, float mslife, int offset );
    void AddSpawnCircle( const Claw::NarrowString& id, float x, float y, float r );

    void BuildNamedObjectsMap();
    const std::vector<StaticObject*>& GetNamedObject( const char* name );
    void KillObject( StaticObject* so );

    void AddMarker( const char* key, const Vectorf& pos, bool exit );
    void RemoveMarker( const char* key );

    void AddAnim( Claw::Surface* anim ) { Claw::SurfacePtr ptr( anim ); if( std::find( m_animList.begin(), m_animList.end(), ptr ) == m_animList.end() ) { m_animList.insert( ptr ); } }

    int l_MoveCamera( lua_State* L );
    int l_GetSpawnData( lua_State* L );
    int l_SetType( lua_State* L );
    int l_KillObject( lua_State* L );
    int l_ObjectToDestroy( lua_State* L );
    int l_AddMarker( lua_State* L );
    int l_RemoveMarker( lua_State* L );

private:
    Claw::LuaPtr m_lua;

    Vectori m_size;
    Vectori m_resolution;
    Vectorf m_offset;

    Scene::AARect m_bv;

    std::vector<StaticObject*> m_background;
    std::vector<StaticObject*> m_ground;
    std::vector<StaticObject*> m_list;
    std::map<Claw::NarrowString, SpawnCircle> m_spawn;
    std::map<const char*, std::vector<StaticObject*>, Comparator> m_namedObjs;
    std::map<const char*, MarkerArrowPtr, Comparator> m_markers;
    std::set<Claw::SurfacePtr> m_animList;

    Claw::SurfacePtr m_bg[3];
    int m_seed;
    Claw::RNG m_rng;

    Type m_type;

    char* m_layout;

    Claw::SurfacePtr m_shadow;
    Claw::SurfacePtr m_hydrantThrash[2];
    ParticleFunctorPtr m_hydrantGeiser;
    Claw::SurfacePtr m_atmThrash[14];
    Claw::SurfacePtr m_miscThrash[32];
    Claw::SurfacePtr m_glassThrash[19];
    Claw::SurfacePtr m_garbageThrash[10];
    Claw::SurfacePtr m_postThrash[15];
    Claw::SurfacePtr m_newspaperThrash[15];
    Claw::SurfacePtr m_belkaThrash[5];
    Claw::SurfacePtr m_chainThrash[2];
    Claw::SurfacePtr m_claspThrash[3];
    Claw::SurfacePtr m_clothThrash[5];
    Claw::SurfacePtr m_dirtyGlassThrash[11];
    Claw::SurfacePtr m_dishThrash[4];
    Claw::SurfacePtr m_doorThrash;
    Claw::SurfacePtr m_engineThrash;
    Claw::SurfacePtr m_gaugeThrash;
    Claw::SurfacePtr m_handleThrash[7];
    Claw::SurfacePtr m_lightThrash[2];
    Claw::SurfacePtr m_pipeThrash[5];
    Claw::SurfacePtr m_radioThrash;
    Claw::SurfacePtr m_railsThrash[8];
    Claw::SurfacePtr m_scrapThrash[38];
    Claw::SurfacePtr m_screwThrash[7];
    Claw::SurfacePtr m_sealThrash[3];
    Claw::SurfacePtr m_spoolThrash;
    Claw::SurfacePtr m_stuffThrash;
    Claw::SurfacePtr m_tireThrash[3];
    Claw::SurfacePtr m_tubeThrash;
    Claw::SurfacePtr m_turbineThrash;
    Claw::SurfacePtr m_windowThrash[4];
    Claw::SurfacePtr m_wireThrash[7];
    Claw::SurfacePtr m_plankThrash[6];

    ParticleFunctorPtr m_atmFunctor;
    ParticleFunctorPtr m_miscFunctor;
    ParticleFunctorPtr m_glassFunctor;
    ParticleFunctorPtr m_garbageFunctor;
    ParticleFunctorPtr m_postFunctor;
    ParticleFunctorPtr m_newspaperFunctor;

    ParticleFunctorPtr m_belkaFunctor;
    ParticleFunctorPtr m_chainFunctor;
    ParticleFunctorPtr m_claspFunctor;
    ParticleFunctorPtr m_clothFunctor;
    ParticleFunctorPtr m_dirtyGlassFunctor;
    ParticleFunctorPtr m_dishFunctor;
    ParticleFunctorPtr m_handleFunctor;
    ParticleFunctorPtr m_lightFunctor;
    ParticleFunctorPtr m_pipeFunctor;
    ParticleFunctorPtr m_railsFunctor;
    ParticleFunctorPtr m_scrapFunctor;
    ParticleFunctorPtr m_screwFunctor;
    ParticleFunctorPtr m_sealFunctor;
    ParticleFunctorPtr m_tireFunctor;
    ParticleFunctorPtr m_windowFunctor;
    ParticleFunctorPtr m_wireFunctor;
    ParticleFunctorPtr m_doorFunctor;
    ParticleFunctorPtr m_engineFunctor;
    ParticleFunctorPtr m_gaugeFunctor;
    ParticleFunctorPtr m_radioFunctor;
    ParticleFunctorPtr m_spoolFunctor;
    ParticleFunctorPtr m_stuffFunctor;
    ParticleFunctorPtr m_tubeFunctor;
    ParticleFunctorPtr m_turbineFunctor;
    ParticleFunctorPtr m_plankFunctor;

    bool m_dustSpawn;
    Vectorf m_dustVector;
    float m_dustTimer;
    float m_dustWait;
    Claw::SurfacePtr m_dustThrash[9];
    int m_dustNum;
    bool m_dustShadow;
};

typedef Claw::SmartPtr<Map> MapPtr;

#endif

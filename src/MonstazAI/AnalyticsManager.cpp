#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/AnalyticsHandlers.hpp"
#include "MonstazAI/ServerConstants.hpp"

#include "claw/compat/Platform.h"
#include "claw/base/Errors.hpp"
#include "claw/base/Registry.hpp"

#include "claw_ext/network/analytics2/flurry/FlurryAnalytics.hpp"
#include "claw_ext/network/analytics2/gameanalytics/GameAnalytics.hpp"
#include "claw_ext/network/server_sync/ServerSync.hpp"

#include "AppVersion.hpp"

#include <sstream>
#include <iomanip>


//#ifndef _CLEAN_FOR_EXPORT
#ifdef CLAW_ANDROID
static const char* GAME_ANALYTICS_KEY       = "0";
static const char* GAME_ANALYTICS_SECRET    = "0";
static const char* FLURRY_ANALYTICS_KEY     = "0";
#elif CLAW_IPHONE
static const char* GAME_ANALYTICS_KEY       = "0";
static const char* GAME_ANALYTICS_SECRET    = "0";
static const char* FLURRY_ANALYTICS_KEY     = "0";
#else
//#else
static const char* GAME_ANALYTICS_KEY       = "0";
static const char* GAME_ANALYTICS_SECRET    = "0";
static const char* FLURRY_ANALYTICS_KEY     = "0";
//#endif

//#ifndef _CLEAN_FOR_EXPORT
#endif
//#endif


static const char* SYNC_GROUP_DEFAULT       = "default";
static const char* HIERARCHY_SEPARATOR = ":";

AnalyticsManager::AnalyticsManager()
    : GameEventHandler( true )
    , m_initialized( false )
    , m_sessionRunning( false )
    , m_lua( NULL )
    , m_game( NULL )
    , m_flurry( NULL )
{}

AnalyticsManager::~AnalyticsManager()
{
    m_flurry->StopSession();
    m_game->StopSession();

    ClawExt::FlurryAnalytics::Release();
    ClawExt::GameAnalytics::Release();

    m_allHandlers.clear();
    m_eventHandlers.clear();
}

void AnalyticsManager::Initialize()
{
    if( !m_initialized )
    {
        m_flurry = ClawExt::FlurryAnalytics::QueryInterface();
        m_game = ClawExt::GameAnalytics::QueryInterface();

        // Check if Game Analytics Key has changed, if yes clear its cache
        // to prevent event migration to new application id.
        const char* gaKey = NULL;
        Claw::Registry::Get()->Get( "/monstaz/analytics/gakey", gaKey );
        if( !gaKey || strcmp(gaKey, GAME_ANALYTICS_KEY) != 0 )
        {
            CLAW_MSG( "AnalyticsManager - GA key changed - clearing cache" );
            m_game->ClearCache();
        }
        Claw::Registry::Get()->Set( "/monstaz/analytics/gakey", GAME_ANALYTICS_KEY );

        m_game->Initialize( GAME_ANALYTICS_KEY, GAME_ANALYTICS_SECRET, GenerateBuildName().c_str() );

        m_initialized = true;

        StartSession();
        CreateHandlers();
    }
}

void AnalyticsManager::StopSession()
{
    if( m_sessionRunning && m_initialized )
    {
        m_game->StopSession();
        m_flurry->StopSession();
        m_sessionRunning = false;
    }
}

void AnalyticsManager::StartSession()
{
    if( !m_sessionRunning && m_initialized )
    {
        m_sessionRunning = true;
        m_flurry->StartSession( FLURRY_ANALYTICS_KEY );
        m_game->StartSession();
    }
}

void AnalyticsManager::CreateHandlers()
{
    AddHandler( new AnalyticsHandlers::Install );
    AddHandler( new AnalyticsHandlers::Tutorial );
    AddHandler( new AnalyticsHandlers::Progress );
    AddHandler( new AnalyticsHandlers::Weapons );
    AddHandler( new AnalyticsHandlers::ShopItems );
    AddHandler( new AnalyticsHandlers::Cash );
    AddHandler( new AnalyticsHandlers::FreeStuff );
    AddHandler( new AnalyticsHandlers::Perks );
    AddHandler( new AnalyticsHandlers::Missions );
    AddHandler( new AnalyticsHandlers::SummaryOffer );
    AddHandler( new AnalyticsHandlers::LimitedTimeOffer );
    AddHandler( new AnalyticsHandlers::SocialLogin );
    AddHandler( new AnalyticsHandlers::SocialFriend );
    AddHandler( new AnalyticsHandlers::SocialGift );
    AddHandler( new AnalyticsHandlers::Fuel );
    AddHandler( new AnalyticsHandlers::NewWeapon );
}

void AnalyticsManager::AddHandler( EventHandler* handler )
{
    std::pair<EventHandlersSet::iterator, bool> result = m_allHandlers.insert( EventHandlerPtr( handler ) ); 
    CLAW_ASSERT( result.second );
    if( result.second )
    {
        handler->Initialize( this );
    }
}

Claw::NarrowString AnalyticsManager::GenerateBuildName( bool realVersion ) const
{
    Claw::NarrowString syncGroup = ClawExt::ServerSync::GetInstance()->GetSyncGroupId();
    if( syncGroup.empty() )
    {
        if( realVersion )
        {
            syncGroup = Claw::g_appVersion;
            syncGroup +=  "-";
        }
        syncGroup += ServerConstants::SYNC_DATA_PLATFORM;
        syncGroup +=  "-";
        syncGroup += SYNC_GROUP_DEFAULT;
    }
    else if( realVersion )
    {
        Claw::NarrowString prefix = Claw::g_appVersion;
        prefix += "-";
        syncGroup = prefix + syncGroup;
    }
    return syncGroup;
}

void AnalyticsManager::OnFocusChange( bool focus )
{
    CLAW_MSG( "AnalyticsManager::OnFocusChange(): " << focus );
    if( focus )
    {
        StartSession();
    }
    else
    {
        StopSession();
    }
}

void AnalyticsManager::RegisterHandler( GamEventId eventId, EventHandler* handler )
{
    m_eventHandlers[eventId].insert( EventHandlerPtr(handler) );
}

bool AnalyticsManager::HandleGameEvent( const GameEvent& ev )
{
    if( m_initialized )
    {
        EventHandlers::iterator it = m_eventHandlers.find( ev.GetId() );
        if( it != m_eventHandlers.end() )
        {
            EventHandlersSet::iterator sIt = it->second.begin();
            EventHandlersSet::iterator sEnd = it->second.end();

            bool ret = false;
            for( ; sIt != sEnd; ++ sIt )
            {
                ret |= (*sIt)->HandleGameEvent( ev );
            }
            return ret;
        }
    }
    return false;
}

Claw::NarrowString AnalyticsManager::GetLevelUID() const
{
    int stage = 0;
    int zone = 0;
    int level = 0;

    bool endless = false;
    bool survival = false;

    // Menu state
    if( m_lua && m_lua->Call( "StoryIsEndlessMode", 0, 1 ) )
    {
        endless = m_lua->CheckBool( -1 );
        m_lua->Pop(1);
        if( !endless )
        {
            Claw::Registry::Get()->Get( "/maps/current", stage );

            m_lua->PushNumber( stage );
            m_lua->Call( "StoryIsBossAreaUnlocked", 1, 1 );
            bool boss = m_lua->CheckBool( -1 );
            m_lua->Pop(1);
            if( boss )
            {
                m_lua->PushNumber( stage );
                m_lua->Call( "StroyGetAreasCount", 1, 1 );
                zone = m_lua->CheckNumber( -1 ) + 1;
                m_lua->Pop(1);

                m_lua->PushNumber( stage );
                m_lua->PushString( "boss" );
                m_lua->Call( "StoryGetMissionIdx", 2, 1 );
                level = m_lua->CheckNumber( -1 );
                m_lua->Pop(1);
            }
            else
            {
                m_lua->PushNumber( stage );
                m_lua->Call( "StoryGetFirstUnlockedArea", 1, 1 );
                zone = m_lua->CheckNumber( -1 );
                m_lua->Pop(1);

                m_lua->PushNumber( stage );
                m_lua->PushNumber( zone );
                m_lua->Call( "StoryGetMissionIdx", 2, 1 );
                level = m_lua->CheckNumber( -1 );
                m_lua->Pop(1);
            }
        }
    }
    // Gameplay state
    else
    {
        // We can fore to report only stroy mode level UID even if we are playing survial
        Claw::Registry::Get()->Get( "/internal/survival", survival );

        if( !survival )
        {
            Claw::Registry::Get()->Get( "/internal/endless", endless );
            if( !endless )
            {
                bool boss = false;
                Claw::Registry::Get()->Get( "/internal/map", stage );
                Claw::Registry::Get()->Get( "/internal/boss", boss );
                if( boss )
                {
                    Claw::Registry::Get()->Get( "/internal/areascount", zone );
                    ++zone;
                }
                else
                {
                    Claw::Registry::Get()->Get( "/internal/areaidx", zone );
                }
                Claw::Registry::Get()->Get( "/internal/levelidx", level );
            }
        }
        else
        {
            Claw::Registry::Get()->Get( "/internal/survivalLevel", level );
        }
    }

    if( endless )
    {
        Claw::Registry::Get()->Get( "/maps/current", stage );

        std::ostringstream completedKey;
        completedKey << "/maps/" << stage << "//completed-missions-count";

        Claw::Registry::Get()->Get( completedKey.str().c_str(), level );
    }

    std::ostringstream result;

    if( survival )
    {
        result << std::setfill('0') << std::setw(3) << level;
    }
    else if( endless )
    {
        result << 1000 + level;
    }
    else
    {
        result << stage << zone << level;
    }

    return result.str();
}

Claw::NarrowString AnalyticsManager::EventHandler::GenerateEvent( const EventHierarchy& eh )
{
    Claw::NarrowString finalEv = "";

    EventHierarchy::const_iterator it = eh.begin();
    EventHierarchy::const_iterator end = eh.end();

    for( ; it != end; ++it )
    {
        if( !finalEv.empty() )
        {
            finalEv.append( HIERARCHY_SEPARATOR );
        }
        finalEv.append( *it );
    }
    return finalEv;
}

void AnalyticsManager::EventHandler::LogBusinessEvent( const Claw::NarrowString& ev, const char* currency, int amount, const char* area )
{
    if( area )
    {
        AnalyticsManager::GetInstance()->GetGameAnalytics()->LogBusinessEvent( ev.c_str(), currency, amount, ClawExt::GameAnalytics::AreaInfo( area ) );
    }
    else
    {
        AnalyticsManager::GetInstance()->GetGameAnalytics()->LogBusinessEvent( ev.c_str(), currency, amount );
    }
}

void AnalyticsManager::EventHandler::LogQualityEvent( const Claw::NarrowString& ev, const char* message, const char* area )
{
    if( area )
    {
        AnalyticsManager::GetInstance()->GetGameAnalytics()->LogQualityEvent( ev.c_str(), message, ClawExt::GameAnalytics::AreaInfo( area ) );
    }
    else
    {
        AnalyticsManager::GetInstance()->GetGameAnalytics()->LogQualityEvent( ev.c_str(), message );
    }
}

void AnalyticsManager::EventHandler::LogDesignEvent( const Claw::NarrowString& ev, float value, const char* area )
{
    if( area )
    {
        AnalyticsManager::GetInstance()->GetGameAnalytics()->LogDesignEvent( ev.c_str(), value, ClawExt::GameAnalytics::AreaInfo( area ) );
    }
    else
    {
        AnalyticsManager::GetInstance()->GetGameAnalytics()->LogDesignEvent( ev.c_str(), value );
    }
}

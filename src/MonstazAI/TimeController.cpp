#include "claw/math/Math.hpp"

#include "MonstazAI/TimeController.hpp"

TimeController::TimeController()
    : m_scale( 1 )
    , m_time( 0 )
    , m_target( 0 )
    , m_stop( false )
{
}

TimeController::~TimeController()
{
}

void TimeController::Update( float dt )
{
    if( m_target > m_time )
    {
        m_time += dt;
        if( m_time > m_target )
        {
            if( m_stop )
            {
                m_scale = 0;
            }
            else
            {
                m_scale = 1;
            }
        }
        else
        {
            if( m_stop )
            {
                m_scale = Claw::SmoothStep( m_target, 0.f, m_time );
            }
            else
            {
                m_scale = Claw::SmoothStep( 0.f, m_target, m_time );
            }
        }
    }
}

void TimeController::Switch( float time )
{
    m_stop = !m_stop;
    m_target = time;
    m_time = 0;
}

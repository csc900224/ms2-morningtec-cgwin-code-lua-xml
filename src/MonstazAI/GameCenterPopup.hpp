#ifndef __MONSTAZ_GAME_CENTER_POPUP_HPP__
#define __MONSTAZ_GAME_CENTER_POPUP_HPP__

class GameCenterPopup
{
public:
    static void Initialize();
    static void Release();

    static void Show( bool authenticate = false );

private:
    GameCenterPopup() {}

}; // class GameCenterPopup

#endif // __MONSTAZ_GAME_CENTER_POPUP_HPP__

#ifndef __INCLUDED__FILECONSTANTS_HPP__
#define __INCLUDED__FILECONSTANTS_HPP__

#include "MonstazAI/LanguageSettings.hpp"

namespace FileConstants
{
    extern const char* LOCALE[LanguageSettings::L_NUM];

} // namespace FileConstants

#endif // !defined __INCLUDED__FILECONSTANS_HPP__
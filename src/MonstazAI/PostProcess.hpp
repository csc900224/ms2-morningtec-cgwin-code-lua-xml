#ifndef __MONSTAZ__POSTPROCESS_HPP__
#define __MONSTAZ__POSTPROCESS_HPP__

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/OpenGLShader.hpp"
#include "claw/graphics/Surface.hpp"

class PostProcess : public Claw::RefCounter
{
public:
    PostProcess( const char* vertex, const char* fragment );
    ~PostProcess();

    void Render( Claw::Surface* src, Claw::Surface* dst );

    Claw::OpenGLShaderPtr m_shader;
    bool m_active;
};

typedef Claw::SmartPtr<PostProcess> PostProcessPtr;

#endif

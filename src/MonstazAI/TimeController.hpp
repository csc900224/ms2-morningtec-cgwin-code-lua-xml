#ifndef __MONSTAZ_TIMECONTROLLER_HPP__
#define __MONSTAZ_TIMECONTROLLER_HPP__

#include "claw/base/SmartPtr.hpp"

class TimeController : public Claw::RefCounter
{
public:
    TimeController();
    ~TimeController();

    void Update( float dt );
    float Transform( float dt ) { return dt * m_scale; }

    void Switch( float time );

private:
    float m_scale;
    float m_time;
    float m_target;
    bool m_stop;
};

typedef Claw::SmartPtr<TimeController> TimeControllerPtr;

#endif

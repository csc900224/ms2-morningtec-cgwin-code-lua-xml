// Internal includes
#include "MonstazAI/VibraController.hpp"

// External includes
#include "claw/base/Errors.hpp"
#include "claw/application/AbstractApp.hpp"
#include "claw/base/Registry.hpp"

static VibraController* s_instance;

struct VfxData
{
    float m_time;
    float m_power;
}; // struct VfxData

static const VfxData VIBRA_FX_TABLE[VibraController::VFX_COUNT] = {
    { 1.0f, 1.0f },
    { 1.0f, 1.0f },
    { 1.0f, 1.0f }
};

LUA_DECLARATION( VibraController )
{
    METHOD( VibraController, StartLoop ),
    METHOD( VibraController, StartVfx ),
    METHOD( VibraController, Stop ),
    {0,0}
};

void VibraController::Init( Claw::Lua* lua )
{
    Claw::Lunar<VibraController>::Register( *lua );
    Claw::Lunar<VibraController>::push( *lua, this );
    lua->RegisterGlobal( "VibraController" );

    lua->CreateEnumTable();
    lua->AddEnum( VFX_MENU_SELECT_ITEM );
    lua->AddEnum( VFX_PLAYER_COLLECT_ITEM );
    lua->AddEnum( VFX_PLAYER_HIT );
    lua->RegisterEnumTable( "Vfx" );
}

int VibraController::l_StartLoop( lua_State* L )
{
    Claw::Lua lua( L );
    if( lua.IsNumber( 1 ) && lua.IsNumber( 2 ) && lua.IsNumber( 3 ) )
    {
        StartLoop( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ), static_cast<int>( lua.CheckNumber( 3 ) ) );
    }
    else
    {
        CLAW_ASSERT( !"Wrong number of parameters or input format." );
    }
    return 0;
}

int VibraController::l_StartVfx( lua_State* L )
{
    Claw::Lua lua( L );
    StartVfx( lua.CheckEnum<Vfx>( 1 ) );
    return 0;
}

int VibraController::l_Stop( lua_State* L )
{
    Stop();
    return 0;
}

VibraController::VibraController()
    : m_defaultPower( 1.0f )
    , m_systemGain( 1.0f )
    , m_loopActiveTime( 0.0f )
    , m_loopSilentTime( 0.0f )
    , m_loopWaitTime( 0.0f )
    , m_loopPower( 0.0f )
    , m_loopCount( 0 )
    , m_enabled( false )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;

    // Register callback for vibra enable/disable via setttings registry
    Claw::Registry::Get()->RegisterCallback( "/monstaz/settings/vibra", VibraChanged, this );
}

VibraController::VibraController( float defaultPower, float systemGain /* = 1.0f */ )
    : m_defaultPower( defaultPower )
    , m_systemGain( systemGain )
    , m_loopActiveTime( 0.0f )
    , m_loopSilentTime( 0.0f )
    , m_loopWaitTime( 0.0f )
    , m_loopPower( 0.0f )
    , m_loopCount( 0 )
    , m_enabled( false )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;

    // Register callback for vibra enable/disable via setttings registry
    Claw::Registry::Get()->RegisterCallback( "/monstaz/settings/vibra", VibraChanged, this );
}

VibraController::~VibraController()
{
    Stop();
}

VibraController* VibraController::GetInstance()
{
    CLAW_ASSERT( s_instance );
    return s_instance;
}

void VibraController::Start( float time )
{
    StartVibraImpl( time, m_defaultPower );
}

void VibraController::Start( float time, float power )
{
    StartVibraImpl( time, power );
}

void VibraController::StartLoop( float time, float interval, unsigned int count )
{
    //CLAW_MSG( "StartLoop(): time: " << time << ", interval: " << interval << ", count: " << count );

    // Currently in loop
    if( m_loopCount > 0 )
        return;
    if( count < 1 )
        return;

    m_loopCount = count - 1;
    m_loopActiveTime = time;
    m_loopSilentTime = interval;
    m_loopWaitTime = time + interval;
    m_loopPower = m_defaultPower;

    StartVibraImpl( m_loopActiveTime, m_loopPower );
}

void VibraController::StartVfx( Vfx vfx )
{
    Start( VIBRA_FX_TABLE[vfx].m_time, VIBRA_FX_TABLE[vfx].m_power );
}

void VibraController::Stop()
{
    m_loopCount = 0;

    StopVibraImpl();
}

void VibraController::Update( float dt )
{
    if( m_loopCount > 0 )
    {
        if( m_loopWaitTime > dt )
        {
            m_loopWaitTime -= dt;
        }
        else
        {
            if( --m_loopCount )
            {
                StartVibraImpl( m_loopActiveTime, m_loopPower );
                m_loopWaitTime += m_loopActiveTime + m_loopSilentTime - dt;
            }
            else
            {
                m_loopWaitTime = 0;
            }
        }
    }
}

void VibraController::StartVibraImpl( float time, float power )
{
    if( m_enabled )
    {
        //CLAW_MSG( "VibraController::StartVibraImpl(): enabled: " << ( m_enabled ? "true" : "false" ) << ", time " << time << ", power: " << power ); 
        Claw::AbstractApp::GetInstance()->StartVibra( time, m_systemGain * power );
    }
}

void VibraController::StopVibraImpl()
{
    //CLAW_MSG( "VibraController::StopVibraImpl(): enabled: " << ( m_enabled ? "true" : "false" ) );
    Claw::AbstractApp::GetInstance()->StopVibra();
}

void VibraController::VibraChanged( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node )
{
    bool enabled = Claw::UnsafeAnyCast<bool>( node->GetData() );
    CLAW_MSG( "VibraController::VibraChanged(): enabled: " << ( enabled ? "true" : "false" ) );
    ( (VibraController*)ptr )->SetEnabled( enabled );
}

// EOF

#include <algorithm>
#include <iterator>

#include "MonstazAI/scene/space/QuadTree.hpp"

namespace Scene
{

    const unsigned int QuadTreeNode::MAX_DEPTH = 10;

    QuadTreeNode::QuadTreeNode()
    {
        for( int i=0; i<4; i++ )
        {
            m_child[i] = NULL;
        }
        m_depth = 0;
        m_bv.SetEmpty();
    }

    QuadTreeNode::QuadTreeNode( QuadTreeNode* parent, const Vectorf& center, const Vectorf& extents )
    {
        for( int i=0; i<4; i++ )
        {
            m_child[i] = NULL;
        }
        m_depth = parent->m_depth + 1;
        m_bv.SetCenterExtents(center, extents);
    }

    QuadTreeNode::~QuadTreeNode()
    {
        for( int i=0; i<4; i++ )
        {
            delete m_child[i];
        }
    }

    void QuadTreeNode::Render( Claw::Surface* target, const Vectorf& offset, float scale )
    {
        Claw::Rect rect(
            m_bv.GetMin().x * scale - offset.x,
            m_bv.GetMin().y * scale - offset.y,
            m_bv.GetExtents().x * 2 * scale,
            m_bv.GetExtents().y * 2 * scale);

        target->DrawRectangle( rect, Claw::MakeRGB( 255, 255, 255 ) );
        for( int i=0; i<4; i++ )
        {
            if( m_child[i] )
            {
                m_child[i]->Render( target, offset, scale );
            }
        }
    }

    void QuadTreeNode::BuildTree( const std::vector<Obstacle*>& v )
    {
        std::vector<Obstacle*>::const_iterator it = v.begin();
        Vectorf min = (*it)->m_bv->GetMin();
        Vectorf max = (*it)->m_bv->GetMax();
        ++it;
        while( it != v.end() )
        {
            Vectorf tmp = (*it)->m_bv->GetMin();
            min.x = std::min( min.x, tmp.x );
            min.y = std::min( min.y, tmp.y );
            tmp = (*it)->m_bv->GetMax();
            max.x = std::max( max.x, tmp.x );
            max.y = std::max( max.y, tmp.y );
            ++it;
        }

        m_bv.SetMinMax( min, max );

        for( std::vector<Obstacle*>::const_iterator it = v.begin(); it != v.end(); ++it )
        {
            AddEntityInternal( *it );
        }
    }

    Obstacle* QuadTreeNode::QueryCollision( const Vectorf& pos )
    {
        for( std::vector<Obstacle*>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it )
        {
            if( (*it)->m_bv->Overlaps( pos ) )
            {
                return *it;
            }
        }

        int idx;
        if( pos.y < m_bv.GetCenter().y )
        {
            if( pos.x < m_bv.GetCenter().x )
            {
                idx = 0;
            }
            else
            {
                idx = 1;
            }
        }
        else
        {
            if( pos.x < m_bv.GetCenter().x )
            {
                idx = 2;
            }
            else
            {
                idx = 3;
            }
        }

        if( m_child[idx] )
        {
            return m_child[idx]->QueryCollision( pos );
        }

        return NULL;
    }

    bool QuadTreeNode::RemoveEntity( Obstacle* entity )
    {
        for( std::vector<Obstacle*>::iterator it = m_entities.begin(); it != m_entities.end(); ++it )
        {
            if( *it == entity )
            {
                m_entities.erase( it );
                return true;
            }
        }
        for( int i=0; i<4; i++ )
        {
            if( m_child[i] )
            {
                if( m_child[i]->RemoveEntity( entity ) )
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool QuadTreeNode::IsInsideQuad( Obstacle* entity, const Vectorf& center, const Vectorf& extents ) const
    {
        const Vectorf& min = entity->m_bv->GetMin();
        const Vectorf& max = entity->m_bv->GetMax();
        return
            min.x >= center.x - extents.x &&
            max.x <= center.x + extents.x &&
            min.y >= center.y - extents.y &&
            max.y <= center.y + extents.y;
    }

    void QuadTreeNode::AddEntityInternal( Obstacle* entity )
    {
        if( m_depth < MAX_DEPTH )
        {
            Vectorf extent = m_bv.GetExtents() * 0.5f;
            const Vectorf& center = m_bv.GetCenter();

            int childIdx = 0;
            for( int j = 0; j < 2; j++ )
            {
                for( int i = 0; i < 2; i++ )
                {
                    Vectorf childNodeCenter( center.x + ((i == 0) ? -extent.x : extent.x), center.y + ((j == 0) ? -extent.y : extent.y) );

                    if( IsInsideQuad( entity, childNodeCenter, extent ) )
                    {
                        if( !m_child[childIdx] )
                        {
                            m_child[childIdx] = new QuadTreeNode( this, childNodeCenter, extent );
                        }
                        return m_child[childIdx]->AddEntityInternal( entity );
                    }

                    childIdx++;
                }
            }
        }

        m_entities.push_back( entity );
    }

    bool QuadTreeNode::Walker(const QuadTreeNode* currentNode, QuadTreeNode::WalkerCallback callback, void* userData)
    {
        if( callback( currentNode, userData ) )
        {
            return true;
        }

        bool ret = false;
        for( int i=0; i<4; i++ )
        {
            if( currentNode->m_child[i] )
            {
                if( QuadTreeNode::Walker( currentNode->m_child[i], callback, userData ) )
                {
                    ret = true;
                }
            }
        }
        return ret;
    }

    bool QuadTreeNode::CollisionCallback(const QuadTreeNode* node, CollisionQuery* query)
    {
        if( !node->m_bv.Intersect( query->GetQueryBV() ) )
        {
            return true;
        }

        for( std::vector<Obstacle*>::const_iterator it = node->m_entities.begin(); it != node->m_entities.end(); ++it )
        {
            if( query->GetQueryBV()->Intersect( (*it)->m_bv ) )
            {
                query->GetColliders().push_back( *it );
            }
        }

        return false;
    }

    bool QuadTreeNode::RayTraceCallback(const QuadTreeNode* node, RayTraceQuery* query)
    {
        if( !query->GetQueryRay()->Intersect( &node->m_bv ) )
        {
            return true;
        }

        for( std::vector<Obstacle*>::const_iterator it = node->m_entities.begin(); it != node->m_entities.end(); ++it )
        {
            if( query->GetQueryRay()->Intersect( (*it)->m_bv ) )
            {
                query->Add( *it );
            }
        }

        return false;
    }

}

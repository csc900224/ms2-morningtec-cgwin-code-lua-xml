#ifndef __SCENE_QUADTREE_HPP__
#define __SCENE_QUADTREE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/obstacle/Obstacle.hpp"
#include "MonstazAI/scene/CollisionQuery.hpp"
#include "MonstazAI/scene/collisions/AARect.hpp"

namespace Scene
{

    class QuadTreeNode
    {
    public:
        QuadTreeNode();
        ~QuadTreeNode();

        void Render( Claw::Surface* target, const Vectorf& offset, float scale );

        void BuildTree( const std::vector<Obstacle*>& v );

        bool QueryCollision( CollisionQuery* query )    { return QuadTreeNode::Walker(this, (QuadTreeNode::WalkerCallback)QuadTreeNode::CollisionCallback, query); }
        Obstacle* QueryCollision( const Vectorf& pos );
        bool QueryRayTrace( RayTraceQuery* query )      { return QuadTreeNode::Walker(this, (QuadTreeNode::WalkerCallback)QuadTreeNode::RayTraceCallback, query); }

        bool RemoveEntity( Obstacle* entity );

        typedef bool (*WalkerCallback)( const QuadTreeNode* currentNode, void* userData );

        static bool Walker( const QuadTreeNode* currentNode, QuadTreeNode::WalkerCallback callback, void* userData );

    protected:
        QuadTreeNode( QuadTreeNode* parent, const Vectorf& center, const Vectorf& extents );

        void AddEntityInternal( Obstacle* entity );
        bool IsInsideQuad( Obstacle* entity, const Vectorf& center, const Vectorf& extents ) const;

        static bool CollisionCallback( const QuadTreeNode* node, CollisionQuery* query );
        static bool RayTraceCallback( const QuadTreeNode* node, RayTraceQuery* query );

        QuadTreeNode* m_child[4];

        std::vector<Obstacle*> m_entities;

        AARect m_bv;
        unsigned int m_depth;

        static const unsigned int MAX_DEPTH;
    };

}

#endif

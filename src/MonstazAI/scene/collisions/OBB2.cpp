#include "MonstazAI/scene/collisions/BoundingArea.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"
#include "MonstazAI/scene/collisions/AARect.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"

#include "claw/base/Errors.hpp"

namespace Scene
{

    OBB2::OBB2()
        : BoundingArea( BA_OBB2 )
    {
        m_pos.x = m_pos.y = 0;

        SetEmpty();
    }

    OBB2::OBB2(const Vectorf& center)
        : BoundingArea( BA_OBB2 )
    {
        m_pos = center;

        SetEmpty();
    }

    bool OBB2::Intersect( const OBB2* pOBB2 ) const
    {
        CLAW_MSG_ASSERT( pOBB2, "NULL passed to function OBB2::Intersect" );

        const Vectorf vAxesA[] = { GetAxis(0), GetAxis(1) };
        const Vectorf vAxesB[] = { pOBB2->GetAxis(0), pOBB2->GetAxis(1) };
        const Vectorf& vExtentsA = m_extents;
        const Vectorf& vExtentsB = pOBB2->m_extents;

        Vectorf vDiff = pOBB2->GetCenter() - GetCenter();

        float aafAbsAdB[2][2];
        float fAbsAdD, fRSum;

        aafAbsAdB[0][0] = fabs( DotProduct( vAxesA[0], vAxesB[0] ) );
        aafAbsAdB[0][1] = fabs( DotProduct( vAxesA[0], vAxesB[1] ) );
        fAbsAdD = fabs( DotProduct( vAxesA[0], vDiff ) );
        fRSum = vExtentsA[0] + vExtentsB[0]*aafAbsAdB[0][0] + vExtentsB[1]*aafAbsAdB[0][1];
        if (fAbsAdD > fRSum)
        {
            return false;
        }

        aafAbsAdB[1][0] = fabs( DotProduct( vAxesA[1], vAxesB[0] ) );
        aafAbsAdB[1][1] = fabs( DotProduct( vAxesA[1], vAxesB[1] ) );
        fAbsAdD = fabs( DotProduct( vAxesA[1], vDiff ) );
        fRSum = vExtentsA[1] + vExtentsB[0]*aafAbsAdB[1][0] + vExtentsB[1]*aafAbsAdB[1][1];
        if (fAbsAdD > fRSum)
        {
            return false;
        }

        fAbsAdD = fabs( DotProduct( vAxesB[0], vDiff ) );
        fRSum = vExtentsB[0] + vExtentsA[0]*aafAbsAdB[0][0] + vExtentsA[1]*aafAbsAdB[1][0];
        if (fAbsAdD > fRSum)
        {
            return false;
        }

        fAbsAdD = fabs( DotProduct( vAxesB[1], vDiff ) );
        fRSum = vExtentsB[1] + vExtentsA[0]*aafAbsAdB[0][1] + vExtentsA[1]*aafAbsAdB[1][1];
        if (fAbsAdD > fRSum)
        {
            return false;
        }

        return true;
    }

    bool OBB2::Intersect( const BoundingArea* pBV ) const
    {
        switch( pBV->GetType() )
        {
        case BA_CIRCLE:
            return Intersect( (Circle*)pBV );
        case BA_OBB2:
            return Intersect( (OBB2*)pBV );
        default:
            return false;
        }
    }

    bool OBB2::Intersect( const Circle* pCircle ) const
    {
        Vectorf vDiff = pCircle->GetCenter() - GetCenter();
        float sqrDistance = 0;

        const Vectorf axes[2] = { m_axisX, Vectorf( -m_axisX.y, m_axisX.x ) };

        for( int i=0; i<2; i++ )
        {
            float dist = DotProduct( vDiff, axes[i] );
            if( dist < -m_extents[i] )
            {
                float fDelta = dist + m_extents[i];
                sqrDistance += fDelta * fDelta;
            }
            else if( dist > m_extents[i])
            {
                float fDelta = dist - m_extents[i];
                sqrDistance += fDelta * fDelta;
            }
        }

        return sqrDistance <= pCircle->GetRadius2();
    }

    bool OBB2::Overlaps( const Vectorf& point ) const
    {
        Vectorf vDiff = point - GetCenter();

        float projDist = DotProduct( m_axisX, vDiff );
        if( projDist < -m_extents.x || projDist > m_extents.x )
            return false;

        projDist = DotProduct( Vectorf( -m_axisX.y, m_axisX.x ), vDiff );
        return projDist >= -m_extents.y && projDist <= m_extents.y;
    }

    void OBB2::SetCenterExtentsAxes( const Vectorf& center, const Vectorf& extent, const Vectorf& axisX )
    {
        m_pos = center;
        m_extents = extent;
        m_axisX = axisX;
        m_axisX.Normalize();
    }

    void OBB2::SetEmpty()
    {
        m_extents.x = m_extents.y = std::numeric_limits<float>::min();
    }

    bool OBB2::IsEmpty()
    {
        return ( m_extents.x == std::numeric_limits<float>::min() ) && ( m_extents.y == std::numeric_limits<float>::min() );
    }

    void OBB2::SetPoint(const Vectorf& rPos )
    {
        m_pos = rPos;
        m_extents.x = m_extents.y = 0;
    }

    Vectorf OBB2::GetMin() const
    {
        return GetCenter() - GetAABBExtent();
    }

    Vectorf OBB2::GetMax() const
    {
        return GetCenter() + GetAABBExtent();
    }

}

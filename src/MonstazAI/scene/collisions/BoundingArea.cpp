#include "MonstazAI/scene/collisions/BoundingArea.hpp"
#include "MonstazAI/scene/collisions/AARect.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

#include "claw/base/Errors.hpp"

namespace Scene
{

    BoundingArea::BoundingArea( BAType baType )
        : m_type( baType )
    {
    }

    BoundingArea* BoundingArea::CreateInstance( BAType baType )
    {
        switch( baType )
        {
        case BA_AARECT:
            return new AARect();
        case BA_CIRCLE:
            return new Circle();
        case BA_OBB2:
            return new OBB2();
        default:
            return NULL;
        }
    }

}

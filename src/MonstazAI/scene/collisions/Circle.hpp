#ifndef __COLLISIONS_CIRCLE_HPP__
#define __COLLISIONS_CIRCLE_HPP__

#include "MonstazAI/scene/collisions/BoundingArea.hpp"

namespace Scene
{
    class AARect;
    class OBB2;

    class Circle : public BoundingArea
    {
    public:
        Circle();
        Circle(const Vectorf& rCenter, float radius);
        Circle(float radius);

        virtual bool Intersect(const BoundingArea* pBA) const;

        bool Intersect(const Circle* pCircle) const;
        bool Intersect(const AARect* pAARect) const;

        virtual bool Overlaps(const Vectorf& pPoint) const;

        void SetTranslation(const Vectorf& rPos)    { m_pos = rPos; }

        virtual void SetEmpty();
        virtual bool IsEmpty();

        virtual void SetPoint(const Vectorf& rPos );

        void SetMinMax(const Vectorf& rMin, const Vectorf& rMax);
        void SetCenterRadius(const Vectorf& rCenter, float radius);

        Vectorf GetMin() const;
        Vectorf GetMax() const;
        const Vectorf& GetCenter() const    { return m_pos; }
        float GetRadius() const             { return m_radius; }
        float GetRadius2() const            { return m_radius2; }

    private:
        float m_radius;
        float m_radius2;
    };

}

#endif

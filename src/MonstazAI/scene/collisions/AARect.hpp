#ifndef __COLLISIONS_AARECT_HPP__
#define __COLLISIONS_AARECT_HPP__

#include <math.h>

#include "MonstazAI/scene/collisions/BoundingArea.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

namespace Scene
{
    class Circle;
    class OBB2;

    class AARect : public BoundingArea
    {
    public:
        AARect();

        virtual bool Intersect(const BoundingArea* pBA) const;
        virtual bool Overlaps(const Vectorf& pPoint) const;

        void SetTranslation(const Vectorf& rCenter);

        virtual void SetEmpty();
        virtual bool IsEmpty();

        virtual void SetPoint(const Vectorf& rPos);

        void SetMinMax(const Vectorf& rMin, const Vectorf& rMax);
        void SetExtents(const Vectorf& rExtents);
        void SetCenterExtents(const Vectorf& rCenter, const Vectorf& rExtents);

        Vectorf GetMin() const;
        Vectorf GetMax() const;
        const Vectorf& GetCenter() const    { return m_pos; }
        const Vectorf& GetExtents() const   { return m_extents; }

        Vectorf GetAxis(unsigned char axis) const   { CLAW_ASSERT( axis < 2 ); return Vectorf( 1-axis, axis ); }

        bool BoundToArea(Vectorf& rPos) const;

    private:
        void ResetMinMax()
        {
            m_min = m_pos - m_extents;
            m_max = m_pos + m_extents;
        }

        void TranslateMinMax( const Vectorf& off )  { m_min += off; m_max += off; }

        Vectorf m_min;
        Vectorf m_max;
        Vectorf m_extents;
    };

}

#endif

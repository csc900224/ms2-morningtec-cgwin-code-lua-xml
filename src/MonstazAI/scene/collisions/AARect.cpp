#include <limits>

#include "MonstazAI/scene/collisions/AARect.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

namespace Scene
{

    AARect::AARect()
        : BoundingArea( BA_AARECT )
    {
        m_pos.x = m_pos.y = 0;
        SetEmpty();
    }

    bool AARect::Overlaps(const Vectorf& pPoint) const
    {
        return
            pPoint.x >= m_min.x &&
            pPoint.y >= m_min.y &&
            pPoint.x <= m_max.x &&
            pPoint.y <= m_max.y;
    }

    void AARect::SetEmpty()
    {
        m_extents.x = m_extents.y = std::numeric_limits<float>::min();
    }

    bool AARect::IsEmpty()
    {
        return ( m_extents.x == std::numeric_limits<float>::min() ) && ( m_extents.y == std::numeric_limits<float>::min() );
    }

    void AARect::SetPoint(const Vectorf& rPos )
    {
        m_pos = rPos;
        m_extents.x = m_extents.y = 0;
    }

    void AARect::SetMinMax(const Vectorf& rMin, const Vectorf& rMax)
    {
        m_pos.x = (rMax.x + rMin.x) * 0.5f;
        m_pos.y = (rMax.y + rMin.y) * 0.5f;
        m_extents.x = (rMax.x - rMin.x) * 0.5f;
        m_extents.y = (rMax.y - rMin.y) * 0.5f;
        ResetMinMax();
    }

    void AARect::SetExtents(const Vectorf& rExtents)
    {
        m_extents = rExtents;
        ResetMinMax();
    }

    void AARect::SetCenterExtents(const Vectorf& rCenter, const Vectorf& rExtents)
    {
        m_pos = rCenter;
        m_extents = rExtents;
        ResetMinMax();
    }

    bool AARect::Intersect(const BoundingArea* pBA) const
    {
        switch( pBA->GetType() )
        {
        case BoundingArea::BA_CIRCLE:
        case BoundingArea::BA_OBB2:
            return pBA->Intersect( this );
        case BoundingArea::BA_AARECT:
            {
            float distX = GetCenter().x - ((AARect*)pBA)->GetCenter().x;
            float distY = GetCenter().y - ((AARect*)pBA)->GetCenter().y;
            return  (fabs(distX) < GetExtents().x + ((AARect*)pBA)->GetExtents().x) &&
                    (fabs(distY) < GetExtents().y + ((AARect*)pBA)->GetExtents().y);
            }
        default:
            return false;
        }
    }

    void AARect::SetTranslation(const Vectorf& rCenter)
    {
        Vectorf off( rCenter - m_pos );
        TranslateMinMax( off );
        m_pos = rCenter;
    }

    bool AARect::BoundToArea(Vectorf& rPos) const
    {
        bool ret = false;

        if( rPos.x < m_min.x )
        {
            rPos.x = m_min.x;
            ret = true;
        }
        else if( rPos.x > m_max.x )
        {
            rPos.x = m_max.x;
            ret = true; 
        }

        if( rPos.y < m_min.y )
        {
            rPos.y = m_min.y;
            ret = true;
        }
        else if( rPos.y > m_max.y )
        {
            rPos.y = m_max.y;
            ret = true;
        }

        return ret;
    }

    Vectorf AARect::GetMin() const
    {
        return m_min;
    }

    Vectorf AARect::GetMax() const
    {
        return m_max;
    }

}

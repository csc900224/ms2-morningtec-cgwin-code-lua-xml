#ifndef __COLLISIONS_BOUNDING_AREA_HPP__
#define __COLLISIONS_BOUNDING_AREA_HPP__

#include "claw/graphics/Color.hpp"

#include "MonstazAI/math/Vector.hpp"

namespace Scene
{
    class BoundingArea
    {
    public:
        enum BAType
        {
            BA_AARECT,
            BA_OBB2,
            BA_CIRCLE,
        };

        virtual ~BoundingArea() {}

        BAType GetType() const { return m_type; }

        virtual bool Intersect(const BoundingArea* pBA) const = 0;
        virtual bool Overlaps(const Vectorf& pPoint) const = 0;

        virtual void SetEmpty() = 0;
        virtual bool IsEmpty() = 0;
        virtual void SetPoint(const Vectorf& rPos ) = 0;
        virtual void SetTranslation(const Vectorf& rPos) = 0;

        virtual Vectorf GetMin() const = 0;
        virtual Vectorf GetMax() const = 0;

        const Vectorf& GetTranslation() const { return m_pos; }

        static BoundingArea* CreateInstance(BAType baType = BA_AARECT);

    protected:
        BoundingArea( BAType baType );

        BAType          m_type;
        Vectorf         m_pos;
    };

}

#endif

#ifndef __COLLISIONS_OBB2_HPP__
#define __COLLISIONS_OBB2_HPP__

#include "claw/base/Errors.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/scene/collisions/OBB2.hpp"

namespace Scene
{

    class AARect;
    class Circle;

    class OBB2 : public BoundingArea
    {
    public:
        OBB2();
        OBB2(const Vectorf& center);

        virtual bool Intersect( const BoundingArea *pBV ) const;

        bool Intersect( const Circle* pCircle ) const;
        bool Intersect( const OBB2* pOBB2 ) const;

        virtual bool Overlaps(const Vectorf& pPoint) const;

        void SetTranslation(const Vectorf& rCenter) { m_pos = rCenter; }

        void SetEmpty();
        bool IsEmpty();

        void SetPoint(const Vectorf& rPos );
        void SetCenterExtentsAxes( const Vectorf& center, const Vectorf& extent, const Vectorf& axisX );

        const Vectorf&  GetCenter() const           { return m_pos; }
        const Vectorf&  GetExtents() const          { return m_extents; }
        Vectorf GetMin() const;
        Vectorf GetMax() const;
        Vectorf GetAxis( unsigned char axis ) const { return axis == 0 ? m_axisX : Vectorf( -m_axisX.y, m_axisX.x ); }

        Vectorf GetAABBExtent() const
        {
            float x = fabs( m_axisX.x * m_extents.x ) + fabs( m_axisX.y * m_extents.y );
            float y = fabs( m_axisX.y * m_extents.x ) + fabs( m_axisX.x * m_extents.y );
            return Vectorf( x, y );
        }

    private:
        Vectorf m_extents;
        Vectorf m_axisX;
    };

}

#endif

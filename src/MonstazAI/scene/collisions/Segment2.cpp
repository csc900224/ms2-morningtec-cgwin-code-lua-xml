#include "MonstazAI/scene/collisions/Segment2.hpp"
#include "MonstazAI/scene/collisions/BoundingArea.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/AARect.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

namespace Scene
{

    bool Segment2::Intersect( const BoundingArea* pBA ) const
    {
        switch( pBA->GetType() )
        {
        case BoundingArea::BA_AARECT:
            return Intersect( (AARect*)pBA );
        case BoundingArea::BA_CIRCLE:
            return Intersect( (Circle*)pBA );
        case BoundingArea::BA_OBB2:
            return Intersect( (OBB2*)pBA );
        default:
            CLAW_ASSERT( false );
            return false;
        }
    }

    static bool IntersectLine( const Vectorf& origin, const Vectorf& dir, const Circle* pCircle, unsigned int& pointsNum, float pointsDist[2] )
    {
        Vectorf vOff = origin - pCircle->GetCenter();
        float a0 = vOff.LengthSqr() - pCircle->GetRadius() * pCircle->GetRadius();
        float a1 = DotProduct( dir, vOff );
        float delta = a1 * a1 - a0;

        if (delta > 0 )
        {
            pointsNum = 2;
            delta = sqrt(delta);
            pointsDist[0] = -a1 - delta;
            pointsDist[1] = -a1 + delta;
        }
        else if (delta < 0 )
        {
            pointsNum = 0;
        }
        else
        {
            pointsNum = 1;
            pointsDist[0] = -a1;
        }

        return pointsNum != 0;
    }

    bool Segment2::Intersect( const Circle* pCircle ) const
    {
        float pointsDist[2];
        unsigned int pointsNum = 0;

        bool lineIntersect = IntersectLine( m_origin, m_dir, pCircle, pointsNum, pointsDist );
        if( lineIntersect )
        {
            if( pointsNum == 2 )
            {
                if( pointsDist[1] < 0 || pointsDist[0] > m_extent )
                {
                    pointsNum = 0;
                }
                else
                {
                    if( pointsDist[1] <= m_extent )
                    {
                        if( pointsDist[0] < 0 )
                        {
                            pointsNum = 1;
                        }
                    }
                    else
                    {
                        pointsNum = pointsDist[0] >= 0 ? 1 : 0;
                    }
                }
            }
        }
        return pointsNum > 0;
    }

    bool Segment2::Intersect( const AARect* pAARect ) const
    {
        Vectorf center = pAARect->GetCenter();
        Vectorf extents = pAARect->GetExtents();

        float Dx = m_origin.x - center.x;    if(fabs(Dx) > extents.x && (Dx * m_dir.x) >=0) return false;
        float Dy = m_origin.y - center.y;    if(fabs(Dy) > extents.y && (Dy * m_dir.y) >=0) return false;

        float f;
        Vectorf absDir( fabs(m_dir.x), fabs(m_dir.y) );
        f = m_dir.x * Dy - m_dir.y * Dx;
        if( fabs(f) > (extents.x * absDir.y) + (extents.y * absDir.x) )
            return false;

        return true;
    }

    bool Segment2::Intersect( const OBB2* pOBB2 ) const
    {
        const Vectorf vOff = m_origin + m_extent * 0.5f * m_dir - pOBB2->GetCenter();
        const Vectorf& vOBBAxisX = pOBB2->GetAxis(0);

        float dirProjAbs[2], offProjAbs[2], fRhs;
        dirProjAbs[0] = fabs( DotProduct( m_dir, vOBBAxisX ) );
        offProjAbs[0] = fabs( DotProduct( vOff, vOBBAxisX ) );
        fRhs = pOBB2->GetExtents().x + m_extent * 0.5f * dirProjAbs[0];
        if( offProjAbs[0] > fRhs )
        {
            return false;
        }

        const Vectorf& vOBBAxisY = pOBB2->GetAxis(1);

        dirProjAbs[1] = fabs( DotProduct( m_dir, vOBBAxisY ) );
        offProjAbs[1] = fabs( DotProduct( vOff, vOBBAxisY ) );
        fRhs = pOBB2->GetExtents().y + m_extent * 0.5f * dirProjAbs[1];
        if( offProjAbs[1] > fRhs )
        {
            return false;
        }

        const Vectorf vPerp( -m_dir.y, m_dir.x );
        float fLhs = fabs( DotProduct( vPerp, vOff ) );
        float fPart0 = fabs( DotProduct( vPerp, vOBBAxisX ) );
        float fPart1 = fabs( DotProduct( vPerp, vOBBAxisY ) );
        fRhs = pOBB2->GetExtents().x * fPart0 + pOBB2->GetExtents().y * fPart1;

        return fLhs <= fRhs;
    }

}

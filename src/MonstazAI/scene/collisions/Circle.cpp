#include <math.h>

#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/AARect.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

namespace Scene
{
    Circle::Circle()
        : BoundingArea( BA_CIRCLE )
    {
        m_pos.x = m_pos.y = 0;
        SetEmpty();
    }

    Circle::Circle(const Vectorf& rCenter, float radius)
        : BoundingArea( BA_CIRCLE )
        , m_radius( radius )
        , m_radius2( radius*radius )
    {
        m_pos = rCenter; 
    }

    Circle::Circle(float radius)
        : BoundingArea( BA_CIRCLE )
        , m_radius( radius )
        , m_radius2( radius*radius )
    {
        m_pos.x = m_pos.y = 0;
    }

    bool Circle::Overlaps(const Vectorf& pPoint) const
    {
        Vectorf v = m_pos - pPoint;
        return DotProduct( v, v ) <= m_radius2;
    }

    void Circle::SetEmpty()
    {
        m_radius2 = m_radius = std::numeric_limits<float>::min();
    }

    bool Circle::IsEmpty()
    {
        return m_radius == std::numeric_limits<float>::min();
    }

    void Circle::SetPoint(const Vectorf& rPos )
    {
        m_pos = rPos;
        m_radius = 0;
        m_radius2 = 0;
    }

    void Circle::SetMinMax(const Vectorf& rMin, const Vectorf& rMax)
    {
        m_pos = ( rMax + rMin ) * 0.5f;
        m_radius = ( rMax - rMin ).Length() * 0.5f;
        m_radius2 = m_radius*m_radius;
    }

    void Circle::SetCenterRadius(const Vectorf& rCenter, float radius)
    {
        m_pos = rCenter;
        m_radius = radius;
        m_radius2 = radius*radius;
    }

    bool Circle::Intersect(const BoundingArea* pBA) const
    {
        switch( pBA->GetType() )
        {
        case BoundingArea::BA_CIRCLE:
            return Intersect((Circle*)pBA);
        case BoundingArea::BA_OBB2:
            return pBA->Intersect( this );
        case BoundingArea::BA_AARECT:
            return Intersect((AARect*)pBA);
        default:
            return false;
        }
    }

    bool Circle::Intersect(const Circle* pCircle) const
    {
        Vectorf v( GetCenter() - pCircle->GetCenter() );
        float r = m_radius + pCircle->GetRadius();
        return DotProduct( v, v ) < r*r;
    }

    bool Circle::Intersect(const AARect* pAARect) const
    {
        float dist = 0; 

        float centreDist = GetCenter().x - pAARect->GetCenter().x;
        float s = centreDist + pAARect->GetExtents().x;

        if(s < 0)
        {
            dist += s*s;
            if(dist > m_radius2)        return false;
        }
        else
        {
            s = centreDist - pAARect->GetExtents().x;
            if(s > 0)
            {
                dist += s*s;
                if(dist > m_radius2)    return false;
            }
        }

        centreDist = GetCenter().y - pAARect->GetCenter().y;
        s = centreDist + pAARect->GetExtents().y;

        if(s < 0)
        {
            dist += s*s;
            if(dist > m_radius2)        return false;
        }
        else
        {
            s = centreDist - pAARect->GetExtents().y;
            if(s > 0)
            {
                dist += s*s;
                if(dist > m_radius2)    return false;
            }
        }
        return dist <= m_radius2;
    }

    Vectorf Circle::GetMin() const
    {
        return Vectorf( m_pos.x - m_radius, m_pos.y - m_radius );
    }

    Vectorf Circle::GetMax() const
    {
        return Vectorf( m_pos.x + m_radius, m_pos.y + m_radius );
    }

}

#ifndef __COLLISIONS_SEGMENT2_HPP__
#define __COLLISIONS_SEGMENT2_HPP__

#include "MonstazAI/math/Vector.hpp"

namespace Scene
{

    class BoundingArea;
    class Circle;
    class AARect;
    class OBB2;

    class Segment2
    {
    public:
        void SetOrigin( const Vectorf& rOrigin )    { m_origin = rOrigin; }
        void SetDir( const Vectorf& rDirection )    { m_dir = rDirection; }
        void SetLength( float rLength )             { m_extent = rLength; }

        bool Intersect( const BoundingArea* pBA ) const;

        bool Intersect( const Circle* pCircle ) const;
        bool Intersect( const AARect* pAARect ) const;
        bool Intersect( const OBB2* pOBB2 ) const;

    private:
        Vectorf m_origin;
        Vectorf m_dir;
        float m_extent;
    };

}

#endif

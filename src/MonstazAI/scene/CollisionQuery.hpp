#ifndef __SCENE_COLLISION_QUERY_HPP__
#define __SCENE_COLLISION_QUERY_HPP__

#include <vector>

#include "MonstazAI/obstacle/Obstacle.hpp"
#include "MonstazAI/scene/collisions/Segment2.hpp"

namespace Scene
{

    class CollisionQuery
    {
    public:
        CollisionQuery() : m_pBV( NULL )        {}

        void SetQueryBV(const BoundingArea* pBV)            { m_colliders.clear(); m_pBV = pBV; }
        const BoundingArea* GetQueryBV() const              { return m_pBV; }
        std::vector<Obstacle*>& GetColliders()              { return m_colliders; }

    private:
        const BoundingArea* m_pBV;
        std::vector<Obstacle*> m_colliders;
    };

    class RayTraceQuery
    {
    public:
        RayTraceQuery()
            : m_pRay( NULL )
        {
        }

        void SetQueryRay( const Segment2* pRay)                     { m_colliders.clear(); m_pRay = pRay; }
        const Segment2* GetQueryRay() const                         { return m_pRay; }
        std::vector<Obstacle*>& GetColliders()                      { return m_colliders; }
        void Add( Obstacle* o )                                     { m_colliders.push_back( o ); }

    private:
        const Segment2* m_pRay;
        std::vector<Obstacle*> m_colliders;
    };

}

#endif

#ifndef __MONSTAZ_VIBRA_CONTROLLER_HPP__
#define __MONSTAZ_VIBRA_CONTROLLER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/base/String.hpp"

// Forward declaration
namespace Claw
{
    class RegistryKey;
}

class VibraController : public Claw::RefCounter
{
public:
    enum Vfx
    {
        VFX_MENU_SELECT_ITEM,

        VFX_PLAYER_COLLECT_ITEM,
        VFX_PLAYER_HIT,

        VFX_COUNT
    }; // enum Vfx

    // Lua related stuff
                    LUA_DEFINITION( VibraController );
                    VibraController( lua_State* L ) { CLAW_ASSERT( false ); }
    void            Init( Claw::Lua* lua );

    int             l_StartLoop( lua_State* L );
    int             l_StartVfx( lua_State* L );
    int             l_Stop( lua_State* L );

    // C++ interface
                    VibraController();
                    VibraController( float defaultPower, float systemGain = 1.0f );

    // Altrough singleton like implementation, need to be public for Lua access.
                    ~VibraController();

    // Must be called in application tick in order to update loops.
    void            Update( float dt );

    // Simple public interface
    void            Start( float time );
    void            Start( float time, float power );

    void            StartLoop( float time, float interval, unsigned int count );
    void            StartVfx( Vfx vibraFx );

    void            Stop();

    inline void     SetEnabled( bool enabled );
    inline bool     IsEnabled() const;

    inline void     SetPower( float power );
    inline float    GetPower() const;

    inline void     SetSystemGain( float systemGain );
    inline float    GetSystemGain() const;

    static
    VibraController* GetInstance();

private:
    void            StartVibraImpl( float time, float power );
    void            StopVibraImpl();

    // Registry callback
    static void     VibraChanged( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node );

    float           m_defaultPower;
    float           m_systemGain;

    float           m_loopActiveTime;
    float           m_loopSilentTime;
    float           m_loopWaitTime;
    float           m_loopPower;
    unsigned int    m_loopCount;

    bool            m_enabled;

}; // class VibraController

// Common type defs
typedef Claw::SmartPtr< VibraController > VibraControllerPtr;

inline void VibraController::SetEnabled( bool enabled )
{
    m_enabled = enabled;
}

inline bool VibraController::IsEnabled() const
{
    return m_enabled;
}

inline void VibraController::SetPower( float power )
{
    m_defaultPower = power;
}

inline float VibraController::GetPower() const
{
    return m_defaultPower;
}

inline void VibraController::SetSystemGain( float systemGain )
{
    m_systemGain = systemGain;
}

inline float VibraController::GetSystemGain() const
{
    return m_systemGain;
}

#endif // !defined __MONSTAZ_VIBRA_CONTROLLER_HPP__
// EOF

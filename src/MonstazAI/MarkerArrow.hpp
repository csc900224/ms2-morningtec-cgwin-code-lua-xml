#ifndef __MONSTAZ_MARKERARROW_HPP__
#define __MONSTAZ_MARKERARROW_HPP__

#include "claw/base/SmartPtr.hpp"

#include "Renderable.hpp"

class MarkerArrow : public Renderable, public Claw::RefCounter
{
public:
    enum Type
    {
        AccessCard,
        Target,
        Exit
    };

    MarkerArrow( const Vectorf& pos, int offset, Type type );
    ~MarkerArrow();

    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void Update( float dt );

    Type GetType() const { return m_type; }

private:
    float m_time;
    Claw::SurfacePtr m_arrow;
    int m_offset;
    Type m_type;
};

typedef Claw::SmartPtr<MarkerArrow> MarkerArrowPtr;

#endif

#include <string.h>

#include "MonstazAI/obstacle/Obstacle.hpp"
#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"

#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

LUA_DECLARATION( Obstacle )
{
    METHOD( Obstacle, GetPos ),
    METHOD( Obstacle, GetType ),
    {0,0}
};

void Obstacle::Init( Claw::Lua* lua )
{
    Claw::Lunar<Obstacle>::Register( *lua );

    ObstacleCircle::Init( lua );
    ObstacleRectangle::Init( lua );
}

Obstacle::Obstacle( float x, float y, Type type, Kind kind, Thrash thrash, const char* name )
    : m_pos( x, y )
    , m_type( type )
    , m_kind( kind )
    , m_thrash( thrash )
    , m_name( NULL )
{
    if( name )
    {
        m_name = new char[strlen( name ) + 1];
        strcpy( m_name, name );
    }

    // Create empty obstacle volume
    if( m_type == Circle )
        m_bv = new Scene::Circle( m_pos, 0 );
    else
        m_bv = new Scene::OBB2( m_pos );
}

Obstacle::~Obstacle()
{
    delete[] m_name;
    delete m_bv;
}

void Obstacle::Render( Claw::Surface* target, const Vectorf& offset, float scale )
{
    if( scale != 1.0f )
    {
        Vectorf pos = m_pos;
        pos *= scale;
        target->DrawFilledCircle( pos.x - offset.x, pos.y - offset.y, 5 * scale, Claw::MakeRGB( 64, 64, 16 ) );
        target->DrawCircle( pos.x - offset.x, pos.y - offset.y, 5 * scale, Claw::MakeRGB( 128, 128, 128 ) );
    }
    else
    {
        target->DrawFilledCircle( m_pos.x - offset.x, m_pos.y - offset.y, 5, Claw::MakeRGB( 64, 64, 16 ) );
        target->DrawCircle( m_pos.x - offset.x, m_pos.y - offset.y, 5, Claw::MakeRGB( 128, 128, 128 ) );
    }
}

void Obstacle::FreeName()
{
    delete[] m_name;
    m_name = NULL;
}

int Obstacle::l_GetPos( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_pos.x );
    lua.PushNumber( m_pos.y );
    return 2;
}

int Obstacle::l_GetType( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_type );
    return 1;
}

Claw::Color Obstacle::GetDebugColor() const
{
    switch( m_kind )
    {
    case Regular:
        return Claw::MakeRGB( 64, 255, 64 );
    case Ground:
        return Claw::MakeRGB( 255, 64, 64 );
    case Holo:
        return Claw::MakeRGB( 64, 64, 255 );
    default:
        return Claw::MakeRGB( 0, 0, 0 );
    }
}

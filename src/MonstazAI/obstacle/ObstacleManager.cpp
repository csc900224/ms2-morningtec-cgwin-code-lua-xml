#include "MonstazAI/obstacle/ObstacleManager.hpp"
#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"
#include "MonstazAI/GameManager.hpp"


LUA_DECLARATION( ObstacleManager )
{
    METHOD( ObstacleManager, Remove ),
    {0,0}
};

ObstacleManager::ObstacleManager( Claw::Lua* lua )
{
    Obstacle::Init( lua );

    Claw::Lunar<ObstacleManager>::Register( *lua );
    Claw::Lunar<ObstacleManager>::push( *lua, this );
    lua->RegisterGlobal( "ObstacleManager" );

    lua->CreateEnumTable();
    lua->AddEnum( Obstacle::Circle );
    lua->AddEnum( Obstacle::Rectangle );
    lua->RegisterEnumTable( "ObstacleType" );
}

ObstacleManager::~ObstacleManager()
{
    for( std::vector<Obstacle*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        delete *it;
    }
}

void ObstacleManager::Render( Claw::Surface* target, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();
    for( std::vector<Obstacle*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        (*it)->Render( target, offset, scale );
    }
    m_quadTree.Render( target, offset, scale );
}

ObstacleCircle* ObstacleManager::AddObstacleCircle( float x, float y, float r, Obstacle::Kind kind, Obstacle::Thrash thrash, const char* msname )
{
    ObstacleCircle* e = new ObstacleCircle( x, y, r, kind, thrash, msname );
    m_ents.push_back( e );
    return e;
}

ObstacleRectangle* ObstacleManager::AddObstacleRectangle( float x, float y, const Vectorf& edge, float perpendicular, Obstacle::Kind kind, Obstacle::Thrash thrash, const char* msname )
{
    ObstacleRectangle* e = new ObstacleRectangle( x, y, edge, perpendicular, kind, thrash, msname );
    m_ents.push_back( e );
    return e;
}

void ObstacleManager::BuildTree()
{
    m_quadTree.BuildTree( m_ents );
}

Scene::CollisionQuery* ObstacleManager::QueryCollision( const Vectorf& pos, float radius )
{
    if( m_queryBv.GetRadius() != radius )
    {
        m_queryBv.SetCenterRadius( pos, radius );
    }
    else
    {
        m_queryBv.SetTranslation( pos );
    }
    m_collisionQuery.SetQueryBV( &m_queryBv );
    m_quadTree.QueryCollision( &m_collisionQuery );
    return &m_collisionQuery;
}

Scene::RayTraceQuery* ObstacleManager::QueryRayTrace( const Vectorf& pos, const Vectorf& dir, float len )
{
    m_querySegment.SetOrigin( pos );
    m_querySegment.SetDir( dir );
    m_querySegment.SetLength( len );
    m_rayTraceQuery.SetQueryRay( &m_querySegment );
    m_quadTree.QueryRayTrace( &m_rayTraceQuery );
    return &m_rayTraceQuery;
}

int ObstacleManager::l_Remove( lua_State* L )
{
    Claw::Lua lua( L );

    const char* name = lua.CheckCString( 1 );
    int removed = 0;

    std::vector<Obstacle*>::iterator it = m_ents.begin();
    while( it != m_ents.end() )
    {
        const char* oname = (*it)->GetName();
        if( oname && strcmp( name, oname ) == 0 )
        {
            CLAW_VERIFY( m_quadTree.RemoveEntity( *it ) );
            removed++;
            it = m_ents.erase( it );
        }
        else
        {
            ++it;
        }
    }

    return removed;
}

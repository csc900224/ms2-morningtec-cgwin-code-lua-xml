#ifndef __MONSTAZ_OBSTACLEMANAGER_HPP__
#define __MONSTAZ_OBSTACLEMANAGER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/obstacle/Obstacle.hpp"
#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"

#include "MonstazAI/scene/space/QuadTree.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/Segment2.hpp"

#include <vector>


class ObstacleManager : public Claw::RefCounter
{
public:
    LUA_DEFINITION( ObstacleManager );
    ObstacleManager( lua_State* L ) { CLAW_ASSERT( false ); }

    ObstacleManager( Claw::Lua* lua );
    ~ObstacleManager();

    void Render( Claw::Surface* target, const Vectorf& offset );

    ObstacleCircle* AddObstacleCircle( float x, float y, float r, Obstacle::Kind kind, Obstacle::Thrash thrash, const char* msname );
    ObstacleRectangle* AddObstacleRectangle( float x, float y, const Vectorf& edge, float perpendicular, Obstacle::Kind kind, Obstacle::Thrash thrash, const char* msname );

    void BuildTree();

    const std::vector<Obstacle*>& GetObstacles() const { return m_ents; }

    Scene::CollisionQuery* QueryCollision( const Vectorf& pos, float radius );
    Obstacle* QueryCollision( const Vectorf& pos ) { return m_quadTree.QueryCollision( pos ); }
    Scene::RayTraceQuery* QueryRayTrace( const Vectorf& pos, const Vectorf& dir, float len );

    int l_Remove( lua_State* L );

private:
    std::vector<Obstacle*> m_ents;

    Scene::QuadTreeNode     m_quadTree;
    Scene::CollisionQuery   m_collisionQuery;
    Scene::RayTraceQuery    m_rayTraceQuery;
    Scene::Circle           m_queryBv;
    Scene::Segment2         m_querySegment;
};

typedef Claw::SmartPtr<ObstacleManager> ObstacleManagerPtr;

#endif

#include "claw/graphics/Color.hpp"

#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"

LUA_DECLARATION( ObstacleCircle )
{
    METHOD( ObstacleCircle, GetPos ),
    METHOD( ObstacleCircle, GetType ),
    METHOD( ObstacleCircle, GetRadius ),
    {0,0}
};

void ObstacleCircle::Init( Claw::Lua* lua )
{
    Claw::Lunar<ObstacleCircle>::Register( *lua );
}

ObstacleCircle::ObstacleCircle( float x, float y, float r, Kind kind, Thrash thrash, const char* name )
    : Obstacle( x, y, Obstacle::Circle, kind, thrash, name )
    , ObjectCircle( r )
{
    static_cast<Scene::Circle*>(m_bv)->SetCenterRadius( Vectorf( x, y ), r );
}

void ObstacleCircle::Render( Claw::Surface* target, const Vectorf& offset, float scale )
{
    Vectorf pos = m_pos;
    pos *= scale;
    target->DrawCircle( pos.x - offset.x, pos.y - offset.y, m_r * scale, GetDebugColor() );
}

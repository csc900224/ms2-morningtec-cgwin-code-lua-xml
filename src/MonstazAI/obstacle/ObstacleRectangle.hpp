#ifndef __MONSTAZ_OBSTACLERECTANGLE_HPP__
#define __MONSTAZ_OBSTACLERECTANGLE_HPP__

#include "MonstazAI/obstacle/Obstacle.hpp"
#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/ObjectRectangle.hpp"

class ObstacleRectangle : public Obstacle, public ObjectRectangle
{
public:
    LUA_DEFINITION( ObstacleRectangle );
    ObstacleRectangle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    ObstacleRectangle( float x, float y, const Vectorf& edge, float perpendicular, Kind kind, Thrash thrash, const char* name );

    void Render( Claw::Surface* target, const Vectorf& offset, float scale );
};

#endif

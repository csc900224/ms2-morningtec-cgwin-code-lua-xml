#ifndef __MONSTAZ_OBSTACLECIRCLE_HPP__
#define __MONSTAZ_OBSTACLECIRCLE_HPP__

#include "MonstazAI/ObjectCircle.hpp"
#include "MonstazAI/obstacle/Obstacle.hpp"

class ObstacleCircle : public Obstacle, public ObjectCircle
{
public:
    LUA_DEFINITION( ObstacleCircle );
    ObstacleCircle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    ObstacleCircle( float x, float y, float r, Kind kind, Thrash thrash, const char* name );

    void Render( Claw::Surface* target, const Vectorf& offset, float scale );
};

#endif

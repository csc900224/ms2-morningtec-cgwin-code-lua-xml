#ifndef __MONSTAZ_OBSTACLE_HPP__
#define __MONSTAZ_OBSTACLE_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/scene/collisions/BoundingArea.hpp"

class Obstacle
{
public:
    enum Type
    {
        Circle,
        Rectangle,
    };
    enum Kind
    {
        None,
        Regular,
        Ground,
        Holo
    };
    enum Thrash
    {
        T_None,
        T_Bush,
        T_Desert,
        T_Skull,
        T_Cactus,
        T_Ice,
        T_Cthulhu
    };

    LUA_DEFINITION( Obstacle );
    Obstacle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    virtual ~Obstacle();

    const Vectorf& GetPos() const { return m_pos; }
    Type GetType() const { return m_type; }
    Kind GetKind() const { return m_kind; }
    Thrash GetThrash() const { return m_thrash; }
    const char* GetName() const { return m_name; }
    void FreeName();

    virtual void Render( Claw::Surface* target, const Vectorf& offset, float scale );

    int l_GetPos( lua_State* L );
    int l_GetType( lua_State* L );

    Scene::BoundingArea* m_bv;

protected:
    Obstacle() { CLAW_ASSERT( false ); }
    Obstacle( float x, float y, Type type, Kind kind, Thrash thrash, const char* name );

    Claw::Color GetDebugColor() const;

    Vectorf m_pos;
    Type m_type;
    Kind m_kind;
    Thrash m_thrash;
    char* m_name;
};

#endif

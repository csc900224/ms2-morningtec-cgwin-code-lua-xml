#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

LUA_DECLARATION( ObstacleRectangle )
{
    METHOD( ObstacleRectangle, GetPos ),
    METHOD( ObstacleRectangle, GetType ),
    METHOD( ObstacleRectangle, GetEdge ),
    METHOD( ObstacleRectangle, GetPerpendicular ),
    {0,0}
};

void ObstacleRectangle::Init( Claw::Lua* lua )
{
    Claw::Lunar<ObstacleRectangle>::Register( *lua );
}

ObstacleRectangle::ObstacleRectangle( float x, float y, const Vectorf& edge, float perpendicular, Kind kind, Thrash thrash, const char* name )
    : Obstacle( x, y, Obstacle::Rectangle, kind, thrash, name )
    , ObjectRectangle( edge, perpendicular )
{
    Vectorf center( x, y );
    center += m_edge * 0.5f;
    center += m_perp * 0.5f;
    Vectorf extent( m_edgeLen, m_perpLen );
    extent *= 0.5f;
    static_cast<Scene::OBB2*>(m_bv)->SetCenterExtentsAxes( center, extent, m_edge );
}

void ObstacleRectangle::Render( Claw::Surface* target, const Vectorf& offset, float scale )
{
    float x = m_pos.x * scale - offset.x;
    float y = m_pos.y * scale - offset.y;

    Vectorf edge = m_edge;
    Vectorf perp = m_perp;
    edge *= scale;
    perp *= scale;

    Claw::Color c = GetDebugColor();

    target->DrawLine( x, y, x + edge[0], y + edge[1], c );
    target->DrawLine( x + edge[0] + perp[0], y + edge[1] + perp[1], x + edge[0], y + edge[1], c );
    target->DrawLine( x + edge[0] + perp[0], y + edge[1] + perp[1], x + perp[0], y + perp[1], c );
    target->DrawLine( x + perp[0], y + perp[1], x, y, c );
}

#ifndef __INCLUDED__LANGUAGE_SETTINGS_HPP__
#define __INCLUDED__LANGUAGE_SETTINGS_HPP__

class LanguageSettings
{
public:
    enum Language
    {
        L_ENGLISH,
        L_JAPAN,

        L_NUM
    };

    //! Get current game language.
    inline static Language GetGameLanguage();


private:
    static Language s_gameLanguage;

}; // class LanguageSettingsSettings

inline LanguageSettings::Language LanguageSettings::GetGameLanguage()
{
    return s_gameLanguage;
}

#endif // !defined __INCLUDED__LANGUAGE_SETTINGS_HPP__
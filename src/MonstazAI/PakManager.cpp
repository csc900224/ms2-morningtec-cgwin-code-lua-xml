#include <assert.h>

#include "claw/vfs/Vfs.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/PakManager.hpp"

struct PakData
{
    const char* f;  // file name
    int v;          // version
    bool r;         // res dependant
};

const PakData PaksFirstTutorial[] = {
    { "glory", 1, false },
    { "hammer", 1, false },
    { "tyrant", 1, true },
    { "adamant", 1, true },
    { "avenger", 1, true },
    { "engager", 1, true },
    { "firestorm", 1, true },
    { "immortal", 1, true },
    { "tormentor", 1, true },
    { "devastator", 1, true },
#ifdef BUILD_JP
    { "override-japan", 1, true },
#endif
    {}
};

const PakData PaksTutorialMenu[] = {
    { "gauntlet", 1, true },
    { "triumph", 1, true },
    {}
};

const PakData PakBoss1[] = { { "gorgon", 1, true }, { "intrepid", 1, false }, {} };
const PakData PakBoss2[] = { { "hydra", 1, true }, { "intrepid", 1, false }, {} };
const PakData PakBoss3[] = { { "inflexible", 1, true }, { "intrepid", 1, false }, {} };
const PakData PakEnemy1[] = { { "imperator", 1, true }, {} };
const PakData PakEnemy2[] = { { "inexorable", 1, true }, {} };
const PakData PakEnemy3[] = { { "judicator", 1, true }, {} };
const PakData PakEnemy4[] = { { "majestic", 1, true }, {} };
const PakData PakEnemy5[] = { { "nemesis", 1, true }, {} };
const PakData PakEnemy6[] = { { "predator", 1, true }, {} };
const PakData PakEnemy7[] = { { "resolution", 1, true }, {} };
const PakData PakEnemy8[] = { { "skyhook", 1, true }, {} };
const PakData PakEnviroIceDesert[] = { { "vanguard", 1, true }, { "intrepid", 1, false }, {} };
const PakData PakPlayerA[] = { { "vendetta", 1, true }, {} };
const PakData PakPlayerB[] = { { "steadfast", 1, true }, {} };
const PakData PakPlayerC[] = { { "stormhawk", 1, true }, {} };
const PakData PakPlayerD[] = { { "agonizer", 1, true }, {} };
const PakData PakPlayerE[] = { { "accuser", 1, true }, {} };
const PakData PakPlayerMech[] = { { "chimaera", 1, true }, {} };

const PakData* PakSets[] = {
    PaksFirstTutorial,
    PaksTutorialMenu,
    PakBoss1,
    PakBoss2,
    PakBoss3,
    PakEnemy1,
    PakEnemy2,
    PakEnemy3,
    PakEnemy4,
    PakEnemy5,
    PakEnemy6,
    PakEnemy7,
    PakEnemy8,
    PakEnviroIceDesert,
    PakPlayerA,
    PakPlayerB,
    PakPlayerC,
    PakPlayerD,
    PakPlayerE,
    PakPlayerMech
};

const char* Resolutions[] = {
    "normal-",
    "android-",
    "retina-",
    "fullhd-",
    "hd-"
};


PakManager* PakManager::s_instance = NULL;

PakManager::PakManager()
    : m_res( Resolutions[((MonstazApp*)MonstazApp::GetInstance())->GetScreenMode()] )
    , m_required( 0 )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;
}

PakManager::~PakManager()
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;
}

void PakManager::Require( Claw::UInt32 set )
{
    return;
    int idx = 0;
    while( set != 0 )
    {
        if( ( set & 0x1 ) != 0 && ( m_required & ( 1 << idx ) ) == 0 )
        {
            const PakData* ptr = PakSets[idx];

            char out[64];
            char in[128];

            while( ptr->f )
            {
                sprintf( out, "assetcache/%s.pak", ptr->f );
#ifdef CLAW_IPHONE
                const char* os = "ios";
#else
                const char* os = "android";
#endif

#ifdef BUILD_JP
                sprintf( in, "http://cdn.game-lion.com/files/ms2japan/paks/%s/%i/%s%s.pak", os, ptr->v, ptr->r ? m_res : "", ptr->f );
#else
                sprintf( in, "http://cdn.game-lion.com/files/ms2/paks/%s/%i/%s%s.pak", os, ptr->v, ptr->r ? m_res : "", ptr->f );
#endif

#ifdef CLAW_SDL
                auto f = std::function<void()>();
#else
                Claw::NarrowString _out( out );
                auto f = [_out] { Claw::MountPak( Claw::OpenFile( _out ), "", Claw::MF_TRANSPARENT ); };
#endif
                Download( in, out, 1 << idx, f );
                ptr++;
            };
            m_required |= ( 1 << idx );
        }
        set >>= 1;
        idx++;
    }
}

void PakManager::Download( const Claw::Uri& uri, const char* dst, Claw::UInt32 set, std::function<void()> f )
{
    return;
    Claw::LockGuard<Claw::Mutex> lock( m_lock );
    Claw::PakDownloaderPtr pd( new Claw::PakDownloader( true ) );
    pd->AddDownload( uri, dst );
    if( pd->CheckDone() )
    {
        if( f ) f();
    }
    else
    {
        CLAW_MSG( "Downloading " << uri );
        pd->Start();
        Task t = { pd, f, set };
        m_dl.push_back( t );
    }
}

void PakManager::Reset( Claw::UInt32 set )
{
    if( set == 0 ) return;

    Claw::LockGuard<Claw::Mutex> lock( m_lock );
    if( set == -1 )
    {
        m_dl.clear();
        m_required = 0;
    }
    else
    {
        std::vector<Task>::iterator it = m_dl.begin();
        while( it != m_dl.end() )
        {
            if( ( it->s & set ) != 0 )
            {
                it = m_dl.erase( it );
            }
            else
            {
                ++it;
            }
        }
        m_required &= ~set;
    }
}

bool PakManager::CheckDone( Claw::UInt32 set )
{
    if( set == 0 ) return true;

    Claw::LockGuard<Claw::Mutex> lock( m_lock );

    for( int i=0; i<m_dl.size(); i++ )
    {
        if( ( set == -1 ) || ( m_dl[i].s & set ) != 0 )
        {
            if( !m_dl[i].dl->CheckDone() )
            {
                if( m_dl[i].dl->CheckCriticalError() )
                {
                    Claw::Application::GetInstance()->Exit();
                }
                return false;
            }
            if( m_dl[i].f )
            {
                m_dl[i].f();
                m_dl[i].f = std::function<void()>();
            }
        }
    }
    return true;
}

Claw::UInt32 PakManager::CheckError( Claw::UInt32 set )
{
    Claw::UInt32 error = 0;
    if( set == 0 ) return error;

    Claw::LockGuard<Claw::Mutex> lock( m_lock );

    for( int i=0; i<m_dl.size(); i++ )
    {
        if( ( set == -1 ) || ( m_dl[i].s & set ) != 0 )
        {
            if( m_dl[i].dl->CheckConnectionError() )
            {
                error |= SoftError;
            }
            if( m_dl[i].dl->CheckCriticalError() )
            {
                error |= HardError;
            }
            if( m_dl[i].dl->CheckDiskError() )
            {
                error |= DiskError;
            }
        }
    }

    return error;
}

float PakManager::GetProgress( Claw::UInt32 set )
{
    Claw::LockGuard<Claw::Mutex> lock( m_lock );
    size_t total = 0;
    float done = 0;
    for( size_t i = 0; i<m_dl.size(); i++ )
    {
        if( set == -1 || ( m_dl[i].s & set ) != 0 )
        {
            size_t size = m_dl[i].dl->GetTotalSize();
            total += size;
            done += size * m_dl[i].dl->GetTotalProgress();
        }
    }
    return done / total;
}

/* /////////////////////////////////////////////////////////////////////////////
 *  FILE:
 *      MonstazAI/AudioSession.cpp
 *
 *  AUTHOR(S):
 *      Jacek Nijaki <jacek.nijaki@game-lion.com>
 *
 *  Copyright (c) 2011, Gamelion. All rights reserved.
 * ////////////////////////////////////////////////////////////////////////// */

#include "MonstazAI/AudioSession.hpp"

void AudioSession::SetCategory( AudioSession::Category /*category*/ )
{
}

bool AudioSession::IsOtherAudioPlaying()
{
    return false;
}

void AudioSession::RegisterIPodPlaybackStateChangedEvent( void (*callback)(void) )
{
}

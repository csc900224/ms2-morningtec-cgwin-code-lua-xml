#ifndef __INCLUDED__SERVER_CONSTANTS__HPP__
#define __INCLUDED__SERVER_CONSTANTS__HPP__

namespace ServerConstants
{
    extern const char* SYNC_DATA_PLATFORM;
    extern const char* SYNC_REMOTE_URL;
    extern const char* SYNC_SAVE_DIR;
    extern const char* SYNC_APP_NAME;
    extern const char* SYNC_ENC_KEY;
    extern const bool  SYNC_UPDATE_EVERY_START;
    extern const int   SYNC_UPDATE_PERIOD;

    extern const bool  SYNC_AB_TESTS_ENABLED;

    extern const char* APP_VERSION_SYNC_TASK;
    extern const char* APP_VERSION_REMOTE_FILE;
    extern const char* APP_VERSION_XML;

    extern const char* DEFAULT_CONFIG_DATA_SYNC_TASK;
    extern const char* DEFAULT_CONFIG_DATA_REMOTE_FILE;
    extern const char* DEFAULT_CONFIG_DATA_XML;

    extern const char* CONFIGURATION_DATA_SYNC_TASK;
    extern const char* CONFIGURATION_DATA_REMOTE_FILE;
    extern const char* CONFIGURATION_DATA_XML;

    extern const char* LUA_MENU_SYNC_TASK;
    extern const char* LUA_MENU_REMOTE_FILE;
    extern const char* LUA_MENU_LOCAL_FILE;

    extern const char* LUA_INGAME_SYNC_TASK;
    extern const char* LUA_INGAME_REMOTE_FILE;
    extern const char* LUA_INGAME_LOCAL_FILE;

}; // class ServerConstants

#endif // __INCLUDED__SERVER_CONSTANTS__HPP__
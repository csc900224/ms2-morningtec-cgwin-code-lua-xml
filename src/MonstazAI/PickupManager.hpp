#ifndef __MONSTAZ__PICKUPMANAGER_HPP__
#define __MONSTAZ__PICKUPMANAGER_HPP__

#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/AnimatedSurface.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"
#include "MonstazAI/Renderable.hpp"
#include "MonstazAI/particle/TextParticle.hpp"

class Pickup : public Renderable
{
public:
    enum Type
    {
        ElectricityGun,
        Flamer,
        Minigun,
        PlasmaGun,
        Railgun,
        RocketLauncher,
        Shotgun,
        CombatShotgun,
        SMG,
        Ripper,
        Spiral,
        Health,
        WeaponBoost,
        Cash,
        Potato,
        MagicOrb,
        Vortex,
        Chainsaw,
        LineGun,
        Magnum,
        Nailer,
        MechFlamer,
        MechRocket,
        Token,

        NumPickups
    };

    enum { LastWeapon = Spiral };

    LUA_DEFINITION( Pickup );
    Pickup( lua_State* L ) { CLAW_ASSERT( false ); }
    Pickup( const Vectorf& pos, Type type, float life, Claw::Surface* anim, void* data );
    virtual ~Pickup();
    static void Init( Claw::Lua* lua );

    bool Update( float dt );
    virtual void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    int l_GetPos( lua_State* L );
    int l_SetPos( lua_State* L );
    int l_GetType( lua_State* L );

    Type m_type;
    float m_life;
    void* m_data;

private:
    Claw::AnimatedSurfacePtr m_anim;
    int m_frame;
    float m_time;
    float m_zoom;
};

class PickupOrb : public Pickup
{
public:
    PickupOrb( const Vectorf& pos, Type type, float life, void* data );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
};


class PickupManager : public Claw::RefCounter
{
public:
    LUA_DEFINITION( PickupManager );
    PickupManager( lua_State* L ) { CLAW_ASSERT( false ); }

    PickupManager( Claw::Lua* lua );
    ~PickupManager();

    static void InitEnum( Claw::Lua* lua );

    void Render( Claw::Surface* target, const Vectorf& offset );
    void Update( float dt );

    Pickup* Add( const Vectorf& pos, Pickup::Type type, float life, void* data = NULL );
    std::vector<Pickup*>& GetPickups() { return m_ents; }

    int l_Add( lua_State* L );
    int l_Remove( lua_State* L );
    int l_CollectCash( lua_State* L );

    float GetTime() const { return m_time; }
    GfxAsset* GetPickup( Pickup::Type pickup ) { return m_pickup[pickup]; }
    GfxAsset* GetGlow( bool item, int frame ) { return item ? m_glowItem[frame] : m_glowWeapon[frame]; }
    Claw::Surface* GetMagicOrb() const { return m_magicOrb; }

    void FirstAidCourse() { m_hpGain *= 2; }
    void WeaponBoost();

private:
    std::vector<Pickup*> m_ents;
    GfxAssetPtr m_pickup[Pickup::NumPickups];
    GfxAssetPtr m_glowItem[12];
    GfxAssetPtr m_glowWeapon[12];
    Claw::SurfacePtr m_magicOrb;
    Claw::SurfacePtr m_itemPickup;
    Claw::SurfacePtr m_weaponPickup;
    Claw::SurfacePtr m_itemAppear;
    Claw::SurfacePtr m_weaponAppear;
    Claw::SurfacePtr m_itemDisappear;
    Claw::SurfacePtr m_weaponDisappear;
    float m_time;

    Claw::SurfacePtr m_cashAnim;
    Claw::SurfacePtr m_boostAnim;
    Claw::SurfacePtr m_boostLoop;

    Claw::SmartPtr<TextParticleFunctor> m_textFunctor;    float m_hpGain;
};

typedef Claw::SmartPtr<PickupManager> PickupManagerPtr;

#endif

#ifndef __MONSTAZ_USER_DATA_INPUT_HPP__
#define __MONSTAZ_USER_DATA_INPUT_HPP__

#include "claw/base/Lua.hpp"

#include "claw/graphics/Surface.hpp"
#include "guif/Screen.hpp"
#include "libpng/png.h"
#include "libpng/pngstruct.h"

class UserDataInput
{
public:
    typedef UserDataInput self;

    ~UserDataInput();

    static self*        GetInstance();
    static void         Release();

    void                Initialize();
    void                InitLua( Claw::LuaPtr lua ) { m_lua = lua; }

    void                Update( float dt );
    void                Render( Claw::Surface* target );
    void                RenderInputFields( Claw::Surface* target , Guif::Screen* screen );

    Claw::NarrowString  GetTextImageFilename( const Claw::NarrowString& url );
    void                PrepareImages() { m_createImage = true; }
    void                RefreshInputs() { m_shouldRefreshInputs = true; }
    void                SetSurfaceForControl( Guif::Control* control , Claw::Surface* surface );
    Claw::SurfacePtr    GetSurfaceForUser( const char* friendUniqueId );
private:
    UserDataInput();

    void CreateImageForText( const char* friendId );
    void WriteSurfaceToPng( Claw::SurfacePtr surface , Claw::FilePtr file );
    void RenderSurfaceForControl( Guif::Control* control , Claw::Surface* surface , Guif::Control* controlBg = NULL );

    static void PngWrite( png_structp png_ptr, png_bytep data, png_size_t length )
    {
        Claw::File* f = (Claw::File*)png_ptr->io_ptr;
        f->Write( data, length );
    }


    static self*                    s_instance;
    Claw::LuaPtr                    m_lua;

    bool                            m_createImage;
    bool                            m_shouldRefreshInputs;
    int                             m_fontSize;
    int                             m_fontSmallSize;

    Claw::SurfacePtr                m_loginSurface;
    Claw::SurfacePtr                m_passSurface;
    Claw::SurfacePtr                m_emailSurface;
    Claw::SurfacePtr                m_emailSurfaceRecovery;
    Claw::SurfacePtr                m_emailSurfaceAccount;
    Claw::SurfacePtr                m_passVerifySurface;
    Claw::SurfacePtr                m_inviteCodeSurface;

    Claw::NarrowString              m_loginText;
    Claw::NarrowString              m_passText;
    Claw::NarrowString              m_emailText;
    Claw::NarrowString              m_passVerifyText;
    Claw::NarrowString              m_inviteCodeText;

    std::map<Claw::NarrowString, Claw::SurfacePtr> m_userNameMap;

}; // class UserDataInput

#endif // !defined __MONSTAZ_USER_DATA_INPUT_HPP__
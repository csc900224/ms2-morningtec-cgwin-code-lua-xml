#ifndef __MONSTAZ_USER_DATA_MANAGER_HPP__
#define __MONSTAZ_USER_DATA_MANAGER_HPP__

#include "claw/base/Lua.hpp"
#include "MonstazAI/db/DatabaseConnection/DatabaseConnection.hpp"
#include "MonstazAI/db/UserDataDb.hpp"
#include "claw/graphics/Surface.hpp"

class UserDataManager
{
public:
    //! Synchronisation process observer class.
    class Observer
    {
    public:
        //! Synchronisation thread/process has been started.
        virtual void    OnSynchronisationStart() = 0;

        //! Synchronisation thread has ended his work.
        virtual void    OnSynchronisationEnd( bool success ) = 0;

        //! Synchronisation was skipped due tu not hitting updatePertiod from last update.
        virtual void    OnSynchronisationSkip() = 0;

        //! Taks with given id was just completed.
        virtual void    OnRequestFinished( bool success ) = 0;
    }; // class Observer

    struct SubRequest
    {
        SubRequest ( int _requestId , std::vector<Claw::NarrowString> _params ) : 
            requestID ( _requestId )
        {
            params = _params;
        }
        int requestID;
        std::vector<Claw::NarrowString> params;
    };

    struct RequestDb
    {
        RequestDb ( int _requestId , int _answerId ) : 
            requestID ( _requestId ),
            answerID ( _answerId ),
            completed ( false ),
            refreshRequired ( false ),
            recieved( false ),
            resultID ( -1 )
        {
            databaseConn.Reset(new DatabaseConnection());
            params = std::vector<Claw::NarrowString>(0);
        }

        int requestID;
        int answerID;
        int resultID;
        DatabaseConnectionPtr databaseConn;
        bool completed;
        bool recieved;
        bool refreshRequired;
        std::vector<Claw::NarrowString> params;
        std::vector<SubRequest*> subRequests;
    };

    typedef UserDataManager self;

    ~UserDataManager();

    static self*        GetInstance();
    static void         Release();

    void                InitLua( Claw::LuaPtr lua ) { m_lua = lua; }
    void                InitUserId( const char* userUniqueID );
    UserDataDb*         GetDb() const;
    Claw::NarrowString  GetLoggedUserUniqueId() const { return m_uniqueIdStr; }
    void                ForceCancel();

    //void                Update( float dt );

    bool                IsLogin();
    bool                IsRequestInProcess() const;
    void                RecieveMessage( RequestDb* reqDb, int msgReceived );
    void                HandleMessage( RequestDb* reqDbd, bool clearMessages );

    //User login
    bool     SendLoginRequest( const char* userLogin, const char* userPass );
    bool     SendLoginWithMailRequest( const char* userMail, const char* userPass );
    bool     SendLoginWithFBRequest( const char* userLogin, const char* fbId );

    //User creation
    bool     SendCreateUserRequest( const char* name, const char* mail, const char* pass, const char* language, const char* hwKey );
    bool     SendCreateUserFBRequest(const char* name ,const char* fbId, const char* language, const char* hwKey );
    bool     SendGetHasFbIdAccountRequest( const char* facebookId );

    //pass and email chenges
    bool     SendChangePasswordRequest( const char* newPass );
    bool     SendChangeEmailRequest( const char* newEmail );
    bool     SendSetEmailPasswordChange(const char* newEmail, const char* newPassword);
    bool     SendRemindPasswordRequest( const char* userEmail );

    bool     SendAddFriendRequest( const char* friendUniqueID );
    bool     SendDeleteFriend( const char* friendUniqueId );
    bool     SendGetFriendsListRequest();
    bool     SendGetFriendRequestsListRequest();
    bool     SendFriendRequestResponse( const char* friendUniqueId, bool accepted );
    bool     SendSetApproveNewFriends();

    //player stats
    bool     SendSetUserLevelRequest( const char* userLevel );
    bool     SendSetUserProgressRequest( const char* enemies_defeated, const char* completed_events);
    bool     SendSetWeaponRequest( const char* weaponName, int weaponLevel = 0);
    bool     SendSetScoreSurvivalRequest( const char* levelId, const char*  levelScore, const char* levelTime );
    bool     SendSetScoreSurvivalRequest( int levelId,int levelScore,int levelTime );

    //friend stats
    bool     SendGetFriendInfoRequest( const char* friendUniqueId );

    //gifts
    bool     SendSendGiftRequest( const char* itemId, const char* friendNickname, const char* quantity );
    bool     SendSendRewardRequest( const char* itemId, const char* friendNickname, const char* quantity );
    bool     SendGetFriendsGiftsRequest();
    bool     SendSetGiftAcceptedAll();

    //not used
    bool     SendGetVersionRequest();
    bool     SendGetDailyQuestRequest();
    bool     SendGetSurvivalScoresRequest();
    bool     SendGetSurvivalScoresFriendsRequest();
    bool     SendGetItemsCategoriesRequest();
    bool     SendSetUserFBidRequest( const char* facebookId );
    void     SendUpdateFriendListInfosRequests();

    //Update db requests
    void     UpdateResistanceDB();
    void     UpdateAll();

    //other stuff
    void     CreateAndAddBotFriend();
    bool     IsBot( const char* uniqueID );

    bool     SendSetUserParams( const char* userLevel, const char* kills, const char* events );
    bool     SendSetUserParams( int userLevel, int kills, int events );
    bool     SendGetUserParams();
    bool     SendGetUserBestWeapon();
    bool     SendSetFriendHelpRequest( const char* friendUniqueId );
    bool     SendGetFriendHelpRequest( const char* friendUniqueId );
    bool     SendResetFriendHelpRequest( const char* friendUniqueId );

    bool     SendSetGiftAccepted( const char* userItemId );

    bool     SendSetVipStatus( bool enabled );

    //! Register observer to be notiffied about synchronisation db events.
    void                        RegisterObserver( Observer* callback );

    //! Unregister observer
    void                        UnregisterObserver( Observer* callback );

    bool                        SyncData( bool all = true );
    static int                  DownloadEntry( void* ptr );

    // Event notifiers
    void                        NotifySynchronisationStart();
    void                        NotifySynchronisationEnd( bool success );
    void                        NotifySynchronisationSkip();
    void                        NotifyRequestFinished( bool success );

    bool                        HandleRequests();
    bool                        ShouldSync();
    bool                        ShouldDbSync();
    void                        SaveSyncTime();
    void                        SaveDbSyncTime();

    void                        ForceSendRequests();

    void                        FillLuaFromDb();
    void                        FillLuaFriendsList();

    void                        UserLoggedIn( const char* uniqueId, bool updateStats = true );
    void                        UserLoggedOut();
    void                        ForceEndRequests();
    void                        Update( float dt );

    void                        SendFbUserAvatarRequest();
    bool                        IsFbUserSet();
    void                        SetFbUserId( const char* fbID );
    const char*                 GetFbUserId() { return m_fbId.c_str(); }
    Claw::NarrowString          ConstructGpUserId( const char* orignalId );
    bool                        IsGooglePlusId( const char* socialId );

    void                        SetDebugBackupFriend( const char* weaponId );
    int                         GetAvatarsSize() { return m_avatarsSize; }
    int                         GetTime();

private:
    typedef std::set<Observer*>             Observers;
    typedef Observers::iterator             ObserversIt;

    UserDataManager();

    //handle friend invitations
    void        HandleResponseFriendList( std::vector<Claw::NarrowString> info );
    void        HandleResponseInvitationsList( std::vector<Claw::NarrowString> info );

    //other
    bool        SendRequest( RequestDb* req );

    int         DoRequest();

    static self*                    s_instance;
    Claw::LuaPtr                    m_lua;

    UserDataDbPtr                   m_userDataDb;
    Claw::NarrowString              m_uniqueIdStr;
    Claw::NarrowString              m_fbId;
    std::vector<RequestDb*>         m_requestsList;

    Claw::Thread*                   m_doRequest;

    volatile bool                   m_syncInProgress;
    bool                            m_initialized;

    Observers                       m_observers;
    bool                            m_dbSyncInProgress;

    int                             m_lastFriendsSyncTime;
    int                             m_lastRequestsSyncTime;
    int                             m_lastLeaderboardsFriendsSyncTime;
    int                             m_lastLeaderboardsGlobalSyncTime;
    int                             m_lastGiftsSyncTime;


    volatile float                  m_requestTimer;
    bool                            m_forceCancel;
    int                             m_avatarsSize;

}; // class UserDataManager

#endif // !defined __MONSTAZ_USER_DATA_MANAGER_HPP__
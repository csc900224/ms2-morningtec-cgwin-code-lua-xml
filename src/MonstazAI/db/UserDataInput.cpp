#include "MonstazAI/db/UserDataInput.hpp"
#include "MonstazAI/db/UserDataDb.hpp"
#include "MonstazAI/db/UserDataManager.hpp"
#include "MonstazAI/Application.hpp"

#include <stdio.h>
#include <sstream>

#include "claw/base/Registry.hpp"
#include "claw/base/ClawCRC32.hpp"
#include "claw/base/TextDict.hpp"
#include "claw/base/AssetDict.hpp"

#include "claw/graphics/pixeldata/PixelDataGL.hpp"
#include "claw/graphics/Batcher.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/graphics/Surface.hpp"

#include "claw/graphics/TextUtils.hpp"
#include "guif/Sprite.hpp"
#include "guif/Control.hpp"

#ifdef CLAW_IPHONE
static const char*  FONT_NAME = "Georgia";
static const int    FONT_SIZE = 12;
static const int    FONT_SIZE_SMALL = 10;
#elif defined CLAW_SDL
static const char*  FONT_NAME = "menu2/font_normal.xml@linear";
static const int    FONT_SIZE = 12;
static const int    FONT_SIZE_SMALL = 12;
#else
static const char*  FONT_NAME = "serif";
static const int    FONT_SIZE = 12;
static const int    FONT_SIZE_SMALL = 10;
#endif

static const char* LOGIN_CONTROL = "/LoginPopup/LoginImg";
static const char* LOGIN_PASS_CONTROL = "/LoginPopup/LoginPassImg";
static const char* REGISTER_EMAIL_CONTROL = "/RegisterPopup/EmailImg";
static const char* REGISTER_EMAIL_CONTROL_BTN = "/RegisterPopup/Email/EmailInputButton";
static const char* REGISTER_LOGIN_CONTROL = "/RegisterPopup/LoginImg";
static const char* REGISTER_LOGIN_PASS_CONTROL = "/RegisterPopup/PassImg";

static const char* RECOVERY_EMAIL_CONTROL = "/RecoverPasswordPopup/RecoveryEmail/RecoveryEmailImg";
static const char* RECOVERY_EMAIL_CONTROL_BTN = "/RecoverPasswordPopup/RecoveryEmail/RecoveryEmailInputButton";

static const char* INVITE_ID_CONTROL = "/InvitePopup/FriendIDInput/FriendIDInputImg";
static const char* REGISTER_FB_LOGIN_CONTROL = "/RegisterSocialPopup/Username/UsernameImg" ;
static const char* REGISTER_FB_LOGIN_CONTROL_BTN = "/RegisterSocialPopup/Username/UsernameInputButton" ;

static const char* SOCIAL_EMAIL_CONTROL = "/Social/ResistanceAccount/Email/EmailImg";
static const char* SOCIAL_EMAIL_CONTROL_BTN = "/Social/ResistanceAccount/Email/EmailInputButton";

static const char* SOCIAL_PASS_CONTROL = "/Social/ResistanceAccount/Pass/PassImg";
static const char* SOCIAL_RE_PASS_CONTROL = "/Social/ResistanceAccount/PassVerify/PassVerifyImg";

UserDataInput* UserDataInput::s_instance = NULL;

UserDataInput* UserDataInput::GetInstance()
{
    if( !s_instance )
    {
        s_instance = new UserDataInput;
    }
    return s_instance;
}

void UserDataInput::Release()
{
    if( s_instance )
    {
        delete s_instance;
        s_instance = NULL;
    }
}

UserDataInput::UserDataInput()
{
    m_createImage = false;
    m_fontSize = FONT_SIZE;
    m_fontSmallSize = FONT_SIZE_SMALL;
    m_shouldRefreshInputs = false;
}

void UserDataInput::Initialize()
{
    m_createImage = false;
    float scale = ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale();
    m_fontSize = FONT_SIZE * scale;
    m_fontSmallSize = FONT_SIZE_SMALL* scale;
}

UserDataInput::~UserDataInput()
{
}

void UserDataInput::Update( float dt )
{
}

void UserDataInput::Render( Claw::Surface* target )
{
    if ( m_createImage )
    {
        m_createImage = false;
        //create for logged user
        CreateImageForText( Claw::Registry::Get()->CheckString( "/internal/login"));

        std::vector<UserDataDb::FriendInfo*> friendList = UserDataManager::GetInstance()->GetDb()->GetFriendsList();
        //create for friends item
        if ( friendList.size() > 0 )
        {
            for ( int n = 0; n < friendList.size() ; n++ )
            {
                const char* friendId = friendList.at(n)->name.c_str();
                CreateImageForText( friendId );
            }
        }

        std::vector<UserDataDb::InviteInfo*>  inviteList = UserDataManager::GetInstance()->GetDb()->GetInvitesList();
        //create for friends item
        if ( inviteList.size() > 0 )
        {
            for ( int n = 0; n < inviteList.size() ; n++ )
            {
                const char* friendId = inviteList.at(n)->name.c_str();
                CreateImageForText( friendId );
            }
        }

        std::vector<UserDataDb::GiftData*>  giftList = UserDataManager::GetInstance()->GetDb()->GetGiftDataList();
        //create for friends item
        int giftsNumber = giftList.size();
        if ( giftsNumber > 0 )
        {
            for ( int n = 0; n < giftsNumber ; n++ )
            {
                const char* friendId = giftList.at(n)->name.c_str();
                CreateImageForText( friendId );
            }
        }

        std::vector<UserDataDb::LeaderboardInfo*>  leaderboardList = UserDataManager::GetInstance()->GetDb()->GetLeaderboardList();
        //create for leadeboards
        int itemNumber = leaderboardList.size();
        if ( itemNumber > 0 )
        {
            for ( int n = 0; n < itemNumber ; n++ )
            {
                const char* friendId = leaderboardList.at(n)->name.c_str();
                CreateImageForText( friendId );
            }
        }
         
    }
    else
    {
        const char* username = NULL;
        Claw::Registry::Get()->Get( "/internal/login", username ); 
        if ( username ) CreateImageForText( username );
    }

    Claw::LuaPtr lua = m_lua;

    lua->Call( "CheckLoginInput", 0, 1 );
    Claw::NarrowString loginText = Claw::NarrowString( Claw::TextDict::Get()->GetText( lua->CheckString( 1 ) ) );
    lua->Pop(1);

    if ( loginText != m_loginText || m_shouldRefreshInputs )
    {
        m_loginText = loginText;

        if ( m_loginText.size() > 0 )
        {
            m_loginSurface.Reset( Claw::CreateSurfaceFromText( m_loginText.c_str() , FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ));
            m_loginSurface->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
        }
        else
            m_loginSurface.Release();
    }


    lua->Call( "CheckPassInput", 0, 1 );
    Claw::NarrowString passwordText = Claw::NarrowString( Claw::TextDict::Get()->GetText( lua->CheckString( 1 ) ) );
    lua->Pop(1);

    if ( passwordText != m_passText || m_shouldRefreshInputs )
    {
        m_passText = passwordText;

        if ( passwordText.size() > 0 )
        {
            m_passSurface.Reset( Claw::CreateSurfaceFromText( m_passText.c_str(), FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ) );
            m_passSurface->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
        }
        else
            m_passSurface.Release();
    }

    lua->Call( "CheckEmailInput", 0, 1 );
    Claw::NarrowString emailText = Claw::NarrowString( Claw::TextDict::Get()->GetText( lua->CheckString( 1 ) ) );
    lua->Pop(1);

    if ( emailText != m_emailText || m_shouldRefreshInputs )
    {
        m_emailText = emailText;

        if ( emailText.size() > 0 )
        {
            m_emailSurface.Reset( Claw::CreateSurfaceFromText( m_emailText.c_str() , FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ));
            m_emailSurfaceRecovery.Reset( Claw::CreateSurfaceFromText( m_emailText.c_str() , FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ));
            m_emailSurfaceAccount.Reset( Claw::CreateSurfaceFromText( m_emailText.c_str() , FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ));
            m_emailSurface->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
            m_emailSurfaceRecovery->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
            m_emailSurfaceAccount->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
        }
        else
        {
            m_emailSurface.Release();
            m_emailSurfaceRecovery.Release();
            m_emailSurfaceAccount.Release();
        }
    }

    lua->Call( "CheckInviteInput", 0, 1 );
    Claw::NarrowString inviteText = Claw::NarrowString( Claw::TextDict::Get()->GetText( lua->CheckString( 1 ) ) );
    lua->Pop(1);

    if ( inviteText != m_inviteCodeText || m_shouldRefreshInputs )
    {
        m_inviteCodeText = inviteText;

        if ( inviteText.size() > 0 )
        {
            m_inviteCodeSurface.Reset( Claw::CreateSurfaceFromText( m_inviteCodeText.c_str(), FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ));
            m_inviteCodeSurface->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
        }
        else
            m_inviteCodeSurface.Release();
    }

    lua->Call( "CheckPassVerifyInput", 0, 1 );
    Claw::NarrowString passVerifyText = Claw::NarrowString( Claw::TextDict::Get()->GetText( lua->CheckString( 1 ) ) );
    lua->Pop(1);

    if ( passVerifyText != m_passVerifyText || m_shouldRefreshInputs )
    {
        m_passVerifyText = passVerifyText;

        if ( passVerifyText.size() > 0 )
        {
            m_passVerifySurface.Reset( Claw::CreateSurfaceFromText( passVerifyText.c_str() , FONT_NAME, m_fontSize, Claw::PF_RGBA_8888 ));
            m_passVerifySurface->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
        }
        else
            m_passVerifySurface.Release();
    }

    m_shouldRefreshInputs = false;
    Claw::g_batcher->Commit();
}

void UserDataInput::RenderInputFields( Claw::Surface* target , Guif::Screen* screen )
{
    Guif::Control* controlLogin = screen->FindControl( LOGIN_CONTROL );
    if ( controlLogin ) RenderSurfaceForControl( controlLogin, m_loginSurface );
   
    Guif::Control* controlPassword = screen->FindControl( LOGIN_PASS_CONTROL );
    if ( controlPassword ) RenderSurfaceForControl( controlPassword, m_passSurface );

    Guif::Control* controlEmail = screen->FindControl( REGISTER_EMAIL_CONTROL );
    if ( controlEmail ) RenderSurfaceForControl( controlEmail , m_emailSurface, screen->FindControl( REGISTER_EMAIL_CONTROL_BTN ) );

    Guif::Control* controlRegisterLogin = screen->FindControl( REGISTER_LOGIN_CONTROL );
    if ( controlRegisterLogin ) RenderSurfaceForControl( controlRegisterLogin, m_loginSurface );

    Guif::Control* controlRegisterPass = screen->FindControl( REGISTER_LOGIN_PASS_CONTROL );
    if ( controlRegisterPass ) RenderSurfaceForControl( controlRegisterPass, m_passSurface );

    Guif::Control* controlRecoveryPass = screen->FindControl( RECOVERY_EMAIL_CONTROL );
    if ( controlRecoveryPass ) RenderSurfaceForControl( controlRecoveryPass, m_emailSurfaceRecovery, screen->FindControl( RECOVERY_EMAIL_CONTROL_BTN ));

    Guif::Control* controlInvite = screen->FindControl( INVITE_ID_CONTROL );
    if ( controlInvite ) RenderSurfaceForControl( controlInvite, m_inviteCodeSurface );

    Guif::Control* controlFbUserName = screen->FindControl( REGISTER_FB_LOGIN_CONTROL );
    if ( controlFbUserName ) RenderSurfaceForControl( controlFbUserName, m_loginSurface , screen->FindControl( REGISTER_FB_LOGIN_CONTROL_BTN ) );

    Guif::Control* controlAccountMail = screen->FindControl( SOCIAL_EMAIL_CONTROL );
    if ( controlAccountMail ) RenderSurfaceForControl( controlAccountMail, m_emailSurfaceAccount, screen->FindControl( SOCIAL_EMAIL_CONTROL_BTN ) );

    Guif::Control* controlAccountPass = screen->FindControl( SOCIAL_PASS_CONTROL );
    if ( controlAccountPass ) RenderSurfaceForControl( controlAccountPass, m_passSurface );

    Guif::Control* controlAccountPassVerify = screen->FindControl( SOCIAL_RE_PASS_CONTROL );
    if ( controlAccountPassVerify ) RenderSurfaceForControl( controlAccountPassVerify, m_passVerifySurface );
}

Claw::NarrowString UserDataInput::GetTextImageFilename( const Claw::NarrowString& url )
{
    const Claw::UInt32 crc = Claw::CRC32::WholeBlock( url.c_str(), url.length() );

    char filename[17];
    sprintf( filename, "%s-%X","save/", crc );

    return Claw::NarrowString( filename );
}

void UserDataInput::CreateImageForText( const char* friendId )
{
    if ( GetSurfaceForUser( friendId ) == NULL )
    {
        Claw::SurfacePtr text( Claw::CreateSurfaceFromText( friendId, FONT_NAME, m_fontSmallSize , Claw::PF_RGBA_8888 ));
        text->SetFlag(Claw::Surface::SF_LINEAR_FILTERING ,true);
        if ( text )
        {
            m_userNameMap.insert( std::make_pair( friendId , text ) );
        }
        else
        {
            CLAW_MSG_ASSERT(text,"Create surface from text failed " );
        }
    }
}

void UserDataInput::WriteSurfaceToPng( Claw::SurfacePtr surface , Claw::FilePtr file )
{
    char* data  = new char[surface->GetWidth() * surface->GetHeight() * 4];

    glReadPixels( 0, 0, surface->GetWidth(), surface->GetHeight(), GL_RGBA, GL_UNSIGNED_BYTE, data );

    // init and write png
    png_structp png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    png_infop info_ptr = png_create_info_struct( png_ptr );
    setjmp( png_jmpbuf( png_ptr ) );

    png_set_write_fn( png_ptr, file.GetPtr(), &UserDataInput::PngWrite, NULL );

    png_set_IHDR( png_ptr, info_ptr, surface->GetWidth(), surface->GetHeight(), 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE );

    png_write_info( png_ptr, info_ptr );

    char* ptr = data;
    for( int i=0; i<surface->GetHeight(); i++ )
    {
        png_write_rows( png_ptr, (png_bytepp)(&ptr), 1 );
        ptr += surface->GetWidth()*4;
    }

    png_write_end( png_ptr, info_ptr );
    png_destroy_write_struct( &png_ptr, &info_ptr );
}

void UserDataInput::RenderSurfaceForControl( Guif::Control* control , Claw::Surface* surface , Guif::Control* controlBg )
{
    if ( control )
    {
        Guif::Sprite* sprite = (Guif::Sprite*) control->GetRepresentation()->GetItem();

        if ( sprite )
        {
            if (surface)
            {
                control->SetVisibility( true );
                int startX = 0;
                const int surfaceWidth = surface->GetWidth();
                if ( controlBg )
                {
                   const int width = ((Guif::Sprite*) controlBg->GetRepresentation()->GetItem())->GetBoundingBox().m_w;

                   if ( surfaceWidth - width > 0 )
                   {
                       startX = surfaceWidth - width;
                   }
                }

                surface->SetClipRect( Claw::Rect( startX ,0, surfaceWidth,surface->GetHeight() ) );
                sprite->SetImage( surface );
            }
            else
            {
                control->SetVisibility( false );
            }
        }
    }
}

void UserDataInput::SetSurfaceForControl( Guif::Control* control , Claw::Surface* surface )
{
    if ( control )
    {
        Guif::Sprite* sprite = (Guif::Sprite*) control->GetRepresentation()->GetItem();

        if ( sprite )
        {
            if (surface)
            {
                control->SetVisibility( true );
                sprite->SetImage( surface );
            }
            else
            {
                control->SetVisibility( false );
            }
        }
    }
}

Claw::SurfacePtr UserDataInput::GetSurfaceForUser(  const char* friendUniqueId )
{
    std::map<Claw::NarrowString, Claw::SurfacePtr>::const_iterator it = m_userNameMap.find( friendUniqueId );
    if( it != m_userNameMap.end() )
    {
        return it->second;
    }

    return Claw::SurfacePtr();
}

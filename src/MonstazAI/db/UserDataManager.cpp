#include "MonstazAI/db/UserDataManager.hpp"
#include "MonstazAI/db/UserDataDb.hpp"
#include "MonstazAI/db/UserDataInput.hpp"
#include "MonstazAI/network/facebook/Facebook.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"

#include <stdio.h>
#include <sstream>

#include "claw/base/Registry.hpp"
#include "claw/application/Time.hpp"

#define SYNCHRONIZATION_TIME_PERIOD 11

#define DB_SYNCHRONIZATION_TIME_PERIOD 152
#define TIME_FOR_TIMEOUT 25

#define AVATAR_SIZE 50

UserDataManager* UserDataManager::s_instance = NULL;

namespace UserDataManagerConst
{
    static const char* BOT_NAME = "Wilson";
    static const char* UNKOWN_FB_ID = "Unknown";
    static const char* GP_ID_PREFIX = "gp";
}
UserDataManager* UserDataManager::GetInstance()
{
    if( !s_instance )
    {
        s_instance = new UserDataManager;
    }
    return s_instance;
}

void UserDataManager::Release()
{
    CLAW_MSG( "Release UserDataManager" );
    if( s_instance )
    {
        delete s_instance;
        s_instance = NULL;
    }
}

UserDataManager::UserDataManager()
    : m_doRequest( NULL )
    , m_requestTimer( 0 )
    , m_syncInProgress( false )
    , m_dbSyncInProgress( false )
    , m_lastFriendsSyncTime( 0 )
    , m_lastRequestsSyncTime( 0 )
    , m_lastLeaderboardsFriendsSyncTime( 0 )
    , m_lastLeaderboardsGlobalSyncTime( 0 )
    , m_lastGiftsSyncTime( 0 )
    , m_forceCancel( false )
{
    m_userDataDb.Reset( new UserDataDb() );
    Claw::Registry::Get()->Set("/monstaz/lastSyncTime", 0 );
    Claw::Registry::Get()->Set("/monstaz/lastDbSyncTime", 0 );
    m_fbId = UserDataManagerConst::UNKOWN_FB_ID;
    float scale = ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale();
    m_avatarsSize = AVATAR_SIZE * scale;
}

UserDataManager::~UserDataManager()
{
    ForceCancel();
    if( m_doRequest )
    {
        delete m_doRequest;
        m_doRequest = NULL;
    }
    m_userDataDb.Release();
}

void  UserDataManager::InitUserId( const char* userUniqueID )
{
    m_uniqueIdStr = userUniqueID;
}

bool UserDataManager::IsLogin()
{
    bool isLogin = false;
    Claw::Registry::Get()->Get( "/internal/loggedIn", isLogin );

    return isLogin;
}

UserDataDb* UserDataManager::GetDb() const
{
    return m_userDataDb;
}

bool UserDataManager::IsRequestInProcess() const
{
    return m_syncInProgress;
}

bool UserDataManager::SendLoginRequest( const char* userLogin,const char* userPass )
{
    //login to account
    RequestDb* req = new RequestDb( MSGID_ACCOUNT_LOGIN, MSGID_RETURN_USER_DATA );
    req->refreshRequired = true;
    req->params.push_back(userLogin);
    req->params.push_back(userPass);
    
    return SendRequest( req );
}

bool UserDataManager::SendLoginWithMailRequest( const char* userMail,const char* userPass )
{
    //login to account with mail
    RequestDb* req = new RequestDb( MSGID_ACCOUNT_LOGIN, MSGID_UNIQUE_ID );
    req->params.push_back( userMail );
    req->params.push_back( userPass );

    return SendRequest( req );
}

bool UserDataManager::SendRemindPasswordRequest( const char* userEmail )
{
    RequestDb* req = new RequestDb( MSGID_REMIND_PASSWORD, MSGID_OK );
    req->params.push_back( userEmail );

    return SendRequest( req );
}

bool UserDataManager::SendCreateUserFBRequest( const char* name ,const char* fbId, const char* language, const char* hwKey )
{
    // create account
    RequestDb* req = new RequestDb( MSGID_CREATE_ACCOUNT_WITH_FB,MSGID_UNIQUE_ID );
    req->params.push_back( name );
    req->params.push_back( fbId );
    req->params.push_back( language );
    req->params.push_back( hwKey );

    return SendRequest( req );
}

bool UserDataManager::SendLoginWithFBRequest( const char* userLogin, const char* fbId )
{
    //login to account with facebook id
    RequestDb* req = new RequestDb( MSGID_ACCOUNT_LOGIN_FB, MSGID_UNIQUE_ID );
    req->params.push_back( userLogin );
    req->params.push_back( fbId );

    return SendRequest( req );
}

bool UserDataManager::SendCreateUserRequest( const char* name ,const char* mail,const char* pass, const char* language, const char* hwKey )
{
    // create account
    RequestDb* req = new RequestDb( MSGID_CREATE_ACCOUNT_WITH_EMAIL,MSGID_UNIQUE_ID );
    req->params.push_back( name );
    req->params.push_back( mail );
    req->params.push_back( pass );
    req->params.push_back( language );
    req->params.push_back( hwKey );
    return SendRequest( req );;
}

bool UserDataManager::SendGetVersionRequest()
{
    // get version
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_GET_VERSION, MSGID_RETURN_VERSION );
    req->params.push_back( m_uniqueIdStr.c_str() );

    return SendRequest( req );
}

bool UserDataManager::SendSetWeaponRequest(const char* weaponName , int weaponLevel )
{
    if ( m_lua )
    {
        //locally keep best weapon 
        Claw::NarrowString weaponId = "smg";
        Claw::NarrowString weaponUpgradeLevel = "0";

        m_lua->Call( "ItemDbGetBestOwnedWeaponId", 0, 2 );
        weaponId = m_lua->CheckString( -2 );
        weaponUpgradeLevel = m_lua->CheckString( -1 );
        m_lua->Pop(2);

        Claw::Registry::Get()->Set( "/monstaz/player/bestweapon", weaponId.c_str() );
        Claw::Registry::Get()->Set( "/monstaz/player/bestweaponlevel", weaponUpgradeLevel.c_str() );
    }
    // set weapon usage
    if ( !IsLogin() ) return false;

    //user_unique_id, weapon_name, weapon_level
    RequestDb* req = new RequestDb ( MSGID_SET_WEAPON , MSGID_OK );

    req->params.push_back( m_uniqueIdStr.c_str() );
    req->params.push_back( weaponName );

    std::ostringstream ss;
    ss << weaponLevel;
    req->params.push_back( ss.str() );

    return SendRequest( req );
}

bool UserDataManager::SendSetUserProgressRequest( const char* enemies_defeated, const char* completed_events)
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb(  MSGID_SET_USER_PROGRESS , MSGID_OK );

    req->params.push_back( m_uniqueIdStr.c_str() );
    req->params.push_back( enemies_defeated );
    req->params.push_back( completed_events );

    return SendRequest( req );
}

bool UserDataManager::SendSetUserLevelRequest( const char* userLevel )
{
    // set user level
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_SET_USER_LEVEL , MSGID_OK );
    req->params.push_back( m_uniqueIdStr.c_str() );
    req->params.push_back( userLevel );

    return SendRequest( req );
}

bool UserDataManager::SendGetFriendsGiftsRequest()
{
    // get friends gifts
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_FRIENDS_GIFTS , MSGID_RETURN_FRIENDS_GIFTS );
    req->params.push_back( m_uniqueIdStr );

    return SendRequest( req );
}

bool UserDataManager::SendSetGiftAccepted( const char* userItemId )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_USER_GIFT_ACCEPTED, MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( userItemId );

    return SendRequest( req );
}

bool UserDataManager::SendGetFriendInfoRequest( const char* friendUniqueId )
{
    // get friend info
    if ( !IsLogin() ) return false;

    if ( IsBot( friendUniqueId ) )
    {
        UserDataDb::FriendInfo* ffInfo = GetDb()->GetUserInfoByUniqueId( friendUniqueId );

        int value = 0;
        Claw::Registry::Get()->Get( "/monstaz/aifriend/sendGift" , value );
        if ( value > 0 )
        {
            int now = GetTime();

            value = 86400 + value - now; //time of send + 24h - time now

            if ( value < 0 )
            {
                value = 0;
            }
        }

        if ( ffInfo )
        {
            Claw::NarrowString weaponId = "smg";
            Claw::NarrowString weaponUpgradeLevel = "0";

            m_lua->Call( "ItemDbGetBestOwnedWeaponId", 0, 2 );
            weaponId = m_lua->CheckString( -2 );
            weaponUpgradeLevel = m_lua->CheckString( -1 );
            m_lua->Pop(2);


            //ffInfo->kills = atoi( content[3].c_str() );
            // ffInfo->eventsCompleted = atoi( content[4].c_str() );

            std::ostringstream ss;
            ss << value;
            ffInfo->canSendGift =  ss.str();
            //ffInfo->canHelp = content[6];
            ffInfo->bestWeaponId = weaponId;
            ffInfo->bestWeaponUpgrade = weaponUpgradeLevel;

            GetDb()->UpdateFriendInfoList ( ffInfo );

            m_lua->PushString( ffInfo->uniqueId );
            m_lua->PushNumber( ffInfo->kills );
            m_lua->PushNumber( ffInfo->eventsCompleted );
            m_lua->PushString( ffInfo->bestWeaponId );
            m_lua->PushString( ffInfo->bestWeaponUpgrade );
            m_lua->PushString( ffInfo->canSendGift );
            m_lua->PushString( ffInfo->canHelp );
            m_lua->Call( "FillFriendInfo", 7, 0 );
            m_lua->Call( "GetInfoFailed", 0, 0 );
            return true;

        }
    }

    RequestDb* req = new RequestDb( MSGID_GET_FRIEND_INFO_SHORT , MSGID_RETURN_FRIEND_INFO_SHORT );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueId );

    return SendRequest( req );
}

bool UserDataManager::SendGetFriendRequestsListRequest()
{
    // get friends list 
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_FRIEND_REQUESTS , MSGID_RETURN_FRIENDS );
    req->params.push_back( m_uniqueIdStr );

    return SendRequest( req );
}

bool UserDataManager::SendFriendRequestResponse( const char* friendUniqueId, bool accepted )
{
    // get friends list 
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_SET_FRIEND_REQUEST_RESPONSE, MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueId );

    if ( accepted ) 
        req->params.push_back( "1" ); //accepted
    else
        req->params.push_back( "2"); //rejected

    return SendRequest( req );
}

bool UserDataManager::SendGetFriendsListRequest()
{
    // get friends list 
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_FRIENDS , MSGID_RETURN_FRIENDS_ALL );
    req->params.push_back( m_uniqueIdStr );
    return SendRequest(req);
}

bool UserDataManager::SendAddFriendRequest( const char* friendUniqueID )
{
    // add a friend request
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_ADD_FRIEND , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueID );

    return SendRequest( req );
}

bool UserDataManager::SendGetItemsCategoriesRequest()
{
    // get items categories
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_ITEM_CATEGORIES , MSGID_RETURN_ITEM_CATEGORIES );
    req->params.push_back( m_uniqueIdStr );
    return SendRequest( req );
}

bool UserDataManager::SendChangeEmailRequest( const char* newEmail )
{
    //get items and prices
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_CHANGE_EMAIL , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( newEmail );

    return SendRequest( req );
}

bool UserDataManager::SendGetDailyQuestRequest()
{
    // daily quest
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_DAILY_QUESTS , MSGID_RETURN_DAILY_QUESTS );
    req->params.push_back( m_uniqueIdStr );

    return SendRequest( req );
}

bool UserDataManager::SendSendGiftRequest( const char* itemId, const char* friendNickname, const char* quantity )
{
    // send gift to a friend // user_unique_id | item_id | friend username | quantity
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_SEND_FRIEND_GIFT , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( itemId );
    req->params.push_back( friendNickname );
    req->params.push_back( quantity );
    //req->params.push_back("0" ); // do not reset counter with this gift
    req->params.push_back( "1" ); // reset counter with this gift

    return SendRequest( req );
}

bool UserDataManager::SendSendRewardRequest( const char* itemId, const char* friendNickname, const char* quantity )
{
    // send gift to a friend // user_unique_id | item_id | friend username | quantity
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_SEND_FRIEND_GIFT , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( itemId );
    req->params.push_back( friendNickname );
    req->params.push_back( quantity );
    req->params.push_back("0" ); // do not reset counter with this gift
    //req->params.push_back( "1" ); // reset counter with this gift

    return SendRequest( req );
}

bool UserDataManager::SendSetUserFBidRequest( const char* facebookId )
{
    // set user fb _id
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_SET_FB_ID , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( facebookId );

    return SendRequest( req );
}

bool UserDataManager::SendGetHasFbIdAccountRequest( const char* facebookId )
{
    if (m_lua) m_lua->Call("SyncIndicatorShow",0,0);

    RequestDb* req = new RequestDb ( MSGID_GET_HAS_FB_ACCOUNT , MSGID_RETURN_USER_DATA );
    req->params.push_back( facebookId );

    return SendRequest( req );
}

bool UserDataManager::SendSetScoreSurvivalRequest( int levelId,int  levelScore,int levelTime )
{
    // set survival score - // user_unique_id, level_id, level score, level_time
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_SET_SURVIVAL_SCORE , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    
    std::ostringstream ss;
    ss << levelId;
    req->params.push_back( ss.str() );

    std::ostringstream sLevelScore;
    sLevelScore << levelScore;
    req->params.push_back( sLevelScore.str() );


    std::ostringstream sLevelTime;
    sLevelTime << levelTime;
    req->params.push_back( sLevelTime.str() );

    return SendRequest( req );
}

bool UserDataManager::SendSetScoreSurvivalRequest( const char* levelId,const char*  levelScore,const char* levelTime )
{
    // set survival score - // user_unique_id, level_id, level score, level_time
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_SET_SURVIVAL_SCORE , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( levelId );
    req->params.push_back( levelScore );
    req->params.push_back( levelTime );

    return SendRequest( req );
}

bool UserDataManager::SendChangePasswordRequest( const char* newPass )
{
    // change account password
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb ( MSGID_PASSWORD_CHANGE , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( newPass );

    return SendRequest( req );
}

bool UserDataManager::SendGetSurvivalScoresRequest()
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_SURVIVAL_SCORES , MSGID_RETURN_SURVIVAL_SCORES );
    req->params.push_back( m_uniqueIdStr );
    return SendRequest( req );
}

bool UserDataManager::SendGetSurvivalScoresFriendsRequest()
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_SURVIVAL_SCORES_FRIENDS , MSGID_RETURN_SURVIVAL_SCORES );
    req->params.push_back( m_uniqueIdStr );
    return SendRequest( req );
}

bool UserDataManager::SendSetFriendHelpRequest( const char* friendUniqueId )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_SET_FRIEND_HELP, MSGID_OK );
    req->refreshRequired = true;
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueId );

    return SendRequest( req );
}

bool UserDataManager::SendGetFriendHelpRequest( const char* friendUniqueId )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_FRIEND_HELP, MSGID_OK );

    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueId );

    return SendRequest( req );
}

bool UserDataManager::SendResetFriendHelpRequest( const char* friendUniqueId )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_RESET_FRIEND_HELP, MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueId );

   return SendRequest( req );
}

bool UserDataManager::SendGetUserParams()
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_USER_PARAMS, MSGID_RETURN_USER_PARAMS );
    req->params.push_back( m_uniqueIdStr );
    return SendRequest( req );
}

bool UserDataManager::SendGetUserBestWeapon()
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_GET_USER_BEST_WEAPON, MSGID_RETURN_USER_BEST_WEAPON );
    req->params.push_back( m_uniqueIdStr );
    return SendRequest( req );
}

bool UserDataManager::SendSetUserParams( const char* userLevel, const char* kills, const char* events )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_SET_USER_PARAMS, MSGID_OK );

    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( userLevel );
    req->params.push_back( kills );
    req->params.push_back( events );

    return SendRequest( req );
}

bool UserDataManager::SendSetUserParams( int userLevel, int kills, int events )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_SET_USER_PARAMS, MSGID_OK );

    req->params.push_back( m_uniqueIdStr );

    std::ostringstream ss;
    ss << userLevel;
    req->params.push_back( ss.str() );

    std::ostringstream sKills;
    sKills << kills;

    req->params.push_back( sKills.str() );

    std::ostringstream sEvents;
    sEvents << events;
    req->params.push_back( sEvents.str() );

    return SendRequest( req );
}

bool UserDataManager::SendSetEmailPasswordChange(const char* newEmail, const char* newPassword)
{
    // changing email and password in one request
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_EMAIL_PASSWORD_CHANGE, MSGID_OK );

    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( newEmail );
    req->params.push_back( newPassword );

    return SendRequest( req );
}

bool UserDataManager::SendSetApproveNewFriends()
{
    if ( !IsLogin() ) return false;

    GetDb()->SetNewFriendsCount( 0 );

    RequestDb* req = new RequestDb ( MSGID_SET_NEW_FRIENDS_APPROVE, MSGID_OK );
    req->params.push_back( m_uniqueIdStr );

    return SendRequest( req );
}

void UserDataManager::RecieveMessage( RequestDb* reqDb, int msgReceived )
{
    reqDb->resultID = msgReceived;
    reqDb->recieved = true;
}

void UserDataManager::HandleMessage( RequestDb* reqDb, bool clearMessages)
{

    if ( reqDb->resultID == -1 && clearMessages ) return;

    DatabaseConnectionPtr databaseConn = reqDb->databaseConn;
    std::vector<Claw::NarrowString> content(0);

    content = databaseConn->getContent( 0 );

    int currentRequestID = reqDb->requestID;

    int currentResultID = reqDb->resultID;
    int expectedtResultID = reqDb->answerID;

    bool success = currentResultID == expectedtResultID;

    switch ( currentRequestID )
    {
    case MSGID_MULTIPART_MESSAGE:
        {
            std::vector<Claw::NarrowString> received;

            int messageCount = reqDb->databaseConn->getMessagesCount();
             CLAW_MSG("Multipart response" << messageCount );
            for (int n=0; n < messageCount; ++n)
            {
                MessageData msg = reqDb->databaseConn->getMessage(0);
                RequestDb* tempReq = new RequestDb ( msg.sent, msg.expected );
                tempReq->databaseConn = reqDb->databaseConn;
                tempReq->resultID = msg.received;
                tempReq->recieved = true;
                tempReq->params = reqDb->subRequests.at(n)->params;
                HandleMessage( tempReq, false);
                reqDb->databaseConn->delFirstMessage();
            }
        }
        break;
    case MSGID_USER_GIFT_ACCEPTED_ALL:
        {
            if ( success )
            {
                m_lua->Call( "ConfirmGiftsActionSucces", 0 , 0 );
            }
            else
            {
                m_lua->Call( "ConfirmGiftsActionFailed", 0 , 0 );
            }
        }
        break;
    case MSGID_RESET_FRIEND_HELP:
        {
            if ( success )
            {
                m_lua->PushString( reqDb->params.at(1) );
                m_lua->Call( "SpeedUpResetSuccess", 1 , 0 );
            }
            else
            {
                m_lua->Call( "SpeedUpResetFailed", 0 , 0 );
            }
        }
        break;
    case MSGID_DELETE_FRIEND:
        {
            if ( success )
            {
                GetDb()->RemoveFriend( reqDb->params[1].c_str() );
                FillLuaFriendsList();
                m_lua->Call( "FriendRemoveSucces", 0, 0 );
            }
            else
            {
                m_lua->Call( "FriendRemoveFailed", 0, 0 );
            }

        }
        break;
    case MSGID_GET_HAS_FB_ACCOUNT:
        {
            if ( success )
            {
                const char* userUniqueID = content.at( 0 ).c_str();
                UserLoggedIn( userUniqueID );

                //CLAW_MSG("Unique id received  :"<< userUniqueID);
                //CLAW_MSG("user name :" << content[1]);
                //CLAW_MSG("user email :" << content[2]);
                //CLAW_MSG("user fb_id :" << content[3]);

                m_lua->PushString( content[0] );
                m_lua->PushString( content[1] );
                m_lua->PushString( content[2] );
                m_lua->PushString( content[3] );
                m_lua->Call( "UserLoggedIn", 4, 0 );
                UserDataInput::GetInstance()->PrepareImages();
                m_lua->Call( "ResetUpdateDBTimer", 0 , 0 );
            }
            else
            {
                if ( currentResultID == MSGID_ERROR_NO_SUCH_USER )
                {
                    CLAW_MSG(" social id of newly created user " << reqDb->params.at(0)); 
                    m_lua->PushString( reqDb->params.at(0) );
                    m_lua->PushBool( IsGooglePlusId( reqDb->params.at(0).c_str()) );
                    m_lua->Call( "RegisterSocialPopupShow",2, 0 );
                }
                else
                {
                     m_lua->Call( "UserLoginFailedNoNetwork", 0, 0 );
                }
            }
            
        }
        break;
    case MSGID_USER_GIFT_ACCEPTED:
        {   
            if ( success )
            {
                const char* giftUniqueId = reqDb->params.at(1).c_str();
                GetDb()->SetGiftAsUsed( giftUniqueId );
                m_lua->Call( "GiftResponseSuccess", 0, 0 );
            }
            else
            {
                m_lua->Call( "GiftResponseFailed", 0, 0 );
            }
        }
        break;
     case MSGID_SET_FRIEND_REQUEST_RESPONSE:
        {   
            if ( success )
            {
                const char* friendUniqueId = reqDb->params.at(1).c_str();
                bool accepted = reqDb->params.at(2) == "1";
                GetDb()->SetInviteAsCompleted( friendUniqueId , accepted );
                if ( accepted )
                {
                    FillLuaFriendsList();
                }
                m_lua->Call( "FriendResponseSuccess", 0, 0 );
            }
            else
            {
                m_lua->Call( "FriendResponseFailed", 0, 0 );
            }
        }
        break;
    case MSGID_GET_SURVIVAL_SCORES_FRIENDS:
        {
            if ( success )
            {
                CLAW_MSG("Has survival friends results");
                int index = 0;
                int itemsCount = 0;
                bool finished = false;
                do {
                    int levelID = atoi( content[index++].c_str() );
                    if ( levelID != 0 )
                    {
                        m_lua->PushNumber( levelID );
                        m_lua->Call( "ResetStatsFriends", 1, 0 );

                        itemsCount = atoi( content[index++].c_str() );
                       // CLAW_MSG("level:"<< levelID << "num of results:" << itemsCount );

                        for (int n= index; n< index + itemsCount*5; n=n+5)
                        {
                            //CLAW_MSG("Place:" << content[n] << " name:"<< content[n+1] << " points:" << content[n+2] 
                            //<< " date:"<< content[n+3] << " level time:"<< content[n+4]);

                            m_lua->PushNumber( levelID );
                            m_lua->PushString( content[n] );
                            m_lua->PushString( content[n+1] );
                            m_lua->PushString( content[n+2] );
                            m_lua->Call( "AddFriendsStatsForLevel", 4, 0 );
                            //AddGlobalStatsForLevel(  level Id , place, "Somebody" ,  67544565656 )
                        }
                    }
                    index += itemsCount*5;
                    if (index >= content.size()) finished = true;
                } 
                while (!finished);

                int now = GetTime();
                m_lua->PushNumber( now );
                m_lua->Call( "SetLastSyncTimeLeaderboardsFriends", 1, 0 );
            }
            else
            {
                m_lua->PushNumber( -1 );
                m_lua->Call( "SetLastSyncTimeLeaderboardsFriends", 1, 0 );
            }

            UserDataInput::GetInstance()->PrepareImages();
        }
        break;
    case MSGID_GET_SURVIVAL_SCORES:
        {
            if ( success )
            {
                //CLAW_MSG("Has survival results");
                int index = 0;
                int itemsCount = 0;
                bool finished = false;
                GetDb()->FlushLeaderboard();
                do {
                    int levelID = atoi( content[index++].c_str() );

                    if ( levelID != 0 )
                    {
                        m_lua->PushNumber( levelID );
                        m_lua->Call( "ResetStatsGlobal", 1, 0 );


                        itemsCount = atoi( content[index++].c_str() );
                        //CLAW_MSG("level:"<< levelID << "num of results:" << itemsCount );

                        for (int n= index; n< index + itemsCount*5; n=n+5)
                        {
                           // CLAW_MSG("Place:" << content[n] << " name:"<< content[n+1] << " points:" << content[n+2] 
                           // << " date:"<< content[n+3] << " level time:"<< content[n+4]);

                            m_lua->PushNumber( levelID );
                            m_lua->PushString( content[n] );
                            m_lua->PushString( content[n+1] );
                            m_lua->PushString( content[n+2] );
                            m_lua->Call( "AddGlobalStatsForLevel", 4, 0 );

                            GetDb()->AddLeaderboardInfo( new UserDataDb::LeaderboardInfo( content[n], content[n+1],content[n+2], true ) );
                            //AddGlobalStatsForLevel(  level Id , place, "Somebody" ,  67544565656 )
                        }
                    }
                    index += itemsCount*5;
                    if ( index >= content.size() )  finished = true;
                } 
                while (!finished);

                UserDataInput::GetInstance()->PrepareImages();
                int now = GetTime();
                m_lua->PushNumber( now );
                m_lua->Call( "SetLastSyncTimeLeaderboardsGlobal", 1, 0 );
            }
            else
            {
                m_lua->PushNumber( -1 );
                m_lua->Call( "SetLastSyncTimeLeaderboardsGlobal", 1, 0 );
            }
        }
        break;
    case MSGID_SET_NEW_FRIENDS_APPROVE:
        {
            if ( success )
            {
                m_lua->Call( "RewardAccepted", 0, 0 );
            }
            else
            {
                m_lua->Call( "RewardAcceptFail", 0, 0 );
            }
        } 
        break;

    case MSGID_SET_FRIEND_HELP:
        {
            //start level even if no network error occured
          //  m_lua->Call( "StartLevelSelected", 0, 0 );
        } 
        break;
    case MSGID_GET_FRIENDS:
        {
            if ( success )
            {
                HandleResponseFriendList( databaseConn->getContent( 0 ) );

                int now = GetTime();
                m_lua->PushNumber( now );
                m_lua->Call( "SetLastSyncTimeFriends", 1, 0 );
            }
            else
            {
                m_lua->PushNumber( -1 );
                m_lua->Call( "SetLastSyncTimeFriends", 1, 0 );
                //TODO: error handling if needed
            }
        } 
        break;
    case MSGID_GET_FRIEND_REQUESTS:
        {
            if ( currentResultID == expectedtResultID || currentResultID == MSGID_OK )
            {
                HandleResponseInvitationsList( databaseConn->getContent( 0 ) );
                int now = GetTime();
                m_lua->PushNumber( now );
                m_lua->Call( "SetLastSyncTimeRequests", 1, 0 );
            }
            else
            {
                //TODO: error handling if needed
                CLAW_MSG("invitation get list failed:"); 
                m_lua->PushNumber( -1 );
                m_lua->Call( "SetLastSyncTimeRequests", 1, 0 );
            }
        }
        break;
    case MSGID_ACCOUNT_LOGIN:
        {
            if ( success )
            {
                //Login succes
                const char* userUniqueID = content.at( 0 ).c_str();
                UserLoggedIn( userUniqueID );

                //CLAW_MSG("Unique id received  :"<< userUniqueID);
                //CLAW_MSG("user name :" << content[1]);
                //CLAW_MSG("user email :" << content[2]);
                //CLAW_MSG("user fb_id :" << content[3]);

                m_lua->PushString( content[0] );
                m_lua->PushString( content[1] );
                m_lua->PushString( content[2] );
                m_lua->PushString( content[3] );
                m_lua->Call( "UserLoggedIn", 4, 0 );
                UserDataInput::GetInstance()->PrepareImages();
                m_lua->Call( "ResetUpdateDBTimer", 0 , 0 );
            }
            else
            {
                 if ( currentResultID == MSGID_ERROR_NO_DATABASE_CONNECTION )
                     m_lua->Call( "UserLoginFailedNoNetwork", 0, 0 );
                 else
                    m_lua->Call( "UserLoginFailed", 0, 0 );
            }
        }
        break;
    case MSGID_SEND_FRIEND_GIFT:
        {
            if ( reqDb->params.at(4) == "1" ) //gift = "1" , reward = "0" ( no confirmation )
            {
                if ( success )
                {
                    m_lua->PushString( reqDb->params.at(2) );
                    m_lua->Call( "SendGiftSucces", 1, 0 );
                    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FRIEND_GIFT_SENT );
                }
                else
                {
                    m_lua->PushBool( currentResultID == MSGID_ERROR_GIFT_ALREADY_GIVEN );
                    m_lua->Call( "SendGiftFailed", 1, 0 );
                }
            }
        }
        break;
    case MSGID_GET_FRIENDS_GIFTS:
        {
            if ( success )
            {
                Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
                Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
                CLAW_MSG("Gifts from friends - items count:"<< content[0]);
                const int itemsCount =  atoi( content[0].c_str() );

                GetDb()->FlushGiftDataList();

                for ( int n=1; n < itemsCount; n+=9 )
                {
                    //CLAW_MSG("Item given by:" << content[n] << " name :" << content[n+1] << "fb:" << content[n+2] << "level:" << content[n+3] << " item_id:"<< content[n+4] << " qty:" <<content[n+5]  << " uset_to_item_id:"<<content[n+6] << "age" << content[n+7] << "vip_status" << content[n+8]);
                    GetDb()->AddGiftData(new UserDataDb::GiftData( content[n] , content[n+1] , content[n+2], content[n+3] ,content[n+4] ,content[n+5],content[n+6], content[n+7], false , content[n+8] )); 

                    if ( content[n+2] != "" )
                    {
                        Claw::NarrowString filename = UserDataInput::GetInstance()->GetTextImageFilename( content[n+2].c_str() );
                        Claw::FilePtr file( Claw::OpenFile( filename ) );

                        if ( !file )
                        { 
                            if ( IsGooglePlusId( content[n+2].c_str() ) )
                            {
                                Claw::NarrowString socialGpId( content[n+2].substr(2, content[n+2].size()) ); 
                                gs->GetAvatar( socialGpId.c_str() , GetAvatarsSize() );
                            }
                            else
                            {
                                fb->GetAvatar( content[n+2].c_str() );
                            }
                        }
                    }
                }

                if( GetDb()->GetGiftDataList().size() > 0 )
                {
                    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FRIEND_GIFT_RECIVED, GetDb()->GetGiftDataList().size() );
                }

                UserDataInput::GetInstance()->PrepareImages();
                int now = GetTime();
                m_lua->PushNumber( now );
                m_lua->Call( "SetLastSyncTimeGifts", 1, 0 );
            }
            else
            {
                m_lua->PushNumber( -1 );
                m_lua->Call( "SetLastSyncTimeGifts", 1, 0 );
                //TODO: error handling if needed
            }
        }
        break;
    case MSGID_GET_FRIEND_INFO_FULL:
        {
            if ( success )
            {
                //CLAW_MSG("has friend info - items count:"<< content[0]);

                int itemsCount = atoi( content[0].c_str() );

                //CLAW_MSG("user_name:" <<content[1] <<" USer level :"<<content[2] <<" USer fb_id :"<< content[3] << " USer best weapon :"<< content[4] <<" USer best weapon level :"<<content[5] << " USer defeated enemies:"<<content[6] <<" USer completed events:"<<content[7]);

                Claw::NarrowString name     = content[1];
                Claw::NarrowString weaponId = content[4];
                Claw::NarrowString fbId     = content[3];
                int level                   = atoi( content[2].c_str() );
                int kills                   = atoi( content[6].c_str() );
                int completedEvents         = atoi( content[7].c_str() );

                UserDataDb::FriendInfo* ffInfo = GetDb()->GetUserInfo( name.c_str() );

                if ( ffInfo )
                {
                    ffInfo->bestWeaponId  = weaponId;
                    ffInfo->kills = kills;
                    ffInfo->eventsCompleted = completedEvents;
                    GetDb()->UpdateFriendInfoList ( ffInfo );

                    m_lua->PushString( ffInfo->uniqueId );
                    m_lua->PushNumber( ffInfo->kills );
                    m_lua->PushNumber( ffInfo->eventsCompleted );
                    m_lua->PushString( ffInfo->bestWeaponId );
                    m_lua->Call( "FillFriendInfo", 4, 0 );
                }
                m_lua->PushString( ffInfo->uniqueId );
                m_lua->Call( "GetInfoSucces",1, 0 );
            }
            else
            {
                //CLAW_MSG("has friend info failed");
                m_lua->Call( "GetInfoFailed", 0, 0 );
            }
        }
        break;
    case MSGID_GET_FRIEND_INFO_SHORT:
        {
            if ( success )
            {
                //CLAW_MSG("has friend info SHORT");


                //CLAW_MSG("user unique id:"<<content[0]);
                //CLAW_MSG("USer best weapon :"<<content[1]);
                //CLAW_MSG("USer best weapon level :"<<content[2]);
                //CLAW_MSG("USer defeated enemies:"<<content[3]);
                //CLAW_MSG("USer completed events:"<<content[4]);
                //CLAW_MSG("USer can send a gifts:"<<content[5]);
                //CLAW_MSG("USer can help:"<<content[6]);


                UserDataDb::FriendInfo* ffInfo = GetDb()->GetUserInfoByUniqueId( content[0].c_str() );

                if ( ffInfo )
                {
                    ffInfo->bestWeaponId  = content[1];
                    ffInfo->bestWeaponUpgrade = content[2];
                    ffInfo->kills = atoi( content[3].c_str() );
                    ffInfo->eventsCompleted = atoi( content[4].c_str() );
                    ffInfo->canSendGift = content[5];
                    ffInfo->canHelp = content[6];

                    GetDb()->UpdateFriendInfoList ( ffInfo );

                    m_lua->PushString( ffInfo->uniqueId );
                    m_lua->PushNumber( ffInfo->kills );
                    m_lua->PushNumber( ffInfo->eventsCompleted );
                    m_lua->PushString( ffInfo->bestWeaponId );
                    m_lua->PushString( ffInfo->bestWeaponUpgrade );
                    m_lua->PushString( ffInfo->canSendGift );
                    m_lua->PushString( ffInfo->canHelp );
                    m_lua->Call( "FillFriendInfo", 7, 0 );
                }

                m_lua->PushString( ffInfo->uniqueId );
                m_lua->Call( "GetInfoSucces",1, 0 );
            }
            else 
            {
                //CLAW_MSG("has friend info failed");
                m_lua->Call( "GetInfoFailed", 0, 0 );
            }
        }
        break;
    case MSGID_EMAIL_PASSWORD_CHANGE:
    case MSGID_PASSWORD_CHANGE:
    case MSGID_CHANGE_EMAIL:
        {
            if ( success )
            {
                if ( reqDb->requestID == MSGID_CHANGE_EMAIL )
                {
                    Claw::Registry::Get()->Set( "/internal/mail",  reqDb->params.at(1).c_str() );
                }
                else if( reqDb->requestID == MSGID_PASSWORD_CHANGE )
                {
                    Claw::Registry::Get()->Set( "/internal/pass",  reqDb->params.at(1).c_str() );
                }
                else
                {
                    Claw::Registry::Get()->Set( "/internal/mail",  reqDb->params.at(1).c_str() );
                    Claw::Registry::Get()->Set( "/internal/pass",  reqDb->params.at(2).c_str() );
                }

                m_lua->Call( "AccountDataSaveSucces",0, 0 );
            }
            else
            {
                if ( currentResultID == MSGID_ERROR_NO_DATABASE_CONNECTION )
                {
                    m_lua->Call( "AccountDataSaveFailedNoNetwork",0, 0 );
                }
                else
                {
                    m_lua->Call( "AccountDataSaveFailed",0, 0 );
                }
            }
        } 
        break;
    case MSGID_REMIND_PASSWORD:
        {
            if ( success )
            {
                m_lua->Call( "RecoveryPasswordMailSendSuccess",0, 0 );
            }
            else
            {
                if ( currentResultID == MSGID_ERROR_NO_DATABASE_CONNECTION )
                {
                    m_lua->Call( "RecoveryPasswordMailSendFailedNoNetwork",0, 0 );
                }
                else
                {
                    m_lua->Call( "RecoveryPasswordMailSendFailed",0, 0 );
                }
            }
        } 
        break;
    case MSGID_CREATE_ACCOUNT_WITH_EMAIL:
    case MSGID_CREATE_ACCOUNT_WITH_FB:
        {
            if ( success )
            {
                const char* userUniqueID = content[0].c_str();
                UserLoggedIn( userUniqueID , false );
                

                if ( currentRequestID == MSGID_CREATE_ACCOUNT_WITH_EMAIL )
                {
                    m_lua->PushString( content[0] );
                    m_lua->PushString( reqDb->params.at(0) );
                    m_lua->PushString( reqDb->params.at(1) );
                    m_lua->PushString( "" );
                }
                else
                {
                    m_lua->PushString( content[0] );
                    m_lua->PushString( reqDb->params.at(0) );
                    m_lua->PushString( "" );
                    m_lua->PushString( reqDb->params.at(1) );
                }

                m_lua->Call( "UserDataSetup", 4, 0 );
                UserDataInput::GetInstance()->PrepareImages();
                m_lua->Call( "ResetUpdateDBTimer", 0 , 0 );
                m_lua->Call( "AccountCreateSuccess",0, 0 );
            }
            else
            {
                if ( currentResultID == MSGID_ERROR_NO_DATABASE_CONNECTION )
                {
                    m_lua->Call( "AccountCreateFailedNoNetwork",0, 0 );
                }
                else if( currentResultID == MSGID_ERROR_BAD_WORDS )
                {
                    bool viaSocial = currentRequestID == MSGID_CREATE_ACCOUNT_WITH_FB;

                    const char* socialId = reqDb->params.at(1).c_str();
                    bool viaGooglePlus = IsGooglePlusId( socialId );
                    m_lua->PushBool( viaSocial );
                    m_lua->PushString( socialId );
                    m_lua->PushBool( viaGooglePlus );
                    m_lua->Call( "AccountCreateFailedBadWords", 3, 0 );
                }
                else
                {
                    bool isNameAlreadyUsed = currentResultID == MSGID_ERROR_NICK_ALREADY_EXISTS;
                    bool viaSocial = currentRequestID == MSGID_CREATE_ACCOUNT_WITH_FB;

                    const char* socialId = reqDb->params.at(1).c_str();
                    bool viaGooglePlus = IsGooglePlusId( socialId );
                    m_lua->PushBool( isNameAlreadyUsed );
                    m_lua->PushBool( viaSocial );
                    m_lua->PushString( socialId );
                    m_lua->PushBool( viaGooglePlus );
                    m_lua->Call( "AccountCreateFailed", 4, 0 );
                }
            }
        } 
        break;
    case MSGID_ADD_FRIEND:
        {
            if ( success )
            {
                m_lua->Call( "InviteSuccess",0, 0 );
                GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FRIEND_INVITED );
            }
            else
            {
                if ( currentResultID == MSGID_ERROR_NO_DATABASE_CONNECTION )
                {
                    m_lua->Call( "InviteFailNoNetwork",0, 0 );
                }
                else
                {
                    bool noUser = currentResultID == MSGID_ERROR_NO_SUCH_USER;
                    bool alreadyFriend = currentResultID == MSGID_ERROR_ALREADY_FRIEND;
                    bool alreadySendedInvite = currentResultID == MSGID_ERROR_WAITING_FOR_FRIEND_RESPONSE;
                    if ( noUser )
                    {
                        m_lua->Call( "InviteFail", 0 , 0 );
                    }

                    if ( alreadyFriend )
                    {
                        m_lua->Call( "InviteFailAlreadyFriend", 0 , 0 );
                    }

                    if ( alreadySendedInvite )
                    {
                        m_lua->Call( "InviteFailAlreadySend", 0 , 0 );
                    }
                }
            }
        } 
        break;
    }

    content.clear();

    if (clearMessages)
    {
        reqDb->databaseConn->emptyMessages();
        reqDb->params.clear();
        reqDb->subRequests.clear();
        reqDb->databaseConn.Release();
        reqDb->completed = true;
    }
}

void UserDataManager::SendUpdateFriendListInfosRequests()
{
    std::vector<UserDataDb::FriendInfo*>  friendList = GetDb()->GetFriendsList();
    if ( friendList.size() > 0 )
    {
        for ( int n = 0; n < friendList.size() ; n++ )
        {
            const char* friendId = friendList.at(n)->uniqueId.c_str();
            //CLAW_MSG("friendList " << friendId);

            if ( !IsBot(friendId) )
            {
                SendGetFriendInfoRequest( friendList.at(n)->uniqueId.c_str() );
            }
        }
    }
}

void UserDataManager::HandleResponseFriendList( std::vector<Claw::NarrowString> info )
{
   // CLAW_MSG("your friends list - Friends count:"<< info[0] << "your new Friends count:"<< info[1]);

    int newFriendsCount = atoi( info[1].c_str());
    GetDb()->SetNewFriendsCount( newFriendsCount );

    if ( newFriendsCount > 0 && m_lua )
    {
        m_lua->PushNumber( newFriendsCount );
        m_lua->Call( "NewFriendsNotifierShow", 1, 0 );
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_FRIEND_INVITATION_CONFIRMED, newFriendsCount );
    }

    //clear first
    GetDb()->FlushFriends();

    //always BOT at least at start
    CreateAndAddBotFriend();

    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();

    //normal friends
    for ( int n = 2; n < info.size(); n+=11 )
    {
        //CLAW_MSG( "friend nickname:" << info[n] << " friend fb_if:" << info[n+1] << " friend unique_id:" << info[n+2] << " friend level:" << info[n+3] 
        //<< "help timer:" << info[n+4] << "weapon_id:" << info[n+5] << "weapon_level:" << info[n+6] << "gift timer:" << info[n+7] << "completed events:" 
        //<< info[n+8] << "killed enemies:" << info[n+9] << "vip status:" << info[n+10] );

        const char* userId = info[n+2].c_str();
        const char* name = info[n].c_str();
        const char* socialId = info[n+1].c_str();
        int userLevel = atoi( info[n+3].c_str() );
        const char* canHelp = info[n+4].c_str();

        const char* weapon_id = info[n+5].c_str();
        const char* weapon_level = info[n+6].c_str();
        const char* gift_timer = info[n+7].c_str();
        int completed_events = atoi( info[n+8].c_str() );
        int killed_enemies = atoi( info[n+9].c_str() );
        const char* vip_status =  info[n+10].c_str();

        GetDb()->AddFriend( new UserDataDb::FriendInfo( userId, name, userLevel , socialId ,  weapon_id , weapon_level , killed_enemies , completed_events , gift_timer , canHelp , vip_status ) );
        //GetDb()->AddFriend( userId, name, userLevel, fbId , canHelp );

        if ( info[n+1] != "" )
        {
            Claw::NarrowString filename = UserDataInput::GetInstance()->GetTextImageFilename( socialId );
            Claw::FilePtr file( Claw::OpenFile( filename ) );
            if ( !file )
            { 
                if ( IsGooglePlusId( socialId ) )
                {
                    Claw::NarrowString socialGpId( info[n+1].substr(2, info[n+1].size()) ); 
                    gs->GetAvatar( socialGpId.c_str() , GetAvatarsSize() );
                }
                else
                {
                    fb->GetAvatar( socialId );
                }
            }
        }
    }

    m_lua->Call( "ResetUpdateDBTimer", 0 , 0 );
   
}

void UserDataManager::HandleResponseInvitationsList( std::vector<Claw::NarrowString> info )
{
    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();
    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
    GetDb()->FlushInvite();
    CLAW_MSG("your invitations - list invitations count: "<< info[0]);
    int count = info.size();

    for ( int n = 1; n < count; n+=12 )
    {
        //CLAW_MSG( "friend nickname:" << info[n] << " friend fb_if:" << info[n+1] << " friend unique_id:" << info[n+2] << " friend level:" << info[n+3] 
        //<< "help timer:" << info[n+4] << "weapon_id:" << info[n+5] << "weapon_level:" << info[n+6] << "gift timer:" << info[n+7] << "completed events:" 
        //<< info[n+8] << "killed enemies:" << info[n+9] << "age:" << info[n+10] << " vip_status: " << info[n+11] );

        const char* userId = info[n+2].c_str();
        const char* name = info[n].c_str();
        const char* socialId = info[n+1].c_str();
        int userLevel = atoi( info[n+3].c_str() );
        const char* canHelp = info[n+4].c_str();

        const char* weapon_id = info[n+5].c_str();
        const char* weapon_level = info[n+6].c_str();
        const char* gift_timer = info[n+7].c_str();
        int completed_events = atoi( info[n+8].c_str() );
        int killed_enemies = atoi( info[n+9].c_str() );
        const char* age = info[n+10].c_str();
        const char* vip_status =  info[n+11].c_str();
        
        GetDb()->AddInvite( new UserDataDb::InviteInfo ( userId, name, userLevel , socialId , weapon_id , weapon_level , killed_enemies , completed_events , gift_timer , canHelp , false, age , vip_status )  );

        if ( info[n+1] != "" )
        {
            Claw::NarrowString filename = UserDataInput::GetInstance()->GetTextImageFilename( info[n+1].c_str() );
            Claw::FilePtr file( Claw::OpenFile( filename ) );
            if ( !file )
            { 
                if ( IsGooglePlusId( socialId ) )
                {
                    Claw::NarrowString socialGpId( info[n+1].substr(2, info[n+1].size()) );
                    gs->GetAvatar( socialGpId.c_str() , GetAvatarsSize() ); 
                }
                else
                {
                    fb->GetAvatar( socialId );
                }
            }
        }
    }

    UserDataInput::GetInstance()->PrepareImages();
}

void UserDataManager::UpdateResistanceDB()
{
    if ( IsLogin() && !m_dbSyncInProgress )
    {
        m_dbSyncInProgress = true;
        if ( m_lua ) m_lua->Call("ResetSyncTimeUpdateInfo",0,0);

        UpdateAll();
    }
}

void UserDataManager::UpdateAll()
{
        if ( !IsLogin() ) return;

        if ( m_lua ) m_lua->Call("ResetSyncTimeUpdateInfo",0,0);

        //Get Friend List Request after login
        SendGetFriendsListRequest();
        //Get inivitations
        SendGetFriendRequestsListRequest();
        //Get gifts
        SendGetFriendsGiftsRequest();
        //leaderboards
        SendGetSurvivalScoresRequest();
        SendGetSurvivalScoresFriendsRequest();

        SyncData();
}

int UserDataManager::DoRequest()
{
    bool allCompleted = false;
    bool errorConnection = false;
    int count = m_requestsList.size();
    bool isDbSyncing = false;

    m_requestTimer = TIME_FOR_TIMEOUT;

    if ( !m_forceCancel ) 
    {
        if ( count > 1 )
        {
            std::vector<RequestDb*> tempRequestsList;
            RequestDb* reqMulti = new RequestDb( MSGID_MULTIPART_MESSAGE , MSGID_MULTIPART_MESSAGE );
            for (int n = 0; n < m_requestsList.size(); n++ )
            {
                RequestDb* req = m_requestsList.at(n);
                if ( req->requestID !=  MSGID_MULTIPART_MESSAGE )
                {
                    int sizeParameters = req->params.size();

                    for (int i=0; i < sizeParameters; i++)
                    {
                        reqMulti->databaseConn->addMessageElement( req->params.at(i) );
                    }

                    reqMulti->databaseConn->addMessage( req->requestID, req->answerID );
                    reqMulti->subRequests.push_back( new SubRequest( req->requestID, req->params ) );

                    if ( !isDbSyncing )
                    {
                        isDbSyncing = req->requestID == MSGID_GET_FRIENDS;
                    }
                }
                else
                {
                    tempRequestsList.push_back( req );
                }
            }

            m_requestsList.clear();
            m_requestsList.push_back( reqMulti );

            if ( tempRequestsList.size() > 0 )
            {
                for (int n = 0; n < tempRequestsList.size(); n++ )
                {
                    m_requestsList.push_back( tempRequestsList.at(n) );
                }
            }

            tempRequestsList.clear();
        }

        for (int n = 0; n < m_requestsList.size(); n++ )
        {
            RequestDb* req = m_requestsList.at(n);

            if ( !req->completed )
            {
                if ( req->requestID == MSGID_MULTIPART_MESSAGE )
                {
                    errorConnection = req->databaseConn->SendMultipart();
                }
                else
                {
                    errorConnection = req->databaseConn->Send();
                }

                if ( errorConnection )
                {
                    RecieveMessage( req , MSGID_ERROR_NO_DATABASE_CONNECTION );
                }
            }
        }

        while( ! allCompleted )
        {
            allCompleted = true;
            //handle requests

            for (int n = 0; n < m_requestsList.size(); n++ )
            {
                RequestDb* req = m_requestsList.at(n);

                if ( !isDbSyncing )
                {
                    isDbSyncing = req->requestID == MSGID_GET_FRIENDS;
                }

                if ( req->recieved == false )
                {
                    allCompleted = false;
                    int msgReceived = req->databaseConn->isMessageReceived();

                    if ( msgReceived != MSGID_NONE && msgReceived != MSGID_FALSE )
                    {
                        RecieveMessage( req , msgReceived );
                    }
                }
            }
        }

        if ( isDbSyncing )
        {
            SaveDbSyncTime();
            m_dbSyncInProgress = false;
        }
    }
    else
    {
        CLAW_MSG( "Forced cancelled all requests" );
    }
    return 1;
}

bool UserDataManager::SendRequest( RequestDb* req )
{
    int sizeParameters = req->params.size();

    for (int i=0; i < sizeParameters; i++)
    {
        req->databaseConn->addMessageElement( req->params.at(i) );
    }

    req->databaseConn->addMessage( req->requestID, req->answerID );
    m_requestsList.push_back(req);

    if ( req->refreshRequired ) 
        ForceSendRequests();
    return true;
}

void UserDataManager::CreateAndAddBotFriend()
{
    int level =  1;
    Claw::Registry::Get()->Get( "/monstaz/player/level", level );

    int kills = 0;
    Claw::Registry::Get()->Get( "/monstaz/player/kills", kills );

    int completedEvents = 0;
    Claw::Registry::Get()->Get( "/monstaz/player/eventsCompleted", completedEvents );

    int timeStampGift = 0;
    Claw::Registry::Get()->Get( "/monstaz/aifriend/sendGift" , timeStampGift );

    int timeStampCanHelp = 0;
    Claw::Registry::Get()->Get( "/monstaz/aifriend/canHelp" , timeStampCanHelp );

    Claw::NarrowString weaponId = "smg";
    Claw::NarrowString weaponLevel = "0"; 

    m_lua->Call( "ItemDbGetBestOwnedWeaponId", 0, 2 );
    weaponId = m_lua->CheckString( -2 );
    weaponLevel = m_lua->CheckString( -1 );
    m_lua->Pop(2);

    Claw::NarrowString canSendGiftTime = "0" ; 
    Claw::NarrowString canSendHelp = "0" ; 

    int now = GetTime();
    int timeLeftGift = timeStampGift + 86400 - now;

    if ( timeLeftGift > 0 )
    {
        std::ostringstream ssGift;
        ssGift << timeLeftGift;
        canSendGiftTime = ssGift.str();
    }

    int timeLeftHelp = ( timeStampCanHelp + 86400 - now);

    if ( timeLeftHelp > 0 )
    {
        std::ostringstream ss;
        ss << timeLeftHelp;
        canSendHelp = ss.str();
    }

    UserDataDb::FriendInfo* bot =  new UserDataDb::FriendInfo( UserDataManagerConst::BOT_NAME,UserDataManagerConst::BOT_NAME, level , "" ,  weaponId , weaponLevel , kills , completedEvents, canSendGiftTime,canSendHelp , "1" );
    GetDb()->UpdateFriendInfoList ( bot );
}

bool UserDataManager::IsBot( const char* uniqueID )
{
    return Claw::NarrowString( uniqueID ) == UserDataManagerConst::BOT_NAME;
}

bool UserDataManager::IsFbUserSet()
{
    return m_fbId != UserDataManagerConst::UNKOWN_FB_ID;
}

void UserDataManager::SetFbUserId( const char* fbID  )
{
     m_fbId = fbID;
}

void UserDataManager::RegisterObserver( Observer* callback )
{
    m_observers.insert( callback );
}

void UserDataManager::UnregisterObserver( Observer* callback )
{
    m_observers.erase( callback );
}

void UserDataManager::NotifySynchronisationStart()
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();

    for( ; it != end; ++it )
        (*it)->OnSynchronisationStart();
}

void UserDataManager::NotifySynchronisationEnd( bool success )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();

    for( ; it != end; ++it )
        (*it)->OnSynchronisationEnd( success );
}

void UserDataManager::NotifySynchronisationSkip()
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();

    for( ; it != end; ++it )
        (*it)->OnSynchronisationSkip();
}

void UserDataManager::NotifyRequestFinished( bool success )
{
    ObserversIt it = m_observers.begin();
    ObserversIt end = m_observers.end();

    for( ; it != end; ++it )
        (*it)->OnRequestFinished( success );
}

int UserDataManager::DownloadEntry( void* ptr )
{
    UserDataManager* udm = (UserDataManager*)ptr;
    bool result = udm->DoRequest();

    if( result )
    {
        // Save last sync time on success
          udm->SaveSyncTime();
    }
    udm->m_syncInProgress = false;

    udm->NotifySynchronisationEnd( result );
    return result;
}

bool UserDataManager::SyncData( bool all )
{
    if ( all && m_requestsList.empty() && ShouldDbSync() )
    {
        UpdateResistanceDB();
    }


    if( !m_requestsList.empty() && !m_syncInProgress )
    {
        if ( ShouldSync() )
        {

            if ( m_doRequest )
            {
                //m_doRequest->Kill();
                delete m_doRequest;
            }
            m_doRequest = new Claw::Thread( UserDataManager::DownloadEntry, this );
            m_syncInProgress = true;
            NotifySynchronisationStart();
            return true;
        }
    }

    NotifySynchronisationSkip();
    return false;
}

bool UserDataManager::HandleRequests()
{
    bool needRefresh = false; 
    for (int n = 0; n < m_requestsList.size() ; n++ )
    {
        RequestDb* req = m_requestsList.at(n);

        if ( !req->completed ) 
        {
            if ( req->requestID == MSGID_ACCOUNT_LOGIN 
            || req->requestID == MSGID_CREATE_ACCOUNT_WITH_EMAIL 
            || req->requestID == MSGID_CREATE_ACCOUNT_WITH_FB 
            || req->requestID == MSGID_GET_HAS_FB_ACCOUNT )
            {
                needRefresh = true;
            }

            HandleMessage( req , true );
        }
    }

//    CLAW_MSG("Clearing requests list (size) " << m_requestsList.size() );

//    for (int n = 0; n < m_requestsList.size() ; n++ )
//    {
//        RequestDb* req = m_requestsList.at(n);
//        CLAW_MSG("RequestDb  " <<  req->requestID << " answer " << req->resultID <<" status recieved " << req->recieved <<" status completed " << req->completed );
//    }

    //m_requestsList.clear();
    std::vector<RequestDb*>::iterator it = m_requestsList.begin();;

    for( ; it != m_requestsList.end();  )
    {
        if ((*it)->recieved == true && (*it)->completed == true )
        {
            delete *it;  
            it = m_requestsList.erase(it);
        }
        else
        {
           ++it;
        }
    }

//    CLAW_MSG("After clear " << m_requestsList.size() );

    if ( needRefresh ) 
        UpdateResistanceDB();

    return true;
}

bool UserDataManager::ShouldSync()
{
    int lastSyncTime = 0;
    int now = GetTime();
    Claw::Registry::Get()->Get("/monstaz/lastSyncTime", lastSyncTime );
    return (now - lastSyncTime) > SYNCHRONIZATION_TIME_PERIOD;
}

bool UserDataManager::ShouldDbSync()
{
    int lastSyncTime = 0;
    int now = GetTime();
    Claw::Registry::Get()->Get("/monstaz/lastDbSyncTime", lastSyncTime );
    bool isDbSocialLock = false;
    if ( m_lua )
    {
        m_lua->Call( "IsSocialHqLocked", 0, 1 );
        isDbSocialLock = m_lua->CheckBool( -1 );
        m_lua->Pop(1);
    }
    return ( !isDbSocialLock && (now - lastSyncTime) > DB_SYNCHRONIZATION_TIME_PERIOD );
}

void UserDataManager::SaveSyncTime()
{
    int now = GetTime();
    Claw::Registry::Get()->Set( "/monstaz/lastSyncTime", now );
}

void UserDataManager::SaveDbSyncTime()
{
    int now = GetTime();
    Claw::Registry::Get()->Set( "/monstaz/lastDbSyncTime", now );
}

void UserDataManager::ForceSendRequests()
{
    Claw::Registry::Get()->Set("/monstaz/lastSyncTime", GetTime() - SYNCHRONIZATION_TIME_PERIOD );
}

void UserDataManager::FillLuaFromDb()
{
    UserDataDb* db = GetDb();

    m_lua->Call( "GetUpdateInfoTimes", 0, 5 );
    int friendsSyncTime = m_lua->CheckNumber( -5 );
    int requestsSyncTime = m_lua->CheckNumber( -4 );
    int leaderboardsFriendsSyncTime = m_lua->CheckNumber( -3 );
    int leaderboardsGlobalSyncTime = m_lua->CheckNumber( -2 );
    int giftsSyncTime = m_lua->CheckNumber( -1 );
    m_lua->Pop(5);


    if ( friendsSyncTime > m_lastFriendsSyncTime )
    {
        m_lastFriendsSyncTime = friendsSyncTime;
        //friends
        FillLuaFriendsList();
    }

    if ( giftsSyncTime > m_lastGiftsSyncTime )
    {
        m_lastGiftsSyncTime = giftsSyncTime;
        //gifts
        std::vector<UserDataDb::GiftData*> gifts = db->GetGiftDataList();
        int sizeGifts = gifts.size();

        m_lua->Call( "FlushGifts", 0 , 0 );

        if ( sizeGifts > 0 )
        {
            for ( int i=0; i < sizeGifts; i++ )
            {
                UserDataDb::GiftData* gift = gifts.at(i);

                if ( !gift->used )
                {
                    m_lua->PushString(  gift->givenBy );
                    m_lua->PushString( gift->name );
                    m_lua->PushString( gift->fbId );
                    m_lua->PushString( gift->level );
                    m_lua->PushString( gift->itemId  );
                    m_lua->PushString( gift->qty );
                    m_lua->PushString( gift->giftUniqueId );
                    m_lua->PushString( gift->age );
                    m_lua->PushString( gift->vipstatus );
                    m_lua->Call( "AddGift", 9, 0 );
                }
            }
        }

         m_lua->Call( "GiftsSortItems", 0, 0 );
         m_lua->Call( "UpdateGiftsButton", 0, 0 );
    }

    if ( requestsSyncTime > m_lastRequestsSyncTime )
    {
        m_lastRequestsSyncTime = requestsSyncTime;
        //Invitations
        std::vector<UserDataDb::InviteInfo*> invites = db->GetInvitesList();
        int sizeInvitations = invites.size();

        m_lua->Call( "FlushInvitations", 0 , 0 );

        if ( sizeInvitations > 0 )
        {
            std::vector<UserDataDb::InviteInfo*>::iterator it, end;
            it = invites.begin();
            end = invites.end();

            for( ; it != end; ++it )
            {
                if ( (*it)->completed == false )
                {

                    m_lua->PushString( (*it)->name); 
                    std::ostringstream sLevel;
                    sLevel << (*it)->userLevel;
                    m_lua->PushString( sLevel.str() );
                    m_lua->PushString( (*it)->facebookId );
                    m_lua->PushString ((*it)->uniqueId );
                    m_lua->PushString ((*it)->canHelp );
                    m_lua->PushString ((*it)->canSendGift );
                    m_lua->PushString ((*it)->age );
                    m_lua->PushString ((*it)->vipstatus );
                    m_lua->Call( "AddInvitation", 8, 0 );

                }
            }

            m_lua->Call( "InviteSortItems", 0, 0 );
        }
    }
}

void UserDataManager::UserLoggedIn( const char* uniqueId, bool updateStats )
{
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_SOCIAL_LOGIN_SUCCEED, 1, uniqueId );

    Claw::Registry::Get()->Set( "/internal/uniqueId",  uniqueId );
    Claw::Registry::Get()->Set( "/internal/loggedIn", true );
    InitUserId( uniqueId );
    GetDb()->SetUserID( uniqueId );

    const char* weaponId = "smg";
    const char* weaponUpgradeLevel = "0";

    Claw::Registry::Get()->Get( "/monstaz/player/bestweapon", weaponId );
    Claw::Registry::Get()->Get( "/monstaz/player/bestweaponlevel", weaponUpgradeLevel );
    UserDataInput::GetInstance()->PrepareImages();
    if ( updateStats )
    {
        SendSetWeaponRequest( weaponId, atoi( weaponUpgradeLevel ) );
        bool subscriptionActive = false;
        Claw::Registry::Get()->Get( "/monstaz/subscription", subscriptionActive );
        SendSetVipStatus( subscriptionActive );
    }
}

void UserDataManager::UserLoggedOut()
{
    GetDb()->FlushFriends();
    GetDb()->FlushGiftDataList();
    GetDb()->FlushInvite();
}

void UserDataManager::ForceEndRequests()
{
    int count = m_requestsList.size();

    for (int n = 0; n < count ; n++ )
    {
        if ( m_requestsList.at(n)->recieved == false )
            RecieveMessage( m_requestsList.at(n) , MSGID_ERROR_NO_DATABASE_CONNECTION );
        if ( m_requestsList.at(n)->databaseConn )
            m_requestsList.at(n)->databaseConn->ForceClose();
    }
}

void UserDataManager::Update( float dt )
{
    if( m_requestsList.size() > 0  && m_requestTimer > 0 )
    {
        m_requestTimer -= dt;
        if( m_requestTimer <= 0 )
        {
            //CLAW_MSG("Db request timeout cancelling requests");
            ForceEndRequests();
        }
    }
}

void UserDataManager::SendFbUserAvatarRequest()
{
    Network::Facebook* fb = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetFacebook();

    if (fb->IsAuthenticated())
    {
        fb->GetAvatar( "me" );
    }
}

bool UserDataManager::SendDeleteFriend( const char* friendUniqueId )
{
    // remove a friend
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_DELETE_FRIEND , MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    req->params.push_back( friendUniqueId );

    return SendRequest( req );
}

void UserDataManager::FillLuaFriendsList()
{
    std::vector<UserDataDb::FriendInfo*> friends = GetDb()->GetFriendsList();
    int sizeFriends = friends.size();

    //new Friends
    m_lua->PushNumber( GetDb()->GetNewFriendCount() );
    m_lua->Call( "SetNewFriendsCount", 1 , 0 );

    m_lua->Call( "FlushFriends", 0 , 0 );

    //normal friends
    for ( int n = 0; n < sizeFriends; n++ )
    {
        UserDataDb::FriendInfo* fi = friends.at(n);

        m_lua->PushString( fi->uniqueId );
        m_lua->PushString( fi->name );
        m_lua->PushNumber( fi->userLevel );
        m_lua->PushString( fi->facebookId );
        m_lua->PushNumber( fi->kills );
        m_lua->PushNumber( fi->eventsCompleted );
        m_lua->PushString( fi->bestWeaponId );
        m_lua->PushString( fi->bestWeaponUpgrade );
        m_lua->PushString( fi->canSendGift );
        m_lua->PushString( fi->canHelp );
        m_lua->PushString( fi->vipstatus );
        m_lua->Call( "AddFriendFull", 11, 0 );
        //AddFriendFull( userID, name , level , fbId , kills , events , weaponId , weaponLevel , canSendGifts , canHelp )

    }

     m_lua->Call( "FriendSortItems", 0 , 0 );
}

bool UserDataManager::SendSetGiftAcceptedAll()
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_USER_GIFT_ACCEPTED_ALL, MSGID_OK );
    req->params.push_back( m_uniqueIdStr );

    return SendRequest( req );
}

void UserDataManager::ForceCancel()
{
    CLAW_MSG(" ForceCancel all user db requests ");
    m_requestsList.clear();
    m_forceCancel = true;
}

void UserDataManager::SetDebugBackupFriend( const char* weaponId )
{
    if (m_lua)
    {
        m_lua->PushString( weaponId );
        m_lua->Call("ForceBackupFriend", 1 , 0 );
    }
}

bool UserDataManager::SendSetVipStatus( bool enabled )
{
    if ( !IsLogin() ) return false;

    RequestDb* req = new RequestDb( MSGID_SET_VIP_STATUS, MSGID_OK );
    req->params.push_back( m_uniqueIdStr );
    if ( enabled )
    {
        req->params.push_back( "1" );
    }
    else
    {
        req->params.push_back( "0" );
    }

    return SendRequest( req );
}

bool UserDataManager::IsGooglePlusId( const char* socialId )
{
    return Claw::NarrowString ( socialId ).substr(0,2) == UserDataManagerConst::GP_ID_PREFIX;
}

Claw::NarrowString UserDataManager::ConstructGpUserId( const char* orignalId )
{
    Claw::NarrowString id( UserDataManagerConst::GP_ID_PREFIX ); 
    id.append( Claw::NarrowString(orignalId) );
    return id; 
}

int UserDataManager::GetTime()
{
    Claw::UInt32 now = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime();
    if( now == 0 ) now = Claw::Time::GetTime();
    return (int) now;
}

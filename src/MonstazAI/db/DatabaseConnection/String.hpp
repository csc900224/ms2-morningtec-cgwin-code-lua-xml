#ifndef DJF_STRING_HPP
#define DJF_STRING_HPP

#include <stdarg.h>

#include "claw/base/RefCounter.hpp"
#include "claw/base/String.hpp"

    typedef wchar_t widechar;

    class WideString : public std::basic_string<widechar, std::char_traits<widechar> >
    {
    public:
        typedef std::basic_string<widechar, std::char_traits<widechar> > Base;

        WideString() {}
        WideString( const WideString &s, size_type pos = 0, size_type n = npos ) : Base(s,pos,n) {}
        WideString( const widechar *s ) : Base(s) {}
        WideString( const widechar *s, size_type n ) : Base(s,n) {}
        WideString( size_type n, widechar c ) : Base(n,c) {}
        WideString( Base b ) : Base(b) {}
        WideString( const WideString::const_iterator& it1, const WideString::const_iterator& it2 ) : Base( it1, it2 ) {}

        explicit WideString( const Claw::NarrowString &s );
        Claw::NarrowString NarrowString() const;
        Claw::String ClawString() const;

#ifdef CLAW_ANDROID
        // Android STL implementation is buggy and requires clearing string before assigning new value,
        // otherwise string will become corrupted.
        WideString &operator=(const WideString &rhs) { clear(); Base::operator=(rhs); return *this; }
        WideString &operator=(const widechar *rhs)  { clear(); Base::operator=(rhs); return *this; }
        WideString &operator=(widechar rhs) { clear(); Base::operator=(rhs); return *this; }
#else
        WideString &operator=(const WideString &rhs) { Base::operator=(rhs); return *this; }
        WideString &operator=(const widechar *rhs)  { Base::operator=(rhs); return *this; }
        WideString &operator=(widechar rhs) { Base::operator=(rhs); return *this; }
#endif

        WideString  substr(size_type _Off = 0, size_type _Count = npos) const
        {
            return (WideString(*this, _Off, _Count) );
        }
    protected:
        static size_t mbstowcs( widechar* pwcs, const char* s, size_t n );
        static size_t wcstombs( char* s, const widechar* src, size_t wn );
    };

    class String : public WideString
    {
    public:
        String() : WideString() {}
        String( const WideString &s, size_type pos = 0, size_type n = npos ) : WideString( s, pos, npos ) {}
        String( const widechar *s ) : WideString( s ) {}
        String( const widechar *s, size_type n ) : WideString( s, n ) {}
        String( size_type n, widechar c ) : WideString( n, c ) {}
        String( Base b ) : WideString( b ) {}
        String( const WideString::const_iterator& it1, const WideString::const_iterator& it2 ) : WideString( it1, it2 ) {}
        String( const Claw::NarrowString &s ) : WideString( s ) {}
        String( const char* fmt, ... );
        String( const widechar* fmt, ... );

        void Format( const char* fmt, ... );
        void Format( const char* fmt, va_list argptr );

        void Format( const widechar* fmt, ... );
        void Format( const widechar* fmt, va_list argptr );

        void FormatAsHHMMSS( int time );
    private:
        void vswprintf(widechar *wcs, size_t maxlen, const widechar *format, va_list args);
    };

#endif // DF_SIGNAL_HPP

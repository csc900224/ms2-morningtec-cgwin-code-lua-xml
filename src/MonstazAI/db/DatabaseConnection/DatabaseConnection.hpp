#ifndef DJF_DATABASE_CONNECTION_INCLUDED
#define DJF_DATABASE_CONNECTION_INCLUDED

#include <vector>

#include "claw/base/RefCounter.hpp"
#include "claw/network/HttpRequest.hpp"
#include "claw/network/Network.hpp"
#include "claw/network/Uri.hpp"

    typedef struct
    {
        int sent;
        int expected;
        int received;
        Claw::NarrowString content;
    } MessageData;

    enum
    {
        MSG_MAX_LENGTH  = 65536,

        MSGID_START = -1,
        MSGID_FALSE = 0,
        MSGID_TRUE = 1,
        MSGID_HELO = 2,
        MSGID_OK = 3,
        MSGID_MULTIPART_MESSAGE = 4,

        MSGID_CREATE_ACCOUNT_WITH_EMAIL = 10,
        MSGID_PASSWORD_CHANGE = 11,

        MSGID_ADD_FRIEND = 12,
        MSGID_GET_FRIENDS = 13,

        MSGID_GET_FRIENDS_GIFTS = 14,
        MSGID_SEND_FRIEND_GIFT = 15,
        MSGID_GET_FRIEND_INFO = 16,
        MSGID_SET_SURVIVAL_SCORE = 17,

        MSGID_GET_SURVIVAL_SCORES = 18,

        MSGID_GET_DAILY_QUESTS = 19,
        MSGID_GET_ITEM_CATEGORIES = 20,
        //MSGID_GET_ITEMS_AND_PRICES = 21,

        MSGID_CHANGE_EMAIL = 21,
        MSGID_GET_VERSION = 22,
        MSGID_UNIQUE_ID = 23,
        MSGID_SET_FB_ID = 24,
        MSGID_SET_WEAPON = 25,
        MSGID_SET_USER_LEVEL = 26,
        MSGID_CREATE_ACCOUNT_WITH_FB = 27,
        MSGID_ACCOUNT_LOGIN_FB = 28,

        MSGID_REMIND_PASSWORD = 29,

        MSGID_ACCOUNT_LOGIN = 30,
        MSGID_RETURN_FRIENDS = 31,
        MSGID_RETURN_FRIENDS_GIFTS = 32,
        MSGID_RETURN_FRIEND_INFO = 33,
        MSGID_RETURN_ITEMS = 34,
        MSGID_RETURN_ITEM_CATEGORIES = 35,
        MSGID_RETURN_VERSION = 36,
        MSGID_RETURN_DAILY_QUESTS = 37,
        MSGID_RETURN_SURVIVAL_SCORES = 38,
        MSGID_SET_USER_PROGRESS = 39,
        MSGID_RETURN_QUESTS_TIME = 40,
        MSGID_CONFIRM_RESET_PASSWORD = 41,
        MSGID_GET_FRIEND_REQUESTS = 42,
        MSGID_SET_FRIEND_REQUEST_RESPONSE = 43,
        MSGID_RETURN_USER_DATA = 44,
        MSGID_GET_HAS_FB_ACCOUNT = 45,
        MSGID_GET_FRIEND_INFO_FULL = 46,
        MSGID_GET_FRIEND_INFO_SHORT = 47,
        MSGID_RETURN_FRIEND_INFO_FULL = 48,
        MSGID_RETURN_FRIEND_INFO_SHORT = 49,
        MSGID_GET_FRIEND_HELP = 50,
        MSGID_SET_FRIEND_HELP = 51,
        MSGID_SET_USER_PARAMS = 52,
        MSGID_GET_USER_PARAMS = 53,
        MSGID_RETURN_USER_PARAMS = 54,
        MSGID_GET_USER_BEST_WEAPON =  55,
        MSGID_RETURN_USER_BEST_WEAPON = 56,
        MSGID_USER_GIFT_ACCEPTED = 57,
        MSGID_EMAIL_PASSWORD_CHANGE = 58,
        MSGID_RETURN_FRIENDS_ALL = 59,
        MSGID_GET_NEW_FRIENDS_NUM = 60,
        MSGID_RETURN_NEW_FRIENDS_NUM = 61,
        MSGID_SET_NEW_FRIENDS_APPROVE = 62,
        MSGID_GET_SURVIVAL_SCORES_FRIENDS =63,
        MSGID_DELETE_FRIEND = 64,
        MSGID_RESET_FRIEND_HELP = 65,
        MSGID_USER_GIFT_ACCEPTED_ALL = 66,
        MSGID_SET_VIP_STATUS = 67,

        MSGID_ERROR_UNDEFINED = 100,
        MSGID_ERROR_WRONG_MSGID = 101,
        MSGID_ERROR_WRONG_PARAMETERS = 102,
        MSGID_ERROR_MALFORMED_MSG = 103,
        MSGID_ERROR_CHECKSUM_FAILED = 105,
        MSGID_ERROR_NICK_ALREADY_EXISTS = 108,
        MSGID_ERROR_INVALID_ACCOUNT_DATA = 109,
        MSGID_ERROR_AUTH_FAILED = 110,
        MSGID_ERROR_WRONG_ENTRIES_COUNT = 111,
        MSGID_ERROR_NO_SUCH_USER = 112,
        MSGID_ERROR_BAD_SIZE = 113,
        MSGID_ERROR_SERVER_EXECUTION_FAILED = 114,
        MSGID_ERROR_NO_SUCH_FRIEND = 115,
        MSGID_ERROR_SUBMIT_DATA_FIRST = 116,
        MSGID_ERROR_ALREADY_FRIEND = 117,
        MSGID_ERROR_UNKNOWN_LANGUAGE = 118,
        MSGID_ERROR_WAITING_FOR_FRIEND_RESPONSE= 119,
        MSGID_ERROR_NO_SUCH_WEAPON =120,
        MSGID_ERROR_EMAIL_ALREADY_USED = 121,
        MSGID_ERROR_GIFT_ALREADY_GIVEN = 122,
        MSGID_ERROR_CANT_HELP_FRIEND = 123,
        MSGID_ERROR_PARSING_EMAIL = 124,
        MSGID_ERROR_HW_KEY_ALREADY_EXISTS = 125,
        MSGID_ERROR_BAD_WORDS = 126,
        MSGID_ERROR_NO_DATABASE_CONNECTION = 198,
        MSGID_ERROR_ = 199,
        MSGID_END = 200,
        MSGID_NONE = 300,

        MSGID_DUMMY = 999
    };

    class DatabaseConnection : public Claw::RefCounter
    {
        std::vector< int >        m_crcTable;
        int m_msgStatus;
    
        std::vector< MessageData >    m_messages;
        std::vector< Claw::NarrowString >    m_msgElements;
    
    public:
        DatabaseConnection();
    
        Claw::HttpRequestPtr m_httpRequest;

        bool Send( bool usePost = false );
        bool SendMultipart( bool usePost = false );

        int addMessage(int msgID);
        int addMessage(int msgID, int expectedResponse);
        void addMessageElement(Claw::NarrowString element);

        void emptyMessages();
        bool IsMessagesEmpty() const;
        void ForceClose();
        int parseDataReceived(std::vector< char >& data);
        int isMessageReceived();
        bool isMessageReceived(int msgID);
        bool transferSuccesful();
        bool transferSuccesful(int msgID);
        Claw::NarrowString constructMsg(int msgID, std::vector< Claw::NarrowString > elements);
        int getCRC(std::vector< char > buf, int offset, int len);
        Claw::NarrowString int2Hex(int i);
        Claw::NarrowString parseInt(int value, int digits);
        std::vector< char > encodeData(Claw::NarrowString src, int baseKey);
        Claw::NarrowString base64_encode(std::vector< char > data);
        std::vector< char > base64_decode(Claw::NarrowString txt);

        Claw::NarrowString blowfishEncode(Claw::NarrowString enc);
        Claw::NarrowString blowfishDecode(Claw::NarrowString enc);

        Claw::NarrowString decodeData(std::vector< char > src, int baseKey);
        void makeCRCTable();
        Claw::NarrowString getResponse();
        Claw::NarrowString getResponse(int msgID);
        Claw::NarrowString getResponse(int msgID, int state);
        std::vector< Claw::NarrowString > splitString( Claw::NarrowString& str);
        Claw::NarrowString constructMultipartMsg(std::vector< MessageData > msgVector);

        int getMsgID(Claw::NarrowString& msg);
        int getMessageLength(Claw::NarrowString msg);

        MessageData decodeMessage(Claw::NarrowString msg);
        std::vector< Claw::NarrowString > getContent(int msgID);
        Claw::NarrowString getContent();
        MessageData getMessage(int index);
        void delFirstMessage();
        int getMessagesCount();

        static const int DEFAULT_ENCRYPT_KEY;
        static const int CRC_TABLE_LENGTH;
        static const Claw::NarrowString ALPHABET64;
        static Claw::NarrowString NETWORK_DATABASE_URL;
        static const Claw::NarrowString NETWORK_PHP_VARIABLE_GET;
        static const Claw::NarrowString NETWORK_PHP_VARIABLE_POST;
        static const int FILL_CHAR;

        std::vector< char > m_vectorData;
        int m_receivedId;
    };

    typedef Claw::SmartPtr< DatabaseConnection > DatabaseConnectionPtr;
#endif // DATABASE_CONNECTION_INCLUDED

#include <stdio.h>
#include <string.h>

#include "MonstazAI/db/DatabaseConnection/String.hpp"

    WideString::WideString( const Claw::NarrowString &src )
    {
        size_t size = src.size();
        if( size > 0 )
        {
            widechar *tmpBuf = new widechar[size];
            size_t wSize = mbstowcs( tmpBuf, src.data(), size );

            if( wSize != (size_t)(-1) )
            {
                assign( tmpBuf, wSize );
            }
            delete[] tmpBuf;
        }
    }

    Claw::NarrowString WideString::NarrowString() const
    {
        return Claw::NarrowString(*this);
    }

    Claw::String WideString::ClawString() const
    {
        return *this;
    }

    size_t WideString::wcstombs( char* s, const widechar* src, size_t wn )
    {
        char *p;
        size_t len, n;
        widechar wc;
        unsigned char m;

        n = 0;

        while( wn )
        {
            wc = *src;

            *s = (char)wc;
            len = 1;

            if( wc & 0xFFFFFF80UL )
            {
                if( ( wc & 0x80000000UL ) || ( ((widechar)(wc - 0xfffeU)) < 2) || ( ((widechar)(wc - 0xd800U)) < (0xe000U - 0xd800U) ) )
                {
                    return (size_t) -1;
                }

                wc >>= 1;
                p = s;
                do
                {
                    ++p;
                }
                while (wc >>= 5);

                wc = *src;
                len = p - s;

                m = 0x80;
                while( p>s )
                {
                    m = ( m | 0x100 ) >> 1;
                    *--p = ( wc | 0x80 ) & 0xBF;
                    wc >>= 6;
                }
                *s |= (m << 1);
            }
            else if (wc == 0)
            {
                /* End of string. */
                break;
            }

            ++src;
            --wn;
            n += len;
            s += len;
        }

        return n;
    }

    size_t WideString::mbstowcs( widechar* pwcs, const char* s, size_t n )
    {
        int len = 0;

        while( n )
        {
            int count = 0;
            unsigned int c = (unsigned char)*s;
            unsigned int i = 0x80;

            while( c & i )
            {
                count++;
                i >>= 1;
            }

            c &= i-1;

            while( count > 1 )
            {
                c <<= 6;
                c |= (*++s) & 0x3F;
                count--;
            }

            if( !c )
            {
                break;
            }

            *pwcs++ = c;
            s++;
            n--;
            len++;
        }

        return len;
    }

    // simple and not complete implementation
    void String::vswprintf(widechar *wcs, size_t maxlen, const widechar *format, va_list args)
    {
        while (*format)
        {
            if (*format == '%')
            {
                format++;
                switch (*format++)
                {
                    case '%':
                        *wcs++ = '%';
                        break;
                    case 'i':
                    case 'd':
                        {
                            int val = va_arg(args, int);
                            char buf[32];
                            sprintf(buf, "%d", val);
                            char* ptr = buf;
                            while (*ptr)
                                *wcs++ = *ptr++;
                        }
                        break;
                    case 's':
                        {
                            widechar* src = va_arg(args, widechar*);
                            while (*src)
                                *wcs++ = *src++;
                        }
                    break;
                    case 0:
                        *wcs++ = 0;
                        break;
                }
            } 
            else
            {
                *wcs++ = *format++;
            }
        }
        *wcs++ = 0;
    }

    String::String( const char* fmt, ... )
        : WideString()
    {
        va_list argptr; 
        va_start(argptr, fmt); 
        
        Format( fmt, argptr); 
     
        va_end(argptr); 
    }

    String::String( const widechar* fmt, ... )
        : WideString()
    {
        va_list argptr; 
        va_start(argptr, fmt); 
        
        Format( fmt, argptr); 
     
        va_end(argptr); 
    }

    void String::Format( const char* fmt, ... )
    {
        va_list argptr; 
        va_start(argptr, fmt); 
        
        Format( fmt, argptr); 
     
        va_end(argptr); 
    }
    
    void String::Format( const char* fmt, va_list argptr )
    {
        char buff[1024] = {0};  // maximum 1024 chars
        vsprintf(buff, fmt, argptr); 
        
        widechar wbuff[1024] = {0};
        memset(wbuff, 0, sizeof(wbuff));
        mbstowcs( wbuff, buff, strlen( buff ) ); 

        clear();
        append( wbuff );
    }

    void String::Format( const widechar* fmt, ... )
    {
        va_list argptr; 
        va_start(argptr, fmt); 
        
        Format( fmt, argptr); 
     
        va_end(argptr); 
    }

    void String::Format( const widechar* fmt, va_list argptr )
    {
        widechar wbuff[1024] = {0};  // maximum 1024 chars
        memset(wbuff, 0, sizeof(wbuff));
        vswprintf(wbuff, 1024, fmt, argptr);
        clear();
        append( wbuff );
    }

    void String::FormatAsHHMMSS( int time )
    {
        int hours = time / 3600;
        int minutes = (time - ( hours * 3600 ) ) / 60;
        int seconds = time - ( hours * 3600 ) - ( minutes * 60 );

        Format("%02d:%02d:%02d", hours, minutes, seconds);
    }

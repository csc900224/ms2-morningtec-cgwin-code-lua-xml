#include "MonstazAI/db/DatabaseConnection/DatabaseConnection.hpp"

#include "claw/network/HttpRequest.hpp"
#include "claw/network/Network.hpp"
#include "claw/network/Uri.hpp"
#include "claw/base/Crypto.hpp"
#include "claw/base/Registry.hpp"

#include <sstream>

std::vector< char > toCharVector(Claw::NarrowString str);


DatabaseConnection::DatabaseConnection()
{
    m_messages = std::vector< MessageData >();
    m_msgElements = std::vector< Claw::NarrowString >();

    m_msgStatus = -1;

    m_receivedId = -100;

#if !defined _DEBUG
        DatabaseConnection::NETWORK_DATABASE_URL = Claw::Registry::Get()->CheckString( "/app-config/dbconsts/url" );
#endif

}

void DatabaseConnection::addMessageElement(Claw::NarrowString element)
{
    m_msgElements.push_back(element);
}


int DatabaseConnection::addMessage(int msgID)
{
    return addMessage(msgID, MSGID_OK);
}


int DatabaseConnection::addMessage(int msgID, int expectedResponse)
{
    MessageData md;
    md.sent     = msgID;
    md.expected = expectedResponse;
    md.received = -100;
    md.content  = constructMsg( msgID, m_msgElements );
    m_messages.push_back( md );

    m_msgElements.clear();
    return m_messages.size() - 1;
}


bool DatabaseConnection::SendMultipart(bool usePost)
{

    Claw::NarrowString str = constructMultipartMsg( m_messages );
    Claw::NarrowString url;
    Claw::NarrowString postData;

    if( !usePost )
    {
        url = ( Claw::NarrowString( NETWORK_DATABASE_URL + NETWORK_PHP_VARIABLE_GET + str/*m_messages[0].content*/ ) );
    }
    else 
    {
        url = ( NETWORK_DATABASE_URL );
        postData = NETWORK_PHP_VARIABLE_POST + m_messages[0].content;
    }

    CLAW_MSG( "URL: " << url );
    CLAW_MSG( "POST :" << postData);
    Claw::Network::Open();
    Claw::Uri uri( url );
    m_httpRequest.Reset( new Claw::HttpRequest( uri ) );

    if (usePost)
    {
        m_httpRequest->SetMode(Claw::HttpRequest::M_POST);
        m_httpRequest->SetPostData(postData);
    }

    bool noConnection = true;

    m_httpRequest->Connect();
    if( !m_httpRequest->CheckError() )
    {
        m_httpRequest->Download();
        noConnection = false;
    }

    Claw::Network::Close();

    return noConnection;
}

bool DatabaseConnection::Send( bool usePost )
{
    // Claw::NarrowString str = constructMultipartMsg( m_messages );
    Claw::NarrowString url;
    Claw::NarrowString postData;

    if( !usePost )
    {
        url = ( Claw::NarrowString( NETWORK_DATABASE_URL + NETWORK_PHP_VARIABLE_GET + m_messages[0].content ) );
    }
    else
    {
        url = ( NETWORK_DATABASE_URL );
        postData = NETWORK_PHP_VARIABLE_POST + m_messages[0].content;
    }

    CLAW_MSG( "URL: " << url );
    CLAW_MSG( "POST :" << postData);
    Claw::Network::Open();
    Claw::Uri uri( url );

    m_httpRequest.Reset( new Claw::HttpRequest( uri ) );

    if (usePost)
    {
        m_httpRequest->SetMode( Claw::HttpRequest::M_POST );
        m_httpRequest->SetPostData( postData );
    }
    
    bool noConnection = true;

    m_httpRequest->Connect();

    if( !m_httpRequest->CheckError() )
    {
        m_httpRequest->Download();
        noConnection = false;
    }

    Claw::Network::Close();

    return noConnection;
}

void DatabaseConnection::ForceClose()
{
    if( m_httpRequest )
    {
       m_httpRequest->Shutdown();
    }
}

void DatabaseConnection::emptyMessages()
{
    m_messages.clear();
    m_msgElements.clear();
}

bool DatabaseConnection::IsMessagesEmpty() const
{
    return m_messages.empty();
}

int DatabaseConnection::parseDataReceived( std::vector< char >& data )
{
    Claw::NarrowString msg;
    for (int i = 0; i < data.size(); i++)
        msg += data[i];

    CLAW_MSG( "Downloaded MSG: " << msg );

    int msgID = getMsgID( msg );

    if (msgID == MSGID_ERROR_MALFORMED_MSG)
    {
        return msgID;
    }

    if (msgID != MSGID_MULTIPART_MESSAGE)
    {
        MessageData md = decodeMessage(msg);
        if( !m_messages.empty() )
        {
            m_messages[0].received = md.received;
            m_messages[0].content = md.content;
        }else
        {
            return MSGID_ERROR_MALFORMED_MSG;
        }
        return m_messages[0].received;
    }else
    {
        //return MSGID_ERROR_MALFORMED_MSG;
    }

    MessageData multipart = decodeMessage(msg);
    int multiLength = getMessageLength(msg);
    std::vector< Claw::NarrowString > multiMessages = splitString(multipart.content);
    int elemOffset;
    Claw::NarrowString p;
    int plen;
    for (int i = 0; i < multiMessages.size(); i++)
    {
        elemOffset = atoi( multiMessages[i].c_str() );
        p = msg.substr(8 + multiLength + elemOffset);
        plen = atoi(p.substr(3,5).c_str() ) + 3 + 5;
        p = p.substr(0, plen);

        MessageData md = decodeMessage(p);
        m_messages[i].received = md.received;
        m_messages[i].content = md.content;
    }

    return multipart.received;
}


int DatabaseConnection::getMsgID( Claw::NarrowString& msg )
{
    // message to short?
    if (msg.size() < 3)
        return MSGID_ERROR_MALFORMED_MSG;

    Claw::NarrowString tempMsg = msg.substr(0, 3);
    int msgID = atoi( tempMsg.c_str() );

    // message id out of range?
    if ((msgID < MSGID_START) || (msgID > MSGID_END))
        return MSGID_ERROR_MALFORMED_MSG;

    return msgID;
}


int DatabaseConnection::getMessageLength(Claw::NarrowString msg)
{
    return atoi( msg.substr( 3, 5 ).c_str() );
}

MessageData DatabaseConnection::decodeMessage(Claw::NarrowString msg)
{
    MessageData md;
    // message to short?
    if (msg.size() < 7)
    {
        md.received = MSGID_ERROR_MALFORMED_MSG;
        return md;
    }

    // message id out of range?
    Claw::NarrowString msgTmp = msg.substr(0, 3);
    int msgId = atoi( msgTmp.c_str() );

    if (msgId < MSGID_START || msgId > MSGID_END)
    {
        md.received = MSGID_ERROR_MALFORMED_MSG;
        return md;
    }

    // message is an error code?
    // or short ok/no message?
    if ((msgId >= MSGID_ERROR_UNDEFINED && msgId < MSGID_ERROR_) || msg.size() == 7)
    {
        md.received = atoi( msgTmp.c_str() );
        return md;
    }

    msgTmp = msg.substr(3, 5);
    int msgLen = atoi( msgTmp.c_str() );
    if ((msgLen < 0) || (msgLen > MSG_MAX_LENGTH))
    {
        md.received = MSGID_ERROR_BAD_SIZE;
        return md;
    }

    Claw::NarrowString msgRaw = msg.substr(8, msgLen);
    //CLAW_MSG( "MSG_RAW: " << msgRaw.ClawClaw::NarrowString() );
    if (msgRaw.size() > 0)
    {
        Claw::NarrowString msgContent = msgRaw.substr(8, msgRaw.size());//"UqDaeJWWYP3csDBIy00gmQ..";//
        Claw::NarrowString encoded = decodeData(base64_decode(msgContent),DEFAULT_ENCRYPT_KEY);

        md.content = blowfishDecode(encoded);
        //md.content = msgContent);
    }
    else
        md.content = Claw::NarrowString("");

    msgTmp = msg.substr(0, 3);
    md.received = atoi( msgTmp.c_str() );

    return md;
}


int DatabaseConnection::isMessageReceived()
{
    //return isMessageReceived(MSGID_DUMMY);
    if( m_httpRequest )
    {
        if( m_vectorData.empty() && m_httpRequest->GetState() == Claw::HttpRequest::S_DONE )
        {
            for( int i = 0; i < m_httpRequest->GetLength(); ++i )
            {
                m_vectorData.push_back( m_httpRequest->GetData()[i] );
            }
            m_receivedId = parseDataReceived( m_vectorData );
            return m_receivedId;
        }else
        {
            return MSGID_NONE;
        }
    }
    return MSGID_NONE;
}

bool DatabaseConnection::isMessageReceived(int msgID)
{
    if( m_httpRequest )
    {
        if( m_vectorData.empty() && m_httpRequest->GetState() == Claw::HttpRequest::S_DONE )
        {
            for( int i = 0; i < m_httpRequest->GetLength(); ++i )
            {
                m_vectorData.push_back( m_httpRequest->GetData()[i] );
            }
            m_receivedId = parseDataReceived( m_vectorData );
            return ( m_receivedId == msgID );
        }else
        {
            return ( m_receivedId == msgID );
        }
    }
    return false;
}


bool DatabaseConnection::transferSuccesful()
{
    bool check = true;
    for (int i = 0; i < m_messages.size(); i++)
        check = check && m_messages[i].expected == m_messages[i].received;
    return check;
}


bool DatabaseConnection::transferSuccesful(int msgID)
{
    if (msgID >= m_messages.size())
        return false;

    return m_messages[msgID].received == m_messages[msgID].expected;
}


Claw::NarrowString DatabaseConnection::constructMultipartMsg(std::vector< MessageData > msgVector)
{
    Claw::NarrowString msg = "";
    std::vector< Claw::NarrowString > offsetVector;
    int offset = 0;
    for (int i = 0; i < msgVector.size(); i++)
    {
        char tmp[64];
        sprintf( tmp, "%d",offset);
        offsetVector.push_back( Claw::NarrowString( tmp ) );
        Claw::NarrowString part = msgVector[i].content;
        offset += part.size();
        msg += part;
    }

    return constructMsg(MSGID_MULTIPART_MESSAGE, offsetVector) + msg;
}

Claw::NarrowString DatabaseConnection::constructMsg(int msgID, std::vector< Claw::NarrowString > elements)
{
    Claw::NarrowString tmpbuf = Claw::NarrowString();
    Claw::NarrowString buf = Claw::NarrowString();
    buf += parseInt(msgID, 3);

    int size = elements.size();

    if (!elements.empty() && size > 0)
    {
        Claw::NarrowString enc = Claw::NarrowString();

        for (int i = 0; i < size; i++)
        {
            enc += elements[i];
            if (i < (size - 1))
                enc += '|';
        }

        int size = enc.length();
        if (size%8 != 0){
            do {
                enc = enc + Claw::NarrowString(" ");
                size = enc.length();
            } while (enc.size() %8 != 0);
        }

        std::vector< char > tmp = toCharVector(enc);

        int crcRaw = (getCRC(tmp, 0, tmp.size()) & 0x0000ffff);

        // encode the Claw::NarrowString
        //enc = "ALA MA KOTA     ";
        enc = blowfishEncode(enc);        
        enc = base64_encode(encodeData(enc, DEFAULT_ENCRYPT_KEY));  
        std::vector<char> teges;
        std::vector<char> teges_enc;
        for (int n=0; n<enc.size();++n){
            teges_enc.push_back(enc[n]);
        }
        teges = base64_decode(decodeData(teges_enc, DEFAULT_ENCRYPT_KEY));
        tmp.clear();
        tmp = toCharVector(enc);
        int crcEnc = getCRC(tmp, 0, tmp.size()) & 0x0000ffff;

        tmpbuf += int2Hex(crcRaw);
        tmpbuf += int2Hex(crcEnc);
        tmpbuf += enc;
    }

    int msgLen = tmpbuf.size();
    buf += parseInt(msgLen, 5);
    buf += tmpbuf;

    return buf;
}


int DatabaseConnection::getCRC( std::vector< char > buf, int offset, int len )
{
    if (m_crcTable.empty())
        makeCRCTable();

    unsigned int crc = 0xffffffff; // init to all 1's
    // dunno why this !works
    // while (len-- >= 0) // run thru specified bytes in buf
    // crc = CRCTable[(crc ^ buf[offset++]) & 0xff] ^ (crc >>> 8);

    for (int n = offset; n < offset + len; n++)
        crc = m_crcTable[(crc ^ buf[n]) & 0xff] ^ (crc >> 8);

    return crc ^ 0xffffffff; // send the 1's complement of the final
}


Claw::NarrowString DatabaseConnection::int2Hex(int i)
{
    Claw::NarrowString buf = Claw::NarrowString();

    char buffer[10];
    int hexLength = sprintf(buffer, "%x", i);
    int cnt = 4 - hexLength;
    for (int a = 0; a < cnt; a++)
        buf += L'0';

    for (int i = 0; i < hexLength; i++)
        buf += buffer[i]; 

    return buf;
}


Claw::NarrowString DatabaseConnection::parseInt(int value, int digits)
{
    Claw::NarrowString s = Claw::NarrowString();
    for (int i = 0; i < digits; i++)
        s += '0';

    char tmp[64];
    sprintf( tmp, "%d", abs(value) );

    Claw::NarrowString str = tmp;
    int strLength = str.length();

    if (strLength <= digits)
    {
        for (int i = 0; i < strLength; i++)
            s[digits - strLength + i] = str[i];
        if (value < 0)
            s[0] = '-';
    }

    return s;
}


std::vector< char > DatabaseConnection::encodeData(Claw::NarrowString src, int baseKey)
{
    Claw::NarrowString tmpSrc = src;

    int key = baseKey;
    int size = tmpSrc.length();

    std::vector< char > buf = std::vector< char >( size );

    for (int i = 0; i < size; i++)
    {
        buf[i] = tmpSrc[i];// ^ key;
        //key = tmpSrc[i];
    }

    return buf;
}


Claw::NarrowString DatabaseConnection::blowfishEncode(Claw::NarrowString enc){

    //enc = "1400955454      ";

    const char* Key = "zxcdsaqwertfgvbnhyuj143567fgrs987bm089o5567g456781234QSD";
    //const char* Key = "2eSt2yUtru53yusebrepruspAswU6uwAr4phEhAtutramu23s98Pa73e";
    // char* buff = new char[16];// = "ALA MA KOTA";
    // buff = "ALA MA KOTA     ";
    //char* tmpBuf = new char[16];
    // char* tmpBuf1 = new char[16];
    char* tmpBuf = new char[enc.size()];
    Claw::RawCrypto* crypto = new Claw::RawCrypto( );
    crypto->SetKey(Key);
    crypto->Encrypt(enc.c_str()/* buff*/, /*16*/enc.size(), tmpBuf);  

    Claw::NarrowString output="";

    for (int n=0; n<enc.size(); ++n){
        output += (unsigned)tmpBuf[n];
    }

    //CLAW_MSG("---------------");
    //CLAW_MSG("---------------");
    //CLAW_MSG("---------------");
    //CLAW_MSG("encrypted buff is");
    //for (int n=0; n<output.size(); ++n){
    //    CLAW_MSG("char "<< n << ": " << (int)output[n]);
    //}

    //crypto->Decrypt(tmpBuf, 16, tmpBuf1);  

    //for (int n=0; n<16; ++n){
    //    output += tmpBuf[n];
    //}
    //CLAW_MSG("decrypted buff is");
    //for (int n=0; n<16; ++n){
    //    CLAW_MSG("char "<< n << ": " << (int)tmpBuf1[n]);
    //}
    return output;
}

Claw::NarrowString DatabaseConnection::blowfishDecode(Claw::NarrowString enc){

    const char* Key = "zxcdsaqwertfgvbnhyuj143567fgrs987bm089o5567g456781234QSD";
    int size = enc.length();
    if (size%8 != 0){
      CLAW_MSG("BLOWFISH decode : incorrect size");
    }

    char* tmpBuf = new char[size];
    Claw::RawCrypto* crypto = new Claw::RawCrypto( );
    crypto->SetKey(Key);
    crypto->Decrypt(enc.c_str(), size, tmpBuf);  

    Claw::NarrowString output="";

    for (int n=0; n<size; ++n){
        if (n > size-8 ){
            if (tmpBuf[n] != ' '){
                output += tmpBuf[n];
            }       
        } else {
            output += tmpBuf[n];
        }
    }

    return output;
}

Claw::NarrowString DatabaseConnection::base64_encode( std::vector< char > data )
{
    int c;
    Claw::NarrowString ret = Claw::NarrowString();

    for (int i = 0; i < data.size(); i++)
    {
        c = (data[i] >> 2) & 0x3f;
        ret += ALPHABET64[c];
        c = (data[i] << 4) & 0x3f;
        if (++i < data.size())
            c = c | (data[i] >> 4) & 0x0f;

        ret += ALPHABET64[c];
        if (i < data.size())
        {
            c = (data[i] << 2) & 0x3f;
            if (++i < data.size())
                c = c | (data[i] >> 6) & 0x03;

            ret += ALPHABET64[c];
        }
        else
        {
            ++i;
            ret += (char) FILL_CHAR;
        }

        if (i < data.size())
        {
            c = data[i] & 0x3f;
            ret += ALPHABET64[c];
        }
        else
            ret += (char) FILL_CHAR;
    }

    return ret;
}

std::vector< char > DatabaseConnection::base64_decode(Claw::NarrowString txt)
{
    int c;
    int c1;
    std::vector< char > data = toCharVector(txt);
    std::vector< char > ret = std::vector< char >(data.size());
    int retCount = 0;

    for (int i = 0; i < data.size(); i++)
    {
        c = ALPHABET64.find(data[i]);
        ++i;
        c1 = ALPHABET64.find(data[i]);
        c = ((c << 2) | ((c1 >> 4) & 0x3));
        ret[retCount++] = ((char)(c & 0xff));
        //ret.append((char) c);
        if (++i < data.size())
        {
            c = data[i];
            if (FILL_CHAR == c)
                break;

            c = ALPHABET64.find((char) c);
            c1 = ((c1 << 4) & 0xf0) | ((c >> 2) & 0xf);
            ret[retCount++] = ((char)(c1 & 0xff));
        }

        if (++i < data.size())
        {
            c1 = data[i];
            if (FILL_CHAR == c1)
                break;

            c1 = ALPHABET64.find((char) c1);
            c = ((c << 6) & 0xc0) | c1;
            ret[retCount++] = ((char)(c & 0xff));
        }
    }

    // return only translated amount of bytes
    ret.resize(retCount);

    //Claw::NarrowString msgAfterBase64decode = decodeData( ret, DEFAULT_ENCRYPT_KEY );
    //CLAW_MSG( "MSG_AFTER_BASE64_DECODE: " << msgAfterBase64decode );

    return ret;
}


Claw::NarrowString DatabaseConnection::decodeData( std::vector< char > src, int baseKey )
{
    int key = baseKey;
    int size = src.size();
    Claw::NarrowString buf = Claw::NarrowString();

    for (int i = 0; i < size; i++)
    {
        buf.push_back( (char)( /*(*/ src[i] /*^ key ) & 0xff*/ ) );
        //key = ( src[i] & 0xff ) ^ key;
    }

    //CLAW_MSG( "MSG_DECODE_DATA: " << buf );

    return Claw::NarrowString( buf );
}


void DatabaseConnection::makeCRCTable()
{
    unsigned int c, n, k;
    m_crcTable = std::vector< int >(CRC_TABLE_LENGTH);

    for (n = 0; n < CRC_TABLE_LENGTH; n++)
    {
        c = n;
        for (k = 0; k < 8; k++)
        {
            if ((c & 1) == 1)
                c = 0xedb88320 ^ (c >> 1);
            else
                c = c >> 1;
        }
        m_crcTable[n] = c;
    }
}


Claw::NarrowString DatabaseConnection::getResponse()
{
    for (int i = 0; i < m_messages.size(); i++)
        if (m_messages[i].expected != m_messages[i].received)
            return getResponse(i);

    return Claw::NarrowString("UNNOWN");
}

Claw::NarrowString DatabaseConnection::getResponse(int msgID)
{
    return getResponse(msgID, 0);
}

Claw::NarrowString DatabaseConnection::getResponse(int msgID, int state)
{
            if (msgID >= m_messages.size())
                return Claw::NarrowString("UNNOWN");

            switch (m_messages[msgID].received)
            {
                case MSGID_ERROR_WRONG_PARAMETERS:
                    return Claw::NarrowString("INVALID_MSG");
                case MSGID_ERROR_MALFORMED_MSG:
                case MSGID_ERROR_CHECKSUM_FAILED:
                    return Claw::NarrowString("CORRUPTED_MSG");
                case MSGID_ERROR_NICK_ALREADY_EXISTS:
                    return Claw::NarrowString("CON_ACCOUNT_EXIST");
                case MSGID_ERROR_INVALID_ACCOUNT_DATA:
                    return Claw::NarrowString("INVALID_DATA");
                case MSGID_ERROR_AUTH_FAILED:
                    return Claw::NarrowString("CON_AUTH_FAILED");
//                case MSGID_ERROR_EXPLICIT_PHRASE:
//                    return Claw::NarrowString("CON_EXPLICIT");
                case MSGID_OK:
                    if (state == 0)
                        return Claw::NarrowString("IDS_CREATE_SUCCESSFUL");
      
                    return Claw::NarrowString("DB_CON_TRANS_SUCCESFUL");
                case -100:
                    return Claw::NarrowString("DB_CON_TIMEOUT");
                default:
                    return Claw::NarrowString("DB_CON_UNKNOWN");
            }
}

Claw::NarrowString DatabaseConnection::getContent()
{
    return (Claw::NarrowString)(m_messages[0].content);
}

std::vector< Claw::NarrowString > DatabaseConnection::getContent(int msgID)
{
    return splitString(m_messages[msgID].content);
}
MessageData DatabaseConnection::getMessage(int index){
    return m_messages[index];
}

void DatabaseConnection::delFirstMessage(){
    m_messages.erase(m_messages.begin());
}

int DatabaseConnection::getMessagesCount(){

    return m_messages.size();}

std::vector< char > toCharVector(Claw::NarrowString str)
{
    Claw::NarrowString tmpStr = str;
    std::vector< char > tmp = std::vector< char >(tmpStr.size());
    for (int i = 0; i < tmpStr.size(); i++)
        tmp[i] = tmpStr[i];
    return tmp;
}


std::vector< Claw::NarrowString > DatabaseConnection::splitString( Claw::NarrowString& str )
{
    //CLAW_MSG( "SplitClaw::NarrowString: " << str.NarrowClaw::NarrowString() );
    std::vector< Claw::NarrowString > v;
    char separator = '|';
    v.clear();

    Claw::NarrowString::const_iterator s = str.begin();

    while (true)
    {
        Claw::NarrowString::const_iterator begin = s;
        bool isSeparator = false;
        bool isEnd = false;
        while ( !isSeparator && !isEnd )
        {
            if( s == str.end() )
                isEnd = true;
            if( !isEnd )
                if( ( *s ) == separator )
                    isSeparator = true;

            if( !isSeparator && !isEnd)
                ++s;
        }
        v.push_back( Claw::NarrowString( begin, s ) );

        if ( s == str.end() )
            break;
        if( ++s == str.end() )
        {
            v.push_back( Claw::NarrowString( "" ) );
            break;
        }
    }
    return v;
}

Claw::NarrowString narrow(const Claw::String& str)
{
    std::ostringstream stm;
    //const ctype<char>& ctfacet = use_facet<ctype<char> >(stm.getloc());
    //for (size_t i = 0; i < str.size(); ++i) 
    //    stm << ctfacet.narrow(str[i], 0);
    return Claw::NarrowString( stm.str() );
}
    const int DatabaseConnection::DEFAULT_ENCRYPT_KEY    = 666;
    const int DatabaseConnection::CRC_TABLE_LENGTH       = 256;
    const Claw::NarrowString DatabaseConnection::ALPHABET64          = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
    
    //default url init. only for debug mode
    //in release mode Look in Database contructor - for each platform different url is set from app config

    Claw::NarrowString DatabaseConnection::NETWORK_DATABASE_URL="http://ms2.glsliveservices.com/engineCN.php";


    const Claw::NarrowString DatabaseConnection::NETWORK_PHP_VARIABLE_GET= "?msg=";
    const Claw::NarrowString DatabaseConnection::NETWORK_PHP_VARIABLE_POST= "msg=";
    const int DatabaseConnection::FILL_CHAR              = '.';

#include "MonstazAI/db/UserDataDb.hpp"
#include <stdio.h>


UserDataDb::UserDataDb()
{
    m_userUniqueID = "0";
    m_newFriendsCount = 0;
}

UserDataDb::~UserDataDb()
{
}

void UserDataDb::SetVersionDb( const char* version )
{
    m_userDbVersion = version;
}

void UserDataDb::SetUserID( const char* userID )
{
    m_userUniqueID = userID;
}

const char* UserDataDb::GetUserID() const
{
    return m_userUniqueID;
}

const char* UserDataDb::GetVersionDb() const
{
    return m_userDbVersion;
}

void UserDataDb::AddFriend( const char* userID,const char* name,int userLevel, const char* fbID , const char* canHelpTime )
{
    m_friendsInfos.push_back( new FriendInfo (userID, name, userLevel , fbID ,  "smg" , "0" , 0 , 0 ,"0" , canHelpTime , "0" )  );
}

void UserDataDb::AddFriend( FriendInfo* friendInfo )
{
    m_friendsInfos.push_back( friendInfo );
}

void UserDataDb::RemoveFriend( const char* userID )
{
    for( std::vector<FriendInfo*>::iterator it = m_friendsInfos.begin(); it != m_friendsInfos.end(); ++it )
    {
        if ((*it)->uniqueId == userID )
        {
            delete *it;  
            m_friendsInfos.erase(it);
            break;
        }
    }
}

void UserDataDb::FlushFriends()
{
    m_friendsInfos.clear();
}

std::vector<UserDataDb::FriendInfo*> UserDataDb::GetFriendsList()
{
    return m_friendsInfos;
}

std::vector<UserDataDb::InviteInfo*> UserDataDb::GetInvitesList()
{
    return m_friendsInvitations;
}

void UserDataDb::FlushInvite()
{
    m_friendsInvitations.clear();
}

void UserDataDb::AddInvite( InviteInfo* inviteInfo )
{
    m_friendsInvitations.push_back( inviteInfo );
}

void UserDataDb::SetInviteAsCompleted( const char* userID , bool accepted )
{
    for( std::vector<InviteInfo*>::iterator it = m_friendsInvitations.begin(); it != m_friendsInvitations.end(); ++it )
    {
        if ((*it)->uniqueId == userID )
        {
            //set invite as completed and add to friend list
            if ( accepted )
            {
                AddFriend( new UserDataDb::FriendInfo( (*it)->uniqueId.c_str() ,(*it)->name.c_str() ,(*it)->userLevel, (*it)->facebookId.c_str(),
                  (*it)->bestWeaponId ,  (*it)->bestWeaponUpgrade ,  (*it)->kills , (*it)->eventsCompleted ,  (*it)->canSendGift , (*it)->canHelp  ,(*it)->vipstatus.c_str() ) );
            }
            (*it)->completed = true;
            break;
        }
    }
}

void UserDataDb::AddDailyQuest( DailyQuestData dqd )
{
}

std::vector<UserDataDb::DailyQuestData> UserDataDb::GetDailyQuestsList()
{
    return m_dailyQuestList;
}

void UserDataDb::FlushDailyQuestList()
{
    m_dailyQuestList.clear();
}

void UserDataDb::AddGiftData ( GiftData* giftData )
{
    m_giftsList.push_back( giftData );
}

std::vector<UserDataDb::GiftData*>  UserDataDb::GetGiftDataList()
{
    return m_giftsList;
}

void UserDataDb::SetGiftAsUsed( const char* giftUniqueId )
{
    for( std::vector<GiftData*>::iterator it = m_giftsList.begin(); it != m_giftsList.end(); ++it )
    {
        if ((*it)->giftUniqueId == giftUniqueId )
        {
            (*it)->used = true;
            break;
        }
    }
}

void UserDataDb::FlushGiftDataList()
{
    m_giftsList.clear();
}

UserDataDb::FriendInfo* UserDataDb::GetUserInfo( const char* nickname )
{
    if (m_friendsInfos.size() > 0)
    {
        for (int n = 0; n < m_friendsInfos.size() ; n++ )
        {
            if (m_friendsInfos.at(n)->name == nickname)
            {
                return m_friendsInfos.at(n);
            }
        }
    }

    return NULL;
}


UserDataDb::FriendInfo* UserDataDb::GetUserInfoByUniqueId( const char* uniqueId )
{
    if (m_friendsInfos.size() > 0)
    {
        for (int n = 0; n < m_friendsInfos.size() ; n++ )
        {
            if (m_friendsInfos.at(n)->uniqueId == uniqueId)
            {
                return m_friendsInfos.at(n);
            }
        }
    }

    return NULL;
}

void UserDataDb::UpdateFriendInfoList( FriendInfo* friendInfo )
{
    if ( m_friendsInfos.size() > 0 )
    {
        for ( int n = 0; n < m_friendsInfos.size() ; n++ )
        {
            if (m_friendsInfos.at(n)->uniqueId == friendInfo->uniqueId)
            {
                m_friendsInfos.at(n) = friendInfo;
                return;
            }
        }
    }

    m_friendsInfos.push_back(friendInfo);
}

void UserDataDb::UpdateFriendInvitationsList( InviteInfo* inviteInfo )
{
    if ( m_friendsInvitations.size() > 0 )
    {
        for ( int n = 0; n < m_friendsInvitations.size() ; n++ )
        {
            if (m_friendsInvitations.at(n)->name == inviteInfo->name)
            {
                m_friendsInvitations.at(n) = inviteInfo;
                return;
            }
        }
    }

    m_friendsInvitations.push_back(inviteInfo);
}

int UserDataDb::GetNewFriendCount() const
{
    return m_newFriendsCount;
}

void UserDataDb::SetNewFriendsCount( int newFriendsNumber )
{
    m_newFriendsCount = newFriendsNumber;
}

void UserDataDb::AddLeaderboardInfo( LeaderboardInfo* info )
{
    m_leaderboardInfos.push_back( info );
}

void UserDataDb::FlushLeaderboard()
{
    m_leaderboardInfos.clear();
}

std::vector<UserDataDb::LeaderboardInfo*> UserDataDb::GetLeaderboardList()
{
    return m_leaderboardInfos;
}
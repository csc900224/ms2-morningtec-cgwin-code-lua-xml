#ifndef __MONSTAZ_USER_DATA_DB_HPP__
#define __MONSTAZ_USER_DATA_DB_HPP__

#include <vector>
#include "claw/base/RefCounter.hpp"
#include "claw/base/SmartPtr.hpp"

class UserDataDb : public Claw::RefCounter 
{
public:

    struct DailyQuestData
    {
        DailyQuestData( Claw::NarrowString _id, Claw::NarrowString _title, Claw::NarrowString _description) : id( _id ), title( _title ), description( _description ) {}
        Claw::NarrowString id;
        Claw::NarrowString title;
        Claw::NarrowString description;
    };

    struct LeaderboardInfo
    {
        LeaderboardInfo( Claw::NarrowString _place, Claw::NarrowString _name , Claw::NarrowString _score , bool _isGlobal ) : place( _place ), name( _name ), score( _score ), isGlobal( _isGlobal ) {}
        Claw::NarrowString place;
        Claw::NarrowString name;
        Claw::NarrowString score;
        bool isGlobal;
    };

    struct GiftData
    {
        GiftData( Claw::NarrowString _givenBy,
            Claw::NarrowString _name,
            Claw::NarrowString _fbId ,
            Claw::NarrowString _level,
            Claw::NarrowString _itemId,
            Claw::NarrowString _qty,
            Claw::NarrowString _giftId,
            Claw::NarrowString _age,
            bool _used,
            Claw::NarrowString _vipstatus
            ) :
            givenBy( _givenBy ),
            name( _name ),
            fbId( _fbId ),
            level( _level ) ,
            itemId( _itemId ),
            qty( _qty ),
            giftUniqueId( _giftId ),
            age( _age ),
            used( _used ),
            vipstatus( _vipstatus )
        {}

        Claw::NarrowString givenBy;
        Claw::NarrowString name;
        Claw::NarrowString fbId;
        Claw::NarrowString level;
        Claw::NarrowString itemId;
        Claw::NarrowString qty;
        Claw::NarrowString giftUniqueId;
        Claw::NarrowString age;
        bool used;
        Claw::NarrowString vipstatus;
    };

    struct FriendInfo
    {
        FriendInfo(  Claw::NarrowString _uniqueId,
            Claw::NarrowString _name ,
            int _userLevel,
            Claw::NarrowString _facebookId,
            Claw::NarrowString _bestWeaponId,
            Claw::NarrowString _bestWeaponLevel,
            int _kills,
            int _completedEvents,
            Claw::NarrowString _canSendGift,
            Claw::NarrowString _canHelpTime,
            Claw::NarrowString _vipstatus) :
            uniqueId( _uniqueId ) ,
            name( _name ) ,
            userLevel( _userLevel ) ,
            facebookId ( _facebookId ) ,
            bestWeaponId( _bestWeaponId ) ,
            bestWeaponUpgrade( _bestWeaponLevel ) ,
            kills( _kills ),
            eventsCompleted ( _completedEvents ),
            canSendGift ( _canSendGift ),
            canHelp ( _canHelpTime ),
            vipstatus( _vipstatus )
        {
        }
        Claw::NarrowString uniqueId;
        Claw::NarrowString name;
        int userLevel;
        Claw::NarrowString facebookId;
        int kills;
        int eventsCompleted;
        Claw::NarrowString bestWeaponId;
        Claw::NarrowString bestWeaponUpgrade;
        Claw::NarrowString canHelp;
        Claw::NarrowString canSendGift;
        Claw::NarrowString vipstatus;
    };

    struct InviteInfo
    {
        InviteInfo( Claw::NarrowString _uniqueId,
            Claw::NarrowString _name ,
            int _userLevel,
            Claw::NarrowString _facebookId,
            Claw::NarrowString _bestWeaponId,
            Claw::NarrowString _bestWeaponLevel,
            int _kills,
            int _completedEvents,
            Claw::NarrowString _canSendGift,
            Claw::NarrowString _canHelpTime,
            bool _completed,
            Claw::NarrowString _age,
            Claw::NarrowString _vipstatus
            ) :
            uniqueId( _uniqueId ) ,
            name( _name ) ,
            userLevel( _userLevel ) ,
            facebookId ( _facebookId ) ,
            bestWeaponId( _bestWeaponId ) ,
            bestWeaponUpgrade( _bestWeaponLevel ) ,
            kills( _kills ),
            eventsCompleted ( _completedEvents ),
            canSendGift ( _canSendGift ),
            canHelp ( _canHelpTime ),
            completed( _completed ),
            age( _age ),
            vipstatus( _vipstatus )
        {
        }
        Claw::NarrowString uniqueId;
        Claw::NarrowString name;
        int userLevel;
        Claw::NarrowString facebookId;
        int kills;
        int eventsCompleted;
        Claw::NarrowString bestWeaponId;
        Claw::NarrowString bestWeaponUpgrade;
        Claw::NarrowString canHelp;
        Claw::NarrowString canSendGift;
        bool completed;
        Claw::NarrowString age;
        Claw::NarrowString vipstatus;
    };


    UserDataDb();
    ~UserDataDb();

    void                            SetVersionDb( const char* version );
    const char*                     GetVersionDb() const;

    void                            SetUserID( const char* userID );
    const char*                     GetUserID() const;

    void                            AddFriend( const char* userID,const char* name,int userLevel, const char* fbID , const char* canHelpTime );
    void                            AddFriend( FriendInfo* friendInfo );
    void                            RemoveFriend( const char* userID );
    void                            FlushFriends();
    std::vector<FriendInfo*>        GetFriendsList();
    void                            FlushInvite();
    void                            AddInvite( InviteInfo* inviteInfo );
    void                            SetInviteAsCompleted( const char* userID , bool accepted );
    std::vector<InviteInfo*>        GetInvitesList();

    
    int                             GetNewFriendCount() const;
    void                            SetNewFriendsCount( int newFriendsNumber );

    void                            AddDailyQuest( DailyQuestData dqd );
    std::vector<DailyQuestData>     GetDailyQuestsList();
    void                            FlushDailyQuestList();

    void                            AddGiftData ( GiftData* giftData );
    std::vector<GiftData*>          GetGiftDataList();
    void                            FlushGiftDataList();
    void                            SetGiftAsUsed( const char* giftUniqueId );

    FriendInfo*                     GetUserInfo( const char* nickname );
    FriendInfo*                     GetUserInfoByUniqueId( const char* uniqueId );
    void                            UpdateFriendInfoList( FriendInfo* friendInfo );

    void                            UpdateFriendInvitationsList( InviteInfo* inviteInfo );

    void                            AddLeaderboardInfo( LeaderboardInfo* info );
    void                            FlushLeaderboard();
    std::vector<LeaderboardInfo*>   GetLeaderboardList();

private:

    const char*                     m_userUniqueID;
    const char*                     m_userDbVersion;

    std::vector<DailyQuestData>     m_dailyQuestList;

    std::vector<GiftData*>          m_giftsList;

    std::vector<FriendInfo*>        m_friendsInfos;
    std::vector<InviteInfo*>        m_friendsInvitations;
    std::vector<LeaderboardInfo*>   m_leaderboardInfos;

    int                             m_newFriendsCount;

}; // class UserDataDb

typedef Claw::SmartPtr<UserDataDb> UserDataDbPtr;

#endif // !defined __MONSTAZ_USER_DATA_DB_HPP__
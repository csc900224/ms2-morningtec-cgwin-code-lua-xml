#include "claw/application/Time.hpp"
#include "claw/base/Registry.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/FuelRefill.hpp"

FuelRefill::FuelRefill()
    : m_full( GetFuel() >= GetMaxFuel() )
    , m_last( std::min<Claw::UInt32>( Claw::Registry::Get()->CheckInt( "/monstaz/cash/fueltime" ), Claw::Time::GetTime() ) )
{
    Claw::Registry::Get()->Set( "/monstaz/cash/fueltime", (int)m_last );
}

FuelRefill::~FuelRefill()
{
}

void FuelRefill::Tick()
{
    int max = GetMaxFuel();
    bool full = GetFuel() >= GetMaxFuel();
    if( !full )
    {
        Claw::UInt32 now = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime();
        if( now == 0 ) return;

        if( m_full )
        {
            m_last = now;
            Claw::Registry::Get()->Set( "/monstaz/cash/fueltime", (int)m_last );
            ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
        }
        else
        {
            int wait = Claw::Registry::Get()->CheckInt( "/app-config/fuel/regen" );
            while( m_last + wait < now )
            {
                m_last += wait;
                Claw::Registry::Get()->Set( "/monstaz/cash/fueltime", (int)m_last );
                int fuel = GetFuel();
                fuel++;
                SetFuel( fuel );
                ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
                if( fuel >= max )
                {
                    m_full = true;
                    return;
                }
            }
        }
    }
    m_full = full;
}

int FuelRefill::GetFuel() const
{
    return Claw::Registry::Get()->CheckInt( "/monstaz/cash/fuel" );
}

void FuelRefill::SetFuel( int val ) const
{
    Claw::Registry::Get()->Set( "/monstaz/cash/fuel", val );
}

int FuelRefill::GetMaxFuel() const
{
    int lvl = Claw::Registry::Get()->CheckInt( "/monstaz/cash/fuelmax" );
    char buf[32];
    strcpy( buf, "/app-config/fuel/0" );
    buf[17] = '0' + lvl;
    return Claw::Registry::Get()->CheckInt( buf );
}

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Vector.hpp"

#include "MonstazAI/DownloadScreen.hpp"
#include "MonstazAI/PakManager.hpp"

DownloadScreen::DownloadScreen()
    : m_bg( Claw::AssetDict::Get<Claw::Surface>( "plate/downloader.@linear" ) )
    , m_font( Claw::AssetDict::Get<Claw::FontEx>( "downloader/font_big.xml" ) )
    , m_progress( 0 )
{
}

DownloadScreen::~DownloadScreen()
{
}

void DownloadScreen::Render( Claw::Surface* target ) const
{
    Claw::UInt32 error = PakManager::GetInstance()->CheckError();

    target->Clear( !error ? Claw::MakeRGB( 233, 137, 37 ) : Claw::MakeRGB( 233, 37, 37 ) );

    const Claw::Vectorf BG_SIZE( 1920, 1280 );
    float w = target->GetWidth();
    float h = target->GetHeight();
    const float IMG_SCALE = std::max( target->GetHeight() / BG_SIZE.y, target->GetWidth() / BG_SIZE.x);
    const Claw::Vectorf drawScale( BG_SIZE.x / m_bg->GetWidth() * IMG_SCALE, BG_SIZE.y / m_bg->GetHeight() * IMG_SCALE );

    float offset = m_bg->GetWidth() * drawScale.x - w;

    float end = 0.5146f + m_progress * 0.3945f;

    target->DrawFilledRectangle( w * 0.5146f - offset, 0, w * end - offset, h, !error ? Claw::MakeRGB( 25, 233, 246 ) : Claw::MakeRGB( 246, 25, 25 ) );
    target->DrawFilledRectangle( w * end - offset, 0, w * ( end + 0.00878f + m_progress * 0.0083f ) - offset, h, !error ? Claw::MakeRGB( 9, 129, 137 ) : Claw::MakeRGB( 137, 9, 9 ) );

    Claw::TriangleEngine::Blit( target, m_bg, -offset, 0, 0, drawScale, Claw::Vectorf( 0, 0 ), Claw::TriangleEngine::FM_NONE );

    if( error != 0 )
    {
        if( error & PakManager::HardError )
        {
            Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
            fontSet->AddFont( "f", m_font );
            Claw::Text::Format format;
            format.SetFontSet( fontSet );
            format.SetFontId( "f" );
            format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );
            Claw::ScreenTextPtr text( new Claw::ScreenText( format, Claw::String( "Critital error.\nPlease contact support." ), Claw::Extent( w * 0.5f, 0 ) ) );
            text->Draw( target, w * 0.5f, h * 0.85f );
        }
        else if( error & PakManager::SoftError )
        {
            Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
            fontSet->AddFont( "f", m_font );
            Claw::Text::Format format;
            format.SetFontSet( fontSet );
            format.SetFontId( "f" );
            format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );
            Claw::ScreenTextPtr text( new Claw::ScreenText( format, Claw::String( "Connection error.\nCheck your network settings." ), Claw::Extent( w * 0.5f, 0 ) ) );
            text->Draw( target, w * 0.5f, h * 0.85f );
        }
        else if( error & PakManager::DiskError )
        {
            Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
            fontSet->AddFont( "f", m_font );
            Claw::Text::Format format;
            format.SetFontSet( fontSet );
            format.SetFontId( "f" );
            format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );
            Claw::ScreenTextPtr text( new Claw::ScreenText( format, Claw::String( "No space left. Free disk space and restart." ), Claw::Extent( w * 0.5f, 0 ) ) );
            text->Draw( target, w * 0.5f, h * 0.85f );
        }
    }
}

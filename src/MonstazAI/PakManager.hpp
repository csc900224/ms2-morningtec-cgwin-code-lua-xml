#ifndef __MONSTAZ__PAKMANAGER_HPP__
#define __MONSTAZ__PAKMANAGER_HPP__

#include <functional>
#include <vector>

#include "claw/base/Mutex.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/network/PakDownloader.hpp"

class PakManager : public Claw::RefCounter
{
public:
    enum Set
    {
        SetFirstTutorial    = 1 << 0,
        SetTutorialMenu     = 1 << 1,
        SetBoss1            = 1 << 2,
        SetBoss2            = 1 << 3,
        SetBoss3            = 1 << 4,
        SetEnemy1           = 1 << 5,
        SetEnemy2           = 1 << 6,
        SetEnemy3           = 1 << 7,
        SetEnemy4           = 1 << 8,
        SetEnemy5           = 1 << 9,
        SetEnemy6           = 1 << 10,
        SetEnemy7           = 1 << 11,
        SetEnemy8           = 1 << 12,
        SetEnviroIceDesert  = 1 << 13,
        SetPlayerA          = 1 << 14,
        SetPlayerB          = 1 << 15,
        SetPlayerC          = 1 << 16,
        SetPlayerD          = 1 << 17,
        SetPlayerE          = 1 << 18,
        SetPlayerMech       = 1 << 19
    };

    enum Error
    {
        SoftError   = 1 << 0,
        HardError   = 1 << 1,
        DiskError   = 1 << 2
    };

    PakManager();
    ~PakManager();

    void Require( Claw::UInt32 set );
    void Download( const Claw::Uri& uri, const char* dst, Claw::UInt32 set = 0, std::function<void()> f = std::function<void()>() );

    void Reset( Claw::UInt32 set = -1 );
    bool CheckDone( Claw::UInt32 set = -1 );
    Claw::UInt32 CheckError( Claw::UInt32 set = -1 );
    float GetProgress( Claw::UInt32 set = -1 );

    static PakManager* GetInstance() { return s_instance; }

private:
    struct Task
    {
        Claw::PakDownloaderPtr dl;
        std::function<void()> f;
        Claw::UInt32 s;
    };

    const char* m_res;
    std::vector<Task> m_dl;
    Claw::Mutex m_lock;
    Claw::UInt32 m_required;

    static PakManager* s_instance;
};

typedef Claw::SmartPtr<PakManager> PakManagerPtr;

#endif

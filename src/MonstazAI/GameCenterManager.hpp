#ifndef __MONSTAZ_GAME_CENTER_MANAGER_HPP__
#define __MONSTAZ_GAME_CENTER_MANAGER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include "MonstazAI/network/gamecenter/GameCenter.hpp"

class GameCenterManager : public GameCenter::Observer
{
public:
    enum ScoreCategory
    {
        SC_MOON = 0,
        SC_MARS,
        SC_SPACESHIP,
        SC_ICE,
        SC_ALL
    }; // enum ScoreCategory

    typedef GameCenterManager self;

                    ~GameCenterManager();

    static self*    GetInstance();

    static void     Release();

    bool            Authenticate();

    void            SubmitAllAchievements();

    bool            SubmitScore( ScoreCategory sc, int score );

    bool            ShowLeaderboard(ScoreCategory sc );

    GameCenter::Type GetDefaultGameCenter() const;

    void            SetDefualtGameCenter( GameCenter::Type type );

    // GameCenter::Observer interface
    virtual void    OnAchievementsLoad( const GameCenter::Achievements& achievements );
    virtual void    OnAuthenticationChange( bool authenticated );
    virtual void    OnLeaderboardView( bool opened );
    virtual void    OnAchievementsView( bool opened );
    
    // Lua binding
                    LUA_DEFINITION( GameCenterManager );

                    GameCenterManager( lua_State* L );
    void            Init( Claw::Lua* lua );

    int             l_SubmitScore( lua_State* L );
    int             l_SubmitScoreKiip( lua_State* L );
    int             l_ShowLeaderboard( lua_State* L );

    int             l_SetDefaultGameCenter( lua_State* L );
    int             l_GetDefaultGameCenter( lua_State* L );

    int             l_Achievement( lua_State* L );
    int             l_ShowAchievements( lua_State* L );
    int             l_SubmitAllAchievements( lua_State* L );

private:
                    GameCenterManager();

    GameCenter*     m_gameCenter;
    GameCenter*     m_openFeint;
    bool            m_authenticationTry;

    static self*    s_instance;

}; // class GameCenterManager

#endif // !defined __MONSTAZ_GAME_CENTER_MANAGER_HPP__
// EOF

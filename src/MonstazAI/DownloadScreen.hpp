#ifndef __MONSTAZ__DOWNLOADSCREEN_HPP__
#define __MONSTAZ__DOWNLOADSCREEN_HPP__

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/FontEx.hpp"

class DownloadScreen : public Claw::RefCounter
{
public:
    DownloadScreen();
    ~DownloadScreen();

    void Render( Claw::Surface* target ) const;
    void SetProgress( float progress ) { m_progress = progress; }

private:
    Claw::SurfacePtr m_bg;
    Claw::FontExPtr m_font;
    float m_progress;
};

typedef Claw::SmartPtr<DownloadScreen> DownloadScreenPtr;

#endif

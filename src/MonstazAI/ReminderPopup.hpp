#ifndef __MONSTAZ_REMINDER_POPUP_HPP__
#define __MONSTAZ_REMINDER_POPUP_HPP__

class ReminderPopup
{
public:
    static void Initialize();
    static void Release();

    static void Show();

private:
    ReminderPopup() {}

}; // class GameCenterPopup

#endif // __MONSTAZ_GAME_CENTER_POPUP_HPP__

#ifndef __INCLUDED__MISSIONS__NOTIFIER_HPP__
#define __INCLUDED__MISSIONS__NOTIFIER_HPP__

#include "MonstazAI/missions/Mission.hpp"

#include "claw/graphics/Surface.hpp"
#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "guif/Screen.hpp"

#include <deque>

namespace Missions
{
    class Notifier : public Mission::Observer
    {
    public:
        LUA_DEFINITION( Notifier );

        Notifier();

        void InitLua( Claw::Lua* lua );
        void Update( float dt, Claw::Lua* lua );
        void Render( Claw::Surface* target );

        void SetActive( bool active ) { m_active = active; }

        void OnMissionProgressChanged( const Mission* mission );
        void OnMissionCompleted( const Mission* mission );
        void OnMissionRewarded( const Mission* mission );
        void OnMissionActivated( const Mission* mission ) {}

        int l_OnNotificationFinished( lua_State* L );

    private:
        typedef std::deque<Mission::Id> NotificationQueue;

        void PopNotification( Claw::Lua* lua );

        NotificationQueue m_notificationQueue;
        bool m_fetchNotification;
        bool m_active;
    };
}

#endif
#ifndef __INCLUDED__MISSIONS__SEQUENCEGROUP_HPP__
#define __INCLUDED__MISSIONS__SEQUENCEGROUP_HPP__

#include "MonstazAI/missions/MissionGroup.hpp"

namespace Missions
{
    class SequenceGroup : public MissionGroup
    {
    public:
        SequenceGroup( const Id& id );

        void SetActive( bool active );
        bool HandleGameEvent( const GameEvent& ev );
        void Validate();

        // Completable::Observer
        void OnCompleted( const Completable* mission );
    };
}

#endif
#include "MonstazAI/missions/Notifier.hpp"
#include "MonstazAI/Application.hpp"

#include "claw/base/Errors.hpp"

namespace Missions
{
    LUA_DECLARATION( Notifier )
    {
        METHOD( Notifier, OnNotificationFinished ),
        {0,0}
    };

    Notifier::Notifier()
        : m_fetchNotification( false )
        , m_active( false )
    {}

    void Notifier::InitLua( Claw::Lua* lua )
    {
        Claw::Lunar<Notifier>::Register( *lua );
        Claw::Lunar<Notifier>::push( *lua, this );
        lua->RegisterGlobal( "MissionNotifier" );
    }

    void Notifier::Update( float dt, Claw::Lua* lua )
    {
        if( m_active && m_fetchNotification )
        {
            PopNotification( lua );
            m_fetchNotification = false;
        }
    }

    void Notifier::OnMissionProgressChanged( const Mission* mission )
    {
    }

    void Notifier::OnMissionCompleted( const Mission* mission )
    {
        CLAW_MSG( "### Mission Completed: " << mission->GetId() );
        m_notificationQueue.push_back( mission->GetPath() );
        if( m_notificationQueue.size() == 1 )
        {
            m_fetchNotification = true;
        }
    }

    void Notifier::OnMissionRewarded( const Mission* mission )
    {
    }

    void Notifier::PopNotification( Claw::Lua* lua )
    {
        if( lua && !m_notificationQueue.empty() )
        {
            lua->PushString( m_notificationQueue.front() );
            lua->Call( "MissionNotifierMissionCompleted", 1, 0 );
        }
    }

    int Notifier::l_OnNotificationFinished( lua_State* L )
    {
        if( m_active && !m_notificationQueue.empty() )
        {
            m_notificationQueue.pop_front();
            m_fetchNotification = m_notificationQueue.size() > 0;
        }
        return 0;
    }
}
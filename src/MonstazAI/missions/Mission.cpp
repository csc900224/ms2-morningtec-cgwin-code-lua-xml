#include "MonstazAI/missions/Mission.hpp"
#include "MonstazAI/missions/Reward.hpp"

#include "claw/base/Errors.hpp"
#include "claw/base/Registry.hpp"

namespace Missions
{
    LUA_DECLARATION( Mission )
    {
        METHOD( Mission, GetProgress ),
        METHOD( Mission, GetTarget ),
        METHOD( Mission, GetValue ),
        METHOD( Mission, GetStatus ),
        METHOD( Mission, GiveRewards ),
        METHOD( Mission, GetId ),
        METHOD( Mission, GetPath ),
        {0,0}
    };

    const char* Mission::TYPE_NAME      = "Mission";

    static const char* OBJECTIVES_NODE  = "/objectives";
    static const char* STATUS_KEY       = "/status";

    Mission::Mission( const Id& id )
        : Completable( id )
        , m_autoRewards( false )
        , m_status( S_DISABLED )
    {}

    Mission::~Mission()
    {
        ObjectivesIt oIt = m_objectives.begin();
        ObjectivesIt oEnd = m_objectives.end();
        for( ; oIt != oEnd; ++oIt )
        {
            delete oIt->second;
        }
        m_objectives.clear();

        RewardsIt rIt = m_rewards.begin();
        RewardsIt rEnd = m_rewards.end();
        for( ; rIt != rEnd; ++rIt )
        {
            delete *rIt;
        }
        m_rewards.clear();
    }

    void Mission::InitLua( Claw::Lua* lua )
    {
        Claw::Lunar<Mission>::Register( *lua );

        lua->CreateEnumTable();
        lua->AddEnum( S_DISABLED );
        lua->AddEnum( S_ACTIVE );
        lua->AddEnum( S_COMPLETED );
        lua->AddEnum( S_REWARDED );
        lua->RegisterEnumTable( "MissionStatus" );
    }

    void Mission::SetActive( bool active )
    {
        if( active )
        {
            CLAW_MSG_ASSERT( m_status == S_DISABLED, "Only disabled mission can be activated" );
            if( m_status == S_DISABLED )
            {
                SetStatus( S_ACTIVE );
            }
        }
        else
        {
            if( m_status == S_ACTIVE )
            {
                SetStatus( S_DISABLED );
            }
        }
    }

    void Mission::Validate()
    {
        if( GetStatus() == S_ACTIVE && !m_objectives.empty() )
        {
            bool allCompleted = true;
            ObjectivesIt it = m_objectives.begin();
            ObjectivesIt end = m_objectives.end();
            for( ; it != end; ++it )
            {
                it->second->OnActivate();
                allCompleted &= it->second->IsCompleted();
            }

            if( allCompleted )
            {
                SetStatus( S_COMPLETED );
            }
        }
        else if( GetStatus() == S_COMPLETED && m_objectives.empty() )
        {
            // Fix to rollback corrupted player ranks
            SetStatus( S_ACTIVE );
        }
    }

    void Mission::SetStatus( Status newStatus )
    {
        if( newStatus != m_status )
        {
            m_status = newStatus;
            Claw::Registry::Get()->Set( m_statusRegPath.c_str(), m_status );

            if( newStatus == S_ACTIVE )
            {
                Validate();
                NotifyMissionActivated();
            }
            else if( newStatus == S_COMPLETED )
            {
                NotifyMissionCompleted();

                if( m_autoRewards )
                {
                    GiveRewards();
                }
            }
        }
    }

    float Mission::GetProgress() const
    {
        float totalProgress = 0;

        ObjectivesConstIt it = m_objectives.begin();
        ObjectivesConstIt end = m_objectives.end();
        for( ; it != end; ++it )
        {
            totalProgress += it->second->GetProgress();
        }
        
        if( m_objectives.size() > 1 )
        {
            return totalProgress / m_objectives.size();
        }
        return totalProgress;
    }

    float Mission::GetTarget() const
    {
        float totalTarget = 0;

        ObjectivesConstIt it = m_objectives.begin();
        ObjectivesConstIt end = m_objectives.end();
        for( ; it != end; ++it )
        {
            totalTarget += it->second->GetTarget();
        }
        return totalTarget;
    }

    float Mission::GetValue() const
    {
        float totalValue = 0;

        ObjectivesConstIt it = m_objectives.begin();
        ObjectivesConstIt end = m_objectives.end();
        for( ; it != end; ++it )
        {
            totalValue += it->second->GetValue();
        }
        return totalValue;
    }

    void Mission::Reset()
    {
        ObjectivesIt it = m_objectives.begin();
        ObjectivesIt end = m_objectives.end();
        for( ; it != end; ++it )
        {
            it->second->Reset();
        }
        SetStatus( S_DISABLED );
    }

    bool Mission::GiveRewards()
    {
        if( m_status == S_COMPLETED )
        {
            RewardsIt it = m_rewards.begin();
            RewardsIt end = m_rewards.end();
            for( ; it != end; ++it )
            {
                (*it)->Give();
            }

            SetStatus( S_REWARDED );
            NotifyMissionRewarded();

            // Completable interface
            NotfiyCompleted();
            return true;
        }
        return false;
    }

    void Mission::Update( float dt )
    {
        if( m_status == S_ACTIVE )
        {
            bool allCompleted = true;
            ObjectivesIt it = m_objectives.begin();
            ObjectivesIt end = m_objectives.end();
            for( ; it != end; ++it )
            {
                Missions::Objectives::Objective* obj = it->second;
                if( !obj->IsCompleted() )
                {
                    allCompleted = false;
                    obj->Update( dt );
                }
            }

            if( allCompleted && !m_objectives.empty() )
            {
                SetStatus( S_COMPLETED );
            }
        }
    }

    bool Mission::SetRegistryBranch( const Claw::NarrowString& branchPath )
    {
        if( Completable::SetRegistryBranch( branchPath) )
        {
            // Objectives
            m_objectivesRegPath = branchPath;
            m_objectivesRegPath.append( "/" );
            m_objectivesRegPath.append( GetId() );
            m_objectivesRegPath.append( OBJECTIVES_NODE );

            ObjectivesIt it = m_objectives.begin();
            ObjectivesIt end = m_objectives.end();
            for( ; it != end; ++it )
            {
                it->second->SetRegistryBranch( m_objectivesRegPath );
            }

            // Status
            m_statusRegPath = branchPath;
            m_statusRegPath.append( "/" );
            m_statusRegPath.append( GetId() );
            m_statusRegPath.append( STATUS_KEY );
            if( !Claw::Registry::Get()->Get( m_statusRegPath.c_str(), (int&)m_status ) )
            {
                Claw::Registry::Get()->Set( m_statusRegPath.c_str(), m_status );
            }
            return true;
        }
        return false;
    }

    void Mission::SetAutoRewarded( bool autoRewards )
    {
        if( m_autoRewards != autoRewards )
        {
            m_autoRewards = autoRewards;
            if( m_autoRewards )
            {
                GiveRewards();
            }
        }
    }

    void Mission::AddObjective( Missions::Objectives::Objective* newObjective )
    {
        ObjectivesResult result = m_objectives.insert( Objectives::value_type( newObjective->GetTypename(), newObjective ) );
        CLAW_MSG_WARNING( result.second, "Objective of the same type already added to this mission" );
        if( result.second )
        {
            newObjective->RegisterObserver( this );
        }
    }

    void Mission::AddReward( Missions::Rewards::Reward* newReward )
    {
        RewardsResult result = m_rewards.insert( newReward );
        CLAW_MSG_WARNING( result.second, "Rewards already added to this mission" );
    }

    void Mission::RegisterObserver( Observer* observer )
    {
        ObserversResult result = m_observers.insert( observer );
        CLAW_MSG_WARNING( result.second, "Objective already registred in this mission this mission" );
    }

    void Mission::UnregisterObserver( Observer* observer )
    {
        m_observers.erase( observer );
    }

    bool Mission::HandleGameEvent( const GameEvent& ev )
    {
        bool retValue = false;
        if( m_status == S_ACTIVE )
        {
            ObjectivesIt it = m_objectives.begin();
            ObjectivesIt end = m_objectives.end();
            for( ; it != end; ++it )
            {
                Missions::Objectives::Objective* obj = it->second;
                if( !obj->IsCompleted() )
                {
                    retValue |= obj->HandleGameEvent( ev );
                }
            }
        }
        return retValue;
    }

    void Mission::NotifyMissionProgressChanged()
    {
        NotifyObservers( &Observer::OnMissionProgressChanged );
    }

    void Mission::NotifyMissionCompleted()
    {
        NotifyObservers( &Observer::OnMissionCompleted );
    }

    void Mission::NotifyMissionRewarded()
    {
        NotifyObservers( &Observer::OnMissionRewarded );
    }

    void Mission::NotifyMissionActivated()
    {
        NotifyObservers( &Observer::OnMissionActivated );
    }

    void Mission::NotifyObservers( ObserverMethod method )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
        {
            ((*it)->*method)(this);
        }
    }

    void Mission::OnObjectiveProgressChanged( const Missions::Objectives::Objective* objective )
    {
        if( m_status == S_ACTIVE )
        {
            NotifyMissionProgressChanged();
        }
    }

    void Mission::OnObjectiveCompleted( const Missions::Objectives::Objective* objective )
    {
        CLAW_ASSERT( m_status == S_ACTIVE )
        if( m_status == S_ACTIVE )
        {
            bool completed = true;

            ObjectivesConstIt it = m_objectives.begin();
            ObjectivesConstIt end = m_objectives.end();
            for( ; it != end; ++it )
            {
                Missions::Objectives::Objective* obj = it->second;
                if( !obj->IsCompleted() )
                {
                    completed = false;
                    break;
                }
            }

            if( completed )
            {
                SetStatus( S_COMPLETED );
            }
        }
    }

    int Mission::l_GetProgress( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushNumber( GetProgress() );
        return 1;
    }

    int Mission::l_GetTarget( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushNumber( GetTarget() );
        return 1;
    }

    int Mission::l_GetValue( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushNumber( GetValue() );
        return 1;
    }

    int Mission::l_GetStatus( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushNumber( GetStatus() );
        return 1;
    }

    int Mission::l_GiveRewards( lua_State* L )
    {
        GiveRewards();
        return 0;
    }

    int Mission::l_GetId( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushString( GetId() );
        return 1;
    }

    int Mission::l_GetPath( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushString( GetPath() );
        return 1;
    }
}
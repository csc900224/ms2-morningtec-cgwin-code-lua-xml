#ifndef __INCLUDED__MISSIONS__UNIFORMGROUP_HPP__
#define __INCLUDED__MISSIONS__UNIFORMGROUP_HPP__

#include "MonstazAI/missions/MissionGroup.hpp"

namespace Missions
{
    class UniformGroup : public MissionGroup
    {
    public:
        UniformGroup( const Id& id );
        void SetActive( bool active );
        void Validate();
    };
}

#endif
#include "MonstazAI/missions/Rewards.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/GameEvent.hpp"
#include "MonstazAI/Shop.hpp"

#include "claw/base/Errors.hpp"
#include "claw/base/Registry.hpp"

namespace Missions
{
namespace Rewards
{
    void Cash::Give()
    {
        Shop::GetInstance()->UpdateCash( (int)GetValue(), 0 );
    }

    void Gold::Give()
    {
        Shop::GetInstance()->UpdateCash( 0, (int)GetValue() );
    }

    void SubscriptionGold::Give()
    {
        bool subscription = false;
        Claw::Registry::Get()->Get( "/monstaz/subscription", subscription );
        const float ratio = subscription ? 1.5f : 1.0f;
        Shop::GetInstance()->UpdateCash( 0, (int)(GetValue() * ratio) );
    }

    void Bullets::Give()
    {
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_BULLETS_EARNED, GetValue() );
    }
}
}
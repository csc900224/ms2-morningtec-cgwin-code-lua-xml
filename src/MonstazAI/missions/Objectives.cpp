#include "MonstazAI/missions/Objectives.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/Shop.hpp"

#include "claw/base/Registry.hpp"

#include <algorithm>
#include <sstream>

namespace Missions
{
namespace Objectives
{
    // --- BULLETS --- //
    Bullets::Bullets() : GameEventValueObjective( GEI_BULLETS_EARNED ) {}

    bool Bullets::HandleGameEvent( const GameEvent& ev )
    {
        if( GameEventValueObjective::HandleGameEvent( ev ) )
        {
            float diff =  GetValue() - GetTarget();
            if( diff > 0 )
            {
                // Bullets overflow - we need to propagate them to new objective
                GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_BULLETS_EARNED, diff );
            }
            return true;
        }
        return false;
    }

    // --- KILL ENEMY --- //
    KillEnemy::KillEnemy()
        : m_elements( 0 )
    {}

    void KillEnemy::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key.find("entity") != Claw::NarrowString::npos )
        {
            m_entityTypes.push_back( lua.CheckEnum<Entity::Type>(-2) );
        }
        else if( key.find("shot") != Claw::NarrowString::npos )
        {
            m_shotTypes.push_back( lua.CheckEnum<Shot::Type>(-2) );
        }
        else if( key == "elements" )
        {
            m_elements = lua.CheckNumber(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    void KillEnemy::PostInitialize()
    {
        std::sort( m_entityTypes.begin(), m_entityTypes.end() );
        std::sort( m_shotTypes.begin(), m_shotTypes.end() );
    }

    void KillEnemy::SetEntityType( const EntityTypes& entityTypes )
    { 
        m_entityTypes = entityTypes;
        std::sort( m_entityTypes.begin(), m_entityTypes.end() );
    }

    void KillEnemy::SetShotType( const ShotTypes shotTypes )
    {
        m_shotTypes = shotTypes;
        std::sort( m_shotTypes.begin(), m_shotTypes.end() );
    }

    bool KillEnemy::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_ENEMY_KILLED )
        {
            bool entityMatch = m_entityTypes.empty() || std::binary_search( m_entityTypes.begin(), m_entityTypes.end(), ((Entity*)ev.GetUserData())->GetType() );
            bool shotMatch = m_shotTypes.empty() || std::binary_search( m_shotTypes.begin(), m_shotTypes.end(), ((Entity*)ev.GetUserData())->GetLastHit() );
            bool elementsMatch = m_elements == 0 || (m_elements & ((Entity*)ev.GetUserData())->GetLastHitElements()) != 0;

            if( shotMatch && entityMatch && elementsMatch )
            {
                ChangeValue( ev.GetValue() );
                return true;
            }
        }
        return false;
    }

    // --- DESTROY OBJECT --- //
    void DestroyObject::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "object" )
        {
            m_objectName = lua.CheckCString(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    bool DestroyObject::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_OBJECT_DESTROYED )
        {
            if( m_objectName.empty() || ev.GetText().find( m_objectName ) != Claw::NarrowString::npos )
            {
                ChangeValue( ev.GetValue() );
            }
        }
        return false;
    }

    // --- COMPLETE LEVEL --- //
    void CompleteLevel::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "story" )
        {
            m_story = lua.CheckBool(-2);
        }
        else if( key == "boss" )
        {
            m_boss = lua.CheckBool(-2);
        }
        else if( key == "tutorial" )
        {
            m_tutorial = lua.CheckBool(-2);
        }
        else if( key == "survival" )
        {
            m_survival = lua.CheckBool(-2);
        }
        else if( key == "enviroment" )
        {
            m_enviroment = lua.CheckEnum<AtlasSet::Type>(-2);
        }
        else if( key == "stage" )
        {
            m_stage = lua.CheckNumber(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    void CompleteLevel::PostInitialize()
    {
        CLAW_MSG_ASSERT( m_story || m_boss || m_survival || m_tutorial, "Level type is not selected!" );
    }

    CompleteLevel::CompleteLevel()
        : m_story( false )
        , m_boss( false )
        , m_survival( false )
        , m_tutorial( false) 
        , m_interested( false )
        , m_stage( 0 )
        , m_enviroment( AtlasSet::Terminator )
    {}

    void CompleteLevel::OnActivate()
    {
        if( m_stage > 0 )
        {
            // First map level after tutorial is finished == 2
            int currentStage = 0;
            Claw::Registry::Get()->Get( "/maps/current", currentStage );
            if( (currentStage-1) > m_stage )
            {
                ChangeValue( 1 );
            }
        }
    }

    void CompleteLevel::SetTypes( bool normal, bool boss, bool survival, bool tutorial )
    {
        m_story = normal;
        m_boss = boss;
        m_tutorial = survival;
        m_survival = tutorial;

        CLAW_MSG_ASSERT( m_story || m_boss || m_survival || m_tutorial, "Level type is not selected!" );
    }

    bool CompleteLevel::HandleGameEvent( const GameEvent& ev )
    {
        if( m_interested && ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_interested = false;
        }

        if( !m_interested && ev.GetId() == GEI_LEVEL_STARTED && 
            ( m_boss && ev.GetValue() == GEP_LEVEL_STARTED_BOSSFIGHT ||
              m_story && ev.GetValue() == GEP_LEVEL_STARTED_STORY ||
              m_survival && ev.GetValue() == GEP_LEVEL_STARTED_SURVIVAL ||
              m_tutorial && ev.GetValue() == GEP_LEVEL_STARTED_TUTORIAL ))
        {
            if( m_enviroment == AtlasSet::Terminator )
            {
                m_interested = true;
            }
            else
            {
                Claw::Lua* lua = MissionManager::GetInstance()->GetCurrentLua();
                lua->PushString( ev.GetText() );
                lua->PushNumber( m_enviroment );
                lua->Call( "LevelCheckAtlas", 2, 1 );
                m_interested = lua->CheckBool( -1 );
            }
            return m_interested;
        }
        else if( m_interested && ev.GetId() == GEI_LEVEL_FINISHED )
        {
            m_interested = false;
            if( ev.GetValue() == GEP_LEVEL_FINISHED_SUCCESS )
            {
                ChangeValue( 1 );
            }
            return true;
        }
        return false;
    }

    // --- COMPLETE TUTORIAL --- //
    CompleteTutorial::CompleteTutorial() : GameEventObjective( GEI_TUTORIAL_COMPLETED ) {}

    // --- PLAY WITH FRIEND --- //
    PlayWithFriend::PlayWithFriend() : GameEventObjective( GEI_LEVEL_WITH_FRIEND ) {}

    // --- COLECT CASH --- //
    CollectCash::CollectCash()
        : m_gamePlay( false )
    {}

    bool CollectCash::HandleGameEvent( const GameEvent& ev )
    {
        if( !m_gamePlay && ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_gamePlay = true;
            return true;
        }
        else if( m_gamePlay && ev.GetId() == GEI_LEVEL_FINISHED )
        {
            m_gamePlay = false;
            return true;
        }
        else if( m_gamePlay && ev.GetId() == GEI_CASH_CHANGED )
        {
            int collected = 0;
            Claw::Registry::Get()->Get( "/internal/money", collected );
            SetValue( (float)collected );
            return true;
        }
        return false;
    }

    // --- HOLD FIRE --- //
    HoldFire::HoldFire()
        : m_timer( 0 )
        , m_gamePlay( false )
    {}

    void HoldFire::Update( float dt )
    {
        if( m_gamePlay && m_timer >= 0 )
        {
            m_timer += dt;
            if( m_timer >= GetTarget() )
            {
                SetValue( m_timer );
                m_timer = -1;
            }
        }
    }

    bool HoldFire::HandleGameEvent( const GameEvent& ev )
    {
        if( !m_gamePlay )
        {
            if( ev.GetId() == GEI_LEVEL_STARTED )
            {
                m_gamePlay = true;
                m_timer = 0;
                return true;
            }
            else if( ev.GetId() == GEI_GAME_RESUMED )
            {
                m_gamePlay = true;
                return true;
            }
        }
        else if( m_gamePlay )
        {
            if( ev.GetId() == GEI_LEVEL_FINISHED || ev.GetId() == GEI_GAME_PAUSED )
            {
                m_gamePlay = false;
                return true;
            }
            else if( ev.GetId() == GEI_SHOT_PLAYER_START )
            {
                m_timer = -1;
                return true;
            }
            else if( ev.GetId() == GEI_SHOT_PLAYER_STOP )
            {
                m_timer = 0;
                return true;
            }
        }
        return false;
    }

    // --- PICKUP --- //
    void Pickup::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "object_type" )
        {
            m_pickupType = lua.CheckEnum< ::Pickup::Type>(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    Pickup::Pickup() 
        : m_pickupType( ::Pickup::NumPickups )
    {}

    bool Pickup::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_OBJECT_PICKUP &&
            (m_pickupType == ::Pickup::NumPickups || ((::Pickup*)ev.GetUserData())->m_type == m_pickupType) )
        {
            ChangeValue( ev.GetValue() );
            return true;
        }
        return false;
    }

    // --- SURVIVE --- //
    Survive::Survive()
        : m_story( false )
        , m_survival( false )
        , m_boss( false )
        , m_timer( -1 )
        , m_pause( false )
    {}

    void Survive::Update( float dt )
    {
        if( !m_pause && m_timer >= 0 )
        {
            m_timer += dt;
            if( m_timer >= GetTarget() )
            {
                SetValue( m_timer );
                m_timer = -1;
            }
        }
    }

    bool Survive::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_STARTED &&
            ( m_boss && ev.GetValue() == GEP_LEVEL_STARTED_BOSSFIGHT ||
              m_story && ev.GetValue() == GEP_LEVEL_STARTED_STORY ||
              m_survival && ev.GetValue() == GEP_LEVEL_STARTED_SURVIVAL ))
        {
            m_timer = 0;
            m_pause = false;
            return true;
        }
        else if( ev.GetId() == GEI_LEVEL_FINISHED )
        {
            m_timer = -1;
            return true;
        }
        else if( ev.GetId() == GEI_GAME_PAUSED )
        {
            m_pause = true;
            return true;
        }
        else if( ev.GetId() == GEI_GAME_RESUMED )
        {
            m_pause = false;
            return true;
        }
        return false;
    }

    void Survive::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "story" )
        {
            m_story = lua.CheckBool(-2);
        }
        else if( key == "boss" )
        {
            m_boss = lua.CheckBool(-2);
        }
        else if( key == "survival" )
        {
            m_survival = lua.CheckBool(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    void Survive::PostInitialize()
    {
        CLAW_MSG_ASSERT( m_story || m_boss || m_survival, "Survive level type is not selected!" );
    }

    // --- USE PERK --- //
    UsePerk::UsePerk()
        : GameEventObjective( GEI_PERK_USED )
        , m_singleRound( false )
    {}

    void UsePerk::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "single_round" )
        {
            m_singleRound = lua.CheckBool(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    bool UsePerk::HandleGameEvent( const GameEvent& ev )
    {
        bool retVal = GameEventObjective::HandleGameEvent( ev );

        if( m_singleRound && ev.GetId() == GEI_LEVEL_FINISHED && !IsCompleted() )
        {
            Reset();
            retVal = true;
        }
        return retVal;
    }

    // --- ITEM OBJECTIBE --- //
    ItemObjective::ItemObjective( const GameEvent::Id& evId ) 
    {
        m_events.insert( evId );
    }

    bool ItemObjective::HandleGameEvent( const GameEvent& ev )
    {
        if( CheckEvent( ev ) )
        {
            ChangeValue( ev.GetValue() );
            return true;
        }
        return false;
    }

    bool ItemObjective::CheckEvent( const GameEvent& ev )
    {
        if( m_events.find( ev.GetId() ) != m_events.end() )
        {
            return  m_items.empty() || std::binary_search( m_items.begin(), m_items.end(), ev.GetText() );
        }
        return false;
    }

    void ItemObjective::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key.find("item") != Claw::NarrowString::npos )
        {
            m_items.push_back( lua.CheckCString(-2) );
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    void ItemObjective::PostInitialize()
    {
        std::sort( m_items.begin(), m_items.end() );
    }

    // --- SHOP ITEM BUY --- //
    ShopItemBuy::ShopItemBuy()
    {
        Events events;
        events.insert( GEI_ITEM_BOUGHT );
        events.insert( GEI_ITEM_UNLOCKED );
        SetEvents( events );
    }

    void ShopItemBuy::OnActivate()
    {
        bool allBought = true;

        Items::iterator it = m_items.begin();
        Items::iterator end = m_items.end();

        for( ; it != end; ++it )
        {
            if( Shop::GetInstance()->CheckOwnership( *it ) == 0 )
            {
                allBought = false;
                break;
            }
        }

        if( allBought )
        {
            ChangeValue( 1 );
        }
    }

    // --- SHOP ITEM UPGRADE MAX --- //
    ShopItemUpgradeMax::ShopItemUpgradeMax()
        : ItemObjective( GEI_ITEM_UPGRADED )
    {}

    bool ShopItemUpgradeMax::HandleGameEvent( const GameEvent& ev )
    {
        if( CheckEvent( ev ) && !Shop::GetInstance()->CanUpgradeItem( ev.GetText() ) )
        {
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    void ShopItemUpgradeMax::OnActivate()
    {
        if( !Shop::GetInstance()->CanUpgradeAnyItem() )
        {
            ChangeValue( 1 );
        }
    }

    // --- KILL BOSS --- //
    KillBoss::KillBoss()
        : m_stage( -1 )
        , m_bossType( Entity::TypesCount )
    {}

    void KillBoss::SetBoss( Entity::Type type, int stage )
    {
        m_bossType = type;
        m_stage = stage;
    }

    bool KillBoss::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_ENEMY_KILLED && m_bossType == ((Entity*)ev.GetUserData())->GetType())
        {
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    void KillBoss::OnActivate()
    {
        // Check if boss was not killed before
        // First map level after tutorial is finished == 2
        int currentStage = 0;
        Claw::Registry::Get()->Get( "/maps/current", currentStage );
        if( (currentStage-1) > m_stage )
        {
            ChangeValue( 1 );
        }
    }

    void KillBoss::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "boss" )
        {
            m_bossType =  lua.CheckEnum<Entity::Type>(-2);
        }
        else if( key == "stage" )
        {
            m_stage = lua.CheckNumber(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    void KillBoss::PostInitialize()
    {
        CLAW_MSG_ASSERT( m_bossType != Entity::TypesCount && m_stage >= 0, "Kill Boss objective requires stage and boss type to be defined!" );
    }

    // --- FIRE BOTH WEAPONS --- //
    FireBothWeapons::FireBothWeapons()
        : m_lastShot( -1 )
    {}

    bool FireBothWeapons::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_lastShot = -1;
            return true;
        }
        else if( ev.GetId() == GEI_SHOT_PLAYER_START )
        {
            int wepId = (int)ev.GetValue();

            if( m_lastShot < 0 )
            {
                m_lastShot = wepId;
                return true;
            }
            else if( m_lastShot != wepId )
            {
                m_lastShot = wepId;
                ChangeValue( 1 );
                return true;
            }
        }
        return false;
    }

    // --- SCORE --- //
    Score::Score() 
        : m_interested( false )
        , m_singleRound( false )
    {}

    bool Score::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_interested = ev.GetValue() == GEP_LEVEL_STARTED_SURVIVAL && (m_map.empty() || m_map == ev.GetText());
            return true;
        }
        else if( m_interested && ev.GetId() == GEI_LEVEL_FINISHED )
        {
            m_interested = false;
            if( m_singleRound )
            {
                Reset();
            }
            return true;
        }
        else if( m_interested && ev.GetId() == GEI_GAME_SCORE_CHANGED )
        {
            ChangeValue( ev.GetValue() );
            return true;
        }
        return false;
    }

    void Score::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "map" )
        {
            m_map = lua.CheckCString(-2);
        }
        else if( key == "single_round" )
        {
            m_singleRound = lua.CheckBool( -2 );
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    // --- MANUAL AIMING --- //
    bool ManualAiming::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_AUTOAIMING_CHANGED && ev.GetValue() == 0 )
        {
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    // --- REVIVE --- //
    Revive::Revive() : GameEventObjective( GEI_REVIVE_PLAYER ) {}

    // --- SETTINGS TAB --- //
    SettingsTab::SettingsTab()
        : m_tabIdx( 0 )
    {}

    bool SettingsTab::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_SETTINGS_TAB_CHANGED && (m_tabIdx <= 0 || m_tabIdx == ev.GetValue()) )
        {
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    void SettingsTab::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "tab" )
        {
            m_tabIdx = lua.CheckNumber(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    // --- ADD A FRIEND --- //
    bool AddFriend::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_FRIEND_INVITATION_CONFIRMED || ev.GetId() == GEI_FRIEND_INVITED )
        {
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    // --- SEND GIFT --- //
    SendGift::SendGift() : GameEventObjective( GEI_FRIEND_GIFT_SENT ) {}

    // --- RECIVE GIFT --- //
    ReciveGift::ReciveGift() : GameEventObjective( GEI_FRIEND_GIFT_RECIVED ) {}

    // --- PLAY SURVIVAL --- //
    PlaySurvival::PlaySurvival()
        : m_interested( false )
        , m_numLevels( 1 )
    {}

    void PlaySurvival::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "levels" )
        {
            m_numLevels = lua.CheckNumber(-2);
        }
        else
        {
            Objective::Initialize( key, lua );
        }
    }

    bool PlaySurvival::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_interested = ev.GetValue() == GEP_LEVEL_STARTED_SURVIVAL;
            return true;
        }
        else if( m_interested && ev.GetId() == GEI_GAME_HISCORE_CHANGED )
        {
            CheckCompleted();
            return true;
        }
        return false;
    }

    void PlaySurvival::OnActivate()
    {
        CheckCompleted();
    }

    void PlaySurvival::CheckCompleted()
    {
        int numPlayed = 0;
        for( int level = 1; true; ++level )
        {
            std::ostringstream key;
            key << "/monstaz/hiscore/" << level << "/score";

            int score = 0;
            if( !Claw::Registry::Get()->Get( key.str().c_str(), score ) )
            {
                break;
            }

            if( score > 0 )
            {
                ++numPlayed;
                if( numPlayed >= m_numLevels )
                {
                    ChangeValue( 1 );
                    break;
                }
            }
        }
    }

    // --- ENTER ENDLESS --- //
    EnterEndless::EnterEndless()
        : m_lastLevel( false )
    {}

    bool EnterEndless::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_lastLevel = m_lastLevelName == ev.GetText();
            return true;
        }
        else if( m_lastLevel && ev.GetId() == GEI_LEVEL_FINISHED && ev.GetValue() == GEP_LEVEL_FINISHED_SUCCESS )
        {
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    void EnterEndless::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "last_level" )
        {
            m_lastLevelName = lua.CheckCString(-2);
        }
        else if( key == "endless_stage" )
        {
            m_endlessStage = lua.CheckNumber(-2);
        }
    }

    void EnterEndless::OnActivate()
    {
        int currentStage = 0;
        Claw::Registry::Get()->Get( "/maps/current", currentStage );
        if( currentStage >= m_endlessStage )
        {
            ChangeValue( 1 );
        }
    }
}
}

#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/missions/DailyGroup.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/Application.hpp"

#include <cstring>

namespace Missions
{
    static const char* REGISTRY_BRANCH = "/missions";
    static const char* MISSION_PATH_DEMIMITER = "/";

    LUA_DECLARATION( MissionManager )
    {
        METHOD( MissionManager, ClaimReward ),
        METHOD( MissionManager, GetMission ),
        METHOD( MissionManager, GetGroup ),
        METHOD( MissionManager, GetGroups ),
        METHOD( MissionManager, GetGroupTimeLeft ),
        METHOD( MissionManager, GetMissionsNumToClaim ),
        {0,0}
    };

    MissionManager::MissionManager()
        : GameEventHandler( true )
        , m_registryBranch( REGISTRY_BRANCH )
        , m_factory( this )
        , m_achievementsMgr( this )
        , m_currentLua( NULL )
        , m_loaded( false )
        , m_paused( false )
        , m_autoSave( true )
    {}

    void  MissionManager::SetPaused( bool paused ) 
    {
        m_paused = paused;
        m_notifier.SetActive( !m_paused );
    }

    MissionManager::~MissionManager()
    {
        Reset();
    }

    void MissionManager::Reset()
    {
        MissionGroupsIt it = m_groups.begin();
        MissionGroupsIt end = m_groups.end();
        for( ; it != end; ++it )
        {
            delete it->second;
        }
        m_groups.clear();
    }

    void MissionManager::InitLua( Claw::Lua* lua, bool setCurrent )
    {
        if( setCurrent )
        {
            m_currentLua = lua;
        }

        GameEvent::InitLua( lua );
        MissionGroup::InitLua( lua );
        Mission::InitLua( lua );

        m_notifier.InitLua( lua );

        Claw::Lunar<MissionManager>::Register( *lua );
        Claw::Lunar<MissionManager>::push( *lua, this );
        lua->RegisterGlobal( "MissionManager" );
    }

    void MissionManager::LoadMissions( const char* luaPath )
    {
        m_loaded = false;

        Claw::Lua lua;
        lua.RegisterLibrary( Claw::Lua::L_BASE );
        lua.RegisterLibrary( Claw::Lua::L_TABLE );
        lua.RegisterLibrary( Claw::Lua::L_MATH );
        lua.RegisterLibrary( Claw::Lua::L_STRING );

        m_factory.InitLua( &lua );
        InitLua( &lua );

        lua.Load( luaPath );
        lua.Call( "MissionLoad", 0, 0 );

        // Recheck state of active missions
        Shop::GetInstance()->Init( &lua );
        lua.Load( "ItemDB.lua" );
        Validate();

        m_loaded = true;
    }

    void MissionManager::Validate()
    {
        MissionGroupsIt it = m_groups.begin();
        MissionGroupsIt end = m_groups.end();
        for( ; it != end; ++it )
        {
            it->second->Validate();
        }
    }

    void MissionManager::Update( float dt )
    {
        if( !m_loaded || m_paused ) return;

        MissionGroupsIt it = m_groups.begin();
        MissionGroupsIt end = m_groups.end();
        for( ; it != end; ++it )
        {
            it->second->Update( dt );
        }
        m_notifier.Update( dt, m_currentLua );
    }

    void MissionManager::UnlockCachedAchievements()
    {
        m_achievementsMgr.UnlockCachedAchievements();
    }

    bool MissionManager::HandleGameEvent( const GameEvent& ev )
    {
        bool retValue = false;
        if( !TutorialManager::GetInstance()->IsActive() || !m_loaded || m_paused )
        {
            MissionGroupsIt it = m_groups.begin();
            MissionGroupsIt end = m_groups.end();
            for( ; it != end; ++it )
            {
                if( it->second->IsActive() )
                {
                    retValue |= it->second->HandleGameEvent( ev );
                }
            }
        }
        return retValue;
    }

    void MissionManager::AddGroup( MissionGroup* newMission )
    {
        std::pair<MissionGroups::iterator, bool> restult = m_groups.insert( MissionGroups::value_type( newMission->GetId(), newMission ) );
        if( restult.second )
        {
            newMission->SetRegistryBranch( m_registryBranch );
            ProcessMissions( newMission, &MissionManager::OnMissionAdded );
        }
    }

    void MissionManager::RemoveGroup( MissionGroup* missionToRemove, bool release )
    {
        m_groups.erase( missionToRemove->GetId() );
        ProcessMissions( missionToRemove, &MissionManager::OnMissionRemoved );

        if( release )
        {
            delete missionToRemove;
        }
    }

    MissionGroup* MissionManager::GetGroup( const MissionGroup::Id& id ) const
    {
        MissionGroupsConstIt it = m_groups.find( id );
        if( it != m_groups.end() )
        {
            return it->second;
        }
        return NULL;
    }

    void MissionManager::ProcessMissions( MissionGroup* parent, MissionProcessor processor )
    {
        Claw::NarrowString path( "" );
        ProcessMissions( parent, processor, path );
    }

    void MissionManager::ProcessMissions( MissionGroup* parent, MissionProcessor processor, const Claw::NarrowString& path )
    {
        MissionGroup::Missions& missions = parent->GetMissions();
        MissionGroup::MissionsIt it = missions.begin();
        MissionGroup::MissionsIt end = missions.end();

        Claw::NarrowString myPath = path;
        myPath.append( parent->GetId() );
        myPath.append( MISSION_PATH_DEMIMITER );

        for( ; it != end; ++it )
        {
            if( !strcmp( (*it)->GetTypeName(), Mission::TYPE_NAME ) )
            {
                (this->*processor)((Mission*)*it, myPath);
            }
            else if( !strcmp( (*it)->GetTypeName(), MissionGroup::TYPE_NAME ) )
            {
                ProcessMissions( (MissionGroup*)*it, processor, myPath );
            }
        }
    }

    void MissionManager::OnMissionAdded( Mission* mission, const Claw::NarrowString& path )
    {
        mission->RegisterObserver( &m_notifier );
        mission->RegisterObserver( this );
        mission->RegisterObserver( &m_achievementsMgr );
        std::pair<PathToMissionMap::iterator, bool> result = m_missionsLUT.insert( PathToMissionMap::value_type( mission->GetPath(), mission ) );
        CLAW_MSG_ASSERT( result.second, "Mission already added: " << mission->GetPath() );
    }

    void MissionManager::OnMissionRemoved( Mission* mission, const Claw::NarrowString& path )
    {
        mission->UnregisterObserver( this );
        mission->UnregisterObserver( &m_notifier );
        m_missionsLUT.erase( mission->GetPath() );
    }

    Mission* MissionManager::GetMission( const Claw::NarrowString& fullMissionPath ) const
    {
        PathToMissionMap::const_iterator it = m_missionsLUT.find( fullMissionPath );
        CLAW_MSG_ASSERT( it != m_missionsLUT.end(), "Mission not found: " << fullMissionPath );
        if( it != m_missionsLUT.end() )
        {
            return it->second;
        }
        return NULL;
    }

    void MissionManager::OnMissionCompleted( const Mission* mission )
    {
        if( m_loaded && m_autoSave )
        {
            ((MonstazApp*)MonstazApp::GetInstance())->Save();
        }

        if( m_currentLua )
        {
            m_currentLua->PushString( mission->GetPath() );
            m_currentLua->Call( "OnMissionCompleted", 1, 0 );
        }
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_MISSION_COMPLETED, 1, "", (void*)mission );
    }

    void MissionManager::OnMissionRewarded( const Mission* mission )
    {
        if( m_loaded && m_autoSave )
        {
            ((MonstazApp*)MonstazApp::GetInstance())->Save();
        }
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_MISSION_CLAIMED, 1, "", (void*)mission );
    }

    void MissionManager::OnMissionActivated( const Mission* mission )
    {
        if( m_loaded && m_autoSave )
        {
            ((MonstazApp*)MonstazApp::GetInstance())->Save();
        }
    }

    int MissionManager::GetMissionsNumToClaim() const
    {
        int val = 0;
        PathToMissionMap::const_iterator it = m_missionsLUT.begin();
        PathToMissionMap::const_iterator end = m_missionsLUT.end();
        for( ; it != end; ++it )
        {
            if( it->second->GetStatus() == Mission::S_COMPLETED )
            {
                ++val;
            }
        }
        return val;
    }

    void MissionManager::SetAutoSaveEnabled( bool autoSave )
    {
        if( m_autoSave != autoSave )
        {
            m_autoSave = autoSave;
            if( m_loaded && autoSave )
            {
                ((MonstazApp*)MonstazApp::GetInstance())->Save();
            }
        }
    }

    int MissionManager::l_ClaimReward( lua_State* L )
    {
        Claw::Lua lua( L );
        Claw::NarrowString missionPath = lua.CheckCString( 1 );

        Mission* mission = GetMission( missionPath );
        CLAW_MSG_ASSERT( mission->GetStatus() == Mission::S_COMPLETED, "Mission is not completed: " << missionPath );
        mission->GiveRewards();

        return 0;
    }

    int MissionManager::l_GetMission( lua_State* L )
    {
        Claw::Lua lua( L );
        Claw::NarrowString missionPath = lua.CheckCString( 1 );

        Mission* mission = GetMission( missionPath );
        if( mission )
        {
            Claw::Lunar<Mission>::push( L, mission );
        }
        else
        {
            lua.PushNil();
        }
        return 1;
    }

    int MissionManager::l_GetGroup( lua_State* L )
    {
        Claw::Lua lua( L );
        Claw::NarrowString groupId = lua.CheckCString( 1 );

        MissionGroup* group = GetGroup( groupId );
        if( group )
        {
            Claw::Lunar<MissionGroup>::push( L, group );
        }
        else
        {
            lua.PushNil();
        }
        return 1;
    }

    int MissionManager::l_GetGroups( lua_State* L )
    {
        MissionGroupsIt it = m_groups.begin();
        MissionGroupsIt end = m_groups.end();
        lua_newtable( L );
        int idx = 1;

        for( ; it != end; ++it )
        {
            lua_pushinteger( L, idx++ );
            Claw::Lunar<MissionGroup>::push( L, it->second );
            lua_settable( L, -3 );
        }
        return 1;
    }

    int MissionManager::l_GetGroupTimeLeft( lua_State* L )
    {
        MissionGroup* missionGroup = Claw::Lunar<MissionGroup>::check( L, 1 );
        Claw::Lua lua( L );
        lua.PushNumber( ((DailyGroup*)missionGroup)->GetTimeLeft() );
        return 1;
    }

    int MissionManager::l_GetMissionsNumToClaim( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushNumber( GetMissionsNumToClaim() );
        return 1;
    }
}
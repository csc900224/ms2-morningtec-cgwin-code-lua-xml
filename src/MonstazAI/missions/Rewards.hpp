#ifndef __INCLUDED__MISSIONS__REWARDS_HPP__
#define __INCLUDED__MISSIONS__REWARDS_HPP__

#include "MonstazAI/missions/Reward.hpp"

namespace Missions
{
namespace Rewards
{
    class Cash : public ValueReward
    {
    public:
        Cash( lua_State* L ) : ValueReward( L ) {}
        void Give();
    };

    class Gold : public ValueReward
    {
    public:
        Gold( lua_State* L ) : ValueReward( L ) {}
        void Give();
    };

    class SubscriptionGold : public ValueReward
    {
    public:
        SubscriptionGold( lua_State* L ) : ValueReward( L ) {}
        void Give();
    };

    class Bullets : public ValueReward
    {
    public:
        Bullets( lua_State* L ) : ValueReward( L ) {}
        void Give();
    };
}
}

#endif
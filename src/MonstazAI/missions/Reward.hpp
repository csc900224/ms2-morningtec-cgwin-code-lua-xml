#ifndef __INCLUDED__MISSIONS__REWARD_HPP__
#define __INCLUDED__MISSIONS__REWARD_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/String.hpp"

namespace Missions
{
namespace Rewards
{
    class Reward
    {
    public:
        Reward( lua_State* L ) {}
        Reward() {}

        virtual ~Reward() {}
        virtual void Give() = 0;
    };

    class ValueReward : public Reward
    {
    public:
        ValueReward( lua_State* L );
        ValueReward( float value );

        float GetValue() const { return m_value; }

    private:
        float m_value;
    };
}
}

#endif
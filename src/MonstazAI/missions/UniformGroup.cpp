#include "MonstazAI/missions/UniformGroup.hpp"
#include "MonstazAI/missions/MissionManager.hpp"

namespace Missions
{
    UniformGroup::UniformGroup( const Id& id )
        : MissionGroup( id )
    {}

    void UniformGroup::SetActive( bool active )
    {
        if( active != IsActive() )
        {
            MissionManager::GetInstance()->SetAutoSaveEnabled( false );
            // Activate all missions
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                Completable* mission = *it;
                if( !mission->IsCompleted() )
                {
                    mission->SetActive( active );
                }
            }
            MissionManager::GetInstance()->SetAutoSaveEnabled( true );
        }
    }

    void UniformGroup::Validate()
    {
        if( IsActive() )
        {
            MissionManager::GetInstance()->SetAutoSaveEnabled( false );
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                Completable* mission = *it;
                if( !mission->IsActive() && !mission->IsCompleted() )
                {
                    mission->SetActive( true );
                }
            }
            MissionManager::GetInstance()->SetAutoSaveEnabled( true );
        }
        MissionGroup::Validate();
    }
}
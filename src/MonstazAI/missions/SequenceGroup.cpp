#include "MonstazAI/missions/SequenceGroup.hpp"
#include "MonstazAI/missions/MissionManager.hpp"

namespace Missions
{
    SequenceGroup::SequenceGroup( const Id& id )
        : MissionGroup( id )
    {}

    void SequenceGroup::SetActive( bool active )
    {
        if( active != IsActive() )
        {
            MissionManager::GetInstance()->SetAutoSaveEnabled( false );
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            bool wasActivated = false;
            for( ; it != end; ++it )
            {
                // Activate first not completed mission
                Completable* mission = *it;
                if( active )
                {
                    if( !wasActivated && !mission->IsActive() && !mission->IsCompleted() )
                    {
                        mission->SetActive( true );
                        wasActivated = true;
                    }
                    else if( wasActivated && mission->IsActive() )
                    {
                        mission->SetActive( false );
                    }
                }
                // Disable all active missions
                else if( !active && mission->IsActive() )
                {
                    mission->SetActive( active );
                }
            }
            MissionManager::GetInstance()->SetAutoSaveEnabled( true );
        }
    }

    void SequenceGroup::Validate()
    {
        MissionManager::GetInstance()->SetAutoSaveEnabled( false );
        bool completedFound = false;

        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            Completable* mission = *it;
            if( !completedFound && mission->IsCompleted() )
            {
                completedFound = true;
            }
            else if( completedFound && !mission->IsActive() && !mission->IsCompleted() )
            {
                // Activate next mission
                mission->SetActive( true );
                break;
            }
            else if( completedFound && mission->IsActive() )
            {
                break;
            }
        }
        MissionManager::GetInstance()->SetAutoSaveEnabled( true );

        MissionGroup::Validate();
    }

    void SequenceGroup::OnCompleted( const Completable* mission )
    {
        MissionGroup::OnCompleted( mission );

        bool lastFound = false;

        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            if( !lastFound && *it == mission )
            {
                lastFound = true;
            }
            else if( lastFound )
            {
                // Activate next mission
                (*it)->SetActive( true );
                break;
            }
        }
    }

    bool SequenceGroup::HandleGameEvent( const GameEvent& ev )
    {
        bool retValue = false;
        if( IsActive() )
        {
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                Completable* misison = *it;
                if( misison->IsActive() && !misison->IsCompleted() )
                {
                    retValue |= misison->HandleGameEvent( ev );

                    // Only one active mission is allowed at time
                    break;
                }
            }
        }
        return retValue;
    }
}
#ifndef __INCLUDED_MISSIONS_ACHIEVEMENTSMANAGER_HPP__
#define __INCLUDED_MISSIONS_ACHIEVEMENTSMANAGER_HPP__

#include "MonstazAI/missions/Mission.hpp"

namespace Missions
{
    class MissionManager;

    class AchievementsManager : public Mission::Observer
    {
    public:
        AchievementsManager( MissionManager* mm );

        void UnlockCachedAchievements();

        //Mission::Observer interface
        void OnMissionProgressChanged( const Mission* mission ) {}
        void OnMissionCompleted( const Mission* mission ) {}
        void OnMissionRewarded( const Mission* mission ) {}
        void OnMissionActivated( const Mission* mission );

    private:
        void UnlockAchievement( const Claw::NarrowString& rank );
        AchievementsManager( const AchievementsManager& );
        AchievementsManager();

        MissionManager* m_missionManager;
    };
}

#endif
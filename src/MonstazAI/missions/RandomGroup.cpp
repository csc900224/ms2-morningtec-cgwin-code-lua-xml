#include "MonstazAI/missions/RandomGroup.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/missions/MissionManager.hpp"

#include <algorithm>
#include <vector>

namespace Missions
{
    RandomGroup::RandomGroup( const Id& id )
        : MissionGroup( id )
        , m_autoRandom( false )
        , m_lastActive( NULL )
    {}

    bool RandomGroup::IsCompleted() const
    {
        bool completed = false;
        MissionsConstIt it = m_missions.begin();
        MissionsConstIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            if( (*it)->IsCompleted() )
            {
                completed = true;
                break;
            }
        }
        return completed;
    }

    void RandomGroup::Randomize()
    {
        if( m_missions.size() > 1 )
        {
            Completable* completedMission = NULL;
            std::vector<Completable*> tmpMissions;

            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                if( !(*it)->IsCompleted() && (m_lastActive == NULL || m_lastActive != *it) )
                {
                    // Do not draw form completed missions
                    tmpMissions.push_back( *it );
                }
            }

            Reset();

            int idx = g_rng.GetInt( 0, tmpMissions.size() - 1 );
            m_lastActive = tmpMissions[idx];
            m_lastActive->SetActive( true );
        }
        else if( m_missions.size() == 1 )
        {
            (*m_missions.begin())->SetActive( true );
        }
    }

    void RandomGroup::SetActive( bool active )
    {
        if( active != IsActive() )
        {
            MissionManager::GetInstance()->SetAutoSaveEnabled( false );
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                Completable* mission = *it;
                if( !mission->IsCompleted() )
                {
                    mission->SetActive( false );
                }
            }

            if( active )
            {
                Randomize();
            }
            MissionManager::GetInstance()->SetAutoSaveEnabled( true );
        }
    }

    void RandomGroup::OnCompleted( const Completable* mission )
    {
        MissionGroup::OnCompleted( mission );

        if( m_autoRandom )
        {
            Randomize();
        }
    }

    bool RandomGroup::SetRegistryBranch( const Claw::NarrowString& branchPath )
    {
        if( MissionGroup::SetRegistryBranch( branchPath ) )
        {
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                if( (*it)->IsActive() )
                {
                    m_lastActive = *it;
                    break;
                }
            }
            return true;
        }
        return false;
    }

    void RandomGroup::AddMission( Completable* newMission )
    {
        MissionGroup::AddMission( newMission );
        if( newMission->IsActive() )
        {
            m_lastActive = newMission;
        }
    }

    void RandomGroup::RemoveMission( Completable* missionToRemove, bool release )
    {
        MissionGroup::RemoveMission( missionToRemove, release );
        if( missionToRemove == m_lastActive )
        {
            m_lastActive = NULL;
        }
    }
}
#include "MonstazAI/missions/Reward.hpp"

namespace Missions
{
namespace Rewards
{
    ValueReward::ValueReward( lua_State* L )
        : Reward( L )
        , m_value( 0 )
    {
        Claw::Lua lua( L );
        CLAW_ASSERT( lua.IsTable(-1) );
        lua.PushNil();
        while( lua.TableNext(-2) )
        {
            // copy the key so that lua_tostring does not modify the original
            lua.PushValue(-2);

            Claw::NarrowString key = lua.CheckCString(-1);
            if( key == "value" )
            {
                m_value = (float)lua.CheckNumber(-2);
            }

            // pop value + copy of key, leaving original key
            lua.Pop( 2 );
        }
    }

    ValueReward::ValueReward( float value )
        : m_value( value )
    {}
}
}
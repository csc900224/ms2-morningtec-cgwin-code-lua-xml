#include "MonstazAI/missions/MissionFactory.hpp"
#include "MonstazAI/missions/Objectives.hpp"
#include "MonstazAI/missions/Rewards.hpp"
#include "MonstazAI/missions/DailyGroup.hpp"
#include "MonstazAI/missions/RandomGroup.hpp"
#include "MonstazAI/missions/UniformGroup.hpp"
#include "MonstazAI/missions/SequenceGroup.hpp"
#include "MonstazAI/missions/MissionManager.hpp"

#include "MonstazAI/AtlasManager.hpp"
#include "MonstazAI/entity/EntityManager.hpp"
#include "MonstazAI/shot/ShotManager.hpp"
#include "MonstazAI/PickupManager.hpp"

#include "claw/base/Errors.hpp"

namespace Missions
{
    LUA_DECLARATION( MissionFactory )
    {
        METHOD( MissionFactory, StartGroup ),
        METHOD( MissionFactory, EndGroup ),
        METHOD( MissionFactory, StartMission ),
        METHOD( MissionFactory, EndMission ),
        METHOD( MissionFactory, CreateObjective ),
        METHOD( MissionFactory, CreateReward ),
        {0,0}
    };

    MissionFactory::MissionFactory( MissionManager* manager )
        : m_manager( manager )
        , m_currentMission( NULL )
    {
        // Register rewards
        m_rewardFactory.Register<Rewards::Cash>("Cash");
        m_rewardFactory.Register<Rewards::Gold>("Gold");
        m_rewardFactory.Register<Rewards::SubscriptionGold>("SubscriptionGold");
        m_rewardFactory.Register<Rewards::Bullets>("Bullets");

        // Register objectives
        m_objectiveFactory.Register<Objectives::Bullets>("Bullets");
        m_objectiveFactory.Register<Objectives::KillEnemy>("KillEnemy");
        m_objectiveFactory.Register<Objectives::DestroyObject>("DestroyObject");
        m_objectiveFactory.Register<Objectives::CompleteLevel>("CompleteLevel");
        m_objectiveFactory.Register<Objectives::CompleteTutorial>("CompleteTutorial");
        m_objectiveFactory.Register<Objectives::PlayWithFriend>("PlayWithFriend");
        m_objectiveFactory.Register<Objectives::CollectCash>("CollectCash");
        m_objectiveFactory.Register<Objectives::HoldFire>("HoldFire");
        m_objectiveFactory.Register<Objectives::Pickup>("Pickup");
        m_objectiveFactory.Register<Objectives::Survive>("Survive");
        m_objectiveFactory.Register<Objectives::UsePerk>("UsePerk");
        m_objectiveFactory.Register<Objectives::ShopItemBuy>("ShopItemBuy");
        m_objectiveFactory.Register<Objectives::ShopItemUse>("ShopItemUse");
        m_objectiveFactory.Register<Objectives::ShopItemUpgrade>("ShopItemUpgrade");
        m_objectiveFactory.Register<Objectives::ShopItemUpgradeMax>("ShopItemUpgradeMax");
        m_objectiveFactory.Register<Objectives::KillBoss>("KillBoss");
        m_objectiveFactory.Register<Objectives::FireBothWeapons>("FireBothWeapons");
        m_objectiveFactory.Register<Objectives::Score>("Score");
        m_objectiveFactory.Register<Objectives::ManualAiming>("ManualAiming");
        m_objectiveFactory.Register<Objectives::Revive>("Revive");
        m_objectiveFactory.Register<Objectives::SettingsTab>("SettingsTab");
        m_objectiveFactory.Register<Objectives::AddFriend>("AddFriend");
        m_objectiveFactory.Register<Objectives::SendGift>("SendGift");
        m_objectiveFactory.Register<Objectives::ReciveGift>("ReciveGift");
        m_objectiveFactory.Register<Objectives::PlaySurvival>("PlaySurvival");
        m_objectiveFactory.Register<Objectives::EnterEndless>("EnterEndless");

        // Register mission gorup
        m_groupFactory.Register<DailyGroup>("Daily");
        m_groupFactory.Register<RandomGroup>("Random");
        m_groupFactory.Register<UniformGroup>("Uniform");
        m_groupFactory.Register<SequenceGroup>("Sequence");
    }

    void MissionFactory::InitLua( Claw::Lua* lua )
    {
        Claw::Lunar<MissionFactory>::Register( *lua );
        Claw::Lunar<MissionFactory>::push( *lua, this );
        lua->RegisterGlobal( "MissionFactory" );

        AtlasManager::InitEnum( lua );
        EntityManager::InitEnum( lua );
        ShotManager::InitEnum( lua );
        PickupManager::InitEnum( lua );
    }

    int MissionFactory::l_StartGroup( lua_State* L )
    {
        Claw::Lua lua( L );
        const char* type = lua.CheckCString( 1 );
        const char* id = lua.CheckCString( 2 );
        MissionGroup* newGroup = m_groupFactory.Create( type, id );
        m_groupStack.push( newGroup );
        return 0;
    }

    int MissionFactory::l_EndGroup( lua_State* L )
    {
        CLAW_ASSERT( !m_groupStack.empty() );
        MissionGroup* finishedGroup = m_groupStack.top();
        m_groupStack.pop();

        if( m_groupStack.empty() )
        {
            m_manager->AddGroup( finishedGroup );
            if( !finishedGroup->IsCompleted() )
            {
                finishedGroup->SetActive( true );
            }
        }
        else
        {
            m_groupStack.top()->AddMission( finishedGroup );
        }
        return 0;
    }

    int MissionFactory::l_StartMission( lua_State* L )
    {
        CLAW_ASSERT( !m_currentMission );

        Claw::Lua lua( L );
        const char* id = lua.CheckCString( 1 );
        bool autoReward = lua.IsNil( 2 ) ? false : lua.CheckBool( 2 );

        m_currentMission = new Mission( id );
        m_currentMission->SetAutoRewarded( autoReward );
        return 0;
    }

    int MissionFactory::l_EndMission( lua_State* L )
    {
        CLAW_ASSERT( !m_groupStack.empty() );
        CLAW_ASSERT( m_currentMission );

        m_groupStack.top()->AddMission( m_currentMission );
        m_currentMission = NULL;
        return 0;
    }

    int MissionFactory::l_CreateObjective( lua_State* L )
    {
        CLAW_ASSERT( m_currentMission );

        Claw::Lua lua( L );
        const char* type = lua.CheckCString( 1 );

        Objectives::Objective* newObjective = m_objectiveFactory.Create( type );
        CLAW_MSG_ASSERT( newObjective, "Objective not found with type: " << type );

        newObjective->Initialize( L );
        m_currentMission->AddObjective( newObjective );
        return 0;
    }

    int MissionFactory::l_CreateReward( lua_State* L )
    {
        CLAW_ASSERT( m_currentMission );

        Claw::Lua lua( L );
        const char* type = lua.CheckCString( 1 );

        Rewards::Reward* newReward = m_rewardFactory.Create( type, L );
        m_currentMission->AddReward( newReward );
        return 0;
    }
}
#ifndef __INCLUDED__MISSIONS__COMPLETABLE_HPP__
#define __INCLUDED__MISSIONS__COMPLETABLE_HPP__

#include "MonstazAI/GameEvent.hpp"

#include <set>

namespace Missions
{
    class Completable : public GameEventHandler
    {
    public:
        class Observer
        {
        public:
            virtual void OnCompleted( const Completable* mission ) = 0;
        };

        typedef std::set<Observer*> Observers;
        typedef Claw::NarrowString Id;
        typedef Claw::NarrowString Path;

        Completable( const Id& id ) : m_id( id ) {}
        virtual ~Completable() {}
        virtual const char* GetTypeName() const = 0;

        const Id& GetId() const { return m_id; }

        virtual void SetParentPath( const Path& parrentPath );
        const Path& GetPath() const;

        virtual void Update( float dt ) = 0;
        virtual bool IsCompleted() const = 0;
        virtual void Validate() = 0;

        virtual bool IsActive() const = 0;
        virtual void SetActive( bool active ) = 0;

        virtual void Reset() = 0;

        virtual bool SetRegistryBranch( const Claw::NarrowString& branchPath );
        const Claw::NarrowString& GetRegistryBranch() const { return m_regBranch; }

        void RegisterObserver( Observer* observer );
        void UnregisterObserver( Observer* observer );

    protected:
        void NotfiyCompleted();

    private:
        Claw::NarrowString m_regBranch;
        Observers m_observers;
        Id m_id;
        Path m_path;
    };

    inline void Completable::RegisterObserver( Observer* observer )
    {
        m_observers.insert( observer );
    }

    inline void Completable::UnregisterObserver( Observer* observer )
    {
        m_observers.erase( observer );
    }

    inline void Completable::NotfiyCompleted()
    {
        Observers::iterator it = m_observers.begin();
        Observers::iterator end = m_observers.end();
        for( ; it != end; ++it )
        {
            (*it)->OnCompleted( this );
        }
    }

    inline bool Completable::SetRegistryBranch( const Claw::NarrowString& branchPath )
    {
        if( branchPath != m_regBranch )
        {
            m_regBranch = branchPath;
            return true;
        }
        return false;
    }

    inline void Completable::SetParentPath( const Path& parrentPath )
    {
        if( parrentPath.empty() )
        {
            m_path = m_id;
        }
        m_path = parrentPath + "/" + m_id;
    }

    inline const Completable::Path& Completable::GetPath() const
    {
        if( m_path.empty() )
        {
            return m_id;
        }
        return m_path;
    }
}

#endif

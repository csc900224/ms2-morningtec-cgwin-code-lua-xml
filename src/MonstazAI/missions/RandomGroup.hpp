#ifndef __INCLUDED__MISSIONS__RANDOMGROUP_HPP__
#define __INCLUDED__MISSIONS__RANDOMGROUP_HPP__

#include "MonstazAI/missions/MissionGroup.hpp"

namespace Missions
{
    class RandomGroup : public MissionGroup
    {
    public:
        RandomGroup( const Id& id );

        bool IsCompleted() const;

        void SetAutoRandom( bool autoRandom ) { m_autoRandom = autoRandom; }
        bool IsAutoRandom() const { return m_autoRandom; }

        void Randomize();
        void SetActive( bool active );
        void OnCompleted( const Completable* mission );

        bool SetRegistryBranch( const Claw::NarrowString& branchPath );
        void AddMission( Completable* newMission );
        void RemoveMission( Completable* missionToRemove, bool release = true );

    private:
        bool m_autoRandom;
        Completable* m_lastActive;
    };
}

#endif 
#ifndef __INCLUDED__MISSIONS__MISSIONMANAGER_HPP__
#define __INCLUDED__MISSIONS__MISSIONMANAGER_HPP__

#include "MonstazAI/missions/MissionGroup.hpp"
#include "MonstazAI/missions/MissionFactory.hpp"
#include "MonstazAI/missions/Notifier.hpp"
#include "MonstazAI/missions/AchievementsManager.hpp"
#include "MonstazAI/GameEvent.hpp"

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include "jungle/patterns/Singleton.hpp"

#include <map>

namespace Missions
{
    class MissionManager 
        : public Jungle::Patterns::Singleton< MissionManager, Jungle::Patterns::ExplicitCreation>
        , public GameEventHandler
        , Mission::Observer
    {
        friend class Jungle::Patterns::ExplicitCreation<MissionManager>;

    public:
        LUA_DEFINITION( MissionManager );

        typedef std::map< Completable::Id, MissionGroup* > MissionGroups;
        typedef MissionGroups::iterator MissionGroupsIt;
        typedef MissionGroups::const_iterator MissionGroupsConstIt;

        ~MissionManager();

        void InitLua( Claw::Lua* lua, bool setCurrent = false );
        Claw::Lua* GetCurrentLua() const { return m_currentLua; }

        void LoadMissions( const char* luaPath );

        void Reset();
        void Validate();
        void SetPaused( bool paused );

        void Update( float dt );

        void UnlockCachedAchievements();

        bool HandleGameEvent( const GameEvent& ev );

        void AddGroup( MissionGroup* newMission );
        void RemoveGroup( MissionGroup* missionToRemove, bool release = true );

        MissionGroup* GetGroup( const MissionGroup::Id& id ) const;
        const MissionGroups& GetGroups() const { return m_groups; }

        Mission* GetMission( const Claw::NarrowString& fullMissionPath ) const;

        int GetMissionsNumToClaim() const;

        void SetAutoSaveEnabled( bool autoSave );

        void OnMissionProgressChanged( const Mission* mission ) {}
        void OnMissionCompleted( const Mission* mission );
        void OnMissionRewarded( const Mission* mission );
        void OnMissionActivated( const Mission* mission );

        int l_ClaimReward( lua_State* L );
        int l_GetMission( lua_State* L );
        int l_GetGroup( lua_State* L );
        int l_GetGroups( lua_State* L );
        int l_GetGroupTimeLeft( lua_State* L );
        int l_GetMissionsNumToClaim( lua_State* L );

    private:
        typedef void (MissionManager::*MissionProcessor)(Mission*, const Claw::NarrowString&);
        typedef std::map< Claw::NarrowString, Mission* > PathToMissionMap;

        MissionManager( lua_State* L ) : m_factory( this ), m_achievementsMgr( this ) { CLAW_ASSERT( false ); }
        MissionManager();

        void ProcessMissions( MissionGroup* parent, MissionProcessor processor );
        void ProcessMissions( MissionGroup* parent, MissionProcessor processor, const Claw::NarrowString& path );
        void OnMissionAdded( Mission* mission, const Claw::NarrowString& path );
        void OnMissionRemoved( Mission* mission, const Claw::NarrowString& path );

        MissionGroups m_groups;
        PathToMissionMap m_missionsLUT;

        Claw::NarrowString m_registryBranch;

        MissionFactory m_factory;
        Notifier m_notifier;
        AchievementsManager m_achievementsMgr;

        Claw::Lua* m_currentLua;
        volatile bool m_loaded;
        bool m_paused;
        bool m_autoSave;
    };
}

#endif
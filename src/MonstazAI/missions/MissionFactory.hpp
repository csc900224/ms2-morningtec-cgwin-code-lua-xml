#ifndef __INCLUDED__MISSIONS__MISSIONFACTORY_HPP__
#define __INCLUDED__MISSIONS__MISSIONFACTORY_HPP__

#include "MonstazAI/missions/MissionGroup.hpp"
#include "MonstazAI/missions/Objective.hpp"
#include "MonstazAI/missions/Reward.hpp"
#include "MonstazAI/missions/Mission.hpp"

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include "jungle/patterns/Factory.hpp"

#include <stack>

namespace Missions
{
    class MissionManager;

    class MissionFactory
    {
    public:
        LUA_DEFINITION( MissionFactory );

        MissionFactory( lua_State* L ) { CLAW_ASSERT( false ); }
        MissionFactory( MissionManager* manager );

        void InitLua( Claw::Lua* lua );

        int l_StartGroup( lua_State* L );
        int l_EndGroup( lua_State* L );
        int l_StartMission( lua_State* L );
        int l_EndMission( lua_State* L );
        int l_CreateObjective( lua_State* L );
        int l_CreateReward( lua_State* L );

    private:
        typedef Jungle::Patterns::Factory<MissionGroup, Claw::NarrowString, const MissionGroup::Id&> MissionGroupFactory;
        typedef Jungle::Patterns::Factory<Objectives::Objective, Claw::NarrowString> ObjectiveFactory;
        typedef Jungle::Patterns::Factory<Rewards::Reward, Claw::NarrowString, lua_State*> RewardFactory;

        typedef std::stack<MissionGroup*> GroupStack;

        MissionGroupFactory m_groupFactory;
        ObjectiveFactory m_objectiveFactory;
        RewardFactory m_rewardFactory;

        GroupStack m_groupStack;
        Mission* m_currentMission;

        MissionManager* m_manager;
    };
}

#endif
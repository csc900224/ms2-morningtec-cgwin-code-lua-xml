#include "MonstazAI/missions/DailyGroup.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/Application.hpp"

#include "claw/base/Registry.hpp"
#include "claw/application/Time.hpp"

#include <algorithm>

namespace Missions
{
    static const int   RANDOMIZE_INTERVAL   = 24 * 60 * 60;
    static const int   DEFAULT_MISSIONS_NUM = 3;
    static const char* TIMESTAMP_KEY        = "/daily-timestamp";

    int Randomizer (int i) { return g_rng.GetInt()%i; }

    DailyGroup::DailyGroup( const Id& id )
        : MissionGroup( id )
        , m_timeStamp( 0 )
        , m_activeMissionsNum( DEFAULT_MISSIONS_NUM )
    {}

    int DailyGroup::GetTimeLeft()
    {
        Claw::UInt32 now = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime();
        if( now == 0 ) return -1;
        return std::max( 0, m_timeStamp + RANDOMIZE_INTERVAL - (int)now );
    }

    void DailyGroup::Update( float dt )
    {
        MissionGroup::Update( dt );

        Claw::UInt32 now = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime();
        if( now > 0 && m_timeStamp + RANDOMIZE_INTERVAL < now )
        {
            Shuffle();
            Claw::NarrowString regKey = GetRegistryBranch();
            regKey.append( TIMESTAMP_KEY );
            if( Claw::Registry::Get()->Set( regKey.c_str(), (int)now ) )
            {
                m_timeStamp = now;
            }
        }
    }

    bool DailyGroup::SetRegistryBranch( const Claw::NarrowString& branchPath )
    {
        if( MissionGroup::SetRegistryBranch( branchPath ) )
        {
            Claw::NarrowString regKey = GetRegistryBranch();
            regKey.append( TIMESTAMP_KEY );
            Claw::Registry::Get()->Get( regKey.c_str(), m_timeStamp );
            return true;
        }
        return false;
    }

    void DailyGroup::Shuffle()
    {
        CLAW_ASSERT( (int)m_missions.size() >= m_activeMissionsNum );
        if( (int)m_missions.size() >= m_activeMissionsNum )
        {
            Reset();

            std::random_shuffle( m_missions.begin(), m_missions.end(), Randomizer );

            for( int i = 0; i < m_activeMissionsNum; ++i )
            {
                m_missions[i]->SetActive( true );
            }
        }
    }

    void DailyGroup::SetActiveMissionsNum( int numActiveMissions )
    {
        if( numActiveMissions != m_activeMissionsNum )
        {
            m_activeMissionsNum = numActiveMissions;
            Shuffle();
        }
    }

    void DailyGroup::SetActive( bool active )
    {
        if( active != IsActive() )
        {
            if( active )
            {
                Shuffle();
            }
            else
            {
                MissionsIt it = m_missions.begin();
                MissionsIt end = m_missions.end();
                for( ; it != end; ++it )
                {
                    Completable* mission = *it;
                    if( !mission->IsCompleted() )
                    {
                        mission->SetActive( false );
                    }
                }
            }
        }
    }

    bool DailyGroup::IsCompleted() const
    {
        int completed = 0;
        MissionsConstIt it = m_missions.begin();
        MissionsConstIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            if( (*it)->IsCompleted() )
            {
                ++completed;
                if( completed >= m_activeMissionsNum )
                {
                    return true;
                }
            }
        }
        return false;
    }
}
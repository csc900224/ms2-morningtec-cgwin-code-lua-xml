#ifndef __INCLUDED__MISSIONS__MISSION_HPP__
#define __INCLUDED__MISSIONS__MISSION_HPP__

#include "MonstazAI/missions/Completable.hpp"
#include "MonstazAI/missions/Objective.hpp"
#include "MonstazAI/missions/Reward.hpp"

#include "claw/base/String.hpp"
#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include <set>
#include <map>

namespace Missions
{
    class Mission : public Completable, public Objectives::Objective::Observer
    {
    public:
        LUA_DEFINITION( Mission );

        class Observer
        {
        public:
            virtual void OnMissionProgressChanged( const Mission* mission ) = 0;
            virtual void OnMissionCompleted( const Mission* mission ) = 0;
            virtual void OnMissionRewarded( const Mission* mission ) = 0;
            virtual void OnMissionActivated( const Mission* mission ) = 0;
        };

        typedef std::map<Claw::NarrowString, Objectives::Objective *> Objectives;
        typedef Objectives::iterator ObjectivesIt;
        typedef Objectives::const_iterator ObjectivesConstIt;
        typedef std::pair<ObjectivesIt, bool> ObjectivesResult;

        typedef std::set<Rewards::Reward *> Rewards;
        typedef Rewards::iterator RewardsIt;
        typedef Rewards::const_iterator RewardsConstIt;
        typedef std::pair<RewardsIt, bool> RewardsResult;

        typedef std::set<Observer*> Observers;
        typedef Observers::iterator ObserversIt;
        typedef Observers::const_iterator ObserversConstIt;
        typedef std::pair<ObserversIt, bool> ObserversResult;

        enum Status
        {
            S_DISABLED,     // Mission is not currently processed
            S_ACTIVE,       // Mission is active and processed
            S_COMPLETED,    // Mission is completed but reward is not given yet
            S_REWARDED      // Reward for mission was given - mission lifecycle is over
        };

        Mission( const Id& id );
        ~Mission();
        static void InitLua( Claw::Lua* lua );

        static const char* TYPE_NAME;
        const char* GetTypeName() const { return TYPE_NAME; }

        Status GetStatus() const { return m_status; }

        bool IsActive() const;
        void SetActive( bool active );
        bool IsCompleted() const;
        void Validate();

        float GetProgress() const;
        float GetTarget() const;
        float GetValue() const;
        void Reset();

        bool GiveRewards();
        void Update( float dt );

        bool SetRegistryBranch( const Claw::NarrowString& branchPath );

        void SetAutoRewarded( bool autoRewards );
        bool IsAutoRewarded() const { return m_autoRewards; }

        void AddObjective( Missions::Objectives::Objective* newObjective );
        void AddReward( Missions::Rewards::Reward* newReward );

        const Objectives& GetObjectives() const { return m_objectives; }
        const Rewards& GetRewards() const { return m_rewards; }

        void RegisterObserver( Observer* observer );
        void UnregisterObserver( Observer* observer );

        // GameEventHandler
        bool HandleGameEvent( const GameEvent& ev );

        // Objective::Observer
        void OnObjectiveProgressChanged( const Missions::Objectives::Objective* objective );
        void OnObjectiveCompleted( const Missions::Objectives::Objective* objective );

        int l_GetProgress( lua_State* L );
        int l_GetTarget( lua_State* L );
        int l_GetValue( lua_State* L );
        int l_GetStatus( lua_State* L );
        int l_GiveRewards( lua_State* L );
        int l_GetId( lua_State* L );
        int l_GetPath( lua_State* L );

    private:
        typedef void (Observer::*ObserverMethod)(const Mission*);
        void NotifyMissionProgressChanged();
        void NotifyMissionCompleted();
        void NotifyMissionRewarded();
        void NotifyMissionActivated();
        void NotifyObservers( ObserverMethod method );

        void SetStatus( Status newStatus );

        Objectives m_objectives;
        Rewards m_rewards;
        Observers m_observers;

        Claw::NarrowString m_objectivesRegPath;
        Claw::NarrowString m_statusRegPath;

        bool m_autoRewards;

        Status m_status;
    };

    inline bool Mission::IsActive() const
    {
        return GetStatus() == S_ACTIVE || GetStatus() == S_COMPLETED;
    }

    inline bool Mission::IsCompleted() const
    {
        return GetStatus() == S_REWARDED;
    }
}

#endif

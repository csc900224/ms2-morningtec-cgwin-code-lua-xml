#include "MonstazAI/missions/Objective.hpp"

#include "claw/base/Registry.hpp"

#include <algorithm>

namespace Missions
{
namespace Objectives
{
    Objective::Objective()
        : m_value( 0 )
        , m_target( 1 )
    {}

    void Objective::Initialize( lua_State* L )
    {
        Claw::Lua lua( L );
        CLAW_ASSERT( lua.IsTable(-1) );
        lua.PushNil();
        while( lua.TableNext(-2) )
        {
            // copy the key so that lua_tostring does not modify the original
            lua.PushValue(-2);

            Claw::NarrowString key = lua.CheckCString(-1);
            Initialize( key, lua );

            // pop value + copy of key, leaving original key
            lua.Pop( 2 );
        }
        PostInitialize();
    }

    void Objective::Initialize( const Claw::NarrowString& key, Claw::Lua& lua )
    {
        if( key == "target" )
        {
            m_target = (float)lua.CheckNumber(-2);
        }
        else if( key == "value" )
        {
            m_value = (float)lua.CheckNumber(-2);
        }
    }

    bool Objective::IsCompleted() const
    {
        return m_value >= m_target;
    }

    void Objective::Reset()
    {
        m_value = 0;
        if( !m_regBranch.empty() )
        {
            Claw::Registry::Get()->Set( m_regBranch.c_str(), m_value );
        }
    }

    void Objective::SetTarget( float target )
    {
        m_target = target;
        m_value = std::min( m_value, m_target );
    }

    void Objective::SetValue( float value )
    {
        if( m_value != value )
        {
            m_value = value;

            if( !m_regBranch.empty() )
            {
                Claw::Registry::Get()->Set( m_regBranch.c_str(), m_value );
            }

            NotifyObjectiveProgressChanged();
            if( IsCompleted() )
            {
                NotifyObjectiveCompleted();
            }
        }
    }

    void Objective::ChangeValue( float delta )
    {
        SetValue( m_value + delta );
    }

    void Objective::Load()
    {
        if( !m_regBranch.empty() )
        {
            if( !Claw::Registry::Get()->Get( m_regBranch.c_str(), m_value ) )
            {
                Claw::Registry::Get()->Set( m_regBranch.c_str(), m_value );
            }
        }
    }

    void Objective::SetRegistryBranch( const Claw::NarrowString& branchPath )
    {
        if( m_regBranch != branchPath )
        {
            m_regBranch = branchPath;
            m_regBranch.append( "/" );
            m_regBranch.append( GetTypename() );
            Load();
        }
    }

    void Objective::RegisterObserver( Observer* observer )
    {
        ObserversResult result = m_observers.insert( observer );
        CLAW_MSG_WARNING( result.second, "Objective already registred in this objective this mission" );
    }

    void Objective::UnregisterObserver( Observer* observer )
    {
        m_observers.erase( observer );
    }

    void Objective::NotifyObjectiveProgressChanged()
    {
        NotifyObservers( &Observer::OnObjectiveProgressChanged );
    }

    void Objective::NotifyObjectiveCompleted()
    {
        NotifyObservers( &Observer::OnObjectiveCompleted );
    }

    void Objective::NotifyObservers( ObserverMethod method )
    {
        ObserversIt it = m_observers.begin();
        ObserversIt end = m_observers.end();

        for( ; it != end; ++it )
        {
            ((*it)->*method)(this);
        }
    }

    GameEventObjective::GameEventObjective( GameEvent::Id eventId )
        : Objective()
        , m_eventId( eventId )
    {}

    bool GameEventObjective::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == m_eventId )
        {
            // Count event occurrences
            ChangeValue( 1 );
            return true;
        }
        return false;
    }

    GameEventValueObjective::GameEventValueObjective( GameEvent::Id eventId )
        : GameEventObjective( eventId )
    {}

    bool GameEventValueObjective::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == m_eventId )
        {
            // Sum event values
            ChangeValue( ev.GetValue() );
            return true;
        }
        return false;
    }
}
}
#ifndef __INCLUDED__MISSIONS__OBJECTIVE_HPP__
#define __INCLUDED__MISSIONS__OBJECTIVE_HPP__

#include "MonstazAI/GameEvent.hpp"

#include "claw/base/Lua.hpp"

#include <set>

namespace Missions
{
namespace Objectives
{
    class Objective : public GameEventHandler
    {
    public:
        class Observer
        {
        public:
            virtual void OnObjectiveProgressChanged( const Objective* objective ) = 0;
            virtual void OnObjectiveCompleted( const Objective* objective ) = 0;
        };

        typedef std::set<Observer*> Observers;
        typedef Observers::iterator ObserversIt;
        typedef Observers::const_iterator ObserversConstIt;
        typedef std::pair<ObserversIt, bool> ObserversResult;

        typedef const char* Typename;
        virtual Typename GetTypename() const = 0;

        Objective();
        virtual ~Objective() {}

        void Initialize( lua_State* L );

        virtual bool IsCompleted() const;

        float GetProgress() const;
        float GetTarget() const { return m_target; }
        float GetValue() const { return m_value; }

        void SetTarget( float target );
        void SetValue( float value );
        void ChangeValue( float delta );

        void SetRegistryBranch( const Claw::NarrowString& branchPath );

        virtual void Update( float dt ) {}
        virtual void OnActivate() {}

        virtual void Reset();
        virtual void Load();

        void RegisterObserver( Observer* observer );
        void UnregisterObserver( Observer* observer );

    protected:
        const Claw::NarrowString& GetRegistryBranch() const { return m_regBranch; }
        virtual void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        virtual void PostInitialize() {}

    private:
        typedef void (Observer::*ObserverMethod)(const Objective*);
        void NotifyObjectiveProgressChanged();
        void NotifyObjectiveCompleted();
        void NotifyObservers( ObserverMethod method );

        Observers m_observers;

        float m_value;
        float m_target;
        Claw::NarrowString m_regBranch;
    };

    class GameEventObjective : public Objective
    {
    public:
        GameEventObjective( GameEvent::Id eventId );
        GameEventObjective() {}

        bool HandleGameEvent( const GameEvent& ev );

    protected:
         GameEvent::Id m_eventId;
    };

    class GameEventValueObjective : public GameEventObjective
    {
    public:
        GameEventValueObjective( GameEvent::Id eventId );

        bool HandleGameEvent( const GameEvent& ev );
    };

    inline float Objective::GetProgress() const
    { 
        return m_value/m_target; 
    }
}
}

#endif
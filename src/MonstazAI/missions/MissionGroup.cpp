#include <string.h>

#include "MonstazAI/missions/MissionGroup.hpp"
#include "MonstazAI/missions/Mission.hpp"

#include "claw/base/Errors.hpp"

namespace Missions
{
    LUA_DECLARATION( MissionGroup )
    {
        METHOD( MissionGroup, GetMission ),
        METHOD( MissionGroup, GetMissions ),
        METHOD( MissionGroup, GetGroup ),
        METHOD( MissionGroup, GetGroups ),
        METHOD( MissionGroup, IsCompleted ),
        METHOD( MissionGroup, IsActive ),
        METHOD( MissionGroup, GetId ),
        METHOD( MissionGroup, GetPath ),
        {0,0}
    };

    const char* MissionGroup::TYPE_NAME = "MissionGroup";

    MissionGroup::MissionGroup( const Id& id )
        : Completable( id )
    {}

    MissionGroup::~MissionGroup()
    {
        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            delete *it;
        }
    }

    void MissionGroup::InitLua( Claw::Lua* lua )
    {
        Claw::Lunar<MissionGroup>::Register( *lua );
    }

    bool MissionGroup::IsCompleted() const
    {
        bool completed = true;
        MissionsConstIt it = m_missions.begin();
        MissionsConstIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            if( !(*it)->IsCompleted() )
            {
                completed = false;
                break;
            }
        }
        return completed;
    }

    bool MissionGroup::IsActive() const
    {
        bool active = false;
        MissionsConstIt it = m_missions.begin();
        MissionsConstIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            if( (*it)->IsActive() )
            {
                active = true;
                break;
            }
        }
        return active;
    }

    void MissionGroup::Validate()
    {
        if( IsActive() )
        {
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                (*it)->Validate();
            }
        }
    }

    void MissionGroup::Reset()
    {
        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            (*it)->Reset();
        }
    }

    void MissionGroup::AddMission( Completable* newMission )
    {
        m_missions.push_back( newMission );
        m_missionsMap.insert( MissionsMap::value_type( newMission->GetId(), newMission ) );
        newMission->RegisterObserver( this );
        newMission->SetRegistryBranch( m_missionsBranch );
        newMission->SetParentPath( GetPath() );
    }

    Completable* MissionGroup::GetMission( const Completable::Id& id )
    {
        MissionsMapIt it = m_missionsMap.find( id );
        if( it != m_missionsMap.end() )
        {
            return it->second;
        }
        return NULL;
    }

    const Completable* MissionGroup::GetMission( const Completable::Id& id ) const
    {
        MissionsMapConstIt it = m_missionsMap.find( id );
        if( it != m_missionsMap.end() )
        {
            return it->second;
        }
        return NULL;
    }

    void MissionGroup::RemoveMission( Completable* missionToRemove, bool release )
    {
        CLAW_ASSERT( missionToRemove );
        if( missionToRemove )
        {
            missionToRemove->UnregisterObserver( this );

            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                (*it)->SetParentPath( "" );
                m_missions.erase( it );
                break;
            }

            MissionsMapIt mapIt = m_missionsMap.find( missionToRemove->GetId() );
            if( mapIt != m_missionsMap.end() )
            {
                m_missionsMap.erase( mapIt );
            }

            if( release )
            {
                delete missionToRemove;
            }
        }
    }

    void MissionGroup::Update( float dt )
    {
        if( IsActive() )
        {
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                Completable* misison = *it;
                if( misison->IsActive() && !misison->IsCompleted() )
                {
                    misison->Update( dt );
                }
            }
        }
    }

    void MissionGroup::OnCompleted( const Completable* mission )
    {
        if( IsCompleted() )
        {
            NotfiyCompleted();
        }
    }

    bool MissionGroup::HandleGameEvent( const GameEvent& ev )
    {
        bool retValue = false;
        if( IsActive() )
        {
            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                Completable* misison = *it;
                if( misison->IsActive() && !misison->IsCompleted() )
                {
                    retValue |= misison->HandleGameEvent( ev );
                }
            }
        }
        return retValue;
    }

    bool MissionGroup::SetRegistryBranch( const Claw::NarrowString& branchPath )
    {
        if( Completable::SetRegistryBranch( branchPath ) )
        {
            m_missionsBranch = branchPath;
            m_missionsBranch.append( "/" );
            m_missionsBranch.append( GetId() );

            MissionsIt it = m_missions.begin();
            MissionsIt end = m_missions.end();
            for( ; it != end; ++it )
            {
                (*it)->SetRegistryBranch( m_missionsBranch );
            }

            return true;
        }
        return false;
    }

    void MissionGroup::SetParentPath( const Path& parrentPath )
    {
        Completable::SetParentPath( parrentPath );

        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();
        for( ; it != end; ++it )
        {
            (*it)->SetParentPath( GetPath() );
        }
    }

    int MissionGroup::l_GetMission( lua_State* L )
    {
        Claw::Lua lua( L );
        Claw::NarrowString missionId = lua.CheckCString( 1 );
        Completable* mission = GetMission( missionId );
        if( mission && !strcmp(mission->GetTypeName(), Mission::TYPE_NAME) )
        {
            Claw::Lunar<Mission>::push( L, (Mission*)mission );
        }
        else
        {
            lua.PushNil();
        }
        return 1;
    }

    int MissionGroup::l_GetMissions( lua_State* L )
    {
        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();

        lua_newtable( L );
        int idx = 1;

        for( ; it != end; ++it )
        {
            if( !strcmp((*it)->GetTypeName(), Mission::TYPE_NAME) )
            {
                lua_pushinteger( L, idx++ );
                Claw::Lunar<Mission>::push( L, (Mission*)*it );
                lua_settable( L, -3 );
            }
        }
        return 1;
    }

    int MissionGroup::l_GetGroup( lua_State* L )
    {
        Claw::Lua lua( L );
        Claw::NarrowString missionId = lua.CheckCString( 1 );
        Completable* mission = GetMission( missionId );
        if( mission && !strcmp(mission->GetTypeName(), MissionGroup::TYPE_NAME) )
        {
            Claw::Lunar<MissionGroup>::push( L, (MissionGroup*)mission );
        }
        else
        {
            lua.PushNil();
        }
        return 1;
    }

    int MissionGroup::l_GetGroups( lua_State* L )
    {
        MissionsIt it = m_missions.begin();
        MissionsIt end = m_missions.end();

        lua_newtable( L );
        int idx = 1;

        for( ; it != end; ++it )
        {
            if( !strcmp((*it)->GetTypeName(), MissionGroup::TYPE_NAME) )
            {
                lua_pushinteger( L, idx++ );
                Claw::Lunar<MissionGroup>::push( L, (MissionGroup*)*it );
                lua_settable( L, -3 );
            }
        }
        return 1;
    }

    int MissionGroup::l_IsCompleted( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushBool( IsCompleted() );
        return 1;
    }

    int MissionGroup::l_IsActive( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushBool( IsActive() );
        return 1;
    }

    int MissionGroup::l_GetId( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushString( GetId() );
        return 1;
    }

    int MissionGroup::l_GetPath( lua_State* L )
    {
        Claw::Lua lua( L );
        lua.PushString( GetPath() );
        return 1;
    }
}
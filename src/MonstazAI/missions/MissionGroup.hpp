#ifndef __INCLUDED__MISSIONS__MISSIONGROUP_HPP__
#define __INCLUDED__MISSIONS__MISSIONGROUP_HPP__

#include "MonstazAI/missions/Completable.hpp"

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

#include <vector>
#include <map>

namespace Missions
{
    class MissionGroup : public Completable, public Completable::Observer
    {
    public:
        LUA_DEFINITION( MissionGroup );

        typedef std::vector<Completable*> Missions;
        typedef Missions::iterator MissionsIt;
        typedef Missions::const_iterator MissionsConstIt;

        typedef std::map<Completable::Id, Completable*> MissionsMap;
        typedef MissionsMap::iterator MissionsMapIt;
        typedef MissionsMap::const_iterator MissionsMapConstIt;

        MissionGroup( const Id& id );
        virtual ~MissionGroup();
        static void InitLua( Claw::Lua* lua );

        static const char* TYPE_NAME;
        const char* GetTypeName() const { return TYPE_NAME; }

        void Update( float dt );
        bool IsCompleted() const;
        void Validate();
        bool IsActive() const;
        void Reset();

        virtual void AddMission( Completable* newMission );
        virtual void RemoveMission( Completable* missionToRemove, bool release = true );

        Completable* GetMission( const Completable::Id& id );
        const Completable* GetMission( const Completable::Id& id ) const;

        Missions& GetMissions() { return m_missions; }
        const Missions& GetMissions() const { return m_missions; }

        bool SetRegistryBranch( const Claw::NarrowString& branchPath );
        void SetParentPath( const Path& parrentPath );

        void OnCompleted( const Completable* mission );

        bool HandleGameEvent( const GameEvent& ev );

        int l_GetMission( lua_State* L );
        int l_GetMissions( lua_State* L );
        int l_GetGroup( lua_State* L );
        int l_GetGroups( lua_State* L );
        int l_IsCompleted( lua_State* L );
        int l_IsActive( lua_State* L );
        int l_GetId( lua_State* L );
        int l_GetPath( lua_State* L );

    protected:
        Missions m_missions;
        MissionsMap m_missionsMap;
        Claw::NarrowString m_missionsBranch;
    };
}

#endif
#ifndef __INCLUDED__MISSIONS__DAILYGROUP_HPP__
#define __INCLUDED__MISSIONS__DAILYGROUP_HPP__

#include "MonstazAI/missions/MissionGroup.hpp"

namespace Missions
{
    class DailyGroup : public MissionGroup
    {
    public:
        DailyGroup( const Id& id );

        void Update( float dt );
        bool SetRegistryBranch( const Claw::NarrowString& branchPath );
        int GetTimeLeft();

        void SetActiveMissionsNum( int numActiveMissions );
        int GetActiveMissionsNum() const { return m_activeMissionsNum; }

        void SetActive( bool active );
        bool IsCompleted() const;

    private:
        void Shuffle();

        int m_timeStamp;
        int m_activeMissionsNum;
    };
}

#endif
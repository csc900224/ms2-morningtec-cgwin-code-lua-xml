#ifndef __INCLUDED__MISSIONS__OBJECTIVES_HPP__
#define __INCLUDED__MISSIONS__OBJECTIVES_HPP__

#include "MonstazAI/missions/Objective.hpp"
#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/shot/Shot.hpp"
#include "MonstazAI/AtlasManager.hpp"
#include "MonstazAI/PickupManager.hpp"

namespace Missions
{
namespace Objectives
{
    class Bullets : public GameEventValueObjective
    {
    public:
        Bullets();

        Typename GetTypename() const { return "Bullets"; }
        bool HandleGameEvent( const GameEvent& ev );
    };

    class KillEnemy : public Objective
    {
    public:
        typedef std::vector<Entity::Type> EntityTypes;
        typedef std::vector<Shot::Type> ShotTypes;

        KillEnemy();

        void SetEntityType( const EntityTypes& entityTypes );
        void SetShotType( const ShotTypes shotTypes );
        void SetElements( Claw::UInt8 elements ) { m_elements = elements; }

        Typename GetTypename() const { return "KillEnemy"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        void PostInitialize();

    private:
        EntityTypes m_entityTypes;
        ShotTypes m_shotTypes;
        Claw::UInt8 m_elements;
    };

    class DestroyObject : public Objective
    {
    public:
        DestroyObject() {}

        void SetObjectName( const char* objectName ) { m_objectName = objectName; }

        Typename GetTypename() const { return "ObjectDestroyed"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );

    private:
        Claw::NarrowString m_objectName;
    };

    class CompleteLevel : public Objective
    {
    public:
        CompleteLevel();

        void SetTypes( bool story, bool boss, bool survival, bool tutorial );
        void SetEnviroment( AtlasSet::Type enviroment ) { m_enviroment = enviroment; }

        Typename GetTypename() const { return "CompleteLevel"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        void PostInitialize();
        void OnActivate();

    private:
        bool m_story;
        bool m_boss;
        bool m_survival;
        bool m_tutorial;
        int m_stage;

        bool m_interested;
        AtlasSet::Type m_enviroment;
    };

    class CompleteTutorial : public GameEventObjective
    {
    public:
        CompleteTutorial();
        Typename GetTypename() const { return "CompleteTutorial"; }
    };

    class PlayWithFriend : public GameEventObjective
    {
    public:
        PlayWithFriend();
        Typename GetTypename() const { return "PlayWithFriend"; }
    };

    class CollectCash : public Objective
    {
    public:
        CollectCash();

        Typename GetTypename() const { return "CollectCash"; }
        bool HandleGameEvent( const GameEvent& ev );

    private:
        bool m_gamePlay;
    };

    class HoldFire : public Objective
    {
    public:
        HoldFire();

        void Update( float dt );

        Typename GetTypename() const { return "HoldFire"; }
        bool HandleGameEvent( const GameEvent& ev );

    private:
        float m_timer;
        bool m_gamePlay;
    };

    class Pickup : public Objective
    {
    public:
        Pickup();

        void SetPickupType( const ::Pickup::Type type ) { m_pickupType = type; }

        Typename GetTypename() const { return "Pickup"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );

    private:
        ::Pickup::Type m_pickupType;
    };

    class Survive : public Objective
    {
    public:
        Survive();

        void Update( float dt );

        Typename GetTypename() const { return "Survive"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        void PostInitialize();

    private:
        bool m_pause;
        float m_timer;
        bool m_story;
        bool m_boss;
        bool m_survival;
    };

    class UsePerk : public GameEventObjective
    {
    public:
        UsePerk();

        Typename GetTypename() const { return "UsePerk"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );

    private:
        bool m_singleRound;
    };

    class ItemObjective : public Objective
    {
    public:
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        typedef std::vector<Claw::NarrowString> Items;
        typedef std::set<GameEvent::Id> Events;

        ItemObjective( const GameEvent::Id& evId );
        ItemObjective() {}

        void SetEvents( const Events& events ) { m_events = events; }
        bool CheckEvent( const GameEvent& ev );
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        void PostInitialize();

        Items m_items;

    private:
        Events m_events;
    };

    class ShopItemBuy : public ItemObjective
    {
    public:
        ShopItemBuy();
        Typename GetTypename() const { return "ShopItemBuy"; }
        void OnActivate();
    };

    class ShopItemUse : public ItemObjective
    {
    public:
        ShopItemUse() : ItemObjective( GEI_ITEM_USED ) {}
        Typename GetTypename() const { return "ShopItemUse"; }
    };

    class ShopItemUpgrade : public ItemObjective
    {
    public:
        ShopItemUpgrade() : ItemObjective( GEI_ITEM_UPGRADED ) {}
        Typename GetTypename() const { return "ShopItemUpgrade"; }
    };

    class ShopItemUpgradeMax : public ItemObjective
    {
    public:
        ShopItemUpgradeMax();
        Typename GetTypename() const { return "ShopItemUpgradeMax"; }
        bool HandleGameEvent( const GameEvent& ev );
        void OnActivate();
    };

    class KillBoss : public Objective
    {
    public:
        KillBoss();

        void SetBoss( Entity::Type type, int stage );

        Typename GetTypename() const { return "KillBoss"; }
        bool HandleGameEvent( const GameEvent& ev );
        void OnActivate();

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        void PostInitialize();

    private:
        Entity::Type m_bossType;
        int m_stage;
    };

    class FireBothWeapons : public Objective
    {
    public:
        FireBothWeapons();

        Typename GetTypename() const { return "FireBothWeapons"; }
        bool HandleGameEvent( const GameEvent& ev );

    private:
        int m_lastShot;
    };

    class Score : public Objective
    {
    public:
        Score();

        Typename GetTypename() const { return "Score"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );

    private:
        Claw::NarrowString m_map;
        bool m_interested;
        bool m_singleRound;
    };

    class ManualAiming : public Objective
    {
    public:
        ManualAiming() {}

        Typename GetTypename() const { return "ManualAiming"; }
        bool HandleGameEvent( const GameEvent& ev );
    };

    class Revive : public GameEventObjective
    {
    public:
        Revive();
        Typename GetTypename() const { return "Revive"; }
    };

    class SettingsTab : public Objective
    {
    public:
        SettingsTab();

        Typename GetTypename() const { return "SettingsTab"; }
        bool HandleGameEvent( const GameEvent& ev );

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );

    private:
        int m_tabIdx;
    };

    class AddFriend : public Objective
    {
    public:
        AddFriend() {}
        Typename GetTypename() const { return "AddFriend"; }
        bool HandleGameEvent( const GameEvent& ev );
    };

    class SendGift : public GameEventObjective
    {
    public:
        SendGift();
        Typename GetTypename() const { return "SendGift"; }
    };

    class ReciveGift : public GameEventObjective
    {
    public:
        ReciveGift();
        Typename GetTypename() const { return "ReciveGift"; }
    };

    class PlaySurvival : public Objective
    {
    public:
        PlaySurvival();

        Typename GetTypename() const { return "PlaySurvival"; }
        bool HandleGameEvent( const GameEvent& ev );
        void OnActivate();

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );
        void CheckCompleted();

    private:
        int m_numLevels;
        bool m_interested;
    };

    class EnterEndless : public Objective
    {
    public:
        EnterEndless();

        Typename GetTypename() const { return "EnterEndless"; }
        bool HandleGameEvent( const GameEvent& ev );
        void OnActivate();

    protected:
        void Initialize( const Claw::NarrowString& key, Claw::Lua& lua );

    private:
        bool m_lastLevel;
        int m_endlessStage;
        Claw::NarrowString m_lastLevelName;
    };
}
}

#endif
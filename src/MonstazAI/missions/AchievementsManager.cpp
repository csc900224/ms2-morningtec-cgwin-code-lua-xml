#include "MonstazAI/missions/AchievementsManager.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/network/googleservices/GoogleServices.hpp"
#include "MonstazAI/Application.hpp"

namespace Missions
{

    AchievementsManager::AchievementsManager( MissionManager* mm )
        : m_missionManager( mm )
    {
    }

    void AchievementsManager::OnMissionActivated( const Mission* mission )
    {
        Claw::NarrowString p = mission->GetPath();
        if( p.find( "Ranks" ) != p.npos )
        {
            UnlockAchievement( (mission)->GetId() );
        }
    }

    void AchievementsManager::UnlockCachedAchievements()
    {
        MissionGroup* grp = m_missionManager->GetGroup( "Ranks" );
        const MissionGroup::Missions& m = grp->GetMissions();
        for( MissionGroup::MissionsConstIt it = m.begin(), end = m.end(); it != end; ++it )
        {
            if( (*it)->IsActive() || (*it)->IsCompleted() )
            {
                UnlockAchievement( (*it)->GetId() );
            }
        }
    }

    void AchievementsManager::UnlockAchievement( const Claw::NarrowString& rank )
    {
        Claw::LuaPtr lua( m_missionManager->GetCurrentLua() );
        Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
        if( lua && gs->IsAuthenticated() )
        {
            lua->PushString( rank );
            lua->Call( "GetAchievementId", 1, 1 );
            Claw::NarrowString googleId = lua->CheckString( 1 );
            lua->Pop( 1 );
            if( !googleId.empty() )
            {
                gs->UnlockAchievement( googleId.c_str() );
            }
        }
    }

}
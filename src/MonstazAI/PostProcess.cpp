#include "claw/graphics/Batcher.hpp"

#include "MonstazAI/PostProcess.hpp"

PostProcess::PostProcess( const char* vertex, const char* fragment )
    : m_shader( new Claw::OpenGLShader() )
    , m_active( false )
{
    m_shader->Load( vertex, fragment );
}

PostProcess::~PostProcess()
{
}

void PostProcess::Render( Claw::Surface* src, Claw::Surface* dst )
{
    Claw::g_batcher->SetShader( m_shader );
    dst->Blit( 0, 0, src );
    Claw::g_batcher->SetShader( NULL );
}

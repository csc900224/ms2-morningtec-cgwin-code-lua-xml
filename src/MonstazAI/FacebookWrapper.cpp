// Internal includes
#include "MonstazAI/FacebookWrapper.hpp"

#include "MonstazAI/network/social/Facebook.hpp"

static FacebookWrapper* s_instance = NULL;

static const char* FACEBOOK_APP_ID    = "264018613640822";

FacebookWrapper::FacebookWrapper()
    : m_facebook( NULL )
{
    m_facebook = Facebook::QueryInterface( FACEBOOK_APP_ID );
}

FacebookWrapper::~FacebookWrapper()
{
    if( m_facebook )
    {
        Facebook::Release( m_facebook );
    }
}

FacebookWrapper* FacebookWrapper::GetInstance()
{
    if( !s_instance )
    {
        s_instance = new FacebookWrapper;
    }
    return s_instance;
}

void FacebookWrapper::Release()
{
    CLAW_ASSERT( s_instance );

    if( s_instance )
    {
        delete s_instance;
        s_instance = NULL;
    }
}

bool FacebookWrapper::Authenticate()
{
    CLAW_ASSERT( m_facebook );
    return m_facebook->Authenticate();
}

bool FacebookWrapper::PublishScore( int score )
{
    CLAW_ASSERT( m_facebook );
    return false;
}

bool FacebookWrapper::PublishLevel( int level )
{
    CLAW_ASSERT( m_facebook );
    return false;
}

/////////////////////////////////////////////////////////////////////
// Lua interface
/////////////////////////////////////////////////////////////////////

LUA_DECLARATION( FacebookWrapper )
{
    METHOD( FacebookWrapper, PublishScore ),
    METHOD( FacebookWrapper, PublishLevel ),
    {0,0}
};

FacebookWrapper::FacebookWrapper( lua_State* L )
{
    CLAW_ASSERT( false );
}

void FacebookWrapper::Init( Claw::Lua* lua )
{
    Claw::Lunar<FacebookWrapper>::Register( *lua );
    Claw::Lunar<FacebookWrapper>::push( *lua, this );
    lua->RegisterGlobal( "FacebookWrapper" );
}

int FacebookWrapper::l_PublishScore( lua_State* L )
{
    Claw::Lua lua( L );
    PublishScore( lua.CheckNumber( 1 ) );
    return 0;
}

int FacebookWrapper::l_PublishLevel( lua_State* L )
{
    Claw::Lua lua( L );
    PublishLevel( lua.CheckNumber( 1 ) );
    return 0;
}

// EOF

#include "claw/compat/Platform.h"
#include "claw/graphics/pixeldata/PixelDataGL.hpp"
#include "claw/graphics/Batcher.hpp"

#include "MonstazAI/Heat.hpp"


static const char* vertex =
    "void main(void)\n"
    "{\n"
    "TEXTURE;\n"
    "GLPOSITION;\n"
    "}";

static const char* fragment =
    "uniform lowp sampler2D heat;\n"
    "void main(void)\n"
    "{\n"
    "mediump vec2 t = texture2D( heat, vTex ).rg * 0.1 - 0.05;\n"
    "gl_FragColor = texture2D( clawTex, vTex + t );\n"
    "}";


Heat::Heat()
{
    m_shader.Load( vertex, fragment );
}

void Heat::Render( Claw::Surface* src, Claw::Surface* dst, float x, float y, const Claw::Rect& clipRect )
{
    Claw::PixelDataGL* pd = (Claw::PixelDataGL*)src->GetPixelData();
    Claw::PixelDataGL* dpd = (Claw::PixelDataGL*)dst->GetPixelData();
    Claw::g_batcher->SetShader( &m_shader );
    m_shader.Uniform( "heat", 2 );
    Claw::g_batcher->SetTexture( 2, m_heat->GetPixelData(), m_heat->QueryFlag() );

    Claw::g_batcher->SetTexturing( true );
    Claw::g_batcher->SetDestination( (Claw::PixelDataGL*)dst->GetPixelData(), dst->QueryFlag() );
    Claw::g_batcher->SetSource( pd, src->QueryFlag(), src->GetAlpha() );
    Claw::g_batcher->SetClipping( dst->GetClipRect() );
    Claw::g_batcher->SetDrawingMode( Claw::Batcher::Blending );
    Claw::g_batcher->SetColorKey( false, 0, 0, 0 );
    Claw::g_batcher->SetPrimitiveType( Claw::Batcher::Triangle );

    float x0 = x + src->GetXOffset();
    float y0 = y + src->GetYOffset();

    float x1, x2, y1, y2;

    x1 = (float)( clipRect.m_x + src->GetStartX() ) * pd->m_scaleX;
    x2 = x1 + (float)clipRect.m_w * pd->m_scaleX;
    y1 = (float)( clipRect.m_y + src->GetStartY() ) * pd->m_scaleY;
    y2 = y1 + (float)clipRect.m_h * pd->m_scaleY;

    Claw::g_batcher->Queue(
        Claw::BatcherVertex( x0,                y0,                x1, y1, 255, 255, 255, 255 ),
        Claw::BatcherVertex( x0 + clipRect.m_w, y0,                x2, y1, 255, 255, 255, 255 ),
        Claw::BatcherVertex( x0 + clipRect.m_w, y0 + clipRect.m_h, x2, y2, 255, 255, 255, 255 ),
        Claw::BatcherVertex( x0,                y0 + clipRect.m_h, x1, y2, 255, 255, 255, 255 ) );

    Claw::g_batcher->SetShader( NULL );
}

#ifndef __MONSTAZ_RENDERABLE_HPP__
#define __MONSTAZ_RENDERABLE_HPP__

#include <list>
#include <vector>

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"

class Renderable
{
public:
    Renderable() : m_pos( 0, 0 ) {}
    Renderable( float x, float y ) : m_pos( x, y ) {}
    Renderable( const Vectorf& pos ) : m_pos( pos ) {}
    virtual ~Renderable();

    inline Vectorf& GetPos() { return m_pos; }
    inline const Vectorf& GetPos() const { return m_pos; }

    virtual void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const = 0;

protected:
    Vectorf m_pos;
};


class RenderableManager : public Claw::RefCounter
{
    enum { MaxRenderables = 8 * 1024 };

public:
    RenderableManager();
    ~RenderableManager();

    void Add( const Renderable* renderable ) { CLAW_ASSERT( renderable ); m_list.push_back( renderable ); }
    void Render( Claw::Surface* target, const Vectorf& offset );

private:
    std::vector<const Renderable*> m_list;
    const Renderable** m_sortBuf;
};

typedef Claw::SmartPtr<RenderableManager> RenderableManagerPtr;

#endif

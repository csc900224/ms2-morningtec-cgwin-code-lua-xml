#ifndef __MONSTAZ_OBJECTRECTANGLE_HPP__
#define __MONSTAZ_OBJECTRECTANGLE_HPP__

#include "claw/base/Errors.hpp"
#include "claw/base/Lua.hpp"
#include "MonstazAI/math/Vector.hpp"

class ObjectRectangle
{
public:
    ObjectRectangle() { CLAW_ASSERT( false ); }
    ObjectRectangle( const Vectorf& edge, float perpendicular );

    const Vectorf& GetEdge() const { return m_edge; }
    const Vectorf& GetPerp() const { return m_perp; }
    float GetEdgeLen() const { return m_edgeLen; }
    float GetPerpLen() const { return m_perpLen; }

    int l_GetEdge( lua_State* L );
    int l_GetPerpendicular( lua_State* L );

protected:
    Vectorf m_edge;
    Vectorf m_perp;
    float m_edgeLen;
    float m_perpLen;
};

#endif

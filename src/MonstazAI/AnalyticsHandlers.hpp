#ifndef __INCLUDED__ANALYTICSHANDLERS__HPP__
#define __INCLUDED__ANALYTICSHANDLERS__HPP__

#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/TutorialManager.hpp"

#include <map>

namespace AnalyticsHandlers
{
    class Install : public AnalyticsManager::EventHandler
    {
    public:
        Install() {}
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev ) { return false; }
    };

    class Age : public AnalyticsManager::EventHandler
    {
    public:
        Age() {}
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );
    };

    class Tutorial : public AnalyticsManager::EventHandler
    {
    public:
        Tutorial();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        typedef std::map<TutorialTask::Id, const char*> TaskLabels;
        typedef std::map<TutorialChapter::Id, TaskLabels> TutorialLabelsMap;

        TutorialLabelsMap m_labels;
        bool m_itemUsed;
        bool m_smgSwitched;
    };

    class Progress : public AnalyticsManager::EventHandler
    {
    public:
        Progress();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        void AddProgressInfo( EventHierarchy& eh );
        int GetLevelCashReward() const;
        Claw::NarrowString GenerateMatchText() const;

    private:
        enum Mode
        {
            M_NONE,
            M_STORY,
            M_ENDLESS,
            M_SURVIVAL
        };

        AnalyticsManager* m_manager;
        Mode m_mode;
        bool m_wasRestarted;
    };

    class Weapons : public AnalyticsManager::EventHandler
    {
    public:
        Weapons();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        AnalyticsManager* m_manager;
    };

    class ShopItems : public AnalyticsManager::EventHandler
    {
    public:
        ShopItems();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        AnalyticsManager* m_manager;
        bool m_gamePlay;
    };

    class NoFunds : public AnalyticsManager::EventHandler
    {
    public:
        NoFunds();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        AnalyticsManager* m_manager;
        Claw::NarrowString m_itemId;
        bool m_active;
        bool m_gamePlay;
    };

    class Cash : public AnalyticsManager::EventHandler
    {
    public:
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );
    };

    class FreeStuff : public AnalyticsManager::EventHandler
    {
    public:
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );
    };

    class Perks : public AnalyticsManager::EventHandler
    {
    public:
        Perks();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        AnalyticsManager* m_manager;
    };

    class Missions : public AnalyticsManager::EventHandler
    {
    public:
        Missions();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        AnalyticsManager* m_manager;
    };

    class SummaryOffer : public AnalyticsManager::EventHandler
    {
    public:
        SummaryOffer();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        bool m_offerShown;
    };

    class LimitedTimeOffer : public AnalyticsManager::EventHandler
    {
    public:
        LimitedTimeOffer();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        bool m_active;
        Claw::NarrowString m_offerType;
        Claw::NarrowString m_productId;
    };

    class SocialLogin : public AnalyticsManager::EventHandler
    {
    public:
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        Claw::NarrowString m_loginMethod;
    };

    class SocialFriend : public AnalyticsManager::EventHandler
    {
    public:
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );
    };

    class SocialGift : public AnalyticsManager::EventHandler
    {
    public:
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );
    };

    class Fuel : public AnalyticsManager::EventHandler
    {
    public:
        Fuel();
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        bool m_ranout;
    };

    class NewWeapon : public AnalyticsManager::EventHandler
    {
    public:
        void Initialize( AnalyticsManager* manager );
        bool HandleGameEvent( const GameEvent& ev );

    private:
        Claw::NarrowString m_weaponId;
    };
}

#endif 
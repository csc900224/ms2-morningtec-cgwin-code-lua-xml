#ifndef __MONSTAZ_TOUCHCONTROLS_HPP__
#define __MONSTAZ_TOUCHCONTROLS_HPP__

#include <vector>

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/FontEx.hpp"
#include "claw/graphics/ScreenText.hpp"

#include "MonstazAI/math/Vector.hpp"

class TouchControls : public Claw::RefCounter
{
    enum { InactiveTouch = -2 };

public:
    struct TouchData
    {
        TouchData() : button( InactiveTouch ), visible( false ) {}

        Vectorf pos;
        Vectorf target;
        Vectorf center;
        Vectorf xperia_center;
        int button;
        bool visible;
        float time;
    };

    TouchControls();
    ~TouchControls();

    void Render( Claw::Surface* target );
    void Update( float dt );
    void Reset();

    void OnTouchUp( int x, int y, int button );
    void OnTouchDown( int x, int y, int button );
    void OnTouchMove( int x, int y, int button );

    void SetNumWeapons( int weaponsNum ) { m_weaponsNum = weaponsNum; }

    void ResetDeadTimer( float time ) { m_deadTimer = std::max( time, m_deadTimer ); }
    float GetDeadTimer() const { return m_deadTimer; }

    const TouchData& GetTouchData( int id ) const { CLAW_ASSERT( id == 0 || id == 1 ); return m_data[id]; }

    float GetTouchRadius() const { return TouchRadius; }
    float GetTouchActiveRadius() const { return TouchActiveRadius; }

    Vectorf GetMedkitPosition() const;
    Vectorf GetShieldPosition() const;
    Vectorf GetMinePosition() const;
    Vectorf GetGrenadePosition() const;
    Vectorf GetSwapPosition() const;
    Vectori GetSwapSize() const;

    void AddWeapon( const Claw::SurfacePtr& weapon );
    void WeaponSwitch();

    void ShowFinger( bool show );

private:
    void OnDisplayTouchDown( int x, int y, int button );
    void OnDisplayTouchMove( int x, int y, int button );

    void OnXperiaTouchDown( int x, int y, int button );
    void OnXperiaTouchMove( int x, int y, int button );

    void ModeSwitched();

    static void FixedVPadSwitchCallback( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node ) { ((TouchControls*)ptr)->FixedVPadSwitch(); }
    void FixedVPadSwitch();

    void TouchHandled( int id ) { if( id >= 0 && id < 10 ) m_touchHandled[id] = true; }

    TouchData m_data[2];
    Vectorf m_center;

    float m_scale;
    float m_leftBtnTime;
    float m_rightBtnTime;
    float m_mineBtnTime;
    float m_shieldBtnTime;
    float m_switchBtnTime;

    float TouchActiveRadius;
    float TouchRadius;
    float TouchShotRadius;
    float TouchNipple;
    float TouchBorder;
    float LimitY;

    float TouchOffset;
    float TouchSquare;
    float TouchSlideSquare;
    float TouchShotSquare;
    float TouchActiveSquare;

    Claw::SurfacePtr m_bg;
    Claw::SurfacePtr m_decorL;
    Claw::SurfacePtr m_decorR;
    Claw::SurfacePtr m_left;
    Claw::SurfacePtr m_right;
    Claw::SurfacePtr m_grenade[3];
    Claw::SurfacePtr m_health[3];
    Claw::SurfacePtr m_mine[3];
    Claw::SurfacePtr m_shield[3];
    Claw::SurfacePtr m_switch[2];
    Claw::SurfacePtr m_finger;
    Claw::SurfacePtr m_arrow;

    Claw::FontExPtr m_font;
    Claw::ScreenTextPtr m_healthText;
    Claw::ScreenTextPtr m_grenadeText;
    Claw::ScreenTextPtr m_mineText;
    Claw::ScreenTextPtr m_shieldText;
    int m_leftNum;
    int m_rightNum;
    int m_mineNum;
    int m_shieldNum;

    bool m_fixed;
    bool m_autoaim;

    float m_deadTimer;
    int m_weaponsNum;

    int m_slideButton;
    float m_slidePos;
    float m_slideTimer;

    bool m_fingerVisible;
    float m_fingerTimer;

    bool m_touchHandled[10];

    std::vector<Claw::SurfacePtr> m_weapons;
};

typedef Claw::SmartPtr<TouchControls> TouchControlsPtr;

#endif

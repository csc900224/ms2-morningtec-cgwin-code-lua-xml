#include "MonstazAI/AnalyticsHandlers.hpp"
#include "MonstazAI/GameEvent.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/missions/Mission.hpp"
#include "MonstazAI/GameManager.hpp"

#include "claw/base/Registry.hpp"

#include <sstream>

namespace AnalyticsHandlers
{
    void Install::Initialize( AnalyticsManager* manager )
    {
        bool firstRun = true;
        Claw::Registry::Get()->Get( "/internal/firstrun", firstRun );

        if( firstRun )
        {
            LogDesignEvent( "Install", 1 );
        }
    }

    void Age::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_AGE_CHECKED, this );
    }

    bool Age::HandleGameEvent( const GameEvent& ev )
    {
        EventHierarchy eh;
        eh.push_back( "Age verification" );
        if( ev.GetValue() == GEP_AGE_GTE_8 )
        {
            eh.push_back( "older" );
        }
        else if( ev.GetValue() == GEP_AGE_LT_8 )
        {
            eh.push_back( "younger" );
        }
        else
        {
            CLAW_ASSERT( !"Unknown event param " )
        }
        LogDesignEvent( GenerateEvent( eh ) );
        return true;
    }

    const char* TUTORIAL_MASTER_EVENT = "Tutorial";

    Tutorial::Tutorial()
        : m_itemUsed( false )
        , m_smgSwitched( false )
    {}

    void Tutorial::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_TUTORIAL_STARTED, this );
        manager->RegisterHandler( GEI_TUTORIAL_COMPLETED, this );
        manager->RegisterHandler( GEI_TUTORIAL_TASK_COMPLETED, this );

        // Intro labels
        TaskLabels& labels1 = m_labels[TutorialChapter::Intro];
        labels1[TutorialTask::IntroSummary]             = "02_Level_completed";

        // Shop
        TaskLabels& labels2 = m_labels[TutorialChapter::Shop];
        labels2[TutorialTask::ShopEnter]                = "03_shop_button";
        labels2[TutorialTask::ShopBuyShotgun]           = "04_weapon_purchased";

        // Premission
        TaskLabels& labels3 = m_labels[TutorialChapter::Premission];
        labels3[TutorialTask::PremissionEquipShotgun]   = "05_second_slot_equipped";
        labels3[TutorialTask::PremissionUpgradeSmg]     = "06_SMG_upgraded";
        labels3[TutorialTask::PremissionPlay]           = "07_second_level_started";

        // Gameplay
        TaskLabels& labels4 = m_labels[TutorialChapter::Gameplay];
        labels4[TutorialTask::GameplayChangeWeapon]     = "08_gun_changed_to_shotgun";
        labels4[TutorialTask::GameplaySelectPerk]       = "09_perk_chosen";
        labels4[TutorialTask::GameplayLevelUp]          = "11_level_up_2";

        // Challenges
        TaskLabels& labels5 = m_labels[TutorialChapter::Challenges];
        labels5[TutorialTask::ChallengesPlay]           = "12_third_level_started";

        // Items
        TaskLabels& labels6 = m_labels[TutorialChapter::Items];
        labels6[TutorialTask::ItemsPause]               = "13_pause_pressed";
        labels6[TutorialTask::ItemsBuyMine]             = "14_grenade_and_mine_purchased";
        labels6[TutorialTask::ItemsKillEnemies]         = "16_third_level_finished";

        // Mech
        TaskLabels& labels7 = m_labels[TutorialChapter::Mech];
        labels7[TutorialTask::MechPlay]                 = "17_fourth_level_started";

        // Final
        TaskLabels& labels8 = m_labels[TutorialChapter::Final];
        labels8[TutorialTask::FinalKillEnemies]          = "18_fourth_level_finished";
    }

    bool Tutorial::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_TUTORIAL_STARTED )
        {
            bool firstRun = true;
            Claw::Registry::Get()->Get( "/internal/firstrun", firstRun );

            EventHierarchy eh;
            eh.push_back( TUTORIAL_MASTER_EVENT );
            eh.push_back( "00_begin" );
            eh.push_back( firstRun ? "started" : "restarted" );

            LogDesignEvent( GenerateEvent( eh ) );
            return true;
        }
        else if( ev.GetId() == GEI_TUTORIAL_COMPLETED )
        {
            EventHierarchy eh;
            eh.push_back( TUTORIAL_MASTER_EVENT );
            eh.push_back( "19_end" );
            LogDesignEvent( GenerateEvent( eh ) );
        }
        else if( ev.GetId() == GEI_TUTORIAL_TASK_COMPLETED )
        {
            const TutorialChapter::Id chapter = TutorialManager::GetInstance()->GetCurrentChapter();
            const TutorialTask::Id task = (TutorialTask::Id)(int)ev.GetValue();

            EventHierarchy eh;
            eh.push_back( TUTORIAL_MASTER_EVENT );

            if( chapter == TutorialChapter::Intro && (task == TutorialTask::IntroPlayerShot || task == TutorialTask::IntroPlayerMove) )
            {
                if( TutorialManager::GetInstance()->IsTaskCompleted( TutorialTask::IntroPlayerShot ) &&
                    TutorialManager::GetInstance()->IsTaskCompleted( TutorialTask::IntroPlayerMove ) )
                {
                    eh.push_back( "01_Both_sticks_used" );
                }
            }
            else if( !m_itemUsed && chapter == TutorialChapter::Items && (task == TutorialTask::ItemsUseGrenade || task == TutorialTask::ItemsUseMine) )
            {
                eh.push_back( "15_first_item_used" );
                if( task == TutorialTask::ItemsUseGrenade )
                {
                    eh.push_back( "grenade" );
                }
                else if( task == TutorialTask::ItemsUseMine )
                {
                    eh.push_back( "mine" );
                }
                else
                {
                    CLAW_ASSERT( !"Unknown item used!" );
                }
                m_itemUsed = true;
            }
            else if( !m_smgSwitched && chapter == TutorialChapter::Gameplay && task == TutorialTask::GameplayUseSmg )
            {
                m_smgSwitched = true;
                eh.push_back( "10_gun_changed_back_to_smg" );
            }
            else
            {
                TutorialLabelsMap::iterator it = m_labels.find( chapter );
                if( it != m_labels.end() )
                {
                    TaskLabels::iterator labelIt = it->second.find( task );
                    if( labelIt != it->second.end() )
                    {
                        eh.push_back( labelIt->second );
                    }
                }
            }

            if( eh.size() > 1 )
            {
                LogDesignEvent( GenerateEvent( eh ) );
                return true;
            }
        }
        return false;
    }

    Progress::Progress()
        : m_manager( NULL )
        , m_mode( M_NONE )
        , m_wasRestarted( false )
    {}

    void Progress::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_LEVEL_STARTED, this );
        manager->RegisterHandler( GEI_LEVEL_RESTARTED, this );
        manager->RegisterHandler( GEI_REVIVE_PLAYER, this );
        manager->RegisterHandler( GEI_LEVEL_FINISHED, this );

        m_manager = manager;
    }

    bool Progress::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_mode = M_NONE;
            if( ev.GetValue() == GEP_LEVEL_STARTED_STORY || ev.GetValue() == GEP_LEVEL_STARTED_BOSSFIGHT )
            {
                bool endless = false;
                Claw::Registry::Get()->Get( "/internal/endless", endless );
                m_mode = endless ? M_ENDLESS : M_STORY;
            }
            else if( ev.GetValue() == GEP_LEVEL_STARTED_SURVIVAL )
            {
                m_mode = M_SURVIVAL;
            }
        }

        // If not story mode - give up
        if( m_mode == M_NONE ) return false;

        EventHierarchy eh;
        switch( m_mode )
        {
        case M_ENDLESS: eh.push_back( "Endless" ); break;
        case M_STORY: eh.push_back( "Story" ); break;
        case M_SURVIVAL: eh.push_back( "Survival" ); break;
        }
        int earned = 0;

        if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            if( m_wasRestarted )
            {
                eh[0] += " restarted";
                m_wasRestarted = false;
            }
            else
            {
                eh[0] += " started";
            }
        }
        else if( ev.GetId() == GEI_REVIVE_PLAYER )
        {
            eh[0] += " revived";
        }
        else if( ev.GetId() == GEI_LEVEL_RESTARTED )
        {
            m_wasRestarted = true;
            return true;
        }
        else if( ev.GetId() == GEI_LEVEL_FINISHED )
        {
            if( ev.GetValue() == GEP_LEVEL_FINISHED_SUCCESS )
            {
                eh[0] += " finished";
                earned = GetLevelCashReward();
            }
            else if( ev.GetValue() == GEP_LEVEL_FINISHED_ABORT )
            {
                eh[0] += " quit";
            }
            else
            {
                if( m_mode == M_SURVIVAL )
                {
                    eh[0] += " finished - coins";
                    earned = GetLevelCashReward();
                }
                else
                {
                    eh[0] += " died";
                }
            }
        }

        AddProgressInfo( eh );
        LogDesignEvent( GenerateEvent( eh ), earned, GenerateMatchText().c_str() );

        // Additional evnts with points and time for survival
        if( m_mode == M_SURVIVAL && ev.GetId() == GEI_LEVEL_FINISHED && ev.GetValue() == GEP_LEVEL_FINISHED_FAIL )
        {
            eh[0] = "Survival finished - points";
            int points = GameManager::GetInstance()->GetStats()->GetPoints();
            LogDesignEvent( GenerateEvent( eh ), points, GenerateMatchText().c_str() );

            eh[0] = "Survival finished - time";
            float time = 0;
            Claw::Registry::Get()->Get( "/internal/storytime", time );
            LogDesignEvent( GenerateEvent( eh ), time, GenerateMatchText().c_str() );
        }
        return true;
    }

    Claw::NarrowString Progress::GenerateMatchText() const
    {
        int missingElement = 0;
        Claw::Registry::Get()->Get( "/internal/missingelements/1", missingElement );
        bool elementsMatch = missingElement == 0;

        bool withFriend = false;
        Claw::Registry::Get()->Get( "/internal/friend", withFriend );

        Claw::NarrowString matchText = elementsMatch ? "match" : "no match";
        matchText += " ";
        matchText += withFriend ? "friend" : "no friend";
        return matchText;
    }

    int Progress::GetLevelCashReward() const
    {
        int reward = 0;
        Claw::Registry::Get()->Get( "/internal/reward/soft", reward );

        int collected = 0;
        Claw::Registry::Get()->Get( "/internal/money", collected );

        return reward + collected;
    }

    void Progress::AddProgressInfo( EventHierarchy& eh )
    {
        std::ostringstream eventStream;
        if( m_mode == M_ENDLESS )
        {
            int playerLevel = 0;
            Claw::Registry::Get()->Get( "/monstaz/player/level", playerLevel );
            eventStream << "Player level " << playerLevel;
            eh.push_back( eventStream.str() );
        }
        else
        {
            eventStream.str( "" );
            eventStream << "level " << m_manager->GetLevelUID();
            eh.push_back( eventStream.str() );
        }
    }

    Weapons::Weapons()
        : m_manager( NULL )
    {}

    void Weapons::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_LEVEL_STARTED, this );
        m_manager = manager;
    }

    bool Weapons::HandleGameEvent( const GameEvent& ev )
    {
        for( int i = 1; true; ++i )
        {
            std::ostringstream regKey;
            regKey << "/monstaz/weaponselection/" << i;

            const char* weapon;
            Claw::Registry::Get()->Get( regKey.str().c_str(), weapon );

            if( strcmp( weapon, "" ) == 0 )
            {
                break;
            }
            else
            {
                Claw::NarrowString levelUID( "level " );
                levelUID += m_manager->GetLevelUID();

                std::ostringstream weaponId;
                weaponId << weapon;
                int upgradeLevel = Shop::GetInstance()->GetUpgrades( weapon );
                if( upgradeLevel > 0 )
                {
                    weaponId << "_" << upgradeLevel;
                }

                EventHierarchy eh;
                eh.push_back( ev.GetValue() == GEP_LEVEL_STARTED_SURVIVAL ? "Weapon used in survival mission" : "Weapon used in story mission" );
                eh.push_back( weaponId.str() );

                int stage = 0;
                Claw::Registry::Get()->Get( "/maps/current", stage );
                Claw::StdOStringStream stageText;
                stageText << "Stage " << stage;
                eh.push_back( stageText.str() );

                LogDesignEvent( GenerateEvent( eh ), 1.0f, levelUID.c_str() );
            }
        }
        return true;
    }

    ShopItems::ShopItems()
        : m_manager( NULL )
        , m_gamePlay( false )
    {}

    void ShopItems::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_ITEM_BOUGHT, this );
        manager->RegisterHandler( GEI_ITEM_UNLOCKED, this );
        manager->RegisterHandler( GEI_ITEM_UPGRADED, this );
        manager->RegisterHandler( GEI_ITEM_USED, this );
        manager->RegisterHandler( GEI_LEVEL_STARTED, this );
        manager->RegisterHandler( GEI_LEVEL_FINISHED, this );
        m_manager = manager;
    }

    bool ShopItems::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_FINISHED )
        {
            m_gamePlay = false;
        }
        else if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_gamePlay = true;
        }
        else
        {
            const Claw::NarrowString item = ev.GetText();
            const Shop::ItemCategory category = Shop::GetInstance()->GetItemCategory( item );
            Claw::NarrowString levelUID( "level " );
            levelUID += m_manager->GetLevelUID();

            Claw::NarrowString categoryName;
            switch( category )
            {
            case Shop::IC_WEAPON:
                categoryName = "Weapon";
                break;
            case Shop::IC_ITEM:
                categoryName = "Item";
                break;
            case Shop::IC_PERK:
                categoryName = "Perk Permanent";
                break;
            default:
                CLAW_ASSERT( false );
            }

            Claw::NarrowString actionName;
            switch( ev.GetId() )
            {
            case GEI_ITEM_BOUGHT:
                actionName = "Purchased";
                break;
            case GEI_ITEM_UNLOCKED:
                actionName = "Unlocked";
                break;
            case GEI_ITEM_UPGRADED:
                actionName = "Upgraded";
                break;
            case GEI_ITEM_USED:
                actionName = "Used";
                break;
            default:
                CLAW_ASSERT( false );
            }

            EventHierarchy eh;
            eh.push_back( categoryName + " " + actionName );
            if( category == Shop::IC_ITEM && ev.GetId() == GEI_ITEM_USED )
            {
                bool survival = false;
                Claw::Registry::Get()->Get( "/internal/survival", survival );
                eh[0] += survival ? " in survival" : " in story";
            }

            eh.push_back( item );
            if( ev.GetId() == GEI_ITEM_UPGRADED )
            {
                std::ostringstream weaponUpgrade;
                weaponUpgrade << item << "_" << (int)ev.GetValue();
                eh[eh.size()-1] = weaponUpgrade.str();
            }
            else if( category == Shop::IC_ITEM && ev.GetId() == GEI_ITEM_BOUGHT )
            {
                eh[0] += m_gamePlay ? " in pause" : " in shop";
            }

            int stage = 0;
            Claw::Registry::Get()->Get( "/maps/current", stage );
            Claw::StdOStringStream stageText;
            stageText << "Stage " << stage;
            eh.push_back( stageText.str() );

            LogDesignEvent( GenerateEvent( eh ), 1.0f, levelUID.c_str() );
        }
        return true;
    }

    NoFunds::NoFunds()
        : m_active( false )
        , m_gamePlay( false )
    {}

    void NoFunds::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_ITEM_CANT_AFFORD, this );
        manager->RegisterHandler( GEI_NO_CASH_ACTION, this );
        manager->RegisterHandler( GEI_CASH_ACTION_FINISHED, this );
        manager->RegisterHandler( GEI_TRANSACTION_CANCELED, this );
        manager->RegisterHandler( GEI_TRANSACTION_COMPLETED, this );
        manager->RegisterHandler( GEI_TRANSACTION_FAILED, this );
        manager->RegisterHandler( GEI_LEVEL_FINISHED, this );
        manager->RegisterHandler( GEI_LEVEL_STARTED, this );
        manager->RegisterHandler( GEI_SHOP_STATE_CHANGED, this );
        manager->RegisterHandler( GEI_ITEM_BOUGHT, this );
        manager->RegisterHandler( GEI_ITEM_UNLOCKED, this );
        manager->RegisterHandler( GEI_ITEM_UPGRADED, this );
    }

    bool NoFunds::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_FINISHED )
        {
            m_gamePlay = false;
            return true;
        }
        else if( ev.GetId() == GEI_LEVEL_STARTED )
        {
            m_gamePlay = true;
            return true;
        }

        if( !m_active )
        {
            if( ev.GetId() == GEI_ITEM_CANT_AFFORD )
            {
                m_active = true;
                m_itemId = ev.GetText();
                return true;
            }
        }
        else
        {
            EventHierarchy eh;
            eh.push_back( "Not enough funds" );
            eh.push_back( m_gamePlay ? "pause" : "shop" );

            if( ev.GetId() == GEI_NO_CASH_ACTION && ev.GetValue() == GEP_NO_CASH_BACK )
            {
                // Back button on popup was pressed
                m_active = false;
                LogDesignEvent( GenerateEvent( eh ), 0, m_itemId.c_str() );
                return true;
            }
            else if( ev.GetId() == GEI_SHOP_STATE_CHANGED || 
                     ev.GetId() == GEI_ITEM_BOUGHT || 
                     ev.GetId() == GEI_ITEM_UNLOCKED || 
                     ev.GetId() == GEI_ITEM_UPGRADED )
            {
                // Shop was closed or action on other item was performed
                m_active = false;
                eh.push_back( "Get button clicked" );
                LogDesignEvent( GenerateEvent( eh ), 0, m_itemId.c_str() );
                return true;
            }
            else if( ev.GetId() == GEI_TRANSACTION_FAILED || ev.GetId() == GEI_TRANSACTION_CANCELED )
            {
                // IAP was seleted but transaction failed/canceled
                m_active = false;
                eh.push_back( "Get button clicked" );
                eh.push_back( "displayed" );
                eh.push_back( ((ClawExt::InAppTransaction*)ev.GetUserData())->GetProductId() );
                LogDesignEvent( GenerateEvent( eh ), 0, m_itemId.c_str() );
                return true;
            }
            else if( ev.GetId() == GEI_TRANSACTION_COMPLETED || ev.GetId() == GEI_CASH_ACTION_FINISHED )
            {
                // Purchase process was completed
                m_active = false;
                eh.push_back( "Get button clicked" );
                eh.push_back( "purchased" );
                if( ev.GetUserData() )
                {
                    eh.push_back( ((ClawExt::InAppTransaction*)ev.GetUserData())->GetProductId() );
                }
                else
                {
                    // Could be Tapjoy transaction
                    eh.push_back( ev.GetText() );
                }
                LogDesignEvent( GenerateEvent( eh ), 0, m_itemId.c_str() );
                return true;
            }
        }
        return false;
    }

    void Cash::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_TRANSACTION_COMPLETED, this );
        manager->RegisterHandler( GEI_TRANSACTION_STARTED, this );
        manager->RegisterHandler( GEI_CASH_ACTION_STARTED, this );
        manager->RegisterHandler( GEI_CASH_ACTION_FINISHED, this );
        manager->RegisterHandler( GEI_CASH_ACTION_SUBSCRIPTION_CANELED, this );
    }

    bool Cash::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_TRANSACTION_STARTED )
        {
            Claw::NarrowString iapId = ev.GetText();
            float usdPrice = Shop::GetInstance()->GetRealItemPriceInUSD( iapId );

            EventHierarchy eh;
            eh.push_back( "IAP purchase attempt" );
            eh.push_back( iapId );
            LogDesignEvent( GenerateEvent( eh ) );
        }
        else if( ev.GetId() == GEI_TRANSACTION_COMPLETED )
        {
            ClawExt::InAppTransaction* transaction = (ClawExt::InAppTransaction*)ev.GetUserData();
            float usdPrice = Shop::GetInstance()->GetRealItemPriceInUSD( transaction->GetProductId() );

            EventHierarchy eh;
            eh.push_back( "Cash purchase completed IAP" );
            eh.push_back( transaction->GetProductId() );
            LogBusinessEvent( GenerateEvent( eh ), "USD", usdPrice );
        }
        else if( ev.GetId() == GEI_TRANSACTION_FAILED || ev.GetId() == GEI_TRANSACTION_CANCELED )
        {
            ClawExt::InAppTransaction* transaction = (ClawExt::InAppTransaction*)ev.GetUserData();
            EventHierarchy eh;
            eh.push_back( "IAP purchase fail/cancel" );
            eh.push_back( transaction->GetProductId() );
            LogDesignEvent( GenerateEvent( eh ) );
        }
        else if( ev.GetId() == GEI_CASH_ACTION_STARTED )
        {
            EventHierarchy eh;
            eh.push_back( "Cash action initialized" );
            eh.push_back( ev.GetText() );
            LogDesignEvent( GenerateEvent( eh ) );
        }
        else if( ev.GetId() == GEI_CASH_ACTION_FINISHED )
        {
            // Design event
            EventHierarchy eh;
            eh.push_back( "Cash action completed" );
            eh.push_back( ev.GetText() );
            float value = ev.GetValue();
            if( ev.GetText() == "Tapjoy" )
            {
                value = (int)(value * Shop::GetInstance()->GetTapjoyConversionRate() + 0.5f);
            }
            LogDesignEvent( GenerateEvent( eh ), value );

            // Business event
            Claw::NarrowString eventName = "Cash purchase completed";
            eventName += " ";
            eventName += ev.GetText();
            LogBusinessEvent( eventName, "USD", value );
        }
        else if( ev.GetId() == GEI_CASH_ACTION_SUBSCRIPTION_CANELED )
        {
            EventHierarchy eh;
            eh.push_back( "IAP subscription canceled" );
            eh.push_back( ev.GetText() );
            LogDesignEvent( GenerateEvent( eh ) );
        }
        return true;
    }

    void FreeStuff::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_FREE_STUFF_ACTION, this );
    }

    bool FreeStuff::HandleGameEvent( const GameEvent& ev )
    {
        EventHierarchy eh;
        eh.push_back( "Free stuff clicked" );
        switch( (GameEventParam)(int)ev.GetValue() )
        {
        case GEP_FREE_STUFF_FACEBOOK:   eh.push_back( "like" ); break;
        case GEP_FREE_STUFF_TWITTER:    eh.push_back( "follow" ); break;
        case GEP_FREE_STUFF_RATE:       eh.push_back( "rate" ); break;
        case GEP_FREE_STUFF_SOUNDTRACK: eh.clear(); eh.push_back( "Soundtrack button clicked" ); break;
        default: CLAW_ASSERT( !"Unsupported action" ); return false;
        }
        LogDesignEvent( GenerateEvent( eh ) );
        return true;
    }

    Perks::Perks()
        : m_manager( NULL )
    {}

    void Perks::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_PERK_USED, this );
        m_manager = manager;
    }

    bool Perks::HandleGameEvent( const GameEvent& ev )
    {
        bool survival = false;
        Claw::Registry::Get()->Get( "/internal/survival", survival );

        Claw::NarrowString levelUID( "level " );
        levelUID += m_manager->GetLevelUID();

        EventHierarchy eh;
        eh.push_back( "Perk" );
        eh.push_back( survival ? "survival" : "story" );
        eh.push_back( ev.GetText() );
        LogDesignEvent( GenerateEvent( eh ), 1.0f, levelUID.c_str() );
        return true;
    }

    Missions::Missions()
        : m_manager( NULL )
    {}

    void Missions::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_MISSION_COMPLETED, this );
        m_manager = manager;
    }

    bool Missions::HandleGameEvent( const GameEvent& ev )
    {
        const ::Missions::Mission* mission = (const ::Missions::Mission*)ev.GetUserData();
        Claw::NarrowString missionPath = mission->GetPath();
        
        EventHierarchy eh;
        size_t pos = 0;
        size_t lastPos = 0;

        // Split mission path to event hierarchy
        bool end = false;
        while( !end )
        {
            end = (pos = missionPath.find( "/", pos )) == std::string::npos;
            Claw::NarrowString event = missionPath.substr( lastPos, (end ? missionPath.size() : pos) - lastPos );
            if( eh.empty() )
            {
                eh.push_back( event );
                eh.push_back( "" );
            }
            else if( eh[1].empty() )
            {
                eh[1] = event;
            }
            else
            {
                eh[1] += "_" + event;
            }
            lastPos = ++pos;
        }

        bool isMission = true;
        bool isDaily = false;
        bool isStroy = false;

        // Events tweeaks
        if( eh[0] == "DailyMissions" )
        {
            eh[0] = "Daily Mission Detailed completed";
            isDaily = true;
        }
        else if( eh[0] == "Ranks" )
        {
            eh[0] = "Rank gained";
            isMission = false;
        }
        else if( eh[0] == "MainMissions" )
        {
            eh[0] = "Mission completed";
            isStroy= true;
        }

        Claw::NarrowString levelUID( "level " );
        levelUID += m_manager->GetLevelUID();

        // level UID as area
        LogDesignEvent( GenerateEvent( eh ), 0, levelUID.c_str() );

        if( isDaily )
        {
            // Find out which daily mission was it completed/claimed today
            ::Missions::MissionGroup* dailyMissionsGroup = ::Missions::MissionManager::GetInstance()->GetGroup( "DailyMissions" );
            const ::Missions::MissionGroup::Missions& missions = dailyMissionsGroup->GetMissions();

            int completedMissionNum = 0;
            ::Missions::MissionGroup::Missions::const_iterator it = missions.begin();
            ::Missions::MissionGroup::Missions::const_iterator end = missions.end();

            for( ; it != end; ++it )
            {
                ::Missions::MissionGroup::Missions::const_iterator it2 = ((::Missions::MissionGroup*)(*it))->GetMissions().begin();
                ::Missions::MissionGroup::Missions::const_iterator end2 = ((::Missions::MissionGroup*)(*it))->GetMissions().end();

                for( ; it2 != end2; ++it2 )
                {
                    ::Missions::Mission* mission = (::Missions::Mission*)*it2;
                    if( mission->GetStatus() == ::Missions::Mission::S_COMPLETED )
                    {
                        ++completedMissionNum;
                    }
                }
            }

            EventHierarchy eh;
            eh.push_back( "Daily Mission completed" );

            std::ostringstream ss;
            ss << completedMissionNum;
            eh.push_back( ss.str() );

            LogDesignEvent( GenerateEvent( eh ) );
        }
        return true;
    }

    SummaryOffer::SummaryOffer()
        : m_offerShown( false )
    {}

    void SummaryOffer::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_SUMMARY_ACTION, this );
        manager->RegisterHandler( GEI_LEVEL_FINISHED, this );
    }

    bool SummaryOffer::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LEVEL_FINISHED )
        {
            bool survival = false;
            Claw::Registry::Get()->Get( "/internal/survival", survival );
            m_offerShown = ev.GetValue() == GEP_LEVEL_FINISHED_FAIL && !survival;
            return true;
        }
        else if( m_offerShown && ev.GetId() == GEI_SUMMARY_ACTION )
        {
            EventHierarchy eh;
            eh.push_back( "Weapon offer death displayed" );

            GameEventParam param = (GameEventParam)(int)ev.GetValue();
            switch (param)
            {
            case GEP_SUMARY_SHOP:
            case GEP_SUMARY_RESTART:
            case GEP_SUMARY_BACK:
                break;
            case GEP_SUMARY_ITEM_1:
            case GEP_SUMARY_ITEM_2:
                eh.push_back( "clicked" );
                break;
            default:
                CLAW_ASSERT( !"Unknown action!" );
                break;
            }
            LogDesignEvent( GenerateEvent( eh ) );
            return true;
        }
        return false;
    }

    LimitedTimeOffer::LimitedTimeOffer()
        : m_active( false )
    {}

    void LimitedTimeOffer::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_LTO_SHOWN, this );
        manager->RegisterHandler( GEI_LTO_ACTION, this );
        manager->RegisterHandler( GEI_SHOP_STATE_CHANGED, this );
        manager->RegisterHandler( GEI_ITEM_BOUGHT, this );
        manager->RegisterHandler( GEI_ITEM_UNLOCKED, this );
    }

    bool LimitedTimeOffer::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_LTO_SHOWN )
        {
            m_active = true;
            m_offerType.clear();
            m_productId.clear();
            return true;
        }
        else if( m_active )
        {
            EventHierarchy eh;
            eh.push_back( "Hot offer displayed" );

            if( ev.GetId() == GEI_LTO_ACTION )
            {
                if( ev.GetValue() == GEP_LTO_BACK )
                {
                    m_active = false;
                }
                else
                {
                    if( ev.GetValue() == GEP_LTO_GET_HARD )
                    {
                        m_offerType = "hard";
                    }
                    else if( ev.GetValue() == GEP_LTO_GET_SOFT )
                    {
                        m_offerType = "soft";
                    }
                    else if( ev.GetValue() == GEP_LTO_GET_REGULAR )
                    {
                        m_offerType = "regular";
                    }
                    else
                    {
                        CLAW_ASSERT( !"Unknown action!" );
                    }
                    m_productId = ev.GetText();
                    return true;
                }
            }
            else if( ev.GetId() == GEI_SHOP_STATE_CHANGED && ev.GetValue() == GEP_SHOP_CLOSE )
            {
                m_active = false;
                eh.push_back( "Get button clicked" );
            }
            else if( (ev.GetId() == GEI_ITEM_BOUGHT || ev.GetId() == GEI_ITEM_UNLOCKED) && 
                     ev.GetText() == m_productId )
            {
                m_active = false;
                eh.push_back( "Get button clicked" );
                eh.push_back( "Purchased" );
            }

            if( !m_active )
            {
                if( m_offerType.size() )
                {
                    eh.push_back( m_offerType );
                }
                LogDesignEvent( GenerateEvent( eh ) );
                return true;
            }
        }
        return false;
    }

    void SocialLogin::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_SOCIAL_LOGIN_SKIP, this );
        manager->RegisterHandler( GEI_SOCIAL_LOGIN_SUCCEED, this );
        manager->RegisterHandler( GEI_SOCIAL_LOGIN_TRY, this );
    }

    bool SocialLogin::HandleGameEvent( const GameEvent& ev )
    {
        EventHierarchy eh;
        eh.push_back( "Social button new" );

        if( ev.GetId() == GEI_SOCIAL_LOGIN_SKIP )
        {
            eh.push_back( "skip" );
        }
        else if( ev.GetId() == GEI_SOCIAL_LOGIN_TRY )
        {
            if( ev.GetValue() == GEP_LOGIN_FACEBOOK )
            {
                m_loginMethod = "facebook";
            }
            else if( ev.GetValue() == GEP_LOGIN_GOOGLE_PLUS )
            {
                m_loginMethod = "google_plus";
            }
            else if( ev.GetValue() == GEP_LOGIN_LOGIN )
            {
                m_loginMethod = "login";
            }
            else if( ev.GetValue() == GEP_LOGIN_CREATE )
            {
                m_loginMethod = "create";
            }
        }
        else if( ev.GetId() == GEI_SOCIAL_LOGIN_SUCCEED )
        {
            eh.push_back( "succesfull" );
            eh.push_back( m_loginMethod );
        }

        if( eh.size() > 1 )
        {
            LogDesignEvent( GenerateEvent( eh ) );
        }
        return true;
    }

    void SocialFriend::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_FRIEND_INVITED, this );
        manager->RegisterHandler( GEI_FRIEND_INVITATION_CANCELED, this );
        manager->RegisterHandler( GEI_FRIEND_INVITATION_CONFIRMED, this );
    }

    bool SocialFriend::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_FRIEND_INVITATION_CONFIRMED )
        {
            LogDesignEvent( "Friend added", ev.GetValue() );
        }
        else
        {
            EventHierarchy eh;
            eh.push_back( "Social button" );
            eh.push_back( "Add Friend" );

            if( ev.GetId() == GEI_FRIEND_INVITED )
            {
                eh.push_back( "invited" );
            }
            else
            {
                eh.push_back( "cancelled" );
            }
            LogDesignEvent( GenerateEvent( eh ) );
        }
        return true;
    }

    void SocialGift::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_FRIEND_GIFT_RECIVED, this );
        manager->RegisterHandler( GEI_FRIEND_PROFILE_ACTION ,this );
    }

    bool SocialGift::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_FRIEND_GIFT_RECIVED )
        {
            LogDesignEvent( "Gift received", ev.GetValue() );
            return true;
        }
        else
        {
            EventHierarchy eh;
            eh.push_back( "Social button" );
            eh.push_back( "View profile" );

            if( ev.GetValue() == GEP_PROFILE_CLOSE )
            {
                eh.push_back( "send gift unavailable" );
            }
            else if( ev.GetValue() == GEP_PROFILE_CLOSE_WITHOUT_GIFT )
            {
                eh.push_back( "send gift available" );
            }
            else if( ev.GetValue() == GEP_PROFILE_SEND_GIFT )
            {
                eh.push_back( "send gift available" );
                eh.push_back( "gift sent" );
            }

            if( eh.size() > 2 )
            {
                LogDesignEvent( GenerateEvent( eh ) );
                return true;
            }
        }
        return false;
    }

    Fuel::Fuel()
        : m_ranout( false )
    {}

    void Fuel::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_FUEL_SHOWN, this );
        manager->RegisterHandler( GEI_FUEL_ACTION, this );
    }

    bool Fuel::HandleGameEvent( const GameEvent& ev )
    {
        if( ev.GetId() == GEI_FUEL_SHOWN )
        {
            m_ranout = ev.GetValue() == GEP_FUEL_RAN_OUT;
        }
        else
        {
            EventHierarchy eh;
            eh.push_back( m_ranout ? "Fuel ran out" : "Fuel HUD" );

            if( ev.GetValue() == GEP_FUEL_REFIL )
            {
                eh.push_back( "refil" );
            }
            else if( ev.GetValue() == GEP_FUEL_USE_GIFT )
            {
                eh.push_back( "use gift" );
            }
            else if( ev.GetValue() == GEP_FUEL_WAIT )
            {
                eh.push_back( "wait" );
            }
            else if( ev.GetValue() == GEP_FUEL_INVITE )
            {
                eh.push_back( "invite" );
                eh.push_back( ev.GetText() );
            }

            LogDesignEvent( GenerateEvent( eh ) );
        }
        return true;
    }

    void NewWeapon::Initialize( AnalyticsManager* manager )
    {
        manager->RegisterHandler( GEI_NEW_WEAPON_ACTION, this );
        manager->RegisterHandler( GEI_NEW_WEAPON_SHOWN, this );
        manager->RegisterHandler( GEI_SHOP_STATE_CHANGED, this );
        manager->RegisterHandler( GEI_ITEM_BOUGHT, this );
    }

    bool NewWeapon::HandleGameEvent( const GameEvent& ev )
    {
        EventHierarchy eh;

        if( ev.GetId() == GEI_NEW_WEAPON_SHOWN )
        {
            m_weaponId = ev.GetText();
            return true;
        }
        else if( !m_weaponId.empty() )
        {
            if( ev.GetId() == GEI_NEW_WEAPON_ACTION && ev.GetValue() == GEP_NEW_WEAPON_BACK )
            {
                eh.push_back( "New weapon displayed");
            }
            else if( ev.GetId() == GEI_SHOP_STATE_CHANGED && ev.GetValue() == GEP_SHOP_CLOSE )
            {
                eh.push_back( "New weapon displayed");
                eh.push_back( "clicked" );
            }
            else if( ev.GetId() == GEI_ITEM_BOUGHT && ev.GetText() == m_weaponId )
            {
                eh.push_back( "New weapon displayed");
                eh.push_back( "clicked" );
                eh.push_back( "purchased" );
            }

            if( eh.size() > 0 )
            {
                eh.push_back( m_weaponId );
                LogDesignEvent( GenerateEvent( eh ) );
                m_weaponId.clear();
                return true;
            }
        }
        return false;
    }
}
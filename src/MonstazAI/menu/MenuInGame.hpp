#ifndef __MONSTAZ_MENUINGAME_HPP__
#define __MONSTAZ_MENUINGAME_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "guif/Screen.hpp"

#include "MonstazAI/PostProcess.hpp"

class MenuInGame : public Claw::RefCounter
{
public:
    LUA_DEFINITION( MenuInGame );
    MenuInGame( lua_State* L ) { CLAW_ASSERT( false ); }

    MenuInGame();
    ~MenuInGame();

    void Update( float dt );
    void Render( Claw::Surface* target );

    bool OnTouchDown( int x, int y, int button );
    bool OnTouchUp( int x, int y, int button );
    bool OnTouchMove( int x, int y, int button );
    void KeyPress( Claw::KeyCode code );

    void StartPerkMenu();
    void StartPauseMenu();
    void StartSummary( int level, int time, int kills, int multi, int score, bool fail );
    void StartTutorial( int id );
    void StartStoryTutorial( const Claw::NarrowString& text );
    void SetupTutorials();
    void StartRevive();

    bool IsPerkIconVisible() const;

    Claw::Lua* GetLua() { return m_screen->GetLuaState(); }

    int l_PerkSelected( lua_State* L );
    int l_StopPerkMenu( lua_State* L );
    int l_StopSummary( lua_State* L );
    int l_RestartLevel( lua_State* L );
    int l_NextLevel( lua_State* L );
    int l_GoToShop( lua_State* L );
    int l_LogPerkName( lua_State* L );
    int l_EndEvent( lua_State* L );
    int l_Reload( lua_State* L );
    int l_PerkMenu( lua_State* L );
    int l_Nuke( lua_State* L );
    int l_NextWeapon( lua_State* L );
    int l_PrevWeapon( lua_State* L );
    int l_NumWeapons( lua_State* L );
    int l_Save( lua_State* L );
    int l_VideoAd( lua_State* L );
    int l_GetControlSize( lua_State* L );
    int l_Playhaven( lua_State* L );
    int l_GetNumPerks( lua_State* L );
    int l_Revive( lua_State* L );
    int l_GetTime( lua_State* L );
    int l_GetNetworkTime( lua_State* L );
    int l_GetVersionString( lua_State* L );
    int l_OpenAchievementsUI( lua_State* L );

    void Dump() { m_screen->DumpState(); }

private:
    Guif::ScreenPtr m_screen;

    PostProcessPtr m_reviveFx;
    float m_reviveTimer;
    bool m_reviveDisable;
};

typedef Claw::SmartPtr<MenuInGame> MenuInGamePtr;

#endif

#include <sstream>

#include "claw/application/AbstractApp.hpp"
#include "claw/base/Registry.hpp"
#include "claw/graphics/TriangleEngine.hpp"
#include "guif/Control.hpp"

#include "claw_ext/monetization/playhaven/Playhaven.hpp"

#include "MonstazAI/job/MainMenuJob.hpp"
#include "MonstazAI/job/IntermediateJob.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/menu/MenuInGame.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/Stats.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/VibraController.hpp"
#include "MonstazAI/GameCenterManager.hpp"
#include "MonstazAI/db/UserDataManager.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/math/LuaMath.hpp"
#include "MonstazAI/MonetizationManager.hpp"
#include "MonstazAI/ConnectionMonitor.hpp"

LUA_DECLARATION( MenuInGame )
{
    METHOD( MenuInGame, PerkSelected ),
    METHOD( MenuInGame, StopPerkMenu ),
    METHOD( MenuInGame, StopSummary ),
    METHOD( MenuInGame, RestartLevel ),
    METHOD( MenuInGame, NextLevel ),
    METHOD( MenuInGame, GoToShop ),
    METHOD( MenuInGame, LogPerkName ),
    METHOD( MenuInGame, EndEvent ),
    METHOD( MenuInGame, Reload ),
    METHOD( MenuInGame, PerkMenu ),
    METHOD( MenuInGame, Nuke ),
    METHOD( MenuInGame, NextWeapon ),
    METHOD( MenuInGame, PrevWeapon ),
    METHOD( MenuInGame, NumWeapons ),
    METHOD( MenuInGame, Save ),
    METHOD( MenuInGame, VideoAd ),
    METHOD( MenuInGame, GetControlSize ),
    METHOD( MenuInGame, Playhaven ),
    METHOD( MenuInGame, GetNumPerks ),
    METHOD( MenuInGame, Revive ),
    METHOD( MenuInGame, GetTime ),
    METHOD( MenuInGame, GetNetworkTime ),
    METHOD( MenuInGame, GetVersionString ),
    METHOD( MenuInGame, OpenAchievementsUI ),
    {0,0}
};

static const char* vertexRevive =
    "void main(void)\n"
    "{\n"
    "TEXTURE;\n"
    "GLPOSITION;\n"
    "}";

static const char* fragmentRevive =
    "uniform mediump float m;\n"
    "void main(void)\n"
    "{\n"
    "lowp vec3 c = texture2D( clawTex, vTex ).rgb;\n"
    "mediump float l = c.r * 0.3 + c.g * 0.59 + c.b * 0.11;\n"
    "l = l * 2.0 - 1.0;\n"
    "gl_FragColor = vec4( mix( c, vec3( l, l, l ), m ), 1.0 );\n"
    "}";


MenuInGame::MenuInGame()
    : m_screen( new Guif::Screen() )
    , m_reviveFx( new PostProcess( vertexRevive, fragmentRevive ) )
    , m_reviveTimer( 0 )
    , m_reviveDisable( false )
{
    GameplayJob* job = (GameplayJob*)((MonstazApp*)MonstazApp::GetInstance())->GetJob();
    job->AddPostProcess( m_reviveFx );

    Claw::LuaPtr lua = m_screen->GetLuaState();

    lua->RegisterLibrary( Claw::Lua::L_MATH );

    RegisterMath( lua );

    // Can't use Lua::Call because randomseed is inside table (and we don't support such scheme now).
    char tmp[48];
    sprintf( tmp, "math.randomseed(%i)", g_rng.GetInt() );
    lua->Execute( tmp );

    Claw::Lunar<MenuInGame>::Register( *lua );
    Claw::Lunar<MenuInGame>::push( *lua, this );
    lua->RegisterGlobal( "callback" );

    MonstazApp::PushScreenModes( lua );

    Claw::Lunar<Claw::Registry>::Register( *lua );
    Claw::Lunar<Claw::Registry>::push( *lua, Claw::Registry::Get() );
    lua->RegisterGlobal( "registry" );

    Claw::Lunar<Claw::TextDict>::Register( *lua );
    Claw::Lunar<Claw::TextDict>::push( *lua, Claw::TextDict::Get() );
    lua->RegisterGlobal( "TextDict" );

    lua->CreateEnumTable();
    lua->AddEnum( Perks::Runner );
    lua->AddEnum( Perks::AmmoManiac );
    lua->AddEnum( Perks::FastHands );
    lua->AddEnum( Perks::ComeGetSome );
    lua->AddEnum( Perks::Slaughter );
    lua->AddEnum( Perks::Regeneration );
    lua->AddEnum( Perks::Unstoppable );
    lua->AddEnum( Perks::Rage );
    lua->AddEnum( Perks::Endoskeleton );
    lua->AddEnum( Perks::ColdVengeance );
    lua->AddEnum( Perks::FirstAid );
    lua->AddEnum( Perks::ToughSkin );
    lua->AddEnum( Perks::OrbExtender );
    lua->AddEnum( Perks::Greed );
    lua->AddEnum( Perks::Wrath );
    lua->AddEnum( Perks::TeamPlayer );
    lua->AddEnum( Perks::MrClean );
    lua->AddEnum( Perks::BigBang );
    lua->AddEnum( Perks::PurfectCat );
    lua->AddEnum( Perks::Ragenerator );
    lua->AddEnum( Perks::RussianRoulette );
    lua->AddEnum( Perks::ExtraExplosives );
    lua->AddEnum( Perks::Withdraw );
    lua->AddEnum( Perks::MuscleFever );

    lua->RegisterEnumTable( "Perk" );

    AtlasManager::InitEnum( lua );
    EntityManager::InitEnum( lua );
    PickupManager::InitEnum( lua );
    ShotManager::InitEnum( lua );

    AudioManager::GetInstance()->Init( lua );
    VibraController::GetInstance()->Init( lua );
    Shop::GetInstance()->Init( lua );
    GameCenterManager::GetInstance()->Init( lua );
    Hud::GetInstance()->Init( lua );
    GameEventDispatcher::GetInstance()->InitLua( lua );
    Missions::MissionManager::GetInstance()->InitLua( lua, true );

    AnalyticsManager::GetInstance()->SetLua( lua );
    ConnectionMonitor::GetInstance()->SetLua( lua );

    TutorialManager* tm = TutorialManager::GetInstance();
    tm->Init( lua );
    tm->SetGameMenuLua( lua );

    lua->Load( "menu2/ingame.lua" );
}

MenuInGame::~MenuInGame()
{
    if ( TutorialManager::GetInstance() )
    {
        TutorialManager::GetInstance()->SetGameMenuLua( NULL );
    }
}

void MenuInGame::Update( float dt )
{
    Claw::Lua* lua = m_screen->GetLuaState();
    lua->PushBool( GameManager::GetInstance()->GetStats()->GetPerks() != 0 );
    lua->PushBool( GameManager::GetInstance()->NukeAvailable() );
    lua->Call( "PM", 2, 0 );
    m_screen->Update( dt );

    if( m_reviveFx->m_active )
    {
        m_reviveTimer += dt * M_PI * 2;
        if( m_reviveTimer > M_PI * 2 )
        {
            m_reviveTimer -= M_PI * 2;
            if( m_reviveDisable )
            {
                m_reviveFx->m_active = false;
            }
        }
        m_reviveFx->m_shader->Uniform( "m", (float)( ( sin( m_reviveTimer - M_PI * 0.5f ) * 0.5f + 0.5f ) * 0.75f ) );
        GameManager::GetInstance()->GetHud()->PlayerDamaged( 1 );
    }
}

void MenuInGame::Render( Claw::Surface* target )
{
    m_screen->Render( target );
}

bool MenuInGame::OnTouchDown( int x, int y, int button )
{
    return m_screen->OnTouchDown( x, y, button );
}

bool MenuInGame::OnTouchUp( int x, int y, int button )
{
    return m_screen->OnTouchUp( x, y, button );
}

bool MenuInGame::OnTouchMove( int x, int y, int button )
{
    return m_screen->OnTouchMove( x, y, button );
}

void MenuInGame::KeyPress( Claw::KeyCode code )
{
    m_screen->OnKeyDown( code );
}

void MenuInGame::StartPerkMenu()
{
    GameManager::GetInstance()->GetAudioManager()->PauseLooped( true );
    m_screen->GetLuaState()->Call( "StartPerkMenu", 0, 0 );
}

void MenuInGame::StartPauseMenu()
{
    GameManager::GetInstance()->GetAudioManager()->PauseLooped( true );
    m_screen->GetLuaState()->Call( "StartPauseMenu", 0, 0 );
}

void MenuInGame::StartSummary( int level, int time, int kills, int multi, int score, bool fail )
{
    GameManager::GetInstance()->GetAudioManager()->KillLooped();

    Claw::Lua* lua = m_screen->GetLuaState();

    lua->PushNumber( level );
    lua->PushNumber( time );
    lua->PushNumber( kills );
    lua->PushNumber( multi );
    lua->PushNumber( score );
    lua->PushBool( fail );
    lua->Call( "StartSummary", 6, 0 );

    bool isSurvival = false;
    Claw::Registry::Get()->Get("/internal/survival", isSurvival );
    bool wasWithFriend = false;
    Claw::Registry::Get()->Get( "/internal/friend", wasWithFriend );

    if( wasWithFriend )
    {
        const char* itemId = "4"; //soft currency 
        const char* friendId =  Claw::Registry::Get()->CheckString( "/internal/friendId" );

        if ( isSurvival || !fail )
        {
            if ( !UserDataManager::GetInstance()->IsBot ( friendId ) )
            {
                // half reward goes to friend or if survival collected cash
                int reward = Claw::Registry::Get()->CheckInt( "/internal/reward/soft" ) / 5;
                if ( isSurvival ) reward = Claw::Registry::Get()->CheckInt( "/internal/money" ) / 5;

                if ( reward > 0 )
                {
                    std::ostringstream ss;
                    ss << reward;
                    UserDataManager::GetInstance()->SendSendRewardRequest( itemId , friendId , ss.str().c_str() );
                }
            }
        }
    }

    if ( isSurvival )
    {
       int survialLevelId = 1;
       Claw::Registry::Get()->Get("/internal/survivalLevel",survialLevelId );
       UserDataManager::GetInstance()->SendSetScoreSurvivalRequest( survialLevelId , score , time );
    }
    else
    {
        int playerLevel = Claw::Registry::Get()->CheckInt("/monstaz/player/level" );
        int scoreValue = Claw::Registry::Get()->CheckInt( "/monstaz/player/kills" );
        int eventsValue = Claw::Registry::Get()->CheckInt( "/monstaz/player/eventsCompleted" );

        UserDataManager::GetInstance()->SendSetUserParams( playerLevel , scoreValue , eventsValue );
    }

    UserDataManager::GetInstance()->SyncData( false );
}

void MenuInGame::StartTutorial( int id )
{
    GameManager::GetInstance()->GetAudioManager()->PauseLooped( true );
    Claw::Lua* lua = m_screen->GetLuaState();
    lua->PushNumber( id );
    lua->Call( "StartTutorial", 1, 0 );
}

void MenuInGame::StartStoryTutorial( const Claw::NarrowString& text )
{
    GameManager::GetInstance()->GetAudioManager()->PauseLooped( true );
    Claw::Lua* lua = m_screen->GetLuaState();
    lua->PushString( text );
    lua->Call( "StartStoryTutorial", 1, 0 );
}

bool MenuInGame::IsPerkIconVisible() const
{
    if( m_screen )
    {
        Guif::Control* control = m_screen->FindControl( "PerkArea" );
        if( control )
        {
            return control->IsTouchable() && control->IsVisible();
        }
    }
    return false;
}

void MenuInGame::SetupTutorials()
{
    if( m_screen )
    {
        Claw::Lua* lua = m_screen->GetLuaState();
        lua->PushBool( ((MonstazApp*)MonstazApp::GetInstance())->IsXperiaPlayKeyboarbAvailable() );
        lua->Call( "SetXperiaTutorialVisible", 1, 0 );
    }
}

void MenuInGame::StartRevive()
{
    m_screen->GetLuaState()->Call( "ReviveMenuShow", 0, 0 );
    m_reviveFx->m_active = true;
    m_reviveFx->m_shader->Uniform( "m", 0.f );
    m_reviveTimer = 0;
    m_reviveDisable = false;
}

int MenuInGame::l_PerkSelected( lua_State* L )
{
    Claw::Lua lua( L );

    Perks::Type perk = (Perks::Type)(int)( lua.CheckNumber( 1 ) );
    GameManager::GetInstance()->GetStats()->EnablePerk( perk );

    return 0;
}

int MenuInGame::l_StopPerkMenu( lua_State* L )
{
    GameManager::GetInstance()->GetAudioManager()->PauseLooped( false );
    GameManager::GetInstance()->ShowPerkMenu( false );
    return 0;
}

int MenuInGame::l_StopSummary( lua_State* L )
{
    bool isSurvival = false;
    Claw::Registry::Get()->Get("/internal/survival", isSurvival );
    if ( isSurvival )
    {
        Claw::Registry::Get()->Set("/internal/survival", false );
    }

    GameManager::GetInstance()->GetAudioManager()->KillLooped();
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new MainMenuJob() );
    return 0;
}

int MenuInGame::l_RestartLevel( lua_State* L )
{
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_LEVEL_RESTARTED );
    GameManager::GetInstance()->GetAudioManager()->KillLooped();
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new IntermediateJob( Claw::Registry::Get()->CheckString( "/internal/levelfile" ) ) );
    return 0;
}

int MenuInGame::l_NextLevel( lua_State* L )
{
    Claw::Lua lua( L );
    GameManager::GetInstance()->GetAudioManager()->KillLooped();
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new IntermediateJob( lua.CheckString( 1 ) ) );
    return 0;
}

int MenuInGame::l_GoToShop( lua_State* L )
{
    GameManager::GetInstance()->GetAudioManager()->KillLooped();
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->SwitchJob( new MainMenuJob( MainMenuJob::SS_SHOP ) );
    return 0;
}

int MenuInGame::l_LogPerkName( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int MenuInGame::l_EndEvent( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int MenuInGame::l_Reload( lua_State* L )
{
    GameManager::GetInstance()->GetLua()->Call( "DoReload", 0, 0 );
    return 0;
}

int MenuInGame::l_PerkMenu( lua_State* L )
{
    GameManager::GetInstance()->ShowPerkMenu( true );
    return 0;
}

int MenuInGame::l_Nuke( lua_State* L )
{
    GameManager::GetInstance()->Nuke();
    return 0;
}

int MenuInGame::l_NextWeapon( lua_State* L )
{
    GameManager::GetInstance()->GetLua()->Call( "NextBoughtWeapon", 0, 0 );
    TutorialManager::GetInstance()->OnWeaponChange();
    return 0;
}

int MenuInGame::l_PrevWeapon( lua_State* L )
{
    GameManager::GetInstance()->GetLua()->Call( "PrevBoughtWeapon", 0, 0 );
    TutorialManager::GetInstance()->OnWeaponChange();
    return 0;
}

int MenuInGame::l_NumWeapons( lua_State* L )
{
    int num = 0;
    Claw::Lua* gmLua = GameManager::GetInstance()->GetLua();
    gmLua->Call( "CheckNumBoughtWeapons", 0, 1 );
    num = gmLua->CheckNumber( -1 );
    gmLua->Pop( 1 );

    Claw::Lua lua( L );
    lua.PushNumber( num );
    return 1;
}

int MenuInGame::l_Save( lua_State* L )
{
    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    return 0;
}

int MenuInGame::l_VideoAd( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int MenuInGame::l_GetControlSize( lua_State* L )
{
    return MonstazAI::MonstazAIApplication::GetControlSize( m_screen, L );
}

int MenuInGame::l_Playhaven( lua_State* L )
{
    Claw::Lua lua( L );
    if( !TutorialManager::GetInstance()->IsActive() )
    {
        MonetizationManager::GetInstance()->GetPlayhaven()->ContentPlacement( lua.CheckString( 1 ).c_str() );
    }
    return 0;
}

int MenuInGame::l_GetNumPerks( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( GameManager::GetInstance()->GetStats()->GetPerks() );
    return 1;
}

int MenuInGame::l_Revive( lua_State* L )
{
    Claw::Lua lua( L );
    bool revive = lua.CheckBool( 1 );
    Entity* e = GameManager::GetInstance()->GetPlayer();
    lua_State* l = *GameManager::GetInstance()->GetLua();
    EntityManager* em = GameManager::GetInstance()->GetEntityManager();
    bool cat = Claw::Registry::Get()->CheckBool( "/internal/revivecat" );

    Entity* ec = NULL;
    const std::vector<Entity*>& ents = GameManager::GetInstance()->GetEntityManager()->GetEntities();
    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        if( (*it)->GetType() == Entity::OctopusFriend )
        {
            ec = *it;
            break;
        }
    }

    if( revive )
    {
        e->SetHitPoints( e->GetMaxHitPoints() );
        if( ec )
        {
            ec->SetHitPoints( ec->GetMaxHitPoints() );
        }
        GameManager::GetInstance()->SetMenuActive( false );
        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_REVIVE_PLAYER );

        GameManager::GetInstance()->GodMode( 3 );

        const std::vector<Entity*>& ents = em->GetEntities();
        for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
        {
            Entity* m = *it;
            Entity::Type t = m->GetType();

            if( t == Entity::MechaBoss || t == Entity::OctobrainBoss || t == Entity::SowerBoss )
            {
                continue;
            }

            if( m->GetClass() != Entity::Friendly )
            {
                Vectorf dist( m->GetPos() - e->GetPos() );
                if( dist.LengthSqr() < 100*100 )
                {
                    m->SetHitPoints( -1 );
                    if( m->GetType() == Entity::FishSimple )
                    {
                        m->SetType( Entity::FishSimpleNonExploding );
                    }
                }
                if( ec )
                {
                    Vectorf dist( m->GetPos() - ec->GetPos() );
                    if( dist.LengthSqr() < 100*100 )
                    {
                        m->SetHitPoints( -1 );
                        if( m->GetType() == Entity::FishSimple )
                        {
                            m->SetType( Entity::FishSimpleNonExploding );
                        }
                    }
                }
            }
        }
    }
    else
    {
        if( cat )
        {
            CLAW_ASSERT( ec );
            Claw::Lunar<Entity>::push( *GameManager::GetInstance()->GetLua(), ec );
            GameManager::GetInstance()->GetLua()->Call( "FriendDied", 1, 0 );
            ec->SetHitPoints( 1000000 );
            ec->SetState( SRunAway );
        }
        else
        {
            GameManager::GetInstance()->KilledPlayer( e );
            em->Remove( e );
        }
        lua_getglobal( l, "GameOver" );
        lua_call( l, 0, 0 );
    }

    lua_pushboolean( l, false );
    lua_setglobal( l, "reviveLock" );

    m_reviveDisable = true;

    return 0;
}

int MenuInGame::l_OpenAchievementsUI( lua_State* L )
{
    Network::GoogleServices* gs = ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetGoogleServices();
    if( gs->IsAuthenticated() )
    {
        gs->OpenAchievementsUI();
    }
    return 0;
}

int MenuInGame::l_GetTime( lua_State* L )
{
    lua_pushnumber( L, Claw::Time::GetTime() );
    return 1;
}

int MenuInGame::l_GetNetworkTime( lua_State* L )
{
    lua_pushnumber( L, ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->GetNetworkTime() );
    return 1;
}

int MenuInGame::l_GetVersionString( lua_State* L )
{
    Claw::Lua lua(L);
    lua.PushString( AnalyticsManager::GetInstance()->GenerateBuildName( true ) );
    return 1;
}

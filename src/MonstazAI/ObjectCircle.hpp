#ifndef __MONSTAZ_OBJECTCIRCLE_HPP__
#define __MONSTAZ_OBJECTCIRCLE_HPP__

#include "claw/base/Errors.hpp"
#include "claw/base/Lua.hpp"

class ObjectCircle
{
public:
    ObjectCircle() { CLAW_ASSERT( false ); }
    ObjectCircle( float r ) : m_r( r ) {}

    float GetRadius() { return m_r; }
    int l_GetRadius( lua_State* L );

protected:
    float m_r;
};

#endif

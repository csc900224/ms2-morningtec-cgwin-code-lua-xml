#ifndef __MONSTAZ_GAME_FACEBOOK_WRAPPER_HPP__
#define __MONSTAZ_GAME_FACEBOOK_WRAPPER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"

// Forward declarations
class Facebook;

class FacebookWrapper
{
public:
    typedef FacebookWrapper self;

                    ~FacebookWrapper();

    static self*    GetInstance();

    static void     Release();

    bool            Authenticate();

    bool            PublishScore( int score );

    bool            PublishLevel( int level );

    // Lua binding
                    LUA_DEFINITION( FacebookWrapper );

                    FacebookWrapper( lua_State* L );
    void            Init( Claw::Lua* lua );

    int             l_PublishScore( lua_State* L );
    int             l_PublishLevel( lua_State* L );

private:
                    FacebookWrapper();

    Facebook*       m_facebook;

}; // class FacebookWrapper

#endif // !defined __MONSTAZ_GAME_FACEBOOK_WRAPPER_HPP__
// EOF

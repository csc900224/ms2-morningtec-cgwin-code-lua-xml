#include "MonstazAI/ServerConstants.hpp"
#include "claw/compat/Platform.h"

namespace ServerConstants
{
    const char* SYNC_REMOTE_URL                     = "http://127.0.0.1/";
    const bool  SYNC_AB_TESTS_ENABLED               = false;
    const char* SYNC_SAVE_DIR                       = "save";
    const char* SYNC_APP_NAME                       = "ms2";
    const char* SYNC_ENC_KEY                        = "0";
    const bool  SYNC_UPDATE_EVERY_START             = true;
#ifdef _DEBUG
    const int   SYNC_UPDATE_PERIOD                  = 10; // 10s
#else
    const int   SYNC_UPDATE_PERIOD                  = 60 * 60 * 8; // 8h
#endif

#if defined AMAZON_BUILD
    const char* SYNC_DATA_PLATFORM                  = "amazon";
#elif defined NOOK_BUILD
    const char* SYNC_DATA_PLATFORM                  = "nook";
#elif defined CLAW_ANDROID
    const char* SYNC_DATA_PLATFORM                  = "gp";
#elif defined CLAW_IPHONE && defined PREMIUM_BUILD
    const char* SYNC_DATA_PLATFORM                  = "iphone-premium";
#else
    const char* SYNC_DATA_PLATFORM                  = "iphone";
#endif

    const char* APP_VERSION_SYNC_TASK               = "AppVersionSync";
    const char* APP_VERSION_REMOTE_FILE             = "Ze7aPAp7.dat";
    const char* APP_VERSION_XML                     = ".dat";

    const char* DEFAULT_CONFIG_DATA_SYNC_TASK       = "DefaultConfigSync";
    const char* DEFAULT_CONFIG_DATA_REMOTE_FILE     = "cr79ajuJ.dat";
    const char* DEFAULT_CONFIG_DATA_XML             = ".xml"; // Override only slected reg keys, but not whole file

    const char* CONFIGURATION_DATA_SYNC_TASK        = "ConfigurationSync";
    const char* CONFIGURATION_DATA_REMOTE_FILE      = "9raphaSa.dat";
    const char* CONFIGURATION_DATA_XML              = ".xml"; // Override only slected reg keys, but not whole file

    const char* LUA_MENU_SYNC_TASK                  = "LuaMenuSync";
    const char* LUA_MENU_REMOTE_FILE                = "PhuDegA8.dat";
    const char* LUA_MENU_LOCAL_FILE                 = ".dat";

    const char* LUA_INGAME_SYNC_TASK                = "LuaIngameSync";
    const char* LUA_INGAME_REMOTE_FILE              = "2aPrEkew.dat";
    const char* LUA_INGAME_LOCAL_FILE               = ".dat";

} // namespace ServerConstants
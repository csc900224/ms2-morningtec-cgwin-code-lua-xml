#ifndef __MONSTAZ_HOLOGRAM_HPP__
#define __MONSTAZ_HOLOGRAM_HPP__

#include "claw/graphics/Surface.hpp"
#include "claw/graphics/OpenGLShader.hpp"

class Hologram : public Claw::RefCounter
{
public:
    Hologram();
    void Render( Claw::Surface* src, Claw::Surface* dst, float x, float y, const Claw::Rect& clipRect );
    void Update( float dt );

    void SetIntensity( float intensity ) { m_intensity = intensity; }

private:
    Claw::OpenGLShader m_shader;
    float m_time;
    float m_intensity;
    bool m_enabled;
};

typedef Claw::SmartPtr<Hologram> HologramPtr;

#endif

#ifndef __MONSTAZ_LOADING_HPP__
#define __MONSTAZ_LOADING_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/Mutex.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/graphics/Surface.hpp"

#include "guif/Screen.hpp"

#include "MonstazAI/DownloadScreen.hpp"

class Loading : public Claw::RefCounter
{
public:
    LUA_DEFINITION( Loading );
    Loading( lua_State* L ) { CLAW_ASSERT( false ); }

    Loading( bool autoaimquery );
    ~Loading();

    void Update( float dt );
    void Render( Claw::Surface* target, Claw::UInt32 dlList );
    void KeyPress( Claw::KeyCode code );
    void TouchUp( int x, int y );
    void TouchDown( int x, int y );

    void WaitForTap( bool wait );
    bool CanLeave() const;
    void BeforeLoadWait() { m_mutex.Enter(); m_mutex.Leave(); }
    void ThreadLoadPopup();

    int l_OnContinue( lua_State* L );
    int l_OnDownload( lua_State* L );
    int l_Release( lua_State* L );
    int l_Analytics( lua_State* L );
    int l_GetControlSize( lua_State* L );
    int l_Playhaven( lua_State* L );

private:
    Claw::SurfacePtr m_bg;
    Claw::SurfacePtr m_blip;
    Claw::SurfacePtr m_blipSpace;

    Claw::FontExPtr m_font;
    Claw::ScreenTextPtr m_text;
    Claw::ScreenTextPtr m_tapText;

    Claw::Mutex m_mutex;
    Guif::ScreenPtr m_autoAimScreen;
    bool m_screenOk;

    DownloadScreenPtr m_dl;

    float m_time;
    float m_textExtent;

    int m_state;

    enum { BLIP_NUM = 5 };
    float m_a[BLIP_NUM];

    bool m_tapWait;
    float m_tapTextAlpha;

    bool m_firstLoading;
    bool m_socialSplash;
    float m_scale;

    bool m_movie;

    float m_currentTime;
    float m_targetTime;
};

typedef Claw::SmartPtr<Loading> LoadingPtr;

#endif

#ifndef __MONSTAZ_AUDIOMANAGER_HPP__
#define __MONSTAZ_AUDIOMANAGER_HPP__

#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/Registry.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/sound/mixer/AudioSource.hpp"
#include "claw/sound/mixer/EffectVolumeShift.hpp"
#include "claw/sound/mixer/EffectPan.hpp"
#include "claw/sound/mixer/EffectVolume.hpp"
#include "claw/sound/mixer/Mixer.hpp"

#include "MonstazAI/math/Vector.hpp"

enum AudioSfx
{
    SFX_DEATH1,
    SFX_DEATH2,
    SFX_DEATH3,
    SFX_PISTOL,
    SFX_PISTOL_END,
    SFX_MINIGUN,
    SFX_RELOAD1,
    SFX_RELOAD2,
    SFX_RELOAD3,
    SFX_RELOAD4,
    SFX_SHOTGUN1,
    SFX_SHOTGUN2,
    SFX_SPINUP,
    SFX_SPINDOWN,
    SFX_ELECTRICITY,
    SFX_ELECTRICITY_START,
    SFX_RAILGUN,
    SFX_FLAMER,
    SFX_PLASMA,
    SFX_PLASMA_END,
    SFX_PLASMA_HIT,
    SFX_PLASMA_SECTOID,
    SFX_RIPPER,
    SFX_ROCKET,
    SFX_EXPLOSION1,
    SFX_EXPLOSION2,
    SFX_EXPLOSION3,
    SFX_SHOCKERBURN,
    SFX_SHOCKERSTART,
    SFX_SHOCKERLOOP,
    SFX_SHOCKEREND,
    SFX_RICOCHET1,
    SFX_RICOCHET2,
    SFX_RICOCHET3,
    SFX_RICOCHET4,
    SFX_RICOCHET5,
    SFX_RICOCHET6,
    SFX_RICOCHET7,
    SFX_PLAYER_HIT1,
    SFX_PLAYER_HIT2,
    SFX_PLAYER_HIT3,
    SFX_PLAYER_HIT4,
    SFX_PLAYER_HIT5,
    SFX_PLAYER_HIT6,
    SFX_PLAYER_HIT7,
    SFX_PLAYER_DEATH,
    SFX_OCTOPUS_HIT1,
    SFX_OCTOPUS_HIT2,
    SFX_OCTOPUS_HIT3,
    SFX_OCTOPUS_ATTACK1,
    SFX_OCTOPUS_ATTACK2,
    SFX_OCTOPUS_ATTACK3,
    SFX_OCTOPUS_ATTACK4,
    SFX_OCTOPUS_ATTACK5,
    SFX_OCTOPUS_ATTACK6,
    SFX_ORB,
    SFX_HEALTH,
    SFX_WEAPON_PICKUP,
    SFX_CASH,
    SFX_QUAD,
    SFX_LEVELUP,
    SFX_PICKUP_APPEAR,
    SFX_PICKUP_DISAPPEAR,
    SFX_NO_AMMO,
    SFX_SIREN,
    SFX_NUKE,
    SFX_BITE1,
    SFX_BITE2,
    SFX_FISH_DEATH1,
    SFX_FISH_DEATH2,
    SFX_FISH_DEATH3,
    SFX_FISH_EXPLOSION,
    SFX_ROLLER_DEATH,
    SFX_SPIT,
    SFX_SPIT_PREP,
    SFX_FISH_HIT1,
    SFX_FISH_HIT2,
    SFX_SHOPKEEPER,
    SFX_MONEYDROP,
    SFX_TELEPORT,
    SFX_MINE_ARM,
    SFX_MINE_ARM2,
    SFX_MINE_DETECT,
    SFX_SHIELD1,
    SFX_SHIELD2,
    SFX_SHIELD3,
    SFX_SHIELD4,
    SFX_SHIELD_HIT1,
    SFX_SHIELD_HIT2,
    SFX_FLOATER_DIE1,
    SFX_FLOATER_DIE2,
    SFX_FLOATER_ELECTRIC_DIE,
    SFX_HOUND_DIE1,
    SFX_HOUND_DIE2,
    SFX_HOUND_SHOOTING_DIE,
    SFX_MULTIKILL1,
    SFX_MULTIKILL2,
    SFX_MULTIKILL3,
    SFX_MULTIKILL4,
    SFX_MULTIKILL5,
    SFX_MULTIKILL6,
    SFX_MULTIKILL7,
    SFX_MULTIKILL8,
    SFX_TRIPALIZER_START,
    SFX_TRIPALIZER_LOOP,
    SFX_TRIPALIZER_END,
    SFX_CRAB_LASER,
    SFX_SOWER_CHARGE,
    SFX_SOWER_DEATH,
    SFX_SOWER_LICK,
    SFX_SOWER_SPIT,
    SFX_SOWER_TOSS_EGGS,
    SFX_JET,
    SFX_SPIKES_SHOT,
    SFX_SPIKES_1,
    SFX_SPIKES_2,
    SFX_SPIKES_3,
    SFX_MAGNUM,
    SFX_AMBIENT,
    SFX_GIANT_BLOT_DMG1,
    SFX_GIANT_BLOT_DMG2,
    SFX_GIANT_BLOT_DMG3,
    SFX_WHALE_CHARGE,
    SFX_SECTOID_DEATH1,
    SFX_SECTOID_DEATH2,
    SFX_SECTOID2_DEATH1,
    SFX_SECTOID2_DEATH2,
    SFX_SECTOID2_DEATH3,
    SFX_SECTOID2_DEATH4,
    SFX_LINEGUN,
    SFX_CRAB_DIGIN,
    SFX_CRAB_DIGOUT,
    SFX_WEAPON_SWIPE,
    SFX_CAT_ANGRY,
    SFX_CAT_VOMIT,
    SFX_VORTEX,
    SFX_CHAINSAW_START,
    SFX_CHAINSAW_LOOP,
    SFX_CHAINSAW_STOP,
    SFX_TELEPORT1,
    SFX_TELEPORT2,
    SFX_TELEPORT3,
    SFX_MECH_START,
    SFX_MECH_LOOP,
    SFX_MECH_SERVO,

    SFX_MENU_BACK,
    SFX_MENU_ROTATE,
    SFX_MENU_SELECT,
    SFX_MENU_SELECT_SMALL,
    SFX_MENU_POPUP,
    SFX_MENU_POPUP_CLOSE,
    SFX_MENU_POPUP_LONG,
    SFX_MENU_POPUP_LONG_CLOSE,
    SFX_MENU_ZOOM,
    SFX_MENU_BUY,
    SFX_MENU_GWIAZDKI,
    SFX_MENU_LEVELUP,
    SFX_MENU_DING,
    SFX_MENU_MISSION_BULLET,
    SFX_MENU_LEVEL_FINISHED,
    SFX_MENU_LEVEL_UP,
    SFX_MENU_RANK_UP,
    SFX_MENU_RUSSIAN_WIN,
    SFX_MENU_RUSSIAN_LOSE,

    SFX_VO_ANOTHER_VICTORY,
    SFX_VO_DONT_GIVE_UP,
    SFX_VO_FATAL_ENCOUNTER,
    SFX_VO_LEVELUP,
    SFX_VO_MAXED_OUT,
    SFX_VO_TAKE_YOUR_REVENGE,
    SFX_VO_TOTAL_DOMINATION,
    SFX_VO_YOU_FAILED,

    SFX_COUNT
};

namespace Claw
{
    typedef SmartPtr<EffectPan> EffectPanPtr;
    typedef WeakPtr<EffectPan> EffectPanWPtr;
    typedef SmartPtr<EffectVolume> EffectVolumePtr;
    typedef WeakPtr<EffectVolume> EffectVolumeWPtr;
}

class AudioManager : public Claw::RefCounter
{
public:
    struct SoundHandle3D
    {
        Claw::AudioChannelWPtr channel;
        Claw::EffectPanWPtr pan;
        Claw::EffectVolumeWPtr volume;
    };

    LUA_DEFINITION( AudioManager );
    AudioManager( lua_State* L ) { CLAW_ASSERT( false ); }

    AudioManager();
    ~AudioManager();

    void Load();

    void Init( Claw::Lua* lua );

    Claw::AudioChannelWPtr Play( AudioSfx sfx );
    void Stop( Claw::AudioChannelWPtr channel );

    Claw::AudioChannelWPtr PlayLooped( AudioSfx sfx );
    void StopLooped( Claw::AudioChannelWPtr acw );

    Claw::AudioChannelWPtr Play3D( AudioSfx sfx, const Vectorf& pos, SoundHandle3D* handle = NULL );
    Claw::AudioChannelWPtr PlayLooped3D( AudioSfx sfx, const Vectorf& pos, SoundHandle3D* handle = NULL );
    void UpdatePos3D( const Vectorf& pos, SoundHandle3D& handle );
    void Stop3D( SoundHandle3D& handle );

    void PlayMusic( const char* fn );
    void StopMusic();
    void MusicVolumeOverride( unsigned int shift );

    void MuteMusic( bool mute );

    void PauseLooped( bool pause );
    void KillLooped();

    void Pause( bool pause );

    int l_Play( lua_State* L );
    int l_PlayLooped( lua_State* L );
    int l_UpdatePos( lua_State* L );
    int l_StopLooped( lua_State* L );
    int l_PlayMusic( lua_State* L );
    int l_MusicVolumeOverride( lua_State* L );

    Claw::Mixer* GetMixer() { return m_mixer; }

    static AudioManager* GetInstance() { return s_instance; }

    static void MasterVolumeChanged( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node );
    static void SfxVolumeChanged( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node );

private:
    void UpdatePos3D( const Vectorf& pos, Claw::EffectVolume& volume, Claw::EffectPan& pan );

    Claw::AudioSourcePtr m_sfx[SFX_COUNT];
    Claw::MixerPtr m_mixer;

    Claw::AudioChannelWPtr m_music;

    bool m_muteMusic;

    Claw::EffectVolumePtr m_masterVolume;
    Claw::EffectVolumePtr m_sfxVolume;

    std::vector<Claw::AudioChannelWPtr> m_loop;

    static AudioManager* s_instance;
};

typedef Claw::SmartPtr<AudioManager> AudioManagerPtr;

struct AudioChannelWrapper
{
    LUA_DEFINITION( AudioChannelWrapper );
    AudioChannelWrapper( const Claw::AudioChannelWPtr& _ptr, const AudioManager::SoundHandle3D& _hnd ) : ptr( _ptr ), hnd( _hnd ) {}
    Claw::AudioChannelWPtr ptr;
    AudioManager::SoundHandle3D hnd;
};

#endif

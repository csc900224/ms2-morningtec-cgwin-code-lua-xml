#include "MonstazAI/StackSM.hpp"

StackSM::StackSM( StateName idleStateId )
    : m_idleStateId ( idleStateId )
    , m_currentId( idleStateId )
{
    for( int i=0; i<NUMBER_OF_STATES; i++ )
    {
        m_register[i] = NULL;
    }
}

void StackSM::Update( Entity& owner, float tick )
{
    if( !m_stack.empty() )
    {
        m_stack.back()->OnUpdate( owner, *this, tick );
    }
}

void StackSM::RegisterState( StateName id, State* pState )
{
    CLAW_ASSERT( m_register[id] == NULL );
    m_register[id] = pState;
}

void StackSM::ReleaseStates()
{
    for( int i=0; i<NUMBER_OF_STATES; i++ )
    {
        delete m_register[i];
    }
}

State* StackSM::GetState( StateName id )
{
    return m_register[id];
}

StateName StackSM::GetStateId( const State* state ) const
{
    int i;
    for( i=0; i<NUMBER_OF_STATES; i++ )
    {
        if( m_register[i] == state ) break;
    }
    return (StateName)i;
}

State* StackSM::GetCurrentState()
{
    if( m_stack.empty() )
    {
        CLAW_MSG( "[StackSM::GetCurrentState()] There is no active state on stack -StackSM is in FREEZE/IDLE state" );
        return NULL;
    }
    else
    {
        return m_stack.back();
    }
}

StateName StackSM::GetCurrentStateId() const
{
    return m_currentId;
}

void StackSM::ChangeState( Entity& owner, StateName id )
{
    if( !m_stack.empty() )
    {
        m_register[m_currentId]->OnExit( owner, *this, id );
        m_stack.pop_back();
    }

    StateName prev = m_currentId;
    m_currentId = id;

    if( id != m_idleStateId )
    {
        CLAW_ASSERT( m_register[id] );
        m_register[id]->OnEnter( owner, *this, prev );
        m_stack.push_back( m_register[id] );
    }
}

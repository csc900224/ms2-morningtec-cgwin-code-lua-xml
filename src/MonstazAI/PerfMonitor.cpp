#include <algorithm>

#include "MonstazAI/PerfMonitor.hpp"

PerfMonitor::PerfMonitor()
{
    Reset();
}

PerfMonitor::~PerfMonitor()
{
}

void PerfMonitor::ReportFPS( unsigned int fps )
{
    m_buckets[std::min( fps, 60u )]++;
    m_samples++;
}

void PerfMonitor::Reset()
{
    std::fill( m_buckets, m_buckets+61, 0 );
    m_samples = 0;
}

unsigned int PerfMonitor::GetMedian() const
{
    unsigned int curr = 0;
    for( int i=0; i<61; i++ )
    {
        curr += m_buckets[i];
        if( curr > m_samples / 2 )
        {
            return i;
        }
    }
    return 30;
}

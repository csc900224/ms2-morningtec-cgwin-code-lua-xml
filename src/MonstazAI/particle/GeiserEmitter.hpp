#ifndef ____INCLUDE__GLOBALARCADE__GEISEREMITTER_HPP__
#define ____INCLUDE__GLOBALARCADE__GEISEREMITTER_HPP__

#include <limits>

#include "MonstazAI/particle/Emitter.hpp"

class Particle;

class GeiserEmitter : public Emitter
{
public:
    GeiserEmitter( ParticleFunctor* creator, ParticleSystem* particleSystem, float x, float y, float vx, float vy, float particlesPerSecond, float life = std::numeric_limits<float>::max() );
    virtual ~GeiserEmitter() {}

    bool Update( float dt );

protected:
    float m_x;
    float m_y;
    float m_vx;
    float m_vy;
    float m_life;

    float m_pps;

private:
    float m_counter;
};

#endif // ____INCLUDE__GLOBALARCADE__EXPLOSIONEMITTER_HPP__

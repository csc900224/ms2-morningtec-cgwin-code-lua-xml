#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/ExplosionParticle.hpp"

ExplosionParticle::ExplosionParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image, bool scale )
    : Particle( x, y, vx, vy )
    , m_decay( decayRate )
    , m_alpha( 255 )
    , m_time( 0 )
    , m_image( image )
    , m_scale( scale )
{
}

ExplosionParticle::~ExplosionParticle()
{
}

bool ExplosionParticle::Update( float dt )
{
    m_alpha -= dt * m_decay;
    m_pos.x += m_vx * dt;
    m_pos.y += m_vy * dt;

    m_time += dt;
    while( m_time > 0.066f )
    {
        m_time -= 0.066f;
        m_vx *= 0.85f;
        m_vy *= 0.85f;
    }

    return m_alpha > 0;
}

void ExplosionParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );
    Claw::TriangleEngine::Blit( target, m_image,
        m_pos.x * scale - offset.x,
        m_pos.y * scale - offset.y,
        0,
        m_scale ? 0.5f + ( cos( m_alpha / 255.f * M_PI / 2 ) ) * 4 : 1,
        Vectorf( m_image->GetSize() / 2 ) );

}

#ifndef ____INCLUDE__GLOBALARCADE__EXPLOSIONEMITTER_HPP__
#define ____INCLUDE__GLOBALARCADE__EXPLOSIONEMITTER_HPP__

#include "MonstazAI/particle/Emitter.hpp"

class Particle;

class ExplosionEmitter : public Emitter
{
public:
    ExplosionEmitter( ParticleFunctor* creator, ParticleSystem* particleSystem, float x, float y, float vx, float vy,
                        float time, float particlesPerSecond );
    virtual ~ExplosionEmitter() {}

    bool Update( float dt );

    void SetPosition( float x, float y ) { m_x = x; m_y = y; }

protected:
    float m_x;
    float m_y;
    float m_vx;
    float m_vy;

    float m_time;
    float m_pps;

private:
    float m_counter;
};

#endif // ____INCLUDE__GLOBALARCADE__EXPLOSIONEMITTER_HPP__

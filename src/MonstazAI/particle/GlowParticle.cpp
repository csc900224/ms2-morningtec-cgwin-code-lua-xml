#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/GlowParticle.hpp"
#include "MonstazAI/RNG.hpp"

GlowParticle::GlowParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image )
    : Particle( x, y, vx, vy )
    , m_decay( decayRate )
    , m_alpha( 255 )
    , m_image( image )
{
}

GlowParticle::~GlowParticle()
{
}

bool GlowParticle::Update( float dt )
{
    m_alpha -= dt * m_decay;
    return m_alpha > 0;
}

void GlowParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );
    Claw::TriangleEngine::BlitAdditive( target, m_image, m_pos.x * scale - offset.x, m_pos.y * scale - offset.y, 0, g_rng.GetDouble( 0.5f, 2 ), Vectorf( m_image->GetSize() / 2 ) );
}

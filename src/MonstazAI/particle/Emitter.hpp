#ifndef ____INCLUDE__GLOBALARCADE__EMITTER_HPP__
#define ____INCLUDE__GLOBALARCADE__EMITTER_HPP__

#include "claw/base/WeakPtr.hpp"

#include "MonstazAI/particle/Particle.hpp"

class ParticleSystem;

class Emitter : public Claw::WeakRefCounter
{
public:
    virtual ~Emitter() {}

    virtual bool Update( float dt ) = 0;

    virtual void SetPosition( float x, float y ) {}

protected:
    Emitter( ParticleSystem* particleSystem, ParticleFunctor* creator )
        : m_particle( particleSystem )
        , f_creator( creator )
    {}

    ParticleSystem* m_particle;
    ParticleFunctorPtr f_creator;
};

#endif // ____INCLUDE__GLOBALARCADE__EMITTER_HPP__

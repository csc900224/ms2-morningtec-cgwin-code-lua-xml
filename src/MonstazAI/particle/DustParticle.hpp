#ifndef ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class DustParticle : public Particle
{
public:
    DustParticle( float x, float y, float vx, float vy, Claw::Surface* image, Claw::Surface* shadow, float time = 5 );
    ~DustParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale );

private:
    float m_time;
    float m_target;
    float m_shadowScale;
    float m_angle, m_angleDt;

    Claw::SurfacePtr m_image, m_shadow;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

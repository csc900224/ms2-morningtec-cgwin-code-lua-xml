#include "MonstazAI/particle/ExplosionEmitter.hpp"
#include "MonstazAI/particle/ParticleSystem.hpp"
#include "MonstazAI/RNG.hpp"

#include "MonstazAI/math/Vector.hpp"

ExplosionEmitter::ExplosionEmitter( ParticleFunctor* creator, ParticleSystem* ps, float x, float y, float vx, float vy,
                                    float time, float particlesPerSecond )
    : Emitter( ps, creator )
    , m_x( x )
    , m_y( y )
    , m_vx( vx )
    , m_vy( vy )
    , m_time( time )
    , m_pps( particlesPerSecond )
    , m_counter( 0 )
{
}

bool ExplosionEmitter::Update( float dt )
{
    float vx, vy;

    m_time -= dt;
    if( m_time < 0 )
    {
        dt += m_time;
    }

    m_counter += dt * m_pps;

    int i = (int)m_counter;
    m_counter -= i;

    while( i-- )
    {
        Vectorf v( g_rng.GetDouble() * 0.5 + 0.6, 0 );
        v.Rotate( g_rng.GetDouble() * M_PI * 2 );
        m_particle->Add( (*f_creator)( m_x, m_y, v.x * m_vx, v.y * m_vy ) );
    }

    return m_time > 0;
}

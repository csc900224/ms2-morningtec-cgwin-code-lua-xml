#ifndef ____INCLUDE__GLOBALARCADE__TARGETPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__TARGETPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class TargetParticle : public Particle
{
public:
    TargetParticle( float x, float y, float vx, float vy, Claw::Surface* circle1, Claw::Surface* circle2, Claw::Surface* arrow );
    ~TargetParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    Claw::SurfacePtr m_circle1;
    Claw::SurfacePtr m_circle2;
    Claw::SurfacePtr m_arrow;

    float m_time;
};

class TargetParticleFunctor : public ParticleFunctor
{
public:
    TargetParticleFunctor( Claw::Surface* circle1, Claw::Surface* circle2, Claw::Surface* arrow )
        : m_circle1( circle1 )
        , m_circle2( circle2 )
        , m_arrow( arrow )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new TargetParticle( x, y, vx, vy, m_circle1, m_circle2, m_arrow );
    }

private:
    Claw::SurfacePtr m_circle1;
    Claw::SurfacePtr m_circle2;
    Claw::SurfacePtr m_arrow;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

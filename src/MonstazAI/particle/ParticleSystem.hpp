#ifndef ____INCLUDE__GLOBALARCADE__PARTICLESYSTEM_HPP__
#define ____INCLUDE__GLOBALARCADE__PARTICLESYSTEM_HPP__

#include <vector>

#include "claw/base/SmartPtr.hpp"
#include "claw/base/WeakPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/particle/Emitter.hpp"

class ParticleSystem : public Claw::RefCounter, public Claw::WeakRefCounter
{
public:
    ParticleSystem( bool active );
    ~ParticleSystem();

    void Update( float dt );
    void Render( Claw::Surface* surface, const Vectorf& offset );
    void RenderDirect( Claw::Surface* surface, const Vectorf& offset );

    bool Add( Particle* particle );
    bool Add( Emitter* emitter );

    bool Remove( Emitter* emitter );

    int GetNumParticles() const { return m_particles.size(); }
    int GetNumEmiters() const { return m_emitters.size(); }

private:
    std::vector<Particle*> m_particles;
    std::vector<Emitter*> m_emitters;

    bool m_active;
};

typedef Claw::SmartPtr<ParticleSystem> ParticleSystemPtr;

#endif // ____INCLUDE__GLOBALARCADE__PARTICLESYSTEM_HPP__

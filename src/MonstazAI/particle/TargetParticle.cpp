#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/TargetParticle.hpp"

static const float TimeLimit = 1.25f;
static const float TimeFade = 0.25f;

TargetParticle::TargetParticle( float x, float y, float vx, float vy, Claw::Surface* circle1, Claw::Surface* circle2, Claw::Surface* arrow )
    : Particle( x, y - 50, vx, vy )
    , m_circle1( circle1 )
    , m_circle2( circle2 )
    , m_arrow( arrow )
    , m_time( 0 )
{
}

TargetParticle::~TargetParticle()
{
}

bool TargetParticle::Update( float dt )
{
    m_time += dt;

    return m_time < TimeLimit + TimeFade;
}

void TargetParticle::Render( Claw::Surface* target, const Vectorf& offset, float s ) const
{
    float a = 1;
    if( m_time > TimeLimit )
    {
        a *= std::max( 0.f, ( ( TimeLimit + TimeFade ) - m_time ) / TimeFade);
    }

    Vectorf v( 1, 0 );
    v.Rotate( m_time );

    float m[4] = { v.x, v.y, -v.y * 0.65f, v.x * 0.65f };

    float x = m_pos.x * s - offset.x;
    float y = ( m_pos.y + 50 ) * s - offset.y;

    m_circle1->SetAlpha( 255 * a * ( 0.6f + 0.4f * sin( m_time * 5 ) ) );
    m_circle2->SetAlpha( 255 * a );
    m_arrow->SetAlpha( 255 * a );

    Claw::TriangleEngine::Blit( target, m_circle1, x, y, m, Vectorf( m_circle1->GetSize() / 2) );
    Claw::TriangleEngine::Blit( target, m_circle2, x, y, m, Vectorf( m_circle2->GetSize() / 2 ) );

    float o = cos( m_time * 7 );
    Claw::TriangleEngine::Blit( target, m_arrow, x, y, m, Vectorf( m_arrow->GetWidth() / 2, m_arrow->GetHeight() * ( 1.25f + o * 0.25f ) ) );
    for( int i=1; i<=3; i++ )
    {
        Vectorf v( 1, 0 );
        v.Rotate( m_time + i * M_PI / 2.f );

        float m[4] = { v.x, v.y, -v.y * 0.65f, v.x * 0.65f };
        Claw::TriangleEngine::Blit( target, m_arrow, x, y, m, Vectorf( m_arrow->GetWidth() / 2, m_arrow->GetHeight() * ( 1.25f + o * 0.25f ) ) );
    }
}

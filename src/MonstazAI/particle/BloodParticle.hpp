#ifndef ____INCLUDE__GLOBALARCADE__BLOODPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__BLOODPARTICLE_HPP__

#include <stdio.h>

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/RNG.hpp"

class BloodParticle : public Particle
{
public:
    BloodParticle( float x, float y, float vx, float vy, float vz, Claw::Surface* image, Claw::Surface* shadow, float scale );
    ~BloodParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale );

private:
    float m_vz;
    float m_time;
    float m_z;
    float m_rotSpeed;
    float m_alpha;
    float m_scale;

    Claw::SurfacePtr m_image;
    Claw::SurfacePtr m_shadow;
};

class BloodParticleFunctor : public ParticleFunctor
{
public:
    BloodParticleFunctor( float scale );

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new BloodParticle( x, y, vx, vy, 40 + g_rng.GetDouble() * 60, m_image[g_rng.GetInt()%6], m_shadow, m_scale );
    }

private:
    float m_scale;
    Claw::SurfacePtr m_image[6];
    Claw::SurfacePtr m_shadow;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

#ifndef ____INCLUDE__GLOBALARCADE__EXPLOSIONPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__EXPLOSIONPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class ExplosionParticle : public Particle
{
public:
    ExplosionParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image, bool scale = true );
    ~ExplosionParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    float m_decay;
    float m_alpha;
    float m_time;
    bool m_scale;

    Claw::SurfacePtr m_image;
};

class ExplosionParticleFunctor : public ParticleFunctor
{
public:
    ExplosionParticleFunctor( float decayRate, Claw::Surface* image, bool scale = true )
        : m_decay( decayRate )
        , m_image( image )
        , m_scale( scale )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new ExplosionParticle( x, y, vx, vy, m_decay, m_image, m_scale );
    }

private:
    float m_decay;
    Claw::SurfacePtr m_image;
    bool m_scale;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

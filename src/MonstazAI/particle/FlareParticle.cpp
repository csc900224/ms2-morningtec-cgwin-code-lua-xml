#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/FlareParticle.hpp"
#include "MonstazAI/RNG.hpp"

FlareParticle::FlareParticle( float x, float y, float vx, float vy, float decayRate, float rotation, Claw::Surface* image, float mul )
    : Particle( x, y+10000, vx, vy )
    , m_decay( decayRate )
    , m_alpha( 255 )
    , m_image( image )
    , m_rotation( rotation )
    , m_scale( 4 )
    , m_mul( mul )
{
}

FlareParticle::~FlareParticle()
{
}

bool FlareParticle::Update( float dt )
{
    m_scale -= dt * 2.5f;
    m_alpha -= dt * m_decay;
    return m_alpha > 0;
}

void FlareParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha * m_mul );
    Claw::TriangleEngine::BlitAdditive( target, m_image, m_pos.x * scale - offset.x, ( m_pos.y - 10000 )* scale - offset.y, m_rotation, m_scale, Vectorf( m_image->GetSize() / 2 ) );
}

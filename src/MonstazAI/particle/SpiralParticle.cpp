#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/SpiralParticle.hpp"

SpiralParticle::SpiralParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image )
    : Particle( x, y, vx, vy )
    , m_decay( decayRate )
    , m_alpha( 128 )
    , m_image( image )
{
}

SpiralParticle::~SpiralParticle()
{
}

bool SpiralParticle::Update( float dt )
{
    m_alpha -= dt * m_decay;
    return m_alpha > 0;
}

void SpiralParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );
    target->BlitAdditive( m_pos.x * scale - offset.x - m_image->GetWidth() / 2, m_pos.y * scale - offset.y - m_image->GetHeight() / 2, m_image );
}

#include "MonstazAI/particle/TextParticle.hpp"
#include "MonstazAI/GameManager.hpp"

TextParticle::TextParticle( float x, float y, float vz, Claw::ScreenText* text, Claw::FontEx* font )
    : Particle( x, y, 0, 0 )
    , m_vz( vz )
    , m_time( 0 )
    , m_z( 40 )
    , m_alpha( 255 )
    , m_text( text )
    , m_font( font )
{
}

TextParticle::~TextParticle()
{
}

bool TextParticle::Update( float dt )
{
    m_z += m_vz * dt;
    m_alpha -= dt * 128;

    return m_alpha > 0;
}

void TextParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_font->GetSurface()->SetAlpha( m_alpha );
    m_text->Draw( target, ( m_pos.x - m_text->GetWidth() / 2 ) * scale - offset.x, ( m_pos.y - m_z ) * scale - offset.y );
}

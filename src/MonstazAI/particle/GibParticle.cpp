#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/GibParticle.hpp"
#include "MonstazAI/GameManager.hpp"

GibParticle::GibParticle( float x, float y, float vx, float vy, float vz, Claw::Surface* image, Claw::Surface* shadow, float shadowScale, bool leave, bool blood )
    : Particle( x, y, vx / 4, vy / 6 )
    , m_vz( vz )
    , m_time( 0 )
    , m_z( g_rng.GetDouble() * 15 + 20 )
    , m_rotSpeed( g_rng.GetDouble() * 20 - 10 )
    , m_alpha( 255 )
    , m_image( image )
    , m_shadow( shadow )
    , m_leave( leave )
    , m_blood( blood )
    , m_shadowScale( shadowScale )
{
}

GibParticle::~GibParticle()
{
}

bool GibParticle::Update( float dt )
{
    m_pos.x += m_vx * dt;
    m_pos.y += m_vy * dt;
    m_z += m_vz * dt;
    m_vz -= dt * 200;

    m_time += dt * m_rotSpeed;

    if( abs(m_vx) < 2 && !m_leave )
    {
        m_alpha -= dt * 512;
    }

    if( m_z < 0 )
    {
        if( abs(m_vx) > 10 && m_blood )
        {
            GameManager::GetInstance()->GenerateSplatter( GetPos(), 1 );
        }

        m_z = 0;
        m_vz *= -0.5f;
        m_vx *= 0.5f;
        m_vy *= 0.5f;
        m_rotSpeed *= 0.5f;
    }

    if( m_leave )
    {
        if( abs( m_vx ) < 1 && abs( m_vy ) < 1 )
        {
            GameManager::GetInstance()->AddSplatter( m_image, m_pos, 1, m_time );
            return false;
        }
        return true;
    }
    else
    {
        return m_alpha > 0;
    }
}

void GibParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );

    Claw::TriangleEngine::Blit( target, m_image,
        m_pos.x * scale - offset.x,
        ( m_pos.y - m_z ) * scale - offset.y,
        m_time,
        1,
        Vectorf( m_image->GetSize() / 2 ) );
}

void GibParticle::RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale )
{
    int a = int( ( 128 - m_z * 3 ) * m_alpha ) >> 8;

    if( a > 0 )
    {
        m_shadow->SetAlpha( a );

        Claw::TriangleEngine::Blit( target, m_shadow,
            m_pos.x * scale - offset.x,
            m_pos.y * scale - offset.y,
            0,
            m_shadowScale,
            Vectorf( m_shadow->GetSize() / 2 ) );
    }
}

#ifndef ____INCLUDE__GLOBALARCADE__PARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__PARTICLE_HPP__

#include <vector>

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/Renderable.hpp"

class Particle : public Renderable
{
public:
    virtual ~Particle() {}

    virtual bool Update( float dt ) = 0;
    virtual void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const = 0;
    virtual void RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale ) {}

protected:
    Particle( float x, float y, float vx = 0, float vy = 0 )
        : Renderable( x, y )
        , m_vx( vx )
        , m_vy( vy )
    {}

    Particle() {}

    float m_vx, m_vy;
};

class ParticleFunctor : public Claw::RefCounter
{
public:
    virtual Particle* operator()( float x, float y, float vx, float vy ) = 0;
};

typedef Claw::SmartPtr<ParticleFunctor> ParticleFunctorPtr;

class MultiParticleFunctor : public ParticleFunctor
{
public:
    virtual Particle* operator()( float x, float y, float vx, float vy );

    void Add( ParticleFunctor* functor ) { m_functors.push_back( ParticleFunctorPtr( functor ) ); }

private:
    std::vector<ParticleFunctorPtr> m_functors;
};

#endif // ____INCLUDE__GLOBALARCADE__PARTICLE_HPP__

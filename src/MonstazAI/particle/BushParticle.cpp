#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/particle/BushParticle.hpp"

BushParticle::BushParticle( float x, float y, float vx, float vy, float dz, Claw::Surface* image, Claw::Surface* shadow )
    : Particle( x, y, vx, vy )
    , m_time( 0 )
    , m_image( image )
    , m_shadow( shadow )
    , m_rate( ( g_rng.GetDouble() * 2 - 1 ) * 0.5f )
    , m_angle( g_rng.GetDouble() * 2 * M_PI )
    , m_z( g_rng.GetDouble() * 10.f + 10.f )
    , m_dz( dz + g_rng.GetDouble() - 0.25f )
{
}

BushParticle::~BushParticle()
{
}

bool BushParticle::Update( float dt )
{
    m_pos.x += m_vx * dt;
    m_pos.y += m_vy * dt;
    m_angle += m_rate;
    m_z = std::max( 0.f, m_z + m_dz );

    m_time += dt;
    while( m_time > 0.066f )
    {
        m_time -= 0.066f;
        m_vx *= 0.88f;
        m_vy *= 0.88f;
        m_rate *= 0.88f;
        m_dz -= 0.05f;
    }

    if( m_z <= 0 )
    {
        GameManager::GetInstance()->AddSplatter( m_image, m_pos, 1, m_angle );
        return false;
    }

    return true;
}

void BushParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( 255 );
    Claw::TriangleEngine::Blit( target, m_image,
        m_pos.x * scale - offset.x,
        ( m_pos.y - m_z ) * scale - offset.y,
        m_angle,
        1,
        Vectorf( m_image->GetSize() / 2 ) );
}

void BushParticle::RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale )
{
    int a = int( 128 - m_z * 3 );

    if( a > 0 )
    {
        m_shadow->SetAlpha( a );

        Claw::TriangleEngine::Blit( target, m_shadow,
            m_pos.x * scale - offset.x,
            m_pos.y * scale - offset.y,
            0,
            0.25f,
            Vectorf( m_shadow->GetSize() / 2 ) );
    }
}
#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/BloodParticle.hpp"
#include "MonstazAI/GameManager.hpp"

BloodParticle::BloodParticle( float x, float y, float vx, float vy, float vz, Claw::Surface* image, Claw::Surface* shadow, float scale )
    : Particle( x, y, vx / 4, vy / 6 )
    , m_vz( vz )
    , m_time( 0 )
    , m_z( g_rng.GetDouble() * 15 + 20 )
    , m_rotSpeed( g_rng.GetDouble() * 20 - 10 )
    , m_alpha( 255 )
    , m_scale( scale )
    , m_image( image )
    , m_shadow( shadow )
{
}

BloodParticle::~BloodParticle()
{
}

bool BloodParticle::Update( float dt )
{
    m_pos.x += m_vx * dt;
    m_pos.y += m_vy * dt;
    m_z += m_vz * dt;
    m_vz -= dt * 200;

    m_time += dt * m_rotSpeed;
    m_alpha -= dt * 192;
    m_scale -= dt * 0.5f;

    if( m_z < 0 )
    {
        GameManager::GetInstance()->GenerateSplatter( GetPos(), 1 );
    }
    return m_alpha > 0 && m_z > 0 && m_scale > 0;
}

void BloodParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );

    Claw::TriangleEngine::Blit( target, m_image,
        m_pos.x * scale - offset.x,
        ( m_pos.y - m_z ) * scale - offset.y,
        m_time,
        m_scale,
        Vectorf( m_image->GetSize() / 2 ) );
}

void BloodParticle::RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale )
{
    int a = int( ( 128 - m_z * 3 ) * m_alpha ) >> 8;

    if( a > 0 )
    {
        m_shadow->SetAlpha( a );

        Claw::TriangleEngine::Blit( target, m_shadow,
            m_pos.x * scale - offset.x,
            m_pos.y * scale - offset.y,
            0,
            0.5f,
            Vectorf( m_shadow->GetSize() / 2 ) );
    }
}

BloodParticleFunctor::BloodParticleFunctor( float scale )
    : m_scale( scale )
{
    for( int i=0; i<6; i++ )
    {
        m_image[i].Reset( GameManager::GetBloodAsset( i ) );
    }
    m_shadow.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/shadow.png@linear" ) );
}

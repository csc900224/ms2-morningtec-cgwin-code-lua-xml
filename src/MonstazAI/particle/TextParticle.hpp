#ifndef ____INCLUDE__GLOBALARCADE__TEXTPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__TEXTPARTICLE_HPP__

#include <stdio.h>

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/Surface.hpp"
#include "claw/graphics/ScreenText.hpp"

#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/RNG.hpp"

class TextParticle : public Particle
{
public:
    TextParticle( float x, float y, float vz, Claw::ScreenText* text, Claw::FontEx* font );
    ~TextParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    float m_vz;
    float m_time;
    float m_z;
    float m_alpha;

    Claw::ScreenTextPtr m_text;
    Claw::FontExPtr m_font;
};

class TextParticleFunctor : public ParticleFunctor
{
public:
    TextParticleFunctor()
        : m_font( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_big.xml@linear" ) )
    {
        Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
        fontSet->AddFont( "f", m_font );

        Claw::Text::Format format;
        format.SetFontSet( fontSet );
        format.SetFontId( "f" );
        format.SetHorizontalAlign( Claw::Text::Format::HA_FLUSH_RIGHT );

    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return NULL;
    }

    Particle* operator()( float x, float y, const Claw::NarrowString& str )
    {
        Claw::ScreenText* st = new Claw::ScreenText( m_font, Claw::String( str ) );
        return new TextParticle( x, y, 40, st, m_font );
    }

private:
    Claw::FontExPtr m_font;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

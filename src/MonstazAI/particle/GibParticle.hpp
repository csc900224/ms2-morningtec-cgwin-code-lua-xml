#ifndef ____INCLUDE__GLOBALARCADE__GIBPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__GIBPARTICLE_HPP__

#include <stdio.h>

#include "claw/base/AssetDict.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/RNG.hpp"

class GibParticle : public Particle
{
public:
    GibParticle( float x, float y, float vx, float vy, float vz, Claw::Surface* image, Claw::Surface* shadow, float shadowScale = 0.5f, bool leave = false, bool blood = true );
    ~GibParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale );

private:
    float m_vz;
    float m_time;
    float m_z;
    float m_rotSpeed;
    float m_alpha;
    float m_shadowScale;
    bool m_leave;
    bool m_blood;

    Claw::SurfacePtr m_image;
    Claw::SurfacePtr m_shadow;
};

class GibParticleFunctor : public ParticleFunctor
{
public:
    GibParticleFunctor( Claw::SurfacePtr* img, int num, bool leave, bool blood )
        : m_image( img )
        , m_num( num )
        , m_leave( leave )
        , m_blood( blood )
    {
        m_shadow.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/shadow.png@linear" ) );
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new GibParticle( x, y, vx, vy, 40 + g_rng.GetDouble() * 60, m_image[g_rng.GetInt()%m_num], m_shadow, 0.5f, m_leave, m_blood );
    }

private:
    float m_decay;
    Claw::SurfacePtr* m_image;
    int m_num;
    Claw::SurfacePtr m_shadow;
    bool m_leave;
    bool m_blood;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

#include "MonstazAI/particle/GeiserEmitter.hpp"
#include "MonstazAI/particle/ParticleSystem.hpp"
#include "MonstazAI/RNG.hpp"

#include "MonstazAI/math/Vector.hpp"

GeiserEmitter::GeiserEmitter( ParticleFunctor* creator, ParticleSystem* ps, float x, float y, float vx, float vy, float particlesPerSecond, float life )
    : Emitter( ps, creator )
    , m_x( x )
    , m_y( y )
    , m_vx( vx )
    , m_vy( vy )
    , m_life( life )
    , m_pps( particlesPerSecond )
    , m_counter( 0 )
{
}

bool GeiserEmitter::Update( float dt )
{
    float vx, vy;

    m_counter += dt * m_pps;

    int i = (int)m_counter;
    m_counter -= i;

    while( i-- )
    {
        m_particle->Add( (*f_creator)( m_x, m_y, ( g_rng.GetDouble() * 2 - 1 ) * m_vx, ( g_rng.GetDouble() * 2 - 1 ) * m_vy ) );
    }

    m_life -= dt;
    return m_life > 0;
}

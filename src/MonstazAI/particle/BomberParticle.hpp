#ifndef ____INCLUDE__GLOBALARCADE__BOMBERPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__BOMBERPARTICLE_HPP__

#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/RNG.hpp"

class BomberParticle : public Particle
{
public:
    BomberParticle( float x, float y, float vx, float vy, Claw::Surface* image, int offset );
    ~BomberParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    Claw::SurfacePtr m_image;
    Vectorf m_dir;
    Vectorf m_target;
    float m_delay;
};

class BomberParticleFunctor : public ParticleFunctor
{
public:
    BomberParticleFunctor( Claw::Surface* image )
        : m_image( image )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        CLAW_ASSERT( false );
        return NULL;
    }

    Particle* operator()( float x, float y, int offset )
    {
        return new BomberParticle( x, y, 0, 0, m_image, offset );
    }

private:
    Claw::SurfacePtr m_image;
};

typedef Claw::SmartPtr<BomberParticleFunctor> BomberParticleFunctorPtr;

#endif

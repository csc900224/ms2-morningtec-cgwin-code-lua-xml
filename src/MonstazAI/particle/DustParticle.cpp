#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/particle/DustParticle.hpp"

DustParticle::DustParticle( float x, float y, float vx, float vy, Claw::Surface* image, Claw::Surface* shadow, float time )
    : Particle( x, y, vx, vy )
    , m_time( 0 )
    , m_target( time )
    , m_image( image )
    , m_shadow( shadow )
    , m_shadowScale( shadow ? ( float( image->GetWidth() ) / shadow->GetWidth() ) : 0 )
    , m_angle( g_rng.GetDouble() * M_PI * 2 )
    , m_angleDt( g_rng.GetDouble() - 0.5f )
{
}

DustParticle::~DustParticle()
{
}

bool DustParticle::Update( float dt )
{
    m_pos.x += m_vx * dt;
    m_pos.y += m_vy * dt;

    m_time += dt;
    m_angle += m_angleDt * dt * 2;

    return m_time < m_target;
}

void DustParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    int a = 192 * std::min( 1.f, std::min( m_time, m_target - m_time ) );
    m_image->SetAlpha( a );
    Claw::TriangleEngine::Blit( target, m_image,
        m_pos.x * scale - offset.x,
        m_pos.y * scale - offset.y - m_image->GetHeight() / 2,
        m_angle,
        1,
        Vectorf( m_image->GetSize() / 2 ) );
}

void DustParticle::RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale )
{
    if( !m_shadow ) return;
    int a = 64 * std::min( 1.f, std::min( m_time, m_target - m_time ) );
    m_shadow->SetAlpha( a );
    Claw::TriangleEngine::Blit( target, m_shadow,
        m_pos.x * scale - offset.x,
        m_pos.y * scale - offset.y,
        0,
        m_shadowScale,
        Vectorf( m_shadow->GetSize() / 2 ) );
}
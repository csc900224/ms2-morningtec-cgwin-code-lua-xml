#ifndef ____INCLUDE__GLOBALARCADE__BUSHPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__BUSHPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class BushParticle : public Particle
{
public:
    BushParticle( float x, float y, float vx, float vy, float dz, Claw::Surface* image, Claw::Surface* shadow );
    ~BushParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;
    void RenderShadow( Claw::Surface* target, const Vectorf& offset, float scale );

private:
    float m_time;
    float m_rate;
    float m_angle;
    float m_z, m_dz;

    Claw::SurfacePtr m_image, m_shadow;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/particle/GeiserParticle.hpp"

GeiserParticle::GeiserParticle( float x, float y, float vx, float vy, float vz, float decayRate, Claw::Surface* image, float scale )
    : Particle( x, y, vx, vy )
    , m_vz( vz )
    , m_decay( decayRate )
    , m_alpha( 255 )
    , m_time( 0 )
    , m_z( 0 )
    , m_image( image )
    , m_scale( scale )
{
}

GeiserParticle::~GeiserParticle()
{
}

bool GeiserParticle::Update( float dt )
{
    m_alpha -= dt * m_decay;
    m_pos.x += m_vx * dt;
    m_pos.y += m_vy * dt;
    m_z -= m_vz * dt;

    m_time += dt;
    while( m_time > 0.066f )
    {
        m_time -= 0.066f;
        m_vx *= 0.75f;
        m_vy *= 0.75f;
        m_vz *= 0.95f;
    }

    return m_alpha > 0;
}

void GeiserParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );
    Claw::TriangleEngine::Blit( target, m_image,
        m_pos.x * scale - offset.x,
        ( m_pos.y + m_z ) * scale - offset.y,
        0,
        0.5f + ( 255 - m_alpha ) / 255.f * scale,
        Vectorf( m_image->GetSize() / 2 ) );
}

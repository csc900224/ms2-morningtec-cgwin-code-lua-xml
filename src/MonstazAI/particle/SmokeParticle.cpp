#include "claw/graphics/TriangleEngine.hpp"

#include "MonstazAI/RNG.hpp"
#include "MonstazAI/particle/SmokeParticle.hpp"

SmokeParticle::SmokeParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image )
    : Particle( x, y, vx, vy )
    , m_decay( decayRate )
    , m_alpha( 255 )
    , m_time( 0 )
    , m_image( image )
    , m_angle( g_rng.GetDouble() * M_PI * 2 )
{
}

SmokeParticle::~SmokeParticle()
{
}

bool SmokeParticle::Update( float dt )
{
    m_alpha -= dt * m_decay * 2;

    m_time += dt;
    while( m_time > 0.066f )
    {
        m_time -= 0.066f;
    }

    return m_alpha > 0;
}

void SmokeParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( m_alpha );

    Claw::TriangleEngine::Blit( target, m_image, m_pos.x * scale - offset.x, m_pos.y * scale - offset.y - 10 * scale, m_angle, 1, Vectorf( m_image->GetSize() / 2 ) );
}

#ifndef ____INCLUDE__GLOBALARCADE__SMOKEPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__SMOKEPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class SmokeParticle : public Particle
{
public:
    SmokeParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image );
    ~SmokeParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    float m_decay;
    float m_alpha;
    float m_time;
    float m_angle;

    Claw::SurfacePtr m_image;
};

class SmokeParticleFunctor : public ParticleFunctor
{
public:
    SmokeParticleFunctor( float decayRate, Claw::Surface* image )
        : m_decay( decayRate )
        , m_image( image )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new SmokeParticle( x, y, vx, vy, m_decay, m_image );
    }

private:
    float m_decay;
    Claw::SurfacePtr m_image;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

#ifndef ____INCLUDE__GLOBALARCADE__SPIRALPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__SPIRALPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class SpiralParticle : public Particle
{
public:
    SpiralParticle( float x, float y, float vx, float vy, float decayRate, Claw::Surface* image );
    ~SpiralParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    float m_decay;
    float m_alpha;

    Claw::SurfacePtr m_image;
};

class SpiralParticleFunctor : public ParticleFunctor
{
public:
    SpiralParticleFunctor( float decayRate, Claw::Surface* image )
        : m_decay( decayRate )
        , m_image( image )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new SpiralParticle( x, y, vx, vy, m_decay, m_image );
    }

private:
    float m_decay;
    Claw::SurfacePtr m_image;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

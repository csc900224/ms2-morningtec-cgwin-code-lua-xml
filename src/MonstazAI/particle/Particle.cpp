#include "MonstazAI/particle/Particle.hpp"

Particle* MultiParticleFunctor::operator()( float x, float y, float vx, float vy )
{
    CLAW_ASSERT( !m_functors.empty() );
    int idx = rand() % m_functors.size();
    return (*m_functors[idx])( x, y, vx, vy );
}

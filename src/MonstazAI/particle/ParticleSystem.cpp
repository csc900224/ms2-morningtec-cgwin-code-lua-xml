#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/particle/ParticleSystem.hpp"

ParticleSystem::ParticleSystem( bool active )
    : m_active( active )
{
}

ParticleSystem::~ParticleSystem()
{
    m_active = false;

    for( std::vector<Particle*>::iterator it = m_particles.begin(); it != m_particles.end(); ++it )
    {
        delete *it;
    }

    for( std::vector<Emitter*>::iterator it = m_emitters.begin(); it != m_emitters.end(); ++it )
    {
        delete *it;
    }
}

void ParticleSystem::Update( float dt )
{
    std::vector<Particle*>::iterator it = m_particles.begin();

    while( it != m_particles.end() )
    {
        if( (*it)->Update( dt ) )
        {
            ++it;
        }
        else
        {
            delete *it;
            it = m_particles.erase( it );
        }
    }

    std::vector<Emitter*>::iterator ite = m_emitters.begin();

    while( ite != m_emitters.end() )
    {
        if( (*ite)->Update( dt ) )
        {
            ++ite;
        }
        else
        {
            delete *ite;
            ite = m_emitters.erase( ite );
        }
    }
}

void ParticleSystem::Render( Claw::Surface* surface, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();
    RenderableManager* rm = GameManager::GetInstance()->GetRenderableManager();
    for( std::vector<Particle*>::iterator it = m_particles.begin(); it != m_particles.end(); ++it )
    {
        rm->Add( *it );
        (*it)->RenderShadow( surface, offset, scale );
    }
}

void ParticleSystem::RenderDirect( Claw::Surface* surface, const Vectorf& offset )
{
    const float scale = GameManager::GetGameScale();
    for( std::vector<Particle*>::iterator it = m_particles.begin(); it != m_particles.end(); ++it )
    {
        (*it)->Render( surface, offset, scale );
        (*it)->RenderShadow( surface, offset, scale );
    }
}

bool ParticleSystem::Add( Particle* particle )
{
    if( m_active )
    {
        m_particles.push_back( particle );
    }
    else
    {
        delete particle;
    }

    return m_active;
}

bool ParticleSystem::Add( Emitter* emitter )
{
    if( m_active )
    {
        m_emitters.push_back( emitter );
    }
    else
    {
        delete emitter;
    }

    return m_active;
}

bool ParticleSystem::Remove( Emitter* emitter )
{
    for( std::vector<Emitter*>::iterator it = m_emitters.begin(); it != m_emitters.end(); ++it )
    {
        if( *it == emitter )
        {
            delete *it;
            m_emitters.erase( it );

            return true;
        }
    }

    return false;
}

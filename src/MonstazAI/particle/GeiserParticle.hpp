#ifndef ____INCLUDE__GLOBALARCADE__GEISERPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__GEISERPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"
#include "MonstazAI/RNG.hpp"

class GeiserParticle : public Particle
{
public:
    GeiserParticle( float x, float y, float vx, float vy, float vz, float decayRate, Claw::Surface* image, float scale );
    ~GeiserParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    float m_vz;
    float m_decay;
    float m_alpha;
    float m_time;
    float m_z;
    float m_scale;

    Claw::SurfacePtr m_image;
};

class GeiserParticleFunctor : public ParticleFunctor
{
public:
    GeiserParticleFunctor( float decayRate, Claw::Surface* image )
        : m_decay( decayRate )
        , m_image( image )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new GeiserParticle( x, y, vx, vy, 70 + g_rng.GetDouble() * 30, m_decay, m_image, 4 );
    }

private:
    float m_decay;
    Claw::SurfacePtr m_image;
};

class HydrantParticleFunctor : public ParticleFunctor
{
public:
    HydrantParticleFunctor( float decayRate, Claw::Surface* image )
        : m_decay( decayRate )
        , m_image( image )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new GeiserParticle( x, y, vx, vy, 200 + g_rng.GetDouble() * 20, m_decay, m_image, 2 );
    }

private:
    float m_decay;
    Claw::SurfacePtr m_image;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

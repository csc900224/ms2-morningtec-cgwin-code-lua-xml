#ifndef ____INCLUDE__GLOBALARCADE__FLAREPARTICLE_HPP__
#define ____INCLUDE__GLOBALARCADE__FLAREPARTICLE_HPP__

#include "claw/graphics/Surface.hpp"

#include "MonstazAI/particle/Particle.hpp"

class FlareParticle : public Particle
{
public:
    FlareParticle( float x, float y, float vx, float vy, float decayRate, float rotation, Claw::Surface* image, float mul );
    ~FlareParticle();

    bool Update( float dt );
    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

private:
    float m_decay;
    float m_alpha;
    float m_rotation;
    float m_scale;
    float m_mul;

    Claw::SurfacePtr m_image;
};

class FlareParticleFunctor : public ParticleFunctor
{
public:
    FlareParticleFunctor( float decayRate, Claw::Surface* image, float rotation = 0, float mul = 1 )
        : m_decay( decayRate )
        , m_image( image )
        , m_rotation( rotation )
        , m_mul( mul )
    {
    }

    Particle* operator()( float x, float y, float vx, float vy )
    {
        return new FlareParticle( x, y, vx, vy, m_decay, m_rotation, m_image, m_mul );
    }

private:
    float m_decay;
    float m_rotation;
    Claw::SurfacePtr m_image;
    float m_mul;
};

#endif // ____INCLUDE__GLOBALARCADE__DUSTPARTICLE_HPP__

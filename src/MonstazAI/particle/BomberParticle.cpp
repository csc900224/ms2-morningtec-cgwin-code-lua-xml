#include "claw/graphics/TriangleEngine.hpp"
#include "claw/math/Matrix.hpp"

#include "MonstazAI/particle/BomberParticle.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/GameManager.hpp"

enum { OffsetZ = 10000 };
const float BombDelay = 0.15f;
const float BombDist = 400;

BomberParticle::BomberParticle( float x, float y, float vx, float vy, Claw::Surface* image, int offset )
    : Particle( x, y + OffsetZ, vx, vy )
    , m_image( image )
    , m_dir( ( g_rng.GetDouble() - 0.5f ) * 1.2f, -1 )
    , m_target( x, y )
    , m_delay( 0 )
{
    m_dir.Normalize();
    m_pos -= m_dir * ( (float)((MonstazApp*)MonstazApp::GetInstance())->GetResolution().y + offset * 200 );
}

BomberParticle::~BomberParticle()
{
}

bool BomberParticle::Update( float dt )
{
    if( m_delay > 0 )
    {
        m_delay -= dt;
    }
    else
    {
        Entity* player = GameManager::GetInstance()->GetPlayer();
        Vectorf pos( m_pos.x, m_pos.y - OffsetZ );
        Vectorf dp = pos - m_target;
        if( DotProduct( dp, dp ) < BombDist * BombDist )
        {
            m_delay = BombDelay;
            Claw::Matrix<float> m( m_dir.y, -m_dir.x, -m_dir.x, -m_dir.y );
            Vectorf v[2] = { Vectorf( -40, 0 ), Vectorf( 40, 0 ) };
            for( int i=0; i<2; i++ )
            {
                v[i] = m * v[i];
                GameManager::GetInstance()->GetShotManager()->Add( player, pos + v[i], Vectorf( 0, -1 ), 0, 20, Shot::Airstrike, 0, 1, false, 0 );
            }
        }
    }

    m_pos += m_dir * dt * 400.f;
    return m_pos.y - OffsetZ > -m_image->GetWidth();  // not a bug, it should be width
}

void BomberParticle::Render( Claw::Surface* target, const Vectorf& offset, float scale ) const
{
    m_image->SetAlpha( 128 );
    float m[4] = { m_dir.y, -m_dir.x, -m_dir.x, -m_dir.y };
    Claw::TriangleEngine::Blit( target, m_image, m_pos.x * scale - offset.x, ( m_pos.y - OffsetZ ) * scale - offset.y,
        m, Vectorf( m_image->GetSize() / 2 ) );
}

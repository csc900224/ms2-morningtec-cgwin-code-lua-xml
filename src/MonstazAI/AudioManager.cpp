#include "claw/base/AssetDict.hpp"
#include "claw/compat/Platform.h"
#include "claw/sound/mixer/AudioPosition.hpp"
#include "claw/sound/mixer/AudioRTAC.hpp"
#include "claw/sound/mixer/AudioStereoExpand.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/GameManager.hpp"

AudioManager* AudioManager::s_instance = NULL;

LUA_DECLARATION( AudioChannelWrapper )
{
    {0,0}
};

struct AudioAsset
{
    AudioSfx id;
    const char* asset;
    bool quiet;
};

AudioAsset AudioAssets[] = {
    { SFX_DEATH1, "death_1.L.rtac", true },
    { SFX_DEATH2, "death_2.L.rtac", true },
    { SFX_DEATH3, "death_3.L.rtac", true },
    { SFX_PISTOL, "smg_loop_V1.rtac", true },
    { SFX_PISTOL_END, "smg_loopEnd_V1.rtac", true },
    { SFX_MINIGUN, "minigun_loop_v2.rtac", true },
    { SFX_RELOAD1, "reload_v1.L.rtac", true },
    { SFX_RELOAD2, "reload_v2.L.rtac", true },
    { SFX_RELOAD3, "reload_v3.L.rtac", true },
    { SFX_RELOAD4, "reload_v4.L.rtac", true },
    { SFX_SHOTGUN1, "combat_shotgun_v3.rtac", true },
    { SFX_SHOTGUN2, "shotgun2.rtac", true },
    { SFX_SPINUP, "minigun-start.L.rtac", true },
    { SFX_SPINDOWN, "minigun_loopend.rtac", true },
    { SFX_ELECTRICITY, "electricity_v3.L.rtac", true },
    { SFX_ELECTRICITY_START, "shocker_start_3.L.rtac", true },
    { SFX_RAILGUN, "railgun_v2.rtac", true },
    { SFX_FLAMER, "flamer.L.rtac", true },
    { SFX_PLASMA, "Plasma.rtac", true },
    { SFX_PLASMA_END, "Plasma_end.rtac", true },
    { SFX_PLASMA_HIT, "plasma.L.rtac", true },
    { SFX_PLASMA_SECTOID, "plasma_singiel_v1.rtac", true },
    { SFX_RIPPER, "Ripper_v2.rtac", true },
    { SFX_ROCKET, "RocketLuncher1.rtac", true },
    { SFX_EXPLOSION1, "explosion_2.L.rtac", true },
    { SFX_EXPLOSION2, "explosion_v1.rtac", true },
    { SFX_EXPLOSION3, "explosion_v3.rtac", true },
    { SFX_SHOCKERBURN, "shocker_burn.L.rtac", true },
    { SFX_SHOCKERSTART, "shocer_start.rtac", true },
    { SFX_SHOCKERLOOP, "shocer_loop.rtac", true },
    { SFX_SHOCKEREND, "shocer_stop.rtac", true },
    { SFX_RICOCHET1, "ricochet_1.L.rtac", true },
    { SFX_RICOCHET2, "ricochet_2.L.rtac", true },
    { SFX_RICOCHET3, "ricochet_3.L.rtac", true },
    { SFX_RICOCHET4, "ricochet_4.L.rtac", true },
    { SFX_RICOCHET5, "RiperBounceMetal1.rtac", true },
    { SFX_RICOCHET6, "RiperBounceMetal2.rtac", true },
    { SFX_RICOCHET7, "RiperBounceMetal3.rtac", true },
    { SFX_PLAYER_HIT1, "player_hit_v2_1.L.rtac", true },
    { SFX_PLAYER_HIT2, "player_hit_v2_2.L.rtac", true },
    { SFX_PLAYER_HIT3, "player_hit_v2_3.L.rtac", true },
    { SFX_PLAYER_HIT4, "player_hit_v2_4.L.rtac", true },
    { SFX_PLAYER_HIT5, "player_hit_v2_5.L.rtac", true },
    { SFX_PLAYER_HIT6, "player_hit_v2_6.L.rtac", true },
    { SFX_PLAYER_HIT7, "player_hit_v2_7.L.rtac", true },
    { SFX_PLAYER_DEATH, "player_death_1.L.rtac", true },
    { SFX_OCTOPUS_HIT1, "tattack_1.L.rtac", true },
    { SFX_OCTOPUS_HIT2, "tattack_2.L.rtac", true },
    { SFX_OCTOPUS_HIT3, "tattack_3.L.rtac", true },
    { SFX_OCTOPUS_ATTACK1, "tattack_popr1.L.rtac", true },
    { SFX_OCTOPUS_ATTACK2, "tattack_popr2.L.rtac", true },
    { SFX_OCTOPUS_ATTACK3, "tattack_popr3.L.rtac", true },
    { SFX_OCTOPUS_ATTACK4, "tattack_popr4.L.rtac", true },
    { SFX_OCTOPUS_ATTACK5, "tattack_popr5.L.rtac", true },
    { SFX_OCTOPUS_ATTACK6, "tattack_popr6.L.rtac", true },
    { SFX_ORB, "orb.rtac", false },
    { SFX_HEALTH, "health.rtac", true },
    { SFX_WEAPON_PICKUP, "weaponpickup.rtac", true },
    { SFX_CASH, "cash.rtac", true },
    { SFX_QUAD, "vengance.rtac", true },
    { SFX_LEVELUP, "levelup.rtac", true },
    { SFX_PICKUP_APPEAR, "pickup_appear.rtac", true },
    { SFX_PICKUP_DISAPPEAR, "pickup_disappear.rtac", true },
    { SFX_NO_AMMO, "no_ammo1.L.rtac", true },
    { SFX_SIREN, "siren.ogg@cache", true },
    { SFX_NUKE, "nuke.ogg@cache", true },
    { SFX_BITE1, "bite1.rtac", true },
    { SFX_BITE2, "bite2.rtac", true },
    { SFX_FISH_DEATH1, "fish_death_1.L.rtac", true },
    { SFX_FISH_DEATH2, "fish_death_2.L.rtac", true },
    { SFX_FISH_DEATH3, "fish_death_3.L.rtac", true },
    { SFX_FISH_EXPLOSION, "fish_explosion_2.L.rtac", true },
    { SFX_ROLLER_DEATH, "roller_death.rtac", true },
    { SFX_SPIT, "spit.rtac", true },
    { SFX_SPIT_PREP, "spit_prep.rtac", true },
    { SFX_FISH_HIT1, "thit_1.L.rtac", true },
    { SFX_FISH_HIT2, "thit_2.L.rtac", true },
    { SFX_SHOPKEEPER, "shopkeeer.rtac", true },
    { SFX_MONEYDROP, "moneydrop.rtac", true },
    { SFX_TELEPORT, "teleport.rtac", true },
    { SFX_MINE_ARM, "mine-arm.rtac", true },
    { SFX_MINE_ARM2, "mine-arm2.rtac", true },
    { SFX_MINE_DETECT, "mine-detect.rtac", true },
    { SFX_SHIELD1, "shield4.rtac", true },
    { SFX_SHIELD2, "shield2.rtac", true },
    { SFX_SHIELD3, "shield1.rtac", true },
    { SFX_SHIELD4, "shield3.rtac", true },
    { SFX_SHIELD_HIT1, "shieldhit1.rtac", true },
    { SFX_SHIELD_HIT2, "shieldhit2.rtac", true },
    { SFX_FLOATER_DIE1, "floater_die_1.rtac", true },
    { SFX_FLOATER_DIE2, "floater_die_2.rtac", true },
    { SFX_FLOATER_ELECTRIC_DIE, "floater_electric_die.rtac", true },
    { SFX_HOUND_DIE1, "hound_die_1.rtac", true },
    { SFX_HOUND_DIE2, "hound_die_2.rtac", true },
    { SFX_HOUND_SHOOTING_DIE, "hound_shooting_die.rtac", true },
    { SFX_MULTIKILL1, "m050.rtac", false },
    { SFX_MULTIKILL2, "m100.rtac", false },
    { SFX_MULTIKILL3, "m150.rtac", false },
    { SFX_MULTIKILL4, "m200.rtac", false },
    { SFX_MULTIKILL5, "m250.rtac", false },
    { SFX_MULTIKILL6, "m300.rtac", false },
    { SFX_MULTIKILL7, "m400.rtac", false },
    { SFX_MULTIKILL8, "m500.rtac", false },
    { SFX_TRIPALIZER_START, "Tripalizer_start.rtac", true },
    { SFX_TRIPALIZER_LOOP, "Tripalizer_loop.rtac", true },
    { SFX_TRIPALIZER_END, "Tripalizer_stop.rtac", true },
    { SFX_CRAB_LASER, "crab_laser.rtac", true },
    { SFX_SOWER_CHARGE, "sower-charge.rtac", false },
    { SFX_SOWER_DEATH, "sower-death.rtac", false },
    { SFX_SOWER_LICK, "sower-lick.rtac", false },
    { SFX_SOWER_SPIT, "sower-spit.rtac", false },
    { SFX_SOWER_TOSS_EGGS, "sower-toss-eggs.rtac", false },
    { SFX_JET, "jet.ogg@cache", false },
    { SFX_SPIKES_SHOT, "spikes-shot.rtac", true },
    { SFX_SPIKES_1, "spikes-01.rtac", true },
    { SFX_SPIKES_2, "spikes-02.rtac", true },
    { SFX_SPIKES_3, "spikes-03.rtac", true },
    { SFX_MAGNUM, "smg_singielFire.rtac", false },
    { SFX_AMBIENT, "ambient_warsounds.ogg@cache", false },
    { SFX_GIANT_BLOT_DMG1, "giant_blot_dmg1.L.rtac", false },
    { SFX_GIANT_BLOT_DMG2, "giant_blot_dmg3.L.rtac", false },
    { SFX_GIANT_BLOT_DMG3, "giant_blot_dmg4.L.rtac", false },
    { SFX_WHALE_CHARGE, "whale_charge.rtac", false },
    { SFX_SECTOID_DEATH1, "sectoid_death_new1.rtac", true },
    { SFX_SECTOID_DEATH2, "sectoid_death_new2.rtac", true },
    { SFX_SECTOID2_DEATH1, "octobus_death1.rtac", true },
    { SFX_SECTOID2_DEATH2, "octobus_death2.rtac", true },
    { SFX_SECTOID2_DEATH3, "octobus_death3.rtac", true },
    { SFX_SECTOID2_DEATH4, "octobus_death4.rtac", true },
    { SFX_LINEGUN, "line_gun_v1.rtac", true },
    { SFX_CRAB_DIGIN, "crab_digin.rtac", true },
    { SFX_CRAB_DIGOUT, "crab_digout.rtac", true },
    { SFX_WEAPON_SWIPE, "reload_weapon9.rtac", true },
    { SFX_CAT_ANGRY, "cat_angry.ogg@cache", true },
    { SFX_CAT_VOMIT, "cat_vomit.ogg@cache", true },
    { SFX_VORTEX, "vortex.rtac", true },
    { SFX_CHAINSAW_START, "chainsaw_start.rtac", true },
    { SFX_CHAINSAW_LOOP, "chainsaw_loop.rtac", true },
    { SFX_CHAINSAW_STOP, "chainsaw_stop.rtac", true },
    { SFX_TELEPORT1, "tele1.rtac", true },
    { SFX_TELEPORT2, "tele2.rtac", true },
    { SFX_TELEPORT3, "tele3.rtac", true },
    { SFX_MECH_START, "mech_start.rtac", false },
    { SFX_MECH_LOOP, "mech_loop.rtac", false },
    { SFX_MECH_SERVO, "servo1a.rtac", false },

    { SFX_MENU_BACK, "gui_back.L.rtac", false },
    { SFX_MENU_ROTATE, "gui_rotate_world_v1.L.rtac", false },
    { SFX_MENU_SELECT, "gui_select.L.rtac", false },
    { SFX_MENU_SELECT_SMALL, "gui_select_small.L.rtac", false },
    { SFX_MENU_POPUP, "gui_popup_v1_open.L.rtac", false },
    { SFX_MENU_POPUP_CLOSE, "gui_popup_v1_close.L.rtac", false },
    { SFX_MENU_POPUP_LONG, "gui_popup__long_v1_open.L.rtac", false },
    { SFX_MENU_POPUP_LONG_CLOSE, "gui_popup__long_v1_close.L.rtac", false },
    { SFX_MENU_ZOOM, "gui_zoom_v1.L.rtac", false },
    { SFX_MENU_BUY, "gui_buy_v2.L.rtac", false },
    { SFX_MENU_GWIAZDKI, "gwiazdki.rtac", false },
    { SFX_MENU_LEVELUP, "level_up_menu.rtac", false },
    { SFX_MENU_DING, "ding.rtac", false },
    { SFX_MENU_MISSION_BULLET, "mission_bullet.rtac", false },
    { SFX_MENU_LEVEL_FINISHED, "mc_level_finnished.ogg@cache", false },
    { SFX_MENU_LEVEL_UP, "mc_level_up.ogg@cache", false },
    { SFX_MENU_RANK_UP, "mc_rank_up.ogg@cache", false },
    { SFX_MENU_RUSSIAN_WIN, "roulette_win.ogg@cache", false },
    { SFX_MENU_RUSSIAN_LOSE, "roulette_lose.ogg@cache", false },

    { SFX_VO_ANOTHER_VICTORY, "another_victory_01.ogg@cache", false },
    { SFX_VO_DONT_GIVE_UP, "dont_give_up_now_06.ogg@cache", false },
    { SFX_VO_FATAL_ENCOUNTER, "fatal_encounter_04.ogg@cache", false },
    { SFX_VO_LEVELUP, "levelup.ogg@cache", false },
    { SFX_VO_MAXED_OUT, "maxed_out_06.ogg@cache", false },
    { SFX_VO_TAKE_YOUR_REVENGE, "take_your_revange_01.ogg@cache", false },
    { SFX_VO_TOTAL_DOMINATION, "total_domination_02.ogg@cache", false },
    { SFX_VO_YOU_FAILED, "you_failed_01.ogg@cache", false },
};

LUA_DECLARATION( AudioManager )
{
    METHOD( AudioManager, Play ),
    METHOD( AudioManager, PlayLooped ),
    METHOD( AudioManager, UpdatePos ),
    METHOD( AudioManager, StopLooped ),
    METHOD( AudioManager, PlayMusic ),
    METHOD( AudioManager, MusicVolumeOverride ),
    {0,0}
};

AudioManager::AudioManager()
    : m_mixer( Claw::Mixer::Get() )
    , m_muteMusic( false )
    , m_masterVolume( new Claw::EffectVolume( m_mixer->GetFormat() ) )
    , m_sfxVolume( new Claw::EffectVolume( m_mixer->GetFormat() ) )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;

    Claw::Registry::Get()->RegisterCallback( "/monstaz/settings/sound", MasterVolumeChanged, this );
    Claw::Registry::Get()->RegisterCallback( "/monstaz/settings/sfx", SfxVolumeChanged, this );
}

AudioManager::~AudioManager()
{
    CLAW_ASSERT( s_instance );
    s_instance = this;
}

void AudioManager::Load()
{
    for( int i=0; i<SFX_COUNT; i++ )
    {
        CLAW_ASSERT( AudioAssets[i].id == (AudioSfx)i );
        Claw::NarrowString asset( "audio/" );
        asset += AudioAssets[i].asset;
        if( asset.substr( asset.size() - 4 ) == "rtac" || asset.substr( asset.size() - 9 ) == "ogg@cache" )
        {
            m_sfx[i].Reset( Claw::AssetDict::Get<Claw::AudioSource>( asset ) );
        }
        else
        {
            m_sfx[i].Reset( new Claw::AudioRTAC( Claw::AssetDict::Get<Claw::AudioSource>( asset ) ) );
        }
    }
}

void AudioManager::Init( Claw::Lua* lua )
{
    Claw::Lunar<AudioChannelWrapper>::Register( *lua );

    Claw::Lunar<AudioManager>::Register( *lua );
    Claw::Lunar<AudioManager>::push( *lua, this );
    lua->RegisterGlobal( "AudioManager" );

    lua->CreateEnumTable();
    lua->AddEnum( SFX_DEATH1 );
    lua->AddEnum( SFX_DEATH2 );
    lua->AddEnum( SFX_DEATH3 );
    lua->AddEnum( SFX_PISTOL );
    lua->AddEnum( SFX_PISTOL_END );
    lua->AddEnum( SFX_MINIGUN );
    lua->AddEnum( SFX_RELOAD1 );
    lua->AddEnum( SFX_RELOAD2 );
    lua->AddEnum( SFX_RELOAD3 );
    lua->AddEnum( SFX_RELOAD4 );
    lua->AddEnum( SFX_SHOTGUN1 );
    lua->AddEnum( SFX_SHOTGUN2 );
    lua->AddEnum( SFX_SPINUP );
    lua->AddEnum( SFX_SPINDOWN );
    lua->AddEnum( SFX_ELECTRICITY );
    lua->AddEnum( SFX_ELECTRICITY_START );
    lua->AddEnum( SFX_RAILGUN );
    lua->AddEnum( SFX_FLAMER );
    lua->AddEnum( SFX_PLASMA );
    lua->AddEnum( SFX_PLASMA_END );
    lua->AddEnum( SFX_PLASMA_HIT );
    lua->AddEnum( SFX_PLASMA_SECTOID );
    lua->AddEnum( SFX_RIPPER );
    lua->AddEnum( SFX_ROCKET );
    lua->AddEnum( SFX_EXPLOSION1 );
    lua->AddEnum( SFX_EXPLOSION2 );
    lua->AddEnum( SFX_EXPLOSION3 );
    lua->AddEnum( SFX_SHOCKERBURN );
    lua->AddEnum( SFX_SHOCKERSTART );
    lua->AddEnum( SFX_SHOCKERLOOP );
    lua->AddEnum( SFX_SHOCKEREND );
    lua->AddEnum( SFX_RICOCHET1 );
    lua->AddEnum( SFX_RICOCHET2 );
    lua->AddEnum( SFX_RICOCHET3 );
    lua->AddEnum( SFX_RICOCHET4 );
    lua->AddEnum( SFX_RICOCHET5 );
    lua->AddEnum( SFX_RICOCHET6 );
    lua->AddEnum( SFX_RICOCHET7 );
    lua->AddEnum( SFX_PLAYER_HIT1 );
    lua->AddEnum( SFX_PLAYER_HIT2 );
    lua->AddEnum( SFX_PLAYER_HIT3 );
    lua->AddEnum( SFX_PLAYER_HIT4 );
    lua->AddEnum( SFX_PLAYER_HIT5 );
    lua->AddEnum( SFX_PLAYER_HIT6 );
    lua->AddEnum( SFX_PLAYER_HIT7 );
    lua->AddEnum( SFX_PLAYER_DEATH );
    lua->AddEnum( SFX_OCTOPUS_HIT1 );
    lua->AddEnum( SFX_OCTOPUS_HIT2 );
    lua->AddEnum( SFX_OCTOPUS_HIT3 );
    lua->AddEnum( SFX_OCTOPUS_ATTACK1 );
    lua->AddEnum( SFX_OCTOPUS_ATTACK2 );
    lua->AddEnum( SFX_OCTOPUS_ATTACK3 );
    lua->AddEnum( SFX_OCTOPUS_ATTACK4 );
    lua->AddEnum( SFX_OCTOPUS_ATTACK5 );
    lua->AddEnum( SFX_OCTOPUS_ATTACK6 );
    lua->AddEnum( SFX_ORB );
    lua->AddEnum( SFX_HEALTH );
    lua->AddEnum( SFX_WEAPON_PICKUP );
    lua->AddEnum( SFX_CASH );
    lua->AddEnum( SFX_QUAD );
    lua->AddEnum( SFX_LEVELUP );
    lua->AddEnum( SFX_PICKUP_APPEAR );
    lua->AddEnum( SFX_PICKUP_DISAPPEAR );
    lua->AddEnum( SFX_NO_AMMO );
    lua->AddEnum( SFX_SIREN );
    lua->AddEnum( SFX_NUKE );
    lua->AddEnum( SFX_BITE1 );
    lua->AddEnum( SFX_BITE2 );
    lua->AddEnum( SFX_FISH_DEATH1 );
    lua->AddEnum( SFX_FISH_DEATH2 );
    lua->AddEnum( SFX_FISH_DEATH3 );
    lua->AddEnum( SFX_FISH_EXPLOSION );
    lua->AddEnum( SFX_ROLLER_DEATH );
    lua->AddEnum( SFX_SPIT );
    lua->AddEnum( SFX_SPIT_PREP );
    lua->AddEnum( SFX_FISH_HIT1 );
    lua->AddEnum( SFX_FISH_HIT2 );
    lua->AddEnum( SFX_SHOPKEEPER );
    lua->AddEnum( SFX_MONEYDROP );
    lua->AddEnum( SFX_TELEPORT );
    lua->AddEnum( SFX_MINE_ARM );
    lua->AddEnum( SFX_MINE_ARM2 );
    lua->AddEnum( SFX_MINE_DETECT );
    lua->AddEnum( SFX_SHIELD1 );
    lua->AddEnum( SFX_SHIELD2 );
    lua->AddEnum( SFX_SHIELD3 );
    lua->AddEnum( SFX_SHIELD4 );
    lua->AddEnum( SFX_SHIELD_HIT1 );
    lua->AddEnum( SFX_SHIELD_HIT2 );
    lua->AddEnum( SFX_FLOATER_DIE1 );
    lua->AddEnum( SFX_FLOATER_DIE2 );
    lua->AddEnum( SFX_FLOATER_ELECTRIC_DIE );
    lua->AddEnum( SFX_HOUND_DIE1 );
    lua->AddEnum( SFX_HOUND_DIE2 );
    lua->AddEnum( SFX_HOUND_SHOOTING_DIE );
    lua->AddEnum( SFX_MULTIKILL1 );
    lua->AddEnum( SFX_MULTIKILL2 );
    lua->AddEnum( SFX_MULTIKILL3 );
    lua->AddEnum( SFX_MULTIKILL4 );
    lua->AddEnum( SFX_MULTIKILL5 );
    lua->AddEnum( SFX_MULTIKILL6 );
    lua->AddEnum( SFX_MULTIKILL7 );
    lua->AddEnum( SFX_MULTIKILL8 );
    lua->AddEnum( SFX_TRIPALIZER_START );
    lua->AddEnum( SFX_TRIPALIZER_LOOP );
    lua->AddEnum( SFX_TRIPALIZER_END );
    lua->AddEnum( SFX_CRAB_LASER );
    lua->AddEnum( SFX_SOWER_CHARGE );
    lua->AddEnum( SFX_SOWER_DEATH );
    lua->AddEnum( SFX_SOWER_LICK );
    lua->AddEnum( SFX_SOWER_SPIT );
    lua->AddEnum( SFX_SOWER_TOSS_EGGS );
    lua->AddEnum( SFX_JET );
    lua->AddEnum( SFX_SPIKES_SHOT );
    lua->AddEnum( SFX_SPIKES_1 );
    lua->AddEnum( SFX_SPIKES_2 );
    lua->AddEnum( SFX_SPIKES_3 );
    lua->AddEnum( SFX_MAGNUM );
    lua->AddEnum( SFX_AMBIENT );
    lua->AddEnum( SFX_GIANT_BLOT_DMG1 );
    lua->AddEnum( SFX_GIANT_BLOT_DMG2 );
    lua->AddEnum( SFX_GIANT_BLOT_DMG3 );
    lua->AddEnum( SFX_WHALE_CHARGE );
    lua->AddEnum( SFX_SECTOID_DEATH1 );
    lua->AddEnum( SFX_SECTOID_DEATH2 );
    lua->AddEnum( SFX_SECTOID2_DEATH1 );
    lua->AddEnum( SFX_SECTOID2_DEATH2 );
    lua->AddEnum( SFX_SECTOID2_DEATH3 );
    lua->AddEnum( SFX_SECTOID2_DEATH4 );
    lua->AddEnum( SFX_LINEGUN );
    lua->AddEnum( SFX_CRAB_DIGIN );
    lua->AddEnum( SFX_CRAB_DIGOUT );
    lua->AddEnum( SFX_WEAPON_SWIPE );
    lua->AddEnum( SFX_CAT_ANGRY );
    lua->AddEnum( SFX_CAT_VOMIT );
    lua->AddEnum( SFX_VORTEX );
    lua->AddEnum( SFX_CHAINSAW_START );
    lua->AddEnum( SFX_CHAINSAW_LOOP );
    lua->AddEnum( SFX_CHAINSAW_STOP );
    lua->AddEnum( SFX_TELEPORT1 );
    lua->AddEnum( SFX_TELEPORT2 );
    lua->AddEnum( SFX_TELEPORT3 );
    lua->AddEnum( SFX_MECH_START );
    lua->AddEnum( SFX_MECH_LOOP );
    lua->AddEnum( SFX_MECH_SERVO );

    lua->AddEnum( SFX_MENU_BACK );
    lua->AddEnum( SFX_MENU_ROTATE );
    lua->AddEnum( SFX_MENU_SELECT );
    lua->AddEnum( SFX_MENU_SELECT_SMALL );
    lua->AddEnum( SFX_MENU_POPUP );
    lua->AddEnum( SFX_MENU_POPUP_CLOSE );
    lua->AddEnum( SFX_MENU_POPUP_LONG );
    lua->AddEnum( SFX_MENU_POPUP_LONG_CLOSE );
    lua->AddEnum( SFX_MENU_ZOOM );
    lua->AddEnum( SFX_MENU_BUY );
    lua->AddEnum( SFX_MENU_GWIAZDKI );
    lua->AddEnum( SFX_MENU_LEVELUP );
    lua->AddEnum( SFX_MENU_DING );
    lua->AddEnum( SFX_MENU_MISSION_BULLET );
    lua->AddEnum( SFX_MENU_LEVEL_FINISHED );
    lua->AddEnum( SFX_MENU_LEVEL_UP );
    lua->AddEnum( SFX_MENU_RANK_UP );
    lua->AddEnum( SFX_MENU_RUSSIAN_WIN );
    lua->AddEnum( SFX_MENU_RUSSIAN_LOSE );

    lua->AddEnum( SFX_VO_ANOTHER_VICTORY );
    lua->AddEnum( SFX_VO_DONT_GIVE_UP );
    lua->AddEnum( SFX_VO_FATAL_ENCOUNTER );
    lua->AddEnum( SFX_VO_LEVELUP );
    lua->AddEnum( SFX_VO_MAXED_OUT );
    lua->AddEnum( SFX_VO_TAKE_YOUR_REVENGE );
    lua->AddEnum( SFX_VO_TOTAL_DOMINATION );
    lua->AddEnum( SFX_VO_YOU_FAILED );

    lua->RegisterEnumTable( "Sfx" );
}

Claw::AudioChannelWPtr AudioManager::Play( AudioSfx sfx )
{
    Claw::AudioChannelWPtr acw = m_mixer->Register( new Claw::AudioPosition( m_sfx[sfx] ), true );
    if( Claw::AudioChannelPtr ac = acw.Lock() )
    {
        if( AudioAssets[sfx].quiet )
        {
            ac->AddEffect( new Claw::EffectVolumeShift( ac->GetFormat(), 1 ) );
        }
        ac->AddEffect( m_sfxVolume );
        ac->SetPause( false );
    }
    return acw;
}

void AudioManager::Stop( Claw::AudioChannelWPtr channel )
{
    if( Claw::AudioChannelPtr channelPtr = channel.Lock() )
    {
        m_mixer->Remove( channelPtr.GetPtr() );
    }
}

Claw::AudioChannelWPtr AudioManager::PlayLooped( AudioSfx sfx )
{
    Claw::AudioChannelWPtr acw = m_mixer->Register( new Claw::AudioPosition( m_sfx[sfx] ), true );
    if( Claw::AudioChannelPtr ac = acw.Lock() )
    {
        if( AudioAssets[sfx].quiet )
        {
            ac->AddEffect( new Claw::EffectVolumeShift( ac->GetFormat(), 1 ) );
        }
        ac->AddEffect( m_sfxVolume );
        ac->SetLoop( true );
        m_loop.push_back( acw );
        ac->SetPause( false );
    }
    return acw;
}

Claw::AudioChannelWPtr AudioManager::Play3D( AudioSfx sfx, const Vectorf& pos, SoundHandle3D* handle )
{
    GameManager* gm = GameManager::GetInstance();
    if( !gm || !gm->GetPlayer() )
    {
        Play( sfx );
        return Claw::AudioChannelWPtr();
    }

    Claw::AudioSource* as = new Claw::AudioPosition( m_sfx[sfx] );
    if( as->GetFormat().m_channels == 1 )
    {
        as = new Claw::AudioStereoExpand( as );
    }
    Claw::AudioChannelWPtr acw = m_mixer->Register( as, true );
    if( Claw::AudioChannelPtr ac = acw.Lock() )
    {
        Claw::EffectVolume* volume = new Claw::EffectVolume( ac->GetFormat() );
        Claw::EffectPan* pan = new Claw::EffectPan( ac->GetFormat() );

        UpdatePos3D( pos, *volume, *pan );

        ac->AddEffect( volume );
        ac->AddEffect( pan );
        ac->AddEffect( m_sfxVolume );
        ac->SetPause( false );

        if( handle )
        {
            handle->channel = acw;
            handle->volume = volume;
            handle->pan = pan;
        }
    }
    return acw;
}

Claw::AudioChannelWPtr AudioManager::PlayLooped3D( AudioSfx sfx, const Vectorf& pos, SoundHandle3D* handle )
{
    Claw::AudioChannelWPtr acw = Play3D( sfx, pos, handle );
    if( Claw::AudioChannelPtr channel = acw.Lock() )
    {
        channel->SetLoop( true );
        m_loop.push_back( acw );
    }
    return acw;
}

void AudioManager::UpdatePos3D( const Vectorf& pos, SoundHandle3D& handle )
{
    Claw::EffectVolumePtr volume = handle.volume.Lock();
    Claw::EffectPanPtr pan = handle.pan.Lock();
    if( volume && pan )
    {
        UpdatePos3D( pos, *volume, *pan );
    }
}

void AudioManager::UpdatePos3D( const Vectorf& pos, Claw::EffectVolume& volume, Claw::EffectPan& pan )
{
    GameManager* gm = GameManager::GetInstance();
    if( !gm ) return;
    Entity* p = gm->GetPlayer();
    if( !p ) return;

    Vectorf offset( p->GetPos() - pos );
    float l = DotProduct( offset, offset );
    l /= 20000.f;

    pan.Set( Claw::MinMax( ( 240 - offset.x ) / 480.f, 0.f, 1.f ) );
    volume.Set( 1.f / ( 1 + l ) );
}

void AudioManager::Stop3D( SoundHandle3D& handle )
{
    Stop( handle.channel );
}

void AudioManager::StopLooped( Claw::AudioChannelWPtr acw )
{
    Stop( acw );

    std::vector<Claw::AudioChannelWPtr>::iterator it = m_loop.begin();
    while( it != m_loop.end() )
    {
        if( !it->Lock() )
        {
            it = m_loop.erase( it );
        }
        else
        {
            ++it;
        }
    }
}

void AudioManager::PlayMusic( const char* fn )
{
    StopMusic();

    char tmp[128];
    sprintf( tmp, "music/%s", fn );

    m_music = m_mixer->Register( Claw::AssetDict::Get<Claw::AudioSource>( tmp ), true );
    if( Claw::AudioChannelPtr music = m_music.Lock() )
    {
        music->AddEffect( m_masterVolume );
        music->SetLoop( true );
        music->SetPause( false );
    }
}

void AudioManager::StopMusic()
{
    Stop( m_music );
}

void AudioManager::MusicVolumeOverride( unsigned int shift )
{
    if( Claw::AudioChannelPtr music = m_music.Lock() )
    {
        music->AddEffect( new Claw::EffectVolumeShift( music->GetFormat(), shift ) );
    }
}

void AudioManager::MuteMusic( bool mute )
{
    if ( mute != m_muteMusic )
    {
        if ( mute )
        {
            m_masterVolume->Set( 0 );
        }
        else
        {
            int volume = Claw::Registry::Get()->CheckInt( "/monstaz/settings/sound" );
            m_masterVolume->Set( volume / 10.f );
        }

        m_muteMusic = mute;
    }
}

void AudioManager::PauseLooped( bool pause )
{
    for( std::vector<Claw::AudioChannelWPtr>::iterator it = m_loop.begin(); it != m_loop.end(); ++it )
    {
        if( Claw::AudioChannelPtr ac = it->Lock() )
        {
            ac->SetPause( pause );
        }
    }
}

void AudioManager::KillLooped()
{
    for( std::vector<Claw::AudioChannelWPtr>::iterator it = m_loop.begin(); it != m_loop.end(); ++it )
    {
        Stop( *it );
    }

    m_loop.clear();
}

void AudioManager::Pause( bool pause )
{
    m_mixer->Pause( pause );
}

int AudioManager::l_Play( lua_State* L )
{
    Claw::Lua lua( L );
    if( lua.IsNumber( 2 ) && lua.IsNumber( 3 ) )
    {
        Play3D( lua.CheckEnum<AudioSfx>( 1 ), Vectorf( lua.CheckNumber( 2 ), lua.CheckNumber( 3 ) ) );
    }
    else
    {
        Play( lua.CheckEnum<AudioSfx>( 1 ) );
    }
    return 0;
}

int AudioManager::l_PlayLooped( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::AudioChannelWPtr acw;
    SoundHandle3D handle;
    if( lua.IsNumber( 2 ) && lua.IsNumber( 3 ) )
    {
        acw = PlayLooped3D( lua.CheckEnum<AudioSfx>( 1 ), Vectorf( lua.CheckNumber( 2 ), lua.CheckNumber( 3 ) ), &handle );
    }
    else
    {
        acw = PlayLooped( lua.CheckEnum<AudioSfx>( 1 ) );
    }
    Claw::Lunar<AudioChannelWrapper>::push( L, new AudioChannelWrapper( acw, handle ), true );
    return 1;
}

int AudioManager::l_UpdatePos( lua_State* L )
{
    Claw::Lua lua( L );
    CLAW_ASSERT( lua_isuserdata( L, 1 ) );
    AudioChannelWrapper* w = Claw::Lunar<AudioChannelWrapper>::check( L, 1 );
    int x = lua.CheckNumber( 2 );
    int y = lua.CheckNumber( 3 );
    UpdatePos3D( Vectorf( x, y ), w->hnd );
    return 0;
}

int AudioManager::l_StopLooped( lua_State* L )
{
    Claw::Lua lua( L );
    CLAW_ASSERT( lua_isuserdata( L, 1 ) );
    AudioChannelWrapper* w = Claw::Lunar<AudioChannelWrapper>::check( L, 1 );
    StopLooped( w->ptr );
    return 0;
}

int AudioManager::l_PlayMusic( lua_State* L )
{
    Claw::Lua lua( L );
    PlayMusic( lua.CheckCString( 1 ) );
    return 0;
}

int AudioManager::l_MusicVolumeOverride( lua_State* L )
{
    MusicVolumeOverride( luaL_checkint( L, 1 ) );
    return 0;
}

void AudioManager::MasterVolumeChanged( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node )
{
    if ( ((AudioManager*)ptr)->m_muteMusic )
    {
        return;
    }

    int volume = Claw::UnsafeAnyCast<int>( node->GetData() );
    ((AudioManager*)ptr)->m_masterVolume->Set( volume / 10.f );
}

void AudioManager::SfxVolumeChanged( void* ptr, const Claw::NarrowString& path, Claw::RegistryKey* node )
{
    int volume = Claw::UnsafeAnyCast<int>( node->GetData() );
    ((AudioManager*)ptr)->m_sfxVolume->Set( volume / 10.f );
}

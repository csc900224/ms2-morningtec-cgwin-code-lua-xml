#ifndef __MONSTAZ_ANIMATIONSET_HPP__
#define __MONSTAZ_ANIMATIONSET_HPP__

#include "claw/base/SmartPtr.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/GfxAsset.hpp"

class AnimationSet : public Claw::RefCounter
{
public:
    AnimationSet( int directions, int frames, const char* path );
    ~AnimationSet();

    GfxAsset* GetAsset( const Vectorf& dir, int frame, int& lastDir ) const;

    static int TranslateFor8( const Vectorf& dir );
    static int TranslateFor16( const Vectorf& dir );
    static int TranslateFor32( const Vectorf& dir );

    static const char* GetFrameAngle8( int idx );
    static const char* GetFrameAngle16( int idx );
    static const char* GetFrameAngle32( int idx );
    static const Vectorf& GetShotPos16( int idx );
    static Vectorf GetShotPos32( int idx, int weapon );

    int GetFrames() const { return m_frames; }

private:
    int m_directions;
    int m_frames;
    GfxAssetPtr** m_gfx;
};

typedef Claw::SmartPtr<AnimationSet> AnimationSetPtr;

#endif

#ifndef __MONSTAZ_HEATBLUR_HPP__
#define __MONSTAZ_HEATBLUR_HPP__

#include "claw/graphics/Surface.hpp"
#include "claw/graphics/OpenGLShader.hpp"

class HeatBlur : public Claw::RefCounter
{
public:
    HeatBlur();
    void Render( Claw::Surface* src, Claw::Surface* dst, float x, float y, const Claw::Rect& clipRect );

private:
    Claw::OpenGLShader m_shader;
};

typedef Claw::SmartPtr<HeatBlur> HeatBlurPtr;

#endif

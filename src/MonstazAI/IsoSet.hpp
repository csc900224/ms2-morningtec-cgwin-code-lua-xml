#ifndef __MONSTAZ_ISOSET_HPP__
#define __MONSTAZ_ISOSET_HPP__

#include "claw/base/SmartPtr.hpp"
#include "claw/base/WeakRefCounter.hpp"
#include "claw/graphics/Rect.hpp"

#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/Renderable.hpp"
#include "MonstazAI/Map.hpp"

class IsoSet;

class IsoSetElement : public Renderable
{
public:
    IsoSetElement();
    IsoSetElement( Claw::Surface* asset, const Claw::Rect& rect, int height, const Vectorf& pos, float scale, IsoSet* parent );

    void Render( Claw::Surface* target, const Vectorf& offset, float scale ) const;

    const Claw::Rect& GetRenderLocalBV() const;
    const Claw::Rect& GetRenderWorldBV() const;

private:
    Claw::SurfacePtr m_asset;
    Claw::Rect m_rect;
    Claw::Rect m_worldBV;
    int m_height;
    IsoSet* m_parent;
};

class IsoSet : public Claw::RefCounter
{
public:
    IsoSet( const Vectorf& pos, float scale, int offset );
    IsoSet( const Claw::NarrowString& fn, const Vectorf& pos, float scale, int offset );
    ~IsoSet();

    virtual void Render( Claw::Surface* target, const Vectorf& offset, float scale );
    virtual void RenderShadow( Claw::Surface* target, const Vectorf& pos );
    virtual void Update( float dt ) {}

    int GetWidth() const { return m_asset->GetWidth(); }
    int GetHeight() const { return m_asset->GetHeight(); }

    Map::StaticObjectIsoSet* m_obj;

private:
    Claw::SurfacePtr m_asset;

    Claw::SurfacePtr m_shadow;
    Vectorf m_shadowPos;
    Claw::Rect m_shadowRect;
    const Vectorf m_pos;
    IsoSetElement* m_rects;
    int m_rectNum;
};

typedef Claw::SmartPtr<IsoSet> IsoSetPtr;

class IsoSetAnim : public IsoSet
{
public:
    IsoSetAnim( const Claw::NarrowString& fn, const Vectorf& pos, float scale, int offset );
    ~IsoSetAnim();

    void Render( Claw::Surface* target, const Vectorf& offset, float scale );
    void RenderShadow( Claw::Surface* target, const Vectorf& pos );
    void Update( float dt );

private:
    int m_count;
    IsoSetPtr* m_frame;
    float* m_time;

    int m_currentFrame;
    float m_timeRemaining;
};

typedef Claw::SmartPtr<IsoSetAnim> IsoSetAnimPtr;

#endif

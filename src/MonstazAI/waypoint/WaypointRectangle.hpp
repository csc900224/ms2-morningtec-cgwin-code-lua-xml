#ifndef __MONSTAZ_WAYPOINTRECTANGLE_HPP__
#define __MONSTAZ_WAYPOINTRECTANGLE_HPP__

#include "MonstazAI/waypoint/Waypoint.hpp"
#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/ObjectRectangle.hpp"

class WaypointRectangle : public Waypoint, public ObjectRectangle
{
public:
    LUA_DEFINITION( WaypointRectangle );
    WaypointRectangle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    WaypointRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular );
};

#endif

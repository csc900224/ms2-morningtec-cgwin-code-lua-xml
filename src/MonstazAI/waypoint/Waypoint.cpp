#include "MonstazAI/waypoint/Waypoint.hpp"
#include "MonstazAI/waypoint/WaypointCircle.hpp"
#include "MonstazAI/waypoint/WaypointRectangle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

LUA_DECLARATION( Waypoint )
{
    METHOD( Waypoint, GetPos ),
    METHOD( Waypoint, GetType ),
    {0,0}
};

void Waypoint::Init( Claw::Lua* lua )
{
    Claw::Lunar<Waypoint>::Register( *lua );

    WaypointCircle::Init( lua );
    WaypointRectangle::Init( lua );
}

Waypoint::Waypoint( const Claw::NarrowString& id, float x, float y, Type type )
    : m_pos( x, y )
    , m_type( type )
    , m_id( id )
{
    // Create trigger bounding area
    if( m_type == Circle )
        m_bv = new Scene::Circle( m_pos, 0 );
    else
        m_bv = new Scene::OBB2( m_pos );
}

Waypoint::~Waypoint()
{
    delete m_bv;
}

bool Waypoint::Check( const Scene::BoundingArea* ba )
{
    return m_bv->Intersect( ba );
}

int Waypoint::l_GetPos( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_pos.x );
    lua.PushNumber( m_pos.y );
    return 2;
}

int Waypoint::l_GetType( lua_State* L )
{
    Claw::Lua lua( L );
    lua.PushNumber( m_type );
    return 1;
}

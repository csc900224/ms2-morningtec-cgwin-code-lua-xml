#include "claw/math/Math.hpp"

#include "MonstazAI/waypoint/WaypointCircle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"

LUA_DECLARATION( WaypointCircle )
{
    METHOD( WaypointCircle, GetPos ),
    METHOD( WaypointCircle, GetType ),
    METHOD( WaypointCircle, GetRadius ),
    {0,0}
};

void WaypointCircle::Init( Claw::Lua* lua )
{
    Claw::Lunar<WaypointCircle>::Register( *lua );
}

WaypointCircle::WaypointCircle( const Claw::NarrowString& id, float x, float y, float r )
    : Waypoint( id, x, y, Waypoint::Circle )
    , ObjectCircle( r )
{
    static_cast<Scene::Circle*>(m_bv)->SetCenterRadius( Vectorf( x, y ), r );
}

#ifndef __MONSTAZ_WAYPOINTCIRCLE_HPP__
#define __MONSTAZ_WAYPOINTCIRCLE_HPP__

#include "MonstazAI/ObjectCircle.hpp"
#include "MonstazAI/waypoint/Waypoint.hpp"

class WaypointCircle : public Waypoint, public ObjectCircle
{
public:
    LUA_DEFINITION( WaypointCircle );
    WaypointCircle( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    WaypointCircle( const Claw::NarrowString& id, float x, float y, float r );
};

#endif

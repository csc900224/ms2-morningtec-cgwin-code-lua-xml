#include "claw/math/Math.hpp"

#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/waypoint/WaypointRectangle.hpp"
#include "MonstazAI/scene/collisions/OBB2.hpp"

LUA_DECLARATION( WaypointRectangle )
{
    METHOD( WaypointRectangle, GetPos ),
    METHOD( WaypointRectangle, GetType ),
    METHOD( WaypointRectangle, GetEdge ),
    METHOD( WaypointRectangle, GetPerpendicular ),
    {0,0}
};

void WaypointRectangle::Init( Claw::Lua* lua )
{
    Claw::Lunar<WaypointRectangle>::Register( *lua );
}

WaypointRectangle::WaypointRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular )
    : Waypoint( id, x, y, Waypoint::Rectangle )
    , ObjectRectangle( edge, perpendicular )
{
    Vectorf center( x, y );
    center += m_edge * 0.5f;
    center += m_perp * 0.5f;
    Vectorf extents( m_edgeLen, m_perpLen );
    extents *= 0.5f;
    static_cast<Scene::OBB2*>(m_bv)->SetCenterExtentsAxes( center, extents, m_edge );
}

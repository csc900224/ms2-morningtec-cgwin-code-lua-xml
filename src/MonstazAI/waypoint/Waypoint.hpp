#ifndef __MONSTAZ_WAYPOINT_HPP__
#define __MONSTAZ_WAYPOINT_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/math/Vector.hpp"

namespace Scene
{
    class BoundingArea;
};

class Waypoint
{
public:
    enum Type
    {
        Circle,
        Rectangle,
        Custom
    };

    LUA_DEFINITION( Waypoint );
    Waypoint( lua_State* L ) { CLAW_ASSERT( false ); }
    static void Init( Claw::Lua* lua );

    virtual ~Waypoint();

    const Vectorf& GetPos() const { return m_pos; }
    Type GetType() const { return m_type; }
    const Claw::NarrowString& GetId() const { return m_id; }

    bool Check( const Scene::BoundingArea* ba );

    int l_GetPos( lua_State* L );
    int l_GetType( lua_State* L );

protected:
    Waypoint() { CLAW_ASSERT( false ); }
    Waypoint( const Claw::NarrowString& id, float x, float y, Type type );

    Vectorf m_pos;
    Type m_type;
    Claw::NarrowString m_id;

    Scene::BoundingArea* m_bv;
};

#endif

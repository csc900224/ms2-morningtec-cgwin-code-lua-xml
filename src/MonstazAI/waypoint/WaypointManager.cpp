#include "MonstazAI/waypoint/WaypointManager.hpp"
#include "MonstazAI/GameManager.hpp"


LUA_DECLARATION( WaypointManager )
{
    METHOD( WaypointManager, Add ),
    {0,0}
};

WaypointManager::WaypointManager( Claw::Lua* lua )
    : m_lua( lua )
{
    Waypoint::Init( lua );

    Claw::Lunar<WaypointManager>::Register( *lua );
    Claw::Lunar<WaypointManager>::push( *lua, this );
    lua->RegisterGlobal( "WaypointManager" );

    lua->CreateEnumTable();
    lua->AddEnum( Waypoint::Circle );
    lua->AddEnum( Waypoint::Rectangle );
    lua->AddEnum( Waypoint::Custom );
    lua->RegisterEnumTable( "WaypointType" );
}

WaypointManager::~WaypointManager()
{
    for( std::vector<Waypoint*>::const_iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        delete *it;
    }
}

WaypointCircle* WaypointManager::AddWaypointCircle( const Claw::NarrowString& id, float x, float y, float r )
{
    WaypointCircle* e = new WaypointCircle( id, x, y, r );
    m_ents.push_back( e );
    return e;
}

WaypointRectangle* WaypointManager::AddWaypointRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular )
{
    WaypointRectangle* e = new WaypointRectangle( id, x, y, edge, perpendicular );
    m_ents.push_back( e );
    return e;
}

Waypoint* WaypointManager::GetWaypoint( const char* str )
{
    for( std::vector<Waypoint*>::iterator it = m_ents.begin(); it != m_ents.end(); ++it )
    {
        if( strcmp( (*it)->GetId().c_str(), str ) == 0 )
        {
            return *it;
        }
    }

    return NULL;
}

int WaypointManager::l_Add( lua_State* L )
{
    Claw::Lua lua( L );

    Waypoint::Type type = lua.CheckEnum<Waypoint::Type>( 1 );

    if( type == Waypoint::Circle )
    {
        WaypointCircle* e = AddWaypointCircle( lua.CheckString( 2 ), lua.CheckNumber( 3 ), lua.CheckNumber( 4 ), lua.CheckNumber( 5 ) );
        Claw::Lunar<WaypointCircle>::push( L, e );
    }
    else if( type == Waypoint::Rectangle )
    {
        WaypointRectangle* e = AddWaypointRectangle( lua.CheckString( 2 ), lua.CheckNumber( 3 ), lua.CheckNumber( 4 ),
            Vectorf( lua.CheckNumber( 5 ), lua.CheckNumber( 6 ) ),
            lua.CheckNumber( 7 ) );
        Claw::Lunar<WaypointRectangle>::push( L, e );
    }
    else
    {
        CLAW_ASSERT( false );
    }

    return 1;
}

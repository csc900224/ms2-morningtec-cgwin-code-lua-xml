#ifndef __MONSTAZ_WAYPOINTMANAGER_HPP__
#define __MONSTAZ_WAYPOINTMANAGER_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/Surface.hpp"

#include "MonstazAI/entity/Entity.hpp"
#include "MonstazAI/waypoint/Waypoint.hpp"
#include "MonstazAI/waypoint/WaypointCircle.hpp"
#include "MonstazAI/waypoint/WaypointRectangle.hpp"
#include "MonstazAI/scene/collisions/Circle.hpp"

#include <vector>


class WaypointManager : public Claw::RefCounter
{
public:
    LUA_DEFINITION( WaypointManager );
    WaypointManager( lua_State* L ) { CLAW_ASSERT( false ); }

    WaypointManager( Claw::Lua* lua );
    ~WaypointManager();

    WaypointCircle* AddWaypointCircle( const Claw::NarrowString& id, float x, float y, float r );
    WaypointRectangle* AddWaypointRectangle( const Claw::NarrowString& id, float x, float y, const Vectorf& edge, float perpendicular );

    Waypoint* GetWaypoint( const char* id );

    const std::vector<Waypoint*>& GetWaypoints() const { return m_ents; }

    int l_Add( lua_State* L );

private:
    std::vector<Waypoint*> m_ents;
    Claw::LuaPtr m_lua;
};

typedef Claw::SmartPtr<WaypointManager> WaypointManagerPtr;

#endif

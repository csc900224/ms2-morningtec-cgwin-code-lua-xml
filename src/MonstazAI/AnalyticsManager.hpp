#ifndef __MONSTAZ_ANALYTICSMANAGER_HPP__
#define __MONSTAZ_ANALYTICSMANAGER_HPP__

#include "MonstazAI/GameEvent.hpp"

#include "claw/base/SmartPtr.hpp"
#include "jungle/patterns/Singleton.hpp"

#include <vector>
#include <map>
#include <set>

namespace ClawExt
{
    class FlurryAnalytics;
    class GameAnalytics;
}

class AnalyticsManager 
    : public Jungle::Patterns::Singleton< AnalyticsManager, Jungle::Patterns::ExplicitCreation >
    , public GameEventHandler
{
public:
    class EventHandler : public GameEventHandler, public Claw::RefCounter
    {
    public:
        virtual ~EventHandler() {}
        virtual void Initialize( AnalyticsManager* manager ) = 0;

    protected:
        typedef std::vector<Claw::NarrowString> EventHierarchy;
        Claw::NarrowString GenerateEvent( const EventHierarchy& eh );

        void LogBusinessEvent( const Claw::NarrowString& ev, const char* currency, int amount, const char* area = NULL );
        void LogQualityEvent( const Claw::NarrowString& ev, const char* message = NULL, const char* area = NULL );
        void LogDesignEvent( const Claw::NarrowString& ev, float value = 1.0f, const char* area = NULL );
    };
    typedef Claw::SmartPtr<EventHandler> EventHandlerPtr;

    ~AnalyticsManager();

    void Initialize();
    void SetLua( Claw::Lua* lua ) { m_lua = lua; }

    void OnFocusChange( bool focus );

    void RegisterHandler( GamEventId eventId, EventHandler* handler );
    bool HandleGameEvent( const GameEvent& ev );

    Claw::NarrowString GetLevelUID() const;

    ClawExt::FlurryAnalytics* GetFlurryAnalytics() const { return m_flurry; }
    ClawExt::GameAnalytics* GetGameAnalytics() const { return m_game; }

    Claw::NarrowString GenerateBuildName( bool realVersion = false ) const;

private:
    friend class Jungle::Patterns::ExplicitCreation< AnalyticsManager >;
    typedef std::set<EventHandlerPtr> EventHandlersSet;
    typedef std::map<GamEventId, EventHandlersSet> EventHandlers;

    AnalyticsManager();
    void CreateHandlers();
    void AddHandler( EventHandler* handler );

    void StopSession();
    void StartSession();

private:
    EventHandlers m_eventHandlers;
    EventHandlersSet m_allHandlers;

    bool m_initialized;
    bool m_sessionRunning;
    Claw::Lua* m_lua;

    ClawExt::FlurryAnalytics* m_flurry;
    ClawExt::GameAnalytics* m_game;
};

#endif

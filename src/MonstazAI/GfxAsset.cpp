#include <stdlib.h>

#include "claw/graphics/TriangleEngine.hpp"
#include "claw/vfs/Vfs.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/GfxAsset.hpp"


std::map<Claw::NarrowString, Vectorf > GfxAsset::s_pivotDB;

GfxAsset::GfxAsset( const Claw::NarrowString& path )
{
    Claw::NarrowString def;
    Claw::NarrowString flags;

    size_t at = path.rfind( '@' );
    if( at != Claw::NarrowString::npos )
    {
        def = path.substr( 0, at );
        flags = path.substr( at );
    }
    else
    {
        def = path;
    }

    m_surface.Reset( Claw::AssetDict::Get<Claw::Surface>( def.substr( 0, def.length() - 6 ) + flags ) );

    std::map<Claw::NarrowString, Vectorf >::iterator it = s_pivotDB.find( def );
    if( it == s_pivotDB.end() )
    {
        CLAW_MSG( def + " is not in the pivot DB!" );

        Claw::NarrowString tmp;
        Claw::FilePtr f( Claw::OpenFile( ( def ).c_str() ) );
        CLAW_ASSERT( f );

        f->ReadLine( tmp );
        m_pivot.x = atof( tmp.c_str() );
        tmp.clear();
        f->ReadLine( tmp );
        m_pivot.y = atof( tmp.c_str() );
    }
    else
    {
        m_pivot = it->second;
    }

    if( ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale() == 3 )
    {
        m_pivot *= 0.75f;
    }
}

GfxAsset::~GfxAsset()
{
}

void GfxAsset::Blit( Claw::Surface* target, float x, float y )
{
    target->Blit( x - m_pivot.x, y - m_pivot.y, m_surface );
}

void GfxAsset::Blit( Claw::Surface* target, float x, float y, float scale )
{
    Claw::TriangleEngine::Blit( target, m_surface, x, y, 0, scale, GetPivot() );
}

void GfxAsset::BlitAdditive( Claw::Surface* target, float x, float y )
{
    target->BlitAdditive( x - m_pivot.x, y - m_pivot.y, m_surface );
}

void GfxAsset::LoadPivotDB( const char* fn )
{
    Claw::FilePtr f( Claw::OpenFile( fn ) );
    if( !f )
    {
        CLAW_CHECK( f, "pivot.db not available. Use build-pivot.bat to create it." );
        return;
    }

    Claw::UInt32 num;
    f->Read( &num, 4 );
    for( int i=0; i<num; i++ )
    {
        Claw::UInt32 size;
        f->Read( &size, 4 );
        CLAW_ASSERT( size < 511 );
        char buf[512];
        f->Read( buf, size );
        buf[size] = '\0';
        float x, y;
        f->Read( &x, 4 );
        f->Read( &y, 4 );

        s_pivotDB.insert( std::make_pair( buf, Vectorf( x, y ) ) );
    }

    CLAW_MSG( "Loaded " << (int)s_pivotDB.size() << " pivots" );
}

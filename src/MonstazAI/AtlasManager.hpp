#ifndef __MONSTAZ_ATLASMANAGER_HPP__
#define __MONSTAZ_ATLASMANAGER_HPP__

#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Mutex.hpp"
#include "claw/base/Semaphore.hpp"
#include "claw/base/SmartPtr.hpp"
#include "claw/base/Thread.hpp"

struct AtlasSet
{
    enum Type
    {
        Background,
        Base,
        Overrides,
        Boss1,
        Boss1Shared,
        Boss2,
        Boss3,
        Enemy1,
        Enemy2,
        Enemy3,
        Enemy4,
        Enemy5,
        Enemy6,
        Enemy7,
        Enemy8,
        Menu,
        Map01,
        Map02,
        Map03,
        Map04,
        Map05,
        Map06,
        EnvCommon,
        EnvUrban,
        EnvSuburbs,
        EnvCity,
        EnvDesert,
        EnvIce,
        Player01,
        Player02,
        Player03,
        Player04,
        Player05,
        Player06,
        Player07,
        Player08,
        Player09,
        Player10,
        Player11,
        Player12,
        Player13,
        Player14,
        Player15,
        Player16,
        Player17,
        Player18,
        PlayerMech,

        NUMBER_OF_SETS,
        Terminator
    };

private:
    AtlasSet();
};

class AtlasManager : public Claw::RefCounter
{
public:
    AtlasManager( int budget );
    ~AtlasManager();

    void Preload( AtlasSet::Type req );
    void Request( const AtlasSet::Type* req );
    bool AreLoaded( const AtlasSet::Type* req );
    bool AreLoaded();
    void Release( const AtlasSet::Type* req );

    int GetSize() const { return m_size; }
    int GetBudget() const { return m_budget; }

    static void InitEnum( Claw::Lua* lua );

    static AtlasManager* GetInstance() { return s_instance; }

private:
    static int ThreadEntry( void* ptr ) { return ((AtlasManager*)ptr)->Worker(); }
    int Worker();

    int ImageSize( const char* atlas );
    void MakeSpace();

    bool AtlasExists( const char* atlas );

    Claw::Thread* m_thread;
    Claw::Semaphore m_workerWait;
    Claw::Mutex m_queueLock;

    bool m_abort;

    std::vector<AtlasSet::Type> m_wanted;
    std::vector<AtlasSet::Type> m_loaded;
    bool m_hold[AtlasSet::NUMBER_OF_SETS];
    int m_imgSize[AtlasSet::NUMBER_OF_SETS];
    AtlasSet::Type m_current;

    int m_size;
    int m_budget;

    static AtlasManager* s_instance;
};

typedef Claw::SmartPtr<AtlasManager> AtlasManagerPtr;

#endif

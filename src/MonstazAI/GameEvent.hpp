#ifndef __INCLUDED__MISSIONS__GAMEVENT_HPP__
#define __INCLUDED__MISSIONS__GAMEVENT_HPP__

#include "claw/base/Lua.hpp"
#include "claw/base/String.hpp"

enum GamEventId
{
    GEI_ENEMY_KILLED,
    GEI_OBJECT_DESTROYED,
    GEI_OBJECT_PICKUP,
    GEI_BULLETS_EARNED,
    GEI_LEVEL_STARTED,
    GEI_LEVEL_RESTARTED,
    GEI_LEVEL_WITH_FRIEND,
    GEI_LEVEL_FINISHED,
    GEI_TUTORIAL_STARTED,
    GEI_TUTORIAL_TASK_COMPLETED,
    GEI_TUTORIAL_COMPLETED,
    GEI_CASH_CHANGED,
    GEI_SHOT_PLAYER_START,
    GEI_SHOT_PLAYER_STOP,
    GEI_GAME_PAUSED,
    GEI_GAME_RESUMED,
    GEI_PERK_USED,
    GEI_ITEM_BOUGHT,
    GEI_ITEM_UNLOCKED,
    GEI_ITEM_UPGRADED,
    GEI_ITEM_USED,
    GEI_ITEM_CANT_AFFORD,
    GEI_GAME_SCORE_CHANGED,
    GEI_GAME_HISCORE_CHANGED,
    GEI_AUTOAIMING_CHANGED,
    GEI_REVIVE_PLAYER,
    GEI_SETTINGS_TAB_CHANGED,
    GEI_FRIEND_INVITED,
    GEI_FRIEND_INVITATION_CANCELED,
    GEI_FRIEND_INVITATION_CONFIRMED,
    GEI_FRIEND_GIFT_SENT,
    GEI_FRIEND_GIFT_RECIVED,
    GEI_FRIEND_PROFILE_SHOWN,
    GEI_FRIEND_PROFILE_ACTION,
    GEI_AGE_CHECKED,
    GEI_TRANSACTION_COMPLETED,
    GEI_TRANSACTION_FAILED,
    GEI_TRANSACTION_CANCELED,
    GEI_TRANSACTION_STARTED,
    GEI_CASH_ACTION_STARTED,
    GEI_CASH_ACTION_FINISHED,
    GEI_CASH_ACTION_SUBSCRIPTION_CANELED,
    GEI_FREE_STUFF_ACTION,
    GEI_NO_CASH_ACTION,
    GEI_SUMMARY_ACTION,
    GEI_SHOP_STATE_CHANGED,
    GEI_MISSION_COMPLETED,
    GEI_MISSION_CLAIMED,
    GEI_LTO_SHOWN,
    GEI_LTO_ACTION,
    GEI_SOCIAL_LOGIN_SKIP,
    GEI_SOCIAL_LOGIN_TRY,
    GEI_SOCIAL_LOGIN_SUCCEED,
    GEI_FUEL_SHOWN,
    GEI_FUEL_ACTION,
    GEI_NEW_WEAPON_SHOWN,
    GEI_NEW_WEAPON_ACTION
};

enum GameEventParam
{
    GEP_LEVEL_STARTED_STORY,
    GEP_LEVEL_STARTED_SURVIVAL,
    GEP_LEVEL_STARTED_TUTORIAL,
    GEP_LEVEL_STARTED_BOSSFIGHT,
    GEP_LEVEL_FINISHED_ABORT,
    GEP_LEVEL_FINISHED_SUCCESS,
    GEP_LEVEL_FINISHED_FAIL,
    GEP_AGE_LT_8,
    GEP_AGE_GTE_8,
    GEP_FREE_STUFF_FACEBOOK,
    GEP_FREE_STUFF_TWITTER,
    GEP_FREE_STUFF_RATE,
    GEP_FREE_STUFF_SOUNDTRACK,
    GEP_NO_CASH_BACK,
    GEP_NO_CASH_GET,
    GEP_SHOP_OPEN,
    GEP_SHOP_CLOSE,
    GEP_SUMARY_SHOP,
    GEP_SUMARY_RESTART,
    GEP_SUMARY_BACK,
    GEP_SUMARY_ITEM_1,
    GEP_SUMARY_ITEM_2,
    GEP_LTO_BACK,
    GEP_LTO_GET_REGULAR,
    GEP_LTO_GET_SOFT,
    GEP_LTO_GET_HARD,
    GEP_LOGIN_FACEBOOK,
    GEP_LOGIN_GOOGLE_PLUS,
    GEP_LOGIN_CREATE,
    GEP_LOGIN_LOGIN,
    GEP_PROFILE_CLOSE,
    GEP_PROFILE_CLOSE_WITHOUT_GIFT,
    GEP_PROFILE_SEND_GIFT,
    GEP_FUEL_HUD,
    GEP_FUEL_RAN_OUT,
    GEP_FUEL_REFIL,
    GEP_FUEL_INVITE,
    GEP_FUEL_USE_GIFT,
    GEP_FUEL_WAIT,
    GEP_NEW_WEAPON_BACK,
    GEP_NEW_WEAPON_GET
};

class GameEvent;

class GameEvent
{
public:
    typedef GamEventId Id;
    GameEvent( const Id& id, float value = 0, const Claw::NarrowString& text = "", void* userData = NULL );

    static void InitLua( Claw::Lua* lua );

    const Id& GetId() const { return m_id; }
    float GetValue() const { return m_paramValue; }
    const Claw::NarrowString& GetText() const { return m_paramText; }
    const void* GetUserData() const { return m_paramUserData; }

private:
    const Id m_id;
    const float m_paramValue;
    const Claw::NarrowString m_paramText;
    const void* m_paramUserData;
};

inline GameEvent::GameEvent( const Id& id, float value, const Claw::NarrowString& text, void* userData )
    : m_id( id )
    , m_paramValue( value )
    , m_paramText( text )
    , m_paramUserData( userData )
{}

class GameEventHandler
{
public:
    virtual ~GameEventHandler();

    virtual bool HandleGameEvent( const GameEvent& ev ) = 0;
    virtual bool HandleGameEvent( const GameEvent::Id& id, float value = 0, const Claw::NarrowString& text = "", void* userData = NULL );

protected:
    GameEventHandler( bool autoRegister = false );

private:
    bool m_autoRegister;
};

#endif

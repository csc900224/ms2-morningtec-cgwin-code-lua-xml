#ifndef __APPLICATION_CONFIG_HPP__
#define __APPLICATION_CONFIG_HPP__

#if defined CLAW_PLAYBOOK
#define CASH_SHOP_DISABLED
#endif

#ifdef CLAW_ANDROID
//#define AMAZON_BUILD   // Amazon compatible build   (see: MonstazAI-amazon.claw)
//#define METAFLOW_BUILD // Metaflow compatible build (see: MonstazAI-metaflow.claw)
#define ANDROID_BUILD
#define XPERIA_PLAY_BUILD
#endif

#if defined AMAZON_BUILD
#define MOREGAMES_DISABLED
#define RATEUS_DISABLED
#define OPENFEINT_DISABLED
#elif defined METAFLOW_BUILD
#define MOREGAMES_DISABLED
#define RATEUS_DISABLED
#define CASH_SHOP_DISABLED
#endif 

#define MOREGAMES_DISABLED
#define RATEUS_DISABLED
#define OPENFEINT_DISABLED

// Permanently disabled stuff
#define AIRPUSH_DISABLED
#define VIDEO_AD_DISABLED

#endif

#include <math.h>

#include "claw/application/DebugOverlay.hpp"
#include "claw/base/AssetDict.hpp"
#include "claw/math/Math.hpp"
#include "claw/base/Registry.hpp"
#include "claw/base/Xml.hpp"
#include "claw/compat/Platform.h"
#include "claw/graphics/TriangleEngine.hpp"
#include "claw/sound/mixer/AudioChannel.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/AnimationSet.hpp"
#include "MonstazAI/GameManager.hpp"
#include "MonstazAI/obstacle/ObstacleCircle.hpp"
#include "MonstazAI/obstacle/ObstacleRectangle.hpp"
#include "MonstazAI/RNG.hpp"
#include "MonstazAI/shot/RipperShot.hpp"
#include "MonstazAI/shot/Mine.hpp"
#include "MonstazAI/particle/GeiserEmitter.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/Application.hpp"
#include "MonstazAI/entity/BehaviorFunctors.hpp"
#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/shot/GrenadeShot.hpp"
#include "MonstazAI/shot/EggShot.hpp"
#include "MonstazAI/shot/HoundShot.hpp"
#include "MonstazAI/entity/effect/EffectHealth.hpp"
#include "MonstazAI/entity/effect/EffectNameplate.hpp"
#include "MonstazAI/VibraController.hpp"
#include "MonstazAI/GameCenterManager.hpp"
#include "MonstazAI/particle/SpiralParticle.hpp"
#include "MonstazAI/particle/ExplosionEmitter.hpp"
#include "MonstazAI/particle/GibParticle.hpp"
#include "MonstazAI/particle/BushParticle.hpp"
#include "MonstazAI/particle/ExplosionParticle.hpp"
#include "MonstazAI/shot/SpiralShot.hpp"
#include "MonstazAI/shot/VortexShot.hpp"
#include "MonstazAI/ServerConstants.hpp"
#include "MonstazAI/IsoSet.hpp"
#include "MonstazAI/missions/MissionManager.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"
#include "MonstazAI/ConnectionMonitor.hpp"

static const float s_nukeTime = 3.0f;
static const float s_airstrikeTime = 6.0f;

const static Vectorf s_nukeLaunch[] = {
    Vectorf( 0, -29 ),
    Vectorf( 0, -29 ),
    Vectorf( 0, -29 ),
    Vectorf( 0, -28 ),
    Vectorf( 0, -27 ),
    Vectorf( 0, -26 ),
    Vectorf( 0, -25 ),
    Vectorf( 0, -24 ),
    Vectorf( 0, -25 ),
    Vectorf( 0, -25 ),
    Vectorf( 0, -25 ),
    Vectorf( 0, -26 ),
    Vectorf( 0, -27 ),
    Vectorf( 0, -28 ),
    Vectorf( 0, -29 ),
    Vectorf( 0, -29 )
};

const static Vectorf s_houndShot[][8] = { {
    Vectorf( -6, -35 ),
    Vectorf( -1, -32 ),
    Vectorf( 5, -34 ),
    Vectorf( 10, -38 ),
    Vectorf( 7, -43 ),
    Vectorf( 1, -45 ),
    Vectorf( -5, -42 ),
    Vectorf( -8, -38 )
},{
    Vectorf( 6, -35 ),
    Vectorf( 9, -39 ),
    Vectorf( 5, -43 ),
    Vectorf( -1, -45 ),
    Vectorf( -7, -43 ),
    Vectorf( -9, -39 ),
    Vectorf( -5, -34 ),
    Vectorf( 1, -32 )
}
};

const static Vectorf s_lobsterShot[][8] = { {
    Vectorf( -6, 54 ),
    Vectorf( -16, 48 ),
    Vectorf( 7, 53 ),
    Vectorf( -17, 40 ),
    Vectorf( 16, 47 ),
    Vectorf( 5, 33 ),
    Vectorf( -7, 33 ),
    Vectorf( 15, 38 )
},{
    Vectorf( 6, 54 ),
    Vectorf( -8, 53 ),
    Vectorf( 15, 48 ),
    Vectorf( -18, 47 ),
    Vectorf( 17, 40 ),
    Vectorf( -6, 33 ),
    Vectorf( -16, 39 ),
    Vectorf( 7, 32 )
}
};

struct SetData
{
    SetData( int directions, int frames, const char* path ) : directions( directions ), frames( frames ), path( path ) {}

    int directions;
    int frames;
    const char* path;
};

SetData s_setData[] = {
    SetData( 8, 12, "gfx/tentacler/Enemy1_Walk" ),
    SetData( 8, 12, "gfx/tentacler2/Enemy1_Walk" ),
    SetData( 8, 7, "gfx/squeezer/Enemy2_Wheelie" ),
    SetData( 8, 7, "gfx/squeezer2/Enemy2_Wheelie" ),
    SetData( 8, 8, "gfx/fish/Enemy3_Walk" ),
    SetData( 8, 8, "gfx/fish2/Enemy3_Walk" ),
    SetData( 8, 12, "gfx/tentacler3/Enemy1_Walk" ),
    SetData( 8, 16, "gfx/floater/Enemy4_Walk" ),
    SetData( 8, 11, "gfx/floater2/Enemy5_Walk" ),
    SetData( 8, 11, "gfx/hound/Enemy6_Walk" ),
    SetData( 8, 11, "gfx/hound2/Enemy7_Walk" ),
    SetData( 8, 7, "gfx/cat/Cat_Walk" ),
    SetData( 8, 8, "gfx/tentacler/Enemy1_Attack" ),
    SetData( 8, 8, "gfx/tentacler2/Enemy1_Attack" ),
    SetData( 8, 7, "gfx/squeezer2/Enemy2_Bite" ),
    SetData( 8, 11, "gfx/fish/Enemy3_Attack" ),
    SetData( 8, 11, "gfx/fish2/Enemy3_Attack" ),
    SetData( 8, 8, "gfx/tentacler3/Enemy1_Attack" ),
    SetData( 8, 14, "gfx/floater/Enemy4_Attack" ),
    SetData( 8, 10, "gfx/floater2/Enemy5_Attack" ),
    SetData( 8, 8, "gfx/hound/Enemy6_Attack" ),
    SetData( 8, 8, "gfx/cat/Cat_Run" ),
    SetData( 8, 12, "gfx/cat/Cat_EatGrass" ),
    SetData( 8, 7, "gfx/cat/Cat_Furrball" ),
    SetData( 1, 18, "gfx/squeezer/Enemy2_Idle" ),
    SetData( 1, 18, "gfx/squeezer2/Enemy2_Idle" ),
    SetData( 8, 8, "gfx/fish/Enemy3_Rage" ),
    SetData( 8, 8, "gfx/fish2/Enemy3_Rage" ),
    SetData( 8, 8, "gfx/cat/Cat_Lick1_loop" ),
    SetData( 8, 8, "gfx/cat/Cat_Lick2_loop" ),
    SetData( 8, 10, "gfx/cat/Cat_Roll_loop" ),
    SetData( 8, 1, "gfx/hound2/Enemy7_Walk" ),
    SetData( 8, 24, "gfx/squeezer/Enemy2_Bounce" ),
    SetData( 8, 24, "gfx/squeezer2/Enemy2_Bounce" ),
    SetData( 8, 20, "gfx/fish2/Enemy3_Throw" ),
    SetData( 8, 10, "gfx/floater/Enemy4_Attack2" ),
    SetData( 8, 10, "gfx/floater2/Enemy5_Attack" ),
    SetData( 8, 9, "gfx/hound2/Enemy7_Shot" ),
    SetData( 8, 6, "gfx/sectoid/GreyA_Attack1" ),
    SetData( 8, 5, "gfx/sectoid/GreyA_Attack2" ),
    SetData( 8, 6, "gfx/sectoid/GreyA_Walk" ),
    SetData( 8, 6, "gfx/killerwhale/KillerWhaleB_Walk" ),
    SetData( 8, 6, "gfx/killerwhale/KillerWhaleB_Attack2" ),
    SetData( 8, 6, "gfx/killerwhale/KillerWhaleB_Idle" ),
    SetData( 8, 6, "gfx/killerwhale/KillerWhaleB_Attack1" ),
    SetData( 8, 6, "gfx/killerwhale2/KillerWhaleA_Walk" ),
    SetData( 8, 6, "gfx/killerwhale2/KillerWhaleA_Attack" ),
    SetData( 8, 16, "gfx/sectoid2/GreyB_Attack3" ),
    SetData( 8, 7, "gfx/sectoid2/GreyB_Walk1" ),
    SetData( 8, 7, "gfx/sectoid2/GreyB_Walk2" ),
    SetData( 8, 1, "gfx/sectoid2/GreyB_Attack3" ),
    SetData( 8, 7, "gfx/crab/Crab_Attack" ),
    SetData( 8, 11, "gfx/crab/Crab_Digin" ),
    SetData( 8, 10, "gfx/crab/Crab_Digout" ),
    SetData( 8, 12, "gfx/crab/Crab_Idle" ),
    SetData( 8, 7, "gfx/crab/Crab_Walk" ),
    SetData( 8, 7, "gfx/mechaboss/MechaT_AttackFlamer1" ),
    SetData( 8, 11, "gfx/mechaboss/MechaT_AttackMace" ),
    SetData( 1, 9, "gfx/mechaboss/MechaT_AttackMace_Round" ),
    SetData( 8, 10, "gfx/mechaboss/MechaT_AttackRocket" ),
    SetData( 8, 5, "gfx/mechaboss/MechaT_Fly" ),
    SetData( 8, 10, "gfx/mechaboss/MechaT_Walk" ),
    SetData( 8, 6, "gfx/lobster/Lobster_AttackIn" ),
    SetData( 8, 5, "gfx/lobster/Lobster_AttackOut" ),
    SetData( 8, 6, "gfx/lobster/Lobster_AttackShoot" ),
    SetData( 8, 4, "gfx/lobster/Lobster_Hit" ),
    SetData( 8, 8, "gfx/lobster/Lobster_Walk" ),
    SetData( 8, 10, "gfx/nautil/Nautil_Bounce_in" ),
    SetData( 8, 6, "gfx/nautil/Nautil_Bounce_loop" ),
    SetData( 8, 8, "gfx/nautil/Nautil_Bounce_out" ),
    SetData( 8, 18, "gfx/nautil/Nautil_Idle" ),
    SetData( 8, 7, "gfx/nautil/Nautil_Wheeli_loop" ),
    SetData( 8, 10, "gfx/nautil2/NautilB_Bounce_in" ),
    SetData( 8, 6, "gfx/nautil2/NautilB_Bounce_loop" ),
    SetData( 8, 8, "gfx/nautil2/NautilB_Bounce_out" ),
    SetData( 8, 18, "gfx/nautil2/NautilB_Idle" ),
    SetData( 8, 7, "gfx/nautil2/NautilB_Wheeli_loop" ),
    SetData( 8, 7, "gfx/nautil2/NautilB_Bite" ),
    SetData( 8, 14, "gfx/nerval/Nerval_Attack1" ),
    SetData( 8, 11, "gfx/nerval/Nerval_Attack2" ),
    SetData( 8, 8, "gfx/nerval/Nerval_Fly" ),
    SetData( 8, 5, "gfx/nerval/Nerval_FlyAttack" ),
    SetData( 8, 8, "gfx/nerval/Nerval_Walk" ),
    SetData( 8, 10, "gfx/sower/Sower_Eggshot" ),
    SetData( 8, 8, "gfx/sower/Sower_Eggspit" ),
    SetData( 8, 8, "gfx/sower/Sower_Tongueshot" ),
    SetData( 8, 10, "gfx/sower/Sower_Walk" ),
    SetData( 8, 7, "gfx/octobrain/Octobrain_AttackBrain" ),
    SetData( 8, 7, "gfx/octobrain/Octobrain_AttackCharge" ),
    SetData( 8, 5, "gfx/octobrain/Octobrain_AttackGun" ),
    SetData( 8, 11, "gfx/octobrain/Octobrain_Death" ),
    SetData( 8, 8, "gfx/octobrain/Octobrain_DeathClone" ),
    SetData( 8, 5, "gfx/octobrain/Octobrain_Hit" ),
    SetData( 8, 6, "gfx/octobrain/Octobrain_Teleport" ),
    SetData( 8, 9, "gfx/octobrain/Octobrain_Walk" ),
    SetData( 16, 10, "gfx/floater3/sowerchild_attack1" ),
    SetData( 16, 14, "gfx/floater3/sowerchild_attack2" ),
    SetData( 16, 16, "gfx/floater3/sowerchild_move" )
};

SetData s_playerSetData[] = {
    SetData( 32, 10, "gfx/player/DumDum_chaingun/DumDum_chaingun" ),            // 0
    SetData( 32, 10, "gfx/player/DumDum_cutter/DumDum_cutter" ),                // 1
    SetData( 32, 10, "gfx/player/DumDum_dualmagnum/DumDum_dualmagnum" ),        // 2
    SetData( 32, 10, "gfx/player/DumDum_energywhip/DumDum_energywhip" ),        // 3
    SetData( 32, 10, "gfx/player/DumDum_frostshotgun/DumDum_frostshotgun" ),    // 4
    SetData( 32, 10, "gfx/player/DumDum_gundi/DumDum_gundi" ),                  // 5
    SetData( 32, 10, "gfx/player/DumDum_icegun/DumDum_icegun" ),                // 6
    SetData( 32, 10, "gfx/player/DumDum_railgun/DumDum_railgun" ),              // 7
    SetData( 32, 10, "gfx/player/DumDum_ripper/DumDum_ripper" ),                // 8
    SetData( 32, 10, "gfx/player/DumDum_roaster/DumDum_roaster" ),              // 9
    SetData( 32, 10, "gfx/player/DumDum_rocket/DumDum_rocket" ),                // 10
    SetData( 32, 10, "gfx/player/DumDum_saw/DumDum_saw" ),                      // 11
    SetData( 32, 10, "gfx/player/DumDum_shocker/DumDum_shocker" ),              // 12
    SetData( 32, 10, "gfx/player/DumDum_shotgun/DumDum_shotgun" ),              // 13
    SetData( 32, 10, "gfx/player/DumDum_spikegun/DumDum_spikegun" ),            // 14
    SetData( 32, 10, "gfx/player/DumDum_submachine/DumDum_submachine" ),        // 15
    SetData( 32, 10, "gfx/player/DumDum_tripalizer/DumDum_tripalizer" ),        // 16
    SetData( 32, 10, "gfx/player/DumDum_vortexgun/DumDum_vortexgun" ),          // 17
    SetData( 32, 12, "gfx/player/MechaDum_Walk/MechaDum_Walk" ),                // 18
    SetData( 32, 12, "gfx/player/MechaDum_Walk/MechaDum_Walk" ),                // 19
    SetData( 32, 10, "gfx/player2/DumDumRed_chaingun/DumDumRed_chaingun" ),     // 20
    SetData( 32, 10, "gfx/player2/DumDumRed_cutter/DumDumRed_cutter" ),         // 21
    SetData( 32, 10, "gfx/player2/DumDumRed_dualmagnum/DumDumRed_dualmagnum" ), // 22
    SetData( 32, 10, "gfx/player2/DumDumRed_energywhip/DumDumRed_energywhip" ), // 23
    SetData( 32, 10, "gfx/player2/DumDumRed_frostshotgun/DumDumRed_frostshotgun" ), // 24
    SetData( 32, 10, "gfx/player2/DumDumRed_gundi/DumDumRed_gundi" ),           // 25
    SetData( 32, 10, "gfx/player2/DumDumRed_icegun/DumDumRed_icegun" ),         // 26
    SetData( 32, 10, "gfx/player2/DumDumRed_railgun/DumDumRed_railgun" ),       // 27
    SetData( 32, 10, "gfx/player2/DumDumRed_ripper/DumDumRed_ripper" ),         // 28
    SetData( 32, 10, "gfx/player2/DumDumRed_roaster/DumDumRed_roaster" ),       // 29
    SetData( 32, 10, "gfx/player2/DumDumRed_rocket/DumDumRed_rocket" ),         // 30
    SetData( 32, 10, "gfx/player2/DumDumRed_saw/DumDumRed_saw" ),               // 31
    SetData( 32, 10, "gfx/player2/DumDumRed_shocker/DumDumRed_shocker" ),       // 32
    SetData( 32, 10, "gfx/player2/DumDumRed_shotgun/DumDumRed_shotgun" ),       // 33
    SetData( 32, 10, "gfx/player2/DumDumRed_spikegun/DumDumRed_spikegun" ),     // 34
    SetData( 32, 10, "gfx/player2/DumDumRed_submachine/DumDumRed_submachine" ), // 35
    SetData( 32, 10, "gfx/player2/DumDumRed_tripalizer/DumDumRed_tripalizer" ), // 36
    SetData( 32, 10, "gfx/player2/DumDumRed_vortexgun/DumDumRed_vortexgun" ),   // 37
    SetData( 32, 12, "gfx/player/MechaDum_Walk/MechaDum_Walk" ),                // 38
    SetData( 32, 12, "gfx/player/MechaDum_Walk/MechaDum_Walk" ),                // 39
};

static const float GAME_END_DELAY               = 2.0f;     // Wait 2 sec for enemies/player death anim playback
static const float GLOP_SPAWN_SQR_DIST          = 80.0f;    // Minimum square distance between consequtive spawns
static const float GLOP_SPAWN_SQR_DIST_RANGE    = 60.0f;    // Spawn distance randomization factor
static const float PAUSE_AREA_SCALE             = 0.4f;

LUA_DECLARATION( GameManager )
{
    METHOD( GameManager, CalculateShotAvoidance ),
    METHOD( GameManager, CalculateAvoidance ),
    METHOD( GameManager, CalculateObstacles ),
    METHOD( GameManager, ProcessExplosions ),
    METHOD( GameManager, AvoidLineOfSight ),
    METHOD( GameManager, ProcessShots ),
    METHOD( GameManager, FillSegmentTable ),
    METHOD( GameManager, MonstersEatPlayer ),
    METHOD( GameManager, MonstersEatFriends ),
    METHOD( GameManager, CheckObstacleCollision ),
    METHOD( GameManager, SummaryScreen ),
    METHOD( GameManager, SetPlayerHP ),
    METHOD( GameManager, NextPerkLevel ),
    METHOD( GameManager, StartStoryTutorial ),
    METHOD( GameManager, Analytics ),
    METHOD( GameManager, GetClosestEnemy ),
    METHOD( GameManager, GetClosestTarget ),
    METHOD( GameManager, SetShieldTime ),
    METHOD( GameManager, PlayerChangedWeapon ),
    METHOD( GameManager, AddVPadWeapon ),
    METHOD( GameManager, VPadWeaponSwitch ),
    METHOD( GameManager, StartRevive ),
    {0,0}
};

GameManager* GameManager::s_instance = NULL;
float GameManager::s_gameScale = 0;

Claw::SurfacePtr GameManager::GetBloodAsset( int idx )
{
    char tmp[64];

    switch( Claw::Registry::Get()->CheckInt( "/monstaz/settings/blood" ) )
    {
    case 0:
        return Claw::AssetDict::Get<Claw::Surface>( "gfx/assets/trans.png@linear" );
    case 1:
        sprintf( tmp, "gfx/splat_0%i.png@linear", idx+1 );
        return Claw::AssetDict::Get<Claw::Surface>( tmp );
    case 2:
        sprintf( tmp, "gfx/g_splat_0%i.png@linear", idx+1 );
        return Claw::AssetDict::Get<Claw::Surface>( tmp );
    default:
        CLAW_ASSERT( false );
        break;
    }
}

static const char* vertexVengeance =
    "uniform highp vec2 offset;\n"
    "varying highp vec2 vTex2[4];\n"
    "void main(void)\n"
    "{\n"
    "TEXTURE;\n"
    "vTex2[0] = inUV + vec2( offset.x, 0.0 );\n"
    "vTex2[1] = inUV - vec2( offset.x, 0.0 );\n"
    "vTex2[2] = inUV + vec2( 0.0, offset.y );\n"
    "vTex2[3] = inUV - vec2( 0.0, offset.y );\n"
    "GLPOSITION;\n"
    "}";

static const char* fragmentVengeance =
    "uniform lowp float m;\n"
    "varying highp vec2 vTex2[4];\n"
    "void main(void)\n"
    "{\n"
    "mediump vec3 t = texture2D( clawTex, vTex ).rgb;\n"
    "gl_FragColor = vec4( mix( t, 5.0 * t - texture2D( clawTex, vTex2[0] ).rgb - texture2D( clawTex, vTex2[1] ).rgb - texture2D( clawTex, vTex2[2] ).rgb - texture2D( clawTex, vTex2[3] ).rgb, m ), 1.0 );\n"
    "}";


GameManager::GameManager( const char* filename, Stats* stats )
    : m_renderObstacles( false )
    , m_withdrawPerk( 0 )
    , m_lua( new Claw::Lua() )
    , m_entityManager( new EntityManager( m_lua ) )
    , m_shotManager( new ShotManager( m_lua ) )
    , m_obstacleManager( new ObstacleManager( m_lua ) )
    , m_triggerManager( new TriggerManager( m_lua ) )
    , m_pickupManager( new PickupManager( m_lua ) )
    , m_explosionManager( new ExplosionManager( m_lua ) )
    , m_renderableManager( new RenderableManager() )
    , m_waypointManager( new WaypointManager( m_lua ) )
    , m_hud( new Hud() )
    , m_stats( stats )
    , m_particleSystem( new ParticleSystem( true ) )
    , m_map( new Map( m_lua ) )
    , m_timeController( new TimeController() )
    , m_audioManager( AudioManager::GetInstance() )
    , m_player( NULL )
    , m_splatterCnt( 0 )
    , m_slaughter( false )
    , m_weaponBoost( 0 )
    , m_weaponBoostTime( 5 )
    , m_nukeAvailable( Shop::GetInstance()->CheckOwnership( Shop::Items::Nuke ) != 0 )
    , m_airstrikeAvailable( Shop::GetInstance()->CheckOwnership( Shop::Items::Airstrike ) != 0  )
    , m_nuke( 0 )
    , m_nukeTimer( 0 )
    , m_airstrikeTimer( 0 )
    , m_segmentTable( new Entity*[STS*STS] )
    , m_menu( new MenuInGame() )
    , m_menuActive( false )
    , m_ticker( 0 )
    , m_time( 0 )
    , m_gameEnd( false )
    , m_gameEndFail( false )
    , m_gameEndTimer( GAME_END_DELAY )
    , m_playerLastHit( 0 )
    , m_playerNextGlopTraceDist( 0 )
    , m_playerLastGlopTracePos( 0, 0 )
    , m_ricochetLastHit( 0 )
    , m_geiserFunctor( new GeiserParticleFunctor( 128, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/geiser.png@linear" ) ) )
    , SCREEN_WIDTH( ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution().x )
    , SCREEN_HEIGHT( ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution().y )
    , m_shadow( Claw::AssetDict::Get<Claw::Surface>( "gfx/shadow.png@linear" ) )
    , m_heatNoise( Claw::AssetDict::Get<Claw::Surface>( "gfx/heat/noise.png@linear" ) )
    , m_nukeLaunch( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/nuke_exp.ani@linear" ) )
    , m_nukeShadow( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/nuke_shadow.png@linear" ) )
    , m_nukePayload( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/nuke.png@linear" ) )
    , m_healthAnim( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/health.ani" ) )
    , m_healthGlow( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/health_glow.png@linear" ) )
    , m_shotFlashGfx( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/DD_fire_glow.png@linear" ) )
    , m_pauseMenu( false )
    , m_survival( Claw::Registry::Get()->CheckBool( "/internal/survival" ) )
    , m_bossFight( false )
    , m_pauseNotification( false )
    , m_deathTimer( 0 )
    , m_displayControlsEnabled( true )
    , m_tracking( NULL )
    , m_trackingTime( 0 )
    , m_autoaim( false )
    , m_orbCollected( false )
    , m_playerShield( 0 )
    , m_weaponSet( 0 )
    , m_spiralTrail( new SpiralParticleFunctor( 1024, Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/flare.png@linear" ) ) )
    , m_bomber( new BomberParticleFunctor( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/bomber.png@linear" ) ) )
    , m_targetIndicator( new TargetParticleFunctor( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/target_circle.png@linear" ), Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/target_lines.png@linear" ), Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/target_arrow.png@linear" ) ) )
    , m_energyBar( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/energy_bar.png@linear" ) )
    , m_energyBarRed( Claw::AssetDict::Get<Claw::Surface>( "gfx/hud/energy_bar_fill.png@linear" ) )
    , m_electricityGlow( new GlowParticleFunctor( 2048, Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/electricity_glow.png@linear" ) ) )
    , m_bloodSplat( new BloodParticleFunctor( 1 ) )
    , m_exploFlare( new FlareParticleFunctor( 440, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/flara5.png@linear" ) ) )
    , m_cloneDieFlare( new FlareParticleFunctor( 1000, Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/flara5.png@linear" ), M_PI / 2 ) )
    , m_hologram( new Hologram() )
    , m_dimmingGradient( Claw::AssetDict::Get<Claw::Surface>( "plate/gradient_black.png@linear" ) )
    , m_shotFlash( false )
    , SEGMENT_SEARCH_RADIUS( m_bossFight ? 3 : 1 )
    , m_vengeanceFx( new PostProcess( vertexVengeance, fragmentVengeance ) )
    , m_mech( false )
    , m_mapPath( NULL )
    , m_godmode( 0 )
    , m_mechTime( 0 )
    , m_mechLoop( false )
{
    typedef int static_assert_enemySet[ sizeof( s_setData ) / sizeof( SetData ) == sizeof( m_monsterSet ) / sizeof( AnimationSetPtr ) ? 1 : -1 ];
    typedef int static_assert_playerSet[ sizeof( s_playerSetData ) / sizeof( SetData ) == sizeof( m_playerSet ) / sizeof( AnimationSetPtr ) ? 1 : -1 ];

    GameplayJob* job = (GameplayJob*)((MonstazApp*)MonstazApp::GetInstance())->GetJob();
    //job->AddPostProcess( m_vengeanceFx );

    Claw::Registry::Get()->Get( "/internal/boss", m_bossFight );
    Claw::Registry::Get()->Set( "/internal/tokens", 0 );

    // Can't use Lua::Call because randomseed is inside table (and we don't support such scheme now).
    char tmp[48];
    sprintf( tmp, "math.randomseed(%i)", g_rng.GetInt() );
    m_lua->Execute( tmp );

    CLAW_ASSERT( Claw::AlignPOT<int>( SplatterSize ) == SplatterSize );
    CLAW_ASSERT( !s_instance );
    s_instance = this;

    TutorialManager::GetInstance()->Init( m_lua );
    TutorialManager::GetInstance()->SetGameLua( m_lua );

    m_audioManager->Init( m_lua );
    VibraController::GetInstance()->Init( m_lua );

    Claw::Lunar<Shop>::Register( *m_lua );
    Claw::Lunar<Shop>::push( *m_lua, Shop::GetInstance() );
    m_lua->RegisterGlobal( "Shop" );

    Claw::Lunar<GameManager>::Register( *m_lua );
    Claw::Lunar<GameManager>::push( *m_lua, this );
    m_lua->RegisterGlobal( "GameManager" );

    Claw::Lunar<Claw::Registry>::Register( *m_lua );
    Claw::Lunar<Claw::Registry>::push( *m_lua, Claw::Registry::Get() );
    m_lua->RegisterGlobal( "registry" );

    GameCenterManager::GetInstance()->Init( m_lua );
    GameEventDispatcher::GetInstance()->InitLua( m_lua );
    Missions::MissionManager::GetInstance()->InitLua( m_lua );

    Claw::Registry::Get()->Get( "/monstaz/settings/autoaim", m_autoaim );

#if defined CLAW_IPHONE || defined CLAW_ANDROID || defined CLAW_PLAYBOOK
    m_touch.Reset( new TouchControls() );
#endif

    m_lua->Load( filename );

    // Update ingame balance
    Claw::FilePtr luaScript = ClawExt::ServerSync::GetInstance()->LockTaskFile( ServerConstants::LUA_INGAME_SYNC_TASK );
    if( luaScript )
    {
        m_lua->Load( luaScript, ServerConstants::LUA_INGAME_REMOTE_FILE );
        m_lua->Call( "Synchronize", 0 , 0 );
        ClawExt::ServerSync::GetInstance()->ReleaseTaskFile( ServerConstants::LUA_INGAME_SYNC_TASK, luaScript );
    }

    // Apply synchronized data
    m_lua->Call( "WeaponApplyUpgrades", 0 , 0 );
    m_lua->Call( "EntityUpdateParams", 0 , 0 );

    // Update pause menu content
    luaScript = ClawExt::ServerSync::GetInstance()->LockTaskFile( ServerConstants::LUA_MENU_SYNC_TASK );
    if( luaScript )
    {
        m_menu->GetLua()->Load( luaScript, ServerConstants::LUA_MENU_REMOTE_FILE );
        m_menu->GetLua()->Call( "Synchronize", 0 , 0 );
        ClawExt::ServerSync::GetInstance()->ReleaseTaskFile( ServerConstants::LUA_MENU_SYNC_TASK, luaScript );
    }

    m_death[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/player/Death_045.ani" ) );
    m_death[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/player2/Death_000.ani" ) );

    m_shield[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/shield_r.ani" ) );
    m_shield[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/shield_y.ani" ) );
    m_shield[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/shield_g.ani" ) );
    m_shield[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/shield_b.ani" ) );

    for( int i=0; i<6; i++ )
    {
        m_blood[i].Reset( GetBloodAsset( i ) );
    }

    m_crabDigIn[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/crab_ground.png@linear" ) );
    m_crabDigIn[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/crab_ground_2.png@linear" ) );
    m_crabDigIn[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/crab_ground_3.png@linear" ) );
    m_glop[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/glop_spot_01.png@linear" ) );
    m_glop[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/glop_spot_02.png@linear" ) );
    m_spit[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/spit_spot_01.png@linear" ) );
    m_spit[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/spit_spot_02.png@linear" ) );
    m_explosionHole.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/exp_hole.png@linear" ) );
    m_explosion.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/explosion.ani" ) );
    m_impactShot[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/bullet1_exp.ani" ) );
    m_impactShot[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/bullet2_exp.ani" ) );

    int blood = Claw::Registry::Get()->CheckInt( "/monstaz/settings/blood" );
    for( int i=0; i<4; i++ )
    {
        char tmp[64];
        switch( blood )
        {
        case 0:
            m_impactBlood[i].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/trans.ani" ) );
            break;
        case 1:
            sprintf( tmp, "gfx/impact/blood%i.ani", i+1 );
            m_impactBlood[i].Reset( Claw::AssetDict::Get<Claw::Surface>( tmp ) );
            break;
        case 2:
            sprintf( tmp, "gfx/impact/g_blood%i.ani", i+1 );
            m_impactBlood[i].Reset( Claw::AssetDict::Get<Claw::Surface>( tmp ) );
            break;
        default:
            CLAW_ASSERT( false );
            break;
        }
    }

    m_impactPlasma.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/plasma_exp.ani" ) );
    m_impactFishGlop.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/fish_hit.ani" ) );
    m_impactSpiral.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/dualizer_exp.ani" ) );
    m_impactSowerSpit.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/impact/fish_hit_yellow.ani" ) );

    m_healthParticle[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/health_particle_01.png@linear" ) );
    m_healthParticle[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/health_particle_02.png@linear" ) );
    m_healthParticle[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/health_particle_03.png@linear" ) );

    m_deathLastFrame[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/player/Death_045_012.ani" ) );
    m_deathLastFrame[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/player/Death_045_012_white.ani" ) );

    m_shockedStars.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fx/sq_stars.ani" ) );

    m_laserGlow[0].Reset( new GlowParticleFunctor( 448, Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/laser_glow.png@linear" ) ) );
    m_laserGlow[1].Reset( new GlowParticleFunctor( 768, Claw::AssetDict::Get<Claw::Surface>( "gfx/projectiles/laser_glow.png@linear" ) ) );

    for( int i=0; i<5; i++ )
    {
        char tmp[64];
        sprintf( tmp, "gfx/thrash/trash_bush%i.png@linear", i+1 );
        m_bushThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( tmp ) );
    }
    m_bushRat.Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/thrash/trash_bush_rat.png@linear" ) );
    for( int i=0; i<8; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/thrash/trash_dust%02i.png@linear", i+1 );
        m_dustThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<13; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/thrash/trash_stone%02i.png@linear", i+1 );
        m_stoneThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
        sprintf( buf, "gfx/thrash/trash_bone%02i.png@linear", i+1 );
        m_skullThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<6; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/thrash/trash_plant%02i.png@linear", i+1 );
        m_cactusThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<7; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/thrash/trash_icedust%02i.png@linear", i+1 );
        m_iceDust[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<12; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/thrash/trash_iceshard%02i.png@linear", i+1 );
        m_iceThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }
    for( int i=0; i<5; i++ )
    {
        char buf[64];
        sprintf( buf, "gfx/thrash/trash_cloth%02i.png@linear", i+1 );
        m_clothThrash[i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
    }

    Claw::Registry::Get()->Set( "/internal/money", 0 );

    for( int i=0; i<RNDSIZE; i++ )
    {
        m_rndBuf[i] = ( g_rng.GetInt() % 256 ) - 128;
    }
}

GameManager::~GameManager()
{
    delete[] m_segmentTable;

    m_pickupManager.Release();

    if ( TutorialManager::GetInstance() )
    {
        TutorialManager::GetInstance()->SetGameLua( NULL );
    }

    CLAW_ASSERT( s_instance );
    s_instance = NULL;

    ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
}

void GameManager::Update( float dt )
{
    if( IsMech() )
    {
        if( m_mechTime == 0 )
        {
            AudioManager::GetInstance()->Play( SFX_MECH_START );
        }
        m_mechTime += dt;
        if( !m_mechLoop && m_mechTime > 3.95f )
        {
            AudioManager::GetInstance()->PlayLooped( SFX_MECH_LOOP );
            m_mechLoop = true;
        }
    }

    m_shotFlash = std::max( 0.f, m_shotFlash - dt * 10 );
    m_hologram->Update( dt );
    m_withdrawPerk -= dt;

    if( m_tracking )
    {
        m_trackingTime += dt;
        if( m_trackingTime > 1 )
        {
            m_tracking = NULL;
            m_trackingTime = 0;
        }
    }
    else
    {
        m_trackingTime = 0;
    }

    if( m_touch )
    {
        if( m_menuActive || !ConnectionMonitor::GetInstance()->HasNetworkConnection() )
        {
            m_touch->ResetDeadTimer( 5 );
        }
        else
        {
            m_touch->Update( dt );
        }
    }

    m_menu->Update( dt );

    if( !m_menuActive )
    {
        if( m_pauseNotification && !m_menuActive )
        {
            m_pauseNotification = false;
            m_hud->ShowPauseNotification();
        }
    }

    UpdateHud( dt );

    m_timeController->Update( dt );
    dt = m_timeController->Transform( dt );

    if( m_gameEndFail )
    {
        m_deathTimer += dt;
    }

    bool updateGameLogic = (!m_menuActive || ( m_gameEnd && ( m_gameEndTimer > 0 || m_gameEndFail ) )) && ConnectionMonitor::GetInstance()->HasNetworkConnection();
    // Gameplay related stuff - update only when menu is not active
    if( updateGameLogic )
    {
        m_time += dt;

        const float tick = 1/60.f;

        m_ticker += dt;
        while( m_ticker > tick )
        {
            m_ticker -= tick;
            m_map->Update( tick );
            m_lua->Call( "Tick", 0, 0 );
            m_entityManager->Update( tick );
            m_shotManager->Update( tick );
        }

        // Give him some time to death (animation, summary popup twean, etc.)
        if( m_gameEnd )
        {
            m_gameEndTimer -= dt;
        }

        if( m_weaponBoost > 0 )
        {
            m_weaponBoost -= dt;
            m_vengeanceFx->m_shader->Uniform( "m", Claw::SmoothStep<float>( 0, 1, Claw::MinMax<float>( std::min( m_weaponBoost, m_weaponBoostTime - m_weaponBoost ), 0, 1 ) ) );
            if( m_weaponBoost <= 0 )
            {
                m_lua->PushBool( false );
                m_lua->Call( "WeaponBoost", 1, 0 );
            }
        }

        m_pickupManager->Update( dt );
        m_explosionManager->Update( dt );
        m_stats->Update( dt );
        m_particleSystem->Update( dt );

        if( m_player )
        {
            m_triggerManager->CheckTriggers();
        }

        UpdateSplatters( dt );
        UpdateAnims( dt );
        UpdateNuke( dt );
        UpdateAirstrike( dt );
        UpdateGlopTrace( dt );

        m_shockedStars->Update( dt );

        m_lua->Call( "CheckRage", 0, 1 );
        if( m_player )
        {
            m_rage->SetActive( m_lua->CheckBool( -1 ) );
        }
        m_lua->Pop( 1 );

        if( m_playerShield > 0 )
        {
            m_playerShield -= dt;
            if( m_playerShield <= 0 )
            {
                m_playerShield = 0;
                m_shieldFx->SetActive( false );
            }
        }

        UpdateDimming( dt );
        UpdateFlash( dt );

        m_vengeanceFx->m_active = m_weaponBoost > 0;
    }

    if( m_godmode > 0 )
    {
        m_godmode -= dt;
        if( m_player )
        {
            m_player->SetHitPoints( m_player->GetMaxHitPoints() );

            const std::vector<Entity*>& ents = m_entityManager->GetEntities();
            for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
            {
                if( (*it)->GetType() == Entity::OctopusFriend )
                {
                    (*it)->SetHitPoints( (*it)->GetMaxHitPoints() );
                    break;
                }
            }
        }
    }
}

void GameManager::UpdateSplatters( float dt )
{
    std::vector<SplatterData>::iterator it = m_fadingSplatter.begin();
    while( it != m_fadingSplatter.end() )
    {
        it->t -= dt;
        if( it->t <= 0 || !IsSplatterOnScreen( *it ) )
        {
            it = m_fadingSplatter.erase( it );
        }
        else
        {
            ++it;
        }
    }
    for( int i=0; i<SplatterSize; i++ )
    {
        m_splatter[i].d -= dt;
    }
}

void GameManager::UpdateAnims( float dt )
{
    if( !( m_nuke == 2 && m_nukeTimer < 3 ) )
    {
        std::vector<AnimData>::iterator ait = m_anims.begin();
        while( ait != m_anims.end() )
        {
            if( ait->s->Update( dt ) )
            {
                ++ait;
            }
            else
            {
                if( ait->l )
                {
                    if( m_splatter[m_splatterCnt].g && IsSplatterOnScreen( m_splatter[m_splatterCnt] ) )
                    {
                        m_fadingSplatter.push_back( m_splatter[m_splatterCnt] );
                    }

                    int x, y;
                    m_splatter[m_splatterCnt].g.Reset( ait->s->GetRawSurface( x, y ) );
                    m_splatter[m_splatterCnt].p.x = ait->s->GetPos().x;
                    m_splatter[m_splatterCnt].p.y = ait->s->GetPos().y;
                    m_splatter[m_splatterCnt].p *= s_gameScale;

                    // Ignore entire surface offset (GetXOffset) substracting it from the real position (after retina transformation),
                    // but add animated surface offset according to the current animation frame. As the result we replace the original
                    // surface offset with the offset of the current frame within animation as it would be when rendering via AnimatedSurface.
                    m_splatter[m_splatterCnt].p.x += x - m_splatter[m_splatterCnt].g->GetXOffset();
                    m_splatter[m_splatterCnt].p.y += y - m_splatter[m_splatterCnt].g->GetYOffset();
                    // Splatters are drawn using their center as a pivot point while animations are left-top corner based
                    // add center pivot offset to match the splatter to the last animation frame (should be drawn in the same place)
                    m_splatter[m_splatterCnt].p.x += m_splatter[m_splatterCnt].g->GetWidth() / 2;
                    m_splatter[m_splatterCnt].p.y += m_splatter[m_splatterCnt].g->GetHeight() / 2;
                    m_splatter[m_splatterCnt].s = 1;
                    m_splatter[m_splatterCnt].d = 0;
                    if( ait->s->GetTransform() == AnimSurfWrap::Rotate )
                    {
                        //m_splatter[m_splatterCnt].a = g_rng.GetDouble() * M_PI * 2;
                        m_splatter[m_splatterCnt].a = -atan2( ait->s->GetRot().x, ait->s->GetRot().y );
                    }
                    else
                    {
                        m_splatter[m_splatterCnt].a = 0;
                    }
                    m_splatter[m_splatterCnt].f = Claw::TriangleEngine::FM_NONE;

                    m_splatterCnt = ( m_splatterCnt + 1 ) & ( SplatterSize - 1 );
                }

                ait = m_anims.erase( ait );
            }
        }
    }
}

void GameManager::UpdateHud( float dt )
{
    if( m_player )
    {
        m_hud->SetLife( m_player->GetHitPoints() / m_stats->GetMaxPlayerHP() );
    }
    else
    {
        m_hud->SetLife( 0 );
    }
    m_hud->SetXp( m_stats->GetXpProgress() );
    m_hud->SetLevelUp( m_stats->GetPerks() != 0 );
    m_hud->SetPoints( m_stats->GetPoints() );
    m_hud->SetMultiplier( m_stats->GetMultiplier() );
    int cash, gold = 0;
    Shop::GetInstance()->GetCash( cash, gold );
    m_hud->SetCash( cash );
    m_hud->SetInventory( 
        Shop::GetInstance()->CheckOwnership( Shop::Items::Grenade ), 
        Shop::GetInstance()->CheckOwnership( Shop::Items::HealthKit ),
        Shop::GetInstance()->CheckOwnership( Shop::Items::Mine ),
        Shop::GetInstance()->CheckOwnership( Shop::Items::Shield )
    );
    m_hud->Update( dt );
}

void GameManager::UpdateNuke( float dt )
{
    if( m_playerLastHit > 0 )
    {
        m_playerLastHit -= dt;
    }
    if( m_ricochetLastHit > 0 )
    {
        m_ricochetLastHit -= dt;
    }

    if( m_nuke == 0 && !m_nukeAvailable )
    {
        m_nukeAvailable = Shop::GetInstance()->CheckOwnership( Shop::Items::Nuke ) > 0;
    }
    else if( m_nuke == 1 )
    {
        m_nukeTimer += dt;
        if( m_nukeTimer > s_nukeTime )
        {
            AudioManager::GetInstance()->Play( SFX_NUKE );

            m_nukeTimer = 0;
            m_nuke = 2;

            int cnt = 0;
            std::vector<Entity*>& ents = m_entityManager->GetEntities();
            for( std::vector<Entity*>::iterator it = ents.begin(); it != ents.end(); ++it )
            {
                Entity::Type t = (*it)->GetType();
                if( t == Entity::MechaBoss || t == Entity::SowerBoss || t == Entity::OctobrainBoss )
                {
                    (*it)->Hit( Shot::Flamer, 255, (*it)->GetHitPoints() * 0.5f );
                }
                else if( (*it)->GetClass() != Entity::Friendly )
                {
                    (*it)->Hit( Shot::Flamer, 0, 0 );
                    (*it)->SetHitPoints( 0 );
                    cnt++;
                }
            }

            m_lua->PushBool( true );
            m_lua->Call( "PauseLevelTick", 1, 0 );
            if( m_player ) m_player->m_invulnerable = true;
        }
    }
    else if( m_nuke == 2 )
    {
        m_nukeTimer += dt;
        if( m_nukeTimer > 6 )
        {
            m_nuke = 0;
            m_nukeTimer = 0;

            m_lua->PushBool( false );
            m_lua->Call( "PauseLevelTick", 1, 0 );
            if( m_player ) m_player->m_invulnerable = false;
        }
    }
}

void GameManager::UpdateAirstrike( float dt )
{
    if( m_airstrikeTimer <= 0 && !m_airstrikeAvailable )
    {
        m_airstrikeAvailable = Shop::GetInstance()->CheckOwnership( Shop::Items::Airstrike ) > 0;
    }
    else
    {
        m_airstrikeTimer -= dt;
    }
}

void GameManager::UpdateGlopTrace( float dt )
{
    if( m_player && m_player->IsSlowedDown() )
    {
        if( ( m_playerLastGlopTracePos - m_player->GetPos() ).LengthSqr() > m_playerNextGlopTraceDist )
        {
            AddGlopRemains( m_player->GetPos(), m_player->GetSlowDownHit() );
        }
    }
}

void GameManager::StartDistortionEffect()
{
    if( m_distortion.state == Effect::ES_OFF  )
    {
        m_distortion.state =  Effect::ES_IN;
        m_distortion.timer = 0;
    }
}

void GameManager::StopDistortionEffect( bool force )
{
    if( m_distortion.state != Effect::ES_OFF )
    {
        m_distortion.state = force ? Effect::ES_OFF : Effect::ES_OUT;
        m_distortion.timer = 0;
    }
}

void GameManager::UpdateDimming( float dt )
{
    if( m_distortion.state != Effect::ES_OFF )
    {
        const float FADE_IN_TIME = 1.0f;
        const float FADE_OUT_TIME = 2.0f;

        float easeFactor = 1.0f;
        if( m_distortion.state == Effect::ES_IN )
        {
            easeFactor = std::min(m_distortion.timer/FADE_IN_TIME, 1.0f);

            if( m_distortion.timer >= FADE_IN_TIME )
            {
                m_distortion.state = Effect::ES_ON;
            }
        }
        else if( m_distortion.state == Effect::ES_OUT )
        {
            easeFactor =  1 - std::min(m_distortion.timer/FADE_OUT_TIME, 1.0f) ;
            if( m_distortion.timer >= FADE_OUT_TIME )
            {
                m_distortion.state = Effect::ES_OFF;
            }
        }

        int alpha = (int)(100 + (1 + sin( tan(m_time * 8) + g_rng.GetDouble() ) ) * 0.5f * 155) * easeFactor;
        m_dimmingGradient->SetAlpha( alpha );

        m_dimmingColor.SetR( (1 + sin( m_time * 3 ) ) * 25 * easeFactor );
        m_dimmingColor.SetG( (1 + sin( m_time * 6 ) ) * 25 * easeFactor );
        m_dimmingColor.SetB( (1 + sin( m_time * 9 ) ) * 25 * easeFactor );

        m_distortion.timer += dt;
    }
}

void GameManager::StartFlashEffect()
{
    if( m_flash.state == Effect::ES_OFF  )
    {
        m_flash.state =  Effect::ES_IN;
        m_flash.timer = 0;
    }
}

void GameManager::StopFlashEffect()
{
    if( m_flash.state != Effect::ES_OFF )
    {
        m_flash.state = Effect::ES_OUT;
        m_flash.timer = 0;
    }
}

void GameManager::UpdateFlash( float dt )
{
    if( m_flash.state != Effect::ES_OFF )
    {
        const float FADE_IN_TIME = 0.1f;
        const float FADE_OUT_TIME = 0.5f;

        float progress = 1.0f;
        if( m_flash.state == Effect::ES_IN )
        {
            progress = std::min(m_flash.timer/FADE_IN_TIME, 1.0f);

            if( m_flash.timer >= FADE_IN_TIME )
            {
                m_flash.state = Effect::ES_ON;
            }
        }
        else if( m_flash.state == Effect::ES_OUT )
        {
            progress =  1 - std::min(m_flash.timer/FADE_OUT_TIME, 1.0f) ;
            if( m_flash.timer >= FADE_OUT_TIME )
            {
                m_flash.state = Effect::ES_OFF;
            }
        }

        m_flashColor.SetA( progress * 255.0f );
        m_flashColor.SetR( 255.0f );
        m_flashColor.SetG( 255.0f );
        m_flashColor.SetB( 255.0f );

        m_flash.timer += dt;
    }
}

void GameManager::Render( Claw::Surface* target )
{
    const Vectorf& offset = m_map->GetOffset();

    m_map->Render( target );

    if( m_renderObstacles )
    {
        m_obstacleManager->Render( target, offset );
    }

    m_pickupManager->Render( target, offset );
    m_entityManager->Render( target, offset );
    m_shotManager->Render( target, offset );
    m_explosionManager->Render( target, offset );
    m_particleSystem->Render( target, offset );

    if( m_nuke == 1 )
    {
        m_nukeShadow->SetAlpha( std::min( int( m_nukeTimer / s_nukeTime * 255 ), 255 ) );
        target->Blit( m_nukePos.x * s_gameScale - offset.x - m_nukeShadow->GetWidth() / 2, m_nukePos.y * s_gameScale - offset.y - m_nukeShadow->GetHeight() / 2, m_nukeShadow );
    }

    for( std::vector<AnimData>::iterator it = m_anims.begin(); it != m_anims.end(); ++it )
    {
        m_renderableManager->Add( it->s );
    }

    if( m_shotFlash > 0 && m_player )
    {
        m_shotFlashGfx->SetAlpha( m_shotFlash * 255 );
        target->BlitAdditive( m_player->GetPos().x * s_gameScale - offset.x - m_shotFlashGfx->GetWidth() / 2,
            m_player->GetPos().y * s_gameScale - offset.y - m_shotFlashGfx->GetHeight() / 2,
            m_shotFlashGfx );
    }

    m_renderableManager->Render( target, offset );

    m_explosionManager->RenderPost( target, offset );
    m_entityManager->RenderPost( target, offset );

    if( m_nuke == 1 )
    {
        target->Blit( m_nukePos.x * s_gameScale - offset.x - m_nukePayload->GetWidth() / 2,
                      m_nukePos.y * s_gameScale - offset.y - m_nukePayload->GetHeight() - ( 1 - m_nukeTimer / s_nukeTime ) * 2000 * s_gameScale,
                      m_nukePayload );
    }
    else if( m_nuke == 2 )
    {
        float l = 1 - m_nukeTimer / 6.f;
        target->DrawFilledRectangle( target->GetClipRect(), Claw::MakeRGBA( l * 4.5f, l * 3.2f, l * 1.5f, l * 1.8f ) );
    }

    RenderDistortion( target );
    RenderFlash( target );
}

void GameManager::RenderDistortion( Claw::Surface* target )
{
    if( m_distortion.state != Effect::ES_OFF )
    {
        Claw::TriangleEngine::BlitColor( target,
            m_dimmingGradient,
            0,
            0,
            0, 
            Vectorf( (float)(target->GetWidth()) / m_dimmingGradient->GetWidth(), (float)(target->GetHeight()) / m_dimmingGradient->GetHeight() ), 
            Vectorf( 0, 0 ),
            Claw::TriangleEngine::FM_NONE,
            m_dimmingGradient->GetClipRect(), 
            m_dimmingColor );
    }
}

void GameManager::RenderFlash( Claw::Surface* target )
{
    if( m_flash.state != Effect::ES_OFF )
    {
        target->DrawFilledRectangle( target->GetClipRect(), m_flashColor );
    }
}

void GameManager::RenderHeat( Claw::Surface* target )
{
    const Vectorf& offset = m_map->GetOffset();

    m_shotManager->RenderHeat( target, offset );
    m_explosionManager->RenderHeat( target, offset );

    if( m_nuke == 2 )
    {
        float l = 1 - m_nukeTimer / 6.f;

        int sx = g_rng.GetInt() % 64;
        int sy = g_rng.GetInt() % 64;

        m_heatNoise->SetAlpha( l * 128 );

        for( int y = -sy; y < target->GetHeight(); y += 64 )
        {
            for( int x = -sx; x < target->GetWidth(); x += 64 )
            {
                target->Blit( x, y, m_heatNoise );
            }
        }
    }
}

void GameManager::Focus( bool focus )
{
    if( !focus )
    {
        // Save game progress progess (ammo, inventory, etc.)
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();
    }

    if( !m_menuActive )
    {
        ShowPauseMenu( true );
    }
}

void GameManager::TouchDown( int x, int y, int button )
{
    if( m_menuActive )
    {
        if( Claw::TouchDevice::GetId( button ) == Claw::TDT_MAIN_TOUCH )
        {
            m_menu->OnTouchDown( x, y, button );
        }
        return;
    }

    if( m_touch )
    {
        const float pauseWidth = m_resolution.x * PAUSE_AREA_SCALE;
        const float pauseHeight = m_resolution.y * PAUSE_AREA_SCALE;

        if( x > (m_resolution.x - pauseWidth) * 0.5f &&
            x < (m_resolution.x + pauseWidth) * 0.5f &&
            y > (m_resolution.y - pauseHeight) * 0.5f &&
            y < (m_resolution.y + pauseHeight) * 0.5f )
        {
            Claw::Registry::Get()->Set( "/monstaz/settings/pausedone", true );
            Claw::Registry::Get()->Set( "/monstaz/settings/pausedone", true );
            ShowPauseMenu( true );
            return;
        }
        else if( y < 60 * s_gameScale && x > m_resolution.x / 2 )
        {
            m_lua->Call( "DoReload", 0, 0 );
        }
    }

    if( x > m_resolution.x - 50 * s_gameScale )
    {
        if( NukeAvailable() &&
            y > m_resolution.y / 2 - 95 * s_gameScale &&
            y < m_resolution.y / 2 - 45 * s_gameScale )
        {
            Nuke();
        }
        else if( AirstrikeAvailable() &&
            y > m_resolution.y / 2 - 45 * s_gameScale &&
            y < m_resolution.y / 2 + 5 * s_gameScale )
        {
            Airstrike();
        }
    }

    if( m_touch && m_displayControlsEnabled || Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH )
    {
        m_touch->OnTouchDown( x, y, button );
    }

    const int w = ((MonstazApp*)MonstazAI::MonstazAIApplication::GetInstance())->GetResolution().x;

    if( !m_touch )
    {
        Vectorf offset = GetMap()->GetOffset();
        x /= s_gameScale; 
        y /= s_gameScale; 
        offset.x /= s_gameScale;
        offset.y /= s_gameScale;

        switch( button )
        {
        case 0:
            m_lua->PushBool( true );
            m_lua->Call( "Shot", 1, 0 );
            break;
        case 1:
            m_lua->PushNumber( x + offset.x );
            m_lua->PushNumber( y + offset.y );
            m_lua->Call( "SpawnEntityUser", 2, 0 );
            break;
        case 2:
            FireGrenade();
            break;
        default:
            break;
        }
    }
}

void GameManager::TouchUp( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) == Claw::TDT_MAIN_TOUCH )
    {
        m_menu->OnTouchUp( x, y, button );
    }

    if( m_touch )
    {
        if( m_displayControlsEnabled || Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH )
        {
            m_touch->OnTouchUp( x, y, button );
        }
    }
    else
    {
        x /= s_gameScale; 
        y /= s_gameScale;

        switch( button )
        {
        case 0:
            m_lua->PushBool( false );
            m_lua->Call( "Shot", 1, 0 );
            m_tracking = NULL;
            break;
        default:
            break;
        }
    }
}

void GameManager::TouchMove( int x, int y, int button )
{
    if( Claw::TouchDevice::GetId( button ) == Claw::TDT_MAIN_TOUCH && m_menu->OnTouchMove( x, y, button ) || m_menuActive )
    {
        return;
    }

    if( m_touch )
    {
        if( m_displayControlsEnabled || Claw::TouchDevice::GetId( button ) != Claw::TDT_MAIN_TOUCH )
        {
            m_touch->OnTouchMove( x, y, button );
        }
    }
    else
    {
        Vectorf offset = GetMap()->GetOffset();
        x /= s_gameScale; 
        y /= s_gameScale; 
        offset.x /= s_gameScale;
        offset.y /= s_gameScale;

        switch( button )
        {
        case -1:
            if( !m_autoaim )
            {
                Entity* p = GetPlayer();
                if( p )
                {
                    Vectorf l( x + offset.x - p->GetPos()[0], y + offset.y - p->GetPos()[1] );
                    l.Normalize();
                    p->SetTarget( l );
                    GetEntityManager()->PlayerLookDirectionChanged();
                }
            }
            break;
        case 1:
            TouchDown( x * 2, y * 2, 1 );
            break;
        default:
            break;
        }
    }
}

void GameManager::RenderHud( Claw::Surface* target )
{
    const Vectorf& o = m_map->GetOffset();
    std::vector<Entity*>& e = m_entityManager->GetEntities();

    for( std::vector<Entity*>::iterator it = e.begin(); it != e.end(); ++it )
    {
        if( (*it)->GetClass() != Entity::Friendly ) continue;

        int offset;
        switch( (*it)->GetType() )
        {
        case Entity::Player:
            continue;
        case Entity::OctopusChaser:
        case Entity::OctopusFriend:
            offset = 37;
            break;
        case Entity::FishSimple:
        case Entity::FishSimpleNonExploding:
        case Entity::FishThrowing:
        case Entity::FloaterElectric:
            offset = 35;
            break;
        case Entity::FloaterSimple:
        case Entity::FloaterSower:
            offset = 29;
            break;
        case Entity::HoundShooting:
            offset = 50;
            break;
        default:
            offset = 31;
            break;
        }

        const Vectorf& p = (*it)->GetPos();
        target->Blit( p.x * s_gameScale - m_energyBar->GetWidth() / 2 - o.x, ( p.y - offset ) * s_gameScale - m_energyBar->GetHeight() / 2 - o.y, m_energyBar );
        float fill = s_gameScale == 1.5f ? 22 : 29;
        Claw::TriangleEngine::Blit( target, m_energyBarRed,
            ( p.x + 3.5f ) * s_gameScale - m_energyBar->GetWidth() / 2 - o.x,
            ( p.y - offset + 3 ) * s_gameScale - m_energyBar->GetHeight() / 2 - o.y,
            0, Vectorf( std::min( 1.f, (*it)->GetHitPoints() / (*it)->GetMaxHitPoints() ) * fill, 1 ), Vectorf( 0, 0 ) );
    }

    std::vector<Map::StaticObject*>& so = m_map->GetStaticObjects();
    for( std::vector<Map::StaticObject*>::iterator it = so.begin(); it != so.end(); ++it )
    {
        if( (*it)->m_marker )
        {
            CLAW_ASSERT( (*it)->IsIsoSet() );
            Map::StaticObjectIsoSet* iso = (Map::StaticObjectIsoSet*)*it;
            CLAW_ASSERT( iso->m_maxLife != 0 );

            const Vectorf& p = (*it)->GetPos();
            target->Blit( p.x * s_gameScale - m_energyBar->GetWidth() / 2 + iso->m_isoset->GetWidth() / 2 - o.x,
                p.y * s_gameScale - m_energyBar->GetHeight() / 2 - o.y, m_energyBar );
            float fill = s_gameScale == 1.5f ? 22 : 29;
            Claw::TriangleEngine::Blit( target, m_energyBarRed,
                ( p.x + 3.5f ) * s_gameScale - m_energyBar->GetWidth() / 2 + iso->m_isoset->GetWidth() / 2 - o.x,
                ( p.y + 3 ) * s_gameScale - m_energyBar->GetHeight() / 2 - o.y,
                0, Vectorf( iso->m_life * iso->m_rmaxLife * fill, 1 ), Vectorf( 0, 0 ) );
        }
    }

    if( m_touch && m_displayControlsEnabled )
    {
        m_touch->Render( target );
    }

    m_hud->Render( target );
    m_menu->Render( target );
}

void GameManager::SetResolution( int w, int h )
{
    m_resolution.x = w;
    m_resolution.y = h;

    m_map->SetResolution( w, h );
}

static const Claw::NarrowString s_lqexclude[] = {
    "city_blockade_01",
    "city_builing_deco_01",
    "city_builing_deco_02",
    "city_builing_deco_03",
    "city_builing_deco_04",
    "city_cables_01",
    "city_drain_01",
    "city_drain_02",
    "city_drain_03",
    "city_neon_03",
    "city_neon_04",
    "drain_01",
    "drain_02",
    "fence_A_02",
    "fence_A_03",
    "fence_A_04",
    "fence_B_02",
    "fence_B_03",
    "fence_B_04",
    "fence_C_02",
    "fence_C_03",
    "fence_C_04",
    "fence_D_02",
    "garbage_01",
    "garbage_02",
    "garbage_03",
    "garbage_04",
    "garbage_05",
    "garbage_07",
    "garbage_08",
    "garbage_09",
    "garbage_10",
    "garbage_11",
    "garbage_12",
    "garbage_13",
    "garbage_14",
    "garbage_15",
    "garbage_16",
    "garbage_17",
    "garbage_18",
    "garbage_19",
    "high_vol_03",
    "high_vol_04",
    "high_vol_05",
    "hole_06",
    "hole_07",
    "hole_08",
    "hole_09",
    "plant_01",
    "plant_02",
    "plant_03",
    "plant_04",
    "plant_05",
    "plant_06",
    "reklama_01",
    "reklama_02",
    "reklama_03",
    "reklama_04",
    "reklama_05",
    "reklama_06",
    "reklama_07",
    "reklama_08",
    "reklama_09",
    "reklama_10",
    "roses",
    "city_cooling_01",
    "city_transformer_02"
};

static int GetDestroyablesHpIncrease()
{
    int ret = 0;

    int level = Claw::Registry::Get()->CheckInt( "/monstaz/player/level" );
    int baselevel = Claw::Registry::Get()->CheckInt( "/app-config/destroyables/baselevel" );
    int hpinc = Claw::Registry::Get()->CheckInt( "/app-config/destroyables/hpinc" );

    if( level >= baselevel )
    {
        ret = ( 1 + level - baselevel ) * hpinc;
    }

    CLAW_MSG( "Destroyables HP increase: " << ret );

    return ret;
}

void GameManager::Load( const char* fn )
{
    m_mapPath = fn;
    Claw::XmlPtr xml( Claw::Xml::LoadFromFile( fn ) );
    Claw::XmlIt root( *xml );

    int width, height;
    root.GetAttribute( "width", width );
    root.GetAttribute( "height", height );

    AvoidMapBorders::SetMapSize( Vectorf( width, height ) );

    m_map->SetSize( width, height );

    const float MAP_BORDER = -GameManager::STS * (GameManager::SEG_BORDER - 1);
    m_mapMin.x = MAP_BORDER + Entity::AVERAGE_RADIUS;
    m_mapMin.y = MAP_BORDER + Entity::AVERAGE_RADIUS;
    m_mapMax.x = GetMap()->GetSize().x;
    m_mapMax.y = GetMap()->GetSize().y;
    m_mapMax -= m_mapMin;

    bool lowquality = Claw::Registry::Get()->CheckBool( "/monstaz/settings/lowquality" );

    int hpinc = GetDestroyablesHpIncrease();

    Claw::XmlIt layers = root.Child( "Layer" );
    while( layers )
    {
        Claw::NarrowString name;
        layers.GetAttribute( "name", name );

        if( name == "background" )
        {
            Claw::XmlIt el = layers.Child( "Image" );
            while( el )
            {
                Claw::NarrowString asset;
                int x, y;
                el.GetAttribute( "asset", asset );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );

                bool ok = true;
                if( lowquality )
                {
                    for( int i=0; i<sizeof( s_lqexclude ) / sizeof( Claw::NarrowString ); i++ )
                    {
                        if( strncmp( asset.c_str(), s_lqexclude[i].c_str(), s_lqexclude[i].size() ) == 0 )
                        {
                            ok = false;
                            break;
                        }
                    }
                }
                if( ok )
                {
                    Map::StaticObject* so = m_map->AddBackgroundObject( Claw::NarrowString( "gfx/assets/" ) + asset + "@linear", x, y );
                    so->m_anim.Reset( GetAnim( asset ) );
                }

                ++el;
            }
        }
        else if( name == "ground" )
        {
            Claw::XmlIt el = layers.Child( "Image" );
            while( el )
            {
                Claw::NarrowString asset;
                int x, y;
                el.GetAttribute( "asset", asset );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );

                bool ok = true;
                if( lowquality )
                {
                    for( int i=0; i<sizeof( s_lqexclude ) / sizeof( Claw::NarrowString ); i++ )
                    {
                        if( strncmp( asset.c_str(), s_lqexclude[i].c_str(), s_lqexclude[i].size() ) == 0 )
                        {
                            ok = false;
                            break;
                        }
                    }
                }
                if( ok )
                {
                    Map::StaticObject* so = m_map->AddBackgroundObject( Claw::NarrowString( "gfx/assets/" ) + asset + "@linear", x, y );
                    so->m_anim.Reset( GetAnim( asset ) );
                }

                ++el;
            }
        }
        else if( name == "static" )
        {
            Claw::XmlIt el = layers.Child( "Image" );
            while( el )
            {
                Claw::NarrowString asset;
                const char* msname = NULL;
                int x, y;
                float mslife = 0;
                int msoffset = 0;
                el.GetAttribute( "asset", asset );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );
                msname = el.GetAttribute( "msname" );
                el.GetAttribute( "mslife", mslife );
                el.GetAttribute( "msoffset", msoffset );

                if( mslife != 0 ) mslife += hpinc;

                bool ok = true;
                if( lowquality )
                {
                    for( int i=0; i<sizeof( s_lqexclude ) / sizeof( Claw::NarrowString ); i++ )
                    {
                        if( strncmp( asset.c_str(), s_lqexclude[i].c_str(), s_lqexclude[i].size() ) == 0 )
                        {
                            ok = false;
                            break;
                        }
                    }
                }
                if( ok )
                {
                    Map::StaticObject* so = m_map->AddStaticObject( asset, x, y, msname, mslife, msoffset );
                    so->m_anim.Reset( GetAnim( asset ) );

                    if( el.HasAttribute( "id" ) )
                    {
                        Claw::NarrowString id;
                        el.GetAttribute( "id", id );
                        if( id == "geiser" )
                        {
                            m_particleSystem->Add( new GeiserEmitter( m_geiserFunctor, m_particleSystem, x + 14, y + 7, 50, 20, 15 ) );
                        }
                    }
                    if( el.HasAttribute( "thrash" ) )
                    {
                        Claw::NarrowString tmp;
                        el.GetAttribute( "thrash", tmp );
                        if( tmp == "hydrant" )
                        {
                            so->m_thrash = Map::T_Hydrant;
                        }
                        else if( tmp == "atm" )
                        {
                            so->m_thrash = Map::T_ATM;
                        }
                        else if( tmp == "garbage" )
                        {
                            so->m_thrash = Map::T_Garbage;
                        }
                        else if( tmp == "glass" )
                        {
                            so->m_thrash = Map::T_Glass;
                        }
                        else if( tmp == "mailbox" )
                        {
                            so->m_thrash = Map::T_Mailbox;
                        }
                        else if( tmp == "newspaper" )
                        {
                            so->m_thrash = Map::T_Newspaper;
                        }
                        else if( tmp == "ufo" )
                        {
                            so->m_thrash = Map::T_UFO;
                        }
                        else if( tmp == "tractor" )
                        {
                            so->m_thrash = Map::T_Tractor;
                        }
                        else if( tmp == "boiler" )
                        {
                            so->m_thrash = Map::T_Boiler;
                        }
                        else if( tmp == "ci-puszka" )
                        {
                            so->m_thrash = Map::T_Puszka;
                        }
                        else if( tmp == "wall" )
                        {
                            so->m_thrash = Map::T_Wall;
                        }
                        else if( tmp == "jet" )
                        {
                            so->m_thrash = Map::T_Jet;
                        }
                        else if( tmp == "tower" )
                        {
                            so->m_thrash = Map::T_Tower;
                        }
                        else if( tmp == "titanic" )
                        {
                            so->m_thrash = Map::T_Titanic;
                        }
                        else if( tmp == "tank" )
                        {
                            so->m_thrash = Map::T_Tank;
                        }
                        else if( tmp == "base" )
                        {
                            so->m_thrash = Map::T_Base;
                        }
                        else if( tmp == "crates" )
                        {
                            so->m_thrash = Map::T_Crates;
                        }
                        else if( tmp == "wychodek" )
                        {
                            so->m_thrash = Map::T_Wychodek;
                        }
                        else if( tmp == "silo" )
                        {
                            so->m_thrash = Map::T_Silo;
                        }
                        else if( tmp == "silohalf" )
                        {
                            so->m_thrash = Map::T_SiloHalf;
                        }
                        else if( tmp == "silofull" )
                        {
                            so->m_thrash = Map::T_SiloFull;
                        }
                        else if( tmp == "periscope" )
                        {
                            so->m_thrash = Map::T_Periscope;
                        }
                        else if( tmp == "antenna" )
                        {
                            so->m_thrash = Map::T_Antenna;
                        }
                        else
                        {
                            CLAW_ASSERT( false );
                        }
                    }
                }

                ++el;
            }
        }
        else if( name == "colliders" )
        {
            Claw::XmlIt el = layers.Child( "Obstacle" );
            while( el )
            {
                Claw::NarrowString type;
                const char* msname = NULL;
                float x, y;
                Obstacle::Kind kind = Obstacle::Regular;
                Obstacle::Thrash thrash = Obstacle::T_None;
                el.GetAttribute( "type", type );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );
                msname = el.GetAttribute( "msname" );
                if( el.HasAttribute( "id" ) )
                {
                    Claw::NarrowString tmp;
                    el.GetAttribute( "id", tmp );
                    if( tmp == "ground" )
                    {
                        kind = Obstacle::Ground;
                    }
                    else if( tmp == "holo" )
                    {
                        kind = Obstacle::Holo;
                    }
                    else
                    {
                        CLAW_ASSERT( false );
                    }
                }
                if( el.HasAttribute( "thrash" ) )
                {
                    Claw::NarrowString tmp;
                    el.GetAttribute( "thrash", tmp );
                    if( tmp == "bush" )
                    {
                        thrash = Obstacle::T_Bush;
                    }
                    else if( tmp == "desert" )
                    {
                        thrash = Obstacle::T_Desert;
                    }
                    else if( tmp == "skull" )
                    {
                        thrash = Obstacle::T_Skull;
                    }
                    else if( tmp == "cactus" )
                    {
                        thrash = Obstacle::T_Cactus;
                    }
                    else if( tmp == "ice" )
                    {
                        thrash = Obstacle::T_Ice;
                    }
                    else if( tmp == "cthulhu" )
                    {
                        thrash = Obstacle::T_Cthulhu;
                    }
                    else
                    {
                        CLAW_ASSERT( false );
                    }
                }
                if( type == "circle" )
                {
                    float r;
                    el.GetAttribute( "r", r );
                    m_obstacleManager->AddObstacleCircle( x, y, r, kind, thrash, msname );
                }
                else if( type == "rectangle" )
                {
                    float ex, ey, p;
                    el.GetAttribute( "ex", ex );
                    el.GetAttribute( "ey", ey );
                    el.GetAttribute( "p", p );
                    m_obstacleManager->AddObstacleRectangle( x, y, Vectorf( ex, ey ), p, kind, thrash, msname );
                }
                else
                {
                    CLAW_ASSERT( false );
                }
                ++el;
            }
            m_obstacleManager->BuildTree();
        }
        else if( name == "triggers" )
        {
            Claw::XmlIt el = layers.Child( "Obstacle" );
            while( el )
            {
                Claw::NarrowString type, id;
                float x, y;
                el.GetAttribute( "type", type );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );
                el.GetAttribute( "id", id );
                if( type == "circle" )
                {
                    float r;
                    el.GetAttribute( "r", r );
                    m_triggerManager->AddTriggerCircle( id, x, y, r );
                }
                else if( type == "rectangle" )
                {
                    float ex, ey, p;
                    el.GetAttribute( "ex", ex );
                    el.GetAttribute( "ey", ey );
                    el.GetAttribute( "p", p );
                    m_triggerManager->AddTriggerRectangle( id, x, y, Vectorf( ex, ey ), p );
                }
                else
                {
                    CLAW_ASSERT( false );
                }
                ++el;
            }
        }
        else if( name == "waypoints" )
        {
            Claw::XmlIt el = layers.Child( "Obstacle" );
            while( el )
            {
                Claw::NarrowString type, id;
                float x, y;
                el.GetAttribute( "type", type );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );
                el.GetAttribute( "id", id );
                if( type == "circle" )
                {
                    float r;
                    el.GetAttribute( "r", r );
                    m_waypointManager->AddWaypointCircle( id, x, y, r );
                }
                else if( type == "rectangle" )
                {
                    float ex, ey, p;
                    el.GetAttribute( "ex", ex );
                    el.GetAttribute( "ey", ey );
                    el.GetAttribute( "p", p );
                    m_waypointManager->AddWaypointRectangle( id, x, y, Vectorf( ex, ey ), p );
                }
                else
                {
                    CLAW_ASSERT( false );
                }
                ++el;
            }
        }
        else if( name == "spawn" )
        {
            Claw::XmlIt el = layers.Child( "Obstacle" );
            while( el )
            {
                Claw::NarrowString id;
                float x, y, r;
                el.GetAttribute( "type", id );
                CLAW_MSG_ASSERT( id == "circle", "Rectangle spawn points not supported" );
                el.GetAttribute( "id", id );
                el.GetAttribute( "x", x );
                el.GetAttribute( "y", y );
                el.GetAttribute( "r", r );

                m_map->AddSpawnCircle( id, x, y, r );

                ++el;
            }
        }
        else
        {
            CLAW_MSG( "Map has unknown layer: " << name );
        }

        ++layers;
    }

    m_map->BuildNamedObjectsMap();

    Claw::Registry::Get()->Set( "/internal/catlevel", false );

    int len = strlen( fn );
    char* lua = new char[len + 1];
    strcpy( lua, fn );
    lua[len-3] = 'l';
    lua[len-2] = 'u';
    lua[len-1] = 'a';
    m_lua->Load( lua );
    delete[] lua;

    if( Claw::Registry::Get()->CheckBool( "/internal/catlevel" ) )
    {
        m_lua->Call( "SetupPerksCatLevel", 0, 0 );
    }
}

void GameManager::FinishSetup()
{
    m_player = m_entityManager->FindPlayerEntity();
    m_rage = new EffectRage( m_player, Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/rage_face.ani" ), Claw::AssetDict::Get<Claw::Surface>( "gfx/items/fx/rage_loop.ani" ), GetGameScale() );
    m_shieldFx = new EffectShield( m_player, m_shield[Shop::GetInstance()->GetUpgrades(Shop::Items::Shield)], GetGameScale() );
    m_player->AddEffect( m_shieldFx );
    m_player->AddEffect( m_rage );
    m_hud->ShowInventory( !m_touch || !m_displayControlsEnabled );
    m_menu->SetupTutorials();

    bool addFriend = false;
    Claw::Registry::Get()->Get( "/internal/friend", addFriend );
    if( addFriend )
    {
        Entity* e = m_entityManager->Add(
            m_player->GetPos().x + ( g_rng.GetDouble() * 10 + 5 ) * ( g_rng.GetDouble() < 0.5f ? 1 : -1 ),
            m_player->GetPos().y + ( g_rng.GetDouble() * 10 + 5 ) * ( g_rng.GetDouble() < 0.5f ? 1 : -1 ),
            Entity::AIFriend );
        e->SetClass( Entity::Friendly );
        e->AddEffect( new EffectNameplate( e, s_gameScale, Claw::Registry::Get()->CheckString( "/internal/friendName") ) );
        Claw::Lunar<Entity>::push( *m_lua, e );
        m_lua->RegisterGlobal( "AIFriend" );
        m_lua->Call("AISetWeapon", 0 , 0 );

        GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_LEVEL_WITH_FRIEND );
    }

    lua_getglobal( *m_lua, "Weapons" );
    lua_getglobal( *m_lua, "WeaponSelected" );
    lua_gettable( *m_lua, -2 );
    lua_pushstring( *m_lua, "Atlas" );
    lua_gettable( *m_lua, -2 );
    m_weaponSet = luaL_checknumber( *m_lua, -1 );
    lua_pop( *m_lua, 3 );
    lua_getglobal( *m_lua, "WeaponSelected" );
    m_lua->Call( "SelectWeapon", 1, 0 );

    GameEventParam eventParam = TutorialManager::GetInstance()->IsActive() ? GEP_LEVEL_STARTED_TUTORIAL :
                                m_survival ? GEP_LEVEL_STARTED_SURVIVAL : 
                                m_bossFight ? GEP_LEVEL_STARTED_BOSSFIGHT :
                                GEP_LEVEL_STARTED_STORY;
    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_LEVEL_STARTED, eventParam, m_mapPath );

    m_lua->Call( "Tick", 0, 0 );
}

void GameManager::MapSetup( Map::Type mapType )
{
    // Setup map dependend assets here
}

void GameManager::ShowPerkMenu( bool show )
{
    m_pauseMenu = false;

    if( show )
    {
        m_menu->StartPerkMenu();
    }

    m_timeController->Switch( 0.25f );
    SetMenuActive( show );
    VibraController::GetInstance()->Stop();

    KeysClear();
}

void GameManager::ShowPauseMenu( bool show )
{
    m_pauseMenu = show;

    if( show )
    {
        // Save progress
        ((MonstazAI::MonstazAIApplication*)Claw::AbstractApp::GetInstance())->Save();

        TutorialManager::GetInstance()->OnPause();
        m_menu->StartPauseMenu();
    }

    m_timeController->Switch( 0.25f );
    SetMenuActive( show );
    VibraController::GetInstance()->Stop();

    KeysClear();
}

void GameManager::TouchUpdate( const Vectorf& touch0, const Vectorf& touch1 )
{
    m_touchControl.m_offset0 = touch0;
    m_touchControl.m_offset1 = touch1;

    if( touch1.x == 0 && touch1.y == 0 )
    {
        m_tracking = NULL;
    }
}

void GameManager::TouchEnable( bool enable )
{
    m_touchControl.m_enabled = enable;
    m_lua->PushBool( enable );
    m_lua->RegisterGlobal( "TouchEnabled" );
}

void GameManager::DisplayControlsEnable( bool enable )
{
    if( m_displayControlsEnabled != enable )
    {
        m_displayControlsEnabled = enable;
        m_hud->ShowInventory( !enable );
        m_menu->SetupTutorials();
        if( m_touch )
        {
            m_touch->Reset();
        }
    }
}

const GameManager::TouchControl& GameManager::GetTouchControl() const
{
    return m_touchControl;
}

void GameManager::KeysUpdate( Claw::KeyCode code, bool pressed )
{
    KeysControl::Key key = KeysControl::KEY_NUM;
    if( code == Claw::KEY_W )       key = KeysControl::KEY_UP;
    else if( code == Claw::KEY_S )  key = KeysControl::KEY_DOWN;
    else if( code == Claw::KEY_A )  key = KeysControl::KEY_LEFT;
    else if( code == Claw::KEY_D )  key = KeysControl::KEY_RIGHT;

    CLAW_ASSERT( key < KeysControl::KEY_NUM );
    m_keysControl.m_key[key] = pressed;
}

void GameManager::KeyPressed( Claw::KeyCode code )
{
    if( m_menu )
        m_menu->KeyPress( code );
}

const GameManager::KeysControl& GameManager::GetKeysControl() const
{
    return m_keysControl;
}

void GameManager::KeysClear()
{
    for( int i = 0; i < KeysControl::KEY_NUM; ++i )
    {
        m_keysControl.m_key[i] = false;
    }
}

AnimationSet* GameManager::GetPlayerSet( int idx )
{
    if( !m_playerSet[idx] )
    {
        m_playerSet[idx].Reset( new AnimationSet( s_playerSetData[idx].directions, s_playerSetData[idx].frames, s_playerSetData[idx].path ) );
    }
    return m_playerSet[idx];
}

AnimationSet* GameManager::GetMonsterSet( int idx )
{
    if( !m_monsterSet[idx] )
    {
        m_monsterSet[idx].Reset( new AnimationSet( s_setData[idx].directions, s_setData[idx].frames, s_setData[idx].path ) );
    }
    return m_monsterSet[idx];
}

Claw::SurfacePtr GameManager::GetMonsterDeathFire(Entity::Type eType)
{
    switch( eType )
    {
    case Entity::OctopusSimple:
        if( !m_enemyDeathFire[0] ) m_enemyDeathFire[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/tentacler/Enemy1_Death_Fire_000.ani" ) );
        return m_enemyDeathFire[0];
    case Entity::OctopusShotAware:
        if( !m_enemyDeathFire[6] ) m_enemyDeathFire[6].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/tentacler3/Enemy1_Death_Fire_000.ani" ) );
        return m_enemyDeathFire[6];
    case Entity::OctopusChaser:
    case Entity::OctopusFriend:
        if( !m_enemyDeathFire[1] ) m_enemyDeathFire[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/tentacler2/Enemy1_Death_Fire_000.ani" ) );
        return m_enemyDeathFire[1];
    case Entity::SqueezerSimple:
        if( !m_enemyDeathFire[2] ) m_enemyDeathFire[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/squeezer/Enemy2_Death_000.ani" ) );
        return m_enemyDeathFire[2];
    case Entity::SqueezerTurning:
        if( !m_enemyDeathFire[3] ) m_enemyDeathFire[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/squeezer2/Enemy2_Death_000.ani" ) );
        return m_enemyDeathFire[3];
    case Entity::FishSimple:
    case Entity::FishSimpleNonExploding:
        if( !m_enemyDeathFire[4] ) m_enemyDeathFire[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fish/Enemy3_Death_000.ani" ) );
        return m_enemyDeathFire[4];
    case Entity::FishThrowing:
        if( !m_enemyDeathFire[5] ) m_enemyDeathFire[5].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/fish2/Enemy3_Death_000.ani" ) );
        return m_enemyDeathFire[5];
    case Entity::FloaterSimple:
        if( !m_enemyDeathFire[7] ) m_enemyDeathFire[7].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/floater/Enemy4_Death_000.ani" ) );
        return m_enemyDeathFire[7];
    case Entity::FloaterElectric:
        if( !m_enemyDeathFire[8] ) m_enemyDeathFire[8].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/floater2/Enemy5_Death_000.ani" ) );
        return m_enemyDeathFire[8];
    case Entity::HoundSimple:
        if( !m_enemyDeathFire[9] ) m_enemyDeathFire[9].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hound/Enemy6_Death_000.ani" ) );
        return m_enemyDeathFire[9];
    case Entity::HoundShooting:
        if( !m_enemyDeathFire[10] ) m_enemyDeathFire[10].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/hound2/Enemy7_Death_000.ani" ) );
        return m_enemyDeathFire[10];
    case Entity::SectoidSimple:
        if( !m_enemyDeathFire[11] ) m_enemyDeathFire[11].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/sectoid/GreyA_Death1_000.ani" ) );
        return m_enemyDeathFire[11];
    case Entity::KillerWhale:
        if( !m_enemyDeathFire[12] ) m_enemyDeathFire[12].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/killerwhale/KillerWhaleB_Death_000.ani" ) );
        return m_enemyDeathFire[12];
    case Entity::KillerWhaleSimple:
        if( !m_enemyDeathFire[13] ) m_enemyDeathFire[13].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/killerwhale2/KillerWhaleA_Death_000.ani" ) );
        return m_enemyDeathFire[13];
    case Entity::SectoidShooting:
        if( !m_enemyDeathFire[14] ) m_enemyDeathFire[14].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/sectoid2/GreyB_Death1_000.ani" ) );
        return m_enemyDeathFire[14];
    case Entity::Crab:
        if( !m_enemyDeathFire[15] ) m_enemyDeathFire[15].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/crab/Crab_Death_000.ani" ) );
        return m_enemyDeathFire[15];
    case Entity::MechaBoss:
        if( !m_enemyDeathFire[16] ) m_enemyDeathFire[16].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/mechaboss/MechaT_Death_000.ani" ) );
        return m_enemyDeathFire[16];
    case Entity::Lobster:
        if( !m_enemyDeathFire[17] ) m_enemyDeathFire[17].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/lobster/Lobster_Death_000.ani" ) );
        return m_enemyDeathFire[17];
    case Entity::NautilSimple:
        if( !m_enemyDeathFire[18] ) m_enemyDeathFire[18].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/nautil/Nautil_Death_000.ani" ) );
        return m_enemyDeathFire[18];
    case Entity::NautilTurning:
        if( !m_enemyDeathFire[19] ) m_enemyDeathFire[19].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/nautil2/NautilB_Death_000.ani" ) );
        return m_enemyDeathFire[19];
    case Entity::Nerval:
        if( !m_enemyDeathFire[20] ) m_enemyDeathFire[20].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/nerval/Nerval_Death_000.ani" ) );
        return m_enemyDeathFire[20];
    case Entity::SowerBoss:
        if( !m_enemyDeathFire[21] ) m_enemyDeathFire[21].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/sower/Sower_Death_000.ani" ) );
        return m_enemyDeathFire[21];
    case Entity::OctobrainBoss:
        if( !m_enemyDeathFire[22] ) m_enemyDeathFire[22].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/octobrain/Octobrain_Death_000.ani" ) );
        return m_enemyDeathFire[22];
    case Entity::OctobrainBossClone:
        if( !m_enemyDeathFire[23] ) m_enemyDeathFire[23].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/octobrain/Octobrain_DeathClone_000.ani" ) );
        return m_enemyDeathFire[23];
    case Entity::FloaterSower:
        if( !m_enemyDeathFire[24] ) m_enemyDeathFire[24].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/floater3/sowerchild_death_000.ani" ) );
        return m_enemyDeathFire[24];
    case Entity::AIFriend:
        return m_death[1];
    default:
        CLAW_ASSERT( false );
    }
    return Claw::SurfacePtr( NULL );
}

Claw::SurfacePtr GameManager::GetMonsterDeathShoot(Entity::Type eType, Vectorf look)
{
    char buf[128];
    switch( eType )
    {
    case Entity::OctopusSimple:
        if( g_rng.GetDouble() < 0.5f )
        {
            if( !m_enemyDeathShotSecond[0] ) m_enemyDeathShotSecond[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/tentacler/Enemy1_Death2_000.ani" ) );
            return m_enemyDeathShotSecond[0];
        }
        else
        {
            if( !m_enemyDeathShot[0][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/tentacler/Enemy1_Death_Shot_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[0][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[0][AnimationSet::TranslateFor8( look )];
        }
    case Entity::OctopusShotAware:
        if( g_rng.GetDouble() < 0.5f )
        {
            if( !m_enemyDeathShotSecond[2] ) m_enemyDeathShotSecond[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/tentacler3/Enemy1_Death2_000.ani" ) );
            return m_enemyDeathShotSecond[2];
        }
        else
        {
            if( !m_enemyDeathShot[6][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/tentacler3/Enemy1_Death_Shot_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[6][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[6][AnimationSet::TranslateFor8( look )];
        }
    case Entity::OctopusChaser:
    case Entity::OctopusFriend:
        if( g_rng.GetDouble() < 0.5f )
        {
            if( !m_enemyDeathShotSecond[1] ) m_enemyDeathShotSecond[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/tentacler2/Enemy1_Death2_000.ani" ) );
            return m_enemyDeathShotSecond[1];
        }
        else
        {
            if( !m_enemyDeathShot[1][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/tentacler2/Enemy1_Death_Shot_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[1][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[1][AnimationSet::TranslateFor8( look )];
        }
    case Entity::SqueezerSimple:
        if( !m_enemyDeathShot[2][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/squeezer/Enemy2_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[2][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[2][AnimationSet::TranslateFor8( look )];
    case Entity::SqueezerTurning:
        if( !m_enemyDeathShot[3][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/squeezer2/Enemy2_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[3][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[3][AnimationSet::TranslateFor8( look )];
    case Entity::FishSimple:
    case Entity::FishSimpleNonExploding:
        if( !m_enemyDeathShot[4][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/fish/Enemy3_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[4][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[4][AnimationSet::TranslateFor8( look )];
    case Entity::FishThrowing:
        if( !m_enemyDeathShot[5][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/fish2/Enemy3_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[5][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[5][AnimationSet::TranslateFor8( look )];
    case Entity::FloaterSimple:
        if( !m_enemyDeathShot[7][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/floater/Enemy4_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[7][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[7][AnimationSet::TranslateFor8( look )];
    case Entity::FloaterElectric:
        if( !m_enemyDeathShot[8][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/floater2/Enemy5_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[8][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[8][AnimationSet::TranslateFor8( look )];
    case Entity::HoundSimple:
        if( !m_enemyDeathShot[9][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/hound/Enemy6_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[9][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[9][AnimationSet::TranslateFor8( look )];
    case Entity::HoundShooting:
        if( !m_enemyDeathShot[10][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/hound2/Enemy7_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[10][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[10][AnimationSet::TranslateFor8( look )];
    case Entity::SectoidSimple:
        if( g_rng.GetDouble() < 0.5f )
        {
            if( !m_enemyDeathShot[11][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/sectoid/GreyA_Death1_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[11][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[11][AnimationSet::TranslateFor8( look )];
        }
        else
        {
            if( !m_enemyDeathShot[12][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/sectoid/GreyA_Death2_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[12][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[12][AnimationSet::TranslateFor8( look )];
        }
    case Entity::KillerWhale:
        if( !m_enemyDeathShot[13][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/killerwhale/KillerWhaleB_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[13][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[13][AnimationSet::TranslateFor8( look )];
    case Entity::KillerWhaleSimple:
        if( !m_enemyDeathShot[14][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/killerwhale2/KillerWhaleA_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[14][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[14][AnimationSet::TranslateFor8( look )];
    case Entity::SectoidShooting:
        if( g_rng.GetDouble() < 0.5f )
        {
            if( !m_enemyDeathShot[15][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/sectoid2/GreyB_Death1_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[15][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[15][AnimationSet::TranslateFor8( look )];
        }
        else
        {
            if( !m_enemyDeathShot[16][0] )
            {
                for( int i=0; i<8; i++ )
                {
                    sprintf( buf, "gfx/sectoid2/GreyB_Death2_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                    m_enemyDeathShot[16][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
                }
            }
            return m_enemyDeathShot[16][AnimationSet::TranslateFor8( look )];
        }
    case Entity::Crab:
        if( !m_enemyDeathShot[17][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/crab/Crab_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[17][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[17][AnimationSet::TranslateFor8( look )];
    case Entity::MechaBoss:
        if( !m_enemyDeathShot[18][0] )
        {
            m_enemyDeathShot[18][0].Reset( Claw::AssetDict::Get<Claw::Surface>( "gfx/mechaboss/MechaT_Death_000.ani" ) );
        }
        return m_enemyDeathShot[18][0];
    case Entity::Lobster:
        if( !m_enemyDeathShot[19][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/lobster/Lobster_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[19][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[19][AnimationSet::TranslateFor8( look )];
    case Entity::NautilSimple:
        if( !m_enemyDeathShot[20][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/nautil/Nautil_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[20][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[20][AnimationSet::TranslateFor8( look )];
    case Entity::NautilTurning:
        if( !m_enemyDeathShot[21][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/nautil2/NautilB_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[21][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[21][AnimationSet::TranslateFor8( look )];
    case Entity::Nerval:
        if( !m_enemyDeathShot[22][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/nerval/Nerval_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[22][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[22][AnimationSet::TranslateFor8( look )];
    case Entity::SowerBoss:
        if( !m_enemyDeathShot[23][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/sower/Sower_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[23][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[23][AnimationSet::TranslateFor8( look )];
    case Entity::OctobrainBoss:
        if( !m_enemyDeathShot[24][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/octobrain/Octobrain_Death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[24][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[24][AnimationSet::TranslateFor8( look )];
    case Entity::OctobrainBossClone:
        if( !m_enemyDeathShot[25][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/octobrain/Octobrain_DeathClone_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[25][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[25][AnimationSet::TranslateFor8( look )];
    case Entity::FloaterSower:
        if( !m_enemyDeathShot[26][0] )
        {
            for( int i=0; i<8; i++ )
            {
                sprintf( buf, "gfx/floater3/sowerchild_death_%s.ani", AnimationSet::GetFrameAngle8( i ) );
                m_enemyDeathShot[26][i].Reset( Claw::AssetDict::Get<Claw::Surface>( buf ) );
            }
        }
        return m_enemyDeathShot[26][AnimationSet::TranslateFor8( look )];
    case Entity::AIFriend:
        return m_death[1];
    default:
        CLAW_ASSERT( false );
    }
    return Claw::SurfacePtr( NULL );
}

void GameManager::GenerateSplatter( const Vectorf& pos, int num )
{
    float fscale = 1;
    float spread = 1;
    bool smallSplatter = num == 1;

    if( num > 5 )
    {
        spread *= 4;
    }

    if( m_slaughter )
    {
        num *= 2;
        spread *= 1.5f;
        fscale = 1.5f;
    }

    for( int i=0; i<num; i++ )
    {
        if( m_splatter[m_splatterCnt].g )
        {
            m_fadingSplatter.push_back( m_splatter[m_splatterCnt] );
        }

        m_splatter[m_splatterCnt].g.Reset( m_blood[ g_rng.GetInt() % 6 ] );
        m_splatter[m_splatterCnt].p.x = ( pos.x + spread * ( g_rng.GetDouble() * 16 - 8 ) ) * s_gameScale;
        m_splatter[m_splatterCnt].p.y = ( pos.y + spread * ( g_rng.GetDouble() * 16 - 8 ) ) * s_gameScale;
        m_splatter[m_splatterCnt].s = ( 0.65 + g_rng.GetDouble() * 0.5 ) * fscale;
        m_splatter[m_splatterCnt].a = g_rng.GetDouble() * M_PI * 2;
        m_splatter[m_splatterCnt].f = g_rng.GetInt() % 3;
        m_splatter[m_splatterCnt].d = g_rng.GetDouble() * 0.5f;

        if( smallSplatter )
        {
            m_splatter[m_splatterCnt].s *= 0.33;
        }

        m_splatterCnt = ( m_splatterCnt + 1 ) & ( SplatterSize - 1 );
    }
}

void GameManager::AddSplatter( Claw::Surface* g, Vectorf p, float s, float a )
{
    if( m_splatter[m_splatterCnt].g )
    {
        m_fadingSplatter.push_back( m_splatter[m_splatterCnt] );
    }

    m_splatter[m_splatterCnt].g.Reset( g );
    m_splatter[m_splatterCnt].p.x = p.x * s_gameScale;
    m_splatter[m_splatterCnt].p.y = p.y * s_gameScale;
    m_splatter[m_splatterCnt].s = s;
    m_splatter[m_splatterCnt].a = a;
    m_splatter[m_splatterCnt].f = 0;
    m_splatter[m_splatterCnt].d = 0;

    m_splatterCnt = ( m_splatterCnt + 1 ) & ( SplatterSize - 1 );
}

void GameManager::AddExplosionHole( const Vectorf& pos )
{
    AddSplatter( m_explosionHole, pos, 1, 0 );

    // also add explosion
    AddAnimation( m_explosion, pos );

    // and sound
    m_audioManager->Play3D( (AudioSfx)( SFX_EXPLOSION1 + g_rng.GetInt( 0, 2 ) ), pos );

    m_particleSystem->Add( (*m_exploFlare)( pos.x, pos.y, 0, 0 ) );
}

void GameManager::AddGlopRemains( const Vectorf& pos, Shot::Type shotType )
{
    const float scaleBase = 0.6f;
    const float scaleRand = scaleBase * 0.8f;
    const float spread = s_gameScale * 6.0f;
    const float timeBase = 0.0f;
    const float timeRange = 3.0f;
    const unsigned int numGlops = shotType == Shot::FishGlop ? sizeof(m_glop) / sizeof(m_glop[0]) : sizeof(m_spit) / sizeof(m_spit[0]);

    SplatterData splatter;
    if( shotType == Shot::FishGlop )
        splatter.g.Reset( m_glop[g_rng.GetInt() % numGlops ] );
    else
        splatter.g.Reset( m_spit[g_rng.GetInt() % numGlops ] );
    splatter.p.x = ( pos.x + (g_rng.GetDouble() * spread - 0.5f * spread ) ) * s_gameScale;
    splatter.p.y = ( pos.y + (g_rng.GetDouble() * spread - 0.5f * spread ) ) * s_gameScale;
    splatter.s = scaleBase + ( g_rng.GetDouble() * scaleRand - 0.5f * scaleRand );
    splatter.a = g_rng.GetDouble() * M_PI * 2;
    splatter.f = g_rng.GetInt() % 3;
    splatter.d = 0;
    // Smaller stains dry faster
    splatter.t = timeBase + (splatter.s * 6.0f); //timeRange * g_rng.GetDouble();
    splatter.invlt = 1 / splatter.t;

    m_fadingSplatter.push_back( splatter );

    m_playerLastGlopTracePos = pos;
    m_playerNextGlopTraceDist = s_gameScale * GLOP_SPAWN_SQR_DIST + s_gameScale * GLOP_SPAWN_SQR_DIST_RANGE * g_rng.GetDouble();
}

void GameManager::DrawSplatter( Claw::Surface* target, const Vectorf& offset )
{
    m_screenRect = Claw::Rect( offset.x - 50 * s_gameScale, offset.y - 50 * s_gameScale, target->GetWidth() + 100 * s_gameScale, target->GetHeight() + 100 * s_gameScale );

    Claw::Rect spriteRect;
    Vectorf spritePivot;
    Vectori spritePos;
    for( std::vector<SplatterData>::iterator it = m_fadingSplatter.begin(); it != m_fadingSplatter.end(); ++it )
    {
        spritePivot = Vectorf( it->g->GetSize() / 2 );
        spriteRect = it->g->GetClipRect();
        spriteRect.m_x += it->p.x - (int)spritePivot.x;
        spriteRect.m_y += it->p.y - (int)spritePivot.y;

        if( m_screenRect.IsIntersect( spriteRect ) )
        {
            it->g->SetAlpha( ( it->t * it->invlt ) * 255 );
            Claw::TriangleEngine::Blit(
                target,
                it->g,
                it->p.x - offset.x,
                it->p.y - offset.y,
                it->a,
                it->s,
                spritePivot,
                (Claw::TriangleEngine::FlipMode)it->f );
        }
    }

    for( int i=0; i<SplatterSize; i++ )
    {
        if( !m_splatter[i].g ) { break; }
        if( m_splatter[i].d > 0 ) { continue; }

        spritePivot = Vectorf( m_splatter[i].g->GetSize() / 2 );
        spriteRect = m_splatter[i].g->GetClipRect();
        spriteRect.m_x += m_splatter[i].p.x - (int)spritePivot.x;
        spriteRect.m_y += m_splatter[i].p.y - (int)spritePivot.y;

        if( m_screenRect.IsIntersect( spriteRect ) )
        {
            m_splatter[i].g->SetAlpha( 255 );
            Claw::TriangleEngine::Blit(
                target,
                m_splatter[i].g,
                m_splatter[i].p.x - offset.x,
                m_splatter[i].p.y - offset.y,
                m_splatter[i].a,
                m_splatter[i].s,
                spritePivot,
                (Claw::TriangleEngine::FlipMode)m_splatter[i].f );
        }
    }
}

float GameManager::WeaponBoost()
{
    m_weaponBoost = m_weaponBoostTime;

    m_lua->PushBool( true );
    m_lua->Call( "WeaponBoost", 1, 0 );

    return m_weaponBoost;
}

void GameManager::Nuke( bool free )
{
    if( !free )
    {
        CLAW_ASSERT( NukeAvailable() );
        m_nukeAvailable = false;
        Shop::GetInstance()->Use( Shop::Items::Nuke );
    }

    m_nuke = 1;
    m_nukePos = m_player->GetPos();

    AudioManager::GetInstance()->Play( SFX_SIREN );

    NukeAnim();
}

void GameManager::NukeAnim()
{
    AddAnimation( m_nukeLaunch, m_player->GetPos() + Vectorf( 0, 80 ) + s_nukeLaunch[AnimationSet::TranslateFor16( m_player->GetLook() )], Vectorf( 0, -1 ) );
}

void GameManager::Airstrike()
{
    if( !AirstrikeAvailable() ) return;

    m_particleSystem->Add( m_bomber->operator()( m_player->GetPos().x, m_player->GetPos().y, 0 ) );
    m_particleSystem->Add( m_bomber->operator()( m_player->GetPos().x, m_player->GetPos().y, 1 ) );
    m_particleSystem->Add( m_bomber->operator()( m_player->GetPos().x, m_player->GetPos().y, 2 ) );

    Shop::GetInstance()->Use( Shop::Items::Airstrike );
    m_airstrikeAvailable = Shop::GetInstance()->CheckOwnership( Shop::Items::Airstrike ) > 0;
    m_airstrikeTimer = s_airstrikeTime;

    AudioManager::GetInstance()->Play( SFX_JET );
}

void GameManager::HoundAnim( Entity* e, bool even )
{
    AddAnimation( m_nukeLaunch, e->GetPos() + Vectorf( 0, 80 ) + s_houndShot[even?0:1][AnimationSet::TranslateFor8( e->GetLook() )], Vectorf( 0, -1 ) );
}

void GameManager::LobsterAnim( Entity* e, bool even )
{
    AddAnimation( m_nukeLaunch, e->GetPos() + Vectorf( 0, 10 ) + s_lobsterShot[even?0:1][AnimationSet::TranslateFor8( e->GetLook() )], Vectorf( 0, -1 ) );
}

void GameManager::FireGrenade()
{
    if( Shop::GetInstance()->CheckOwnership( Shop::Items::Grenade ) != 0 )
    {
        m_lua->Call( "FireGrenade", 0, 0 );
        TutorialManager::GetInstance()->OnFireGrenade();
    }
}

void GameManager::PlaceMine()
{
    if( Shop::GetInstance()->CheckOwnership( Shop::Items::Mine ) != 0 )
    {
        m_lua->Call( "PlaceMine", 0, 0 );
        TutorialManager::GetInstance()->OnPlaceMine();
    }
}

void GameManager::EnableShield()
{
    if( m_playerShield <= 0 && Shop::GetInstance()->CheckOwnership( Shop::Items::Shield ) > 0 )
    {
        m_playerShield = m_shieldTime;
        m_shieldFx->SetActive( true );
        Shop::GetInstance()->Use( Shop::Items::Shield, true );
    }
}

void GameManager::HealthKit( float gain )
{
    float maxhp = m_stats->GetMaxPlayerHP();
    m_player->AddEffect( new EffectHealth( m_player, m_healthAnim, m_healthGlow, m_healthParticle, GetGameScale(), maxhp, gain ) );
    GameManager::GetInstance()->GetAudioManager()->Play( SFX_HEALTH );

    if( Claw::Registry::Get()->CheckBool( "/internal/catlevel" ) )
    {
        std::vector<Entity*>& ents = m_entityManager->GetEntities();
        for( std::vector<Entity*>::iterator it = ents.begin(); it != ents.end(); ++it )
        {
            if( (*it)->GetType() != Entity::OctopusFriend ) continue;

            float maxhp = (*it)->GetMaxHitPoints();
            (*it)->AddEffect( new EffectHealth( *it, m_healthAnim, m_healthGlow, m_healthParticle, GetGameScale(), maxhp, gain ) );
            GameManager::GetInstance()->GetAudioManager()->Play3D( SFX_HEALTH, (*it)->GetPos() );
        }
    }
}

void GameManager::UseHealthKit()
{
    if( !IsPlayerMaxHP() && Shop::GetInstance()->Use( Shop::Items::HealthKit ) )
    {
        HealthKit( Shop::GetInstance()->CheckOwnership( "medkitbonus" ) == 0 ? 0.3f : 0.5f );
    }
}

bool GameManager::IsPlayerMaxHP() const
{
    return m_player->GetHitPoints() == m_stats->GetMaxPlayerHP();
}

void GameManager::CrabDigIn( const Vectorf& pos )
{
    AddSplatter( m_crabDigIn[g_rng.GetInt()%3], pos, 1, 0 );
}

float GameManager::ApplyShotHitMultiplier( Shot::Type shotType, Entity::Type entType, float val ) const
{
    if( entType == Entity::Player )
    {
        switch( shotType )
        {
        case Shot::Saw:
            if( m_stats->CheckPerk( Perks::ToughSkin ) )
            {
                return 0.1f * val;
            }
            else
            {
                return 0.5f * val;
            }
        case Shot::FishGlop:
            return 4.0f * val;
        default:
            break;
        }
    }
    return val;
}

AnimSurfWrapPtr GameManager::AddAnimation( Claw::Surface* anim, const Vectorf& pos, bool leave )
{
    AnimSurfWrapPtr ptr( new AnimSurfWrap( (Claw::AnimatedSurface*)anim, pos ) );
    m_anims.push_back( AnimData( ptr, leave ) );
    return ptr;
}

AnimSurfWrapPtr GameManager::AddAnimation( Claw::Surface* anim, const Vectorf& pos, const Vectorf& look, bool leave )
{
    AnimSurfWrapPtr ptr( new AnimSurfWrap( (Claw::AnimatedSurface*)anim, pos, look ) );
    m_anims.push_back( AnimData( ptr, leave ) );
    return ptr;
}

void GameManager::KilledEnemy( Entity* entity )
{
    if( entity->GetType() == Entity::OctopusFriend )
    {
        entity->SetHitPoints( 100000 );
        //entity->SetState( SRunAway );
        return;
    }
    else if( entity->GetType() == Entity::OctobrainBossClone )
    {
        m_particleSystem->Add( new ExplosionEmitter( m_cloneDieFlare, m_particleSystem, entity->GetPos().x, entity->GetPos().y, 0, 0, 0.1f, 10 ) );
    }

    Claw::Surface* gfx = NULL;

    switch( entity->GetLastHit() )
    {
    case Shot::Vortex:
        m_particleSystem->Add( new ExplosionEmitter( m_bloodSplat, m_particleSystem, entity->GetPos().x, entity->GetPos().y, 200, 200, 0.1f, 100 ) );
        break;
    case Shot::Bullet:
    case Shot::Minigun:
    case Shot::Shell:
    case Shot::CombatShell:
    case Shot::Railgun:
    case Shot::FishGlop:
    case Shot::SowerSpit:
    case Shot::Saw:
    case Shot::Lurker:
        gfx = GetMonsterDeathShoot( entity->GetType(), entity->GetLook() );
        break;
    default:
        gfx = GetMonsterDeathFire( entity->GetType() );
        break;
    }

    if( gfx )
    {
        gfx->SetAlpha( entity->GetAlpha() );
        AddAnimation( gfx, entity->GetPos(), entity->GetType() != Entity::OctobrainBossClone );
    }

    m_stats->IncreaseKills();

    entity->Die();

    m_lua->Call( "MonsterKilled", 0, 0 );

    if( m_tracking == entity )
    {
        m_tracking = NULL;
    }

    TutorialManager::GetInstance()->OnEnemyKilled( entity );

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_ENEMY_KILLED, 1, "", entity );
}

void GameManager::KilledPlayer( Entity* entity )
{
    CLAW_ASSERT( entity == m_player );

    AddAnimation( m_death[0], entity->GetPos(), false );
    m_deathBody.Reset( new AnimSurfWrap( (Claw::AnimatedSurface*)m_deathLastFrame[0].GetPtr(), entity->GetPos() ) );
    m_deathBodyWhite.Reset( new AnimSurfWrap( (Claw::AnimatedSurface*)m_deathLastFrame[1].GetPtr(), entity->GetPos() + Vectorf( 0, 0.1f ) ) );

    entity->Die();
    m_hud->PlayerDied();

    m_player = NULL;
}

bool GameManager::CheckRicochetLastHit()
{
    if( m_ricochetLastHit <= 0 )
    {
        m_ricochetLastHit = 0.1f + g_rng.GetDouble() * 0.05f;
        return true;
    }
    else
    {
        return false;
    }
}

int GameManager::l_CalculateShotAvoidance( lua_State* L )
{
    Claw::Lua lua( L );

    float srf = lua.CheckNumber( 1 );

    const ShotManager::Shots& shots = m_shotManager->GetShots();
    std::vector<Entity*>& ents = m_entityManager->GetEntities();

    for( std::vector<Entity*>::iterator eit = ents.begin(); eit != ents.end(); ++eit )
    {
        (*eit)->GetShotAvoidance() = Vectorf( 0, 0 );
    }

    // Calculate how many segments are affected by shot avoidance range (round up result).
    const int SegmentRadius = SEGMENT_SEARCH_RADIUS + (int)(srf / STS); 

    for( ShotManager::ShotsConstIt sit = shots.begin(); sit != shots.end(); ++sit )
    {
        const Vectorf& shotPos = (*sit)->GetPos();
        const Vectorf& shotDir = (*sit)->GetDir();

        Vectori segPos( Segmentize( shotPos[0] ), Segmentize( shotPos[1] ) );

        Entity* entList = *( m_segmentTable + segPos[0] + segPos[1] * STS );

        for( int i=segPos[0]-SegmentRadius; i<=segPos[0]+SegmentRadius; i++ )
        {
            for( int j=segPos[1]-SegmentRadius; j<=segPos[1]+SegmentRadius; j++ )
            {
                entList = *( m_segmentTable + i + j * STS );
                for( ; entList ; entList = entList->m_nextSeg )
                {
                    const Vectorf& entityPos = entList->GetPos();
                    Vectorf v( entityPos - shotPos );

                    if( (*sit)->GetType() == Shot::HoundShot )
                    {
                        if( v.LengthSqr() < 0.001 ) continue;

                        v.Normalize();
                        entList->GetShotAvoidance() += v;
                    }
                    else
                    {
                        // Shot goes away ?
                        if( DotProduct( shotDir, v ) < 0 )
                        {
                            continue;
                        }

                        // Get shot tangent vector (perpendicular to its direction)
                        Vectorf shotTangent = (*sit)->GetTangent();
                        // Project offset vector between entity and shot onto tangent to get distance
                        // between shot and entity in tangent direction
                        float dist = DotProduct( shotTangent, entityPos - shotPos );
                        // Safety check
                        if( abs( dist ) < 0.001 )
                        {
                            dist = 0.001;
                        }
                        entList->GetShotAvoidance() += shotTangent * srf / dist;
                    }
                }
            }
        }
    }

    return 0;
}

int GameManager::l_CalculateAvoidance( lua_State* L )
{
    Claw::Lua lua( L );
    const std::vector<Entity*>& ents = m_entityManager->GetEntities();

    float rep = lua.CheckNumber( 1 );
    float rf = lua.CheckNumber( 2 );
    float rep2 = rep * rep;

    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        Entity *currEnt = (*it);
        if( currEnt->GetBehavior() == Entity::Human )
        {
            continue;
        }

        const Vectorf& e = currEnt->GetPos();
        Vectori s( Segmentize( e[0] ), Segmentize( e[1] ) );

        Entity* otherEnt = *( m_segmentTable + s[0] + s[1]*STS );
        if( !otherEnt ) continue;

        if( otherEnt == currEnt )
        {
            *( m_segmentTable + s[0] + s[1]*STS ) = otherEnt->m_nextSeg;
        }
        else if( otherEnt->m_nextSeg )
        {
            Entity* prev = otherEnt;
            otherEnt = otherEnt->m_nextSeg;
            while( otherEnt )
            {
                if( otherEnt == currEnt )
                {
                    prev->m_nextSeg = otherEnt->m_nextSeg;
                    break;
                }
                prev = otherEnt;
                otherEnt = otherEnt->m_nextSeg;
            }
        }

        const int SegmentRadius = SEGMENT_SEARCH_RADIUS;

        for( int i=s[0]-SegmentRadius; i<=s[0]+SegmentRadius; i++ )
        {
            for( int j=s[1]-SegmentRadius; j<=s[1]+SegmentRadius; j++ )
            {
                otherEnt = *( m_segmentTable + i + j*STS );
                while( otherEnt )
                {
                    if( currEnt->GetClass() == otherEnt->GetClass() )
                    {
                        const Vectorf& f = otherEnt->GetPos();
                        Vectorf v( e.x - f.x, e.y - f.y );
                        float l = DotProduct( v, v );
                        if( l < rep2 /* dupa */ )
                        {
                            v *= rf / l;
                            currEnt->GetAvoidance() += v;
                            otherEnt->GetAvoidance() -= v;
                        }
                    }
                    otherEnt = otherEnt->m_nextSeg;
                }
            }
        }
    }

    return 0;
}

int GameManager::l_CalculateObstacles( lua_State* L )
{
    Claw::Lua lua( L );
    const std::vector<Entity*>& ents = m_entityManager->GetEntities();

    float repulsionDist = lua.CheckNumber( 1 );
    float repulsionForce = lua.CheckNumber( 2 );
    float repulsionDist2 = repulsionDist * repulsionDist;

    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        // Entities that collide with obstacles doesn't use avoidance force
        if( (*it)->MayCollideObstacles() )
            continue;

        Entity::Type type = (*it)->GetType();

        // Query obstacles in "repulsionDist" area range
        const Vectorf& entPos = (*it)->GetPos();
        Scene::CollisionQuery* query = m_obstacleManager->QueryCollision( entPos, repulsionDist );

        (*it)->SetAvoidanceAbility( 0.0f );

        // Iterate obstacles in the area to calculate avoidance
        for( std::vector<Obstacle*>::const_iterator oit = query->GetColliders().begin(); oit != query->GetColliders().end() ; ++oit )
        {
            if( (*oit)->GetKind() == Obstacle::Holo || ( ( type == Entity::FloaterSimple || type == Entity::FloaterElectric || type == Entity::FloaterSower ) && (*oit)->GetKind() == Obstacle::Ground ) )
            {
                continue;
            }

            if( (*oit)->GetType() == Obstacle::Circle )
            {
                ObstacleCircle* cir = (ObstacleCircle*)(*oit);
                const Vectorf& cirPos = cir->GetPos();
                float cirRad = cir->GetRadius();
                float cirRad2 = cirRad * cirRad;
                Vectorf off = entPos - cirPos;
                float dist = DotProduct( off, off );
                // Entity actually colliding obstacle (there is intersection)
                if( dist < cirRad2 )
                {
                    dist = sqrt( dist );
                    off /= dist;
                    (*it)->SetAvoidanceAbility( 1.0f );
                    // Add repulsion force to prevent further movement
                    (*it)->GetObstacleAvoidance() += off * repulsionForce;
                    //if( (*it)->MayCollideObstacles() ) (*it)->CollideObstacle( off );
                }
                // Close but not colliding - accumulate repulsion force
                else
                {
                    float ro = cirRad + repulsionDist;
                    cirRad2 = ro * ro;
                    if( dist < cirRad2 )
                    {
                        dist = sqrt( dist );
                        off /= dist;
                        dist = ( ro - dist ) / repulsionDist;
                        (*it)->SetAvoidanceAbility( dist );
                        dist *= repulsionForce;
                        (*it)->GetObstacleAvoidance() += off * dist;
                    }
                }
            }
            else
            {
                ObstacleRectangle* rect = (ObstacleRectangle*)(*oit);
                const Vectorf& rectPos = rect->GetPos();
                Vectorf rectEdg = rect->GetEdge();
                Vectorf rectPerp = rect->GetPerp();

                Vectorf off = entPos - rectPos;

                float dp1 = DotProduct( off, rectEdg );
                float dp2 = DotProduct( off, rectPerp );

                bool OnEdge = dp1 >= 0 && dp1 <= DotProduct( rectEdg, rectEdg );
                bool OnPerp = dp2 >= 0 && dp2 <= DotProduct( rectPerp, rectPerp );

                // Entity actually intersects obstacle (in contact)
                if( OnEdge && OnPerp )
                {
                    Vectorf c( off - ( rectEdg + rectPerp ) * 0.5f );
                    // Add repulsion to prevent futher movement
                    c.Normalize();
                    (*it)->SetAvoidanceAbility( 1.0f );
                    (*it)->GetObstacleAvoidance() += c * repulsionForce;
                    //if( (*it)->MayCollideObstacles() ) (*it)->CollideObstacle( c );
                }
                else if( OnEdge )
                {
                    Vectorf n( rectEdg.y, -rectEdg.x );
                    n.Normalize();
                    float d = DotProduct( off, n );
                    float d2 = -d - rect->GetPerpLen();
                    if( d > 0 && d < repulsionDist )
                    {
                        float ll = repulsionDist / d;
                        (*it)->SetAvoidanceAbility( ll );
                        ll *= 0.1f;
                        ll *= repulsionForce;
                        (*it)->GetObstacleAvoidance() += n * ll;
                    }
                    else if( d2 > 0 && d2 < repulsionDist )
                    {
                        float ll = repulsionDist / d2;
                        (*it)->SetAvoidanceAbility( ll );
                        ll *= 0.1f;
                        ll *= repulsionForce;
                        (*it)->GetObstacleAvoidance() -= n * ll;
                    }
                }
                else if( OnPerp )
                {
                    Vectorf n( -rectPerp.y, rectPerp.x );
                    n.Normalize();
                    float d = DotProduct( off, n );
                    float d2 = -d - rect->GetEdgeLen();
                    if( d > 0 && d < repulsionDist )
                    {
                        float ll = repulsionDist / d;
                        (*it)->SetAvoidanceAbility( ll );
                        ll *= 0.1f;
                        ll *= repulsionForce;
                        (*it)->GetObstacleAvoidance() += n * ll;
                    }
                    else if( d2 > 0 && d2 < repulsionDist )
                    {
                        float ll = repulsionDist / d2;
                        (*it)->SetAvoidanceAbility( ll );
                        ll *= 0.1f;
                        ll *= repulsionForce;
                        (*it)->GetObstacleAvoidance() -= n * ll;
                    }
                }
                else
                {
                    if( dp1 < 0 )
                    {
                        if( dp2 > 0 )
                        {
                            off -= rectPerp;
                        }
                    }
                    else
                    {
                        if( dp2 < 0 )
                        {
                            off -= rectEdg;
                        }
                        else
                        {
                            off -= rectEdg + rectPerp;
                        }
                    }

                    Vectorf n( off );
                    float l = n.Normalize();
                    if( l < repulsionDist )
                    {
                        float ll = repulsionDist / l;
                        (*it)->SetAvoidanceAbility( ll );
                        ll *= 0.1f;
                        ll *= repulsionForce;
                        (*it)->GetObstacleAvoidance() += n * ll;
                    }
                }
            }
        }
    }
    return 0;
}

int GameManager::l_ProcessExplosions( lua_State* L )
{
    const std::vector<Entity*>& ents = m_entityManager->GetEntities();
    const std::vector<Explosion*>& expl = m_explosionManager->GetExplosions();

    for( std::vector<Explosion*>::const_iterator it = expl.begin(); it != expl.end(); ++it )
    {
        Explosion* e = *it;
        float r2 = e->m_radius * e->m_radius;
        float dist, distSqr;
        for( std::vector<Entity*>::const_iterator eit = ents.begin(); eit != ents.end(); ++eit )
        {
            const Vectorf& p = (*eit)->GetPos();
            distSqr = DotProduct( p, e->m_pos );
            if( distSqr < r2 )
            {
                GenerateSplatter( p, 1 );
            }
        }
    }
    return 0;
}

int GameManager::l_AvoidLineOfSight( lua_State* L )
{
    Claw::Lua lua( L );

    Entity* e = Claw::Lunar<Entity>::check( L, 1 );
    float angleLimit = lua.CheckNumber( 2 );
    float avoidanceForce = lua.CheckNumber( 3 );

    Vectorf off( AvoidLineOfSight( *e, angleLimit, avoidanceForce ) );

    lua.PushNumber( off.x  );
    lua.PushNumber( off.y );
    return 2;
}

Vectorf GameManager::AvoidLineOfSight( const Entity& e, float angleLimit, float avoidanceForce ) const
{
    if( !m_player ) return Vectorf(0,0);

    const Vectorf& ep = e.GetPos();
    const Vectorf& pp = m_player->GetPos();
    const Vectorf& pl = m_player->GetLook();

    Vectorf off( ep - pp );
    float len = off.Length();

    if( DotProduct( pl, off ) < angleLimit * len )
    {
        return Vectorf( 0.0f, 0.0f );
    }

    Vectorf lookPerp( pl.y, -pl.x );
    float l = DotProduct( lookPerp, off );
    if( abs( l ) < 0.001 )
    {
        l = 0.001;
    }

    float f = avoidanceForce / l;
    return lookPerp * f;
}

int GameManager::l_ProcessShots( lua_State* L )
{
    // This must be smaller then map segmentation margin see Segmentize() (STS and SEG_BORDER)
    const int MapBorder = 50 * s_gameScale;

    Claw::Lua lua( L );

    m_sheathedHitMultiplier = lua.CheckNumber( 1 );

    ShotManager::Shots& shots = m_shotManager->GetShots();
    ShotManager::ShotsIt it = shots.begin();

    const Vectori& mapSize = m_map->GetSize();
    Claw::Rect viewRect( m_map->GetOffset().x - MapBorder, m_map->GetOffset().y - MapBorder,
                        GetResolution().x + 2 * MapBorder, GetResolution().y + 2 * MapBorder );

    viewRect.m_x /= s_gameScale;
    viewRect.m_y /= s_gameScale;
    viewRect.m_w /= s_gameScale;
    viewRect.m_h /= s_gameScale;

    while( it != shots.end() )
    {
        bool remove = false;
        Shot* s = *it;
        Shot::Type type = s->GetType();
        Vectorf oldpos = s->GetPos();

        int steps = s->GetSteps();
        float speed = s->GetSpeed() / steps;
        float sdt = 1/( 60.f * steps );

        while( !remove && steps-- )
        {
            s->GetPos() += s->GetDir() * speed;
            if( s->GetLife() >= 0 )
            {
                s->SetLife( s->GetLife() - 1 );
            }

            if( s->GetPos().x < -MapBorder || s->GetPos().y < -MapBorder ||
                s->GetPos().x > mapSize.x + MapBorder || s->GetPos().y > mapSize.y + MapBorder || s->GetLife() == 0 )
            {
                remove = true;
            }

            if( !remove && type != Shot::Mine && type != Shot::HoundShot && ( type != Shot::Grenade || ((GrenadeShot*)s)->m_z < 32 ) && ( type != Shot::SowerEgg || ((EggShot*)s)->m_z < 32 ) && ( type != Shot::Airstrike || ((HoundShot*)s)->TimeLeft() < 0.15f ) )
            {
                Obstacle* o = m_obstacleManager->QueryCollision( s->GetPos() );
                if( o )
                {
                    if( o->GetKind() == Obstacle::Regular || ( type == Shot::Lurker && o->GetKind() != Obstacle::Holo ) )
                    {
                        float dmg = s->GetDamage();
                        if( type == Shot::Lurker )
                        {
                            dmg *= 10;
                        }

                        const char* oname = o->GetName();
                        if( oname != NULL )
                        {
                            DamageObject( o, dmg );
                        }
                        Obstacle::Thrash thrash = o->GetThrash();
                        const Vectorf& sp = s->GetPos();
                        const Vectorf& sd = s->GetDir();
                        switch( thrash )
                        {
                        case Obstacle::T_None:
                            break;
                        case Obstacle::T_Bush:
                            if( g_rng.GetDouble() < 0.01f )
                            {
                                m_particleSystem->Add( new GibParticle( sp.x, sp.y, -sd.x * 150.f, -sd.y * 150.f, 20, m_bushRat, m_shadow ) );
                            }
                            else
                            {
                                m_particleSystem->Add( new BushParticle( sp.x, sp.y,
                                    ( -sd.x + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    ( -sd.y + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    0, m_bushThrash[g_rng.GetInt( 4 )], m_shadow ) );
                            }
                            break;
                        case Obstacle::T_Desert:
                            {
                                Vectorf dir = sd * 10.f;
                                dir.Rotate( g_rng.GetDouble( -0.5f, 0.5f ) );
                                m_particleSystem->Add( new ExplosionParticle( sp.x, sp.y,
                                    ( -dir.x + ( g_rng.GetDouble() - 0.5f ) ) * 10.f,
                                    ( -dir.y + ( g_rng.GetDouble() - 0.5f ) ) * 10.f,
                                    1024, m_dustThrash[g_rng.GetInt( 7 )], false ) );
                                m_particleSystem->Add( new BushParticle( sp.x, sp.y,
                                    ( -sd.x + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    ( -sd.y + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    -1, m_stoneThrash[g_rng.GetInt( 12 )], m_shadow ) );
                            }
                            break;
                        case Obstacle::T_Skull:
                            m_particleSystem->Add( new BushParticle( sp.x, sp.y,
                                ( -sd.x + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                ( -sd.y + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                -1, m_skullThrash[g_rng.GetInt( 12 )], m_shadow ) );
                            break;
                        case Obstacle::T_Cactus:
                            m_particleSystem->Add( new BushParticle( sp.x, sp.y,
                                ( -sd.x + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                ( -sd.y + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                -0.5f, m_cactusThrash[g_rng.GetInt( 5 )], m_shadow ) );
                            break;
                        case Obstacle::T_Ice:
                            {
                                Vectorf dir = sd * 10.f;
                                dir.Rotate( g_rng.GetDouble( -0.5f, 0.5f ) );
                                m_particleSystem->Add( new ExplosionParticle( sp.x, sp.y,
                                    ( -dir.x + ( g_rng.GetDouble() - 0.5f ) ) * 10.f,
                                    ( -dir.y + ( g_rng.GetDouble() - 0.5f ) ) * 10.f,
                                    1024, m_iceDust[g_rng.GetInt( 6 )], false ) );
                                m_particleSystem->Add( new BushParticle( sp.x, sp.y,
                                    ( -sd.x + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    ( -sd.y + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    -1, m_iceThrash[g_rng.GetInt( 11 )], m_shadow ) );
                            }
                            break;
                        case Obstacle::T_Cthulhu:
                            {
                                Vectorf dir = sd * 10.f;
                                dir.Rotate( g_rng.GetDouble( -0.5f, 0.5f ) );
                                m_particleSystem->Add( new ExplosionParticle( sp.x, sp.y,
                                    ( -dir.x + ( g_rng.GetDouble() - 0.5f ) ) * 10.f,
                                    ( -dir.y + ( g_rng.GetDouble() - 0.5f ) ) * 10.f,
                                    1024, m_iceDust[g_rng.GetInt( 6 )], false ) );
                                m_particleSystem->Add( new BushParticle( sp.x, sp.y,
                                    ( -sd.x + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    ( -sd.y + ( g_rng.GetDouble() - 0.5f ) ) * 50.f,
                                    -1, m_clothThrash[g_rng.GetInt( 4 )], m_shadow ) );
                            }
                            break;
                        default:
                            CLAW_ASSERT( false );
                            break;
                        }

                        ShotHit( s, false );
                        switch( type )
                        {
                        case Shot::Saw:
                            if( !((RipperShot*)s)->ShotHit( o ) )
                            {
                                remove = true;
                            }
                            break;
                        case Shot::Vortex:
                            ((VortexShot*)s)->ShotHit( o );
                            break;
                        case Shot::FishGlop:
                        case Shot::SowerSpit:
                        case Shot::Plasma:
                            GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_PLASMA_HIT ), s->GetPos() );
                            remove = true;
                            break;
                        case Shot::Bullet:
                        case Shot::Minigun:
                        case Shot::Railgun:
                        case Shot::Shell:
                        case Shot::CombatShell:
                            if( GameManager::GetInstance()->CheckRicochetLastHit() )
                            {
                                GameManager::GetInstance()->GetAudioManager()->Play3D( AudioSfx( SFX_RICOCHET1 + g_rng.GetInt() % 7 ), s->GetPos() );
                            }
                            // fallthrough
                        default:
                            remove = true;
                        };
                    }
                    else if( o->GetKind() == Obstacle::Holo )
                    {
                        CLAW_ASSERT( o->GetName() );
                        const std::vector<Map::StaticObject*>& v = m_map->GetNamedObject( o->GetName() );

                        for( std::vector<Map::StaticObject*>::const_iterator vit = v.begin(); vit != v.end(); ++vit )
                        {
                            (*vit)->m_holo = std::min( 1.f, (*vit)->m_holo + 0.1f );
                        }
                    }
                }
            }

            if( !remove )
            {
                if( type == Shot::Spiral )
                {
                    ((SpiralShot*)s)->Step( sdt );
                    m_particleSystem->Add( (*m_spiralTrail)( s->GetPos().x, s->GetPos().y - 5 * s_gameScale, 0, 0 ) );
                }

                // Calculate collisions with dynamic objects placed in segments map (player, enemies, etc).
                if( type != Shot::HoundShot && ( type != Shot::Grenade || ((GrenadeShot*)s)->m_z < 32 ) && ( type != Shot::Mine || ((Mine*)s)->GetState() == Mine::S_LIVE ) && ( type != Shot::SowerEgg || ((EggShot*)s)->m_z < 32 ) && ( type != Shot::Airstrike || ((HoundShot*)s)->TimeLeft() < 0.15f ) )
                {
                    Vectori sv( Segmentize( s->GetPos().x ), Segmentize( s->GetPos().y ) );
                    Entity* closest = NULL;
                    float cdist = 8192;

                    const Entity* owner = s->GetOwner();
                    if( !m_entityManager->IsValid( owner ) )
                    {
                        owner = NULL;
                    }

                    int area = 1;
                    if( type == Shot::Vortex )
                    {
                        area = 2;
                    }

                    for( int i=sv.x-area; i<=sv.x+area; i++ )
                    {
                        for( int j=sv.y-area; j<=sv.y+area; j++ )
                        {
                            Entity* seg = *( m_segmentTable + i + j*STS );
                            for( ; seg; seg = seg->m_nextSeg )
                            {
                                if( !seg->m_draw )
                                    continue;
                                if( !( seg->GetClass() != Entity::Friendly || ( owner && owner->GetClass() != Entity::Friendly ) || type == Shot::Mine ) )
                                    continue;
                                if( owner && owner->GetType() == Entity::OctobrainBossClone && seg->GetClass() == owner->GetClass() )
                                    continue;
                                if( seg == owner )
                                    continue;
                                if( type == Shot::Mine && seg->GetType() == Entity::OctopusFriend )
                                    continue;

                                Vectorf p = seg->GetPos() - s->GetPos();
                                float l = DotProduct( p, p );
                                if( type == Shot::Vortex && l < 128*128 && l > 1 )
                                {
                                    bool collide = false;
                                    Scene::CollisionQuery* query = GameManager::GetInstance()->GetObstacleManager()->QueryCollision( seg->GetPos(), sqrt(seg->m_entityRadius) );
                                    const std::vector<Obstacle*>& v = query->GetColliders();
                                    for( std::vector<Obstacle*>::const_iterator oit = v.begin(); oit != v.end(); ++oit )
                                    {
                                        if( (*oit)->GetKind() != Obstacle::Holo )
                                        {
                                            collide = true;
                                            break;
                                        }
                                    }
                                    if( !collide )
                                    {
                                        float l = p.Normalize();
                                        p *= ( 128 - l ) / 24;
                                        seg->m_forced -= p;
                                    }
                                }
                                float radius = seg->m_entityRadius;
                                if( type == Shot::Lurker )
                                {
                                    radius = sqrt( radius ) + 32;
                                    radius = radius * radius;
                                }
                                else if( type == Shot::Mine )
                                {
                                    radius = sqrt( radius ) + 16;
                                    radius = radius * radius;
                                }
                                if( l < radius )
                                {
                                    if( seg->IsInviolable( s ) )
                                        continue;

                                    if( seg->m_invulnerable )
                                    {
                                        ShotHit( s, false );
                                        remove = true;
                                        goto done;
                                    }

                                    if( type == Shot::Mine )
                                    {
                                        ((Mine*)s)->Countdown();
                                        goto done;
                                    }

                                    bool bleed = seg->CanBleed();
                                    bool shield = ( m_entityManager->GetData( seg->GetType() ).shield & ~s->GetElements() ) != 0;

                                    // Do not hurt player in the game end sequence
                                    if( !(m_gameEnd && seg->GetType() == Entity::Player) )
                                    {
                                        float dmg = s->GetDamage();
                                        if( shield )
                                        {
                                            dmg *= 0.1f;
                                            seg->ElementHit();
                                        }
                                        seg->Hit( type, s->GetElements(), dmg );

                                        if( seg->GetType() == Entity::Player )
                                        {
                                            m_hud->PlayerDamaged( dmg );
                                        }
                                    }

                                    s->SetLastHit( seg );

                                    // Hitting rollers does not cause blood splatter but should look more like an obstacle hit
                                    if( seg->IsSheathed( type ) )
                                    {
                                        ShotHit( s, false );
                                    }
                                    else
                                    {
                                        if( bleed )
                                        {
                                            GenerateSplatter( seg->GetPos(), 1 );
                                        }
                                        ShotHit( s, bleed && !shield, bleed );
                                    }
                                    switch( type )
                                    {
                                    case Shot::Vortex:
                                        break;
                                    case Shot::Railgun:
                                        s->IncKills();
                                        break;
                                    case Shot::Saw:
                                        if( !((RipperShot*)s)->ShotHit( seg ) )
                                        {
                                            remove = true;
                                        }
                                        goto done;
                                    case Shot::FishGlop:
                                    case Shot::SowerSpit:
                                        if( seg->GetType() == Entity::Player )
                                        {
                                            remove = true;
                                        }
                                        goto done;
                                    case Shot::Lurker:
                                        break;
                                    default:
                                        remove = true;
                                        goto done;
                                    }
                                }
                                else if( type == Shot::Rocket && l < cdist && ( owner && owner->GetClass() == Entity::Friendly ) )
                                {
                                    cdist = l;
                                    closest = seg;
                                }
                            }
                        }
                    }

                    if( closest )
                    {
                        Vectorf dir = closest->GetPos() - s->GetPos();
                        dir.Normalize();
                        dir *= 0.4f;
                        Vectorf newdir = s->GetDir() + dir;
                        newdir.Normalize();
                        s->SetDir( newdir );
                    }
                }
done:;
            }
        }

        if( remove )
        {
            it = m_shotManager->RemoveShot( it );
        }
        else
        {
            switch( type )
            {
            case Shot::Flamer:
            case Shot::Plasma:
            case Shot::Grenade:
            case Shot::SowerEgg:
            case Shot::Mine:
            case Shot::Spiral:
            case Shot::HoundShot:
            case Shot::Lurker:
                break;
            default:
                m_shotManager->AddTrail( s->GetType(), oldpos, s->GetPos(), s->IsRage() );
                break;
            }
            ++it;
        }
    }

    return 0;
}

int GameManager::l_FillSegmentTable( lua_State* L )
{
    memset( m_segmentTable, 0, STS*STS*sizeof(Entity*) );
    const std::vector<Entity*>& ents = m_entityManager->GetEntities();

    for( std::vector<Entity*>::const_iterator it = ents.begin(); it != ents.end(); ++it )
    {
        Entity** seg = m_segmentTable + Segmentize((*it)->GetPos()[0]) + Segmentize((*it)->GetPos()[1])*STS;
        (*it)->GetAvoidance() = Vectorf( 0, 0 );;
        (*it)->GetObstacleAvoidance() = Vectorf( 0, 0 );
        (*it)->m_nextSeg = *seg;
        *seg = *it;
    }

    return 0;
}

int GameManager::l_MonstersEatPlayer( lua_State* L )
{
    Claw::Lua lua( L );

    // Do not hit player in the end sequence
    if( IsGameEnd() )
        return 0;

    bool reduced = lua.CheckBool( 1 );

    Vectorf& pp = m_player->GetPos();
    Vectori s( Segmentize( pp.x ), Segmentize( pp.y ) );

    // Remove player entity from segment table.
    // IMPORTANT: This must be the first function that uses the segment table, as
    //            other ones rely on player entity not being in the table.
    Entity* seg = *( m_segmentTable + s[0] + s[1]*STS );
    if( seg == m_player )
    {
        *( m_segmentTable + s[0] + s[1]*STS ) = seg->m_nextSeg;
    }
    else
    {
        Entity* prev = seg;
        seg = seg->m_nextSeg;
        while( seg )
        {
            if( seg == m_player )
            {
                prev->m_nextSeg = seg->m_nextSeg;
                break;
            }
            prev = seg;
            seg = seg->m_nextSeg;
        }
    }

    if( m_player->m_invulnerable ) return 0;

    const int SegmentRadius = SEGMENT_SEARCH_RADIUS;

    for( int i=s[0]-SegmentRadius; i<=s[0]+SegmentRadius; i++ )
    {
        for( int j=s[1]-SegmentRadius; j<=s[1]+SegmentRadius; j++ )
        {
            Entity* seg = *( m_segmentTable + i + j*STS );
            while( seg )
            {
                if( seg->IsStateSet( SSqueezerShocked ) || seg->GetClass() != Entity::Enemy || !seg->m_draw || seg->IsHarmless() )
                {
                    seg = seg->m_nextSeg;
                    continue;
                }

                Vectorf f = seg->GetPos();
                Vectorf v = pp - f;
                float l = DotProduct( v, v );
                if( l < seg->m_entityRadius * 8 )
                {
                    PlayerUnderAttack();
                    float damage = m_entityManager->GetData( seg->GetType() ).damage;
                    if( reduced )
                    {
                        damage *= 0.33f;
                    }
                    if( m_playerShield == 0 )
                    {
                        const float hp = m_player->GetHitPoints();
                        m_player->SetHitPoints( hp - damage );
                        m_hud->PlayerDamaged( damage );
                    }

                    if( m_playerLastHit <= 0  )
                    {
                        if( m_playerShield > 0 )
                        {
                            m_audioManager->Play3D( AudioSfx( SFX_SHIELD_HIT1 + g_rng.GetInt() % 2 ), seg->GetPos() );
                        }
                        else
                        {
                            GenerateSplatter( pp, 1 );
                            m_audioManager->Play3D( AudioSfx( SFX_PLAYER_HIT1 + g_rng.GetInt() % 7 ), seg->GetPos() );
                        }
                        VibraController::GetInstance()->StartVfx( VibraController::VFX_PLAYER_HIT );
                        m_playerLastHit = 0.3f + g_rng.GetDouble() * 0.15f;
                    }
                }

                seg = seg->m_nextSeg;
            }
        }
    }

    return 0;
}

int GameManager::l_MonstersEatFriends( lua_State* L )
{
    if( IsGameEnd() )
        return 0;

    std::vector<Entity*>& ents = m_entityManager->GetEntities();

    for( std::vector<Entity*>::iterator it = ents.begin(); it != ents.end(); ++it )
    {
        if( (*it)->GetClass() != Entity::Friendly ) continue;

        Vectorf& pp = (*it)->GetPos();
        Vectori s( Segmentize( pp.x ), Segmentize( pp.y ) );

        const int SegmentRadius = SEGMENT_SEARCH_RADIUS;

        for( int i=s[0]-SegmentRadius; i<=s[0]+SegmentRadius; i++ )
        {
            for( int j=s[1]-SegmentRadius; j<=s[1]+SegmentRadius; j++ )
            {
                Entity* seg = *( m_segmentTable + i + j*STS );
                while( seg )
                {
                    if( seg->IsStateSet( SSqueezerShocked ) || seg->GetClass() == Entity::Friendly || !seg->m_draw || seg->IsHarmless() )
                    {
                        seg = seg->m_nextSeg;
                        continue;
                    }

                    Vectorf f = seg->GetPos();
                    Vectorf v = pp - f;
                    float l = DotProduct( v, v );
                    if( l < seg->m_entityRadius * 8 )
                    {
                        float damage = m_entityManager->GetData( seg->GetType() ).damage;
                        m_hud->PlayerDamaged( damage * 0.15f );
                        (*it)->Hit( Shot::EnemyHit, 0, damage );
                    }

                    seg = seg->m_nextSeg;
                }
            }
        }
    }

    return 0;
}

int GameManager::l_CheckObstacleCollision( lua_State* L )
{
    Claw::Lua lua( L );
    Obstacle::Kind o = CheckObstacleCollision( Vectorf( lua.CheckNumber( 1 ), lua.CheckNumber( 2 ) ) );
    lua.PushBool( o == Obstacle::Regular || o == Obstacle::Ground );
    return 1;
}

int GameManager::l_SummaryScreen( lua_State* L )
{
    Claw::Lua lua( L );

    bool fail = lua.CheckBool( 1 );
    float ticks = lua.CheckNumber( 2 );

    Claw::Registry::Get()->Set( "/internal/storytime", ticks/60.0f );

    int level;
    Claw::Registry::Get()->Get( "/internal/storylevel", level );

    if( !fail )
    {
        int eventsCompletedValue = 0;
        Claw::Registry::Get()->Get( "/monstaz/player/eventsCompleted", eventsCompletedValue );
        eventsCompletedValue += 1;
        Claw::Registry::Get()->Set( "/monstaz/player/eventsCompleted", eventsCompletedValue );
    }

    int currValue = 0;
    Claw::Registry::Get()->Get( "/monstaz/player/kills", currValue );
    currValue += m_stats->GetKills();
    Claw::Registry::Get()->Set( "/monstaz/player/kills", currValue );

    lua.PushNumber( m_stats->GetPoints() );
    lua.PushBool( fail );
    lua.Call( "ReportPoints", 2, 0 );

    TutorialManager::GetInstance()->OnLevelSummary( fail );

    m_menu->StartSummary(
        m_stats->GetLevel(),
        ticks / 60.f,
        m_stats->GetKills(),
        m_stats->GetMaxMultiplier(),
        m_stats->GetPoints(),
        fail
        );
    SetMenuActive( true );
    m_gameEnd = true;
    m_gameEndFail = fail;
    VibraController::GetInstance()->Stop();
    m_touchControl.Reset();

    GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_LEVEL_FINISHED, m_gameEndFail ? GEP_LEVEL_FINISHED_FAIL : GEP_LEVEL_FINISHED_SUCCESS );

    return 0;
}

int GameManager::l_SetPlayerHP( lua_State* L )
{
    Claw::Lua lua( L );

    m_stats->GetMaxPlayerHP() = lua.CheckNumber( 1 );

    return 0;
}

int GameManager::l_NextPerkLevel( lua_State* L )
{
    Claw::Lua lua( L );
    m_stats->AddPerkLevel( lua.CheckNumber( 1 ) );
    return 0;
}

int GameManager::l_StartStoryTutorial( lua_State* L )
{
    Claw::Lua lua( L );
    m_menu->StartStoryTutorial( lua.CheckString( 1 ) );
    m_timeController->Switch( 0.25f );
    SetMenuActive( true );
    return 0;
}

int GameManager::l_Analytics( lua_State* L )
{
    // TODO: Remove
    return 0;
}

int GameManager::l_GetClosestEnemy( lua_State* L )
{
    if( m_tracking )
    {
        Claw::Lunar<Entity>::push( L, m_tracking );
        return 1;
    }

    const Vectorf& pp = m_player->GetPos();

    std::vector<Entity*>& e = m_entityManager->GetEntities();
    Entity* ent = NULL;
    float dist = 256*256;

    for( std::vector<Entity*>::const_iterator it = e.begin(); it != e.end(); ++it )
    {
        if( (*it)->GetClass() == Entity::Friendly || !(*it)->m_draw || (*it)->IsHarmless() )
        {
            continue;
        }

        Vectorf v( pp - (*it)->GetPos() );
        float d = v.LengthSqr();
        if( d < dist )
        {
            dist = d;
            ent = *it;
        }
    }

    if( ent )
    {
        m_tracking = ent;
        Claw::Lunar<Entity>::push( L, ent );
        return 1;
    }
    else
    {
        return 0;
    }
}

// TODO target collision objects, not gfx assets
int GameManager::l_GetClosestTarget( lua_State* L )
{
    const std::vector<Map::StaticObject*>& so = m_map->GetStaticObjects();
    const Vectorf& pp = m_player->GetPos();

    Map::StaticObject* o = NULL;
    Vectorf p;
    float dist = 256*256;
    for( std::vector<Map::StaticObject*>::const_iterator it = so.begin(); it != so.end(); ++it )
    {
        if( !(*it)->m_marker ) continue;
        CLAW_ASSERT( (*it)->IsIsoSet() );
        Map::StaticObjectIsoSet* iso = (Map::StaticObjectIsoSet*)*it;

        Vectorf pos( iso->GetPos() );
        pos += Vectorf( iso->m_isoset->GetWidth() * 0.5f, iso->m_isoset->GetHeight() * 0.75f ) / s_gameScale;

        Vectorf dv( pos - pp );
        float d = DotProduct( dv, dv );
        if( d < dist )
        {
            o = iso;
            dist = d;
            p = pos;
        }
    }

    if( o )
    {
        m_lua->PushNumber( p.x );
        m_lua->PushNumber( p.y );
        return 2;
    }

    return 0;
}

int GameManager::l_SetShieldTime( lua_State* L )
{
    Claw::Lua lua( L );
    m_shieldTime = lua.CheckNumber( 1 );
    return 0;
}

int GameManager::l_PlayerChangedWeapon( lua_State* L )
{
    Claw::Lua lua( L );
    m_weaponSet = lua.CheckNumber( 1 );
    m_player->SetAnimSet( Entity::AS_MOVE, GetPlayerSet( m_weaponSet ) );
    m_mech = m_weaponSet == 18 || m_weaponSet == 19;
    m_entityManager->SetPlayerAnimationSpeed( m_mech ? 15 : 6 );
    m_player->m_entityRadius = m_mech ? 30*30 : 12*12;
    m_player->m_shadowScale = m_mech ? 1.75f : 1;
    m_shieldFx->SetLocalScale( m_mech ? 2 : 1 );
    return 0;
}

int GameManager::l_AddVPadWeapon( lua_State* L )
{
    Claw::Lua lua( L );
    Claw::SurfacePtr w( GetWeaponHUDAsset( lua.CheckCString( 1 ) ) );
    CLAW_ASSERT( w );
    if( m_touch )
    {
        m_touch->AddWeapon( w );
    }
    return 0;
}

int GameManager::l_VPadWeaponSwitch( lua_State* L )
{
    if( m_touch )
    {
        m_touch->WeaponSwitch();
    }
    return 0;
}

int GameManager::l_StartRevive( lua_State* L )
{
    SetMenuActive( true );
    m_menu->StartRevive();
    return 0;
}

void GameManager::HoundFire()
{
    if( m_player )
    {
        Vectorf p = m_player->GetPos() + m_player->GetDir() * GameManager::GetInstance()->GetEntityManager()->GetData(Entity::Player).moveSpeed * (float)g_rng.GetDouble() * 100.f;
        p.x += g_rng.GetDouble() * 20 - 10;
        p.y += g_rng.GetDouble() * 20 - 10;
        m_particleSystem->Add( (*m_targetIndicator)( p.x, p.y, 0, 0 ) );

        m_lua->PushNumber( p.x );
        m_lua->PushNumber( p.y );
        m_lua->Call( "HoundShot", 2, 0 );
    }
}

void GameManager::LobsterFire()
{
    if( m_player )
    {
        Vectorf p = m_player->GetPos() + m_player->GetDir() * GameManager::GetInstance()->GetEntityManager()->GetData(Entity::Player).moveSpeed * ((float)g_rng.GetDouble() * 25  + 25);
        p.x += g_rng.GetDouble() * 10 - 5;
        p.y += g_rng.GetDouble() * 10 - 5;
        m_particleSystem->Add( (*m_targetIndicator)( p.x, p.y, 0, 0 ) );

        m_lua->PushNumber( p.x );
        m_lua->PushNumber( p.y );
        m_lua->Call( "LobsterShot", 2, 0 );
    }
}

void GameManager::ShotHit( Shot* shot, bool bloodyHit, bool leaveImpact )
{
    Vectorf pos = shot->GetPos() - Vectorf( 0, 10 );

    switch( shot->GetType() )
    {
    case Shot::Vortex:
        break;
    case Shot::Grenade:
        ((GrenadeShot*)shot)->Explode();
        break;
    case Shot::SowerEgg:
        break;
    case Shot::Rocket:
        {
        Explosion::Params p( 0.2f, 1.5f, 400, 4 );
        m_explosionManager->Add( shot->GetPos(), p, false, Shot::Rocket );
        AddExplosionHole( shot->GetPos() );
        }
        break;
    case Shot::Plasma:
        {
        Vectorf unit( 1, 0 );
        unit.Rotate( g_rng.GetDouble() * M_PI * 2 );
        AddAnimation( m_impactPlasma, pos, unit, leaveImpact );
        }
        break;
    case Shot::Railgun:
        if( bloodyHit )
        {
            for( int i=0; i<(m_slaughter?2:1); i++ )
            {
                AddAnimation( m_impactBlood[g_rng.GetInt()%4], pos, shot->GetDir() * float( 1.5f + 0.75f * g_rng.GetDouble() ) );
                AddAnimation( m_impactBlood[g_rng.GetInt()%4], pos + shot->GetDir() * 15.f, shot->GetDir() * float( 1.5f + 0.75f * g_rng.GetDouble() ) * -1.f, leaveImpact );
            }
        }
        else
        {
            AddAnimation( m_impactShot[g_rng.GetInt()%2], pos, shot->GetDir(), leaveImpact );
        }
        break;
    case Shot::FishGlop:
        AddAnimation( m_impactFishGlop, pos, shot->GetDir(), leaveImpact );
        if( bloodyHit )
        {
            AddAnimation( m_impactBlood[g_rng.GetInt()%4], pos, shot->GetDir() * float( 1.5f + 0.75f * g_rng.GetDouble() ) );
        }
        break;
    case Shot::SowerSpit:
        AddAnimation( m_impactSowerSpit, shot->GetPos(), shot->GetDir(), leaveImpact );
        if( bloodyHit )
        {
            AddAnimation( m_impactBlood[g_rng.GetInt()%4], shot->GetPos(), shot->GetDir() * float( 1.5f + 0.75f * g_rng.GetDouble() ) );
        }
        break;
    case Shot::Spiral:
        {
        Vectorf unit( 1, 0 );
        unit.Rotate( g_rng.GetDouble() * M_PI * 2 );
        AddAnimation( m_impactSpiral, pos, unit, leaveImpact );
        }
        break;
    case Shot::Lurker:
        break;
    default:
        if( bloodyHit )
        {
            for( int i=0; i<(m_slaughter?2:1); i++ )
            {
                Vectorf ro( -4 + g_rng.GetDouble() * 8, -4 + g_rng.GetDouble() * 8 );
                AddAnimation( m_impactBlood[g_rng.GetInt()%4], pos + ro, shot->GetDir() * float( 1.f + 0.5f * g_rng.GetDouble() ) );
                AddAnimation( m_impactBlood[g_rng.GetInt()%4], pos + ro + shot->GetDir() * 15.f, shot->GetDir() * float( 1.f + 0.5f * g_rng.GetDouble() ) * -1.f, leaveImpact );
            }
        }
        else
        {
            AddAnimation( m_impactShot[g_rng.GetInt()%2], pos, shot->GetDir(), leaveImpact );
        }
    }
}

AnimSurfWrapPtr GameManager::AddHitAnimation( const Vectorf& pos, const Vectorf& vel, bool useBlood, bool leave /* = false */ )
{
    if( useBlood )
        return AddAnimation( m_impactBlood[g_rng.GetInt()%4], pos, vel, leave );
    else
        return AddAnimation( m_impactShot[g_rng.GetInt()%2], pos, vel, leave );
}

Claw::Surface* GameManager::GetAnim( const Claw::NarrowString& base )
{
    Claw::SurfacePtr ret;
    Claw::NarrowString animfilename( "gfx/assets/" + base.substr( 0, base.length() - 3 ) + "ani" );
    Claw::FilePtr animfile( Claw::OpenFile( animfilename ) );
    if( animfile )
    {
        Claw::NarrowString animfilename2( "gfx/assets/" + base.substr( 0, base.length() - 4 ) + "_b.ani" );
        Claw::FilePtr animfile2( Claw::OpenFile( animfilename2 ) );
        if( animfile2 && g_rng.GetDouble() < 0.5f )
        {
            ret = Claw::AssetDict::Get<Claw::Surface>( animfilename2 );
        }
        else
        {
            ret = Claw::AssetDict::Get<Claw::Surface>( animfilename );
        }
        m_map->AddAnim( ret );
    }
    return ret;
}

Claw::Surface* GameManager::GetWeaponHUDAsset( const Claw::NarrowString& weapon )
{
    std::map<Claw::NarrowString, Claw::SurfacePtr>::const_iterator it = m_weaponMap.find( weapon );
    if( it != m_weaponMap.end() )
    {
        return it->second;
    }

    Claw::Lua* lua = GetMenu( true )->GetLua();

    lua->PushString( weapon );
    lua->Call( "ItemDbGetItemIcon", 1, 1 );
    Claw::NarrowString wepIcon = lua->CheckString( -1 );
    lua->Pop( 1 );

    CLAW_MSG_WARNING( !wepIcon.empty(), "Missing HUD weapon icons definded in ItemsDB. Missing item: " << weapon );

    if( !wepIcon.empty() )
    {
        Claw::SurfacePtr w( Claw::AssetDict::Get<Claw::Surface>( wepIcon ) );
        m_weaponMap.insert( std::make_pair( weapon, w ) );
        return w;
    }

    return NULL;
}

void GameManager::DrawBufferSize( int x, int y )
{
    m_vengeanceFx->m_shader->Uniform( "offset", 1.f / Claw::AlignPOT( x ) , 1.f / Claw::AlignPOT( y ) );
}

void GameManager::SetMenuActive( bool active )
{
    if( m_menuActive != active )
    {
        m_menuActive = active;
        GameEventDispatcher::GetInstance()->HandleGameEvent( m_menuActive ? GEI_GAME_PAUSED : GEI_GAME_RESUMED );
    }
}

void GameManager::DamageObject( Obstacle* o, float damage )
{
    std::vector<Map::StaticObject*> remove;
    const std::vector<Map::StaticObject*>& so = m_map->GetNamedObject( o->GetName() );
    if( !so.empty() )
    {
        for( std::vector<Map::StaticObject*>::const_iterator it = so.begin(); it != so.end(); ++it )
        {
            m_lua->PushString( o->GetName() );
            (*it)->m_life -= damage;
            if( (*it)->m_life <= 0 )
            {
                GameEventDispatcher::GetInstance()->HandleGameEvent( GEI_OBJECT_DESTROYED, 1, o->GetName() );

                m_lua->Call( "ObjectDestroyedInternal", 1, 0 );
                remove.push_back( *it );
            }
            else
            {
                m_lua->PushNumber( (*it)->m_life * (*it)->m_rmaxLife );
                m_lua->Call( "ObjectDamaged", 2, 0 );
            }
        }
    }
    for( std::vector<Map::StaticObject*>::const_iterator it = remove.begin(); it != remove.end(); ++it )
    {
        m_map->KillObject( *it );
    }
}

#ifndef __MONSTAZ_STATS_HPP__
#define __MONSTAZ_STATS_HPP__

#include <vector>
#include "claw/base/SmartPtr.hpp"
#include "claw/sound/mixer/AudioChannel.hpp"

struct Perks
{
    enum Type
    {
        Runner,
        AmmoManiac,
        FastHands,
        ComeGetSome,
        Slaughter,
        Regeneration,
        Unstoppable,
        Rage,
        Endoskeleton,
        ColdVengeance,
        FirstAid,
        ToughSkin,
        OrbExtender,
        Greed,
        Wrath,
        TeamPlayer,
        MrClean,
        BigBang,
        PurfectCat,
        Ragenerator,
        RussianRoulette,
        ExtraExplosives,
        Withdraw,
        MuscleFever,

        PerkCount
    };
};

class Stats : public Claw::RefCounter
{
public:
    Stats();
    ~Stats();

    void Update( float dt );

    void AddXp( int xp );
    void AddPoints( int amount );

    float GetXpProgress() const;
    int GetLevel() const { return m_level; }
    int GetPerks() const { return m_perks; }
    bool CheckPerk( Perks::Type perk ) const { return m_perkState[perk]; }
    int GetPoints() const { return m_points; }
    int GetMultiplier() const { return m_multi + 0.5f; }
    int GetMaxMultiplier() const { return m_maxMulti; }
    int GetKills() const { return m_kills; }
    float& GetMaxPlayerHP() { return m_maxPlayerHP; }

    void EnablePerk( Perks::Type perk );
    void IncreaseMultiplier();
    void IncreaseKills();

    void AddPerkLevel( int xp ) { m_levels.insert( m_levels.end()-1, xp ); }

private:
    int m_xp;
    int m_level;
    int m_perks;
    int m_points;
    float m_multi;
    float m_fallSpeed;
    int m_multiKill;
    float m_multiKillTimer;
    int m_maxMulti;
    int m_kills;
    float m_maxPlayerHP;

    bool m_perkState[Perks::PerkCount];
    std::vector<int> m_levels;

    Claw::AudioChannelWPtr m_announcer;
};

typedef Claw::SmartPtr<Stats> StatsPtr;

#endif

#include "MonstazAI/GameEvent.hpp"
#include "MonstazAI/GameEventDispatcher.hpp"

void GameEvent::InitLua( Claw::Lua* lua )
{
    lua->CreateEnumTable();
    lua->AddEnum( GEI_ENEMY_KILLED );
    lua->AddEnum( GEI_OBJECT_DESTROYED );
    lua->AddEnum( GEI_OBJECT_PICKUP );
    lua->AddEnum( GEI_BULLETS_EARNED );
    lua->AddEnum( GEI_LEVEL_STARTED );
    lua->AddEnum( GEI_LEVEL_RESTARTED );
    lua->AddEnum( GEI_LEVEL_WITH_FRIEND );
    lua->AddEnum( GEI_LEVEL_FINISHED );
    lua->AddEnum( GEI_TUTORIAL_STARTED );
    lua->AddEnum( GEI_TUTORIAL_TASK_COMPLETED );
    lua->AddEnum( GEI_TUTORIAL_COMPLETED );
    lua->AddEnum( GEI_CASH_CHANGED );
    lua->AddEnum( GEI_SHOT_PLAYER_START );
    lua->AddEnum( GEI_SHOT_PLAYER_STOP );
    lua->AddEnum( GEI_GAME_PAUSED );
    lua->AddEnum( GEI_GAME_RESUMED );
    lua->AddEnum( GEI_PERK_USED );
    lua->AddEnum( GEI_ITEM_BOUGHT );
    lua->AddEnum( GEI_ITEM_UNLOCKED );
    lua->AddEnum( GEI_ITEM_UPGRADED );
    lua->AddEnum( GEI_ITEM_USED );
    lua->AddEnum( GEI_ITEM_CANT_AFFORD );
    lua->AddEnum( GEI_GAME_SCORE_CHANGED );
    lua->AddEnum( GEI_GAME_HISCORE_CHANGED );
    lua->AddEnum( GEI_AUTOAIMING_CHANGED );
    lua->AddEnum( GEI_REVIVE_PLAYER );
    lua->AddEnum( GEI_SETTINGS_TAB_CHANGED );
    lua->AddEnum( GEI_FRIEND_INVITED );
    lua->AddEnum( GEI_FRIEND_INVITATION_CANCELED );
    lua->AddEnum( GEI_FRIEND_INVITATION_CONFIRMED );
    lua->AddEnum( GEI_FRIEND_GIFT_SENT );
    lua->AddEnum( GEI_FRIEND_GIFT_RECIVED );
    lua->AddEnum( GEI_FRIEND_PROFILE_SHOWN );
    lua->AddEnum( GEI_FRIEND_PROFILE_ACTION );
    lua->AddEnum( GEI_AGE_CHECKED );
    lua->AddEnum( GEI_TRANSACTION_COMPLETED );
    lua->AddEnum( GEI_TRANSACTION_FAILED );
    lua->AddEnum( GEI_TRANSACTION_CANCELED );
    lua->AddEnum( GEI_TRANSACTION_STARTED );
    lua->AddEnum( GEI_CASH_ACTION_STARTED );
    lua->AddEnum( GEI_CASH_ACTION_FINISHED );
    lua->AddEnum( GEI_CASH_ACTION_SUBSCRIPTION_CANELED );
    lua->AddEnum( GEI_FREE_STUFF_ACTION );
    lua->AddEnum( GEI_NO_CASH_ACTION );
    lua->AddEnum( GEI_SUMMARY_ACTION );
    lua->AddEnum( GEI_SHOP_STATE_CHANGED );
    lua->AddEnum( GEI_MISSION_COMPLETED );
    lua->AddEnum( GEI_MISSION_CLAIMED );
    lua->AddEnum( GEI_LTO_SHOWN );
    lua->AddEnum( GEI_LTO_ACTION );
    lua->AddEnum( GEI_SOCIAL_LOGIN_SKIP );
    lua->AddEnum( GEI_SOCIAL_LOGIN_TRY );
    lua->AddEnum( GEI_SOCIAL_LOGIN_SUCCEED );
    lua->AddEnum( GEI_FUEL_SHOWN );
    lua->AddEnum( GEI_FUEL_ACTION );
    lua->AddEnum( GEI_NEW_WEAPON_SHOWN );
    lua->AddEnum( GEI_NEW_WEAPON_ACTION );
    lua->RegisterEnumTable( "GameEvent" );

    lua->CreateEnumTable();
    lua->AddEnum( GEP_LEVEL_STARTED_STORY );
    lua->AddEnum( GEP_LEVEL_STARTED_SURVIVAL );
    lua->AddEnum( GEP_LEVEL_STARTED_TUTORIAL );
    lua->AddEnum( GEP_LEVEL_STARTED_BOSSFIGHT );
    lua->AddEnum( GEP_LEVEL_FINISHED_ABORT );
    lua->AddEnum( GEP_LEVEL_FINISHED_SUCCESS );
    lua->AddEnum( GEP_LEVEL_FINISHED_FAIL );
    lua->AddEnum( GEP_AGE_LT_8 );
    lua->AddEnum( GEP_AGE_GTE_8 );
    lua->AddEnum( GEP_FREE_STUFF_FACEBOOK );
    lua->AddEnum( GEP_FREE_STUFF_TWITTER );
    lua->AddEnum( GEP_FREE_STUFF_RATE );
    lua->AddEnum( GEP_FREE_STUFF_SOUNDTRACK );
    lua->AddEnum( GEP_NO_CASH_BACK );
    lua->AddEnum( GEP_NO_CASH_GET );
    lua->AddEnum( GEP_SHOP_OPEN );
    lua->AddEnum( GEP_SHOP_CLOSE );
    lua->AddEnum( GEP_SUMARY_SHOP );
    lua->AddEnum( GEP_SUMARY_RESTART );
    lua->AddEnum( GEP_SUMARY_BACK );
    lua->AddEnum( GEP_SUMARY_ITEM_1 );
    lua->AddEnum( GEP_SUMARY_ITEM_2 );
    lua->AddEnum( GEP_LTO_BACK );
    lua->AddEnum( GEP_LTO_GET_REGULAR );
    lua->AddEnum( GEP_LTO_GET_SOFT );
    lua->AddEnum( GEP_LTO_GET_HARD );
    lua->AddEnum( GEP_LOGIN_FACEBOOK );
    lua->AddEnum( GEP_LOGIN_GOOGLE_PLUS );
    lua->AddEnum( GEP_LOGIN_CREATE );
    lua->AddEnum( GEP_LOGIN_LOGIN );
    lua->AddEnum( GEP_PROFILE_CLOSE );
    lua->AddEnum( GEP_PROFILE_CLOSE_WITHOUT_GIFT );
    lua->AddEnum( GEP_PROFILE_SEND_GIFT );
    lua->AddEnum( GEP_FUEL_HUD );
    lua->AddEnum( GEP_FUEL_RAN_OUT );
    lua->AddEnum( GEP_FUEL_REFIL );
    lua->AddEnum( GEP_FUEL_INVITE );
    lua->AddEnum( GEP_FUEL_USE_GIFT );
    lua->AddEnum( GEP_FUEL_WAIT );
    lua->AddEnum( GEP_NEW_WEAPON_BACK );
    lua->AddEnum( GEP_NEW_WEAPON_GET );
    lua->RegisterEnumTable( "GameEventParam" );
}

GameEventHandler::GameEventHandler( bool autoRegister )
    : m_autoRegister( autoRegister )
{
    if( m_autoRegister )
    {
        GameEventDispatcher::GetInstance()->RegisterGameEventHandler( this );
    }
}

GameEventHandler::~GameEventHandler()
{
    if( m_autoRegister )
    {
        GameEventDispatcher::GetInstance()->UnregisterGameEventHandler( this );
    }
}

bool GameEventHandler::HandleGameEvent( const GameEvent::Id& id, float value, const Claw::NarrowString& text, void* userData )
{
    GameEvent ev( id, value, text, userData );
    return HandleGameEvent( ev );
}

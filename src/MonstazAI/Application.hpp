#ifndef __APPLICATION_HPP__
#define __APPLICATION_HPP__

#include <list>
#include <queue>

#include "claw/application/Application.hpp"
#include "claw/application/DebugOverlay.hpp"
#include "claw/base/Lua.hpp"
#include "claw/graphics/OpenGLShader.hpp"

#include "claw_ext/network/server_sync/ServerSync.hpp"
#include "claw_ext/network/twitter/TwitterService.hpp"
#include "claw_ext/network/sms/SmsService.hpp"
#include "claw_ext/network/email/EmailService.hpp"

#include "MonstazAI/ApplicationConfig.hpp"
#include "MonstazAI/job/Job.hpp"
#include "MonstazAI/job/GameplayJob.hpp"
#include "MonstazAI/job/MainMenuJob.hpp"
#include "MonstazAI/AudioManager.hpp"
#include "MonstazAI/VibraController.hpp"
#include "MonstazAI/Shop.hpp"
#include "MonstazAI/math/Vector.hpp"
#include "MonstazAI/CashTimer.hpp"
#include "MonstazAI/AtlasManager.hpp"
#include "MonstazAI/PakManager.hpp"
#include "MonstazAI/TutorialManager.hpp"
#include "MonstazAI/FuelRefill.hpp"

//#define PUBLIC_BUILD

class InitJob;

namespace Network
{
    class Facebook;
    class GoogleServices;
}

namespace MonstazAI
{
    class MonstazAIApplication : public Claw::Application, public ClawExt::ServerSync::Observer
    {
        friend class ::InitJob;

    public:
        enum ScreenMode
        {
            SM_UNSUPPORTED = -1,
            SM_NORMAL,
            SM_MEDIUM,
            SM_HIGH,
            SM_FULLHD,
            SM_HD
        };

        MonstazAIApplication();
        ~MonstazAIApplication();

        void OnStartup();
        void OnShutdown();

        void OnUpdate( float dt );
        void OnRender( Claw::Surface* target );
        void OnText( const char* text );
        void OnTextNewLine();

        void OnKeyPress( Claw::KeyCode code );
        void OnKeyRelease( Claw::KeyCode code );
        void OnTouchDown( int x, int y, int button );
        void OnTouchUp( int x, int y, int button );
        void OnTouchMove( int x, int y, int button );
        void OnTouchDeviceChange();

        void OnResize( int w, int h );
        void OnFocusChange( bool focus );

        void SwitchJob( Job* job );
        void SaveLoaded();
        void Save( bool forced = false );

        const Vectori& GetResolution() const;
        float GetGameScale() const;

        static const char* GetFacebookAppId();
        Network::Facebook* GetFacebook() const { return m_facebook; }
        Network::GoogleServices* GetGoogleServices() const { return m_googleServices; }
        TwitterService* GetTwitter() const { return m_twitter; }
        SmsService* GetSmsService() const { return m_sms; }
        EmailService* GetEmailService() const { return m_email; }

        bool IsXperiaPlayKeyboarbAvailable() const { return m_xperiaPlayTouchpadAvailable; }
        const Vectori& GetXperiaPlayTouchpadSize() const { return m_xperiaPlayTouchSize; }

        // ServerSync::Observer
        virtual void OnSynchronisationStart();
        virtual void OnSynchronisationEnd( bool success );
        virtual void OnSynchronisationSkip();
        virtual void OnTaskFinished( const ClawExt::ServerSync::TaskId& taskId, bool success );
        virtual void OnGroupChanged( const ClawExt::ServerSync::GroupName& newGroupName, bool initial );

        static ScreenMode GetScreenMode();
        static void PushScreenModes( Claw::Lua* lua );
        static int GetControlSize( Guif::Screen* screen, lua_State* L );

        static Vectorf GetBgImgSize();

        Job* GetJob() { return m_job; }

        void StartRefill();
        Claw::UInt32 GetNetworkTime() const { return m_cashTimer->Epoch(); }

    private:
        bool SaveWithBackup( const char* fileName, const char* branchName );

        static void LuaCallbackEntry( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { ((MonstazAIApplication*)ptr)->LuaCallback( args, conn ); }
        void LuaCallback( const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn );

        static void LuaStackEntry( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { ((MonstazAIApplication*)ptr)->LuaDumpStack( conn ); }
        void LuaDumpStack( Claw::DebugOverlay::Connection* conn );

        static void SwitchObstacleRendering( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { GameManager::GetInstance()->m_renderObstacles = !GameManager::GetInstance()->m_renderObstacles; }

        static void DumpMenuState( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { ((MonstazAIApplication*)ptr)->m_job->MenuDump(); }
        static void SwitchMap( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { ((MonstazAIApplication*)ptr)->m_scheduledMapSwitch = args; }

        static void AddCash( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { Shop::GetInstance()->UpdateCash( atoi( args.c_str() ), 0 ); }
        static void AddGold( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { Shop::GetInstance()->UpdateCash( 0, atoi( args.c_str() ) ); }
        static void ZeroCash( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { Shop::GetInstance()->SetCash( 0, 0 ); }

        static void Win( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { GameManager::GetInstance()->GetLua()->Call( "GameWon", 0, 0 ); }
        static void AddPoints( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { GameManager::GetInstance()->GetStats()->AddPoints( atoi( args.c_str() ) ); }

        static void ShowWorldMap( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn );
        static void SkipTutorial( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn ) { TutorialManager::GetInstance()->Skip(); }
        static void AddBackup( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn )  { UserDataManager::GetInstance()->SetDebugBackupFriend( args.c_str() ); }
        static void PreloadAtlas( void* ptr, const Claw::NarrowString& args, Claw::DebugOverlay::Connection* conn );
        static void BackgroundMusicStateCallback();

        double GetTimeMs();
        static int LuaGetTime( lua_State* L );

        void CheckScreenMode();

        void DetectXpreriaPlayKeyboard();

        char* m_time;
        double m_freq;

        Claw::UInt64 m_mallocCount;
        Claw::UInt64 m_freeCount;

        std::queue<Claw::NarrowString> m_luaQueue;

        JobPtr m_job;
        JobPtr m_nextJob;

        ShopPtr m_shop;
        AudioManagerPtr m_audioManager;
        VibraControllerPtr m_vibraController;
        CashTimerPtr m_cashTimer;
        AtlasManagerPtr m_atlasManager;
        PakManagerPtr m_pakManager;
        TutorialManagerPtr m_tutorialManager;

        Vectori     m_xperiaPlayTouchSize;
        bool        m_xperiaPlayTouchpadAvailable;

        Vectori     m_resolution;
        ScreenMode  m_screenMode;
        bool        m_lowVerticalSpace;
        float m_backgroundMusicCheckTimer;

        static const char* ENCRYPTION_KEY;
        static const int   BACKUP_SAVE_NUM;
        std::map<Claw::NarrowString, int> m_saveIdx;

        Network::Facebook* m_facebook;
        Network::GoogleServices* m_googleServices;
        TwitterServicePtr m_twitter;
        SmsServicePtr m_sms;
        EmailServicePtr m_email;
        FuelRefillPtr m_refill;

        Claw::SurfacePtr m_drawBuffer;
        float m_scaleX, m_scaleY;
        Claw::OpenGLShader m_bicubic;

        Claw::NarrowString m_scheduledMapSwitch;
    };
}

// Typedef shortcut
typedef MonstazAI::MonstazAIApplication MonstazApp;

#endif

#include <algorithm>

#include "MonstazAI/Renderable.hpp"
#include "MonstazAI/GameManager.hpp"

Renderable::~Renderable()
{
}


struct Comparator
{
    bool operator() ( const Renderable* r1, const Renderable* r2 )
    {
        if( r1->GetPos().y == r2->GetPos().y )
        {
            return r1->GetPos().x < r2->GetPos().x;
        }
        else
        {
            return r1->GetPos().y < r2->GetPos().y;
        }
    }
};


RenderableManager::RenderableManager()
    : m_sortBuf( new const Renderable*[MaxRenderables] )
{
    m_list.reserve( MaxRenderables );
}

RenderableManager::~RenderableManager()
{
    delete[] m_sortBuf;
}

void RenderableManager::Render( Claw::Surface* target, const Vectorf& offset )
{
    CLAW_ASSERT( m_list.size() < MaxRenderables - 1 );
    const float scale = GameManager::GetGameScale();

    Claw::Rect screen( offset.x, offset.y, target->GetWidth(), target->GetHeight() );
    if( scale != 1.0f )
    {
        screen.m_x /= scale;
        screen.m_y /= scale;
        screen.m_w /= scale;
        screen.m_h /= scale;
    }

    const Renderable** ptr = m_sortBuf;
    std::vector<const Renderable*>::const_iterator it = m_list.begin();
    std::vector<const Renderable*>::const_iterator end = m_list.end();
    for( ; it != end; ++it )
    {
        *ptr++ = *it;
    }
    *ptr = NULL;
    std::sort( m_sortBuf, ptr, Comparator() );

    ptr = m_sortBuf;
    while( *ptr )
    {
        (*ptr++)->Render( target, offset, scale );
    }

    m_list.clear();
}

#ifndef __MONSTAZ_TUTORIALMANAGER_HPP__
#define __MONSTAZ_TUTORIALMANAGER_HPP__

#include <vector>

#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/base/SmartPtr.hpp"

#include "MonstazAI/entity/Entity.hpp"

struct TutorialChapter
{
    enum Id
    {
        None,
        Intro,
        Shop,
        Premission,
        Gameplay,
        Challenges,
        Items,
        Mech,
        Final,
        Outro
    };

private:
    TutorialChapter();
};

struct TutorialTask
{
    enum Id
    {
        IntroPlayerMove = 1 << 0,
        IntroPlayerShot = 1 << 1,
        IntroKillFirstWave = 1 << 2,
        IntroSummary = 1 << 3,

        ShopEnter = 1 << 0,
        ShopBuyShotgun = 1 << 1,
        ShopBuyShotgunPopup = 1 << 2,
        ShopExit = 1 << 3,

        PremissionSelect = 1 << 0,
        PremissionSecondWeaponSlot = 1 << 1,
        PremissionEquipShotgun = 1 << 2,
        PremissionFirstWeaponSlot = 1 << 3,
        PremissionUpgradeSmg = 1 << 4,
        PremissionUpgradeSmgBack = 1 << 5,
        PremissionPlay = 1 << 6,

        GameplayChangeWeapon = 1 << 0,
        GameplayKillEnemies = 1 << 1,
        GameplayRage = 1 << 2,
        GameplaySelectPerk = 1 << 3,
        GameplayKillMoreEnemies = 1 << 4,
        GameplayKillElectricEnemies = 1 << 5,
        GameplayUseSmg = 1 << 6,
        GameplayLevelUp = 1 << 7,
        GameplaySummary = 1 << 8,

        ChallengesSelectArea = 1 << 0,
        ChallengesPlay = 1 << 1,

        ItemsPause = 1 << 0,
        ItemsBuy = 1 << 1,
        ItemsBuyGrenade = 1 << 2,
        ItemsBuyMine = 1 << 3,
        ItemsResume = 1 << 4,
        ItemsUse = 1 << 5,
        ItemsUseGrenade = 1 << 6,
        ItemsUseMine = 1 << 7,
        ItemsKillEnemies = 1 << 8,
        ItemsSummary = 1 << 9,

        MechSelectArea = 1 << 0,
        MechPlay = 1 << 1,

        FinalKillEnemies = 1 << 0,
        FinalSummary = 1 << 1,

        OutroGoodbye = 1 << 0,
    };

private:
    TutorialTask();
};

struct TutorialAnchor
{
    enum Pos
    {
        Bottom,
        Top,
        Left,
        Right
    };

private:
    TutorialAnchor();
};

namespace Guif
{
    class Screen;
}

class TutorialManager : public Claw::RefCounter
{
public:
    LUA_DEFINITION( TutorialManager );
    TutorialManager( lua_State* L ) { CLAW_ASSERT( false ); }

    TutorialManager();
    ~TutorialManager();

    void Init();
    void Init( Claw::Lua* lua );

    void Skip();

    void OnPause();
    void OnLevelLoaded();
    void OnLevelSummary( bool fail );
    void OnPlayerMove();
    void OnPlayerShot();
    void OnWeaponChange();
    void OnEnemyKilled( Entity* entity );
    void OnEnemyWaveKilled( int wave );
    void OnRage();
    void OnElementHit();

    void OnMainMenu();

    void OnFireGrenade();
    void OnPlaceMine();

    bool IsActive() const { return m_chapter != TutorialChapter::None; }

    void SetGameLua( Claw::Lua* lua ) { m_gameLua = lua; }
    void SetGameMenuLua( Claw::Lua* lua ) { m_gameMenuLua = lua; }
    void SetMainMenuLua( Claw::Lua* lua ) { m_mainMenuLua = lua; }

    TutorialChapter::Id GetCurrentChapter() const { return m_chapter; }

    bool IsTaskActive( TutorialTask::Id task ) const { return m_activeTasks & task; }
    bool IsTaskCompleted( TutorialTask::Id task ) const { return m_completedTasks & task; }

    bool IsWeaponChangeAllowed() const { return m_weaponChangeAllowed; }

    int l_GetCurrentChapter( lua_State* L );
    int l_IsTaskActive( lua_State* L );
    int l_IsTaskCompleted( lua_State* L );
    int l_SetTaskActive( lua_State* L );
    int l_SetTaskCompleted( lua_State* L );
    int l_OnEnemyWaveKilled( lua_State* L );

    static TutorialManager* GetInstance() { return s_instance; }

private:
    TutorialChapter::Id m_chapter;
    int m_activeTasks;
    int m_completedTasks;

    Claw::Lua* m_gameLua;
    Claw::Lua* m_gameMenuLua;
    Claw::Lua* m_mainMenuLua;

    bool m_weaponChangeAllowed;

    static TutorialManager* s_instance;

    void SetChapterCompleted();
    void SetTaskCompleted( TutorialTask::Id task );

    void SetTaskActive( TutorialTask::Id task );

    void ShowTooltip( const char* parent, const char* name, const char* message, int x, int y, TutorialAnchor::Pos anchor, float delay = 0, bool showArrow = true, float hideDelay = 0 );
    void HideTooltip( const char* path );
};

typedef Claw::SmartPtr<TutorialManager> TutorialManagerPtr;

#endif

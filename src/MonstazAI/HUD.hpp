#ifndef __MONSTAZ_HUD_HPP__
#define __MONSTAZ_HUD_HPP__

#include <deque>
#include <map>

#include "claw/base/SmartPtr.hpp"
#include "claw/graphics/FontEx.hpp"
#include "claw/graphics/ScreenText.hpp"
#include "claw/base/Lua.hpp"
#include "claw/base/Lunar.hpp"
#include "claw/graphics/OpenGLShader.hpp"

class Hud : public Claw::RefCounter
{
    enum CashPopupState
    {
        CPS_DORMANT,
        CPS_SLIDEIN,
        CPS_ACTIVE,
        CPS_SLIDEOUT
    };

    enum CashPopupType
    {
        CPT_INVALID,
        CPT_CASH,
        CPT_MISSION1,
        CPT_MISSION2,
        CPT_MISSION3
    };

public:
    LUA_DEFINITION( Hud );

    Hud( lua_State* L );
    void Init( Claw::Lua* lua );
    int l_ShowXp( lua_State* L );
    int l_ShowScore( lua_State* L );
    int l_ShowNuke( lua_State* L );
    int l_ShowAirstrike( lua_State* L );

    Hud();
    ~Hud();

    static Hud* GetInstance();

    void Render( Claw::Surface* target );
    void Update( float dt );

    void SetLife( float life ) { m_life = life; }
    void SetXp( float xp ) { m_xp = xp; }
    void SetWeapon( const char* weapon, int elements );
    void SetWeapon( const Claw::NarrowString& weapon, int elements );
    void SetReload( float reload );
    void SetBullets( int current ) { m_refreshBullets = m_current != current; m_current = current; }
    void SetLevelUp( bool levelup );
    void SetPoints( int points ) { m_refreshPoints = m_points != points; m_points = points; }
    void SetMultiplier( int multi ) { m_refreshMulti = m_multi != multi; m_multi = multi; }
    void SetCash( int cash );
    void SetMissionDone( int num );
    void SetInventory( int grenades, int stimpaks, int landmines, int shield );
    void PlayerDamaged( float damage ) { m_playerDamage += damage; }
    void PlayerDied() { m_playerDead = true; }

    void ShowXp( bool enable )          { m_showXp = enable; }
    void ShowScore( bool enable )       { m_showScore = enable; }
    void ShowNuke( bool enable )        { m_showNuke = enable; }
    void ShowAirstrike( bool enable )   { m_showAirstrike = enable; }
    void ShowInventory( bool enable )   { m_showInventory = enable; }

    void ShowPauseNotification( int repeat = 1 )    { m_pauseTime = repeat/m_pauseSpeed; }

    Vectorf GetWeaponPosition() const;

private:
    void RenderXp( Claw::Surface* target );
    void RenderWeapon( Claw::Surface* target );
    void RenderScore( Claw::Surface* target );
    void RenderCash( Claw::Surface* target );
    void RenderTexts( Claw::Surface* target );
    void RenderNuke( Claw::Surface* target );
    void RenderAirstrike( Claw::Surface* target );
    void RenderInventory( Claw::Surface* target );

    void RenderPickupHints( Claw::Surface* target );
    void RenderHint( Claw::Surface* target, int idx, Vectorf pos );

    void RefreshPopup();

    void RenderHit( Claw::Surface* target, float intensity );

    float m_scale;
    float m_rscale;
    float m_time;

    float m_life;
    float m_xp;
    Claw::NarrowString m_weaponSel;
    int m_elements;
    float m_reloadState;
    bool m_reloadFinish;
    int m_current;
    bool m_refreshBullets;
    bool m_isLevelup;
    int m_points;
    bool m_refreshPoints;
    int m_multi;
    bool m_refreshMulti;
    int m_cash;
    float m_playerDamage;
    bool m_playerDead;
    CashPopupType m_cpt;
    std::deque<CashPopupType> m_cashQueue;
    int m_cashMissionIco;
    int m_grenades;
    int m_stimpaks;
    int m_mines;
    int m_shields;
    bool m_refreshStimpaks;
    bool m_refreshGrenades;
    bool m_refreshMines;
    bool m_refreshShield;

    bool m_showXp;
    bool m_showScore;
    bool m_showNuke;
    bool m_showAirstrike;
    bool m_showInventory;

    CashPopupState m_cashState;
    float m_cashSlider;
    float m_cashPosition;
    float m_pauseTime;
    float m_pauseSpeed;

    Claw::SurfacePtr m_health;
    Claw::SurfacePtr m_healthBar;
    Claw::SurfacePtr m_healthBarBg;
    Claw::SurfacePtr m_rageBar;
    Claw::SurfacePtr m_rageBarBg;
    Claw::SurfacePtr m_gunInfo;
    Claw::SurfacePtr m_gunIce;
    Claw::SurfacePtr m_gunFire;
    Claw::SurfacePtr m_gunElectric;
    Claw::SurfacePtr m_reload;
    Claw::SurfacePtr m_reloadFill;
    Claw::SurfacePtr m_reloadEnd;
    Claw::SurfacePtr m_scoreBar;
    Claw::SurfacePtr m_scoreBar2;
    Claw::SurfacePtr m_pickupArrow[8];
    Claw::SurfacePtr m_pickupExclamation[8];
    Claw::SurfacePtr m_cashBg;
    Claw::SurfacePtr m_cashIcon;
    Claw::SurfacePtr m_bokeh[2];
    Claw::SurfacePtr m_hit;
    Claw::SurfacePtr m_nuke[2];
    Claw::SurfacePtr m_airstrike[2];
    Claw::SurfacePtr m_tick;
    Claw::SurfacePtr m_missionIco[3];
    Claw::SurfacePtr m_pause;
    Claw::SurfacePtr m_grenadesIcon;
    Claw::SurfacePtr m_stimpakIcon;
    Claw::SurfacePtr m_minesIcon;
    Claw::SurfacePtr m_shieldIcon;

    Claw::FontExPtr m_font;
    Claw::FontExPtr m_fontSmall;
    Claw::ScreenTextPtr m_bullets;
    Claw::ScreenTextPtr m_pointsTxt;
    Claw::ScreenTextPtr m_multiTxt;
    Claw::ScreenTextPtr m_cashTxt;
    Claw::ScreenTextPtr m_stimpaksTxt;
    Claw::ScreenTextPtr m_grenadesTxt;
    Claw::ScreenTextPtr m_minesTxt;
    Claw::ScreenTextPtr m_shieldTxt;

    Claw::OpenGLShader m_shader;
    Claw::OpenGLShader m_hpShader;
    bool m_lowquality;

    static Hud* s_instance;
};

typedef Claw::SmartPtr<Hud> HudPtr;

#endif

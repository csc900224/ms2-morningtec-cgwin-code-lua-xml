#include <stdio.h>

#include "claw/base/AssetDict.hpp"

#include "MonstazAI/AnimationSet.hpp"

static const char* d8[] = {
    "000",    // 0
    "045",    // 1
    "090",    // 2
    "135",    // 3
    "180",    // 4
    "225",    // 5
    "270",    // 6
    "315",    // 7
    0
};

static const char* d16[] = {
    "000",  // 0
    "022",  // 1
    "315",  // 2
    "337",  // 3
    "045",  // 4
    "067",  // 5
    "270",  // 6
    "292",  // 7
    "090",  // 8
    "112",  // 9
    "180",  // 10
    "225",  // 11
    "247",  // 12
    "135",  // 13
    "157",  // 14
    "202",  // 15
    0
};

static const char* d32[] = {
    "000",  // 0
    "011",  // 1
    "022",  // 2
    "033",  // 3
    "045",  // 4
    "056",  // 5
    "067",  // 6
    "078",  // 7
    "090",  // 8
    "101",  // 9
    "112",  // 10
    "123",  // 11
    "135",  // 12
    "146",  // 13
    "157",  // 14
    "168",  // 15
    "180",  // 16
    "191",  // 17
    "202",  // 18
    "213",  // 19
    "225",  // 20
    "236",  // 21
    "247",  // 22
    "258",  // 23
    "270",  // 24
    "281",  // 25
    "292",  // 26
    "303",  // 27
    "315",  // 28
    "326",  // 29
    "337",  // 30
    "348",  // 31
    0
};

static const Vectorf sp16[] = {
    Vectorf( 12, 8 ),
    Vectorf( 21, 2 ),
    Vectorf( -7, 8 ),
    Vectorf( 1, 11 ),
    Vectorf( 24, -3 ),
    Vectorf( 25, -10 ),
    Vectorf( -21, 0 ),
    Vectorf( -13, 5 ),
    Vectorf( 21, -15 ),
    Vectorf( 16, -20 ),
    Vectorf( -12, -24 ),
    Vectorf( -20, -12 ),
    Vectorf( -22, -6 ),
    Vectorf( 5, -26 ),
    Vectorf( -2, -27 ),
    Vectorf( -19, -18 )
};

static const Vectorf sp32d[32] = {
    Vectorf( -11, -2 ),
    Vectorf( -8, -3 ),
    Vectorf( -5, -4 ),
    Vectorf( -1, -4 ),
    Vectorf( 4, -4 ),
    Vectorf( 7, -3 ),
    Vectorf( 11, -2 ),
    Vectorf( 14, -1 ),
    Vectorf( 17, 1 ),
    Vectorf( 19, 3 ),
    Vectorf( 19, 6 ),
    Vectorf( 20, 9 ),
    Vectorf( 19, 12 ),
    Vectorf( 18, 14 ),
    Vectorf( 17, 16 ),
    Vectorf( 14, 18 ),
    Vectorf( 11, 19 ),
    Vectorf( 8, 20 ),
    Vectorf( 4, 21 ),
    Vectorf( 0, 21 ),
    Vectorf( -4, 21 ),
    Vectorf( -8, 20 ),
    Vectorf( -12, 18 ),
    Vectorf( -15, 16 ),
    Vectorf( -17, 14 ),
    Vectorf( -19, 12 ),
    Vectorf( -19, 10 ),
    Vectorf( -20, 8 ),
    Vectorf( -20, 5 ),
    Vectorf( -19, 3 ),
    Vectorf( -17, 1 ),
    Vectorf( -14, -1 )
};

static const Vectorf sp32[20][32] = {
    // chaingun
    {
        Vectorf( 11, -8 ),
        Vectorf( 15, -6 ),
        Vectorf( 19, -4 ),
        Vectorf( 21, -2 ),
        Vectorf( 24, 1 ),
        Vectorf( 25, 4 ),
        Vectorf( 26, 7 ),
        Vectorf( 26, 10 ),
        Vectorf( 24, 14 ),
        Vectorf( 21, 16 ),
        Vectorf( 18, 18 ),
        Vectorf( 14, 21 ),
        Vectorf( 9, 24 ),
        Vectorf( 3, 27 ),
        Vectorf( -4, 27 ),
        Vectorf( -7, 26 ),
        Vectorf( -11, 22 ),
        Vectorf( -16, 20 ),
        Vectorf( -19, 18 ),
        Vectorf( -22, 16 ),
        Vectorf( -25, 13 ),
        Vectorf( -26, 9 ),
        Vectorf( -26, 6 ),
        Vectorf( -26, 3 ),
        Vectorf( -24, 0 ),
        Vectorf( -21, -3 ),
        Vectorf( -18, -5 ),
        Vectorf( -15, -7 ),
        Vectorf( -9, -9 ),
        Vectorf( -4, -9 ),
        Vectorf( 1, -9 ),
        Vectorf( 6, -9 ),
    },
    // cutter
    {
        Vectorf( 12, -5 ),
        Vectorf( 15, -3 ),
        Vectorf( 18, -1 ),
        Vectorf( 20, 1 ),
        Vectorf( 21, 4 ),
        Vectorf( 22, 7 ),
        Vectorf( 21, 9 ),
        Vectorf( 20, 12 ),
        Vectorf( 19, 15 ),
        Vectorf( 17, 17 ),
        Vectorf( 14, 19 ),
        Vectorf( 11, 20 ),
        Vectorf( 8, 21 ),
        Vectorf( 3, 22 ),
        Vectorf( -2, 22 ),
        Vectorf( -6, 21 ),
        Vectorf( -11, 20 ),
        Vectorf( -15, 18 ),
        Vectorf( -17, 16 ),
        Vectorf( -19, 14 ),
        Vectorf( -21, 12 ),
        Vectorf( -22, 9 ),
        Vectorf( -22, 6 ),
        Vectorf( -22, 3 ),
        Vectorf( -19, 0 ),
        Vectorf( -16, -3 ),
        Vectorf( -13, -5 ),
        Vectorf( -10, -7 ),
        Vectorf( -5, -8 ),
        Vectorf( -1, -9 ),
        Vectorf( 3, -9 ),
        Vectorf( 8, -7 ),
    },
    // dualmagnum
    {
        Vectorf( 12, -3 ),
        Vectorf( 15, -1 ),
        Vectorf( 18, 1 ),
        Vectorf( 20, 3 ),
        Vectorf( 22, 6 ),
        Vectorf( 23, 9 ),
        Vectorf( 23, 11 ),
        Vectorf( 22, 14 ),
        Vectorf( 20, 16 ),
        Vectorf( 17, 19 ),
        Vectorf( 14, 21 ),
        Vectorf( 11, 22 ),
        Vectorf( 7, 23 ),
        Vectorf( 3, 24 ),
        Vectorf( -3, 24 ),
        Vectorf( -8, 24 ),
        Vectorf( -12, 23 ),
        Vectorf( -16, 21 ),
        Vectorf( -19, 18 ),
        Vectorf( -21, 16 ),
        Vectorf( -22, 13 ),
        Vectorf( -23, 10 ),
        Vectorf( -22, 7 ),
        Vectorf( -21, 4 ),
        Vectorf( -19, 2 ),
        Vectorf( -16, 0 ),
        Vectorf( -13, -2 ),
        Vectorf( -9, -3 ),
        Vectorf( -5, -4 ),
        Vectorf( -1, -4 ),
        Vectorf( 4, -4 ),
        Vectorf( 8, -4 ),
    },
    // energywhip
    {
        Vectorf( 13, -4 ),
        Vectorf( 16, -2 ),
        Vectorf( 18, 0 ),
        Vectorf( 19, 2 ),
        Vectorf( 20, 5 ),
        Vectorf( 20, 7 ),
        Vectorf( 19, 9 ),
        Vectorf( 17, 11 ),
        Vectorf( 15, 13 ),
        Vectorf( 13, 15 ),
        Vectorf( 10, 16 ),
        Vectorf( 7, 17 ),
        Vectorf( 4, 18 ),
        Vectorf( -1, 18 ),
        Vectorf( -6, 18 ),
        Vectorf( -10, 17 ),
        Vectorf( -13, 16 ),
        Vectorf( -17, 13 ),
        Vectorf( -19, 11 ),
        Vectorf( -20, 9 ),
        Vectorf( -21, 7 ),
        Vectorf( -21, 4 ),
        Vectorf( -20, 1 ),
        Vectorf( -17, -1 ),
        Vectorf( -14, -3 ),
        Vectorf( -12, -5 ),
        Vectorf( -8, -6 ),
        Vectorf( -4, -7 ),
        Vectorf( 0, -7 ),
        Vectorf( 3, -7 ),
        Vectorf( 7, -6 ),
        Vectorf( 10, -5 ),
    },
    // frostshotgun
    {
        Vectorf( 12, -5 ),
        Vectorf( 16, -3 ),
        Vectorf( 19, -1 ),
        Vectorf( 21, 1 ),
        Vectorf( 23, 4 ),
        Vectorf( 23, 6 ),
        Vectorf( 22, 9 ),
        Vectorf( 22, 12 ),
        Vectorf( 20, 14 ),
        Vectorf( 17, 17 ),
        Vectorf( 14, 19 ),
        Vectorf( 11, 21 ),
        Vectorf( 7, 21 ),
        Vectorf( 2, 22 ),
        Vectorf( -4, 22 ),
        Vectorf( -10, 21 ),
        Vectorf( -13, 20 ),
        Vectorf( -16, 18 ),
        Vectorf( -19, 16 ),
        Vectorf( -21, 13 ),
        Vectorf( -23, 10 ),
        Vectorf( -24, 7 ),
        Vectorf( -23, 4 ),
        Vectorf( -22, 2 ),
        Vectorf( -20, -1 ),
        Vectorf( -17, -3 ),
        Vectorf( -14, -5 ),
        Vectorf( -10, -6 ),
        Vectorf( -5, -7 ),
        Vectorf( 0, -7 ),
        Vectorf( 4, -7 ),
        Vectorf( 8, -6 ),
    },
    // gundi
    {
        Vectorf( 12, -2 ),
        Vectorf( 15, 0 ),
        Vectorf( 17, 2 ),
        Vectorf( 19, 4 ),
        Vectorf( 20, 6 ),
        Vectorf( 20, 9 ),
        Vectorf( 20, 12 ),
        Vectorf( 19, 14 ),
        Vectorf( 17, 16 ),
        Vectorf( 14, 18 ),
        Vectorf( 11, 19 ),
        Vectorf( 7, 21 ),
        Vectorf( 4, 21 ),
        Vectorf( 0, 22 ),
        Vectorf( -4, 22 ),
        Vectorf( -9, 21 ),
        Vectorf( -12, 19 ),
        Vectorf( -16, 18 ),
        Vectorf( -18, 15 ),
        Vectorf( -19, 13 ),
        Vectorf( -21, 11 ),
        Vectorf( -21, 8 ),
        Vectorf( -21, 5 ),
        Vectorf( -20, 3 ),
        Vectorf( -17, 0 ),
        Vectorf( -15, -2 ),
        Vectorf( -11, -3 ),
        Vectorf( -8, -4 ),
        Vectorf( -4, -5 ),
        Vectorf( 0, -5 ),
        Vectorf( 5, -5 ),
        Vectorf( 9, -3 ),
    },
    // icegun
    {
        Vectorf( 12, -6 ),
        Vectorf( 15, -4 ),
        Vectorf( 18, -2 ),
        Vectorf( 20, 1 ),
        Vectorf( 21, 3 ),
        Vectorf( 22, 5 ),
        Vectorf( 22, 7 ),
        Vectorf( 21, 10 ),
        Vectorf( 19, 12 ),
        Vectorf( 16, 14 ),
        Vectorf( 13, 16 ),
        Vectorf( 9, 19 ),
        Vectorf( 5, 20 ),
        Vectorf( 1, 21 ),
        Vectorf( -3, 21 ),
        Vectorf( -8, 21 ),
        Vectorf( -12, 19 ),
        Vectorf( -16, 17 ),
        Vectorf( -18, 15 ),
        Vectorf( -20, 12 ),
        Vectorf( -21, 10 ),
        Vectorf( -22, 7 ),
        Vectorf( -22, 4 ),
        Vectorf( -21, 1 ),
        Vectorf( -19, -2 ),
        Vectorf( -16, -4 ),
        Vectorf( -12, -5 ),
        Vectorf( -8, -6 ),
        Vectorf( -4, -7 ),
        Vectorf( 0, -7 ),
        Vectorf( 4, -7 ),
        Vectorf( 8, -7 ),
    },
    // railgun
    {
        Vectorf( 13, -10 ),
        Vectorf( 17, -8 ),
        Vectorf( 21, -6 ),
        Vectorf( 24, -3 ),
        Vectorf( 26, 0 ),
        Vectorf( 27, 3 ),
        Vectorf( 27, 7 ),
        Vectorf( 27, 10 ),
        Vectorf( 24, 13 ),
        Vectorf( 21, 16 ),
        Vectorf( 18, 18 ),
        Vectorf( 14, 21 ),
        Vectorf( 10, 23 ),
        Vectorf( 5, 25 ),
        Vectorf( 0, 25 ),
        Vectorf( -6, 25 ),
        Vectorf( -13, 23 ),
        Vectorf( -17, 20 ),
        Vectorf( -21, 16 ),
        Vectorf( -24, 13 ),
        Vectorf( -26, 10 ),
        Vectorf( -27, 7 ),
        Vectorf( -27, 4 ),
        Vectorf( -26, 0 ),
        Vectorf( -23, -3 ),
        Vectorf( -21, -6 ),
        Vectorf( -17, -8 ),
        Vectorf( -13, -10 ),
        Vectorf( -8, -11 ),
        Vectorf( -3, -12 ),
        Vectorf( 2, -12 ),
        Vectorf( 8, -11 ),
    },
    // ripper
    {
        Vectorf( 13, -10 ),
        Vectorf( 17, -8 ),
        Vectorf( 20, -6 ),
        Vectorf( 23, -4 ),
        Vectorf( 25, -1 ),
        Vectorf( 26, 2 ),
        Vectorf( 26, 5 ),
        Vectorf( 25, 8 ),
        Vectorf( 24, 12 ),
        Vectorf( 20, 14 ),
        Vectorf( 17, 16 ),
        Vectorf( 13, 18 ),
        Vectorf( 8, 20 ),
        Vectorf( 3, 21 ),
        Vectorf( -2, 21 ),
        Vectorf( -7, 20 ),
        Vectorf( -13, 18 ),
        Vectorf( -18, 16 ),
        Vectorf( -21, 13 ),
        Vectorf( -23, 10 ),
        Vectorf( -25, 8 ),
        Vectorf( -26, 5 ),
        Vectorf( -26, 1 ),
        Vectorf( -25, -2 ),
        Vectorf( -23, -5 ),
        Vectorf( -20, -7 ),
        Vectorf( -15, -9 ),
        Vectorf( -11, -12 ),
        Vectorf( -7, -14 ),
        Vectorf( -1, -14 ),
        Vectorf( 4, -13 ),
        Vectorf( 9, -12 ),
    },
    // roaster
    {
        Vectorf( 13, -7 ),
        Vectorf( 17, -5 ),
        Vectorf( 20, -2 ),
        Vectorf( 22, 1 ),
        Vectorf( 24, 3 ),
        Vectorf( 25, 6 ),
        Vectorf( 25, 9 ),
        Vectorf( 25, 12 ),
        Vectorf( 22, 15 ),
        Vectorf( 20, 18 ),
        Vectorf( 16, 20 ),
        Vectorf( 11, 21 ),
        Vectorf( 6, 22 ),
        Vectorf( 1, 22 ),
        Vectorf( -4, 21 ),
        Vectorf( -10, 20 ),
        Vectorf( -13, 19 ),
        Vectorf( -17, 18 ),
        Vectorf( -19, 15 ),
        Vectorf( -20, 13 ),
        Vectorf( -22, 9 ),
        Vectorf( -23, 7 ),
        Vectorf( -24, 5 ),
        Vectorf( -24, 1 ),
        Vectorf( -21, -2 ),
        Vectorf( -19, -4 ),
        Vectorf( -15, -6 ),
        Vectorf( -11, -8 ),
        Vectorf( -6, -9 ),
        Vectorf( -2, -9 ),
        Vectorf( 3, -9 ),
        Vectorf( 8, -8 ),
    },
    // rocket
    {
        Vectorf( 14, -2 ),
        Vectorf( 18, 0 ),
        Vectorf( 21, 3 ),
        Vectorf( 23, 6 ),
        Vectorf( 23, 8 ),
        Vectorf( 23, 11 ),
        Vectorf( 23, 14 ),
        Vectorf( 22, 16 ),
        Vectorf( 19, 19 ),
        Vectorf( 16, 21 ),
        Vectorf( 13, 22 ),
        Vectorf( 9, 24 ),
        Vectorf( 5, 25 ),
        Vectorf( -1, 26 ),
        Vectorf( -6, 25 ),
        Vectorf( -10, 24 ),
        Vectorf( -14, 22 ),
        Vectorf( -17, 20 ),
        Vectorf( -20, 18 ),
        Vectorf( -22, 15 ),
        Vectorf( -23, 12 ),
        Vectorf( -23, 9 ),
        Vectorf( -23, 5 ),
        Vectorf( -21, 3 ),
        Vectorf( -19, 1 ),
        Vectorf( -16, 0 ),
        Vectorf( -12, -1 ),
        Vectorf( -8, -3 ),
        Vectorf( -3, -4 ),
        Vectorf( 1, -4 ),
        Vectorf( 6, -4 ),
        Vectorf( 10, -3 ),
    },
    // saw
    {
        Vectorf( 1, -5 ),
        Vectorf( 6, -5 ),
        Vectorf( 11, -3 ),
        Vectorf( 15, -1 ),
        Vectorf( 18, 1 ),
        Vectorf( 21, 3 ),
        Vectorf( 24, 5 ),
        Vectorf( 25, 8 ),
        Vectorf( 25, 11 ),
        Vectorf( 24, 14 ),
        Vectorf( 22, 17 ),
        Vectorf( 19, 19 ),
        Vectorf( 16, 22 ),
        Vectorf( 13, 24 ),
        Vectorf( 9, 26 ),
        Vectorf( 5, 27 ),
        Vectorf( 0, 28 ),
        Vectorf( -6, 28 ),
        Vectorf( -11, 26 ),
        Vectorf( -15, 24 ),
        Vectorf( -18, 21 ),
        Vectorf( -22, 19 ),
        Vectorf( -24, 16 ),
        Vectorf( -25, 13 ),
        Vectorf( -25, 10 ),
        Vectorf( -25, 6 ),
        Vectorf( -23, 3 ),
        Vectorf( -20, 1 ),
        Vectorf( -16, -2 ),
        Vectorf( -13, -4 ),
        Vectorf( -9, -5 ),
        Vectorf( -4, -5 ),
    },
    // shocker
    {
        Vectorf( 13, -8 ),
        Vectorf( 17, -6 ),
        Vectorf( 20, -3 ),
        Vectorf( 22, 0 ),
        Vectorf( 23, 2 ),
        Vectorf( 24, 5 ),
        Vectorf( 24, 8 ),
        Vectorf( 23, 11 ),
        Vectorf( 21, 14 ),
        Vectorf( 18, 16 ),
        Vectorf( 15, 18 ),
        Vectorf( 11, 20 ),
        Vectorf( 7, 22 ),
        Vectorf( 2, 24 ),
        Vectorf( -3, 24 ),
        Vectorf( -9, 22 ),
        Vectorf( -12, 20 ),
        Vectorf( -17, 18 ),
        Vectorf( -20, 16 ),
        Vectorf( -23, 13 ),
        Vectorf( -24, 10 ),
        Vectorf( -25, 7 ),
        Vectorf( -25, 4 ),
        Vectorf( -24, 1 ),
        Vectorf( -22, -2 ),
        Vectorf( -19, -4 ),
        Vectorf( -15, -6 ),
        Vectorf( -11, -8 ),
        Vectorf( -6, -10 ),
        Vectorf( -1, -10 ),
        Vectorf( 4, -10 ),
        Vectorf( 8, -9 ),
    },
    // shotgun
    {
        Vectorf( 12, -4 ),
        Vectorf( 16, -2 ),
        Vectorf( 19, 0 ),
        Vectorf( 21, 2 ),
        Vectorf( 22, 4 ),
        Vectorf( 23, 7 ),
        Vectorf( 23, 10 ),
        Vectorf( 22, 12 ),
        Vectorf( 20, 15 ),
        Vectorf( 17, 17 ),
        Vectorf( 14, 19 ),
        Vectorf( 11, 21 ),
        Vectorf( 7, 22 ),
        Vectorf( 3, 23 ),
        Vectorf( -2, 24 ),
        Vectorf( -8, 23 ),
        Vectorf( -12, 21 ),
        Vectorf( -16, 19 ),
        Vectorf( -20, 17 ),
        Vectorf( -22, 14 ),
        Vectorf( -23, 11 ),
        Vectorf( -24, 8 ),
        Vectorf( -23, 5 ),
        Vectorf( -22, 2 ),
        Vectorf( -20, 0 ),
        Vectorf( -17, -2 ),
        Vectorf( -14, -4 ),
        Vectorf( -10, -6 ),
        Vectorf( -5, -7 ),
        Vectorf( -1, -7 ),
        Vectorf( 4, -6 ),
        Vectorf( 7, -5 ),
    },
    // spikegun
    {
        Vectorf( 12, -8 ),
        Vectorf( 16, -6 ),
        Vectorf( 19, -4 ),
        Vectorf( 21, -2 ),
        Vectorf( 23, 1 ),
        Vectorf( 24, 4 ),
        Vectorf( 24, 7 ),
        Vectorf( 24, 10 ),
        Vectorf( 22, 13 ),
        Vectorf( 20, 16 ),
        Vectorf( 16, 18 ),
        Vectorf( 12, 20 ),
        Vectorf( 7, 22 ),
        Vectorf( 2, 23 ),
        Vectorf( -3, 23 ),
        Vectorf( -8, 22 ),
        Vectorf( -12, 20 ),
        Vectorf( -17, 18 ),
        Vectorf( -20, 16 ),
        Vectorf( -23, 14 ),
        Vectorf( -25, 10 ),
        Vectorf( -26, 7 ),
        Vectorf( -26, 4 ),
        Vectorf( -25, 1 ),
        Vectorf( -22, -2 ),
        Vectorf( -20, -5 ),
        Vectorf( -16, -7 ),
        Vectorf( -12, -9 ),
        Vectorf( -7, -10 ),
        Vectorf( -2, -10 ),
        Vectorf( 3, -10 ),
        Vectorf( 8, -9 ),
    },
    // submachine
    {
        Vectorf( 12, -2 ),
        Vectorf( 15, 0 ),
        Vectorf( 17, 2 ),
        Vectorf( 19, 4 ),
        Vectorf( 20, 6 ),
        Vectorf( 20, 9 ),
        Vectorf( 20, 12 ),
        Vectorf( 19, 14 ),
        Vectorf( 17, 16 ),
        Vectorf( 14, 18 ),
        Vectorf( 11, 19 ),
        Vectorf( 7, 21 ),
        Vectorf( 4, 21 ),
        Vectorf( 0, 22 ),
        Vectorf( -4, 22 ),
        Vectorf( -9, 21 ),
        Vectorf( -12, 19 ),
        Vectorf( -16, 18 ),
        Vectorf( -18, 15 ),
        Vectorf( -19, 13 ),
        Vectorf( -21, 11 ),
        Vectorf( -21, 8 ),
        Vectorf( -21, 5 ),
        Vectorf( -20, 3 ),
        Vectorf( -17, 0 ),
        Vectorf( -15, -2 ),
        Vectorf( -11, -3 ),
        Vectorf( -8, -4 ),
        Vectorf( -4, -5 ),
        Vectorf( 0, -5 ),
        Vectorf( 5, -5 ),
        Vectorf( 9, -3 ),
    },
    // tripalizer
    {
        Vectorf( 13, -10 ),
        Vectorf( 17, -8 ),
        Vectorf( 20, -6 ),
        Vectorf( 24, -3 ),
        Vectorf( 27, 0 ),
        Vectorf( 27, 4 ),
        Vectorf( 27, 7 ),
        Vectorf( 26, 10 ),
        Vectorf( 24, 13 ),
        Vectorf( 21, 16 ),
        Vectorf( 17, 18 ),
        Vectorf( 13, 20 ),
        Vectorf( 9, 22 ),
        Vectorf( 4, 24 ),
        Vectorf( -2, 24 ),
        Vectorf( -8, 23 ),
        Vectorf( -13, 21 ),
        Vectorf( -18, 18 ),
        Vectorf( -21, 16 ),
        Vectorf( -25, 13 ),
        Vectorf( -27, 9 ),
        Vectorf( -27, 6 ),
        Vectorf( -27, 3 ),
        Vectorf( -26, 0 ),
        Vectorf( -24, -3 ),
        Vectorf( -21, -6 ),
        Vectorf( -17, -9 ),
        Vectorf( -13, -11 ),
        Vectorf( -8, -12 ),
        Vectorf( -2, -12 ),
        Vectorf( 3, -12 ),
        Vectorf( 8, -11 ),
    },
    // vortexgun
    {
        Vectorf( 12, -7 ),
        Vectorf( 15, -6 ),
        Vectorf( 18, -3 ),
        Vectorf( 21, 0 ),
        Vectorf( 22, 2 ),
        Vectorf( 23, 5 ),
        Vectorf( 23, 7 ),
        Vectorf( 22, 10 ),
        Vectorf( 20, 12 ),
        Vectorf( 18, 14 ),
        Vectorf( 15, 16 ),
        Vectorf( 12, 17 ),
        Vectorf( 8, 19 ),
        Vectorf( 3, 20 ),
        Vectorf( -2, 20 ),
        Vectorf( -7, 20 ),
        Vectorf( -12, 19 ),
        Vectorf( -16, 17 ),
        Vectorf( -19, 15 ),
        Vectorf( -21, 12 ),
        Vectorf( -23, 9 ),
        Vectorf( -23, 6 ),
        Vectorf( -23, 3 ),
        Vectorf( -22, 0 ),
        Vectorf( -20, -2 ),
        Vectorf( -17, -4 ),
        Vectorf( -14, -6 ),
        Vectorf( -10, -7 ),
        Vectorf( -6, -8 ),
        Vectorf( -2, -9 ),
        Vectorf( 2, -9 ),
        Vectorf( 7, -9 ),
    },
    // mech flamer
    {
        Vectorf( 22, 13 ),
        Vectorf( 26, 16 ),
        Vectorf( 28, 19 ),
        Vectorf( 30, 22 ),
        Vectorf( 30, 26 ),
        Vectorf( 30, 30 ),
        Vectorf( 28, 33 ), 
        Vectorf( 25, 36 ), 
        Vectorf( 22, 40 ), 
        Vectorf( 18, 43 ), 
        Vectorf( 14, 46 ), 
        Vectorf( 9, 48 ), 
        Vectorf( 2, 49 ), 
        Vectorf( -6, 48 ), 
        Vectorf( -12, 46 ), 
        Vectorf( -18, 44 ), 
        Vectorf( -23, 41 ), 
        Vectorf( -28, 36 ), 
        Vectorf( -32, 32 ), 
        Vectorf( -33, 29 ), 
        Vectorf( -33, 25 ), 
        Vectorf( -32, 21 ), 
        Vectorf( -29, 17 ), 
        Vectorf( -26, 14 ), 
        Vectorf( -21, 11 ), 
        Vectorf( -17, 9 ), 
        Vectorf( -12, 7 ), 
        Vectorf( -6, 6 ), 
        Vectorf( 1, 6 ), 
        Vectorf( 7, 7 ),
        Vectorf( 13, 9 ),
        Vectorf( 18, 11 ),
    },
    // mech rocket
    {
        Vectorf( -23, 13 ), 
        Vectorf( -19, 10 ), 
        Vectorf( -14, 8 ), 
        Vectorf( -9, 7 ), 
        Vectorf( -3, 7 ), 
        Vectorf( 3, 7 ), 
        Vectorf( 9, 7 ), 
        Vectorf( 14, 9 ), 
        Vectorf( 19, 11 ), 
        Vectorf( 24, 14 ),
        Vectorf( 28, 18 ), 
        Vectorf( 30, 22 ), 
        Vectorf( 32, 26 ), 
        Vectorf( 31, 30 ), 
        Vectorf( 30, 34 ), 
        Vectorf( 27, 37 ), 
        Vectorf( 24, 40 ), 
        Vectorf( 20, 42 ), 
        Vectorf( 15, 43 ), 
        Vectorf( 10, 43 ),
        Vectorf( 5, 44 ), 
        Vectorf( -1, 44 ), 
        Vectorf( -8, 44 ), 
        Vectorf( -15, 43 ), 
        Vectorf( -21, 41 ), 
        Vectorf( -26, 37 ), 
        Vectorf( -28, 34 ), 
        Vectorf( -30, 30 ), 
        Vectorf( -31, 27 ), 
        Vectorf( -31, 23 ),
        Vectorf( -30, 20 ),
        Vectorf( -27, 16 ),
    }
};

AnimationSet::AnimationSet( int directions, int frames, const char* path )
    : m_directions( directions )
    , m_frames( frames )
    , m_gfx( new GfxAssetPtr*[directions] )
{
    if( directions == 32 )
    {
        const char** ptr = d32;
        for( int i=0; i<directions; i++ )
        {
            m_gfx[i] = new GfxAssetPtr[frames];
            for( int j=0; j<frames; j++ )
            {
                char buf[128];
                sprintf( buf, "%s_%s_%03i.png.pivot@linear", path, *ptr, j+1 );
                m_gfx[i][j].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
            }
            ptr++;
        }
    }
    else if( directions == 16 )
    {
        const char** ptr = d16;
        for( int i=0; i<directions; i++ )
        {
            m_gfx[i] = new GfxAssetPtr[frames];
            for( int j=0; j<frames; j++ )
            {
                char buf[128];
                sprintf( buf, "%s_%s_%03i.png.pivot@linear", path, *ptr, j+1 );
                m_gfx[i][j].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
            }
            ptr++;
        }
    }
    else if( directions == 8 )
    {
        const char** ptr = d8;
        for( int i=0; i<directions; i++ )
        {
            m_gfx[i] = new GfxAssetPtr[frames];
            for( int j=0; j<frames; j++ )
            {
                char buf[128];
                sprintf( buf, "%s_%s_%03i.png.pivot@linear", path, *ptr, j+1 );
                m_gfx[i][j].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
            }
            ptr++;
        }
    }
    else if( directions == 1 )
    {
        m_gfx[0] = new GfxAssetPtr[frames];
        for( int j=0; j<frames; j++ )
        {
            char buf[128];
            sprintf( buf, "%s_000_%03i.png.pivot@linear", path, j+1 );
            m_gfx[0][j].Reset( Claw::AssetDict::Get<GfxAsset>( buf ) );
        }
    }
    else
    {
        CLAW_MSG_ASSERT( false, "Wrong number of directions specified." );
    }
}

AnimationSet::~AnimationSet()
{
    for( int i=0; i<m_directions; i++ )
    {
        delete[] m_gfx[i];
    }
    delete[] m_gfx;
}

GfxAsset* AnimationSet::GetAsset( const Vectorf& dir, int frame, int& lastDir ) const
{
    CLAW_ASSERT( frame < m_frames );

    int i;
    if( m_directions == 8 )
    {
        i = lastDir;
        int k = TranslateFor8( dir );
        int a = abs( lastDir - k );
        if( a != 0 )
        {
            if( a == 1 || a == 7 )
            {
                int j = TranslateFor32( dir );
                a = abs( lastDir * 4 - j );
                if( a > 2 && a < 30 )
                {
                    i = k;
                }
            }
            else
            {
                i = k;
            }
        }
        lastDir = i;
    }
    else if( m_directions == 16 )
    {
        i = TranslateFor16( dir );
    }
    else if( m_directions == 32 )
    {
        i = TranslateFor32( dir );
    }
    else
    {
        i = 0;
    }
    return m_gfx[i][frame];
}

int AnimationSet::TranslateFor8( const Vectorf& dir )
{
    int i;

    if( dir.y > 0.923f ) // 45/2 deg
    {
        i = 0;
    }
    else if( dir.y < -0.923f )
    {
        i = 4;
    }
    else if( dir.x > 0.923f )
    {
        i = 2;
    }
    else if( dir.x < -0.923f )
    {
        i = 6;
    }
    else if( dir.x * dir.y > 0 )
    {
        if( dir.x > 0 )
        {
            i = 1;
        }
        else
        {
            i = 5;
        }
    }
    else
    {
        if( dir.x > 0 )
        {
            i = 3;
        }
        else
        {
            i = 7;
        }
    }

    return i;
}

int AnimationSet::TranslateFor16( const Vectorf& dir )
{
    int i;

    if( dir.y > 0.981f ) // 22.5/2 deg
    {
        i = 0;
    }
    else if( dir.y < -0.981f )
    {
        i = 10;
    }
    else if( dir.x > 0.981f )
    {
        i = 8;
    }
    else if( dir.x < -0.981f )
    {
        i = 6;
    }
    else if( dir.x * dir.y > 0 )
    {
        if( dir.x > 0 )
        {
            if( dir.y > 0.831 ) // 22.5/2+22.5 deg
            {
                i = 1;
            }
            else if( dir.x > 0.831 )
            {
                i = 5;
            }
            else
            {
                i = 4;
            }
        }
        else
        {
            if( dir.y < -0.831 ) // 22.5/2+22.5 deg
            {
                i = 15;
            }
            else if( dir.x < -0.831 )
            {
                i = 12;
            }
            else
            {
                i = 11;
            }
        }
    }
    else
    {
        if( dir.x > 0 )
        {
            if( dir.y < -0.831 ) // 22.5/2+22.5 deg
            {
                i = 14;
            }
            else if( dir.x > 0.831 )
            {
                i = 9;
            }
            else
            {
                i = 13;
            }
        }
        else
        {
            if( dir.y > 0.831 ) // 22.5/2+22.5 deg
            {
                i = 3;
            }
            else if( dir.x < -0.831 )
            {
                i = 7;
            }
            else
            {
                i = 2;
            }
        }
    }

    return i;
}

int AnimationSet::TranslateFor32( const Vectorf& dir )
{
    int i;


    if( dir.y > 0.995184727f ) // 11.25/2 deg
    {
        i = 0;
    }
    else if( dir.y < -0.995184727f )
    {
        i = 16;
    }
    else if( dir.x > 0.995184727f )
    {
        i = 8;
    }
    else if( dir.x < -0.995184727f )
    {
        i = 24;
    }
    else if( dir.x * dir.y > 0 )
    {
        if( dir.x > 0 )
        {
            if( dir.x < 0.290284677f )
            {
                i = 1;
            }
            else if( dir.x < 0.471396737f )
            {
                i = 2;
            }
            else if( dir.x < 0.634393284f )
            {
                i = 3;
            }
            else if( dir.x < 0.773010453f )
            {
                i = 4;
            }
            else if( dir.x < 0.881921264f )
            {
                i = 5;
            }
            else if( dir.x < 0.956940336f )
            {
                i = 6;
            }
            else if( dir.x < 0.995184727f )
            {
                i = 7;
            }
        }
        else
        {
            if( -dir.x < 0.290284677f )
            {
                i = 17;
            }
            else if( -dir.x < 0.471396737f )
            {
                i = 18;
            }
            else if( -dir.x < 0.634393284f )
            {
                i = 19;
            }
            else if( -dir.x < 0.773010453f )
            {
                i = 20;
            }
            else if( -dir.x < 0.881921264f )
            {
                i = 21;
            }
            else if( -dir.x < 0.956940336f )
            {
                i = 22;
            }
            else if( -dir.x < 0.995184727f )
            {
                i = 23;
            }
        }
    }
    else
    {
        if( dir.x > 0 )
        {
            if( dir.x < 0.290284677f )
            {
                i = 15;
            }
            else if( dir.x < 0.471396737f )
            {
                i = 14;
            }
            else if( dir.x < 0.634393284f )
            {
                i = 13;
            }
            else if( dir.x < 0.773010453f )
            {
                i = 12;
            }
            else if( dir.x < 0.881921264f )
            {
                i = 11;
            }
            else if( dir.x < 0.956940336f )
            {
                i = 10;
            }
            else if( dir.x < 0.995184727f )
            {
                i = 9;
            }
        }
        else
        {
            if( -dir.x < 0.290284677f )
            {
                i = 31;
            }
            else if( -dir.x < 0.471396737f )
            {
                i = 30;
            }
            else if( -dir.x < 0.634393284f )
            {
                i = 29;
            }
            else if( -dir.x < 0.773010453f )
            {
                i = 28;
            }
            else if( -dir.x < 0.881921264f )
            {
                i = 27;
            }
            else if( -dir.x < 0.956940336f )
            {
                i = 26;
            }
            else if( -dir.x < 0.995184727f )
            {
                i = 25;
            }
        }
    }

    return i;
}

const char* AnimationSet::GetFrameAngle8( int idx )
{
    return d8[idx];
}

const char* AnimationSet::GetFrameAngle16( int idx )
{
    return d16[idx];
}

const Vectorf& AnimationSet::GetShotPos16( int idx )
{
    return sp16[idx];
}

Vectorf AnimationSet::GetShotPos32( int idx, int weapon )
{
    if( weapon < 0 )
    {
        const Vectorf& v = sp32d[idx];
        return Vectorf( v.x, -v.y );
    }
    else
    {
        const Vectorf& v = sp32[weapon][idx];
        return Vectorf( v.x, -v.y );
    }
}

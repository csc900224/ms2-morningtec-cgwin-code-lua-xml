#include "claw/base/Errors.hpp"
#include "claw/base/Registry.hpp"

#include "MonstazAI/AnalyticsManager.hpp"
#include "MonstazAI/CashTimer.hpp"
#include "MonstazAI/ReminderPopup.hpp"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define REMINDER_POPUP_TAG  838

bool g_popupSraka = false;

@interface Popup2 : NSObject
{
}

- (void) show;
- (void) alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex;

@end

@implementation Popup2

- (void) show
{
    g_popupSraka = true;
    UIAlertView* popup = [[[UIAlertView alloc]
                          initWithTitle:@"Free Monster Cash"
                          message:@"Do you want to be notified about free Monster Cash?"
                          delegate:self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes", nil] autorelease];

    [popup setTag:REMINDER_POPUP_TAG];
    [popup show];
}

- (void) alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if ( alertView.tag != REMINDER_POPUP_TAG )
    {
        return;
    }
    
    g_popupSraka = false;
    
    NSString* title = [alertView buttonTitleAtIndex:buttonIndex];
    if ( [title isEqualToString:@"Yes"] )
    {
        //Claw::Registry::Get()->Set( "/monstaz/settings/notifications", true );
        //SystemNotification::Notify( CashTimer::GetInstance()->Target() );
    }
}

@end

static Popup2* s_popup = NULL;

void ReminderPopup::Initialize()
{
    if ( !s_popup )
    {
        s_popup = [[Popup2 alloc] init];
    }
}

void ReminderPopup::Release()
{
    if ( s_popup )
    {
        [s_popup release];
        s_popup = NULL;
    }
}

void ReminderPopup::Show()
{
    CLAW_ASSERT( s_popup );
    bool agreed = Claw::Registry::Get()->CheckBool( "/monstaz/settings/notifications" );
    if( !agreed )
    {
        [s_popup show];
    }
    else
    {
        //SystemNotification::Notify( CashTimer::GetInstance()->Target() );
    }
}

#include "claw/base/AssetDict.hpp"
#include "claw/math/Math.hpp"

#include "MonstazAI/Application.hpp"
#include "MonstazAI/Achievements.hpp"
#include "MonstazAI/AudioManager.hpp"

static Achievements* s_instance = NULL;

Achievements::Achievements()
    : m_scale( ((MonstazApp*)MonstazApp::GetInstance())->GetGameScale() )
    , m_font1( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_normal.xml@linear" ) )
    , m_font2( Claw::AssetDict::Get<Claw::FontEx>( "menu2/font_normal.xml@linear" ) )
    , m_state( 0 )
    , m_time( 0 )
    , m_iconId( 0 )
{
    CLAW_ASSERT( !s_instance );
    s_instance = this;

    Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
    fontSet->AddFont( "f", m_font1 );

    Claw::Text::Format format;
    format.SetFontSet( fontSet );
    format.SetFontId( "f" );
    format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );

    m_text1.Reset( new Claw::ScreenText( format, Claw::String( "achievement unlocked!" ), Claw::Extent( ( 270 - 30 ) * m_scale, 0 ) ) );

    m_icon[0].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/achiev_rank.png@linear" ) );
    m_icon[1].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/achiev_stars_01.png@linear" ) );
    m_icon[2].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/achiev_stars_02.png@linear" ) );
    m_icon[3].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/achiev_stars_03.png@linear" ) );
    m_icon[4].Reset( Claw::AssetDict::Get<Claw::Surface>( "menu/gfx/achiev_stars_04.png@linear" ) );
}

Achievements::~Achievements()
{
    CLAW_ASSERT( s_instance );
    s_instance = NULL;
}

void Achievements::Render( Claw::Surface* target )
{
    if( m_state == 0 )
    {
        return;
    }

    m_font1->GetSurface()->SetAlpha( 255 );
    m_font2->GetSurface()->SetAlpha( 255 );

    int scale = 0;
    if( target->GetWidth() >= 800 || target->GetHeight() >= 800 )
    {
        scale = 1;
    }

    target->DrawFilledRectangle( Claw::Rect( ( target->GetWidth() - ( 270 << scale ) ) / 2, ( -35 << scale ) * ( 1 - m_pos ), 270 << scale, 35 << scale ), Claw::MakeRGBA( 0, 0, 0, 165 ) );
    target->Blit( ( target->GetWidth() - ( 270 << scale ) ) / 2 + (5 << scale), ( -35 << scale ) * ( 1 - m_pos ) + (3 << scale), m_icon[m_iconId] );

    if( m_text2 )
    {
        m_text2->Draw( target, ( target->GetWidth() - ( 270 << scale ) ) / 2 + ( 30 << scale ), ( -35 << scale ) * ( 1 - m_pos ) + (4 << scale) );
        m_text1->Draw( target, ( target->GetWidth() - ( 270 << scale ) ) / 2 + ( 30 << scale ), ( -35 << scale ) * ( 1 - m_pos ) + (14 << scale) );
    }    
}

void Achievements::Update( float dt )
{
    m_time += dt;

    switch( m_state )
    {
    case 0:
        break;
    case 1:
        m_pos = Claw::SmoothStep( 0.f, 0.25f, m_time );
        if( m_time > 0.25f )
        {
            m_state = 2;
            m_time = 0;
        }
        break;
    case 2:
        if( m_time > 3 )
        {
            m_state = 3;
            m_time = 0;
        }
        break;
    case 3:
        m_pos = Claw::SmoothStep( 0.f, 0.25f, 0.25f - m_time );
        if( m_time > 0.25f )
        {
            m_state = 0;
            m_time = 0;
        }
        break;
    default:
        break;
    }
}

void Achievements::Show( const Claw::NarrowString& str, int id )
{
    Claw::Text::FontSetPtr fontSet( new Claw::Text::FontSet() );
    fontSet->AddFont( "f", m_font2 );

    Claw::Text::Format format;
    format.SetFontSet( fontSet );
    format.SetFontId( "f" );
    format.SetHorizontalAlign( Claw::Text::Format::HA_CENTER );

    m_text2.Reset( new Claw::ScreenText( format, Claw::String( str ), Claw::Extent( ( 270 - 30 ) * m_scale, 0 ) ) );

    m_time = 0;
    m_state = 1;
    m_iconId = id;

    AudioManager::GetInstance()->Play( SFX_MENU_ROTATE );
}

Achievements* Achievements::GetInstance()
{
    CLAW_ASSERT( s_instance );
    return s_instance;
}
